
<?php

//if ( ! defined( YITH_YWPI_VERSION ) ) {
//    exit( 'Direct access forbidden.' );
//}


/*
* This file belongs to the YIT Framework.
*
* This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.gnu.org/licenses/gpl-3.0.txt
*/

if ( isset( $_POST['yith_ywpi_fields_names_settings_field'] ) && wp_verify_nonce( $_POST['yith_ywpi_fields_names_settings_field'], 'yith_ywpi_fields_names_settings_action' ) ) {

    if ( isset( $_POST['ywpi_invoice_product_column_name'] ) ) {
        update_option('ywpi_invoice_product_column_name', $_POST['ywpi_invoice_product_column_name'] );
    }

}

$product_column = get_option( 'ywpi_invoice_product_column_name', true );

?>

<div id="yith_ywpi_main_div">

    <form action="" method="post">

        <?php wp_nonce_field( 'yith_ywpi_fields_names_settings_action', 'yith_ywpi_fields_names_settings_field' ); ?>

        <h3>Columns headers names</h3>
        <table class="form-table">
            <tbody>

            <tr valign="top">
                <th>
                    <label for="ywpi_invoice_product_column_name">Product</label>
                </th>
                <td>
                    <input name="ywpi_invoice_product_column_name" id="ywpi_invoice_product_column_name" value="<?php echo $product_column; ?>" placeholder="Product" type="text">
                </td>
            </tr>

            </tbody></table>

        <input type="submit" class="button-primary" value="<?php _e( 'Save Changes', 'yith-woocommerce-pdf-invoice' ); ?>">

    </form>

</div>










