<?php

if ( class_exists( 'Gravity_Flow_Step' ) ) {

	class Gravity_Flow_Step_Incoming_Webhook extends Gravity_Flow_Step {
		public $_step_type = 'incoming_webhook';

		public function get_icon_url() {
			return '<i class="fa fa-sign-in" aria-hidden="true"></i>';
		}

		public function get_label() {
			return esc_html__( 'Incoming Webhook', 'gravityflowincomingwebhook' );
		}

		public function get_settings() {
			$settings = array(
				'title'  => __( 'Incoming Webhook', 'gravityflowincomingwebhook' ),
				'fields' => array(
					array(
						'name'     => 'application',
						'class'         => 'medium',
						'label'    => esc_html__( 'Application Name', 'gravityflowincomingwebhook' ),
						'type'     => 'text',
						'required' => true,
					),
					array(
						'name'          => 'api_key',
						'label'         => esc_html__( 'API Key', 'gravityflowincomingwebhook' ),
						'type'          => 'text',
						'tooltip'   => __( 'Enter a unique string that will be used to identify the application. Warning: If this value is changed then all entries that are on this step will not be able to continue the workflow.', 'gravityflowincomingwebhook' ),
						'class'         => 'medium',
						'default_value' => wp_hash( uniqid() ),
						'required'      => true,
					),
					array(
						'name'          => 'api_secret',
						'class'         => 'medium',
						'tooltip'   => __( 'Enter a unique string that will be used to authenticate the request. The value should be changed periodically.', 'gravityflowincomingwebhook' ),
						'label'         => esc_html__( 'API Secret', 'gravityflowincomingwebhook' ),
						'type'          => 'text',
						'default_value' => wp_hash( uniqid() ),
						'required'      => true,
					),
					array(
						'name'    => 'display_fields',
						'label'   => __( 'Display Fields', 'gravityflowincomingwebhook' ),
						'tooltip' => __( 'Select the fields to hide or display.', 'gravityflowincomingwebhook' ),
						'type'    => 'display_fields',
					),
					array(
						'name' => 'body',
						'label' => esc_html__( 'Request Mapping', 'gravityflow' ),
						'type' => 'radio',
						'default_value' => '',
						'horizontal' => true,
						'onchange'    => "jQuery(this).closest('form').submit();",
						'choices' => array(
							array(
								'label' => __( 'None', 'gravityflow' ),
								'value' => '',
							),
							array(
								'label' => __( 'Select Fields', 'gravityflow' ),
								'value' => 'select_fields',
							),
						),
					),
					array(
						'name'                => 'mappings',
						'label'               => esc_html__( 'Field Values', 'gravityflow' ),
						'type'                => 'generic_map',
						'enable_custom_key'   => false,
						'enable_custom_value' => false,
						'key_field_title'     => esc_html__( 'Key', 'gravityflow' ),
						'value_field_title'   => esc_html__( 'Field', 'gravityflow' ),
						'value_choices'       => $this->value_mappings(),
						'tooltip'             => '<h6>' . esc_html__( 'Mapping', 'gravityflow' ) . '</h6>' . esc_html__( 'Map the fields of the incoming request to the selected form. Values from the request will be saved in the entry in the selected form', 'gravityflow' ),
						'dependency'          => array(
							'field'  => 'body',
							'values' => array( 'select_fields' ),
						),
					),
				),
			);

			return $settings;
		}

		public function evaluate_status() {

			if ( $this->is_queued() ) {
				return 'queued';
			}

			$assignee_details = $this->get_assignees();

			$step_status = 'complete';

			foreach ( $assignee_details as $assignee ) {
				$user_status = $assignee->get_status();

				if ( empty( $user_status ) || $user_status == 'pending' ) {
					$step_status = 'pending';
				}
			}

			return $step_status;
		}

		public function get_assignees() {

			$assignees = array();

			$assignee_key = 'api_key|' . $this->api_key;

			$assignees[] = new Gravity_Flow_Assignee( $assignee_key, $this );

			return $assignees;
		}

		/**
		 * Prepare value map.
		 *
		 * @return array
		 */
		public function value_mappings() {

			$form = $this->get_form();

			$fields = gravity_flow_incoming_webhook()->get_field_map_choices( $form['id'], null, array( 'fileupload', 'post_title', 'post_content', 'post_excerpt', 'post_tags', 'post_category', 'post_image', 'post_custom_field', 'product', 'singleproduct', 'quantity', 'option', 'shipping', 'singleshipping', 'total' ) );

			return $fields;
		}

		public function process() {
			/**
			* Fires when the workflow is first assigned to the incoming webhook step.
			*
			* @since 2.0.2
			*
			* @param array $entry The current entry.
			* @param array $form The current form.
			* @param array $step The current step.
			*/
			do_action( 'gravityflowincomingwebhook_step_started', $this->get_entry(), $this->get_form(), $this );
			$this->assign();
		}

		/**
		 * Performs the mappings from response to entry
		 *
		 * @since 1.1-dev
		 *
		 * @param array $data         The request data to map into $entry.
		 * @param array $content_type The content type of incoming request
		 */
		public function do_request_entry_mapping( $data, $content_type ) {

			gravity_flow()->log_debug( __METHOD__ . '(): Mapping request data into $entry' );
			$this->log_debug( __METHOD__ . '(): Mapping request data into $entry' );

			if ( ! is_array( $this->mappings ) ) {
				$this->log_debug( __METHOD__ . '(): Step has no mappings defined' );
				return;
			}

			$entry = $this->get_entry();

			foreach ( $this->mappings as $mapping ) {
				if ( rgblank( $mapping['key'] ) ) {
					continue;
				}

				$entry = $this->add_mapping_to_entry( $entry, $mapping, $data, $content_type );
			}

			$result = GFAPI::update_entry( $entry );

			if ( is_wp_error( $result ) ) {
				$this->log_debug( __METHOD__ . '(): There was an issue with updating the entry.' );
			} else {
				$this->log_debug( __METHOD__ . '(): Updated entry: ' . print_r( $entry, true ) );
			}
		}

		/**
		 * Add the mapped value to the entry.
		 *
		 * @since 1.1-dev
		 *
		 * @param array $entry        The entry to update.
		 * @param array $mapping      The properties for the mapping being processed.
		 * @param array $data         The request params
		 * @param array $content_type The content type of incoming request
		 *
		 * @return array
		 */
		public function add_mapping_to_entry( $entry, $mapping, $data, $content_type ) {

			$skip_mapping = false;

			$source_field_id = trim( $mapping['custom_key'] );

			$target_field_id = (string) $mapping['value'];

			if ( ! isset( $data[ $source_field_id ] ) ) {
				return $entry;
			}

			$form = $this->get_form();

			$target_field = GFFormsModel::get_field( $form, $target_field_id );

			if ( $target_field instanceof GF_FIELD ) {

				if ( in_array( $target_field->type, array( 'fileupload', 'post_title', 'post_content', 'post_excerpt', 'post_tags', 'post_category', 'post_image', 'post_custom_field', 'product', 'singleproduct', 'quantity', 'option', 'shipping', 'singleshipping', 'total' ) ) ) {
					$skip_mapping = true;
				} else {
					$this->log_debug( __METHOD__ . '(): Mapping into Field #' . $target_field->id . ' / Type: ' . $target_field->type );

					$is_full_target      = $target_field_id === (string) intval( $target_field_id );
					$target_field_inputs = $target_field->get_entry_inputs();

					//Non-Choice Field Type
					if ( $is_full_target && ! is_array( $target_field_inputs ) ) {

						if ( rgar( $content_type, 'subtype' ) == 'json' && in_array( $target_field->type, array( 'multiselect', 'workflow_multi_user' ) ) ) {

							if ( ! is_array( $data[ $source_field_id ] ) ) {
								$data[ $source_field_id ] = json_decode( $data[ $source_field_id ] );
							}

							$entry[ $target_field_id ] = $target_field->get_value_save_entry( $data[ $source_field_id ], $form, false, $entry['id'], $entry );

						} elseif ( in_array( $target_field->type, array( 'workflow_discussion' ) ) ) {
							$entry[ $target_field_id ] = $target_field->get_value_save_entry( $data[ $source_field_id ], $form, false, $entry['id'], $entry );
						} else {
							$entry[ $target_field_id ] = $target_field->sanitize_entry_value( $data[ $source_field_id ],  $form['id'] );
						}

						//Choice Field Types
					} elseif ( is_array( $target_field_inputs ) ) {

						//Received Parent Input ID
						if ( $target_field_id == $target_field['id'] ) {

							if ( is_array( $data[ $source_field_id ] ) ) {
								$choices = $data[ $source_field_id ];
							} else {
								$choices = json_decode( $data[ $source_field_id ], true );
							}

							foreach ( $choices as $source_field ) {

								$entry[ $source_field['id'] ] = $target_field->sanitize_entry_value( $source_field['value'], $form['id'] );
							}

							//Received Direct Input ID
						} else {
							foreach ( $target_field_inputs as $input ) {
								if ( $target_field_id === $input['id'] ) {
									$entry[ $target_field_id ] = $target_field->sanitize_entry_value( $data[ $source_field_id ], $form['id'] );
									break;
								}
							}
						}
					} else {
						$skip_mapping = true;
					}
				}
			} else {
				$skip_mapping = true;
			}

			if ( $skip_mapping ) {
				if ( is_object( $target_field ) ) {
					$this->log_debug( __METHOD__ . '(): Field Type ' . $target_field->type . ' - Not available for import yet.' );
				} else {
					$this->log_debug( __METHOD__ . '(): Incoming field mapping error.' );
				}
			}
			return $entry;
		}

		public function workflow_detail_box( $form, $args ) {
			$status_str        = __( 'Pending', 'gravityflowincomingwebhook' );
			$approve_icon      = '<i class="fa fa-check" style="color:green"></i>';
			$input_step_status = $this->get_status();
			if ( $input_step_status == 'complete' ) {
				$status_str = $approve_icon . __( 'Complete', 'gravityflowincomingwebhook' );
			} elseif ( $input_step_status == 'queued' ) {
				$status_str = __( 'Queued', 'gravityflowincomingwebhook' );
			}
			$display_step_status = (bool) $args['step_status'];
			?>
			<h4 style="margin-bottom:10px;"><?php echo $this->get_name() . ' (' . $status_str . ')'; ?></h4>
			<?php if ( $display_step_status ) : ?>
				<div>

					<ul>
						<?php
						$assignees = $this->get_assignees();

						$this->log_debug( __METHOD__ . '(): assignee details: ' . print_r( $assignees, true ) );

						foreach ( $assignees as $assignee ) {

							$this->log_debug( __METHOD__ . '(): showing status for: ' . $assignee->get_key() );

							$assignee_status = $assignee->get_status();

							$this->log_debug( __METHOD__ . '(): assignee status: ' . $assignee_status );

							if ( ! empty( $assignee_status ) ) {

								$assignee_type = $assignee->get_type();

								if ( $assignee_type == 'api_key' ) {
									$status_label = $this->get_status_label( $assignee_status );
									echo sprintf( '<li>%s: %s (%s)</li>', esc_html__( 'Application', 'gravityflowincomingwebhook' ), $this->application, $status_label );
								}
							}
						}

						?>
					</ul>
				</div>

				<?php
			endif;
		}

		public function entry_detail_status_box( $form ) {
			$status = $this->get_status();
			?>
			<h4 style="padding:10px;"><?php echo $this->get_name() . ': ' . $status; ?></h4>

			<div style="padding:10px;">
				<ul>
					<?php

					$assignees = $this->get_assignees();

					foreach ( $assignees as $assignee ) {

						$assignee_type = $this->get_type();

						$status = $assignee->get_status();

						if ( ! empty( $user_status ) ) {
							$status_label = $this->get_status_label( $status );
							switch ( $assignee_type ) {
								case 'api':
									echo sprintf( '<li>%s: %s (%s)</li>', esc_html__( 'Application', 'gravityflowincomingwebhook' ), $this->application, $status_label );
									break;

							}
						}
					}

					?>
				</ul>
			</div>
			<?php
		}

		/**
		 * Logs debug messages to the Gravity Flow Incoming Webhook log file generated by the Gravity Forms Logging Add-On.
		 *
		 * @param string $message The message to be logged.
		 */
		public function log_debug( $message ) {
			gravity_flow_incoming_webhook()->log_debug( $message );
		}
	}
}



