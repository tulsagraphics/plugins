=== Gravity Flow Incoming Webhook Extension ===
Contributors: stevehenty
Tags: gravity forms, approvals, workflow
Requires at least: 4.0
Tested up to: 4.2.2
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Gravity Flow Incoming Webhook Extension is a commercial plugin for WordPress which adds incoming webhooks to Gravity Flow.

== Description ==

Gravity Flow is an Add-On for [Gravity Forms](https://gravityflow.io/gravityforms)

Facebook: [Gravity Flow](https://www.facebook.com/gravityflow.io)

= Requirements =

1. [Purchase and install Gravity Forms](https://gravityflow.io/gravityforms)
1. [Purchase and install Gravity Flow](https://gravityflow.io)
1. Wordpress 4.2+
1. Gravity Forms 1.9.4+
1. Gravity Flow 1.1+


= Support =
If you find any that needs fixing, or if you have any ideas for improvements, please get in touch:
https://gravityflow.io/contact/


== Installation ==

1.  Download the zipped file.
1.  Extract and upload the contents of the folder to /wp-contents/plugins/ folder
1.  Go to the Plugin management page of WordPress admin section and enable the 'Gravity Flow PDF Extension' plugin.

== Frequently Asked Questions ==

= Which license of Gravity Flow do I need? =
The Gravity Flow PDF Extension will work with any license of [Gravity Flow](https://gravityflow.io).


== ChangeLog ==

= 1.1 =
Added request mapping settings to step enabling incoming requests to update entry values based on Form or JSON data.

= 1.0 =
All new!

