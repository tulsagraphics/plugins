<?php
/*
Plugin Name: Gravity Flow Incoming Webhook
Plugin URI: https://gravityflow.io
Description: Incoming Webhook Extension for Gravity Flow.
Version: 1.1
Author: Gravity Flow
Author URI: https://gravityflow.io
License: GPL-3.0+

------------------------------------------------------------------------
Copyright 2016-2018 Steven Henty S.L.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

define( 'GRAVITY_FLOW_INCOMING_WEBHOOK_VERSION', '1.1' );

define( 'GRAVITY_FLOW_INCOMING_WEBHOOK_EDD_STORE_URL', 'https://gravityflow.io' );

define( 'GRAVITY_FLOW_INCOMING_WEBHOOK_EDD_ITEM_NAME', 'Incoming Webhook' );

add_action( 'gravityflow_loaded', array( 'Gravity_Flow_Incoming_Webhook_Bootstrap', 'load' ), 1 );

class Gravity_Flow_Incoming_Webhook_Bootstrap {

	public static function load() {

		require_once( 'includes/class-step-incoming-webhook.php' );

		Gravity_Flow_Steps::register( new Gravity_Flow_Step_Incoming_Webhook() );

		require_once( 'class-incoming-webhook.php' );

		// Registers the class name with GFAddOn.
		GFAddOn::register( 'Gravity_Flow_Incoming_Webhook' );
	}
}


add_action( 'init', 'gravityflow_incoming_webhook_edd_plugin_updater', 0 );

function gravityflow_incoming_webhook_edd_plugin_updater() {

	if ( ! function_exists( 'gravity_flow_incoming_webhook' ) ) {
		return;
	}

	$gravity_flow_incoming_webhook = gravity_flow_incoming_webhook();
	if ( $gravity_flow_incoming_webhook ) {
		$settings = $gravity_flow_incoming_webhook->get_app_settings();

		$license_key = trim( rgar( $settings, 'license_key' ) );

		$edd_updater = new Gravity_Flow_EDD_SL_Plugin_Updater( GRAVITY_FLOW_EDD_STORE_URL, __FILE__, array(
			'version'   => GRAVITY_FLOW_INCOMING_WEBHOOK_VERSION,
			'license'   => $license_key,
			'item_name' => GRAVITY_FLOW_INCOMING_WEBHOOK_EDD_ITEM_NAME,
			'author'    => 'Steven Henty',
		) );
	}

}

function gravity_flow_incoming_webhook() {
	if ( class_exists( 'Gravity_Flow_Incoming_Webhook' ) ) {
		return Gravity_Flow_Incoming_Webhook::get_instance();
	}
}
