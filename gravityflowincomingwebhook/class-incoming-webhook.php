<?php // Make sure Gravity Forms is active and already loaded.
if ( class_exists( 'GFForms' ) ) {
	class Gravity_Flow_Incoming_Webhook extends Gravity_Flow_Extension {

		private static $_instance = null;

		public $_version = GRAVITY_FLOW_INCOMING_WEBHOOK_VERSION;

		public $edd_item_name = GRAVITY_FLOW_INCOMING_WEBHOOK_EDD_ITEM_NAME;

		// The Framework will display an appropriate message on the plugins page if necessary
		protected $_min_gravityforms_version = '1.9.10';

		protected $_slug = 'gravityflowincomingwebhook';

		protected $_path = 'gravityflowincomingwebhook/incomingwebhook.php';

		protected $_full_path = __FILE__;

		// Title of the plugin to be used on the settings page, form settings and plugins page.
		protected $_title = 'Incoming Webhook Extension';

		// Short version of the plugin title to be used on menus and other places where a less verbose string is useful.
		protected $_short_title = 'Incoming Webhook';

		protected $_capabilities = array(
			'gravityflowincomingwebhook_uninstall',
			'gravityflowincomingwebhook_settings',
		);

		protected $_capabilities_app_settings = 'gravityflowincomingwebhook_settings';
		protected $_capabilities_uninstall = 'gravityflowincomingwebhook_uninstall';

		public static function get_instance() {
			if ( self::$_instance == null ) {
				self::$_instance = new Gravity_Flow_Incoming_Webhook();
			}

			return self::$_instance;
		}

		private function __clone() {
		} /* do nothing */

		public function pre_init() {
			parent::pre_init();

			// Gravity Forms Web API Version 1
			add_action( 'gform_webapi_post_entries_workflow-hooks', array( $this, 'get_entries_workflow_hooks' ), 10, 2 );
			add_filter( 'gform_webapi_authentication_required_post_entries_workflow-hooks', '__return_false' );

			// Gravity Forms Web API Version 2

			add_action( 'rest_api_init', function () {
				$route      = '/entries/(?P<entry_id>[\d]+)/workflow-hooks';
				$route      = apply_filters( 'gravityflowincomingwebhook_route', $route );
				$route_args = array(
					'methods'             => 'POST',
					'callback'            => array( $this, 'process_incoming_webhook' ),
					'permission_callback' => array( $this, 'check_permissions' ),
					'args'                => array(
						'entry_id'            => array(
							'required'          => true,
							'validate_callback' => function ( $param, $request, $key ) {
								return is_numeric( $param );
							},
							'sanitize_callback' => function ( $param, $request, $key ) {
								return absint( $param );
							},
						),
						'workflow-api-key'    => array(
							'required'          => true,
							'sanitize_callback' => function ( $param, $request, $key ) {
								return sanitize_text_field( $param );
							},
						),
						'workflow-api-secret' => array(
							'required'          => true,
							'sanitize_callback' => function ( $param, $request, $key ) {
								return sanitize_text_field( $param );
							},
						),
					),
				);
				$route_args = apply_filters( 'gravityflowincomingwebhook_route_args', $route_args );

				register_rest_route( 'gf/v2', $route, $route_args );
			} );
		}

		/**
		 * Add the extension capabilities to the Gravity Flow group in Members.
		 *
		 * @since 1.1-dev
		 *
		 * @param array $caps The capabilities and their human readable labels.
		 *
		 * @return array
		 */
		public function get_members_capabilities( $caps ) {
			$prefix = $this->get_short_title() . ': ';

			$caps['gravityflowincomingwebhook_settings']  = $prefix . __( 'Manage Settings', 'gravityflowincomingwebhook' );
			$caps['gravityflowincomingwebhook_uninstall'] = $prefix . __( 'Uninstall', 'gravityflowincomingwebhook' );

			return $caps;
		}

		/**
		 * Legacy support for version 1 of the Gravity Forms Web API.
		 *
		 * @param $entry_id
		 */
		public function get_entries_workflow_hooks( $entry_id ) {
			$request = new WP_REST_Request( 'POST', '/gf/v2/entries/' . $entry_id . '/workflow-hooks' );

			$params = array(
				'workflow-api-key'    => $_REQUEST['workflow-api-key'],
				'workflow-api-secret' => $_REQUEST['workflow-api-secret'],
				'entry_id'            => $entry_id,
			);
			$request->set_url_params( $params );
			$permission = $this->check_permissions( $request );

			if ( ! $permission ) {
				GFWebAPI::end( 401, __( 'Unauthorized', 'gravityflowincomingwebhook' ) );
			}

			/* @var WP_REST_Response $response */
			$rest_response = $this->process_incoming_webhook( $request );

			GFWebAPI::end( $rest_response->get_status(), $rest_response->get_data() );
		}

		/**
		 * Checks whether the request can be fulfilled. Checks permissions but will also bail if the entry is not on an incoming webhook step.
		 *
		 * @param WP_Rest_Request $request
		 *
		 * @return bool
		 */
		public function check_permissions( $request ) {

			$has_permission = null;

			/**
			 * Provides a way for developers to override the permissions check.
			 *
			 * @param null            $has_permission Return true or false to override. Return null to continue with the permissions check.
			 * @param WP_Rest_Request $request        The current WP_Rest_Request object.
			 */
			$has_permission = apply_filters( 'gravityflowincomingwebhook_check_permissions', $has_permission, $request );

			if ( $has_permission === false || $has_permission === true ) {
				return $has_permission;
			}

			if ( empty( $request['workflow-api-key'] ) ) {
				return false;
			}

			$api_key = $request['workflow-api-key'];

			if ( empty( $request['workflow-api-secret'] ) ) {
				return false;
			}

			$api_secret = $request['workflow-api-secret'];

			if ( empty( $request['entry_id'] ) ) {
				return false;
			}

			$entry_id = $request['entry_id'];

			if ( empty( $entry_id ) ) {
				return false;
			}

			$entry = GFAPI::get_entry( $entry_id );

			if ( is_wp_error( $entry ) ) {
				return false;
			}

			$api = new Gravity_Flow_API( $entry['form_id'] );

			$current_step = $api->get_current_step( $entry );

			if ( ! $current_step ) {
				return false;
			}

			if ( $current_step->get_type() != 'incoming_webhook' ) {
				return false;
			}

			if ( empty( $current_step->api_key ) ) {
				return false;
			}

			if ( $api_key != $current_step->api_key ) {
				return false;
			}

			if ( $api_secret != $current_step->api_secret ) {
				return false;
			}

			return true;
		}

		/**
		 * Handle the request.
		 *
		 * @param WP_REST_Request|array $request
		 *
		 * @return mixed|WP_Error|WP_REST_Response
		 */
		public function process_incoming_webhook( $request ) {

			$response = null;

			/**
			 * Allows the processing of the webhook to be overridden entirely.
			 *
			 * @param null            $response Return a WP_REST_Response object to override.
			 * @param WP_REST_Request $request  The entire request object.
			 */
			$response = apply_filters( 'gravityflowincomingwebhook_response_pre_process', $response, $request );

			if ( ! empty( $response ) ) {
				return $response;
			}

			$content_type = $request->get_content_type();
			$this->log_debug( __METHOD__ . '(): Content Type: ' . $content_type['value'] );

			if ( rgar( $content_type, 'subtype' ) === 'json' ) {
				$data = $request->get_json_params();
			} else {
				$data = $request->get_body_params();
			}

			$this->log_debug( __METHOD__ . '(): Request Data ' . print_r( $data, true ) );

			$entry_id = $request['entry_id'];
			$entry    = GFAPI::get_entry( $entry_id );

			$api          = new Gravity_Flow_API( $entry['form_id'] );
			$current_step = $api->get_current_step( $entry );

			if ( $current_step->body == 'select_fields' ) {
				$current_step->do_request_entry_mapping( $data, $content_type );
			}

			$api_key  = $request['workflow-api-key'];
			$assignee = $current_step->get_assignee( 'api_key|' . $api_key );
			$assignee->update_status( 'complete' );

			$api->process_workflow( $entry_id );

			// refresh entry
			$entry = $current_step->refresh_entry();

			$response = new WP_REST_Response( 'complete', 200 );

			/**
			 * Allows the WP_REST_Response object to be modified after processing.
			 *
			 * @param WP_REST_Response $response
			 * @param array            $entry
			 * @param array            $data    The request data.
			 * @param WP_REST_Request  $request The entire request object.
			 */
			$response = apply_filters( 'gravityflowincomingwebhook_response_post_process', $response, $entry, $data, $request );

			return $response;
		}
	}
}
