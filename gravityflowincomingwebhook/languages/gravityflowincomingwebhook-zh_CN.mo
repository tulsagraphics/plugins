��          �      �       H     I  
   Q     \     h     y  n   �  �   �     �     �  ,   �                 B  "     e  
   m     x  	   |  	   �  r   �  �        �     �  1   �  	   �  	      	   
           
         	                                             API Key API Secret Application Application Name Complete Enter a unique string that will be used to authenticate the request. The value should be changed periodically. Enter a unique string that will be used to identify the application. Warning: If this value is changed then all entries that are on this step will not be able to continue the workflow. Gravity Flow Incoming Webhook Incoming Webhook Incoming Webhook Extension for Gravity Flow. Pending Queued Unauthorized Project-Id-Version: gravityflowincomingwebhook
Report-Msgid-Bugs-To: https://www.gravityflow.io
POT-Creation-Date: 2017-10-22 17:19:48+00:00
PO-Revision-Date: 2017-MO-DA HO:MI+ZONE
Last-Translator: michael edi <michaeledi@163.com>, 2016
Language-Team: Chinese (China) (https://www.transifex.com/gravityflow/teams/50678/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
X-Generator: Gravity Flow Build Script
X-Poedit-Basepath: ../
X-Poedit-Bookmarks: 
X-Poedit-Country: United States
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-SearchPath-0: .
X-Poedit-SourceCharset: utf-8
X-Textdomain-Support: yes
 API Key API Secret App App名称 已完成 输入一个唯一的字符串，将用来对请求进行身份验证。为确保安全，应定期更改此值。 请输入一个唯一字符串，用于标识应用程序。警告︰ 如果更改此值然后在此步骤中的所有条目将不能继续流程。 Gravity Flow Incoming Webhook 传入Webhook 通过外部webhook触发Gravity Flow工作流。 待处理 已队列 未授权 