��          �      \      �     �  
   �     �     �          
  n     �   �     A     N     l  ,   }     �     �     �  %   �     �  	   �       <       Z  
   c  
   n     y  	   �     �  �   �  �   *     �  "   �     	  /   +	     [	     t	     |	  (   �	     �	     �	     �	            	                                                     
                                  API Key API Secret Application Application Name Complete Display Fields Enter a unique string that will be used to authenticate the request. The value should be changed periodically. Enter a unique string that will be used to identify the application. Warning: If this value is changed then all entries that are on this step will not be able to continue the workflow. Gravity Flow Gravity Flow Incoming Webhook Incoming Webhook Incoming Webhook Extension for Gravity Flow. Manage Settings Pending Queued Select the fields to hide or display. Unauthorized Uninstall https://gravityflow.io Project-Id-Version: gravityflowincomingwebhook
Report-Msgid-Bugs-To: https://www.gravityflow.io
POT-Creation-Date: 2017-10-22 17:19:48+00:00
PO-Revision-Date: 2017-MO-DA HO:MI+ZONE
Last-Translator: Xavi Ivars <xavi.ivars@gmail.com>, 2017
Language-Team: Catalan (https://www.transifex.com/gravityflow/teams/50678/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gravity Flow Build Script
X-Poedit-Basepath: ../
X-Poedit-Bookmarks: 
X-Poedit-Country: United States
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-SearchPath-0: .
X-Poedit-SourceCharset: utf-8
X-Textdomain-Support: yes
 Clau API API Secret Aplicació Nom de l'aplicació Completat Mostra els camps Introduïu un valor singular que s'utilitzarà per a autentificar la petició. El valor s'hauria de canviar de manera periòdica. Introduïu una cadena singular que s'utilitzarà per a identificar l'aplicació. Atenció: si aquest valor es canvia, cap de les entrades d'aquest pas no podran continuar el flux de treball. Gravity Flow Webhook d'entrada del Gravity Flow Webhook d'entrada Extensió Webhook d'entrada per al Gravity Flow Gestiona els paràmetres Pendent En cua Seleccioneu els camps a mostrar o amagar No autoritzat Desinstal·la https://gravityflow.io 