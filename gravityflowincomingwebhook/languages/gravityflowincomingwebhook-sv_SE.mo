��          �      \      �     �  
   �     �     �          
  n     �   �     A     N     l  ,   }     �     �     �  %   �     �  	   �       N    
   l     w     �     �     �  
   �  w   �  �   7     	     	     ;	  3   N	     �	     �	  
   �	  .   �	  	   �	     �	     �	            	                                                     
                                  API Key API Secret Application Application Name Complete Display Fields Enter a unique string that will be used to authenticate the request. The value should be changed periodically. Enter a unique string that will be used to identify the application. Warning: If this value is changed then all entries that are on this step will not be able to continue the workflow. Gravity Flow Gravity Flow Incoming Webhook Incoming Webhook Incoming Webhook Extension for Gravity Flow. Manage Settings Pending Queued Select the fields to hide or display. Unauthorized Uninstall https://gravityflow.io Project-Id-Version: gravityflowincomingwebhook
Report-Msgid-Bugs-To: https://www.gravityflow.io
POT-Creation-Date: 2017-10-22 17:19:48+00:00
PO-Revision-Date: 2017-MO-DA HO:MI+ZONE
Last-Translator: FX Bénard <fxb@wp-translations.org>, 2017
Language-Team: Swedish (Sweden) (https://www.transifex.com/gravityflow/teams/50678/sv_SE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Language: sv_SE
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gravity Flow Build Script
X-Poedit-Basepath: ../
X-Poedit-Bookmarks: 
X-Poedit-Country: United States
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-SearchPath-0: .
X-Poedit-SourceCharset: utf-8
X-Textdomain-Support: yes
 API-nyckel Hemlig nyckel för API Applikation Applikationsnamn Färdigt Visa fält Ange en unik sträng som kommer att användas för att autentisera begäran. Värdet bör ändras med jämna mellanrum. Skriv in en unik sträng som kommer att användas för att identifiera applikationen. Varning: Om detta värde ändras kommer inte några ärenden som befinner sig på detta steg att kunna fortsätta i arbetsflödet. Gravity Flow Gravity Flow Incoming Webhook Inkommande webhook Tillägg till Gravity Flow för inkommande webhook. Hantera inställningar Väntar Lagt i kö Välj vilka fält som ska döljas eller visas. Obehörig Avinstallera https://gravityflow.io 