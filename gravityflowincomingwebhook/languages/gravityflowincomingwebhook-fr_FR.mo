��          �      \      �     �  
   �     �     �          
  n     �   �     A     N     l  ,   }     �     �     �  %   �     �  	   �       L    
   j     u     �     �     �     �  }   �  �   B     	     	     <	  /   L	     |	  
   �	     �	  3   �	     �	     �	     
            	                                                     
                                  API Key API Secret Application Application Name Complete Display Fields Enter a unique string that will be used to authenticate the request. The value should be changed periodically. Enter a unique string that will be used to identify the application. Warning: If this value is changed then all entries that are on this step will not be able to continue the workflow. Gravity Flow Gravity Flow Incoming Webhook Incoming Webhook Incoming Webhook Extension for Gravity Flow. Manage Settings Pending Queued Select the fields to hide or display. Unauthorized Uninstall https://gravityflow.io Project-Id-Version: gravityflowincomingwebhook
Report-Msgid-Bugs-To: https://www.gravityflow.io
POT-Creation-Date: 2017-10-22 17:19:48+00:00
PO-Revision-Date: 2017-MO-DA HO:MI+ZONE
Last-Translator: FX Bénard <fxb@wp-translations.org>, 2017
Language-Team: French (France) (https://www.transifex.com/gravityflow/teams/50678/fr_FR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Language: fr_FR
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Gravity Flow Build Script
X-Poedit-Basepath: ../
X-Poedit-Bookmarks: 
X-Poedit-Country: United States
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-SearchPath-0: .
X-Poedit-SourceCharset: utf-8
X-Textdomain-Support: yes
 Clé d'API Secret d’API Application Nom de l’application Terminé Afficher les champs Saisissez une chaîne unique qui sera utilisée pour authentifier la requête. La valeur doit être changée périodiquement. Saisissez une chaîne unique qui sera utilisée pour identifier l’application. Attention : Si cette valeur est changée alors les entrées qui sont dans cette étape ne pourront pas continuer le workflow. Gravity Flow Gravity Flow Incoming Webhook Webhook entrant Extension de webhook entrant pour Gravity Flow. Gestion des réglages En attente En file d’attente Sélectionnez les champs à masquer ou à afficher. Non autorisé Désinstaller https://gravityflow.io 