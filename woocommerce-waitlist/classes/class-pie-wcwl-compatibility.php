<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
if ( ! class_exists( 'Pie_WCWL_Compatibility' ) ) {
	/**
	 * Adds compatibility functions for ensuring Waitlist will work with different versions of WooCommerce
	 */
	class Pie_WCWL_Compatibility {

		/**
		 * Retrieves an array of parent product IDs based on current WooCommerce version
		 *
		 * @param \WC_Product $product product object
		 *
		 * @since 1.8.0
		 *
		 * @return array parent IDs
		 */
		public static function get_parent_id( WC_Product $product ) {
			$parent_ids = array();
			if ( $parent_id = $product->get_parent_id() ) {
				$parent_ids[] = $parent_id;
			}
			$parent_ids = array_merge( $parent_ids, self::get_grouped_parent_id( $product ) );

			return $parent_ids;
		}

		/**
		 * Check all grouped products to see if they have this product as a child product
		 *
		 * @param WC_Product $product
		 *
		 * @return array
		 */
		public static function get_grouped_parent_id( WC_Product $product ) {
			$parent_products  = array();
			$args             = array(
				'type'  => 'grouped',
				'limit' => - 1,
			);
			$grouped_products = wc_get_products( $args );
			foreach ( $grouped_products as $grouped_product ) {
				foreach ( $grouped_product->get_children() as $child_id ) {
					if ( $child_id == $product->get_id() ) {
						$parent_products[] = $grouped_product->get_id();
					}
				}
			}

			return $parent_products;
		}

		/**
		 * Return the current version of WooCommerce
		 *
		 * @since 1.5.0
		 *
		 * @return string woocommerce version number/null
		 */
		protected static function get_wc_version() {
			global $woocommerce;

			return isset( $woocommerce->version ) ? $woocommerce->version : null;
		}

		/**
		 * Returns true if the current version of WooCommerce is at least 3.0
		 *
		 * @since 1.5.0
		 *
		 * @return boolean
		 */
		public static function wc_is_at_least_3_0() {
			return self::get_wc_version() && version_compare( self::get_wc_version(), '3.0', '>=' );
		}
	}
}