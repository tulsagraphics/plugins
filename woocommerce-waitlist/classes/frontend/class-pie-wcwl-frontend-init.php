<?php
/**
 * Initialise waitlist on the frontend product pages and load shortcode.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
if ( ! class_exists( 'Pie_WCWL_Frontend_Init' ) ) {
	class Pie_WCWL_Frontend_Init {

		/**
		 * Hooks up the frontend initialisation and any functions that need to run before the 'init' hook
		 *
		 * @access public
		 */
		public function init() {
			add_action( 'wp', array( $this, 'frontend_init' ) );
			add_action( 'wc_quick_view_before_single_product', array( $this, 'quickview_init' ) );
			if ( ! is_admin() && 'yes' == get_option( 'woocommerce_waitlist_notify_admin' ) ) {
				add_action( 'wcwl_after_add_user_to_waitlist', array( $this, 'email_admin_user_joined_waitlist' ), 10, 2 );
			}
			// Compatibility
			add_filter( 'wc_get_template', array( $this, 'check_theme_directory_for_waitlist_template' ), 10, 5 );
		}

		/**
		 * Output the HTML to display a list of products the current user is on the waitlist for
		 */
		public function display_users_waitlists() {
			if ( ! is_user_logged_in() ) {
				return;
			}
			$user_id = get_current_user_id();
			wc_get_template( 'waitlist-user-waitlist.php', array(
				'title'    => __( 'Your Waitlists', 'woocommerce-waitlist' ),
				'products' => WooCommerce_Waitlist_Plugin::get_waitlist_products_by_user_id( $user_id ),
				'archives' => WooCommerce_Waitlist_Plugin::get_waitlist_archives_by_user_id( $user_id ),
			), '', WooCommerce_Waitlist_Plugin::$path . 'templates/' );
		}

		/**
		 * Check requirements and run initialise if waitlist is enabled
		 */
		public function frontend_init() {
			add_action( 'wp_enqueue_scripts', array( $this, 'frontend_enqueue_scripts' ) );
			$product_id = $this->return_product_id();
			$this->initialise_waitlist_for_product( $product_id );
			$this->load_user_waitlist_classes();
		}

		/**
		 * Enqueue scripts and styles for the frontend if user is on a product page
		 *
		 * @access public
		 * @return void
		 * @since  1.3
		 */
		public function frontend_enqueue_scripts() {
			wp_enqueue_script( 'wcwl_frontend', WCWL_ENQUEUE_PATH . '/includes/js/src/wcwl_frontend.min.js', array(), WCWL_VERSION, true );
			wp_enqueue_style( 'wcwl_frontend', WCWL_ENQUEUE_PATH . '/includes/css/src/wcwl_frontend.min.css', array(), WCWL_VERSION );
		}

		/**
		 * Retrieve the product ID for the current page/request
		 *
		 * @return int|mixed|string
		 */
		protected function return_product_id() {
			if ( self::is_ajax_variation_request() && ( isset( $_REQUEST['product_id'] ) && '' != $_REQUEST['product_id'] ) ) {
				return absint( $_REQUEST['product_id'] );
			}
			global $post;
			if ( is_product() ) {
				return $post->ID;
			}
			if ( $this->post_has_shortcode( $post, 'product_page' ) ) {
				return $this->get_product_id_from_post_content( $post->post_content );
			}

			return '';
		}

		/**
		 * Is WooCommerce performing an ajax request to return a child variation
		 *
		 * @return bool
		 */
		public static function is_ajax_variation_request() {
			if ( isset( $_REQUEST['wc-ajax'] ) && 'get_variation' == $_REQUEST['wc-ajax'] ) {
				return true;
			}

			return false;
		}

		/**
		 * Check if current page is using the given shortcode (checks for product_page or woocommerce_my_waitlist)
		 *
		 * @param $post
		 *
		 * @return bool
		 */
		protected function post_has_shortcode( $post, $shortcode ) {
			if ( ! empty( $post->post_content ) && strstr( $post->post_content, '[' . $shortcode ) ) {
				return true;
			}

			return false;
		}

		/**
		 * Look fpr the product ID in the provided post content
		 *
		 * @param $content
		 *
		 * @return mixed
		 */
		public static function get_product_id_from_post_content( $content ) {
			$content_after_shortcode    = substr( $content, strpos( $content, '[product_page' ) + 1 );
			$content_before_closing_tag = strtok( $content_after_shortcode, ']' );
			$product_id                 = filter_var( $content_before_closing_tag, FILTER_SANITIZE_NUMBER_INT );

			return $product_id;
		}

		/**
		 * Load required files and classes for frontend user waitlist
		 */
		protected function load_user_waitlist_classes() {
			global $post;
			require_once 'account/class-pie-wcwl-frontend-user-waitlist.php';
			require_once 'account/class-pie-wcwl-frontend-shortcode.php';
			new Pie_WCWL_Frontend_Shortcode();
			if ( is_account_page() && apply_filters( 'wcwl_enable_waitlist_account_tab', true ) ) {
				require_once 'account/class-pie-wcwl-frontend-account.php';
				new Pie_WCWL_Frontend_Account();
			}
			if ( 'yes' == get_option( 'woocommerce_waitlist_show_on_shop' ) && ( is_shop() || is_product_category() ) ) {
				$this->load_class( 'shop' );
			}
			if ( $post && function_exists( 'tribe_events_has_tickets' ) &&
			     'yes' == get_option( 'woocommerce_waitlist_events' ) &&
			     tribe_events_has_tickets( $post->ID ) ) {
				$this->load_class( 'event' );
			}
		}

		/**
		 * Load everything required for the product on the frontend
		 *
		 * @param $product_id
		 */
		protected function initialise_waitlist_for_product( $product_id ) {
			$product = wc_get_product( $product_id );
			if ( $product && array_key_exists( $product->get_type(), WooCommerce_Waitlist_Plugin::$allowed_product_types ) ) {
				$this->load_class( $product );
			}
		}

		/**
		 * Check requirements and run initialise if required
		 */
		public function quickview_init() {
			global $post;
			$product = wc_get_product( $post );
			if ( $product && array_key_exists( $product->get_type(), WooCommerce_Waitlist_Plugin::$allowed_product_types ) ) {
				$this->load_class( $product );
			}
		}

		/**
		 * Load required class for product type
		 */
		public function load_class( $product ) {
			require_once 'class-pie-wcwl-frontend-product.php';
			if ( 'shop' === $product ) {
				require_once 'product-types/class-pie-wcwl-frontend-shop.php';
				new Pie_WCWL_Frontend_Shop();
			} elseif ( 'event' === $product ) {
				require_once 'product-types/class-pie-wcwl-frontend-event.php';
				new Pie_WCWL_Frontend_Event();
			} else {
				$class = WooCommerce_Waitlist_Plugin::$allowed_product_types[ $product->get_type() ];
				require_once $class['filepath'];
				new $class['class'];
			}
		}

		/**
		 * Email the site admin when a user joins a waitlist
		 *
		 * @param $product_id
		 * @param $user
		 *
		 * @since 1.8.0
		 */
		public function email_admin_user_joined_waitlist( $product_id, $user ) {
			$email = get_option( 'woocommerce_waitlist_admin_email' );
			if ( ! is_email( $email ) ) {
				$email = get_option( 'admin_email' );
			}
			$product = wc_get_product( $product_id );
			if ( $product ) {
				$subject = apply_filters( 'wcwl_admin_notification_email_subject', __( 'A user has just joined a waitlist!', 'woocommerce-waitlist' ) );
				$message = sprintf( __( '%s%s has just joined the waitlist for %s%s', 'woocommerce-waitlist' ), '<p>', $user->user_email, $product->get_name(), '</p>' );
				$message .= sprintf( __( '%sTo view or make adjustments to the waitlist %splease visit the edit screen for this product%s.%s', 'woocommerce-waitlist' ), '<p>', '<a href="' . $this->get_edit_post_link( $product ) . '">', '</a>', '</p>' );
				add_filter( 'wp_mail_content_type', array( $this, 'set_email_content_type' ) );
				wp_mail( $email, $subject, apply_filters( 'wcwl_admin_notification_email_message', $message ) );
				remove_filter( 'wp_mail_content_type', array( $this, 'set_email_content_type' ) );
			}
		}

		/**
		 * Return the edit link for the given product
		 *
		 * Required to determine when to return the parent post link (variations)
		 *
		 * @param WC_Product $product
		 *
		 * @return null|string
		 */
		protected function get_edit_post_link( WC_Product $product ) {
			if ( WooCommerce_Waitlist_Plugin::is_variation( $product ) ) {
				$product_id = $product->get_parent_id();
			} else {
				$product_id = $product->get_id();
			}
			return get_edit_post_link( $product_id );
		}

		/**
		 * Set our email to go out as HTML
		 *
		 * @return string
		 * @since  1.8.0
		 */
		public function set_email_content_type() {
			return "text/html";
		}

		/**
		 * If the template is not located in the woocommerce directory of the theme, check the root directory
		 * Ensures backwards compatibility
		 *
		 * @param $located
		 * @param $template_name
		 * @param $args
		 * @param $template_path
		 * @param $default_path
		 *
		 * @return $located
		 */
		public function check_theme_directory_for_waitlist_template( $located, $template_name, $args, $template_path, $default_path ) {
			// Is this a waitlist template?
			if ( WooCommerce_Waitlist_Plugin::$path . 'templates/' !== $default_path ) {
				return $located;
			}
			// Are we still trying to load from the default path?
			if ( $located && $default_path && strpos( $located, $default_path ) !== false ) {
				// Check the theme directory
				$located = locate_template( array(
					$template_path . $template_name,
					$template_name,
				) );
				if ( ! $located ) {
					$located = $default_path . $template_name;
				}
			}
			return $located;
		}
	}
}