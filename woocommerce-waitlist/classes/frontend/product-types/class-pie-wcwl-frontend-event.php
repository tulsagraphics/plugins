<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
if ( ! class_exists( 'Pie_WCWL_Frontend_Event' ) ) {
	/**
	 * Loads up the waitlist for simple products
	 *
	 * @package  WooCommerce Waitlist
	 */
	class Pie_WCWL_Frontend_Event extends Pie_WCWL_Frontend_Product {

		public $has_out_of_stock_tickets = false;

		/**
		 * Pie_WCWL_Frontend_Event constructor.
		 */
		public function __construct() {
			parent::__construct();
			$this->init();
		}

		/**
		 * Load up hooks if product is out of stock
		 */
		protected function init() {
			if ( ! isset( $_POST['add-to-cart'] ) && $this->user_modified_waitlist ) {
				$this->process_waitlist_update();
			}
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_event_script_data' ) );
		}

		/**
		 * Enqueue required scripts
		 */
		public function enqueue_event_script_data() {
			$data = $this->load_data_for_outputting_html_with_js();
			wp_localize_script( 'wcwl_frontend', 'wcwl_event_data', $data );
		}

		/**
		 * Save an array of out of stock WooCommerce tickets
		 */
		protected function load_data_for_outputting_html_with_js() {
			global $post;
			$html    = array( 'tickets' => array() );
			$tickets = Tribe__Tickets__Tickets::get_all_event_tickets( $post->ID );
			if ( $tickets ) {
				foreach ( $tickets as $ticket ) {
					if ( 0 === $ticket->stock && 'Tribe__Tickets_Plus__Commerce__WooCommerce__Main' === $ticket->provider_class && $this->waitlist_is_enabled_for_product( $ticket->ID ) ) {
						$html['tickets'][ $ticket->ID ] = $this->get_checkbox_html( $ticket->ID );
						$this->has_out_of_stock_tickets = true;
					} else {
						$html['tickets'][ $ticket->ID ] = '';
					}
				}
				if ( $this->has_out_of_stock_tickets ) {
					$html['button'] = $this->get_waitlist_html();
				}
			}

			return $html;
		}

		/**
		 * Is current user on waitlist for this ticket
		 *
		 * @param $ticket_id
		 *
		 * @return boolean
		 */
		protected function user_is_on_waitlist( $ticket_id ) {
			$waitlist = new Pie_WCWL_Waitlist( wc_get_product( $ticket_id ) );
			if ( $waitlist->user_is_registered( $this->user->ID ) ) {
				return true;
			}

			return false;
		}

		/**
		 * If the user has attempted to modify the waitlist process the request
		 */
		protected function process_waitlist_update() {
			if ( ! $this->user ) {
				if ( $this->new_user_can_join_waitlist() ) {
					$this->toggle_waitlist_action_event();
				}
				$this->user = false;
			} else {
				$this->toggle_waitlist_action_event();
			}
		}

		/**
		 * Run the process to update the required product waitlists
		 */
		protected function toggle_waitlist_action_event() {
			if ( isset( $_REQUEST['wcwl_join'] ) && ! empty( $_REQUEST['wcwl_join'] ) ) {
				$this->process_user_join_waitlists( explode( ',', $_REQUEST['wcwl_join'] ) );
			}
			if ( isset( $_REQUEST['wcwl_leave'] ) && ! empty( $_REQUEST['wcwl_leave'] ) ) {
				$this->process_user_leave_waitlists( explode( ',', $_REQUEST['wcwl_leave'] ) );
			}
			Tribe__Notices::set_notice( 'woocommerce-waitlist-updated-lists', apply_filters( 'wcwl_ticket_product_joined_message_text', $this->grouped_product_joined_message_text ) );
		}

		/**
		 * Add user to required products
		 *
		 * @param $products
		 */
		protected function process_user_join_waitlists( $products ) {
			foreach ( $products as $product_id ) {
				$waitlist = new Pie_WCWL_Waitlist( wc_get_product( absint( $product_id ) ) );
				$waitlist->register_user( $this->user );
			}
		}

		/**
		 * Remove user from required products
		 *
		 * @param $products
		 */
		protected function process_user_leave_waitlists( $products ) {
			foreach ( $products as $product_id ) {
				$waitlist = new Pie_WCWL_Waitlist( wc_get_product( absint( $product_id ) ) );
				$waitlist->unregister_user( $this->user );
			}
		}

		/**
		 * Get HTML for the checkbox elements for the event
		 *
		 * @param string $context whether the checkbox should be checked or not
		 * @param        $product_id
		 *
		 * @return string HTML for join waitlist button
		 */
		protected function get_checkbox_html( $product_id ) {
			if ( $this->has_wpml ) {
				$product = wc_get_product( $this->get_main_product_id( $product_id ) );
			} else {
				$product = wc_get_product( $product_id );
			}
			if ( ! $this->waitlist_is_enabled_for_product( $product->get_id() ) ) {
				return '';
			}
			$waitlist = new Pie_WCWL_Waitlist( $product );
			if ( $this->user && $waitlist->user_is_registered( $this->user->ID ) ) {
				$context = 'leave';
				$checked = 'checked';
			} else {
				$context = 'join';
				$checked = '';
			}

			return '<label class="' . WCWL_SLUG . '_label" > - ' . apply_filters( 'wcwl_' . $context . '_waitlist_button_text', $this->join_waitlist_button_text ) . '<input id="wcwl_checked_' . $product->get_id() . '" class="wcwl_checkbox" type="checkbox" name="' . ( 'join' == $context ? $context : WCWL_SLUG . '_product_id' . '[]' ) . '" ' . $checked . '/></label>';
		}

		/**
		 * Get required HTML for the waitlist message and button for events
		 *
		 * @return $string
		 */
		protected function get_waitlist_html() {
			$html  = $this->return_waitlist_message();
			$html .= $this->return_waitlist_control();

			return $html;
		}

		/**
		 * Outputs the appropriate Ticket Product message HTML
		 *
		 * @hooked action woocommerce_after_add_to_cart_form
		 * @access public
		 */
		protected function return_waitlist_message() {
			$classes = implode( ' ', apply_filters( 'wcwl_ticket_product_message_classes', array(
				'out-of-stock',
				WCWL_SLUG,
			) ) );
			if ( $this->user ) {
				$text = apply_filters( 'wcwl_ticket_product_message_registered_user_text', $this->grouped_product_message_text );
			} else {
				$text = apply_filters( 'wcwl_ticket_product_message_unregistered_user_text', $this->no_user_grouped_product_message_text );
			}
			return apply_filters( 'wcwl_ticket_product_message_html', '<p class="' . esc_attr( $classes ) . '">' . $text . '</p>' );
		}

		/**
		 * Get HTML for the waitlist button and email field/opt-in elements as required
		 *
		 * @return string
		 */
		protected function return_waitlist_control() {
			$string = '<div class="wcwl_frontend_wrap">';
			if ( ! $this->user && WooCommerce_Waitlist_Plugin::users_must_be_logged_in_to_join_waitlist() ) {
				$string .= $this->get_waitlist_control( 'join' );
			} elseif ( ! $this->user ) {
				$string .= $this->get_waitlist_email_field();
				$string .= $this->get_waitlist_control( 'join' );
			} else {
				if ( 'yes' == get_option( 'woocommerce_waitlist_registered_user_opt-in' ) ) {
					$notice = apply_filters( 'wcwl_registered_user_opt-in_text', $this->registered_opt_in_text );
					$string .= $this->get_waitlist_opt_in_html( $notice );
				}
				$string .= $this->get_waitlist_control( 'update' );
			}
			$string .= '</div>';

			return $string;
		}

		/**
		 * Get HTML for waitlist elements depending on product type
		 *
		 * @param string $context the context in which the button should be generated (join|leave)
		 *
		 * @return string HTML for join waitlist button
		 */
		protected function get_waitlist_control( $context ) {
			$text_string = $context . '_waitlist_button_text';
			$classes     = implode( ' ', apply_filters( 'wcwl_' . $context . '_waitlist_button_classes', array( 'button', 'alt', WCWL_SLUG, $context, ) ) );
			$text        = apply_filters( 'wcwl_' . $context . '_waitlist_button_text', $this->$text_string );
			$url         = $this->create_button_url( $context, 0 );

			return apply_filters( 'wcwl_' . $context . '_waitlist_submit_button_html', '<div class="wcwl_control"><a href="' . esc_url( $url ) . '" class="' . esc_attr( $classes ) . '">' . esc_html( $text ) . '</a></div>' );
		}

		/**
		 * Check if current user is able to use the waitlist functionality and output error if not
		 *
		 * @return bool
		 */
		protected function new_user_can_join_waitlist() {
			if ( 'yes' == get_option( 'woocommerce_waitlist_registration_needed' ) ) {
				Tribe__Notices::set_notice( 'woocommerce-waitlist-user-must-register', apply_filters( 'wcwl_join_waitlist_user_requires_registration_message_text', $this->users_must_register_and_login_message_text ) );

				return false;
			} elseif ( ! isset( $_REQUEST['wcwl_email'] ) || ! is_email( $_REQUEST['wcwl_email'] ) ) {
				Tribe__Notices::set_notice( 'woocommerce-waitlist-invalid-email', apply_filters( 'wcwl_join_waitlist_invalid_email_message_text', $this->join_waitlist_invalid_email_message_text ) );

				return false;
			} elseif ( empty( $_REQUEST['wcwl_join'] ) ) {
				Tribe__Notices::set_notice( 'woocommerce-waitlist-no-product-selected', apply_filters( 'wcwl_toggle_waitlist_no_product_message_text', $this->toggle_waitlist_no_product_message_text ) );

				return false;
			} else {
				$this->setup_new_user( sanitize_email( $_REQUEST['wcwl_email'] ) );

				return true;
			}
		}
	}
}