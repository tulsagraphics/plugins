<?php

$message = '';
$list = array();
$counted = count($thanks_list);

if ($counted > $this->settings['limit_display']) {
    $message = sprintf(__("Total of %s users thanked author for this post. Here are last %s listed.", "gd-bbpress-toolbox"), $counted, $this->settings['limit_display']);
} else {
    $message = sprintf(_n("%s user thanked author for this post.", "%s users thanked author for this post.", $counted, "gd-bbpress-toolbox"), $counted);
}

$thanks_list = array_slice($thanks_list, 0, $this->settings['limit_display']);

foreach ($thanks_list as $user) {
    if (get_userdata($user) !== false) {
        $list[] = get_avatar($user, '16').' '.bbp_get_user_profile_link($user);
    }
}

?>
<div class="gdbbx-said-thanks">
    <h6><?php echo $message; ?></h6>

    <div class="gdbbx-thanks-list">
        <?php echo join(', ', $list); ?>
    </div>
</div>