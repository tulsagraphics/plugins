/*jslint regexp: true, nomen: true, undef: true, sloppy: true, eqeq: true, vars: true, white: true, plusplus: true, maxerr: 50, indent: 4 */
/*global gdbbx_data, tinymce, tinyMCE */
var gdbbx_render, gdbbx_helper;

;(function($, window, document, undefined) {
    gdbbx_helper = {
        detect_msie: function() {
            var ua = window.navigator.userAgent;

            var msie = ua.indexOf('MSIE ');

            if (msie > 0) {
                return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            }

            var trident = ua.indexOf('Trident/');
            if (trident > 0) {
                var rv = ua.indexOf('rv:');

                return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
            }

            return 99;
        },
        get_selection: function() {
            var t = "";

            if (window.getSelection){
                t = window.getSelection();
            } else if (document.getSelection){
                t = document.getSelection();
            } else if (document.selection){
                t = document.selection.createRange().text;
            }

            return $.trim(t.toString());
        },
        file_extension: function(name) {
            return name.substr(name.lastIndexOf(".") + 1).toLowerCase();
        },
        is_tinymce: function() {
            var id = $("#bbp_topic_content").length > 0 ? "bbp_topic_content" : "bbp_reply_content";

            return gdbbx_data.wp_editor && !$("#" + id).is(":visible");
        },
        into_editor: function(text) {
            var id = $("#bbp_topic_content").length > 0 ? "bbp_topic_content" : "bbp_reply_content";

            if (gdbbx_helper.is_tinymce()) {
                text+= "<br/><br/>";

                tinymce.get(id).execCommand("mceInsertContent", false, text);
            } else {
                var txtr = $("#" + id);
                var cntn = txtr.val();

                if ($.trim(cntn) !== "") {
                    text = "\n\n" + text;
                }

                text+= "\n\n";

                txtr.val(cntn + text);
            }
        },
        scroll_to_editor: function() {
            if (gdbbx_helper.detect_msie() > 8) {
                $("html, body").animate({scrollTop: $("#new-post").offset().top}, 1000);
            } else {
                document.location.href = "#new-post";
            }

            $(".bbp-the-content-wrapper textarea").focus();
        }
    };

    gdbbx_render = {
        storage: {
            sending_report: false,
            files_counter: 1,
            attachments_submit: true,
            attachments_extensions: [],
            attachments_enhanced: true
        },
        attachments: {
            init: function() {
                $("form#new-post").attr("enctype", "multipart/form-data");
                $("form#new-post").attr("encoding", "multipart/form-data");

                $(document).on("click", ".gdbbx-attachment-insert", function(e){
                    e.preventDefault();

                    var id = $(this).attr("href").substr(1),
                        shortcode = '[attachment file="' + id + '"]';

                    gdbbx_helper.into_editor(shortcode);
                    gdbbx_helper.scroll_to_editor();
                });

                $(document).on("click", ".gdbbx-attachment-confirm", function(e){
                    if (!confirm(gdbbx_data.text_are_you_sure)) {
                        e.preventDefault();
                    }
                });

                $(document).on("click", ".gdbbx-attachment-add-file", function(e){
                    e.preventDefault();

                    if (!gdbbx_data.limiter || gdbbx_render.storage.files_counter < gdbbx_data.max_files) {
                        $(this).before(gdbbx_render.attachments.block());
                        gdbbx_render.storage.files_counter++;
                    }

                    if (gdbbx_data.limiter && gdbbx_render.storage.files_counter === gdbbx_data.max_files) {
                        $(this).hide();
                    }
                });

                if (gdbbx_render.storage.attachments_enhanced) {
                    $(".gdbbx-attachments-form > div").addClass("gdbbx-validation-active");

                    gdbbx_render.attachments.validation();
                }
            },
            block: function() {
                var cls = "gdbbx-attachments-input", block;

                if (gdbbx_render.storage.attachments_enhanced) {
                    cls+= " gdbbx-validation-active";
                } else {
                    cls+= " gdbbx-validation-disabled";
                }

                block = '<div class="' + cls + '">';

                if (gdbbx_render.storage.attachments_enhanced) {
                    block+= '<div role="button" class="gdbbx-attachment-preview"><span aria-hidden="true">' + gdbbx_data.text_select_file + '</span></div>';
                }

                block+= '<label><input type="file" size="40" name="gdbbx-attachment[]" />';
                block+= '<span class="gdbbx-accessibility-show-for-sr">' + gdbbx_data.text_select_file + '</span></label>';

                if (gdbbx_render.storage.attachments_enhanced) {
                    block+= '<div class="gdbbx-attachment-control"></div>';
                }

                block+= '</div>';

                return block;
            },
            check_submit: function() {
                var valid = true;

                $(".gdbbx-attachments-form .gdbbx-attachments-input").each(function(){
                    if ($(this).hasClass("gdbbx-attachment-invalid")) {
                        valid = false;
                    }
                });

                $(".gdbbx-attachments-form").closest("form").find(".bbp-submit-wrapper button").attr("disabled", !valid);
            },
            validation: function() {
                gdbbx_render.storage.attachments_extensions = gdbbx_data.allowed_extensions.split(" ");

                $(document).on("click", ".gdbbx-attachment-preview", function(){
                    $(this).closest(".gdbbx-attachments-input").find("input[type=file]").click();
                });

                $(document).on("click", ".gdbbx-attachments-input a.bbp-att-remove", function(e){
                    e.preventDefault();

                    $(this).closest(".gdbbx-attachments-input").fadeOut("slow", function(){
                        gdbbx_render.storage.files_counter--;

                        $(".gdbbx-attachment-add-file").show();

                        if (gdbbx_render.storage.files_counter === 0) {
                            $(this).after(gdbbx_render.attachments.block());
                            gdbbx_render.storage.files_counter++;
                        }

                        $(this).remove();

                        gdbbx_render.attachments.check_submit();
                    });
                });

                $(document).on("click", ".gdbbx-attachments-input a.bbp-att-caption", function(e){
                    e.preventDefault();

                    $(this).prev().find("input").show();
                    $(this).hide();
                });

                $(document).on("click", ".gdbbx-attachments-input a.bbp-att-shortcode", function(e){
                    e.preventDefault();

                    var shortcode = '[attachment file="' + $(this).data("file") + '"]';

                    gdbbx_helper.into_editor(shortcode);
                    gdbbx_helper.scroll_to_editor();
                });

                $(document).on("change", ".gdbbx-attachments-input input[type=file]", function(){
                    if (!this.files) {
                        return;
                    }

                    var block = $(this).closest(".gdbbx-attachments-input"), file = this.files[0], txt = "",
                        size = Math.round(file.size / 1024), img = "", limiter = gdbbx_data.limiter, 
                        valid = true, valid_size = true, valid_type = true, forbidden = ["js", "php"], 
                        ext = gdbbx_helper.file_extension(file.name),
                        regex = /^([a-zA-Z0-9\s_\\.\-:\+])+(.jpg|.jpeg|.gif|.png|.bmp)$/;

                    block.removeClass("gdbbx-attachment-invalid");

                    txt = '<div>' + gdbbx_data.text_file_name + ": <strong>" + file.name + "</strong></div>";
                    txt+= '<div>' + gdbbx_data.text_file_size + ": <strong>" + size + " kb</strong>, ";
                    txt+= gdbbx_data.text_file_type + ": <strong>" + ext.toUpperCase() + "</strong></div>";

                    if ($.inArray(ext, forbidden) > -1) {
                        valid = false;
                        valid_type = false;
                    }

                    if (limiter && file.size > gdbbx_data.max_size) {
                        valid = false;
                        valid_size = false;
                    }

                    if (limiter && $.inArray(ext, gdbbx_render.storage.attachments_extensions) === -1) {
                        valid = false;
                        valid_type = false;
                    }

                    if (!valid) {
                        txt+= "<strong>";
                        txt+= gdbbx_data.text_file_validation;

                        if (!valid_type) {
                            txt+= " " + gdbbx_data.text_file_validation_type;
                        }

                        if (!valid_size) {
                            txt+= " " + gdbbx_data.text_file_validation_size;
                        }

                        txt+= "</strong><br/>";

                        block.addClass("gdbbx-attachment-invalid");
                    }

                    if (valid) {
                        txt+= "<div><label><input name='gdbbx-attachment_caption[]' type='text' style='display: none' placeholder='" + gdbbx_data.text_file_caption_placeholder + "' /><span class='gdbbx-accessibility-show-for-sr'>" + gdbbx_data.text_file_caption_placeholder + "</span></label><a data-file='" + file.name + "' class='bbp-att-caption' href='#'>" + gdbbx_data.text_file_caption + "</a></div>";

                        if (gdbbx_data.insert_into_content) {
                            txt+= "<div><a class='bbp-att-shortcode' href='#'>" + gdbbx_data.text_file_shortcode + "</a></div>";
                        }
                    }

                    txt+= "<div><a class='bbp-att-remove' href='#'>" + gdbbx_data.text_file_remove + "</a></div>";

                    block.find(".gdbbx-attachment-control").html(txt);
                    block.find(".gdbbx-attachment-control .bbp-att-shortcode").data('file', file.name);
                    block.find(".gdbbx-attachment-preview .gdbbx-attached-file").remove();

                    if (window.FileReader && regex.test(file.name.toLowerCase())) {
                        var reader = new FileReader();
                        reader.readAsDataURL(file);

                        reader.onloadend = function(){
                            img = '<img class="gdbbx-attached-file" alt="' + file.name + '" src="' + this.result + '" />';
                            block.find(".gdbbx-attachment-preview").prepend(img);
                        };
                    } else {
                        img = '<p class="gdbbx-attached-file" title="' + file.name + '">.' + ext.toUpperCase() + '</p>';
                        block.find(".gdbbx-attachment-preview").prepend(img);
                    }

                    gdbbx_render.attachments.check_submit();
                });
            }
        },
        bbcodes: {
            init: function() {
                $(".gdbbx-bbcode-spoiler").each(function(){
                    var hover = $(this).data("hover"),
                        normal = $(this).data("color");

                    $(this).hover(
                        function() {
                            $(this).css("background", hover);
                        },
                        function() {
                            $(this).css("background", normal);
                        }
                    );
                });
            }
        },
        quotes: {
            init: function() {
                $(document).on("click", ".gdbbx-link-quote", function(e){
                    e.preventDefault();

                    if ($("#bbp_reply_content").length > 0) {
                        var qout = gdbbx_helper.get_selection(), id = $(this).data("id"),
                            eol = gdbbx_helper.is_tinymce() ? "</br></br>" : "\n",
                            quote_id = "#gdbbx-quote-wrapper-" + id;

                        if (qout === "") {
                            qout = $(quote_id).html();
                        }

                        qout = qout.replace(/&nbsp;/g, " ");
                        qout = qout.replace(/<\s*p[^>]*>/g, "");
                        qout = qout.replace(/<\s*\/\s*(p|br)\s*>|<\s*br\s*>/g, eol);
                        qout = qout.trim();

                        qout = $("<div>").html(qout).html();

                        if (gdbbx_data.quote_method === "bbcode") {
                            qout = "[quote quote=" + id + "]" + qout + "[/quote]";
                        } else {
                            var title = '<div class="gdbbx-quote-title"><a href="' + $(this).data("url") + '">';
                            title+= $(this).data("author") + ' ' + gdbbx_data.quote_wrote + ':</a></div>';
                            qout = '<blockquote class="gdbbx-bbcode-quote">' + title + qout + '</blockquote>';
                        }

                        gdbbx_helper.into_editor(qout);
                        gdbbx_helper.scroll_to_editor();
                    }
                });
            }
        },
        canned_replies: {
            init: function() {
                $(".gdbbx-canned-replies .gdbbx-canned-replies-show").click(function(e){
                    e.preventDefault();

                    var container = $(this).closest(".gdbbx-canned-replies");

                    $(this).hide();
                    $(".gdbbx-canned-replies-hide", container).show();
                    $(".gdbbx-canned-replies-list", container).slideDown();
                });

                $(".gdbbx-canned-replies .gdbbx-canned-replies-hide").click(function(e){
                    e.preventDefault();

                    var container = $(this).closest(".gdbbx-canned-replies");

                    $(this).hide();
                    $(".gdbbx-canned-replies-show", container).show();
                    $(".gdbbx-canned-replies-list", container).slideUp();
                });

                $(".gdbbx-canned-replies .gdbbx-canned-reply-insert").click(function(e){
                    e.preventDefault();

                    var container = $(this).closest(".gdbbx-canned-reply"),
                        content = $(".gdbbx-canned-reply-content", container).html();

                    gdbbx_helper.into_editor(content);

                    if (gdbbx_data.auto_close_on_insert) {
                        var wrapper = $(this).closest(".gdbbx-canned-replies");

                        $(".gdbbx-canned-replies-hide", wrapper).click();
                    }
                });
            }
        },
        fitvids: {
            init: function() {
                $(".bbp-topic-content, .bbp-reply-content").fitVids();
            }
        },
        report: {
            init: function() {
                $(".gdbbx-link-report").click(function(e){
                    e.preventDefault();

                    if (!gdbbx_render.storage.sending_report) {
                        if (gdbbx_data.report_mode === "form") {
                            gdbbx_render.report.form($(this));
                        } else {
                            gdbbx_render.report.button($(this));
                        }
                    }
                });
            },
            button: function(el) {
                var id = el.data("id"), nonce = el.data("nonce");

                if (gdbbx_data.report_mode === "confirm") {
                    if (confirm(gdbbx_data.report_confirm) === false) {
                        return;
                    }
                }

                var call = {
                    post: id,
                    nonce: nonce
                };

                gdbbx_render.storage.sending_report = true;

                $.ajax({
                    dataType: "html", type: "post", data: call,
                    url: gdbbx_data.url + "?action=gdbbx_report_post",
                    success: function(html) {
                        gdbbx_render.storage.sending_report = false;

                        $(".gdbbx-link-report-" + call.post).replaceWith("<span>" + gdbbx_data.report_after + "</span>");
                    }
                });
            },
            form: function(el) {
                var id = el.data("id"), post_type = el.data("post-type"), 
                    type = el.data("type"), nonce = el.data("nonce"), 
                    forums = el.closest("#bbpress-forums"),
                    content = el.closest("#bbpress-forums").find(".post-" + id + " .bbp-reply-content, .post-" + id + " .bbp-topic-content");

                if (content.length === 1) {
                    if (content.find(".gdbbx-report-wrapper").length === 0) {
                        $(".gdbbx-report-wrapper").remove();

                        var form = $(".gdbbx-report-template > div")
                                .clone()
                                .addClass("gdbbx-report-wrapper");

                        form.find("button")
                            .data("id", id)
                            .data("nonce", nonce);

                        content.append(form);

                        form.find("input").focus();

                        gdbbx_render.report.handle(content.find(".gdbbx-report-wrapper"));
                    }

                    if (gdbbx_data.report_scroll) {
                        var offset = 0;

                        if ($("#wpadminbar").length > 0) {
                            offset = $("#wpadminbar").height();
                        }

                        $("html, body").animate({
                            scrollTop: content.find(".gdbbx-report-wrapper").offset().top - offset
                        }, 500);
                    }
                }
            },
            handle: function(el) {
                $("button.gdbbx-report-cancel", el).click(function(){
                    $(".gdbbx-report-wrapper").remove();
                });

                $("button.gdbbx-report-send", el).click(function(){
                    var text = $("input", el).val();

                    if (text.length < gdbbx_data.report_min) {
                        alert(gdbbx_data.report_alert);
                    } else {
                        var call = {
                            report: text,
                            post: $(this).data("id"),
                            nonce: $(this).data("nonce")
                        };

                        $(".gdbbx-report-form", el).hide();
                        $(".gdbbx-report-sending", el).show();

                        gdbbx_render.storage.sending_report = true;

                        $.ajax({
                            dataType: "html", type: "post", data: call,
                            url: gdbbx_data.url + "?action=gdbbx_report_post",
                            success: function(html) {
                                gdbbx_render.storage.sending_report = false;

                                $(".gdbbx-report-sending", el).hide();
                                $(".gdbbx-report-sent", el).show();

                                $(".gdbbx-link-report-" + call.post).replaceWith("<span>" + gdbbx_data.report_after + "</span>");
                            }
                        });
                    }
                });
            }
        },
        thanks: {
            init: function() {
                $(".gdbbx-link-thanks, .gdbbx-link-unthanks").click(function(e){
                    e.preventDefault();

                    gdbbx_render.thanks.handle(this);
                });
            },
            handle: function(el) {
                var call = {
                        nonce: $(el).data("thanks-nonce"),
                        say: $(el).data("thanks-action"),
                        id: $(el).data("thanks-id")
                    }, button = $(el), 
                    is_thanks = button.hasClass("gdbbx-link-thanks");

                $.ajax({
                    dataType: "html", type: "post", data: call,
                    url: gdbbx_data.url + "?action=gdbbx_say_thanks",
                    success: function(html) {
                        var thanks = $(html).fadeIn(600);

                        $(".gdbbx-thanks-post-" + call.id).fadeOut(400).replaceWith(thanks);

                        if (is_thanks) {
                            if (gdbbx_data.thanks_removal) {
                                button.removeClass("gdbbx-link-thanks")
                                      .addClass("gdbbx-link-unthanks")
                                      .data("thanks-action", "unthanks")
                                      .html(gdbbx_data.thanks_unthanks);
                            } else {
                                button.replaceWith("<span>" + gdbbx_data.thanks_saved + "</span>");
                            }
                        } else {
                            button.removeClass("gdbbx-link-unthanks")
                                  .addClass("gdbbx-link-thanks")
                                  .data("thanks-action", "thanks")
                                  .html(gdbbx_data.thanks_thanks);
                        }
                    }
                });
            }
        },
        misc: {
            privacy: function() {
                $(".gdbbx-private-reply-hidden").each(function(){
                    $(this).hide().prev(".bbp-reply-header").hide();
                });
            }
        },
        run: function() {
            if (typeof gdbbx_data !== "undefined") {
                gdbbx_render.misc.privacy();

                if (gdbbx_data.run_quote) {
                    gdbbx_render.quotes.init();
                }

                if (gdbbx_data.run_attachments) {
                    gdbbx_render.storage.attachments_enhanced = gdbbx_data.validate_attachments && gdbbx_helper.detect_msie() > 9;

                    gdbbx_render.attachments.init();
                }

                if (gdbbx_data.run_bbcodes) {
                    gdbbx_render.bbcodes.init();
                }

                if (gdbbx_data.run_report) {
                    gdbbx_render.report.init();
                }

                if (gdbbx_data.run_thanks) {
                    gdbbx_render.thanks.init();
                }

                if (gdbbx_data.run_canned_replies) {
                    gdbbx_render.canned_replies.init();
                }

                if (gdbbx_data.run_fitvids && $.fn.fitVids) {
                    gdbbx_render.fitvids.init();
                }
            }
        }
    };

    gdbbx_render.run();
})(jQuery, window, document);
