=== GD bbPress Toolbox Pro ===
Contributors: GDragoN
Version: 5.3
Requires at least: 4.4
Tested up to: 4.9
Stable tag: trunk
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Expand bbPress powered forums with attachments upload, BBCodes support, signatures, widgets, quotes, toolbar menu, activity tracking, enhanced widgets, extra views...

== Description ==
Expand bbPress powered forums with attachments upload, BBCodes support, signatures, widgets, quotes, toolbar menu, activity tracking, enhanced widgets, extra views...

== Installation ==
= General Requirements =
* PHP: 5.5 or newer
* bbPress 2.5 or newer
* mySQL: 5.5 or newer
* WordPress: 4.4 or newer
* jQuery: 1.11.1 or newer

= PHP Notice =
* The plugin should work with PHP 5.3 and 5.4, but these versions are no longer used for testing, and they are no longer supported.
* The plugin doesn't work with PHP 5.2 or older versions.

= WordPress Notice =
* The plugin should work with WordPress 4.0, 4.1, 4.2 and 4.3, but these versions are no longer used for testing, and they are no longer supported.
* The plugin doesn't work with WordPress 3.9 or older versions.

= Basic Installation =
* Plugin folder in the WordPress plugins folder must be `gd-bbpress-toolbox`.
* Upload `gd-bbpress-toolbox` folder to the `/wp-content/plugins/` directory.
* Activate the plugin through the 'Plugins' menu in WordPress.
* Check all the plugin and plugin widgets settings before using the plugin.
