<?php

if (!defined('ABSPATH')) exit;

class gdbbx_admin_getback {
    public function __construct() {
        if (gdbbx_admin()->page === 'tools') {
            if (isset($_GET['run']) && $_GET['run'] == 'export') {
                $this->tools_export();
            }
        }

        if (gdbbx_admin()->page === 'front') {
            if (isset($_GET['action']) && $_GET['action'] == 'gdbbx-disable-free') {
                $this->front_disable_free();
            }

            if (isset($_GET['action']) && $_GET['action'] == 'dismiss-topic-prefix') {
                $this->front_dismiss_prefix();
            }

            if (isset($_GET['action']) && $_GET['action'] == 'dismiss-topic-polls') {
                $this->front_dismiss_polls();
            }
        }

        if (gdbbx_admin()->page === 'reported-posts') {
            if (isset($_GET['single-action'])) {
                $this->reported_posts_action();
            }
        }

        if (gdbbx_admin()->page === 'thanks-list') {
            if (isset($_GET['single-action'])) {
                
            }
        }

        if (gdbbx_admin()->page === 'attachments-list') {
            if (isset($_GET['single-action'])) {
                $this->attachments_single_action();
            }

            if (isset($_GET['action']) || isset($_GET['action2'])) {
                $this->attachments_bulk_action();
            }
        }

        if (gdbbx_admin()->page === 'errors') {
            if (isset($_GET['single-action'])) {
                $this->errors_single_action();
            }

            if (isset($_GET['action']) || isset($_GET['action2'])) {
                $this->errors_bulk_action();
            }
        }
    }

    private function action_delete_errors($ids) {
        foreach ($ids as $meta_id) {
            delete_metadata_by_mid('post', $meta_id);
        }
    }

    private function action_delete_attachments($ids) {
        global $user_ID;

        foreach ($ids as $att_id) {
            $attachment = get_post($att_id);
            $parent = $attachment->post_parent;

            $file = get_attached_file($att_id);
            $file = pathinfo($file, PATHINFO_BASENAME);

            wp_delete_attachment($att_id);

            add_post_meta($parent, '_bbp_attachment_log', array(
                'code' => 'delete_attachment', 'user' => $user_ID, 'file' => $file)
            );
        }
    }

    private function action_unattach_attachments($ids) {
        global $user_ID;

        foreach ($ids as $att_id) {
            $attachment = get_post($att_id);
            $parent = $attachment->post_parent;

            $file = get_attached_file($att_id);
            $file = pathinfo($file, PATHINFO_BASENAME);

            gdbbx_db()->update(gdbbx_db()->wpdb()->posts, array('post_parent' => 0), array('ID' => $att_id));

            add_post_meta($parent, '_bbp_attachment_log', array(
                'code' => 'detach_attachment', 'user' => $user_ID, 'file' => $file, 'attachment_id' => $att_id)
            );
        }
    }

    private function tools_export() {
        @ini_set('memory_limit', '128M');
        @set_time_limit(360);

        check_ajax_referer('dev4press-plugin-export');

        if (!d4p_is_current_user_admin()) {
            wp_die(__("Only administrators can use export features.", "gd-bbpress-toolbox"));
        }

        $export_date = date('Y-m-d-H-m-s');

        header('Content-type: application/force-download');
        header('Content-Disposition: attachment; filename="gd_bbpress_toolbox_settings_'.$export_date.'.gdbbx"');

        die(gdbbx()->serialized_export());
    }

    private function front_disable_free() {
        deactivate_plugins(array(
            'gd-bbpress-attachments/gd-bbpress-attachments.php',
            'gd-bbpress-tools/gd-bbpress-tools.php',
            'gd-bbpress-widgets/gd-bbpress-widgets.php'
        ));

        wp_redirect('admin.php?page=gd-bbpress-toolbox-front&message=free-disabled');
        exit;
    }

    private function front_dismiss_prefix() {
        gdbbx_settings()->set('notice_gdtox_hide', true, 'core', true);

        wp_redirect('admin.php?page=gd-bbpress-toolbox-front');
        exit;
    }

    private function front_dismiss_polls() {
        gdbbx_settings()->set('notice_gdpol_hide', true, 'core', true);

        wp_redirect('admin.php?page=gd-bbpress-toolbox-front');
        exit;
    }

    private function reported_posts_action() {
        $nonce = isset($_GET['_wpnonce']) ? $_GET['_wpnonce'] : '';

        if (wp_verify_nonce($nonce, 'gd-bbpress-toolbox-report') !== false) {
            $user = get_current_user_id();
            $action = $_GET['single-action'];
            $id = isset($_GET['report']) ? absint($_GET['report']) : 0;

            if ($action == 'close-report' && $id > 0) {
                gdbbx_db()->report_status($id, 'closed');
                gdbbx_db()->report_closed($id, $user);

                wp_redirect('admin.php?page=gd-bbpress-toolbox-reported-posts');
                exit;
            }
        }
    }

    private function attachments_single_action() {
        $nonce = isset($_GET['_wpnonce']) ? $_GET['_wpnonce'] : '';

        if (wp_verify_nonce($nonce, 'gd-bbpress-toolbox-attachment') !== false) {
            $action = $_GET['single-action'];
            $id = isset($_GET['attachment']) ? array($_GET['attachment']) : array();

            if ($action == 'delete') {
                $this->action_delete_attachments($id);
            } else if ($action == 'unattach') {
                $this->action_unattach_attachments($id);
            }

            wp_redirect('admin.php?page=gd-bbpress-toolbox-attachments&message=attachment-'.$action);
            exit;
        }
    }

    private function attachments_bulk_action() {
        check_admin_referer('bulk-attachments');

        $action = isset($_GET['action']) && $_GET['action'] != '' && $_GET['action'] != '-1' ? $_GET['action'] : '';

        if ($action == '') {
            $action = isset($_GET['action2']) && $_GET['action2'] != '' && $_GET['action2'] != '-1' ? $_GET['action2'] : '';
        }

        if ($action != '') {
            $ids = isset($_GET['attachment']) ? (array)$_GET['attachment'] : array();

            if (!empty($ids)) {
                if ($action == 'delete') {
                    $this->action_delete_attachments($ids);
                } else if ($action == 'unattach') {
                    $this->action_unattach_attachments($ids);
                }
            }

            wp_redirect('admin.php?page=gd-bbpress-toolbox-attachments&message=attachments-'.$action);
            exit;
        }
    }

    private function errors_single_action() {
        $nonce = isset($_GET['_wpnonce']) ? $_GET['_wpnonce'] : '';

        if (wp_verify_nonce($nonce, 'gd-bbpress-toolbox-error') !== false) {
            $action = $_GET['single-action'];
            $id = isset($_GET['error']) ? array($_GET['error']) : array();

            if ($action == 'delete') {
                $this->action_delete_errors($id);
            }

            wp_redirect('admin.php?page=gd-bbpress-toolbox-errors&message=error-deleted');
            exit;
        }
    }

    private function errors_bulk_action() {
        check_admin_referer('bulk-errors');

        $action = isset($_GET['action']) && $_GET['action'] != '' && $_GET['action'] != '-1' ? $_GET['action'] : '';

        if ($action == '') {
            $action = isset($_GET['action2']) && $_GET['action2'] != '' && $_GET['action2'] != '-1' ? $_GET['action2'] : '';
        }

        if ($action != '') {
            $ids = isset($_GET['error']) ? (array)$_GET['error'] : array();

            if (!empty($ids)) {
                if ($action == 'delete') {
                    $this->action_delete_errors($ids);
                }
            }

            wp_redirect('admin.php?page=gd-bbpress-toolbox-errors&message=errors-deleted');
            exit;
        }
    }
}
