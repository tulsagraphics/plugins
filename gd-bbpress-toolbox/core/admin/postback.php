<?php

if (!defined('ABSPATH')) exit;

class gdbbx_admin_postback {
    public function __construct() {
        $page = isset($_POST['option_page']) ? $_POST['option_page'] : false;

        if ($page !== false) {
            if ($page == 'gd-bbpress-toolbox-tools') {
                $this->tools();
            }

            if ($page == 'gd-bbpress-toolbox-settings') {
                $this->settings();
            }

            if ($page == 'gd-bbpress-toolbox-wizard') {
                $this->wizard();
            }

            do_action('gdbbx_admin_postback_handler', $page);
        }
    }

    private function tools() {
        check_admin_referer('gd-bbpress-toolbox-tools-options');

        $post = $_POST['gdbbxtools'];
        $action = $post['panel'];

        $url = 'admin.php?page=gd-bbpress-toolbox-tools&panel='.$action;

        $message = 'nothing';
        if ($action == 'remove') {
            if (isset($post['remove']['settings']) && $post['remove']['settings'] == 'on') {
                gdbbx()->remove_plugin_settings();

                $message = 'removed';
            }

            if (isset($post['remove']['forums']) && $post['remove']['forums'] == 'on') {
                gdbbx()->remove_forums_settings();

                $message = 'removed';
            }

            if (isset($post['remove']['tracking']) && $post['remove']['tracking'] == 'on') {
                gdbbx()->remove_tracking_settings();

                $message = 'removed';
            }

            if (isset($post['remove']['signature']) && $post['remove']['signature'] == 'on') {
                gdbbx()->remove_signature_settings();

                $message = 'removed';
            }
        } else if ($action == 'wp44_update') {
            require_once(GDBBX_PATH.'core/admin/update.php');

            $count = gdbbx_shortcodes_wp44_update();

            gdbbx_settings()->set('wp44_update', true, 'core', true);

            $url = 'admin.php?page=gd-bbpress-toolbox-tools';
            $message = 'wp44update&count='.$count;
        } else if ($action == 'removeips') {
            if (isset($post['removeips']['remove']) && $post['removeips']['remove'] == 'on') {
                require_once(GDBBX_PATH.'core/objects/core.cleanup.php');

                $_ips_count = gdbbx_cleanup()->delete_author_ips_from_postmeta();

                if ($_ips_count > 0) {
                    $message = 'ips-removed&ips='.$_ips_count;
                }
            }
        } else if ($action == 'cleanup') {
            if (isset($post['cleanup']['thanks']) && $post['cleanup']['thanks'] == 'on') {
                require_once(GDBBX_PATH.'core/objects/core.cleanup.php');

                $_thanks_count = gdbbx_cleanup()->delete_thanks_for_missing_posts();

                if ($_thanks_count > 0) {
                    $message = 'cleanup-thanks&thanks='.$_thanks_count;
                }
            }
        } else if ($action == 'close') {
            $_topics_closed = 0;

            if (isset($post['close']['inactive']) && $post['close']['inactive'] == 'on') {
                $days = intval($post['close']['inactivity']);

                if ($days > 0) {
                    $_topics_closed+= gdbbx_db()->close_inactive_topics($days);
                }
            }

            if (isset($post['close']['old']) && $post['close']['old'] == 'on') {
                $days = intval($post['close']['age']);

                if ($days > 0) {
                    $_topics_closed+= gdbbx_db()->close_old_topics($days);
                }
            }

            if ($_topics_closed > 0) {
                $message = 'closed&topics='.$_topics_closed;
            }
        } else if ($action == 'import') {
            if (is_uploaded_file($_FILES['import_file']['tmp_name'])) {
                $data = file_get_contents($_FILES['import_file']['tmp_name']);
                $data = maybe_unserialize($data);

                if (is_object($data)) {
                    gdbbx()->import_from_object($data);

                    $message = 'imported';
                }
            }
        }

        wp_redirect($url.'&message='.$message);
        exit;
    }

    private function settings() {
        check_admin_referer('gd-bbpress-toolbox-settings-options');

        d4p_includes(array(
            array('name' => 'walkers', 'directory' => 'admin'),
            array('name' => 'settings', 'directory' => 'admin')
        ), GDBBX_D4PLIB);

        include(GDBBX_PATH.'core/admin/internal.php');

        $options = new gdbbx_admin_settings();
        $settings = $options->settings(gdbbx_admin()->panel);

        $processor = new d4pSettingsProcess($settings);
        $processor->base = 'gdbbxvalue';

        $data = $processor->process();

        foreach ($data as $group => $values) {
            foreach ($values as $name => $value) {
                gdbbx()->set($name, $value, $group);
            }

            gdbbx()->save($group);
        }

        if (gdbbx_admin()->page == 'views') {
            wp_flush_rewrite_rules();
        }

        $url = 'admin.php?page=gd-bbpress-toolbox-'.gdbbx_admin()->page.'&panel='.gdbbx_admin()->panel;
        wp_redirect($url.'&message=saved');
        exit;
    }

    private function wizard() {
        require_once(GDBBX_PATH.'core/objects/core.wizard.php');

        gdbbx_wizard()->panel_postback();
    }
}
