<?php

if (!defined('ABSPATH')) exit;

class gdbbx_core_bbpress {
    public $loop_posts = array();
    public $loop_users = array();

    public function __construct() {
        add_action('bbp_template_before_replies_loop', array($this, 'before_replies_loop'));
        add_action('bbp_template_before_topics_loop', array($this, 'before_topics_loop'));

        add_filter('bbp_get_template_stack', array($this, 'template_stack'));

        add_filter('bbp_kses_allowed_tags', array($this, 'kses_allowed_tags'), 10000);

        add_filter('upload_mimes', array($this, 'upload_mimes'));
    }

    public function before_replies_loop() {
        foreach (bbpress()->reply_query->posts as $post) {
            $this->loop_posts[] = $post->ID;

            if ($post->post_author > 0 && !in_array($post->post_author, $this->loop_users)) {
                $this->loop_users[] = $post->post_author;
            }
        }

        do_action('gdbbx_template_before_replies_loop', $this->loop_posts, $this->loop_users);
    }

    public function before_topics_loop() {
        foreach (bbpress()->topic_query->posts as $post) {
            $this->loop_posts[] = $post->ID;

            if ($post->post_author > 0 && !in_array($post->post_author, $this->loop_users)) {
                $this->loop_users[] = $post->post_author;
            }
        }

        do_action('gdbbx_template_before_topics_loop', $this->loop_posts, $this->loop_users);
    }

    public function template_stack($stack) {
        $stack[] = GDBBX_PATH.'templates/default/bbpress';

        return $stack;
    }

    public function kses_allowed_tags($list) {
        $tags = gdbbx()->get('kses_allowed_override', 'bbpress');

        if ($tags == 'bbpress') {
            if (gdbbx()->get('allowed_tags_div', 'bbpress')) {
                $list['div'] = array('class' => true, 'title' => true);
            }

            if (gdbbx()->get('kses_img_class', 'bbpress')) {
                $list['img']['class'] = true;
            }
        } else if ($tags == 'post') {
            $list = wp_kses_allowed_html('post');
        } else if ($tags == 'expanded') {
            $list = d4p_kses_expanded_list_of_tags();
        }

        return $list;
    }

    public function upload_mimes($mimes) {
        $custom = gdbbx()->get('extra_mime_types', 'tools');

        if (!empty($custom)) {
            return array_merge($mimes, $custom);
        }

        return $mimes;
    }
}
