<?php

if (!defined('ABSPATH')) exit;

class gdbbx_core_cleanup {
    public function __construct() { }

    public function count_thanks_for_missing_posts() {
        $sql = "SELECT COUNT(*) FROM ".gdbbx_db()->actions." WHERE `action` = 'thanks' AND post_id NOT IN (SELECT ID FROM ".gdbbx_db()->wpdb()->posts.")";

        return gdbbx_db()->get_var($sql);
    }

    public function delete_thanks_for_missing_posts() {
        $sql = "DELETE FROM ".gdbbx_db()->actions." WHERE `action` = 'thanks' AND post_id NOT IN (SELECT ID FROM ".gdbbx_db()->wpdb()->posts.")";

        gdbbx_db()->query($sql);
        return gdbbx_db()->rows_affected();
    }

    public function delete_author_ips_from_postmeta() {
        $sql = "DELETE FROM ".gdbbx_db()->wpdb()->postmeta." WHERE `meta_key` = '_bbp_author_ip'";

        gdbbx_db()->query($sql);
        return gdbbx_db()->rows_affected();
    }
}

global $_gdbbx_core_cleanup;
$_gdbbx_core_cleanup = new gdbbx_core_cleanup();

function gdbbx_cleanup() {
    global $_gdbbx_core_cleanup;
    return $_gdbbx_core_cleanup;
}
