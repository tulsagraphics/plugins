<?php

if (!defined('ABSPATH')) exit;

class gdbbx_core_db extends d4p_wpdb_core {
    public $db_site = array();

    public $_prefix = 'gdbbx';
    public $_tables = array(
        'actions', 
        'actionmeta',
        'tracker'
    );
    public $_metas = array(
        'action' => 'action_id'
    );

    /** @global wpdb $wpdb */
    public function init() {
        global $wpdb;

        $plugin = new stdClass();
        $this->db = new stdClass();
        $this->db_site = array();

        foreach ($this->_tables as $name) {
            $prefix = in_array($name, $this->_network_tables) ? $wpdb->base_prefix : $wpdb->prefix;

            $wpdb_name = $this->_prefix.'_'.$name;
            $real_name = $prefix.$wpdb_name;

            $plugin->$name = $real_name;
            $this->db->$name = $real_name;

            $wpdb->$wpdb_name = $real_name;

            if (!in_array($name, $this->_network_tables)) {
                $this->db_site[] = $real_name;
            }
        }

        $wpdb->{$this->_prefix} = $plugin;

        if (!empty($this->_prefix) && !empty($this->_metas)) {
            foreach ($this->_metas as $key => $id) {
                $this->_meta_translate[$this->_prefix.'_'.$key.'_id'] = $id;
            }

            add_filter('sanitize_key', array($this, 'sanitize_meta'));
        }
    }

    public function get_user_favorites_topic_ids($user_id = 0) {
        $user_id = bbp_get_user_id($user_id);

        if ($user_id > 0) {
            if (gdbbx_bbpress_version() < 26) {
                $ids = get_user_option('_bbp_favorites', $user_id);
                $ids = array_filter(wp_parse_id_list($ids));

                return $ids;
            } else {
                $sql = "SELECT p.ID FROM ".$this->wpdb()->posts." p INNER JOIN ".$this->wpdb()->postmeta." m ON p.ID = m.post_id ";
                $sql.= "WHERE m.meta_key = '_bbp_favorite' AND m.meta_value = '".absint($user_id)."' ";
                $sql.= "AND p.post_type = '".bbp_get_topic_post_type()."' ";
                $sql.= "AND p.post_status IN ('".join("', '", gdbbx_get_all_topic_statuses())."') ";
                $sql.= "GROUP BY p.ID ORDER BY p.post_date DESC";

                $ids = $this->get_results($sql);

                return wp_list_pluck($ids, 'ID');
            }
        }

        return array();
    }

    public function get_user_subscribed_topic_ids($user_id = 0) {
        $user_id = bbp_get_user_id($user_id);

        if ($user_id > 0) {
            if (gdbbx_bbpress_version() < 26) {
                $ids = get_user_option('_bbp_subscriptions', $user_id);
                $ids = array_filter(wp_parse_id_list($ids));

                return $ids;
            } else {
                $sql = "SELECT p.ID FROM ".$this->wpdb()->posts." p INNER JOIN ".$this->wpdb()->postmeta." m ON p.ID = m.post_id ";
                $sql.= "WHERE m.meta_key = '_bbp_subscription' AND m.meta_value = '".absint($user_id)."' ";
                $sql.= "AND p.post_type = '".bbp_get_topic_post_type()."' ";
                $sql.= "AND p.post_status IN ('".join("', '", gdbbx_get_all_topic_statuses())."') ";
                $sql.= "GROUP BY p.ID ORDER BY p.post_date DESC";

                $ids = $this->get_results($sql);

                return wp_list_pluck($ids, 'ID');
            }
        }

        return array();
    }

    public function get_topics_count_since($timestamp) {
        $from = date('Y-m-d H:i:s', $timestamp);

        $sql = "SELECT COUNT(*) FROM ".$this->wpdb()->posts."
                WHERE post_date_gmt > '".$from."' AND post_type = '".bbp_get_topic_post_type()."' 
                AND post_status IN ('publish', 'closed')";

        return $this->get_var($sql);
    }

    public function get_replies_count_since($timestamp) {
        $from = date('Y-m-d H:i:s', $timestamp);

        $sql = "SELECT COUNT(*) FROM ".$this->wpdb()->posts."
                WHERE post_date_gmt > '".$from."' AND post_type = '".bbp_get_reply_post_type()."' 
                AND post_status IN ('publish', 'closed')";

        return $this->get_var($sql);
    }

    public function get_total_users_count() {
        $query = "SELECT COUNT(*) FROM ".$this->users." u INNER JOIN ".$this->usermeta." m ON m.user_id = u.ID 
                  WHERE meta_key = '".$this->prefix()."capabilities' AND meta_value != 'a:0:{}'";

        return absint($this->get_var($query));
    }

    public function track_topic_visit($user_id, $topic_id, $forum_id, $reply_id) {
        $previous = gdbbx_cache()->tracking_topic_last_visit($topic_id, $user_id);

        $latest = $this->datetime();

        if ($previous === false) {
            $this->insert($this->tracker, array(
                'user_id' => $user_id,
                'topic_id' => $topic_id,
                'forum_id' => $forum_id,
                'reply_id' => $reply_id,
                'latest' => $latest
            ));
        } else {
            $this->update($this->tracker, array(
                'forum_id' => $forum_id,
                'reply_id' => $reply_id,
                'latest' => $latest
            ), array(
                'user_id' => $user_id,
                'topic_id' => $topic_id
            ));
        }
    }

    public function get_topic_replies_ids($topic_id) {
        $sql = $this->wpdb()->prepare(
            "SELECT ID FROM ".$this->wpdb()->posts." WHERE post_type = %s AND ID IN 
                (".$this->_reply_inner_query($topic_id).") AND post_status IN ('publish', 'closed')", 
                bbp_get_reply_post_type()
            );

        $raw = $this->get_results($sql);

        return wp_list_pluck($raw, 'ID');
    }

    public function get_topic_participants($topic_id) {
        $sql = $this->wpdb()->prepare(
            "SELECT DISTINCT post_author FROM ".$this->wpdb()->posts." WHERE post_type = %s AND ID IN 
                (".$this->_reply_inner_query($topic_id).") AND post_status IN ('publish', 'closed')", 
                bbp_get_reply_post_type()
            );

        $raw = $this->get_results($sql);

        $users = array(absint(bbp_get_topic_author_id($topic_id)));

        foreach ($raw as $user) {
            $users[] = absint($user->post_author);
        }

        return array_unique(array_filter($users));
    }

    public function get_forum_last_post_time($forum_ids, $format = 'G') {
        $query = "SELECT p.post_date_gmt FROM ".$this->wpdb()->posts." p
                  INNER JOIN ".$this->wpdb()->postmeta." m ON m.post_id = p.ID AND m.meta_key = '_bbp_forum_id'
                  WHERE p.post_type IN ('".bbp_get_topic_post_type()."', '".bbp_get_topic_post_type()."') 
                  AND p.post_status IN ('publish', 'closed') 
                  AND CAST(m.meta_value AS UNSIGNED) IN (".join(', ', $forum_ids).")
                  ORDER BY p.post_date_gmt DESC LIMIT 0, 1";
        $date = $this->get_var($query);

        if (is_null($date)) {
            return false;
        }

        return mysql2date($format, $date);
    }

    public function get_forum_last_visit($user_id, $forum_ids) {
        $query = $this->wpdb()->prepare(
            "SELECT forum_id, topic_id, reply_id, latest FROM ".$this->tracker."
                WHERE user_id = %s AND forum_id in (".join(', ', $forum_ids).")
                ORDER BY latest DESC LIMIT 0, 1",
                $user_id);

        $latest = $this->get_row($query);

        if (is_null($latest)) {
            return false;
        } else {
            $latest->latest = mysql2date('G', $latest->latest);
        }

        return $latest;
    }

    public function get_topic_last_visit($user_id, $topic_id) {
        $query = $this->wpdb()->prepare(
            "SELECT forum_id, reply_id, latest FROM ".$this->tracker." WHERE user_id = %s AND topic_id = %s",
                $user_id, $topic_id);

        $latest = $this->get_row($query);
        
        if (is_null($latest)) {
            return false;
        } else {
            $latest->latest = mysql2date('G', $latest->latest);
        }

        return $latest;
    }

    public function get_topic_next_reply_id($topic_id, $reply_id) {
        $query = $this->wpdb()->prepare(
            "SELECT post_id FROM ".$this->wpdb()->postmeta." WHERE meta_key = '_bbp_topic_id' 
            AND meta_value = %s AND post_id > %d ORDER BY post_id ASC LIMIT 0, 1",
                $topic_id, $reply_id
            );

        $next_reply_id = $this->get_var($query);

        return is_null($next_reply_id) ? false : intval($next_reply_id);
    }

    public function get_attachment_id_from_name($name) {
        $query = $this->wpdb()->prepare(
            "SELECT post_id FROM ".$this->wpdb()->postmeta." WHERE meta_key = '_bbp_attachment_name' AND meta_value = '%s';", 
                $name);

        $result = $this->get_var($query);

        return $result ? absint($result) : 0;
    }

    public function get_attachment_id_from_name_alt($name) {
        $query = $this->wpdb()->prepare(
            "SELECT post_id FROM ".$this->wpdb()->postmeta." WHERE meta_key = '_wp_attached_file' AND meta_value LIKE '%s';", 
                '%'.$name);

        $result = $this->get_var($query);

        return $result ? absint($result) : 0;
    }

    public function get_topic_attachments_count($topic_id) {
        $topic_id = absint($topic_id);

        $count = get_post_meta($topic_id, '_bbp_attachments_count', true);

        if ($count == '') {
            $count = $this->update_topic_attachments_count($topic_id);
        }

        return $count;
    }

    public function get_topic_thread_attachments_ids($topic_id) {
        $topic_id = absint($topic_id);
        
        $sql = "SELECT ID FROM ".$this->wpdb()->posts." WHERE (post_parent IN (".$this->_reply_inner_query($topic_id).") OR post_parent = ".$topic_id.") AND post_type = 'attachment'";
        $raw = $this->get_results($sql);

        $attachments = wp_list_pluck($raw, 'ID');

        if (!empty($attachments)) {
            $attachments = array_map('absint', $attachments);
            $attachments = array_filter($attachments);
        }

        return array();
    }

    public function update_topic_attachments_count($topic_id) {
        $topic_id = absint($topic_id);
        
        $sql = "SELECT COUNT(*) FROM ".$this->wpdb()->posts." WHERE (post_parent IN (".$this->_reply_inner_query($topic_id).") OR post_parent = ".$topic_id.") AND post_type = 'attachment'";

        $count = absint($this->get_var($sql));

        update_post_meta($topic_id, '_bbp_attachments_count', $count);

        return $count;
    }

    public function get_users_active_in_past($seconds = 86400, $limit = 0) {
        $min = intval($this->timestamp() - $seconds);

        $sql = "SELECT user_id, CAST(meta_value AS UNSIGNED) as last_active
                FROM ".$this->wpdb()->usermeta."
                WHERE meta_key = '".gdbbx_plugin()->user_meta_key_last_activity()."'
                AND CAST(meta_value AS UNSIGNED) > ".$min."
                ORDER BY last_active DESC";

        if ($limit > 0) {
            $sql.= " LIMIT 0, ".absint($limit);
        }

        $raw = $this->get_results($sql);

        $users = array();

        foreach ($raw as $user) {
            $users[$user->user_id] = $user->last_active;
        }

        return $users;
    }

    public function get_topics_with_user_reply($user_id, $offset = 0, $limit = 4096) {
        $sql = $this->wpdb()->prepare("SELECT DISTINCT CAST(m.meta_value AS UNSIGNED) as topic
                FROM ".$this->wpdb()->posts." p INNER JOIN ".$this->wpdb()->postmeta." m 
                ON m.post_id = p.ID AND m.meta_key = '_bbp_topic_id'
                WHERE p.post_type = '".bbp_get_reply_post_type()."' AND p.post_author = %d
                ORDER BY p.ID DESC LIMIT %d, %d", $user_id, $offset, $limit);
        $raw = $this->get_results($sql);

        return wp_list_pluck($raw, 'topic');
    }

    public function get_forum_visit_time($user_id, $forum_id, $return = 'timestamp') {
        $query = $this->wpdb()->prepare(
            "SELECT latest FROM ".$this->tracker." WHERE user_id = %s AND forum_id = %s",
                $user_id, $forum_id
            );

        $latest = $this->get_var($query);

        if (is_null($latest)) {
            return false;
        } else {
            if ($return == 'timestamp') {
                return mysql2date('G', $latest);
            }
        }

        return $latest;
    }

    public function get_forum_children($forum_id) {
        $query = $this->wpdb()->prepare(
                "SELECT ID FROM ".$this->wpdb()->posts." WHERE post_parent = %d 
                AND post_type = %s AND post_status IN ('publish', 'private')", 
                $forum_id, bbp_get_forum_post_type()
            );
        $raw = $this->get_results($query);

        if (empty($raw)) {
            return array();
        } else {
            return wp_list_pluck($raw, 'ID');
        }
    }

    public function user_replied_to_topic($topic_id = 0, $user_id = 0) {
        if ($topic_id == 0) {
            $topic_id = bbp_get_topic_id();
        }

        if ($user_id == 0) {
            $user_id = bbp_get_current_user_id();
        }

        $sql = $this->wpdb()->prepare(
            "SELECT COUNT(*) FROM ".$this->wpdb()->posts." WHERE post_author = %d AND 
                ID IN (".$this->_reply_inner_query($topic_id).") AND post_type = %s", 
                $user_id, bbp_get_reply_post_type()
            );

        return $this->get_var($sql) > 0;
    }

    public function thanks_list_recent($limit = 3) {
        $sql = "SELECT a.user_id, a.post_id, p.post_author, p.post_type
                FROM ".$this->actions." a
                LEFT JOIN ".$this->wpdb()->posts." p ON p.ID = a.post_id AND p.post_type IN ('".bbp_get_topic_post_type()."', '".bbp_get_reply_post_type()."')
                WHERE a.`action` = 'thanks'
                ORDER BY a.action_id DESC
                LIMIT 0, ".$limit;
        return $this->get_results($sql);
    }

    public function thanks_statistics() {
        $statistics = array(
            'types' => array(
                bbp_get_topic_post_type() => 0,
                bbp_get_reply_post_type() => 0),
            'users' => array(
                'from' => 0, 
                'to' => 0)
        );

        $sql = "SELECT p.post_type, COUNT(*) AS thanks FROM ".$this->actions." a
                INNER JOIN ".$this->wpdb()->posts." p ON p.ID = a.post_id AND p.post_type IN ('".bbp_get_topic_post_type()."', '".bbp_get_reply_post_type()."')
                WHERE a.`action` = 'thanks' GROUP BY p.post_type";

        $raw = $this->get_results($sql);

        foreach ($raw as $r) {
            $statistics['types'][$r->post_type] = $r->thanks;
        }

        $sql = "SELECT count(distinct(a.user_id)) as thanks_from FROM ".$this->actions." a
                INNER JOIN ".$this->wpdb()->posts." p ON p.ID = a.post_id AND p.post_type IN ('".bbp_get_topic_post_type()."', '".bbp_get_reply_post_type()."')
                WHERE a.`action` = 'thanks'";
        $statistics['users']['from'] = $this->get_var($sql);

        $sql = "SELECT count(distinct(p.post_author)) as thanks_to FROM ".$this->actions." a
                INNER JOIN ".$this->wpdb()->posts." p ON p.ID = a.post_id AND p.post_type IN ('".bbp_get_topic_post_type()."', '".bbp_get_reply_post_type()."')
                WHERE a.`action` = 'thanks'";
        $statistics['users']['to'] = $this->get_var($sql);

        return $statistics;
    }

    public function thanks_add($post_id, $user_id) {
        $this->thanks_remove($post_id, $user_id);

        return $this->insert($this->actions, array('user_id' => $user_id, 'post_id' => $post_id, 'logged' => $this->datetime(), 'action' => 'thanks'));
    }

    public function thanks_remove($post_id, $user_id) {
        return $this->delete($this->actions, array('user_id' => $user_id, 'post_id' => $post_id, 'action' => 'thanks'));
    }

    public function thanks_given($post_id, $user_id) {
        $sql = $this->wpdb()->prepare(
            "SELECT COUNT(*) FROM ".$this->actions." WHERE post_id = %d AND user_id = %d AND action = 'thanks'", 
                $post_id, $user_id
            );

        return $this->get_var($sql) > 0;
    }

    public function thanks_list($post_id) {
        $sql = $this->wpdb()->prepare(
            "SELECT SQL_CALC_FOUND_ROWS user_id FROM ".$this->actions." WHERE action = 'thanks' AND 
            post_id = %d ORDER BY logged DESC", 
                $post_id
            );

        return wp_list_pluck($this->run($sql), 'user_id');
    }

    public function count_all_thanks_given($user_id) {
        $sql = $this->wpdb()->prepare(
            "SELECT COUNT(*) FROM ".$this->actions." WHERE action = 'thanks' AND user_id = %d", 
                $user_id
            );

        return $this->get_var($sql);
    }

    public function count_all_thanks_received($user_id) {
        $sql = $this->wpdb()->prepare(
            "SELECT COUNT(*) FROM ".$this->wpdb()->posts." p INNER JOIN ".$this->actions." a ON 
            a.post_id = p.ID WHERE a.action = 'thanks' AND p.post_author = %d", 
                $user_id
            );

        return $this->get_var($sql);
    }

    public function bulk_thanks_list($posts) {
        if (empty($posts)) {
            return array();
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS post_id, user_id FROM ".$this->actions." WHERE action = 'thanks' AND 
            post_id IN (".join(',', (array)$posts).") ORDER BY post_id ASC, logged DESC";

        $raw = $this->run($sql);
        $results = array();

        foreach ($raw as $row) {
            if (!isset($results[$row->post_id])) {
                $results[$row->post_id] = array();
            }

            $results[$row->post_id][] = $row->user_id;
        }

        return $results;
    }

    public function bulk_thanks_given($posts, $user_id) {
        if (empty($posts) || empty($user_id)) {
            return array();
        }

        $sql = $this->wpdb()->prepare(
            "SELECT post_id FROM ".$this->actions." WHERE post_id IN (".join(', ', $posts).") AND 
             user_id = %d AND action = 'thanks'", 
                $user_id
            );

        return $this->get_results($sql);
    }

    public function bulk_count_all_thanks_given($users) {
        if (empty($users)) {
            return array();
        }

        $sql = "SELECT user_id, COUNT(*) as thanks FROM ".$this->actions." WHERE action = 'thanks' AND 
            user_id IN (".join(', ', (array)$users).") GROUP BY user_id";

        return $this->get_results($sql);
    }

    public function bulk_count_all_thanks_received($users) {
        if (empty($users)) {
            return array();
        }

        $sql = "SELECT p.post_author as user_id, COUNT(*) as thanks FROM ".$this->wpdb()->posts." p INNER JOIN ".$this->actions." a ON 
            a.post_id = p.ID WHERE a.action = 'thanks' AND p.post_author IN (".join(', ', (array)$users).") 
            GROUP BY p.post_author";

        return $this->get_results($sql);
    }

    public function bulk_count_all_topics_replies($users) {
        if (empty($users)) {
            return array();
        }

        $sql = "SELECT post_author AS user_id, post_type, COUNT(*) AS posts FROM ".$this->wpdb()->posts."  
            WHERE post_type IN ('".bbp_get_topic_post_type()."', '".bbp_get_reply_post_type()."') 
            AND post_status IN ('publish', 'private', 'closed') 
            AND post_author IN (".join(', ', (array)$users).") GROUP BY post_author, post_type";

        $raw = $this->get_results($sql);
        $results = array();

        foreach ($raw as $row) {
            if (!isset($results[$row->user_id])) {
                $results[$row->user_id] = array(
                    bbp_get_topic_post_type() => 0,
                    bbp_get_reply_post_type() => 0
                );
            }

            $results[$row->user_id][$row->post_type] = $row->posts;
        }

        return $results;
    }

    public function bulk_count_attachments($posts) {
        if (empty($posts)) {
            return array();
        }

        $sql = "SELECT r.ID as post_id, count(a.ID) AS attachments FROM ".$this->wpdb()->posts." a
                INNER JOIN ".$this->wpdb()->posts." r ON r.ID = a.post_parent WHERE 
                r.post_type in ('".bbp_get_topic_post_type()."', '".bbp_get_reply_post_type()."') 
                AND a.post_type = 'attachment' AND r.ID IN (".join(',', (array)$posts).") 
                GROUP BY post_id";

        $raw = $this->run($sql);
        $results = array();

        foreach ($raw as $row) {
            $results[$row->post_id] = $row->attachments;
        }

        return $results;
    }

    public function bulk_count_topic_attachments($posts) {
        if (empty($posts)) {
            return array();
        }

        $sql = "SELECT post_id, CAST(meta_value AS UNSIGNED) AS attachments FROM ".$this->wpdb()->postmeta." WHERE 
                meta_key = '_bbp_attachments_count' AND post_id IN (".join(',', (array)$posts).")";

        $raw = $this->run($sql);
        $results = array();

        foreach ($raw as $row) {
            $results[$row->post_id] = $row->attachments;
        }

        return $results;
    }

    public function bulk_user_replied_to_topics($topics, $user_id) {
        if (empty($topics) || empty($user_id)) {
            return array();
        }

        $sql = $this->wpdb()->prepare(
            "SELECT DISTINCT CAST(m.meta_value AS UNSIGNED) AS topic_id FROM ".$this->wpdb()->postmeta." m 
             INNER JOIN ".$this->wpdb()->posts." p ON p.ID = m.post_id 
             WHERE m.meta_key = '_bbp_topic_id' AND m.meta_value IN (".join(', ', (array)$topics).") AND 
             m.post_id != m.meta_value AND p.post_author = %d", 
                $user_id
            );

        return wp_list_pluck($this->get_results($sql), 'topic_id');
    }

    public function bulk_list_topic_replies($topics) {
        if (empty($topics)) {
            return array();
        }

        $sql = "SELECT CAST(meta_value as UNSIGNED) as reply FROM ".$this->wpdb()->postmeta." WHERE 
                meta_value != '0' AND meta_key = '_bbp_last_reply_id' AND post_id IN (".join(', ', (array)$topics).")";
        $raw = $this->run($sql);

        return wp_list_pluck($raw, 'reply');
    }

    public function bulk_list_topics_last_visit($topics, $user_id) {
        if (empty($topics) || empty($user_id)) {
            return array();
        }

        $query = $this->wpdb()->prepare(
            "SELECT forum_id, topic_id, reply_id, latest FROM ".$this->tracker." WHERE 
             user_id = %s AND topic_id IN (".join(',', (array)$topics).")",
                $user_id);

        $raw = $this->run($query);
        $results = array();

        foreach ($raw as $row) {
            $row->latest = mysql2date('G', $row->latest);
            $results[$row->topic_id] = $row;
        }

        return $results;
    }

    public function report_list_recent($limit = 3) {
        $sql = "SELECT a.user_id, a.post_id, p.post_author, p.post_type, r.meta_value as report
                FROM ".$this->actions." a
                LEFT JOIN ".$this->wpdb()->posts." p ON p.ID = a.post_id AND p.post_type IN ('".bbp_get_topic_post_type()."', '".bbp_get_reply_post_type()."')
                INNER JOIN ".$this->actionmeta." m ON m.action_id = a.action_id AND m.meta_key = 'status'
                INNER JOIN ".$this->actionmeta." r ON r.action_id = a.action_id AND r.meta_key = 'content'
                WHERE a.`action` = 'report' AND m.meta_value = 'waiting'
                ORDER BY a.action_id DESC
                LIMIT 0, ".$limit;
        return $this->get_results($sql);
    }

    public function report_statistics() {
        $sql = "SELECT r.post_type, r.report, COUNT(*) AS reported
                FROM (
                    SELECT t.meta_value AS post_type, IF (p.ID IS NULL, 'closed', m.meta_value) AS report
                    FROM ".$this->actions." a
                    LEFT JOIN ".$this->wpdb()->posts." p ON p.ID = a.post_id 
                         AND p.post_type IN ('".bbp_get_topic_post_type()."', '".bbp_get_reply_post_type()."')
                    INNER JOIN ".$this->actionmeta." m ON m.action_id = a.action_id AND m.meta_key = 'status'
                    INNER JOIN ".$this->actionmeta." t ON t.action_id = a.action_id AND t.meta_key = 'type'
                    WHERE a.action = 'report') r
                GROUP BY r.post_type, r.report";
        $raw = $this->get_results($sql);

        $list = array();

        foreach ($raw as $r) {
            if (!isset($list[$r->post_type])) {
                $list[$r->post_type] = array('total' => 0, 'active' => 0);
            }

            $list[$r->post_type]['total']+= $r->reported;

            if ($r->report == 'waiting') {
                $list[$r->post_type]['active']+= $r->reported;
            }
        }

        return $list;
    }

    public function report_given($post_id, $user_id) {
        $sql = $this->wpdb()->prepare(
            "SELECT COUNT(*) FROM ".$this->actions." a INNER JOIN ".$this->actionmeta." m ON m.action_id = a.action_id
            WHERE a.post_id = %d AND a.user_id = %d AND a.action = 'report' AND m.meta_key = 'status' AND m.meta_value = 'waiting'", 
                $post_id, $user_id
            );

        return $this->get_var($sql) > 0;
    }

    public function report_add($post_id, $user_id, $content = '') {
        $this->insert($this->actions, array('user_id' => $user_id, 'post_id' => $post_id, 'logged' => $this->datetime(), 'action' => 'report'));

        $action_id = $this->get_insert_id();
        $type = bbp_is_reply($post_id) ? bbp_get_reply_post_type() : bbp_get_topic_post_type();

        $this->insert($this->actionmeta, array('action_id' => $action_id, 'meta_key' => 'content', 'meta_value' => $content));
        $this->insert($this->actionmeta, array('action_id' => $action_id, 'meta_key' => 'type', 'meta_value' => $type));
        $this->insert($this->actionmeta, array('action_id' => $action_id, 'meta_key' => 'status', 'meta_value' => 'waiting'));
    }

    public function report_closed($id, $user_id) {
        $sql = "SELECT post_id FROM ".$this->actions." WHERE action_id = ".$id;
        $post_id = absint($this->get_var($sql));

        if ($post_id > 0) {
            $this->insert($this->actions, array('user_id' => $user_id, 'post_id' => $post_id, 'logged' => $this->datetime(), 'action' => 'report-closed', 'reference_id' => $id));
        }
    }

    public function report_status($report_id, $status) {
        $this->update($this->actionmeta, array('meta_value' => $status), array('action_id' => $report_id, 'meta_key' => 'status'));
    }

    public function reported($post_id) {
        $sql = $this->wpdb()->prepare(
            "SELECT user_id FROM ".$this->actions." a INNER JOIN ".$this->actionmeta." m ON m.action_id = a.action_id
            WHERE a.post_id = %d AND a.action = 'report' AND m.meta_key = 'status' AND m.meta_value = 'waiting'", 
                $post_id
            );

        return wp_list_pluck($this->run($sql), 'user_id');
    }

    public function bulk_reported($posts) {
        $sql = "SELECT a.post_id, a.user_id FROM ".$this->actions." a INNER JOIN ".$this->actionmeta." m ON m.action_id = a.action_id
            WHERE a.post_id IN (".join(',', (array)$posts).") AND a.action = 'report' AND m.meta_key = 'status' AND m.meta_value = 'waiting'";

        $raw = $this->run($sql);
        $results = array();

        foreach ($raw as $row) {
            if (!isset($results[$row->post_id])) {
                $results[$row->post_id] = array();
            }

            $results[$row->post_id][] = $row->user_id;
        }

        return $results;
    }

    public function close_old_topics($days) {
        $sql = "UPDATE ".$this->wpdb()->posts." SET post_status = '".bbp_get_closed_status_id()."'
                WHERE post_type = '".bbp_get_topic_post_type()."'
                AND post_status = '".bbp_get_public_status_id()."' AND post_date < DATE_SUB(curdate(), interval ".intval($days)." day)";

        return $this->query($sql);
    }

    public function close_inactive_topics($days) {
        $sql = "UPDATE ".$this->wpdb()->posts." p INNER JOIN ".$this->wpdb()->postmeta." m ON m.post_id = p.ID
                SET p.post_status = '".bbp_get_closed_status_id()."'
                WHERE p.post_type = '".bbp_get_topic_post_type()."'
                AND p.post_status = '".bbp_get_public_status_id()."' 
                AND m.meta_key = '_bbp_last_active_time' 
                AND STR_TO_DATE(m.meta_value, '%Y-%m-%d %T') < DATE_SUB(curdate(), interval ".intval($days)." day)";

        return $this->query($sql);
    }

    public function count_recent_posts($seconds, $until = null, $gmt = false) {
        $post_types = array(
            bbp_get_topic_post_type(),
            bbp_get_reply_post_type()
        );

        $post_statuses = array(
            bbp_get_public_status_id(),
            bbp_get_private_status_id(),
            bbp_get_closed_status_id()
        );

        $until = is_null($until) ? $this->datetime($gmt) : $until;
        $column = $gmt ? 'post_date_gmt' : 'post_date';

        $sql = "SELECT post_type, count(*) AS posts FROM ".$this->wpdb()->posts." WHERE post_type IN ('".join("', '", $post_types)."') AND post_status IN ('".join("', '", $post_statuses)."') AND ".$column." > DATE_SUB(%s, INTERVAL %d SECOND) AND ".$column." <= %s GROUP BY post_type";
        $sql = $this->prepare($sql, $until, $seconds, $until);

        $raw = $this->get_results($sql);
        $out = array();

        foreach ($raw as $r) {
            $out[$r->post_type] = absint($r->posts);
        }

        return $out;
    }

    private function _reply_inner_query($topic_id) {
        return $this->wpdb()->prepare(
            "SELECT post_id FROM ".$this->wpdb()->postmeta."
            WHERE meta_key = '_bbp_topic_id' AND meta_value = %s AND post_id != %d", 
                $topic_id, $topic_id);
    }
}
