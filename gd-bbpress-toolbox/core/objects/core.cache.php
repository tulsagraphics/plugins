<?php

if (!defined('ABSPATH')) { exit; }

class gdbbx_core_cache extends d4p_cache_core {
    public $store = 'gdbbx';

    public function tracking_topic_last_visit($topic_id, $user_id = 0) {
        if ($user_id == 0) {
            $user_id = bbp_get_current_user_id();
        }

        if (!$this->in('topic-tracking', $topic_id)) {
            $latest = gdbbx_db()->get_topic_last_visit($user_id, $topic_id);
            $this->set('topic-tracking', $topic_id, $latest);
        }

        return $this->get('topic-tracking', $topic_id, false);
    }

    public function tracking_run_bulk_topics($posts, $user_id = 0) {
        if ($user_id == 0) {
            $user_id = bbp_get_current_user_id();
        }

        $raw = gdbbx_db()->bulk_list_topics_last_visit($posts, $user_id);

        foreach ($raw as $post_id => $track) {
            $this->set('topic-tracking', $post_id, $track);
        }

        foreach ($posts as $post_id) {
            if (!$this->in('topic-tracking', $post_id)) {
                $this->set('topic-tracking', $post_id, false);
            }
        }
    }

    public function userreplied_user_replied($post_id, $user_id = 0) {
        if ($user_id == 0) {
            $user_id = bbp_get_current_user_id();
        }

        if (!$this->in('user-replied', $post_id)) {
            $this->userreplied_run_bulk_topics(array($post_id), $user_id);
        }

        return $this->get('user-replied', $post_id, false);
    }

    public function userreplied_run_bulk_topics($posts, $user_id = 0) {
        if ($user_id == 0) {
            $user_id = bbp_get_current_user_id();
        }

        $raw = gdbbx_db()->bulk_user_replied_to_topics($posts, $user_id);

        foreach ($raw as $post_id) {
            $this->set('user-replied', $post_id, true);
        }

        foreach ($posts as $post_id) {
            if (!$this->in('user-replied', $post_id)) {
                $this->set('user-replied', $post_id, false);
            }
        }
    }

    public function userstats_count_posts($user_id, $type = 'topic') {
        if (!$this->in('user-posts-count', $user_id)) {
            $this->userstats_run_bulk_counts(array($user_id));
        }

        $data = $this->get('user-posts-count', $user_id, array(
            bbp_get_topic_post_type() => 0,
            bbp_get_reply_post_type() => 0
        ));

        if (isset($data[$type])) {
            return $data[$type];
        }

        return 0;
    }

    public function userstats_run_bulk_counts($users) {
        $raw = gdbbx_db()->bulk_count_all_topics_replies($users);

        foreach ($raw as $user_id => $counts) {
            $this->set('user-posts-count', $user_id, $counts);
        }

        foreach ($users as $user_id) {
            if (!$this->in('user-posts-count', $user_id)) {
                $this->set('user-posts-count', $user_id, array(
                    bbp_get_topic_post_type() => 0,
                    bbp_get_reply_post_type() => 0
                ));
            }
        }
    }

    public function attachments_has_attachments($post_id) {
        if (!$this->in('attachments-count', $post_id)) {
            $this->attachments_run_bulk_counts(array($post_id));
        }

        $count = $this->get('attachments-count', $post_id, 0);

        return $count > 0;
    }

    public function attachments_count_attachments($post_id) {
        if (!$this->in('attachments-count', $post_id)) {
            $this->attachments_run_bulk_counts(array($post_id));
        }

        return $this->get('attachments-count', $post_id, 0);
    }

    public function attachments_run_bulk_counts($posts) {
        $raw = gdbbx_db()->bulk_count_attachments($posts);

        foreach ($raw as $post_id => $count) {
            $this->set('attachments-count', $post_id, $count);
        }

        foreach ($posts as $post_id) {
            if (!$this->in('attachments-count', $post_id)) {
                $this->set('attachments-count', $post_id, 0);
            }
        }
    }

    public function attachments_has_topic_attachments($post_id) {
        if (!$this->in('attachments-topic-count', $post_id)) {
            $this->attachments_run_bulk_topics_counts(array($post_id));
        }

        $count = $this->get('attachments-topic-count', $post_id, 0);

        return $count > 0;
    }

    public function attachments_count_topic_attachments($post_id) {
        if (!$this->in('attachments-topic-count', $post_id)) {
            $this->attachments_run_bulk_topics_counts(array($post_id));
        }

        return $this->get('attachments-topic-count', $post_id, 0);
    }

    public function attachments_run_bulk_topics_counts($posts) {
        $raw = gdbbx_db()->bulk_count_topic_attachments($posts);

        foreach ($raw as $post_id => $count) {
            $this->set('attachments-topic-count', $post_id, $count);
        }

        foreach ($posts as $post_id) {
            if (!$this->in('attachments-topic-count', $post_id)) {
                $this->set('attachments-topic-count', $post_id, 0);
            }
        }
    }

    public function report_user_reported($post_id, $user_id = 0) {
        if ($user_id == 0) {
            $user_id = bbp_get_current_user_id();
        }

        if (!$this->in('report-list', $post_id)) {
            $users = gdbbx_db()->reported($post_id);
            $this->set('report-list', $post_id, $users);
        }

        $list = $this->get('report-list', $post_id, array());

        return in_array($user_id, $list);
    }

    public function report_is_reported($post_id) {
        if (!$this->in('report-list', $post_id)) {
            $users = gdbbx_db()->reported($post_id);
            $this->set('report-list', $post_id, $users);
        }

        $list = $this->get('report-list', $post_id, array());

        return !empty($list);
    }

    public function report_run_bulk_list($posts) {
        $raw = gdbbx_db()->bulk_reported($posts);

        foreach ($raw as $post_id => $users) {
            $this->set('report-list', $post_id, $users);
        }

        foreach ($posts as $post_id) {
            if (!$this->in('report-list', $post_id)) {
                $this->set('report-list', $post_id, array());
            }
        }
    }

    public function thanks_get_list($post_id) {
        if (!$this->in('thanks-list', $post_id)) {
            $thanks = gdbbx_db()->thanks_list($post_id);
            $this->set('thanks-list', $post_id, $thanks);
        }

        return $this->get('thanks-list', $post_id, array());
    }

    public function thanks_run_bulk_list($posts) {
        $raw = gdbbx_db()->bulk_thanks_list($posts);

        foreach ($raw as $post_id => $users) {
            $this->set('thanks-list', $post_id, $users);
        }

        foreach ($posts as $post_id) {
            if (!$this->in('thanks-list', $post_id)) {
                $this->set('thanks-list', $post_id, array());
            }
        }
    }

    public function thanks_get_given($post_id, $user_id = 0) {
        if ($user_id == 0) {
            $user_id = bbp_get_current_user_id();
        }

        if (!$this->in('thanks-thread-'.absint($post_id), $user_id)) {
            $thanks = gdbbx_db()->thanks_given($post_id, $user_id);
            $this->set('thanks-thread-'.absint($post_id), $user_id, $thanks);
        }

        return $this->get('thanks-thread-'.absint($post_id), $user_id, false);
    }

    public function thanks_run_bulk_given($posts, $user_id = 0) {
        if ($user_id == 0) {
            $user_id = bbp_get_current_user_id();
        }

        $raw = gdbbx_db()->bulk_thanks_given($posts, $user_id);

        foreach ($raw as $row) {
            $this->set('thanks-thread-'.absint($row->post_id), $user_id, true);
        }

        foreach ($posts as $post_id) {
            if (!$this->in('thanks-thread-'.absint($post_id), $user_id)) {
                $this->set('thanks-thread-'.absint($post_id), $user_id, false);
            }
        }
    }

    public function thanks_get_count_given($user_id = 0) {
        if ($user_id == 0) {
            $user_id = bbp_get_current_user_id();
        }

        if (!$this->in('thanks-given', absint($user_id))) {
            $thanks = gdbbx_db()->count_all_thanks_given($user_id);
            $this->set('thanks-given', absint($user_id), absint($thanks));
        }

        return $this->get('thanks-given', $user_id, 0);
    }

    public function thanks_get_count_received($user_id = 0) {
        if ($user_id == 0) {
            $user_id = bbp_get_current_user_id();
        }

        if (!$this->in('thanks-received', absint($user_id))) {
            $thanks = gdbbx_db()->count_all_thanks_received($user_id);
            $this->set('thanks-received', absint($user_id), absint($thanks));
        }

        return $this->get('thanks-received', $user_id, 0);
    }

    public function thanks_run_bulk_count_given($users) {
        $raw = gdbbx_db()->bulk_count_all_thanks_given($users);

        foreach ($raw as $row) {
            $this->set('thanks-given', absint($row->user_id), absint($row->thanks));
        }

        foreach ($users as $user) {
            if (!$this->in('thanks-given', absint($user))) {
                $this->set('thanks-given', absint($user), 0);
            }
        }
    }

    public function thanks_run_bulk_count_received($users) {
        $raw = gdbbx_db()->bulk_count_all_thanks_received($users);

        foreach ($raw as $row) {
            $this->set('thanks-received', absint($row->user_id), absint($row->thanks));
        }

        foreach ($users as $user) {
            if (!$this->in('thanks-received', absint($user))) {
                $this->set('thanks-received', absint($user), 0);
            }
        }
    }
}

global $_gdbbx_cache;
$_gdbbx_cache = new gdbbx_core_cache();

/** @return gdbbx_core_cache */
function gdbbx_cache() {
    global $_gdbbx_cache;
    return $_gdbbx_cache;
}
