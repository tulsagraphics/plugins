<?php

if (!defined('ABSPATH')) exit;

class gdbbx_core_wizard {
    public $panel = false;
    public $panels = array();

    public $_default = array(
        'toolbar' => array(
            'toolbar_active' => true,
            'toolbar_super_admin' => true,
            'toolbar_visitor' => true,
            'toolbar_roles' => null,
            'toolbar_title' => 'Forums',
            'toolbar_information' => true
        ),
        'signatures' => array(
            'signature_active' => true,
            'signature_limiter' => true,
            'signature_length' => 512,
            'signature_super_admin' => true,
            'signature_roles' => null,
            'signature_edit_super_admin' => true,
            'signature_edit_roles' => null,
            'signature_editor' => 'textarea',
            'signature_enhanced_active' => true,
            'signature_enhanced_method' => 'html',
            'signature_enhanced_super_admin' => true,
            'signature_enhanced_roles' => null,
            'signature_process_smilies' => true,
            'signature_process_chars' => true,
            'signature_process_autop' => true
        ),
        'quotes' => array(
            'quote_active' => true,
            'quote_location' => 'footer',
            'quote_super_admin' => true,
            'quote_roles' => null
        ),
        'thanks' => array(
            'active' => false,
            'removal' => false,
            'topic' => true,
            'reply' => true,
            'location' => 'footer',
            'allow_super_admin' => true,
            'allow_roles' => null,
            'limit_display' => 20
        ),
        'report' => array(
            'active' => true,
            'location' => 'footer',
            'allow_roles' => null,
            'notify_active' => true,
            'report_mode' => 'form',
            'scroll_form' => true,
            'show_report_status' => false,
            'notify_keymasters' => true,
            'notify_moderators' => true,
            'notify_shortcodes' => true
        ),
        'private' => array(
            'private_topic_form_position' => 'bbp_theme_before_topic_form_submit_wrapper',
            'private_reply_form_position' => 'bbp_theme_before_reply_form_submit_wrapper',
            'private_topics' => true,
            'private_topics_super_admin' => true,
            'private_topics_roles' => null,
            'private_topics_visitor' => false,
            'private_topics_default' => 'unchecked',
            'private_topics_icon' => true,
            'private_replies' => true,
            'private_replies_threaded' => true,
            'private_replies_super_admin' => true,
            'private_replies_roles' => null,
            'private_replies_visitor' => false,
            'private_replies_default' => 'unchecked',
            'private_replies_css_hide' => false
        ),
        'stats' => array(
            'users_stats_active' => true,
            'users_stats_super_admin' => true,
            'users_stats_visitor' => false,
            'users_stats_roles' => null,
            'users_stats_show_registration_date' => false,
            'users_stats_show_topics' => true,
            'users_stats_show_replies' => true,
            'users_stats_show_thanks_given' => false,
            'users_stats_show_thanks_received' => false
        ),
        'attachments' => array(
            'hide_from_visitors' => true,
            'preview_for_visitors' => false,
            'max_file_size' => 512,
            'max_to_upload' => 4,
            'file_target_blank' => false,
            'roles_to_upload' => null,
            'roles_no_limit' => array('bbp_keymaster'),
            'attachment_icon' => true,
            'attachment_icons' => true,
            'download_link_attribute' => true
        ),
        'tinymce' => array(
            'editor_topic_active' => true,
            'editor_topic_tinymce' => true,
            'editor_topic_teeny' => false,
            'editor_topic_media_buttons' => false,
            'editor_topic_wpautop' => true,
            'editor_topic_quicktags' => true,
            'editor_topic_textarea_rows' => 12,
            'editor_reply_active' => true,
            'editor_reply_tinymce' => true,
            'editor_reply_teeny' => false,
            'editor_reply_media_buttons' => false,
            'editor_reply_wpautop' => true,
            'editor_reply_quicktags' => true,
            'editor_reply_textarea_rows' => 12,
        ),
        'activity_off' => array(
            'latest_track_users_topic' => false,
            'latest_topic_new_replies_badge' => false,
            'latest_topic_new_replies_mark' => false,
            'latest_topic_new_replies_strong_title' => false,
            'latest_topic_new_replies_in_thread' => false,
            'latest_topic_new_topic_badge' => false,
            'latest_topic_new_topic_strong_title' => false,
            'latest_topic_unread_topic_badge' => false,
            'latest_topic_unread_topic_strong_title' => false,
            'latest_forum_new_posts_badge' => false,
            'latest_forum_new_posts_strong_title' => false,
            'latest_forum_unread_forum_badge' => false,
            'latest_forum_unread_forum_strong_title' => false,
            'track_last_activity_active' => false
        ),
        'activity' => array(
            'latest_track_users_topic' => true,
            'latest_use_cutoff_timestamp' => true,
            'latest_topic_new_replies_badge' => true,
            'latest_topic_new_replies_mark' => true,
            'latest_topic_new_replies_strong_title' => true,
            'latest_topic_new_replies_in_thread' => true,
            'latest_topic_new_topic_badge' => true,
            'latest_topic_new_topic_strong_title' => true,
            'latest_topic_unread_topic_badge' => true,
            'latest_topic_unread_topic_strong_title' => true,
            'latest_forum_new_posts_badge' => true,
            'latest_forum_new_posts_strong_title' => false,
            'latest_forum_unread_forum_badge' => true,
            'latest_forum_unread_forum_strong_title' => false,
            'track_last_activity_active' => true,
            'track_current_session_cookie_expiration' => 60,
            'track_basic_cookie_expiration' => 365
        )
    );
    
    public function __construct() {
        $this->_init_panels();
    }

    private function _init_panels() {
        $this->panels = array(
            'intro' => array('label' => __("Intro", "gd-bbpress-toolbox")),
            'modules' => array('label' => __("Modules", "gd-bbpress-toolbox")),
            'attachments' => array('label' => __("Attachments", "gd-bbpress-toolbox")),
            'editors' => array('label' => __("Editors", "gd-bbpress-toolbox")),
            'tracking' => array('label' => __("Tracking", "gd-bbpress-toolbox")),
            'finish' => array('label' => __("Finish", "gd-bbpress-toolbox"))
        );

        $this->setup_panel(gdbbx_admin()->panel);
    }

    public function setup_panel($panel) {
        $this->panel = $panel;

        if (!isset($this->panels[$panel]) || $panel === false || is_null($panel)) {
            $this->panel = 'intro';
        }
    }

    public function current_panel() {
        return $this->panel;
    }

    public function panels_index() {
        return array_keys($this->panels);
    }

    public function next_panel() {
        $panel = $this->current_panel();
        $all = $this->panels_index();

        $index = array_search($panel, $all);
        $next = $index + 1;

        if ($next == count($all)) {
            $next = 0;
        }

        return $all[$next];
    }

    public function is_last_panel() {
        $panel = $this->current_panel();
        $all = $this->panels_index();

        $index = array_search($panel, $all);

        return $index + 1 == count($all);
    }

    public function get_form_action() {
        return 'admin.php?page=gd-bbpress-toolbox-wizard&panel='.$this->next_panel();
    }

    public function get_form_nonce() {
        return wp_create_nonce('gdbbx-wizard-nonce-'.$this->current_panel());
    }

    public function panel_postback() {
        $post = $_POST['gdbbx']['wizard'];

        $this->setup_panel($post['_page']);

        if (wp_verify_nonce($post['_nonce'], 'gdbbx-wizard-nonce-'.$this->current_panel())) {
            $data = isset($post[$this->current_panel()]) ? (array)$post[$this->current_panel()] : array();

            switch ($this->current_panel()) {
                case 'intro':
                    $this->_postback_intro($data);
                    break;
                case 'modules':
                    $this->_postback_modules($data);
                    break;
                case 'attachments':
                    $this->_postback_attachments($data);
                    break;
                case 'editors':
                    $this->_postback_editors($data);
                    break;
                case 'tracking':
                    $this->_postback_tracking($data);
                    break;
            }

            wp_redirect('admin.php?page=gd-bbpress-toolbox-wizard&panel='.$this->next_panel());
            exit;
        } else {
            wp_redirect('admin.php?page=gd-bbpress-toolbox-wizard&panel='.$this->current_panel());
            exit;
        }
    }

    private function _postback_intro($data) {
        if (isset($data['toolbar']) && $data['toolbar'] == 'yes') {
            foreach ($this->_default['toolbar'] as $key => $value) {
                gdbbx_settings()->set($key, $value, 'tools');
            }
        } else {
            gdbbx_settings()->set('toolbar_active', false, 'tools');
        }

        if (isset($data['signatures']) && $data['signatures'] == 'yes') {
            foreach ($this->_default['signatures'] as $key => $value) {
                gdbbx_settings()->set($key, $value, 'tools');
            }
        } else {
            gdbbx_settings()->set('signature_active', false, 'tools');
        }

        if (isset($data['bbcodes']) && $data['bbcodes'] == 'yes') {
            gdbbx_settings()->set('bbcodes_active', true, 'tools');
        } else {
            gdbbx_settings()->set('bbcodes_active', false, 'tools');
        }

        if (isset($data['quotes']) && $data['quotes'] == 'yes') {
            foreach ($this->_default['quotes'] as $key => $value) {
                gdbbx_settings()->set($key, $value, 'tools');
            }

            gdbbx_settings()->set('kses_allowed_override', 'expanded', 'bbpress');

            if (isset($data['bbcodes']) && $data['bbcodes'] == 'yes') {
                gdbbx_settings()->set('quote_method', 'bbcode', 'tools');
            } else {
                gdbbx_settings()->set('quote_method', 'html', 'tools');
            }
        } else {
            gdbbx_settings()->set('quote_active', false, 'tools');
        }

        gdbbx_settings()->save('tools');
        gdbbx_settings()->save('bbpress');
    }

    private function _postback_modules($data) {
        if (isset($data['canned']) && $data['canned'] == 'yes') {
            gdbbx_settings()->set('active', true, 'canned');
        } else {
            gdbbx_settings()->set('active', false, 'canned');
        }

        if (isset($data['thanks']) && $data['thanks'] == 'yes') {
            foreach ($this->_default['thanks'] as $key => $value) {
                gdbbx_settings()->set($key, $value, 'thanks');
            }
        } else {
            gdbbx_settings()->set('active', false, 'thanks');
        }

        if (isset($data['report']) && $data['report'] == 'yes') {
            foreach ($this->_default['report'] as $key => $value) {
                gdbbx_settings()->set($key, $value, 'report');
            }
        } else {
            gdbbx_settings()->set('active', false, 'report');
        }

        if (isset($data['private']) && $data['private'] == 'yes') {
            foreach ($this->_default['private'] as $key => $value) {
                gdbbx_settings()->set($key, $value, 'bbpress');
            }
        } else {
            gdbbx_settings()->set('private_topics', false, 'bbpress');
            gdbbx_settings()->set('private_replies', false, 'bbpress');
        }

        if (isset($data['stats']) && $data['stats'] == 'yes') {
            foreach ($this->_default['stats'] as $key => $value) {
                gdbbx_settings()->set($key, $value, 'tools');
            }

            if (isset($data['thanks']) && $data['thanks'] == 'yes') {
                gdbbx_settings()->set('users_stats_show_thanks_given', true, 'tools');
                gdbbx_settings()->set('users_stats_show_thanks_received', true, 'tools');
            }
        } else {
            gdbbx_settings()->set('users_stats_active', false, 'tools');
        }

        gdbbx_settings()->save('canned');
        gdbbx_settings()->save('thanks');
        gdbbx_settings()->save('report');
        gdbbx_settings()->save('bbpress');
        gdbbx_settings()->save('tools');
    }

    private function _postback_attachments($data) {
        if (isset($data['attach']) && $data['attach'] == 'yes') {
            gdbbx_settings()->set('attachments_active', true, 'attachments');

            foreach ($this->_default['attachments'] as $key => $value) {
                gdbbx_settings()->set($key, $value, 'attachments');
            }

            if (isset($data['enhance']) && $data['enhance'] == 'yes') {
                gdbbx_settings()->set('validation_active', true, 'attachments');
            } else {
                gdbbx_settings()->set('validation_active', false, 'attachments');
            }

            if (isset($data['images']) && $data['images'] == 'yes') {
                gdbbx_settings()->set('image_thumbnail_active', true, 'attachments');
            } else {
                gdbbx_settings()->set('image_thumbnail_active', false, 'attachments');
            }

            $value = array();
            $mime = isset($data['mime']) ? sanitize_text_field($data['mime']) : 'all';

            switch ($mime) {
                default:
                case 'all':
                    $value = array();
                    break;
                case 'images':
                    $value = array('jpg|jpeg|jpe', 'png', 'gif');
                    break;
                case 'media':
                    $value = array('jpg|jpeg|jpe', 'png', 'gif', 'mp3|m4a|m4b', 'mov|qt', 'avi', 'wmv', 'mid|midi');
                    break;
            }

            gdbbx_settings()->set('mime_types_list', $value, 'attachments');
        } else {
            gdbbx_settings()->set('attachments_active', false, 'attachments');
        }

        gdbbx_settings()->save('attachments');
    }

    private function _postback_editors($data) {
        $update_tinymce_settings = false;

        update_option('_bbp_use_wp_editor', false);

        gdbbx_settings()->set('bbcodes_toolbar_active', false, 'tools');
        gdbbx_settings()->set('editor_topic_active', false, 'tools');
        gdbbx_settings()->set('editor_reply_active', false, 'tools');

        if (isset($data['replace']) && $data['replace'] == 'yes') {
            switch ($data['editor']) {
                case 'quicktags':
                    update_option('_bbp_use_wp_editor', true);
                    break;
                case 'bbcodes':
                    gdbbx_settings()->set('bbcodes_toolbar_active', true, 'tools');
                    break;
                case 'teeny':
                case 'tinymce':
                    $update_tinymce_settings = true;
                    update_option('_bbp_use_wp_editor', true);

                    foreach ($this->_default['tinymce'] as $key => $value) {
                        gdbbx_settings()->set($key, $value, 'tools');
                    }
                    break;
            }

            if ($data['editor'] == 'teeny') {
                gdbbx_settings()->set('editor_topic_teeny', true, 'tools');
                gdbbx_settings()->set('editor_reply_teeny', true, 'tools');
            }
        }

        if (isset($data['library']) && $data['library'] == 'yes') {
            gdbbx_settings()->set('participant_media_library_upload', true, 'bbpress');

            if ($update_tinymce_settings) {
                gdbbx_settings()->set('editor_topic_media_buttons', true, 'tools');
                gdbbx_settings()->set('editor_reply_media_buttons', true, 'tools');
            }
        } else {
            gdbbx_settings()->set('participant_media_library_upload', false, 'bbpress');
        }

        gdbbx_settings()->save('bbpress');
        gdbbx_settings()->save('tools');
    }

    private function _postback_tracking($data) {
        if (isset($data['online']) && $data['online'] == 'yes') {
            gdbbx_settings()->set('active', true, 'online');
            gdbbx_settings()->set('track_users', true, 'online');
            gdbbx_settings()->set('track_guests', true, 'online');
        } else {
            gdbbx_settings()->set('active', false, 'online');
        }

        if (isset($data['activity']) && $data['activity'] == 'yes') {
            foreach ($this->_default['activity'] as $key => $value) {
                gdbbx_settings()->set($key, $value, 'tools');
            }
        } else {
            foreach ($this->_default['activity_off'] as $key => $value) {
                gdbbx_settings()->set($key, $value, 'tools');
            }
        }

        gdbbx_settings()->save('online');
        gdbbx_settings()->save('tools');
    }
}

global $_gdbbx_the_wizard;
$_gdbbx_the_wizard = new gdbbx_core_wizard();

/** @return gdbbx_core_wizard */
function gdbbx_wizard() {
    global $_gdbbx_the_wizard;
    return $_gdbbx_the_wizard;
}
