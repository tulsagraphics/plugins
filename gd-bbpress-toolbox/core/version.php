<?php

if (!defined('ABSPATH')) exit;

class gdbbx_core_info {
    public $code = 'gd-bbpress-toolbox';

    public $version = '5.3';
    public $build = 718;
    public $updated = '2018.07.18';
    public $status = 'stable';
    public $edition = 'pro';
    public $url = 'https://plugins.dev4press.com/gd-bbpress-toolbox/';
    public $author_name = 'Milan Petrovic';
    public $author_url = 'https://www.dev4press.com/';
    public $released = '2012.05.27';

    public $install = false;
    public $update = false;
    public $previous = 0;

    function __construct() { }

    public function to_array() {
        return (array)$this;
    }
}
