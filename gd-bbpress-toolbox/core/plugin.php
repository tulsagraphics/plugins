<?php

if (!defined('ABSPATH')) exit;

class gdbbx_error {
    public $errors = array();

    function __construct() { }

    function add($code, $message, $data) {
        $this->errors[$code][] = array($message, $data);
    }
}

class gdbbx_core_plugin {
    function __construct() {
        add_action('plugins_loaded', array($this, 'core'));
        add_action('after_setup_theme', array($this, 'init'));

        add_action('gdbbx_cron_daily_maintenance_job', array($this, 'daily_maintenance_job'));
        add_action('gdbbx_cron_auto_close_topics', array($this, 'auto_close_topics'));

        add_action('gdbbx_plugin_settings_loaded', array($this, 'settings_loaded'));
    }

    public function settings_loaded() {
        if (gdbbx()->get('fix_404_headers_error', 'bbpress')) {
            add_action('parse_query', array($this, 'fix_404_issues'), 100000);
            add_action('wp', array($this, 'fix_404_issues_status'), 100000);
        }
    }

    public function user_meta_key_last_activity() {
        return gdbbx_db()->prefix().'bbp_last_activity';
    }

    public function get_user_last_activity($user_id) {
        $timestamp = get_user_meta($user_id, $this->user_meta_key_last_activity(), true);

        if ($timestamp == '') {
            $timestamp = get_user_meta($user_id, 'bbp_last_activity', true);
        }

        return intval($timestamp);
    }

    public function update_user_last_activity($user_id, $timestamp = 0) {
        update_user_meta($user_id, $this->user_meta_key_last_activity(), $timestamp);
    }

    public function fix_404_issues() {
        global $wp_query;

        if ($wp_query->bbp_is_single_user || $wp_query->bbp_is_single_user_profile || $wp_query->bbp_is_view) {
            $wp_query->is_404 = false;
        }
    }

    public function fix_404_issues_status() {
        global $wp_query;

        if ($wp_query->bbp_is_single_user || $wp_query->bbp_is_single_user_profile || $wp_query->bbp_is_view) {
            $wp_query->is_404 = false;

            status_header(200);
            nocache_headers();
        }
    }

    public function core() {
        global $wp_version;

        $version = substr(str_replace('.', '', $wp_version), 0, 2);
        define('GDBBX_WPV', intval($version));

        if (gdbbx_has_bbpress()) {
            $this->init_lang();
            $this->init_cron();

            do_action('gdbbx_plugin_core_ready');
        }
    }

    public function init() {
        require_once(GDBBX_PATH.'core/functions/theme.php');
    }

    public function init_cron() {
        if (!wp_next_scheduled('gdbbx_cron_daily_maintenance_job')) {
            $cron_hour = apply_filters('gdbbx_cron_daily_maintenance_job_hour', 3);
            $cron_time = mktime($cron_hour, 0, 0, date('m'), date('d') + 1, date('Y'));

            wp_schedule_event($cron_time, 'daily', 'gdbbx_cron_daily_maintenance_job');
        }
    }

    public function init_lang() {
        load_plugin_textdomain('gd-bbpress-toolbox', false, 'gd-bbpress-toolbox/languages');
    }

    public function daily_maintenance_job() {
        $this->auto_close_topics(true);
    }

    public function auto_close_topics($first = false) {
        if (gdbbx_module_topics_closing()->auto_close_is_active()) {
            if ($first) {
                gdbbx_module_topics_closing()->clear_auto_close_skip();
            }

            gdbbx_module_topics_closing()->run_auto_close();
        }
    }

    public function recommend($panel = 'update') {
        d4p_includes(array(
            array('name' => 'ip', 'directory' => 'classes'), 
            array('name' => 'four', 'directory' => 'classes')
        ), GDBBX_D4PLIB);

        $four = new d4p_core_four('plugin', 'gd-bbpress-toolbox', gdbbx()->info_version, gdbbx()->info_build);
        $four->ad();

        return $four->ad_render($panel);
    }
}
