<?php

if (!defined('ABSPATH')) exit;

/** Will be removed in 5.2 */
function gdbbx_get_attachment($file, $post = 0) {
    _deprecated_function(__FUNCTION__, '4.8.4', 'gdbbx_get_attachment_id_from_name()');

    return gdbbx_get_attachment_id_from_name($file);
}
