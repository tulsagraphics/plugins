<?php

if (!defined('ABSPATH')) exit;

class gdbbx_admin_core {
    public $plugin = 'gd-bbpress-toolbox';

    public $debug;

    public $page = false;
    public $panel = false;
    public $free = array();

    public $menu_items;

    function __construct() {
        add_action('gdbbx_plugin_core_ready', array($this, 'core'));

        if (is_multisite()) {
            add_filter('wpmu_drop_tables', array($this, 'wpmu_drop_tables'));
        }
    }

    public function wpmu_drop_tables($drop_tables) {
        return array_merge($drop_tables, gdbbx_db()->db_site);
    }

    private function file($type, $name, $d4p = false) {
        $get = GDBBX_URL;

        if ($d4p) {
            $get.= 'd4plib/resources/';
        }

        if ($name == 'font') {
            $get.= 'font/styles.css';
        } else {
            $get.= $type.'/'.$name;

            if (!$this->debug && $type != 'font') {
                $get.= '.min';
            }

            $get.= '.'.$type;
        }

        return $get;
    }

    public function core() {
        $this->debug = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG;

        if (gdbbx_has_bbpress()) {
            $this->init();

            add_action('admin_init', array($this, 'admin_init'));
            add_action('admin_menu', array($this, 'admin_menu'), 9);
            add_action('admin_enqueue_scripts', array($this, 'enqueue_scripts'));
            add_filter('set-screen-option', array($this, 'screen_options_grid_rows_save'), 10, 3);
            add_action('current_screen', array($this, 'current_screen'));
        }

        if (gdbbx()->is_install()) {
            add_action('admin_notices', array($this, 'install_notice'));
        }

        if (gdbbx()->is_update()) {
            add_action('admin_notices', array($this, 'update_notice'));
        }

        $this->free = gdbbx()->has_free_plugins();

        if (!empty($this->free)) {
            add_action('admin_notices', array($this, 'free_plugins_notice'));
        }
    }

    public function current_url($with_panel = true) {
        $page = 'admin.php?page='.$this->plugin.'-';

        $page.= $this->page;

        if ($with_panel && $this->panel !== false && $this->panel != '') {
            $page.= '&panel='.$this->panel;
        }

        return self_admin_url($page);
    }

    public function free_plugins_notice() {
        if (!empty($this->free)) {
            echo '<div class="error"><p>';
            echo sprintf(__("GD bbPress Toolbox Pro detected that following plugins are still active: %s. They need to be disabled before you can use GD bbPress Toolbox.", "gd-bbpress-toolbox"), 
                '<strong>'.join('</strong>, <strong>', $this->free).'</strong>');
            echo '<br>'.__("You can", "gd-bbpress-toolbox").' <a href="plugins.php">'.__("open plugins page", "gd-bbpress-toolbox").'</a> '.__("to disable them manually", "gd-bbpress-toolbox").' '.__("or", "gd-bbpress-toolbox").' <a href="admin.php?page=gd-bbpress-toolbox-front&gdbbx_handler=getback&action=gdbbx-disable-free">'.__("click here", "gd-bbpress-toolbox").'</a> '.__("to disabled them automatically", "gd-bbpress-toolbox").'.';
            echo '</p></div>';
        }
    }

    public function update_notice() {
        if (current_user_can('install_plugins') && $this->page === false) {
            echo '<div class="updated"><p>';
            echo __("GD bbPress Toolbox Pro is updated, and you need to review the update process.", "gd-bbpress-toolbox");
            echo ' <a href="admin.php?page=gd-bbpress-toolbox-front">'.__("Click Here", "gd-bbpress-toolbox").'</a>.';
            echo '</p></div>';
        }
    }

    public function install_notice() {
        if (current_user_can('install_plugins') && $this->page === false) {
            echo '<div class="updated"><p>';
            echo __("GD bbPress Toolbox Pro is activated and it needs to finish installation.", "gd-bbpress-toolbox");
            echo ' <a href="admin.php?page=gd-bbpress-toolbox-front">'.__("Click Here", "gd-bbpress-toolbox").'</a>.';
            echo '</p></div>';
        }
    }

    public function screen_options_grid_rows_save($status, $option, $value) {
        if (in_array($option, array('gdbbx_rows_per_page_users', 'gdbbx_rows_per_page_thanks', 'gdbbx_rows_per_page_attachments', 'gdbbx_rows_per_page_errors', 'gdbbx_rows_per_page_reports'))) {
            return absint($value);
        }

        return $status;
    }

    public function screen_options_grid_rows_thanks() {
        $args = array(
            'label' => __("Rows", "gd-bbpress-toolbox"),
            'default' => 25,
            'option' => 'gdbbx_rows_per_page_thanks'
        );

        add_screen_option('per_page', $args);

        require_once(GDBBX_PATH.'core/grids/thanks.php');

        new gdbbx_grid_thanks();
    }

    public function screen_options_grid_rows_reports() {
        $args = array(
            'label' => __("Rows", "gd-bbpress-toolbox"),
            'default' => 25,
            'option' => 'gdbbx_rows_per_page_reports'
        );

        add_screen_option('per_page', $args);

        require_once(GDBBX_PATH.'core/grids/reports.php');

        new gdbbx_grid_reports();
    }

    public function screen_options_grid_rows_users() {
        $args = array(
            'label' => __("Rows", "gd-bbpress-toolbox"),
            'default' => 25,
            'option' => 'gdbbx_rows_per_page_users'
        );

        add_screen_option('per_page', $args);

        require_once(GDBBX_PATH.'core/grids/users.php');

        new gdbbx_grid_users();
    }

    public function screen_options_grid_rows_attchments() {
        $args = array(
            'label' => __("Rows", "gd-bbpress-toolbox"),
            'default' => 25,
            'option' => 'gdbbx_rows_per_page_attachments'
        );

        add_screen_option('per_page', $args);

        require_once(GDBBX_PATH.'core/grids/attachments.php');

        new gdbbx_grid_attachments();
    }

    public function screen_options_grid_rows_errors() {
        $args = array(
            'label' => __("Rows", "gd-bbpress-toolbox"),
            'default' => 25,
            'option' => 'gdbbx_rows_per_page_errors'
        );

        add_screen_option('per_page', $args);

        require_once(GDBBX_PATH.'core/grids/errors.php');

        new gdbbx_grid_errors();
    }

    public function init() {
        $this->menu_items = apply_filters('gdbbx_admin_menu_items', array(
            'front' => array('title' => __("Dashboard", "gd-bbpress-toolbox"), 'icon' => 'home', 'cap' => 'gdbbx_moderation'),
            'about' => array('title' => __("About", "gd-bbpress-toolbox"), 'icon' => 'info-circle'),
            'modules' => array('title' => __("Modules", "gd-bbpress-toolbox"), 'icon' => 'th-large'),
            'views' => array('title' => __("Topic Views", "gd-bbpress-toolbox"), 'icon' => 'files-o'),
            'bbcodes' => array('title' => __("BBCodes", "gd-bbpress-toolbox"), 'icon' => 'pencil-square'),
            'attachments' => array('title' => __("Attachments", "gd-bbpress-toolbox"), 'icon' => 'paperclip'),
            'settings' => array('title' => __("Settings", "gd-bbpress-toolbox"), 'icon' => 'cogs'),
            'users' => array('title' => __("Users", "gd-bbpress-toolbox"), 'icon' => 'users', 'cap' => 'gdbbx_moderation_users'),
            'reported-posts' => array('title' => __("Reported Posts", "gd-bbpress-toolbox"), 'icon' => 'exclamation-triangle', 'cap' => 'gdbbx_moderation_report'),
            'thanks-list' => array('title' => __("Thanks List", "gd-bbpress-toolbox"), 'icon' => 'check-square', 'cap' => 'gdbbx_moderation_attachments'),
            'attachments-list' => array('title' => __("Attachments List", "gd-bbpress-toolbox"), 'icon' => 'file-text-o', 'cap' => 'gdbbx_moderation_attachments'),
            'errors' => array('title' => __("Errors Log", "gd-bbpress-toolbox"), 'icon' => 'bug', 'cap' => 'gdbbx_moderation_attachments'),
            'wizard' => array('title' => __("Setup Wizard", "gd-bbpress-toolbox"), 'icon' => 'magic'),
            'tools' => array('title' => __("Tools", "gd-bbpress-toolbox"), 'icon' => 'wrench')
        ));
    }

    public function admin_init() {
        global $submenu;

        d4p_include('grid', 'admin', GDBBX_D4PLIB);

        if (gdbbx()->get('active', 'canned')) {
            if (isset($submenu['gd-bbpress-toolbox-front'])) {
                $canned = $submenu['gd-bbpress-toolbox-front'][14];
                $canned[0] = gdbbx()->get('post_type_plural', 'canned');
                unset($submenu['gd-bbpress-toolbox-front'][14]);

                array_splice($submenu['gd-bbpress-toolbox-front'], 7, 0, array($canned));
            }
        }
    }

    public function admin_menu() {
        $parent = 'gd-bbpress-toolbox-front';

        $caps = gdbbx()->get('roles_gdbbx_moderation', 'bbpress');

        $cap = $caps ? 'gdbbx_moderation' : GDBBX_CAP;

        $this->page_ids[] = add_menu_page(
                        'GD bbPress Toolbox Pro', 
                        'bbPress Toolbox', 
                        $cap, 
                        $parent, 
                        array($this, 'panel_general'), 
                        gdbbx_loader()->svg_icon);

        foreach($this->menu_items as $item => $data) {
            $cap = GDBBX_CAP;

            if ($caps && isset($data['cap'])) {
                $cap = $data['cap'];
            }

            $this->page_ids[] = add_submenu_page($parent, 
                            'GD bbPress Toolbox Pro: '.$data['title'], 
                            $data['title'], 
                            $cap, 
                            'gd-bbpress-toolbox-'.$item, 
                            array($this, 'panel_general'));
        }

        $this->admin_load_hooks();
    }

    public function enqueue_scripts($hook) {
        $load_admin_data = false;

        if ($this->page !== false) {
            d4p_admin_enqueue_defaults();

            wp_enqueue_style('fontawesome', gdbbx_loader()->fontawesome_url(), array(), gdbbx_loader()->fontawesome_version);

            wp_enqueue_style('d4plib-font-22', $this->file('css', 'font', true), array(), D4P_VERSION.'.'.D4P_BUILD);
            wp_enqueue_style('d4plib-shared', $this->file('css', 'shared', true), array(), D4P_VERSION.'.'.D4P_BUILD);
            wp_enqueue_style('d4plib-admin', $this->file('css', 'admin', true), array('d4plib-shared'), D4P_VERSION.'.'.D4P_BUILD);

            wp_enqueue_script('d4pjs-areyousure', GDBBX_URL.'d4pjs/are-you-sure/jquery.are-you-sure.min.js', array('jquery'), D4P_VERSION.'.'.D4P_BUILD, true);
            wp_enqueue_script('d4plib-shared', $this->file('js', 'shared', true), array('jquery', 'wp-color-picker'), D4P_VERSION.'.'.D4P_BUILD, true);
            wp_enqueue_script('d4plib-admin', $this->file('js', 'admin', true), array('d4plib-shared'), D4P_VERSION.'.'.D4P_BUILD, true);

            wp_enqueue_style('gdbbx-plugin', $this->file('css', 'admin-core'), array('d4plib-admin'), gdbbx()->file_version());
            wp_enqueue_script('gdbbx-plugin', $this->file('js', 'admin-core'), array('d4plib-admin', 'd4pjs-areyousure'), gdbbx()->file_version(), true);

            if ($this->page == 'wizard') {
                wp_enqueue_style('d4plib-wizard', $this->file('css', 'wizard', true), array(), D4P_VERSION.'.'.D4P_BUILD);
                wp_enqueue_script('d4plib-wizard', $this->file('js', 'wizard', true), array(), D4P_VERSION.'.'.D4P_BUILD, true);
            }

            if ($this->page == 'about') {
                wp_enqueue_style('d4plib-grid', $this->file('css', 'grid', true), array(), D4P_VERSION.'.'.D4P_BUILD);
            }

            $load_admin_data = true;
        }

        if ($hook == 'post.php' || $hook == 'post-new.php') {
            $post_type = $this->get_post_type();

            if ($post_type !== false) {
                wp_enqueue_style('d4plib-shared', $this->file('css', 'shared', true), array(), D4P_VERSION.'.'.D4P_BUILD);
                wp_enqueue_style('d4plib-metabox', $this->file('css', 'meta', true), array('d4plib-shared'), D4P_VERSION.'.'.D4P_BUILD);

                wp_enqueue_script('d4plib-shared', $this->file('js', 'shared', true), array('jquery', 'wp-color-picker'), D4P_VERSION.'.'.D4P_BUILD, true);
                wp_enqueue_script('d4plib-metabox', $this->file('js', 'meta', true), array('d4plib-shared'), D4P_VERSION.'.'.D4P_BUILD, true);

                $load_admin_data = true;
            }
        }

        if ($hook == 'index.php') {
            wp_enqueue_style('gdbbx-dashboard', $this->file('css', 'admin-dashboard'), array(), D4P_VERSION.'.'.D4P_BUILD);
        }

        if ($hook == 'widgets.php') {
            wp_enqueue_script('jquery-ui-sortable');

            wp_enqueue_style('d4plib-widgets', $this->file('css', 'widgets', true), array(), D4P_VERSION.'.'.D4P_BUILD);
            wp_enqueue_script('d4plib-widgets', $this->file('js', 'widgets', true), array('jquery'), D4P_VERSION.'.'.D4P_BUILD, true);

            wp_enqueue_script('gdbbx-widgets', $this->file('js', 'admin-widgets'), array('d4plib-widgets', 'jquery-ui-sortable'), gdbbx()->file_version(), true);
        }

        if ($load_admin_data) {
            wp_localize_script('d4plib-shared', 'd4plib_admin_data', array(
                'string_media_image_remove' => __("Remove", "gd-bbpress-toolbox"),
                'string_media_image_preview' => __("Preview", "gd-bbpress-toolbox"),
                'string_media_image_title' => __("Select Image", "gd-bbpress-toolbox"),
                'string_media_image_button' => __("Use Selected Image", "gd-bbpress-toolbox"),
                'string_are_you_sure' => __("Are you sure you want to do this?", "gd-bbpress-toolbox"),
                'string_image_not_selected' => __("Image not selected.", "gd-bbpress-toolbox")
            ));
        }
    }

    public function get_post_type() {
        if (isset($_GET['post_type'])) {
            $post_type = $_GET['post_type'];
        } else {
            global $post;

            if ($post) {
                $post_type = $post->post_type;
            }
        }

        if (in_array($post_type, array(
            bbp_get_forum_post_type(),
            bbp_get_topic_post_type(),
            bbp_get_reply_post_type()
        ))) {
            return $post_type;
        } else {
            return false;
        }
    }

    public function admin_load_hooks() {
        foreach ($this->page_ids as $id) {
            add_action('load-'.$id, array($this, 'load_admin_page'));
        }

        add_action('load-bbpress-toolbox_page_gd-bbpress-toolbox-users', array($this, 'screen_options_grid_rows_users'));
        add_action('load-bbpress-toolbox_page_gd-bbpress-toolbox-reported-posts', array($this, 'screen_options_grid_rows_reports'));
        add_action('load-bbpress-toolbox_page_gd-bbpress-toolbox-thanks-list', array($this, 'screen_options_grid_rows_thanks'));
        add_action('load-bbpress-toolbox_page_gd-bbpress-toolbox-attachments-list', array($this, 'screen_options_grid_rows_attchments'));
        add_action('load-bbpress-toolbox_page_gd-bbpress-toolbox-errors', array($this, 'screen_options_grid_rows_errors'));
    }

    public function help_tab_sidebar() {
        $screen = get_current_screen();

        $screen->set_help_sidebar(
            '<p><strong>GD bbPress Toolbox Pro</strong></p>'.
            '<p><a target="_blank" href="https://plugins.dev4press.com/'.$this->plugin.'/">'.__("Home Page", "gd-bbpress-toolbox").'</a><br/>'.
            '<a target="_blank" href="https://support.dev4press.com/kb/product/'.$this->plugin.'/">'.__("Knowledge Base", "gd-bbpress-toolbox").'</a><br/>'.
            '<a target="_blank" href="https://support.dev4press.com/forums/forum/plugins/'.$this->plugin.'/">'.__("Support Forum", "gd-bbpress-toolbox").'</a></p>'
        );
    }

    public function help_tab_getting_help() {
        $screen = get_current_screen();

        $screen->add_help_tab(
            array(
                'id' => 'gdbbx-help-info',
                'title' => __("Help & Support", "gd-bbpress-toolbox"),
                'content' => '<h2>'.__("Help & Support", "gd-bbpress-toolbox").'</h2><p>'.__("To get help with this plugin, you can start with Knowledge Base list of frequently asked questions, user guides, articles (tutorials) and reference guide (for developers).", "gd-bbpress-toolbox").
                             '</p><p><a href="https://support.dev4press.com/kb/product/'.$this->plugin.'/" class="button-primary" target="_blank">'.__("Knowledge Base", "gd-bbpress-toolbox").'</a> <a href="https://support.dev4press.com/forums/forum/plugins/'.$this->plugin.'/" class="button-secondary" target="_blank">'.__("Support Forum", "gd-bbpress-toolbox").'</a></p>'
            )
        );

        $screen->add_help_tab(
            array(
                'id' => 'gdbbx-help-bugs',
                'title' => __("Found a bug?", "gd-bbpress-toolbox"),
                'content' => '<h2>'.__("Found a bug?", "gd-bbpress-toolbox").'</h2><p>'.__("If you find a bug in GD bbPress Toolbox Pro, you can report it in the support forum.", "gd-bbpress-toolbox").
                             '</p><p>'.__("Before reporting a bug, make sure you use latest plugin version, your website and server meet system requirements. And, please be as descriptive as possible, include server side logged errors, or errors from browser debugger.", "gd-bbpress-toolbox").
                             '</p><p><a href="https://support.dev4press.com/forums/forum/plugins/'.$this->plugin.'/" class="button-primary" target="_blank">'.__("Open new topic", "gd-bbpress-toolbox").'</a></p>'
            )
        );

        $screen->add_help_tab(
            array(
                'id' => 'gdbbx-help-wizard',
                'title' => __("Setup Wizard", "gd-bbpress-toolbox"),
                'content' => '<h2>'.__("Setup Wizard", "gd-bbpress-toolbox").'</h2><p>'.__("If you need to quickly reconfigure the plugin, you can access the setup wizard again.", "gd-bbpress-toolbox").
                             '</p><p><a href="'. admin_url('admin.php?page=gd-bbpress-toolbox-wizard').'" class="button-primary" target="_blank">'.__("Open setup wizard", "gd-bbpress-toolbox").'</a></p>'
            )
        );
    }

    public function load_admin_page() {
        $this->help_tab_sidebar();

        do_action('gdbbx_load_admin_page_'.$this->page);

        if ($this->panel !== false && $this->panel != '') {
            do_action('gdbbx_load_admin_page_'.$this->page.'_'.$this->panel);
        }

        $this->help_tab_getting_help();
    }

    public function current_screen($screen) {
        if (isset($_GET['panel']) && $_GET['panel'] != '') {
            $this->panel = d4p_sanitize_slug($_GET['panel']);
        }

        $id = $screen->id;

        if ($id == 'toplevel_page_gd-bbpress-toolbox-front') {
            $this->page = 'front';
        } else if (substr($id, 0, 40) == 'bbpress-toolbox_page_gd-bbpress-toolbox-') {
            $this->page = substr($id, 40);
        }

        if (isset($_POST['gdbbx_handler']) && $_POST['gdbbx_handler'] == 'postback') {
            require_once(GDBBX_PATH.'core/admin/postback.php');

            $postback = new gdbbx_admin_postback();
        } else if (isset($_GET['gdbbx_handler']) && $_GET['gdbbx_handler'] == 'getback') {
            require_once(GDBBX_PATH.'core/admin/getback.php');

            $getback = new gdbbx_admin_getback();
        }
    }

    public function install_or_update() {
        $install = gdbbx()->is_install();
        $update = gdbbx()->is_update();

        if ($install) {
            include(GDBBX_PATH.'forms/install.php');
        } else if ($update) {
            include(GDBBX_PATH.'forms/update.php');
        }

        return $install || $update;
    }

    public function panel_general() {
        if (!$this->install_or_update()) {
            $_current_page = $this->page;

            $path = apply_filters('gdbbx_admin_menu_panel_'.$_current_page, GDBBX_PATH.'forms/'.$_current_page.'.php');

            if (!file_exists($path)) {
                $path = GDBBX_PATH.'forms/shared/invalid.php';
            }

            include($path);
        }
    }
}

global $_gdbbx_core_admin;
$_gdbbx_core_admin = new gdbbx_admin_core();

function gdbbx_admin() {
    global $_gdbbx_core_admin;
    return $_gdbbx_core_admin;
}
