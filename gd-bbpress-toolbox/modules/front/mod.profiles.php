<?php

if (!defined('ABSPATH')) exit;

class gdbbx_mod_profiles {
    function __construct() {
        if (!is_user_logged_in() && gdbbx()->get('user_profile_hide_from_visitors', 'bbpress')) {
            add_filter('bbp_get_template_part', array($this, 'replace_profile'), 10, 3);
        }
    }

    public function replace_profile($templates, $slug, $name) {
        if ($slug == 'content' && $name == 'single-user') {
            $templates = array('gdbbx-user-profile-protected.php');
        }

        return $templates;
    }

    public function message_profile_protected() {
        return apply_filters('gdbbx_user_profile_protected_message', sprintf(__("You must be <a href='%s'>logged in</a> in to access user profile pages.", "gd-bbpress-toolbox"), wp_login_url(get_permalink())));
    }
}

/** @return gdbbx_mod_profiles */
function gdbbx_module_profiles() {
    return gdbbx_loader()->modules['front.profiles'];
}
