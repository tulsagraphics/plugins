<?php

if (!defined('ABSPATH')) exit;

class gdbbx_mod_tweaks {
    function __construct() {
        if (gdbbx()->get('enable_lead_topic', 'bbpress')) {
            add_filter('bbp_show_lead_topic', '__return_true', 10000);
        }

        if (gdbbx()->get('disable_bbpress_breadcrumbs', 'bbpress')) {
            add_filter('bbp_no_breadcrumb', '__return_true');
        }

        if (gdbbx()->get('enable_topic_reversed_replies', 'bbpress')) {
            add_filter('bbp_before_has_replies_parse_args', array($this, 'change_replies_order'));
        }

        if (gdbbx()->get('title_length_value', 'bbpress')) {
            add_filter('bbp_get_title_max_length', array($this, 'custom_title_length'));
        }

        if (!gdbbx()->get('nofollow_topic_content', 'bbpress')) {
            remove_filter('bbp_get_topic_content', 'bbp_rel_nofollow', 50);
        }

        if (!gdbbx()->get('nofollow_reply_content', 'bbpress')) {
            remove_filter('bbp_get_reply_content', 'bbp_rel_nofollow', 50);
        }

        if (!gdbbx()->get('nofollow_topic_author', 'bbpress')) {
            remove_filter('bbp_get_topic_author_link', 'bbp_rel_nofollow');
        }

        if (!gdbbx()->get('nofollow_reply_author', 'bbpress')) {
            remove_filter('bbp_get_reply_author_link', 'bbp_rel_nofollow');
        }
    }

    public function change_replies_order($r) {
        $r['order'] = 'DESC';

        return $r;
    }

    public function custom_title_length($value) {
        $custom = (int)gdbbx()->get('title_length_value', 'bbpress');

        if ($custom > 0) {
            $value = $custom;
        }

        return $value;
    }
}
