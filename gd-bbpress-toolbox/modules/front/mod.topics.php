<?php

if (!defined('ABSPATH')) exit;

class gdbbx_mod_topics {
    function __construct() {
        add_action('gdbbx_template', array($this, 'template_loader'));

        if (gdbbx()->get('forum_list_topic_thumbnail', 'bbpress')) {
            add_action('bbp_theme_before_topic_title', array($this, 'show_thumbnail'));
        }

        if (gdbbx()->get('new_topic_minmax_active', 'bbpress')) {
            add_filter('bbp_new_topic_pre_title', array($this, 'new_topic_title'));
            add_filter('bbp_new_topic_pre_content', array($this, 'new_topic_content'));
        }

        if (gdbbx()->get('topic_load_search_for_all_topics', 'bbpress')) {
            add_action('bbp_template_before_single_topic', 'gdbbx_load_seach_form_template');
        }
    }

    public function template_loader() {
        add_filter('bbp_topic_admin_links', array($this, 'topic_admin_links'));
        add_filter('gdbbx_topic_footer_links', array($this, 'topic_footer_links'), 10, 2);

        if (gdbbx_current_user_can_moderate() && gdbbx()->get('topic_single_copy_active', 'bbpress')) {
            add_action('bbp_get_request_dupe_topic', array($this, 'process_duplicate_topic'));

            if (gdbbx()->get('topic_single_copy_location', 'bbpress') == 'header') {
                add_filter('bbp_topic_admin_links', array($this, 'duplicate_topic_link'), 20, 2);
            }

            if (gdbbx()->get('topic_single_copy_location', 'bbpress') == 'footer') {
                add_filter('gdbbx_topic_footer_links', array($this, 'duplicate_topic_link'), 20, 2);
            }
        }
    }

    public function topic_admin_links($links) {
        if (gdbbx()->get('topic_links_remove_merge', 'bbpress')) {
            if (isset($links['merge'])) {
                unset($links['merge']);
            }
        }

        if (gdbbx()->get('topic_links_edit_footer', 'bbpress')) {
            if (isset($links['edit'])) {
                unset($links['edit']);
            }
        }

        if (gdbbx()->get('topic_links_reply_footer', 'bbpress')) {
            if (isset($links['reply'])) {
                unset($links['reply']);
            }
        }

        return $links;
    }

    public function topic_footer_links($links, $id) {
        if (gdbbx()->get('topic_links_edit_footer', 'bbpress')) {
            $edit = bbp_get_topic_edit_link(array('id' => $id));

            if (!empty($edit)) {
                $links['edit'] = $edit;
            }
        }

        if (gdbbx()->get('topic_links_reply_footer', 'bbpress')) {
            $reply = bbp_get_topic_reply_link(array('id' => $id));

            if (!empty($reply)) {
                $links['reply'] = $reply;
            }
        }

        return $links;
    }

    public function process_duplicate_topic() {
        $post_id = intval($_GET['id']);

        if (!gdbbx_current_user_can_moderate()) {
            bbp_add_error('bgdbx_lock_not_moderator', __("<strong>ERROR</strong>: You can't duplicate topics.", "gd-bbpress-toolbox"));
        }

        if (!bbp_verify_nonce_request('gdbbx_dupe_topic_'.$post_id)) {
            bbp_add_error('bgdbx_lock_nonce', __("<strong>ERROR</strong>: Are you sure you wanted to do that?", "gd-bbpress-toolbox"));
        }

        if (bbp_has_errors()) {
            return;
        }

        require_once(GDBBX_PATH.'core/objects/core.duplicate.php');

        $dupe = new gdbbx_core_duplicate();
        $id = $dupe->duplicate_topic($post_id);

        wp_redirect(get_permalink($id));
        exit;
    }

    public function duplicate_topic_link($links, $id) {
        $url = bbp_get_topic_permalink($id);
        $url = add_query_arg('id', $id, $url);
        $url = add_query_arg('_wpnonce', wp_create_nonce('gdbbx_dupe_topic_'.$id), $url);
        $url = add_query_arg('action', 'dupe_topic', $url);

        $links['gdbbx_dupe_topic'] = '<a href="'.$url.'" class="d4p-bbt-dupe-topic-link">'.__("Duplicate Topic", "gd-bbpress-toolbox").'</a>';

        return $links;
    }

    public function show_thumbnail() {
        $img = gdbbx_get_topic_thumbnail();

        if ($img != '') {
            echo '<div class="gdbbx-topic-thumbnail"><a href="'.bbp_get_topic_permalink().'"><img src="'.$img.'" alt="'.bbp_get_topic_title().'" /></a></div>';
        }
    }

    public function new_topic_title($title) {
        $length = strlen($title);

        $check = gdbbx()->prefix_get('new_topic_', 'bbpress');

        if ($check['min_title_length'] > 0) {
            if ($length < $check['min_title_length']) {
                bbp_add_error('bbp_topic_title', __("<strong>ERROR</strong>: Your topic title is too short.", "gd-bbpress-toolbox"));
            }
        }

        if ($check['max_title_length'] > 0) {
            if ($length > $check['max_title_length']) {
                bbp_add_error('bbp_topic_title', __("<strong>ERROR</strong>: Your topic title is too long.", "gd-bbpress-toolbox"));
            }
        }

        return $title;
    }

    public function new_topic_content($content) {
        $length = strlen($content);

        $check = gdbbx()->prefix_get('new_topic_', 'bbpress');

        if ($check['min_content_length'] > 0) {
            if ($length < $check['min_content_length']) {
                bbp_add_error('bbp_topic_content', __("<strong>ERROR</strong>: Your topic is too short.", "gd-bbpress-toolbox"));
            }
        }

        if ($check['max_content_length'] > 0) {
            if ($length > $check['max_content_length']) {
                bbp_add_error('bbp_topic_content', __("<strong>ERROR</strong>: Your topic is too long.", "gd-bbpress-toolbox"));
            }
        }

        return $content;
    }
}
