<?php

if (!defined('ABSPATH')) exit;

class gdbbx_mod_user_stats {
    function __construct() {
        add_action('bbp_theme_after_topic_author_details', array($this, 'user_stats'));
        add_action('bbp_theme_after_reply_author_details', array($this, 'user_stats'));

        add_action('gdbbx_template_before_replies_loop', array($this, 'before_replies_loop'), 10, 2);
    }

    public function before_replies_loop($posts, $users) {
        gdbbx_cache()->userstats_run_bulk_counts($users);
    }

    public function user_stats() {
        if (bbp_is_reply_anonymous()) {
            return;
        }

        gdbbx_enqueue_files_force();

        $author = bbp_get_reply_author_id();

        $list = array();

        if (gdbbx()->get('users_stats_show_registration_date', 'tools')) {
            $user = get_user_by('id', $author);
            $date = $user->user_registered;
            $format = apply_filters('gdbbx_user_stats_registered_date_format', get_option('date_format'));

            $item = apply_filters('gdbbx_user_stats_registered_on', 
                    '<div class="gdbbx-user-stats-block gdbbx-user-stats-registered">
                     <span class="gdbbx-label">'.__("Registered On", "gd-bbpress-toolbox").':</span> <span class="gdbbx-value">'.mysql2date($format, $date).'</span>
                     </div>', $date, $format);

            $list['registered'] = $item;
        }

        if (gdbbx()->get('users_stats_show_topics', 'tools')) {
            $topics = gdbbx_cache()->userstats_count_posts($author, bbp_get_topic_post_type());

            $item = apply_filters('gdbbx_user_stats_topics_count', 
                    '<div class="gdbbx-user-stats-block gdbbx-user-stats-topics">
                     <span class="gdbbx-label">'.__("Topics", "gd-bbpress-toolbox").':</span> <span class="gdbbx-value">'.$topics.'</span>
                     </div>', $topics);

            $list['topics'] = $item;
        }

        if (gdbbx()->get('users_stats_show_replies', 'tools')) {
            $replies = gdbbx_cache()->userstats_count_posts($author, bbp_get_reply_post_type());

            $item = apply_filters('gdbbx_user_stats_replies_count', 
                    '<div class="gdbbx-user-stats-block gdbbx-user-stats-replies">
                     <span class="gdbbx-label">'.__("Replies", "gd-bbpress-toolbox").':</span> <span class="gdbbx-value">'.$replies.'</span>
                     </div>', $replies);

            $list['replies'] = $item;
        }

        if (gdbbx_is_module_loaded('thanks')) {
            if (gdbbx()->get('users_stats_show_thanks_given', 'tools')) {
                $thanks_given = gdbbx_cache()->thanks_get_count_given($author);

                $item = apply_filters('gdbbx_user_stats_thanks_given_count', 
                        '<div class="gdbbx-user-stats-block gdbbx-user-stats-thanks-given">
                         <span class="gdbbx-label">'.__("Has thanked", "gd-bbpress-toolbox").':</span> <span class="gdbbx-value">'.$thanks_given.' '._n("time", "times", $thanks_given, "gd-bbpress-toolbox").'</span>
                         </div>', $thanks_given);

                $list['thanks_given'] = $item;
            }

            if (gdbbx()->get('users_stats_show_thanks_received', 'tools')) {
                $thanks_received = gdbbx_cache()->thanks_get_count_received($author);

                $item = apply_filters('gdbbx_user_stats_thanks_received_count', 
                        '<div class="gdbbx-user-stats-block gdbbx-user-stats-thanks-received">
                         <span class="gdbbx-label">'.__("Been thanked", "gd-bbpress-toolbox").':</span> <span class="gdbbx-value">'.$thanks_received.' '._n("time", "times", $thanks_received, "gd-bbpress-toolbox").'</span>
                         </div>', $thanks_received);

                $list['thanks_received'] = $item;
            }
        }

        $list = apply_filters('gdbbx_user_stats_items', $list, $author);

        echo '<div class="gdbbx-user-stats">'.join('', $list).'</div>';
    }
}
