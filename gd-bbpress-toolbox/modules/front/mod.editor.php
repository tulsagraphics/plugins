<?php

if (!defined('ABSPATH')) exit;

class gdbbx_mod_editor {
    public $context = array(
        'topic' => array(), 
        'reply' => array()
    );

    function __construct() {
        $this->context['topic'] = gdbbx()->prefix_get('editor_topic_', 'tools');
        $this->context['reply'] = gdbbx()->prefix_get('editor_reply_', 'tools');

        if ($this->context['topic']['active'] || $this->context['reply']['active']) {
            add_filter('bbp_after_get_the_content_parse_args', array($this, 'control'));
        }
    }

    public function control($args) {
        $context = $args['context'];

        if (isset($this->context[$context]) && $this->context[$context]['active']) {
            foreach ($this->context[$context] as $key => $val) {
                if ($key != 'active') {
                    $args[$key] = $val;
                }
            }
        }

        return $args;
    }
}
