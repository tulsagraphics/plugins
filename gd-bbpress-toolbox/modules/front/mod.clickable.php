<?php

if (!defined('ABSPATH')) exit;

class gdbbx_mod_clickable {
    function __construct() {
        if (gdbbx()->get('disable_make_clickable_topic', 'bbpress')) {
            remove_filter('bbp_get_topic_content', 'bbp_make_clickable', 4);
        }

        if (gdbbx()->get('disable_make_clickable_reply', 'bbpress')) {
            remove_filter('bbp_get_reply_content', 'bbp_make_clickable', 4);
        }

        if (gdbbx()->get('remove_clickable_urls', 'bbpress')) {
            remove_filter('bbp_make_clickable', 'bbp_make_urls_clickable', 2);
        }

        if (gdbbx()->get('remove_clickable_ftps', 'bbpress')) {
            remove_filter('bbp_make_clickable', 'bbp_make_ftps_clickable', 4);
        }

        if (gdbbx()->get('remove_clickable_emails', 'bbpress')) {
            remove_filter('bbp_make_clickable', 'bbp_make_emails_clickable', 6);
        }

        if (gdbbx()->get('remove_clickable_mentions', 'bbpress')) {
            remove_filter('bbp_make_clickable', 'bbp_make_mentions_clickable', 8);
        }
    }
}
