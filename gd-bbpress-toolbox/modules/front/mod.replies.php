<?php

if (!defined('ABSPATH')) exit;

class gdbbx_mod_replies {
    function __construct() {
        add_action('gdbbx_template', array($this, 'template_loader'));

        if (gdbbx()->get('reply_titles', 'bbpress')) {
            add_action('bbp_theme_before_reply_form_content', array($this, 'reply_titles_form_field'));
            add_action('bbp_theme_before_reply_content', array($this, 'reply_titles_print_title'));
        }

        if (gdbbx()->get('revisions_reply_protection_active', 'bbpress')) {
            remove_filter('bbp_get_topic_content', 'bbp_topic_content_append_revisions', 99, 2);
            remove_filter('bbp_get_reply_content', 'bbp_reply_content_append_revisions', 99, 2);

            add_filter('bbp_get_topic_content', array($this, 'reply_content_append_revisions'), 99, 2);
            add_filter('bbp_get_reply_content', array($this, 'reply_content_append_revisions'), 99, 2);
        }

        if (gdbbx()->get('tags_in_reply_form_only_for_author', 'bbpress')) {
            add_action('bbp_theme_before_reply_form', array($this, 'theme_before_reply_form'));
        }

        if (gdbbx()->get('new_reply_minmax_active', 'bbpress')) {
            add_filter('bbp_new_reply_pre_title', array($this, 'new_reply_title'));
            add_filter('bbp_new_reply_pre_content', array($this, 'new_reply_content'));
        }
    }

    public function template_loader() {
        add_filter('bbp_reply_admin_links', array($this, 'reply_admin_links'));
        add_filter('gdbbx_reply_footer_links', array($this, 'reply_footer_links'), 10, 2);
    }

    public function reply_admin_links($links) {
        if (gdbbx()->get('reply_links_remove_split', 'bbpress')) {
            if (isset($links['split'])) {
                unset($links['split']);
            }
        }

        if (gdbbx()->get('reply_links_edit_footer', 'bbpress')) {
            if (isset($links['edit'])) {
                unset($links['edit']);
            }
        }

        if (gdbbx()->get('reply_links_reply_footer', 'bbpress')) {
            if (isset($links['reply'])) {
                unset($links['reply']);
            }
        }

        return $links;
    }

    public function reply_footer_links($links, $id) {
        if (gdbbx()->get('reply_links_edit_footer', 'bbpress')) {
            $edit = bbp_get_reply_edit_link(array('id' => $id));

            if (!empty($edit)) {
                $links['edit'] = $edit;
            }
        }

        if (gdbbx()->get('reply_links_reply_footer', 'bbpress')) {
            $reply = bbp_get_reply_to_link(array('id' => $id));

            if (!empty($reply)) {
                $links['reply'] = $reply;
            }
        }

        return $links;
    }

    public function reply_titles_print_title() {
        remove_filter('the_title', 'bbp_get_reply_title_fallback', 2, 2);

        $topic_title = bbp_get_reply_title();

        add_filter('the_title', 'bbp_get_reply_title_fallback', 2, 2);

	if ($topic_title && $topic_title !== bbp_get_topic_title()) {
            echo '<h4 class="bbp-reply-title">'.$topic_title.'</h4>';
	}
    }

    public function reply_titles_form_field() {
        ?>

        <p>
            <label for="bbp_reply_title"><?php printf(__("Reply Title (Maximum Length: %d):", "gd-bbpress-toolbox"), bbp_get_title_max_length()); ?></label><br />
            <input type="text" id="bbp_topic_title" value="<?php bbp_form_reply_title(); ?>" tabindex="<?php bbp_tab_index(); ?>" size="40" name="bbp_reply_title" maxlength="<?php bbp_title_max_length(); ?>" />
        </p>

        <?php
    }

    public function reply_content_append_revisions($content = '', $id = 0) {
        $is_topic = bbp_is_topic($id);
        $is_reply = bbp_is_reply($id);

        $author = $is_topic ? bbp_get_topic_author_id($id) : bbp_get_reply_author_id($id);
        $author_topic = $is_reply ? bbp_get_topic_author_id(bbp_get_reply_topic_id($id)) : $author;

        $user = bbp_get_current_user_id();

        $allowed = false;
        if ($user == $author && gdbbx()->get('revisions_reply_protection_author', 'bbpress')) {
            $allowed = true;
        }

        if (!$allowed && $is_reply && $user == $author_topic && gdbbx()->get('revisions_reply_protection_topic_author', 'bbpress')) {
            $allowed = true;
        }

        if (!$allowed) {
            $allowed = gdbbx()->allowed('revisions_reply_protection', 'bbpress', true);
        }

        if ($allowed) {
            if ($is_topic) {
                $content = apply_filters('bbp_topic_append_revisions', $content.bbp_get_topic_revision_log($id), $content, $id);
            } else {
                $content = apply_filters('bbp_reply_append_revisions', $content.bbp_get_reply_revision_log($id), $content, $id);
            }
        }

        return $content;
    }

    public function theme_before_reply_form() {
        $topic_id = bbp_get_topic_id();

        if (get_current_user_id() != bbp_get_topic_author_id($topic_id)) {
            add_filter('bbp_allow_topic_tags', '__return_false', 10000);
        }
    }

    public function new_reply_title($title) {
        $length = strlen($title);

        $check = gdbbx()->prefix_get('new_reply_', 'bbpress');

        if ($check['min_title_length'] > 0) {
            if ($length < $check['min_title_length']) {
                bbp_add_error('bbp_reply_title', __("<strong>ERROR</strong>: Your reply title is too short.", "gd-bbpress-toolbox"));
            }
        }

        if ($check['max_title_length'] > 0) {
            if ($length > $check['max_title_length']) {
                bbp_add_error('bbp_reply_title', __("<strong>ERROR</strong>: Your reply title is too long.", "gd-bbpress-toolbox"));
            }
        }

        return $title;
    }

    public function new_reply_content($content) {
        $length = strlen($content);

        $check = gdbbx()->prefix_get('new_reply_', 'bbpress');

        if ($check['min_content_length'] > 0) {
            if ($length < $check['min_content_length']) {
                bbp_add_error('bbp_reply_content', __("<strong>ERROR</strong>: Your reply is too short.", "gd-bbpress-toolbox"));
            }
        }

        if ($check['max_content_length'] > 0) {
            if ($length > $check['max_content_length']) {
                bbp_add_error('bbp_reply_content', __("<strong>ERROR</strong>: Your reply is too long.", "gd-bbpress-toolbox"));
            }
        }

        return $content;
    }
}
