<?php

if (!defined('ABSPATH')) exit;

class gdbbx_mod_forums {
    function __construct() {
        add_action('bbp_theme_before_topic_title', array($this, 'show_icon_marks'), 25);

        add_action('gdbbx_template_before_topics_loop', array($this, 'before_topics_loop'), 10, 2);

        if (gdbbx()->get('forum_load_search_for_all_forums', 'bbpress')) {
            add_action('bbp_template_before_single_forum', 'gdbbx_load_seach_form_template');
        }
    }

    public function before_topics_loop($posts, $users) {
        if ($this->_do_mark_replied()) {
            gdbbx_cache()->userreplied_run_bulk_topics($posts);
        }
    }

    public function show_icon_marks() {
        $topic_id = bbp_get_topic_id();

        if (gdbbx()->get('forum_mark_lock', 'bbpress') && gdbbx_module_lock()->is_topic_temp_locked($topic_id)) {
            echo gdbbx_obj_icons()->locked_topic();
        }

        if (gdbbx()->get('forum_mark_stick', 'bbpress') && bbp_is_topic_sticky($topic_id)) {
            echo gdbbx_obj_icons()->sticky_topic();
        }

        if ($this->_do_mark_replied()) {
            if (gdbbx_cache()->userreplied_user_replied($topic_id)) {
                echo gdbbx_obj_icons()->replied_to_topic();
            }
        }
    }

    private function _do_mark_replied() {
        return is_user_logged_in() && gdbbx()->get('forum_mark_replied', 'bbpress');
    }
}
