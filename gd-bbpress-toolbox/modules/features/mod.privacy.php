<?php

if (!defined('ABSPATH')) exit;

class gdbbx_mod_privacy {
    public function __construct() {
        if (gdbbx()->get('disable_ip_logging', 'privacy')) {
            add_filter('bbp_current_author_ip', '__return_empty_string');
        }

        if (gdbbx()->get('disable_ip_display', 'privacy')) {
            add_filter('bbp_get_author_ip', '__return_empty_string');
        }
    }
}

/** @return gdbbx_mod_privacy */
function gdbbx_module_privacy() {
    return gdbbx_loader()->modules['privacy'];
}
