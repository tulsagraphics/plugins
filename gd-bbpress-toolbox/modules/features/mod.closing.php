<?php

if (!defined('ABSPATH')) exit;

class gdbbx_mod_topics_closing {
    public $limit = 500;

    function __construct() {
        if ($this->auto_close_is_active()) {
            add_action('bbp_theme_before_reply_form_notices', array($this, 'closing_notice'));
        }

        if (gdbbx()->get('reply_close_topic_checkbox_active', 'bbpress')) {
            add_action(gdbbx()->get('reply_close_topic_checkbox_form_position', 'bbpress'), array($this, 'close_topic_checkbox'), 9);

            add_action('bbp_new_reply',  array($this, 'close_topic_save'), 1, 2);
            add_action('bbp_edit_reply',  array($this, 'close_topic_save'), 1, 2);
        }
    }

    public function minimum_days_allowed() {
        return apply_filters('gdbbx_auto_close_topic_minimum_days_allowed', 7);
    }

    public function auto_close_is_active() {
        return gdbbx()->get('topic_auto_close_after_status', 'bbpress');
    }

    public function auto_close_forum_rule($forum_id) {
        $_meta = gdbbx_obj_forums()->forum($forum_id)->topic_auto_close()->all();

        if ($_meta['active'] == 'default') {
            $_meta['active'] = gdbbx()->get('topic_auto_close_after_active', 'bbpress');
        } else {
            $_meta['active'] = $_meta['active'] == 'yes';
        }

        if ($_meta['notice'] == 'default') {
            $_meta['notice'] = gdbbx()->get('topic_auto_close_after_notice', 'bbpress');
        } else {
            $_meta['notice'] = $_meta['notice'] == 'yes';
        }

        if (empty($_meta['days'])) {
            $_meta['days'] = gdbbx()->get('topic_auto_close_after_days', 'bbpress');
        }

        $_meta['days'] = absint($_meta['days']);

        if ($_meta['days'] < $this->minimum_days_allowed()) {
            $_meta['days'] = absint($this->minimum_days_allowed());
        }

        if ($_meta['days'] < 1) {
            $_meta['days'] = 1;
        }

        return $_meta;
    }

    public function closing_notice() {
        $_meta = $this->auto_close_forum_rule(bbp_get_forum_id());

        if ($_meta['active'] && $_meta['notice'] && bbp_is_topic_open()) {
            $days = $_meta['days'];

            $message = sprintf(_n("This topic will close <strong>%s day</strong> after the last reply.", "This topic will close <strong>%s days</strong> after the last reply.", $days, "gd-bbpress-toolbox"), $days);
            $notice = '<div class="bbp-template-notice info"><p>'.$message.'</p></div>';

            echo apply_filters('gdbxx_auto_close_topic_notice', $notice, $message, $days);
        }
    }

    public function run_auto_close() {
        set_time_limit(0);

        $sql = $this->build_auto_close_query();

        $raw = gdbbx_db()->get_results($sql);
        $total = gdbbx_db()->get_found_rows();

        $forums = array();

        foreach ($raw as $row) {
            if (!isset($forums[$row->forum_id])) {
                $forums[$row->forum_id] = $this->auto_close_forum_rule($row->forum_id);
            }
        }

        foreach ($raw as $row) {
            $rule = $forums[$row->forum_id];

            if ($rule['active'] && $rule['days'] < $row->last_active) {
                $done = bbp_close_topic($row->topic_id);

                if (!is_wp_error($done)) {
                    update_post_meta($row->topic_id, '_gdbbx_auto_closed', time());
                }
            } else {
                update_post_meta($row->topic_id, '_gdbbx_auto_close_skip', time());
            }
        }

        if ($total > count($raw)) {
            wp_schedule_single_event(time() + 5, 'gdbbx_cron_auto_close_topics');
        } else {
            $this->clear_auto_close_skip();
        }
    }

    public function clear_auto_close_skip() {
        $sql = "DELETE FROM ".gdbbx_db()->wpdb()->postmeta." WHERE meta_key = '_gdbbx_auto_close_skip'";

        gdbbx_db()->query($sql);
    }

    private function build_auto_close_query() {
        return "SELECT SQL_CALC_FOUND_ROWS p.ID AS topic_id, p.post_parent as forum_id, DATEDIFF(CURDATE(), CAST(ma.meta_value AS DATETIME)) AS last_active
                FROM ".gdbbx_db()->wpdb()->posts." p
                INNER JOIN ".gdbbx_db()->wpdb()->postmeta." ma ON ma.post_id = p.ID AND ma.meta_key = '_bbp_last_active_time'
                LEFT JOIN ".gdbbx_db()->wpdb()->postmeta." mx ON mx.post_id = p.ID AND mx.meta_key = '_gdbbx_auto_close_skip'
                WHERE p.post_type = '".bbp_get_topic_post_type()."' AND p.post_status = 'publish' AND mx.meta_value IS NULL
                AND CAST(ma.meta_value AS DATETIME) < DATE_SUB(CURDATE(), INTERVAL ".$this->minimum_days_allowed()." DAY)
                ORDER BY topic_id ASC LIMIT ".$this->limit;
    }

    public function close_topic_checkbox() {
        $allowed = gdbbx()->allowed('reply_close_topic_checkbox', 'bbpress');

        if (!$allowed && is_user_logged_in()) {
            if (gdbbx()->get('reply_close_topic_checkbox_topic_author', 'bbpress') && bbp_get_topic_author_id() == bbp_get_current_user_id()) {
                $allowed = true;
            }
        }

        if ($allowed) {
            $tabindex = apply_filters('gdbbx_close_topic_checkbox_tab_index', bbp_get_tab_index());
            $label = apply_filters('gdbbx_close_topic_checkbox_label', __("Close this topic", "gd-bbpress-toolbox"));

            ?>

            <p>
                <input name="gdbbx_close_topic" id="gdbbx_close_topic" type="checkbox" value="close" tabindex="<?php echo esc_attr($tabindex); ?>" /> 
                <label for="gdbbx_close_topic"><?php echo $label; ?></label>
            </p>

            <?php 
        }
    }

    public function close_topic_save($reply_id = 0, $topic_id = 0) {
        $allowed = gdbbx()->allowed('reply_close_topic_checkbox', 'bbpress');

        if (!$allowed && is_user_logged_in()) {
            if (gdbbx()->get('reply_close_topic_checkbox_topic_author', 'bbpress') && bbp_get_topic_author_id($topic_id) == bbp_get_current_user_id()) {
                $allowed = true;
            }
        }

        if ($allowed) {
            if (isset($_POST['gdbbx_close_topic']) && $_POST['gdbbx_close_topic'] == 'close') {
                bbp_close_topic($topic_id);
            }
        }
    }
}

/** @return gdbbx_mod_topics_closing */
function gdbbx_module_topics_closing() {
    return gdbbx_loader()->modules['topics_closing'];
}
