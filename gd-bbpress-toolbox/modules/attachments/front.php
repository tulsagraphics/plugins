<?php

if (!defined('ABSPATH')) exit;

class gdbbx_attachments_front {
    private $inserted = array();

    public $enabled = false;
    public $file_size = 0;

    public $forum_id;
    public $user_id;

    /** @var gdbbx_attachments_display */
    public $display;

    private $icons = array(
        'code' => 'c|cc|h|js|class|json', 
        'xml' => 'xml', 
        'excel' => 'xla|xls|xlsx|xlt|xlw|xlam|xlsb|xlsm|xltm', 
        'word' => 'docx|dotx|docm|dotm', 
        'image' => 'png|gif|jpg|jpeg|jpe|jp|bmp|tif|tiff', 
        'psd' => 'psd', 
        'ai' => 'ai', 
        'archive' => 'zip|rar|gz|gzip|tar',
        'text' => 'txt|asc|nfo', 
        'powerpoint' => 'pot|pps|ppt|pptx|ppam|pptm|sldm|ppsm|potm', 
        'pdf' => 'pdf', 
        'html' => 'htm|html|css', 
        'video' => 'avi|asf|asx|wax|wmv|wmx|divx|flv|mov|qt|mpeg|mpg|mpe|mp4|m4v|ogv|mkv', 
        'documents' => 'odt|odp|ods|odg|odc|odb|odf|wp|wpd|rtf',
        'audio' => 'mp3|m4a|m4b|mp4|m4v|wav|ra|ram|ogg|oga|mid|midi|wma|mka',
        'icon' => 'ico'
    );

    private $fonti = array(
        'file-code-o' => 'code|xml|html',
        'file-picture-o' => 'image|psd|ai|icon',
        'file-pdf-o' => 'pdf',
        'file-excel-o' => 'excel',
        'file-word-o' => 'word',
        'file-powerpoint-o' => 'powerpoint',
        'file-text-o' => 'text|documents',
        'file-video-o' => 'video',
        'file-archive-o' => 'archive',
        'file-audio-o' => 'audio',
        'file-o' => 'generic'
    );

    function __construct() {
        add_action('gdbbx_core', array($this, 'load'));
    }

    public function load() {
        $this->icons = apply_filters('gdbbx_attachments_icons_sets', $this->icons);
        $this->fonti = apply_filters('gdbbx_attachments_font_icons_sets', $this->fonti);

        add_action('gdbbx_template_before_replies_loop', array($this, 'before_replies_loop'), 10, 2);
        add_action('gdbbx_template_before_topics_loop', array($this, 'before_topics_loop'), 10, 2);

        add_filter('gdbbx_script_values', array($this, 'script_values'));
        add_action('gdbbx_attachments_form_notices', array($this, 'form_notices'));

        add_action(gdbbx()->get('form_position_reply', 'attachments'), array($this, 'embed_form'));
        add_action(gdbbx()->get('form_position_topic', 'attachments'), array($this, 'embed_form'));

        add_action('bbp_edit_reply', array($this, 'save_reply'), 10, 5);
        add_action('bbp_edit_topic', array($this, 'save_topic'), 10, 4);
        add_action('bbp_new_reply', array($this, 'save_reply'), 10, 5);
        add_action('bbp_new_topic', array($this, 'save_topic'), 10, 4);

        $this->add_content_filters();

        if (gdbbx()->get('attachment_icon', 'attachments')) {
            add_action('bbp_theme_before_topic_title', array($this, 'show_attachments_icon'), 20);
        }
    }

    public function upload_dir($args) {
        $newdir = $this->_upload_dir_structure();

        $args['path'] = str_replace($args['subdir'], '', $args['path']);
        $args['url'] = str_replace($args['subdir'], '', $args['url']);      
        $args['subdir'] = $newdir;
        $args['path'].= $newdir; 
        $args['url'].= $newdir;

        return $args;
    }

    public function before_replies_loop($posts, $users) {
        gdbbx_cache()->attachments_run_bulk_counts($posts);
    }

    public function before_topics_loop($posts, $users) {
        gdbbx_cache()->attachments_run_bulk_topics_counts($posts);
    }

    public function add_content_filters() {
        if (!$this->enabled) {
            $this->enabled = true;

            if (gdbbx()->get('files_list_position', 'attachments') == 'content') {
                add_filter('bbp_get_reply_content', array($this, 'embed_attachments'), 100, 2);
                add_filter('bbp_get_topic_content', array($this, 'embed_attachments'), 100, 2);
            } else if (gdbbx()->get('files_list_position', 'attachments') == 'after') {
                add_action('bbp_theme_after_topic_content', array($this, 'after_attachments'), 20);
                add_action('bbp_theme_after_reply_content', array($this, 'after_attachments'), 20);
            }
        }
    }

    public function remove_content_filters() {
        $this->enabled = false;

        remove_filter('bbp_get_topic_content', array($this, 'embed_attachments'), 100, 2);
        remove_filter('bbp_get_reply_content', array($this, 'embed_attachments'), 100, 2);
    }

    public function script_values($values) {
        $values['run_attachments'] = true;
        $values['validate_attachments'] = gdbbx_attachments()->get('validation_active');
        $values['max_files'] = gdbbx_attachments()->get_max_files();
        $values['max_size'] = gdbbx_attachments()->get_file_size() * 1024;
        $values['limiter'] = !gdbbx_attachments()->is_no_limit();
        $values['allowed_extensions'] = strtolower(join(' ', gdbbx_attachments()->get_file_extensions()));
        $values['text_select_file'] = __("Select File", "gd-bbpress-toolbox");
        $values['text_file_name'] = __("Name", "gd-bbpress-toolbox");
        $values['text_file_size'] = __("Size", "gd-bbpress-toolbox");
        $values['text_file_type'] = __("Extension", "gd-bbpress-toolbox");
        $values['text_file_validation'] = __("Error!", "gd-bbpress-toolbox");
        $values['text_file_validation_size'] = __("The file is too big.", "gd-bbpress-toolbox");
        $values['text_file_validation_type'] = __("File type not allowed.", "gd-bbpress-toolbox");
        $values['text_file_remove'] = __("Remove this file", "gd-bbpress-toolbox");
        $values['text_file_shortcode'] = __("Insert into content", "gd-bbpress-toolbox");
        $values['text_file_caption'] = __("Set file caption", "gd-bbpress-toolbox");
        $values['text_file_caption_placeholder'] = __("Caption...", "gd-bbpress-toolbox");

        $values['insert_into_content'] = false;
        if (gdbbx_attachments()->get('insert_into_content')) {
            if (gdbbx()->allowed('insert_into_content', 'attachments', false, false)) {
                $values['insert_into_content'] = true;
            }
        }

        return $values;
    }

    public function form_notices() {
        if (gdbbx_attachments()->is_no_limit()) {
            $message = __("Your account has the ability to upload any attachment regardless of size and type.", "gd-bbpress-toolbox");

            echo apply_filters('gdbbx_notice_attachments_no_limit', '<div class="bbp-template-notice info"><p>'.$message.'</p></div>', $message);
        } else {
            $file_size = d4p_file_size_format($this->file_size * 1024, 2);

            $message = sprintf(__("Maximum file size allowed is %s.", "gd-bbpress-toolbox"), '<strong>'.$file_size.'</strong>');

            echo apply_filters('gdbbx_notice_attachments_limit_file_size', '<div class="bbp-template-notice"><p>'.$message.'</p></div>', $message, $file_size);

            if (gdbbx()->get('mime_types_limit_active', 'attachments') && gdbbx()->get('mime_types_limit_display', 'attachments')) {
                $show = gdbbx_attachments()->get_file_extensions();

                $message = sprintf(__("File types allowed for upload: %s.", "gd-bbpress-toolbox"), '<strong>.'.join('</strong>, <strong>.', $show).'</strong>');

                echo apply_filters('gdbbx_notice_attachments_limit_file_types', '<div class="bbp-template-notice"><p>'.$message.'</p></div>', $message, $show);
            }
        }
    }

    public function save_topic($topic_id, $forum_id, $anonymous_data, $topic_author) {
        $this->save_reply(0, $topic_id, $forum_id, $anonymous_data, $topic_author);
    }

    public function save_reply($reply_id, $topic_id, $forum_id, $anonymous_data, $reply_author) {
        $is_topic = $reply_id == 0;

        $post_id = $reply_id == 0 ? $topic_id : $reply_id;

        if (isset($_POST['gdbbx']['remove-attachment'])) {
            $attachments = (array)$_POST['gdbbx']['remove-attachment'];

            foreach ($attachments as $id => $action) {
                $attachment_id = absint($id);

                if ($action == 'delete' || $action == 'detach') {
                    gdbbx_attachments()->delete_attachment($attachment_id, $post_id, $action);
                }
            }
        }

        $uploads = array();
        $original = array();
        $uploads_captions = array();

        $featured = false; 
        if ($is_topic) {
            $featured = gdbbx()->get('topic_featured_image', 'attachments');
        } else {
            $featured = gdbbx()->get('reply_featured_image', 'attachments');
        }

        $counter = 0;
        $captions = isset($_POST['gdbbx-attachment_caption']) ? (array)$_POST['gdbbx-attachment_caption'] : array();

        if (!empty($_FILES) && !empty($_FILES['gdbbx-attachment'])) {
            require_once(ABSPATH.'wp-admin/includes/file.php');

            $errors = new gdbbx_error();
            $overrides = array(
                'test_form' => false, 
                'upload_error_handler' => 'gdbbx_attachment_handle_upload_error'
            );

            foreach ($_FILES['gdbbx-attachment']['error'] as $key => $error) {
                $file_name = $_FILES['gdbbx-attachment']['name'][$key];

                if ($error == UPLOAD_ERR_OK) {
                    $file = array('name' => $file_name,
                        'type' => $_FILES['gdbbx-attachment']['type'][$key],
                        'size' => $_FILES['gdbbx-attachment']['size'][$key],
                        'tmp_name' => $_FILES['gdbbx-attachment']['tmp_name'][$key],
                        'error' => $_FILES['gdbbx-attachment']['error'][$key]
                    );

                    $file_name = sanitize_file_name($file_name);

                    if (gdbbx_attachments()->is_right_size($file, $forum_id)) {
                        $mimes = gdbbx_attachments()->filter_mime_types($forum_id);
                        if (!is_null($mimes) && !empty($mimes)) {
                            $overrides['mimes'] = $mimes;
                        }

                        $this->forum_id = $forum_id;
                        $this->user_id = $reply_author;

                        if (gdbbx()->get('upload_dir_override', 'attachments')) {
                            add_filter('upload_dir', array($this, 'upload_dir'));
                        }

                        $upload = wp_handle_upload($file, $overrides);

                        if (gdbbx()->get('upload_dir_override', 'attachments')) {
                            remove_filter('upload_dir', array($this, 'upload_dir'));
                        }

                        $caption = isset($captions[$counter]) ? sanitize_text_field($captions[$counter]) : '';

                        if (!is_wp_error($upload)) {
                            $uploads[] = $upload;
                            $original[] = $file_name;
                            $uploads_captions[] = $caption;
                        } else {
                            $errors->add('wp_upload', $upload->errors['wp_upload_error'][0], $file_name);
                        }
                    } else {
                        $errors->add('d4p_upload', 'File exceeds allowed file size.', $file_name);
                    }
                } else {
                    switch ($error) {
                        default:
                        case 'UPLOAD_ERR_NO_FILE':
                            $errors->add('php_upload', 'File not uploaded.', $file_name);
                            break;
                        case 'UPLOAD_ERR_INI_SIZE':
                            $errors->add('php_upload', 'Upload file size exceeds PHP maximum file size allowed.', $file_name);
                            break;
                        case 'UPLOAD_ERR_FORM_SIZE':
                            $errors->add('php_upload', 'Upload file size exceeds FORM specified file size.', $file_name);
                            break;
                        case 'UPLOAD_ERR_PARTIAL':
                            $errors->add('php_upload', 'Upload file only partially uploaded.', $file_name);
                            break;
                        case 'UPLOAD_ERR_CANT_WRITE':
                            $errors->add('php_upload', 'Can\'t write file to the disk.', $file_name);
                            break;
                        case 'UPLOAD_ERR_NO_TMP_DIR':
                            $errors->add('php_upload', 'Temporary folder for upload is missing.', $file_name);
                            break;
                        case 'UPLOAD_ERR_EXTENSION':
                            $errors->add('php_upload', 'Server extension restriction stopped upload.', $file_name);
                            break;
                    }
                }

                $counter++;
            }
        }

        if (!empty($errors->errors) && gdbbx()->get('log_upload_errors', 'attachments') == 1) {
            foreach ($errors->errors as $code => $errs) {
                foreach ($errs as $error) {
                    if ($error[0] != '' && $error[1] != '') {
                        add_post_meta($post_id, '_bbp_attachment_upload_error', array(
                            'code' => $code, 'file' => $error[1], 'message' => $error[0])
                        );
                    }
                }
            }
        }

        if (!empty($uploads)) {
            require_once(ABSPATH.'wp-admin/includes/media.php');
            require_once(ABSPATH.'wp-admin/includes/image.php');

            $counter = 0;
            $update_attachments = array();
            foreach ($uploads as $_key => $upload) {
                $wp_filetype = wp_check_filetype(basename($upload['file']));
                $att_name = basename($upload['file']);
                $org_name = $original[$_key];

                $attachment = array('post_mime_type' => $wp_filetype['type'],
                    'post_title' => preg_replace('/\.[^.]+$/', '', $att_name),
                    'post_excerpt' => $uploads_captions[$counter],
                    'post_content' => '','post_status' => 'inherit'
                );

                $attach_id = wp_insert_attachment($attachment, $upload['file'], $post_id);
                $attach_data = wp_generate_attachment_metadata($attach_id, $upload['file']);

                wp_update_attachment_metadata($attach_id, $attach_data);

                update_post_meta($attach_id, '_bbp_attachment', '1');
                update_post_meta($attach_id, '_bbp_attachment_name', $att_name);
                update_post_meta($attach_id, '_bbp_attachment_original_name', $org_name);

                $update_attachments[] = array('name' => $org_name, 'id' => $attach_id);

                $counter++;
            }

            if (!empty($update_attachments)) {
                $post = get_post($post_id);
                $content = $post->post_content;

                $matches = array();
                $new_list = array();

                $preg = preg_match_all("/(?<attachment>\[attachment.+?\])/i", $content, $matches);
                $list = isset($matches['attachment']) ? $matches['attachment'] : array();

                if (!empty($list)) {
                    foreach ($update_attachments as $att) {
                        $search = '"'.$att['name'].'"';
                        $replace = $att['id'];

                        foreach ($list as $_key => $file) {
                            if (stripos($file, $search) !== false) {
                                $nfile = str_replace($search, $replace, $file);
                                $new_list[$_key] = $nfile;
                                break;
                            }
                        }
                    }

                    if (!empty($new_list)) {
                        foreach ($list as $_key => $att) {
                            if (isset($new_list[$_key])) {
                                $content = str_replace($att, $new_list[$_key], $content);
                            }
                        }

                        wp_update_post(array(
                            'ID' => $post->ID,
                            'post_content' => $content
                        ));
                    }
                }
            }

            if (current_theme_supports('post-thumbnails')) {
                if ($featured && !has_post_thumbnail($post_id)) {
                    $args = array(
                        'numberposts' => 1,
                        'order' => 'ASC',
                        'post_mime_type' => 'image',
                        'post_parent' => $post_id,
                        'post_status' => null,
                        'post_type' => 'attachment',
                    );

                    $images = get_children($args);

                    if (!empty($images)) {
                        foreach ($images as $image) {
                            set_post_thumbnail($post_id, $image->ID);
                        }
                    }
                }
            }
        }

        gdbbx_db()->update_topic_attachments_count($topic_id);
    }

    public function show_attachments_icon() {
        $topic_id = bbp_get_topic_id();

        if (gdbbx_cache()->attachments_has_topic_attachments($topic_id)) {
            echo $this->render_topic_list_icon(gdbbx_cache()->attachments_count_topic_attachments($topic_id));
        }
    }

    public function embed_attachments_edit($attachments, $post_id) {
        d4p_include('functions', 'admin', GDBBX_D4PLIB);

        $_icons = gdbbx()->get('attachment_icons', 'attachments');
        $_type = gdbbx()->get('icons_mode', 'attachments');

        $_deletion = gdbbx()->get('delete_method', 'attachments') == 'edit';

        $actions = array();

        if ($_deletion) {
            $post = get_post($post_id);
            $author_id = $post->post_author;

            $allow = gdbbx_attachments()->deletion_status($author_id);

            if ($allow != 'no') {
                $actions[''] = __("Do Nothing", "gd-bbpress-toolbox");

                if ($allow == 'delete' || $allow == 'both') {
                    $actions['delete'] = __("Delete", "gd-bbpress-toolbox");
                }

                if ($allow == 'detach' || $allow == 'both') {
                    $actions['detach'] = __("Detach", "gd-bbpress-toolbox");
                }
            }
        }

        $content = '<div class="gdbbx-attachments gdbbx-attachments-edit">';
        $content.= '<input type="hidden" />';
        $content.= '<ol';

        if ($_icons) {
            switch ($_type) {
                case 'images':
                    $content.= ' class="with-icons"';
                    break;
                case 'font':
                    $content.= ' class="with-font-icons"';
                    break;
            }
        }

        $content.= '>';

        foreach ($attachments as $attachment) {
            $insert = array('<a role="button" class="gdbbx-attachment-insert" href="#'.$attachment->ID.'">'.__("insert into content", "gd-bbpress-toolbox").'</a>');

            $file = get_attached_file($attachment->ID);
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $filename = pathinfo($file, PATHINFO_BASENAME);
            $url = wp_get_attachment_url($attachment->ID);

            $a_title = $filename;
            $html = $filename;
            $class_li = '';

            if ($_icons && $_type == 'images') {
                $class_li = "gdbbx-image gdbbx-image-".$this->icon($ext);
            }

            if ($_icons && $_type == 'font') {
                $html = $this->render_attachment_icon($ext).$html;
            }

            $item = '<li id="gdbbx-attachment-id-'.$attachment->ID.'" class="gdbbx-attachment gdbbx-attachment-'.$ext.' '.$class_li.'">';
            $item.= '<a href="'.$url.'" title="'.$a_title.'" download>'.$html.'</a>';
            $item.= ' ['.join(' | ', $insert).']';

            if (!empty($actions)) {
                $item.= d4p_render_select($actions, array('name' => 'gdbbx[remove-attachment]['.$attachment->ID.']', 'echo' => false), array('title' => __("Attachment Actions", "gd-bbpress-toolbox")));
            }

            $item.= '</li>';

            $content.= $item;
        }

        $content.= '</ol>';
        $content.= '</div>';

        return $content;
    }

    public function after_attachments() {
        $id = bbp_get_reply_id();

        if ($id == 0) {
            $id = bbp_get_topic_id();
        }

        echo $this->embed_attachments('', $id);
    }

    public function attachment_inserted($id, $attachment_id) {
        if (!isset($this->inserted[$id])) {
            $this->inserted[$id] = array();
        }

        $this->inserted[$id][] = $attachment_id;
    }

    public function get_inserted_attachments($id) {
        if (isset($this->inserted[$id]) && !empty($this->inserted[$id])) {
            return $this->inserted[$id];
        }

        return array();
    }

    public function embed_attachments($content, $id) {
        if (gdbbx_is_feed()) {
            return $content;
        }

        if (gdbbx_cache()->attachments_has_attachments($id)) {
            require_once(GDBBX_PATH.'modules/attachments/display.php');
            $this->display = new gdbbx_attachments_display($this);

            return $content.$this->display->show($id);
        }

        return $content;
    }

    public function embed_form() {
        $is_this_edit = bbp_is_topic_edit() || bbp_is_reply_edit();

        $can_upload = apply_filters('gdbbx_attchaments_allow_upload', gdbbx_attachments()->is_user_allowed(), bbp_get_forum_id());

        if ($can_upload) {
            if (gdbbx_attachments()->is_active()) {
                $this->file_size = apply_filters('gdbbx_attchaments_max_file_size', gdbbx_attachments()->get_file_size(), bbp_get_forum_id());

                if ($is_this_edit) {
                    $id = bbp_is_topic_edit() ? bbp_get_topic_id() : bbp_get_reply_id();

                    $attachments = gdbbx_get_post_attachments($id);

                    if (!empty($attachments)) {
                        include(gdbbx_get_template_part('gdbbx-form-attachment-edit.php'));
                    }
                }

                include(gdbbx_get_template_part('gdbbx-form-attachment.php'));

                gdbbx_enqueue_files_force();
            }
        }
    }

    public function render_attachment_icon($ext) {
        $icon = $this->icon($ext);

        $cls = 'gdbbx-icon gdbbx-icon-';
        foreach ($this->fonti as $fa => $list) {
            $list = explode('|', $list);

            if (in_array($icon, $list)) {
                $cls.= $fa;
            }
        }

        return '<i class="'.$cls.' gdbbx-fw"></i> ';
    }

    public function render_topic_list_icon($count) {
        $render = '';

        if (gdbbx()->get('icons_mode', 'attachments') == 'images') {
            $render = '<span class="gdbbx-image-mark gdbbx-image-paperclip" title="'.$count.' '._n("attachment", "attachments", $count, "gd-bbpress-toolbox").'"></span>';
        } else if (gdbbx()->get('icons_mode', 'attachments') == 'font') {
            $render = '<i class="gdbbx-icon-mark gdbbx-icon gdbbx-icon-paperclip" title="'.$count.' '._n("attachment", "attachments", $count, "gd-bbpress-toolbox").'"></i> ';
        }

        return $render;
    }

    public function icon($ext) {
        foreach ($this->icons as $icon => $list) {
            $list = explode('|', $list);

            if (in_array($ext, $list)) {
                return $icon;
            }
        }

        return 'generic';
    }

    private function _upload_dir_structure() {
        $base = d4p_sanitize_file_path(gdbbx()->get('upload_dir_forums_base', 'attachments'));
        $structure = gdbbx()->get('upload_dir_structure', 'attachments');

        $forum = get_post($this->forum_id);
        $forum_name = $forum->post_name;

        switch ($structure) {
            default:
            case '/forums':
                return '/'.$base;
            case '/forums/forum-id':
                return '/'.$base.'/'.$this->forum_id;
            case '/forums/forum-name':
                return '/'.$base.'/'.$forum_name;
            case '/forums/user-id':
                return '/'.$base.'/'.$this->user_id;
        }
    }
}
