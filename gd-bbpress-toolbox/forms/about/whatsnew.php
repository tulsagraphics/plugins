<?php // include(GDBBX_PATH.'forms/about/minor.php'); ?>

<div class="d4p-about-whatsnew">
    <div class="d4p-whatsnew-section d4p-whatsnew-heading">
        <div class="d4p-layout-grid">
            <div class="d4p-layout-unit whole align-center">
                <h2 style="font-size: 52px;">Auto closing topics &amp; new widget</h2>
                <p class="lead-description">
                    GD bbPress Toolbox Pro 5.3 is an important, major Summer update.
                </p>
                <p>
                    New version brings new module for Closing Topics with auto close features for inactive topics, new forum information widget, various BuddyPress signature support improvements, other changes and fixes.
                </p>

                <?php if (isset($_GET['install']) && $_GET['install'] == 'on') { ?>
                    <a class="button-primary" href="admin.php?page=gd-bbpress-toolbox-wizard"><?php _e("Run Setup Wizard", "gd-bbpress-toolbox"); ?></a>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="d4p-whatsnew-section">
        <div class="d4p-layout-grid">
            <div class="d4p-layout-unit half align-left">
                <h3>Auto close inactive topics</h3>
                <p>
                    Plugin version 5.3 includes brand new module that can help you with auto closing the topics that are no longer active.
                </p>
                <p>
                    Global auto close settings can be overriden for individual forums, and settings include display of the auto close notice, and number of inactivity days.
                </p>
                <p>
                    Close topic checkbox settings are moved to this module
                </p>
            </div>
            <div class="d4p-layout-unit half align-left">
                <img src="https://dev4press.s3.amazonaws.com/plugins/gd-bbpress-toolbox/5.3/about/autoclose.jpg" />
            </div>
        </div>

        <div class="d4p-layout-grid">
            <div class="d4p-layout-unit two-thirds align-left">
                <h3>Forum information widget</h3>
                <p>
                    The plugin adds a new widget that will show basic information about the current forum. This can include the link to parent forum (if available), number of topics and replies, latest activity and last poster.
                </p>
            </div>
            <div class="d4p-layout-unit one-third align-left">
                <img src="https://dev4press.s3.amazonaws.com/plugins/gd-bbpress-toolbox/5.3/about/foruminfo.jpg" />
            </div>
        </div>
    </div>

    <div class="d4p-whatsnew-section">
        <div class="d4p-layout-grid">
            <div class="d4p-layout-unit half align-left">
                <h3 style="margin-top: 0;">Improvements and Updates</h3>
                <p>
                    The plugin includes new filters for changing labels for Quote and Report buttons, metaboxes have been refreshed with icons, buddypress support for signature improved.
                </p>
            </div>
            <div class="d4p-layout-unit half align-left">
                <h3 style="margin-top: 0;">Several bug fixes</h3>
                <p>
                    This version also fixes several bugs, including email notifications issues, some buddypress related issues, minor widgets problems and few more things.
                </p>
            </div>
        </div>
    </div>
</div>
