<div class="d4p-about-minor">
    <h3><?php _e("Maintenance and Security Releases", "gd-bbpress-toolbox"); ?></h3>
    <p>
        <strong><?php _e("Version", "gd-bbpress-toolbox"); ?> <span>5.3.1</span></strong> &minus; 
        Upload dir bug fixed. Shared library updated to 2.3.2.
    </p>
    <p>
        <?php printf(__("For more information, see <a href='%s'>the changelog</a>.", "gd-bbpress-toolbox"), 'admin.php?page=gd-bbpress-toolbox-about&panel=changelog'); ?>
    </p>
</div>
