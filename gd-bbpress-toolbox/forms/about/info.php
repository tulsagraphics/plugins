<div class="d4p-group d4p-group-import d4p-group-about">
    <h3>GD bbPress Toolbox Pro</h3>
    <div class="d4p-group-inner">
        <ul>
            <li><?php _e("Version", "gd-bbpress-toolbox"); ?>: <span><?php echo gdbbx()->info_version; ?></span></li>
            <li><?php _e("Status", "gd-bbpress-toolbox"); ?>: <span><?php echo ucfirst(gdbbx()->info_status); ?></span></li>
            <li><?php _e("Build", "gd-bbpress-toolbox"); ?>: <span><?php echo gdbbx()->info_build; ?></span></li>
            <li><?php _e("Date", "gd-bbpress-toolbox"); ?>: <span><?php echo gdbbx()->info_updated; ?></span></li>
        </ul>
        <hr style="margin: 1em 0 .7em; border-top: 1px solid #eee"/>
        <ul>
            <li><?php _e("First released", "gd-bbpress-toolbox"); ?>: <span><?php echo gdbbx()->info_released; ?></span></li>
        </ul>
    </div>
</div>

<div class="d4p-group d4p-group-import d4p-group-about">
    <h3><?php _e("Important Links", "gd-bbpress-toolbox"); ?></h3>
    <div class="d4p-group-inner">
        <ul>
            <li><?php _e("On Dev4Press", "gd-bbpress-toolbox"); ?>: <span><a href="https://plugins.dev4press.com/gd-bbpress-toolbox/" target="_blank">www.dev4press.com/plugins/gd-bbpress-toolbox</a></span></li>
        </ul>
    </div>
</div>

<div class="d4p-group d4p-group-changelog">
    <h3><?php _e("Translations", "gd-bbpress-toolbox"); ?></h3>
    <div class="d4p-group-inner">
        <h4>fa_IR (Persian – Farsi)</h4>
        <ul>
            <li><?php _e("Version", "gd-bbpress-toolbox"); ?>: <span>4.4.3</span></li>
            <li><?php _e("Translator", "gd-bbpress-toolbox"); ?>: <span>Saiyed Mohammad Sadegh Hosseini</span></li>
            <li><?php _e("URL", "gd-bbpress-toolbox"); ?>: <span><a target="_blank" href="http://sitet.net/">http://sitet.net/</a></span></li>
        </ul>

        <h4>sv_SE (Svenska)</h4>
        <ul>
            <li><?php _e("Version", "gd-bbpress-toolbox"); ?>: <span>4.4</span></li>
            <li><?php _e("Translator", "gd-bbpress-toolbox"); ?>: <span>Mattias Tengblad, WordPress Sverige</span></li>
            <li><?php _e("URL", "gd-bbpress-toolbox"); ?>: <span><a target="_blank" href="https://wpsv.se">https://wpsv.se</a></span></li>
        </ul>
    </div>
</div>
