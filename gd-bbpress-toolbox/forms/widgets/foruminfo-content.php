<h4><?php _e("Information to show", "gd-bbpress-toolbox"); ?></h4>
<table>
    <tbody>
        <tr>
            <td class="cell-left">
                <div class="d4plib-checkbox-list">
                    <label for="<?php echo $this->get_field_id('show_parent_forum'); ?>">
                        <input class="widefat" <?php echo $instance['show_parent_forum'] == 1 ? 'checked="checked"' : ''; ?> type="checkbox" id="<?php echo $this->get_field_id('show_parent_forum'); ?>" name="<?php echo $this->get_field_name('show_parent_forum'); ?>" />
                        <?php _e("Show parent forum", "gd-bbpress-toolbox"); ?></label><br/>

                    <label for="<?php echo $this->get_field_id('show_count_replies'); ?>">
                        <input class="widefat" <?php echo $instance['show_count_replies'] == 1 ? 'checked="checked"' : ''; ?> type="checkbox" id="<?php echo $this->get_field_id('show_count_replies'); ?>" name="<?php echo $this->get_field_name('show_count_replies'); ?>" />
                        <?php _e("Show replies count", "gd-bbpress-toolbox"); ?></label><br/>

                    <label for="<?php echo $this->get_field_id('show_count_topics'); ?>">
                        <input class="widefat" <?php echo $instance['show_count_topics'] == 1 ? 'checked="checked"' : ''; ?> type="checkbox" id="<?php echo $this->get_field_id('show_count_voices'); ?>" name="<?php echo $this->get_field_name('show_count_topics'); ?>" />
                        <?php _e("Show voices count", "gd-bbpress-toolbox"); ?></label>
                </div>
            </td>
            <td class="cell-right">
                <div class="d4plib-checkbox-list">
                    <label for="<?php echo $this->get_field_id('show_last_post_user'); ?>">
                        <input class="widefat" <?php echo $instance['show_last_post_user'] == 1 ? 'checked="checked"' : ''; ?> type="checkbox" id="<?php echo $this->get_field_id('show_last_post_user'); ?>" name="<?php echo $this->get_field_name('show_last_post_user'); ?>" />
                        <?php _e("Show last post user", "gd-bbpress-toolbox"); ?></label><br/>

                    <label for="<?php echo $this->get_field_id('show_last_activity'); ?>">
                        <input class="widefat" <?php echo $instance['show_last_activity'] == 1 ? 'checked="checked"' : ''; ?> type="checkbox" id="<?php echo $this->get_field_id('show_last_activity'); ?>" name="<?php echo $this->get_field_name('show_last_activity'); ?>" />
                        <?php _e("Show last activity", "gd-bbpress-toolbox"); ?></label>
                </div>
            </td>
        </tr>
    </tbody>
</table>
