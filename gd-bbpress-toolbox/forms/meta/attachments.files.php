<?php

$attachments = gdbbx_get_post_attachments($post_id);

if (empty($attachments)) {
    echo '<p>'.__("No attachments here.", "gd-bbpress-toolbox").'</p>';
} else {
    echo '<ul style="list-style: decimal outside; margin-left: 1.5em;">';
    foreach ($attachments as $attachment) {
        $file = get_attached_file($attachment->ID);
        $filename = pathinfo($file, PATHINFO_BASENAME);

        echo '<li>'.$filename;
        echo ' - <a href="'.admin_url('media.php?action=edit&attachment_id='.$attachment->ID).'">'.__("edit", "gd-bbpress-toolbox").'</a>';
        echo '</li>';
    }
    echo '</ul>';
}
