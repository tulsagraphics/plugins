<div>
    <p>
        <?php _e("This wizard has reach the end. But, there are a lot more features, a lot more settings available to explore. This was just the starting point, to configure few basic things, before you try exploring the plugin and testing the features you want to use.", "gd-bbpress-toolbox"); ?>
    </p>
    <p>
        <?php _e("Settings for the plugin are available on several pages (you can access them from the WordPress navigation menu): Attachments, BBCodes, Topic Views, Modules and Settings. And, you have several pages with list of attachments, attachments upload errors, list of users, reported posts and list of thanks.", "gd-bbpress-toolbox"); ?>
    </p>
</div>
