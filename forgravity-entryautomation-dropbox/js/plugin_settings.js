jQuery( document ).ready( function () {

	// De-Authorize Dropbox.
	jQuery( '#fg_entryautomation_dropbox_deauth_button' ).on( 'click', function ( e ) {

		// Prevent default event.
		e.preventDefault();

		// Set disabled state.
		jQuery( this ).attr( 'disabled', 'disabled' );

		// De-Authorize.
		jQuery.ajax( {
			async:    false,
			url:      ajaxurl,
			dataType: 'json',
			data:     { action: 'fg_entryautomation_dropbox_deauthorize' },
			success:  function ( response ) {

				if ( response.success ) {
					window.location.reload();
				} else {
					alert( response.data.message );
				}

				jQuery( this ).removeAttr( 'disabled' );

			}
		} );

	} );


} );