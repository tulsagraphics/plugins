var FGEntryAutomationDropbox = function () {

	var self = this,
		$ = jQuery;

	self.init = function () {

		// Define strings.
		self.strings = forgravity_entryautomation_dropbox_feed_settings_strings;

		// Define element and input.
		self.$elem = $( '#gaddon-setting-row-dropboxPath' );
		self.$input = $( '#dropboxPath' );

		// Define icon classes.
		self.iconClasses = {
			loading: 'fa fa-spinner fa-spin',
			success: 'fa icon-check fa-check gf_valid',
			error:   'fa icon-remove fa-times gf_invalid',
		};
		// Initialize icons.
		self.initIcons();

		// Bind validation event.
		self.bindValidationEvent();

	}





	// # VALIDATION ----------------------------------------------------------------------------------------------------

	/**
	 * Bind Dropbox folder validation event to input change.
	 *
	 * @since 1.0
	 */
	self.bindValidationEvent = function () {

		self.$input.on( 'blur', self.validatePath );

	}

	/**
	 * Validate Dropbox folder when inputs change.
	 *
	 * @since 1.0
	 */
	self.validatePath = function () {

		// Add spinner icon.
		self.$elem.find( 'td i' ).removeClass().addClass( self.iconClasses.loading );

		$.ajax(
			{
				url:      ajaxurl,
				type:     'POST',
				dataType: 'json',
				data:     {
					action: 'fg_entryautomation_dropbox_validate_path',
					nonce:  self.strings.nonce,
					path:   self.$input.val(),
				},
				success:  function ( response ) {

					// Remove spinner icon.
					self.$elem.find( 'td i' ).removeClass();

					// Add icon class.
					if ( response.success === true ) {
						self.$elem.find( 'td i' ).addClass( self.iconClasses.success );
					} else if ( response.success === false ) {
						self.$elem.find( 'td i' ).addClass( self.iconClasses.error );
					}

				}

			}
		);

	}





	// # ICONS ---------------------------------------------------------------------------------------------------------

	/**
	 * Initialize icon container.
	 *
	 * @since 1.0
	 */
	self.initIcons = function () {

		// If icon container is found, skip.
		if ( self.$elem.find( 'td .fa' ).length > 0 ) {
			return;
		}

		// Add icon container.
		self.$elem.find( 'td' ).append( ' <i class="fa"></i>' );

	}

	self.init();

}

jQuery( document ).ready( function () {
	new FGEntryAutomationDropbox();
} );
