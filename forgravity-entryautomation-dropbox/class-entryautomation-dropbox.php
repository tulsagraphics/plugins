<?php

namespace ForGravity\EntryAutomation\Extensions;

use ForGravity\Entry_Automation;
use ForGravity\EntryAutomation\Extension;

use Exception;
use GFAddOn;
use GFCommon;

use League\Flysystem\Filesystem;
use Spatie\FlysystemDropbox\DropboxAdapter;
use Spatie\Dropbox\Client as DropboxClient;

/**
 * Entry Automation Dropbox Add-On.
 *
 * @since     1.0
 * @author    ForGravity
 * @copyright Copyright (c) 2018, Travis Lopes
 */
class Dropbox extends Extension {

	/**
	 * Contains an instance of this class, if available.
	 *
	 * @since  1.0
	 * @access private
	 * @var    object $_instance If available, contains an instance of this class.
	 */
	private static $_instance = null;

	/**
	 * Defines the version of Entry Automation Dropbox Extension.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_version Contains the version, defined from entryautomation-dropbox.php
	 */
	protected $_version = FG_ENTRYAUTOMATION_DROPBOX_VERSION;

	/**
	 * Defines the plugin slug.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_slug The slug used for this plugin.
	 */
	protected $_slug = 'forgravity-entryautomation-dropbox';

	/**
	 * Defines the main plugin file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_path The path to the main plugin file, relative to the plugins folder.
	 */
	protected $_path = 'forgravity-entryautomation-dropbox/entryautomation-dropbox.php';

	/**
	 * Defines the full path to this class file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_full_path The full path.
	 */
	protected $_full_path = __FILE__;

	/**
	 * Defines the title of this Extension.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_title The title of the Extension.
	 */
	protected $_title = 'Entry Automation Dropbox Extension';

	/**
	 * Defines the short title of the Extension.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_short_title The short title.
	 */
	protected $_short_title = 'Dropbox Extension';

	/**
	 * Contains an instance of the Dropbox client, if available.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    DropboxClient $api If available, contains an instance of the Dropbox Client.
	 */
	private $api = null;

	/**
	 * Get instance of this class.
	 *
	 * @since  1.0
	 * @access public
	 * @static
	 *
	 * @return Dropbox
	 */
	public static function get_instance() {

		if ( null === self::$_instance ) {
			self::$_instance = new self;
		}

		return self::$_instance;

	}

	/**
	 * Register needed hooks.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function init() {

		parent::init();

		add_filter( 'fg_entryautomation_plugin_settings', array( $this, 'extension_settings_fields' ) );

		add_filter( 'fg_entryautomation_export_settings_fields', array( $this, 'settings_fields' ), 10, 1 );
		add_action( 'fg_entryautomation_after_export', array( $this, 'upload_file' ), 10, 3 );

	}

	/**
	 * Register needed admin hooks.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function init_admin() {

		add_action( 'admin_init', array( $this, 'handle_auth_response' ) );

		parent::init_admin();

	}

	/**
	 * Add AJAX callbacks for de-authorizing and path validation.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function init_ajax() {

		// Add AJAX callback for de-authorizing with Dropbox.
		add_action( 'wp_ajax_fg_entryautomation_dropbox_deauthorize', array( $this, 'ajax_deauthorize' ) );

		// Add AJAX callback for validating Dropbox folder path.
		add_action( 'wp_ajax_fg_entryautomation_dropbox_validate_path', array( $this, 'ajax_validate_path' ), 10 );

		parent::init_ajax();

	}

	/**
	 * Enqueue admin scripts.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @return array
	 */
	public function scripts() {

		$scripts = array(
			array(
				'handle'  => $this->get_slug() . '_plugin_settings',
				'deps'    => array( 'jquery' ),
				'src'     => $this->get_base_url() . '/js/plugin_settings.js',
				'version' => $this->get_version(),
				'enqueue' => array(
					array(
						'admin_page' => array( 'plugin_settings' ),
						'tab'        => fg_entryautomation()->get_slug(),
					),
				),
			),
			array(
				'handle'  => 'forgravity_entryautomation_dropbox_feed_settings',
				'deps'    => array( 'jquery' ),
				'src'     => $this->get_base_url() . '/js/feed_settings.js',
				'version' => $this->get_version(),
				'enqueue' => array(
					array(
						'admin_page' => array( 'form_settings' ),
						'tab'        => fg_entryautomation()->get_slug(),
					),
				),
				'strings' => array(
					'nonce' => wp_create_nonce( $this->get_slug() ),
				),
			),
		);

		return array_merge( parent::scripts(), $scripts );

	}

	/**
	 * Enqueue folder tree styling.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @return array
	 */
	public function styles() {

		$styles = array(
			array(
				'handle'  => $this->get_slug() . '_plugin_settings',
				'src'     => $this->get_base_url() . '/css/plugin_settings.css',
				'version' => $this->get_version(),
				'enqueue' => array(
					array(
						'admin_page' => array( 'plugin_settings' ),
						'tab'        => fg_entryautomation()->get_slug(),
					),
				),
			),
		);

		return array_merge( parent::styles(), $styles );

	}





	// # EXTENSION SETTINGS --------------------------------------------------------------------------------------------

	/**
	 * Register extension settings.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $settings Entry Automation plugin settings.
	 *
	 * @return array
	 */
	public function extension_settings_fields( $settings ) {

		$settings[] = array(
			'title'  => esc_html__( 'Dropbox Settings', 'forgravity_entryautomation_dropbox' ),
			'class'  => 'fg-entryautomation-dropbox',
			'fields' => array(
				array(
					'name'     => 'dropboxAuthToken',
					'label'    => null,
					'type'     => 'auth_token_button',
					'callback' => array( $this, 'settings_auth_token_button' ),
				),
			),
		);

		return $settings;

	}

	/**
	 * Create Generate Auth Token settings field.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $field Field settings.
	 * @param bool  $echo  Display field. Defaults to true.
	 *
	 * @uses   Dropbox::initialize_api()
	 * @uses   DropboxClient::getAccountInfo()
	 * @uses   GFAddOn::get_plugin_settings()
	 * @uses   GFAddOn::get_slug()
	 *
	 * @return string
	 */
	public function settings_auth_token_button( $field, $echo = true ) {

		// Initialize return HTML.
		$html = '';

		// If Dropbox is authenticated, display de-authorize button.
		if ( $this->initialize_api() ) {

			// Add hidden input storing token.
			$html .= fg_entryautomation()->settings_hidden( $field, false );

			// Get account information.
			$account = $this->api->getAccountInfo();

			$html .= sprintf( '<p>%s</p>', sprintf( esc_html__( 'Authenticated with Dropbox as: %s', 'forgravity_entryautomation_dropbox' ), $account['name']['display_name'] ) );
			$html .= sprintf(
				' <a href="#" class="button" id="fg_entryautomation_dropbox_deauth_button">%s</a>',
				esc_html__( 'De-Authorize Dropbox', 'forgravity_entryautomation_dropbox' )
			);

		} else {

			// Prepare authorization URL.
			$settings_url = urlencode( admin_url( 'admin.php?page=gf_settings&subview=' . fg_entryautomation()->get_slug() ) );
			$auth_url     = add_query_arg( array( 'redirect_to' => $settings_url ), FG_EDD_STORE_URL . '/wp-json/fg/v1/auth/dropbox' );

			$html .= sprintf(
				'<a href="%2$s" class="button" id="fg_entryautomation_dropbox_auth_button">%1$s</a>',
				esc_html__( 'Click here to authenticate with Dropbox.', 'forgravity_entryautomation_dropbox' ),
				$auth_url
			);

		}

		if ( $echo ) {
			echo $html;
		}

		return $html;

	}

	/**
	 * Handle Dropbox authorization response.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   GFAddOn::get_plugin_settings()
	 * @uses   GFAddOn::update_plugin_settings()
	 * @uses   GFCommon::add_error_message()
	 */
	public function handle_auth_response() {

		// If access token is provided, save it.
		if ( rgget( 'dropbox_access_token' ) ) {

			// Get current plugin settings.
			$settings = fg_entryautomation()->get_plugin_settings();

			// Add access token to plugin settings.
			$settings['dropboxAuthToken'] = rgget( 'dropbox_access_token' );

			// Save plugin settings.
			fg_entryautomation()->update_plugin_settings( $settings );

		}

		// If error is provided, display message.
		if ( rgget( 'auth_error' ) ) {

			// Add error message.
			GFCommon::add_error_message( esc_html__( 'Unable to authenticate with Dropbox.', 'forgravity_entryautomation_dropbox' ) );

		}


	}

	/**
	 * Deauthorize with Dropbox.
	 *
	 * @since  2.0
	 * @access public
	 *
	 * @uses   DropboxAPI::getAuthHelper()
	 * @uses   DropboxApp()
	 * @uses   GFAddOn::get_plugin_settings()
	 * @uses   GFAddOn::log_debug()
	 * @uses   GFAddOn::update_plugin_settings()
	 */
	public function ajax_deauthorize() {

		// Load needed libraries.
		require_once 'includes/vendor/autoload.php';

		// Get plugin settings.
		$settings = fg_entryautomation()->get_plugin_settings();

		// Setup a new Dropbox App object.
		$dropbox = new DropboxClient( $settings['dropboxAuthToken'] );

		try {

			// Revoke access token.
			$dropbox->revokeToken();

			// Log that we revoked the access token.
			$this->log_debug( __METHOD__ . '(): Access token revoked.' );

			// Remove access token from settings.
			unset( $settings['dropboxAuthToken'] );

			// Save settings.
			fg_entryautomation()->update_plugin_settings( $settings );

			// Return success response.
			wp_send_json_success();

		} catch ( Exception $e ) {

			// Log that we could not revoke the access token.
			$this->log_debug( __METHOD__ . '(): Unable to revoke access token; ' . $e->getMessage() );

			// Return error response.
			wp_send_json_error( array( 'message' => $e->getMessage() ) );

		}

	}





	// # ACTION SETTINGS -----------------------------------------------------------------------------------------------

	/**
	 * Adds Export to Dropbox settings tab.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $fields Entry Automation settings fields.
	 *
	 * @uses   Dropbox::initialize_api()
	 * @uses   Entry_Automation::is_current_section()
	 * @uses   GFAddOn::get_slug()
	 *
	 * @return array
	 */
	public function settings_fields( $fields ) {

		// If API is not initialized, return.
		if ( ! $this->initialize_api() ) {
			return $fields;
		}

		// Add Dropbox section.
		$fields[] = array(
			'id'         => 'section-export-dropbox',
			'class'      => 'entryautomation-feed-section ' . fg_entryautomation()->is_current_section( 'section-export-dropbox', false, true ),
			'tab'        => array(
				'label' => esc_html__( 'Dropbox Settings', 'forgravity_entryautomation_dropbox' ),
				'icon'  => ' fa-dropbox',
			),
			'dependency' => array( 'field' => 'action', 'values' => array( 'export' ) ),
			'fields'     => array(
				array(
					'name'    => 'dropboxEnable',
					'label'   => esc_html__( 'Upload Export File', 'forgravity_entryautomation_dropbox' ),
					'type'    => 'checkbox',
					'onclick' => "jQuery( this ).parents( 'form' ).submit()",
					'choices' => array(
						array(
							'name'  => 'dropboxEnable',
							'label' => esc_html__( 'Upload export file to your Dropbox account', 'forgravity_entryautomation_dropbox' ),
						),
					),
				),
				array(
					'name'              => 'dropboxPath',
					'label'             => esc_html__( 'Destination Folder', 'forgravity_entryautomation_dropbox' ),
					'type'              => 'text',
					'class'             => 'medium',
					'required'          => true,
					'dependency'        => array( 'field' => 'dropboxEnable', 'values' => array( '1' ) ),
					'default_value'     => '/',
					'feedback_callback' => array( $this, 'validate_path' ),
					'tooltip'           => sprintf(
						'<h6>%s</h6>%s',
						esc_html__( 'Destination Folder', 'forgravity_entryautomation_dropbox' ),
						esc_html__( 'Enter the path to the folder the export file will be uploaded to.', 'forgravity_entryautomation_dropbox' )
					),
				),
			),
		);

		return $fields;

	}

	/**
	 * Validate path to Dropbox folder.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param string $path Path to Dropbox folder.
	 *
	 * @uses   Dropbox::get_filesystem()
	 * @uses   Filesystem::getMetadata()
	 *
	 * @return bool|null
	 */
	public function validate_path( $path = '' ) {

		// If path is not defined, return.
		if ( rgblank( $path ) ) {
			return null;
		}

		// If root path, return.
		if ( '/' === $path ) {
			return true;
		}

		// Get Flysystem adapter.
		$filesystem = $this->get_filesystem();

		// If API could not be initialized, return.
		if ( ! $filesystem ) {
			return null;
		}

		try {

			// Get path metadata.
			$metadata = $filesystem->getMetadata( $path );

		} catch ( Exception $e ) {

			return false;

		}

		return 'dir' === rgar( $metadata, 'type' );

	}

	/**
	 * Validate Dropbox folder path via AJAX.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   Dropbox::validate_path()
	 * @uses   GFAddOn::get_slug()
	 */
	public function ajax_validate_path() {

		// Verify nonce.
		if ( ! wp_verify_nonce( rgpost( 'nonce' ), $this->get_slug() ) ) {
			wp_send_json_error();
		}

		// Determine if successful.
		$valid = $this->validate_path( sanitize_text_field( rgpost( 'path' ) ) );

		if ( true === $valid ) {
			wp_send_json_success();
		} else if ( false === $valid ) {
			wp_send_json_error();
		}

	}





	// # ON EXPORT -----------------------------------------------------------------------------------------------------

	/**
	 * Handle uploading file upon export.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array  $task      The current Task meta.
	 * @param array  $form      The current Form object.
	 * @param string $file_path Path to export file.
	 *
	 * @uses   Filesystem::writeStream()
	 * @uses   GFAddOn::log_error()
	 * @uses   Dropbox::get_filesystem()
	 */
	public function upload_file( $task, $form, $file_path ) {

		// If Dropbox is not enabled for task, return.
		if ( ! rgar( $task, 'dropboxEnable' ) ) {
			return;
		}

		// Get Flysystem adapter.
		$filesystem = $this->get_filesystem();

		// If API could not be initialized, return.
		if ( ! $filesystem ) {
			fg_entryautomation()->log_error( __METHOD__ . '(): Unable to upload export file to Dropbox because API could not be initialized.' );
			return;
		}

		// Get file name.
		$file_name = basename( $file_path );

		/**
		 * Modify the Dropbox file path before uploading.
		 *
		 * @since 1.0
		 *
		 * @param string $remote_file_path Destination file path.
		 * @param string $file_name        Export file name.
		 * @param array  $task             Entry Automation Task meta.
		 * @param array  $form             The Form object.
		 */
		$remote_file_path = gf_apply_filters( [
			'fg_entryautomation_dropbox_file_path',
			$form['id'],
			$task['task_id'],
		], trailingslashit( rgar( $task, 'dropboxPath' ) ) . $file_name, $file_name, $task, $form );

		try {

			// Upload file.
			$uploaded = $filesystem->writeStream( $remote_file_path, fopen( $file_path, 'r' ) );

		} catch ( Exception $e ) {

			// Log that file could not be uploaded.
			fg_entryautomation()->log_error( __METHOD__ . '(): Unable to upload export file to Dropbox; ' . $e->getMessage() );

		}

		// Log that file could not be uploaded.
		if ( ! $uploaded ) {
			fg_entryautomation()->log_error( __METHOD__ . '(): Unable to upload export file to Dropbox.' );
		}

	}





	// # HELPER METHODS ------------------------------------------------------------------------------------------------

	/**
	 * Validate Dropbox credentials.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   DropboxClient::getAccountInfo()
	 * @uses   GFAddOn::log_debug()
	 * @uses   GFAddOn::log_error()
	 * @uses   GFAddOn::get_plugin_settings()
	 * @uses   GFAddOn::get_posted_settings()
	 * @uses   GFAddOn::is_plugin_settings()
	 * @uses   GFAddOn::is_postback()
	 *
	 * @return bool|null
	 */
	public function initialize_api() {

		// If API is already initialized, return.
		if ( ! is_null( $this->api ) ) {
			return true;
		}

		// Load needed libraries.
		require_once 'includes/vendor/autoload.php';

		// Get the plugin settings.
		$settings = $this->is_plugin_settings( 'entryautomation' ) && $this->is_postback() ? $this->get_posted_settings() : fg_entryautomation()->get_plugin_settings();

		// If authorization token is not provided, return.
		if ( ! rgar( $settings, 'dropboxAuthToken' ) ) {
			return null;
		}

		// Log that we are validating API credentials.
		$this->log_debug( __METHOD__ . '(): Validating API info.' );

		try {

			// Initialize new Dropbox client.
			$dropbox = new DropboxClient( $settings['dropboxAuthToken'] );

			// Try getting account information.
			$dropbox->getAccountInfo();

			// Log that credentials are valid.
			$this->log_debug( __METHOD__ . '(): API credentials are valid.' );

			// Assign Dropbox client to the class.
			$this->api = $dropbox;

			return true;

		} catch ( Exception $e ) {

			// Log that credentials are invalid.
			$this->log_error( __METHOD__ . '(): API credentials are invalid; ' . $e->getMessage() );

		}

		return false;

	}

	/**
	 * Get Flysystem adapter.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   Dropbox::initialize_api()
	 *
	 * @return bool|Filesystem
	 */
	public function get_filesystem() {

		// If API could not be initialized, return.
		if ( ! $this->initialize_api() ) {
			return false;
		}

		// Initialize Flysystem.
		$adapter    = new DropboxAdapter( $this->api );
		$filesystem = new Filesystem( $adapter );

		return $filesystem;

	}

}
