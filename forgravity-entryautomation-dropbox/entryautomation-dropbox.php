<?php
/**
 * Plugin Name: Entry Automation Dropbox Extension
 * Plugin URI: http://forgravity.com/plugins/entry-automation/
 * Description: Upload your Entry Automation export files to your Dropbox account
 * Version: 1.0
 * Author: ForGravity
 * Author URI: http://forgravity.com
 * Text Domain: forgravity_entryautomation_dropbox
 * Domain Path: /languages
 **/

if ( ! defined( 'FG_EDD_STORE_URL' ) ) {
	define( 'FG_EDD_STORE_URL', 'https://forgravity.com' );
}

define( 'FG_ENTRYAUTOMATION_DROPBOX_VERSION', '1.0' );
define( 'FG_ENTRYAUTOMATION_DROPBOX_EDD_ITEM_ID', 3320 );

// Initialize plugin updater.
add_action( 'init', array( 'EntryAutomation_DROPBOX_Bootstrap', 'updater' ), 0 );

// If Gravity Forms is loaded, bootstrap the Entry Automation Dropbox Add-On.
add_action( 'gform_loaded', array( 'EntryAutomation_Dropbox_Bootstrap', 'load' ), 6 );

/**
 * Class EntryAutomation_Dropbox_Bootstrap
 *
 * Handles the loading of the Entry Automation Dropbox Extension.
 */
class EntryAutomation_Dropbox_Bootstrap {

	/**
	 * If Entry Automation exists, Entry Automation Dropbox Extension is loaded.
	 *
	 * @access public
	 * @static
	 */
	public static function load() {

		if ( ! function_exists( 'fg_entryautomation' ) ) {
			return;
		}

		if ( ! class_exists( '\ForGravity\EntryAutomation\EDD_SL_Plugin_Updater' ) ) {
			require_once( fg_entryautomation()->get_base_path() . '/includes/EDD_SL_Plugin_Updater.php' );
		}

		require_once( 'class-entryautomation-dropbox.php' );

		fg_entryautomation_dropbox();

	}

	/**
	 * Initialize plugin updater.
	 *
	 * @access public
	 * @static
	 */
	public static function updater() {

		// Get Entry Automation instance.
		$entry_automation = function_exists( 'fg_entryautomation' ) ? fg_entryautomation() : false;

		// If Entry Automation could not be retrieved, exit.
		if ( ! $entry_automation ) {
			return;
		}

		// Get license key.
		$license_key = fg_entryautomation()->get_license_key();

		new ForGravity\EntryAutomation\EDD_SL_Plugin_Updater(
			FG_EDD_STORE_URL,
			__FILE__,
			array(
				'version' => FG_ENTRYAUTOMATION_DROPBOX_VERSION,
				'license' => $license_key,
				'item_id' => FG_ENTRYAUTOMATION_DROPBOX_EDD_ITEM_ID,
				'author'  => 'ForGravity',
			)
		);

	}
}

/**
 * Returns an instance of the Dropbox class
 *
 * @see    Dropbox::get_instance()
 *
 * @return bool|ForGravity\EntryAutomation\Extensions\Dropbox
 */
function fg_entryautomation_dropbox() {
	return class_exists( '\ForGravity\EntryAutomation\Extensions\Dropbox' ) ? ForGravity\EntryAutomation\Extensions\Dropbox::get_instance() : false;
}
