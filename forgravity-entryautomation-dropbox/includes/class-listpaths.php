<?php

namespace ForGravity\EntryAutomation\Extensions\Dropbox;

use League\Flysystem\Plugin\AbstractPlugin;

class ListPaths extends AbstractPlugin {

	/**
	 * Get the method name.
	 *
	 * @return string
	 */
	public function getMethod() {

		return 'listPaths';

	}

	/**
	 * List all paths.
	 *
	 * @param string $directory
	 * @param bool   $recursive
	 *
	 * @return array paths
	 */
	public function handle( $directory = '', $recursive = false ) {

		$result   = [];
		$contents = $this->filesystem->listContents( $directory, $recursive );

		foreach ( $contents as $object ) {

			if ( $object['type'] !== 'dir' || isset( $object['extension'] ) ) {
				continue;
			}

			$result[] = $object['path'];
		}

		return $result;

	}

}
