=== Premium Addons PRO ===
Contributors: leap13
Tags: Elementor, Elementor Page Builder, Elements, Elementor Addons, Add-ons, page builder
Requires at least: 4.5
Tested up to: 4.9.8
Requires PHP: 5.4
Stable tag: 1.1.2
License: GPL v3.0
License URI: https://opensource.org/licenses/GPL-3.0

This is the PRO extension of Premium Addons for Elementor Plugin that includes Pro widgets and Addons for Elementor Page Builder.

== Description ==
This plugin is an extension to Premium Addons for Elementor Plugin that includes Pro Elementor widgets and Addons for Elementor Page Builder. Visit [Premium Addons Website](https://www.premiumaddons.com/) to check widgets and addons demos.

Premium Addons PRO plugin’s widgets are cross browser compatible and also fully responsive, Your website will rock on all browsers as well as tables and mobile devices.

When using Premium Addons’ widgets you will notice that it has more customization options than any other widget in other plugin.

Premium Addons PRO can be used only as a complement of Elementor page builder plugin as it’s not a standalone plugin.


### Features

* Fully Customizable Elements.
* Options panel for enabling desired elements only for faster performance.
* Free Support through online forums.


### Available Elements

1. [Premium Alert Box](https://premiumaddons.com/alert-box-widget-for-elementor-page-builder/)
2. [Premium Animated Section Gradient](https://premiumaddons.com/animated-section-gradients-for-elementor-page-builder/)
3. [Premium Behance Feed](https://premiumaddons.com/behance-feed-widget-for-elementor-page-builder/)
4. [Premium Charts](https://premiumaddons.com/charts-widget-for-elementor-page-builder/)
5. [Premium Content Switcher](https://premiumaddons.com/content-switcher-widget-for-elementor-page-builder/)
6. [Premium Divider](https://premiumaddons.com/dual-header-widget-for-elementor-page-builder/)
7. [Premium Facebook Messenger](https://premiumaddons.com/facebook-messenger-widget-for-elementor-page-builder/)
8. [Premium Facebook Reviews](https://premiumaddons.com/facebook-reviews-widget-for-elementor-page-builder/)
9. [Premium Flip Box](https://premiumaddons.com/flip-box-widget-for-elementor-page-builder/)
10. [Premium Google Reviews](https://premiumaddons.com/google-reviews-for-elementor-page-builder/)
11. [Premium Icon Box](https://premiumaddons.com/icon-box-widget-for-elementor-page-builder/)
12. [Premium iHover](https://premiumaddons.com/ihover-widget-for-elementor-page-builder/)
13. [Premium Image Comparison](https://premiumaddons.com/image-comparison-widget-for-elementor-page-builder/)
14. [Premium Image Hotspots](https://premiumaddons.com/image-hotspots-widget-for-elementor-page-builder/)
15. [Premium Image Layers](https://premiumaddons.com/image-layers-widget-for-elementor-page-builder/)
16. [Premium Instagram Feed](https://premiumaddons.com/instagram-feed-widget-for-elementor-page-builder/)
17. [Premium Ken Burns](https://premiumaddons.com/ken-burns-section-addon-for-elementor-page-builder/)
18. [Premium Magic Section](https://premiumaddons.com/magic-section-widget-for-elementor-page-builder/)
19. [Premium Parallax](https://premiumaddons.com/parallax-section-addon-for-elementor-page-builder/)
20. [Premium Particles](https://premiumaddons.com/particles-section-addon-for-elementor-page-builder/)
21. [Premium Preview Window](https://premiumaddons.com/preview-window-widget-for-elementor-page-builder/)
22. [Premium Tables](https://premiumaddons.com/table-widget-for-elementor-page-builder/)
23. [Premium Twitter Feed](https://premiumaddons.com/twitter-feed-widget-for-elementor-page-builder/)
24. [Premium Tabs](https://premiumaddons.com/tabs-widget-for-elementor-page-builder-2/)
25. [Premium Unfold](https://premiumaddons.com/unfold-widget-for-elementor-page-builder/)
26. [Premium WhatsApp Chat](https://premiumaddons.com/whatsapp-widget-for-elementor-page-builder/)

== Installation ==

= Minimum Requirements =

* WordPress 4.5 or greater
* PHP version 5.4 or greater
* MySQL version 5.0 or greater

= Installation Instructions =

- First make sure that Elementor Page Builder and Premium Addons for Elementor are installed, As this plugin works only with both of them.
- Download the plugin then Upload it to the plugin folder: /wp-content/plugins/ or install it through the WordPress plugins screen directly.
- Activate the plugin through the ‘Plugins’ screen in WordPress
- You can find Premium Addons Elements under the category “Premium Addons” on your Elementor element/widget list.

== Frequently Asked Questions ==
= Is this a standalone Plugin? =

No. You cannot use Premium Addons PRO without Elementor and Premium Addons for Elementor.

= Does it work with any WordPress theme? =

Yes, it will work with any WordPress theme as long as you are using Elementor as a page builder.

= Will this plugin slow down my website speed? =

Premium Addons for Elementor is light weight and we also gave you the control to enable only the elements you actually use on your website for faster performance.

== Changelog ==

= 1.1.2 = 

- Tweak: Added convert values to percentage in Premium Charts widget.
- Fixed: Call to a member function get_error_message() on array after license activation.

= 1.1.1 = 

- Tweak: Plugin core enhancement for faster performance.
- Tweak: Premium Divider widget performance enhanced.
- Tweak: Premium Flip Box widget performance enhanced.
- Tweak: Premium Icon Box widget performance enhanced.
- Tweak: Premium Image Layers widget performance enhanced.
- Tweak: Premium iHover widget performance enhanced.

= 1.1.0 = 

- Fixed: Tabs issue on IE browser.
- Fixed: Group Control issue in Premium Facebook/Twitter widgets.

= 1.0.9 = 

- Fixed: Warning "Invalid argument supplied for foreach() in /premium-addons-pro/widgets/premium-tables.php on line 1567".

= 1.0.8 =

- Fixed: License not Valid after trying to activate license.

= 1.0.7 =

- New: Added Whatsapp Chat widget.

= 1.0.6 =

- Fixed: Image size issue in Premium Parallax addon.
- Fixed: Transparent images grey background issue.
- Fixed: Invisible charts issue.

= 1.0.5 =

- Tweak: App ID and App secret are changed to Access Token in Facebook Feed widget.

= 1.0.4 =

- Tweak: Animations starts on scrolling in Premium Charts widget.

= 1.0.3 =

- New: Added Facebook Feed element.

= 1.0.2 =

- Tweak: Added Background Size/Position options in Premium Section Parallax.
- Fixed: Instagram Feed grid issue when Masonry is disabled.
- Fixed: Fatal error: Can't use function return value in write context for PHP versions below 5.5

= 1.0.1 =

- Tweak: Added URLs option to each dataset in Premium Charts widget.
- Tweak: Added image fill option in Premium Ken Burns.

= 1.0.0 =

- Initial stable release.