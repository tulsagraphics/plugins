function _rplg_lang() {
    var n = navigator;
    return (n.language || n.systemLanguage || n.userLanguage ||  'en').substr(0, 2).toLowerCase();
}

document.addEventListener('DOMContentLoaded', function() {
    var reviewTimes = document.querySelectorAll('.wpac [data-time]');
    for (var i = 0; i < reviewTimes.length; i++) {
        var clss = reviewTimes[i].className, time;
        if (clss.indexOf('google') > -1) {
            time = parseInt(reviewTimes[i].getAttribute('data-time'));
            time *= 1000;
        } else if (clss.indexOf('facebook') > -1) {
            time = new Date(reviewTimes[i].getAttribute('data-time').replace(/\+\d+$/, '') + '.000Z').getTime();
        } else {
            time = new Date(reviewTimes[i].getAttribute('data-time').replace(/ /, 'T')).getTime();
        }
        reviewTimes[i].innerHTML = WPacTime.getTime(time, _rplg_lang(), 'ago');
    }
});
