(function($){

  $.fn.twentytwenty = function(options) {
    var options = $.extend({
      default_offset_pct: 0.5,
      orientation: 'horizontal',
      switch_before_label: true,
      before_label: 'Before',
      switch_after_label: true,
      after_label: 'After',
      no_overlay: false,
      move_slider_on_hover: false,
      move_with_handle_only: true,
      click_to_move: false,
      show_drag: true,
      show_sep: true,
      horbeforePos: 'middle',
      horafterPos: 'middle',
      verbeforePos: 'center',
      verafterPos: 'center'
    }, options);

    return this.each(function() {

      var sliderPct = options.default_offset_pct;
      var container = $(this);
      var sliderOrientation = options.orientation;
      var beforeDirection = (sliderOrientation === 'vertical') ? 'down' : 'left';
      var afterDirection = (sliderOrientation === 'vertical') ? 'up' : 'right';


      container.wrap("<div class='premium-twentytwenty-wrapper premium-twentytwenty-" + sliderOrientation + "'></div>");
      if(!options.no_overlay) {
        container.append("<div class='premium-twentytwenty-overlay premium-twentytwenty-show'></div>");
      } else {
        container.append("<div class='premium-twentytwenty-overlay premium-twentytwenty-hide'></div>");
      }
      var beforeImg = container.find("img:first");
      var afterImg = container.find("img:last");
      
        if(options.show_sep){
            container.append("<div class='premium-twentytwenty-handle'></div>");
        } else {
            container.append("<div class='premium-twentytwenty-handle premium-twentytwenty-hide'></div>");
        }
        var slider = container.find(".premium-twentytwenty-handle");
        slider.append("<span class='premium-twentytwenty-" + beforeDirection + "-arrow'></span>");
        slider.append("<span class='premium-twentytwenty-" + afterDirection + "-arrow'></span>");
        container.addClass("premium-twentytwenty-container");
        beforeImg.addClass("premium-twentytwenty-before");
        afterImg.addClass("premium-twentytwenty-after");
        if(!options.show_drag) {
            slider.css("opacity","0");
      }
      

      var overlay = container.find(".premium-twentytwenty-overlay");
      if(options.switch_before_label){
          var beforeLab = "<div class='premium-twentytwenty-before-label premium-twentytwenty-before-label-"+options.horbeforePos + " premium-twentytwenty-before-label-"+options.verbeforePos+"'><span>"+options.before_label+"</span></div>";
          overlay.append(beforeLab);
      }
      if(options.switch_after_label){
          var afterLab = "<div class='premium-twentytwenty-after-label  premium-twentytwenty-after-label-"+options.horafterPos + " premium-twentytwenty-after-label-"+options.verafterPos+"'><span>"+options.after_label+"</span></div>";
        overlay.append(afterLab);
    }

      var calcOffset = function(dimensionPct) {
        var w = beforeImg.width();
        var h = beforeImg.height();
        return {
          w: w+"px",
          h: h+"px",
          cw: (dimensionPct*w)+"px",
          ch: (dimensionPct*h)+"px"
        };
      };

      var adjustContainer = function(offset) {
      	if (sliderOrientation === 'vertical') {
          beforeImg.css("clip", "rect(0,"+offset.w+","+offset.ch+",0)");
          afterImg.css("clip", "rect("+offset.ch+","+offset.w+","+offset.h+",0)");
      	}
      	else {
          beforeImg.css("clip", "rect(0,"+offset.cw+","+offset.h+",0)");
          afterImg.css("clip", "rect(0,"+offset.w+","+offset.h+","+offset.cw+")");
    	}
        container.css("height", offset.h);
      };

      var adjustSlider = function(pct) {
        var offset = calcOffset(pct);
        var labelBefore = $(container).find(".premium-twentytwenty-before-label"),
            labelAfter  = $(container).find(".premium-twentytwenty-after-label");
        
        slider.css((sliderOrientation==="vertical") ? "top" : "left", (sliderOrientation==="vertical") ? offset.ch : offset.cw);
        if(sliderOrientation==="horizontal"){
            var labelBeforeOff = ( 'undefined' !== typeof ( labelBefore.css("left") ) ) ? parseInt(labelBefore.css("left").replace(/px/,'')) : '',
                labelBeforeWidth = parseInt(labelBefore.outerWidth());
            var labelAfterOff = ( 'undefined' !== typeof ( labelAfter.css("left") ) ) ? parseInt(labelAfter.css("left").replace(/px/,'')): '',
                sliderOff = parseInt(slider.css("left").replace(/px/,''));
            if(sliderOff < (labelBeforeOff + labelBeforeWidth) ){
                labelBefore.addClass("premium-label-hidden");
            } else {
                labelBefore.removeClass("premium-label-hidden");
            }
            if(sliderOff > labelAfterOff ){
                labelAfter.addClass("premium-label-hidden");
            } else {
                labelAfter.removeClass("premium-label-hidden");
            }
        } else {
            var labelBeforeOff = parseInt(labelBefore.css("top").replace(/px/,'')),
                labelBeforeWidth = parseInt(labelBefore.outerHeight()),
                labelAfterOff = parseInt(labelAfter.css("top").replace(/px/,'')),
                sliderOff = parseInt(slider.css("top").replace(/px/,''));
            if(sliderOff < (labelBeforeOff + labelBeforeWidth) ){
                labelBefore.addClass("premium-label-hidden");
            } else {
                labelBefore.removeClass("premium-label-hidden");
            }
            if(sliderOff > labelAfterOff && !( labelAfterOff < 0) ){
                labelAfter.addClass("premium-label-hidden");
            } else {
                labelAfter.removeClass("premium-label-hidden");
            }
            
        }
        
        adjustContainer(offset);
      };

      // Return the number specified or the min/max number if it outside the range given.
      var minMaxNumber = function(num, min, max) {
        return Math.max(min, Math.min(max, num));
      };

      // Calculate the slider percentage based on the position.
      var getSliderPercentage = function(positionX, positionY) {
        var sliderPercentage = (sliderOrientation === 'vertical') ?
          (positionY-offsetY)/imgHeight :
          (positionX-offsetX)/imgWidth;

        return minMaxNumber(sliderPercentage, 0, 1);
      };


      $(window).on("resize.twentytwenty", function(e) {
        adjustSlider(sliderPct);
      });

      var offsetX = 0;
      var offsetY = 0;
      var imgWidth = 0;
      var imgHeight = 0;
      var onMoveStart = function(e) {
        if (((e.distX > e.distY && e.distX < -e.distY) || (e.distX < e.distY && e.distX > -e.distY)) && sliderOrientation !== 'vertical') {
          e.preventDefault();
        }
        else if (((e.distX < e.distY && e.distX < -e.distY) || (e.distX > e.distY && e.distX > -e.distY)) && sliderOrientation === 'vertical') {
          e.preventDefault();
        }
        container.addClass("active");
        offsetX = container.offset().left;
        offsetY = container.offset().top;
        imgWidth = beforeImg.width(); 
        imgHeight = beforeImg.height();          
      };
      var onMove = function(e) {
        if (container.hasClass("active")) {
          sliderPct = getSliderPercentage(e.pageX, e.pageY);
          adjustSlider(sliderPct);
        }
      };
      var onMoveEnd = function() {
        
        container.removeClass("active");
      };

      var moveTarget = options.move_with_handle_only ? slider : container;
      moveTarget.on("movestart",onMoveStart);
      moveTarget.on("move",onMove);
      moveTarget.on("moveend",onMoveEnd);

      if (options.move_slider_on_hover) {
        container.on("mouseenter", onMoveStart);
        container.on("mousemove", onMove);
        container.on("mouseleave", onMoveEnd);
      }

      slider.on("touchmove", function(e) {
        e.preventDefault();
      });

      container.find("img").on("mousedown", function(event) {
        event.preventDefault();
      });

      if (options.click_to_move) {
        container.on('click', function(e) {
          offsetX = container.offset().left;
          offsetY = container.offset().top;
          imgWidth = beforeImg.width();
          imgHeight = beforeImg.height();

          sliderPct = getSliderPercentage(e.pageX, e.pageY);
          adjustSlider(sliderPct);
        });
      }

      $(window).trigger("resize.twentytwenty");
    });
  };

})(jQuery);