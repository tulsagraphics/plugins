<?php

namespace Elementor;

if(!defined('ABSPATH')) exit;

class Premium_Tables extends Widget_Base {
    
    public  function get_name(){
         return 'premium-tables-addon';
     }
     
    public function get_title() {
		return \PremiumAddons\Helper_Functions::get_prefix() . ' Table';
	}
    
    public function get_icon(){
        return 'pa-pro-table';
    }
    
    public function get_categories() {
        return ['premium-elements'];
    }
    
    public function is_reload_preview_required(){
        return true;
    }
    
    public function get_script_depends(){
        return [
            'table-sorter',
            'table-search',
            'premium-pro-js'
            ];
    }
    
    public function getTemplateInstance() {
		return $this->templateInstance = premium_Template_Tags::getInstance();
	}
    
    protected function get_repeater_controls($repeater, $condition = [] ){
        
        $repeater->add_control('premium_table_text',
            [
                'label'         => esc_html__('Text', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'dynamic'       => [ 'active' => true ],
                'placeholder'   => 'Text',
                'condition'     => array_merge( $condition, [] ),
            ]
        );

        $repeater->add_control('premium_table_icon_selector',
            [
                'label'        => esc_html__('Icon Type','premium-addons-for-elementor'),
                'type'         => Controls_Manager::SELECT,
                'default'      => 'font-awesome-icon',
                'options'      =>[
                    'font-awesome-icon' => esc_html__('Font Awesome Icon','premium-addons-for-elementor'),
                    'custom-image'     => esc_html__('Custom Image','premium-addons-for-elementor'),
                    ],
                'condition'     => array_merge( $condition, [] ),
                ]
            );
        
        $repeater->add_control('premium_table_cell_icon',
                [
                    'label'         => esc_html__('Icon', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::ICON,
                    'label_block'   => true,
                    'condition'     => array_merge( $condition, [
                        'premium_table_icon_selector'   => 'font-awesome-icon'
                    ] ),
                ]
                );

        $repeater->add_control('premium_table_cell_icon_img',
            [
                'label'        => esc_html__('Custom Image','premium-addons-for-elementor'),
                'type'         => Controls_Manager::MEDIA,
                'condition'     => array_merge( $condition, [
                        'premium_table_icon_selector'   => 'custom-image'
                    ] ),
                ]
            );
        
        $repeater->add_control('premium_table_cell_icon_align', 
                [
                    'label'         => esc_html__('Icon Position', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SELECT,
                    'default'       => 'before',
                    'options'       => [
                        'top'           => esc_html__('Top', 'premium-addons-for-elementor'),
                        'before'        => esc_html__('Before', 'premium-addons-for-elementor'),
                        'after'         => esc_html__('After', 'premium-addons-for-elementor'),
                        ],
                    'condition'	=> array_merge( $condition, [] ),
                    'label_block'   => true,
                    ]
                );
        
        $repeater->add_control('premium_table_cell_icon_spacing',
                [
                    'label'         => esc_html__('Icon Spacing', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'condition'	=> array_merge( $condition, [] ),
                    'selectors'     => [
                        '{{WRAPPER}} {{CURRENT_ITEM}} .premium-table-text .premium-table-cell-icon-before'   => 'margin-right: {{SIZE}}px',
                        '{{WRAPPER}} {{CURRENT_ITEM}} .premium-table-text .premium-table-cell-icon-after'    => 'margin-left: {{SIZE}}px',
                        '{{WRAPPER}} {{CURRENT_ITEM}} .premium-table-text.premium-table-cell-top .premium-table-cell-icon-top'    => 'margin-bottom: {{SIZE}}px',
                    ],
                    'separator'     => 'below',
                ]
            );
        
        $repeater->add_control('premium_table_cell_row_span',
            [
                'label'         => esc_html__('Row Span', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::NUMBER,
                'title'         => esc_html__('Enter the number of rows for the cell', 'premium-addons-for-elementor'),
                'default'       => 1,
                'min'           => 1,
                'max'           => 10,
                'condition'     => array_merge( $condition, [] ),
            ]
        );
        
        $repeater->add_control('premium_table_cell_span',
            [
                'label'         => esc_html__('Column Span', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::NUMBER,
                'title'         => esc_html__('Enter the number of columns for the cell', 'premium-addons-for-elementor'),
                'default'       => 1,
                'min'           => 1,
                'max'           => 10,
                'condition'     => array_merge( $condition, [] ),
            ]
        );
        
        $repeater->add_responsive_control('premium_table_cell_align',
                [
                    'label'         => esc_html__( 'Alignment', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'left'      => [
                            'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-left',
                            ],
                        'center'    => [
                            'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-center',
                            ],
                        'right'     => [
                            'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-right',
                            ],
                        ],
                    'default'       => 'left',
                    'selectors'     => [
                        '{{WRAPPER}} {{CURRENT_ITEM}} .premium-table-text' => 'justify-content: {{VALUE}};',
                        ],
                    'condition'     => array_merge( $condition, [
                        'premium_table_cell_icon_align!' => 'top'
                    ] ),
                    ]
                );
        
        $repeater->add_responsive_control('premium_table_cell_top_align',
                [
                    'label'         => esc_html__( 'Alignment', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'flex-start'      => [
                            'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-left',
                            ],
                        'center'    => [
                            'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-center',
                            ],
                        'flex-end'     => [
                            'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-right',
                            ],
                        ],
                    'default'       => 'left',
                    'selectors'     => [
                        '{{WRAPPER}} {{CURRENT_ITEM}} .premium-table-cell-top' => 'align-items: {{VALUE}};',
                        ],
                    'condition'     => array_merge( $condition, [
                        'premium_table_cell_icon_align' => 'top'
                    ] ),
                    ]
                );
        
    }
    
    protected function _register_controls(){
        
        $this->start_controls_section('premium_table_data_section',
            [
                'label'     => esc_html__('Data', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_table_data_type',
            [
                'label'     => esc_html__('Data Type', 'premium-addons-for-elementor'),
                'type'      => Controls_Manager::SELECT,
                'options'   => [
                    'custom'    => esc_html__('Custom', 'premium-addons-for-elementor'),
                    'csv'       => 'CSV' . esc_html__(' File','premium-addons-for-elementor')
                ],
                'default'   => 'custom'
            ]);
        
        $this->add_control('premium_table_csv_type',
            [
                'label'     => esc_html__('File Type', 'premium-addons-for-elementor'),
                'type'      => Controls_Manager::SELECT,
                'options'   => [
                    'file'      => esc_html__('Upload FIle', 'premium-addons-for-elementor'),
                    'url'       => esc_html__('Remote File','premium-addons-for-elementor')
                ],
                'condition' => [
                    'premium_table_data_type'   => 'csv',
                ],
                'default'   => 'file'
            ]);
        
        $this->add_control('premium_table_csv',
            [
                'label'     => esc_html__('Upload CSV File', 'premium-addons-for-elementor'),
                'type'      => Controls_Manager::MEDIA,
                'condition' => [
                    'premium_table_data_type'   => 'csv',
                    'premium_table_csv_type'    => 'file'
                ]
            ]);
        
        $this->add_control('premium_table_csv_url',
            [
                'label'     => esc_html__('File URL', 'premium-addons-for-elementor'),
                'type'      => Controls_Manager::TEXT,
                'label_block'   => true,
                'condition' => [
                    'premium_table_data_type'   => 'csv',
                    'premium_table_csv_type'    => 'url'
                ]
            ]);
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_table_head_section',
            [
                'label'     => esc_html__('Head', 'premium-addons-for-elementor'),
                'condition' => [
                    'premium_table_data_type'   => 'custom'
                ]
            ]
        );
        
        $head_repeater = new Repeater();
        
        $this->get_repeater_controls($head_repeater, array() );
        
        $this->add_control('premium_table_head_repeater',
            [
                'label'         => esc_html__('Cell', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::REPEATER,
                'fields'        => $head_repeater->get_controls(),
                'default'       => [
                    [
                        'premium_table_text'    => esc_html__('First Head', 'premium-addons-for-elementor'),
                    ],
                    [
                        'premium_table_text'    => esc_html__('Second Head', 'premium-addons-for-elementor'),
                    ],
                    [
                        'premium_table_text'    => esc_html__('Third Head', 'premium-addons-for-elementor'),
                    ]
                ],
                'title_field'   =>  '{{{ premium_table_text }}}',
                'prevent_empty' => true
            ]
        );
        
        //Text Align
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_table_body_section',
            [
                'label'     => esc_html__('Body', 'premium-addons-for-elementor'),
                'condition' => [
                    'premium_table_data_type'   => 'custom'
                ]
            ]
        );
        
        $body_repeater = new Repeater();
        
        $body_repeater->add_control('premium_table_elem_type',
            [
                'label'     => esc_html__('Type', 'premium-addons-for-elementor'),
                'type'      => Controls_Manager::SELECT,
                'options'   => [
                    'cell'      => esc_html__('Cell', 'premium-addons-for-elementor'),
                    'row'       => esc_html__('Row', 'premium-addons-for-elementor'),
                ],
                'default'   => 'cell'
            ]
        );
        
        $body_repeater->add_control('premium_table_cell_type',
            [
                'label'     => esc_html__('Cell Type', 'premium-addons-for-elementor'),
                'type'      => Controls_Manager::SELECT,
                'options'   => [
                    'td'        => esc_html__('Body', 'premium-addons-for-elementor'),
                    'th'        => esc_html__('Head', 'premium-addons-for-elementor'),
                ],
                'default'   => 'td',
                'condition'     => [
                    'premium_table_elem_type'   => 'cell'
                ]
            ]
        );
        
        $this->get_repeater_controls( $body_repeater, array( 'premium_table_elem_type' => 'cell' ) );

        $body_repeater->add_control('premium_table_link_switcher',
            [
                'label'         => esc_html__('Link', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'condition'     => [
                    'premium_table_elem_type'   => 'cell'
                ]
            ]
        );

        $body_repeater->add_control('premium_table_link_selection', 
            [
                'label'         => esc_html__('Link Type', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'url'   => esc_html__('URL', 'premium-addons-for-elementor'),
                    'link'  => esc_html__('Existing Page', 'premium-addons-for-elementor'),
                ],
                'default'       => 'url',
                'label_block'   => true,
                'condition'		=> [
                    'premium_table_link_switcher'	=> 'yes',
                    'premium_table_elem_type'       => 'cell'
                    ]
                ]
            );
        
        $body_repeater->add_control('premium_table_link',
            [
                'label'         => esc_html__('Link', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::URL,
                'default'       => [
                    'url'   => '#',
                    ],
                'placeholder'   => 'https://premiumaddons.com/',
                'label_block'   => true,
                'separator'     => 'after',
                'condition'     => [
                    'premium_table_elem_type'       => 'cell',
                    'premium_table_link_switcher'	=> 'yes',
                    'premium_table_link_selection'  => 'url'
                ]
            ]
        );
        
        $body_repeater->add_control('premium_table_existing_link',
            [
                'label'         => esc_html__('Existing Page', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT2,
                'options'       => $this->getTemplateInstance()->get_all_post(),
                'condition'     => [
                    'premium_table_elem_type'       => 'cell',
                    'premium_table_link_switcher'	=> 'yes',
                    'premium_table_link_selection'  => 'link',
                ],
                'multiple'      => false,
                'label_block'   => true,
            ]
        );
        
    $this->add_control('premium_table_body_repeater',
				[
					'label' 	=> __( 'Rows', 'premium-addons-for-elementor' ),
					'type' 		=> Controls_Manager::REPEATER,
					'default' 	=> [
						[
							'premium_table_elem_type'           => 'row',
						],
						[
							'premium_table_elem_type' 			=> 'cell',
							'premium_table_text'                => esc_html__( 'Column #1', 'premium-addons-for-elementor' ),
						],
						[
							'premium_table_elem_type' 			=> 'cell',
							'premium_table_text'                => esc_html__( 'Column #2', 'premium-addons-for-elementor' ),
						],
						[
							'premium_table_elem_type'           => 'cell',
							'premium_table_text'                => esc_html__( 'Column #3', 'premium-addons-for-elementor' ),
						],
						[
							'premium_table_elem_type'           => 'row',
						],
						[
							'premium_table_elem_type'           => 'cell',
							'premium_table_text'                => esc_html__( 'Column #1', 'premium-addons-for-elementor' ),
						],
						[
							'premium_table_elem_type'           => 'cell',
							'premium_table_text'                => esc_html__( 'Column #2', 'premium-addons-for-elementor' ),
						],
						[
							'premium_table_elem_type'           => 'cell',
							'premium_table_text'                => esc_html__( 'Column #3', 'premium-addons-for-elementor' ),
						],
					],
					'fields' 			=> array_values( $body_repeater->get_controls() ),
					'title_field' 		=> '{{ premium_table_elem_type }}: {{{ premium_table_text }}}',
				]
			);
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_table_display', 
            [
                'label'         => esc_html__('Display Options', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_responsive_control('premium_table_width',
                [
                    'label'         => esc_html__('Width', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%'],
                    'range'         => [
                        'px'    => [
                            'min'   => 1,
                            'max'   => 700
                        ]
                    ],
                    'selectors'     => [
                        '{{WRAPPER}} .elementor-widget-container' => 'width: {{SIZE}}{{UNIT}};'
                        ]
                    ]
                );
        
        $this->add_control('premium_table_responsive',
            [
                'label' 		=> esc_html__( 'Responsive', 'premium-addons-for-elementor' ),
                'type' 			=> Controls_Manager::SWITCHER,
                'description'   => esc_html__( 'Enables scroll on mobile.', 'premium-addons-for-elementor' ),
                'frontend_available' => true,
            ]
        );
        
        $this->add_responsive_control('premium_table_align',
                [
                    'label'         => esc_html__( 'Table Alignment', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'flex-start'      => [
                            'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-left',
                            ],
                        'center'    => [
                            'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-center',
                            ],
                        'flex-end'     => [
                            'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-right',
                            ],
                        ],
                    'default'       => 'center',
                    'selectors'     => [
                        '{{WRAPPER}}' => 'justify-content: {{VALUE}};',
                        ],
                    ]
                );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_table_advanced', 
            [
                'label'         => esc_html__('Advanced Settings', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_table_sort',
            [
                'label' 		=> esc_html__( 'Sortable', 'premium-addons-for-elementor' ),
                'type' 			=> Controls_Manager::SWITCHER,
                'description'   => esc_html__( 'Enables sorting with respect to the table heads.', 'premium-addons-for-elementor' ),
                'frontend_available' => true,
                'condition'     => [
                    'premium_table_data_type!'   => 'csv'
                ]
            ]
        );
        
        $this->add_control('premium_table_sort_mob',
            [
                'label'             => esc_html__( 'Sort on Mobile', 'premium-addons-for-elementor' ),
                'type'              => Controls_Manager::SWITCHER,
                'frontend_available'=> true,
                'condition'         => [
                    'premium_table_sort'    => 'yes'
                ]
            ]
        );
        
        $this->add_control('premium_table_search',
            [
                'label' 		=> esc_html__( 'Search', 'premium-addons-for-elementor' ),
                'type' 			=> Controls_Manager::SWITCHER,
                'description'   => esc_html__( 'Enables searching through the table using rows\' first cell keyword.', 'premium-addons-for-elementor' ),
                'frontend_available' => true,
            ]
        );
        
        $this->add_responsive_control('premium_table_search_align',
                [
                    'label'         => esc_html__( 'Search Alignment', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'left'      => [
                            'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-left',
                            ],
                        'center'    => [
                            'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-center',
                            ],
                        'right'     => [
                            'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-right',
                            ],
                        ],
                    'default'       => 'right',
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table-search-wrap' => 'text-align: {{VALUE}};',
                        ],
                    'condition'         => [
                        'premium_table_search'    => 'yes'
                        ]
                    ]
                );
        
        $this->add_control('premium_table_search_dir',
                [
                    'label'         => esc_html__('Direction', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'           => [
                        'ltr'    => [
                            'title' => __( 'LTR', 'premium-addons-for-elementor' ),
                            'icon'  => 'fa fa-arrow-circle-right',
                        ],
                        'rtl' => [
                            'title' => __( 'RTL', 'premium-addons-for-elementor' ),
                            'icon'  => 'fa fa-arrow-circle-left',
                        ],
                    ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table-wrap' => 'direction: {{VALUE}};',
                    ],
                    'default'       => 'ltr',
                    'label_block'   => true,
                ]
                );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_table_head_style', 
            [
                'label'         => esc_html__('Head', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE
            ]
        );
        
        $this->start_controls_tabs('premium_table_head_style_tabs');
        
        $this->start_controls_tab('premium_table_odd_head_odd_style',
                [
                    'label'     => esc_html__('Odd', 'premium-addons-for-elementor'),
                ]
            );
        
        $this->add_control('premium_table_odd_head_color',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(odd) .premium-table-text' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_control('premium_table_odd_head_hover_color',
            [
                'label'         => esc_html__('Text Hover Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(odd) .premium-table-text:hover' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_control('premium_table_odd_head_icon_color',
            [
                'label'         => esc_html__('Icon Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(odd) .premium-table-text i' => 'color: {{VALUE}};'
                ],
                'condition' => [
                    'premium_table_data_type'   => 'custom'
                ]
            ]
        );
        
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_table_odd_head_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(odd) .premium-table-text'
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'name'          => 'premium_table_odd_head_text_shadow',
                    'selector'      => '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(odd) .premium-table-text'
                    ]
                );
        
        $this->add_control('premium_table_odd_head_background_popover',
                [
                    'label'         => esc_html__('Background', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::POPOVER_TOGGLE,
                    ]
                );

        $this->start_popover();
        
        $this->add_control('premium_table_odd_head_background_heading',
            [
                'label'             => esc_html__('Normal', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::HEADING
            ]);
        
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_table_odd_head_background',
                    'types'             => [ 'classic', 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(odd)'
                ]
                );
        
        $this->add_control('premium_table_odd_head_hover_background_heading',
            [
                'label'             => esc_html__('Hover', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::HEADING
            ]);
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_table_odd_head_hover_background',
                    'types'             => [ 'classic', 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(odd):hover'
                ]
                );
        
        $this->end_popover();
        
        $this->add_responsive_control('premium_table_odd_head_align',
                [
                    'label'         => esc_html__( 'Alignment', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'left'      => [
                            'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-left',
                            ],
                        'center'    => [
                            'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-center',
                            ],
                        'right'     => [
                            'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-right',
                            ],
                        ],
                    'default'       => 'left',
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(odd) .premium-table-text' => 'justify-content: {{VALUE}};',
                        ],
                    'condition'     => [
                            'premium_table_data_type' => 'csv'
                        ] ,
                    ]
                );
        
        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_table_head_even_style',
                [
                    'label'     => esc_html__('Even', 'premium-addons-for-elementor'),
                ]
            );
        
        $this->add_control('premium_table_even_head_color',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(even) .premium-table-text' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_control('premium_table_even_head_hover_color',
            [
                'label'         => esc_html__('Text Hover Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(even) .premium-table-text:hover' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_control('premium_table_even_head_icon_color',
            [
                'label'         => esc_html__('Icon Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(even) .premium-table-text i' => 'color: {{VALUE}};'
                ],
                'condition' => [
                    'premium_table_data_type'   => 'custom'
                ]
            ]
        );
        
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_table_even_head_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(even) .premium-table-text'
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'name'          => 'premium_table_even_head_text_shadow',
                    'selector'      => '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(even) .premium-table-text'
                    ]
                );
        
        $this->add_control('premium_table_even_head_background_popover',
                [
                    'label'         => esc_html__('Background', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::POPOVER_TOGGLE,
                    ]
                );

        $this->start_popover();
        
        $this->add_control('premium_table_even_head_background_heading',
            [
                'label'             => esc_html__('Normal', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::HEADING
            ]);
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_table_even_head_background',
                    'types'             => [ 'classic', 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(even)'
                ]
                );
        
        $this->add_control('premium_table_even_head_hover_background_heading',
            [
                'label'             => esc_html__('Hover', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::HEADING
            ]);
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_table_even_head_hover_background',
                    'types'             => [ 'classic', 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(even):hover'
                ]
                );
        
        $this->end_popover();
        
        $this->add_responsive_control('premium_table_even_head_align',
                [
                    'label'         => esc_html__( 'Alignment', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'left'      => [
                            'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-left',
                            ],
                        'center'    => [
                            'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-center',
                            ],
                        'right'     => [
                            'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-right',
                            ],
                        ],
                    'default'       => 'left',
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell:nth-child(even) .premium-table-text' => 'justify-content: {{VALUE}};',
                        ],
                    'condition'     => [
                            'premium_table_data_type' => 'csv'
                        ] ,
                    ]
                );
        
        $this->end_controls_tab();
        
        $this->end_controls_tabs();
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_table_head_rows_border',
                    'selector'      => '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell',
                    'separator'     => 'before'
                ]
                );
        
        $this->add_responsive_control('premium_table_head_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table .premium-table-row th.premium-table-cell .premium-table-text' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_table_row_style', 
            [
                'label'         => esc_html__('Rows', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE
            ]
        );
        
        $this->start_controls_tabs('premium_table_row_style_tabs');
        
        $this->start_controls_tab('premium_table_odd_row_odd_style',
                [
                    'label'     => esc_html__('Odd', 'premium-addons-for-elementor'),
                ]
            );
        
        $this->add_control('premium_table_odd_row_color',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table tbody tr:nth-of-type(odd) .premium-table-cell .premium-table-text' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_control('premium_table_odd_row_hover_color',
            [
                'label'         => esc_html__('Text Hover Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table tbody tr:nth-of-type(odd) .premium-table-cell .premium-table-text:hover' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_control('premium_table_row_row_icon_color',
            [
                'label'         => esc_html__('Icon Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table tbody tr:nth-of-type(odd) .premium-table-cell .premium-table-text i' => 'color: {{VALUE}};'
                ],
                'condition' => [
                    'premium_table_data_type'   => 'custom'
                ]
            ]
        );
        
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_table_odd_row_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-table tbody tr:nth-of-type(odd) .premium-table-cell .premium-table-text'
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'name'          => 'premium_table_odd_row_text_shadow',
                    'selector'      => '{{WRAPPER}} .premium-table tbody tr:nth-of-type(odd) .premium-table-cell .premium-table-text'
                    ]
                );
        
        $this->add_control('premium_table_odd_row_background_popover',
                [
                    'label'         => esc_html__('Background', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::POPOVER_TOGGLE,
                    ]
                );

        $this->start_popover();
        
        $this->add_control('premium_table_odd_row_background_heading',
            [
                'label'             => esc_html__('Normal', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::HEADING
            ]);
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_table_odd_row_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-table tbody tr:nth-of-type(odd) .premium-table-cell'
                    ]
                );
        
        $this->add_control('premium_table_odd_row_hover_background_heading',
            [
                'label'             => esc_html__('Hover', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::HEADING
            ]);
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_table_odd_row_hover_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-table tbody tr:nth-of-type(odd) .premium-table-cell:hover'
                    ]
                );
        
        $this->end_popover();
        
        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_table_row_even_style',
                [
                    'label'     => esc_html__('Even', 'premium-addons-for-elementor'),
                ]
            );
        
        $this->add_control('premium_table_even_row_color',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table tbody tr:nth-of-type(even) .premium-table-cell .premium-table-text' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_control('premium_table_even_row_hover_color',
            [
                'label'         => esc_html__('Text Hover Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table tbody tr:nth-of-type(even) .premium-table-cell .premium-table-text:hover' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_control('premium_table_row_even_icon_color',
            [
                'label'         => esc_html__('Icon Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table tbody tr:nth-of-type(even) .premium-table-cell .premium-table-text i' => 'color: {{VALUE}};'
                ],
                'condition' => [
                    'premium_table_data_type'   => 'custom'
                ]
            ]
        );
        
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_table_even_row_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-table tbody tr:nth-of-type(even) .premium-table-cell .premium-table-text'
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'name'          => 'premium_table_even_row_text_shadow',
                    'selector'      => '{{WRAPPER}} .premium-table tbody tr:nth-of-type(even) .premium-table-cell .premium-table-text'
                    ]
                );
        
        $this->add_control('premium_table_even_row_background_popover',
                [
                    'label'         => esc_html__('Background', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::POPOVER_TOGGLE,
                    ]
                );

        $this->start_popover();
        
        $this->add_control('premium_table_even_row_background_heading',
            [
                'label'             => esc_html__('Normal', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::HEADING
            ]);
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_table_even_row_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-table tbody tr:nth-of-type(even) .premium-table-cell'
                    ]
                );
        
        $this->add_control('premium_table_even_row_hover_background_heading',
            [
                'label'             => esc_html__('Hover', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::HEADING
            ]);
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_table_even_row_hover_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-table tbody tr:nth-of-type(even) .premium-table-cell:hover'
                    ]
                );
        
        $this->end_popover();
                
        $this->end_controls_tab();
        
        $this->end_controls_tabs();

        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_table_row_border',
                    'separator'		=> 'before',
                    'selector'      => '{{WRAPPER}} .premium-table .premium-table-row td.premium-table-cell'
                ]
                );
        
        $this->add_responsive_control('premium_table_odd_row_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table .premium-table-row td.premium-table-cell .premium-table-text' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]      
        );
         
       
        $this->end_controls_section();

        $this->start_controls_section('premium_table_col_style', 
            [
                'label'         => esc_html__('Columns', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE
            ]
        );
        
        $this->start_controls_tabs('premium_table_col_style_tabs');
        
        $this->start_controls_tab('premium_table_odd_col_odd_style',
                [
                    'label'     => esc_html__('Odd', 'premium-addons-for-elementor'),
                ]
            );
        
        $this->add_control('premium_table_odd_col_background_popover',
                [
                    'label'         => esc_html__('Background', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::POPOVER_TOGGLE,
                    ]
                );

        $this->start_popover();
        
        $this->add_control('premium_table_odd_col_background_heading',
            [
                'label'             => esc_html__('Normal', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::HEADING
            ]);
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_table_odd_col_background',
                    'types'             => [ 'classic', 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-table .premium-table-row .premium-table-cell:nth-child(odd)'
                ]
                );
        
        
        $this->add_control('premium_table_odd_col_hover_background_heading',
            [
                'label'             => esc_html__('Hover', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::HEADING
            ]);
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_table_odd_col_hover_background',
                    'types'             => [ 'classic', 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-table .premium-table-row .premium-table-cell:nth-child(odd):hover'
                ]
                );
        
        $this->end_popover();
        
        $this->add_responsive_control('premium_table_odd_col_align',
                [
                    'label'         => esc_html__( 'Alignment', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'left'      => [
                            'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-left',
                            ],
                        'center'    => [
                            'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-center',
                            ],
                        'right'     => [
                            'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-right',
                            ],
                        ],
                    'default'       => 'left',
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table .premium-table-row .premium-table-cell:nth-child(odd) .premium-table-text' => 'justify-content: {{VALUE}};',
                        ],
                    'condition'     => [
                            'premium_table_data_type' => 'csv'
                        ] ,
                    ]
                );
        
        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_table_col_even_style',
                [
                    'label'     => esc_html__('Even', 'premium-addons-for-elementor'),
                ]
            );
        
        $this->add_control('premium_table_even_col_background_popover',
                [
                    'label'         => esc_html__('Background', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::POPOVER_TOGGLE,
                    ]
                );

        $this->start_popover();
        
        $this->add_control('premium_table_even_col_background_heading',
            [
                'label'             => esc_html__('Normal', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::HEADING
            ]);
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_table_even_col_background',
                    'types'             => [ 'classic', 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-table .premium-table-row .premium-table-cell:nth-child(even)'
                ]
                );
        
        $this->add_control('premium_table_even_col_hover_background_heading',
            [
                'label'             => esc_html__('Hover', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::HEADING
            ]);
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_table_even_col_hover_background',
                    'types'             => [ 'classic', 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-table .premium-table-row .premium-table-cell:nth-child(even):hover'
                ]
                );
        
        $this->end_popover();
        
        $this->add_responsive_control('premium_table_even_col_align',
                [
                    'label'         => esc_html__( 'Alignment', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'left'      => [
                            'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-left',
                            ],
                        'center'    => [
                            'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-center',
                            ],
                        'right'     => [
                            'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-right',
                            ],
                        ],
                    'default'       => 'left',
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table .premium-table-row td.premium-table-cell:nth-child(even) .premium-table-text' => 'justify-content: {{VALUE}};',
                        ],
                    'condition'     => [
                            'premium_table_data_type' => 'csv'
                        ] ,
                    ]
                );
        
        $this->end_controls_tab();
        
        $this->end_controls_tabs();
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_table_sort_style', 
            [
                'label'         => esc_html__('Sort', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_table_sort'    => 'yes',
                    'premium_table_data_type!'  => 'csv'
                ]
            ]
        );
        
        $this->add_control('premium_table_sort_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table thead .premium-table-cell .premium-table-sort-icon:before' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_control('premium_table_sort_hover_color',
            [
                'label'         => esc_html__('Hover Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table thead .premium-table-cell:hover .premium-table-sort-icon:before' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_control('premium_table_sort_background',
            [
                'label'         => esc_html__('Background', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-table thead .premium-table-cell .premium-table-sort-icon:before' => 'background: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_table_sort_border',
                    'selector'      => '{{WRAPPER}} .premium-table thead .premium-table-cell .premium-table-sort-icon:before'
                ]
                );
        
        $this->add_control('premium_table_sort_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table thead .premium-table-cell .premium-table-sort-icon:before' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'name'          => 'premium_table_sort_box_shadow',
                    'selector'      => '{{WRAPPER}} .premium-table thead .premium-table-cell .premium-table-sort-icon:before'
                    ]
                );

        $this->add_responsive_control('premium_table_sort_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table thead .premium-table-cell .premium-table-sort-icon:before' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_table_search_style', 
            [
                'label'         => esc_html__('Search', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_table_search'    => 'yes'
                ]
            ]
        );
        
        $this->add_control('premium_table_search_width',
                [
                    'label'         => esc_html__('Width', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'range'         => [
                        'px'    => [
                            'min'   => 1,
                            'max'   => 300
                        ],
                        'em'    => [
                            'min'   => 1,
                            'max'   => 20
                        ]
                    ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table-search-field' => 'width: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_table_search_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-table-search-field',
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_table_search_border',
                    'selector'      => '{{WRAPPER}} .premium-table-search-field',
                ]
                );
        
        $this->add_control('premium_table_search_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table-search-field' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'name'          => 'premium_table_container_search_shadow',
                    'selector'      => '{{WRAPPER}} .premium-table-search-field',
                    ]
                );
        
        $this->add_responsive_control('premium_table_search_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table-search-field' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]      
        );

        $this->add_responsive_control('premium_table_search_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table-search-field' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]      
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_table_style', 
            [
                'label'         => esc_html__('Table', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE
            ]
        );
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_table_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-table',
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_table_border',
                    'selector'      => '{{WRAPPER}} .premium-table',
                ]
                );
        
        $this->add_control('premium_table_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-table' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'name'          => 'premium_table_box_shadow',
                    'selector'      => '{{WRAPPER}} .premium-table',
                    ]
                );
        
        $this->end_controls_section();
        
    }
    
    protected function get_table_head(){
        $settings = $this->get_settings_for_display();
        
        
        $head = '';
        
        $this->add_render_attribute('table_head', 'class', 'premium-table-head');
        
        $this->add_render_attribute('table_row', 'class', 'premium-table-row');
        
        ?>

        <thead <?php echo $this->get_render_attribute_string('table_head'); ?>>
            
            <tr <?php echo $this->get_render_attribute_string('table_row'); ?>>
                
                <?php 
                
                if( 'custom' == $settings['premium_table_data_type'] ) {
                    
                    foreach($settings['premium_table_head_repeater'] as $index => $head_cell ) {
                    
                        $head_cell_text = $this->get_repeater_setting_key('premium_table_text','premium_table_head_repeater', $index);

                        $this->add_render_attribute('head-cell-' . $index , 'class', 'premium-table-cell');
                        $this->add_render_attribute('head-cell-' . $index , 'class', 'elementor-repeater-item-' . $head_cell['_id']);

                        $this->add_render_attribute('head-text-' . $index, 'class', 'premium-table-text');

                        if( 'top' == $head_cell['premium_table_cell_icon_align'] && ('' != $head_cell['premium_table_cell_icon'] || '' != $head_cell['premium_table_cell_icon_img']['url'] ) ){
                            $this->add_render_attribute('head-text-' . $index, 'class', 'premium-table-cell-top');
                        }

                        $this->add_render_attribute($head_cell_text, 'class', 'premium-table-inner');

                        $this->add_inline_editing_attributes($head_cell_text, 'basic');

                        if( $head_cell['premium_table_cell_span'] > 1 ){
                            $this->add_render_attribute('head-cell-'. $index,'colspan', $head_cell['premium_table_cell_span']);
                        }
                        if( $head_cell['premium_table_cell_row_span'] > 1 ){
                            $this->add_render_attribute('head-cell-'. $index,'rowspan', $head_cell['premium_table_cell_row_span']);
                        }

                        $head .= '<th ' . $this->get_render_attribute_string('head-cell-'. $index) . '>';
                        $head .= '<span ' . $this->get_render_attribute_string('head-text-'. $index) . '>';
                        if( '' != $head_cell['premium_table_cell_icon'] || '' != $head_cell['premium_table_cell_icon_img']['url']){

                            $this->add_render_attribute('cell-icon-' . $index , 'class', 'premium-table-cell-icon-' . $head_cell['premium_table_cell_icon_align']);
                            $this->add_render_attribute('head-text-' . $index, 'class', 'premium-table-text');

                            $head .= '<span ' . $this->get_render_attribute_string( 'cell-icon-' . $index ) . '>';
                                if($head_cell['premium_table_icon_selector'] == 'font-awesome-icon'){
                                    $head .= '<i class="' . esc_attr( $head_cell['premium_table_cell_icon'] ) . '"></i>';
                                } else {
                                    $head .= '<img src="' . esc_attr( $head_cell['premium_table_cell_icon_img']['url'] ) . '">';
                                }

                            $head .= '</span>';
                        }

                        $head .= '<span ' . $this->get_render_attribute_string($head_cell_text) . '>' . $head_cell['premium_table_text'] . '</span>';
                        if ( 'yes' === $settings['premium_table_sort'] ) {
                            $head .= '<span class="premium-table-sort-icon premium-icon-sort fa fa-sort"></span>';
                            $head .= '<span class="premium-table-sort-icon premium-icon-sort-up fa fa-sort-up"></span>';
                            $head .= '<span class="premium-table-sort-icon premium-icon-sort-down fa fa-sort-down"></span>';
                        }
                        $head .= '</span>';
                        $head .= '</th>';

                    }

                    echo $head;
                    
                }
                
                ?>
                
            </tr>
            
        </thead>
            
    <?php }
    
protected function is_first_elem_row() {
    $settings = $this->get_settings();

    if ( 'row' === $settings['premium_table_body_repeater'][0]['premium_table_elem_type'] )
        return false;

    return true;
}
    
protected function get_table_body(){
     
    $settings = $this->get_settings();
        
    $body = '';
    
    $counter 		= 1;
    
    $cell_counter 	= 0;
    
    $row_count 		= count( $settings['premium_table_body_repeater'] );
        
    $this->add_render_attribute('table_body', 'class', 'premium-table-body');
        
    $this->add_render_attribute('table_row', 'class', 'premium-table-row');
        
    ?>
        
        <tbody <?php echo $this->get_render_attribute_string('table_body'); ?>>
            <?php if($this->is_first_elem_row()) { ?>
                <tr <?php echo $this->get_render_attribute_string('table_row'); ?>
            <?php } ?>
            <?php foreach($settings['premium_table_body_repeater'] as $index => $elem){
                
                $html_tag = 'span';
                
                $body_cell_text = $this->get_repeater_setting_key('premium_table_text','premium_table_body_repeater', $index);
                
                if( 'yes' == $elem['premium_table_link_switcher']) {
                    $html_tag = 'a';
                    if($elem['premium_table_link_selection'] == 'url'){
                        $this->add_render_attribute('body-cell-text-' . $counter, 'href', $elem['premium_table_link']['url']);
                    } else {
                        $this->add_render_attribute('body-cell-text-' . $counter, 'href', get_permalink($elem['premium_table_existing_link']));
                    }
                    if($elem['premium_table_link']['is_external']){
                        $this->add_render_attribute('body-cell-text-' . $counter, 'target', '_blank');
                    }
                    if($elem['premium_table_link']['nofollow']){
                        $this->add_render_attribute('body-cell-text-' . $counter, 'rel', 'nofollow');
                    }
                }
                
                if($elem['premium_table_elem_type'] == 'cell'){
                    $this->add_render_attribute('body-cell-' . $counter , 'class', 'premium-table-cell');
                    $this->add_render_attribute('body-cell-' . $counter , 'class', 'elementor-repeater-item-' . $elem['_id']);
                    
                    $this->add_render_attribute('body-cell-text-' . $counter, 'class', 'premium-table-text');
                    
                    if( 'top' == $elem['premium_table_cell_icon_align'] && ('' != $elem['premium_table_cell_icon'] || '' != $elem['premium_table_cell_icon_img']['url'] ) ){
                        $this->add_render_attribute('body-cell-text-' . $counter, 'class', 'premium-table-cell-top');
                    }
                    
                    $this->add_render_attribute($body_cell_text, 'class', 'premium-table-inner');
                    
                    $this->add_inline_editing_attributes($body_cell_text, 'basic');
                    if( $elem['_id'] ){
                        $this->add_render_attribute('body-cell-'.$counter,'id', $elem['_id']);
                    }
                    if( $elem['premium_table_cell_span'] > 1 ){
                        $this->add_render_attribute('body-cell-'. $counter,'colspan', $elem['premium_table_cell_span']);
                    }
                    if( $elem['premium_table_cell_row_span'] > 1 ){
                        $this->add_render_attribute('body-cell-'. $counter,'rowspan', $elem['premium_table_cell_row_span']);
                    }
                    
                    $body .= '<' . $elem['premium_table_cell_type'] . ' ' . $this->get_render_attribute_string('body-cell-' .$counter) . '>';
                    $body .= '<' . $html_tag . ' ' . $this->get_render_attribute_string('body-cell-text-'.$counter ) . '>';
                    if( '' != $elem['premium_table_cell_icon'] || '' != $elem['premium_table_cell_icon_img']['url']){
                        $this->add_render_attribute('cell-icon-' . $counter , 'class', 'premium-table-cell-icon-' . $elem['premium_table_cell_icon_align']);
                        $body .= '<span ' . $this->get_render_attribute_string( 'cell-icon-' . $counter ) . '>';
							if($elem['premium_table_icon_selector'] == 'font-awesome-icon'){
                                $body .= '<i class="' . esc_attr( $elem['premium_table_cell_icon'] ) . '"></i>';
                            } else {
                                $body .= '<img src="' . esc_attr( $elem['premium_table_cell_icon_img']['url'] ) . '">';
                            }
						$body .= '</span>';
                    }
                    $body .= '<span ' . $this->get_render_attribute_string($body_cell_text) . '>' . $elem['premium_table_text'] . '</span>';
                    $body .= '</span>';
                    $body .= '</' . $html_tag .'>';
                    $body .= '</' . $elem['premium_table_cell_type'] .'>';
                } else {
                    
                    $this->add_render_attribute( 'body-row-' . $counter, 'class', 'premium-table-row' );
                    $this->add_render_attribute( 'body-row-' . $counter, 'class', 'elementor-repeater-item-' . $elem['_id'] );

                    if ( $counter > 1 && $counter < $row_count ) {
                    $body .= '</tr><tr ' . $this->get_render_attribute_string( 'body-row-' . $counter ) . '>';

                    } else if ( $counter === 1 && false === $this->is_first_elem_row() ) {
                        $body .= '<tr ' . $this->get_render_attribute_string( 'body-row-' . $counter ) . '>';
                    }

                    $cell_counter = 0;
                    }

                    $counter++;
                }
                
                echo $body; ?>
            </tr>
        </tbody>
     
 <?php }


 protected function render(){
        $settings = $this->get_settings();
        
        $this->add_render_attribute('table_wrap', 'class', 'premium-table-wrap');
        if($settings['premium_table_responsive'] == 'yes'){
            $this->add_render_attribute('table_wrap', 'class', 'premium-table-responsive');
        }
        
        $this->add_render_attribute('table', 'class', 'premium-table');
        
        if($settings['premium_table_search'] == 'yes'){
            $this->add_render_attribute('table', 'class', 'premium-table-search');
        }
        
        if($settings['premium_table_sort'] == 'yes'){
            $this->add_render_attribute('table', 'class', 'premium-table-sort');
        }
        
        $table_settings = [
            'sort'      => ($settings['premium_table_sort'] == 'yes') ? true : false,
            'sortMob'   => ($settings['premium_table_sort_mob'] == 'yes') ? true : false,
            'search'    => ($settings['premium_table_search'] == 'yes') ? true : false,
            'dataType'  => $settings['premium_table_data_type'],
            'csvFile'   => ($settings['premium_table_csv_type'] == 'file') ? $settings['premium_table_csv']['url'] : $settings['premium_table_csv_url']
        ];
            
        $this->add_render_attribute('table', 'data-settings', wp_json_encode($table_settings));
        ?>
        
        <div <?php echo $this->get_render_attribute_string('table_wrap'); ?>>
            <?php if($settings['premium_table_search'] == 'yes') : ?>
            <div class="premium-table-search-wrap">
                <input type="text" id="premium-table-search-field" class="premium-table-search-field" placeholder="Live Search...">
            </div>
            
            <?php endif; ?>
            <table <?php echo $this->get_render_attribute_string('table'); ?>>
        
            <?php $this->get_table_head(); ?>
            <?php if(!empty($settings['premium_table_body_repeater'])) : $this->get_table_body(); endif; ?>
        
            </table>
        </div>
        
    <?php }
    
}