<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Premium_Image_Hotspots extends Widget_Base {
	protected $templateInstance;

    public function getTemplateInstance() {
        return $this->templateInstance = premium_Template_Tags::getInstance();
    }

    public function get_name() {
        return 'premium-addon-image-hotspots';
    }

    public function is_reload_preview_required()
    {
        return true;
    }
    
    public function get_title() {
		return \PremiumAddons\Helper_Functions::get_prefix() . ' Hotspots';
	}

    public function get_icon() {
        return 'pa-pro-hot-spot';
    }

    public function get_script_depends()
    {
        return [
            'tooltipster-bundle-js',
            'premium-pro-js'
            ];
    }

    public function get_categories() {
        return [ 'premium-elements' ];
    }

    // Adding the controls fields for the premium image hotspots
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {

    	/**START Background Image Section  **/
    	$this->start_controls_section('premium_image_hotspots_image_section',
			[
				'label' => esc_html__( 'Image', 'premium-addons-for-elementor' ),
			]
		);

		$this->add_control('premium_image_hotspots_image',
			[
				'label' => __( 'Choose Image', 'premium-addons-for-elementor' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
                'label_block'   => true
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'background_image', // Actually its `image_size`.
				'default' => 'full'
			]
		);

		$this->end_controls_section();
        
		$this->start_controls_section('premium_image_hotspots_icons_settings',
            [
                'label'         => esc_html__('Hotspots', 'premium-addons-for-elementor'),
            ]
        );

        $repeater = new Repeater();
        
        $repeater->add_control('premium_image_hotspots_icon_type_switch',
            [
                'label'         => esc_html__('Display On', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'font_awesome_icon'     => esc_html__('Font Awesome Icon', 'premium-addons-for-elementor'),
                    'custom_image'          => esc_html__('Custom Image', 'premium-addons-for-elementor'),
                    'text'                  => esc_html__('Text', 'premium-addons-for-elementor'),
                    ],
                'default'       => 'font_awesome_icon',
                'label_block'   => true,
            ]);
        
        $repeater->add_control('premium_image_hotspots_font_awesome_icon',
            [
                'label'         => esc_html__('Select Icon', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::ICON,
                'default'       => 'fa fa-map-marker',
                'condition'     => [
                    'premium_image_hotspots_icon_type_switch'     => 'font_awesome_icon',
                    ]
                ]
            );
        
        $repeater->add_control('premium_image_hotspots_custom_image',
            [
                'label'         => esc_html__('Custom Image', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::MEDIA,
                'default' => [
                    'url' => Utils::get_placeholder_image_src(),
                ],
                'condition'     => [
                        'premium_image_hotspots_icon_type_switch'     => 'custom_image',
                    ]
                ]
            );
        
        $repeater->add_control('premium_image_hotspots_text',
            [
                'label'         => esc_html__('Text', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'dynamic'       => [ 'active' => true ],
                'condition'     => [
                    'premium_image_hotspots_icon_type_switch'     => 'text',
	                    ]
            ]);
        
        $repeater->add_responsive_control('preimum_image_hotspots_main_icons_horizontal_position',
            [
                'label'     => esc_html__('Horizontal Position', 'premium-addons-for-elementor'),
                'type'      => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em', '%'],
                'range'     => [
                    'px'    => [
                        'min'   => 0,
                        'max'   => 200,
                    ],
                    'em'	=> [
                        'min'   => 0,
                        'max'   => 20,
                    ],
                ],
                'default'   => [
                    'size'  => 50,
                    'unit'  => '%'
                ],
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}.premium-image-hotspots-main-icons'    => 'left: {{SIZE}}{{UNIT}}'
                ]
            ]
            );
        
        $repeater->add_responsive_control('preimum_image_hotspots_main_icons_vertical_position',
            [
                'label'     => esc_html__('Vertical Position', 'premium-addons-for-elementor'),
                'type'      => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em', '%'],
                'range'     => [
                    'px'    => [
                        'min'   => 0,
                        'max'   => 200,
                    ],
                    'em'    => [
                        'min'   => 0,
                        'max'   => 20,
                    ],
                ],
                'default'   => [
                    'size'  => 50,
                    'unit'  => '%'
                ],
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}.premium-image-hotspots-main-icons'    => 'top: {{SIZE}}{{UNIT}}'
                ]
            ]
            );
        
        $repeater->add_control('premium_image_hotspots_content', 
            [
                'label'         => esc_html__('Content to Show', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'text_editor'           => esc_html__('Text Editor', 'premium-addons-for-elementor'),
                    'elementor_templates'   => esc_html__('Elementor Template', 'premium-addons-for-elementor'),
                ],
                'default'       => 'text_editor'
            ]
        );
        
        $repeater->add_control('premium_image_hotspots_tooltips_texts',
            [
                'type'          => Controls_Manager::WYSIWYG,
                'default'       => 'Lorem ipsum',
                'dynamic'       => [ 'active' => true ],
                'label_block'   => true,
                'condition'     => [
                    'premium_image_hotspots_content'    => 'text_editor'
                ]
            ]);
        
        $repeater->add_control('premium_image_hotspots_tooltips_temp',
            [
                'label'         => esc_html__( 'Elementor Template', 'premium-addons-for-elementor' ),
                'description'   => esc_html__( 'Elementor Template is a template which you can choose from Elementor library. Each template will be shown in content', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT2,
                'options'       => $this->getTemplateInstance()->get_elementor_page_list(),
                'label_block'   => true,
                'condition'     => [
                   'premium_image_hotspots_content'  => 'elementor_templates',
                ],
            ]
        );
        
        $repeater->add_control('premium_image_hotspots_link_switcher',
            [
                'label'         => esc_html__('Link', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('Add a custom link or select an existing page link','premium-addons-for-elementor'),
            ]);
        
        $repeater->add_control('premium_image_hotspots_link_type',
            [
                'label'         => esc_html__('Link/URL', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'url'   => esc_html__('URL', 'premium-addons-for-elementor'),
                    'link'  => esc_html__('Existing Page', 'premium-addons-for-elementor'),
                ],
                'default'       => 'url',
                'condition'     => [
                    'premium_image_hotspots_link_switcher'  => 'yes',
                ],
                'label_block'   => true,
            ]);
        
        $repeater->add_control('premium_image_hotspots_existing_page',
            [
                'label'         => esc_html__('Existing Page', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT2,
                'description'   => esc_html__('Active only when tooltips trigger is set to hover','premium-addons-for-elementor'),
                'options'       => $this->getTemplateInstance()->get_all_post(),
                'multiple'      => false,
                'condition'     => [
                    'premium_image_hotspots_link_switcher'  => 'yes',
                    'premium_image_hotspots_link_type'     => 'link',
                ],
                'label_block'   => true,
            ]);
        
        $repeater->add_control('premium_image_hotspots_url',
            [
                'label'         => esc_html__('URL', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::URL,
                'condition'     => [
                    'premium_image_hotspots_link_switcher'  => 'yes',
                    'premium_image_hotspots_link_type'     => 'url',
                ],
                'placeholder'   => 'https://premiumaddons.com/',
                'label_block'   => true
            ]);
        
        $repeater->add_control('premium_image_hotspots_link_text',
            [
                'label'         => esc_html__('Link Title', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'dynamic'       => [ 'active' => true ],
                'condition'     => [
                    'premium_image_hotspots_link_switcher' => 'yes',
                ],
                'label_block'   => true
            ]);
        
        $this->add_control('premium_image_hotspots_icons',
            [
                'label'         => esc_html__('Hotspots', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::REPEATER,
                'fields'        => array_values( $repeater->get_controls() ),
            ]
        );
        
        $this->add_control('premium_image_hotspots_icons_animation',
                [
                    'label'         => esc_html__('Radar Animation', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SWITCHER,
                ]
                );

        $this->end_controls_section();
        
        $this->start_controls_section('premium_image_hotspots_tooltips_section',
            [
                'label'         => esc_html__('Tooltips', 'premium-addons-for-elementor'),
            ]
        );

        $this->add_control(
			'premium_image_hotspots_trigger_type', 
		    [
		        'label'         => esc_html__('Trigger', 'premium-addons-for-elementor'),
		        'type'          => Controls_Manager::SELECT,
		        'options'       => [
		            'click'   => esc_html__('Click', 'premium-addons-for-elementor'),
		            'hover'  => esc_html__('Hover', 'premium-addons-for-elementor'),
		        ],
		        'default'       => 'hover'
		    ]
        );

        $this->add_control(
			'premium_image_hotspots_arrow', 
		    [
		        'label'         => esc_html__('Show Arrow', 'premium-addons-for-elementor'),
		        'type'          => Controls_Manager::SWITCHER,
                'label_on'      => esc_html__('Show', 'premium-addons-for-elementor'),
                'label_off'     => esc_html__('Hide', 'premium-addons-for-elementor'),
		    ]
        );

        $this->add_control(
        	'premium_image_hotspots_tooltips_position',
	        [
	            'label'         => esc_html__('Positon', 'premium-addons-for-elementor'),
	            'type'          => Controls_Manager::SELECT2,
	            'options'       => [
	                'top'   => esc_html__('Top', 'premium-addons-for-elementor'),
	                'bottom'=> esc_html__('Bottom', 'premium-addons-for-elementor'),
	                'left'  => esc_html__('Left', 'premium-addons-for-elementor'),
	                'right' => esc_html__('Right', 'premium-addons-for-elementor'),
	            ],
                'description'       => esc_html__('Sets the side of the tooltip. The value may one of the following: \'top\', \'bottom\', \'left\', \'right\'. It may also be an array containing one or more of these values. When using an array, the order of values is taken into account as order of fallbacks and the absence of a side disables it','premium-addons-for-elementor'),
                'default'           => ['top','bottom'],
	            'label_block'   => true,
                'multiple'      => true
	        ]
        );
        
        $this->add_control('premium_image_hotspots_tooltips_distance_position',
            [
                'label'             => esc_html__('Spacing', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::NUMBER,
                'title'       => esc_html__('The distance between the origin and the tooltip in pixels, default is 6','premium-addons-for-elementor'),
                'default'           => 6,
            ]
            );
        
        $this->add_control('premium_image_hotspots_min_width',
            [
                'label'             => esc_html__('Min Width', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::SLIDER,
                'range'         => [
                    'px'    => [
                        'min'       => 0,
                        'max'       => 800,
                    ],
                ],
                'description'       => esc_html__('Set a minimum width for the tooltip in pixels, default: 0 (auto width)','premium-addons-for-elemeneot'),
            ]
            );
        
        $this->add_control('premium_image_hotspots_max_width',
            [
                'label'             => esc_html__('Max Width', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::SLIDER,
                'range'         => [
                    'px'    => [
                        'min'       => 0,
                        'max'       => 800,
                    ],
                ],
                'description'       => esc_html__('Set a maximum width for the tooltip in pixels, default: null (no max width)','premium-addons-for-elemeneot'),
            ]
            );
        
        $this->add_responsive_control('premium_image_hotspots_tooltips_wrapper_height',
            [
                'label'         => esc_html__('Height', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em' , '%'],
                'range'         => [
                    'px'    => [
                        'min'       => 0,
                        'max'       => 500,
                    ],
                    'em'    => [
                        'min'       => 0,
                        'max'       => 20,
                    ]
                ],
                'label_block'   => true,
                'selectors'     => [
                    '.tooltipster-box.tooltipster-box-{{ID}}' => 'height: {{SIZE}}{{UNIT}} !important;'
                ]
        	]
        );

        $this->add_control('premium_image_hotspots_anim', 
            [
                'label'         => esc_html__('Animation', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'fade'  => esc_html__('Fade', 'premium-addons-for-elementor'),
                    'grow'  => esc_html__('Grow', 'premium-addons-for-elementor'),
                    'swing' => esc_html__('Swing', 'premium-addons-for-elementor'),
                    'slide' => esc_html__('Slide', 'premium-addons-for-elementor'),
                    'fall'  => esc_html__('Fall', 'premium-addons-for-elementor'),
                ],
                'default'       => 'fade',
                'label_block'   => true,
            ]
        );
        
        $this->add_control('premium_image_hotspots_anim_dur',
                [
                    'label'             => esc_html__('Animation Duration', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::NUMBER,
                    'title'             => esc_html__('Set the animation duration in milliseconds, default is 350', 'premium-addons-for-elementor'),
                    'default'           => 350,
                ]
                );
        
        $this->add_control('premium_image_hotspots_delay',
                [
                    'label'             => esc_html__('Delay', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::NUMBER,
                    'title'              => esc_html__('Set the animation delay in milliseconds, default is 10'),
                    'default'           => 10,
                ]
                );
        
        $this->add_control('premium_image_hotspots_hide',
                [
                    'label'         => esc_html__('Hide on Mobiles', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SWITCHER,
                    'label_on'      => 'Show',
                    'label_off'     => 'Hide',
                    'description'   => esc_html__('Hide tooltips on mobile phones', 'premium-addons-for-elementor'),
                    'return_value'  => true,
                ]
                );

        $this->end_controls_section();

        $this->start_controls_section('premium_image_hotspots_image_style_settings',
            [
                'label'         => esc_html__('Image', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_image_hotspots_image_border',
                'selector'      => '{{WRAPPER}} .premium-image-hotspots-container .premium-addons-image-hotspots-ib-img',
            ]
        );

        $this->add_control('premium_image_hotspots_image_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em' , '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-image-hotspots-container .premium-addons-image-hotspots-ib-img' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control('premium_image_hotspots_image_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em' ,'%'],
				'selectors'     => [
                  '{{WRAPPER}} .premium-image-hotspots-container .premium-addons-image-hotspots-ib-img' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]     
        );

        $this->end_controls_section();

        $this->start_controls_section('premium_image_hotspots_Hotspots_style_settings',
            [
                'label'         => esc_html__('Hotspots', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->start_controls_tabs('premium_image_hotspots_main_icons_active_borders_style_tabs');

        $this->start_controls_tab('premium_image_hotspots_main_icons_style_tab',
            [
                'label'         => esc_html__('Icon', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_image_hotspots_main_icons_color',
			[
				'label' => __( 'Color', 'premium-addons-for-elementor' ),
				'type' => Controls_Manager::COLOR,
				'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
				'selectors' => [
					'{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-icon' => 'color: {{VALUE}};',
				]
			]
		);

        $this->add_responsive_control('preimum_image_hotspots_main_icons_size',
	        [
	            'label'         => esc_html__('Size', 'premium-addons-for-elementor'),
	            'type'          => Controls_Manager::SLIDER,
	            'size_units'    => ['px', 'em'],
	            'range'     => [
	                'px'    => [
	                    'min'   => 0,
	                    'max'   => 500,
	                ],
	                'em'	=> [
	                    'min'   => 0,
	                    'max'   => 20,
	                ]
	            ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-icon' => 'font-size: {{SIZE}}{{UNIT}}',
                ],
	    	]
    	);

		$this->add_control('premium_image_hotspots_main_icons_background_color',
			[
				'label' => __( 'Background Color', 'premium-addons-for-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-icon' => 'background-color: {{VALUE}};',
				]
			]
		);
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_image_hotspots_main_icons_border',
                    'selector'      => '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-icon'
                ]
                );

        $this->add_control('premium_image_hotspots_main_icons_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,    
                'size_units'    => ['px', 'em' , '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-icon' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow', 'premium-addons-for-elementor'),
                    'name'          => 'premium_image_hotspots_main_icons_shadow',
                    'selector'      => '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-icon'
                ]
                );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'name'          => 'premium_image_hotspots_main_icons_shadow',
                    'selector'      => '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-icon',
                    'condition'     => [
                        'premium_image_hotspots_icons_animation!' => 'yes'
                    ]
                ]
                );
        
        $this->add_responsive_control('premium_image_hotspots_main_icons_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_responsive_control('premium_image_hotspots_main_icons_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-icon' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_image_hotspots_main_images_style_tab',
            [
                'label'         => esc_html__('Image', 'premium-addons-for-elementor'),
            ]
        );

        $this->add_responsive_control('preimum_image_hotspots_main_images_size',
	        [
	            'label'         => esc_html__('Size', 'premium-addons-for-elementor'),
	            'type'          => Controls_Manager::SLIDER,
	            'size_units'    => ['px', 'em'],
	            'range'     => [
	                'px'    => [
	                    'min'   => 0,
	                    'max'   => 500,
	                ],
	                'em'	=> [
	                    'min'   => 0,
	                    'max'   => 20,
	                ]
	            ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-image-icon' => 'width:{{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
                ],
	    	]
    	);

		$this->add_control('preimum_image_hotspots_main_images_background',
			[
				'label' => __( 'Background Color', 'premium-addons-for-elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
				'selectors' => [
					'{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-image-icon' => 'background-color: {{VALUE}};',
				]
			]
		);
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'preimum_image_hotspots_main_images_border',
                    'selector'      => '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-image-icon'
                ]
                );

        $this->add_control('preimum_image_hotspots_main_images_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,    
                'size_units'    => ['px', 'em' , '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-image-icon' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'name'          => 'premium_image_hotspots_main_images_shadow',
                    'selector'      => '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-image-icon',
                    'condition'     => [
                        'premium_image_hotspots_icons_animation!' => 'yes'
                    ]
                ]
                );
        
        $this->add_responsive_control('premium_image_hotspots_main_images_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-image-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_responsive_control('premium_image_hotspots_main_images_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-image-icon' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_image_hotspots_main_text_style_tab',
            [
                'label'         => esc_html__('Text', 'premium-addons-for-elementor'),
            ]
        );

        $this->add_control(
			'premium_image_hotspots_main_text_color',
			[
				'label'         => esc_html__( 'Text Color', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::COLOR,
				'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
				'selectors'     => [
					'{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-text' => 'color: {{VALUE}};',
				]
			]
		);
        
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_image_hotspots_main_text_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-text'
                    ]
                ); 
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'name'          => 'premium_image_hotspots_main_text_shadow',
                    'selector'      => '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-text'
                ]
                );

		$this->add_control(
			'premium_image_hotspots_main_text_background_color',
			[
				'label' => __( 'Background Color', 'premium-addons-for-elementor' ),
				'type' => Controls_Manager::COLOR,
				'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
				'selectors' => [
					'{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-text' => 'background-color: {{VALUE}};',
				]
			]
		);
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_image_hotspots_main_text_border',
                    'selector'      => '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-text'
                ]
                );

        $this->add_control('premium_image_hotspots_main_text_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,    
                'size_units'    => ['px', 'em' , '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-text' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'name'          => 'premium_image_hotspots_main_text_shadow',
                    'selector'      => '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-text',
                    'condition'     => [
                        'premium_image_hotspots_icons_animation!' => 'yes'
                    ]
                ]
                );
        
        $this->add_responsive_control('premium_image_hotspots_main_text_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-text' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_responsive_control('premium_image_hotspots_main_text_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-image-hotspots-main-icons .premium-image-hotspots-text' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();

        $this->end_controls_tabs();
        
        $this->add_control('premium_image_hotspots_radar_background',
			[
				'label' => __( 'Radar Background Color', 'premium-addons-for-elementor' ),
				'type' => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'condition' => [
                    'premium_image_hotspots_icons_animation'  => 'yes'
                ],
				'selectors' => [
					'{{WRAPPER}} .premium-image-hotspots-main-icons.premium-image-hotspots-anim::before' => 'background-color: {{VALUE}};',
				],
                'separator' => 'before'
			]
		);
        
        $this->add_control('premium_image_hotspots_radar_border_radius',
            [
                'label'         => esc_html__('Radar Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,    
                'size_units'    => ['px', 'em' , '%'],
                'condition' => [
                    'premium_image_hotspots_icons_animation'  => 'yes'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-image-hotspots-main-icons.premium-image-hotspots-anim::before' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_control(
			'premium_image_hotspots_main_icons_opacity',
			[
				'label'         => esc_html__( 'Hotspots Opacity', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::SLIDER,
				'range'         => [
					'px'    => [
		                'min' => 0,
		                'max' => 1,
		                'step'=> .1,
		            ]
				],
				'selectors'     => [
		            '{{WRAPPER}} .premium-image-hotspots-main-icons' => 'opacity: {{SIZE}};',
		        ],
                'separator' => 'before'
			]
		);


         $this->add_control('preimum_image_hotspots_main_icons_hover_animation',
	        [
				'label' => __( 'Hover Animation', 'premium-addons-for-elementor' ),
				'type' => Controls_Manager::HOVER_ANIMATION,
			]
		);

        $this->end_controls_section();
        
        $this->start_controls_section('premium_image_hotspots_tooltips_style_settings',
            [
                'label'         => esc_html__('Tooltips', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );
        
        $this->add_control('premium_image_hotspots_tooltips_wrapper_color',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '.tooltipster-box.tooltipster-box-{{ID}} .premium-image-hotspots-tooltips-text' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'                  => 'premium_image_hotspots_tooltips_wrapper_typo',
				'scheme'                => Scheme_Typography::TYPOGRAPHY_1,
                'selector'              => '.tooltipster-box.tooltipster-box-{{ID}} .premium-image-hotspots-tooltips-text'
			]
		);
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'premium_image_hotspots_tooltips_content_text_shadow',
                'selector'      => '.tooltipster-box.tooltipster-box-{{ID}} .premium-image-hotspots-tooltips-text'
            ]
        );
        
        $this->add_control('premium_image_hotspots_tooltips_wrapper_background_color',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '.tooltipster-box.tooltipster-box-{{ID}} .tooltipster-content' => 'background: {{VALUE}};',
                    '.premium-tooltipster-base.tooltipster-top .tooltipster-arrow-{{ID}} .tooltipster-arrow-background' => 'border-top-color: {{VALUE}};',
                    '.premium-tooltipster-base.tooltipster-bottom .tooltipster-arrow-{{ID}} .tooltipster-arrow-background' => 'border-bottom-color: {{VALUE}};',
                    '.premium-tooltipster-base.tooltipster-right .tooltipster-arrow-{{ID}} .tooltipster-arrow-background' => 'border-right-color: {{VALUE}};',
                    '.premium-tooltipster-base.tooltipster-left .tooltipster-arrow-{{ID}} .tooltipster-arrow-background' => 'border-left-color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_group_control(
                Group_Control_Border::get_type(),
                [
                    'name'              => 'premium_image_hotspots_tooltips_wrapper_border',
                    'selector'          => '.tooltipster-box.tooltipster-box-{{ID}} .tooltipster-content'
                    ]
                );

        $this->add_control('premium_image_hotspots_tooltips_wrapper_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em' , '%'],
                'selectors'		=>[
                	'.tooltipster-box.tooltipster-box-{{ID}} .tooltipster-content'	=>	'border-radius: {{SIZE}}{{UNIT}}'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'premium_image_hotspots_tooltips_wrapper_box_shadow',
                'selector'      => '.tooltipster-box.tooltipster-box-{{ID}} .tooltipster-content'
            ]
        );
        
        $this->add_responsive_control('premium_image_hotspots_tooltips_wrapper_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em', '%' ],
				'selectors'     => [
                  '.tooltipster-box.tooltipster-box-{{ID}} .tooltipster-content, .tooltipster-arrow-{{ID}}' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
//        $this->add_responsive_control('premium_image_hotspots_tooltips_wrapper_padding',
//            [
//                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
//                'type'          => Controls_Manager::DIMENSIONS,
//                'size_units'    => [ 'px', 'em', '%' ],
//				'selectors'     => [
//                  '.tooltipster-box.tooltipster-box-{{ID}} div.premium-image-hotspots-tooltips-text' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
//                ]
//            ]
//        );

        $this->end_controls_section();
		
        $this->start_controls_section('premium_img_hotspots_container_style',
            [
                'label'         => esc_html__('Container', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );
        
        $this->add_control('premium_img_hotspots_container_background',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-image-hotspots-container' => 'background: {{VALUE}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_img_hotspots_container_border',
                'selector'      => '{{WRAPPER}} .premium-image-hotspots-container',
            ]
        );

        $this->add_control('premium_img_hotspots_container_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-image-hotspots-container' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'premium_img_hotspots_container_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-image-hotspots-container',
            ]
        );

        $this->add_responsive_control('premium_img_hotspots_container_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-image-hotspots-container' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]      
        );
        
        $this->add_responsive_control('premium_img_hotspots_container_padding',
            [
                'label'         => esc_html__('Paddding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-image-hotspots-container' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]      
        );
        
        $this->end_controls_section();
	}


	protected function render($instance = [])
    {
    	// get our input from the widget settings.
    	$settings = $this->get_settings_for_display();
        $animation_class = '';
        if($settings['premium_image_hotspots_icons_animation'] == 'yes') {
            $animation_class = 'premium-image-hotspots-anim';
        }

		$image_src = $settings['premium_image_hotspots_image'];

		$image_src_size = Group_Control_Image_Size::get_attachment_image_src( $image_src['id'], 'background_image', $settings );
		if( empty( $image_src_size ) ) : $image_src_size = $image_src['url']; else: $image_src_size = $image_src_size; endif;
        
        $image_hotspots_settings = [
            'anim'      => $settings['premium_image_hotspots_anim'],
            'animDur'   => !empty($settings['premium_image_hotspots_anim_dur']) ? $settings['premium_image_hotspots_anim_dur'] : 350,
            'delay'     => !empty($settings['premium_image_hotspots_anim_delay']) ? $settings['premium_image_hotspots_anim_delay'] : 10,
            'arrow'     => ($settings['premium_image_hotspots_arrow'] == 'yes' ) ? true : false,
            'distance'  => !empty($settings['premium_image_hotspots_tooltips_distance_position']) ? $settings['premium_image_hotspots_tooltips_distance_position'] : 6,
            'minWidth'  =>  !empty($settings['premium_image_hotspots_min_width']['size']) ? $settings['premium_image_hotspots_min_width']['size'] : 0,
            'maxWidth'  => !empty($settings['premium_image_hotspots_max_width']['size']) ? $settings['premium_image_hotspots_max_width']['size'] : 'null',
            'side'      => !empty($settings['premium_image_hotspots_tooltips_position']) ? $settings['premium_image_hotspots_tooltips_position'] : array('right', 'left'),
            'hideMobiles'=> ($settings['premium_image_hotspots_hide'] == true) ? true : false,
            'trigger'   => $settings['premium_image_hotspots_trigger_type'],
            'id'        => $this->get_id()
        ];
?>
	<div id="premium-image-hotspots-<?php echo esc_attr($this->get_id()); ?>" class="premium-image-hotspots-container" data-settings='<?php echo wp_json_encode($image_hotspots_settings); ?>'>
        <img class="premium-addons-image-hotspots-ib-img" alt="Background" src="<?php echo $image_src_size; ?>">
    	<?php foreach ($settings['premium_image_hotspots_icons'] as $index => $item) {
            $list_item_key = 'premium_img_hotspot_' . $index;
            $this->add_render_attribute( $list_item_key, 'class',
             [
                 $animation_class,
                'premium-image-hotspots-main-icons',
                'elementor-repeater-item-' . $item['_id'],
                'tooltip-wrapper',
                'premium-image-hotspots-main-icons-'. $item['_id']
            ]);
		?>
	        <div <?php echo $this->get_render_attribute_string( $list_item_key ); ?> data-tooltip-content="#tooltip_content">
	        	<?php 
	    	    	$link_type = $item['premium_image_hotspots_link_type'];
				        if ($link_type == 'url') {
				            $link_url = $item['premium_image_hotspots_url']['url'];
				        } elseif ($link_type == 'link') {
				            $link_url = get_permalink( $item['premium_image_hotspots_existing_page' ]);
				        }
	            if ( $item['premium_image_hotspots_link_switcher'] == 'yes' && $settings['premium_image_hotspots_trigger_type'] == 'hover' ) : 
	            ?>
	        	<a class="premium-image-hotspots-tooltips-link" href="<?php echo esc_url($link_url); ?>" title="<?php echo $item['premium_image_hotspots_link_text']; ?>" <?php if(!empty($item['premium_image_hotspots_url']['is_external'])) : ?>target="_blank"<?php endif; ?><?php if(!empty($item['premium_image_hotspots_url']['nofollow'])) : ?>rel="nofollow"<?php endif; ?>>
		    		<?php if ( $item['premium_image_hotspots_icon_type_switch'] == 'font_awesome_icon') : ?>
		        	<i class="premium-image-hotspots-icon <?php echo esc_attr( $item['premium_image_hotspots_font_awesome_icon']);?> elementor-animation-<?php echo $settings['preimum_image_hotspots_main_icons_hover_animation']; ?>"></i>
		    		<?php elseif ( $item['premium_image_hotspots_icon_type_switch'] == 'custom_image') : ?>
	    			<div class="pica">
						<img alt="Hotspot Image" class="premium-image-hotspots-image-icon elementor-animation-<?php echo $settings['preimum_image_hotspots_main_icons_hover_animation']; ?>" src="<?php echo $item['premium_image_hotspots_custom_image']['url']; ?>">
					</div>
					<?php elseif ( $item['premium_image_hotspots_icon_type_switch'] == 'text') : ?>
						<p class="premium-image-hotspots-text elementor-animation-<?php echo $settings['preimum_image_hotspots_main_icons_hover_animation']; ?>"><?php echo esc_attr( $item['premium_image_hotspots_text']);?></p>

		    		<?php endif;?>
	    		</a>
	    	<?php else : ?>
	    		<?php if ( $item['premium_image_hotspots_icon_type_switch'] == 'font_awesome_icon') : ?>
		        	<i class="premium-image-hotspots-icon <?php echo esc_attr( $item['premium_image_hotspots_font_awesome_icon']);?> elementor-animation-<?php echo $settings['preimum_image_hotspots_main_icons_hover_animation']; ?>"></i>
		    		<?php elseif ( $item['premium_image_hotspots_icon_type_switch'] == 'custom_image') : ?>
	    			<div class="pica elementor-animation-<?php echo $settings['preimum_image_hotspots_main_icons_hover_animation']; ?>">
						<img alt="Hotspot Image" class="premium-image-hotspots-image-icon" src="<?php echo $item['premium_image_hotspots_custom_image']['url']; ?>">
					</div>
					<?php elseif ( $item['premium_image_hotspots_icon_type_switch'] == 'text') : ?>
						<p class="premium-image-hotspots-text elementor-animation-<?php echo $settings['preimum_image_hotspots_main_icons_hover_animation']; ?>"><?php echo esc_attr( $item['premium_image_hotspots_text']);?></p>

	    		<?php endif;?>
    		<?php endif; ?>
		        <div class="premium-image-hotspots-tooltips-wrapper">
		            <div id="tooltip_content" class="premium-image-hotspots-tooltips-text"><?php
                    if($item['premium_image_hotspots_content'] == 'elementor_templates') {
                        $elementor_post_id = $item['premium_image_hotspots_tooltips_temp'];
                        $premium_elements_frontend = new Frontend;
                        echo $premium_elements_frontend->get_builder_content($elementor_post_id, true);
                    } else {
                        echo $item['premium_image_hotspots_tooltips_texts']; } ?></div>
		        </div>
	        </div>
        <?php } ?>
    </div>
    
        <?php
	}
}