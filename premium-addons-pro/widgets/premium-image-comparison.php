<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Premium_Image_Comparison extends Widget_Base {

    public function get_name() {
        return 'premium-addon-image-comparison';
    }

    public function is_reload_preview_required()
    {
        return true;
    }

    public function get_title() {
		return \PremiumAddons\Helper_Functions::get_prefix() . ' Image Comparison';
	}

    public function get_icon() {
        return 'pa-pro-image-comparison';
    }

    public function get_script_depends() {
        return [
            'pa-imagesloaded',
            'event-move',
            'pa-imgcompare',
            'premium-pro-js'
            ];
    }

    public function get_categories() {
        return [ 'premium-elements' ];
    }

    // Adding the controls fields for the premium image comparison
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {

        $this->start_controls_section('premium_img_compare_original_image_section',
            [
                'label' => esc_html__( 'Original Image', 'premium-addons-for-elementor' ),
            ]
        );

        $this->add_control('premium_image_comparison_original_image',
            [
                'label' => __( 'Choose Image', 'premium-addons-for-elementor' ),
                'type' => Controls_Manager::MEDIA,
                'description'   => esc_html__('It\'s recommended to use two images that have the same size','premium-addons-for-elementor'),
                'default' => [
                    'url' => Utils::get_placeholder_image_src(),
                ],
                'label_block'   => true
            ]
        );

        $this->add_control('premium_img_compare_original_img_label_switcher', 
            [
                'label'         => esc_html__('Label', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes'
            ]
        );

        $this->add_control('premium_img_compare_original_img_label',
            [
                'label' => esc_html__( 'Text', 'premium-addons-for-elementor' ),
                'type' => Controls_Manager::TEXT,
                'default' => esc_html__( 'Before', 'premium-addons-for-elementor' ),
                'placeholder'   => 'Before',
                'condition'     => [
                   'premium_img_compare_original_img_label_switcher'  => 'yes',
                ],
                'label_block' => true
            ]
        );
        
        $this->add_control('premium_img_compare_original_hor_label_position',
                [
                    'label'         => esc_html__('Horizontal Position', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'left'  => [
                            'title'     => esc_html__('Left', 'premium-addons-for-elementor'),
                            'icon'      => 'fa fa-align-left'
                        ],
                        'center'  => [
                            'title'     => esc_html__('Center', 'premium-addons-for-elementor'),
                            'icon'      => 'fa fa-align-center'
                        ],
                        'right'  => [
                            'title'     => esc_html__('Right', 'premium-addons-for-elementor'),
                            'icon'      => 'fa fa-align-right'
                        ],
                    ],
                    'condition'     => [
                        'premium_img_compare_original_img_label_switcher'  => 'yes',
                        'premium_image_comparison_orientation'          => 'vertical'
                    ],
                    'default'       => 'center',
                    'label_block'   => true,
                ]
                );

        $this->add_responsive_control('premium_img_compare_original_label_horizontal_offset',
            [
                'label'     	=> esc_html__('Horizontal Offset', 'premium-addons-for-elementor'),
                'type'      	=> Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em','%'],
                'range'         => [
                    'px'    => [
                        'min'   => 0,
                        'max'   => 300
                    ],
                ],
                'condition'     => [
                   'premium_img_compare_original_img_label_switcher'  => 'yes',
                    'premium_image_comparison_orientation'          => 'horizontal'
                ],
                'selectors' => [
                    '{{WRAPPER}} .premium-twentytwenty-horizontal .premium-twentytwenty-before-label' => 'left: {{SIZE}}{{UNIT}};'
                ]
            ]  
        );
        
        $this->add_control('premium_img_compare_original_label_position',
                [
                    'label'         => esc_html__('Vertical Position', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'           => [
                        'top'    => [
                            'title' => __( 'Top', 'premium-addons-for-elementor' ),
                            'icon'  => 'fa fa-arrow-circle-up',
                        ],
                        'middle' => [
                            'title' => __( 'Middle', 'premium-addons-for-elementor' ),
                            'icon'  => 'fa fa-align-center',
                        ],
                        'bottom' => [
                            'title' => __( 'Bottom', 'premium-addons-for-elementor' ),
                            'icon'  => 'fa fa-arrow-circle-down',
                        ],
                    ],
                    'condition'     => [
                        'premium_img_compare_original_img_label_switcher'  => 'yes',
                        'premium_image_comparison_orientation'          => 'horizontal'
                    ],
                    'default'       => 'middle',
                    'label_block'   => true,
                ]
                );
        
        $this->add_responsive_control('premium_img_compare_original_label_vertical_offset',
            [
                'label'     	=> esc_html__('Vertical Offset', 'premium-addons-for-elementor'),
                'type'      	=> Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em','%'],
                'range'         => [
                    'px'    => [
                        'min'   => 0,
                        'max'   => 300
                    ],
                ],
                'condition'     => [
                   'premium_img_compare_original_img_label_switcher'  => 'yes',
                    'premium_image_comparison_orientation'          => 'vertical'
                ],
                'selectors' => [
                    '{{WRAPPER}} .premium-twentytwenty-vertical .premium-twentytwenty-before-label' => 'top: {{SIZE}}{{UNIT}};',
                ]
            ]
        );
        
        $this->end_controls_section();

        
        $this->start_controls_section('premium_image_comparison_modified_image_section',
            [
                'label' => esc_html__( 'Modified Image', 'premium-addons-for-elementor' ),
            ]
        );

        $this->add_control('premium_image_comparison_modified_image',
            [
                'label' => __( 'Choose Image', 'premium-addons-for-elementor' ),
                'type' => Controls_Manager::MEDIA,
                'description'   => esc_html__('It\'s recommended to use two images that have the same size','premium-addons-for-elementor'),
                'default' => [
                    'url' => Utils::get_placeholder_image_src(),
                ],
                'label_block'   => true
            ]
        );

        $this->add_control('premium_image_comparison_modified_image_label_switcher', 
            [
                'label'         => esc_html__('Label', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes'
            ]
        );

        $this->add_control('premium_image_comparison_modified_image_label',
            [
                'label' => esc_html__( 'Text', 'premium-addons-for-elementor' ),
                'type' => Controls_Manager::TEXT,
                'placeholder'   => 'After',
                'default' => esc_html__( 'After', 'premium-addons-for-elementor' ),
                'condition'     => [
                   'premium_image_comparison_modified_image_label_switcher'  => 'yes',
                ],
                'label_block' => true
            ]
        );
        
        $this->add_control('premium_img_compare_modified_hor_label_position',
                [
                    'label'         => esc_html__('Horizontal Position', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'left'  => [
                            'title'     => esc_html__('Left', 'premium-addons-for-elementor'),
                            'icon'      => 'fa fa-align-left'
                        ],
                        'center'  => [
                            'title'     => esc_html__('Center', 'premium-addons-for-elementor'),
                            'icon'      => 'fa fa-align-center'
                        ],
                        'right'  => [
                            'title'     => esc_html__('Right', 'premium-addons-for-elementor'),
                            'icon'      => 'fa fa-align-right'
                        ],
                    ],
                    'condition'     => [
                        'premium_image_comparison_modified_image_label_switcher'  => 'yes',
                        'premium_image_comparison_orientation'          => 'vertical'
                    ],
                    'default'       => 'center',
                    'label_block'   => true,
                ]
                );

        $this->add_responsive_control('premium_img_compare_modified_label_horizontal_offset',
            [
                'label'     	=> esc_html__('Horizontal Offset', 'premium-addons-for-elementor'),
                'type'      	=> Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em','%'],
                'range'         => [
                    'px'    => [
                        'min'   => 0,
                        'max'   => 300
                    ],
                ],
                'condition'     => [
                   'premium_image_comparison_modified_image_label_switcher'  => 'yes',
                    'premium_image_comparison_orientation'          => 'horizontal'
                ],
                'selectors' => [
                    '{{WRAPPER}} .premium-twentytwenty-horizontal .premium-twentytwenty-after-label' => 'right: {{SIZE}}{{UNIT}};',
                ]
            ]  
        );

        $this->add_control('premium_img_compare_modified_label_position',
                [
                    'label'         => esc_html__('Vertical Position', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'           => [
                        'top'    => [
                            'title' => __( 'Top', 'premium-addons-for-elementor' ),
                            'icon'  => 'fa fa-arrow-circle-up',
                        ],
                        'middle' => [
                            'title' => __( 'Middle', 'premium-addons-for-elementor' ),
                            'icon'  => 'fa fa-align-center',
                        ],
                        'bottom' => [
                            'title' => __( 'Bottom', 'premium-addons-for-elementor' ),
                            'icon'  => 'fa fa-arrow-circle-down',
                        ],
                    ],
                    'condition'     => [
                        'premium_image_comparison_modified_image_label_switcher'  => 'yes',
                        'premium_image_comparison_orientation'          => 'horizontal'
                    ],
                    'default'       => 'middle',
                    'label_block'   => true,
                ]
            );
        
        $this->add_responsive_control('premium_img_compare_modified_label_vertical_offset',
            [
                'label'     	=> esc_html__('Vertical Offset', 'premium-addons-for-elementor'),
                'type'      	=> Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em','%'],
                'range'         => [
                    'px'    => [
                        'min'   => 0,
                        'max'   => 300
                    ],
                ],
                'condition'     => [
                   'premium_image_comparison_modified_image_label_switcher'  => 'yes',
                    'premium_image_comparison_orientation'          => 'vertical'
                ],
                'selectors' => [
                    '{{WRAPPER}} .premium-twentytwenty-vertical .premium-twentytwenty-after-label' => 'bottom: {{SIZE}}{{UNIT}};',
                ]
            ]
        );

        $this->end_controls_section();
        
        
        $this->start_controls_section('premium_img_compare_display_options',
            [
                'label' => esc_html__( 'Display Options', 'premium-addons-for-elementor' ),
            ]
        );
        
        $this->add_group_control(
            Group_Control_Image_Size::get_type(),
                [
                    'name' => 'prmium_img_compare_images_size', 
                    'default' => 'full',
                    ]
                );
        
        $this->add_control('premium_image_comparison_orientation',
            [
                'label'         => esc_html__('Orientation', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'horizontal'    => esc_html__('Vertical'),
                    'vertical'      => esc_html__('Horizontal'),
                ],
                'default'       => 'horizontal',
                'label_block'   => true,
            ]
        );

        $this->add_control('premium_img_compare_visible_ratio',
            [
                'label'   => esc_html__( 'Visible Ratio', 'premium-addons-for-elementor' ),
                'type'    => Controls_Manager::NUMBER,
                'default' => 0.5,
                'min'     => 0,
                'step'    => 0.1,
                'max'     => 1,
            ]
        );

        $this->add_control('premium_image_comparison_add_drag_handle',
            [
                'label'         => esc_html__('Show Drag Handle','premium-addons-for-elementor'),
                'description'   => esc_html__('Show drag handle between the images', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
            ]
        );
        
        $this->add_control('premium_image_comparison_add_separator',
            [
                'label'         => esc_html__('Show Separator','premium-addons-for-elementor'),
                'description'   => esc_html__('Show separator between the images', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'condition'     => [
                    'premium_image_comparison_add_drag_handle'  => 'yes'
                ]
            ]
        );
        
        $this->add_control('premium_image_comparison_interaction_mode',
            [
                'label'         => esc_html__('Interaction Mode', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'mousemove'     => esc_html__('Mouse Move','premium-addons-for-elementor'),
                    'drag'          => esc_html__('Mouse Drag','premium-addons-for-elementor'),
                    'click'         => esc_html__('Mouse Click','premium-addons-for-elementor'),
                ],
                'default'       => 'mousemove',
                'label_block'   => true,
            ]
        );
        
        $this->add_control('premium_image_comparison_overlay',
            [
                'label'         => esc_html__('Overlay Color','premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                
            ]
        );

        $this->end_controls_section();

        
        $this->start_controls_section('premium_img_compare_original_img_label_style_tab',
            [
                'label'         => esc_html__('First Label', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_img_compare_original_img_label_switcher'  => 'yes'
                ]
            ]
        );
        
        $this->add_control('premium_image_comparison_original_label_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-before-label span'   => 'color: {{VALUE}};',
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'premium_image_comparison_original_label_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-twentytwenty-before-label span',
            ]
        );
        
        $this->add_control('premium_image_comparison_original_label_background_color',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-before-label span'  => 'background-color: {{VALUE}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_image_comparison_original_label_border',
                'selector'      => '{{WRAPPER}} .premium-twentytwenty-before-label span',
            ]
        );

        $this->add_control('premium_image_comparison_original_label_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em' , '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-before-label span' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'premium_image_comparison_original_label_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-twentytwenty-before-label span',
            ]
        );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'              => 'premium_image_comparison_original_label_text_shadow',
                'label'             => esc_html__('Shadow','premium-addons-for-elementor'),
                'selector'          => '{{WRAPPER}} .premium-twentytwenty-before-label span',
            ]
        );

        $this->add_responsive_control('premium_image_comparison_original_label_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-before-label span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]      
        );

        $this->end_controls_section();

        $this->start_controls_section('premium_image_comparison_modified_image_label_style_tab',
            [
                'label'         => esc_html__('Second Label', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_image_comparison_modified_image_label_switcher'  => 'yes',
                ]
            ]
        );
        
        $this->add_control('premium_image_comparison_modified_label_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-after-label span'   => 'color: {{VALUE}};',
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'premium_image_comparison_modified_label_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-twentytwenty-after-label span',
            ]
        );
        
        $this->add_control('premium_image_comparison_modified_label_background_color',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-after-label span'  => 'background-color: {{VALUE}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_image_comparison_modified_label_border',
                'selector'      => '{{WRAPPER}} .premium-twentytwenty-after-label span',
            ]
        );

        $this->add_control('premium_image_comparison_modified_label_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em' , '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-after-label span' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'premium_image_comparison_modified_label_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-twentytwenty-after-label span',
            ]
        );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'              => 'premium_image_comparison_modified_label_text_shadow',
                'label'             => esc_html__('Shadow','premium-addons-for-elementor'),
                'selector'          => '{{WRAPPER}} .premium-twentytwenty-after-label span',
            ]
        );

        $this->add_responsive_control('premium_image_comparison_modified_label_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-after-label span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]      
        );

        $this->end_controls_section();
        
        $this->start_controls_section('premium_image_comparison_drag_style_settings',
            [
                'label'         => esc_html__('Drag', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                   'premium_image_comparison_add_drag_handle'  => 'yes',
                ],
            ]
        );
        
        $this->add_responsive_control('premium_image_comparison_drag_width',
            [
                'label'         => esc_html__('Width', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'description'   => esc_html__('Enter Drag width in (PX), default is 50px', 'premium-addons-for-elementor'),
                'size_units'    => ['px', 'em'],
                'label_block'   => true,
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-handle' => 'width:{{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control('premium_image_comparison_drag_height',
            [
                'label'         => esc_html__('Height', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'range'         => [
                    'px'    => [
                        'min'   => 0,
                        'max'   => 300,
                    ]
                ],
                'description'   => esc_html__('Enter Drag height in (PX), default is 50px', 'premium-addons-for-elementor'),
                'size_units'    => ['px','em'],
                'label_block'   => true,
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-handle' => 'height: {{SIZE}}{{UNIT}};',
                ]
            ]
        );

        $this->add_control('premium_image_comparison_drag_background_color',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-handle'  => 'background-color: {{VALUE}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_image_comparison_drag_border',
                'selector'      => '{{WRAPPER}} .premium-twentytwenty-handle',
            ]
        );

        $this->add_control('premium_image_comparison_drag_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-handle' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'premium_image_comparison_drag_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-twentytwenty-handle',
            ]
        );

        $this->add_responsive_control('premium_image_comparison_drag_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-handle' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]      
        );

        $this->end_controls_section();

        $this->start_controls_section('premium_image_comparison_arrow_style',
            [
                'label'         => esc_html__('Arrows', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                   'premium_image_comparison_add_drag_handle'  => 'yes',
                ],
            ]
        );

        $this->add_responsive_control('premium_image_comparison_arrows_size',
            [
                'label'         => esc_html__('Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'label_block'   => true,
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-left-arrow' => 'border: {{SIZE}}px inset transparent; border-right: {{SIZE}}px solid; margin-top: -{{size}}px',
                    '{{WRAPPER}} .premium-twentytwenty-right-arrow' => 'border: {{SIZE}}px inset transparent; border-left: {{SIZE}}px solid; margin-top: -{{size}}px',
                    '{{WRAPPER}} .premium-twentytwenty-down-arrow' => 'border: {{SIZE}}px inset transparent; border-top: {{SIZE}}px solid; margin-left: -{{size}}px',
                    '{{WRAPPER}} .premium-twentytwenty-up-arrow' => 'border: {{SIZE}}px inset transparent; border-bottom: {{SIZE}}px solid; margin-left: -{{size}}px',
                ]
            ]
        );

        $this->add_control('premium_image_comparison_arrows_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-left-arrow' => 'border-right-color: {{VALUE}}',
                    '{{WRAPPER}} .premium-twentytwenty-right-arrow' => 'border-left-color: {{VALUE}}',
                    '{{WRAPPER}} .premium-twentytwenty-down-arrow' => 'border-top-color: {{VALUE}};',
                    '{{WRAPPER}} .premium-twentytwenty-up-arrow' => 'border-bottom-color: {{VALUE}};',
                ]
            ]
        );

        $this->end_controls_section();
        
        $this->start_controls_section('premium_img_compare_separator_style_settings',
            [
                'label'         => esc_html__('Separator', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                   'premium_image_comparison_add_drag_handle'  => 'yes',
                    'premium_image_comparison_add_separator'    => 'yes'
                ],
            ]
        );
        
        $this->add_control('premium_img_compare_separator_background_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-handle:after, {{WRAPPER}} .premium-twentytwenty-handle:before'  => 'background-color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_responsive_control('premium_img_compare_separator_spacing',
            [
                'label'         => esc_html__('Spacing', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'range'         => [
                    'px' => [
                        'min'   => 0,
                        'max'   => 200,
                    ]
                ],
                'label_block'   => true,
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-horizontal .premium-twentytwenty-handle:after' => 'top: {{SIZE}}%;',
                    '{{WRAPPER}} .premium-twentytwenty-horizontal .premium-twentytwenty-handle:before' => 'bottom: {{SIZE}}%;',
                    '{{WRAPPER}} .premium-twentytwenty-vertical .premium-twentytwenty-handle:after' => 'right: {{SIZE}}%;',
                    '{{WRAPPER}} .premium-twentytwenty-vertical .premium-twentytwenty-handle:before' => 'left: {{SIZE}}%;'
                ]
            ]
        );
        
        $this->add_responsive_control('premium_img_compare_separator_width',
            [
                'label'         => esc_html__('Height', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em'],
                'label_block'   => true,
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-vertical .premium-twentytwenty-handle:before,{{WRAPPER}} .premium-twentytwenty-vertical .premium-twentytwenty-handle:after' => 'height: {{SIZE}}{{UNIT}};',
                ],
                'condition'     => [
                   'premium_image_comparison_add_drag_handle'   => 'yes',
                    'premium_image_comparison_add_separator'    => 'yes',
                    'premium_image_comparison_orientation'      => 'vertical'
                ],
            ]
        );

        $this->add_responsive_control('premium_img_compare_separator_height',
            [
                'label'         => esc_html__('Width', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em', '%'],
                'label_block'   => true,
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-horizontal .premium-twentytwenty-handle:after,{{WRAPPER}} .premium-twentytwenty-horizontal .premium-twentytwenty-handle:before' => 'width: {{SIZE}}{{UNIT}};',
                ],
                'condition'     => [
                   'premium_image_comparison_add_drag_handle'   => 'yes',
                    'premium_image_comparison_add_separator'    => 'yes',
                    'premium_image_comparison_orientation'      => 'horizontal'
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'premium_img_compare_separator_shadow',
                'selector'      => '{{WRAPPER}} .premium-twentytwenty-handle:after,{{WRAPPER}} .premium-twentytwenty-handle:before',
            ]
        );

        $this->end_controls_section();

         
        $this->start_controls_section('premium_image_comparison_contents_wrapper_style_settings',
            [
                'label'         => esc_html__('Container', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control('premium_image_comparison_overlay_background',
            [
                'label'         => esc_html__('Overlay Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-twentytwenty-overlay.premium-twentytwenty-show:hover'  => 'background: {{VALUE}};'
                ],
                'condition'     => [
                    'premium_image_comparison_overlay'  => 'yes'
                ]
            ]
        );
       
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_image_comparison_contents_wrapper_border',
                'selector'      => '{{WRAPPER}} .premium-images-compare-container',
            ]
        );

        $this->add_control('premium_image_comparison_contents_wrapper_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-images-compare-container' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'premium_image_comparison_contents_wrapper_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-images-compare-container',
            ]
        );

        $this->add_responsive_control('premium_image_comparison_contents_wrapper_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-images-compare-container' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]      
        );
        
        $this->end_controls_section();

    }

    protected function render($instance = [])
    {
        // get our input from the widget settings.
        $settings = $this->get_settings();
            
        $original_image = $settings['premium_image_comparison_original_image'];

        $modified_image = $settings['premium_image_comparison_modified_image'];

        $original_image_src = Group_Control_Image_Size::get_attachment_image_src( $original_image['id'], 'prmium_img_compare_images_size', $settings );
        
        $original_image_src = empty($original_image_src) ? $original_image['url'] : $original_image_src;
        
        $modified_image_src = Group_Control_Image_Size::get_attachment_image_src( $modified_image['id'], 'prmium_img_compare_images_size', $settings );
        
        $modified_image_src = empty($modified_image_src) ? $modified_image['url'] : $modified_image_src;
        

        $img_compare_setttings = [
            'orientation'      => $settings['premium_image_comparison_orientation'],
            'visibleRatio'     => !empty($settings['premium_img_compare_visible_ratio']) ? $settings['premium_img_compare_visible_ratio'] : 0.1,
            'switchBefore'      => ($settings['premium_img_compare_original_img_label_switcher'] == 'yes') ? true: false,
            'beforeLabel'       => ($settings['premium_img_compare_original_img_label_switcher'] == 'yes' && !empty($settings['premium_img_compare_original_img_label'])) ? $settings['premium_img_compare_original_img_label'] : '',
            'switchAfter'       => ($settings['premium_image_comparison_modified_image_label_switcher'] == 'yes') ? true: false,
            'afterLabel'        => ($settings['premium_image_comparison_modified_image_label_switcher'] == 'yes' && !empty($settings['premium_image_comparison_modified_image_label'])) ? $settings['premium_image_comparison_modified_image_label'] : '',
            'mouseMove'         => ($settings['premium_image_comparison_interaction_mode'] == 'mousemove') ? true: false,
            'clickMove'         => ($settings['premium_image_comparison_interaction_mode'] == 'click') ? true: false,
            'showDrag'          => ($settings['premium_image_comparison_add_drag_handle'] == 'yes') ? true : false,
            'showSep'           => ($settings['premium_image_comparison_add_separator'] == 'yes') ? true : false,
            'overlay'           => ($settings['premium_image_comparison_overlay'] == 'yes') ? false : true,
            'beforePos'         => $settings['premium_img_compare_original_label_position'],
            'afterPos'          => $settings['premium_img_compare_modified_label_position'],
            'verbeforePos'      => $settings['premium_img_compare_original_hor_label_position'],
            'verafterPos'       => $settings['premium_img_compare_modified_hor_label_position'],
            
            
        ];
?>

    <!-- Main div container -->
    
        <div class="premium-image-comparison-contents-wrapper premium-twentytwenty-wrapper">
            <div id="premium-image-comparison-contents-wrapper-<?php echo esc_attr($this->get_id()); ?>" class="premium-images-compare-container premium-twentytwenty-container" data-settings='<?php echo wp_json_encode($img_compare_setttings); ?>'>
                <img src="<?php echo esc_url($original_image_src); ?>" alt="<?php echo $settings['premium_img_compare_original_img_label']; ?>">
                <img src="<?php echo esc_url($modified_image_src) ?>" alt="<?php echo $settings['premium_image_comparison_modified_image_label']; ?>">
                
            </div>
        </div>
    
<?php

    }
}