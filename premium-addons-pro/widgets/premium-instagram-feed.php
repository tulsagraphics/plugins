<?php
namespace Elementor;

use Elementor\Core\Responsive\Responsive;

if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Premium_Instagram_Feed extends Widget_Base {
    public function get_name() {
        return 'premium-addon-instagram-feed';
    }

    public function get_title() {
		return \PremiumAddons\Helper_Functions::get_prefix() . ' Instagram Feed';
	}
    
    public function is_reload_preview_required(){
        return true;
    }
    
    public function get_script_depends(){
        return [
            'pa-imagesloaded',
            'pa-insta-prettyphoto',
            'masonry-js',
            'instafeed-js',
            'premium-pro-js',
        ];
    }

    public function get_icon() {
        return 'pa-pro-instagram-feed';
    }

    public function get_categories() {
        return [ 'premium-elements' ];
    }

    // Adding the controls fields for the premium instagram feed
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {
        
       /*Start General Settings Section*/
        $this->start_controls_section('premium_instagram_feed_general_settings_section',
                [
                    'label'         => esc_html__('Access Credentials', 'premium-addons-for-elementor')
                    ]
                );
        
        /*Client ID*/
        $this->add_control('premium_instagram_feed_client_id',
                [
                    'label'         => esc_html__('Client ID', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'default'       => '8f051adcfa134a18bd6afecc7521ef44',
                    'label_block'   => false,
                    'description'   => '<a href="https://www.instagram.com/developer/" target="_blank">Get Client ID</a> by creating a new app or selecting an existing app ',
                    ]
                );
                
        /*Access Token*/
        $this->add_control('premium_instagram_feed_client_access_token',
                [
                    'label'         => esc_html__('Access Token', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'default'       => '2075884021.1677ed0.2fd28d5d3abf45d4a80534bee8376f4c',
                    'label_block'   => false,
                    'description'   => '<a href="http://instagram.pixelunion.net/" target="_blank">Get Access Token</a> by creating a new app or selecting an existing app ',
                    ]
                );
        
        /*Search For userID*/
        $this->add_control('premium_instagram_feed_user_id',
                [
                    'label'         => esc_html__('User Id', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'default'       => '2075884021',
                    'label_block'   => false,
                    'description'   => 'Click <a href="https://codeofaninja.com/tools/find-instagram-user-id" target="_blank">Here</a> to get User ID',
                    ]
                );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_instagram_feed_query_section',
                [
                    'label'             => esc_html__('Queries', 'premium-addons-for-elementor')
                ]);
        
        /*Search For*/
        $this->add_control('premium_instagram_feed_search_for',
                [
                    'label'             => esc_html__('Search for:', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::SELECT,
                    'options'           => [
                        'user'      => esc_html__('User', 'premium-addons-for-elementor'),
                        'location'  => esc_html__('Location', 'premium-addons-for-elementor'),
                        'tagged'    => esc_html__('Tag', 'premium-addons-for-elementor'),
                    ],
                    'default'           => 'user',
                ]
                );
        
        /*Search For Location id*/
        $this->add_control('premium_instagram_feed_location_id',
                [
                    'label'         => esc_html__('Location Id', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'default'       => '6889842',
                    'description'   => 'Click <a href="http://docs.social-streams.com/article/118-find-instagram-location-id" target="_blank">Here</a> to find how to get location ID',
                    'condition'     => [
                        'premium_instagram_feed_search_for' => 'location',
                        ]
                    ]
                );
        
        /*Search For Location id*/
        $this->add_control('premium_instagram_feed_tag_name',
                [
                    'label'         => esc_html__('Tag Name', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'default'       => 'sport',
                    'condition'     => [
                        'premium_instagram_feed_search_for' => 'tagged',
                        ]
                    ]
                );
        
        /*Images Sort By*/
        $this->add_control('premium_instagram_feed_sort_by',
                [
                    'label'             => esc_html__('Sort By:', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::SELECT,
                    'options'           => [
                        'none'              => esc_html__('none', 'premium-addons-for-elementor'),
                        'most-recent'       => esc_html__('Most Recent', 'premium-addons-for-elementor'),
                        'least-recent'      => esc_html__('Least Recent', 'premium-addons-for-elementor'),
                        'most-liked'        => esc_html__('Most Liked', 'premium-addons-for-elementor'),
                        'least-liked'       => esc_html__('Least Liked', 'premium-addons-for-elementor'),
                        'most-commented'    => esc_html__('Most Commented', 'premium-addons-for-elementor'),
                        'least-commented'   => esc_html__('Least Commented', 'premium-addons-for-elementor'),
                        'random'            => esc_html__('Random', 'premium-addons-for-elementor'),
                    ],
                    'default'           => 'none',
                ]
                );
        
        $this->add_control('premium_instagram_feed_link',
			[
				'label'                 => esc_html__( 'Enable Redirection', 'premium-addons-for-elementor' ),
				'type'                  => Controls_Manager::SWITCHER,
                'description'           => esc_html__('Redirect to Photo Link on Instgram','premium-addons-for-elementor'),
			]
		);

		$this->add_control('premium_instagram_feed_new_tab',
			[
				'label'                 => esc_html__( 'Open in a New Tab', 'premium-addons-for-elementor' ),
				'type'                  => Controls_Manager::SWITCHER,
                'condition'             => [
                    'premium_instagram_feed_link'   => 'yes'
                ]
			]
		);
        
        $this->add_control('premium_instagram_feed_popup',
			[
				'label'                 => esc_html__( 'Modal Image', 'premium-addons-for-elementor' ),
				'type'                  => Controls_Manager::SWITCHER,
                'description'           => esc_html__('Modal image works only on the frontend', 'premium-addons-for-elementor'),
                'condition'             => [
                    'premium_instagram_feed_link!'   => 'yes'
                ]
			]
		);
        
        $this->add_control('premium_instagram_feed_show_likes',
			[
				'label'                 => esc_html__( 'Likes Info', 'premium-addons-for-elementor' ),
				'type'                  => Controls_Manager::SWITCHER,
			]
		);
        
        $this->add_control('premium_instagram_feed_show_comments',
			[
				'label'                 => esc_html__( 'Comments Info', 'premium-addons-for-elementor' ),
				'type'                  => Controls_Manager::SWITCHER,
			]
		);
        
        /*End General Settings Section*/
        $this->end_controls_section();
        
        /*Start Layout Settings*/
        $this->start_controls_section('premium_instagram_feed_layout_settings_section',
            [
                'label'             => esc_html__('Layout', 'premium-addons-for-elementor'),
            ]);
        
        /*Number of Images*/
        $this->add_control('premium_instagram_feed_img_number',
                [
                    'label'             => esc_html__('Maximum Images Number', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::NUMBER,
                    'default'           => 6,
                ]
                );
        
        /*Image Resolution*/
        $this->add_control('premium_instagram_feed_resolution',
                [
                    'label'             => esc_html__('Image Resolution', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::SELECT,
                    'options'           => [
                        'thumbnail'             => esc_html__('Thumbnail (150x150)', 'premium-addons-for-elementor'),
                        'low_resolution'        => esc_html__('Low (306x306)', 'premium-addons-for-elementor'),
                        'standard_resolution'   => esc_html__('Standard (612x612)', 'premium-addons-for-elementor'),
                        
                    ],
                    'default'           => 'standard_resolution',
                ]
                );
        
        $this->add_control('premium_instagram_feed_masonry',
			[
				'label'                 => esc_html__( 'Masonry', 'premium-addons-for-elementor' ),
				'type'                  => Controls_Manager::SWITCHER,
                'default'               => 'yes'
			]
		);
        
        $this->add_control('premium_instagram_feed_column_number',
                [
                    'label'             => esc_html__('Number of Columns', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::SELECT,
                    'options'           => [
                        'col-1' => esc_html__('1 Column', 'premium-addons-for-elementor'),
                        'col-2' => esc_html__('2 Columns', 'premium-addons-for-elementor'),
                        'col-3' => esc_html__('3 Columns', 'premium-addons-for-elementor'),
                        'col-4' => esc_html__('4 Columns', 'premium-addons-for-elementor'),
                        'col-5' => esc_html__('5 Columns', 'premium-addons-for-elementor'),
                        'col-6' => esc_html__('6 Columns', 'premium-addons-for-elementor'),
                    ],
                    'default'           => 'col-3',
                    ]
                );
        
        /*Hover Image Effect*/ 
        $this->add_control('premium_instagram_feed_image_hover',
                [
                    'label'         => esc_html__('Hover Image Effect', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SELECT,
                    'options'       => [
                        'none'  => esc_html__('None', 'premium-addons-for-elementor'),
                        'zoomin' => esc_html__('Zoom In', 'premium-addons-for-elementor'),
                        'zoomout'=> esc_html__('Zoom Out', 'premium-addons-for-elementor'),
                        'scale'  => esc_html__('Scale', 'premium-addons-for-elementor'),
                        'gray'   => esc_html__('Grayscale', 'premium-addons-for-elementor'),
                        'blur'   => esc_html__('Blur', 'premium-addons-for-elementor'),
                        'sepia'  => esc_html__('Sepia', 'premium-addons-for-elementor'),
                        'bright' => esc_html__('Brightness', 'premium-addons-for-elementor'),
                        'trans'   => esc_html__('Translate', 'premium-addons-for-elementor'),
                    ],
                    'default'       => 'zoomin',
                    'label_block'   => true
                ]
                );
        
        /*End Account Settings Section*/
        $this->end_controls_section();
        
        /*Start Tweet Box Section*/
        $this->start_controls_section('premium_instgram_feed_photo_box_style',
                [
                    'label'             => esc_html__('Photo Box','premium-addons-for-elementor'),
                    'tab'               => Controls_Manager::TAB_STYLE,
                    ]
            );
        
        $this->add_responsive_control('premium_instgram_feed_image_height',
                [
                    'label'         => esc_html__('Image Height', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px',"em"],
                    'range'             => [
                        'px'    => [
                            'min' => 50, 
                            'max' => 500,
                            ],
                        'em'    => [
                            'min' => 1, 
                            'max' => 100,
                            ]
                        ],
                    'condition'     => [
                        'premium_instagram_feed_masonry!'    => 'yes'
                    ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-img-wrap img' => 'min-height: {{SIZE}}{{UNIT}};'
                        ]
                    ]
                );
        
        $this->start_controls_tabs( 'premium_instgram_feed_tweet_box' );
        
        $this->start_controls_tab('premium_instgram_feed_photo_box_normal',
            [
                'label'             => esc_html__('Normal', 'premium-addons-for-elementor'),
            ]
            );
        
        /*Tweet Box Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_instgram_feed_photo_box_border',
                    'selector'      => '{{WRAPPER}} .premium-insta-img-wrap',
                ]
                );
        
        /*Tweet Box Border Radius*/
        $this->add_control('premium_instgram_feed_photo_box_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'default'       => [
                        'unit'  => 'px',
                        'size'  => 0,
                    ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-img-wrap' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        /*Tweet Box Shadow*/    
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_instgram_feed_photo_box_shadow',
                    'selector'      => '{{WRAPPER}} .premium-insta-img-wrap',
                ]
                );
        
         /*Tweet Box Margin*/
        $this->add_responsive_control('premium_instgram_feed_photo_box_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-img-wrap' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
                
        /*Tweet Box Padding*/
        $this->add_responsive_control('premium_instgram_feed_photo_box_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-img-wrap' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->end_controls_tab();


        $this->start_controls_tab('premium_instgram_feed_photo_box_hover',
            [
                'label'             => esc_html__('Hover', 'premium-addons-for-elementor'),
            ]
            );
        
        $this->add_control('premium_instgram_feed_overlay_background', 
                [
                    'label'         => esc_html__('Overlay Background', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-info-wrap' => 'background-color: {{VALUE}};',
                        ],
                    ]
                );
                
        /*Tweet Box Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_instgram_feed_photo_box_border_hover',
                    'selector'      => '{{WRAPPER}} .premium-insta-feed-wrap:hover .premium-insta-img-wrap',
                ]
                );
        
        /*Tweet Box Border Radius*/
        $this->add_control('premium_instgram_feed_photo_box_border_radius_hover',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'default'       => [
                        'unit'  => 'px',
                        'size'  => 0,
                    ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-feed-wrap:hover .premium-insta-img-wrap' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        /*Tweet Box Shadow*/
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_instgram_feed_photo_box_shadow_hover',
                    'selector'      => '{{WRAPPER}} .premium-insta-feed-wrap:hover .premium-insta-img-wrap',
                ]
                );
        
        /*Tweet Box Margin*/
        $this->add_responsive_control('premium_instgram_feed_photo_box_margin_hover',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-feed-wrap:hover .premium-insta-img-wrap' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
 
        $this->end_controls_tab();

        $this->end_controls_tabs();
            
        /*End Tweet Box Section*/
        $this->end_controls_section();

        $this->start_controls_section('premium_instgram_feed_photo_likes_style',
            [
                'label'             => esc_html__('Likes','premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
                'condition'         => [
                    'premium_instagram_feed_show_likes' => 'yes'
                ]
            ]
        );
        
        $this->start_controls_tabs( 'premium_instgram_feed_likes' );
        
        $this->start_controls_tab('premium_instgram_feed_likes_icon',
            [
                'label'             => esc_html__('Icon', 'premium-addons-for-elementor'),
            ]
            );

        /*Likes Icon Color*/
        $this->add_control('premium_instgram_feed_likes_color', 
                [
                    'label'         => esc_html__('Icon Color', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'scheme' => [
                        'type'  => Scheme_Color::get_type(),
                        'value' => Scheme_Color::COLOR_1,
                        ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-heart' => 'color: {{VALUE}};',
                        ],
                    ]
                );

        /*Likes Icon Size*/
        $this->add_responsive_control('premium_instgram_feed_likes_size',
                [
                    'label'         => esc_html__('Size', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px',"em"],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-heart' => 'font-size: {{SIZE}}{{UNIT}};'
                        ]
                    ]
                );
        
        $this->add_control('premium_instgram_feed_likes_background', 
                [
                    'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-heart' => 'background-color: {{VALUE}};',
                        ],
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_instgram_feed_likes_border',
                    'selector'      => '{{WRAPPER}} .premium-insta-heart',
                ]
                );
        
        /*Container Border Radius*/
        $this->add_control('premium_instgram_feed_likes_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'default'       => [
                        'unit'  => 'px',
                        'size'  => 0,
                    ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-heart' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        /*Container Box Shadow*/
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_instgram_feed_likes_shadow',
                    'selector'      => '{{WRAPPER}} .premium-insta-heart',
                ]
                );
        
        $this->add_responsive_control('premium_instgram_feed_likes_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-heart' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
                
        /*Container Padding*/
        $this->add_responsive_control('premium_instgram_feed_likes_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-heart' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_instgram_feed_likes_number',
            [
                'label'             => esc_html__('Number', 'premium-addons-for-elementor'),
            ]
            );
        
        /*Likes Number Color*/
        $this->add_control('premium_instgram_feed_likes_number_color', 
                [
                    'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'scheme' => [
                        'type'  => Scheme_Color::get_type(),
                        'value' => Scheme_Color::COLOR_2,
                        ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-likes' => 'color: {{VALUE}};',
                        ],
                    ]
                );
        
        /*Likes Number Typography*/
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_instgram_feed_likes_number_type',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-insta-likes',
                    ]
                );
        
        $this->add_control('premium_instgram_feed_likes_number_background', 
                [
                    'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'selectors'     => [
                        '{{WRAPPER}}  .premium-insta-likes' => 'background-color: {{VALUE}};',
                        ],
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_instgram_feed_likes_number_border',
                    'selector'      => '{{WRAPPER}} .premium-insta-likes',
                ]
                );
        
        /*Container Border Radius*/
        $this->add_control('premium_instgram_feed_likes_number_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'default'       => [
                        'unit'  => 'px',
                        'size'  => 0,
                    ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-likes' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        /*Container Box Shadow*/
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_instgram_feed_likes_number_shadow',
                    'selector'      => '{{WRAPPER}} .premium-insta-likes',
                ]
                );
        
        $this->add_responsive_control('premium_instgram_feed_likes_number_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-likes' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
                
        /*Container Padding*/
        $this->add_responsive_control('premium_instgram_feed_likes_number_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-likes' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->end_controls_tab();
        
        $this->end_controls_tabs();
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_instgram_feed_photo_comments_style',
            [
                'label'             => esc_html__('Comments','premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
                'condition'         => [
                    'premium_instagram_feed_show_comments' => 'yes'
                ]
            ]
        );
        
        $this->start_controls_tabs( 'premium_instgram_feed_comments' );
        
        $this->start_controls_tab('premium_instgram_feed_comments_icon',
            [
                'label'             => esc_html__('Icon', 'premium-addons-for-elementor'),
            ]
            );

        /*Likes Icon Color*/
        $this->add_control('premium_instgram_feed_comment_color', 
                [
                    'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'scheme' => [
                        'type'  => Scheme_Color::get_type(),
                        'value' => Scheme_Color::COLOR_1,
                        ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-comment' => 'color: {{VALUE}};',
                        ],
                    ]
                );

        /*Likes Icon Size*/
        $this->add_responsive_control('premium_instgram_feed_comment_size',
                [
                    'label'         => esc_html__('Size', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px',"em"],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-comment' => 'font-size: {{SIZE}}{{UNIT}};'
                        ]
                    ]
                );
        
        $this->add_control('premium_instgram_feed_comment_background', 
                [
                    'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-comment' => 'background-color: {{VALUE}};',
                        ],
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_instgram_feed_comments_border',
                    'selector'      => '{{WRAPPER}} .premium-insta-comment',
                ]
                );
        
        /*Likes Border Radius*/
        $this->add_control('premium_instgram_feed_comment_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'default'       => [
                        'unit'  => 'px',
                        'size'  => 0,
                    ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-comment' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        /*Likes Box Shadow*/
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_instgram_feed_comments_shadow',
                    'selector'      => '{{WRAPPER}} .premium-insta-comment',
                ]
                );
        
        $this->add_responsive_control('premium_instgram_feed_comments_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-comment' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
                
        /*Likes Padding*/
        $this->add_responsive_control('premium_instgram_feed_comments_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-comment' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_instgram_feed_comments_number',
            [
                'label'             => esc_html__('Number', 'premium-addons-for-elementor'),
            ]
            );
        
        /*Likes Number Color*/
        $this->add_control('premium_instgram_feed_comments_number_color', 
                [
                    'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'scheme' => [
                        'type'  => Scheme_Color::get_type(),
                        'value' => Scheme_Color::COLOR_2,
                        ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-comments' => 'color: {{VALUE}};',
                        ],
                    ]
                );
        
        /*Likes Number Typography*/
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_instgram_feed_comments_number_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-insta-comments',
                    ]
                );
        
        $this->add_control('premium_instgram_feed_comments_number_background', 
                [
                    'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'selectors'     => [
                        '{{WRAPPER}}  .premium-insta-comments' => 'background-color: {{VALUE}};',
                        ],
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_instgram_feed_comments_number_border',
                    'selector'      => '{{WRAPPER}} .premium-insta-comments',
                ]
                );
        
        $this->add_control('premium_instgram_feed_comments_number_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'default'       => [
                        'unit'  => 'px',
                        'size'  => 0,
                    ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-comments' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_instgram_feed_comments_number_shadow',
                    'selector'      => '{{WRAPPER}} .premium-insta-comments',
                ]
                );
        
        $this->add_responsive_control('premium_instgram_feed_comments_number_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-comments' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_responsive_control('premium_instgram_feed_comments_number_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-insta-comments' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->end_controls_tab();
        
        $this->end_controls_tabs();
        
        $this->end_controls_section();
        
        /*Start General Style Section*/
        $this->start_controls_section('premium_instgram_feed_general_style',
                [
                    'label'             => esc_html__('Container','premium-addons-for-elementor'),
                    'tab'               => Controls_Manager::TAB_STYLE,
                    ]
            );
            
        /*Container Background*/
        $this->add_control('premium_instgram_feed_container_background', 
                [
                    'label'         => esc_html__('Background', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
    				'scheme' => [
					    'type'  => Scheme_Color::get_type(),
					    'value' => Scheme_Color::COLOR_1,
					    ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-instafeed-container' => 'background-color: {{VALUE}};',
                        ],
                    ]
                );
        
        /*Container Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_instgram_feed_container_box_border',
                    'selector'      => '{{WRAPPER}} .premium-instafeed-container',
                ]
                );
        
        /*Container Border Radius*/
        $this->add_control('premium_instgram_feed_container_box_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'default'       => [
                        'unit'  => 'px',
                        'size'  => 0,
                    ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-instafeed-container' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        /*Container Box Shadow*/
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'name'          => 'premium_instgram_feed_container_box_shadow',
                    'selector'      => '{{WRAPPER}} .premium-instafeed-container',
                ]
                );
        
        /*Container Margin*/
        $this->add_responsive_control('premium_instgram_feed_container_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-instafeed-container' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
                
        /*Container Padding*/
        $this->add_responsive_control('premium_instgram_feed_container_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-instafeed-container' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*End General Style Section*/
        $this->end_controls_section();

                
    }
    
    protected function get_instagram_responsive_style() {
        
        $breakpoints = Responsive::get_breakpoints();
        $style = '<style>';
        $style .= '@media ( max-width: ' . $breakpoints['lg'] . 'px ) {';
        $style .= '.premium-insta-feed {';
        $style .= 'width: 50% !important;';
        $style .= '}';
        $style .= '}';
        $style .= '@media ( max-width: ' . $breakpoints['md'] . 'px ) {';
        $style .= '.premium-insta-feed {';
        $style .= 'width: 100% !important;';
        $style .= '}';
        $style .= '}';
        $style .= '</style>';
        
        return $style;
        
    }

    protected function render($instance = [])
    {
        // get our input from the widget settings.
        $settings = $this->get_settings();
        
        $get_from = $settings['premium_instagram_feed_search_for'];
        
        $columns_class = 'premium-insta-' . $settings['premium_instagram_feed_column_number'];
        
        $hover_effect = 'premium-insta-' . $settings['premium_instagram_feed_image_hover'];
        
        $new_tab = $settings['premium_instagram_feed_new_tab'] == 'yes' ? 'target="_blank"' : '' ;
        if($settings['premium_instagram_feed_link'] == 'yes') {
            $link = '<a href="{{link}}"' . $new_tab . '></a>';
        } else {
            if($settings['premium_instagram_feed_popup'] == 'yes') {
                $link = '<a href="{{image}}" data-rel="prettyPhoto[premium-insta-'.esc_attr($this->get_id()).']"></a>';
            } else {
                $link = '';
            }
        }
        
        if($settings['premium_instagram_feed_show_likes'] == 'yes') {
            $likes = '<p> <i class="fa fa-heart premium-insta-heart" aria-hidden="true"></i> <span  class="premium-insta-likes">{{likes}}</span></p>';
        } else {
            $likes = '';
        }
        
        if($settings['premium_instagram_feed_show_comments'] == 'yes') {
            $comments = '<p><i class="fa fa-comment premium-insta-comment" aria-hidden="true"></i><span class="premium-insta-comments">{{comments}}</span></p>';
        } else {
            $comments = '';
        }
        
        $client_id = !empty( $settings['premium_instagram_feed_client_id'] ) ? $settings['premium_instagram_feed_client_id'] : '';
        $access_token = !empty( $settings['premium_instagram_feed_client_access_token'] ) ? $settings['premium_instagram_feed_client_access_token'] : '';
        $location = !empty( $settings['premium_instagram_feed_location_id'] ) ? $settings['premium_instagram_feed_location_id'] : '';
        $user = !empty( $settings['premium_instagram_feed_user_id'] ) ? $settings['premium_instagram_feed_user_id'] : '';
        $tag = !empty( $settings['premium_instagram_feed_tag_name'] ) ? $settings['premium_instagram_feed_tag_name'] : '';
        $sort = $settings['premium_instagram_feed_sort_by'];
        $res = $settings['premium_instagram_feed_resolution'];
        $limit  = !empty( $settings['premium_instagram_feed_img_number'] ) ? $settings['premium_instagram_feed_img_number'] : 6;
        $instagram_settings = [
            'clientId'      => $client_id,
            'accesstok'     => $access_token,
            'get'           => $get_from,
            'location'      => $location,
            'user'          => $user,
            'tag'           => $tag,
            'sort'          => $sort,
            'limit'         => $limit,
            'res'           => $res,
            'hoverEff'      => $hover_effect,
            'likes'         => $likes,
            'comments'      => $comments,
            'link'          => $link,
            'id'            => 'premium-instafeed-container-' . $this->get_id(),
            'masonry'       => ($settings['premium_instagram_feed_masonry'] == 'yes') ? true : false
        ];
?>
    

    <?php if( empty ( $settings['premium_instagram_feed_client_id'] ) || empty ( $settings['premium_instagram_feed_client_access_token'] ) ) : ?>
        <div class="premium-fbrev-error">
                <?php echo esc_html__('Please fill the required fields: Consumer Key & Consumer Secret','premium-addons-for-elementor'); ?>
        </div>
        <?php else: ?>
        <div class="premium-instafeed-container <?php echo $columns_class; ?>" data-settings='<?php echo wp_json_encode($instagram_settings); ?>'>
            <div id="premium-instafeed-container-<?php echo esc_attr($this->get_id()); ?>" class="premium-insta-grid"></div>
            <div class="premium-loading-feed">
                <div class="premium-loader"></div>
            </div>
        </div>
    <?php echo $this->get_instagram_responsive_style(); ?>
    <?php endif; ?>

    <?php
        
    }
}