<?php

/**
 * Class: Premium_Facebook_Feed_Widget
 * Name: Facebook Feed
 * Slug: premium-facebook-feed
 */

namespace Elementor;

use Elementor\Core\Responsive\Responsive;

if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Premium_Facebook_Feed extends Widget_Base {
	
    public function get_name() {
        return 'premium-facebook-feed';
    }
    
    public function get_title() {
		return \PremiumAddons\Helper_Functions::get_prefix() . ' Facebook Feed';
	}

    public function get_icon() {
        return 'pa-pro-facebook-feed';
    }
    
    public function get_categories() {
        return [ 'premium-elements' ];
    }
    
    public function get_script_depends() {
        return [
            'codebird-js',
            'dot-js',
            'moment-js',
            'jquery-socialfeed-js',
            'masonry-js',
            'premium-pro-js',
        ];
    }
    
    public function is_reload_preview_required() {
        return true;
    }
   
    // Adding the controls fields for the Facebook Feed
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {
    
        $this->start_controls_section('access_credentials_section',
            [
                'label'         => esc_html__('Access Credentials', 'premium-addons-for-elementor')
            ]
        );

        $this->add_control('access_token',
            [
                'label'         => esc_html__('Access Token', 'premium-addons-for-elementor'),
                'default'       => 'EAAVVPjFKgSEBAMV3amGyeedKiUj4KVPD0JNzkdu0wK7fUitmH4wZCScqGBxZCnqLRBf3ZABVn2165jtrk3zbZCRf6rpn9mDVznKsIOGsH4PZArW7gXAILZB8hKzqcZCPSZB5ZAZC9vAO4MUReXxZAqVweJxI5HriqX2GrHb07pkh4v01wZDZD',
                'description'   => 'Click <a href="https://www.youtube.com/watch?v=Zb8YWXlXo-k" target="_blank">Here</a> to know how to get your page access token',
                'type'          => Controls_Manager::TEXT,
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('account_settings_section',
            [
                'label'         => esc_html__('Account', 'premium-addons-for-elementor')
            ]
        );

        $this->add_control('account_id',
            [
                'label'         => esc_html__( 'Page Slug /User ID', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::TEXT,
                'default'       => '!leap13themes',
                'description'   => 'Account ID is prefixed by @, Page Slug is prefixed by !, click <a target="_blank" href="https://findmyfbid.com/"> here</a> to get account ID',
                'label_block'   => true,
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('page_settings',
            [
                'label'         => esc_html__( 'Layout', 'premium-addons-for-elementor' ),
            ]
        );

        $this->add_control('layout_style',
            [
                'label'         => esc_html__('Style', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'description'   => esc_html__( 'Choose the layout style for the posts', 'premium-addons-for-elementor' ),
                'options'       => [
                    'list'          => esc_html__('List', 'premium-addons-for-elementor'),
                    'masonry'       => esc_html__('Masonry', 'premium-addons-for-elementor'),
                ],
                'default'       => 'list',
            ]
        );

        $this->add_control('column_number',
            [
               'label'          => esc_html__( 'Number of Columns', 'premium-addons-for-elementor' ),
               'type'           => Controls_Manager::SELECT,
               'options'        => [
                    'two'           => esc_html__('2 Columns', 'premium-addons-for-elementor'),
                    'three'         => esc_html__('3 Columns', 'premium-addons-for-elementor'),
                    'four'          => esc_html__('4 Columns', 'premium-addons-for-elementor'),
                    'five'          => esc_html__('5 Columns', 'premium-addons-for-elementor'),
                    'six'           => esc_html__('6 Columns', 'premium-addons-for-elementor'),
                ],
                'default'       => 'two',
                'condition'     => [
                    'layout_style' => 'masonry'
                ],
            ]                                                                                
        );

        $this->add_control('direction',
            [
                'label'         => esc_html__( 'Direction', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'ltr'    => [
                        'title' => __( 'Left to Right', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-chevron-circle-right',
                    ],
                    'rtl'   => [
                        'title' => __( 'Right to Left', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-chevron-circle-left',
                    ],
                ],
                'default'       => 'ltr',
            ]
        );

        $this->add_responsive_control('align',
            [
                'label'         => esc_html__( 'Content Alignment', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'          => [
                        'title' => __( 'Left', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-left',
                    ],
                    'center'        => [
                        'title' => __( 'Center', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-center',
                    ],
                    'right'          => [
                        'title' => __( 'Right', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-right',
                    ],
                    'justify'       => [
                        'title' => esc_html__( 'Justify', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-justify',
                    ],
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-text, {{WRAPPER}} .premium-read-button' => 'text-align: {{VALUE}}',
                ],
                'default'       => 'left',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('layout_settings',
            [
                'label'         => esc_html__('Advanced Settings', 'premium-addons-for-elementor' )
            ]
        );

        $this->add_control('post_number',
            [
                'label'         => esc_html__( 'Posts/Account', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::NUMBER,
                'label_block'   => false,
                'description'       => esc_html__( 'How many posts will be shown for each account', 'premium-addons-for-elementor' ),
                'default'       => 5
            ]
        );
        
        $this->add_control('content_length',
            [
                'label'         => esc_html__('Post Length','premium-addons-for-elementor'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => 200,
            ]
        );

        $this->add_control('posts_media',
            [
                'label'         => esc_html__('Show Post Media','premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'default'       => 'yes',
            ]
        );

        $this->add_control('show_avatar', 
            [
                'label'             => esc_html__('Show Avatar','premium-addons-for-elementor'),
                'type'              => Controls_Manager::SELECT,
                'options'           => [
                    'block' => esc_html__('Show', 'premium-addons-for-elementor'),
                    'none'  => esc_html__('Hide', 'premium-addons-for-elementor'),
                ],
                'default'           => 'block',
                'selectors'         => [
                    '{{WRAPPER}} .premium-author-img'   => 'display: {{VALUE}}'
                ]
            ]
        );
        
        $this->add_control('show_date', 
            [
                'label'             => esc_html__('Show Date','premium-addons-for-elementor'),
                'type'              => Controls_Manager::SELECT,
                'options'           => [
                    'block' => esc_html__('Show', 'premium-addons-for-elementor'),
                    'none'  => esc_html__('Hide', 'premium-addons-for-elementor'),
                ],
                'default'           => 'block',
                'selectors'         => [
                    '{{WRAPPER}} .premium-social-date'   => 'display: {{VALUE}}'
                ]
            ]
        );

        $this->add_control('read', 
            [
                'label'             => esc_html__('Show Read More Button','premium-addons-for-elementor'),
                'type'              => Controls_Manager::SELECT,
                'options'           => [
                    'block' => esc_html__('Show', 'premium-addons-for-elementor'),
                    'none'  => esc_html__('Hide', 'premium-addons-for-elementor'),
                ],
                'default'           => 'block',
                'selectors'         => [
                    '{{WRAPPER}} .premium-read-button'   => 'display: {{VALUE}}'
                ]
            ]
        );
        
        $this->add_control('show_icon',
            [
                'label'             => esc_html__('Show Facebook Icon','premium-addons-for-elementor'),
                'type'              => Controls_Manager::SELECT,
                'options'           => [
                    'inline-block' => esc_html__('Show', 'premium-addons-for-elementor'),
                    'none'  => esc_html__('Hide', 'premium-addons-for-elementor'),
                ],
                'default'           => 'inline-block',
                'selectors'         => [
                    '{{WRAPPER}} .premium-social-icon'   => 'display: {{VALUE}}'
                ]
            ]
        );


        $this->end_controls_section();


        $this->start_controls_section('post_box_style',
            [
                'label'         => esc_html__('Post Box','premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->start_controls_tabs( 'post_box' );

        $this->start_controls_tab('post_box_normal',
            [
                'label'         => esc_html__('Normal', 'premium-addons-for-elementor'),
            ]
        );

        $this->add_control('post_box_background', 
            [
                'label'         => esc_html__('Background', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-element' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'post_box_border',
                'selector'      => '{{WRAPPER}} .premium-social-feed-element',
            ]
        );

        $this->add_control('post_box_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'default'       => [
                    'unit'  => 'px',
                    'size'  => 0,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-element' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'post_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-social-feed-element',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab('post_box_hover',
            [
                'label'         => esc_html__('Hover', 'premium-addons-for-elementor'),
            ]
        );

        $this->add_control('post_box_background_hover', 
            [
                'label'         => esc_html__('Background', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-element:hover' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'post_box_border_hover',
                'selector'      => '{{WRAPPER}} .premium-social-feed-element:hover',
            ]
        );

        $this->add_control('post_box_border_radius_hover',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'default'       => [
                    'unit'  => 'px',
                    'size'  => 0,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-element:hover' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'post_box_shadow_hover',
                'selector'      => '{{WRAPPER}} .premium-social-feed-element:hover',
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_responsive_control('post_box_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'separator'     => 'before',
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-element' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control('post_box_padding',
                [
                    'label'     => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'      => Controls_Manager::DIMENSIONS,
                    'size_units'=> ['px', 'em', '%'],
                    'selectors' => [
                        '{{WRAPPER}} .premium-social-feed-element' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('content_style',
            [
                'label'         => esc_html__('Content','premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control('content_color', 
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-text' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'facebook_feed_content_typography',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-social-feed-text',
            ]
        );

        $this->add_responsive_control('facebook_feed_content_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-text' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_control('facebook_feed_read_more_heading',
            [
                'label'         => esc_html__('Read More', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::HEADING,
                'condition'     => [
                    'read'  => 'yes'
                ],
            ]
        );

        $this->add_control('read_more_color', 
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                    ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-read-button' => 'color: {{VALUE}};',
                ],
                'condition'     => [
                    'read'  => 'yes'
                ],
            ]
        );

        $this->add_control('read_more_color_hover', 
            [
                'label'         => esc_html__('Hover Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_3,
                    ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-read-button:hover' => 'color: {{VALUE}};',
                ],
                'condition'     => [
                    'read'  => 'yes'
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'facebook_feed_read_more_typography',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-read-button',
                'condition'     => [
                    'read'  => 'yes'
                ],
            ]
        );


        $this->end_controls_section();

        $this->start_controls_section('avatar_style',
            [
                'label'         => esc_html__('Avatar','premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'show_avatar'    => 'yes'
                ]
            ]
        );

        $this->add_responsive_control('avatar_size',
            [
                'label'         => esc_html__('Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em", '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-feed-element .media-object ' => 'width: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'avatar_border',
                'selector'      => '{{WRAPPER}} .premium-author-img img',
            ]
        );

        $this->add_control('avatar_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em", '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-author-img img' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control('avatar_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-author-img img' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('icon_style',
            [
                'label'         => esc_html__('Facebook Icon','premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'show_icon'    => 'yes'
                ]
            ]
        );

        $this->add_control('facebook_icon_color', 
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-icon' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control('facebook_icon_size',
            [
                'label'         => esc_html__('Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px',"em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-icon' => 'font-size: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control('icon_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('title_style',
            [
                'label'         => esc_html__('Author','premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control('title_color', 
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-author-title a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control('title_hover_color', 
            [
                'label'         => esc_html__('Hover Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-author-title:hover a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'title_typography',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-social-author-title a',
            ]
        ); 

        $this->add_responsive_control('title_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-author-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('date_style',
            [
                'label'         => esc_html__('Date','premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'show_date'  => 'yes'
                ]
            ]
        );

        $this->add_control('date_color', 
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-date a' => 'color: {{VALUE}};',
                ],
                'separator'     => 'before',
            ]
        );

        $this->add_control('date_hover_color',
            [
                'label'         => esc_html__('Hover Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-date:hover a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'date_typography',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-social-date a',
            ]
        ); 

        $this->add_responsive_control('date_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-date' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('general_style',
            [
                'label'         => esc_html__('Container','premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control('container_background', 
            [
                'label'         => esc_html__('Background', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-facebook-feed-wrapper' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'container_box_border',
                'selector'      => '{{WRAPPER}} .premium-facebook-feed-wrapper',
            ]
        );

        $this->add_control('container_box_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-facebook-feed-wrapper' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'container_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-facebook-feed-wrapper',
            ]
        );

        $this->add_responsive_control('container_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-facebook-feed-wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control('container_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-facebook-feed-wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_section();

	}
    
    /**
     * returns the responsive style based on Elementor's Breakpoints
	 * @access protected
	 * @return string
	 */
    protected function get_facebook_responsive_style() {
        
        $breakpoints = Responsive::get_breakpoints();

        $style .= '@media ( max-width: ' . $breakpoints['lg'] . 'px ) {';
        $style .= '.premium-social-feed-element-wrap {';
        $style .= 'width: 50% !important;';
        $style .= '}';
        $style .= '}';
        $style .= '@media ( max-width: ' . $breakpoints['md'] . 'px ) {';
        $style .= '.premium-social-feed-element-wrap {';
        $style .= 'width: 100% !important;';
        $style .= '}';
        $style .= '}';
        
        return $style;
        
    }

    /**
	 * renders the HTML content of the widget
	 * @return void
	 */
	protected function render() {

      	$settings = $this->get_settings();
        
        $layout_class = $settings['layout_style'] == 'list' ? 'list-layout' : 'grid-layout';
        
        $template = $settings['layout_style'] == 'list' ? 'list-template.php' : 'grid-template.php';

        $direction = $settings['direction'];

        $limit = !empty( $settings['post_number'] ) ? $settings['post_number'] : 2;
        
        $post_length = !empty( $settings['content_length'] ) ? $settings['content_length'] : 130;
        
        $show_media = ( $settings['posts_media'] == 'yes' ) ? true : false;
        
        $facebook_settings = [
            'accounts'  => esc_html($settings['account_id']),
            'limit'     => $limit,
            'accessTok' => esc_html($settings['access_token']),
            'length'    => $post_length,
            'showMedia' => $show_media,
            'layout'    => $layout_class,
            'template'  => plugins_url( '/templates/', __FILE__ ) . $template,
        ];
      
	?>

<?php if( empty ( $settings['access_token'] ) ) : ?>
    <div class="premium-fbrev-error">
            <?php echo esc_html__('Please fill the required fields: App ID & Access Token','premium-addons-for-elementor'); ?>
    </div>
<?php else: ?>
	<div class="premium-facebook-feed-wrapper <?php echo esc_attr( $settings['column_number'] ) . ' ' . esc_attr( $direction ); ?>" data-settings='<?php echo wp_json_encode( $facebook_settings ); ?>'>
		<div id="<?php echo 'premium-social-feed-container-' . esc_attr($this->get_id() ); ?>"class="premium-social-feed-container <?php echo esc_attr( $layout_class );?>"></div>
        <div class="premium-loading-feed">
            <div class="premium-loader"></div>
        </div>
	</div>
<?php endif; ?>
	
	<?php
		echo '<style>' . $this->get_twitter_responsive_style() . '</style>';
	}
}