<?php 

namespace Elementor;

if(!defined('ABSPATH')) exit; // If this file is called directly, abort.

class Premium_Unfold extends Widget_Base {
    
    public function get_name(){
        return 'premium-unfold-addon';
    }
    
    public function get_title() {
		return \PremiumAddons\Helper_Functions::get_prefix() . ' Unfold';
	}
    
    public function get_icon() {
        return 'pa-pro-unfold';
    }
    
    public function is_reload_preview_required(){
        return true;
    }
    
    public function get_script_depends(){
        return [
            'jquery-ui',
            'premium-pro-js'
            ];
    }
    
    public function get_categories() {
        return [ 'premium-elements' ];
    }
    
    // Adding the controls fields for the premium unfold
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {
        
        $this->start_controls_section('premium_unfold_general_settings',
                [
                    'label'         => esc_html__('Content', 'premium-addons-for-elementor'),
                    ]
                );
        
        $this->add_control('premium_unfold_title_switcher',
            [
                'label'         => esc_html__('Title', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes'
            ]
        );
        
        $this->add_control('premium_unfold_title', 
            [
                'label'         => esc_html__('Title','premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'dynamic'       => [ 'active' => true ],
                'default'       =>'Premium Unfold',
                'condition'     => [
                    'premium_unfold_title_switcher'    => 'yes'
                ]
            ]
        );
        
        $this->add_control('premium_unfold_title_heading', 
            [
                'label'         => esc_html__('Title Heading','premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'h3',
                'options'       =>[
                    'h1'    => 'H1',
                    'h2'    => 'H2',
                    'h3'    => 'H3',
                    'h4'    => 'H4',
                    'h5'    => 'H5',
                    'h6'    => 'H6',
                ],
                'condition'     => [
                    'premium_unfold_title_switcher'    => 'yes'
                ]
            ]
        );
        
        $this->add_control('premium_unfold_content',
                [
                    'type'          => Controls_Manager::WYSIWYG,
                    'default'       => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'premium-addons-for-elementor'),
                    'dynamic'       => [ 'active' => true ],
                    ]
                );
        
        $this->add_responsive_control('premium_unfold_content_align',
			[
				'label'             => esc_html__( 'Alignment', 'premium-addons-for-elementor' ),
				'type'              => Controls_Manager::CHOOSE,
				'options'           => [
					'left'    => [
						'title' => __( 'Left', 'premium-addons-for-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'premium-addons-for-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'premium-addons-for-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
                    'justify'   => [
                        'title'=> esc_html__( 'Justify', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-justify',
                    ],
				],
                'selectors'         => [
                    '{{WRAPPER}} .premium-unfold-content,{{WRAPPER}} .premium-unfold-heading' => 'text-align: {{VALUE}}',
                ],
				'default' => 'left',
			]
		);
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_unfold_button_settings',
                [
                    'label'         => esc_html__('Button', 'premium-addons-for-elementor'),
                    ]
                );
        
        $this->add_control('premium_unfold_button_fold_text',
                [
                    'label'         => esc_html__('Unfold Text', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'dynamic'       => [ 'active' => true ],
                    'default'       => esc_html__('Show more', 'premium-addons-for-elementor'),
                    ]
                );
        
        $this->add_control('premium_unfold_button_unfold_text',
                [
                    'label'         => esc_html__('Fold Text', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'dynamic'       => [ 'active' => true ],
                    'default'       => esc_html__('Show Less', 'premium-addons-for-elementor'),
                    ]
                );
        
        $this->add_control('premium_unfold_button_icon_switcher',
                [
                    'label'         => esc_html__('Icon', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SWITCHER,
                    'description'   => esc_html__('Enable or disable button icon','premium-addons-for-elementor'),
                    'separator'     => 'before'
                ]
                );

        /*Button Icon Selection*/ 
        $this->add_control('premium_unfold_button_icon',
                [
                    'label'         => esc_html__('Fold Icon', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::ICON,
                    'default'       => 'fa fa-arrow-up',
                    'condition'     => [
                        'premium_unfold_button_icon_switcher' => 'yes',
                    ],
                    'label_block'   => true,
                ]
                );
        
        $this->add_control('premium_unfold_button_icon_unfolded',
                [
                    'label'         => esc_html__('Unfold Icon', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::ICON,
                    'default'       => 'fa fa-arrow-down',
                    'condition'     => [
                        'premium_unfold_button_icon_switcher' => 'yes',
                    ],
                    'label_block'   => true,
                ]
                );
        
        $this->add_control('premium_unfold_button_icon_position', 
                [
                    'label'         => esc_html__('Icon Position', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SELECT,
                    'default'       => 'before',
                    'options'       => [
                        'before'        => esc_html__('Before'),
                        'after'         => esc_html__('After'),
                        ],
                    'condition'     => [
                        'premium_unfold_button_icon_switcher' => 'yes',
                    ],
                    'label_block'   => true,
                    ]
                );
        
        $this->add_control('premium_unfold_button_size', 
                [
                    'label'         => esc_html__('Button Size', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SELECT,
                    'default'       => 'sm',
                    'options'       => [
                            'sm'            => esc_html__('Small'),
                            'md'            => esc_html__('Medium'),
                            'lg'            => esc_html__('Large'),
                            'block'         => esc_html__('Block'),
                        ],
                    'label_block'   => true,
                    'separator'     => 'before',
                    ]
                );
        
        $this->add_control('premium_unfold_button_position', 
                [
                    'label'         => esc_html__('Button Position', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SELECT,
                    'default'       => 'inside',
                    'options'       => [
                        'inside'            => esc_html__('Inside'),
                        'outside'           => esc_html__('Outside'),
                        ],
                    'label_block'   => true,
                    'separator'     => 'before',
                    ]
                );
        
        $this->add_responsive_control('premium_unfold_button_align',
			[
				'label'             => esc_html__( 'Alignment', 'premium-addons-for-elementor' ),
				'type'              => Controls_Manager::CHOOSE,
				'options'           => [
					'left'    => [
						'title' => __( 'Left', 'premium-addons-for-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'premium-addons-for-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'premium-addons-for-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
				],
                'selectors'         => [
                    '{{WRAPPER}} .premium-unfold-button-container' => 'text-align: {{VALUE}}',
                ],
				'default' => 'center',
			]
		);
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_unfold_sep_settings',
                [
                    'label'         => esc_html__('Fade Effect', 'premium-addons-for-elementor'),
                    ]
                );
        
        $this->add_control('premium_unfold_sep_switcher',
                [
                    'label'         => esc_html__('Faded Content', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SWITCHER,
                    'default'       => 'yes',
                ]
                );
        
        $this->add_control('premium_unfold_sep_height', 
                    [
                        'label'     => esc_html__('Fade Height', 'premium-addons-for-elementor'),
                        'type'      => Controls_Manager::SLIDER,
                        'description'=> esc_html__('Increase or decrease fade height. The default value is 30px','premium-addons-for-elementor'),
                        'range'     => [
                            'px'    => [
                                'min'   => 1,
                                'max'   => 400,
                            ],
                        ],
                        'default'   => [
                            'size'=> 30,
                            'unit'=> 'px'
                        ],
                        'condition' => [
                            'premium_unfold_sep_switcher'  => 'yes'
                        ]
                    ]
                    );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_unfold_adv_settings',
                [
                    'label'         => esc_html__('Advanced Settings', 'premium-addons-for-elementor'),
                    ]
                );

        $this->add_control('premium_unfold_fold_height_select',
                [
                    'label'         => esc_html__('Fold Height', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SELECT,
                    'default'       => 'percent',
                    'options'       => [
                        'percent' => esc_html__('Percentage','premium-addons-for-elementor'),
                        'pixel'   => esc_html__('Pixels','premium-addons-for-elementor'),
                        ],
                    'label_block'   =>  true,
                    'separator'     => 'before'
                    ]
                );
        
        $this->add_control('premium_unfold_fold_height',
            [
                'label'             => esc_html__('Fold Height', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::NUMBER,
                'description'   => esc_html__('How much of the folded content should be shown, default is 60%','premium-addons-for-elementor'),
                'min'               => 0,
                'default'           => 60,
                'condition'         => [
                    'premium_unfold_fold_height_select' => 'percent'
                ]
            ]);

        $this->add_control('premium_unfold_fold_height_pix',
            [
                'label'             => esc_html__('Fold Height', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::NUMBER,
                'description'   => esc_html__('How much of the folded content should be shown, default is 100px','premium-addons-for-elementor'),
                'min'               => 0,
                'default'           => 100,
                'condition'         => [
                    'premium_unfold_fold_height_select' => 'pixel'
                ]
            ]);
        
        $this->add_control('premium_unfold_fold_dur_select',
                [
                    'label'         => esc_html__('Fold Duration', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SELECT,
                    'default'       => 'fast',
                    'options'       => [
                        'slow'    => esc_html__('Slow','premium-addons-for-elementor'),
                        'fast'    => esc_html__('Fast','premium-addons-for-elementor'),
                        'custom'  => esc_html__('Custom','premium-addons-for-elementor'),
                        ],
                    'label_block'   =>  true,
                    'separator'     => 'before'
                    ]
                );
        
        $this->add_control('premium_unfold_fold_dur',
            [
                'label'             => esc_html__('Number of Seconds', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::NUMBER,
                'description'       => esc_html__('How much time does it take for the fold, default is 0.5s','premium-addons-for-elementor'),
                'min'               => 0.1,
                'default'           => 0.5,
                'condition'         => [
                    'premium_unfold_fold_dur_select' => 'custom'
                ]
            ]);
        
        $this->add_control('premium_unfold_fold_easing',
                [
                    'label'         => esc_html__('Fold Easing', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SELECT,
                    'default'       => 'swing',
                    'options'       => [
                        'swing' => 'Swing',
                        'linear'=> 'Linear',
                        ],
                    'label_block'   =>  true,
                    ]
                );
        
        $this->add_control('premium_unfold_unfold_dur_select',
                [
                    'label'         => esc_html__('Unfold Duration', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SELECT,
                    'default'       => 'fast',
                    'options'       => [
                        'slow'    => 'Slow',
                        'fast'    => 'Fast',
                        'custom'  => 'Custom',
                        ],
                    'label_block'   =>  true,
                    'separator'     => 'before',
                    ]
                );
        
        $this->add_control('premium_unfold_unfold_dur',
            [
                'label'             => esc_html__('Number of Seconds', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::NUMBER,
                'description'       => esc_html__('How much time does it take for the unfold, default is 0.5s','premium-addons-for-elementor'),
                'min'               => 0.1,
                'default'           => 0.5,
                'condition'         => [
                    'premium_unfold_unfold_dur_select' => 'custom'
                ]
            ]);
        
        $this->add_control('premium_unfold_unfold_easing',
                [
                    'label'         => esc_html__('Unfold Easing', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SELECT,
                    'description'   => esc_html__('Choose the animation style','premium-addons-for-elementor'),
                    'default'       => 'swing',
                    'options'       => [
                        'swing' => 'Swing',
                        'linear'=> 'Linear',
                        ],
                    'label_block'   =>  true,
                    ]
                );
        
        $this->end_controls_section();
        
        /*Start Box Style Settings*/
        $this->start_controls_section('premium_unfold_style_settings',
                [
                    'label'         => esc_html__('Box Settings', 'premium-addons-for-elementor'),
                    'tab'           => Controls_Manager::TAB_STYLE,
                ]
                );
        
        $this->start_controls_tabs('premium_unfold_box_style_tabs');
        
        $this->start_controls_tab('premium_unfold_box_style_normal',
                [
                    'label'         => esc_html__('Normal', 'premium-addons-for-elementor'),
                ]
                );
        
        /*Box Background*/
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_unfold_box_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-unfold-container',
                    ]
                );
        
        /*Box Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_unfold_box_border',
                    'selector'      => '{{WRAPPER}} .premium-unfold-container',
                ]
                );
        
        /*Box Border Radius*/
        $this->add_control('premium_unfold_box_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-unfold-container' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        /*Button Shadow*/
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'name'          => 'premium_unfold_box_shadow',
                    'selector'      => '{{WRAPPER}} .premium-unfold-container',
                ]
                );
        
        /*Box Margin*/
        $this->add_responsive_control('premium_unfold_box_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-unfold-container' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Box Padding*/
        $this->add_responsive_control('premium_unfold_box_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-unfold-container' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->end_controls_tab();

        $this->start_controls_tab('premium_unfold_box_style_hover',
        [
            'label'         => esc_html__('Hover', 'premium-addons-for-elementor'),
	        ]
        );
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_unfold_box_background_hover',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-unfold-container:hover',
                    ]
                );
        
        
        /*Box Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_unfold_box_border_hover',
                    'selector'      => '{{WRAPPER}} .premium-unfold-container:hover',
                ]
                );
        
        /*Box Border Radius*/
        $this->add_control('premium_unfold_box_border_radius_hover',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', 'em' , '%' ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-unfold-container:hover' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        /*Box Shadow*/
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'name'          => 'premium_unfold_box_shadow_hover',
                    'selector'      => '{{WRAPPER}} .premium-unfold-container:hover',
                ]
                );
        
        /*Box Margin*/
        $this->add_responsive_control('premium_unfold_box_margin_hover',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-unfold-container:hover' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Box Padding*/
        $this->add_responsive_control('premium_unfold_box_padding_hover',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-unfold-container:hover' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        

        $this->end_controls_tab();
        
        $this->end_controls_tabs();
        
        /*End Box Style Settings*/
        $this->end_controls_section();
        
        $this->start_controls_section('premium_unfold_title_style',
                [
                    'label'         => esc_html__('Title', 'premium-addons-for-elementor'),
                    'tab'           => Controls_Manager::TAB_STYLE,
                    'condition'     => [
                        'premium_unfold_title_switcher' => 'yes'
                    ]
                ]
                );
        
        /*Title Color*/
        $this->add_control('premium_unfold_heading_color',
                [
                    'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'scheme'        => [
                        'type'  => Scheme_Color::get_type(),
                        'value' => Scheme_Color::COLOR_2,
                    ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-unfold-heading'  => 'color: {{VALUE}};'
                        ]
                    ]
                );
        
        /*Title Typography*/
        $this->add_group_control(
            Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_unfold_heading_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-unfold-heading',
                ]
            );
        
        /*Title Background*/
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_unfold_heading_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-unfold-heading',
                    ]
                );
        
        /*Title Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_unfold_title_border',
                    'selector'      => '{{WRAPPER}} .premium-unfold-heading',
                ]
                );
        
        /*Title Border Radius*/
        $this->add_control('premium_unfold_title_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-unfold-heading' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_unfold_title_shadow',
                    'selector'      => '{{WRAPPER}} .premium-unfold-heading',
                ]
                );
        
        /*TItle Margin*/
        $this->add_responsive_control('premium_unfold_title_margin',
                [
                    'label'             => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::DIMENSIONS,
                    'size_units'        => ['px', 'em', '%'],
                    'selectors'         => [
                    '{{WRAPPER}} .premium-unfold-heading' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]      
        );
        
        /*Title Padding*/
        $this->add_responsive_control('premium_unfold_title_padding',
                [
                    'label'             => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::DIMENSIONS,
                    'size_units'        => ['px', 'em', '%'],
                    'selectors'         => [
                    '{{WRAPPER}} .premium-unfold-heading' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]      
        );
        
        /*End Content Style Settings*/
        $this->end_controls_section();
        
        $this->start_controls_section('premium_unfold_content_style',
                [
                    'label'         => esc_html__('Content', 'premium-addons-for-elementor'),
                    'tab'           => Controls_Manager::TAB_STYLE,
                ]
                );
        
                /*Description Color*/
        $this->add_control('premium_pricing_desc_color',
                [
                    'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'scheme'        => [
                        'type'  => Scheme_Color::get_type(),
                        'value' => Scheme_Color::COLOR_1,
                    ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-unfold-content'  => 'color: {{VALUE}};'
                        ]
                    ]
                );
        
        /*Description Typography*/
        $this->add_group_control(
            Group_Control_Typography::get_type(),
                [
                    'name'          => 'unfold_content_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-unfold-content',
                ]
            );
        
        /*Description Background*/
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_unfold_content_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-unfold-content',
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_unfold_content_shadow',
                    'selector'      => '{{WRAPPER}} .premium-unfold-content',
                ]
                );
        
        /*Description Margin*/
        $this->add_responsive_control('premium_unfold_content_margin',
                [
                    'label'             => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::DIMENSIONS,
                    'size_units'        => ['px', 'em', '%'],
                    'selectors'         => [
                    '{{WRAPPER}} .premium-unfold-content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]      
        );
        
        /*Description Padding*/
        $this->add_responsive_control('premium_unfold_content_padding',
                [
                    'label'             => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::DIMENSIONS,
                    'size_units'        => ['px', 'em', '%'],
                    'selectors'         => [
                    '{{WRAPPER}} .premium-unfold-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]      
        );
        
        /*End Content Style Settings*/
        $this->end_controls_section();
        
        /*Start Styling Section*/
        $this->start_controls_section('premium_unfold_button_style_section',
            [
                'label'             => esc_html__('Button', 'premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
            ]
        );
        
        $this->add_control('premium_unfold_button_icon_size',
                [
                    'label'             => esc_html__('Icon Size', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::SLIDER,
                    'size_units'        => ['px','em','%'],
                    'selectors'         => [
                        '{{WRAPPER}} .premium-button i' => 'font-size: {{SIZE}}{{UNIT}};',
                        ],
                    'condition'         => [
                            'premium_unfold_button_icon_switcher'   => 'yes'
                        ]
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'              => 'premium_unfold_button_typo',
                'scheme'            => Scheme_Typography::TYPOGRAPHY_1,
                'selector'          => '{{WRAPPER}} .premium-button',
            ]
            );
        
        $this->start_controls_tabs('premium_unfold_button_style_tabs');
        
        $this->start_controls_tab('premium_unfold_button_style_normal',
            [
                'label'             => esc_html__('Normal', 'premium-addons-for-elementor'),
            ]
            );
        
        $this->add_control('premium_unfold_button_text_color_normal',
            [
                'label'             => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'         => [
                    '{{WRAPPER}} .premium-button span'   => 'color: {{VALUE}};',
                ]
            ]);
        
        $this->add_control('premium_unfold_button_icon_color_normal',
            [
                'label'             => esc_html__('Icon Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'         => [
                    '{{WRAPPER}} .premium-button i'   => 'color: {{VALUE}};',
                ],
                'condition'         => [
                    'premium_unfold_button_icon_switcher'  => 'yes',
                ]
            ]);
        
        $this->add_control('premium_unfold_button_background_normal',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'scheme'            => [
                        'type'  => Scheme_Color::get_type(),
                        'value' => Scheme_Color::COLOR_1,
                    ],
                    'selectors'      => [
                        '{{WRAPPER}} .premium-button'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Button Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_unfold_button_border_normal',
                    'selector'      => '{{WRAPPER}} .premium-button',
                ]
                );
        
        /*Button Border Radius*/
        $this->add_control('premium_unfold_button_border_radius_normal',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-button' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        /*Icon Shadow*/
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Icon Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_unfold_button_icon_shadow_normal',
                    'selector'      => '{{WRAPPER}} .premium-button i',
                    'condition'         => [
                            'premium_unfold_button_icon_switcher'  => 'yes',
                        ]
                    ]
                );
        
        /*Text Shadow*/
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Text Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_unfold_button_text_shadow_normal',
                    'selector'      => '{{WRAPPER}} .premium-button span',
                    ]
                );
        
        /*Button Shadow*/
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'label'         => esc_html__('Button Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_unfold_button_box_shadow_normal',
                    'selector'      => '{{WRAPPER}} .premium-button',
                ]
                );
        
        /*Button Margin*/
        $this->add_responsive_control('premium_unfold_button_margin_normal',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-button' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Button Padding*/
        $this->add_responsive_control('premium_unfold_button_padding_normal',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_unfold_button_style_hover',
            [
                'label'             => esc_html__('Hover', 'premium-addons-for-elementor'),
            ]
            );
        
        $this->add_control('premium_unfold_button_text_color_hover',
            [
                'label'             => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'         => [
                    '{{WRAPPER}} .premium-button:hover span'   => 'color: {{VALUE}};',
                ],
            ]);
        
        $this->add_control('premium_unfold_button_icon_color_hover',
            [
                'label'             => esc_html__('Icon Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'         => [
                    '{{WRAPPER}} .premium-button:hover i'   => 'color: {{VALUE}};',
                ],
                'condition'         => [
                    'premium_unfold_button_icon_switcher'  => 'yes',
                ]
            ]);
        
        $this->add_control('premium_unfold_button_background_hover',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'scheme'            => [
                        'type'  => Scheme_Color::get_type(),
                        'value' => Scheme_Color::COLOR_3
                    ],
                    'selectors'          => [
                        '{{WRAPPER}} .premium-button:hover' => 'background-color: {{VALUE}};',
                    ],
                    ]
                );
        
        /*Button Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_unfold_button_border_hover',
                    'selector'      => '{{WRAPPER}} .premium-button:hover',
                ]
                );
        
        /*Button Border Radius*/
        $this->add_control('premium_unfold_button_border_radius_hover',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-button:hover' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        /*Icon Shadow*/
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Icon Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_unfold_button_icon_shadow_hover',
                    'selector'      => '{{WRAPPER}} .premium-button:hover i',
                    'condition'         => [
                            'premium_unfold_button_icon_switcher'  => 'yes',
                        ]
                    ]
                );

        /*Text Shadow*/
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Text Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_unfold_button_text_shadow_hover',
                    'selector'      => '{{WRAPPER}} .premium-button:hover span',
                    ]
                );
        
        /*Button Shadow*/
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'label'         => esc_html__('Button Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_unfold_button_box_shadow_hover',
                    'selector'      => '{{WRAPPER}} .premium-button:hover',
                ]
                );
        
        /*Button Margin*/
        $this->add_responsive_control('premium_unfold_button_margin_hover',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-button:hover' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Button Padding*/
        $this->add_responsive_control('premium_unfold_button_padding_hover',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-button:hover' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->end_controls_tab();
        
        $this->end_controls_tabs();

        /*End Button Style Section*/
        $this->end_controls_section();
        
        $this->start_controls_section('premium_unfold_grad_style',
                [
                    'label'         => esc_html__('Fade Color', 'premium-addons-for-elementor'),
                    'tab'           => Controls_Manager::TAB_STYLE,
                    'condition'     => [
                        'premium_unfold_sep_switcher'  => 'yes',
                    ]
                ]
                );
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_unfold_sep_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-unfold-gradient',
                    ]
                );
        
        /*Separator Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_unfold_sep_border',
                    'selector'      => '{{WRAPPER}} .premium-unfold-gradient',
                ]
                );
        
        /*Separator Border Radius*/
        $this->add_control('premium_unfold_sep_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-unfold-gradient' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        /*Separator Padding*/
        $this->add_responsive_control('premium_unfold_sep_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-unfold-gradient' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        
        $this->end_controls_section();
        
    }
    
    protected  function get_unfold_button(){
        $settings = $this->get_settings_for_display();
        $button_size = 'premium-button-' . $settings['premium_unfold_button_size'];
        ?>
        <div class="premium-unfold-button-container">
            <a id='premium-unfold-button-<?php echo esc_attr($this->get_id()); ?>' class="premium-button <?php echo esc_attr($button_size); ?>">
            <?php if($settings['premium_unfold_button_icon_switcher'] && $settings['premium_unfold_button_icon_position'] == 'before' && !empty($settings['premium_unfold_button_icon'])) : ?><i class="premium-unfold-before"></i><?php endif; ?><span id="premium-unfold-button-text-<?php echo esc_attr($this->get_id()); ?>" class="premium-unfold-button-text"></span><?php if($settings['premium_unfold_button_icon_switcher'] && $settings['premium_unfold_button_icon_position'] == 'after' && !empty($settings['premium_unfold_button_icon'])) : ?><i class="premium-unfold-after"></i><?php endif; ?>
            </a>
        </div>
    <?php }


    protected function render($instance = [])
    {
        // get our input from the widget settings.
        $settings = $this->get_settings_for_display();
        
        $this->add_render_attribute( 'premium_unfold_title', 'class', 'premium-unfold-heading' );
        
        $this->add_inline_editing_attributes('premium_unfold_title', 'basic');

        $this->add_render_attribute( 'premium_unfold_content', 'class', 'premium-unfold-editor-content' );
        
        $this->add_inline_editing_attributes('premium_unfold_content', 'advanced');
        
        $button_size = 'premium-button-' . $settings['premium_unfold_button_size'];
        
        $button_icon = $settings['premium_unfold_button_icon'];
        
        $button_icon_unfolded = $settings['premium_unfold_button_icon_unfolded'];
        
        if($settings['premium_unfold_fold_height_select'] == 'percent'){
            $fold_height = $settings['premium_unfold_fold_height'];
        } else {
            $fold_height = $settings['premium_unfold_fold_height_pix'];
        }
        
        
        if($settings['premium_unfold_fold_dur_select'] == 'custom'){
            $fold_dur = $settings['premium_unfold_fold_dur'] * 1000;
        } else {
            $fold_dur = $settings['premium_unfold_fold_dur_select'];
        }
        
        if($settings['premium_unfold_unfold_dur_select'] == 'custom'){
            $unfold_dur = $settings['premium_unfold_unfold_dur'] * 1000;
        } else {
            $unfold_dur = $settings['premium_unfold_unfold_dur_select'] ;
        }
        
        if(!empty($settings['premium_unfold_sep_height'])){
            $sep_height = $settings['premium_unfold_sep_height']['size'] . 'px';
        }
        
        $fold_ease =  $settings['premium_unfold_fold_easing'];
        $unfold_ease =  $settings['premium_unfold_unfold_easing'];
        
        $unfold_settings = [
          'buttonIcon'          => $button_icon,
          'buttonUnfoldIcon'    => $button_icon_unfolded,
          'foldSelect'          => $settings['premium_unfold_fold_height_select'],
          'foldHeight'          => $fold_height,
          'foldDur'             => $fold_dur,
          'unfoldDur'           => $unfold_dur,
          'foldEase'            => $fold_ease,
          'unfoldEase'          => $unfold_ease,
          'foldText'            => $settings['premium_unfold_button_fold_text'],
          'unfoldText'          => $settings['premium_unfold_button_unfold_text'],
        ];
        
        
?>

<div class="premium-unfold-wrap" data-settings='<?php echo wp_json_encode($unfold_settings); ?>'>
    <div class='premium-unfold-container'>
        <div class='premium-unfold-folder'>
            <?php if($settings['premium_unfold_title_switcher'] == 'yes' && !empty($settings['premium_unfold_title'])) : ?>
            <<?php echo $settings['premium_unfold_title_heading'] . ' ' . $this->get_render_attribute_string('premium_unfold_title'); ?>><?php echo $settings['premium_unfold_title']; ?></<?php echo $settings['premium_unfold_title_heading']; ?>>
            <?php endif; ?>
            <div id="premium-unfold-content-<?php echo $this->get_id(); ?>" class="premium-unfold-content toggled">
                <div <?php echo $this->get_render_attribute_string('premium_unfold_content');  ?>><?php echo $this->parse_text_editor($settings['premium_unfold_content']); ?></div>
            </div>
            <?php if($settings['premium_unfold_sep_switcher'] == 'yes') : ?>
            <div id="premium-unfold-gradient-<?php echo esc_attr($this->get_id()); ?>" class="premium-unfold-gradient toggled" style="<?php echo 'height:' . $sep_height; ?>"></div>
            <?php endif; ?>
        </div>
    <?php if($settings['premium_unfold_button_position'] == 'inside') $this->get_unfold_button(); ?>
    </div>
    <?php if($settings['premium_unfold_button_position'] == 'outside') $this->get_unfold_button(); ?>
</div>
    <?php
    }   
}