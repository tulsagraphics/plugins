<?php 

/**
 * Class: Premium_Facebook_Reviews_Widget
 * Name: Facebook Reviews
 * Slug: premium-facebook-reviews
 */

namespace Elementor;

use Elementor\Core\Responsive\Responsive;

if( !defined( 'ABSPATH' ) ) exit;

class Premium_Facebook_Reviews extends Widget_Base {

    public function get_name() {
        return 'premium-facebook-reviews';
    }

    public function get_title() {
        return \PremiumAddons\Helper_Functions::get_prefix() . ' Facebook Reviews';
    }
    
    public function get_icon() {
        return 'pa-pro-facebook-reviews';
    }
    
    public function get_categories() {
        return ['premium-elements'];
    }
    
    public function get_script_depends() {
        return [
            'masonry-js',
            'premium-pro-js'
        ];
    }

    public function is_reload_preview_required() {
        return true;
    }
    
    // Adding the controls fields for the Premium Facebook Reviews
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls(){

        $this->start_controls_section('general',
            [
                'label'         => esc_html__( 'Access Credentials', 'premium-addons-for-elementor' )
            ]
        );

        $this->add_control('page_name',
            [
                'label'         => esc_html__('Page Name', 'premium-addons-for-elementor'),
                'default'       => 'leap13',
                'type'          => Controls_Manager::TEXT,
            ]
        );

        $this->add_control('page_id',
            [
                'label'         => esc_html__('Page ID', 'premium-addons-for-elementor'),
                'default'       => '734199906647993',
                'description'   => 'Click <a href="https://www.facebook.com/help/community/question/?id=378910098941520" target="_blank">Here</a> to know how to get your page ID',
                'type'          => Controls_Manager::TEXT,
            ]
        );
        
        $this->add_control('page_access',
            [
                'label'         => esc_html__('Page Access Token', 'premium-addons-for-elementor'),
                'default'       => 'EAAVVPjFKgSEBAMV3amGyeedKiUj4KVPD0JNzkdu0wK7fUitmH4wZCScqGBxZCnqLRBf3ZABVn2165jtrk3zbZCRf6rpn9mDVznKsIOGsH4PZArW7gXAILZB8hKzqcZCPSZB5ZAZC9vAO4MUReXxZAqVweJxI5HriqX2GrHb07pkh4v01wZDZD',
                'description'   => 'Click <a href="https://www.youtube.com/watch?v=Zb8YWXlXo-k" target="_blank">Here</a> to know how to get your page access token',
                'type'          => Controls_Manager::TEXT,
            ]
        );

        $this->end_controls_section();
        
        $this->start_controls_section('content',
            [
                'label'         => esc_html__( 'Display Options', 'premium-addons-for-elementor' )
            ]
        );
        
        $this->start_controls_tabs('display_tabs');
        
        $this->start_controls_tab('page_tab',
            [
                'label'         => esc_html__('Page', 'premium-addons-for-elementor'),
                'condition'     => [
                    'page_info'  => 'yes'
                ]
            ]
        );
        
        $this->add_control('page_custom_image_switch',
            [
                'label'         => esc_html__('Replace Page Image', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'condition'     => [
                    'page_info'  => 'yes'
                ]
            ]
        );
        
        $this->add_control('page_custom_image',
           [
                'label'         => esc_html__( 'Upload Image', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::MEDIA,
                'condition'     => [
                    'page_info'  => 'yes',
                    'page_custom_image_switch'  => 'yes'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Image_Size::get_type(),
            [
                'name'          => 'thumbnail',
                'default'       => 'full',
                'condition'     => [
                    'page_info'                     => 'yes',
                    'page_custom_image_switch'      => 'yes'
                ],
            ]
        );
        
        $this->add_control('page_display',
            [
                'label'         => esc_html__( 'Display', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'inline'        => esc_html__('Inline', 'premium-addons-for-elementor'),
                    'block'         => esc_html__('Block', 'premium-addons-for-elementor'),
                ],
                'default'       => 'block',
                'condition'         => [
                    'page_info'  => 'yes'
                ]
            ]
        );
        
        $this->add_control('page_dir',
                [
                    'label'     => esc_html__( 'Direction', 'premium-addons-for-elementor' ),
                    'type'      => Controls_Manager::SELECT,
                    'options'   => [
                        'rtl'       => 'RTL',
                        'ltr'       => 'LTR',
                    ],
                    'default'   => 'ltr',
                    'condition' => [
                        'page_display'   => 'inline'
                    ]
                ]
            );
        
        $this->add_responsive_control('page_align',
            [
                'label'         => esc_html__( 'Page Alignment', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'          => [
                        'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-left',
                        ],
                    'center'        => [
                        'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-center',
                        ],
                    'right'         => [
                        'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-right',
                        ],
                    ],
                'default'       => 'center',
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-container .premium-fb-rev-page' => 'text-align: {{VALUE}};',
                    ],
                'condition'     => [
                    'page_info'   => 'yes'
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->start_controls_tab('reviews_tab',
            [
                'label'         => esc_html__('Reviews', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('reviews_columns',
            [
                'label'         => esc_html__('Number of Columns', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    '1col'          => esc_html__('1 Column', 'premium-addons-for-elementor'),
                    '2col'          => esc_html__('2 Columns', 'premium-addons-for-elementor'),
                    '3col'          => esc_html__('3 Columns', 'premium-addons-for-elementor'),
                    '4col'          => esc_html__('4 Columns', 'premium-addons-for-elementor'),
                    '5col'          => esc_html__('5 Columns', 'premium-addons-for-elementor'),
                ],
                'default'       => '3col',
            ]
        );
        
        $this->add_control('reviews_display',
            [
                'label'         => esc_html__( 'Display', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'inline'        => esc_html__('Inline', 'premium-addons-for-elementor'),
                    'block'         => esc_html__('Block', 'premium-addons-for-elementor'),
                ],
                'default'       => 'block',
            ]
        );
        
        $this->add_control('reviews_style',
            [
                'label'         => esc_html__( 'Layout', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'even'          => esc_html__('Even', 'premium-addons-for-elementor'),
                    'masonry'       => esc_html__('Masonry', 'premium-addons-for-elementor'),
                ],
                'default'       => 'masonry',
                'condition'     => [
                    'reviews_columns!'  => '1col'
                ]
            ]
        );
        
        $this->add_control('reviews_dir',
            [
                'label'         => esc_html__( 'Direction', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'rtl'           => 'RTL',
                    'ltr'           => 'LTR',
                ],
                'default'       => 'ltr',
                'condition'     => [
                    'reviews_display'   => 'inline'
                ]
            ]
        );
        
        $this->add_responsive_control('content_align',
            [
                'label'         => esc_html__( 'Content Alignment', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'      => [
                        'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-left',
                        ],
                    'center'    => [
                        'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-center',
                        ],
                    'right'     => [
                        'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-right',
                        ],
                    'justify'   => [
                        'title'=> esc_html__( 'Justify', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-justify',
                    ],
                ],
                'default'       => 'center',
                'condition'     => [
                    'reviews_display'   => 'block'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-container .premium-fb-rev-review' => 'text-align: {{VALUE}};',
                    ],
                ]
            );
        
        $this->end_controls_tab();
        
        $this->end_controls_tabs();
        
        $this->end_controls_section();
        
        $this->start_controls_section('adv',
            [
                'label'         => esc_html__( 'Advanced Settings', 'premium-addons-for-elementor' )
            ]
        );
        
        $this->add_control('page_info',
            [
                'label'         => esc_html__('Page Info', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes'
            ]
        );
        
        $this->add_control('page_rate',
            [
                'label'         => esc_html__('Page Rate', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
                'condition'     => [
                    'page_info'  => 'yes'
                ],
            ]
        );
        
        $this->add_control('text',
            [
                'label'         => esc_html__('Show Review Text', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
            ]
        );
        
        $this->add_control('stars',
            [
                'label'         => esc_html__('Show Stars', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes'
            ]
        );
        
        $this->add_control('date',
            [
                'label'         => esc_html__('Show Date', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes'
            ]
        );

        $this->add_control('filter',
            [
                'label'         => esc_html__('Filter', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
        );

        $this->add_control('filter_min',
            [
                'label'         => esc_html__( 'Min Stars', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::NUMBER,
                'min'           => 1,
                'max'           => 5,
                'condition'     => [
                    'filter' => 'yes'
                ],
            ]
        );

        $this->add_control('filter_max',
            [
                'label'         => esc_html__( 'Max Stars', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::NUMBER,
                'min'           => 1,
                'max'           => 5,
                'condition'     => [
                    'filter' => 'yes'
                ],
            ]
        );
        
        $this->add_control('limit',
            [
                'label'         => esc_html__('Reviews Limit', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
        );
        
        $this->add_control('limit_num',
            [
                'label'         => esc_html__( 'Number of Reviews', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::NUMBER,
                'min'           => 0,
                'description'   => esc_html__( 'Set a number of reviews to retrieve', 'premium-addons-for-elementor' ),
                'condition'     => [
                    'limit' => 'yes'
                ],
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('images',
            [
                'label'         => esc_html__('Images', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                ]
            );
        
        $this->start_controls_tabs('images_tabs');
        
        $this->start_controls_tab('page_img_tab',
            [
                'label'         => esc_html__('Page', 'premium-addons-for-elementor'),
                'condition'     => [
                    'page_info'  => 'yes'
                ]
            ]
        );
        
        $this->add_responsive_control('page_image_size',
            [
                'label'         => esc_html__('Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%', "em"],
                'range'         => [
                    'px'    => [
                        'min'       => 1, 
                        'max'       => 200,
                    ],
                ],
                'default'       => [
                    'unit'  => 'px',
                    'size'  => 60
                ],
                'condition'     => [
                    'page_custom_image_switch!'    => 'yes'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .prmeium-fb-rev-page-inner .premium-fb-rev-img' => 'width: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'page_image_border',
                'selector'      => '{{WRAPPER}} .prmeium-fb-rev-page-inner .premium-fb-rev-img',
            ]
        );
        
        $this->add_control('page_image_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'selectors'     => [
                    '{{WRAPPER}} .prmeium-fb-rev-page-inner .premium-fb-rev-img' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                'name'          => 'page_image_shadow',
                'selector'      => '{{WRAPPER}} .prmeium-fb-rev-page-inner .premium-fb-rev-img',
            ]
        );
        
        $this->add_responsive_control('page_image_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em', '%' ],
                'selectors'     => [
                    '{{WRAPPER}} .prmeium-fb-rev-page-inner .premium-fb-rev-page-left' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->start_controls_tab('img_tab',
            [
                'label'         => esc_html__('Review', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_responsive_control('image_size',
            [
                'label'         => esc_html__('Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%', "em"],
                'range'         => [
                    'px'    => [
                        'min'   => 1, 
                        'max'   => 200,
                    ],
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-review-inner .premium-fb-rev-img' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'reviewer_image_border',
                'selector'      => '{{WRAPPER}} .premium-fb-rev-review-inner .premium-fb-rev-img',
            ]
        );
        
        $this->add_control('reviewer_image_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-review-inner .premium-fb-rev-img' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'label'     => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'      => 'image_shadow',
                    'selector'  => '{{WRAPPER}} .premium-fb-rev-review-inner .premium-fb-rev-img',
                ]
            );
        
        $this->add_responsive_control('image_margin',
                [
                    'label'     => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'      => Controls_Manager::DIMENSIONS,
                    'size_units'=> [ 'px', 'em', '%' ],
                    'selectors' => [
                        '{{WRAPPER}} .premium-fb-rev-review-inner .premium-fb-rev-content-left' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                    ]
                ]
            );
        
        $this->end_controls_tab();
        
        $this->end_controls_tabs();
        
        $this->end_controls_section();
        
        $this->start_controls_section('page',
            [
                'label'         => esc_html__('Page Info', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'page_info'  => 'yes'
                ]
            ]
        );
        
        $this->start_controls_tabs('page_info_tabs');
        
        $this->start_controls_tab('page_container',
            [
                'label'         => esc_html__('Container', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'          => 'page_container_background',
                'types'         => [ 'classic' , 'gradient' ],
                'selector'      => '{{WRAPPER}} .premium-fb-rev-page',
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'page_container_border',
                'selector'      => '{{WRAPPER}} .premium-fb-rev-page',
            ]
        );
        
        $this->add_control('page_container_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-page' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'page_container_shadow',
                'selector'      => '{{WRAPPER}} .premium-fb-rev-page',
            ]
        );
        
        $this->add_responsive_control('page_container_margin',
                [
                    'label'      => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'       => Controls_Manager::DIMENSIONS,
                    'size_units' => [ 'px', 'em', '%' ],
                    'selectors'  => [
                        '{{WRAPPER}} .premium-fb-rev-page' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                    ]
                ]
            );
        
        $this->add_responsive_control('page_container_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em', '%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-page' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->start_controls_tab('page_link',
            [
                'label'         => esc_html__('Name', 'premium-addons-for-elementor'),
            ]
            );
        
        $this->add_control('page_color',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-page-link' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_control('page_hover_color',
            [
                'label'         => esc_html__('Hover Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-page-link:hover' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'page_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-fb-rev-page-link',
            ]
        );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'page_shadow',
                'selector'      => '{{WRAPPER}} .premium-fb-rev-page-link',
            ]
        );
        
        $this->add_responsive_control('page_margin',
                [
                    'label'     => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'      => Controls_Manager::DIMENSIONS,
                    'size_units'=> [ 'px', 'em', '%' ],
                    'selectors' => [
                        '{{WRAPPER}} .premium-fb-rev-page-link-wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->start_controls_tab('page_rate_link',
            [
                'label'        => esc_html__('Rate', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('page_star_size',
            [
                'label'         => esc_html__('Star Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::NUMBER,
                'min'           => 1,
                'max'           => 50,
                'condition'     => [
                    'stars'         =>  'yes'
                ]
            ]
        );
        
        $this->add_control('page_fill',
            [
                'label'         => esc_html__('Star Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'condition'     => [
                    'stars'         =>  'yes'
                ]
            ]
        );
        
        $this->add_control('page_empty',
            [
                'label'         => esc_html__('Empty Star Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'condition'     => [
                    'stars'         =>  'yes'
                ]
            ]
        );
        
        $this->add_control('page_rate_color',
            [
                'label'        => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'         => Controls_Manager::COLOR,
                'scheme'       => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'condition'    => [
                    'page_rate'  => 'yes'
                ],
                'selectors'    => [
                    '{{WRAPPER}} .premium-fb-rev-page-rating' => 'color: {{VALUE}};'
                ]
            ]
            );

        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'      => 'page_rate_typo',
                    'scheme'    => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'  => '{{WRAPPER}} .premium-fb-rev-page-rating',
                    'condition'    => [
                            'page_rate'  => 'yes'
                        ]
                    ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'page_rate_shadow',
                'selector'      => '{{WRAPPER}} .premium-fb-rev-page-rating',
                'condition'    => [
                    'page_rate'  => 'yes'
                ]
            ]
        );
        
        $this->add_responsive_control('page_rate_margin',
            [
                'label'     => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'      => Controls_Manager::DIMENSIONS,
                'size_units'=> [ 'px', 'em', '%' ],
                'selectors' => [
                '{{WRAPPER}} .premium-fb-rev-page-rating-wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ],
                'condition'    => [
                    'page_rate'  => 'yes'
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->end_controls_tabs();
        
        $this->end_controls_section();
        
        $this->start_controls_section('review_container',
            [
                'label'             => esc_html__('Review', 'premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
            ]
        );
        
        $this->add_control('reviews_star_size',
            [
                'label'         => esc_html__('Star Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::NUMBER,
                'min'           => 1,
                'max'           => 50,
            ]
        );
        
        $this->add_control('reviews_fill',
            [
                'label'         => esc_html__('Star Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
            ]
        );
        
        $this->add_control('reviews_empty',
            [
                'label'         => esc_html__('Empty Star Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'              => 'review_container_background',
                'types'             => [ 'classic' , 'gradient' ],
                'selector'          => '{{WRAPPER}} .premium-fb-rev-review',
            ]
        );
        
        /*Icon Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'review_container_border',
                'selector'      => '{{WRAPPER}} .premium-fb-rev-review',
            ]
        );
        
        /*Icon Border Radius*/
        $this->add_control('review_container_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-review' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'review_container_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-fb-rev-review',
            ]
        );
        
        $this->add_responsive_control('reviews_gap',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', '%', "em"],
                'condition'     => [
                    'reviews_columns!'   => '1col'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-review-wrap' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control('review_container_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-review' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]      
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('reviewer',
            [
                'label'         => esc_html__('Reviewer Name', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );
        
        $this->add_control('reviewer_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-reviewer-link' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_control('reviewer_hover_color',
            [
                'label'         => esc_html__('Hover Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-reviewer-link:hover' => 'color: {{VALUE}};'
                ]
            ]
            );
        
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'reviewer_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-fb-rev-reviewer-link',
            ]
        );
        
        $this->add_responsive_control('reviewer_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em', '%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-content-link-wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ]
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('date_style',
            [
                'label'         => esc_html__('Date', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'date'   => 'yes'
                ]
            ]
        );
        
        $this->add_control('date_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-time .premium-fb-rev-time-text' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_control('reviewer_date_color_hover',
            [
                'label'         => esc_html__('Hover Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-time .premium-fb-rev-time-text:hover' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'date_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-fb-rev-time .premium-fb-rev-time-text',
            ]
        );
        
        $this->add_responsive_control('date_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em', '%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-time' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ]
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('reviewer_txt',
            [
                'label'         => esc_html__('Review Content', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );
        
        $this->add_control('reviewer_txt_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-text' => 'color: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'      => 'reviewer_txt_typo',
                    'scheme'    => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'  => '{{WRAPPER}} .premium-fb-rev-text',
                    ]
                );

        $this->add_responsive_control('reviewer_txt_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em', '%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-text-wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ]
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('container',
            [
                'label'         => esc_html__('Container', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control('container_width',
            [
                'label'         => esc_html__('Max Width', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'range'         => [
                    'px'    => [
                        'min'   => 1,
                        'max'   => 300,
                    ]
                ],
                'selectors'     => [
                    '{{WRAPPER}} .elementor-widget-container' => 'max-width: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control('container_align',
            [
                'label'         => esc_html__( 'Alignment', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'flex-start'      => [
                        'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-left',
                        ],
                    'center'    => [
                        'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-center',
                        ],
                    'flex-end'     => [
                        'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-right',
                        ],
                    ],
                'default'       => 'center',
                'selectors'     => [
                    '{{WRAPPER}}' => 'justify-content: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'          => 'container_background',
                'types'         => [ 'classic' , 'gradient' ],
                'selector'      => '{{WRAPPER}} .premium-fb-rev-container',
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'container_border',
                'selector'      => '{{WRAPPER}} .premium-fb-rev-container',
            ]
        );
        
        $this->add_control('container_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-fb-rev-container' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'container_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-fb-rev-container',
            ]
        );
        
        $this->add_responsive_control('container_margin',
                [
                    'label'      => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'       => Controls_Manager::DIMENSIONS,
                    'size_units' => ['px', 'em', '%'],
                    'selectors'  => [
                        '{{WRAPPER}} .premium-fb-rev-container' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]      
        );

        $this->end_controls_section();
    }
    
    /**
     * returns the responsive style based on Elementor's Breakpoints
     * @access protected
     * @return string
     */
    protected function get_review_responsive_style() {
        
        $breakpoints = Responsive::get_breakpoints();
        
        $style = '<style>';
        $style .= '@media ( max-width: ' . $breakpoints['lg'] . 'px ) {';
        $style .= '.premium-fb-rev-review-wrap {';
        $style .= 'width: 50% !important;';
        $style .= '}';
        $style .= '}';
        $style .= '@media ( max-width: ' . $breakpoints['md'] . 'px ) {';
        $style .= '.premium-fb-rev-review-wrap {';
        $style .= 'width: 100% !important;';
        $style .= '}';
        $style .= '}';
        $style .= '</style>';
        
        return $style;
        
    }
    
    /**
     * renders the HTML content of the widget
     * @return void
     */
    protected function render(){
        
        $settings = $this->get_settings();
        
        $page_name = $settings['page_name'];
        
        $page_id = $settings['page_id'];
        
        $page_access = $settings['page_access'];
        
        if( 'yes' == $settings['page_info'] && 'yes' == $settings['page_custom_image_switch'] ) {
            
            $image_src = $settings['page_custom_image'];
            
            $image_src_size = Group_Control_Image_Size::get_attachment_image_src( $image_src['id'], 'thumbnail', $settings );
            
            if( empty( $image_src_size ) ) : $image_src_size = $image_src['url']; else: $image_src_size = $image_src_size; endif;
            
            $custom_image = ! empty( $image_src_size ) ? $image_src_size : '';
            
            
        } else {
            
            $custom_image = '';
        }
        
        if($settings['stars'] == 'yes' ) {
            $show_stars = true;
        } else {
            $show_stars = false;
        }
        
        if($settings['date'] == 'yes' ) {
            $show_date = true;
        } else {
            $show_date = false;
        }
        
        if( 'inline' == $settings['page_display'] ) {
            $this->add_render_attribute( 'page_dir', 'class', [ $settings['page_dir'], 'premium-fb-rev-page' ] );
        } else {
            $this->add_render_attribute( 'page_dir', 'class', 'premium-fb-rev-page' );
        }
        
        if( 'inline' == $settings['reviews_display'] ) {
            $this->add_render_attribute( 'reviews_dir', 'class', [ $settings['reviews_dir'], 'premium-fb-rev-content' ] );
        } else {
            $this->add_render_attribute( 'reviews_dir', 'class', 'premium-fb-rev-content' );
        }
        
        if( 'yes' == $settings['page_info'] && 'yes' == $settings['page_rate'] ) {
            $page_rate = true;
        } else {
            $page_rate = false;
        }
        
        if( 'yes' == $settings['text'] ){
            $rev_text = true;
        } else {
            $rev_text = false;
        }
        
        $page_star_size    = ! empty( $settings['page_star_size'] ) ? $settings['page_star_size'] : 16;
        $page_fill_color   = ! empty( $settings['page_fill'] ) ? $settings['page_fill'] : '#6ec1e4';
        $page_empty_color  = ! empty( $settings['page_empty'] ) ? $settings['page_empty'] : '#ccc';
        
        $rev_star_size      = ! empty( $settings['reviews_star_size'] ) ? $settings['reviews_star_size'] : 16;
        $rev_fill_color     = ! empty( $settings['reviews_fill'] ) ? $settings['reviews_fill'] : '#6ec1e4';
        $rev_empty_color    = ! empty( $settings['reviews_empty'] ) ? $settings['reviews_empty'] : '#ccc';
        
        if( 'yes' == $settings['limit'] ){
            if  ( '0' == $settings['limit_num'] ) {
                $limit = 0;
            } else {
                $limit = ! empty( $settings['limit_num'] ) ? $settings['limit_num'] : 9999;    
            }
            
        } else {
            $limit = 9999;
        }
        
        $response = premium_fb_rev_api_rating( $page_id, $page_access );
        
        if( 'yes' == $settings['filter'] ){
            $min_filter = !empty( $settings['filter_min'] ) ? $settings['filter_min'] : 1;
            $max_filter = !empty( $settings['filter_max'] ) ? $settings['filter_max'] : 5;
        } else {
            $min_filter = 1;
            $max_filter = 5;
        }
        
        $response_data = $response['data'];
        
        $response_json = rplg_json_decode($response_data);
        
        if( isset( $response_json->ratings ) && isset( $response_json->ratings->data ) ) {
            
            $reviews = $response_json->ratings->data;
            
            update_option('premium_facebook_reviews-' . $this->get_id() , $reviews );
            
            update_option( 'premium_facebook_reviews-page-' . $this->get_id() , $page_id );
            
        } elseif ( ! isset( $response_json->ratings->data[0]->reviewer ) ) {
            
            $stored_id = get_option( 'premium_facebook_reviews-page-' . $this->get_id() );
            
            if( !empty( $stored_id ) && $stored_id == $page_id ) {
                
                $reviews = get_option('premium_facebook_reviews-' . $this->get_id() , array() );
                
            }
        } elseif ( empty( $page_id ) || empty( $page_access ) ) {
            
            $reviews = array();
            
            ?>
            <div class="premium-fbrev-error">
                <?php echo esc_html__('Please fill the required fields: Page ID & Page Access Token','premium-addons-for-elementor'); ?>
            </div>
            <?php
        }
            
        $rating = 0;
        if( count( $reviews ) > 0 ) {
            foreach ( $reviews as $review ) {
                $rating = $rating + $review->rating;
            }
            $rating = round($rating / count($reviews), 1);
            $rating = number_format((float)$rating, 1, '.', '');
        }
        

        ?>
<div class="premium-fb-rev-container <?php echo 'premium-review-'. $settings['reviews_style'] .' premium-review-'.esc_attr( $settings['reviews_columns'] ); ?>" data-col="<?php echo esc_attr( $settings['reviews_columns'] ); ?>" data-style="<?php echo esc_attr($settings['reviews_style']); ?>">
    <div class="premium-fb-rev-list">
        <?php if( 'yes' == $settings['page_info'] ) : ?>
        <div <?php echo $this->get_render_attribute_string( 'page_dir' ); ?>>
            <div class="prmeium-fb-rev-page-inner">
                <?php premium_fb_rev_page( $page_id, $page_name, $rating,$page_fill_color, $page_empty_color, $show_stars, $page_star_size, $page_rate, $custom_image ); ?>
            </div>
        </div>
        <?php endif; ?>
        <div <?php echo $this->get_render_attribute_string( 'reviews_dir' ); ?>>
            <?php premium_fb_rev_reviews($reviews, $rev_fill_color, $rev_empty_color, $show_stars, $rev_star_size, $min_filter, $max_filter, $show_date, $rev_text, $limit); ?>
        </div>
    </div>
</div>
    <?php
    
    echo $this->get_review_responsive_style();
    
    }
}