<?php
namespace Elementor;

use Elementor\Core\Responsive\Responsive;

if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Premium_Notbar extends Widget_Base
{
    protected  $templateInstance;
    
    public function getTemplateInstance(){
        return $this->templateInstance = premium_Template_Tags::getInstance();
    }
    
    public function get_name() {
        return 'premium-notbar';
    }

    public function get_title() {
        return \PremiumAddons\Helper_Functions::get_prefix() . ' Alert Box';
    }

    public function get_icon() {
        return 'pa-pro-notification-bar';
    }
    
    public function is_reload_preview_required(){
        return true;
    }
    
    public function get_script_depends(){
        return ['premium-pro-js'];
    }

    public function get_categories() {
        return [ 'premium-elements' ];
    }

    // Adding the controls fields for the premium notification bar
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {

        /*Start Bar Settings Section */
        $this->start_controls_section('premium_notbar_general_section',
                [
                    'label'         => esc_html__('Bar', 'premium-addons-for-elementor'),
                    ]
                );
        
        /*Bar Position*/ 
        $this->add_control('premium_notbar_position',
                [
                    'label'         => esc_html__('Position', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'           => [
                        'top'    => [
                            'title' => __( 'Top', 'premium-addons-for-elementor' ),
                            'icon'  => 'fa fa-arrow-circle-up',
                        ],
                        'bottom' => [
                            'title' => __( 'Bottom', 'premium-addons-for-elementor' ),
                            'icon'  => 'fa fa-arrow-circle-down',
                        ],
                        'middle' => [
                            'title' => __( 'Middle', 'premium-addons-for-elementor' ),
                            'icon'  => 'fa fa-align-center',
                        ],
                        'float' => [
                            'title' => __( 'Custom', 'premium-addons-for-elementor' ),
                            'icon'  => 'fa fa-align-justify',
                        ],
                    ],
                    'default'       => 'float',
                    'toggle'        => false,
                    'label_block'   => true,
                ]
                );
        
        $this->add_responsive_control('premium_notbar_float_pos',
                [
                    'label'         => esc_html__('Vertical Offset (%)', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'default'       => [
                        'size'  => 10,
                        'unit'  => '%'
                    ],
                    'condition'     => [
                        'premium_notbar_position'    => 'float',
                    ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-notbar'   => 'top: {{SIZE}}%;'
                    ]
                ]
                );
        
        $this->add_control('premium_notbar_top_select',
                [
                    'label'         => esc_html__('Layout', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SELECT,
                    'options'       => [
                        'fixed'  => esc_html__('Fixed','premium-addons-for-elementor'),
                        'relative' => esc_html__('Relative','premium-addons-for-elementor'),
                    ],
                    'default'       => 'relative',
                    'condition'     => [
                        'premium_notbar_position'    => 'top',
                    ],
                    'label_block'   => true,
                ]
                );
        
        $this->add_control('premium_notbar_width',
                [
                    'label'         => esc_html__('Width', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SELECT,
                    'options'       => [
                        'wide'  => esc_html__('Full Width','premium-addons-for-elementor'),
                        'boxed' => esc_html__('Boxed','premium-addons-for-elementor'),
                    ],
                    'default'       => 'boxed',
                    'label_block'   => true,
                ]
                );

        $this->add_control('premium_notbar_direction',
            [
                'label'         => esc_html__('Direction', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::CHOOSE,
                'options'           => [
                    'row'       => [
                        'title'     => __( 'LTR', 'premium-addons-for-elementor' ),
                        'icon'      => 'fa fa-arrow-circle-right',
                    ],
                    'row-reverse' => [
                        'title'     => __( 'RTL', 'premium-addons-for-elementor' ),
                        'icon'      => 'fa fa-arrow-circle-left',
                    ],
                ],
                'default'       => 'row',
                'selectors'     => [
                    '{{WRAPPER}} .premium-notbar-text-container, {{WRAPPER}} .premium-notbar-icon-text-container'    => '-webkit-flex-direction: {{VALUE}}; flex-direction: {{VALUE}};'
                ],
                'condition'     => [
                        'premium_notbar_content_type'    => 'editor',
                    ],
                'label_block'   => true,
                'toggle'        => false
            ]
        );
        
        $this->add_control('premium_notbar_close_heading',
            [
                'label'         => esc_html__('Close Button', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::HEADING,
            ]
        );
        
        $this->add_control('premium_notbar_close_hor_position',
            [
                'label'         => esc_html__('Horizontal Position', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'row'           => __( 'After', 'premium-addons-for-elementor' ),
                    'row-reverse'   => __( 'Before', 'premium-addons-for-elementor' ),
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-notbar-text-container'    => '-webkit-flex-direction: {{VALUE}}; flex-direction: {{VALUE}};'
                ],
                'default'       => 'row',
                'label_block'   => true,
            ]
        );
        
        $this->add_control('premium_notbar_close_ver_position',
            [
                'label'         => esc_html__('Vertical Position', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'flex-start'    => __( 'Top', 'premium-addons-for-elementor' ),
                    'center'        => __( 'Middle', 'premium-addons-for-elementor' ),
                    'flex-end'      => __( 'Bottom', 'premium-addons-for-elementor' ),
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-notbar-text-container'    => 'align-items: {{VALUE}};'
                ],
                'default'       => 'center',
                'label_block'   => true,
                'separator'     => 'after'
            ]
        );
        
        $this->add_control('premium_notbar_index',
            [
                'label'         => esc_html__('Z-index', 'premium-addons-for-elementor'),
                'description'   => esc_html__('Set a z-index for the notification bar, default is: 9999','premium-addons-for-elementor'),
                'type'          => Controls_Manager::NUMBER,    
                'min'           => 0,
                'max'           => 9999,
                'selectors'     => [
                    '#premium-notbar-{{ID}}'   => 'z-index: {{VALUE}};'
                ]
            ]
        );

        $this->end_controls_section();
        
        $this->start_controls_section('premium_notbar_content',
            [
                'label'             => esc_html__('Content', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_notbar_content_type',
                [
                    'label'         => esc_html__('Content to Show', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SELECT,
                    'options'       => [
                        'editor'        => esc_html('Text Editor', 'premium-addons-for-elementor'),
                        'template'      => esc_html('Elementor Template', 'premium-addons-for-elementor'),
                    ],
                    'default'       => 'editor',
                    'label_block'   => true
                ]
                );
        
        $this->add_control('premium_notbar_content_temp',
                [
                    'label'         => esc_html__( 'Content', 'premium-addons-for-elementor' ),
                    'description'   => esc_html__( 'Elementor Template is a template which you can choose from Elementor Templates library', 'premium-addons-for-elementor' ),
                    'type' => Controls_Manager::SELECT2,
                    'options' => $this->getTemplateInstance()->get_elementor_page_list(),
                    'condition'     => [
                        'premium_notbar_content_type'    => 'template',
                    ],
                ]
            );
        
        $this->add_responsive_control('premium_notbar_temp_width',
                [
                    'label'         => esc_html__('Content Width (%)', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'condition'     => [
                        'premium_notbar_content_type'    => 'template',
                    ],
                    'selectors'     => [
                        '#premium-notbar-{{ID}} .premium-notbar-text-container'   => 'width: {{SIZE}}%;'
                    ]
                ]
                );
        
        $this->add_control('premium_notbar_icon_switcher',
                [
                    'label'         => esc_html__('Icon', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SWITCHER,
                    'default'       => 'yes',
                    'condition'     => [
                        'premium_notbar_content_type'    => 'editor',
                    ],
                ]
                );
        
        $this->add_control('premium_notbar_icon_selector',
            [
                'label'        => esc_html__('Icon Type','premium-addons-for-elementor'),
                'type'         => Controls_Manager::SELECT,
                'default'      => 'font-awesome-icon',
                'options'      =>[
                    'font-awesome-icon' => esc_html__('Font Awesome Icon','premium-addons-for-elementor'),
                    'custom-image'     => esc_html__('Custom Image','premium-addons-for-elementor'),
                    ],
                'condition'     => [
                    'premium_notbar_content_type'    => 'editor',
                    'premium_notbar_icon_switcher'    => 'yes',
                    ]
                ]
            );
        
        $this->add_control('premium_notbar_icon',
            [
                'label'             => esc_html__('Icon','premium-addons-for-elementor'),
                'type'              => Controls_Manager::ICON,
                'default'           => 'fa fa-exclamation-circle',
                'condition'         => [
                    'premium_notbar_icon_switcher'  => 'yes',
                    'premium_notbar_content_type'   => 'editor',
                    'premium_notbar_icon_selector'  => 'font-awesome-icon'
                ]
            ]
            );
        
        $this->add_control('premium_notbar_custom_image',
            [
                'label'        => esc_html__('Custom Image','premium-addons-for-elementor'),
                'type'         => Controls_Manager::MEDIA,
                'default'      => [
                    'url'	=> Utils::get_placeholder_image_src()
                ],
                'condition'    => [
                    'premium_notbar_icon_switcher'  => 'yes',
                    'premium_notbar_content_type'   => 'editor',
                    'premium_notbar_icon_selector'  => 'custom-image'
                ]
            ]
            );
        
        $this->add_control('premium_notbar_text',
                [
                    'type'          => Controls_Manager::WYSIWYG,
                    'dynamic'       => [ 'active' => true ],
                    'default'       => 'Morbi vel neque a est hendrerit laoreet in quis massa.',
                    'condition'     => [
                        'premium_notbar_content_type'    => 'editor',
                    ],
                    'show_label'    => false,
                ]
            );
        
        $this->add_control('premium_notbar_close_text',
                [
                    'label'         => esc_html__('Close Button Text', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'dynamic'       => [ 'active' => true ],
                    'default'       => 'x',
                ]
            );
        
        $this->add_responsive_control('premium_notbar_text_align',
            [
                'label'             => __( 'Alignment', 'premium-addons-for-elementor' ),
                'type'              => Controls_Manager::CHOOSE,
                'options'           => [
                    'left'    => [
                        'title' => __( 'Left', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-right',
                    ],
                ],
                'condition'     => [
                    'premium_notbar_content_type'    => 'editor',
                ],
                'selectors'         => [
                    '#premium-notbar-{{ID}} .premium-notbar-icon-text-container' => 'justify-content: {{VALUE}}; text-align: {{VALUE}};',
                ],
                'default' => 'left',
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_notbar_advanced',
            [
                'label'     => esc_html__('Advanced Settings', 'premium-addons-for-elementor'),
            ]
        );

        $this->add_control('premium_notbar_cookies',
            [
                'label'         => esc_html__('Use Cookies', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('This option will use cookies to remember user action', 'premium-addons-for-elementor')
            ]);
        
        $this->add_control('premium_notbar_interval',
            [
                'label'         => esc_html__('Expiration Time', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => 1,
                'min'           => 0,
                'title'         => esc_html__('How much time before removing cookie, set the value in hours, default is: 1 hour', 'premium-addons-for-elementor'),
                'condition'     => [
                    'premium_notbar_cookies'    => 'yes'
                ]
            ]
        );

        $this->add_control('premium_notbar_responsive_switcher',
            [
                'label'         => esc_html__('Responsive Controls', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('This options will hide the notification bar below a specific screen size', 'premium-addons-for-elementor')
            ]);
        
        $this->add_responsive_control('premium_notbar_height',
                [
                    'label'         => esc_html__('Height', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', 'em', 'vh'],
                    'selectors'     => [
                        '#premium-notbar-{{ID}} .premium-notbar-icon-text-container' => 'height: {{SIZE}}{{UNIT}};'
                        ]
                    ]
                );
        
        $this->add_responsive_control('premium_notbar_overflow',
                [
                    'label'         => esc_html__('Overflow', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SELECT,
                    'options'       => [
                        'visible'       => esc_html__('Show', 'premium-addons-for-elementor'),
                        'scroll'        => esc_html__('Scroll', 'premium-addons-for-elementor'),
                        'auto'          => esc_html__('Auto', 'premium-addons-for-elementor'),
                    ],
                    'label_block'   => true,
                    'default'       => 'auto',
                    'selectors'     => [
                        '#premium-notbar-{{ID}} .premium-notbar-icon-text-container' => 'overflow-y: {{VALUE}};'
                        ]
                    ]
                );
        
        $this->add_control('premium_notbar_hide_tabs',
            [
                'label'         => esc_html__('Hide on Tablets', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('Hide Notification Bar below Elementor\'s Tablet Breakpoint ', 'premium-addons-for-elementor'),
                'condition'     => [
                    'premium_notbar_responsive_switcher'    => 'yes'
                ],
            ]);
        
        $this->add_control('premium_notbar_hide_mobs',
            [
                'label'         => esc_html__('Hide on Mobiles', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('Hide Notification Bar below Elementor\'s Mobile Breakpoint ', 'premium-addons-for-elementor'),
                'condition'     => [
                    'premium_notbar_responsive_switcher'    => 'yes'
                ],
            ]);
        
        $this->end_controls_section();

        $this->start_controls_section('premium_notbar_style',
            [
                'label'             => esc_html__('Bar','premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
            ]);
        
        $this->add_control('premium_notbar_background',
            [
                'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'         => [
                    '#premium-notbar-{{ID}}'   => 'background-color: {{VALUE}};'
                ]
            ]);
        
        $this->add_group_control(
            Group_Control_Border::get_type(),
                [
                    'name'              => 'premium_notbar_border',
                    'selector'          => '#premium-notbar-{{ID}}',
                    ]
                );
        
        $this->add_control('premium_notbar_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%', 'em'],
                    'selectors'     => [
                        '#premium-notbar-{{ID}}' => 'border-radius: {{SIZE}}{{UNIT}};'
                        ]
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'name'              => 'premium_notbar_shadow',
                    'selector'          => '#premium-notbar-{{ID}}',
                    ]
                );
        
        $this->add_responsive_control('premium_notbar_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                        '#premium-notbar-{{ID}}' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                        ]
                    ]
                );
        
        $this->add_responsive_control('premium_notbar_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                        '#premium-notbar-{{ID}}' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                        ]
                    ]
                );
        
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_notbar_icon_style',
            [
                'label'             => esc_html__('Icon','premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
                'condition'         => [
                    'premium_notbar_icon_switcher'  => 'yes',
                    'premium_notbar_content_type'    => 'editor',
                ]
            ]);
        
        $this->add_control('premium_notbar_icon_color',
            [
                'label'             => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'condition'         => [
                    'premium_notbar_icon_switcher'  => 'yes',
                    'premium_notbar_content_type'   => 'editor',
                    'premium_notbar_icon_selector'  => 'font-awesome-icon'
                ],
                'selectors'         => [
                    '#premium-notbar-{{ID}} .premium-notbar-icon'   => 'color: {{VALUE}};'
                ]
            ]);
        
        $this->add_control('premium_notbar_icon_hover_color',
            [
                'label'             => esc_html__('Hover Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'condition'         => [
                    'premium_notbar_icon_switcher'  => 'yes',
                    'premium_notbar_content_type'   => 'editor',
                    'premium_notbar_icon_selector'  => 'font-awesome-icon'
                ],
                'selectors'         => [
                    '#premium-notbar-{{ID}}:hover .premium-notbar-icon'   => 'color: {{VALUE}};'
                ]
            ]);
        
        $this->add_responsive_control('premium_notbar_icon_size',
                [
                    'label'         => esc_html__('Size', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', 'em'],
                    'selectors'     => [
                        '#premium-notbar-{{ID}} .premium-notbar-icon' => 'font-size: {{SIZE}}px;',
                        '#premium-notbar-{{ID}} .premium-notbar-custom-image' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );
        
        $this->add_control('premium_notbar_icon_backcolor',
            [
                'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'selectors'         => [
                    '#premium-notbar-{{ID}} .premium-notbar-icon, #premium-notbar-{{ID}} .premium-notbar-custom-image'   => 'background-color: {{VALUE}};'
                ]
            ]);
        
        $this->add_group_control(
            Group_Control_Border::get_type(),
                [
                    'name'              => 'premium_notbar_icon_border',
                    'selector'          => '#premium-notbar-{{ID}} .premium-notbar-icon,#premium-notbar-{{ID}} .premium-notbar-custom-image'
                    ]
                );
        
        $this->add_control('premium_notbar_icon_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%', 'em'],
                    'selectors'     => [
                        '#premium-notbar-{{ID}} .premium-notbar-icon, #premium-notbar-{{ID}} .premium-notbar-custom-image' => 'border-radius: {{SIZE}}{{UNIT}};'
                        ]
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'             => esc_html__('Shadow', 'premium-addons-for-elementor'),
                    'name'              => 'premium_notbar_icon_shadow',
                    'selector'          => '#premium-notbar-{{ID}} .premium-notbar-icon',
                    'condition'         => [
                            'premium_notbar_icon_switcher'  => 'yes',
                            'premium_notbar_content_type'   => 'editor',
                            'premium_notbar_icon_selector'  => 'font-awesome-icon'
                        ],
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'name'              => 'premium_notbar_img_shadow',
                    'selector'          => '#premium-notbar-{{ID}} .premium-notbar-custom-image',
                    'condition'         => [
                            'premium_notbar_icon_switcher'  => 'yes',
                            'premium_notbar_content_type'   => 'editor',
                            'premium_notbar_icon_selector'  => 'custom-image'
                        ],
                    ]
                );
        
        $this->add_responsive_control('premium_notbar_icon_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                        '#premium-notbar-{{ID}} .premium-notbar-icon , #premium-notbar-{{ID}} .premium-notbar-custom-image' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                        ]
                    ]
                );
        
        $this->add_responsive_control('premium_notbar_icon_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                        '#premium-notbar-{{ID}} .premium-notbar-icon , #premium-notbar-{{ID}} .premium-notbar-custom-image' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                        ]
                    ]
                );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_notbar_text_style',
            [
                'label'             => esc_html__('Text','premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
                'condition'         => [
                    'premium_notbar_content_type'   => 'editor'
                ]
            ]);
        
        $this->add_control('premium_notbar_text_color',
            [
                'label'             => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'         => [
                    '#premium-notbar-{{ID}} .premium-notbar-text'   => 'color: {{VALUE}};'
                ]
            ]);
        
        $this->add_group_control(
            Group_Control_Typography::get_type(),
                [
                    'name'              => 'premium_notbar_text_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'          => '#premium-notbar-{{ID}} .premium-notbar-text',
            ]);
        
        $this->add_control('premium_notbar_text_backcolor',
            [
                'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'selectors'         => [
                    '#premium-notbar-{{ID}} .premium-notbar-text-container .premium-notbar-text'   => 'background-color: {{VALUE}};'
                ]
            ]);
        
        $this->add_group_control(
            Group_Control_Border::get_type(),
                [
                    'name'              => 'premium_notbar_text_border',
                    'selector'          => '#premium-notbar-{{ID}} .premium-notbar-text-container .premium-notbar-text',
                    ]
                );
        
        $this->add_control('premium_notbar_text_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%', 'em'],
                    'selectors'     => [
                        '#premium-notbar-{{ID}} .premium-notbar-text-container .premium-notbar-text' => 'border-radius: {{SIZE}}{{UNIT}};'
                        ]
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'             => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'              => 'premium_notbar_text_shadow',
                    'selector'          => '#premium-notbar-{{ID}} .premium-notbar-text .premium-notbar-text',
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'name'              => 'premium_notbar_text_box_shadow',
                    'selector'          => '#premium-notbar-{{ID}} .premium-notbar-text-container .premium-notbar-text',
                    ]
                );
        
        $this->add_responsive_control('premium_notbar_text_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                        '#premium-notbar-{{ID}} .premium-notbar-text-container .premium-notbar-text' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                        ]
                    ]
                );
        
        $this->add_responsive_control('premium_notbar_text_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                        '#premium-notbar-{{ID}} .premium-notbar-text-container .premium-notbar-text' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                        ]
                    ]
                );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_notbar_close_style',
            [
                'label'             => esc_html__('Close','premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
            ]);
        
        $this->add_control('premium_notbar_close_color',
            [
                'label'             => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'         => [
                    '#premium-notbar-{{ID}} .premium-notbar-close'   => 'color: {{VALUE}};'
                ]
            ]);
        
        $this->add_control('premium_notbar_close_hover_color',
            [
                'label'             => esc_html__('Hover Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'         => [
                    '#premium-notbar-{{ID}} .premium-notbar-close:hover'   => 'color: {{VALUE}};'
                ]
            ]);
        
        $this->add_group_control(
            Group_Control_Typography::get_type(),
                [
                    'name'              => 'premium_notbar_close_typo',
                    'scheme'            => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'          => '#premium-notbar-{{ID}} .premium-notbar-close',
            ]);
        
        $this->add_control('premium_notbar_close_backcolor',
            [
                'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'         => [
                    '#premium-notbar-{{ID}} .premium-notbar-close'   => 'background-color: {{VALUE}};'
                ]
            ]);
        
        $this->add_group_control(
            Group_Control_Border::get_type(),
                [
                    'name'              => 'premium_notbar_close_border',
                    'selector'          => '#premium-notbar-{{ID}} .premium-notbar-close',
                    ]
                );
        
        $this->add_control('premium_notbar_close_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%', 'em'],
                    'selectors'     => [
                        '#premium-notbar-{{ID}} .premium-notbar-close' => 'border-radius: {{SIZE}}{{UNIT}};'
                        ]
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'             => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'              => 'premium_notbar_close_shadow',
                    'selector'          => '#premium-notbar-{{ID}} .premium-notbar-close',
                    ]
                );
        
        $this->add_responsive_control('premium_notbar_close_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                        '#premium-notbar-{{ID}} .premium-notbar-close' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                        ]
                    ]
                );
        
        $this->add_responsive_control('premium_notbar_close_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                        '#premium-notbar-{{ID}} .premium-notbar-close' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                        ]
                    ]
                );
        
        $this->end_controls_section();
    }
    
    /**
     * returns the responsive style based on Elementor's Breakpoints
	 * @access protected
	 * @return string
	 */
    protected function get_notbar_responsive_style() {
        
        $breakpoints = Responsive::get_breakpoints();
        $style = '<style>';
        $style .= '@media ( max-width: ' . $breakpoints['md'] . 'px ) {';
        $style .= '.premium-notbar-text-container, .premium-notbar-icon-text-container {';
        $style .= 'flex-direction: column !important; -moz-flex-direction: column !important; -webkit-flex-direction: column !important;';
        $style .= '}';
        $style .= '}';
        $style .= '</style>';
        
        return $style;
        
    }

    protected function render() {
        
        $settings       = $this->get_settings_for_display();
        
        $image_custom   = $settings['premium_notbar_custom_image']['url'];
        
        $bar_position   = $settings['premium_notbar_position'];
        
        $bar_layout     = 'premium-notbar-'.$settings['premium_notbar_top_select'];
        
        $bar_width      = $settings['premium_notbar_width'];
        
        $elementor_post_id = $settings['premium_notbar_content_temp'];
        
        $premium_elements_frontend = new Frontend;
        
        $this->add_render_attribute( 'premium_notbar_text', 'class', 'premium-notbar-text' );
        
        $not_settings   = [
            'layout'    => $bar_width,
            'location'  => $bar_position,
            'position'  => $bar_layout,
            'varPos'    => !empty( $settings['premium_notbar_float_pos'] ) ? $settings['premium_notbar_float_pos'] : '10%',
            'responsive'=> ( $settings['premium_notbar_responsive_switcher'] == 'yes' ) ? true : false,
            'hideTabs'  => ( $settings['premium_notbar_hide_tabs'] == 'yes' ) ? true: false,
            'tabSize'   => ( $settings['premium_notbar_hide_tabs'] == 'yes' ) ? Responsive::get_breakpoints()['lg'] : Responsive::get_breakpoints()['lg'],
            'hideMobs'  => ( $settings['premium_notbar_hide_mobs'] == 'yes' ) ? true: false,
            'mobSize'   => ( $settings['premium_notbar_hide_mobs'] == 'yes' ) ? Responsive::get_breakpoints()['md'] : Responsive::get_breakpoints()['md'],
            'cookies'   => ( $settings['premium_notbar_cookies'] == 'yes' ) ? true : false,
            'interval'  => ! empty( $settings['premium_notbar_interval'] ) ? $settings['premium_notbar_interval'] : 1,
            'id'        => $this->get_id()
        ];
            
        
?>
<div id="premium-notbar-outer-container-<?php echo esc_attr($this->get_id()); ?>" class="premium-notbar-outer-container premium-notbar-<?php echo esc_attr( $settings['premium_notbar_content_type'] ); ?>" data-settings='<?php echo wp_json_encode($not_settings); ?>'>
    
    <div id="premium-notbar-<?php echo esc_attr($this->get_id()); ?>" class="premium-notbar <?php if($bar_position != 'top') echo 'premium-notbar-'.esc_attr($bar_position); elseif($bar_position == 'top' && is_user_logged_in()) echo 'premium-notbar-edit-top ' . $bar_layout; else echo 'premium-notbar-top ' . $bar_layout; ?> <?php echo 'premium-notbar-' . esc_attr($bar_width); ?>">
    
        <div class="premium-notbar-text-container">
            <div class="premium-notbar-icon-text-container">
                <?php if($settings['premium_notbar_icon_switcher'] == 'yes' && $settings['premium_notbar_content_type'] == 'editor' ) : ?>
                    <div class="premium-notbar-icon-wrap">
                    <?php if($settings['premium_notbar_icon_selector'] == 'font-awesome-icon' && !empty($settings['premium_notbar_icon'])) : ?>
                        
                            <i class="premium-notbar-icon <?php echo esc_attr( $settings['premium_notbar_icon'] ); ?>"></i>
                        
                    <?php else:?>

                        <img class="premium-notbar-custom-image" alt ="Premium Notification Bar" src="<?php echo $image_custom;?>" >

                    <?php endif; ?>
                    </div>
                <?php endif; ?>

                <?php if( $settings['premium_notbar_content_type'] == 'editor') : ?>

                    <span <?php echo $this->get_render_attribute_string('premium_notbar_text'); ?>><?php echo $settings['premium_notbar_text']; ?></span>

                <?php elseif ($settings['premium_notbar_content_type'] == 'template') : echo $premium_elements_frontend->get_builder_content($elementor_post_id, true); endif; ?>

            </div>
            <div class="premium-notbar-button-wrap">
                <button type="button" id="premium-notbar-close-<?php echo esc_attr($this->get_id()); ?>" class="premium-notbar-close <?php if($bar_position != 'top') echo 'premium-notbar-' . esc_attr( $bar_position ); elseif($bar_position == 'top' && is_user_logged_in()) echo 'premium-notbar-edit-top'; else echo 'premium-notbar-top'; ?>"><?php echo esc_html($settings['premium_notbar_close_text']); ?></button>
            </div>

        </div>
    
    </div>
    
</div>
<?php echo $this->get_notbar_responsive_style();
    }
}