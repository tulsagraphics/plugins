<?php

/**
 * Class: Premium_Flipbox_Widget
 * Name: Flip Box
 * Slug: premium-addon-flip-box
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Premium_Flipbox extends Widget_Base{
    
    public function getTemplateInstance() {
		return $this->templateInstance = premium_Template_Tags::getInstance();
	}
    
    public function get_name() {
        return 'premium-addon-flip-box';
    }
    
    public function get_title() {
		return \PremiumAddons\Helper_Functions::get_prefix() . ' Flip Box';
	}
    
    public function get_icon() {
        return 'pa-pro-flip-box';
    }

    public function get_categories() {
        return [ 'premium-elements' ];
    }

    public function get_script_depends() {
        return ['premium-pro-js'];
    }

    // Adding the controls fields for the Flip Box
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() { 
        
        $this->start_controls_section('premium_flip_front_settings',
            [
                'label'         => esc_html__('Front', 'premium-addons-for-elementor'),
            ]
        );

        $this->start_controls_tabs('premium_flip_front_tabs');
        
        $this->start_controls_tab('premium_flip_front_content_tab',
            [
                'label'         => esc_html__('Content', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_flip_icon_fa_switcher',
            [
                'label'         => esc_html__('Icon', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
            ]
        );
        
        $this->add_control('premium_flip_icon_selection',
            [
                'label'         => esc_html__('Icon Type', 'premium-addons-for-elementor'),
                'description'   => esc_html__('Select type for the icon', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'icon',
                'options'       => [
                    'icon'  => esc_html__('Font Awesome Icon','premium-addons-for-elementor'),
                    'image' => esc_html__('Custom Image','premium-addons-for-elementor'),
                    ],
                'label_block'   =>  true,
                'condition'     => [
                    'premium_flip_icon_fa_switcher' => 'yes',
                ],
            ]
        );
        
        $this->add_control('premium_flip_icon_fa', 
            [
                'label'         => esc_html__('Icon', 'premium-addons-for-elementor'),
                'description'   => esc_html__( 'Choose an Icon for Front Side', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::ICON,
                'label_block'   => true,
                'default'       => 'fa fa-picture-o',
                'condition'     => [
                    'premium_flip_icon_fa_switcher' => 'yes',
                    "premium_flip_icon_selection"   => 'icon'
                ],
            ]
        );
        
        $this->add_control('premium_flip_icon_image',
            [
                'label'         => esc_html__('Image', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::MEDIA,
                'default'       => [
                    'url'	=> Utils::get_placeholder_image_src(),
                    ],
                'description'   => esc_html__('Choose the icon image', 'premium-addons-for-elementor' ),
                'label_block'   => true,
                'condition'     => [
                    'premium_flip_icon_fa_switcher' => 'yes',
                    "premium_flip_icon_selection"   => 'image'
                ]
            ]
        );

        $this->add_control('premium_flip_icon_size',
            [
                'label'         => esc_html__('Icon Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px','em','%'],
                'default'       => [
                    'size'  => 40,
                ],
                'range'         => [
                    'px'    => [
                        'min' => 5,
                        'max' => 80
                    ],
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-icon' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
                'condition'     => [
                    'premium_flip_icon_fa_switcher' => 'yes',
                    'premium_flip_icon_selection'   => 'icon'
                ]
            ]
        );
        
        $this->add_control('premium_flip_image_size',
            [
                'label'         => esc_html__('Icon Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px','em'],
                'default'       => [
                    'size'  => 40,
                ],
                'range'         => [
                    'px'    => [
                        'min' => 5,
                        'max' => 200
                    ],
                ],
                'selectors'         => [
                    '{{WRAPPER}} .premium-flip-front-image' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
                ],
                'condition'         => [
                    'premium_flip_icon_fa_switcher' => 'yes',
                    'premium_flip_icon_selection'   => 'image'
                ]
            ]
        );
        
        $this->add_control('premium_flip_title_switcher',
            [
                'label'         => esc_html__('Title', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
            ]
        );

        $this->add_control('premium_flip_paragraph_header',
            [
                'label'         => esc_html__('Title', 'premium-addons-for-elementor'),
                'description'   => esc_html__( 'Type a title for the front side', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::TEXT,
                'dynamic'       => [ 'active' => true ],
                'default'       => esc_html__('Front Box Title', 'premium-addons-for-elementor'),
                'label_block'   => true,
                'condition'     => [
                    'premium_flip_title_switcher' => 'yes',
                ],
            ]
        );

        $this->add_control('premium_flip_paragraph_header_size',
            [
                'label'         => esc_html__('HTML Tag', 'premium-addons-for-elementor'),
                'description'   => esc_html__( 'Select the front side title tag', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'h3',
                'options'       => [
                    'h1' => 'H1',
                    'h2' => 'H2',
                    'h3' => 'H3',
                    'h4' => 'H4',
                    'h5' => 'H5',
                    'h6' => 'H6' 
                    ],
                'label_block'   =>  true,
                'condition'     => [
                    'premium_flip_title_switcher' => 'yes',
                ],
            ]
        );
        
        $this->add_control('premium_flip_description_switcher',
            [
                'label'         => esc_html__('Description', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
        );

        $this->add_control('premium_flip_paragraph_text',
            [
                'label'         => esc_html__('Description', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::WYSIWYG,
                'dynamic'       => [ 'active' => true ],
                'default'       => esc_html__('Your Cool Description', 'premium-addons-for-elementor'),
                'condition'     => [
                    'premium_flip_description_switcher' => 'yes',
                ],
            ]
        );

        $this->add_responsive_control('premium_flip_vertical_align',
            [
                'label'         => esc_html__( 'Vertical Position', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'top'      => [
                        'title'=> esc_html__( 'Top', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-long-arrow-up',
                    ],
                    'middle'    => [
                        'title'=> esc_html__( 'Middle', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-justify',
                    ],
                    'bottom'     => [
                        'title'=> esc_html__( 'Bottom', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-long-arrow-down',
                    ],
                ],
                'default'       => 'middle',
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-text-wrapper' => 'vertical-align: {{VALUE}};',
                ],
                'separator'     => 'before',
            ]
        );
        
        $this->add_responsive_control('premium_flip_text_align',
            [
                'label'         => esc_html__( 'Content Alignment', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'  => [
                        'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center'    => [
                        'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right'     => [
                        'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-right',
                    ],
                    'justify'   => [
                        'title'=> esc_html__( 'Justify', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-justify',
                    ],
                ],
                'default'       => 'center',
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab('premium_flip_front_background_tab',
            [
                'label'         => esc_html__('Background', 'premium-addons-for-elementor'),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'          => 'premium_flip_front_background_type',
                'types'         => [ 'classic', 'gradient' ],
                'selector'      => '{{WRAPPER}} .premium-flip-front',
            ]
        );
        
        $this->add_control('premium_flip_overlay_selection',
            [
                'label'         => esc_html__('Overlay Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-overlay'    => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section('premium_flip_back_settings',
            [
                'label'         => esc_html__('Back', 'premium-addons-for-elementor'),
            ]
        );

        $this->start_controls_tabs('premium_flip_back_tabs');
        
        $this->start_controls_tab('premium_flip_back_content_tab',
            [
                'label'         => esc_html__('Content', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_flip_back_icon_fa_switcher',
            [
                'label'         => esc_html__('Icon', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
            ]
        );
        
        $this->add_control('premium_flip_back_icon_selection',
            [
                'label'         => esc_html__('Icon Type', 'premium-addons-for-elementor'),
                'description'   => esc_html__('Select type for the icon', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'icon',
                'options'       => [
                    'icon'  => esc_html__('Font Awesome Icon','premium-addons-for-elementor'),
                    'image' => esc_html__('Custom Image','premium-addons-for-elementor'),
                    ],
                'label_block'   =>  true,
                'condition'     => [
                    'premium_flip_back_icon_fa_switcher' => 'yes',
                ],
            ]
        );
        
        $this->add_control('premium_flip_back_icon_fa',
            [
                'label'         => esc_html__('Icon', 'premium-addons-for-elementor'),
                'description'   => esc_html__( 'Choose an Icon for Back Side', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::ICON,
                'label_block'   => true,
                'default'       => 'fa fa-star-half-empty',
                'condition'     => [
                    'premium_flip_back_icon_fa_switcher'    => 'yes',
                    'premium_flip_back_icon_selection'      => 'icon',
                ],
            ]
        );
        
        $this->add_control('premium_flip_back_icon_image',
            [
                'label'         => esc_html__('Image', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::MEDIA,
                'default'       => [
                    'url'	=> Utils::get_placeholder_image_src(),
                ],
                'description'   => esc_html__('Choose the icon image', 'premium-addons-for-elementor' ),
                'label_block'   => true,
                'condition'     => [
                    'premium_flip_back_icon_fa_switcher' => 'yes',
                    "premium_flip_back_icon_selection"   => 'image'
                ]
            ]
        );

        $this->add_control('premium_flip_back_icon_size',
            [
                'label'         => esc_html__('Icon Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px','em','%'],
                'default'       => [
                    'size'  => 60,
                ],
                'range'         => [
                    'px'   => [
                        'min' => 5,
                        'max' => 80,
                    ]
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-icon' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
                'condition'     => [
                    'premium_flip_back_icon_fa_switcher'    => 'yes',
                    'premium_flip_back_icon_selection'      => 'icon'
                ]
            ]
        );
        
        $this->add_control('premium_flip_back_image_size',
            [
                'label'         => esc_html__('Icon Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px','em'],
                'default'       => [
                    'size'      => 40,
                ],
                'range'         => [
                    'px'    => [
                        'min' => 5,
                        'max' => 200
                    ],
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-image' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
                ],
                'condition'     => [
                    'premium_flip_back_icon_fa_switcher'    => 'yes',
                    'premium_flip_back_icon_selection'   => 'image'
                ]
            ]
        );
        
        $this->add_control('premium_flip_back_title_switcher',
            [
                'label'         => esc_html__('Title', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
            ]
        );

        $this->add_control('premium_flip_back_paragraph_header',
            [
                'label'         => esc_html__('Title', 'premium-addons-for-elementor'),
                'description'   => esc_html__( 'Type a title for the back side', 'premium-addons-for-elementor' ),
                'dynamic'       => [ 'active' => true ],
                'type'          => Controls_Manager::TEXT,
                'default'       => esc_html__('Back Box Title', 'premium-addons-for-elementor'),
                'label_block'   => true,
                'condition'     => [
                    'premium_flip_back_title_switcher' => 'yes',
                ],
            ]
        );

        $this->add_control('premium_flip_back_paragraph_header_size',
            [
                'label'         => esc_html__('HTML Tag', 'premium-addons-for-elementor'),
                'description'   => esc_html__('Select the tag for the title', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'default'       => esc_html__('h3','premium-addons-for-elementor'),
                'options'       => [
                    'h1' => 'H1',
                    'h2' => 'H2',
                    'h3' => 'H3',
                    'h4' => 'H4',
                    'h5' => 'H5',
                    'h6' => 'H6'
                    ],
                'label_block'   => true,
                'condition'     => [
                    'premium_flip_back_title_switcher' => 'yes',
                ],
            ]
        );

        $this->add_control('premium_flip_back_description_switcher',
            [
                'label'         => esc_html__('Description', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
        );
        
        $this->add_control('premium_flip_back_paragraph_text',
            [
                'label'         => esc_html__('Description', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::WYSIWYG,
                'dynamic'       => [ 'active' => true ],
                'default'       => esc_html__('Your Cool Description', 'premium-addons-for-elementor'),
                'label_block'   => true,
                'condition'     => [
                    'premium_flip_back_description_switcher' => 'yes',
                ],
            ]
        );

        $this->add_control('premium_flip_back_link_switcher',
            [
                'label'         => esc_html__('Link', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
        );

        $this->add_control('premium_flip_back_link_trigger', 
            [
                'label'         => esc_html__('Apply on', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'text'  => esc_html__('Button Only', 'premium-addons-for-elementor'),
                    'full'  => esc_html__('Whole Box', 'premium-addons-for-elementor'),
                ],
                'default'       => 'text',
                'label_block'   => true,
                'condition'		=> [
                    'premium_flip_back_link_switcher'	=> 'yes'
                ]
            ]
        );

        $this->add_control('premium_flip_back_link_text',
            [
                'label'         => esc_html__('Text', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'dynamic'       => [ 'active' => true ],
                'default'       => esc_html__('Click Me','premium-addons-for-elementor'),
                'label_block'   => true,
                'condition'		=> [
                    'premium_flip_back_link_trigger'	=> 'text',
                    'premium_flip_back_link_switcher'	=> 'yes'
                ]
            ]
        );
        
        $this->add_control('premium_flip_back_link_selection', 
            [
                'label'         => esc_html__('Link Type', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'url'   => esc_html__('URL', 'premium-addons-for-elementor'),
                    'link'  => esc_html__('Existing Page', 'premium-addons-for-elementor'),
                ],
                'default'       => 'url',
                'label_block'   => true,
                'condition'		=> [
                    'premium_flip_back_link_switcher'	=> 'yes'
                ]
            ]
        );
        
        $this->add_control('premium_flip_back_link',
            [
                'label'         => esc_html__('Link', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::URL,
                'default'       => [
                    'url'   => '#',
                ],
                'placeholder'   => 'https://premiumaddons.com/',
                'label_block'   => true,
                'separator'     => 'after',
                'condition'     => [
                    'premium_flip_back_link_switcher'	=> 'yes',
                    'premium_flip_back_link_selection' => 'url'
                ]
            ]
        );
        
        $this->add_control('premium_flip_back_existing_link',
            [
                'label'         => esc_html__('Existing Page', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT2,
                'options'       => $this->getTemplateInstance()->get_all_post(),
                'condition'     => [
                    'premium_flip_back_link_switcher'	=> 'yes',
                    'premium_flip_back_link_selection'     => 'link',
                ],
                'multiple'      => false,
                'separator'     => 'after',
                'label_block'   => true,
            ]
        );

        $this->add_responsive_control('premium_flip_back_vertical_align',
            [
                'label'         => esc_html__( 'Vertical Position', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'top'      => [
                        'title'=> esc_html__( 'Top', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-long-arrow-up',
                        ],
                    'middle'   => [
                        'title'=> esc_html__( 'Middle', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-justify',
                        ],
                    'bottom'   => [
                        'title'=> esc_html__( 'Bottom', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-long-arrow-down',
                        ],
                    ],
                'default'       => 'middle',
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-text-wrapper' => 'vertical-align: {{VALUE}};',
                ],
                'separator'     => 'before',
            ]
        );
        
        $this->add_responsive_control('premium_flip_back_text_align',
            [
                'label'         => esc_html__( 'Content Alignment', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'      => [
                        'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center'    => [
                        'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right'     => [
                        'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-right',
                    ],
                    'justify'   => [
                        'title'=> esc_html__( 'Justify', 'premium-addons-for-elementor' ),
                        'icon' => 'fa fa-align-justify',
                    ],
                ],
                'default'       => 'center',
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab('premium_flip_back_background_tab',
            [
                'label'          => esc_html__('Background', 'premium-addons-for-elementor'),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'          => 'premium_flip_back_background_type',
                'types'         => [ 'classic', 'gradient' ],
                'selector'      => '{{WRAPPER}} .premium-flip-back',
            ]
        );
        
        $this->add_control('premium_flip_back_overlay_selection',
            [
                'label'         => esc_html__('Overlay Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-overlay'    => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section('premium_flip_control_settings',
            [
                'label'         => esc_html__('Additional Settings', 'premium-addons-for-elementor'),
            ]
        );

        $this->add_control('premium_flip_direction',
            [
                'label'         => esc_html__('Flip Direction', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'rl' => esc_html__('Right to Left', 'premium-addons-for-elementor'),
                    'lr' => esc_html__('Left to Right', 'premium-addons-for-elementor'),
                    'tb' => esc_html__('Top to Bottom', 'premium-addons-for-elementor'),
                    'bt' => esc_html__('Bottom to Top', 'premium-addons-for-elementor'),
                ],
                'default'       => 'rl'
            ]   
        );

        $this->add_responsive_control('premium_flip_box_height',
            [
                'label'         => esc_html__('Height', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'default'       => [
                    'size'  => 380,
                ],
                'range'         => [
                    'px'    => [
                        'min' => 155, 
                        'max' => 1500,
                    ]
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front, {{WRAPPER}} .premium-flip-back, {{WRAPPER}} .premium-flip-main-Box'    => 'height: {{SIZE}}px;',
                ]
            ]
        );

        $this->start_controls_tabs('premium_flip_box_border_tabs');
        
        $this->start_controls_tab('premium_flip_box_border_normal',
            [
                'label'         => esc_html__('Normal', 'premium-addons-for-elementor'),
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_flip_border_settings_normal',
                'selector'      => '{{WRAPPER}} .premium-flip-front',
            ]
        );

        $this->add_control('premium_flip_border_radius_normal',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px','em','%'],
                'range'         => [
                    'px' => [
                        'min' => 0,
                        'max' => 150,
                    ]
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front'  => 'border-radius: {{SIZE}}{{UNIT}};',
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_flip_box_border_hover',
            [
                'label'         => esc_html__('hover', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_flip_border_settings_hover',
                'selector'      => '{{WRAPPER}} .premium-flip-main-Box:hover .premium-flip-back',
            ]
        );

        $this->add_control('premium_flip_border_radius_hover',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px','em','%'],
                'range'         => [
                    'px' => [
                        'min' => 0,
                        'max' => 150,
                    ]
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-main-Box:hover .premium-flip-back'  => 'border-radius: {{SIZE}}{{UNIT}};',
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section('premium_flip_front_section_title_style',
            [
                'label'         => esc_html__('Front', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'frontboxshadow',
                'selector'      => '{{WRAPPER}} .premium-flip-front',
            ]
        );

        $this->start_controls_tabs('premium_flip_box_style_tabs');
        
        $this->start_controls_tab('premium_flip_box_icon_style',
            [
                'label'         => esc_html__('Icon', 'premium-addons-for-elementor'),
            ]
        );

        $this->add_control('premium_flip_fa_color_selection',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-icon' => 'color: {{VALUE}};',
                ],
                'condition'     => [
                    'premium_flip_icon_selection'   => 'icon'
                ]
            ]
        );
        
        $this->add_control('premium_flip_fa_color_background_selection',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-icon, {{WRAPPER}} .premium-flip-front-image'    => 'background: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_flip_icon_border',
                'selector'      => '{{WRAPPER}} .premium-flip-front-icon,{{WRAPPER}} .premium-flip-front-image',
            ]
        );
        
        $this->add_control('premium_flip_icon_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px','em','%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-icon, {{WRAPPER}} .premium-flip-front-image'  => 'border-radius: {{SIZE}}{{UNIT}};',
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                'name'          => 'premium_flip_icon_shadow',
                'selector'      => '{{WRAPPER}} .premium-flip-front-icon, {{WRAPPER}} .premium-flip-front-image',
                'condition'     => [
                    'premium_flip_icon_selection'   => 'icon'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                'name'          => 'premium_flip_image_shadow',
                'selector'      => '{{WRAPPER}} .premium-flip-front-image',
                'condition'     => [
                    'premium_flip_icon_selection'   => 'image'
                ]
            ]
        );

        $this->add_responsive_control('premium_flip_icon_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-icon , {{WRAPPER}} .premium-flip-front-image' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        
        $this->add_responsive_control('premium_flip_icon_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-icon, {{WRAPPER}} .premium-flip-front-image' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab('premium_flip_box_title_style',
            [
                'label'         => esc_html__('Title', 'premium-addons-for-elementor'),
            ]
        );

        $this->add_control('premium_flip_title_color',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-title' => 'color: {{VALUE}};',
                ],
                'separator'     => 'before',
            ]
        );
        
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'premium_flip_front_title_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-flip-front-title',
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'premium_flip_title_shadow',
                'selector'      => '{{WRAPPER}} .premium-flip-front-title',
            ]
        );
        
        $this->add_control('premium_flip_title_background_color',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-title'    => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control('premium_flip_title_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );
        
        $this->add_responsive_control('premium_flip_title_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );
        
        $this->end_controls_tab();

        $this->start_controls_tab('premium_flip_box_description_style',
            [
                'label'         => esc_html__('Description', 'premium-addons-for-elementor'),
            ]
        );

        $this->add_control('premium_flip_desc_color',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-description' => 'color: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'premium_flip_desc_typography',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-flip-front-description',
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'premium_flip_description_shadow',
                'selector'      => '{{WRAPPER}} .premium-flip-front-description',
            ]
        );
        
        $this->add_control('premium_flip_description_background_color',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-description'    => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control('premium_flip_desc_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-description' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );
        
        $this->add_responsive_control('premium_flip_desc_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-front-description' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section('back_section_title_style',
            [
                'label'         => esc_html__('Back', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'backboxshadow',
                'selector'      => '{{WRAPPER}} .premium-flip-back',
            ]
        );

        $this->start_controls_tabs('premium_flip_box_back_style_tabs');
        
        $this->start_controls_tab('premium_flip_box_back_icon_style',
            [
                'label'         => esc_html__('Icon', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_flip_back_fa_color_selection',
            [
                'label'         => esc_html__('Icon Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-icon' => 'color: {{VALUE}};',
                ],
                'condition'     => [
                    'premium_flip_back_icon_fa_switcher'    => 'yes',
                    'premium_flip_back_icon_selection'      => 'icon'
                ]
            ]
        );
        
        $this->add_control('premium_flip_back_fa_color_background_selection',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-icon'    => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_flip_back_icon_border',
                'selector'      => '{{WRAPPER}} .premium-flip-back-icon, {{WRAPPER}} .premium-flip-back-image',
            ]
        );

        $this->add_control('premium_flip_back_icon_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px','em','%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-icon, {{WRAPPER}} .premium-flip-back-image'  => 'border-radius: {{SIZE}}{{UNIT}};',
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'     => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'      => 'premium_flip_back_icon_shadow',
                    'selector'  => '{{WRAPPER}} .premium-flip-back-icon, {{WRAPPER}} .premium-flip-back-image',
                    'condition' => [
                        'premium_flip_back_icon_selection'   => 'icon'
                    ]
                ]
            );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                'name'          => 'premium_flip_back_image_shadow',
                'selector'      => '{{WRAPPER}} .premium-flip-back-image',
                'condition'     => [
                    'premium_flip_back_icon_selection'   => 'image'
                ]
            ]
        );

        $this->add_responsive_control('premium_flip_back_icon_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        
        $this->add_responsive_control('premium_flip_back_icon_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-icon' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_flip_box_back_title_style',
            [
                'label'         => esc_html__('Title', 'premium-addons-for-elementor'),
            ]
        );

        $this->add_control('premium_flip_back_title_color',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-title' => 'color: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'          => 'premium_flip_back_title_typo',
				'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-flip-back-title',
			]
		);
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'premium_flip_back_title_shadow',
                'selector'      => '{{WRAPPER}} .premium-flip-back-title',
            ]
        );
        
        $this->add_control('premium_flip_back_title_background_color',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-title'    => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control('premium_flip_back_title_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{RIGHT}}{{UNIT}};',
                ],
            ]
        );
        
        $this->add_responsive_control('premium_flip_back_title_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{RIGHT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_flip_box_back_description_style',
            [
                'label'         => esc_html__('Description', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_flip_back_desc_color',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-description' => 'color: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'          => 'premium_flip_back_desc_typography',
				'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-flip-back-description',
			]
		);
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'premium_flip_back_description_shadow',
                'selector'      => '{{WRAPPER}} .premium-flip-back-description',
                ]
            );
        
        $this->add_control('premium_flip_back_description_background_color',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-description'    => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control('premium_flip_back_desc_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-description' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}}  {{LEFT}}{{UNIT}};',
                    ]
                ]
            );
        
        $this->add_responsive_control('premium_flip_back_desc_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-back-description' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}}  {{LEFT}}{{UNIT}};',
                ]
            ]
        );
        
       $this->end_controls_tab();

       $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section('premium_flip_box_link_style',
            [
                'label'         => esc_html__('Link', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_flip_back_link_switcher'    => 'yes',
                    'premium_flip_back_link_trigger'    => 'text'
                ]
            ]
        );
       
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'premium_flip_box_link_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-flip-box-link',
                'condition'     => [
                    'premium_flip_back_link_switcher'   => 'yes',
                    'premium_flip_back_link_trigger'    => 'text'
                ]
            ]
        );
        
        $this->start_controls_tabs('premium_flip_box_link_style_tabs');
        
        $this->start_controls_tab('premium_flip_box_link_style_normal',
            [
                'label'         => esc_html__('Normal', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_flip_box_link_color',
            [
                'label'         => esc_html__('Text Color','premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-box-link' => 'color:{{VALUE}};'
                ],	
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'premium_flip_box_link_shadow',
                'selector'      => '{{WRAPPER}} .premium-flip-box-link',
            ]
        );
        
        $this->add_control('premium_flip_box_link_background',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-box-link'  => 'background-color: {{VALUE}};',
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_flip_box_link_border',
                'selector'      => '{{WRAPPER}} .premium-flip-box-link',
            ]
        );
        
        $this->add_control('premium_flip_box_link_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-box-link' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control('premium_flip_box_link_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-box-link' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control('premium_flip_box_link_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-box-link' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_flip_box_link_style_hover',
            [
                'label'         => esc_html__('Hover', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_flip_box_link_hover_color',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-box-link:hover'   => 'color: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'premium_flip_box_link_hover_shadow',
                'selector'      => '{{WRAPPER}} .premium-flip-box-link:hover',
            ]
        );
        
        $this->add_control('premium_flip_box_link_hover_background',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-box-link:hover' => 'background-color: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_flip_box_link_hover_border',
                'selector'      => '{{WRAPPER}} .premium-flip-box-link:hover',
            ]
        );
        
        $this->add_control('premium_flip_box_link_hover_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-box-link:hover' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control('premium_flip_box_link_hover_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-box-link:hover' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control('premium_flip_box_link_hover_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-flip-box-link:hover' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->end_controls_tabs();
        
        $this->end_controls_section();
    }

    /**
	 * renders the HTML content of the widget
	 * @return void
	 */
    protected function render() {
        
        $settings   = $this->get_settings_for_display();
        
        $trigger    = $settings['premium_flip_back_link_trigger'];
        
        if( 'url' == $settings['premium_flip_back_link_selection'] ) {
            
            $button_url = $settings['premium_flip_back_link']['url'];
            
        } else {
            
            $button_url = get_permalink($settings['premium_flip_back_existing_link']);
            
        }
    ?>
    
    <div class="premium-flip-main-Box">
        
    <?php if( 'yes' == $settings['premium_flip_back_link_switcher'] && 'full' == $trigger ) : ?>
        
        <a class="premium-flip-box-link" <?php if( !empty( $button_url ) ) : ?> href="<?php echo esc_url( $button_url ); ?>"<?php endif;?><?php if(!empty( $settings['premium_flip_back_link']['is_external'] ) ) : ?> target="_blank"<?php endif; ?><?php if( !empty( $settings['premium_flip_back_link']['nofollow'] ) ) : ?> rel="nofollow" <?php endif; ?>>
            
	<?php endif; ?>
            
        <div class="premium-flip-back premium-flip-back<?php echo $settings['premium_flip_direction']; ?>">
            <div class="premium-flip-back-overlay">
                <div class="premium-flip-back-content-container">
                    <div class="premium-flip-back-content">
                        <div class="premium-flip-back-text-wrapper">
                            
                            <?php if( 'yes' == $settings['premium_flip_back_icon_fa_switcher'] && 'icon' == $settings['premium_flip_back_icon_selection'] ) : ?>
                            
                            <i class="premium-flip-back-icon <?php echo $settings['premium_flip_back_icon_fa']; ?>"></i>
                            
                            <?php elseif( 'yes' == $settings['premium_flip_back_icon_fa_switcher'] && 'image' == $settings['premium_flip_back_icon_selection'] ) : ?>
                            <img alt="back side img" class="premium-flip-back-image" src="<?php echo $settings['premium_flip_back_icon_image']['url']; ?>">
                            <?php endif; ?>
                            
                            <?php if( 'yes' == $settings['premium_flip_back_title_switcher'] && !empty( $settings['premium_flip_back_paragraph_header'] ) ) : ?>
                            
                                <<?php echo $settings[ 'premium_flip_back_paragraph_header_size']; ?> class="premium-flip-back-title">
                                    <?php echo $settings['premium_flip_back_paragraph_header']; ?>
                                </<?php echo $settings[ 'premium_flip_back_paragraph_header_size']; ?>>
                            
                            <?php endif; ?>
                            
                            <?php if( $settings['premium_flip_back_description_switcher'] == 'yes' ) : ?>
                            
                                <span class="premium-flip-back-description"><?php  echo $settings['premium_flip_back_paragraph_text']; ?></span>
                                
                            <?php endif; ?>
                            
                            <?php if($settings['premium_flip_back_link_switcher'] == 'yes' && $trigger == 'text') : ?>
                                
                            	<a class="premium-flip-box-link text" <?php if( !empty( $button_url ) ) : ?> href="<?php echo esc_url( $button_url ); ?>"<?php endif;?><?php if( !empty( $settings['premium_flip_back_link']['is_external'] ) ) : ?> target="_blank" <?php endif; ?><?php if( !empty( $settings['premium_flip_back_link']['nofollow'] ) ) : ?> rel="nofollow" <?php endif; ?>><?php echo esc_html__( $settings['premium_flip_back_link_text'] ); ?></a>
                                
                        	<?php endif; ?>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    <?php if( $trigger == 'full' ) : ?>
        </a>
	<?php endif; ?>

        <div class="premium-flip-front premium-flip-front<?php echo $settings['premium_flip_direction']; ?>">
            <div class="premium-flip-front-overlay">
                <div class="premium-flip-front-content-container">
                    <div class="premium-flip-front-content">
                        <div class="premium-flip-text-wrapper">
                            <?php if( 'yes' == $settings['premium_flip_icon_fa_switcher'] &&  'icon' == $settings['premium_flip_icon_selection'] ) : ?>
                            
                                <i class="premium-flip-front-icon <?php echo $settings['premium_flip_icon_fa']; ?>"></i>
                                
                            <?php elseif( 'yes' == $settings['premium_flip_icon_fa_switcher'] && 'image' == $settings['premium_flip_icon_selection'] ) : ?>
                                <img alt="front side img" class="premium-flip-front-image" src="<?php echo $settings['premium_flip_icon_image']['url']; ?>">
                            <?php endif; ?>
                                
                            <?php if( 'yes' == $settings['premium_flip_title_switcher'] && !empty( $settings['premium_flip_paragraph_header'] ) ) : ?>
                                <<?php echo $settings['premium_flip_paragraph_header_size']; ?> class="premium-flip-front-title">
                                    <?php echo $settings['premium_flip_paragraph_header']; ?>
                                </<?php echo $settings['premium_flip_paragraph_header_size']; ?>>
                                
                            <?php endif; ?>
                                
                            <?php if( 'yes' == $settings['premium_flip_description_switcher'] ) : ?>
                                
                                <div class="premium-flip-front-description">
                                    <?php echo $settings['premium_flip_paragraph_text']; ?>
                                </div> 
                                
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
    <?php
    }
    
    protected function _content_template() {
        ?>
        <#
            var trigger = settings.premium_flip_back_link_trigger,
            
                buttonUrl = 'url' == settings.premium_flip_back_link_selection ? settings.premium_flip_back_link.url : settings.premium_flip_back_existing_link,
                
                backTitleTag = settings.premium_flip_back_paragraph_header_size,
                
                frontTitleTag = settings.premium_flip_paragraph_header_size,
                
                FlipDir = settings.premium_flip_direction;
                
            view.addRenderAttribute('back_side_wrap', 'class', ['premium-flip-back','premium-flip-back' + FlipDir ] );
            
            view.addRenderAttribute('front_side_wrap', 'class', ['premium-flip-front','premium-flip-front' + FlipDir ] );
        
        #>
        
        <div class="premium-flip-main-Box">
        
            <# if( 'yes' == settings.premium_flip_back_link_switcher && 'full' == trigger ) { #>
        
                <a class="premium-flip-box-link" href="{{ buttonUrl }}">
            
            <# } #>
            
            <div {{{ view.getRenderAttributeString('back_side_wrap') }}}>
                <div class="premium-flip-back-overlay">
                    <div class="premium-flip-back-content-container">
                        <div class="premium-flip-back-content">
                            <div class="premium-flip-back-text-wrapper">
                                <# if( 'yes' == settings.premium_flip_back_icon_fa_switcher && 'icon' == settings.premium_flip_back_icon_selection ) { #>
                                    <i class="premium-flip-back-icon {{ settings.premium_flip_back_icon_fa }} "></i>
                            
                                <# } else if( 'yes' == settings.premium_flip_back_icon_fa_switcher && 'image' == settings.premium_flip_back_icon_selection ) { #>
                                    <img alt="back side img" class="premium-flip-back-image" src="{{ settings.premium_flip_back_icon_image.url }}">
                                <# } #>
                                
                                <# if( 'yes' == settings.premium_flip_back_title_switcher && '' != settings.premium_flip_back_paragraph_header ) { #>
                            
                                <{{{backTitleTag}}} class="premium-flip-back-title">{{{ settings.premium_flip_back_paragraph_header }}}</{{{backTitleTag}}}>
                            
                                <# } #>
                                
                                <# if( 'yes' == settings.premium_flip_back_description_switcher ) { #>
                                
                                    <div class="premium-flip-back-description">{{{settings.premium_flip_back_paragraph_text}}}</div> 
                                
                                <# } #>
                                
                                <# if( 'yes' == settings.premium_flip_back_link_switcher && 'text' == trigger ) { #>
                                
                            	<a class="premium-flip-box-link text" href="{{ buttonUrl }}">{{{ settings.premium_flip_back_link_text }}}</a>
                                
                                <# } #>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <# if( 'full' == trigger ) { #>
                </a>
            <# } #>
            
            <div {{{ view.getRenderAttributeString('front_side_wrap') }}}>
                <div class="premium-flip-front-overlay">
                    <div class="premium-flip-front-content-container">
                        <div class="premium-flip-front-content">
                            <div class="premium-flip-text-wrapper">
                                <# if( 'yes' == settings.premium_flip_icon_fa_switcher && 'icon' == settings.premium_flip_icon_selection ) { #>
                                    <i class="premium-flip-front-icon {{ settings.premium_flip_icon_fa }} "></i>
                            
                                <# } else if( 'yes' == settings.premium_flip_icon_fa_switcher && 'image' == settings.premium_flip_icon_selection ) { #>
                                    <img alt="front side img" class="premium-flip-front-image" src="{{ settings.premium_flip_icon_image.url }}">
                                <# } #>
                                
                                <# if( 'yes' == settings.premium_flip_title_switcher && '' != settings.premium_flip_paragraph_header ) { #>
                            
                                <{{{frontTitleTag}}} class="premium-flip-front-title">{{{ settings.premium_flip_paragraph_header }}}</{{{frontTitleTag}}}>
                            
                                <# } #>
                                
                                <# if( 'yes' == settings.premium_flip_description_switcher ) { #>
                                
                                    <div class="premium-flip-front-description">{{{settings.premium_flip_paragraph_text}}}</div> 
                                
                                <# } #>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        <?php
    }
}