<?php

/**
 * Class: Premium_iHover_Widget
 * Name: iHover
 * Slug: premium-ihover
 */

namespace Elementor;

if(!defined('ABSPATH')) exit;

class Premium_Ihover extends Widget_Base {

    protected $templateInstance;
    
    public function getTemplateInstance() {
        return $this->templateInstance = premium_Template_Tags::getInstance();
    }

    public function get_name() {
        return 'premium-ihover';
    }
    
    public function get_title() {
        return \PremiumAddons\Helper_Functions::get_prefix() . ' iHover';
    }
    
    public function get_icon() {
        return 'pa-pro-ihover';
    }
    
    public function get_categories() {
        return ['premium-elements'];
    }
    
    
    // Adding the controls fields for the iHover
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls(){

        $this->start_controls_section('premium_ihover_image_content_section',
            [
                'label'         => esc_html__('Image','premium-addons-for-elementor'),
            ]
        );

        $this->add_control('premium_ihover_thumbnail_front_image',
            [
                'label'         => esc_html__( 'Choose Image', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::MEDIA,
                'default'       => [
                    'url' => Utils::get_placeholder_image_src(),
                ],
                'label_block'   => true
            ]
        );
        
        $this->add_responsive_control('premium_ihover_thumbnail_size',
            [
                'label'         => esc_html__('Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em', '%'],
                'range'         => [
                    'px'    => [
                        'min'   => 0,
                        'max'   => 500
                    ],
                    'em'    => [
                        'min'   => 0,
                        'max'   => 20
                    ]
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-item.style20 .premium-ihover-spinner, {{WRAPPER}} .premium-ihover-item, {{WRAPPER}} .premium-ihover-img-wrap, {{WRAPPER}} .premium-ihover-img-wrap .premium-ihover-img, {{WRAPPER}} .premium-ihover-info-wrap' => 'width: {{SIZE}}{{UNIT}};height:{{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_control('premium_ihover_thumbnail_hover_effect',
            [
                'label'         => esc_html__( 'Hover Effect', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'style18',
                'options'       => [
                    'style18'       => 'Advertising',
                    'style19'       => 'Book Cover',
                    'style10'       => 'Backward',
                    'style15'       => 'Faded In Background',
                    'style17'       => 'Flash Rotation',
                    'style4'        => 'Flip Background',
                    'style16'       => 'Flip Door',
                    'style9'        => 'Heroes Flying-Top',
                    'style9-1'      => 'Heroes Flying-Bottom',
                    'style9-2'      => 'Heroes Flying-Right',
                    'style14'       => 'Magic Door',
                    'style2'        => 'Reduced Image-Top',
                    'style2-2'      => 'Reduced Image-Right',
                    'style6'        => 'Reduced Image-Bottom',
                    'style2-1'      => 'Reduced Image-Left',
                    'style7'        => 'Rotated Image-Left',
                    'style7-1'      => 'Rotated Image-Right',
                    'style13'       => 'Rotated Wheel Image-Left',
                    'style8'        => 'Rotating Wheel-Left',
                    'style8-1'      => 'Rotating Wheel-Top',
                    'style8-2'      => 'Rotating Wheel-Bottom',
                    'style8-3'      => 'Rotating Wheel-Right',
                    'style1'        => 'Rotor Cube',
                    'style11'       => 'Slided Out Image',
                    'style12'       => 'Slided In Image',
                    'style20'       => 'Spinner',
                    'style5'        => 'Zoom In ',
                    'style5-1'      => 'Zoom Out'
                ],
                'label_block'   => true,
            ]
        );
         
        $this->add_control('premium_ihover_thumbnail_link_switcher',
            [
                'label'         => esc_html__('Link', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('Add a custom link or select an existing page link','premium-addons-for-elementor')
            ]
        );

        $this->add_control('premium_ihover_thumbnail_link_type', 
            [
                'label'         => esc_html__('Link/URL', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'url'   => esc_html__('URL', 'premium-addons-for-elementor'),
                    'link'  => esc_html__('Existing Page', 'premium-addons-for-elementor'),
                ],
                'default'       => 'url',
                'condition'     => [
                   'premium_ihover_thumbnail_link_switcher'  => 'yes',
                ],
                'label_block'   => true,
            ]
        );

        $this->add_control('premium_ihover_thumbnail_existing_page', 
            [
                'label'         => esc_html__('Existing Page', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT2,
                'options'       => $this->getTemplateInstance()->get_all_post(),
                'condition'     => [
                   'premium_ihover_thumbnail_link_switcher'  => 'yes',
                    'premium_ihover_thumbnail_link_type'     => 'link',
                ],
                'multiple'      => false,
                'label_block'   => true,
            ]
        );

        $this->add_control('premium_ihover_thumbnail_url',
            [
                'label'         => esc_html__('URL', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::URL,
                'placeholder'   => 'https://premiumaddons.com/',
                'default'       => [
                  'url'     => '#'
                ],
                'condition'     => [
                    'premium_ihover_thumbnail_link_switcher'  => 'yes',
                    'premium_ihover_thumbnail_link_type'     => 'url',
                ],
                'label_block'   => true
            ]
        );

        $this->add_control('premium_ihover_thumbnail_link_text',
            [
                'label'         => esc_html__('Link Title', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'dynamic'       => [ 'active' => true ],
                'condition'     => [
                    'premium_ihover_thumbnail_link_switcher' => 'yes',
                ],
                'label_block'   => true
            ]
        );

        $this->add_control('premium_ihover_thumbnail_alignment', 
            [
                'label'         => esc_html__('Alignment', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'  => [
                        'title'     => esc_html__('Left', 'premium-addons-for-elementor'),
                        'icon'      => 'fa fa-align-left'
                    ],
                    'center'=> [
                        'title'     => esc_html__('Center', 'premium-addons-for-elementor'),
                        'icon'      => 'fa fa-align-center'
                    ],
                    'right' => [
                        'title'     => esc_html__('Right', 'premium-addons-for-elementor'),
                        'icon'      => 'fa fa-align-right'
                    ],
                ],
                'default'       => 'center',
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-list' => 'text-align: {{VALUE}}'
                ]
            ]
        );

        $this->add_control(
            'premium_ihover_css_classes',
            [
                'label'         => esc_html__( 'CSS Classes', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::TEXT,
                'default'       => '',
                'label_block'   => true,
                'title'         => esc_html__( 'Add your custom class without the dot. e.g: my-class', 'premium-addons-for-elementor' )
            ]
        );
        
        $this->end_controls_section();

        $this->start_controls_section('premium_ihover_description_content_section',
            [
                'label'         => esc_html__('Content','premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_ihover_icon_fa_switcher',
            [
                'label'         => esc_html__('Icon', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
            ]
        );
        
        $this->add_control('premium_ihover_icon_selection',
            [
                'label'         => esc_html__('Icon Type', 'premium-addons-for-elementor'),
                'description'   => esc_html__('Select type for the icon', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'icon',
                'options'       => [
                    'icon'  => esc_html__('Font Awesome Icon','premium-addons-for-elementor'),
                    'image' => esc_html__('Custom Image','premium-addons-for-elementor'),
                ],
                'label_block'   =>  true,
                'condition'     => [
                    'premium_ihover_icon_fa_switcher' => 'yes',
                ],
            ]
        );
        
        $this->add_control('premium_ihover_icon_fa', 
            [
                'label'         => esc_html__('Icon', 'premium-addons-for-elementor'),
                'description'   => esc_html__( 'Choose an Icon for Front Side', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::ICON,
                'label_block'   => true,
                'default'       => 'fa fa-picture-o',
                'condition'     => [
                    'premium_ihover_icon_fa_switcher' => 'yes',
                    "premium_ihover_icon_selection"   => 'icon'
                ],
            ]
        );
        
        $this->add_control('premium_ihover_icon_image',
            [
                'label'         => esc_html__('Image', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::MEDIA,
                'default'       => [
                    'url'   => Utils::get_placeholder_image_src(),
                    ],
                'description'   => esc_html__('Choose the icon image', 'premium-addons-for-elementor' ),
                'label_block'   => true,
                'condition'     => [
                    'premium_ihover_icon_fa_switcher' => 'yes',
                    "premium_ihover_icon_selection"   => 'image'
                ]
            ]
        );

        $this->add_control('premium_ihover_icon_size',
            [
                'label'         => esc_html__('Icon Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px','em','%'],
                'default'       => [
                    'size'  => 30,
                ],
                'range'         => [
                    'px'    => [
                        'min' => 5,
                        'max' => 80
                    ],
                ],
                'selectors'         => [
                    '{{WRAPPER}} .premium-ihover-icon' => 'font-size: {{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} .premium-ihover-icon-image' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
                ],
                'condition'      => [
                    'premium_ihover_icon_fa_switcher' => 'yes',
                ]
            ]
        );

        $this->add_control('premium_ihover_thumbnail_back_title_switcher', 
            [
                'label'         => esc_html__('Title', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('Enable/Disable Title','premium-addons-for-elementor'),
                'default'       => 'yes',
            ]
        );

        $this->add_control('premium_ihover_thumbnail_back_title',
            [
                'label'         => esc_html__( 'Text', 'premium-addons-for-elementor' ),
                'placeholder'   => esc_html__( 'Awesome Title', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::TEXT,
                'dynamic'       => [ 'active' => true ],
                'dynamic'       => [
                    'active'        => true,
                ],
                'default'       => esc_html__( 'Awesome Title', 'premium-addons-for-elementor' ),
                'condition'     => [
                   'premium_ihover_thumbnail_back_title_switcher'  => 'yes',
                ],
                'label_block'   => false
            ]
        );

         $this->add_control('premium_ihover_thumbnail_back_title_tag',
            [
                'label'         => esc_html__( 'HTML Tag', 'premium-addons-for-elementor' ),
                'description'   => esc_html__( 'Select a heading tag for the title. Headings are defined with H1 to H6 tags', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'h4',
                'options'       => [
                    'h1'    => 'H1',
                    'h2'    => 'H2',
                    'h3'    => 'H3',
                    'h4'    => 'H4',
                    'h5'    => 'H5',
                    'h6'    => 'H6'
                ],
                'condition'     => [
                   'premium_ihover_thumbnail_back_title_switcher'  => 'yes',
                ],
                'label_block'   => true,
            ]
        );

        $this->add_control('premium_ihover_thumbnail_back_separator_switcher', 
            [
                'label'         => esc_html__('Separator', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('Enable/Disable Separator','premium-addons-for-elementor'),
            ]
        );

        $this->add_control('premium_ihover_thumbnail_back_description_switcher', 
            [
                'label'         => esc_html__('Description', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('Enable/Disable Description','premium-addons-for-elementor'),
            ]
        );

        $this->add_control('premium_ihover_thumbnail_back_description',
            [
                'label'         => esc_html__( 'Text', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::WYSIWYG,
                'dynamic'       => [ 'active' => true ],
                'default'       => esc_html__( 'Cool Description', 'premium-addons-for-elementor' ),
                'condition'     => [
                   'premium_ihover_thumbnail_back_description_switcher'  => 'yes',
                ],
                'dynamic'       => [
                    'active'    => true
                ],
                'label_block'   => true
            ]
        );

        /** Description Alignment **/
        $this->add_control('premium_ihover_description_alignment_content', 
            [
                'label'         => esc_html__('Alignment', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'  => [
                        'title'     => esc_html__('Left', 'premium-addons-for-elementor'),
                        'icon'      => 'fa fa-align-left'
                    ],
                    'center'=> [
                        'title'     => esc_html__('Center', 'premium-addons-for-elementor'),
                        'icon'      => 'fa fa-align-center'
                    ],
                    'right' => [
                        'title'     => esc_html__('Right', 'premium-addons-for-elementor'),
                        'icon'      => 'fa fa-align-right'
                    ],
                ],
                'default'       => 'center',
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-content-wrap' => 'text-align: {{VALUE}}'
                ]
            ]
        );


        $this->end_controls_section();
        
        $this->start_controls_section('premium_ihover_icon_style_section',
            [
                'label'         => esc_html__( 'Icon', 'premium-addons-for-elementor' ),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_ihover_icon_fa_switcher'  => 'yes'
                ]
            ]
        );
        
        $this->add_control('premium_ihover_fa_color_selection',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-icon' => 'color: {{VALUE}};',
                ],
                'condition'     => [
                    'premium_ihover_icon_selection'   => 'icon'
                ]
            ]
        );
        
        $this->add_control('premium_ihover_icon_background',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-icon, {{WRAPPER}} .premium-ihover-icon-image'    => 'background: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_ihover_icon_border',
                'selector'      => '{{WRAPPER}} .premium-ihover-icon,{{WRAPPER}} .premium-ihover-icon-image',
            ]
        );
        
        $this->add_control('premium_ihover_icon_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px','em','%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-icon, {{WRAPPER}} .premium-ihover-icon-image'  => 'border-radius: {{SIZE}}{{UNIT}};',
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                'name'          => 'premium_ihover_icon_shadow',
                'selector'      => '{{WRAPPER}} .premium-ihover-icon, {{WRAPPER}} .premium-ihover-icon-image',
                'condition'     => [
                    'premium_ihover_icon_selection'   => 'icon'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                'name'          => 'premium_ihover_image_shadow',
                'selector'      => '{{WRAPPER}} .premium-ihover-icon-image',
                'condition'     => [
                    'premium_ihover_icon_selection'   => 'image'
                ]
            ]
        );

        $this->add_responsive_control('premium_ihover_icon_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-icon , {{WRAPPER}} .premium-ihover-icon-image' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        
        $this->add_responsive_control('premium_ihover_icon_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-icon, {{WRAPPER}} .premium-ihover-icon-image' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_ihover_title_style_section',
            [
                'label'         => esc_html__( 'Title', 'premium-addons-for-elementor' ),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_ihover_thumbnail_back_title_switcher'  => 'yes'
                ]
            ]
        );
        
        $this->add_control('premium_ihover_thumbnail_title_color',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-title' => 'color: {{VALUE}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'premium_ihover_thumbnail_title_typhography',
                'selector'      => '{{WRAPPER}} .premium-ihover-title'
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'premium_ihover_thumbnail_title_text_shadow',
                'selector'      => '{{WRAPPER}} .premium-ihover-title'
            ]
        );

        $this->add_responsive_control('premium_ihover_thumbnail_title_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em','%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control('premium_ihover_thumbnail_title_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em','%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_ihover_thumbnail_divider_style_tab',
            [
                'label'         => esc_html__('Separator', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                   'premium_ihover_thumbnail_back_separator_switcher'  => 'yes'
                ]
            ]
        );
        
        $this->add_control('premium_ihover_thumbnail_divider_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-divider .premium-ihover-divider-line' => 'border-color: {{VALUE}};'
                ]
            ]
        );

        $this->add_control('premium_ihover_thumbnail_divider_type',
            [
                'label'         => esc_html__( 'Style', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'none'          => esc_html__( 'None', 'premium-addons-for-elementor' ),
                    'solid'         => esc_html__( 'Solid', 'premium-addons-for-elementor' ),
                    'double'        => esc_html__( 'Double', 'premium-addons-for-elementor' ),
                    'dotted'        => esc_html__( 'Dotted', 'premium-addons-for-elementor' ),
                ],
                'default'       =>'solid',
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-divider .premium-ihover-divider-line' => 'border-style: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_responsive_control('premium_ihover_thumbnail_divider_width',
            [
                'label'         => esc_html__('Width', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'description'   => esc_html__('Enter Separator width in (PX, EM, %), default is 100%', 'premium-addons-for-elementor'),
                'size_units'    => ['px', 'em' , '%'],
                'range'         => [
                    'px'    => [
                        'min'       => 0,
                        'max'       => 450
                    ],
                    'em'    => [
                        'min'       => 0,
                        'max'       => 30
                    ]
                ],
                'label_block'   => true,
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-divider .premium-ihover-divider-line' => 'border-width: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control('premium_ihover_thumbnail_divider_height',
            [
                'label'         => esc_html__('Height', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em'],
                'label_block'   => true,
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-divider' => 'height:{{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'premium_ihover_thumbnail_divider_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-ihover-divider'
            ]
        );

        $this->add_responsive_control('premium_ihover_thumbnail_divider_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em','%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-divider' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_section();

        $this->start_controls_section('premium_ihover_thumbnail_description_style_tab',
            [
                'label'         => esc_html__('Description', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                   'premium_ihover_thumbnail_back_description_switcher'  => 'yes'
                ]
            ]
        );

        $this->add_control('premium_ihover_thumbnail_description_color',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-description' => 'color: {{VALUE}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'premium_ihover_thumbnail_description_typhography',
                'selector'      => '{{WRAPPER}} .premium-ihover-description'
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'premium_ihover_thumbnail_description_text_shadow',
                'selector'      => '{{WRAPPER}} .premium-ihover-description'
            ]
        );

        $this->add_responsive_control('premium_ihover_thumbnail_description_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em','%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-description' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->add_responsive_control('premium_ihover_thumbnail_description_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em','%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-description' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $this->end_controls_section();
        
        $this->start_controls_section('premium_ihover_thumbnail_spinner_style_section',
            [
                'label'         => esc_html__( 'Spinner', 'premium-addons-for-elementor' ),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                   'premium_ihover_thumbnail_hover_effect'  => 'style20'
                ]
            ]
        );

        $this->add_control('premium_ihover_thumbnail_spinner_type',
            [
                'label'         => esc_html__( 'Style', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'none'          => esc_html__( 'None', 'premium-addons-for-elementor' ),
                    'solid'         => esc_html__( 'Solid', 'premium-addons-for-elementor' ),
                    'double'        => esc_html__( 'Double', 'premium-addons-for-elementor' ),
                    'dotted'        => esc_html__( 'Dotted', 'premium-addons-for-elementor' ),
                    'dashed'        => esc_html__( 'Dashed', 'premium-addons-for-elementor' ),
                    'groove'        => esc_html__( 'Groove', 'premium-addons-for-elementor' )
                ],
                'default'       =>'solid',
                'condition'     => [
                   'premium_ihover_thumbnail_hover_effect'  => 'style20'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-item.style20 .premium-ihover-spinner' => 'border-style: {{VALUE}};'
                ]
            ]
        );

        $this->add_control('premium_ihover_thumbnail_spinner_border_width',
            [
                'label'         => esc_html__('Border Width', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em', '%'],
                'condition'     => [
                   'premium_ihover_thumbnail_hover_effect'  => 'style20'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-item.style20 .premium-ihover-spinner' => 'border-width:{{SIZE}}{{UNIT}};'
                ]
            ]    
        );

        $this->add_control('premium_ihover_thumbnail_spinner_border_left_color',
            [
                'label'         => esc_html__('First Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'condition'     => [
                   'premium_ihover_thumbnail_hover_effect'  => 'style20'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-spinner' => 'border-top-color: {{VALUE}}; border-left-color: {{VALUE}};',
                ]
            ]
        );

        $this->add_control('premium_ihover_thumbnail_spinner_border_right_color',
            [
                'label'         => esc_html__('Second Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'condition'     => [
                   'premium_ihover_thumbnail_hover_effect'  => 'style20'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-spinner' => 'border-bottom-color: {{VALUE}};border-right-color: {{VALUE}};',
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('premium_ihover_container_style_section',
            [
                'label'         => esc_html__( 'Container', 'premium-addons-for-elementor' ),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );
        
        $this->add_control('premium_ihover_thumbnail_background_color',
            [
                'label'         => esc_html__('Overlay Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-info-back' => 'background: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_control('premium_ihover_container_background',
            [
                'label'         => esc_html__('Background', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-item-wrap' => 'background: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_ihover_container_border',
                'selector'      => '{{WRAPPER}} .premium-ihover-item-wrap',
                'condition'     => [
                   'premium_ihover_thumbnail_hover_effect!'  => 'style20'
                ]
            ]
        );
        
        $this->add_control('premium_ihover_container_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em' , '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-item-wrap, {{WRAPPER}} .premium-ihover-img, {{WRAPPER}} .premium-ihover-info-back, {{WRAPPER}} .premium-ihover-spinner' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'premium_ihover_container_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-ihover-img',
            ]
        );

        
        $this->add_responsive_control('premium_ihover_container_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em','%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-item-wrap' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control('premium_ihover_container_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em','%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-ihover-item-wrap' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_section();
        
    }
    
    /**
     * renders the HTML content of the widget
     * @return void
     */
    protected function render(){
        
        $settings = $this->get_settings_for_display();
        
        $this->add_render_attribute('premium_ihover_container', 'class', [ 'premium-ihover-container', $settings['premium_ihover_css_classes'] ] );
        
        $ihover_title_tag = $settings['premium_ihover_thumbnail_back_title_tag'];
        
        $this->add_inline_editing_attributes('premium_ihover_thumbnail_back_title', 'basic');
        
        $this->add_render_attribute('premium_ihover_thumbnail_back_title', 'class', 'premium-ihover-title');
        
        $this->add_inline_editing_attributes('premium_ihover_thumbnail_back_description', 'advanced');
        
        $this->add_render_attribute('premium_ihover_thumbnail_back_description', 'class', 'premium-ihover-description');
        
        ?>
        <div <?php echo $this->get_render_attribute_string('premium_ihover_container'); ?>>
            <div class="premium-ihover-list">
                <div class="premium-ihover-item-wrap <?php // echo $hide_class; ?>">
                <?php 

                $ihover_link_type = $settings['premium_ihover_thumbnail_link_type'];
                    if ( 'url' == $ihover_link_type ) {
                        $link_url = $settings['premium_ihover_thumbnail_url']['url'];
                    } elseif ( 'link' == $ihover_link_type ) {
                        $link_url = get_permalink( $settings['premium_ihover_thumbnail_existing_page' ]);
                    }
                    
                if ( 'yes' == $settings['premium_ihover_thumbnail_link_switcher'] ) : ?>
                <a class="premium-ihover-item-link" href="<?php echo esc_url( $link_url ); ?>" title="<?php echo $settings['premium_ihover_thumbnail_link_text']; ?>" <?php if( ! empty( $settings['premium_ihover_thumbnail_url']['is_external'] ) ) : ?> target="_blank" <?php endif; ?><?php if ( ! empty( $settings['premium_ihover_thumbnail_url']['nofollow'] ) ) : ?> rel="nofollow" <?php endif; ?>><?php endif; ?>
                    <div class="premium-ihover-item <?php echo $settings['premium_ihover_thumbnail_hover_effect']; ?>">
                        <?php if( 'style20' == $settings['premium_ihover_thumbnail_hover_effect'] ){ ?>
                            <div class="premium-ihover-spinner"></div>
                        <?php } ?>
                        <div class="premium-ihover-img-wrap">
                            <div class="premium-ihover-img-front">
                                <div class="premium-ihover-img-inner-wrap"></div>
                                <img class="premium-ihover-img" src="<?php echo $settings['premium_ihover_thumbnail_front_image']['url']; ?>">
                            </div>
                        </div>
                        <div class="premium-ihover-info-wrap">
                            <div class="premium-ihover-info-back">
                                <div class="premium-ihover-content">
                                    <div class="premium-ihover-content-wrap">
                                        <div class="premium-ihover-title-wrap">
                                            <?php if( 'yes' == $settings['premium_ihover_icon_fa_switcher'] && 'icon' == $settings['premium_ihover_icon_selection'] ) : ?>
                                            <i class="premium-ihover-icon <?php echo $settings['premium_ihover_icon_fa']; ?>"></i>
                                            <?php elseif( 'yes' == $settings['premium_ihover_icon_fa_switcher'] && 'image' == $settings['premium_ihover_icon_selection'] ) : ?>
                                            <img alt="Premium iHover Image" class="premium-ihover-icon-image" src="<?php echo $settings['premium_ihover_icon_image']['url']; ?>">
                                            <?php endif; ?>
                                        
                                        
                                        <?php if( 'yes' == $settings['premium_ihover_thumbnail_back_title_switcher'] ) : ?>
                                            <<?php echo $ihover_title_tag . ' ' . $this->get_render_attribute_string('premium_ihover_thumbnail_back_title'); ?>><?php echo esc_html( $settings['premium_ihover_thumbnail_back_title'] ); ?></<?php echo $ihover_title_tag; ?>>
                                        <?php endif; ?>
                                            
                                        </div>
                                        
                                        <?php if( 'yes' == $settings['premium_ihover_thumbnail_back_separator_switcher'] ) : ?>
                                            <div class="premium-ihover-divider"><span class="premium-ihover-divider-line"></span></div>
                                        <?php endif; ?>
                                            
                                        <?php if( 'yes' == $settings['premium_ihover_thumbnail_back_description_switcher'] ) : ?>
                                            
                                        <div <?php echo $this->get_render_attribute_string('premium_ihover_thumbnail_back_description'); ?>><?php echo $settings['premium_ihover_thumbnail_back_description']; ?></div>
                                        
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php if( 'yes' == $settings['premium_ihover_thumbnail_link_switcher'] ) : ?>
                    </a>
                <?php endif; ?>
                </div>
            </div>  
        </div>
    <?php 
    }
    
    protected function _content_template() {
        ?>
        <#
        
            view.addRenderAttribute('premium_ihover_container', 'class', [ 'premium-ihover-container', settings.premium_ihover_css_classes ] );
        
            var titleTag = settings.premium_ihover_thumbnail_back_title_tag,
            
                linkUrl = 'url' == settings.premium_ihover_thumbnail_link_type ? settings.premium_ihover_thumbnail_url.url : settings.premium_ihover_thumbnail_existing_page,
                
                hoverEffect = settings.premium_ihover_thumbnail_hover_effect;

            view.addInlineEditingAttributes('premium_ihover_thumbnail_back_title', 'basic');
            
            view.addRenderAttribute('premium_ihover_thumbnail_back_title', 'class', 'premium-ihover-title');
            
            view.addInlineEditingAttributes('premium_ihover_thumbnail_back_description', 'advanced');
            
            view.addRenderAttribute('premium_ihover_thumbnail_back_description', 'class', 'premium-ihover-description');
            
            view.addRenderAttribute('premium_ihover_item', 'class', [ 'premium-ihover-item', hoverEffect ] );

        #>
        
        <div {{{ view.getRenderAttributeString('premium_ihover_container') }}}>
            <div class="premium-ihover-list">
                <div class="premium-ihover-item-wrap">
                    <# if( 'yes' == settings.premium_ihover_thumbnail_link_switcher ) { #>
                        <a class="premium-ihover-item-link" href="{{ linkUrl }}" title="{{ settings.premium_ihover_thumbnail_link_text }}">
                    <# } #>
                    <div {{{ view.getRenderAttributeString('premium_ihover_item') }}}>
                        <# if( 'style20' == hoverEffect ){ #>
                            <div class="premium-ihover-spinner"></div>
                        <# } #>
                        <div class="premium-ihover-img-wrap">
                            <div class="premium-ihover-img-front">
                                <div class="premium-ihover-img-inner-wrap"></div>
                                <img class="premium-ihover-img" src="{{ settings.premium_ihover_thumbnail_front_image.url }}">
                            </div>
                        </div>
                        
                        <div class="premium-ihover-info-wrap">
                            <div class="premium-ihover-info-back">
                                <div class="premium-ihover-content">
                                    <div class="premium-ihover-content-wrap">
                                        <div class="premium-ihover-title-wrap">
                                            <# if( 'yes' == settings.premium_ihover_icon_fa_switcher && 'icon' == settings.premium_ihover_icon_selection ) { #>
                                                <i class="premium-ihover-icon {{ settings.premium_ihover_icon_fa }}"></i>
                                            <# } else if( 'yes' == settings.premium_ihover_icon_fa_switcher && 'image' == settings.premium_ihover_icon_selection ) { #>
                                                <img alt="Premium iHover Image" class="premium-ihover-icon-image" src="{{ settings.premium_ihover_icon_image.url }}">
                                            <# } #>
                                            
                                            <# if( 'yes' == settings.premium_ihover_thumbnail_back_title_switcher ) { #>
                                                <{{{titleTag}}} {{{ view.getRenderAttributeString('premium_ihover_thumbnail_back_title') }}} >{{{ settings.premium_ihover_thumbnail_back_title }}}</{{{titleTag}}}>
                                            <# } #>
                                            
                                        </div>
                                        <# if( 'yes' == settings.premium_ihover_thumbnail_back_separator_switcher ) { #>
                                            <div class="premium-ihover-divider"><span class="premium-ihover-divider-line"></span></div>
                                        <# } #>
                                        
                                        <# if( 'yes' == settings.premium_ihover_thumbnail_back_description_switcher ) { #>
                                            <div {{{ view.getRenderAttributeString('premium_ihover_thumbnail_back_description') }}}>
                                                {{{ settings.premium_ihover_thumbnail_back_description }}}
                                            </div>
                                        <# } #>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <# if( 'yes' == settings.premium_ihover_thumbnail_link_switcher ) { #>
                        </a>
                    <# } #>
                </div>
            </div>
        </div>
        
        <?php
    }
}