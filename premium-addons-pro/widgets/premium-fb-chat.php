<?php

/**
 * Class: Premium_Facebook_Chat_Widget
 * Name: Behance Feed
 * Slug: premium-behance-feed
 */

namespace Elementor;

if( !defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Premium_Fb_Chat extends Widget_Base {
    
    public function get_name(){
        return 'premium-addon-facebook-chat';
    }
    
    public function get_title() {
		return \PremiumAddons\Helper_Functions::get_prefix() . ' Messenger Chat';
	}
    
    public function is_reload_preview_required(){
        return true;
    }
    
    public function get_icon(){
        return 'pa-pro-messenger-chat';
    }
    
    public function get_script_depends(){
        return ['premium-pro-js'];
    }
    
    public function get_categories(){
        return [ 'premium-elements' ];
    }

    // Adding the controls fields for the Premium Facebook Chat
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls(){
        
        $this->start_controls_section('premium_fbchat_account_settings',
            [
                'label'         => esc_html__('Account Settings','premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_fbchat_app_id',
            [
                'label'         => esc_html__('App ID', 'premium-addons-for-elementor'),
                'description'   => 'Click <a href="https://developers.facebook.com/docs/apps/register" target="_blank">Here</a> to create and get App Id',
                'type'          => Controls_Manager::TEXT,
            ]
        );
        
        $this->add_control('premium_fbchat_page_id',
            [
                'label'         => esc_html__('Page ID', 'premium-addons-for-elementor'),
                'description'   => 'You need to put your site URL in whitelisted domains in your page messenger platform settings, Click <a href="https://www.facebook.com/help/community/question/?id=378910098941520" target="_blank">Here</a> to know how to get your page ID',
                'type'          => Controls_Manager::TEXT,
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_fbchat_message',
            [
                'label'         => esc_html__('Messages','premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_fbchat_login_msg',
            [
                'label'         => esc_html__('Logged in Message', 'premium-addons-for-elementor'),
                'dynamic'       => [ 'active' => true ],
                'description'   => esc_html__('The greeting text that will be displayed if the user is currently logged in to Facebook. Maximum 80 characters.','premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
            ]
        );
        
        $this->add_control('premium_fbchat_logout_msg',
            [
                'label'         => esc_html__('Logged out Message', 'premium-addons-for-elementor'),
                'dynamic'       => [ 'active' => true ],
                'description'   => esc_html__('The greeting text that will be displayed if the user is not currently logged in to Facebook. Maximum 80 characters.','premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
            ]
        );
        
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_fbchat_adv_section',
            [
                'label'         => esc_html__('Advanced Settings','premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_fbchat_ref',
            [
                'label'         => esc_html__('Ref', 'premium-addons-for-elementor'),
                'description'   => esc_html__('Optional. Custom string passed to your webhook in messaging_postbacks and messaging_referrals events.','premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_fbchat_mini',
            [
                'label'         => esc_html__('Minimized', 'premium-addons-for-elementor'),
                'description'   => esc_html__('Specifies whether the plugin should be minimized or shown. Defaults to false on desktop and true on mobile browsers.','premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
        );
        
        $this->add_control('premium_fbchat_hide_mobile',
            [
                'label'         => esc_html__('Hide on Mobiles', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('This will hide the messenger chat on mobile phon','premium-addons-for-elementor'),
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_fbchat_style',
            [
                'label'         => esc_html__('Icon','premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE
            ]
        );
        
        $this->add_control('premium_fbchat_theme_color',
            [
                'label'         => esc_html__('Theme Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
            ]
        );
        
        $this->add_control('premium_fbchat_position_select', 
            [
                'label'         => esc_html__('Position', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'topleft'       => esc_html__('Top Left'),
                    'topright'      => esc_html__('Top Right'),
                    'bottomleft'    => esc_html__('Bottom Left'),
                    'bottomright'   => esc_html__('Bottom Right'),
                    'custom'        =>      esc_html__('Custom'),
                ],
                'default'       => 'bottomright',
                'label_block'   => true,
            ]
        );
        
        $this->add_control('premium_fbchat_hor_position',
            [
                'label'         => esc_html__('Horizontal Position', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'default'       =>  [
                    'size'  => 0
                    ],
                'condition'     => [
                    'premium_fbchat_position_select'    => 'custom'
                ]
            ]
        );
                
        $this->add_control('premium_fbchat_ver_position',
            [
                'label'         => esc_html__('Vertical Position', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'default'       =>  [
                    'size'  => 0
                ],
                'condition'     => [
                    'premium_fbchat_position_select'    => 'custom'
                ]
            ]
        );
        
        $this->end_controls_section();
        
        
    }
    
    /**
	 * renders the HTML content of the widget
	 * @return void
	 */
    protected function render(){
        
        $settings   = $this->get_settings_for_display();
        
        $app_id     = $settings['premium_fbchat_app_id'];
        
        $page_id    = $settings['premium_fbchat_page_id'];
        
        $login_msg  = $settings['premium_fbchat_login_msg'];
        
        $logout_msg = $settings['premium_fbchat_logout_msg'];
        
        $theme_color= $settings['premium_fbchat_theme_color'];
        
        $position   = $settings['premium_fbchat_position_select'];
        
        $hide_mobile= $settings['premium_fbchat_hide_mobile'] == 'yes' ? true : false;
        
        $ref        = $settings['premium_fbchat_ref'];
        
        if( 'yes' == $settings['premium_fbchat_mini'] ){
            $minimized = true;
        } else {
            $minimized = false;
        }
        
        $pa_chat_settings = [
            'appId'         => $app_id,
            'hideMobile'    => $hide_mobile,
        ];
    
        ?>
    
    <div id="premium-fbchat-container" class="premium-fbchat-container" data-settings='<?php echo wp_json_encode( $pa_chat_settings ); ?>'>
      <div class="fb-customerchat"
      page_id="<?php echo esc_attr( $page_id ); ?>"
      theme_color="<?php echo esc_attr( $theme_color ); ?>" 
      logged_in_greeting="<?php echo esc_attr( $login_msg ); ?>" 
      logged_out_greeting="<?php echo esc_attr( $logout_msg ); ?>" 
      ref="<?php echo esc_attr( $ref ); ?>"
      minimized="<?php echo esc_attr( $minimized ); ?>">
      </div>
    </div>
    <style>
        <?php if( 'bottomleft' == $position ) : ?>
        .fb_dialog,
        .fb-customerchat * > iframe {
            left: 18pt !important;
            right: auto;
        }
        <?php elseif( 'bottomright' == $position ) : ?>
        .fb_dialog,
        .fb-customerchat * > iframe {
            right: 18pt !important;
            left: auto;
        }
        <?php elseif('topleft' == $position ) : ?>
        .fb_dialog,
        .fb-customerchat * > iframe {
            left: 18pt !important;
            right: auto;
            top:18px !important;
            bottom: auto;
        }
        <?php elseif( 'topright' == $position ) : ?>
        .fb_dialog,
        .fb-customerchat * > iframe {
            right: 18pt !important;
            left: auto;
            top:18px !important;
            bottom: auto;
        }
        <?php elseif( 'custom' == $position ) : ?>
        .fb_dialog,
        .fb-customerchat * > iframe {
            left: <?php echo $settings['premium_fbchat_hor_position']['size'] . '%'; ?> !important;
            right: auto;
            top: <?php echo $settings['premium_fbchat_ver_position']['size'] . '%'; ?> !important;
            bottom: auto;
            -webkit-transform: translate(-50%,-50%);
            transform: translate(-50%,-50%);
        }
        <?php endif; ?>
    </style>
    
<?php 
    }
}