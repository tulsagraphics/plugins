<?php

/**
 * Class: Premium_Whatsapp_Chat_Widget
 * Name: Whatsapp Chat
 * Slug: premium-whatsapp-chat
 */

namespace Elementor;

use Elementor\Core\Responsive\Responsive;

if( !defined('ABSPATH') ) exit;

class Premium_Whatsapp_Chat extends Widget_Base {
    
    public function get_name(){
        return 'premium-whatsapp-chat';
    }
    
    public function get_title(){
        return \PremiumAddons\Helper_Functions::get_prefix() . ' Whatsapp Chat';
    }
    
    public function get_icon() {
        return 'pa-pro-whatsapp';
    }
    
    public function get_categories() {
        return ['premium-elements'];
    }
    
    public function get_script_depends() {
        return [
            'tooltipster-bundle-js',
            'premium-pro-js'
        ];
    }
    
    public function check_rtl(){
        return is_rtl();
    }
    
    // Adding the controls fields for the Whatsapp Chat
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {
        
        $this->start_controls_section('chat',
            [
                'label'         => esc_html__('Chat','premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('chat_type', 
            [
                'label'         => esc_html__('Chat', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'private'       => esc_html__('Private', 'premium-addons-for-elementor'),
                    'group'         => esc_html__('Group', 'premium-addons-for-elementor'),
                ],
                'default'       => 'private',
                'label_block'   => true,
            ]
        );
        
        $this->add_control('number',
            [
                'label'         => esc_html__('Phone Number', 'premium-addons-for-elementor'),
                'description'   => 'Example: +1123456789',
                'dynamic'       => [ 'active' => true ],
                'condition'     => [
                    'chat_type'     => 'private'
                ],
                'type'          => Controls_Manager::TEXT,
            ]
        );
        
        $this->add_control('group_id',
            [
                'label'         => esc_html__('Group Id', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'description'   => 'click <a href="https://www.youtube.com/watch?time_continue=13&v=Vx53spbt_qk" target="_blank"> here</a> to know how to get the group id',
                'dynamic'       => [ 'active' => true ],
                'default'       => '9EHLsEsOeJk6AVtE8AvXiA',
                'condition'     => [
                    'chat_type'     => 'group'
                ],
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('settings',
            [
                'label'         => esc_html__('Button','premium-addons-for-elementor'),
            ]
        );
         
        $this->add_control('button_float',
            [
                'label'         => esc_html__('Float', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER
            ]
        );
        
        $this->add_responsive_control('horizontal_position',
            [
                'label'         => esc_html__('Horizontal Offset', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em', '%'],
                'range'         => [
                    'px' => [
                        'min'   => 0,
                        'max'   => 300,
                    ]
                ],
                'condition'     => [
                    'button_float'  => 'yes',
                    'position'      => 'right'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link' => 'right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        
        $this->add_responsive_control('horizontal_position_left',
            [
                'label'         => esc_html__('Horizontal Offset', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em', '%'],
                'range'         => [
                    'px' => [
                        'min'   => 0,
                        'max'   => 300,
                    ]
                ],
                'condition'     => [
                    'button_float'  => 'yes',
                    'position'      => 'left'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link' => 'left: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        
        $this->add_responsive_control('vertical_position',
            [
                'label'         => esc_html__('Vertical Offset', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em', '%'],
                'range'         => [
                    'px' => [
                        'min'   => 0,
                        'max'   => 300,
                    ]
                ],
                'condition'     => [
                    'button_float'  => 'yes',
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link' => 'bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
         
        $this->add_control('button_text',
            [
                'label'         => esc_html__('Text', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'dynamic'       => [ 'active' => true ],
                'default'       => esc_html__('Contact us','premium-addons-for-elementor'),
                'label_block'   => true,
            ]
        );
        
        $this->add_control('icon_switcher',
            [
                'label'         => esc_html__('Icon', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('Enable or disable button icon','premium-addons-for-elementor'),
                'default'       => 'yes',
                'separator'     => 'before',
            ]
        );

        $this->add_control('icon_selection',
            [
                'label'         => esc_html__('Icon', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::ICON,
                'default'       => 'fa fa-whatsapp',
                'condition'     => [
                    'icon_switcher' => 'yes',
                ],
                'label_block'   => true,
            ]
        );
        
        $this->add_control('icon_position', 
            [
                'label'         => esc_html__('Icon Position', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'row-reverse'   => esc_html__('Before', 'premium-addons-for-elementor'),
                    'row'           => esc_html__('After', 'premium-addons-for-elementor'),
                ],
                'condition'     => [
                    'icon_switcher'     => 'yes',
                    'icon_selection!'    => '',
                    'button_text!'      => ''
                ],
                'default'       => 'row',
                'label_block'   => true,
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link'    => '-webkit-flex-direction: {{VALUE}}; flex-direction: {{VALUE}};'
                ]
            ]
        );
        
        $this->add_responsive_control('icon_size',
            [
                'label'         => esc_html__('Icon Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'condition'     => [
                    'icon_switcher'     => 'yes',
                    'icon_selection!'    => '',
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link .premium-whatsapp-icon' => 'font-size: {{SIZE}}px',
                ]
            ]
        );
        
        $this->add_responsive_control('icon_spacing',
            [
                'label'         => esc_html__('Icon Spacing', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'condition'     => [
                    'icon_switcher'     => 'yes',
                    'icon_position'     => 'row',
                    'icon_selection!'    => '',
                    'button_text!'      => ''
                ],
                'default'       => [
                    'size'  => 15
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link i' => 'margin-left: {{SIZE}}px',
                ],
                'separator'     => 'after',
            ]
        );
        
        $this->add_responsive_control('icon_before_spacing',
            [
                'label'         => esc_html__('Icon Spacing', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'condition'     => [
                    'icon_switcher'     => 'yes',
                    'icon_position'     => 'row-reverse',
                    'icon_selection!'   => '',
                    'button_text!'      => ''
                ],
                'default'       => [
                    'size'  => 15
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link i' => 'margin-right: {{SIZE}}px',
                ],
                'separator'     => 'after',
            ]
        );
        
        $this->add_control('button_size', 
            [
                'label'         => esc_html__('Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'lg',
                'options'       => [
                    'sm'    => esc_html__('Small'),
                    'md'    => esc_html__('Medium'),
                    'lg'    => esc_html__('Large'),
                    'block' => esc_html__('Block'),
                ],
                'label_block'   => true,
                'separator'     => 'before'
            ]
        );
        
        $this->add_responsive_control('button_alignment',
            [
                'label'         => esc_html__( 'Button Alignment', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'    => [
                        'title' => __( 'Left', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-right',
                    ],
                ],
                'toggle'        => false,
                'condition'     => [
                    'button_float!' => 'yes',
                    'button_size!'  => 'block'
                ],
                'default'       => 'center',
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link-wrap' => 'text-align: {{VALUE}}',
                ],
            ]
        );
        
        $this->add_responsive_control('text_alignment',
            [
                'label'         => esc_html__( 'Text Alignment', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'    => [
                        'title' => __( 'Left', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-right',
                    ],
                ],
                'toggle'        => false,
                'condition'     => [
                    'button_float!' => 'yes',
                    'icon_position' => 'row',
                    'button_size'  => 'block'
                ],
                'default'       => 'center',
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link-wrap .premium-whatsapp-link' => 'justify-content: {{VALUE}}',
                ],
            ]
        );

        $this->add_responsive_control('text_alignment_after',
            [
                'label'         => esc_html__( 'Text Alignment', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'right'    => [
                        'title' => __( 'Left', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-center',
                    ],
                    'left' => [
                        'title' => __( 'Right', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-right',
                    ],
                ],
                'toggle'        => false,
                'condition'     => [
                    'button_float!' => 'yes',
                    'icon_position' => 'row-reverse',
                    'button_size'  => 'block'
                ],
                'default'       => 'center',
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link-wrap .premium-whatsapp-link' => 'justify-content: {{VALUE}}',
                ],
            ]
        );
        
        $this->add_control('position', 
            [
                'label'         => esc_html__('Button Position', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'right' => esc_html__('Right', 'premium-addons-for-elementor'),
                    'left'  => esc_html__('Left', 'premium-addons-for-elementor'),
                ],
                'condition'     => [
                    'button_float'  => 'yes'
                ],
                'toggle'        => false,
                'default'       => 'right',
                'label_block'   => true,
            ]
        );
        
        $this->add_control('button_hover_animation',
	        [
				'label'         => __( 'Hover Animation', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::HOVER_ANIMATION,
			]
		);
    
        $this->add_control('link_new_tab',
            [
                'label'         => esc_html__('Open Link in New Tab', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('advanced',
            [
                'label'         => esc_html__('Advanced Settings','premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('hide_tabs',
            [
                'label'         => esc_html__('Hide on Tabs', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('This will hide the chat button on tablets','premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('hide_mobiles',
            [
                'label'         => esc_html__('Hide on Mobiles', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('This will hide the chat button on mobile phones','premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('tooltips',
            [
                'label'         => esc_html__('Tooltips', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('This will show a tooltip next to the button when hovered','premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('tooltips_msg',
            [
                'label'         => esc_html__('Tooltip Message', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'dynamic'       => [ 'active' => true ],
                'default'       => esc_html__('Message us', 'premium-addons-for-elementor'),
                'condition'     => [
                    'tooltips'      => 'yes'
                ]
            ]
        );
       
        $this->add_control('tooltips_anim', 
            [
                'label'         => esc_html__('Animation', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'fade'  => esc_html__('Fade', 'premium-addons-for-elementor'),
                    'grow'  => esc_html__('Grow', 'premium-addons-for-elementor'),
                    'swing' => esc_html__('Swing', 'premium-addons-for-elementor'),
                    'slide' => esc_html__('Slide', 'premium-addons-for-elementor'),
                    'fall'  => esc_html__('Fall', 'premium-addons-for-elementor'),
                ],
                'condition'     => [
                    'tooltips'      => 'yes'
                ],
                'default'       => 'fade',
                'label_block'   => true,
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_button_style_section',
            [
                'label'         => esc_html__('Button', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );
        
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'button_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'condition'     => [
                    'button_text!'  => ''
                ],
                'selector'      => '{{WRAPPER}} .premium-whatsapp-link span',
            ]
        );
        
        $this->start_controls_tabs('button_style_tabs');
        
        $this->start_controls_tab('button_style_normal',
            [
                'label'         => esc_html__('Normal', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('text_color_normal',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'condition'     => [
                    'button_text!'  => ''
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link span'   => 'color: {{VALUE}};',
                ]
            ]
        );
        
        $this->add_control('button_icon_color_normal',
            [
                'label'         => esc_html__('Icon Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link i'   => 'color: {{VALUE}};',
                ],
                'condition'     => [
                    'icon_switcher'     => 'yes',
                    'icon_selection!'   => ''
                ]
            ]
        );
        
        $this->add_control('button_background_normal',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_4,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link'  => 'background-color: {{VALUE}};',
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'button_border_normal',
                'selector'      => '{{WRAPPER}} .premium-whatsapp-link',
            ]
        );
        
        $this->add_control('button_border_radius_normal',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'label'         => esc_html__('Icon Shadow','premium-addons-for-elementor'),
                'name'          => 'button_icon_shadow_normal',
                'selector'      => '{{WRAPPER}} .premium-whatsapp-link i',
                'condition'         => [
                    'icon_switcher' => 'yes',
                    'icon_switcher!'=> ''
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'label'         => esc_html__('Text Shadow','premium-addons-for-elementor'),
                'name'          => 'button_text_shadow_normal',
                'condition'     => [
                    'button_text!'  => ''
                ],
                'selector'      => '{{WRAPPER}} .premium-whatsapp-link span',
            ]
        );
    
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'label'         => esc_html__('Button Shadow','premium-addons-for-elementor'),
                'name'          => 'button_box_shadow_normal',
                'selector'      => '{{WRAPPER}} .premium-whatsapp-link',
            ]
        );
        
        $this->add_responsive_control('button_margin_normal',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control('button_padding_normal',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->start_controls_tab('button_style_hover',
            [
                'label'         => esc_html__('Hover', 'premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('tooltips_background', 
            [
               'label'          => esc_html__('Tooltips Background Color','premuium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '.tooltipster-sidetip div.tooltipster-box-{{ID}} .tooltipster-content'  => 'background-color:{{VALUE}};'
                ],
                'condition'     => [
                    'tooltips'   => 'yes'
                ]
            ]
        );
        
        $this->add_control('text_color_hover',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link:hover span'   => 'color: {{VALUE}};',
                ],
                'condition'     => [
                    'button_text!'   => ''
                ]
            ]
        );
        
        $this->add_control('icon_color_hover',
            [
                'label'             => esc_html__('Icon Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'         => [
                    '{{WRAPPER}} .premium-whatsapp-link:hover i'   => 'color: {{VALUE}};',
                ],
                'condition'         => [
                    'icon_selection!'   => '',
                    'icon_switcher'    => 'yes',
                ]
            ]
        );
        
        $this->add_control('button_background_hover',
            [
                'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_4
                ],
                'selectors'          => [
                    '{{WRAPPER}} .premium-whatsapp-link:hover' => 'background-color: {{VALUE}};',
                ],
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'button_border_hover',
                    'selector'      => '{{WRAPPER}} .premium-whatsapp-link:hover',
                ]
                );
        
        $this->add_control('button_border_radius_hover',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%' ,'em'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link:hover' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'label'         => esc_html__('Icon Shadow','premium-addons-for-elementor'),
                'name'          => 'button_icon_shadow_hover',
                'selector'      => '{{WRAPPER}} .premium-whatsapp-link:hover i',
                'condition'     => [
                        'icon_switcher'     => 'yes',
                        'icon_selection!'   => '',
                    ]
                ]
            );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Text Shadow','premium-addons-for-elementor'),
                    'name'          => 'button_text_shadow_hover',
                    'selector'      => '{{WRAPPER}} .premium-button:hover span',
                    'condition'         => [
                       'button_text!'   => ''
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'label'         => esc_html__('Button Shadow','premium-addons-for-elementor'),
                'name'          => 'button_shadow_hover',
                'selector'      => '{{WRAPPER}} .premium-whatsapp-link:hover',
            ]
        );
        
        $this->add_responsive_control('button_margin_hover',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link:hover' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control('button_padding_hover',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-whatsapp-link:hover' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_tab();
        
        $this->end_controls_tabs();

        $this->end_controls_section();
        
    }
    
    /**
     * renders the HTML content of the widget
     * @return void
     */
    protected function render() {
        
        $settings       = $this->get_settings_for_display();
        
        $breakpoints    = Responsive::get_breakpoints();
        
        $pa_whats_chat_settings = array( 
            'tooltips'  => $settings['tooltips'],
            'anim'      => $settings['tooltips_anim'],
            'hideMobile'=> $settings['hide_mobiles'] == 'yes' ? true : false,
            'hideTab'   => $settings['hide_tabs'] == 'yes' ? true : false,
            'mob'       => $breakpoints['md'],
            'tab'       => $breakpoints['lg'],
            'id'        => $this->get_id()
        );
        
        $target = 'yes' == $settings['link_new_tab'] ? "_blank" : "_self";
        
        $id = ( 'private' == $settings['chat_type'] ) ? $settings['number'] : $settings['group_id'];
        
        $is_mobile = ( wp_is_mobile() ) ? 'api' : 'web';
        
        $browser = $_SERVER['HTTP_USER_AGENT'];
        
        $is_firefox = ( false !==  strpos( $browser, 'Firefox' ) ) ? 'web' : 'chat';
        
        $prefix = ( 'private' == $settings['chat_type'] ) ? $is_mobile : $is_firefox;
        
        $suffix = ( 'private' == $settings['chat_type'] ) ? 'send?phone=' : '';
        
        $href = sprintf('https://%s.whatsapp.com/%s%s', $prefix, $suffix, $id );
        
        $pos = 'yes' == $settings['button_float'] ? 'premium-button-float' : '';
        
        $button_size = 'premium-button-' . $settings['button_size'];
        
        $this->add_render_attribute( 'button_link', 'class', 'premium-whatsapp-link' );
        
        $this->add_render_attribute( 'button_link', 'class', $button_size );
        
        $this->add_render_attribute( 'button_link', 'class', $pos );
        
        $this->add_render_attribute( 'button_link', 'data-tooltip-content', '#tooltip_content' );
        
        $this->add_render_attribute( 'button_link', 'class', $settings['position'] );
        
        $this->add_render_attribute( 'button_link', 'href', esc_attr( $href ) );
        
        $this->add_render_attribute( 'button_link', 'target', $target );
        
        $this->add_render_attribute( 'button_link', 'class', 'elementor-animation-' . $settings['button_hover_animation'] );
        
        
        ?>

    <div id="premium-whatsapp-container" class="premium-whatsapp-container" data-settings='<?php echo wp_json_encode( $pa_whats_chat_settings ); ?>'>
    <div class="premium-whatsapp-link-wrap">
        <a <?php echo $this->get_render_attribute_string( 'button_link' ); ?>>
            <?php if( ! empty ( $settings['button_text'] ) ) : ?>
                <span class="premium-whatsapp-text"><?php echo esc_html( $settings['button_text'] ); ?></span>
            <?php endif; ?>
            <?php if( ! empty ( $settings['icon_selection'] ) && 'yes' == $settings['icon_switcher'] ) : ?>
                <i class="premium-whatsapp-icon <?php echo esc_attr( $settings['icon_selection'] ); ?>"></i>
            <?php endif; ?>
            <?php if( 'yes' == $settings['tooltips'] ) : ?>
                <div id="tooltip_content">
                    <span><?php echo esc_html( $settings['tooltips_msg'] ); ?></span>
                </div>
            <?php endif; ?>
        </a>
        
    </div>
        
    </div>
    
    <?php }
    
}