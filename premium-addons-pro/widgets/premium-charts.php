<?php

/**
 * Class: Premium_Charts_Widget
 * Name: Charts
 * Slug: premium-chart
 */

namespace Elementor;

if(!defined('ABSPATH')) exit;

class Premium_Charts extends Widget_Base {
    
    public function get_name() {
        return 'premium-chart';
    }
    
    public function get_title() {
		return \PremiumAddons\Helper_Functions::get_prefix() . ' Chart';
	}
    
    public function get_icon() {
        return 'pa-pro-charts';
    }
    
    public function get_categories() {
        return ['premium-elements'];
    }
    
    public function get_script_depends() {
        return [
            'chart-js',
            'waypoints',
            'premium-pro-js',
        ];
    }
    
    public function is_reload_preview_required() {
        return true;
    }
    
    // Adding the controls fields for the Premium Charts
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {
        
        $this->start_controls_section('general_settings',
            [
                'label'         => esc_html__('Premium Charts','premium-addons-for-elementor'),
            ]
        );

        $this->add_control('type', 
            [
                'label'         => esc_html__('Layout', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'line'          => esc_html__('Line','premium-addons-for-elementor'),
                    'bar'           => esc_html__('Bar','premium-addons-for-elementor'),
                    'horizontalBar' => esc_html__('Horizontal Bar','premium-addons-for-elementor'),
                    'pie'           => esc_html__('Pie','premium-addons-for-elementor'),
                    'radar'         => esc_html__('Radar','premium-addons-for-elementor'),
                    'doughnut'      => esc_html__('Doughnut','premium-addons-for-elementor'),
                    'polarArea'     => esc_html__('Polar Area','premium-addons-for-elementor'),

                    ],
                'default'       => 'bar',
                'label_block'   => true,
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('x_axis',
            [
                'label'         => esc_html__('X-Axis','premium-addons-for-elementor'),
            ]
        );

        $this->add_control('x_axis_label_switch',
            [
                'label'         => esc_html__('Show Axis Label', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'return_value'  => 'true',
                'description'   => esc_html__('Show or Hide X-Axis Label','premium-addons-for-elementor'),
            ]
        );

        $this->add_control('x_axis_label',
                [
                    'label'     => esc_html__('Label', 'premium-addons-for-elementor'),
                    'type'      => Controls_Manager::TEXT,
                    'default'   => 'X-Axis',
                    'label_block'=> true,
                    'condition' => [
                        'x_axis_label_switch'    => 'true',
                    ]
                ]
            );

        $this->add_control('x_axis_labels',
            [
                'label'         => esc_html__('Data Labels', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'default'       => 'Jan,Feb,Mar,Apr,June',
                'description'   => esc_html__('Enter labels for X-Axis separated with \' , \' ','premium-addons-for-elementor'),
                'label_block'   => true,
            ]
        );
        
        $this->add_control('x_axis_grid',
            [
                'label'         => esc_html__('Show Grid Lines', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'return_value'  => 'true',
                'default'       => 'true',
                'description'   => esc_html__('Show or Hide X-Axis Grid Lines','premium-addons-for-elementor'),
            ]
        );

        $this->add_control('x_axis_begin',
            [
                'label'         => esc_html__('Begin at Zero', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'return_value'  => 'true',
                'description'   => esc_html__('Start X-Axis Labels at zero','premium-addons-for-elementor'),
            ]
        );

        $this->add_control('x_axis_label_rotation',
            [
                'label'			=> esc_html__( 'Labels\' Rotation ', 'premium-addons-for-elementor' ),
                'type'			=> Controls_Manager::NUMBER,
                'min'           => 0,
                'max'           => 360,
                'default'       => 0
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('y_axis',
            [
                'label'         => esc_html__('Y-Axis','premium-addons-for-elementor'),
            ]
        );

        $this->add_control('y_axis_label_switch',
            [
                'label'         => esc_html__('Show Axis Label', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'return_value'  => 'true',
                'description'   => esc_html__('Show or Hide Y-Axis Label','premium-addons-for-elementor'),
            ]
        );

        $this->add_control('y_axis_label',
            [
                'label'         => esc_html__('Label', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'default'       => 'Y-Axis',
                'label_block'   => true,
                'condition'     => [
                    'y_axis_label_switch'    => 'true',
                ]
            ]
        );

        $data_repeater = new REPEATER();

        $data_repeater->add_control('y_axis_column_title',
            [
                'label'         => esc_html__( 'Title', 'premium-addons-for-elementor' ),
               'default'        => esc_html__('Dataset','premium-addons-for-elementor'),
               'type'           => Controls_Manager::TEXT,
            ]
        );

        $data_repeater->add_control('y_axis_column_data',
            [
                'label'         => esc_html__( 'Data', 'premium-addons-for-elementor' ),
                'description'   => esc_html__('Enter Data Numbers for Y-Axis separated with \' , \' ','premium-addons-for-elementor'),
                'default'       => '1,5,2,3,7',
                'type'          => Controls_Manager::TEXT,

            ]
        );
        
        $data_repeater->add_control('y_axis_urls',
            [
                'label'         => esc_html__('URLs', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'description'   => esc_html__('Enter URLs for each Dataset separated with \' , \' ','premium-addons-for-elementor'),
                'label_block'   => true,
            ]
        );

        $data_repeater->add_control('y_axis_column_color',
            [
                'label'         => esc_html__( 'Fill Color', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::COLOR,
                'default'       => '#6ec1e4',
            ]
        );

        $data_repeater->add_control('y_axis_circle_color',
            [
               'label'          => esc_html__( 'Fill Colors', 'premium-addons-for-elementor' ),
               'description'    => esc_html__('Enter Colors separated with \' , \', this will work only for pie and doughnut charts ','premium-addons-for-elementor'),
               'type'           => Controls_Manager::TEXT,
            ]
        );

        $data_repeater->add_control('y_axis_column_border_color',
            [
                'label'         => esc_html__( 'Border Color', 'premium-addons-for-elementor' ),
               'type'           => Controls_Manager::COLOR,
            ]
        );

        $this->add_control('y_axis_data',
           [
               'label'          => __( 'Data', 'premium-addons-for-elementor' ),
               'type'           => Controls_Manager::REPEATER,
               'default'        => [
                    [
                       'y_axis_column_data'   => '1,5,2,3,7',
                    ],
               ],
               'fields'         => array_values( $data_repeater->get_controls() ),
           ]
        );
        
        $this->add_control('data_type',
            [
                'label'         => esc_html__('Data Type', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'linear'            => esc_html__('Linear', 'premium-addons-for-elementor'),
                    'logarithmic'       => esc_html__('Logarithmic', 'premium-addons-for-elementor'),
                ],
                'default'       => 'linear',
                'condition'     => [
                    'type!'             => 'horizontalBar'
                    ]
                ]
            );

        $this->add_control('y_axis_grid',
            [
                'label'         => esc_html__('Show Grid Lines', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'return_value'  => 'true',
                'default'       => 'true',
                'description'   => esc_html__('Show or Hide Y-Axis Grid Lines','premium-addons-for-elementor'),
            ]
        );

        $this->add_control('y_axis_begin',
            [
                'label'         => esc_html__('Begin at Zero', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'true',
                'return_value'  => 'true',
                'description'   => esc_html__('Start Y-Axis Data at zero','premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('y_axis_urls_target',
            [
                'label'         => esc_html__('Open Links in new tab', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'return_value'  => 'true',
                'default'       => 'true',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('title_content',
            [
                'label'         => esc_html__('Title','premium-addons-for-elementor'),
            ]
        );

        $this->add_control('title_switcher',
            [
                'label'         => esc_html__('Title', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'return_value'  => 'true',
            ]
        );

        $this->add_control('title',
            [
                'label'         => esc_html__('Title', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'description'   => esc_html__('Enter a Title for the Chart','premium-addons-for-elementor'),
                'label_block'   => true,
                'dynamic'       => [ 'active' => true ],
                'condition'     => [
                    'title_switcher'  => 'true'
                ]
            ]
        );

        $this->add_control('title_tag',
            [
                'label'         => esc_html__('HTML Tag', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'h3',
                'options'       => [
                    'h1' => 'H1',
                    'h2' => 'H2',
                    'h3' => 'H3',
                    'h4' => 'H4',
                    'h5' => 'H5',
                    'h6' => 'H6' 
                    ],
                'label_block'   =>  true,
                'condition'     => [
                    'title_switcher'  => 'true'
                ]
            ]
        );

        $this->add_control('title_position',
        [
            'label'             => esc_html__('Position', 'premium-addons-for-elementor'),
            'type'              => Controls_Manager::SELECT,
            'options'           => [
                'top'       => esc_html__('Top', 'premium-addons-for-elementor'),
                'bottom'    => esc_html__('Bottom', 'premium-addons-for-elementor'),
            ],
            'default'           => 'top',
            'condition'         => [
                'title_switcher'  => 'true'
                ]
            ]
        );

        $this->add_responsive_control('title_align',
            [
                'label'         => __( 'Alignment', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'    => [
                        'title' => __( 'Left', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'premium-addons-for-elementor' ),
                        'icon'  => 'fa fa-align-right',
                    ],
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-chart-title' => 'text-align: {{VALUE}}',
                ],
                'default'       => 'center',
                'condition'     => [
                    'title_switcher'  => 'true'
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('advanced',
            [
                'label'         => esc_html__('Advanced Settings','premium-addons-for-elementor'),
            ]
        );

        $this->add_control('y_axis_max',
            [
                'label'         => esc_html__( 'Maximum Value', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::NUMBER,
                'title'         => esc_html__('Set Y-axis maximum value, this will be overriden if data has a larger value', 'premium-addons-for-elementor'),
                'min'           => 0,
                'default'       => 1,
                'condition'     => [
                    'type!'   => [ 'pie', 'doughnut', 'polarArea', 'radar' ]
                ]
            ]
        );

        $this->add_control('legend_display',
            [
                'label'         => esc_html__('Show Legend', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'return_value'  => 'true',
                'description'   => esc_html__('Show or Hide chart legend','premium-addons-for-elementor'),
            ]
        );

        $this->add_control('legend_position',
            [
                'label'         => esc_html__('Legend Position', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'top'           => esc_html__('Top', 'premium-addons-for-elementor'),
                    'right'         => esc_html__('Right', 'premium-addons-for-elementor'),
                    'bottom'        => esc_html__('Bottom', 'premium-addons-for-elementor'),
                    'left'          => esc_html__('Left', 'premium-addons-for-elementor'),
                ],
                'default'       => 'top',
                'condition'     => [
                    'legend_display'  => 'true'
                ]
            ]
        );

        $this->add_control('legend_reverse',
            [
                'label'         => esc_html__('Reverse', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('Enable or Disable legend data reverse','premium-addons-for-elementor'),
                'return_value'  => 'true',
                'condition'     => [
                    'legend_display'  => 'true'
                ]
            ]
        );

        $this->add_control('tool_tips',
            [
                'label'         => esc_html__('Show Values on Hover', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => 'Show',
                'label_off'     => 'Hide',
                'return_value'  => 'true',
            ]
        );
        
        $this->add_control('tool_tips_percent',
            [
                'label'         => esc_html__('Convert Values to percent', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'return_value'  => 'true',
                'condition'     => [
                    'tool_tips'  => 'true'
                ]
            ]
        );

        $this->add_control('tool_tips_mode',
            [
                'label'         => esc_html__('Mode', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'point'         => esc_html__('Point', 'premium-addons-for-elementor'),
                    'nearest'       => esc_html__('Nearest', 'premium-addons-for-elementor'),
                    'dataset'       => esc_html__('Dataset', 'premium-addons-for-elementor'),
                    'x'             => esc_html__('X', 'premium-addons-for-elementor'),
                    'y'             => esc_html__('Y', 'premium-addons-for-elementor'),
                ],
                'default'       => 'nearest',
                'condition'     => [
                    'tool_tips'  => 'true'
                ]
            ]
        );

        $this->add_control('start_animation',
            [
                'label'         => esc_html__('Animation', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'linear'            => esc_html__('Linear','premium-addons-for-elementor'),
                    'easeInQuad'        => esc_html__('Ease in Quad','premium-addons-for-elementor'),
                    'easeOutQuad'       => esc_html__('Ease out Quad','premium-addons-for-elementor'),
                    'easeInOutQuad'     => esc_html__('Ease in out Quad','premium-addons-for-elementor'),
                    'easeInCubic'       => esc_html__('Ease in Cubic','premium-addons-for-elementor'),
                    'easeOutCubic'      => esc_html__('Ease out Cubic','premium-addons-for-elementor'),
                    'easeInOutCubic'    => esc_html__('Ease in out Cubic','premium-addons-for-elementor'),
                    'easeInQuart'       => esc_html__('Ease in Quart','premium-addons-for-elementor'),
                    'easeOutQuart'      => esc_html__('Ease out Quart','premium-addons-for-elementor'),
                    'easeInOutQuart'    => esc_html__('Ease in out Quart','premium-addons-for-elementor'),
                    'easeInQuint'       => esc_html__('Ease in Quint','premium-addons-for-elementor'),
                    'easeOutQuint'      => esc_html__('Ease out Quint','premium-addons-for-elementor'),
                    'easeInOutQuint'    => esc_html__('Ease in out Quint','premium-addons-for-elementor'),
                    'easeInSine'        => esc_html__('Ease in Sine','premium-addons-for-elementor'),
                    'easeOutSine'       => esc_html__('Ease out Sine','premium-addons-for-elementor'),
                    'easeInOutSine'     => esc_html__('Ease in out Sine','premium-addons-for-elementor'),
                    'easeInExpo'        => esc_html__('Ease in Expo','premium-addons-for-elementor'),
                    'easeOutExpo'       => esc_html__('Ease out Expo','premium-addons-for-elementor'),
                    'easeInOutExpo'     => esc_html__('Ease in out Cubic','premium-addons-for-elementor'),
                    'easeInCirc'        => esc_html__('Ease in Circle','premium-addons-for-elementor'),
                    'easeOutCirc'       => esc_html__('Ease out Circle','premium-addons-for-elementor'),
                    'easeInOutCirc'     => esc_html__('Ease in out Circle','premium-addons-for-elementor'),
                    'easeInElastic'     => esc_html__('Ease in Elastic','premium-addons-for-elementor'),
                    'easeOutElastic'    => esc_html__('Ease out Elastic','premium-addons-for-elementor'),
                    'easeInOutElastic'  => esc_html__('Ease in out Elastic','premium-addons-for-elementor'),
                    'easeInBack'        => esc_html__('Ease in Back','premium-addons-for-elementor'),
                    'easeOutBack'       => esc_html__('Ease out Back','premium-addons-for-elementor'),
                    'easeInOutBack'     => esc_html__('Ease in Out Back','premium-addons-for-elementor'),
                    'easeInBounce'      => esc_html__('Ease in Bounce','premium-addons-for-elementor'),
                    'easeOutBounce'     => esc_html__('Ease out Bounce','premium-addons-for-elementor'),
                    'easeInOutBounce'   => esc_html__('Ease in out Bounce','premium-addons-for-elementor'),
                ],
                'default'       => 'easeInQuad',
            ]
        );


        $this->end_controls_section();

        $this->start_controls_section('general_style',
            [
                'label'         => esc_html__('General','premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control('height',
            [
                'label'         => esc_html__('Height', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::NUMBER,
                'title'         => esc_html__('Set the height of the graph in pixels', 'premium-addons-for-elementor')
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'          => 'general_background',
                'types'         => [ 'classic', 'gradient' ],
                'selector'      => '{{WRAPPER}} .premium-chart-container',
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name'          => 'general_border',
                'selector'      => '{{WRAPPER}} .premium-chart-container',
                ]
            );

        $this->add_control('general_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%', 'em'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-chart-container' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'general_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-chart-container',
            ]
        );

        $this->add_responsive_control('general_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em', '%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-chart-container' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ]
            ]
        );

        $this->add_responsive_control('general_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em', '%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-chart-container' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('title_style',
            [
                'label'         => esc_html__('Title','premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'title_switcher'  => 'true'
                ]
            ]
        );

        $this->add_control('title_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-chart-title' => 'color: {{VALUE}};',
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'title_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'selector'      => '{{WRAPPER}} .premium-chart-title',
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'          => 'title_background',
                'types'         => [ 'classic', 'gradient' ],
                'selector'      => '{{WRAPPER}} .premium-chart-title-container',
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
                [
                    'name'      => 'title_border',
                    'selector'  => '{{WRAPPER}} .premium-chart-title-container',
                ]
            );

        $this->add_control('title_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', '%', 'em'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-chart-title-container' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'title_box_shadow',
                'selector'      => '{{WRAPPER}} .premium-chart-title',
            ]
        );

        $this->add_responsive_control('title_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em', '%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-chart-title-container .premium-chart-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ]
            ]
        );

        $this->add_responsive_control('title_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => [ 'px', 'em', '%' ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-chart-title-container' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('x_axis_style',
            [
                'label'         => esc_html__('X-Axis','premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control('x_axis_label_pop',
            [
                'label'         => esc_html__('Axis Label', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::POPOVER_TOGGLE,
                'condition'     => [
                    'x_axis_label_switch' => 'true'
                ]
            ]
        );

        $this->start_popover();

        $this->add_control('x_axis_label_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
            ]
        );

        $this->add_control('x_axis_label_size',
            [
                'label'			=> esc_html__( 'Size', 'premium-addons-for-elementor' ),
                'type'			=> Controls_Manager::NUMBER,
                'min'           => 0,
                'max'           => 50,
                'default'       => 12
            ]
        );

        $this->end_popover();

        $this->add_control('x_axis_labels_pop',
            [
                'label'         => esc_html__('Data Labels', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::POPOVER_TOGGLE,
            ]
        );

        $this->start_popover();

        $this->add_control('x_axis_labels_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
            ]
        );

        $this->add_control('x_axis_labels_size',
            [
                'label'			=> esc_html__( 'Size', 'premium-addons-for-elementor' ),
                'type'			=> Controls_Manager::NUMBER,
                'min'           => 0,
                'max'           => 50,
                'default'       => 12
            ]
        );

        $this->end_popover();

        $this->add_control('x_axis_grid_pop',
            [
                'label'         => esc_html__('Grid', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::POPOVER_TOGGLE,
                'condition'     => [
                    'x_axis_grid' => 'true'
                ]
            ]
        );

        $this->start_popover();

        $this->add_control('x_axis_grid_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'default'       => '#6ec1e4',
            ]
        );

        $this->end_popover();

        $this->end_controls_section();

        $this->start_controls_section('y_axis_style',
            [
                'label'         => esc_html__('Y-Axis','premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control('y_axis_label_pop',
            [
                'label'         => esc_html__('Axis Label', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::POPOVER_TOGGLE,
                'condition'     => [
                    'y_axis_label_switch' => 'true'
                ]
            ]
        );

        $this->start_popover();

        $this->add_control('y_axis_label_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                    ],
                ]
            );

        $this->add_control('y_axis_label_size',
            [
                'label'			=> esc_html__( 'Size', 'premium-addons-for-elementor' ),
                'type'			=> Controls_Manager::NUMBER,
                'min'           => 0,
                'max'           => 50,
                'default'       => 12
            ]
        );

        $this->end_popover();

        $this->add_control('y_axis_data_pop',
            [
                'label'         => esc_html__('Data', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::POPOVER_TOGGLE,
            ]
        );

        $this->start_popover();

        $this->add_control('y_axis_labels_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
            ]
        );

        $this->add_control('y_axis_labels_size',
            [
                'label'			=> esc_html__( 'Size', 'premium-addons-for-elementor' ),
                'type'			=> Controls_Manager::NUMBER,
                'min'           => 0,
                'max'           => 50,
                'default'       => 12
            ]
        );

        $this->end_popover();

        $this->add_control('y_axis_grid_pop',
            [
                'label'         => esc_html__('Grid', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::POPOVER_TOGGLE,
                'condition'     => [
                    'y_axis_grid' => 'true'
                ]
            ]
        );

        $this->start_popover();

        $this->add_control('y_axis_grid_color',
            [
                'label'         => esc_html__('Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'default'       => '#54595f',
            ]
        );

        $this->end_popover();

        $this->end_controls_section();
        
        $this->start_controls_section('legend_style',
            [
                'label'         => esc_html__('Legend','premium-charts'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'legend_display'   => 'true'
                ]
            ]
        );
        
        $this->add_control('legend_text_color',
            [
                'label'         => esc_html__('Color', 'premium-charts'),
                'type'          => Controls_Manager::COLOR,
            ]
        );
        
        $this->add_control('legend_text_size',
			[
				'label'			=> esc_html__( 'Size', 'premium-charts' ),
				'type'			=> Controls_Manager::NUMBER,
                'min'           => 0,
                'max'           => 50,
                'default'       => 12
			]
		);
        
        $this->add_control(
            'legend_item_width',
			[
				'label'			=> esc_html__( 'Item Width', 'premium-charts' ),
				'type'			=> Controls_Manager::NUMBER,
                'min'           => 1,
                'default'       => 40
			]
		);
        
        $this->end_controls_section();
        
    }
    
    /**
	 * renders the HTML content of the widget
	 * @return void
	 */
    protected function render() {
        
        $settings = $this->get_settings_for_display();
        
        if(!empty($settings['title']) && $settings['title_switcher']){
            $title = '<' . $settings['title_tag'] . ' class="premium-chart-title">'. $settings['title'] .'</'.$settings['title_tag'] . '>';
        }
        $xlabels = explode(',', $settings['x_axis_labels']);

        $col_data = '';
        $columns_array = array();
        
        foreach( $settings['y_axis_data'] as $column_data ) {
            
            if( 'pie' != $settings['type'] && 'doughnut' != $settings['type'] ) {
                $background = $column_data['y_axis_column_color'];
            } else {
                $background = explode(',', $column_data['y_axis_circle_color']);
            }
            
            $col_settings = [
                'label'             => $column_data['y_axis_column_title'],
                'data'              => explode( ',', $column_data['y_axis_column_data'] ),
                'links'             => explode( ',', $column_data['y_axis_urls']),
                'backgroundColor'   => $background,
                'borderColor'       => $column_data['y_axis_column_border_color'],
                'borderWidth'       => 1
            ];
            
            array_push($columns_array, $col_settings);
            
        }
        
        $labels_rotation    = !empty( $settings['x_axis_label_rotation'] ) ? $settings['x_axis_label_rotation'] : 0;
        
        $x_label_size       = !empty( $settings['x_axis_labels_size'] ) ? $settings['x_axis_labels_size'] : 12;
        
        $y_label_size       = !empty( $settings['y_axis_labels_size'] ) ? $settings['y_axis_labels_size'] : 12;
        
        if ( 'horizontalBar' != $settings['type'] ) {
            
            $ytype = $settings['data_type'];
            
        } else {
            
            $ytype = 'category';
            
        }
        
        $chart_id = 'premium-chart-canvas-' . esc_attr($this->get_id());
        
        $chart_settings = [
            'type'          => $settings['type'],
            'xlabeldis'     => $settings['x_axis_label_switch'],
            'xlabel'        => $settings['x_axis_label'],
            'ylabeldis'     => $settings['y_axis_label_switch'],
            'ylabel'        => $settings['y_axis_label'],
            'xlabels'       => $xlabels,
            'easing'        => $settings['start_animation'],
            'enTooltips'    => $settings['tool_tips'],
            'percentage'    => $settings['tool_tips_percent'],
            'modTooltips'   => $settings['tool_tips_mode'],
            'legDis'        => $settings['legend_display'],
            'legPos'        => $settings['legend_position'],
            'legRev'        => $settings['legend_reverse'],
            'legCol'        => ! empty( $settings['legend_text_color'] ) ? esc_html( $settings['legend_text_color'] ) : '#54595f',
            'legSize'       => esc_html( $settings['legend_text_size'] ),
            'itemWid'       => esc_html( $settings['legend_item_width'] ),
            'xGrid'         => $settings['x_axis_grid'],
            'xGridCol'      => $settings['x_axis_grid_color'],
            'xTicksSize'    => $x_label_size,
            'xlabelcol'     => $settings['x_axis_label_color'],
            'ylabelcol'     => $settings['y_axis_label_color'],
            'xlabelsize'    => $settings['x_axis_label_size'],
            'ylabelsize'    => $settings['y_axis_label_size'],
            'xTicksCol'     => !empty( $settings['x_axis_labels_color'] ) ? $settings['x_axis_labels_color'] : '#54595f',
            'xTicksRot'     => $labels_rotation,
            'xTicksBeg'     => $settings['x_axis_begin'],
            'yAxis'         => $ytype,
            'yGrid'         => $settings['y_axis_grid'],
            'yGridCol'      => $settings['y_axis_grid_color'],
            'yTicksSize'    => $y_label_size,
            'yTicksCol'     => !empty( $settings['y_axis_labels_color'] ) ? $settings['y_axis_labels_color'] : '#54595f',
            'yTicksBeg'     => $settings['y_axis_begin'],
            'chartId'       => $chart_id,
            'suggestedMax'  => !empty( $settings['y_axis_max'] ) ? $settings['y_axis_max'] : 1,
            'height'        => !empty( $settings['height'] ) ? $settings['height'] : 400,
            'target'        => ( $settings['y_axis_urls_target'] ) ? '_blank' : '_top'
        ];
        
        ?>

<div id="premium-chart-container-<?php echo esc_attr($this->get_id()); ?>" class="premium-chart-container" data-chart='<?php echo wp_json_encode( $columns_array ); ?>' data-settings='<?php echo wp_json_encode( $chart_settings ); ?>'>
    <?php if( !empty( $settings['title'] ) && $settings['title_switcher'] && 'top' == $settings['title_position'] ) : ?>
        <div class="premium-chart-title-container"><?php echo $title; ?></div>
    <?php endif; ?>
    <div class="premium-chart-canvas-container">
        <canvas id="premium-chart-canvas-<?php echo esc_attr( $this->get_id() ); ?>" class="premium-chart-canvas" width="400" height="400"></canvas>
    </div>
    <?php if( !empty( $settings['title'] ) && $settings['title_switcher'] && 'bottom' == $settings['title_position'] ) : ?>
        <div class="premium-chart-title-container"><?php echo $title; ?></div>
    <?php endif; ?>
</div>

    <?php }
    
}