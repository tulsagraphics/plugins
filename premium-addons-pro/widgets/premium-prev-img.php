<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Premium_Prev_Img extends Widget_Base {
    public function getTemplateInstance() {
		return $this->templateInstance = premium_Template_Tags::getInstance();
	}
   
    public function get_name() {
        return 'premium-addon-preview-image';
    }

    public function get_title() {
		return \PremiumAddons\Helper_Functions::get_prefix() . ' Preview Window';
	}

    public function get_icon() {
        return 'pa-pro-preview-window';
    }
    
     public function is_reload_preview_required() {
        return true;
    }	

    public function get_script_depends(){
        return [
            'tooltipster-bundle-js',
            'premium-pro-js'
            ];
    }
    
    public function get_categories() {
        return [ 'premium-elements' ];
    }

    // Adding the controls fields for the Preview Image
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {

        /* Start Image Preview Content Section */
        $this->start_controls_section('premium_preview_image',
            [
                'label'         => esc_html__('Trigger Image', 'premium-addons-for-elementor'),
                ]
            );
        
        $this->add_control('premium_preview_image_main',
            [
                'label' => esc_html__( 'Choose Image', 'premium-addons-for-elementor' ),
                'type' => Controls_Manager::MEDIA,
                'default'       => [
                    'url'	=> PREMIUM_PRO_ADDONS_URL. 'assets/images/placeholder.png',
                    ],
                ]
            );
        
	$this->add_group_control(
		Group_Control_Image_Size::get_type(),
            [
                'name' => 'premium_preview_image_main_size', 
                'default' => 'full',
                ]
            );
    
        $this->add_control('premium_preview_image_alt', 
            [
                'label'     => esc_html__('Alt','premium-addons-for-elementor'),
                'type'      => Controls_Manager::TEXT,
                'dynamic'   => [ 'active' => true ],
                'default'	=>'Premium Preview Window',
                ]
            );
        
        $this->add_control('premium_preview_image_caption', 
            [
                'label'     => esc_html__('Caption','premium-addons-for-elementor'),
                'type'      => Controls_Manager::TEXT,
                'dynamic'   => [ 'active' => true ],
                'default'	=>'Premium Preview Window',
                ]
            );
    
        $this->add_control('premium_preview_image_link_switcher',
                [
                    'label'         => esc_html__('Link', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SWITCHER,
                ]
                );
        
        $this->add_control('premium_preview_image_link_selection', 
            [
                'label'         => esc_html__('Link Type', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'url'   => esc_html__('URL', 'premium-addons-for-elementor'),
                    'link'  => esc_html__('Existing Page', 'premium-addons-for-elementor'),
                ],
                'default'       => 'url',
                'label_block'   => true,
                'condition'     => [
                    'premium_preview_image_link_switcher'    => 'yes'
                ]
            ]
        );
        
        $this->add_control('premium_preview_image_link',
            [
                'label'         => esc_html__('Link', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::URL,
                'default'       => [
                    'url'   => '#',
                    ],
                'placeholder'   => 'https://premiumaddons.com/',
                'label_block'   => true,
                'condition'     => [
                    'premium_preview_image_link_selection' => 'url',
                    'premium_preview_image_link_switcher'    => 'yes'
                ]
            ]
            );
        
        $this->add_control('premium_preview_image_existing_link',
            [
                'label'         => esc_html__('Existing Page', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT2,
                'options'       => $this->getTemplateInstance()->get_all_post(),
                'condition'     => [
                    'premium_preview_image_link_selection'     => 'link',
                    'premium_preview_image_link_switcher'    => 'yes'
                ],
                'multiple'      => false,
                'label_block'   => true,
                ]
            );
        
        $this->add_control('premium_preview_image_align',
                [
                    'label'         => esc_html__('Alignment','premuim-addons-for-elementor'),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'left'      => [
                               'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                               'icon' => 'fa fa-align-left',   
                                ],
                       'center'     => [
                               'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                               'icon' => 'fa fa-align-center',
                                ],
                       'right'     => [
                               'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                               'icon' => 'fa fa-align-right',
                               ],
                       ],
                    'default'       => 'center',
                    'selectors'     => [
                       '{{WRAPPER}} .premium-preview-image-trig-img-wrap' => 'text-align: {{VALUE}};',
                        ],
                    ]
                );
         
        $this->end_controls_section();
        
        $this->start_controls_section('premium_preview_image_magnifier',
                [
                    'label'             => esc_html__('Preview Window', 'premium-addons-for-elementor'),
                    ]
                );
        
        $this->add_control('premium_preview_image_content_selection', 
            [
                'label'         => esc_html__('Content Type', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'custom'        => esc_html__('Custom Content', 'premium-addons-for-elementor'),
                    'template'      => esc_html__('Elementor Template', 'premium-addons-for-elementor'),
                ],
                'default'       => 'custom',
                'label_block'   => true,
            ]
        );
        
        $this->add_control('premium_preview_image_img_switcher',
            [
                'label'         => esc_html__('Image', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
                'condition' => [
                    'premium_preview_image_content_selection'   => 'custom'
                ]
            ]
            );
        
        $this->add_control('premium_preview_image_tooltips_image',
		[
            'label' => esc_html__( 'Choose Image', 'premium-addons-for-elementor' ),
            'type' => Controls_Manager::MEDIA,
            'default'       => [
                'url'	=> PREMIUM_PRO_ADDONS_URL. 'assets/images/placeholder.png',
                ],
            'condition' => [
                'premium_preview_image_content_selection'   => 'custom',
                'premium_preview_image_img_switcher'        => 'yes'
                ]
            ]
            );
        
        $this->add_group_control(
            Group_Control_Image_Size::get_type(),
            [
                'name' => 'premium_preview_image_tooltips_image_size', 
                'default' => 'full',
                'condition' => [
                    'premium_preview_image_content_selection'   => 'custom',
                    'premium_preview_image_img_switcher'        => 'yes'
                ]
            ]
            );
        
        $this->add_control('premium_preview_image_tooltips_image_alt', 
            [
                'label'     => esc_html__('Alt','premium-addons-for-elementor'),
                'type'      => Controls_Manager::TEXT,
                'dynamic'   => [ 'active' => true ],
                'default'	=> 'Premium Preview Window',
                'condition' => [
                    'premium_preview_image_content_selection'   => 'custom',
                    'premium_preview_image_img_switcher'        => 'yes'
                    ]
                ]
            );
        
        $this->add_responsive_control('premium_preview_image_tooltip_img_align',
                [
                    'label'         => esc_html__('Alignment','premuim-addons-for-elementor'),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'left'      => [
                               'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                               'icon' => 'fa fa-align-left',   
                                ],
                       'center'     => [
                               'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                               'icon' => 'fa fa-align-center',
                                ],
                       'right'     => [
                               'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                               'icon' => 'fa fa-align-right',
                               ],
                       ],
                    'default'       => 'center',
                    'selectors'     => [
                       '.premium-prev-img-tooltip-img-wrap-{{ID}}' => 'text-align: {{VALUE}};',
                    ],
                    'condition' => [
                        'premium_preview_image_content_selection'   => 'custom',
                        'premium_preview_image_img_switcher'        => 'yes'
                        ]
                    ]
                );
        
        $this->add_control('premium_preview_image_title_switcher',
            [
                'label'         => esc_html__('Title', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'condition' => [
                    'premium_preview_image_content_selection'   => 'custom'
                ]
            ]
            );
        
        $this->add_control('premium_preview_image_title', 
                [
                    'label'     => esc_html__('Title','premium-addons-for-elementor'),
                    'type'      => Controls_Manager::TEXT,
                    'default'	=>'Premium Preview Image',
                    'condition' => [
                        'premium_preview_image_title_switcher'      => 'yes',
                        'premium_preview_image_content_selection'   => 'custom'
                        ]
                    ]
                );
        
        $this->add_control('premium_image_preview_title_heading', 
                [
                    'label'     => esc_html__('HTML Tag','premium-addons-for-elementor'),
                    'type'      => Controls_Manager::SELECT,
                    'default'   =>'h3',
                    'options'   =>[
                        'h1'    => 'H1',
                        'h2'    => 'H2',
                        'h3'    => 'H3',
                        'h4'    => 'H4',
                        'h5'    => 'H5',
                        'h6'    => 'H6'
                        ],
                    'condition' => [
                        'premium_preview_image_title_switcher'      => 'yes',
                        'premium_preview_image_content_selection'   => 'custom'
                        ]
                    ]
                );
        
        $this->add_responsive_control('premium_preview_image_tooltip_title_align',
                [
                    'label'         => esc_html__('Alignment','premuim-addons-for-elementor'),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'left'      => [
                               'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                               'icon' => 'fa fa-align-left',   
                                ],
                       'center'     => [
                               'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                               'icon' => 'fa fa-align-center',
                                ],
                       'right'     => [
                               'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                               'icon' => 'fa fa-align-right',
                               ],
                       ],
                    'default'       => 'center',
                    'selectors'     => [
                       '.premium-prev-img-tooltip-title-wrap-{{ID}}' => 'text-align: {{VALUE}};',
                    ],
                    'condition' => [
                        'premium_preview_image_content_selection'   => 'custom',
                        'premium_preview_image_title_switcher'        => 'yes'
                        ]
                    ]
                );
                
        $this->add_control('premium_preview_image_desc_switcher',
            [
                'label'         => esc_html__('Description', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'condition'     => [
                    'premium_preview_image_content_selection'   => 'custom'
                ]
            ]
            );
        
        $this->add_control('premium_preview_image_desc',
		[
                'label' => esc_html__( 'Content', 'premium-addons-for-elementor' ),
                'type' => Controls_Manager::WYSIWYG,
                'dynamic'   => [ 'active' => true ],
                'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit', 'premium_elementor'),
                'condition' => [
                    'premium_preview_image_desc_switcher'      => 'yes',
                    'premium_preview_image_content_selection'   => 'custom'
                ]
            ]
        );
        
        $this->add_responsive_control('premium_preview_image_tooltip_desc_align',
                [
                    'label'         => esc_html__('Alignment','premuim-addons-for-elementor'),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'left'      => [
                               'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                               'icon' => 'fa fa-align-left',   
                                ],
                       'center'     => [
                               'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                               'icon' => 'fa fa-align-center',
                                ],
                       'right'     => [
                               'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                               'icon' => 'fa fa-align-right',
                               ],
                       ],
                    'default'       => 'center',
                    'selectors'     => [
                       '.premium-prev-img-tooltip-desc-wrap-{{ID}}' => 'text-align: {{VALUE}};',
                    ],
                    'condition' => [
                        'premium_preview_image_content_selection'   => 'custom',
                        'premium_preview_image_desc_switcher'        => 'yes'
                        ]
                    ]
                );
        
        $this->add_control('premium_preview_image_content_temp',
                [
                    'label'			=> esc_html__( 'Choose Template', 'premium-addons-for-elementor' ),
                    'description'	=> esc_html__( 'Template content is a template which you can choose from Elementor library', 'premium-addons-for-elementor' ),
                    'type' => Controls_Manager::SELECT2,
                    'options' => $this->getTemplateInstance()->get_elementor_page_list(),
                    'condition' => [
                        'premium_preview_image_content_selection'   => 'template'
                    ]
                ]
            );
    
        $this->end_controls_section();
        
        $this->start_controls_section('premium_preview_image_advanced',
                [
                    'label'             => esc_html__('Advanced Settings', 'premium-addons-for-elementor'),
                    ]
                );
        
        $this->add_control('premium_preview_image_interactive',
                [
                    'label'         => esc_html__('Interactive', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SWITCHER,
                    'description'   => esc_html__('Give users the possibility to interact with the content of the tooltip', 'premium-addons-for-elementor'),
                ]
                );
        
        $this->add_control('premium_preview_image_responsive',
                [
                    'label'         => esc_html__('Responsive', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SWITCHER,
                    'description'   => esc_html__('Resize tooltip image to fit screen', 'premium-addons-for-elementor'),
                ]
                );
        
        $this->add_control('premium_preview_image_theme', 
            [
                'label'         => esc_html__('Tooltip Theme', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'description'   => esc_html__('Kindly, be noted that this will override styling for the container', 'premium-addons-for-elementor'),
                'options'       => [
                    'tooltipster-default'   => esc_html__('Custom', 'premium-addons-for-elementor'),
                    'tooltipster-light'     => esc_html__('Light', 'premium-addons-for-elementor'),
                    'tooltipster-borderless'=> esc_html__('Borderless', 'premium-addons-for-elementor'),
                    'tooltipster-punk'      => esc_html__('Punk', 'premium-addons-for-elementor'),
                    'tooltipster-noir'      => esc_html__('Noir', 'premium-addons-for-elementor'),
                    'tooltipster-shadow'    => esc_html__('Shadow', 'premium-addons-for-elementor'),
                ],
                'default'       => 'tooltipster-default',
                'label_block'   => true,
            ]
        );
        
        $this->add_control('premium_preview_image_anim', 
            [
                'label'         => esc_html__('Animation', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'fade'  => esc_html__('Fade', 'premium-addons-for-elementor'),
                    'grow'  => esc_html__('Grow', 'premium-addons-for-elementor'),
                    'swing' => esc_html__('Swing', 'premium-addons-for-elementor'),
                    'slide' => esc_html__('Slide', 'premium-addons-for-elementor'),
                    'fall'  => esc_html__('Fall', 'premium-addons-for-elementor'),
                ],
                'default'       => 'fade',
                'label_block'   => true,
            ]
        );
        
        $this->add_control('premium_preview_image_anim_dur',
                [
                    'label'             => esc_html__('Animation Duration', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::NUMBER,
                    'description'       => esc_html__('Set the animation duration in milliseconds, default is 350', 'premium-addons-for-elementor'),
                    'default'           => 350,
                ]
                );
        
        $this->add_control('premium_preview_image_delay',
                [
                    'label'             => esc_html__('Delay', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::NUMBER,
                    'description'       => esc_html__('Set the animation delay in milliseconds, default is 10'),
                    'default'           => 10,
                ]
                );
        
        $this->add_control('premium_preview_image_arrow',
                [
                    'label'         => esc_html__('Arrow', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SWITCHER,
                    'label_on'      => 'Show',
                    'label_off'     => 'Hide',
                    'description'   => esc_html__('Show an arrow beside the tooltip', 'premium-addons-for-elementor'),
                    'return_value'  => true,
                ]
                );
        
        $this->add_control('premium_preview_image_distance',
            [
                'label'             => esc_html__('Spacing', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::NUMBER,
                'description'       => esc_html__('The distance between the origin and the tooltip in pixels, default is 6','premium-addons-for-elementor'),
                'default'           => -1,
            ]
            );
        
        $this->add_responsive_control('premium_preview_image_min_width',
            [
                'label'             => esc_html__('Min Width', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::NUMBER,
                'description'       => esc_html__('Set a minimum width for the tooltip in pixels, default: 0 (auto width)','premium-addons-for-elemeneot'),
            ]
            );
        
        $this->add_responsive_control('premium_preview_image_max_width',
            [
                'label'             => esc_html__('Max Width', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::NUMBER,
                'description'       => esc_html__('Set a maximum width for the tooltip in pixels, default: null (no max width)','premium-addons-for-elemeneot'),
            ]
            );
        
        $this->add_control('premium_preview_image_custom_height_switcher',
                [
                    'label'         => esc_html__('Custom Height', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SWITCHER,
                    'label_on'      => 'Show',
                    'label_off'     => 'Hide',
                    'return_value'  => true,
                ]
                );
        
        $this->add_responsive_control('premium_preview_image_custom_height',
            [
                'label'         => esc_html__('Height', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em' , '%'],
                'range'         => [
                    'px'    => [
                        'min'       => 0,
                        'max'       => 800,
                    ],
                    'em'    => [
                        'min'       => 0,
                        'max'       => 20,
                    ]
                ],
                'default'       => [
                    'unit'  => 'px',
                    'size'  => 200,
                ],
                'label_block'   => true,
                'condition'     => [
                    'premium_preview_image_custom_height_switcher'  => 'true'
                ],
                'selectors'     => [
                    '.premium-prev-img-tooltip-wrap-{{ID}}' => 'height: {{SIZE}}{{UNIT}} !important;'
                ]
        	]
        );
        
        $this->add_control('premium_preview_image_side',
            [
                'label'             => esc_html__('Side', 'premium-addons-for-elementor'),
                'type'              => Controls_Manager::SELECT2,
                'options'           => [
                    'top'   => esc_html__('Top','premium-addons-for-elementor'),
                    'right' => esc_html__('Right','premium-addons-for-elementor'),
                    'bottom'=> esc_html__('Bottom','premium-addons-for-elementor'),
                    'left'  => esc_html__('Left','premium-addons-for-elementor'),
                ],
                'description'       => esc_html__('Sets the side of the tooltip. The value may one of the following: \'top\', \'bottom\', \'left\', \'right\'. It may also be an array containing one or more of these values. When using an array, the order of values is taken into account as order of fallbacks and the absence of a side disables it','premium-addons-for-elementor'),
                'default'           => ['right','left'],
                'multiple'          => true,
            ]);
        
        $this->add_control('premium_preview_image_hide',
                [
                    'label'         => esc_html__('Hide on Mobiles', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SWITCHER,
                    'label_on'      => 'Show',
                    'label_off'     => 'Hide',
                    'description'   => esc_html__('Hide tooltips on mobile phones', 'premium-addons-for-elementor'),
                    'return_value'  => true,
                ]
                );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_preview_image_trigger_style_settings',
            [
                'label'             => esc_html__('Trigger Image', 'premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
                ]
            );
        
        $this->add_control('premium_preview_image_trigger_background', 
            [
               'label'              => esc_html__('Background Color','premuium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'         => [
                    '{{WRAPPER}} .premium-preview-image-trigger'  => 'background-color:{{VALUE}};'
                    ]
               ]
            );
        
        $this->add_group_control(
                Group_Control_Border::get_type(),
                [
                    'name'              => 'premium_preview_image_trigger_border',
                    'selector'          => '{{WRAPPER}} .premium-preview-image-trigger',
                    ]
                );
        
        $this->add_responsive_control('premium_preview_image_trigger_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-preview-image-trigger' => 'border-top-left-radius: {{TOP}}{{UNIT}}; border-top-right-radius: {{RIGHT}}{{UNIT}}; border-bottom-right-radius: {{BOTTOM}}{{UNIT}}; border-bottom-left-radius: {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_group_control(
                Group_Control_Box_Shadow::get_type(),
                [
                    'name'              => 'premium_preview_image_trigger_shadow',
                    'selector'          => '{{WRAPPER}} .premium-preview-image-trigger',
                    ]
                );
        
        $this->add_responsive_control('premium_preview_image_trigger_margin',
                [
                    'label'         => esc_html__( 'Margin', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                       '{{WRAPPER}} .premium-preview-image-trigger' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                      	],
                    ]
               );

        $this->add_responsive_control('premium_preview_image_trigger_padding',
                [
                    'label'         => esc_html__( 'Padding', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                      '{{WRAPPER}} .premium-preview-image-trigger' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ],
                    ]
                );
      
        $this->end_controls_section();
        
        $this->start_controls_section('premium_preview_image_caption_style',
            [
                'label'             => esc_html__('Trigger Image Caption', 'premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
                'condition'         => [
                    'premium_preview_image_caption!'    => ''
                    ]
                ]
            );
        
        $this->add_control('premium_preview_image_caption_color',
                [
                    'label'             => esc_html__('Text Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'scheme'        => [
                        'type'  => Scheme_Color::get_type(),
                        'value' => Scheme_Color::COLOR_2,
                    ],
                    'selectors'         => [
                        '{{WRAPPER}} .premium-preview-image-figcap' => 'color: {{VALUE}};',
                        ],
                    ]
                );
        
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'                  => 'premium_preview_image_caption_typo',
                'scheme'                => Scheme_Typography::TYPOGRAPHY_1,
                'selector'              => '{{WRAPPER}} .premium-preview-image-figcap'
            ]
        );
        
        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'name'          => 'premium_preview_image_caption_shadow',
                    'selector'      => '{{WRAPPER}} .premium-preview-image-figcap'
                    ]
                );
        
        $this->add_control('premium_preview_image_caption_background_color',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'         => [
                        '{{WRAPPER}} .premium-preview-image-figcap'    => 'background: {{VALUE}};',
                        ],
                    ]
                );

        
        $this->add_group_control(
                Group_Control_Border::get_type(),
                [
                    'name'              => 'premium_preview_image_caption_border',
                    'selector'          => '{{WRAPPER}} .premium-preview-image-figcap'
                    ]
                );
        
        $this->add_responsive_control('premium_preview_image_caption_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-preview-image-figcap' => 'border-radius: {{SIZE}}{{UNIT}}'
                    ]
                ]);
        
        $this->add_responsive_control('premium_preview_image_caption_margin',
                [
                    'label'             => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::DIMENSIONS,
                    'size_units'        => ['px', 'em', '%'],
                    'selectors'         => [
                        '{{WRAPPER}} .premium-preview-image-figcap' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ]
                    ]
                );
        
        $this->add_responsive_control('premium_preview_image_caption_padding',
                [
                    'label'             => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::DIMENSIONS,
                    'size_units'        => ['px', 'em', '%'],
                    'selectors'         => [
                        '{{WRAPPER}} .premium-preview-image-figcap' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ]
                    ]
                );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_preview_image_tooltip_style_settings',
            [
                'label'             => esc_html__('Preview Window Content', 'premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
                ]
            );
        
	$this->add_control('premium_preview_image_tooltip_background', 
            [
               'label'              => esc_html__('Background Color','premuium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'         => [
                    '.premium-prev-img-tooltip-wrap-{{ID}}'  => 'background-color:{{VALUE}};'
                    ]
               ]
            );
        
        $this->add_group_control(
                Group_Control_Border::get_type(),
                [
                    'name'              => 'premium_preview_image_tooltip_border',
                    'selector'          => '.premium-prev-img-tooltip-wrap-{{ID}}'
                    ]
                );
        
        $this->add_responsive_control('premium_preview_image_tooltip_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '.premium-prev-img-tooltip-wrap-{{ID}}' => 'border-top-left-radius: {{TOP}}{{UNIT}}; border-top-right-radius: {{RIGHT}}{{UNIT}}; border-bottom-right-radius: {{BOTTOM}}{{UNIT}}; border-bottom-left-radius: {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_group_control(
                Group_Control_Box_Shadow::get_type(),
                [
                    'name'              => 'premium_preview_image_tooltip_shadow',
                    'selector'          => '.premium-prev-img-tooltip-wrap-{{ID}}'
                    ]
                );
        
        $this->add_responsive_control('premium_preview_image_tooltip_margin',
                [
                    'label'         => esc_html__( 'Margin', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                       '.premium-prev-img-tooltip-wrap-{{ID}}' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                      	],
                    ]
               );

        $this->add_responsive_control('premium_preview_image_tooltip_padding',
                [
                    'label'         => esc_html__( 'Padding', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                      '.premium-prev-img-tooltip-wrap-{{ID}}' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ],
                    ]
                );
      
        $this->end_controls_section();
        
        $this->start_controls_section('premium_preview_image_tooltip_img_style_settings',
            [
                'label'             => esc_html__('Preview Window Image', 'premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'premium_preview_image_content_selection'   => 'custom',
                    'premium_preview_image_img_switcher'        => 'yes'
                ]
                ]
            );
        
        $this->add_control('premium_preview_image_tooltip_img_background', 
            [
               'label'              => esc_html__('Background Color','premuium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'         => [
                    '.premium-prev-img-tooltip-img-wrap-{{ID}} .premium-preview-image-tooltips-img'  => 'background-color:{{VALUE}};'
                    ]
               ]
            );
        
        $this->add_group_control(
                Group_Control_Border::get_type(),
                [
                    'name'              => 'premium_preview_image_tooltip_img_border',
                    'selector'          => '.premium-prev-img-tooltip-img-wrap-{{ID}} .premium-preview-image-tooltips-img'
                    ]
                );
        
        $this->add_responsive_control('premium_preview_image_tooltip_img_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '.premium-prev-img-tooltip-img-wrap-{{ID}} .premium-preview-image-tooltips-img' => 'border-top-left-radius: {{TOP}}{{UNIT}}; border-top-right-radius: {{RIGHT}}{{UNIT}}; border-bottom-right-radius: {{BOTTOM}}{{UNIT}}; border-bottom-left-radius: {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_group_control(
                Group_Control_Box_Shadow::get_type(),
                [
                    'name'              => 'premium_preview_image_tooltip_img_shadow',
                    'selector'          => '.premium-prev-img-tooltip-img-wrap-{{ID}} .premium-preview-image-tooltips-img'
                    ]
                );
        
        $this->add_responsive_control('premium_preview_image_tooltip_img_margin',
                [
                    'label'         => esc_html__( 'Margin', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                       '.premium-prev-img-tooltip-img-wrap-{{ID}} .premium-preview-image-tooltips-img' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                      	],
                    ]
               );

        $this->add_responsive_control('premium_preview_image_tooltip_img_padding',
                [
                    'label'         => esc_html__( 'Padding', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                      '.premium-prev-img-tooltip-img-wrap-{{ID}} .premium-preview-image-tooltips-img' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ],
                    ]
                );
      
        $this->end_controls_section();
        
        $this->start_controls_section('premium_preview_image_tooltip_title_style_settings',
            [
                'label'             => esc_html__('Preview Window Title', 'premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'premium_preview_image_content_selection'   => 'custom',
                    'premium_preview_image_title_switcher'        => 'yes'
                ]
                ]
            );
        
        $this->add_control('premium_preview_image_tooltip_title_color', 
            [
               'label'              => esc_html__('Color','premuium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors'         => [
                    '.premium-prev-img-tooltip-title-wrap-{{ID}} .premium-previmg-tooltip-title'  => 'color: {{VALUE}};'
                    ]
               ]
            );
        
        $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'                  => 'premium_preview_image_tooltip_title_typo',
				'scheme'                => Scheme_Typography::TYPOGRAPHY_1,
                'selector'              => '.premium-prev-img-tooltip-title-wrap-{{ID}} .premium-previmg-tooltip-title'
			]
		);

        $this->add_group_control(
                Group_Control_Text_Shadow::get_type(),
                [
                    'name'              => 'premium_preview_image_tooltip_title_shadow',
                    'selector'          => '.premium-prev-img-tooltip-title-wrap-{{ID}}'
                    ]
                );
        
        $this->add_control('premium_preview_image_tooltip_title_background', 
            [
               'label'              => esc_html__('Background Color','premuium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'         => [
                    '.premium-prev-img-tooltip-title-wrap-{{ID}}'  => 'background-color:{{VALUE}};'
                    ]
               ]
            );
        
        $this->add_group_control(
                Group_Control_Border::get_type(),
                [
                    'name'              => 'premium_preview_image_tooltip_title_border',
                    'selector'          => '.premium-prev-img-tooltip-title-wrap-{{ID}}'
                    ]
                );
        
        $this->add_responsive_control('premium_preview_image_tooltip_title_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '.premium-prev-img-tooltip-title-wrap-{{ID}}' => 'border-top-left-radius: {{TOP}}{{UNIT}}; border-top-right-radius: {{RIGHT}}{{UNIT}}; border-bottom-right-radius: {{BOTTOM}}{{UNIT}}; border-bottom-left-radius: {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_responsive_control('premium_preview_image_tooltip_title_margin',
                [
                    'label'         => esc_html__( 'Margin', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                       '.premium-prev-img-tooltip-title-wrap-{{ID}}' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                      	],
                    ]
               );

        $this->add_responsive_control('premium_preview_image_tooltip_title_padding',
                [
                    'label'         => esc_html__( 'Padding', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                      '.premium-prev-img-tooltip-title-wrap-{{ID}}' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ],
                    ]
                );
      
        $this->end_controls_section();
        
        $this->start_controls_section('premium_preview_image_tooltip_desc_style_settings',
            [
                'label'             => esc_html__('Preview Window Description', 'premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'premium_preview_image_content_selection'   => 'custom',
                    'premium_preview_image_desc_switcher'        => 'yes'
                ]
                ]
            );
        
        $this->add_control('premium_preview_image_tooltip_desc_color', 
            [
               'label'              => esc_html__('Color','premuium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'         => [
                    '.premium-prev-img-tooltip-desc-wrap-{{ID}}'  => 'color:{{VALUE}};'
                    ]
               ]
            );
        
        
        $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'                  => 'premium_preview_image_tooltip_desc_typo',
				'scheme'                => Scheme_Typography::TYPOGRAPHY_1,
                'selector'              => '.premium-prev-img-tooltip-desc-wrap-{{ID}}'
			]
		);

        $this->add_group_control(
                Group_Control_Text_Shadow::get_type(),
                [
                    'name'              => 'premium_preview_image_tooltip_desc_shadow',
                    'selector'          => '.premium-prev-img-tooltip-desc-wrap-{{ID}}'
                    ]
                );
        
        $this->add_control('premium_preview_image_tooltip_desc_background', 
            [
               'label'              => esc_html__('Background Color','premuium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'         => [
                    '.premium-prev-img-tooltip-desc-wrap-{{ID}}'  => 'background-color:{{VALUE}};'
                    ]
               ]
            );
        
        $this->add_group_control(
                Group_Control_Border::get_type(),
                [
                    'name'              => 'premium_preview_image_tooltip_desc_border',
                    'selector'          => '.premium-prev-img-tooltip-desc-wrap-{{ID}}'
                    ]
                );
        
        $this->add_responsive_control('premium_preview_image_tooltip_desc_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '.premium-prev-img-tooltip-desc-wrap-{{ID}}' => 'border-top-left-radius: {{TOP}}{{UNIT}}; border-top-right-radius: {{RIGHT}}{{UNIT}}; border-bottom-right-radius: {{BOTTOM}}{{UNIT}}; border-bottom-left-radius: {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_responsive_control('premium_preview_image_tooltip_desc_margin',
                [
                    'label'         => esc_html__( 'Margin', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                       '.premium-prev-img-tooltip-desc-wrap-{{ID}}' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                      	],
                    ]
               );

        $this->add_responsive_control('premium_preview_image_tooltip_desc_padding',
                [
                    'label'         => esc_html__( 'Padding', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                      '.premium-prev-img-tooltip-desc-wrap-{{ID}}' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ],
                    ]
                );
      
        $this->end_controls_section();
        
        $this->start_controls_section('premium_preview_image_tooltip_container',
            [
                'label'             => esc_html__('Preview Window Container', 'premium-addons-for-elementor'),
                'tab'               => Controls_Manager::TAB_STYLE,
                ]
            );
        
        $this->add_control('premium_preview_image_tooltip_container_background', 
            [
               'label'              => esc_html__('Background Color','premuium-addons-for-elementor'),
                'type'              => Controls_Manager::COLOR,
                'scheme'            => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'         => [
                    '.tooltipster-sidetip div.tooltipster-box-{{ID}}'  => 'background-color:{{VALUE}};'
                    ]
               ]
            );
        
        $this->add_group_control(
                Group_Control_Border::get_type(),
                [
                    'name'              => 'premium_preview_image_tooltip_container_border',
                    'selector'          => '.tooltipster-sidetip div.tooltipster-box-{{ID}}'
                    ]
                );
        
        $this->add_responsive_control('premium_preview_image_tooltip_container_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '.tooltipster-sidetip div.tooltipster-box-{{ID}}' => 'border-top-left-radius: {{TOP}}{{UNIT}}; border-top-right-radius: {{RIGHT}}{{UNIT}}; border-bottom-right-radius: {{BOTTOM}}{{UNIT}}; border-bottom-left-radius: {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_group_control(
                Group_Control_Box_Shadow::get_type(),
                [
                    'name'              => 'premium_preview_image_tooltip_container_shadow',
                    'selector'          => '.tooltipster-sidetip div.tooltipster-box-{{ID}}'
                    ]
                );
        
        $this->add_responsive_control('premium_preview_image_tooltip_containe_rpadding',
                [
                    'label'         => esc_html__( 'Padding', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => [ 'px', 'em', '%' ],
                    'selectors'     => [
                      '.tooltipster-sidetip div.tooltipster-box-{{ID}}' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ],
                    ]
                );
        
        $this->end_controls_section();
       
    }

    protected function render($instance = [])
    {
        $settings = $this->get_settings_for_display();
        $elementor_post_id = $settings['premium_preview_image_content_temp'];
        $premium_elements_frontend = new Frontend;
        
        if(!empty($settings['premium_preview_image_main']['url'])) {
            $image_main = $settings['premium_preview_image_main'];
            $image_url_main = Group_Control_Image_Size::get_attachment_image_src( $image_main['id'], 'premium_preview_image_main_size', $settings );
            if( empty( $image_url_main ) ) { 
                $image_url_main = $image_main['url']; 
            } else {
                $image_url_main = $image_url_main;
            }       
        }
        
        $size = 0;
        if(!empty($settings['premium_preview_image_tooltips_image']['url'])) {
            
            $tooltips_image = $settings['premium_preview_image_tooltips_image'];
            $selected_size = $settings[ 'premium_preview_image_tooltips_image_size' . '_size' ];
            $size = wp_get_attachment_image_src($tooltips_image['id'],$selected_size);
            $tool_tips_image_url = Group_Control_Image_Size::get_attachment_image_src( $tooltips_image['id'], 'premium_preview_image_tooltips_image_size', $settings );
            if( empty( $tool_tips_image_url ) ) { 
                $tool_tips_image_url = $tooltips_image['url']; 
            } else {
                $tool_tips_image_url = $tool_tips_image_url;
            }       
        }
        
        $this->add_render_attribute('premium_preview_img_trigger', 'class', 'premium-preview-image-trigger');
        $this->add_render_attribute('premium_preview_img_trigger', 'alt', $settings['premium_preview_image_alt']);
        $this->add_render_attribute('premium_preview_img_trigger', 'src', $image_url_main);
        
        $this->add_inline_editing_attributes('premium_preview_image_caption', 'basic');
        $this->add_render_attribute('premium_preview_image_caption', 'class', 'premium-preview-image-figcap');
        
        
        if($settings['premium_preview_image_link_switcher'] == 'yes'){
            if($settings['premium_preview_image_link_selection'] == 'url'){
                $preview_img_url = $settings['premium_preview_image_link']['url'];
            } else {
                $preview_img_url = get_permalink($settings['premium_preview_image_existing_link']);
            }
        }
        
        $tooltip_container = [
          'background'      => $settings['premium_preview_image_tooltip_container_background']
        ];
        
        $prev_img_settings = [
            'theme'     => $settings['premium_preview_image_theme'],
            'anim'      => $settings['premium_preview_image_anim'],
            'animDur'   => !empty($settings['premium_preview_image_anim_dur']) ? $settings['premium_preview_image_anim_dur'] : 350,
            'delay'     => !empty($settings['premium_preview_image_delay']) ? $settings['premium_preview_image_delay'] : 10,
            'arrow'     => ( $settings['premium_preview_image_arrow'] == true ) ? true : false,
            'active'    => ( $settings['premium_preview_image_interactive'] == 'yes') ? true : false,
            'responsive'    => ( $settings['premium_preview_image_responsive'] == 'yes' ) ? true : false,
            'distance'  => !empty($settings['premium_preview_image_distance']) ? $settings['premium_preview_image_distance'] : 6,
            'maxWidth'  => !empty($settings['premium_preview_image_max_width']) ? $settings['premium_preview_image_max_width'] : 'null',
            'minWidth'  =>  !empty($settings['premium_preview_image_min_width']) ? $settings['premium_preview_image_min_width'] : $size[1],
            'maxWidthTabs'  => !empty($settings['premium_preview_image_max_width_tablet']) ? $settings['premium_preview_image_max_width_tablet'] : 'null',
            'minWidthTabs'  =>  !empty($settings['premium_preview_image_min_width_tablet']) ? $settings['premium_preview_image_min_width_tablet'] : $size[1],
            'maxWidthMobs'  => !empty($settings['premium_preview_image_max_width_mobile']) ? $settings['premium_preview_image_max_width_mobile'] : 'null',
            'minWidthMobs'  =>  !empty($settings['premium_preview_image_min_width_mobile']) ? $settings['premium_preview_image_min_width_mobile'] : $size[1],
            'side'      => !empty($settings['premium_preview_image_side']) ? $settings['premium_preview_image_side'] : array('right', 'left'),
            'container' => $tooltip_container,
            'hideMobiles'=> ($settings['premium_preview_image_hide'] == true) ? true : false,
            'id'        => $this->get_id(),
        ];
        
        if($settings['premium_preview_image_title_switcher'] == 'yes' && !empty($settings['premium_preview_image_title'] ) ){
            $title = '<' . $settings['premium_image_preview_title_heading'] . ' class="premium-previmg-tooltip-title">' . $settings['premium_preview_image_title'] . '</'. $settings['premium_image_preview_title_heading'] . '>';
        }
        
  ?>		

    <div id="premium-preview-image-main-<?php echo esc_attr($this->get_id());?>" class="premium-preview-image-wrap" data-settings='<?php echo wp_json_encode($prev_img_settings); ?>'>
        
        <div class="premium-preview-image-trig-img-wrap">
            
            <div class="premium-preview-image-inner-trig-img" data-tooltip-content="#tooltip_content">
                
                <?php if($settings['premium_preview_image_link_switcher'] == 'yes') : ?>
                    <a class = "premium-preview-img-link" <?php if(!empty($preview_img_url)) : ?> href="<?php echo esc_url($preview_img_url); ?>"<?php endif;?><?php if(!empty($settings['premium_preview_image_link']['is_external'])) : ?> target="_blank"<?php endif; ?><?php if(!empty($settings['premium_preview_image_link']['nofollow'])) : ?> rel="nofollow" <?php endif; ?>>
                <?php endif; ?>
                        <figure class="premium-preview-image-figure">
                            <img <?php echo $this->get_render_attribute_string('premium_preview_img_trigger'); ?>>
                            <?php if(!empty($settings['premium_preview_image_caption'])) : ?>
                                <figcaption <?php echo $this->get_render_attribute_string('premium_preview_image_caption'); ?>><?php echo esc_html($settings['premium_preview_image_caption']); ?></figcaption>
                            <?php endif; ?>
                        </figure>
                        <?php if($settings['premium_preview_image_link_switcher'] == 'yes') : ?></a>
                    <?php endif; ?>
                
                <div id="tooltip_content" class="premium-prev-img-tooltip-wrap premium-prev-img-tooltip-wrap-<?php echo esc_attr($this->get_id()); ?>">
                    <?php if($settings['premium_preview_image_content_selection'] == 'custom') : ?>
                        <?php if($settings['premium_preview_image_img_switcher'] == 'yes') : ?>
                            <div class="premium-prev-img-tooltip-img-wrap-<?php echo esc_attr($this->get_id()); ?>">
                                <img class="premium-preview-image-tooltips-img" alt="<?php echo esc_attr($settings['premium_preview_image_tooltips_image_alt']); ?>" src="<?php echo esc_url($tool_tips_image_url);?>">
                            </div>
                        <?php endif; ?>

                        <?php if($settings['premium_preview_image_title_switcher'] == 'yes' && !empty($settings['premium_preview_image_title'])) : ?>
                            <div class="premium-prev-img-tooltip-title-wrap premium-prev-img-tooltip-title-wrap-<?php echo esc_attr($this->get_id()); ?>">
                                <?php  echo $title; ?>
                            </div>
                        <?php endif; ?>

                        <?php if($settings['premium_preview_image_desc_switcher'] == 'yes' && !empty($settings['premium_preview_image_desc'])) : ?>
                            <div class="premium-prev-img-tooltip-desc-wrap premium-prev-img-tooltip-desc-wrap-<?php echo esc_attr($this->get_id()); ?>">
                                <?php echo $settings['premium_preview_image_desc']; ?>
                            </div>
                        <?php endif; ?>
                    <?php else:
                        echo $premium_elements_frontend->get_builder_content($elementor_post_id, true); ?>
                    <?php endif; ?>
                </div>
                
            </div>
            
        </div>
        
    </div>

	<?php 
   }
}