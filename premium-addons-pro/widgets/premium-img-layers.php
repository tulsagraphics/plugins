<?php

/**
 * Class: Premium_Image_Layers_Widget
 * Name: Image Layers
 * Slug: premium-img-layers-addon
 */

namespace Elementor;

if(!defined('ABSPATH')) exit;

class Premium_Img_Layers extends Widget_Base {
    
    private $templateInstance; 
    
    public function getTemplateInstance() {
        return $this->templateInstance = premium_Template_Tags::getInstance();
    }
    
    public function get_name() {
        return 'premium-img-layers-addon';
    }

    public function get_title() {
        return \PremiumAddons\Helper_Functions::get_prefix() . ' Image Layers';
    }

    public function get_icon() {
        return 'pa-pro-image-layers';
    }

    public function get_categories() {
        return ['premium-elements'];
    }

    public function get_script_depends() {
        return [
            'parallaxmouse-js',
            'tweenmax-js',
            'tilt-js',
            'waypoints',
            'premium-pro-js'
        ];
    }

    public function is_reload_preview_required() {
        return true;
    }
    
    // Adding the controls fields for the Image layers
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls() {
        
        $this->start_controls_section('premium_img_layers_content',
            [
                'label'         => esc_html__('Images','premium-addons-for-elementor'),
            ]
        );
        
        $repeater = new Repeater();
        
        $repeater->add_control('premium_img_layers_image',
           [
                'label'         => esc_html__( 'Upload Image', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::MEDIA,
                'default'       => [
                    'url'	=> Utils::get_placeholder_image_src(),
                ],
            ]
        );
        
        $repeater->add_control('premium_img_layers_alt',
            [
                'label'         => esc_html__( 'Alt', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::TEXT,
                'dynamic'       => [ 'active' => true ],
                'placeholder'   => 'Premium Image Layers',
                'label_block'   => true,
            ]
        );
        
        $repeater->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'          => 'thumbnail',
				'default'       => 'full',
			]
		);

        $repeater->add_control('premium_img_layers_position',
            [
                'label'         => esc_html__('Position', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'relative'      => esc_html__('Relative','premium-addons-for-elementor'),
                    'absolute'      => esc_html__('Absolute','premium-addons-for-elementor'),
                ],
                'default'       => 'relative'
            ]
        );
        
        $repeater->add_responsive_control('premium_img_layers_hor_position',
            [
                'label'         => esc_html__('Horizontal Offset','premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'description'   => esc_html__('Mousemove Interactivity works only with pixels', 'premium-addons-for-elementor'),
                'size_units'    => ['px', "em", '%'],
                'range'         => [
                    'px'    => [
                        'min'   => -200, 
                        'max'   => 300,
                    ],
                    'em'    => [
                        'min'   => -20, 
                        'max'   => 30,
                    ],
                    '%'    => [
                        'min'   => -50, 
                        'max'   => 100,
                    ],
                ],
                'default'       => [
                    'size'  => 50,
                    'unit'  => '%',
                ],
                'selectors'     => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}.absolute' => 'left: {{SIZE}}{{UNIT}};'
                ],
                'condition'     => [
                    'premium_img_layers_position'   => 'absolute'
                ]
            ]
        );
        
        $repeater->add_responsive_control('premium_img_layers_ver_position',
            [
                'label'         => esc_html__('Vertical Offset','premium-addons-for-elementor'),
                'description'   => esc_html__('Mousemove Interactivity works only with pixels', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', "em", '%'],
                'range'         => [
                    'px'    => [
                        'min'   => -200, 
                        'max'   => 300,
                    ],
                    'em'    => [
                        'min'   => -20, 
                        'max'   => 30,
                    ],
                    '%'    => [
                        'min'   => -50, 
                        'max'   => 100,
                    ],
                ],
                'default'       => [
                    'size'  => 50,
                    'unit'  => '%',
                ],
                'selectors'     => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}.absolute' => 'top: {{SIZE}}{{UNIT}};'
                ],
                'condition'     => [
                    'premium_img_layers_position'   => 'absolute'
                ],
                'separator'     => 'after',
            ]
        );

        $repeater->add_control('premium_img_layers_link_switcher',
            [
                'label'         => esc_html__('Link','premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
        );
        
        $repeater->add_control('premium_img_layers_link_selection', 
            [
                'label'         => esc_html__('Link Type', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'url'   => esc_html__('URL', 'premium-addons-for-elementor'),
                    'link'  => esc_html__('Existing Page', 'premium-addons-for-elementor'),
                ],
                'default'       => 'url',
                'label_block'   => true,
                'condition'		=> [
                	'premium_img_layers_link_switcher'	=> 'yes'
            	]
        	]
        );
        
        $repeater->add_control('premium_img_layers_link',
            [
                'label'         => esc_html__('Link', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::URL,
                'default'       => [
                    'url'   => '#',
                ],
                'placeholder'   => 'https://premiumaddons.com/',
                'label_block'   => true,
                'separator'     => 'after',
                'condition'     => [
                	'premium_img_layers_link_switcher'	=> 'yes',
                    'premium_img_layers_link_selection' => 'url'
                ]
            ]
        );
        
        $repeater->add_control('premium_img_layers_existing_link',
            [
                'label'         => esc_html__('Existing Page', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT2,
                'options'       => $this->getTemplateInstance()->get_all_post(),
                'condition'     => [
                	'premium_img_layers_link_switcher'	=> 'yes',
                    'premium_img_layers_link_selection' => 'link',
                ],
                'multiple'      => false,
                'separator'     => 'after',
                'label_block'   => true,
            ]
        );

        $repeater->add_control('premium_img_layers_rotate',
            [
                'label'         => esc_html__('Rotate', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
        );

        $repeater->add_control('premium_img_layers_rotatex',
            [
                'label'         => esc_html__('Degrees', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::NUMBER,
                'description'   => esc_html__('Set rotation value in degress', 'premium-addons-for-elementor'),
                'min'           => -180,
                'max'           => 180,
                'condition'     => [
                    'premium_img_layers_rotate'   => 'yes'
                ],
                'separator'     => 'after',
                'selectors'     => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => '-webkit-transform: rotate({{VALUE}}deg); -moz-transform: rotate({{VALUE}}deg); -o-transform: rotate({{VALUE}}deg); transform: rotate({{VALUE}}deg);'
                ],
            ]
        );
        
        $repeater->add_control('premium_img_layers_animation_switcher',
            [
                'label'        => esc_html__('Animation','premium-addons-for-elementor'),
                'type'         => Controls_Manager::SWITCHER,
            ]
        );
        
        $repeater->add_control('premium_img_layers_animation',
			[
				'label'         => esc_html__( 'Entrance Animation', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::ANIMATION,
				'default'       => '',
				'label_block'   => false,
                'frontend_available' => true,
                'condition'     => [
                    'premium_img_layers_animation_switcher' => 'yes'
                ],
				'frontend_available' => true,
			]
		);
        
        $repeater->add_control('premium_img_layers_animation_duration',
			[
				'label'         => __( 'Animation Duration', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::SELECT,
				'default'       => '',
				'options'       => [
					'slow' => __( 'Slow', 'premium-addons-for-elementor' ),
					''     => __( 'Normal', 'premium-addons-for-elementor' ),
					'fast' => __( 'Fast', 'premium-addons-for-elementor' ),
				],
                'condition'     => [
                    'premium_img_layers_animation_switcher' => 'yes',
					'premium_img_layers_animation!'    => '',
				],
			]
		);
        
        $repeater->add_control('premium_img_layers_animation_delay',
			[
				'label'         => esc_html__( 'Animation Delay', 'premium-addons-for-elementor' ) . ' (s)',
				'type'          => Controls_Manager::NUMBER,
				'default'       => '',
				'min'           => 0,
				'step'          => 0.1,
				'condition'     => [
                    'premium_img_layers_animation_switcher' => 'yes',
					'premium_img_layers_animation!'    => '',
				],
				'frontend_available' => true,
                'selectors'     => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}.animated' => '-webkit-animation-delay:{{VALUE}}s; -moz-animation-delay: {{VALUE}}s; -o-animation-delay: {{VALUE}}s; animation-delay: {{VALUE}}s;'
                ]
			]
		);
        
        $repeater->add_control('premium_img_layers_mouse',
            [
                'label'         => esc_html__('Mousemove Interactivity', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('Enable or disable mousemove interaction','   premium-addons-for-elementor'),
            ]
        );
        
        $repeater->add_control('premium_img_layers_mouse_type', 
            [
                'label'         => esc_html__('Interactivity Style', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'parallax'      => esc_html__('Mouse Parallax', 'premium-addons-for-elementor'),
                    'tilt'          => esc_html__('Tilt', 'premium-addons-for-elementor'),
                ],
                'default'       => 'parallax',
                'label_block'   => true,
                'condition'		=> [
                	'premium_img_layers_mouse'	=> 'yes'
            	]
        	]
        );
        
        $repeater->add_control('premium_img_layers_rate',
            [
                'label'         => esc_html__('Rate', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => -10,
                'min'           => -20,
                'max'           => 20,
                'step'          => 1,
                'description'   => esc_html__('Choose the movement rate for the layer image, default: -10','premium-addons-for-elementor'),
                'separator'     => 'after',
                'label_block'   => true,
                'condition'     => [
                    'premium_img_layers_mouse' => 'yes',
                            'premium_img_layers_mouse_type' => 'parallax'
                    ]
                ]
            );
        
        $repeater->add_control('premium_img_layers_zindex',
            [
                'label'         => esc_html__('z-index','premium-addons-for-elementor'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => 1,
                'selectors'     => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}.premium-img-layers-list-item' => 'z-index: {{VALUE}};'
                    ],
                ]
            );
        
        $repeater->add_control('premium_img_layers_class', 
            [
                'label'         => esc_html__('CSS Classes','premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'description'   => esc_html__('Separate class with spaces','premium-addons-for-elementor'),
            ]
        );
        
        $this->add_control('premium_img_layers_images_repeater',
			[
				'type'          => Controls_Manager::REPEATER,
				'fields'        => array_values( $repeater->get_controls() ),
                'title_field'   => '{{{ premium_img_layers_alt }}}',
			]
		);
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_img_layers_images_style',
            [
                'label'         => esc_html__('Image', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
            );
        
        $this->add_control('premium_img_layers_images_background',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-img-layers-list-item .premium-img-layers-image'  => 'background-color: {{VALUE}};',
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_img_layers_images_border',
                'selector'      => '{{WRAPPER}} .premium-img-layers-list-item .premium-img-layers-image'
            ]
        );
        
        $this->add_responsive_control('premium_img_layers_images_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-img-layers-list-item .premium-img-layers-image' => 'border-top-left-radius: {{TOP}}{{UNIT}}; border-top-right-radius: {{RIGHT}}{{UNIT}}; border-bottom-right-radius: {{BOTTOM}}{{UNIT}}; border-bottom-left-radius: {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'premium_img_layers_images_shadow',
                'selector'      => '{{WRAPPER}} .premium-img-layers-list-item .premium-img-layers-image'
            ]
        );
        
        $this->add_responsive_control('premium_img_layers_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-img-layers-list-item .premium-img-layers-image' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control('premium_img_layers_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-img-layers-list-item .premium-img-layers-image' => 'padding:  {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_img_layers_container_style',
            [
                'label'         => esc_html__('Container', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
            ]
        );
        
        $this->add_responsive_control('premium_img_layers_height',
            [
                'label'         => esc_html__('Height','premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', "em"],
                'range'         => [
                    'px'    => [
                        'min'   => 1, 
                        'max'   => 800,
                    ],
                    'em'    => [
                        'min'   => 1, 
                        'max'   => 80,
                    ],
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-img-layers-wrapper' => 'min-height: {{SIZE}}{{UNIT}};'
                ],
            ]
        );
        
        $this->add_control('premium_img_layers_container_background',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'selectors'     => [
                    '{{WRAPPER}} .premium-img-layers-wrapper'  => 'background-color: {{VALUE}};',
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_img_layers_container_border',
                'selector'      => '{{WRAPPER}} .premium-img-layers-wrapper'
            ]
        );
        
        $this->add_responsive_control('premium_img_layers_container_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-img-layers-wrapper' => 'border-top-left-radius: {{TOP}}{{UNIT}}; border-top-right-radius: {{RIGHT}}{{UNIT}}; border-bottom-right-radius: {{BOTTOM}}{{UNIT}}; border-bottom-left-radius: {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'premium_img_layers_container_shadow',
                'selector'      => '{{WRAPPER}} .premium-img-layers-wrapper'
            ]
        );
        
        $this->add_responsive_control('premium_img_layers_container_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-img-layers-wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->add_responsive_control('premium_img_layers_container_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-img-layers-wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $this->end_controls_section();
        
    }
    
    protected function render(){

        $settings = $this->get_settings_for_display();
        
    ?>

<div id="premium-img-layers-wrapper" class="premium-img-layers-wrapper">
    <ul class="premium-img-layers-list-wrapper">
        <?php $animation_arr = array();
        foreach( $settings['premium_img_layers_images_repeater'] as $index => $image ) :
            array_push( $animation_arr, $image['premium_img_layers_animation_switcher'] );
            if( 'yes' == $animation_arr[$index] ) {

                $animation_class = $image['premium_img_layers_animation'];

                if( '' != $image['premium_img_layers_animation_duration'] ) {

                    $animation_dur = 'animated-' . $image['premium_img_layers_animation_duration'];

                } else {
                    $animation_dur = 'animated-';
                }
            } else {

                    $animation_class = '';

                    $animation_dur = '';

            }

            $list_item_key = 'premium_img_layer_' . $index;

            $this->add_render_attribute( $list_item_key, 'class',
                [
                    'premium-img-layers-list-item',
                    $image['premium_img_layers_position'],
                    esc_attr($image['premium_img_layers_class']),
                    'elementor-repeater-item-' . $image['_id']
                ]
            );

            $this->add_render_attribute( $list_item_key, 'data-layer-animation',
                [
                    $animation_class,
                    $animation_dur,
                ]
            );

            if( 'url' == $image['premium_img_layers_link_selection'] ){

                $image_url = $image['premium_img_layers_link']['url'];

            } else {

                $image_url = get_permalink($image['premium_img_layers_existing_link']);

            }
        ?>

        <li <?php echo $this->get_render_attribute_string( $list_item_key ); ?> <?php if( 'yes' == $image['premium_img_layers_mouse'] ) : echo 'data-' . $image['premium_img_layers_mouse_type'] . '="true"'; ?> data-rate="<?php echo esc_attr( ! empty( $image['premium_img_layers_rate'] ) ? $image['premium_img_layers_rate'] : -10 ); ?>"<?php endif; ?>>
                <?php
                    $image_src = $image['premium_img_layers_image'];

                    $image_src_size = Group_Control_Image_Size::get_attachment_image_src( $image_src['id'], 'thumbnail', $image );

                    if( empty( $image_src_size ) ) : $image_src_size = $image_src['url']; else: $image_src_size = $image_src_size; endif;
                ?>

                <?php if( 'yes' == $image['premium_img_layers_link_switcher'] ) : ?>

                    <a class="premium-img-layers-link" <?php if( !empty( $image_url ) ) : ?>href="<?php echo esc_url( $image_url ); ?>"<?php endif;?><?php if( ! empty( $image['premium_img_layers_link']['is_external'] ) ) : ?> target="_blank" <?php endif; ?><?php if( ! empty( $image['premium_img_layers_link']['nofollow'] ) ) : ?> rel="nofollow"<?php endif; ?>>

                <?php endif; ?>
                        <img src="<?php echo $image_src_size; ?>" class="premium-img-layers-image" alt="<?php echo esc_attr($image['premium_img_layers_alt']); ?>">

                <?php if($image['premium_img_layers_link_switcher'] == 'yes') : ?>

                    </a>

                <?php endif; ?>

            </li>
        <?php endforeach; ?>
    </ul>
</div>

<?php }

    protected function _content_template() {
        ?>
        
        <div id="premium-img-layers-wrapper" class="premium-img-layers-wrapper">
            <ul class="premium-img-layers-list-wrapper">
                
            <# var animationClass, animationDur, listItemKey, imageUrl, animationArr = [];
            
            _.each( settings.premium_img_layers_images_repeater, function( image, index ){
            
                animationArr.push( image.premium_img_layers_animation_switcher );
                
                if( 'yes' == animationArr[index] ) {

                    animationClass = image.premium_img_layers_animation;

                    if( '' != image.premium_img_layers_animation_duration ) {

                        animationDur = 'animated-' + image.premium_img_layers_animation_duration;

                    } else {
                        animationDur = 'animated-';
                    }
                } else {

                        animationClass = '';

                        animationDur = '';

                }
                
                listItemKey = 'premium_img_layer_' + index;

                view.addRenderAttribute( listItemKey, 'class',
                    [
                        'premium-img-layers-list-item',
                        image.premium_img_layers_position,
                        image.premium_img_layers_class,
                        'elementor-repeater-item-' + image._id
                    ]
                );

                view.addRenderAttribute( listItemKey, 'data-layer-animation',
                    [
                        animationClass,
                        animationDur,
                    ]
                );
                
                if( 'yes' == image.premium_img_layers_mouse ) {
                
                    var rate = '' != image.premium_img_layers_rate ? image.premium_img_layers_rate : -10;
                    
                    view.addRenderAttribute( listItemKey, 'data-' + image.premium_img_layers_mouse_type , 'true' );
                    
                    view.addRenderAttribute( listItemKey, 'data-rate', rate );
                
                }
                
                
                if( 'url' == image.premium_img_layers_link_selection ) {

                    imageUrl = image.premium_img_layers_link.url;

                } else {

                    imageUrl = image.premium_img_layers_existing_link;

                } 
                
                var imageObj = {
                    id: image.premium_img_layers_image.id,
                    url: image.premium_img_layers_image.url,
                    size: image.thumbnail_size,
                    dimension: image.thumbnail_custom_dimension,
                    model: view.getEditModel()
                },
                
                image_url = elementor.imagesManager.getImageUrl( imageObj );
                
                #>
                
                <li {{{ view.getRenderAttributeString(listItemKey) }}}>
                    
                    <# if( 'yes' == image.premium_img_layers_link_switcher ) { #>

                    <a class="premium-img-layers-link" href="{{ imageUrl }}">

                    <# } #>
                        <img src="{{ image_url }}" class="premium-img-layers-image" alt="{{ image.premium_img_layers_alt }}">

                    <# if( 'yes' == image.premium_img_layers_link_switcher ) { #>

                    </a>

                    <# } #>
                </li>
                
            <# } );
                
            #>
                
            </ul>
        </div>

        <?php
    }

}