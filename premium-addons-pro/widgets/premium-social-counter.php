<?php 

namespace Elementor;

if(!defined('ABSPATH')) exit;

class Premium_Social_Counter_Widget extends Widget_Base {
    
    public function get_name(){
        return 'premium-addon-social-counter';
    }
    
    public function get_title() {
		return \PremiumAddons\Helper_Functions::get_prefix() . ' Social Counter';
	}
    
    public function get_icon(){
        return 'pa-pro-social-counter';
    }
    
    public function is_reload_preview_required(){
        return true;
    }
    
    public function get_categories(){
        return ['premium-elements'];
    }
    
    // Adding the controls fields for the premium social counters
    // This will controls the animation, colors and background, dimensions etc
    protected function _register_controls(){
        
        $this->start_controls_section('premium_social_counter_general',
            [
                'label'     => esc_html__('Social Counter', 'premium-addons-for-elementor'),
            ]);
        
        $this->add_control('premium_social_counter_twitter',
            [
                'label'         => esc_html__('Twitter', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
            );
        
        $this->add_control('premium_social_counter_google',
            [
                'label'         => esc_html__('Google Plus', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
            );
        
        $this->add_control('premium_social_counter_youtube',
            [
                'label'         => esc_html__('YouTube', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
            );

        $this->add_control('premium_social_counter_facebook',
            [
                'label'         => esc_html__('Facebook', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
            );

        $this->add_control('premium_social_counter_insta',
            [
                'label'         => esc_html__('Instagram', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
            );
        
        $this->add_control('premium_social_counter_behance',
            [
                'label'         => esc_html__('Behance', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
            );
        
        $this->add_control('premium_social_counter_vimeo',
            [
                'label'         => esc_html__('Vimeo', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
            );

        $this->add_control('premium_social_counter_pin',
            [
                'label'         => esc_html__('Pinterest', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
            );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_social_counter_twitter_tab',
            [
                'label'     => esc_html__('Twitter', 'premium-addons-for-elementor'),
                'condition'     => [
                    'premium_social_counter_twitter'    => 'yes'
                ]
            ]);
        
        /*Consumer Key*/
        $this->add_control('premium_social_counter_twitter_consumer_key',
                [
                    'label'         => esc_html__('Consumer Key', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'default'       => 'AgV213XdiJzwCvrdaDRsxnwti',
                    'description'   => '<a href="https://apps.twitter.com/" target="_blank">Get Consumer Key </a>by creating a new app or selecting an existing app ',
                    ]
                );
                
        /*Consumer Secret Key*/
        $this->add_control('premium_social_counter_twitter_consumer_secret',
                [
                    'label'         => esc_html__('Consumer Secret Key', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'default'       => 'qRfkwcdL4y9l18WFd0sDIEYUC34iJGmCKUzniS6YomO3crBOkU',
                    'description'   => '<a href="https://apps.twitter.com/" target="_blank">Get Consumer Secret Key </a>by creating a new app or selecting an existing app',
                    ]
                );

        /*Access Token*/
        $this->add_control('premium_social_counter_twitter_access',
                [
                    'label'         => esc_html__('Access Token', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'default'       => '776918558542561280-JbPcfDYJZGzisaOzwYjjQ6Ue96Zvl5m',
                    'description'   => '<a href="https://apps.twitter.com/" target="_blank">Get Access Token </a>by creating a new app or selecting an existing app ',
                    ]
                );
                
        /*Access Token Secret*/
        $this->add_control('premium_social_counter_twitter_access_secret',
                [
                    'label'         => esc_html__('Access Token Secret', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'default'       => 'm95AwoiWmAATOFliFMQoNPTUDzJ7C1LhZYygJxWivY07t',
                    'description'   => '<a href="https://apps.twitter.com/" target="_blank">Get Access Token Secret </a>by creating a new app or selecting an existing app',
                    ]
                );
        
        /*Username*/
        $this->add_control('premium_social_counter_twitter_username',
                [
                    'label'         => esc_html__('Username', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'default'       => '@leap13themes',
                    'description'   => 'Username is prefixed by @',
                    ]
                );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_social_counter_google_section',
            [
                'label'     => esc_html__('Google Plus', 'premium-addons-for-elementor'),
                'condition'     => [
                    'premium_social_counter_google'    => 'yes'
                ]
            ]);
        
        /*API Key*/
        $this->add_control('premium_social_counter_google_api',
                [
                    'label'         => esc_html__('API Key', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'default'       => '',
                    'description'   => '<a href="https://console.developers.google.com/project" target="_blank">Get API Key </a>by creating a new app or selecting an existing app ',
                    ]
                );
                
        /*Page Name/Profile ID*/
        $this->add_control('premium_social_counter_google_username',
                [
                    'label'         => esc_html__('Page Name/Profile ID', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'default'       => '',
                    ]
                );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_social_counter_youtube_section',
            [
                'label'     => esc_html__('Youtube', 'premium-addons-for-elementor'),
                'condition'     => [
                    'premium_social_counter_youtube'    => 'yes'
                ]
            ]);
        
        /*API Key*/
        $this->add_control('premium_social_counter_youtube_api',
                [
                    'label'         => esc_html__('API Key', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'default'       => 'AIzaSyAHJUveoE7OiAJlyFT2K8C59jJFroS5VWc',
                    'description'   => '<a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank">Get API Key </a>by creating a new app or selecting an existing app ',
                    ]
                );
                
        /*Page Name/Profile ID*/
        $this->add_control('premium_social_counter_youtube_channel',
                [
                    'label'         => esc_html__('Channel ID', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'default'       => 'UCXcJ9BeO2sKKHor7Q9VglTQ',
                    'label_block'   => false,
                    'description'   => '<a href="https://support.google.com/youtube/answer/3250431?hl=en-GB" target="_blank">Get Channel ID</a> from here',
                    ]
                );
        
        $this->add_control('premium_social_counter_youtube_name',
                [
                    'label'         => esc_html__('Channel Name', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'default'       => 'Leap13',
                    'label_block'   => false,
                    ]
                );
        
        $this->end_controls_section();

        $this->start_controls_section('premium_social_counter_facebook_section',
            [
                'label'     => esc_html__('Facebook', 'premium-addons-for-elementor'),
                'condition'     => [
                    'premium_social_counter_facebook'    => 'yes'
                ]
            ]);
        
        $this->add_control('premium_social_counter_facebook_access',
                [
                    'label'         => esc_html__('Access Token', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'description'   => '<a href="https://developers.facebook.com/" target="_blank">Get App secret </a>by creating a new app or selecting an existing app ',
                    ]
                );
                
        /*Page Name/Profile ID*/
        $this->add_control('premium_social_counter_facebook_page_id',
                [
                    'label'         => esc_html__('Page ID', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'description'   => 'Click <a href="https://www.facebook.com/help/community/question/?id=378910098941520" target="_blank">Here </a>to get your Facebook Page ID',
                    ]
                );

        $this->end_controls_section();

        $this->start_controls_section('premium_social_counter_insta_tab',
            [
                'label'     => esc_html__('Instagram', 'premium-addons-for-elementor'),
                'condition'     => [
                    'premium_social_counter_insta'    => 'yes'
                ]
            ]);
        
        /*Access Token*/
        $this->add_control('premium_social_counter_instagram_access',
                [
                    'label'         => esc_html__('Access Token', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'default'       => '2075884021.1677ed0.2fd28d5d3abf45d4a80534bee8376f4c',
                    'description'   => '<a href="http://instagram.pixelunion.net/" target="_blank">Get Access Token</a> by creating a new app or selecting an existing app ',
                    ]
                );
        
        /*Username*/
        $this->add_control('premium_social_counter_instagram_username',
                [
                    'label'         => esc_html__('Username', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'default'       => 'Leap13themes',
                    ]
                );
        
        $this->end_controls_section();

        $this->start_controls_section('premium_social_counter_behance_section',
            [
                'label'     => esc_html__('Behance', 'premium-addons-for-elementor'),
                'condition'     => [
                    'premium_social_counter_behance'    => 'yes'
                ]
            ]);
        
        /*API Key*/
        $this->add_control('premium_social_counter_behance_api',
                [
                    'label'         => esc_html__('API Key', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'default'       => 'Zb0XWEqAvBua4lmCbfb6derptH2tp4cc',
                    'description'   => '<a href="https://www.behance.net/dev" target="_blank">Get API Key </a>by creating a new app or selecting an existing app ',
                    ]
                );
                
        /*Page Name/Profile ID*/
        $this->add_control('premium_social_counter_behance_username',
                [
                    'label'         => esc_html__('Profile ID', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    'default'       => 'rimounadel',
                    ]
                );
        
        $this->end_controls_section();
        
        $this->start_controls_section('premium_social_counter_vimeo_section',
            [
                'label'     => esc_html__('Vimeo', 'premium-addons-for-elementor'),
                'condition'     => [
                    'premium_social_counter_vimeo'    => 'yes'
                ]
            ]);
        
        /*Channel Name*/
        $this->add_control('premium_social_counter_vimeo_channel',
                [
                    'label'         => esc_html__('Channel ID', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    ]
                );
        
        $this->end_controls_section();

        $this->start_controls_section('premium_social_counter_pin_section',
            [
                'label'     => esc_html__('Pinterest', 'premium-addons-for-elementor'),
                'condition'     => [
                    'premium_social_counter_pin'    => 'yes'
                ]
            ]);
        
        /*Blog Hostname*/
        $this->add_control('premium_social_counter_pin_id',
                [
                    'label'         => esc_html__('Profile ID', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::TEXT,
                    'label_block'   => false,
                    ]
                );

        $this->end_controls_section();
        
        $this->start_controls_section('premium_social_counter_display_section',
            [
                'label'     => esc_html__('Display Options', 'premium-addons-for-elementor'),
            ]);
        
        $this->add_control('premium_social_counter_username',
            [
                'label'         => esc_html__('Show Username', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'		=> 'yes'
            ]
            );
        
        $this->add_control('premium_social_counter_label',
            [
                'label'         => esc_html__('Show Label', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'		=> 'yes'
            ]
            );
        
        $this->add_control('premium_social_counter_format',
            [
                'label'         => esc_html__('Format Number', 'premium-addons-for-elementor'),
                'description'   => esc_html__('Shortens numbers with suffix \'K\',\'M\'', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'default'		=> 'yes'
            ]
            );
        
        $this->add_control('premium_social_counter_new_tab',
            [
                'label'         => esc_html__('Open Links in new tab', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
            ]
            );
        
        $this->add_control('premium_social_counter_display',
                [
                    'label'         => esc_html__( 'Display', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::SELECT,
                    'options'       => [
                        'inline'        => esc_html__('Inline', 'premium-addons-for-elementor'),
                        'block'         => esc_html__('Block', 'premium-addons-for-elementor'),
                        ],
                    'default'       => 'block',
                    ]
                );
        
        $this->add_responsive_control('premium_social_counter_width',
            [
                'label'         => esc_html__('Width','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em' , '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-count-ul.inline .premium-social-list-item' => 'width: {{SIZE}}{{UNIT}};'
                    ],
                'condition'     => [
                    'premium_social_counter_display'    => 'inline'
                    ]
                ]
            );
        
        $this->add_responsive_control('premium_social_counter_height',
            [
                'label'         => esc_html__('Height','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em' ],
                'range'         => [
                    'px'    => [
                        'min'   => 1,
                        'max'   => 400
                    ],
                    'em'    => [
                        'min'   => 1,
                        'max'   => 100
                    ]
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-count-ul.inline .premium-social-list-item' => 'height: {{SIZE}}{{UNIT}};'
                    ],
                'condition'     => [
                    'premium_social_counter_display'    => 'inline'
                    ]
                ]
            );

        $this->add_responsive_control('premium_social_counter_content_align',
                [
                    'label'         => esc_html__( 'Content Alignment', 'premium-addons-for-elementor' ),
                    'type'          => Controls_Manager::CHOOSE,
                    'options'       => [
                        'left'      => [
                            'title'=> esc_html__( 'Left', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-left',
                            ],
                        'center'    => [
                            'title'=> esc_html__( 'Center', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-center',
                            ],
                        'right'     => [
                            'title'=> esc_html__( 'Right', 'premium-addons-for-elementor' ),
                            'icon' => 'fa fa-align-right',
                            ],
                        ],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-ul .premium-social-list-item' => 'text-align: {{VALUE}};',
                        ],
                    'default'       => 'center',
                    ]
                );

        $this->end_controls_section();

        $this->start_controls_section('premium_social_twitter_style',
            [
                'label'         => esc_html__('Twitter', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_social_counter_twitter'    => 'yes'
                ]
            ]
        );

        $this->start_controls_tabs('premium_social_twitter_style_tabs');
        
        $this->start_controls_tab('premium_social_twitter_icon_style',
            [
                'label'             => esc_html__('Icon', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_twitter_icon_color',
            [
                'label'         =>esc_html__('Color','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-twitter-icon' => 'color:{{VALUE}};'
                ],
            ]
        );

        $this->add_responsive_control('premium_social_twitter_icon_size',
            [
                'label'         => esc_html__('Size','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::SLIDER,
                'size_units'    => ['px', "em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-twitter-icon' => 'font-size: {{SIZE}}{{UNIT}};'
                    ],
                ]
            );
        
        $this->add_control('premium_social_twitter_icon_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-twitter-icon'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Icon Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_twitter_icon_border',
                    'selector'      => '{{WRAPPER}} .premium-social-twitter-icon',
                ]
                );
        
        /*Icon Border Radius*/
        $this->add_control('premium_social_twitter_icon_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-twitter-icon' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_twitter_icon_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-twitter-icon',
                    ]
                );
        
        $this->add_responsive_control('premium_social_twitter_icon_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-twitter-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_responsive_control('premium_social_twitter_icon_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-twitter-icon' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_twitter_text_style',
            [
                'label'             => esc_html__('Text', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_twitter_text_color',
                [
                    'label'         => esc_html__('Color','premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'scheme'        => [
                        'type'          => Scheme_Color::get_type(),
                        'value'         => Scheme_Color::COLOR_1
                    ],
                   'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-twitter'  => 'color:{{VALUE}};'
                        ]
                ]
        );
        
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_social_twitter_text_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-social-count-twitter',
                ]
        );
        
        $this->add_control('premium_social_twitter_text_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-count-twitter'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Title Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_twitter_text_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-twitter',
                ]
                );
        
        /*Title Border Radius*/
        $this->add_control('premium_social_twitter_text_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-twitter' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_twitter_text_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-twitter',
                    ]
                );
        
        /*Title Margin*/
        $this->add_responsive_control('premium_social_twitter_text_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-twitter' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Title Padding*/
        $this->add_responsive_control('premium_social_twitter_text_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-twitter' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_twitter_container_style',
            [
                'label'             => esc_html__('Container', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_social_twitter_container_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-social-count-twitter-li',
                    ]
                );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_twitter_container_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-twitter-li',
                ]
                );
        
        /*Box Border Radius*/
        $this->add_control('premium_social_twitter_container_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-twitter-li' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'label'         => esc_html__('Box Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_twitter_container_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-twitter-li',
                ]
                );

        /*Box Margin*/
        $this->add_responsive_control('premium_social_twitter_container_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-twitter-li' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Box Padding*/
        $this->add_responsive_control('premium_social_twitter_container_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-twitter-li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section('premium_social_google_style',
            [
                'label'         => esc_html__('Google Plus', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_social_counter_google'    => 'yes'
                ]
            ]
        );

        $this->start_controls_tabs('premium_social_google_style_tabs');
        
        $this->start_controls_tab('premium_social_google_icon_style',
            [
                'label'             => esc_html__('Icon', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_google_icon_color',
            [
                'label'         =>esc_html__('Color','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-google-icon' => 'color:{{VALUE}};'
                ],
            ]
        );

        $this->add_responsive_control('premium_social_google_icon_size',
            [
                'label'         => esc_html__('Size','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::SLIDER,
                'size_units'    => ['px', "em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-google-icon' => 'font-size: {{SIZE}}{{UNIT}};'
                    ],
                ]
            );
        
        $this->add_control('premium_social_google_icon_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-google-icon'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Icon Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_google_icon_border',
                    'selector'      => '{{WRAPPER}} .premium-social-google-icon',
                ]
                );
        
        /*Icon Border Radius*/
        $this->add_control('premium_social_google_icon_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-google-icon' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_google_icon_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-google-icon',
                    ]
                );
        
        $this->add_responsive_control('premium_social_google_icon_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-google-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_responsive_control('premium_social_google_icon_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-google-icon' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_google_text_style',
            [
                'label'             => esc_html__('Text', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_google_text_color',
                [
                    'label'         => esc_html__('Color','premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'scheme'        => [
                        'type'          => Scheme_Color::get_type(),
                        'value'         => Scheme_Color::COLOR_1
                    ],
                   'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-google'  => 'color:{{VALUE}};'
                        ]
                ]
        );
        
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_social_google_text_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-social-count-google',
                ]
        );
        
        $this->add_control('premium_social_google_text_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-count-google'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Title Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_google_text_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-google',
                ]
                );
        
        /*Title Border Radius*/
        $this->add_control('premium_social_google_text_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-google' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_google_text_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-google',
                    ]
                );
        
        /*Title Margin*/
        $this->add_responsive_control('premium_social_google_text_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-google' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Title Padding*/
        $this->add_responsive_control('premium_social_google_text_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-google' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_google_container_style',
            [
                'label'             => esc_html__('Container', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_social_google_container_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-social-count-google-li',
                    ]
                );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_google_container_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-google-li',
                ]
                );
        
        /*Box Border Radius*/
        $this->add_control('premium_social_google_container_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-google-li' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'label'         => esc_html__('Box Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_google_container_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-google-li',
                ]
                );

        /*Box Margin*/
        $this->add_responsive_control('premium_social_google_container_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-google-li' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Box Padding*/
        $this->add_responsive_control('premium_social_google_container_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-google-li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section('premium_social_youtube_style',
            [
                'label'         => esc_html__('Youtube', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_social_counter_youtube'    => 'yes'
                ]
            ]
        );

        $this->start_controls_tabs('premium_social_youtube_style_tabs');
        
        $this->start_controls_tab('premium_social_youtube_icon_style',
            [
                'label'             => esc_html__('Icon', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_youtube_icon_color',
            [
                'label'         =>esc_html__('Color','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-youtube-icon' => 'color:{{VALUE}};'
                ],
            ]
        );
        
        $this->add_responsive_control('premium_social_youtube_icon_size',
            [
                'label'         => esc_html__('Size','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::SLIDER,
                'size_units'    => ['px', "em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-youtube-icon' => 'font-size: {{SIZE}}{{UNIT}};'
                    ],
                ]
            );

        $this->add_control('premium_social_youtube_icon_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-youtube-icon'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Icon Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_youtube_icon_border',
                    'selector'      => '{{WRAPPER}} .premium-social-youtube-icon',
                ]
                );
        
        /*Icon Border Radius*/
        $this->add_control('premium_social_youtube_icon_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-youtube-icon' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_youtube_icon_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-youtube-icon',
                    ]
                );
        
        $this->add_responsive_control('premium_social_youtube_icon_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-youtube-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_responsive_control('premium_social_youtube_icon_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-youtube-icon' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_youtube_text_style',
            [
                'label'             => esc_html__('Text', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_youtube_text_color',
                [
                    'label'         => esc_html__('Color','premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'scheme'        => [
                        'type'          => Scheme_Color::get_type(),
                        'value'         => Scheme_Color::COLOR_1
                    ],
                   'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-youtube'  => 'color:{{VALUE}};'
                        ]
                ]
        );
        
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_social_youtube_text_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-social-count-youtube',
                ]
        );
        
        $this->add_control('premium_social_youtube_text_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-count-youtube'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Title Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_youtube_text_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-youtube',
                ]
                );
        
        /*Title Border Radius*/
        $this->add_control('premium_social_youtube_text_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-youtube' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_youtube_text_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-youtube',
                    ]
                );
        
        /*Title Margin*/
        $this->add_responsive_control('premium_social_youtube_text_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-youtube' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Title Padding*/
        $this->add_responsive_control('premium_social_youtube_text_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-youtube' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_youtube_container_style',
            [
                'label'             => esc_html__('Container', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_social_youtube_container_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-social-count-youtube-li',
                    ]
                );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_youtube_container_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-youtube-li',
                ]
                );
        
        /*Box Border Radius*/
        $this->add_control('premium_social_youtube_container_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-youtube-li' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'label'         => esc_html__('Box Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_youtube_container_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-youtube-li',
                ]
                );

        /*Box Margin*/
        $this->add_responsive_control('premium_social_youtube_container_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-youtube-li' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Box Padding*/
        $this->add_responsive_control('premium_social_youtube_container_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-youtube-li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section('premium_social_facebook_style',
            [
                'label'         => esc_html__('Facebook', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_social_counter_facebook'    => 'yes'
                ]
            ]
        );

        $this->start_controls_tabs('premium_social_facebook_style_tabs');
        
        $this->start_controls_tab('premium_social_facebook_icon_style',
            [
                'label'             => esc_html__('Icon', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_facebook_icon_color',
            [
                'label'         =>esc_html__('Color','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-facebook-icon' => 'color:{{VALUE}};'
                ],
            ]
        );

        $this->add_responsive_control('premium_social_facebook_icon_size',
            [
                'label'         => esc_html__('Size','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::SLIDER,
                'size_units'    => ['px', "em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-facebook-icon' => 'font-size: {{SIZE}}{{UNIT}};'
                    ],
                ]
            );
        
        $this->add_control('premium_social_facebook_icon_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-facebook-icon'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Icon Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_facebook_icon_border',
                    'selector'      => '{{WRAPPER}} .premium-social-facebook-icon',
                ]
                );
        
        /*Icon Border Radius*/
        $this->add_control('premium_social_facebook_icon_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-facebook-icon' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_facebook_icon_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-facebook-icon',
                    ]
                );
        
        $this->add_responsive_control('premium_social_facebook_icon_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-facebook-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_responsive_control('premium_social_facebook_icon_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-facebook-icon' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_facebook_text_style',
            [
                'label'             => esc_html__('Text', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_facebook_text_color',
                [
                    'label'         => esc_html__('Color','premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'scheme'        => [
                        'type'          => Scheme_Color::get_type(),
                        'value'         => Scheme_Color::COLOR_1
                    ],
                   'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-facebook'  => 'color:{{VALUE}};'
                        ]
                ]
        );
        
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_social_facebook_text_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-social-count-facebook',
                ]
        );
        
        $this->add_control('premium_social_facebook_text_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-count-facebook'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Title Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_facebook_text_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-facebook',
                ]
                );
        
        /*Title Border Radius*/
        $this->add_control('premium_social_facebook_text_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-facebook' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_facebook_text_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-facebook',
                    ]
                );
        
        /*Title Margin*/
        $this->add_responsive_control('premium_social_facebook_text_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-facebook' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Title Padding*/
        $this->add_responsive_control('premium_social_facebook_text_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-facebook' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_facebook_container_style',
            [
                'label'             => esc_html__('Container', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_social_facebook_container_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-social-count-facebook-li',
                    ]
                );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_facebook_container_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-facebook-li',
                ]
                );
        
        /*Box Border Radius*/
        $this->add_control('premium_social_facebook_container_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-facebook-li' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'label'         => esc_html__('Box Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_facebook_container_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-facebook-li',
                ]
                );

        /*Box Margin*/
        $this->add_responsive_control('premium_social_facebook_container_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-facebook-li' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Box Padding*/
        $this->add_responsive_control('premium_social_facebook_container_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-facebook-li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section('premium_social_insta_style',
            [
                'label'         => esc_html__('Instagram', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_social_counter_insta'    => 'yes'
                ]
            ]
        );

        $this->start_controls_tabs('premium_social_insta_style_tabs');
        
        $this->start_controls_tab('premium_social_insta_icon_style',
            [
                'label'             => esc_html__('Icon', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_insta_icon_color',
            [
                'label'         =>esc_html__('Color','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-insta-icon' => 'color:{{VALUE}};'
                ],
            ]
        );

        $this->add_responsive_control('premium_social_insta_icon_size',
            [
                'label'         => esc_html__('Size','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::SLIDER,
                'size_units'    => ['px', "em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-insta-icon' => 'font-size: {{SIZE}}{{UNIT}};'
                    ],
                ]
            );
        
        $this->add_control('premium_social_insta_icon_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-insta-icon'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Icon Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_insta_icon_border',
                    'selector'      => '{{WRAPPER}} .premium-social-insta-icon',
                ]
                );
        
        /*Icon Border Radius*/
        $this->add_control('premium_social_insta_icon_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-insta-icon' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_insta_icon_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-insta-icon',
                    ]
                );
        
        $this->add_responsive_control('premium_social_insta_icon_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-insta-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_responsive_control('premium_social_insta_icon_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-insta-icon' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_insta_text_style',
            [
                'label'             => esc_html__('Text', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_insta_text_color',
                [
                    'label'         => esc_html__('Color','premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'scheme'        => [
                        'type'          => Scheme_Color::get_type(),
                        'value'         => Scheme_Color::COLOR_1
                    ],
                   'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-insta'  => 'color:{{VALUE}};'
                        ]
                ]
        );
        
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_social_insta_text_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-social-count-insta',
                ]
        );
        
        $this->add_control('premium_social_insta_text_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-count-insta'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Title Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_insta_text_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-insta',
                ]
                );
        
        /*Title Border Radius*/
        $this->add_control('premium_social_insta_text_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-insta' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_insta_text_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-insta',
                    ]
                );
        
        /*Title Margin*/
        $this->add_responsive_control('premium_social_insta_text_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-insta' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Title Padding*/
        $this->add_responsive_control('premium_social_insta_text_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-insta' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_insta_container_style',
            [
                'label'             => esc_html__('Container', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_social_insta_container_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-social-count-insta-li',
                    ]
                );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_insta_container_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-insta-li',
                ]
                );
        
        /*Box Border Radius*/
        $this->add_control('premium_social_insta_container_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-insta-li' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'label'         => esc_html__('Box Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_insta_container_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-insta-li',
                ]
                );

        /*Box Margin*/
        $this->add_responsive_control('premium_social_insta_container_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-insta-li' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Box Padding*/
        $this->add_responsive_control('premium_social_insta_container_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-insta-li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->end_controls_tabs();

        $this->end_controls_section();
        
        $this->start_controls_section('premium_social_behance_style',
            [
                'label'         => esc_html__('Behance', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_social_counter_behance'    => 'yes'
                ]
            ]
        );

        $this->start_controls_tabs('premium_social_behance_style_tabs');
        
        $this->start_controls_tab('premium_social_behance_icon_style',
            [
                'label'             => esc_html__('Icon', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_behance_icon_color',
            [
                'label'         =>esc_html__('Color','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-behance-icon' => 'color:{{VALUE}};'
                ],
            ]
        );

        $this->add_responsive_control('premium_social_behance_icon_size',
            [
                'label'         => esc_html__('Size','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::SLIDER,
                'size_units'    => ['px', "em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-behance-icon' => 'font-size: {{SIZE}}{{UNIT}};'
                    ],
                ]
            );
        
        $this->add_control('premium_social_behance_icon_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-behance-icon'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Icon Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_behance_icon_border',
                    'selector'      => '{{WRAPPER}} .premium-social-behance-icon',
                ]
                );
        
        /*Icon Border Radius*/
        $this->add_control('premium_social_behance_icon_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-behance-icon' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_behance_icon_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-behance-icon',
                    ]
                );
        
        $this->add_responsive_control('premium_social_behance_icon_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-behance-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_responsive_control('premium_social_behance_icon_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-behance-icon' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_behance_text_style',
            [
                'label'             => esc_html__('Text', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_behance_text_color',
                [
                    'label'         => esc_html__('Color','premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'scheme'        => [
                        'type'          => Scheme_Color::get_type(),
                        'value'         => Scheme_Color::COLOR_1
                    ],
                   'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-behance'  => 'color:{{VALUE}};'
                        ]
                ]
        );
        
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_social_behance_text_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-social-count-behance',
                ]
        );
        
        $this->add_control('premium_social_behance_text_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-count-behance'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Title Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_behance_text_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-behance',
                ]
                );
        
        /*Title Border Radius*/
        $this->add_control('premium_social_behance_text_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-behance' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_behance_text_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-behance',
                    ]
                );
        
        /*Title Margin*/
        $this->add_responsive_control('premium_social_behance_text_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-behance' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Title Padding*/
        $this->add_responsive_control('premium_social_behance_text_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-behance' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_behance_container_style',
            [
                'label'             => esc_html__('Container', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_social_behance_container_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-social-count-behance-li',
                    ]
                );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_behance_container_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-behance-li',
                ]
                );
        
        /*Box Border Radius*/
        $this->add_control('premium_social_behance_container_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-behance-li' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'label'         => esc_html__('Box Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_behance_container_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-behance-li',
                ]
                );

        /*Box Margin*/
        $this->add_responsive_control('premium_social_behance_container_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-behance-li' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Box Padding*/
        $this->add_responsive_control('premium_social_behance_container_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-behance-li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section('premium_social_vimeo_style',
            [
                'label'         => esc_html__('Vimeo', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_social_counter_vimeo'    => 'yes'
                ]
            ]
        );

        $this->start_controls_tabs('premium_social_vimeo_style_tabs');
        
        $this->start_controls_tab('premium_social_vimeo_icon_style',
            [
                'label'             => esc_html__('Icon', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_vimeo_icon_color',
            [
                'label'         =>esc_html__('Color','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-vimeo-icon' => 'color:{{VALUE}};'
                ],
            ]
        );

        $this->add_responsive_control('premium_social_vimeo_icon_size',
            [
                'label'         => esc_html__('Size','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::SLIDER,
                'size_units'    => ['px', "em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-vimeo-icon' => 'font-size: {{SIZE}}{{UNIT}};'
                    ],
                ]
            );
        
        $this->add_control('premium_social_vimeo_icon_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-vimeo-icon'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Icon Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_vimeo_icon_border',
                    'selector'      => '{{WRAPPER}} .premium-social-vimeo-icon',
                ]
                );
        
        /*Icon Border Radius*/
        $this->add_control('premium_social_vimeo_icon_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-vimeo-icon' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_vimeo_icon_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-vimeo-icon',
                    ]
                );
        
        $this->add_responsive_control('premium_social_vimeo_icon_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-vimeo-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_responsive_control('premium_social_vimeo_icon_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-vimeo-icon' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_vimeo_text_style',
            [
                'label'             => esc_html__('Text', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_vimeo_text_color',
                [
                    'label'         => esc_html__('Color','premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'scheme'        => [
                        'type'          => Scheme_Color::get_type(),
                        'value'         => Scheme_Color::COLOR_1
                    ],
                   'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-vimeo'  => 'color:{{VALUE}};'
                        ]
                ]
        );
        
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_social_vimeo_text_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-social-count-vimeo',
                ]
        );
        
        $this->add_control('premium_social_vimeo_text_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-count-vimeo'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Title Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_vimeo_text_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-vimeo',
                ]
                );
        
        /*Title Border Radius*/
        $this->add_control('premium_social_vimeo_text_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-vimeo' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_vimeo_text_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-vimeo',
                    ]
                );
        
        /*Title Margin*/
        $this->add_responsive_control('premium_social_vimeo_text_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-vimeo' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Title Padding*/
        $this->add_responsive_control('premium_social_vimeo_text_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-vimeo' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_vimeo_container_style',
            [
                'label'             => esc_html__('Container', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_social_vimeo_container_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-social-count-vimeo-li',
                    ]
                );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_vimeo_container_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-vimeo-li',
                ]
                );
        
        /*Box Border Radius*/
        $this->add_control('premium_social_vimeo_container_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-vimeo-li' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'label'         => esc_html__('Box Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_vimeo_container_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-vimeo-li',
                ]
                );

        /*Box Margin*/
        $this->add_responsive_control('premium_social_vimeo_container_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-vimeo-li' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Box Padding*/
        $this->add_responsive_control('premium_social_vimeo_container_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-vimeo-li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section('premium_social_pin_style',
            [
                'label'         => esc_html__('Pinterest', 'premium-addons-for-elementor'),
                'tab'           => Controls_Manager::TAB_STYLE,
                'condition'     => [
                    'premium_social_counter_pin'    => 'yes'
                ]
            ]
        );

        $this->start_controls_tabs('premium_social_pin_style_tabs');
        
        $this->start_controls_tab('premium_social_pin_icon_style',
            [
                'label'             => esc_html__('Icon', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_pin_icon_color',
            [
                'label'         =>esc_html__('Color','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::COLOR,
                'scheme'        => [
                    'type'      => Scheme_Color::get_type(),
                    'value'     => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-pin-icon' => 'color:{{VALUE}};'
                ],
            ]
        );

        $this->add_responsive_control('premium_social_pin_icon_size',
            [
                'label'         => esc_html__('Size','premium-addons-for-elementor'),
                'type'          =>Controls_Manager::SLIDER,
                'size_units'    => ['px', "em"],
                'selectors'     => [
                    '{{WRAPPER}} .premium-social-pin-icon' => 'font-size: {{SIZE}}{{UNIT}};'
                    ],
                ]
            );
        
        $this->add_control('premium_social_pin_icon_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-pin-icon'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Icon Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_pin_icon_border',
                    'selector'      => '{{WRAPPER}} .premium-social-pin-icon',
                ]
                );
        
        /*Icon Border Radius*/
        $this->add_control('premium_social_pin_icon_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-pin-icon' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_pin_icon_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-pin-icon',
                    ]
                );
        
        $this->add_responsive_control('premium_social_pin_icon_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-pin-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        $this->add_responsive_control('premium_social_pin_icon_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-pin-icon' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_pin_text_style',
            [
                'label'             => esc_html__('Text', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_control('premium_social_pin_text_color',
                [
                    'label'         => esc_html__('Color','premium-addons-for-elementor'),
                    'type'          => Controls_Manager::COLOR,
                    'scheme'        => [
                        'type'          => Scheme_Color::get_type(),
                        'value'         => Scheme_Color::COLOR_1
                    ],
                   'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-pin'  => 'color:{{VALUE}};'
                        ]
                ]
        );
        
        $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name'          => 'premium_social_pin_text_typo',
                    'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                    'selector'      => '{{WRAPPER}} .premium-social-count-pin',
                ]
        );
        
        $this->add_control('premium_social_pin_text_background',
                [
                    'label'             => esc_html__('Background Color', 'premium-addons-for-elementor'),
                    'type'              => Controls_Manager::COLOR,
                    'selectors'      => [
                        '{{WRAPPER}} .premium-social-count-pin'  => 'background-color: {{VALUE}};',
                        ]
                    ]
                );
        
        /*Title Border*/
        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_pin_text_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-pin',
                ]
                );
        
        /*Title Border Radius*/
        $this->add_control('premium_social_pin_text_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-pin' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(),
                [
                    'label'         => esc_html__('Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_pin_text_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-pin',
                    ]
                );
        
        /*Title Margin*/
        $this->add_responsive_control('premium_social_pin_text_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-pin' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Title Padding*/
        $this->add_responsive_control('premium_social_pin_text_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-pin' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->start_controls_tab('premium_social_pin_container_style',
            [
                'label'             => esc_html__('Container', 'premium-addons-for-elementor'),
            ]
            );

        $this->add_group_control(
            Group_Control_Background::get_type(),
                [
                    'name'              => 'premium_social_pin_container_background',
                    'types'             => [ 'classic' , 'gradient' ],
                    'selector'          => '{{WRAPPER}} .premium-social-count-pin-li',
                    ]
                );

        $this->add_group_control(
            Group_Control_Border::get_type(), 
                [
                    'name'          => 'premium_social_pin_container_border',
                    'selector'      => '{{WRAPPER}} .premium-social-count-pin-li',
                ]
                );
        
        /*Box Border Radius*/
        $this->add_control('premium_social_pin_container_border_radius',
                [
                    'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::SLIDER,
                    'size_units'    => ['px', '%' ,'em'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-pin-li' => 'border-radius: {{SIZE}}{{UNIT}};'
                    ]
                ]
                );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
                [
                    'label'         => esc_html__('Box Shadow','premium-addons-for-elementor'),
                    'name'          => 'premium_social_pin_container_shadow',
                    'selector'      => '{{WRAPPER}} .premium-social-count-pin-li',
                ]
                );

        /*Box Margin*/
        $this->add_responsive_control('premium_social_pin_container_margin',
                [
                    'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-pin-li' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);
        
        /*Box Padding*/
        $this->add_responsive_control('premium_social_pin_container_padding',
                [
                    'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                    'type'          => Controls_Manager::DIMENSIONS,
                    'size_units'    => ['px', 'em', '%'],
                    'selectors'     => [
                        '{{WRAPPER}} .premium-social-count-pin-li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                    ]
                ]);

        $this->end_controls_tab();
        
        $this->end_controls_tabs();

        $this->end_controls_section();

        
    }
    
    protected function render(){
        $settings = $this->get_settings();
        $new_tab = ($settings['premium_social_counter_new_tab'] == 'yes') ? 'target="_blank"' : 'target="_self"';
        $show_label = ($settings['premium_social_counter_label'] == 'yes') ? true : false;
        $show_user = ($settings['premium_social_counter_username'] == 'yes') ? true : false;
        
        //Twitter Social Counter
        if($settings['premium_social_counter_twitter'] == 'yes'){
            $twitter_consumer_key = $settings['premium_social_counter_twitter_consumer_key'];
            $twitter_consumer_secret = $settings['premium_social_counter_twitter_consumer_secret'];
            $twitter_user = $settings['premium_social_counter_twitter_username'];
            $twitter_access = $settings['premium_social_counter_twitter_access'];
            $twitter_access_secret = $settings['premium_social_counter_twitter_access_secret'];
            if(!empty($twitter_consumer_key) && !empty($twitter_consumer_secret) && !empty($twitter_access) && !empty($twitter_access_secret)){
                $twitter = premium_twitter_followers_count($twitter_user, $twitter_consumer_key, $twitter_consumer_secret,$twitter_access,$twitter_access_secret);
                $twitter_follower = ($settings['premium_social_counter_format'] == 'yes') ? premium_number_format($twitter[ 'followers_count' ]) : number_format($twitter['followers_count']);
                $twitter_acc_url = $twitter[ 'twitter_acc' ];
                $twitter_name = $twitter['twitter_name'];
            }
        }
        //Google Social Counter
        if($settings['premium_social_counter_google'] == 'yes'){
            $google_api = $settings['premium_social_counter_google_api'];
            $google_user = $settings['premium_social_counter_google_username'];
            if(!empty($google_api) && !empty($google_user)){
                $google = premium_google_followers_count($google_api, $google_user);
                $google_follower = ($settings['premium_social_counter_format'] == 'yes') ? premium_number_format($google[ 'followers_count' ]) : number_format($google['followers_count']);
                $google_acc_url = $google[ 'google_acc' ];
                $google_name = $google['google_name'];
            }
        }
        //Youtube Social Counter
        if($settings['premium_social_counter_youtube'] == 'yes'){
            $youtube_api = $settings['premium_social_counter_youtube_api'];
            $youtube_channel = $settings['premium_social_counter_youtube_channel'];
            $youtube_name = $settings['premium_social_counter_youtube_name'];
            if(!empty($youtube_api) && !empty($youtube_channel)){
                $youtube = premium_youtube_followers_count($youtube_api,$youtube_channel);
                $youtube_follower = ($settings['premium_social_counter_format'] == 'yes') ? premium_number_format($youtube[ 'followers_count' ]) : number_format($youtube['followers_count']);
                $youtube_acc_url = $youtube[ 'youtube_acc' ];
            }
        }
        //Facebook Social Counter
        if($settings['premium_social_counter_facebook'] == 'yes'){
            $facebook_access = $settings['premium_social_counter_facebook_access'];
            $facebook_page = $settings['premium_social_counter_facebook_page_id'];
            if(!empty($facebook_access) && !empty($facebook_page)){
                $facebook = premium_facebook_followers_count($facebook_access,$facebook_page);
                $facebook_follower = ($settings['premium_social_counter_format'] == 'yes') ? premium_number_format($facebook[ 'followers_count' ]) : number_format($facebook['followers_count']);
                $facebook_acc_url = $facebook[ 'facebook_acc' ];
                $facebook_name = $facebook['facebook_name'];
            }
        }
        //Instagram Social Counter
        if($settings['premium_social_counter_insta'] == 'yes'){
            $insta_access = $settings['premium_social_counter_instagram_access'];
            $insta_user = $settings['premium_social_counter_instagram_username'];
            if(!empty($insta_access) && !empty($insta_user)){
                $insta = premium_insta_followers_count($insta_access,$insta_user);
                $insta_follower = ($settings['premium_social_counter_format'] == 'yes') ? premium_number_format($insta[ 'followers_count' ]) : number_format($insta['followers_count']);
                $insta_acc_url = $insta[ 'insta_acc' ];
                $insta_name = $insta['insta_name'];
            }
        }
        //Behance Social Counter
        if($settings['premium_social_counter_behance'] == 'yes'){
            $behance_api = $settings['premium_social_counter_behance_api'];
            $behance_user = $settings['premium_social_counter_behance_username'];
            if(!empty($behance_api) && !empty($behance_user)){
                $behance = premium_behance_followers_count($behance_user,$behance_api);
                $behance_follower = ($settings['premium_social_counter_format'] == 'yes') ? premium_number_format($behance[ 'followers_count' ]) : number_format($behance['followers_count']);
                $behance_acc_url = $behance[ 'behance_acc' ];
                $behance_name = $behance['behance_name'];
            }
        }
        //Vimeo Social Counter
        if($settings['premium_social_counter_vimeo'] == 'yes'){
            $vimeo_channel = $settings['premium_social_counter_vimeo_channel'];
            if(!empty($vimeo_channel)){
                $vimeo = premium_vimeo_followers_count($vimeo_channel);
                $vimeo_follower = ($settings['premium_social_counter_format'] == 'yes') ? premium_number_format($vimeo[ 'followers_count' ]) : number_format($vimeo['followers_count']);
                $vimeo_acc_url = $vimeo[ 'vimeo_acc' ];
                $vimeo_name = $vimeo['vimeo_name'];
            }
        }
        //Pinterest Social Counter
        if($settings['premium_social_counter_pin'] == 'yes'){
            $pin_id = $settings['premium_social_counter_pin_id'];
            if(!empty($pin_id)){
                $pin = premium_pin_followers_count($pin_id);
                $pin_follower = ($settings['premium_social_counter_format'] == 'yes') ? premium_number_format($pin[ 'followers_count' ]) : number_format($pin['followers_count']);
                $pin_acc_url = $pin[ 'pin_acc' ];
                $pin_name = $pin['pin_name'];
            }
        }
        ?>

<div class="premium-social-count-wrap">
    <ul class="premium-social-count-ul <?php echo esc_attr($settings['premium_social_counter_display']); ?>">
        <?php if($settings['premium_social_counter_twitter'] == 'yes' && !empty($twitter_consumer_key) && !empty($twitter_consumer_secret) && !empty($twitter_user)) : ?>
        <li class="premium-social-count-twitter-li premium-social-list-item">
            <a href="<?php echo $twitter_acc_url; ?>" <?php echo $new_tab; ?>>
                <i class="fa fa-twitter premium-social-twitter-icon"></i>
                <div class="premium-social-count-twitter">
                    <span>
                        <?php if($show_user) : echo $twitter_name . ' ' . $twitter_follower;
                        else : 
                            echo $twitter_follower;
                        endif;
?>
                    </span>
                    <?php if($show_label) : ?>
                        <small>Followers</small>
                    <?php endif; ?>
                </div>
            </a>
        </li>
        <?php endif; ?>
        <?php if($settings['premium_social_counter_google'] == 'yes' && !empty($google_api) && !empty($google_user) ) : ?>
        <li class="premium-social-count-twitter-li premium-social-list-item">
            <a href="<?php echo $google_acc_url; ?>" <?php echo $new_tab; ?>>
                <i class="fa fa-google-plus premium-social-google-icon"></i>
                <div class="premium-social-count-google"><span>
                    <?php if($show_user) : echo $google_name . ' ' . $google_follower;
                    else:
                        echo $google_follower;
                    endif;
                    ?>
                        
                    </span>
                    <?php if($show_label) : ?>
                        <small>Followers</small>
                    <?php endif; ?>
                </div>
            </a>
        </li>
        <?php endif; ?>
        <?php if($settings['premium_social_counter_youtube'] == 'yes' && !empty($youtube_api) && !empty($youtube_channel)) : ?>
        <li class="premium-social-count-youtube-li premium-social-list-item">
            <a href="<?php echo $youtube_acc_url; ?>" <?php echo $new_tab; ?>>
                <i class="fa fa-youtube premium-social-youtube-icon"></i>
                <div class="premium-social-count-youtube"><span>
                    <?php if($show_user) : echo $youtube_name . ' ' . $youtube_follower;
                    else: 
                        echo $youtube_follower;
                    endif;
?>
                    </span>
                    <?php if($show_label) : ?>
                        <small>Subscribers</small>
                    <?php endif; ?>
                </div>
            </a>
        </li>
        <?php endif; ?>
        <?php if($settings['premium_social_counter_facebook'] == 'yes' && !empty($facebook_access) && !empty($facebook_page)) : ?>
        <li class="premium-social-count-facebook-li premium-social-list-item">
            <a href="<?php echo $facebook_acc_url; ?>" <?php echo $new_tab; ?>>
                <i class="fa fa-facebook premium-social-facebook-icon"></i>
                <div class="premium-social-count-facebook"><span>
                    <?php if($show_user) : echo $facebook_name . ' ' . $facebook_follower;
                    else: 
                        echo $facebook_follower;
                    endif;
?>
                    </span>
                    <?php if($show_label) : ?>
                        <small>Likes</small>
                    <?php endif; ?>
                </div>
            </a>
        </li>
        <?php endif; ?>
        <?php if($settings['premium_social_counter_insta'] == 'yes' && !empty($insta_access) && !empty($insta_user)) : ?>
        <li class="premium-social-count-insta-li premium-social-list-item">
            <a href="<?php echo $insta_acc_url; ?>" <?php echo $new_tab; ?>>
                <i class="fa fa-instagram premium-social-insta-icon"></i>
                <div class="premium-social-count-insta"><span>
                    <?php if($show_user) : echo $insta_name . ' ' . $insta_follower;
                    else:
                        echo $insta_follower;
                    endif;
?>
                    </span>
                    <?php if($show_label) : ?>
                        <small>Followers</small>
                    <?php endif; ?>
                </div>
            </a>
        </li>
        <?php endif; ?>
        <?php if($settings['premium_social_counter_behance'] == 'yes' && !empty($behance_api) && !empty($behance_user)) : ?>
        <li class="premium-social-count-behance-li premium-social-list-item">
            <a href="<?php echo $behance_acc_url; ?>" <?php echo $new_tab; ?>>
                <i class="fa fa-behance premium-social-behance-icon"></i>
                <div class="premium-social-count-behance"><span>
                    <?php if($show_user) : echo $behance_name . ' ' . $behance_follower;
                    else:
                        echo $behance_follower;
                    endif;
?>
                    </span>
                    <?php if($show_label) : ?>
                        <small>Followers</small>
                    <?php endif; ?>
                </div>
            </a>
        </li>
        <?php endif; ?>
        <?php if($settings['premium_social_counter_vimeo'] == 'yes' && !empty($vimeo_channel)) : ?>
        <li class="premium-social-count-vimeo-li premium-social-list-item">
            <a href="<?php echo $vimeo_acc_url; ?>" <?php echo $new_tab; ?>>
                <i class="fa fa-vimeo premium-social-vimeo-icon"></i>
                <div class="premium-social-count-vimeo"><span>
                    <?php if($show_user) : echo $vimeo_name . ' ' . $vimeo_follower;
                    else:
                        echo $vimeo_follower;
                    endif;
?>
                    </span>
                    <?php if($show_label) : ?>
                        <small>Followers</small>
                    <?php endif; ?>
                </div>
            </a>
        </li>
        <?php endif; ?>
        <?php if($settings['premium_social_counter_pin'] == 'yes' && !empty($pin_id)) : ?>
        <li class="premium-social-count-pin-li premium-social-list-item">
            <a href="<?php echo $pin_acc_url; ?>" <?php echo $new_tab; ?>>
                <i class="fa fa-pinterest premium-social-pin-icon"></i>
                <div class="premium-social-count-pin"><span>
                    <?php if($show_user) : echo $pin_name . ' ' . $pin_follower;
                    else:
                        echo $pin_follower;
                    endif;
?>
                    </span>
                    <?php if($show_label) : ?>
                        <small>Followers</small>
                    <?php endif; ?>
                </div>
            </a>
        </li>
        <?php endif; ?>
        <div class="premium-clearfix"></div>
    </ul>
</div>

        <?php
    }
    
}
PLUGIN::instance()->widgets_manager->register_widget_type(new Premium_Social_Counter_Widget());