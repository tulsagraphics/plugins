<?php

namespace PremiumAddonsPro;

if( ! defined( 'ABSPATH' ) ) exit();

class Premium_Addons_Integration {
    
    private static $instance = null;
    
    public function __construct()
    {
        //Load plugin icons font
        add_action( 'elementor/editor/before_enqueue_styles', array( $this, 'premium_pro_font_setup' ) );
        
        // Load main CSS file
        add_action( 'elementor/frontend/after_enqueue_styles', array( $this, 'premium_pro_required_assets' ) );
        
        // Load widgets files
        add_action( 'elementor/widgets/widgets_registered', array( $this, 'premium_pro_widget_register' ) );
        
        // Registers required JS files
        add_action( 'elementor/frontend/after_register_scripts', array( $this, 'premium_pro_register_scripts' ) );
        
    }
    
    /**
    * Loads plugin icons font
    * @since 1.0.0
    * @access public
    * @return void
    */
    public function premium_pro_font_setup() {
        wp_enqueue_style(
            'premium-pro-elements',
            PREMIUM_PRO_ADDONS_URL . 'admin/assets/pa-elements-font/css/pa-elements.css'
        );
    }
    
    /** Load widgets require function
    * @since 1.0.0
    * @access public
    */
    public function premium_pro_widget_register() {
        $this->premium_pro_widgets_area();
    }
    
    /** Enqueue required CSS files
        * @since 1.0.0
        * @access public
        */
    public function premium_pro_required_assets() {

        wp_enqueue_style(
            'premium-pro',
            PREMIUM_PRO_ADDONS_URL . 'assets/css/premium-addons.css',
            array(),
            PREMIUM_PRO_ADDONS_VERSION,
            'all'
        );

    }
    
    /** Requires widgets files
    * @since 1.0.0
    * @access private
    */
    private function premium_pro_widgets_area() {

        $check_component_active = Premium_Pro_Admin_Settings::get_enabled_keys();
        
        foreach ( glob( PREMIUM_PRO_ADDONS_PATH . 'widgets/' . '*.php' ) as $file ) {
            $slug = basename( $file, '.php' );
            
            $enabled = isset( $check_component_active[ $slug ] ) ? $check_component_active[ $slug ] : '';
            
            if ( filter_var( $enabled, FILTER_VALIDATE_BOOLEAN ) || ! $check_component_active ) {
                $this->register_addon( $file );
            }
        }

    }
    
    /** Registers required JS files
    * @since 1.0.0
    * @access public
    */
    public function premium_pro_register_scripts() {

        $check_component_active = Premium_Pro_Admin_Settings::get_enabled_keys();
        
        if( in_array( true, $check_component_active ) ) {
            wp_register_script('premium-pro-js', PREMIUM_PRO_ADDONS_URL . 'assets/js/premium-addons.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
        }

        if( $check_component_active['premium-twitter-feed'] || $check_component_active['premium-facebook-feed'] ) {
            wp_register_script('codebird-js', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/codebird.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
            wp_register_script('dot-js', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/doT.min.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
            wp_register_script('moment-js', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/moment.min.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
            wp_register_script('jquery-socialfeed-js', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/jquery.socialfeed.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
        }

        if( $check_component_active['premium-instagram-feed'] ) {
            wp_enqueue_style('pa-prettyphoto', PREMIUM_PRO_ADDONS_URL . 'assets/css/prettyphoto.css', array(), PREMIUM_PRO_ADDONS_VERSION, 'all');
            wp_register_script('pa-insta-prettyphoto', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/prettyPhoto.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
            wp_register_script('instafeed-js', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/instafeed.min.js', array(), PREMIUM_PRO_ADDONS_VERSION, true);
        }

        if( $check_component_active['premium-tabs'] ) {
            wp_register_script('cbpfwtabs', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/cbpFWTabs.js', array(), PREMIUM_PRO_ADDONS_VERSION, true);
        }

        if( $check_component_active['premium-charts'] ) {
            wp_register_script('chart-js', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/Chart.js', array(), PREMIUM_PRO_ADDONS_VERSION, false);
        }

        if( $check_component_active['premium-instagram-feed'] || $check_component_active['premium-twitter-feed'] || $check_component_active['premium-facebook-feed'] || $check_component_active['premium-facebook-reviews'] || $check_component_active['premium-google-reviews'] ) {
            wp_register_script('masonry-js', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/masonry.min.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);            
        }

        if( $check_component_active['premium-image-comparison'] ) {
            wp_register_script('event-move', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/jquery.event.move.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
            wp_register_script('pa-imgcompare', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/imgcompare.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
        }

        if( $check_component_active['premium-prev-img'] ) {
            wp_enqueue_style( 'tooltipster-themes', PREMIUM_PRO_ADDONS_URL . 'assets/css/tooltipster-themes.css', array(), PREMIUM_PRO_ADDONS_VERSION, 'all' );
        }

        if( $check_component_active['premium-behance'] ) {
            wp_register_script('premium-behance-js', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/embed.behance.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
        }

        if( $check_component_active['premium-img-layers'] ){
            wp_register_script('tweenmax-js', PREMIUM_PRO_ADDONS_URL.'assets/js/lib/TweenMax.min.js',array( 'jquery' ), PREMIUM_PRO_ADDONS_VERSION, true);
            wp_register_script('parallaxmouse-js', PREMIUM_PRO_ADDONS_URL.'assets/js/lib/jquery-parallax.js',array( 'jquery' ), PREMIUM_PRO_ADDONS_VERSION, true);
            wp_register_script('tilt-js', PREMIUM_PRO_ADDONS_URL.'assets/js/lib/universal-tilt.min.js',array( 'jquery' ), PREMIUM_PRO_ADDONS_VERSION, true);
        }
        
        if( $check_component_active['premium-tables'] ){
            wp_register_script('table-sorter', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/jquery.tablesorter.min.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
        }
        
        if( $check_component_active['premium-instagram-feed'] || $check_component_active['premium-image-comparison'] ){
            wp_register_script('pa-imagesloaded', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/imagesloaded.min.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
        }
        
        if( $check_component_active['premium-charts'] || $check_component_active['premium-img-layers'] ){
            wp_register_script('waypoints', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/jquery.waypoints.js', array('jquery'), PREMIUM_ADDONS_VERSION, true);
        }

        if( $check_component_active['premium-prev-img'] || $check_component_active['premium-image-hotspots'] || $check_component_active['premium-whatsapp-chat'] ){
            wp_enqueue_style( 'tooltipster-bundle', PREMIUM_PRO_ADDONS_URL . 'assets/css/tooltipster.bundle.min.css', array(), PREMIUM_PRO_ADDONS_VERSION, 'all' );
            wp_register_script( 'tooltipster-bundle-js', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/tooltipster.bundle.min.js', array(), PREMIUM_PRO_ADDONS_VERSION, true );
        }
    }
    
    /**
    * Register addon by file name
    *
    * @param  string $file            File name.
    * @param  object $widgets_manager Widgets manager instance.
    * @return void
    */
    public function register_addon( $file ) {

        $base  = basename( str_replace( '.php', '', $file ) );
        $class = ucwords( str_replace( '-', ' ', $base ) );
        $class = str_replace( ' ', '_', $class );
        $class = sprintf( 'Elementor\%s', $class );

        require $file;
        
        if ( 'Elementor\Premium_Facebook_Reviews'== $class || 'Elementor\Premium_Google_Reviews' == $class ) {
            require_once (PREMIUM_PRO_ADDONS_PATH . 'includes/urlopen.php');

            require_once (PREMIUM_PRO_ADDONS_PATH . 'includes/premium-revs.php'); 
        }

        if ( class_exists( $class ) ) {
            \Elementor\PLUGIN::instance()->widgets_manager->register_widget_type( new $class );
        }
    }
    
    /**
    * Creates and returns an instance of the class
    * @since 1.0.0
    * @access public
    * return object
    */
   public static function get_instance(){
       if( self::$instance == null ) {
           self::$instance = new self;
       }
       return self::$instance;
   }
}
    

if ( ! function_exists( 'premium_addons_integration' ) ) {

	/**
	 * Returns an instance of the plugin class.
	 * @since  1.0.0
	 * @return object
	 */
	function premium_addons_integration() {
		return Premium_Addons_Integration::get_instance();
	}
}
premium_addons_integration();