<?php

/**
 * Class: Module
 * Name: Column Badge
 * Slug: premium-badge
 */

namespace PremiumAddonsPro\Modules\PremiumColumnBadge;

use Elementor;
use Elementor\Utils;
use Elementor\Elementor_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Scheme_Typography;
use Elementor\Scheme_Color;
use Elementor\Element_Base;
use Elementor\Widget_Base;
use PremiumAddonsPro\Base\Module_Base;
use PremiumAddonsPro\Plugin;


if( !defined( 'ABSPATH' ) ) exit;

class Module extends Module_Base {
    
    public function __construct(){
        
        parent::__construct();
        
        //Checks if Column Badge is enabled
        $check_badge_active = isset( get_option( 'pa_pro_save_settings' )['premium-badge'] ) ? get_option( 'pa_pro_save_settings' )['premium-badge'] : 1;
        if( $check_badge_active ){
            
            //Creates Premium Badge tab at the end of column layout tab
            add_action( 'elementor/element/column/layout/after_section_end',array( $this,'register_controls'), 10 );
            
            //Renders the badge before column rendering
            add_action( 'elementor/frontend/column/before_render',array( $this,'before_render') );
            
            //Enqueue the required JS file
            add_action( 'elementor/frontend/before_enqueue_scripts', array( $this, 'premium_badge_enqueue_scripts' ), 9 );
            
        }
    }
    
    public function get_name(){
        return 'premium-badge';
    }
    
    public function register_controls( $element ){
        
        $element->start_controls_section('premium_soc_badge_section',
            [
            'label'             => esc_html__('Premium Badge', 'premium-addons-for-elementor'),
            'tab'               => Controls_Manager::TAB_LAYOUT
            ]
        );
        
        $element->add_control('premium_badge_switcher',
            [
				'label'         => __( 'Enable Badge', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::SWITCHER,
			]
		);
        
        $element->add_control('premium_badge_notice',
            [
                'raw'           => __( 'NOTICE: Badge for inner columns works only on the Front-end side', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::RAW_HTML,
            ]
        );
         
        $element->start_controls_tabs('premium_soc_badge_tabs');

        $element->start_controls_tab('premium_soc_badge_content_tab',
            [
                'label'         => esc_html__('Content', 'premium-addons-for-elementor'),
            ]
        );

        $element->add_control('premium_soc_badge_badge_body_content',
            [
                'label'         => esc_html__('Badge', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::HEADING,
            ]
        );

        $element->add_control('premium_soc_badge_type',
            [
                'label'         => esc_html__( 'Style', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'badge_image'               => esc_html__( 'Image', 'premium-addons-for-elementor' ),
                    'create_your_own_style'     => esc_html__( 'Create Your Own Style', 'premium-addons-for-elementor' ),
                ],
                'default'       => 'create_your_own_style',
                'label_block'   => 'true',
            ]
        );
        
        $element->add_control('premium_soc_badge_image',
            [
                'label'         => esc_html__( 'Choose Image', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::MEDIA,
                'default'       => [
                    'url' => Utils::get_placeholder_image_src(),
                ],
                'label_block'   => true,
                'condition'     => [
                    'premium_soc_badge_type' => 'badge_image'
                ]
            ]
        );

        $element->add_control('premium_soc_badge_shape',
            [
                'label'         => esc_html__( 'Badge Shape', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'square'            => esc_html__( 'Square', 'premium-addons-for-elementor' ),
                    'rectangle'         => esc_html__( 'Rectangle', 'premium-addons-for-elementor' ),
                    'circle'            => esc_html__( 'Circle', 'premium-addons-for-elementor' ),
                    'triangle'          => esc_html__( 'Triangle', 'premium-addons-for-elementor' ),
                    'stripe'            => esc_html__( 'Stripe', 'premium-addons-for-elementor' )
                ],
                'default'       => 'square',
                'label_block'   => 'true',
                'condition'     => [
                    'premium_soc_badge_type' => 'create_your_own_style'
                ]
            ]
        );

        $element->add_control('premium_soc_badge_size',
            [
                'label'         => esc_html__('Badge Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em', '%'],
                'default'       => [
                    'size'  => 70,
                    'unit'  =>'px'
                ],
                'range'         => [
                    'px'    => [
                        'min'   => 0,
                        'max'   => 200,
                        'step'  => .1
                    ],
                    'em'    => [
                        'min'   => 0,
                        'max'   => 40,
                        'step'  => .1
                    ],
                ],
                'condition'     => [
                    'premium_soc_badge_type'    => 'create_your_own_style',
                    'premium_soc_badge_shape!'  => 'stripe',
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-shape-square .premium-soc-badge-content'    => 'width: {{SIZE}}{{UNIT}};height:{{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} .premium-soc-badge-shape-circle .premium-soc-badge-content'    => 'width: {{SIZE}}{{UNIT}};height:{{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} .premium-soc-badge-shape-rectangle .premium-soc-badge-content' => 'width: calc( 2 * {{SIZE}}{{UNIT}}) !important;height:{{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} .premium-soc-badge-shape-triangle .premium-soc-badge-content'  => 'border-width: 0px {{SIZE}}{{UNIT}} {{SIZE}}{{UNIT}} 0px;',
                ],
            ]    
        );

        $element->add_control('premium_soc_badge_image_badge_size',
            [
                'label'         => esc_html__('Badge Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em', '%'],
                'default'       => [
                    'size'  => 70,
                    'unit'  => 'px'
                ],
                'range'         => [
                    'px'    => [
                        'min'   => 0,
                        'max'   => 200,
                    ],
                    'em'    => [
                        'min'   => 0,
                        'max'   => 40,
                        'step'  => .1
                    ],
                ],
                'condition'     => [
                    'premium_soc_badge_type' => 'badge_image',
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-img .premium-soc-badge-content' => 'width: {{SIZE}}{{UNIT}};height:{{SIZE}}{{UNIT}};background-size:{{SIZE}}{{UNIT}} {{SIZE}}{{UNIT}};'
                ],
            ]    
        );

        $element->add_control('premium_soc_badge_stripe_badge_size',
            [
                'label'         => esc_html__('Badge Size', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'range'         => [
                    'px' => [
                        'min' => 0,
                        'max' => 5,
                        'step'=> .1,
                    ]   
                ],
                'condition'     => [
                    'premium_soc_badge_type' => 'create_your_own_style',
                    'premium_soc_badge_shape' => 'stripe',
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-shape-stripe .premium-soc-badge-content-parent' => 'transform: scale({{SIZE}});-ms-transform: scale({{SIZE}});-webkit-transform: scale({{SIZE}});'
                ],
            ]    
        );

        $element->add_control('premium_soc_badge_badge_position',
            [
                'label'         => esc_html__( 'Badge Position', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'top-left'          => esc_html__( 'Top Left', 'premium-addons-for-elementor' ),
                    'top-right'         => esc_html__( 'Top Right', 'premium-addons-for-elementor' ),
                    'bottom-left'       => esc_html__( 'Bottom Left', 'premium-addons-for-elementor' ),
                    'bottom-right'      => esc_html__( 'Bottom Right', 'premium-addons-for-elementor' ),
                    'custom'            => esc_html__( 'Custom Position', 'premium-addons-for-elementor' ),
                ],
                'default'       => 'top-left',
                'label_block'   => 'true',
            ]
        );

        $element->add_responsive_control(
            'premium_soc_badge_horizontal_position',
            [
                'label'         => esc_html__('Horizontal Offset', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'range'         => [
                    'px' => [
                        'min'   => 0,
                        'max'   => 100,
                        'step'  => 0.1,
                    ]
                ],
                'condition'     => [
                    'premium_soc_badge_badge_position' => 'custom'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-container .premium-soc-badge-body' => 'left: {{SIZE}}%;',
                ],
            ]
        );

        $element->add_responsive_control('premium_soc_badge_vertical_position',
            [
                'label'         => esc_html__('Vertical Offset', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'range'         => [
                    'px' => [
                        'min'   => 0,
                        'max'   => 100,
                        'step'  => 0.1,
                    ]
                ],
                'condition'     => [
                    'premium_soc_badge_badge_position' => 'custom'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-container .premium-soc-badge-body' => 'top: {{SIZE}}%;',
                    
                ],

            ]
        );

        $element->add_control('premium_badge_rotate',
            [
                'label'         => esc_html__('Rotate', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'condition'     => [
                    'premium_soc_badge_badge_position' => 'custom'
                ],
            ]
        );

        $element->add_responsive_control('premium_badge_rotate_deg',
            [
                'label'         => esc_html__('Degrees', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::NUMBER,
                'description'   => esc_html__('Set rotation value in degress', 'premium-addons-for-elementor'),
                'min'           => -180,
                'max'           => 180,
                'condition'     => [
                    'premium_soc_badge_badge_position' => 'custom',
                    'premium_badge_rotate'   => 'yes'
                ],
                'separator'     => 'after',
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-body' => '-webkit-transform: rotate({{VALUE}}deg); -moz-transform: rotate({{VALUE}}deg); -o-transform: rotate({{VALUE}}deg); transform: rotate({{VALUE}}deg);'
                ],
            ]
        );

        $element->add_control('premium_soc_badge_show_text_editor',
            [
                'label'         => esc_html__( 'Badge Text', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SWITCHER,
                'label_on'      => esc_html__( 'Show', 'premium-addons-for-elementor' ),
                'label_off'     => esc_html__( 'Hide', 'premium-addons-for-elementor' ),
                'default'       => 'no',
            ]
        );

        $element->add_control('premium_soc_badge_text_content',
            [
                'label'         => esc_html__('Text', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::HEADING,
                'separator'     => 'before',
                'condition'     => [
                    'premium_soc_badge_show_text_editor'  => 'yes'
                ]
            ]
        );

        $element->add_control('premium_soc_badge_text_editor',
            [
                'label'         => esc_html__('Text', 'premium-addons-for-elementor'),
                'default'       => esc_html__('Sale', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::TEXT,
                'label_block'   => true,
                'condition'     => [
                    'premium_soc_badge_show_text_editor'  => 'yes'
                ]
            ]
        );

        $element->add_control(
            'premium_soc_badge_stripe_text_horizontal_position',
            [
                'label'         => esc_html__('Horizontal Offset', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'condition'     => [
                    'premium_soc_badge_type' => 'create_your_own_style',
                    'premium_soc_badge_shape' => 'stripe',
                    'premium_soc_badge_show_text_editor'  => 'yes'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-shape-stripe .premium-soc-badge-text-container' => 'left:{{SIZE}}%',
                ]
            ]
        );

        $element->add_control('premium_soc_badge_stripe_text_vertical_position',
            [
                'label'         => esc_html__('Vertical Offset', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'condition'     => [
                    'premium_soc_badge_type' => 'create_your_own_style',
                    'premium_soc_badge_shape' => 'stripe',
                    'premium_soc_badge_show_text_editor'  => 'yes'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-shape-stripe .premium-soc-badge-text-container' => 'top:{{SIZE}}%',
                ]
            ]
        );

        $element->add_responsive_control('premium_soc_badge_text_rotate',
            [
                'label'         => esc_html__('Rotate', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'range'         => [
                    'px'     => [
                        'min'   => -180,
                        'max'   => 180,
                    ]
                ],
                'condition'     => [
                    'premium_soc_badge_type' => 'create_your_own_style',
                    'premium_soc_badge_shape' => 'triangle',
                    'premium_soc_badge_show_text_editor'  => 'yes'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-container .premium-soc-badge-text-container' => 'transform: rotate({{SIZE}}deg);-ms-transform: rotate({{SIZE}}deg);-moz-transform: rotate({{SIZE}}deg);-webkit-transform: rotate({{SIZE}}deg);-o-transform: rotate({{SIZE}}deg);',
                ]
            ]
        );

        $element->add_responsive_control('premium_soc_badge_text_align_badge_style', 
            [
                'label'         => esc_html__('Alignment', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'  => [
                        'title'     => esc_html__('Left', 'premium-addons-for-elementor'),
                        'icon'      => 'fa fa-align-left'
                    ],
                    'center'  => [
                        'title'     => esc_html__('Center', 'premium-addons-for-elementor'),
                        'icon'      => 'fa fa-align-center'
                    ],
                    'right'  => [
                        'title'     => esc_html__('Right', 'premium-addons-for-elementor'),
                        'icon'      => 'fa fa-align-right'
                    ],
                ],
                'default'       => 'center',
                'condition'     => [
                    'premium_soc_badge_type' => 'create_your_own_style',
                    'premium_soc_badge_shape!' => 'triangle',
                    'premium_soc_badge_show_text_editor'  => 'yes',
                ],
                'selectors'         => [
                    '{{WRAPPER}} .premium-soc-badge-container .premium-soc-badge-text-container' => 'text-align: {{VALUE}}'
                ]
            ]
        );

        $element->add_control('premium_soc_badge_text_align_badge_image', 
            [
                'label'         => esc_html__('Alignment', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::CHOOSE,
                'options'       => [
                    'left'      => [
                        'title'     => esc_html__('Left', 'premium-addons-for-elementor'),
                        'icon'      => 'fa fa-align-left'
                    ],
                    'center'    => [
                        'title'     => esc_html__('Center', 'premium-addons-for-elementor'),
                        'icon'      => 'fa fa-align-center'
                    ],
                    'right'     => [
                        'title'     => esc_html__('Right', 'premium-addons-for-elementor'),
                        'icon'      => 'fa fa-align-right'
                    ],
                ],
                'default'       => 'center',
                'condition'     => [
                    'premium_soc_badge_type' => 'badge_image',
                    'premium_soc_badge_show_text_editor'  => 'yes'
                ],
                'selectors'         => [
                    '{{WRAPPER}} .premium-soc-badge-container .premium-soc-badge-text-container' => 'text-align: {{VALUE}}'
                ]
            ]
        );

        $element->add_control('premium_soc_badge_index',
            [
                'label'         => esc_html__('Z-Index', 'premium-addons-for-elementor'),
                'description'   => esc_html__('Put a Z-index, default: 3','premium-addons-for-elementor'),
                'type'          => Controls_Manager::NUMBER,    
                'min'           => 0,
                'max'           => 10000,
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-body'   => 'z-index: {{VALUE}};'
                ]
            ]
        );

        $element->end_controls_tab();

        $element->start_controls_tab('premium_soc_badge_style_tab',
            [
                'label'         => esc_html__('Style', 'premium-addons-for-elementor'),
            ]
        );

        $element->add_control('premium_soc_badge_badge_heading',
            [
                'label'         => esc_html__('Badge', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::HEADING,
            ]
        );

        $element->add_control('premium_soc_badge_background_color',
            [
                'label'         => esc_html__('Background Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-shape-square .premium-soc-badge-content, {{WRAPPER}} .premium-soc-badge-shape-rectangle .premium-soc-badge-content, {{WRAPPER}} .premium-soc-badge-shape-circle .premium-soc-badge-content' => 'background: {{VALUE}};',
                    '{{WRAPPER}} .premium-soc-badge-shape-triangle .premium-soc-badge-content' => 'border-color: transparent transparent {{VALUE}} transparent;'
                ],
                'condition'     => [
                    
                    'premium_soc_badge_type' => 'create_your_own_style',
                    'premium_soc_badge_shape!' => 'stripe'
                ],
                'separator'     => 'before'
            ]
        );

        $element->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'          => 'premium_soc_badge_stripe_badge_background_color',
                'types'         => [ 'gradient' ],
                'selector'      => '{{WRAPPER}} .premium-soc-badge-shape-stripe .premium-soc-badge-content:before , {{WRAPPER}} .premium-soc-badge-shape-stripe .premium-soc-badge-content:after',
                'condition'     => [
                    'premium_soc_badge_type' => 'create_your_own_style',
                    'premium_soc_badge_shape' => 'stripe',
                ],
                'separator'     => 'before'
            ]
        );

        $element->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_soc_badge_image_bage_border',
                'selector'      => '{{WRAPPER}} .premium-soc-badge-content',
                'condition'     => [
                    'premium_soc_badge_type' => 'badge_image'
                ]
            ]
        );

        $element->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_soc_badge_your_style_border',
                'selector'      => '{{WRAPPER}} .premium-soc-badge-content',
                'condition'     => [
                    'premium_soc_badge_type' => 'create_your_own_style',
                    'premium_soc_badge_shape!' => ['triangle','stripe']
                ]
            ]
        );

        $element->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_soc_badge_your_style_stripe_border',
                'selector'      => '{{WRAPPER}} .premium-soc-badge-content-parent',
                'condition'     => [
                    'premium_soc_badge_type' => 'create_your_own_style',
                    'premium_soc_badge_shape' => 'stripe'
                ]
            ]
        );

        $element->add_control('premium_soc_badge_your_style_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em' , '%'],
                'condition'     => [
                    'premium_soc_badge_shape!' => ['circle','triangle']
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-shape-square .premium-soc-badge-content, {{WRAPPER}} .premium-soc-badge-shape-rectangle .premium-soc-badge-content, {{WRAPPER}} .premium-soc-badge-shape-stripe .premium-soc-badge-content-parent' => 'border-radius: {{SIZE}}{{UNIT}};',
                ]
            ]
        );

        $element->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'          => 'premium_soc_badge_your_style_box_shadow',
                'condition'     => [
                    'premium_soc_badge_shape!' => ['triangle','stripe']
                ],
                'selector'      => '{{WRAPPER}} .premium-soc-badge-content',
            ]
        );

        $element->add_responsive_control('premium_soc_badge_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-content-parent' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        
        $element->add_responsive_control('premium_soc_badge_stripe_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'condition'     => [
                    'premium_soc_badge_type' => 'create_your_own_style',
                    'premium_soc_badge_shape' => 'stripe'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-content-parent' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $element->add_control('premium_soc_badge_text_heading',
            [
                'label'         => esc_html__('Text', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::HEADING,
                'condition'     => [
                    'premium_soc_badge_show_text_editor'  => 'yes'
                ]
            ]
        );

        $element->add_control('premium_soc_badge_text_color',
            [
                'label'         => esc_html__('Text Color', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::COLOR,
                'scheme'        => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'condition'     => [
                    'premium_soc_badge_show_text_editor'  => 'yes'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-body .premium-soc-badge-text-container'  => 'color: {{VALUE}};'
                ],
                'separator'     => 'before'
            ]
        );

        $element->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'          => 'premium_soc_badge_text_typo',
                'scheme'        => Scheme_Typography::TYPOGRAPHY_1,
                'condition'     => [
                    'premium_soc_badge_show_text_editor'  => 'yes'
                ],
                'selector'      => '{{WRAPPER}} .premium-soc-badge-body .premium-soc-badge-text-container'
            ]
        );
        
        $element->add_group_control(
            Group_Control_Text_Shadow::get_type(),
            [
                'name'          => 'premium_soc_badge_text_shadow',
                'condition'     => [
                    'premium_soc_badge_show_text_editor'  => 'yes'
                ],
                'selector'      => '{{WRAPPER}} .premium-soc-badge-body .premium-soc-badge-text-container'
            ]
        );

        $element->add_group_control(
            Group_Control_Border::get_type(), 
            [
                'name'          => 'premium_soc_badge_text_border',
                'condition'     => [
                    'premium_soc_badge_show_text_editor'  => 'yes'
                ],
                'selector'      => '{{WRAPPER}} .premium-soc-badge-body .premium-soc-badge-text-container'
            ]
        );
        
        $element->add_control('premium_soc_badge_text_border_radius',
            [
                'label'         => esc_html__('Border Radius', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'size_units'    => ['px', 'em' , '%'],
                'condition'     => [
                    'premium_soc_badge_show_text_editor'  => 'yes'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-body .premium-soc-badge-text-container' => 'border-radius: {{SIZE}}{{UNIT}};'
                ]
            ]
        );

        $element->add_responsive_control('premium_soc_badge_text_margin',
            [
                'label'         => esc_html__('Margin', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'condition'     => [
                    'premium_soc_badge_show_text_editor'  => 'yes'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-body .premium-soc-badge-text-container' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );

        $element->add_responsive_control('premium_soc_badge_text_padding',
            [
                'label'         => esc_html__('Padding', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::DIMENSIONS,
                'size_units'    => ['px', 'em', '%'],
                'condition'     => [
                    'premium_soc_badge_show_text_editor'  => 'yes'
                ],
                'selectors'     => [
                    '{{WRAPPER}} .premium-soc-badge-body .premium-soc-badge-text-container' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ],
                'separator'     => 'after'
            ]
        );

            $element->end_controls_tab();
        
            $element->end_controls_tabs();
        
            $element->end_controls_section();

        }
        
        public function premium_badge_enqueue_scripts(){
        
            wp_enqueue_script(
                'premium-pro-js',
                PREMIUM_PRO_ADDONS_URL . 'assets/js/premium-addons.js',
                array( 'jquery' ),
                PREMIUM_PRO_ADDONS_VERSION,
                true 
            );
        
        }

        public function before_render( $element ){
            
            $data 		= $element->get_data();
            
            $type       = $data['elType'];
            
            $badge_type = $element->get_settings('premium_soc_badge_type');
            
            if( 'column' == $type && $element->get_settings('premium_badge_switcher') == 'yes' && isset( $badge_type ) ){
                
                $element->add_render_attribute( '_wrapper','class', 'premium-soc-badge premium-soc-badge-' . $element->get_id() );
                
                $this->premium_soc_badge_body_structure( $element );
                
            }
        }

        public function premium_soc_badge_body_structure( $element ) {
            
            $badge_style    = $element->get_settings('premium_soc_badge_type');
            
            $badge_pos      = $element->get_settings('premium_soc_badge_badge_position');
            
            $badge_pos_class = 'premium-soc-badge-' . $badge_pos . '-position';
            
            if( $badge_style == 'create_your_own_style' ){
                
                $badge_html         = '';
                
                $img_badge_class    = '';
                
                $badge_shape        = 'premium-soc-badge-shape-' . $element->get_settings('premium_soc_badge_shape');
                
                $triangle_pos       = ( 'triangle' == $element->get_settings('premium_soc_badge_shape') ) ? 'premium-soc-badge-' . $badge_pos . '-triangle' : '';
                
            } else {
                
                $triangle_pos       = '';
                
                $img_badge_class    = 'premium-soc-badge-img';
                
                $badge_shape        = '';
                
                $badge_html         = 'style="background-image:url(' . $element->get_settings('premium_soc_badge_image')['url'] . ')"';
                
            }
            
            $text_html = ( 'yes' == $element->get_settings('premium_soc_badge_show_text_editor') ) ? '<div class="premium-soc-badge-text-container">' . $element->get_settings('premium_soc_badge_text_editor') . '</div>' : '';
            
            ?>
            <script>
                jQuery( document ).ready( function( $ ) {
                    
                    $('.premium-soc-badge-<?php echo $element->get_id(); ?>.elementor-column').append('<div class="premium-soc-badge-container"><div class="premium-soc-badge-body <?php echo $badge_shape . ' ' . $triangle_pos . ' ' . $badge_pos_class . ' ' . $img_badge_class; ?>"><div class="premium-soc-badge-content-parent"><div class="premium-soc-badge-content" <?php echo $badge_html; ?>><?php echo $text_html; ?></div></div></div></div>');
                });
            </script>
            
        <?php
    }
}