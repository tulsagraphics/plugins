<?php

/**
 * Class: Module
 * Name: Section Animated Gradient
 * Slug: premium-gradient
 */

namespace PremiumAddonsPro\Modules\PremiumSectionGradient;

use Elementor;
use Elementor\Utils;
use Elementor\Repeater;
use Elementor\Elementor_Base;
use Elementor\Controls_Manager;
use Elementor\Element_Base;
use Elementor\Widget_Base;
use PremiumAddonsPro\Base\Module_Base;
use PremiumAddonsPro\Plugin;

if( ! defined( 'ABSPATH' ) ) exit;

class Module extends Module_Base {
    
    public function __construct() {
        
        parent::__construct();
        
        //Checks if Section Gradient is enabled
        $check_gradient_active = isset( get_option( 'pa_pro_save_settings' )['premium-gradient'] ) ? get_option( 'pa_pro_save_settings' )['premium-gradient'] : 1;
        
        if( $check_gradient_active ) {
            
            //Creates Premium Animated Gradient tab at the end of section layout tab
            add_action( 'elementor/element/section/section_layout/after_section_end',array( $this,'register_controls'), 10 );
            
            //Renders the badge after section rendering
            add_action( 'elementor/frontend/section/after_render',array( $this,'after_render') );
            
            //Enqueue the required JS file
            add_action( 'elementor/frontend/before_enqueue_scripts', array( $this, 'premium_gradient_enqueue_scripts' ), 9 );
        }
    }
    
    public function get_name() {
        return 'premium-gradient';
    }
    
    public function register_controls( $element ) {
        
        $element->start_controls_section('premium_gradient_section',
            [
                'label'         => \PremiumAddons\Helper_Functions::get_prefix() . ' Animated Gradient',
                'tab'           => Controls_Manager::TAB_LAYOUT
            ]
        );
        
        $element->add_control('premium_gradient_update',
            [
               'label'          => '<div class="elementor-update-preview" style="background-color: #fff;"><div class="elementor-update-preview-title">Update changes to page</div><div class="elementor-update-preview-button-wrapper"><button class="elementor-update-preview-button elementor-button elementor-button-success">Apply</button></div></div>',
                'type'          => Controls_Manager::RAW_HTML
            ]
        );
        
        $element->add_control('premium_gradient_switcher',
            [
				'label'         => __( 'Enable Animated Gradient', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::SWITCHER,
			]
		);
		
        $repeater = new Repeater();
		
        $element->add_control('premium_gradient_notice',
            [
                'raw'           => __( 'NOTICE: Please remove Elementor\'s background image/gradient for this section', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::RAW_HTML,
            ]
        );
        
        $repeater->add_control('premium_gradient_colors',
			[
				'label'         => __( 'Select Color', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::COLOR,
                'label_block'   => true,
			]
		);
        
        $element->add_control('premium_gradient_colors_repeater',
            [
                'type'          => Controls_Manager::REPEATER,
                'fields'        => array_values( $repeater->get_controls() ),
                'title_field'   => '{{{ premium_gradient_colors }}}'
            ]
        );

        $element->add_control('premium_gradient_angle',
			[
				'label'         => __( 'Put the Gradient Angle in degrees ', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::NUMBER,
                'default'       => -45,
                'min'           => -180,
                'max'           => 180,
                'label_block'   => true,
			]
		);
        
        $element->end_controls_section();
        
    }
    
    public function premium_gradient_enqueue_scripts() {
        
        wp_enqueue_script( 
           'premium-pro-js',
            PREMIUM_PRO_ADDONS_URL . 'assets/js/premium-addons.js',
            array('jquery'),
            PREMIUM_PRO_ADDONS_VERSION,
            true
        );
        
    }
    
    public function after_render( $element ) {
        
		$data 		= $element->get_data();
        
        $type       = $data['elType'];
        
        $settings   = $data['settings'];
        
		if( 'section' == $type && $element->get_settings('premium_gradient_switcher') == 'yes' && isset( $settings['premium_gradient_colors_repeater'] ) ) {
            
            $grad_angle         = !empty( $settings['premium_gradient_angle'] ) ? $settings['premium_gradient_angle'] : -45;
            
            $gradient_style     = 'linear-gradient(' . $grad_angle . 'deg,';
            
            foreach($settings['premium_gradient_colors_repeater'] as $color){
                
                $gradient_style .= $color['premium_gradient_colors'] . ',';
                
            }
            
            $gradient_style .= ')';
            
            $gradient_style = str_replace(',)', ')', $gradient_style);
            
		?>
            <style>
                .elementor-element-<?php echo $element->get_id(); ?>.premium-gradient-move {
                    background: <?php echo $gradient_style; ?>;
                }
            </style>
            
			<script>
				( function( $ ) {
					"use strict";
                    $('.elementor-element-<?php echo $element->get_id(); ?>').addClass('premium-gradient-move');
				}( jQuery ) );
			</script>
		<?php }
	}    
}