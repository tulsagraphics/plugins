<?php

/**
 * Class: Module
 * Name: Section Particles
 * Slug: premium-particles
 */

namespace PremiumAddonsPro\Modules\PremiumSectionParticles;

use Elementor;
use Elementor\Elementor_Base;
use Elementor\Controls_Manager;
use Elementor\Element_Base;
use Elementor\Widget_Base;
use PremiumAddonsPro\Base\Module_Base;
use PremiumAddonsPro\Plugin;

if( !defined( 'ABSPATH' ) ) exit;

class Module extends Module_Base {
    
    public function __construct() {
        parent::__construct();
        
        //Checks if Section Particles is enabled
        $check_particles_active = isset( get_option( 'pa_pro_save_settings' )['premium-particles'] ) ? get_option( 'pa_pro_save_settings' )['premium-particles'] : 1;
        
        if( $check_particles_active ){
            
            //Puts ID to the section before render
            add_action( 'elementor/frontend/section/before_render',array( $this,'before_render') );
            
            //Checks if Section Particles is enabled
            add_action('elementor/element/section/section_layout/after_section_end',array($this,'register_controls'),10 );
           
            //Renders particles after section rendering
            add_action( 'elementor/frontend/section/after_render',array( $this,'after_render') );

            //Enqueue the required JS file
            add_action('wp_enqueue_scripts', array($this,'premium_particles_enqueue_scripts'));
            
        }
    }
    
    public function get_name() {
        return 'premium-particles';
    }
    
    public function register_controls( $element ) {
        
        $element->start_controls_section('premium_particles_section',
            [
                'label'         => \PremiumAddons\Helper_Functions::get_prefix() . ' Particles',
                'tab'           => Controls_Manager::TAB_LAYOUT
            ]
        );
        
        $element->add_control('premium_particles_switcher',
            [
                'label'         => __( 'Enable Particles', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SWITCHER,
            ]
        );
        
        $element->add_control('premium_particles_zindex',
            [
                'label'         => __( 'Z-index', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::NUMBER,
                'default'       => 0
            ]
        );
        
        $element->add_control('premium_particles_custom_style',
            [
                'label'         => __( 'Custom Style', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::TEXTAREA,
                'description'   => __( 'Paste your particles JSON code here - Generate it from <a href="http://vincentgarreau.com/particles.js/#default" target="_blank">Here!</a>', 'premium-addons-for-elementor' ),
            ]
        );

        $element->add_control('premium_particles_notice',
            [
                'raw'           => __( 'Kindly, be noted that you will need to add a background as particles JSON code doesn\'t include a background color', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::RAW_HTML,
            ]
        );
        
        $element->end_controls_section();
        
    }
    
    public function premium_particles_enqueue_scripts() {
        
        wp_enqueue_script('particles-js', PREMIUM_PRO_ADDONS_URL.'assets/js/lib/particles.js',array(), PREMIUM_PRO_ADDONS_VERSION, false);
        
        wp_enqueue_script('premium-pro-js', PREMIUM_PRO_ADDONS_URL . 'assets/js/premium-addons.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
        
    }
    
    public function before_render( $element ) {
        
        if( $element->get_settings('premium_particles_switcher') == 'yes' ){
            
            $element->add_render_attribute( '_wrapper','id', 'premium-section-particles-' . $element->get_id() );
            
        }
        
    }
    
    public function after_render( $element ) {
        
        $data               = $element->get_data();
        
        $type               = $data['elType'];
        
        $settings           = $data['settings'];
        
        $particles_zindex   = ! empty( $settings['premium_particles_zindex'] ) ? $settings['premium_particles_zindex'] : 0;
        
        if( 'section' == $type && $element->get_settings('premium_particles_switcher') == 'yes' ){
            
            if( !empty( $settings['premium_particles_custom_style'] ) ){
                
                ?>
                <style>
                    .elementor-element-<?php echo $element->get_id(); ?>.premium-particles-section > canvas{
                        z-index: <?php echo $particles_zindex; ?>;
                        position: absolute;
                        top:0;
                    }
                </style>
                
                <script>
                    ( function( $ ) {
                        "use strict";
                        $('.elementor-element-<?php echo $element->get_id(); ?>').addClass('premium-particles-section');
                        
                        particlesJS("premium-section-particles-<?php echo $element->get_id(); ?>", <?php echo $settings['premium_particles_custom_style']; ?> );
                        
                    }( jQuery ) );
                    
                </script>
                
            <?php }
        }
    }
}