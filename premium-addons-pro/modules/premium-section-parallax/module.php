<?php

/**
 * Class: Module
 * Name: Section Parallax
 * Slug: premium-parallax
 */

namespace PremiumAddonsPro\Modules\PremiumSectionParallax;

use Elementor;
use Elementor\Utils;
use Elementor\Repeater;
use Elementor\Element_Base;
use Elementor\Elementor_Base;
use Elementor\Controls_Manager;
use Elementor\Core\Responsive\Responsive;
use Elementor\Widget_Base;
use PremiumAddonsPro\Base\Module_Base;
use PremiumAddonsPro\Plugin;

if( !defined( 'ABSPATH' ) ) exit;

class Module extends Module_Base {
    
    public function __construct(){
        parent::__construct();
        
        //Checks if Section Parallax is enabled
        $check_parallax_active = isset(get_option( 'pa_pro_save_settings' )['premium-parallax']) ? get_option( 'pa_pro_save_settings' )['premium-parallax'] : 1;
        
        if( $check_parallax_active ) {
            
            //Creates Premium Prallax tab at the end of section layout tab
            add_action('elementor/element/section/section_layout/after_section_end',array($this,'register_controls'), 10 );
            
            //Renders parallax after section rendering
            add_action('elementor/frontend/section/after_render',array($this,'after_render'), 10);
            
            //Enqueue the required JS file
            add_action('wp_enqueue_scripts', array($this,'premium_parallax_enqueue_scripts'), 9);
        }
    }
    
    public function get_name(){
        return 'premium-parallax';
    }
    
    public function register_controls( $element ){
        
        $element->start_controls_section('premium_parallax_section',
            [
                'label'         => \PremiumAddons\Helper_Functions::get_prefix() . ' Parallax',
                'tab'           => Controls_Manager::TAB_LAYOUT
            ]
        );
        
        $element->add_control('premium_parallax_update',
            [
               'label'          => '<div class="elementor-update-preview" style="background-color: #fff;"><div class="elementor-update-preview-title">Update changes to page</div><div class="elementor-update-preview-button-wrapper"><button class="elementor-update-preview-button elementor-button elementor-button-success">Apply</button></div></div>',
                'type'          => Controls_Manager::RAW_HTML
            ]
        );
        
        $element->add_control('premium_parallax_switcher',
            [
				'label'         => __( 'Enable Parallax', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::SWITCHER,
			]
		);
        
		$element->add_control('premium_parallax_type',
			[
				'label'         => __( 'Type', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::SELECT,
				'options'       => [
					'scroll' 			=> __( 'Scroll', 'premium-addons-for-elementor' ),
					'scroll-opacity' 	=> __( 'Scroll + Opacity', 'premium-addons-for-elementor' ),
					'opacity' 			=> __( 'Opacity', 'premium-addons-for-elementor' ),
					'scale' 			=> __( 'Scale', 'premium-addons-for-elementor' ),
					'scale-opacity' 	=> __( 'Scale + Opacity', 'premium-addons-for-elementor' ),
                    'automove'          => __( 'Auto Moving Background', 'premium-addons-for-elementor' ),
                    'multi'             => __( 'Multi Layer Parallax', 'premium-addons-for-elementor')
				],
				'label_block'   => 'true',
			]
		);
        
        $repeater = new Elementor\Repeater();
        
        $repeater->add_control('premium_parallax_layer_image',
			[
				'label'         => __( 'Choose Image', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::MEDIA,
				'default'       => [
					'url' => Utils::get_placeholder_image_src(),
				],
                'label_block'   => true
			]
		);
        
        $repeater->add_control('premium_parallax_layer_hor_pos',
            [
                'label'         => esc_html__('Horizontal Position', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'description'   => esc_html__('Choose the horizontal position for the layer background, default: 50%','premium-addons-for-elementor'),
                'default'       => [
                    'size'  => 50
                ],
                'min'           => 0,
                'max'           => 100,
                'label_block'   => true,
            ]
        );
        
        $repeater->add_control('premium_parallax_layer_ver_pos',
            [
                'label'         => esc_html__('Vertical Position', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'default'       => [
                    'size'  => 50
                ],
                'min'           => 0,
                'max'           => 100,
                'description'   => esc_html__('Choose the vertical position for the layer background, default: 50%','premium-addons-for-elementor'),
                'label_block'   => true,
                ]
            );

        $repeater->add_control('premium_parallax_layer_back_size',
            [
                'label'         => esc_html__( 'Background Size', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'auto',
                'options'       => [
                    'auto'    => esc_html__( 'Auto', 'premium-addons-for-elementor' ),
                    'cover'   => esc_html__( 'Cover', 'premium-addons-for-elementor' ),
                    'contain' => esc_html__( 'Contain', 'premium-addons-for-elementor' ),
                ],
            ]
        );
        
        $repeater->add_control('premium_parallax_layer_z_index',
			[
				'label'         => __( 'z-index', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::NUMBER,
				'default'       => 1,
				'description'   => esc_html__( 'Choose z-index for the parallax layer, default: 3', 'premium-addons-for-elementor' ),
			]
		);
        
        $repeater->add_control('premium_parallax_layer_mouse',
            [
                'label'         => esc_html__('Mousemove Parallax', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SWITCHER,
                'description'   => esc_html__('Enable or disable mousemove interaction','premium-addons-for-elementor'),
            ]
        );
        
        $repeater->add_control('premium_parallax_layer_rate',
            [
                'label'         => esc_html__('Rate', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => -10,
                'min'           => -20,
                'max'           => 20,
                'step'          => 1,
                'description'   => esc_html__('Choose the movement rate for the layer background, default: -10','premium-addons-for-elementor'),
                'label_block'   => true,
                'condition'     => [
                    'premium_parallax_layer_mouse' => 'yes'
                ]
            ]
        );
        
		$element->add_control('premium_parallax_auto_type',
			[
				'label'         => __( 'Direction', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::SELECT,
				'default'       => 'left',
				'options'       => [
					'left'  => __( 'Left to Right', 'premium-addons-for-elementor' ),
					'right' => __( 'Right to Left', 'premium-addons-for-elementor' ),
					'top' 	=> __( 'Top to Bottom', 'premium-addons-for-elementor' ),
					'bottom'=> __( 'Bottom to Top', 'premium-addons-for-elementor' ),
				],
				'condition'     => [
					'premium_parallax_type'     => 'automove'
				],
			]
		);

		
		$element->add_control('premium_parallax_speed',
			[
				'label'         => __( 'Speed', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::SLIDER,
				'range'         => [
                    'px'  => [
                        'min'   => -1,
                        'max'   => 2,
                        'step'  => 0.1
                    ]
                ],
				'condition'     => [
                    'premium_parallax_type!'    => [ 'automove' , 'multi' ]
				],
			]
		);

        $element->add_control('premium_parallax_background_size',
            [
                'label'         => __( 'Background Size', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'auto'       => __( 'Auto', 'premium-addons-for-elementor' ),
                    'contain'    => __( 'Contain', 'premium-addons-for-elementor' ),
                    'cover'      => __( 'Cover', 'premium-addons-for-elementor' ),
                ],
                'default'       => 'cover',
                'label_block'   => 'true',
                'condition'     => [
                    'premium_parallax_type!'    => [ 'automove' , 'multi' ]
                ],
            ]
        );

        $element->add_control('premium_parallax_background_pos',
            [
                'label'         => __( 'Background Position', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::SELECT,
                'options'       => [
                    'center center'         => __( 'Center Center', 'premium-addons-for-elementor' ),
                    'center left'           => __( 'Center Left', 'premium-addons-for-elementor' ),
                    'center right'          => __( 'Center Right', 'premium-addons-for-elementor' ),
                    'top center'            => __( 'Top Center', 'premium-addons-for-elementor' ),
                    'bottom center'         => __( 'Bottom Center', 'premium-addons-for-elementor' ),
                    'top left'              => __( 'Top Left', 'premium-addons-for-elementor' ),
                    'top right'             => __( 'Top Right', 'premium-addons-for-elementor' ),
                    'bottom left'           => __( 'Bottom Left', 'premium-addons-for-elementor' ),
                    'bottom right'          => __( 'Bottom Right', 'premium-addons-for-elementor' ),
                ],
                'default'           => 'top center',
                'label_block'       => 'true',
                'condition'     => [
                    'premium_parallax_type!'    => [ 'automove' , 'multi' ]
                ],
            ]
        );
        
        $element->add_control('premium_auto_speed',
			[
				'label'         => __( 'Speed', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::NUMBER,
				'default'       => 3,
                'min'           => 0,
                'max'           => 150,
				'description'   => esc_html__( 'Set the speed of background movement, default: 3', 'premium-addons-for-elementor' ),
				'condition'     => [
                    'premium_parallax_type'     => 'automove'
				],
			]
		);
		
		$element->add_control('premium_parallax_android_support',
			[
				'label'         => esc_html__( 'Enable Parallax on Android', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::SWITCHER,
				'condition'     => [
                    'premium_parallax_type!'    => [ 'automove' , 'multi' ]
				],
			]
		);
		
		$element->add_control('premium_parallax_ios_support',
			[
				'label'         => esc_html__( 'Enable Parallax on iOS', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::SWITCHER,
				'condition'     => [
                    'premium_parallax_type!'    => [ 'automove' , 'multi' ]
				],
			]
		);
        
        $element->add_control('premium_parallax_notice',
            [
                'raw'           => __( 'Kindly, be noted that the option below works only with multi layer parallax effect', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::RAW_HTML,
            ]
        );
        
        $element->add_control('premium_parallax_layers_list',
            [
                'type'          => Controls_Manager::REPEATER,
                'fields'        => array_values( $repeater->get_controls() ),
            ]
        );
        
        $element->end_controls_section();
        
    }
    
    public function premium_parallax_enqueue_scripts() {
        
        wp_enqueue_script('tweenmax-js', PREMIUM_PRO_ADDONS_URL.'assets/js/lib/TweenMax.min.js',array( 'jquery' ), PREMIUM_PRO_ADDONS_VERSION, true);
        
        wp_enqueue_script('parallax-js', PREMIUM_PRO_ADDONS_URL.'assets/js/lib/jarallax.js',array( 'jquery' ), PREMIUM_PRO_ADDONS_VERSION, true);
        
        wp_enqueue_script('parallaxmouse-js', PREMIUM_PRO_ADDONS_URL.'assets/js/lib/jquery-parallax.js',array( 'jquery' ), PREMIUM_PRO_ADDONS_VERSION, true);
        
        wp_enqueue_script('premium-pro-js', PREMIUM_PRO_ADDONS_URL . 'assets/js/premium-addons.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
        
    }
    
    public function after_render( $element ) {
        
        $data       = $element->get_data();
        
        $type       = $data['elType'];
        
        $settings   = $data['settings'];
        
        $parallax   = isset( $settings['premium_parallax_type'] ) ? $settings['premium_parallax_type']: '';
        
        if( 'section' === $type && isset( $parallax ) && '' !== $parallax && 'yes' === $element->get_settings('premium_parallax_switcher') ) {
            
            $android    = ( isset( $settings['premium_parallax_android_support'] ) && $settings['premium_parallax_android_support'] == 'yes' ) ? 0 : 1;
            
            $ios        = ( isset( $settings['premium_parallax_ios_support'] ) && $settings['premium_parallax_ios_support'] == 'yes' ) ? 0 : 1;
            
            $speed      = ! empty( $settings['premium_parallax_speed']['size'] ) ? $settings['premium_parallax_speed']['size'] : 0.5;

            $img_size   = $element->get_settings('premium_parallax_background_size');
            
            $background_pos = isset( $settings['premium_parallax_background_pos'] ) ? $settings['premium_parallax_background_pos'] : '';

            $auto_speed = !empty( $settings['premium_auto_speed'] ) ? $settings['premium_auto_speed'] : 3;
            
            $repeater_list = ( isset( $settings['premium_parallax_layers_list'] ) && $settings['premium_parallax_layers_list'] ) ? $settings['premium_parallax_layers_list'] : array();
            
		?>		
			<script>	
				( function( $ ) {
                    
					"use strict";
                    var target = $('.elementor-element-<?php echo $element->get_id(); ?>');
                    
                    <?php if( 'automove' != $parallax && 'multi' != $parallax ) : ?>
                        
                    var premiumParallaxElement = {
                        
						init: function() {
                            
							elementorFrontend.hooks.addAction('frontend/element_ready/global', premiumParallaxElement.initWidget);
                            
						},
						initWidget: function( $scope ) {
                            
							target.jarallax({
								type: '<?php echo $parallax; ?>',
								speed: <?php echo $speed; ?>,
								keepImg: true,
								imgSize: '<?php echo $img_size; ?>',
								imgPosition: '<?php echo $background_pos; ?>',
								noAndroid: <?php echo $android; ?>,
								noIos: <?php echo $ios; ?>,
							});
						}
                        
					};
                    
					$( window ).on('elementor/frontend/init', premiumParallaxElement.init);
                    
                    <?php elseif ('multi' == $parallax ) : $counter = 0; ?>
            
                    target.addClass('premium-prallax-multi');
                    
                        <?php foreach( $repeater_list as $layer ): $counter = $counter + 1; ?>
                            
                            var backgroundImage = '<?php echo $layer['premium_parallax_layer_image']['url']; ?>',
                                mouseParallax   = ' data-parallax="' + <?php  echo ($layer['premium_parallax_layer_mouse'] == 'yes') ? 'true' : 'false'; ?> +'" ',
                                mouseRate       = ' data-rate="' + <?php echo $layer['premium_parallax_layer_rate']; ?> + '" ';
                        
                            $('<div id="premium-parallax-layer-<?php echo $element->get_id() . '-'. $counter; ?>"' + mouseParallax + mouseRate +' class="premium-parallax-layer"></div>').prependTo( target ).css({
                                'z-index'               : <?php echo ! empty( $layer['premium_parallax_layer_z_index'] ) ? $layer['premium_parallax_layer_z_index'] : 0 ; ?>,
                                'background-image'      : 'url(' + backgroundImage + ')',
                                'background-size'       : '<?php echo $layer['premium_parallax_layer_back_size']; ?>',
                                'background-position-x' : <?php echo ! empty( $layer['premium_parallax_layer_hor_pos']['size'] ) ? $layer['premium_parallax_layer_hor_pos']['size'] : 50 ; ?> + '%',
                                'background-position-y' : <?php echo ! empty( $layer['premium_parallax_layer_ver_pos']['size'] ) ? $layer['premium_parallax_layer_ver_pos']['size'] : 50 ; ?> + '%'
                        
                            });
                                
                        <?php endforeach; ?>
                        
                        if( $(window).outerWidth() > <?php echo esc_js ( Responsive::get_breakpoints()['md'] ); ?> ) {
                            
                                $('.elementor-element-<?php echo $element->get_id(); ?>').mousemove( function( e ) {
                            
                                $( this ).find('.premium-parallax-layer[data-parallax="true"]').each(function( index,element ){
                                
                                    $( this ).parallax( $( this ).data('rate'), e );
                                
                                });
                            
                            });
                        
                        }
                        
                        
                    <?php else: ?>
                        
                        target.css('background-position','0px 0px');
                        
						<?php if( 'left' == $settings['premium_parallax_auto_type'] ) : ?>
                            
                            var position = parseInt( target.css('background-position-x') );
                            setInterval( function() {
                                
                                position = position + <?php echo $auto_speed; ?>;
                                
                                target.css("backgroundPosition", position + "px 0");
                                
                            },70 );
                            
                    	<?php elseif( 'right' == $settings['premium_parallax_auto_type'] ) : ?>
                        
                    	var position = parseInt( target.css('background-position-x') );
                        
                            setInterval( function() {
                                
                                position = position - <?php echo $auto_speed; ?>;
                                
                                target.css("backgroundPosition", position + "px 0");
                                
                            },70 );
                            
                    	<?php elseif( 'top' == $settings['premium_parallax_auto_type'] ) : ?>
                        
                    	var position = parseInt(target.css('background-position-y'));
                        
                            setInterval( function() {
                                
                                position = position + <?php echo $auto_speed; ?>;
                                
                                target.css("backgroundPosition", "0 " + position + "px");
                                
                            },70 );
                            
                    	<?php elseif( 'bottom' == $settings['premium_parallax_auto_type'] ) : ?>
                        
                    	var position = parseInt( target.css('background-position-y') );
                        
                            setInterval( function() {
                                
                                position = position - <?php echo $auto_speed; ?>;
                                target.css("backgroundPosition", "0 " + position + "px");
                                
                            },70 );
                            
                    	<?php endif; ?>
                    <?php endif; ?>
				}( jQuery ) );
			</script>
		<?php }
	}    
}