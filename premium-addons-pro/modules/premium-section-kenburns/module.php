<?php

namespace PremiumAddonsPro\Modules\PremiumSectionKenburns;

use Elementor;
use Elementor\Utils;
use Elementor\Repeater;
use Elementor\Elementor_Base;
use Elementor\Controls_Manager;
use Elementor\Element_Base;
use Elementor\Widget_Base;
use PremiumAddonsPro\Base\Module_Base;
use PremiumAddonsPro\Plugin;

if( !defined( 'ABSPATH' ) ) exit;

class Module extends Module_Base {
    
    public function __construct(){
        parent::__construct();
        
        $check_gradient_active = isset(get_option( 'pa_pro_save_settings' )['premium-kenburns']) ? get_option( 'pa_pro_save_settings' )['premium-kenburns'] : 1;
        if($check_gradient_active){
            
            add_action( 'elementor/element/section/section_layout/after_section_end',array( $this,'register_controls'), 10 );
            
            add_action( 'elementor/frontend/section/after_render',array( $this,'after_render') );
            
            add_action( 'elementor/frontend/before_enqueue_scripts', array( $this, 'premium_kenburns_enqueue_scripts' ), 9 );
        }
    }
    
    public function get_name(){
        return 'premium-kenburns';
    }
    
    public function register_controls( $element ){
        
        $element->start_controls_section('premium_kenburns_section',
                [
                    'label'         => \PremiumAddons\Helper_Functions::get_prefix() . ' Ken Burns Effect',
                    'tab'           => Controls_Manager::TAB_LAYOUT
                    ]
                );
        
        $element->add_control('premium_kenburns_notice',
            [
                'raw'           => __( 'Add the images that you need, Save and Preview to see your changes', 'premium-addons-for-elementor' ),
                'type'          => Controls_Manager::RAW_HTML,
            ]
        );
        
        $element->add_control('premium_kenburns_switcher',
            [
				'label'             => __( 'Enable Ken Burns Effect', 'premium-addons-for-elementor' ),
				'type'              => Controls_Manager::SWITCHER,
			]
		);
		
        $repeater = new Repeater();
		
        $repeater->add_control('premium_kenburns_images',
			[
				'label'             => __( 'Upload Image', 'premium-addons-for-elementor' ),
				'type'              => Controls_Manager::MEDIA,
                'label_block'       => true,
			]
		);
        
        $repeater->add_control('premium_kenburns_image_fit',
			[
				'label'         => __( 'Image Fit', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::SELECT,
				'options'       => [
					'pa-fill'       => __( 'Fill', 'premium-addons-for-elementor' ),
                    'pa-contain'    => __( 'Contain', 'premium-addons-for-elementor' ),
					'pa-cover'      => __( 'Cover', 'premium-addons-for-elementor' ),
				],
                'default'       => 'pa-fill',
				'label_block'   => 'true',
			]
		);
        
        $repeater->add_control('premium_kenburns_dir',
			[
				'label'         => __( 'Direction', 'premium-addons-for-elementor' ),
				'type'          => Controls_Manager::SELECT,
				'options'       => [
					'center' 		=> __( 'Center Center', 'premium-addons-for-elementor' ),
                    'cl'            => __( 'Center Left', 'premium-addons-for-elementor' ),
					'cr' 			=> __( 'Center Right', 'premium-addons-for-elementor' ),
					'tc' 			=> __( 'Top Center', 'premium-addons-for-elementor' ),
					'bc'            => __( 'Bottom Center', 'premium-addons-for-elementor' ),
					'tl'            => __( 'Top Left', 'premium-addons-for-elementor' ),
					'tr' 			=> __( 'Top Right', 'premium-addons-for-elementor' ),
					'bl' 			=> __( 'Bottom Left', 'premium-addons-for-elementor' ),
					'br'            => __( 'Bottom Right', 'premium-addons-for-elementor' ),
				],
                'default'           => 'center',
				'label_block'       => 'true',
			]
		);
        
        $element->add_control('premium_kenburns_repeater',
				[
					'type'          => Controls_Manager::REPEATER,
					'fields'        => array_values( $repeater->get_controls() ),
				]
			);
        
        $element->add_control('premium_kenburns_speed',
            [
                'label'         => esc_html__('Transition Speed (sec)', 'premium-addons-for-elementor'),
                'type'          => Controls_Manager::SLIDER,
                'range'         => [
                    'px'    => [
                        'min'   => 1,
                        'max'   => 10,
                        'step'  => 0.1
                    ]
                ],
                'min'           => 0,
                'label_block'   => true,
                'selectors'     => [
                    '{{WRAPPER}} .kenburns-scale' => '-webkit-animation-duration: {{SIZE}}s; animation-duration: {{SIZE}}s;'
                ]
            ]
        );
        
        $element->end_controls_section();
         
   }
    
    public function premium_kenburns_enqueue_scripts(){
        
        wp_enqueue_script('cycle', PREMIUM_PRO_ADDONS_URL . 'assets/js/lib/cycle.min.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
        
        wp_enqueue_script('premium-pro-js', PREMIUM_PRO_ADDONS_URL . 'assets/js/premium-addons.js', array('jquery'), PREMIUM_PRO_ADDONS_VERSION, true);
        
    }
    
    public function after_render( $element ) {
        
		$data 		= $element->get_data();
        
        $type       = $data['elType'];
        
        $settings   = $data['settings'];
        
		if( 'section' == $type && 'yes' == $element->get_settings('premium_kenburns_switcher') && isset( $settings['premium_kenburns_repeater'] ) ) {
            
            $slides_repeater = $settings['premium_kenburns_repeater'];
            
            $transition = 1000 * ( ( isset( $settings['premium_kenburns_speed'] ) && !empty( $settings['premium_kenburns_speed']['size'] ) ) ? $settings['premium_kenburns_speed']['size'] : 6.5 );
            
		?>
        <script>	
            ( function($) {
                "use strict";
                var section = $('section.elementor-element-<?php echo $element->get_id(); ?>');
//                new_effect,
//                old_effect,
//                position = ['tr','tl','bl','br'];
        
                section.addClass('cycle-slideshow');
                
                section.attr('data-cycle-timeout', <?php echo esc_attr( $transition ) ?> - 900 );
                
//                function random_effect() {
//                    
//                    var randomIndex = Math.floor(Math.random() * 4);
//                    
//                    return position[randomIndex];
//                    
//                }
                
                function generateLayout() {
                    
                    <?php
                    
                    $layout = '';
                    
                    foreach( $slides_repeater as $slide ) {
                        
                        $layout .= '<img class="premium-kenburns-img ' . $slide['premium_kenburns_image_fit'] .' premium-kenburns-' . $slide['premium_kenburns_dir'] . '" src="' . $slide['premium_kenburns_images']['url'] . '">';
                        
                    } ?>
                                
                    section.prepend('<?php echo $layout; ?>');
                    
                }
                   
              var sectionIndex = section.find('.elementor-container').css("z-index");
                    
                generateLayout();
                    
//              new_effect = random_effect();
                
                section.find('.premium-kenburns-img:first-child').addClass('kenburns-scale');
                    
                section.find('.premium-kenburns-img:first-child').addClass('premium-kenburns-center');
                
                
                    
                if ( 0 === sectionIndex || 'auto' === sectionIndex ) {
                    section.find('.elementor-container').css("z-index", "1");
                }
                    
                    $('.cycle-slideshow').on('cycle-before', function( event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag ) {
                        
//                        old_effect = new_effect;
//                        
//                        new_effect = random_effect();
//                        
                        $( incomingSlideEl ).addClass('kenburns-scale');
                        
//                        $(incomingSlideEl).addClass('premium-kenburns-' + new_effect);
                        
                    });

                    $('.cycle-slideshow').on('cycle-after', function( event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag ) {
                        
                        $( outgoingSlideEl ).removeClass('kenburns-scale');
                        
//                        $(outgoingSlideEl).removeClass('premium-kenburns-' + old_effect);
                        
                    });
                
            }( jQuery ) );
            
        </script>
		<?php }
	}    
}