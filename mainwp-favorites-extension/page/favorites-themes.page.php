<?php

class Favorites_Themes {
	public static $instance = null;

	static function get_instance() {
		if ( null == Favorites_Themes::$instance ) {
			Favorites_Themes::$instance = new Favorites_Themes();
		}

		return Favorites_Themes::$instance;
	}

	function render_page() {
		$nameorder = '';
		$orderby   = 'fav.name';
		if ( isset( $_GET['orderby'] ) ) {
			if ( ( 'name' == $_GET['orderby'] ) ) {
				$orderby   = 'fav.name ' . ( 'asc' == $_GET['order'] ? 'asc' : 'desc' );
				$nameorder = 'asc' == $_GET['order'] ? 'desc' : 'asc';
			}
		} else {
			$nameorder = 'desc';
		}

		Favorites_Extension::validate_favories( 'theme' );
		?>
		<div class="wrap" id="mainwp-ap-option">
			<div class="clearfix"></div>
			
			<div class="inside">
				<div id="favorites_select_sites_box" class="mainwp_config_box_right">
					<?php do_action( 'mainwp_select_sites_box', __( 'Select Sites', 'mainwp' ), 'checkbox', true, true, 'mainwp_select_sites_box_right' ); ?>
				</div>
				<div class="mainwp_config_box_left" style="width: calc(100% - 290px);">
					<div class="error below-h2" style="display: none;" id="ajax-error-zone"></div>
					<div class="postbox">
						<h3 class="mainwp_box_title"><i class="fa fa-cog"></i> <?php _e( 'Favorites Options', 'mainwp' ); ?></h3>
						<div class="inside">
							<input type="checkbox" value="2" checked id="chk_overwrite"/> <label for="chk_overwrite"><?php _e( 'Overwrite existing', 'mainwp' ); ?></label>
							<a href="?page=Extensions-Mainwp-Favorites-Extension&tab=theme" class="button button-hero button-primary" style="float: right;"><?php echo __( 'Favorites Settings' ); ?></a>
							<div style="clear:both;"></div>
						</div>
					</div>
					<table id="mainwp-table" class="wp-list-table widefat" cellspacing="0">
						<thead>
						<tr>
							<th scope="col" id="role" class="manage-column column-posts" style=""><a
									href="?page=ThemesFavorite&orderby=name&order=<?php echo( '' == $nameorder ? 'asc' : $nameorder ); ?>"><?php _e( 'Theme', 'mainwp' ); ?></a>
							</th>
							<th scope="col" id="role" class="manage-column column-posts"
							    style=""><?php _e( 'Version', 'mainwp' ); ?></th>
							<th scope="col" id="role" class="manage-column column-posts"
							    style=""><?php _e( 'Notes', 'mainwp' ); ?></th>
						</tr>
						</thead>

						<tfoot>
						<tr>
							<th scope="col" id="role" class="manage-column column-posts" style=""><a
									href="?page=ThemesFavorite&orderby=name&order=<?php echo( '' == $nameorder ? 'asc' : $nameorder ); ?>"><?php _e( 'Theme', 'mainwp' ); ?></a>
							</th>
							<th scope="col" id="role" class="manage-column column-posts"
							    style=""><?php _e( 'Version', 'mainwp' ); ?></th>
							<th scope="col" id="role" class="manage-column column-posts"
							    style=""><?php _e( 'Notes', 'mainwp' ); ?></th>
						</tr>
						</tfoot>

						<tbody id="the-sites-list" class="list:sites">
						<?php $this->get_table_content( $orderby ); ?>
						</tbody>

					</table>
					<br/><br/>
					<table id="mainwp-table" class="wp-list-table widefat" cellspacing="0">
						<thead>
						<tr>
							<th scope="col" id="role" class="manage-column column-posts" style="">
								<strong><?php _e( 'Theme Groups', 'mainwp' ); ?></strong></th>
							<th scope="col" id="role" class="manage-column column-posts"
							    style=""><?php _e( 'Themes', 'mainwp' ); ?></th>
							<th scope="col" id="role" class="manage-column column-posts"
							    style=""><?php _e( 'Notes', 'mainwp' ); ?></th>
						</tr>
						</thead>

						<tfoot>
						<tr>
							<th scope="col" id="role" class="manage-column column-posts" style="">
								<strong><?php _e( 'Theme Groups', 'mainwp' ); ?></strong></th>
							<th scope="col" id="role" class="manage-column column-posts"
							    style=""><?php _e( 'Themes', 'mainwp' ); ?></th>
							<th scope="col" id="role" class="manage-column column-posts"
							    style=""><?php _e( 'Notes', 'mainwp' ); ?></th>
						</tr>
						</tfoot>

						<tbody id="the-sites-list" class="list:sites">
						<?php $this->get_table_groups_content(); ?>
						</tbody>

					</table>

				</div>

			</div>

			<br class="clear"/>

			<div id="favorite_notes_overlay" class="mainwp_overlay"></div>
			<div id="favorite_notes" class="mainwp_popup">
				<a id="favorite_notes_closeX" class="mainwp_closeX" style="display: inline; "></a>

				<div id="favorite_notes_title" class="mainwp_popup_title"></span>
				</div>
				<div id="favorite_notes_content">
                                        <textarea style="width: 580px !important; height: 300px;"
                                                  id="favorite_notes_note"></textarea>
				</div>
				<form>
					<div style="float: right" id="favorite_notes_status"></div>
					<input type="button" class="button cont button-primary" id="favorite_notes_save"
					       value="<?php _e( 'Save Note', 'mainwp' ); ?>"/>
					<input type="button" class="button cont" id="favorite_notes_cancel"
					       value="<?php _e( 'Close', 'mainwp' ); ?>"/>
					<input type="hidden" id="favorite_notes_id" value=""/>
				</form>
			</div>

			<div id="group_notes_overlay" class="mainwp_overlay"></div>
			<div id="group_notes" class="mainwp_popup">
				<a id="group_notes_closeX" class="mainwp_closeX" style="display: inline; "></a>

				<div id="group_notes_title" class="mainwp_popup_title"></span>
				</div>
				<div id="group_notes_content">
                                    <textarea style="width: 580px !important; height: 300px;"
                                              id="group_notes_note"></textarea>
				</div>
				<form>
					<div style="float: right" id="group_notes_status"></div>
					<input type="button" class="button cont button-primary" id="group_notes_save"
					       value="<?php _e( 'Save Note', 'mainwp' ); ?>"/>
					<input type="button" class="button cont" id="group_notes_cancel"
					       value="<?php _e( 'Close', 'mainwp' ); ?>"/>
					<input type="hidden" id="group_notes_id" value=""/>
				</form>
			</div>

		</div>
		<script>
			jQuery(document).ready(function () {
				favorites_install_set_install_links();
				group_install_set_install_links();
			});
		</script>

		<?php
	}

	public function get_table_content( $orderby ) {

		$favorites = Favorites_Extension_DB::get_instance()->query( Favorites_Extension_DB::get_instance()->get_sql_favorites_for_current_user( 'theme', false, $orderby ) );
		while ( $favorites && ( $favorite = @Favorites_Extension_DB::fetch_object( $favorites ) ) ) {
			$download_url = ! empty( $favorite->file ) ? Favorites_Extension::get_instance()->getFavoritesDownloadUrl('themes', $favorite->file ) : '';
			?>
			<tr id="fav-<?php echo $favorite->id; ?>" favorite_id="<?php echo rawurlencode( $favorite->id ); ?>"
			    favorite_name="<?php echo urlencode( $favorite->name ); ?>">
				<td scope="row" class="">
					<?php
					if ( ! empty( $favorite->slug ) ) { ?>
						<a title="<?php echo __( 'Preview' ) . ' ' . $favorite->name; ?>"
						   class="thickbox thickbox-preview onclick previewlink" target="_blank"
						   href="http://wp-themes.com/<?php echo $favorite->slug; ?>?TB_iframe=true&width=600&height=400"><?php echo $favorite->name; ?></a>
					<?php } else { ?>
						<strong><?php echo $favorite->name; ?></strong>
					<?php } ?>
					<br/>

					<div id="fav-status-<?php echo $favorite->id; ?>" style="float: left; padding-right: 20px"></div>
					<div class="row-actions">
						<span class="edit"><a
								id="fav-install-theme-<?php echo( ! empty( $favorite->slug ) ? $favorite->slug : '' ); ?>"
								download-url="<?php echo $download_url; ?>"
								href="admin.php?page=ThemesFavorite"><i class="fa fa-download" aria-hidden="true"></i> <?php _e( 'Install', 'mainwp' ); ?></a> | </span>
						<span class="delete"><a class="submitdelete" href="#"
						                        onClick="return managefavorite_remove(<?php echo "'" . $favorite->type . "', '" . $favorite->file . "', " . $favorite->id; ?>)"><i class="fa fa-trash-o" aria-hidden="true"></i> <?php _e( 'Delete', 'mainwp' ); ?></a></span>
					</div>
				</td>
				<td class=""><?php echo $favorite->version; ?></td>
				<td class="">
					<a href="#" class="favorite_notes_show"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?php _e( 'Open', 'mainwp' ); ?></a>

					<div style="display: none" class="note"><?php echo $favorite->note; ?></div>
				</td>
			</tr>
			<?php
		}
	}

	public function get_table_groups_content() {

		$groups = Favorites_Extension_DB::get_instance()->get_groups( 'theme', true );

		foreach ( $groups as $group ) {

			?>
			<tr id="group-<?php echo $group->id; ?>" group_id="<?php echo rawurlencode( $group->id ); ?>"
			    group_name="<?php echo urlencode( $group->name ); ?>">
				<td scope="row" class=""><strong><?php echo stripslashes( $group->name ); ?></strong></a>
					<br/>

					<div id="group-status-<?php echo $group->id; ?>" style="float: left; padding-right: 20px"></div>
					<div class="row-actions">
						<?php if ( ! empty( $group->favorites ) ) { ?> <span class="edit"><a
								id="group-install-theme-<?php echo $group->id; ?>"
								group-name="<?php echo $group->name; ?>"
								href="admin.php?page=PluginsFavorite"><i class="fa fa-download" aria-hidden="true"></i> <?php _e( 'Install', 'mainwp' ); ?></a> | <?php } ?></span>
						<span class="delete"><a class="submitdelete" href="#"
						                        onClick="return managegroup_remove(<?php echo "'" . $group->type . "', " . $group->id; ?>)"><i class="fa fa-trash-o" aria-hidden="true"></i> <?php _e( 'Delete', 'mainwp' ); ?></a></span>
					</div>
				</td>
				<td class=""><?php echo $group->favorites; ?></td>
				<td class="">
					<a href="#" class="group_notes_show"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?php _e( 'Open', 'mainwp' ); ?></a>

					<div style="display: none" class="note"><?php echo $group->note; ?></div>
				</td>
			</tr>
			<?php
		}
	}
}

?>
