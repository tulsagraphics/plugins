<?php

class Manage_Favorites {
	private static $instance = null;
	private $errors = array();

	function __construct() {

	}

	//Constructor

	static function get_instance() {

		if ( null == Manage_Favorites::$instance ) {
			Manage_Favorites::$instance = new Manage_Favorites();
		}

		return Manage_Favorites::$instance;
	}

	static function add_favorite() {

		global $current_user;
		if ( intval( $current_user->ID ) && ! empty( $_POST['type'] ) && ! empty( $_POST['slug'] ) ) {
			include_once( ABSPATH . '/wp-admin/includes/plugin-install.php' );
			if ( 'plugin' == $_POST['type'] ) {
				$api = plugins_api( 'plugin_information', array(
					'slug'   => $_POST['slug'],
					'fields' => array( 'sections' => false ),
				) );
			} else {
				$api = themes_api( 'theme_information', array(
					'slug'   => $_POST['slug'],
					'fields' => array( 'sections' => false ),
				) );
			}
			if ( ! is_wp_error( $api ) ) {
				$result = Favorites_Extension_DB::get_instance()->add_favorite( $current_user->ID, $_POST['type'], $_POST['slug'], $api->name, $api->version );
				if ( 'NEWER_EXISTED' == $result ) {
					die( $result );
				} else if ( $result ) {
					die( 'SUCCESS' );
				}
			} else {
				die( 'FAIL' );
			}
		}
		die( 'FAIL' );
	}

	public static function add_group() {

		global $current_user;
		if ( isset( $_POST['newName'] ) ) {
			$groupId = Favorites_Extension_DB::get_instance()->add_group( $current_user->ID, self::check_group_name( $_POST['type'], $_POST['newName'] ), $_POST['type'] );
			$group   = Favorites_Extension_DB::get_instance()->get_favorites_group_by_id( $groupId );
			self::create_group_item( $_POST['type'], $group );
			die();
		}
		die( 'ERROR' );
	}

	protected static function check_group_name( $type, $groupName, $groupId = null ) {

		if ( '' == $groupName ) {
			$groupName = __( 'New Group' );
		}

		$cnt = null;
		if ( preg_match( '/(.*) \(\d\)/', $groupName, $matches ) ) {
			$groupName = $matches[1];
		}

		$group = Favorites_Extension_DB::get_instance()->get_group_by_name_for_user( $type, $groupName );
		while ( $group && ( ( null == $groupId ) || ( $group->id != $groupId ) ) ) {
			if ( null == $cnt ) {
				$cnt = 1;
			} else {
				$cnt ++;
			}

			$group = Favorites_Extension_DB::get_instance()->get_group_by_name_for_user( $type, $groupName . ' (' . $cnt . ')' );
		}

		return $groupName . ( null == $cnt ? '' : ' (' . $cnt . ')' );
	}

	private static function create_group_item( $type, $group ) {

		?>
		<li class="managefavgroups-listitem" id="<?php echo base64_encode( $group->id ); ?>">
			<span class="mainwp_group-actions actions-text hidden"><a class="managefavgroups-rename"
			                                                          href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?php _e( 'Rename', 'mainwp-favorites-extension' ); ?></a> | <a
					class="managefavgroups-delete" href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> <?php _e( 'Delete', 'mainwp-favorites-extension' ); ?></a></span>
			<span class="mainwp_group-actions actions-input hidden"><a class="managefavgroups-save"
			                                                           href="#"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php _e( 'Save', 'mainwp-favorites-extension' ); ?></a> | <a
					class="managefavgroups-delete" href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> <?php _e( 'Delete', 'mainwp-favorites-extension' ); ?></a></span>
            <span class="mainwp-radio">
            	<input type="radio" id="rad-<?php echo $group->id; ?>" class="managefavgroups-radio"
	                   value="<?php echo base64_encode( $group->id ); ?>" name="<?php echo $type; ?>-groups">
            	<label for="rad-<?php echo $group->id; ?>"></label>
            </span>
			<span class="text"><?php echo stripslashes( $group->name ); ?></span>

			<span class="input hidden"><input type="text" value="<?php echo $group->name; ?>" name="name"></span>
		</li>
		<?php
	}

	public static function delete_group() {
		if ( isset( $_POST['groupId'] ) && Favorites_Extension::ctype_digit( base64_decode( $_POST['groupId'] ) ) ) {
			$group = Favorites_Extension_DB::get_instance()->get_favorites_group_by_id( base64_decode( $_POST['groupId'] ) );
			if ( Favorites_Extension::can_edit_group( $group ) ) {
				//Remove from DB
				$nr = Favorites_Extension_DB::get_instance()->remove_group( $group->id );

				if ( $nr > 0 ) {
					die( 'OK' );
				}
			}
		}
		die( 'ERROR' );
	}

	public static function rename_group() {

		if ( isset( $_POST['groupId'] ) && Favorites_Extension::ctype_digit( base64_decode( $_POST['groupId'] ) ) ) {
			$group = Favorites_Extension_DB::get_instance()->get_favorites_group_by_id( base64_decode( $_POST['groupId'] ) );
			if ( Favorites_Extension::can_edit_group( $group ) ) {
				$name = $_POST['newName'];
				if ( '' == $name ) {
					$name = $group->name;
				}

				$name = self::check_group_name( $_POST['type'], $name, $group->id );
				//update group
				$nr = Favorites_Extension_DB::get_instance()->update_group( $group->id, $name );

				//Reload group
				$group = Favorites_Extension_DB::get_instance()->get_favorites_group_by_id( $group->id );
				die( $group->name );
			}
		}
	}


    public static function on_load_page() {
        $i = 0;
        do_action('mainwp_enqueue_meta_boxes_scripts');
        add_meta_box(
                'favor-settings-contentbox-' . $i++,
                '<i class="fa fa-cog"></i> ' . __( 'Settings', 'mainwp-favorites-extension' ),
                array( 'Manage_Favorites', 'render_settings' ),
                'mainwp_favorites_postboxes_settings',
                'normal',
                'core',
                array()
        );
    }

    static function render_settings() {

        $settings = Favorites_Extension::get_instance()->settings;
        $custom_folder = isset($settings['custom_folder']) ? $settings['custom_folder'] : '';
        ?>
        <form method="post">
            <table class="form-table">
                <tbody>
                <tr>
                    <th scope="row"><?php _e( 'Custom Favorites folder', 'mainwp-favorites-extension' ); ?></th>
                    <td>
                        <input type="text" name="custom_folder" size="35"
                               value="<?php echo esc_html( $custom_folder ); ?>"/><br/>
												<em><strong><?php echo "For example: " . get_home_path() . 'custom_directory'; ?></strong></em><br/><br/>
                        <em><?php _e( 'Changing the favorites directory will update the Plugins and Themes that are added from the WordPress.org', 'mainwp-favorites-extension' ); ?></em>
                    </td>
                </tr>
                </tbody>
            </table>
            <input type="hidden" name="favor_nonce" value="<?php echo wp_create_nonce('favor-nonce'); ?>"/>
            <input type="submit" class="button button-primary button button-hero" name="save_settings" value="<?php _e('Save Settings', 'mainwp-favorites-extension'); ?>">
        </form>
        <?php
    }


    public function uploadFile( $file ) {
		header( 'Content-Description: File Transfer' );
		if ( self::endsWith( $file, '.tar.gz' ) ) {
			header( 'Content-Type: application/x-gzip' );
			header( "Content-Encoding: gzip" );
		} else {
			header( 'Content-Type: application/octet-stream' );
		}
		header( 'Content-Disposition: attachment; filename="' . basename( $file ) . '"' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate' );
		header( 'Pragma: public' );
		header( 'Content-Length: ' . filesize( $file ) );
		while ( @ob_get_level() ) {
			@ob_end_clean();
		}
		$this->readfile_chunked( $file );
		exit();
	}

    function readfile_chunked( $filename ) {
		$chunksize = 1024; // how many bytes per chunk
		$handle    = @fopen( $filename, 'rb' );
		if ( $handle === false ) {
			return false;
		}

		while ( ! @feof( $handle ) ) {
			$buffer = @fread( $handle, $chunksize );
			echo $buffer;
			@ob_flush();
			@flush();
			$buffer = null;
		}

		return @fclose( $handle );
	}

    public static function endsWith( $haystack, $needle ) {
		$length = strlen( $needle );
		if ( $length == 0 ) {
			return true;
		}

		return ( substr( $haystack, - $length ) === $needle );
	}

	public static function get_favorites() {

		if ( isset( $_POST['groupId'] ) && Favorites_Extension::ctype_digit( base64_decode( $_POST['groupId'] ) ) ) {
			$group = Favorites_Extension_DB::get_instance()->get_favorites_group_by_id( base64_decode( $_POST['groupId'] ) );

			if ( Favorites_Extension::can_edit_group( $group ) ) {
				$favorites   = Favorites_Extension_DB::get_instance()->get_favorites_by_group_id( $group->id );
				$favoriteIds = array();
				if ( ! empty( $favorites ) ) {
					foreach ( $favorites as $favorite ) {
						$favoriteIds[] = base64_encode( $favorite->id );
					}
				}

				return json_encode( $favoriteIds );
			}
		}
		die( 'ERROR' );
	}

	public static function save_favorite_note() {

		if ( isset( $_POST['favoriteid'] ) && Favorites_Extension::ctype_digit( $_POST['favoriteid'] ) ) {
			$favorite = Favorites_Extension_DB::get_instance()->get_favorite_by( 'id', $_POST['favoriteid'] );
			if ( false !== Favorites_Extension_DB::get_instance()->update_favorite_note( $favorite->id, $_POST['note'] ) ) {
				die( 'SUCCESS' );
			} else {
				die( 'ERROR' );
			}
		}
		die( 'ERROR' );
	}

	public static function save_group_note() {

		if ( isset( $_POST['groupid'] ) && Favorites_Extension::ctype_digit( $_POST['groupid'] ) ) {
			$group = Favorites_Extension_DB::get_instance()->get_group_by( 'id', $_POST['groupid'] );
			if ( false !== Favorites_Extension_DB::get_instance()->update_group_note( $group->id, $_POST['note'] ) ) {
				die( 'SUCCESS' );
			} else {
				die( 'ERROR' );
			}
		}
		die( 'ERROR' );
	}

	public static function update_group() {

		if ( isset( $_POST['groupId'] ) && Favorites_Extension::ctype_digit( base64_decode( $_POST['groupId'] ) ) ) {
			$group = Favorites_Extension_DB::get_instance()->get_favorites_group_by_id( base64_decode( $_POST['groupId'] ) );
			if ( Favorites_Extension::can_edit_group( $group ) ) {
				Favorites_Extension_DB::get_instance()->clear_group( $group->id );
				if ( isset( $_POST['favoriteIds'] ) ) {
					foreach ( $_POST['favoriteIds'] as $favoriteId ) {
						$favorite = Favorites_Extension_DB::get_instance()->get_favorite_by( 'id', base64_decode( $favoriteId ) );
						if ( Favorites_Extension::can_edit_favorite( $favorite ) ) {
							Favorites_Extension_DB::get_instance()->update_group_site( $group->id, $favorite->id );
						}
					}
				}
				die();
			}
		}
	}

	function render_manage() {

        $updated = false;
        if ( isset( $_GET['updated'] ) ) {
            $updated = true;
        }

        $show_plugins_tab = $show_themes_tab = $show_settings_tab = false;

        if (isset($_GET['tab'])) {
            if ($_GET['tab'] == 'plugin') {
                $show_plugins_tab = true;
            } else if ($_GET['tab'] == 'theme') {
                $show_themes_tab = true;
            } else if ($_GET['tab'] == 'setting') {
                $show_settings_tab = true;
            } else {
                $show_plugins_tab = true;
            }
        } else {
            $show_plugins_tab = true;
        }

        $fav_plugins_link = '<a href="admin.php?page=Extensions-Mainwp-Favorites-Extension" class="mainwp_action ' . ($show_plugins_tab? 'mainwp_action_down selected' : '') . '">' . __( 'Plugins', 'mainwp-favorites-extension' ) . '</a>';
        $fav_themes_link = '<a href="admin.php?page=Extensions-Mainwp-Favorites-Extension&tab=theme" class="mainwp_action ' . ($show_themes_tab ? 'mainwp_action_down selected' : '') . '">' . __( 'Themes', 'mainwp-favorites-extension' ) . '</a>';
        $fav_settings_link = '<a href="admin.php?page=Extensions-Mainwp-Favorites-Extension&tab=setting" class="mainwp_action ' . ($show_settings_tab ? 'mainwp_action_down selected' : '') . '">' . __( 'Settings', 'mainwp-favorites-extension' ) . '</a>';


        ?>
			<script>
				function createUploaderPlugins() {
					var uploader = new qq.FileUploader({
						element: document.getElementById('favorites-file-uploader-plugin'),
						action: location.href,
						template: '<div class="qq-uploader">' +
						'<div class="qq-upload-drop-area"><span>Drop files here to upload</span></div>' +
						'<div class="qq-upload-button button button-hero button-primary">Add / Update Plugin</div>' +
						'<ul class="qq-upload-list"></ul>' +
						'</div>',
						onComplete: function (id, fileName, result) {
							favorites_setting_uploadbulk_oncomplete(id, fileName, result)
						},
						params: {favorites_do: 'FavoritesInstallBulk-uploadfile', type: 'plugin'}
					});
				}
				function createUploaderThemes() {
					var uploader = new qq.FileUploader({
						element: document.getElementById('favorites-file-uploader-theme'),
						action: location.href,
						template: '<div class="qq-uploader">' +
						'<div class="qq-upload-drop-area"><span>Drop files here to upload</span></div>' +
						'<div class="qq-upload-button button button-hero button-primary">Add / Update Theme</div>' +
						'<ul class="qq-upload-list"></ul>' +
						'</div>',
						onComplete: function (id, fileName, result) {
							favorites_setting_uploadbulk_oncomplete(id, fileName, result)
						},
						params: {favorites_do: 'FavoritesInstallBulk-uploadfile', type: 'theme'}
					});
				}
			</script>

            <div class="mainwp-subnav-tabs">
                <div><?php echo $fav_plugins_link . $fav_themes_link . $fav_settings_link ?></div>
                <div style="clear:both;"></div>
            </div>
            <br/>
			<?php
			Favorites_Extension::validate_favories( 'plugin' );
			Favorites_Extension::validate_favories( 'theme' );
			?>
			<div class="mainwp_error" id="favorites-pth-error-box"></div>
            <div class="mainwp-notice mainwp-notice-green" id="favorites-pth-info-box" <?php echo $updated ? '' : 'style="display:none"'; ?>>
                <?php
                if ($updated) {
                    _e('Settings saved', 'mainwp-favorites-extension');
                }
                ?>
            </div>
            <?php
            if ($show_plugins_tab) {
            ?>
			<div class="favorites-container-box">
				<input type="hidden" class="hidden-fav-type" value="plugin"/>

				<div class="favorites-container">
					<?php echo Manage_Favorites::render_groups( 'plugin' ); ?>
				</div>
				<div class="favorites-container">
					<?php echo Manage_Favorites::render_favorites( 'plugin' ); ?>
				</div>
				<div class="favorites-container last">
					<?php echo Manage_Favorites::render_upload( 'plugin' ); ?>
				</div>
			</div>
            <?php } ?>
            <?php
            if ($show_themes_tab) {
            ?>
			<div class="favorites-container-box">
				<input type="hidden" class="hidden-fav-type" value="theme"/>

				<div class="favorites-container">
					<?php echo Manage_Favorites::render_groups( 'theme' ); ?>
				</div>
				<div class="favorites-container">
					<?php echo Manage_Favorites::render_favorites( 'theme' ); ?>
				</div>
				<div class="favorites-container last">
					<?php echo Manage_Favorites::render_upload( 'theme' ); ?>
				</div>
			</div>
            <?php } ?>

            <?php
            if ($show_settings_tab) {
                do_action( 'mainwp_do_meta_boxes', 'mainwp_favorites_postboxes_settings');
            }
	}

	static function render_groups( $type ) {

		?>
		<div class="mainwp_managegroups-outsidebox">
			<div class="mainwp_managegroups-insidebox">
			<h3><?php echo ( 'plugin' == $type ) ? __( 'Plugin Groups', 'mainwp-favorites-extension' ) : __( 'Theme Groups', 'mainwp-favorites-extension' ); ?> <input type="button" style="float: right; margin-top: -5px;" class="managefavgroups-addnew button-primary button" value="Add New" name="Add new"></h3>

				<ul id="managefavgroups-list" class="mainwp-favorites-list">
					<li class="managefavgroups-listitem managefavgroups-group-add hidden">
						<span class="mainwp_group-actions actions-input"><a class="managefavgroups-savenew" href="#"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php _e( 'Save', 'mainwp-favorites-extension' ); ?></a> | <a
								class="managefavgroups-cancel" href="#"><i class="fa fa-times" aria-hidden="true"></i> <?php _e( 'Cancel', 'mainwp-favorites-extension' ); ?></a></span>
						<input type="text" value="" name="name">
					</li>
					<?php echo Manage_Favorites::get_group_list_content( $type ); ?>
				</ul>
			</div>
		</div>
		<div style="clear: both;"></div>
		<br>
		<a href="?page=<?php echo ( 'plugin' == $type ) ? 'Plugins' : 'Themes'; ?>Favorite"
		   class="button button-hero"><?php echo __( 'Install Favorite', 'mainwp-favorites-extension' ); ?><?php echo ( 'plugin' == $type ) ? __( 'Plugins', 'mainwp-favorites-extension' ) : __( 'Themes', 'mainwp-favorites-extension' ); ?></a>
		<?php
	}

	public static function get_group_list_content( $type ) {

		$groups = Favorites_Extension_DB::get_instance()->get_groups_and_count( $type );

		foreach ( $groups as $group ) {
			self::create_group_item( $type, $group );
		}
	}

	static function render_favorites( $type ) {

		?>
		<div class="mainwp_managegroups-outsidebox">
			<div id="managefavgroups-sites-list" class="mainwp_managegroups-insidebox">
			<h3><?php echo 'plugin' == $type ? __( 'Plugins', 'mainwp-favorites-extension' ) : __( 'Themes', 'mainwp-favorites-extension' ); ?></h3>
				<ul class="mainwp-favorites-list">
					<?php echo Manage_Favorites::get_favorites_list_content( $type ); ?>
				</ul>
			</div>
		</div>
		<div style="clear: both;"></div>
		<br />
		<input type="button"
			   class="managefavgroups-saveAll button-primary button button-hero"
			   value="Save Selection"
		       name="Save selection"> <span id="managefavgroups-saved"><?php _e( 'Saved', 'mainwp-favorites-extension' ); ?></span>
		<?php
	}

	public static function get_favorites_list_content( $type ) {

		$favorites = Favorites_Extension_DB::get_instance()->query( Favorites_Extension_DB::get_instance()->get_sql_favorites_for_current_user( $type ) );

		while ( $favorites && ( $favorite = @Favorites_Extension_DB::fetch_object( $favorites ) ) ) {
			$name = self::get_short_string( $favorite->name, 28 );
			?>
			<li class="managefavgroups-listitem" ><input type="checkbox" name="<?php echo $type ?>-item"
			           value="<?php echo base64_encode( $favorite->id ); ?>" id="<?php echo $favorite->id; ?>"
			           class="mainwp-checkbox2"><label for="<?php echo $favorite->id; ?>" class="mainwp-label2"><span
						class="website_url" style="display: none;"><?php echo $name; ?></span><span class="website_name"
			                                                                                        title="version <?php echo $favorite->version; ?>"><?php echo $name; ?></span></label>
			</li>
			<?php
		}
		@Favorites_Extension_DB::free_result( $favorites );
	}

	public static function get_short_string( $string, $len = 20 ) {
		if ( strlen( $string ) <= $len ) {
			return $string;
		} else {
			return substr( $string, 0, $len ) . '..';
		}
	}

	static function render_upload( $type ) {

		?>
		<div class="mainwp_managegroups-outsidebox">

				<div id="managefavgroups-sites-list" class="mainwp_managegroups-insidebox">
					<?php if ( 'plugin' == $type ) { ?>
						<p class="fav-upload-title"><?php echo __( 'Upload plugins in .zip format', 'mainwp-favorites-extension' ); ?></p>
						<div style="margin-bottom: 1em;" class="mainwp-upload-button-area"><em><?php echo __( 'If you have plugins in a .zip format, you may upload it here.', 'mainwp-favorites-extension' ); ?></em></div>
					<?php } else { ?>
						<p class="fav-upload-title"><?php echo __( 'Upload themes in .zip format', 'mainwp-favorites-extension' ); ?></p>
						<div style="margin-bottom: 1em;" class="mainwp-upload-button-area"><em><?php echo __( 'If you have themes in a .zip format, you may upload it here.', 'mainwp-favorites-extension' ); ?></em></div>
					<?php } ?>
					<div id="favorites-file-uploader-<?php echo $type; ?>">
						<noscript>
							<div style="margin-bottom: 1em;" class="mainwp-upload-button-area"><?php _e( 'Please enable JavaScript to use file uploader.', 'mainwp-favorites-extension' ); ?></div>
						</noscript>
					</div>
				</div>

		</div>
		<script>
			// in your app create uploader as soon as the DOM is ready
			// don't wait for the window to load
			<?php if ( 'plugin' == $type ) {?>
			createUploaderPlugins();
			<?php } else { ?>
			createUploaderThemes();
			<?php } ?>
		</script>


		<?php
	}
}
