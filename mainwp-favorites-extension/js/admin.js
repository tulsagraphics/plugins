/*
 Administration Javascript for MainWP Favorites Extension
 */

jQuery( document ).ready(function ($) {
	jQuery( '.managefavgroups-listitem' ).live({
		mouseenter: function () {
			if (jQuery( this ).find( '.text' ).is( ":visible" )) { jQuery( this ).find( '.actions-text' ).show(); } else { jQuery( this ).find( '.actions-input' ).show(); }
		},
		mouseleave: function () {
			jQuery( this ).find( '.actions-text' ).hide();
			jQuery( this ).find( '.actions-input' ).hide();
		}
	});

	jQuery( '.managefavgroups-rename' ).live('click', function () {
		var parentObj = jQuery( this ).parents( '.managefavgroups-listitem' );
		parentObj.find( '.text' ).hide();
		parentObj.find( '.actions-text' ).hide();
		parentObj.find( '.input' ).show();
		parentObj.find( '.actions-input' ).show();
		return false;
	});

	jQuery( '.managefavgroups-save' ).live('click', function () {
		var parentObj = jQuery( this ).parents( '.managefavgroups-listitem' );
		var groupId = parentObj.attr( 'id' );
		var newName = parentObj.find( '.input input' ).val();
		var containerObj = jQuery( this ).parents( '.favorites-container-box' );
		var type = containerObj.find( ".hidden-fav-type" ).val();

		var data = {
			action: 'favorites_group_rename',
			groupId: groupId,
			newName: newName,
			type: type,
		}

		jQuery.post(ajaxurl, data, function (pParentObj) {
			return function (response) {
				response = jQuery.trim( response );
				pParentObj.find( '.input input' ).val( response );
				pParentObj.find( '.text' ).html( response );

				pParentObj.find( '.input' ).hide();
				pParentObj.find( '.actions-input' ).hide();
				pParentObj.find( '.text' ).show();
				pParentObj.find( '.actions-text' ).show();
			}
		}(parentObj));

		return false;
	});

	jQuery( '.managefavgroups-delete' ).live('click', function () {
		var confirmed = confirm( 'This will permanently delete this group. Proceed?' );
		if (confirmed) {
			var parentObj = jQuery( this ).parents( '.managefavgroups-listitem' );
			parentObj.css( 'background-color', '#F8E0E0' )
			var groupId = parentObj.attr( 'id' );

			var data = {
				action: 'favorites_group_delete',
				groupId: groupId
			}

			jQuery.post(ajaxurl, data, function(pParentObj) {
				return function (response) {
					response = jQuery.trim( response );
                                        if (response == 'OK') 
                                            pParentObj.animate({opacity: 0}, 300, function() {pParentObj.remove()});					
			    }
			}(parentObj));
		}
		return false;
	});

	jQuery( '.managefavgroups-addnew' ).live('click', function () {
		var addNewContainer = jQuery( this ).parents( '.favorites-container' ).find( '.managefavgroups-group-add' );
		addNewContainer.find( 'input' ).val( '' );
		addNewContainer.show();
	});

	jQuery( '.managefavgroups-cancel' ).live('click', function () {
		var addNewContainer = jQuery( this ).closest( '.managefavgroups-group-add' );
		addNewContainer.hide();
		addNewContainer.find( 'input' ).val( '' );
		return false;
	});

	jQuery( '.managefavgroups-savenew' ).live('click', function () {
		var parentObj = jQuery( this ).parents( '.managefavgroups-listitem' );
		var newName = parentObj.find( 'input' ).val();
		var containerObj = jQuery( this ).parents( '.favorites-container-box' );
		var newType = containerObj.find( ".hidden-fav-type" ).val();

		var data = {
			action: 'favorites_group_add',
			newName: newName,
			type: newType
		};

		jQuery.post(ajaxurl, data, function (response) {
			response = jQuery.trim( response );
			if (response == 'ERROR') { return; }

			var addNewContainer = containerObj.find( '.managefavgroups-group-add' );
			addNewContainer.hide();
			addNewContainer.find( 'input' ).val( '' );

			addNewContainer.after( response );
		});

		return false;
	});

	jQuery( '.managefavgroups-radio' ).live('click', function () {

		var parentObj = jQuery( this ).parents( '.managefavgroups-listitem' );
		var groupId = parentObj.attr( 'id' );
		var type = jQuery( this ).parents( '.favorites-container-box' ).find( ".hidden-fav-type" ).val();

		var data = {
			action: 'favorites_group_getfavorites',
			groupId: groupId
		}

		jQuery.post(ajaxurl, data, function (response) {
			response = jQuery.trim( response );
			if (response == 'ERROR') { return; }

			jQuery( 'input[name="' + type + '-item"]' ).attr( 'checked', false );

			var websiteIds = jQuery.parseJSON( response );
			for (var i = 0; i < websiteIds.length; i++) {
				jQuery( 'input[name="' + type + '-item"][value="' + websiteIds[i] + '"]' ).attr( 'checked', true );
			}
		});
	});

	jQuery( '.managefavgroups-saveAll' ).live('click', function () {
		var containerObj = jQuery( this ).parents( '.favorites-container-box' );
		var type = containerObj.find( ".hidden-fav-type" ).val();
		var checkedGroup = jQuery( 'input[name="' + type + '-groups"]:checked' );
		var groupId = checkedGroup.val();
		if (groupId == undefined) 
                    return;
                var allCheckedWebsites = containerObj.find( 'input[name="' + type + '-item"]:checked' ); 
		var allCheckedIds = [];
		for (var i = 0; i < allCheckedWebsites.length; i++) {
			allCheckedIds.push( jQuery( allCheckedWebsites[i] ).val() );
		}

		var data = {
			action: 'favorites_group_updategroup',
			groupId: groupId,
			favoriteIds: allCheckedIds
		};

		jQuery.post(ajaxurl, data, function (response) {
			response = jQuery.trim( response );
			containerObj.find( '#managefavgroups-saved' ).stop( true, true );
			containerObj.find( '#managefavgroups-saved' ).show();
			containerObj.find( '#managefavgroups-saved' ).fadeOut( 2000 );
			return;
		});
	});

	jQuery( '#favorite_notes_cancel' ).live('click', function () {
		favorite_notes_hide();
		return false;
	});
	jQuery( '#favorite_notes_closeX' ).live('click', function () {
		favorite_notes_hide();
		return false;
	});

	jQuery( '.favorite_notes_show' ).live('click', function () {
		var rowEl = jQuery( jQuery( this ).parents( 'tr' )[0] );
		var fav_id = rowEl.attr( 'favorite_id' );
		var name = rowEl.attr( 'favorite_name' );
		var note = rowEl.find( '.note' ).html();
		jQuery( '#favorite_notes_title' ).html( decodeURIComponent( name ) );
		jQuery( '#favorite_notes_note' ).val( note );
		jQuery( '#favorite_notes_id' ).val( fav_id );
		favorite_notes_show();
		return false;
	});

	jQuery( '#favorite_notes_save' ).on('click', function () {
		var fav_id = jQuery( '#favorite_notes_id' ).val();
		var newnote = jQuery( '#favorite_notes_note' ).val();
		var data = {
			action: 'favorite_notes_save',
			favoriteid: fav_id,
			note: newnote
		};
		jQuery( '#favorite_notes_status' ).html( '<img src="' + mainwpParams['image_url'] + 'loader.gif"> ' + __( 'Please wait while we are saving your note' ) );
		jQuery.post(ajaxurl, data, function (pId) {
			return function (response) {
				var rowEl = jQuery( 'tr[favorite_id="' + pId + '"]' );
				if (response == 'SUCCESS') {
					jQuery( '#favorite_notes_status' ).html( __( 'Note saved.' ) );
					rowEl.find( '.note' ).html( jQuery( '#favorite_notes_note' ).val() );
				} else {
					jQuery( '#favorite_notes_status' ).html( __( 'An error occured while saving your message.' ) );
				}
			}
		}(fav_id));
		return false;
	});

	jQuery( '#group_notes_cancel' ).live('click', function () {
		group_notes_hide();
		return false;
	});
	jQuery( '#group_notes_closeX' ).live('click', function () {
		group_notes_hide();
		return false;
	});

	jQuery( '.group_notes_show' ).live('click', function () {
		var rowEl = jQuery( jQuery( this ).parents( 'tr' )[0] );
		var fav_id = rowEl.attr( 'group_id' );
		var name = rowEl.attr( 'group_name' );
		var note = rowEl.find( '.note' ).html();
		jQuery( '#group_notes_title' ).html( decodeURIComponent( name ) );
		jQuery( '#group_notes_note' ).val( note );
		jQuery( '#group_notes_id' ).val( fav_id );
		group_notes_show();
		return false;
	});

	jQuery( '.mainpw-box-dismiss' ).live('click', function () {
		$( this ).closest( 'div' ).hide();
		return false;
	});

	jQuery( '#group_notes_save' ).on('click', function () {
		var gr_id = jQuery( '#group_notes_id' ).val();
		var newnote = jQuery( '#group_notes_note' ).val();
		var data = {
			action: 'group_notes_save',
			groupid: gr_id,
			note: newnote
		};
		jQuery( '#group_notes_status' ).html( '<img src="' + mainwpParams['image_url'] + 'loader.gif"> ' + __( 'Please wait while we are saving your note' ) );
		jQuery.post(ajaxurl, data, function (pId) {
			return function (response) {
				var rowEl = jQuery( 'tr[group_id="' + pId + '"]' );
				if (response == 'SUCCESS') {
					jQuery( '#group_notes_status' ).html( __( 'Note saved.' ) );
					rowEl.find( '.note' ).html( jQuery( '#group_notes_note' ).val() );
				} else {
					jQuery( '#group_notes_status' ).html( __( 'An error occured while saving your message.' ) );
				}
			}
		}(gr_id));
		return false;
	});

});

favorite_notes_show = function () {
	group_notes_hide();
	jQuery( '#favorite_notes_status' ).html( '' );
	jQuery( '#favorite_notes_overlay' ).height( jQuery( document ).height() );
	jQuery( '#favorite_notes_overlay' ).show();
	jQuery( '#favorite_notes' ).show();
};

favorite_notes_hide = function () {
	jQuery( '#favorite_notes' ).hide();
	jQuery( '#favorite_notes_overlay' ).hide();
	jQuery( '#favorite_notes_status' ).html( '' );
};

group_notes_show = function () {
	favorite_notes_hide();
	jQuery( '#group_notes_status' ).html( '' );
	jQuery( '#group_notes_overlay' ).height( jQuery( document ).height() );
	jQuery( '#group_notes_overlay' ).show();
	jQuery( '#group_notes' ).show();
};

group_notes_hide = function () {
	jQuery( '#group_notes' ).hide();
	jQuery( '#group_notes_overlay' ).hide();
	jQuery( '#group_notes_status' ).html( '' );
};

var favorites_overwrite_existing = false;
var favorites_activate_plugin = false;

favorites_install_set_install_links = function (event) {
	jQuery( 'a[id^="fav-install-"]' ).each(function (index, value) {
		if (divId = /^fav-install-([^\-]*)-(.*)$/.exec( value.id )) {
			var url = '';
			if (divId[2] == '') {
                            url = jQuery( this ).attr( 'download-url' ); 
                        }
			jQuery( value ).bind('click', function (event, what, slug, url) {
				return function () {
					favorite_install_bulk( what, slug, url );
					return false;
				}
			}(event, divId[1], divId[2], url));
		}
	});
};

favorite_install_bulk = function (type, slug, url) {
	var data = {
		action: 'favorite_prepareinstallplugintheme',
		type: type,
		slug: slug,
		selected_by: jQuery( '#select_by' ).val()
	};
	if (url) {
		data['url'] = url; }

	if (jQuery( '#select_by' ).val() == 'site') {
		var selected_sites = [];
		jQuery( "input[name='selected_sites[]']:checked" ).each(function (i) {
			selected_sites.push( jQuery( this ).val() );
		});

		if (selected_sites.length == 0) {
			show_error( 'ajax-error-zone', __( 'Please select websites or groups on the right side to install files.' ) );
			return;
		}
		data['selected_sites[]'] = selected_sites;
	} else {
		var selected_groups = [];
		jQuery( "input[name='selected_groups[]']:checked" ).each(function (i) {
			selected_groups.push( jQuery( this ).val() );
		});
		if (selected_groups.length == 0) {
			show_error( 'ajax-error-zone', __( 'Please select websites or groups on the right side to install files.' ) )
			return;
		}
		data['selected_groups[]'] = selected_groups;
	}
        
        favorites_overwrite_existing = jQuery( '#chk_overwrite' ).is( ':checked' );
	if (type == 'plugin') {
            favorites_activate_plugin = jQuery( '#chk_activate_plugin' ).is( ':checked' ); 
        }
        
        jQuery.post(ajaxurl, data, function (pType) {
		return function (response) {
			var installQueue = '<h3>Installing ' + type + '</h3>';
			for (var siteId in response.sites) {
				var site = response.sites[siteId];
				var site_name = site['name'].replace( /\\(.)/mg, "$1" );
				installQueue += '<span class="siteBulkInstall" siteid="' + siteId + '" status="queue"><strong>' + site_name + '</strong>: <span class="queue">' + __( 'Queued' ) + '</span><span class="progress"><img src="' + mainwpParams['image_url'] + 'loader.gif"> ' + __( 'In progress' ) + '</span><span class="status"></span></span><br />';
			}

			jQuery( '#mainwp_wrap-inside div.inside' ).html( installQueue );
			favorite_install_bulk_start_next( pType, response.url );
		}
	}(type), 'json');
	jQuery( '#mainwp_wrap-inside div.inside' ).html( '<br /><img src="' + mainwpParams['image_url'] + 'loader.gif"> ' + __( 'Preparing %1 installation.', type ) );
};

fav_bulkInstallMaxThreads = 3;
fav_bulkInstallCurrentThreads = 0;

favorite_install_bulk_start_next = function (pType, pUrl) {
	while ((siteToInstall = jQuery( '.siteBulkInstall[status="queue"]:first' )) && (siteToInstall.length > 0) && (fav_bulkInstallCurrentThreads < fav_bulkInstallMaxThreads)) {
		favorite_install_bulk_start_specific( pType, pUrl, siteToInstall );
	}
};

favorite_install_bulk_start_specific = function (pType, pUrl, pSiteToInstall) {
	fav_bulkInstallCurrentThreads++;
	pSiteToInstall.attr( 'status', 'progress' );

	pSiteToInstall.find( '.queue' ).hide();
	pSiteToInstall.find( '.progress' ).show();
//	var activatePlugin = false;
//	if (pType == 'plugin') {
//            activatePlugin = jQuery( '#chk_activate_plugin' ).is( ':checked' ); 
//        }
//	var pOverwrite = jQuery( '#chk_overwrite' ).is( ':checked' );

	var data = mainwp_secure_data({
		action: 'favorite_performinstallplugintheme',
		type: pType,
		url: pUrl,
		siteId: pSiteToInstall.attr( 'siteid' ),
		activatePlugin: favorites_activate_plugin,
		overwrite: favorites_overwrite_existing,
	});

	jQuery.post(ajaxurl, data, function (pType, pUrl, pSiteToInstall) {
		return function (response) {
			pSiteToInstall.attr( 'status', 'done' );

			pSiteToInstall.find( '.progress' ).hide();
			var statusEl = pSiteToInstall.find( '.status' );
			statusEl.show();

			if ((response.ok != undefined) && (response.ok[pSiteToInstall.attr( 'siteid' )] != undefined)) {
				statusEl.html( __( 'Installation successful' ) );
			} else if ((response.errors != undefined) && (response.errors[pSiteToInstall.attr( 'siteid' )] != undefined)) {
				statusEl.html( __( 'Installation failed' ) + ': ' + response.errors[pSiteToInstall.attr( 'siteid' )][1] );
				statusEl.css( 'color', 'red' );
			} else {
				statusEl.html( __( 'Installation failed' ) );
				statusEl.css( 'color', 'red' );
			}

			fav_bulkInstallCurrentThreads--;
			favorite_install_bulk_start_next( pType, pUrl );
		}
	}(pType, pUrl, pSiteToInstall), 'json');
};


group_install_set_install_links = function (event) {
	jQuery( 'a[id^="group-install-"]' ).each(function (index, value) {
		if (divId = /^group-install-([^\-]*)-(.*)$/.exec( value.id )) {
			jQuery( value ).bind('click', function (event, what, gid) {
				return function () {
					group_install_bulk( what, gid );
					return false;
				}
			}(event, divId[1], divId[2]));
		}
	});
};

group_install_bulk = function (type, groupid) {
	var data = {
		action: 'favorite_prepareinstallgroupplugintheme',
		groupid: groupid
	};
	var params = {
		selected_by: jQuery( '#select_by' ).val()
	}
        
        favorites_overwrite_existing = jQuery( '#chk_overwrite' ).is( ':checked' );
        if (type == 'plugin') {
            favorites_activate_plugin = jQuery( '#chk_activate_plugin' ).is( ':checked' ); 
        }
                
	if (jQuery( '#select_by' ).val() == 'site') {
		var selected_sites = [];
		jQuery( "input[name='selected_sites[]']:checked" ).each(function (i) {
			selected_sites.push( jQuery( this ).val() );
		});

		if (selected_sites.length == 0) {
			show_error( 'ajax-error-zone', __( 'Please select websites or groups on the right side to install files.' ) );
			return;
		}
		params['selected_sites'] = selected_sites;
	} else {
		var selected_groups = [];
		jQuery( "input[name='selected_groups[]']:checked" ).each(function (i) {
			selected_groups.push( jQuery( this ).val() );
		});
		if (selected_groups.length == 0) {
			show_error( 'ajax-error-zone', __( 'Please select websites or groups on the right side to install files.' ) )
			return;
		}
		params['selected_groups'] = selected_groups;
	}

	jQuery.post(ajaxurl, data, function (pType, pParams) {
		return function (response) {
			var installFavoriteQueue = '<h3>Installing ' + pType + ' group:</h3>';
			for (var favoriteId in response.favorites) {
				var favorite = response.favorites[favoriteId];
				installFavoriteQueue += '<span class="favoriteBulkInstall" favoriteName="' + favorite['name'] + '" favoriteDownloadUrl="' + favorite['download_url'] + '" favoriteSlug="' + favorite['slug'] + '" status="queue"><h3>' + favorite['name'] + '</h3> <span class="progress"><img src="' + mainwpParams['image_url'] + 'loader.gif"> ' + __( 'sites loading..' ) + '</span><span class="status"></span></span><br />';
			}

			jQuery( '#mainwp_wrap-inside div.inside' ).html( installFavoriteQueue );
			group_loading_sites_start_next( pType, pParams );
		}
	}(type, params), 'json');
	jQuery( '#mainwp_wrap-inside div.inside' ).html( '<br /><img src="' + mainwpParams['image_url'] + 'loader.gif"> ' + __( 'Preparing %1 installation.', type ) );
}

group_favoriteBulkInstallCurrentThreads = 0;

group_loading_sites_start_next = function (pType, pParams) {
	while ((favoriteToInstall = jQuery( '.favoriteBulkInstall[status="queue"]:first' )) && (favoriteToInstall.length > 0) && (group_favoriteBulkInstallCurrentThreads < fav_bulkInstallMaxThreads)) {
		url = favoriteToInstall.attr( 'favoriteDownloadUrl' );
		slug = favoriteToInstall.attr( 'favoriteSlug' );
		name = favoriteToInstall.attr( 'favoriteName' );
		group_loading_sites_start_specific( pType, slug, url, name, pParams, favoriteToInstall );
	}
	if (jQuery( '.favoriteBulkInstall[status="queue"]' ).length == 0) {
		group_favorite_install_bulk_start_next( pType ); }
};

group_loading_sites_start_specific = function (type, slug, url, name, pParams, pFavoriteToInstall) {
	var data = {
		action: 'favorite_prepareinstallplugintheme',
		type: type,
		slug: slug,
		selected_by: pParams['selected_by'],
		'selected_sites[]': pParams['selected_sites'],
		'selected_groups[]': pParams['selected_groups']
	};
	if (url) {
		data['url'] = url; }
	group_favoriteBulkInstallCurrentThreads++;
	pFavoriteToInstall.attr( 'status', 'sites-loaded' );
	jQuery.post(ajaxurl, data, function (pType) {
		return function (response) {
			var installQueue = '<h3>' + name + '</h3>';
			for (var siteId in response.sites) {
				var site = response.sites[siteId];
				var site_name = site['name'].replace( /\\(.)/mg, "$1" );
				installQueue += '<span class="siteBulkInstall" download-url="' + response.url + '" siteid="' + siteId + '" status="queue"><strong>' + site_name + '</strong>: <span class="queue">' + __( 'waiting..' ) + '</span><span class="progress"><img src="' + mainwpParams['image_url'] + 'loader.gif"> ' + __( 'In progress' ) + '</span><span class="status"></span></span><br />';
			}
			pFavoriteToInstall.html( installQueue );
			group_favoriteBulkInstallCurrentThreads--;
			group_loading_sites_start_next( type, pParams );
		}
	}(type), 'json');
}


group_favorite_install_bulk_start_next = function (pType) {
	while ((siteToInstall = jQuery( '.siteBulkInstall[status="queue"]:first' )) && (siteToInstall.length > 0) && (fav_bulkInstallCurrentThreads < fav_bulkInstallMaxThreads)) {
		var pUrl = siteToInstall.attr( 'download-url' );
		group_favorite_install_bulk_start_specific( pType, pUrl, siteToInstall );
	}
};

group_favorite_install_bulk_start_specific = function (pType, pUrl, pSiteToInstall) {
	fav_bulkInstallCurrentThreads++;
	pSiteToInstall.attr( 'status', 'progress' );

	pSiteToInstall.find( '.queue' ).hide();
	pSiteToInstall.find( '.progress' ).show();

//	var activatePlugin = false;
//	if (pType == 'plugin') {
//		activatePlugin = jQuery( '#chk_activate_plugin' ).is( ':checked' ); }
	//var pOverwrite = jQuery( '#chk_overwrite' ).is( ':checked' );

	var data = mainwp_secure_data({
		action: 'favorite_performinstallplugintheme',
		type: pType,
		url: pUrl,
		siteId: pSiteToInstall.attr( 'siteid' ),
		activatePlugin: favorites_activate_plugin,
		overwrite: favorites_overwrite_existing,
	});

	jQuery.post(ajaxurl, data, function (pType, pUrl, pSiteToInstall) {
		return function (response) {
			pSiteToInstall.attr( 'status', 'done' );

			pSiteToInstall.find( '.progress' ).hide();
			var statusEl = pSiteToInstall.find( '.status' );
			statusEl.show();

			if ((response.ok != undefined) && (response.ok[pSiteToInstall.attr( 'siteid' )] != undefined)) {
				statusEl.html( __( 'Installation successful' ) );
			} else if ((response.errors != undefined) && (response.errors[pSiteToInstall.attr( 'siteid' )] != undefined)) {
				statusEl.html( __( 'Installation failed' ) + ': ' + response.errors[pSiteToInstall.attr( 'siteid' )][1] );
				statusEl.css( 'color', 'red' );
			} else {
				statusEl.html( __( 'Installation failed' ) );
				statusEl.css( 'color', 'red' );
			}

			fav_bulkInstallCurrentThreads--;
			group_favorite_install_bulk_start_next( pType );
		}
	}(pType, pUrl, pSiteToInstall), 'json');
};


managefavorite_remove = function (type, file, id) {
	var q = confirm( __( 'Are you sure you want to delete this favorite item?' ) );
	if (q) {
		jQuery( '#fav-status' + id ).html( __( 'Removing the favorite..' ) );
		var data = {
			action: 'favorite_removefavorite',
			id: id,
			type: type,
			file: file
		};
		jQuery.post(ajaxurl, data, function (response) {
			response = jQuery.trim( response );
			var result = '';
			var error = '';
			if (response == 'SUCCESS') {
				result = __( 'Your favorite has been removed.' );
			} else {
				error = __( 'An unspecified error occured' );
			}

			if (error != '') {
				jQuery( '#fav-status-' + id ).html( error );
			} else if (result != '') {
				jQuery( '#fav-' + id ).html( '<td colspan="4">' + result + '</td>' );
			}

		});
	}
	return false;
};


managegroup_remove = function (type, id) {
	var q = confirm( __( 'Are you sure you want to delete this group item?' ) );
	if (q) {
		jQuery( '#fav-status' + id ).html( __( 'Removing the group..' ) );
		var data = {
			action: 'favorite_removegroup',
			id: id
		};
		jQuery.post(ajaxurl, data, function (response) {
			response = jQuery.trim( response );
			var result = '';
			var error = '';
			if (response == 'SUCCESS') {
				result = __( 'Your group has been removed.' );
			} else {
				error = __( 'An unspecified error occured' );
			}

			if (error != '') {
				jQuery( '#group-status-' + id ).html( error );
			} else if (result != '') {
				jQuery( '#group-' + id ).html( '<td colspan="4">' + result + '</td>' );
			}

		});
	}
	return false;
};

favorites_setting_uploadbulk_oncomplete = function (id, fileName, result) {
	if (result.success) {
		if (totalSuccess > 0) { // global variable
			jQuery( ".qq-upload-file" ).each(function (i) {
				if (jQuery( this ).parent().attr( 'class' ) && jQuery( this ).parent().attr( 'class' ).replace( /^\s+|\s+$/g, "" ) == 'qq-upload-success') {
					_file = jQuery( this ).attr( 'filename' );
					if (jQuery( this ).next().next().attr( 'class' ) != 'favorites-setting-add-file') {
						jQuery( this ).next().after( '<span class="favorites-setting-add-file" status="queue" id="' + id + '" realfile = "' + fileName + '" file="' + _file + '">' + __( 'Queue..' ) + '</span> ' );
						favorites_setting_add_to_favorites_next();
					}
				}
			});
		}
	}
}

bulkSettingAddCurrentThreads = 0;

favorites_setting_add_to_favorites_next = function () {
	while ((favoriteToAdd = jQuery( '.favorites-setting-add-file[status="queue"]:first' )) && (favoriteToAdd.length > 0) && (bulkSettingAddCurrentThreads < bulkInstallMaxThreads)) {
		var type = favoriteToAdd.closest( '.favorites-container-box' ).find( ".hidden-fav-type" ).val();
		//console.log(type);
		favorites_add_to_favorites_start_specific( type, favoriteToAdd );
	}
}

favorites_add_to_favorites_start_specific = function (type, pFavoriteToAdd) {
	bulkSettingAddCurrentThreads++;
	pFavoriteToAdd.attr( 'status', 'progress' );
	pFavoriteToAdd.html( '<img src="' + mainwpParams['image_url'] + 'loader.gif"> ' + __( 'Adding to favorites.' ) );
	var data = {
		action: 'favorites_uploadbulkaddtofavorites',
		type: type,
		file: pFavoriteToAdd.attr( 'file' ),
		copy: 'no'
	};
	jQuery.post(ajaxurl, data, function (pFavoriteToAdd) {
		return function (response) {
			if (response == 'NEWER_EXISTED') {
				pFavoriteToAdd.html( __( 'Newer version existed, doesn\'t add to favorites.' ) );
			} else if (response == 'SUCCESS') {
				pFavoriteToAdd.html( __( 'Add to favorites successfully.' ) );
			} else { 				
                                pFavoriteToAdd.html( __( 'Add to favorites failed.' ) ); 
                        }
			bulkSettingAddCurrentThreads--;
		}
	}(pFavoriteToAdd));
};
