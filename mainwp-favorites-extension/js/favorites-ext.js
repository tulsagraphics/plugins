/*
* Administration Javascript for MainWP Favorites Extension
* For Install Themes/Plugins pages
*/

jQuery( document ).ready(function ($) {

});

favorites_set_add_links = function () {
	jQuery( 'a[id^="add-favorite-"]' ).each(function (index, value) {
		if (divId = /^add-favorite-([^\-]*)-(.*)$/.exec( value.id )) {
			jQuery( value ).bind('click', function (what, slug) {
				var linkId = value.id;
				return function () {
					favorites_add_favorite( linkId, what, slug );
					return false;
				};
			}(divId[1], divId[2]));
		}
	});
};

favorites_add_favorite = function (linkID, type, slug) {
	var data = {
		action: 'favorites_addplugintheme',
		type: type,
		slug: slug,
	};
	var parent = jQuery( '#' + linkID ).parent();
	parent.html( __( 'Adding favorite' ) + ' ' + type + ' <img src="' + mainwpParams['image_url'] + 'loader.gif"> ' );
	jQuery.post(ajaxurl, data, function (response) {
		if (response == 'FAIL') {
			parent.html( __( 'Added to favorites failure.' ) );
		} else { 			parent.html( __( 'Added to favorites successfully.' ) ); }
	});
};

favorites_uploadbulk_oncomplete = function (id, fileName, result, type) {
	if (result.success) {
		if (totalSuccess > 0) { // global variable
			jQuery( ".qq-upload-file" ).each(function (i) {
				if (jQuery( this ).parent().attr( 'class' ) && jQuery( this ).parent().attr( 'class' ).replace( /^\s+|\s+$/g, "" ) == 'qq-upload-success') {
					_file = jQuery( this ).attr( 'filename' );
					if (jQuery( this ).next().next().attr( 'class' ) != 'favorites-add-file') {
						jQuery( this ).next().after( '<span class="favorites-add-file"><a class="add-favorites" href="#" onclick="return favorites_upload_add_to_favorites(\'' + _file + '\', this, \'' + type + '\')" title="' + __( "Add To Favorites" ) + '">' + __( "Add To Favorites" ) + '</a></span> ' );
					}
				}
			});
		}
	}
}

favorites_upload_add_to_favorites = function (file, obj, type) {
	var pFavoriteToAdd = jQuery( obj ).closest( '.favorites-add-file' );
	pFavoriteToAdd.html( '<img src="' + mainwpParams['image_url'] + 'loader.gif"> ' + __( 'Adding to favorites.' ) );

	var data = {
		action: 'favorites_uploadbulkaddtofavorites',
		type: type,
		file: file,
		copy: 'yes'
	};
	jQuery.post(ajaxurl, data, function (pFavoriteToAdd) {
		return function (response) {
			if (response == 'NEWER_EXISTED') {
				pFavoriteToAdd.html( __( 'Newer version existed, doesn\'t add to favorites.' ) );
			} else if (response == 'SUCCESS') {
				pFavoriteToAdd.html( __( 'Add to favorites successfully.' ) );
			} else { 				
                                pFavoriteToAdd.html( __( 'Add to favorites failed.' ) ); 
                        }
		}
	}(pFavoriteToAdd));
	return false;
};
