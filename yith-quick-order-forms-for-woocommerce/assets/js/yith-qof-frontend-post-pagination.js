jQuery(function ($) {

    function enable_post_pagination() {

        var current_url = $('body #Yith_WC_QOF_Frontend_Pagination_ID').data('current_url');

        $("#Yith_WC_QOF_Frontend_Pagination_ID a").each(function () {

            var href = $(this).attr('href').split("/");

            $(this).attr("href", current_url + href[href.length - 2]);

        });

    }

    enable_post_pagination();

});