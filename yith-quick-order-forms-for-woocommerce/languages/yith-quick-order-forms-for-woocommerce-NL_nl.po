msgid ""
msgstr ""
"Project-Id-Version: YITH Quick Order Froms for WooCommerce\n"
"POT-Creation-Date: 2018-05-09 17:48+0200\n"
"PO-Revision-Date: 2018-05-09 17:59+0200\n"
"Last-Translator: \n"
"Language-Team: YITH <plugins@yithemes.com>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.13\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;"
"__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;"
"_nx_noop:4c,1,2\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: plugin-fw\n"

#: includes/class.yith-wc-quick-order-forms-admin.php:119
msgid "Settings"
msgstr "Instellingen"

#: includes/class.yith-wc-quick-order-forms-admin.php:141
msgid "Show only in forms"
msgstr "Alleen weergeven in formulieren"

#: includes/class.yith-wc-quick-order-forms-admin.php:169
msgid "Quick Order Forms"
msgstr "Snelle bestellingsformulieren"

#: includes/class.yith-wc-quick-order-forms-admin.php:172
msgid "Forms"
msgstr "Formulieren"

#: includes/class.yith-wc-quick-order-forms-admin.php:229
msgid "Choose a role"
msgstr "Kies een rol"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:39
#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:40
#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:104
#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:105
#: templates/form_tab.php:25
msgid "Quick order forms"
msgstr "Quick order forms"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:52
msgid "Title"
msgstr "Titel"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:53
#: templates/custom_post_types/yith_cpt_forms_settings.php:20
msgid "Description"
msgstr "Beschrijving"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:95
msgid "All"
msgstr "Alle"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:96
msgid "Published"
msgstr "Gepubliceerd"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:97
msgid "Trash"
msgstr "Prullebak"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:214
msgid "Delete permanently"
msgstr "Permanent verwijderen"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:218
msgid "Delete"
msgstr "Verwijderen"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:258
#, php-format
msgid "Edit &#8220;%s&#8221;"
msgstr "Bewerk &#8220;%s &#8221;"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:259
#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:108
msgid "Edit"
msgstr "Bewerken"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:269
#, php-format
msgid "Restore &#8220;%s&#8221; from the Trash"
msgstr "Herstel &#8220;%s&#8221; van de prullenbak"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:270
msgid "Restore"
msgstr "Herstel"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:277
#, php-format
msgid "Move &#8220;%s&#8221; to the Trash"
msgstr "Verplaats &#8220;%s&#8221; naar de prullenbak"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:278
msgctxt "verb"
msgid "Trash"
msgstr "Prullebak"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:286
#, php-format
msgid "Delete &#8220;%s&#8221; permanently"
msgstr "Verwijder &#8220;%s&#8221; blijvend"

#: includes/class.yith-wc-quick-order-forms-custom-post-type-table.php:287
msgid "Delete Permanently"
msgstr "Verwijder Permanent"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:106
#: templates/form_tab.php:26
msgid "Add new form"
msgstr "Voeg nieuw formulier"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:107
msgid "Add New Form"
msgstr "Voeg Nieuw Formulier toe"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:109
msgid "Edit Form"
msgstr "Formulier bewerken"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:110
msgid "New Form"
msgstr "Nieuw Formulier"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:111
#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:112
msgid "View Form"
msgstr "Weergeef Formulier"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:113
msgid "Search Forms"
msgstr "Zoek Formulieren"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:114
msgid "No Forms found"
msgstr "Geen Formulieren gevonden"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:115
msgid "No Forms found in trash"
msgstr "Geen formulieren gevonden in de prullenbak"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:116
msgid "Parent Forms"
msgstr "Parent Formulieren"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:117
msgctxt "Admin menu name"
msgid "YITH Forms"
msgstr "YITH Formulieren"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:118
msgid "All YITH Forms"
msgstr "Alle YITH Formulieren"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:122
msgid "List of the product forms"
msgstr "Lijst met productformulieren"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:124
msgid "This is where the list of products is stored."
msgstr "Dit is waar de lijst met producten wordt opgeslagen."

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:176
msgid "Shortcode and php code"
msgstr "Shortcode en php code"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:208
msgid "Form Settings"
msgstr "Formulier instellingen"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:228
msgid "Form permissions"
msgstr "Formulier machtiging"

#: includes/class.yith-wc-quick-order-forms-custom-post-type.php:248
msgid "Products chosen"
msgstr "Gekozen Product"

#: init.php:36
msgctxt "Alert Message: WooCommerce require"
msgid ""
"YITH WooCommerce Quick Order Forms is enabled but not effective. It requires "
"WooCommerce in order to work."
msgstr ""
"YITH WooCommerce Quick Order Forms is ingeschakeld maar niet effectief. Het "
"vereist WooCommerce om te kunnen werken."

#: templates/custom_post_types/yith_cpt_forms_permissions.php:16
msgid "Allow all users (logged-in and guests)"
msgstr "Alle gebruikers toestaan ​​(ingelogd en gasten)"

#: templates/custom_post_types/yith_cpt_forms_permissions.php:46
msgid "Allow all the user roles"
msgstr "Sta alle gebruikersrollen toe"

#: templates/custom_post_types/yith_cpt_forms_permissions.php:54
msgid "Allow specific user roles"
msgstr "Sta specifieke gebruikersrollen toe"

#: templates/custom_post_types/yith_cpt_forms_permissions.php:62
msgid "Disable user roles"
msgstr "Schakel gebruikersrollen uit"

#: templates/custom_post_types/yith_cpt_forms_permissions.php:127
msgid "Allow all users"
msgstr "Sta alle gebruikers toe"

#: templates/custom_post_types/yith_cpt_forms_permissions.php:135
msgid "Allow specific users"
msgstr "Sta specifieke gebruikers toe"

#: templates/custom_post_types/yith_cpt_forms_permissions.php:143
msgid "Disable users"
msgstr "Schakel gebruikers uit"

#: templates/custom_post_types/yith_cpt_forms_permissions.php:176
msgid "Search for a customer&hellip;"
msgstr "Zoeken naar een klant&hellip;"

#: templates/custom_post_types/yith_cpt_forms_permissions.php:205
msgid "Custom message for restricted visibility"
msgstr "Aangepast bericht voor beperkt zicht"

#: templates/custom_post_types/yith_cpt_forms_permissions.php:209
msgid "Message for restricted visibility"
msgstr "Bericht voor beperkt zicht"

#: templates/custom_post_types/yith_cpt_forms_products_chosen.php:23
msgid "Choose all products "
msgstr "Kies alle producten"

#: templates/custom_post_types/yith_cpt_forms_products_chosen.php:31
msgid "Allow specific products"
msgstr "Sta specifieke producten toe"

#: templates/custom_post_types/yith_cpt_forms_products_chosen.php:39
msgid "Disable products "
msgstr "Schakel producten uit"

#: templates/custom_post_types/yith_cpt_forms_products_chosen.php:65
msgid "Search for a product&hellip;"
msgstr "Zoeken naar een product &hellip;"

#: templates/custom_post_types/yith_cpt_forms_products_chosen.php:114
msgid "Choose all categories "
msgstr "Kies alle categorieën"

#: templates/custom_post_types/yith_cpt_forms_products_chosen.php:122
msgid "Allow specific categories"
msgstr "Specifieke categorieën toestaan"

#: templates/custom_post_types/yith_cpt_forms_products_chosen.php:130
msgid "Disable categories "
msgstr "Schakel categorieën uit"

#: templates/custom_post_types/yith_cpt_forms_products_chosen.php:207
msgid "Choose all tags "
msgstr "Kies alle tags"

#: templates/custom_post_types/yith_cpt_forms_products_chosen.php:215
msgid "Allow specific tags"
msgstr "Sta specifieke tags toe"

#: templates/custom_post_types/yith_cpt_forms_products_chosen.php:223
msgid "Disable tags "
msgstr "Tags uitschakelen"

#: templates/custom_post_types/yith_cpt_forms_settings.php:42
msgid "Pagination"
msgstr "Paginering"

#: templates/custom_post_types/yith_cpt_forms_settings.php:49
msgid "Products per page"
msgstr "Producten per pagina"

#: templates/custom_post_types/yith_cpt_forms_settings.php:98
msgid "Choose product information to show"
msgstr "Kies productinformatie om te laten zien"

#: templates/custom_post_types/yith_cpt_forms_settings.php:105
#: templates/custom_post_types/yith_show_products.php:389
msgid "SKU"
msgstr "SKU"

#: templates/custom_post_types/yith_cpt_forms_settings.php:110
msgid "thumbnail"
msgstr "thumbnail"

#: templates/custom_post_types/yith_cpt_forms_settings.php:115
msgid "price"
msgstr "Prijs"

#: templates/custom_post_types/yith_cpt_forms_settings.php:120
msgid "discount"
msgstr "Korting"

#: templates/custom_post_types/yith_cpt_forms_settings.php:125
msgid "show search filter"
msgstr "Weergeef zoek filter"

#: templates/custom_post_types/yith_cpt_forms_settings.php:130
msgid "show number of products"
msgstr "Weergeef producten om te laten zien"

#: templates/custom_post_types/yith_cpt_forms_settings.php:135
msgid "show stock"
msgstr "Weergeef voorraad"

#: templates/custom_post_types/yith_cpt_forms_settings.php:140
msgid "show variable price"
msgstr "Toon geavanceerde prijs"

#: templates/custom_post_types/yith_cpt_forms_settings.php:148
msgid "show quantity"
msgstr "Toon Hoeveelheid"

#: templates/custom_post_types/yith_cpt_forms_settings.php:153
msgid "default quantity"
msgstr "Standaard hoeveelheid"

#: templates/custom_post_types/yith_cpt_forms_settings.php:180
msgid "Choose which add to cart buttons you want to show"
msgstr "Kies welke voeg toe aan winkelwagen knop je wilt weergeven"

#: templates/custom_post_types/yith_cpt_forms_settings.php:188
msgid "individal add to cart"
msgstr "Individuele voeg toe aan winkelwagen"

#: templates/custom_post_types/yith_cpt_forms_settings.php:193
msgid "add to car for all product at the top"
msgstr "Voeg toe aan winkelwagen voor al je producten "

#: templates/custom_post_types/yith_cpt_forms_settings.php:198
msgid "add to car for all product at the bottom"
msgstr "Voeg toe aan winkelwagen op de bodem"

#: templates/custom_post_types/yith_cpt_forms_settings.php:216
msgid "Choose how to sort the form by default"
msgstr "Kies hoe je de standaard formulieren wilt weergeven"

#: templates/custom_post_types/yith_cpt_forms_settings.php:224
#: templates/custom_post_types/yith_show_products_filter.php:25
msgid "Default sorting "
msgstr "Standaard sortering"

#: templates/custom_post_types/yith_cpt_forms_settings.php:226
#: templates/custom_post_types/yith_show_products_filter.php:27
msgid "Sort by price: low to high "
msgstr "Sorteer op prijs: laag naar hoog"

#: templates/custom_post_types/yith_cpt_forms_settings.php:227
#: templates/custom_post_types/yith_show_products_filter.php:28
msgid "Sort by price: high to low "
msgstr "Sorteer op prijs: hoog naar laag"

#: templates/custom_post_types/yith_cpt_forms_settings.php:229
#: templates/custom_post_types/yith_show_products_filter.php:30
msgid "Sort by SKU: low to high "
msgstr "Sorteren op SKU: laag naar hoog"

#: templates/custom_post_types/yith_cpt_forms_settings.php:230
#: templates/custom_post_types/yith_show_products_filter.php:31
msgid "Sort by SKU: high to low "
msgstr "Sorteren op SKU: hoog naar laag"

#: templates/custom_post_types/yith_cpt_forms_settings.php:232
#: templates/custom_post_types/yith_show_products_filter.php:33
msgid "Sort by newness "
msgstr "Sorteer op nieuwheid"

#: templates/custom_post_types/yith_cpt_forms_settings.php:233
#: templates/custom_post_types/yith_show_products_filter.php:34
msgid "Sort by lateness "
msgstr "Sorteer op laatheid"

#: templates/custom_post_types/yith_cpt_forms_settings.php:235
#: templates/custom_post_types/yith_show_products_filter.php:36
msgid "Sort by Name asc"
msgstr "Sorteer op naam oplopend"

#: templates/custom_post_types/yith_cpt_forms_settings.php:236
#: templates/custom_post_types/yith_show_products_filter.php:37
msgid "Sort by Name desc"
msgstr "Sorteer op naam desc"

#: templates/custom_post_types/yith_cpt_forms_settings.php:238
#: templates/custom_post_types/yith_show_products_filter.php:39
msgid "Sort by Menu order asc"
msgstr "Sorteer via bestellingen menu asc"

#: templates/custom_post_types/yith_cpt_forms_settings.php:239
#: templates/custom_post_types/yith_show_products_filter.php:40
msgid "Sort by Menu order desc"
msgstr "Sorteer via bestellings menu desc"

#: templates/custom_post_types/yith_show_product_line.php:82
msgid "No product matching the selection found"
msgstr "Geen product passend bij de selectie gevonden"

#: templates/custom_post_types/yith_show_product_line.php:200
#: templates/custom_post_types/yith_show_product_line.php:208
msgid "Add to cart"
msgstr "Voeg toe aan winkelwagen"

#: templates/custom_post_types/yith_show_products.php:139
msgid "There are no products to show in this form"
msgstr "Er zijn geen producten om in dit formulier te tonen"

#: templates/custom_post_types/yith_show_products.php:361
#: templates/custom_post_types/yith_show_products.php:490
msgid "Add all products to cart"
msgstr "Voeg producten toe aan winkelwagen"

#: templates/custom_post_types/yith_show_products.php:377
msgid "Image"
msgstr "Afbeelding"

#: templates/custom_post_types/yith_show_products.php:383
msgid "Name"
msgstr "Naam"

#: templates/custom_post_types/yith_show_products.php:397
msgid "Price"
msgstr "Prijs"

#: templates/custom_post_types/yith_show_products.php:408
msgid " products found"
msgstr "producten gevonden"

#: templates/custom_post_types/yith_show_products.php:501
msgid "No products found"
msgstr "Geen producten gevonden"

#: templates/custom_post_types/yith_show_products.php:509
msgid "Page"
msgstr "Pagina"

#: templates/custom_post_types/yith_show_products_filter.php:74
msgid "Select a category"
msgstr "Selecteer een categorie"

#: templates/custom_post_types/yith_show_products_filter.php:146
msgid "Select a tag"
msgstr "Selecteer een tag"

#: templates/custom_post_types/yith_show_products_filter.php:170
msgid "search by name"
msgstr "Zoek op naam"

#: templates/custom_post_types/yith_show_products_filter.php:172
msgid "search by SKU"
msgstr "Zoek op SKU"

#: templates/custom_post_types/yith_show_products_filter.php:174
msgid "Search"
msgstr "Zoeken"
