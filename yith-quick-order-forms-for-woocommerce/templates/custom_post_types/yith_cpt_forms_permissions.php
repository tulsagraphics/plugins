<?php
/* =========== USER LOGGED IN =========== */

$value   = get_post_meta( $post->ID, 'yith_wc_quick_order_all_users_checkbox', true );
$checked = ( $value == '1' ? 'checked="checked"' : '' );

?>

<div class="YITH_QOF_Form_Line">
	<ul>

		<li>
			<label>
				<input type="checkbox" id="yith_wc_quick_order_all_users_checkbox" name="yith_wc_quick_order_all_users_checkbox" <?php echo $checked; ?>/>
                <span class="Yith_wc_order_quick_forms_margin_right">
                    <?php _e( 'Allow all users (logged-in and guests)', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SLUG' ) ); ?>
                </span>
			</label>
		</li>
	</ul>
</div>

<?php
/* =========== ENABLE || DISABLE FORM FOR USER ROLE =========== */

$radio = get_post_meta( $post->ID, 'yith_wc_quick_order_form_user_roles_list_radio_button', true );

?>

<div class="YITH_QOF_Custom_Post_Type_Parent">

	<div id="YITH_QOF_Choose_Permissions_Displaying" class="YITH_QOF_Blank_Brightness"></div>

	<div class="YITH_QOF_Form_Line">

		<ul>

			<li>

				<p>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_user_roles_list_radio_button" checked="checked" value="radio_all" <?php echo( ( $radio == "radio_all" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Allow all the user roles', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SLUG' ) ); ?>
                        </span>
					</label>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_user_roles_list_radio_button" value="radio_enable_user_roles_list" <?php echo( ( $radio == "radio_enable_user_roles_list" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Allow specific user roles', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SLUG' ) ); ?>
                        </span>
					</label>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_user_roles_list_radio_button" value="radio_disable_user_roles_list" <?php echo( ( $radio == "radio_disable_user_roles_list" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Disable user roles', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SLUG' ) ); ?>
                        </span>
					</label>

				</p>

				<div class="YITH_QOF_Form_Line_div_parent">

					<div id="YITH_QOF_Choose_User_Roles_List_Permissions_Displaying" class="YITH_QOF_Blank_Brightness"></div>

					<?php

					$value = get_post_meta( $post->ID, 'yith_wc_quick_order_form_enable_disable_roles_selected_select_multiple', true );

					$value = ( empty( $value ) ) ? array() : $value;

					?>

					<select multiple="multiple" id="yith_wc_quick_order_form_enable_disable_roles_selected_select_multiple[]" name="yith_wc_quick_order_form_enable_disable_roles_selected_select_multiple[]" class="yith_wc_qof_roles_search" style="width: 500px;">

						<?php

						global $wp_roles;
						$roles = $wp_roles->roles;

						foreach ( $roles as $role ) {

							if ( in_array( $role['name'], $value ) ) {
								echo '<option selected="selected" value="' . $role['name'] . '">' . $role['name'] . '</option>';
							} else {
								echo '<option value="' . $role['name'] . '">' . $role['name'] . '</option>';
							}
						}

						?>

					</select>

				</div>

			</li>

		</ul>

	</div>

	<?php
	/* =========== ENABLE USER PERMISSIONS =========== */

	$radio = get_post_meta( $post->ID, 'yith_wc_quick_order_form_users_list_radio_button', true );

	?>

	<div class="YITH_QOF_Form_Line">

		<ul>

			<li>

				<p>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_users_list_radio_button" checked="checked" value="radio_all" <?php echo( ( $radio == "radio_all" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Allow all users', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SLUG' ) ); ?>
                        </span>
					</label>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_users_list_radio_button" value="radio_enable_users_list" <?php echo( ( $radio == "radio_enable_users_list" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Allow specific users', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SLUG' ) ); ?>
                        </span>
					</label>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_users_list_radio_button" value="radio_disable_users_list" <?php echo( ( $radio == "radio_disable_users_list" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Disable users', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SLUG' ) ); ?>
                        </span>
					</label>

				</p>

				<div class="YITH_QOF_Form_Line_div_parent">

					<div id="YITH_QOF_Choose_Users_List_Permissions_Displaying" class="YITH_QOF_Blank_Brightness"></div>

					<?php

					$value = get_post_meta( $post->ID, 'yith_wc_quick_order_form_enable_disable_customers_selected_select_multiple', true );

					if ( ! empty( $value ) ) {
						$users = is_array( $value ) ? $value : explode( ',', $value );
					}

					$data_selected = array();

					if ( ! empty( $users ) ) {
						foreach ( $users as $user_index ) {
							$user = get_user_by( 'id', $user_index );
							//var_dump($user);
							$data_selected[ $user_index ] = "# " . $user->ID . " - " . $user->display_name . " - " . $user->user_email;
						}
					}

					$search_product_array = array(
						'type'             => 'hidden',
						'class'            => 'wc-customer-search',
						'id'               => 'yith_wc_quick_order_form_enable_disable_customers_selected_select_multiple',
						'name'             => 'yith_wc_quick_order_form_enable_disable_customers_selected_select_multiple',
						'data-placeholder' => __( 'Search for a customer&hellip;', 'yith-quick-order-forms-for-woocommerce' ),
						'data-allow_clear' => true,
						'data-selected'    => $data_selected,
						'data-multiple'    => true,
						'data-action'      => 'woocommerce_json_search_customer',
						'value'            => empty( $value ) ? '' : $value,
						'style'            => 'width: 500px'
					);
					yit_add_select2_fields( $search_product_array );

					?>

				</div>
			</li>

		</ul>

	</div>

	<?php
	/* =========== VISIBILITY RESTRICTED =========== */

	$value = get_post_meta( $post->ID, 'yith_wc_quick_order_form_visibility_restricted_textarea', true );

	?>

	<div class="YITH_QOF_Form_Line">
		<ul>
			<li>
				<label class="YITH_QOF_Form_strong" for="yith_wc_quick_order_form_visibility_restricted_textarea"><?php _e( 'Custom message for restricted visibility', 'yith-quick-order-forms-for-woocommerce' ); ?>: </label>
			</li>

			<li>
				<textarea placeholder="<?php _e( 'Message for restricted visibility', 'yith-quick-order-forms-for-woocommerce' ); ?>" id="yith_wc_quick_order_form_visibility_restricted_textarea" name="yith_wc_quick_order_form_visibility_restricted_textarea"><?php echo esc_attr( $value ); ?></textarea>
			</li>
		</ul>

	</div>

</div>