<?php

/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

/**
 *
 *
 * @class      YITH_WC_Quick_Order_Forms
 * @package    Yithemes
 * @since      Version 1.0.0
 * @author     Your Inspiration Themes
 *
 */

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class YITH_WC_Quick_Order_Forms_Custom_Post_Type_Table extends WP_List_Table {


	/**
	 * YITH_WC_Quick_Order_Forms_Custom_Post_Type_Table constructor.
	 *
	 * @param array $args
	 */
	public function __construct( $args = array() ) {
		parent::__construct( array(
			'singular' => __( 'Quick order forms', 'yith-quick-order-forms-for-woocommerce' ),
			'plural'   => __( 'Quick order forms', 'yith-quick-order-forms-for-woocommerce' ),
			'ajax'     => false
		) );
	}

	/**
	 * @return array
	 */
	function get_columns() {

		$columns = array(
			'cb'          => '<input type="checkbox" />',
			'title'       => __( 'Title', 'yith-quick-order-forms-for-woocommerce' ),
			'description' => __( 'Description', 'yith-quick-order-forms-for-woocommerce' ),
		);


		return apply_filters( 'yith_wc_quick_order_forms_list_columns', $columns );
	}

	/**
	 * Column default function
	 *
	 * @param object $item
	 * @param string $column_name
	 *
	 * @return string|void
	 */
	function column_default( $item, $column_name ) {

		switch ( $column_name ) {

			case 'title':
				return ( $item->post_title ) ? '<span><a href="' . admin_url( 'post.php?post=' . $item->ID . '&action=edit' ) . '">' . $item->post_title . '</a></span>' : '';
				break;

			case 'description':
				$description = get_post_meta( $item->ID, 'yith_wc_quick_order_form_description_textarea', true );

				return ( $description ) ? '<span>' . $description . '</span>' : '';
				break;

			default:
				return apply_filters( 'yith_wc_quick_oder_forms_column_default', '', $item, $column_name );
		}
	}

	/**
	 * Get views for the table
	 * @author YITHEMES
	 * @since  1.0.0
	 * @return array
	 */
	protected function get_views() {
		$views = array(
			'all'     => __( 'All', 'yith-quick-order-forms-for-woocommerce' ),
			'publish' => __( 'Published', 'yith-quick-order-forms-for-woocommerce' ),
			'trash'   => __( 'Trash', 'yith-quick-order-forms-for-woocommerce' )
		);

		$current_view = $this->get_current_view();

		foreach ( $views as $view_id => $view ) {

			$query_args = array(
				'posts_per_page'  => - 1,
				'post_type'       => 'yith_wc_qof_1',
				'post_status'     => 'publish',
				'suppress_filter' => false
			);
			$status     = 'status';
			$id         = $view_id;

			if ( 'all' !== $view_id ) {
				$query_args['post_status'] = $view_id;
			}

			$href              = esc_url( add_query_arg( $status, $id ) );
			$total_items       = count( get_posts( $query_args ) );
			$class             = $view_id == $current_view ? 'current' : '';
			$views[ $view_id ] = sprintf( "<a href='%s' class='%s'>%s <span class='count'>(%d)</span></a>", $href, $class, $view, $total_items );
		}


		return $views;
	}

	/**
	 * return current view
	 * @author YITHEMES
	 * @since  1.0.0
	 * @return string
	 */
	public function get_current_view() {

		return empty( $_GET['status'] ) ? 'all' : $_GET['status'];
	}

	/**
	 * Prepares the list of items for displaying.
	 *
	 * @since 1.0.0
	 */
	function prepare_items() {

		$current_view = $this->get_current_view();
		if ( $current_view == 'all' ) {
			$current_view = 'any';
		}

		$columns               = $this->get_columns();
		$hidden                = array();
		$sortable              = $this->get_sortable_columns();
		$this->_column_headers = array( $columns, $hidden, $sortable );
		$perpage               = apply_filters( 'yith_wc_quick_oder_forms_per_page', 15 );

		$args        = array(
			'post_type'      => 'yith_wc_qof_1',
			'post_status'    => $current_view,
			'posts_per_page' => $perpage,
			'paged'          => absint( $this->get_pagenum() ),
			'orderby'        => 'ID',
			'order'          => 'DESC',
		);
		$query       = new WP_Query( $args );
		$this->items = $query->posts;

		/* -- Register the pagination -- */
		$this->set_pagination_args( array(
			"total_items" => $query->found_posts,
			"per_page"    => $perpage,
		) );
	}

	/**
	 * @author YITHEMES
	 * @since  1.0.0
	 *
	 * @param object $item
	 *
	 * @return string
	 */
	public function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="yithwcqof_ids[]" value="%s" />', $item->ID
		);
	}

	/**
	 * return bulk actions
	 * @author YITHEMES
	 * @since  1.0.0
	 * @return array|false|string
	 */
	public function get_bulk_actions() {

		$actions = $this->current_action();

		if ( isset( $_REQUEST['yithwcqof_ids'] ) ) {

			$rules = $_REQUEST['yithwcqof_ids'];

			if ( $actions == 'delete' ) {
				foreach ( $rules as $rule_id ) {
					wp_delete_post( $rule_id, true );
				}
			}

			$this->prepare_items();
		}

		$current_view = $this->get_current_view();
		if ( $current_view == 'trash' ) {
			$actions = array(
				'delete' => __( 'Delete permanently', 'yith-quick-order-forms-for-woocommerce' )
			);
		} else {
			$actions = array(
				'delete' => __( 'Delete', 'yith-quick-order-forms-for-woocommerce' )
			);
		}

		return $actions;
	}

	/**
	 * @return array
	 */
	function get_sortable_columns() {

		$sortable_columns = array(
			'post_title' => array( 'Rule name', false ),
			'rule_type'  => array( 'Rule type', false ),
			'priority'   => array( 'Priority', false ),
		);

		return $sortable_columns;
	}

	/**
	 * Function to edit or delete rules
	 * @return array
	 */
	protected function handle_row_actions( $post, $column_name, $primary ) {
		if ( $primary !== $column_name ) {
			return '';
		}

		$post_type_object = get_post_type_object( $post->post_type );
		$can_edit_post    = current_user_can( 'edit_post', $post->ID );
		$title            = _draft_or_post_title();
		$actions          = array();

		if ( $can_edit_post && 'trash' != $post->post_status ) {
			$actions['edit'] = sprintf(
				'<a href="%s" aria-label="%s">%s</a>',
				get_edit_post_link( $post->ID ),
				/* translators: %s: post title */
				esc_attr( sprintf( __( 'Edit &#8220;%s&#8221;' ), $title ) ),
				__( 'Edit' )
			);
		}

		if ( current_user_can( 'delete_post', $post->ID ) ) {
			if ( 'trash' === $post->post_status ) {
				$actions['untrash'] = sprintf(
					'<a href="%s" aria-label="%s">%s</a>',
					wp_nonce_url( admin_url( sprintf( $post_type_object->_edit_link . '&amp;action=untrash', $post->ID ) ), 'untrash-post_' . $post->ID ),
					/* translators: %s: post title */
					esc_attr( sprintf( __( 'Restore &#8220;%s&#8221; from the Trash' ), $title ) ),
					__( 'Restore' )
				);
			} elseif ( EMPTY_TRASH_DAYS ) {
				$actions['trash'] = sprintf(
					'<a href="%s" class="submitdelete" aria-label="%s">%s</a>',
					get_delete_post_link( $post->ID ),
					/* translators: %s: post title */
					esc_attr( sprintf( __( 'Move &#8220;%s&#8221; to the Trash' ), $title ) ),
					_x( 'Trash', 'verb' )
				);
			}
			if ( 'trash' === $post->post_status || ! EMPTY_TRASH_DAYS ) {
				$actions['delete'] = sprintf(
					'<a href="%s" class="submitdelete" aria-label="%s">%s</a>',
					get_delete_post_link( $post->ID, '', true ),
					/* translators: %s: post title */
					esc_attr( sprintf( __( 'Delete &#8220;%s&#8221; permanently' ), $title ) ),
					__( 'Delete Permanently' )
				);
			}
		}

		return $this->row_actions( $actions );
	}

}
