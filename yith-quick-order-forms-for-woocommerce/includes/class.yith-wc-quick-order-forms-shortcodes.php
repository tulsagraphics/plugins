<?php
/**
 * Created by PhpStorm.
 * User: dssaez
 * Date: 7/06/17
 * Time: 12:36
 */
if ( ! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'Quick_Order_Forms_Shortcodes' ) ) {

	class Quick_Order_Forms_Shortcodes {

		public $array_shortcodes = array();

		public static function init() {

			$shortcodes = array(
				'yith_wc_quick_order_form' => __CLASS__ . '::display_form'
			);

			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}

			shortcode_atts( array( 'id' => '' ), array(), 'yith_wc_quick_order_form' );

		}

		public static function display_form( $atts ) {

			$Yith_Post_ID = $atts['id'];

			$Visibility_Restrictied = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_visibility_restricted_textarea', true );

			/* =========================================================
			======== Checking if the user has to be logged in ==========
			==========================================================*/
			$Yith_All_Users = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_all_users_checkbox', true );

			if ( ! $Yith_All_Users ) {

				if ( ! is_user_logged_in() ) {

					echo $Visibility_Restrictied;

					return;

				} else {

					$User_ID = get_current_user_id();

					$Check_role   = 1;
					$Not_in_users = 0;

					/* ==== We get the option of the user lists ==== */
					$Yith_option_users_list = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_users_list_radio_button', true );

					if ( $Yith_option_users_list != 'radio_all_users_list' ) {

						/* =========================================================
						======= Checking if the user is in the disable list ========
						==========================================================*/

						$Yith_Customers_enable_disable = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_enable_disable_customers_selected_select_multiple', true );
						$Yith_Customers_enable_disable = ( empty( $Yith_Customers_enable_disable ) ? array() : $Yith_Customers_enable_disable );

						if ( ! empty( $Yith_Customers_enable_disable ) && ( $Yith_option_users_list == 'radio_disable_users_list' ) ) {
							if ( in_array( $User_ID, $Yith_Customers_enable_disable ) ) {
								echo $Visibility_Restrictied;

								return;
							}
						}

						/* =========================================================
						======= Checking if the user is in the enable list =========
						==========================================================*/

						if ( $Yith_option_users_list == 'radio_enable_users_list' ) {
							if ( in_array( $User_ID, $Yith_Customers_enable_disable ) ) {
								$Check_role = 0;
							} else {
								$Not_in_users = 1;
							}
						}

					} // -> ----- radio_all_users_list ---------

					/* ==== We get the option of the user roles lists ==== */

					$Yith_option_user_roles_list = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_user_roles_list_radio_button', true );

					if ( $Yith_option_user_roles_list != 'radio_all_user_roles_list' ) {

						if ( $Check_role ) {

                            global $wp_roles;

							$User_Role = get_userdata( $User_ID )->roles;

							/* =========================================================
							=== Checking if the user role is in the disable role list ==
							==========================================================*/
							$Yith_Role_enable_disable = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_enable_disable_roles_selected_select_multiple', true );

							if ( ! empty( $Yith_Role_enable_disable ) && ( $Yith_option_user_roles_list == 'radio_disable_user_roles_list' ) ) {

								$Yith_Role_enable_disable = array_map( 'strtolower', $Yith_Role_enable_disable );

                                $role_name = $wp_roles->role_names[ $User_Role[0] ];

								if ( in_array( strtolower( $role_name ), $Yith_Role_enable_disable ) ) {
									echo $Visibility_Restrictied;

									return;
								}

							}

							/* =========================================================
							=== Checking if the user role is in the enable role list ===
							==========================================================*/

							if ( $Yith_option_user_roles_list == 'radio_enable_user_roles_list' ) {

								$Yith_Role_enable_disable = ( empty( $Yith_Role_enable_disable ) ? array() : $Yith_Role_enable_disable );

								$Yith_Role_enable_disable = array_map( 'strtolower', $Yith_Role_enable_disable );

                                $role_name = $wp_roles->role_names[ $User_Role[0] ];

								if ( ! in_array( strtolower( $role_name ), $Yith_Role_enable_disable ) ) {
									echo $Visibility_Restrictied;

									return;
								} else {
									$Not_in_users = 0;
								}

							}

							if ( $Not_in_users ) {
								echo $Visibility_Restrictied;

								return;
							}

						} // -> ----- $Check_role ---------

					} // -> ----- radio_all_user_roles_list ---------

				} // -> ----- !is_user_logged_in() ---------

			} // -> ----- $Yith_All_Users ---------

			$array_products = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_products_selected_select_multiple', true );

			$array_categories = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_categories_selected_select_multiple', true );

			$array_tags = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_tags_selected_select_multiple', true );

			ob_start();

			echo "<div class='Yith_WC_QOF_Main_Product_Div_Class' id='Yith_WC_QOF_Main_Product_Div'>";

			$args = array(
				'array_products'   => $array_products,
				'array_categories' => $array_categories,
				'array_tags'       => $array_tags,
				'Yith_Post_ID'     => $Yith_Post_ID,
			);
			wc_get_template( 'yith_show_products.php', $args, '', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_TEMPLATE_PATH' ) . 'custom_post_types/' );

			echo "</div>";

			$content = ob_get_clean();

			return $content;

		}

	}

}
