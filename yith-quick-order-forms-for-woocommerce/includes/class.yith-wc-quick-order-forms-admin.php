<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

/**
 *
 *
 * @class      YITH_WC_Favoritos_Admin
 * @package    Yithemes
 * @since      Version 1.0.0
 * @author
 *
 */

if ( ! class_exists( 'YITH_WC_Quick_Order_Forms_Admin' ) ) {
	/**
	 * Class YITH_WC_Favoritos_Admin
	 *
	 * @author
	 */
	class YITH_WC_Quick_Order_Forms_Admin {

		/**
		 * @var Panel object
		 */
		protected $_panel = null;

		/**
		 * @var Panel page
		 */
		protected $_panel_page = 'yith_wc_quick_order_forms_panel';

		/**
		 * @var bool Show the premium landing page
		 */
		public $show_premium_landing = true;

		/**
		 * @var string Official plugin documentation
		 */
		protected $_official_documentation = 'https://docs.yithemes.com/yith-quick-order-forms-for-woocommerce/';

		/**
		 * @var string Official plugin landing page
		 */
		protected $_premium_landing = 'https://yithemes.com/themes/plugins/yith-quick-order-forms-for-woocommerce/';

		/**
		 * @var string Official plugin landing page
		 */
		protected $_premium_live = 'https://plugins.yithemes.com/yith-quick-order-forms-for-woocommerce/';

        /**
         * @var string Official plugin support page
         */
        protected $_support = 'https://yithemes.com/my-account/support/dashboard/';

		/**
		 * Construct
		 *
		 * @author Daniel Sanchez Saez <dssaez@gmail.com>
		 * @since  1.0
		 */
		/**
		 * @var doc_url
		 */
		protected $doc_url = '';

		/**
		 * @var string Official plugin documentation
		 */

		/**
		 * @var widget class of the cart
		 */
		public $widget_cart = null;

		public function __construct() {

			add_filter( 'plugin_action_links_' . plugin_basename( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . '/' . basename( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_FILE' ) ) ), array(
				$this,
				'action_links'
			) );

            add_filter( 'plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 4 );

			/* ====== ENQUEUE STYLES AND JS ====== */

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

			/* ====== Sessions initation for ajax to include the classes with "Backend" value ====== */
			$this->yith_woocommerce_sessions();

			/* === Register Panel Settings === */
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );

			/* === Forms Tab === */
			add_action( 'yith_wc_quick_order_forms_show_tab', array( $this, 'show_forms_tab' ) );

			/* ====== AJAX ADMIN FUNCTIONS ====== */

			require_once( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . 'includes/class.yith-wc-' . YITH_WC_QOF_FILES_INCLUDE_NAME . '-ajax-admin.php' );
			$this->ajax = YITH_WC_Quick_Order_Forms_Ajax_Admin::get_instance();

			/* ====== Adding and saving checkbox show only forms to the widget cart ====== */

			add_filter( 'in_widget_form', array( $this, 'yith_add_show_only_in_forms_to_widget_wc_cart' ), 10, 3 );

			add_filter( 'widget_update_callback', array(
				$this,
				'yith_save_show_only_in_forms_to_widget_wc_cart'
			), 10, 4 );

		}

		public function action_links( $links ) {

			$links[] = '<a href="' . admin_url( "admin.php?page={$this->_panel_page}" ) . '">' . __( 'Settings', 'yith-quick-order-forms-for-woocommerce' ) . '</a>';

			$links[] = sprintf( '<a href="%s">%s</a>', YIT_Plugin_Licence::get_license_activation_url(),__( 'License',  'yith-woocommerce-gift-cards' ) );

			return $links;
		}

        /**
         * plugin_row_meta
         *
         * add the action links to plugin admin page
         *
         * @param $plugin_meta
         * @param $plugin_file
         * @param $plugin_data
         * @param $status
         *
         * @return   Array
         * @since    1.0.32
         * @author   Daniel Sánchez <daniel.sanchez@yithemes.com>
         * @use      plugin_row_meta
         */
        public function plugin_row_meta( $plugin_meta, $plugin_file, $plugin_data, $status, $init_file = 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_INIT' ) {

            $to_add = array();
            if ( defined( $init_file ) && constant( $init_file ) == $plugin_file ) {

                $to_add[] = array(
                    'label' => _x( 'Live Demo', 'Plugin Row Meta', 'yith-woocommerce-gift-cards' ),
                    'url'   => $this->_premium_live,
                    'icon'  => 'dashicons  dashicons-laptop',
                );

                $to_add[] = array(
                    'label' => _x( 'Documentation', 'Plugin Row Meta', 'yith-woocommerce-gift-cards' ),
                    'url'   => $this->_official_documentation,
                    'icon'  => 'dashicons  dashicons-search',
                );

                $to_add[] = array(
                    'label' => __( 'Support', 'Plugin Row Meta', 'yith-woocommerce-gift-cards' ),
                    'url'   => $this->_support,
                    'icon'  => 'dashicons  dashicons-admin-users',
                );
            }

            foreach ( $to_add as $row_meta ){
                $plugin_meta[] = sprintf( '<a href="%s" target="_blank"><span class="%s"></span>%s</a>', $row_meta['url'], $row_meta['icon'], $row_meta['label'] );
            }

            return $plugin_meta;

        }

		public function yith_add_show_only_in_forms_to_widget_wc_cart( $widget, $return, $instance ) {

			// Are we dealing with a nav menu widget?
			if ( 'woocommerce_widget_cart' == $widget->id_base ) {

				// Display the description option.
				$only_in_forms = isset( $instance['only_in_forms'] ) ? $instance['only_in_forms'] : '';
				?>
				<p>
					<input class="checkbox" type="checkbox" id="<?php echo $widget->get_field_id( 'only_in_forms' ); ?>" name="<?php echo $widget->get_field_name( 'only_in_forms' ); ?>" <?php checked( true, $only_in_forms ); ?> />
					<label for="<?php echo $widget->get_field_id( 'only_in_forms' ); ?>">
						<?php _e( 'Show only in forms', 'yith-quick-order-forms-for-woocommerce' ); ?>
					</label>
				</p>
				<?php
			}
		}

		public function yith_save_show_only_in_forms_to_widget_wc_cart( $instance, $new_instance, $old, $widget ) {

			if ( isset( $instance['woocommerce_widget_cart'] ) ) {
				error_log( print_r( 'woocommerce_widget_cart enable', true ) );
			}

			// Is the instance a nav menu and are descriptions enabled?
			if ( 'woocommerce_widget_cart' == $widget->id_base && ! empty( $new_instance['only_in_forms'] ) ) {
				$new_instance['only_in_forms'] = 1;
			}

			return $new_instance;
		}


		public function register_panel() {

			if ( ! empty( $this->_panel ) ) {
				return;
			}

			$menu_title = __( 'Quick Order Forms', 'yith-quick-order-forms-for-woocommerce' );

			$admin_tabs = apply_filters( 'yith_wc_quick_order_forms_admin_tabs', array(
					'forms' => __( 'Forms', 'yith-quick-order-forms-for-woocommerce' ),
				)
			);

			$args = array(
				'create_menu_page' => true,
				'parent_slug'      => '',
				'page_title'       => $menu_title,
				'menu_title'       => $menu_title,
				'capability'       => 'manage_options',
				'parent'           => '',
				'parent_page'      => 'yit_plugin_panel',
				'page'             => $this->_panel_page,
				'admin-tabs'       => $admin_tabs,
				'options-path'     => constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_OPTIONS_PATH' ),
			);

			/* === Fixed: not updated theme/old plugin framework  === */
			if ( ! class_exists( 'YIT_Plugin_Panel_WooCommerce' ) ) {
				require_once( 'plugin-fw/lib/yit-plugin-panel-wc.php' );
			}

			$this->_panel = new YIT_Plugin_Panel_WooCommerce( $args );
		}

		public function show_forms_tab() {

			if ( file_exists( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . 'templates/form_tab.php' ) ) {
				require_once( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . 'templates/form_tab.php' );
			}

		}


		public function enqueue_scripts() {

			/* ====== Style ====== */

			wp_register_style( 'yith-wc-' . YITH_WC_QOF_CONSTANT_NAME . '-style', apply_filters( 'yith_wc_qof_admin_css', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . 'css/yith-qof-admin.css' ), array(), constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ) );
			wp_enqueue_style( 'yith-wc-' . YITH_WC_QOF_CONSTANT_NAME . '-style' );

			/* ====== Script ====== */

            if ( ! wp_script_is( 'selectWoo' ) ) {
                wp_enqueue_script( 'selectWoo' );
                wp_enqueue_script( 'wc-enhanced-select' );
            }

			wp_register_script( 'yith-wc-' . YITH_WC_QOF_CONSTANT_NAME . '-js', apply_filters( 'yith_wc_qof_admin_js', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . 'js/yith-qof-admin.js' ), array(
				'jquery',
				'jquery-ui-sortable'
			), constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ), true );

			global $woocommerce;

			wp_localize_script( 'yith-wc-' . YITH_WC_QOF_CONSTANT_NAME . '-js', 'yith_wc_quick_order_forms_object', apply_filters( 'yith_wc_qof_frontend_localize', array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
                'string_choose_rol' => __( 'Choose a role', 'yith-quick-order-forms-for-woocommerce' ),
				'yith_wc_search_categories_nonce' => wp_create_nonce( 'yith_wc_qof_category_search' ),
				'yith_wc_search_tags_nonce' => wp_create_nonce( 'yith_wc_qof_tag_search' ),
				'yith_wc_search_roles_nonce' => wp_create_nonce( 'yith_wc_qof_role_search' ),
				'yith_wc_legacy' => version_compare( $woocommerce->version, '3.0', '<' ),
				'ajax_loader' => constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . '/images/ajax-loader.gif',
			) ) );

			wp_enqueue_script( 'yith-wc-' . YITH_WC_QOF_CONSTANT_NAME . '-js' );

			wp_enqueue_script( 'yith-wc-' . YITH_WC_QOF_CONSTANT_NAME . '-yith-qof-onload-js', apply_filters( 'yith_wc_qof_admin_onload', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . 'js/yith-qof-onload.js' ) );

		}

		public function yith_woocommerce_sessions() {

			$_SESSION[ 'yith_wc_' . YITH_WC_QOF_CONSTANT_NAME . '_current_ajax' ] = "Backend";

		}

	}
}
