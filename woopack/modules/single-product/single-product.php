<?php
/**
* @class WooPackSingleProduct
*/
class WooPackSingleProduct extends FLBuilderModule {
    /**
    * Constructor function for the module. You must pass the
    * name, description, dir and url in an array to the parent class.
    *
    * @method __construct
    */
    public function __construct()
    {
        parent::__construct(array(
            'name' 				=> __('Single Product', 'woopack'),
            'description' 		=> __('Addon to display Single Product.', 'woopack'),
            'group'             => WooPack_Helper::get_modules_group(),
            'category' 			=> WOOPACK_CAT,
            'dir' 				=> WOOPACK_DIR . 'modules/single-product/',
            'url' 				=> WOOPACK_URL . 'modules/single-product/',
            'editor_export' 	=> true, // Defaults to true and can be omitted.
            'enabled' 			=> true, // Defaults to true and can be omitted.
        ));
    }

     /**
    * @method enqueue_scripts
    */
    public function enqueue_scripts()
    {
        $this->add_js('jquery-imagesloaded');
    }
}

/**
* Register the module and its form settings.
*/
FLBuilder::register_module('WooPackSingleProduct', array(
    'layouts'   => array(
        'title'     => __('Layout', 'woopack'),
        'sections'  => array(
            'layout'    => array(
                'title'     => '',
                'fields'    => array(
                    'product_layout'    => array(
                        'type'              => 'layout',
                        'label'             => __('', 'woopack'),
                        'default'           => 1,
                        'options'           => array(
                            1                    => WOOPACK_URL . 'modules/single-product/images/layout-1.png',
                            2                    => WOOPACK_URL . 'modules/single-product/images/layout-2.png',
                            3                    => WOOPACK_URL . 'modules/single-product/images/layout-3.png',
                            4                    => WOOPACK_URL . 'modules/single-product/images/layout-4.png',
                        ),
                    ),
                ),
            ),
        ),
    ),
    'content'   => array(
        'title'     => __('Content', 'woopack'),
        'sections'  => array(
            'product'           => array(
                'title'             => __('Product', 'woopack'),
                'fields'            => array(
                    'product_id'                => array(
                        'type'                      => 'select',
                        'label'                     => __('Product', 'woopack'),
                        'options'                   => WooPack_Helper::get_products_list()
                    ),
                    'product_title'             => array(
                        'type'                      => 'select',
                        'label'                     => __('Show Product Title?', 'woopack'),
                        'default'                   => 'yes',
                        'options'                   => array(
                            'yes'                       => __('Yes', 'woopack'),
                            'no'                        => __('No', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'yes'                       => array(
                                'sections'                  => array( 'product_title_style' ),
                            ),
                        ),
                    ),
                    'product_price'             => array(
                        'type'                      => 'select',
                        'label'                     => __('Show Price?', 'woopack'),
                        'default'                   => 'yes',
                        'options'                   => array(
                            'yes'                       => __('Yes', 'woopack'),
                            'no'                        => __('No', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'yes'                       => array(
                                'sections'              => array( 'product_price_align', 'regular_price_fonts', 'sale_price_fonts' ),
                            ),
                        ),
                    ),
                    'product_rating'            => array(
                        'type'                      => 'select',
                        'label'                     => __('Show Rating?', 'woopack'),
                        'default'                   => 'yes',
                        'options'                   => array(
                            'yes'                       => __('Yes', 'woopack'),
                            'no'                        => __('No', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'yes'                       => array(
                                'sections'              => array( 'product_rating_style' ),
                            ),
                        ),
                    ),
                    'product_short_description' => array(
                        'type'                      => 'select',
                        'label'                     => __('Show Short Description?', 'woopack'),
                        'default'                   => 'yes',
                        'options'                   => array(
                            'yes'                       => __('Yes', 'woopack'),
                            'no'                        => __('No', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'yes'                       => array(
                                'sections'                  => array( 'short_description' ),
                            ),
                        ),
                    ),
                ),
            ),
            'sale_badge'        => array(
                'title'         => __('Sale Badge', 'woopack'),
                'fields'        => array(
                    'show_sale_badge'   => array(
                        'type'              => 'select',
                        'label'             => __('Show Sale Badge?', 'woopack'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no'                => __('No', 'woopack'),
                        ),
                        'toggle'            => array(
                            'yes'               => array(
                                'sections'          => array( 'sale_badge_style', 'sale_badge_fonts' ),
                            ),
                        ),
                    ),
                ),
            ),
            'product_button'    => array(
                'title'             => __('Button', 'woopack'),
                'fields'            => array(
                    'button_type'       => array(
                        'type'              => 'select',
                        'label'             => __('Button Type', 'woopack'),
                        'default'           => 'cart',
                        'options'           => array(
                            'cart'              => __('Add to Cart', 'woopack'),
                            'custom'            => __('Custom', 'woopack'),
                            'none'              => __('None', 'woopack'),
                        ),
                        'toggle'            => array(
                            'cart'              => array(
                                'tabs'				=> array( 'button' ),
                            ),
                            'custom'            => array(
                                'tabs'				=> array( 'button' ),
                                'fields'            => array( 'button_text', 'button_link', 'button_link_target' ),
                            ),
                        ),
                    ),
                    'button_text'       => array(
                        'type'              => 'text',
                        'label'             => __('Custom Button Text', 'woopack'),
                        'connections'       => array('string'),
                        'preview'           => array(
                            'type'      		=> 'text',
                            'selector'  		=> '.woocommerce-product-add-to-cart .woopack-product-button-custom'
                        ),
                    ),
                    'button_link'       => array(
                        'type'              => 'link',
                        'label'             => __('Link', 'woopack'),
                        'connections'       => array('url'),
                    ),
                    'button_link_target'=> array(
                        'type'              => 'select',
                        'label'             => __('Target', 'woopack'),
                        'options'			=> array(
                            '_self'				=> __('Same Window', 'woopack'),
                            '_blank'			=> __('New Window', 'woopack')
                        )
                    ),
                ),
            ),
            'product_image'     => array(
                'title'             => __('Product Image', 'woopack'),
                'fields'            => array(
                    'show_image'        => array(
                        'type'              => 'select',
                        'label'             => __('Show Image?', 'woopack'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no'                => __('No', 'woopack'),
                        ),
                        'toggle'            => array(
                            'yes'               => array(
                                'sections'          => array( 'image_style' ),
                                'fields'            => array( 'image_size' ),
                            ),
                        ),
                    ),
                    'image_size'        => array(
                        'type'              => 'photo-sizes',
                        'label'             => __('Size', 'woopack'),
                        'default'           => 'medium',
                    ),
                ),
            ),
            'product_taxonomy'  => array(
                'title'             => __('Product Meta', 'woopack'),
                'fields'            => array(
					'show_sku'			=> array(
						'type'				=> 'select',
						'label'				=> __('Show SKU', 'woopack'),
						'default'			=> 'no',
						'options' 		    => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no' 			    => __('No', 'woopack'),
                        ),
					),
                    'show_taxonomy'             => array(
                        'type'                      => 'select',
                        'label'                     => __('Show Taxonomy', 'woopack'),
                        'default' 		            => 'no',
                        'options' 		            => array(
                            'yes'                       => __('Yes', 'woopack'),
                            'no' 			            => __('No', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'yes'                       => array(
                                'sections'                  => array('meta_style'),
                                'fields'                    => array('select_taxonomy', 'show_taxonomy_custom_text')
                            )
                        )
                    ),
                    'select_taxonomy'           => array(
                        'type'                      => 'select',
                        'label'                     => __('Select Taxonomy', 'woopack'),
                        'options'                   => WooPack_Helper::get_taxonomies_list()
                    ),
                    'show_taxonomy_custom_text' => array(
                        'type'                      => 'select',
                        'label'                     => __('Use Custom Taxonomy Label?', 'woopack'),
                        'default'                   => 'no',
                        'options' 		            => array(
                            'yes'                       => __('Yes', 'woopack'),
                            'no' 			            => __('No', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'yes'                       => array(
                                'fields'                    => array('taxonomy_custom_text')
                            )
                        )
                    ),
                    'taxonomy_custom_text'      => array(
                        'type'                      => 'text',
                        'label'                     => __('Custom Label', 'woopack'),
                        'default'                   => __('Category: ', 'woopack'),
                        'connections'               => array('string'),
                    ),
                ),
            ),
        ),
    ),
    'style'     => array(
        'title'     => __('Style', 'woopack'),
        'sections'  => array(
            'box'               => array(
                'title'             => __('Box', 'woopack'),
                'fields'            => array(
                    'content_bg_color'      => array(
                        'type'                  => 'color',
                        'label'                 => __('Background Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true,
                        'show_alpha'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woopack-single-product',
                            'property'              => 'background-color',
                        ),
                    ),
                    'content_bg_color_hover'=> array(
                        'type'                  => 'color',
                        'label'                 => __('Background Hover Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true,
                        'show_alpha'            => true,
                        'preview'               => array(
                            'type'                  => 'none'
                        )
                    ),
                    'content_alignment'     => array(
                        'type'                  => 'select',
                        'label'                 => __('Content Alignment', 'woopack'),
                        'default'               => 'default',
                        'options'               => array(
                            'default'               => __('Default', 'woopack'),
                            'left'                  => __('Left', 'woopack'),
                            'center'                => __('Center', 'woopack'),
                            'right'                 => __('Right', 'woopack'),
                        ),
                    ),
                    'box_padding'       => array(
                        'type'                  => 'dimension',
                        'label' 			    => __('Padding', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'			    => '.woopack-single-product',
                            'property'			    => 'padding',
                            'unit' 				    => 'px'
                        ),
                        'responsive'            => array(
                            'placeholder' 		    => array(
                                'default'               => '',
                                'medium' 			    => '',
                                'responsive'            => '',
                            ),
                        ),
                    ),
                    'box_border_style'      => array(
                        'type'                  => 'select',
                        'label'                 => __('Border Style', 'woopack'),
                        'default'               => 'none',
                        'options'               => array(
                            'none'                  => __('None', 'woopack'),
                            'solid'                 => __('Solid', 'woopack'),
                            'dotted'                => __('Dotted', 'woopack'),
                            'dashed'                => __('Dashed', 'woopack'),
                        ),
                        'toggle'                => array(
                            'solid'                 => array(
                                'fields'                => array( 'box_border', 'box_border_color'),
                            ),
                            'dotted'                => array(
                                'fields'                => array( 'box_border', 'box_border_color'),
                            ),
                            'dashed'                => array(
                                'fields'                => array( 'box_border', 'box_border_color'),
                            ),
                        ),
                    ),
                    'box_border'    	=> array(
                        'type'                  => 'dimension',
                        'label'                 => __('Border', 'woopack'),
                        'description' 		    => 'px',
                        'default' 			    => '1',
                        'preview' 			    => array(
                            'type'                  => 'css',
                            'selector'			    => '.woopack-single-product',
                            'property'      	    => 'border-width',
                            'unit'				    => 'px',
                        ),
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'box_border_color'      => array(
                        'type'                  => 'color',
                        'label'                 => __('Border Color', 'woopack'),
                        'show_reset'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woopack-single-product',
                            'property'              => 'border-color',
                        ),
                    ),
                    'box_border_radius'     => array(
                        'type'                  => 'unit',
                        'label'                 => __('Round Corners', 'woopack'),
                        'description'           => 'px',
                        'default'               => '0',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woopack-single-product',
                            'property'              => 'border-radius',
                            'unit'                  => 'px',
                        ),
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                ),
            ),
            'box_shadow'        => array(
                'title'             => __('Box Shadow', 'woopack'),
                'fields'            => array(
                    'box_shadow'        => array(
                        'type'              => 'select',
                        'label'             => __('Enable Box Shadow?', 'woopack'),
                        'default'           => 'no',
                        'options'           => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no'                => __('No', 'woopack'),
                        ),
                        'toggle'            => array(
                            'yes'               => array(
                                'fields'            => array('box_shadow_h', 'box_shadow_v', 'box_shadow_blur', 'box_shadow_spread', 'box_shadow_color'),
                            ),
                        ),
                    ),
                    'box_shadow_h'      => array(
                        'type'              => 'unit',
                        'label'             => __('Horizontal', 'woopack'),
                        'default' 		    => '0',
                        'description' 	    => 'px',
                        'preview' 		    => array(
                            'type'              => 'css',
                            'selector'          => '.woopack-single-product',
                            'property'          => 'box-shadow',
                        ),
                        'responsive'        => array(
                            'placeholder'       => array(
                                'default' 		    => '',
                                'medium' 			=> '',
                                'responsive'		=> '',
                            ),
                        ),
                    ),
                    'box_shadow_v'      => array(
                        'type'              => 'unit',
                        'label'             => __('Vertical', 'woopack'),
                        'default'           => '0',
                        'description' 	    => 'px',
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.woopack-single-product',
                            'property'          => 'box-shadow',
                        ),
                        'responsive' 		=> array(
                            'placeholder'       => array(
                                'default' 		    => '',
                                'medium' 			=> '',
                                'responsive'		=> '',
                            ),
                        ),
                    ),
                    'box_shadow_blur'   => array(
                        'type'              => 'unit',
                        'label'             => __('Blur', 'woopack'),
                        'default'           => '0',
                        'description' 	    => 'px',
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.woopack-single-product',
                            'property'          => 'box-shadow',
                        ),
                        'responsive'        => array(
                            'placeholder'       => array(
                                'default' 		    => '',
                                'medium' 			=> '',
                                'responsive'		=> '',
                            ),
                        ),
                    ),
                    'box_shadow_spread' => array(
                        'type'              => 'unit',
                        'label'             => __('Spread', 'woopack'),
                        'default'           => '0',
                        'description'       => 'px',
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.woopack-single-product',
                            'property'          => 'box-shadow',
                        ),
                        'responsive'        => array(
                            'placeholder'       => array(
                                'default' 		    => '',
                                'medium' 			=> '',
                                'responsive'		=> '',
                            ),
                        ),
                    ),
                    'box_shadow_color'  => array(
                        'type'              => 'color',
                        'label'             => __('Color', 'woopack'),
                        'show_reset'        => true,
                        'show_alpha'        => true,
                        'default'           => 'rgba(0,0,0,0.2)',
                    ),
                ),
            ),
            'content_style' 	=> array(
                'title'             => __('Content', 'woopack'),
                'fields'            => array(
                    'content_padding'       => array(
                        'type'                      => 'dimension',
                        'label' 			        => __('Padding', 'woopack'),
                        'description' 		        => 'px',
                        'preview' 			        => array(
                            'type'                      => 'css',
                            'selector'                  => '.woopack-single-product .product-content',
                            'property'                  => 'padding',
                            'unit'                      => 'px'
                        ),
                        'responsive' 		        => array(
                            'placeholder'               => array(
                                'default' 			        => '',
                                'medium' 			        => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                ),
            ),
            'product_rating_style'=> array(
                'title'                 => __('Rating', 'woopack'),
                'fields'                => array(
                    'product_rating_size'           => array(
                        'type'                          => 'unit',
                        'label'                         => __('Size', 'woopack'),
                        'description' 			        => _x('px', 'Value unit for font size. Such as: "15 px"', 'woopack'),
                        'responsive' 			        => array(
                            'placeholder'                   => array(
                                'default'                       => '20',
                                'medium'                        => '',
                                'responsive'                    => '',
                            ),
                        ),
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'				        => '.woocommerce-product-rating',
                            'property'                      => 'font-size',
                            'unit'                          => 'px',
                        ),
                    ),
                    'product_rating_default_color'  => array(
                        'type'                          => 'color',
                        'label'                         => __('Rating Star Color', 'woopack'),
                        'show_reset'                    => true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.woocommerce-product-rating .star-rating:before',
                            'property'                      => 'color',
                        ),
                    ),
                    'product_rating_color'          => array(
                        'type'                          => 'color',
                        'label'                         => __('Rating Star Active Color', 'woopack'),
                        'show_reset'                    => true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.woocommerce-product-rating .star-rating span:before',
                            'property'                      => 'color',
                        ),
                    ),
                    'product_rating_margin'         => array(
                        'type'                          => 'unit',
                        'label'                         => __('Margin Bottom', 'woopack'),
                        'default'                       => '',
                        'description'                   => 'px',
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.woopack-single-product .woocommerce-product-rating',
                            'property'                      => 'margin-bottom',
                            'unit'                          => 'px',
                        ),
                        'responsive' 			        => array(
                            'placeholder' 			       => array(
                                'default' 				        => '',
                                'medium' 				        => '',
                                'responsive'                    => '',
                            ),
                        ),
                    ),
                ),
            ),
            'sale_badge_style' 	=> array(
                'title'             => __('Sale Badge', 'woopack'),
                'fields'            => array(
                    'badge_position'            => array(
                        'type'                      => 'select',
                        'label'  			        => __('Sale Badge Position', 'woopack'),
                        'default'                   => 'top-left',
                        'options' 		            => array(
                            'top-left'                  => __('Top Left', 'woopack'),
                            'top-center'                => __('Top Center', 'woopack'),
                            'top-right'                 => __('Top Right', 'woopack'),
                            'bottom-left'               => __('Bottom Left', 'woopack'),
                            'bottom-center'             => __('Bottom Center', 'woopack'),
                            'bottom-right'              => __('Bottom Right', 'woopack'),
                        ),
                    ),
                    'badge_bg_color'            => array(
                        'type'                      => 'color',
                        'label'                     => __('Background Color', 'woopack'),
                        'default'                   => '1e8cbe',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'			        => '.onsale',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'badge_color'               => array(
                        'type'                      => 'color',
                        'label'                     => __('Color', 'woopack'),
                        'show_reset'                => true,
                        'default'                   => 'ffffff',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.onsale',
                            'property'                  => 'color',
                        ),
                    ),
					'badge_margin_left_right'     	=> array(
	                    'type'              		=> 'unit',
	                    'label' 					=> __('Horizontal Spacing', 'woopack'),
	                    'description' 				=> 'px',
	                    'preview' 					=> array(
	                        'type' 						=> 'css',
	                        'rules'						=> array(
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'margin-left',
			                        'unit' 						=> 'px'
								),
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'margin-right',
			                        'unit' 						=> 'px'
								),
							)
	                    ),
	                    'responsive' 				=> array(
	                        'placeholder' 				=> array(
	                            'default' 					=> '',
	                            'medium' 					=> '',
	                            'responsive'				=> '',
	                        ),
	                    ),
	                ),
					'badge_margin_top_bottom'     	=> array(
	                    'type'              		=> 'unit',
	                    'label' 					=> __('Vertical Spacing', 'woopack'),
	                    'description' 				=> 'px',
	                    'preview' 					=> array(
	                        'type' 						=> 'css',
	                        'rules'						=> array(
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'margin-top',
			                        'unit' 						=> 'px'
								),
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'margin-bottom',
			                        'unit' 						=> 'px'
								),
							)
	                    ),
	                    'responsive' 				=> array(
	                        'placeholder' 				=> array(
	                            'default' 					=> '',
	                            'medium' 					=> '',
	                            'responsive'				=> '',
	                        ),
	                    ),
	                ),
                    'badge_padding_left_right'     	=> array(
	                    'type'              		=> 'unit',
	                    'label' 					=> __('Padding Left/Right', 'woopack'),
	                    'description' 				=> 'px',
	                    'preview' 					=> array(
	                        'type' 						=> 'css',
	                        'rules'						=> array(
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'padding-left',
			                        'unit' 						=> 'px'
								),
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'padding-right',
			                        'unit' 						=> 'px'
								),
							)
	                    ),
	                    'responsive' 				=> array(
	                        'placeholder' 				=> array(
	                            'default' 					=> '',
	                            'medium' 					=> '',
	                            'responsive'				=> '',
	                        ),
	                    ),
	                ),
					'badge_padding_top_bottom'     	=> array(
	                    'type'              		=> 'unit',
	                    'label' 					=> __('Padding Top/Bottom', 'woopack'),
	                    'description' 				=> 'px',
	                    'preview' 					=> array(
	                        'type' 						=> 'css',
	                        'rules'						=> array(
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'padding-top',
			                        'unit' 						=> 'px'
								),
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'padding-bottom',
			                        'unit' 						=> 'px'
								),
							)
	                    ),
	                    'responsive' 				=> array(
	                        'placeholder' 				=> array(
	                            'default' 					=> '',
	                            'medium' 					=> '',
	                            'responsive'				=> '',
	                        ),
	                    ),
	                ),
                    'sale_badge_border_style'   => array(
                        'type'                      => 'select',
                        'label'                     => __('Border Style', 'woopack'),
                        'default'                   => 'none',
                        'options'                   => array(
                            'none'                      => __('None', 'woopack'),
                            'solid'                     => __('Solid', 'woopack'),
                            'dotted'                    => __('Dotted', 'woopack'),
                            'dashed'                    => __('Dashed', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'solid'                     => array(
                                'fields'                    => array( 'sale_badge_border_width', 'sale_badge_border_color'),
                            ),
                            'dotted'                    => array(
                                'fields'                    => array( 'sale_badge_border_width', 'sale_badge_border_color'),
                            ),
                            'dashed'                    => array(
                                'fields'                    => array( 'sale_badge_border_width', 'sale_badge_border_color'),
                            ),
                        ),
                    ),
                    'sale_badge_border_width'   => array(
                        'type'                      => 'unit',
                        'label' 			        => __('Width', 'woopack'),
                        'description'               => 'px',
                        'default'                   => '1',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.onsale',
                            'property'                  => 'border-width',
                            'unit'                      => 'px',
                        ),
                        'responsive' 			    => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                    'sale_badge_border_color'   => array(
                        'type'                      => 'color',
                        'label'                     => __('Border Color', 'woopack'),
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.onsale',
                            'property'                  => 'border-color',
                        ),
                    ),
                    'sale_badge_border_radius'  => array(
                        'type'                      => 'unit',
                        'label'                     => __('Round Corners', 'woopack'),
                        'description'               => 'px',
                        'default'                   => '0',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.onsale',
                            'property'                  => 'border-radius',
                            'unit'                      => 'px',
                        ),
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                ),
            ),
            'meta_style'        => array(
                'title'             => __('Product Meta', 'woopack'),
                'fields'            => array(
                    'meta_text_color'    => array(
                        'type'              => 'color',
                        'label'             => __('Text Color', 'woopack'),
                        'show_reset'        => true,
                        'preview'           => array(
                            'type' 				=> 'css',
                            'selector'			=> '.woopack-single-product .product_meta',
                            'property'			=> 'color',
                        ),
                    ),
                    'meta_link_color'    => array(
                        'type'              => 'color',
                        'label'             => __('Link Color', 'woopack'),
                        'show_reset'        => true,
                    ),
                    'meta_border'	     => array(
                        'type'              => 'select',
                        'label'  			=> __('Show Divider', 'woopack'),
                        'default'           => 'yes',
                        'options' 		    => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no'             	=> __('No', 'woopack'),
                        ),
                        'toggle'            => array(
                            'yes'               => array(
                                'fields'            => array('meta_border_color')
                            )
                        )
                    ),
                    'meta_border_color'  => array(
                        'type'              => 'color',
                        'label'             => __('Divider Color', 'woopack'),
                        'show_reset'        => true,
                        'default'           => 'eeeeee',
                        'preview'           => array(
                            'type' 				=> 'css',
                            'selector'			=> '.woopack-single-product .product_meta',
                            'property'			=> 'border-color',
                        ),
                    ),
                    'meta_padding'   => array(
                        'type'              => 'dimension',
                        'label'             => __('Padding', 'woopack'),
                        'description' 		=> 'px',
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.woopack-single-product .product_meta',
                            'property'          => 'padding',
                            'unit'              => 'px'
                        ),
                        'responsive' 		=> array(
                            'placeholder' 		=> array(
                                'default' 			=> '',
                                'medium' 			=> '',
                                'responsive'		=> '',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'button'    => array(
        'title'     => __('Button', 'woopack'),
        'sections'  => array(
            'button_property'   => array(
                'title'             => __('Structure', 'woopack'),
                'fields'            => array(
                    'button_width'          => array(
                        'type'                  => 'select',
                        'label'                 => __('Width', 'woopack'),
                        'default'               => 'auto',
                        'options'               => array(
                            'auto'                  => __('Auto', 'woopack'),
                            'full_width'            => __('Full Width', 'woopack'),
                            'custom'                => __('Custom', 'woopack'),
                        ),
                        'toggle'                => array(
                            'custom'                => array(
                                'fields'                => array('button_width_custom')
                            ),
                        ),
                    ),
                    'button_width_custom'   => array(
                        'type'                  => 'unit',
                        'label'                 => __('Custom Width', 'woopack'),
                        'description' 			=> '%',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '30',
                                'medium' 				=> '',
                                'responsive' 			=> '',
                            ),
                        ),
                    ),
                    'button_margin_top'     => array(
                        'type'                  => 'unit',
                        'label'                 => __('Margin Top', 'woopack'),
                        'default'               => '5',
                        'description' 			=> 'px',
                        'preview' 				=> array(
                            'type' 					=> 'css',
                            'selector'				=> '.woocommerce-product-add-to-cart',
                            'property'				=> 'margin-top',
                            'unit' 					=> 'px'
                        ),
                        'responsive'        => array(
                            'placeholder'       => array(
                                'default' 		    => '',
                                'medium' 			=> '',
                                'responsive'		=> '',
                            ),
                        ),
                    ),
                    'button_margin_bottom'  => array(
                        'type'                  => 'unit',
                        'label'                 => __('Margin Bottom', 'woopack'),
                        'default'               => '15',
                        'description' 			=> 'px',
                        'preview' 				=> array(
                            'type' 					=> 'css',
                            'selector'				=> '.woocommerce-product-add-to-cart',
                            'property'				=> 'margin-bottom',
                            'unit' 					=> 'px'
                        ),
                        'responsive'        => array(
                            'placeholder'       => array(
                                'default' 		    => '',
                                'medium' 			=> '',
                                'responsive'		=> '',
                            ),
                        ),
                    ),
                ),
            ),
            'button_color'      => array(
                'title'             => __('Colors', 'woopack'),
                'fields'            => array(
                    'button_bg_color'       => array(
                        'type'                  => 'color',
                        'label'                 => __('Background Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true,
                        'show_alpha'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woocommerce-product-add-to-cart .add_to_cart_inline a, .woocommerce-product-add-to-cart a, .woocommerce-product-add-to-cart .button',
                            'property'              => 'background-color',
                        ),
                    ),
                    'button_bg_color_hover' => array(
                        'type'                  => 'color',
                        'label'                 => __('Background Hover Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true,
                        'show_alpha'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woocommerce-product-add-to-cart .add_to_cart_inline a:hover, .woocommerce-product-add-to-cart a:hover, .woocommerce-product-add-to-cart .button:hover',
                            'property'              => 'background-color',
                        ),
                    ),
                    'button_color'          => array(
                        'type'                  => 'color',
                        'label'                 => __('Text Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woocommerce-product-add-to-cart .add_to_cart_inline a, .woocommerce-product-add-to-cart a, .woocommerce-product-add-to-cart .button',
                            'property'              => 'color',
                        ),
                    ),
                    'button_color_hover'    => array(
                        'type'                  => 'color',
                        'label'                 => __('Text Hover Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true,
                    ),
                ),
            ),
            'button_border'     => array(
                'title'             => __('Border', 'woopack'),
                'fields'            => array(
                    'button_border_style'       => array(
                        'type'                      => 'select',
                        'label'                     => __('Border Style', 'woopack'),
                        'default'                   => 'default',
                        'options'                   => array(
                            'default'                   => __('Default', 'woopack'),
                            'none'                      => __('None', 'woopack'),
                            'solid'                     => __('Solid', 'woopack'),
                            'dotted'                    => __('Dotted', 'woopack'),
                            'dashed'                    => __('Dashed', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'solid'                     => array(
                                'fields'                    => array( 'button_border_width', 'button_border_color', 'button_border_color_hover'),
                            ),
                            'dotted'                    => array(
                                'fields'                    => array( 'button_border_width', 'button_border_color', 'button_border_color_hover'),
                            ),
                            'dashed'                    => array(
                                'fields'                    => array( 'button_border_width', 'button_border_color', 'button_border_color_hover'),
                            ),
                        ),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'			        => '.woocommerce-product-add-to-cart .add_to_cart_inline a, .woocommerce-product-add-to-cart a, .woocommerce-product-add-to-cart .button',
                            'property'                  => 'border-style',
                        ),
                    ),
                    'button_border_width'       => array(
                        'type'                      => 'unit',
                        'default' 			        => '',
                        'label'                     => __('Border Width', 'woopack'),
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'			        => '.woocommerce-product-add-to-cart .add_to_cart_inline a, .woocommerce-product-add-to-cart a, .woocommerce-product-add-to-cart .button',
                            'property'                  => 'border-width',
                            'unit'                      => 'px',
                        ),
                        'responsive' 			    => array(
                            'placeholder' 			    => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                    'button_border_color'       => array(
                        'type'                      => 'color',
                        'label'                     => __('Border Color', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'default'                   => '',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce-product-add-to-cart a, .woocommerce-product-add-to-cart .button',
                            'property'                  => 'border-color',
                        ),
                    ),
                    'button_border_color_hover' => array(
                        'type'                      => 'color',
                        'label'                     => __('Border Color on Hover', 'woopack'),
                        'default'			        => '',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce-product-add-to-cart a:hover, .woocommerce-product-add-to-cart .button:hover',
                            'property'                  => 'border-color',
                        ),
                    ),
                    'button_border_radius'      => array(
                        'type'                      => 'unit',
                        'label'                     => __('Round Corners', 'woopack'),
                        'default'                   => '5',
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce-product-add-to-cart a, .woocommerce-product-add-to-cart .button',
                            'property'                  => 'border-radius',
                            'unit'                      => 'px',
                        ),
                        'responsive' 			    => array(
                            'placeholder' 			    => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                ),
            ),
            'button_padding'    => array(
                'title'             => __('Padding', 'woopack'),
                'fields'            => array(
                    'button_padding'    => array(
                        'type'                  => 'dimension',
                        'label' 			    => __('Padding', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woocommerce-product-add-to-cart .add_to_cart_inline a, .woocommerce-product-add-to-cart a, .woocommerce-product-add-to-cart .button',
                            'property'              => 'padding',
                            'unit'                  => 'px'
                        ),
                        'responsive'            => array(
                            'placeholder'           => array(
                                'default' 			    => '',
                                'medium' 			    => '',
                                'responsive'            => '',
                            ),
                        ),
                    ),
                ),
            ),
            'button_font'       => array(
                'title'             => __('Typography', 'woopack'),
                'fields'            => array(
                    'button_font'               => array(
                        'type'                      => 'font',
                        'default' 		            => array(
                            'family'                    => 'Default',
                            'weight'                    => 300
                        ),
                        'label'                     => __('Font', 'woopack'),
                        'preview'                   => array(
                            'type'                      => 'font',
                            'selector'                  => '.woocommerce-product-add-to-cart .add_to_cart_inline a, .woocommerce-product-add-to-cart a, .woocommerce-product-add-to-cart .button',
                            'property'		            => 'font-family',
                        ),
                    ),
                    'button_font_size'          => array(
                        'type'                      => 'select',
                        'label'  			        => __('Font Size', 'woopack'),
                        'default'                   => 'default',
                        'options' 		            => array(
                            'default' 		            => __('Default', 'woopack'),
                            'custom'                    => __('Custom', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'custom'                    => array(
                                'fields'                    => array('button_font_size_custom')
                            ),
                        ),
                        'preview'                   => array(
                            'type'                      => 'none'
                        )
                    ),
                    'button_font_size_custom'   => array(
                        'type'                      => 'unit',
                        'label'                     => __('Custom Font Size', 'woopack'),
                        'description' 			    => 'px',
                        'responsive' 			    => array(
                            'placeholder'               => array(
                                'default'                   => '15',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                        'preview'				    => array(
                            'type' 					    => 'css',
                            'selector'				    => '.woocommerce-product-add-to-cart .add_to_cart_inline a, .woocommerce-product-add-to-cart a, .woocommerce-product-add-to-cart .button',
                            'property'      		    => 'font-size',
                            'unit'					    => 'px',
                        ),
                    ),
                    'button_text_transform'     => array(
                        'type'                      => 'select',
                        'label' 		            => __('Text Transform', 'woopack'),
                        'default'                   => 'default',
                        'options'                   => array(
                            'default' 		           => __('Default', 'woopack'),
                            'capitalize' 		       => __('Capitalize', 'woopack'),
                            'lowercase'                => __('lowercase', 'woopack'),
                            'uppercase'                => __('UPPERCASE', 'woopack'),
                        ),
                        'preview' 		            => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce-product-add-to-cart .add_to_cart_inline a, .woocommerce-product-add-to-cart a, .woocommerce-product-add-to-cart .button',
                            'property'                  => 'text-transform',
                        ),
                    ),
                ),
            ),
        ),
    ),
    'typography'=> array(
        'title'         => __('Typography', 'woopack'),
        'sections' 	    => array(
            'product_title_style'   => array(
                'title'                 => __('Title', 'woopack'),
                'fields'                => array(
                    'product_title_heading_tag'     => array(
                        'type'                          => 'select',
                        'label' 		                => __('Heading Tag', 'woopack'),
                        'default'                       => 'h2',
                        'options'                       => array(
                            'h1'                            => __('H1', 'woopack'),
                            'h2'                            => __('H2', 'woopack'),
                            'h3'                            => __('H3', 'woopack'),
                            'h4'                            => __('H4', 'woopack'),
                            'h5'                            => __('H5', 'woopack'),
                            'h6'                            => __('H6', 'woopack'),
                        ),
                    ),
                    'product_title_font'            => array(
                        'type'                          => 'font',
                        'default' 		                => array(
                            'family'                        => 'Default',
                            'weight'                        => 300
                        ),
                        'label'                         => __('Font', 'woopack'),
                        'preview'                           => array(
                            'type'                              => 'font',
                            'selector'                          => '.woopack-product-title',
                            'property'		                    => 'font-family',
                        ),
                    ),
                    'product_title_font_size'       => array(
                        'type'                          => 'select',
                        'label'  			            => __('Font Size', 'woopack'),
                        'default'                       => 'default',
                        'options' 		                => array(
                            'default' 		                => __('Default', 'woopack'),
                            'custom'                        => __('Custom', 'woopack'),
                        ),
                        'toggle'                        => array(
                            'custom'                        => array(
                                'fields'                        => array('product_title_font_size_custom')
                            ),
                        ),
                    ),
                    'product_title_font_size_custom'=> array(
                        'type'                          => 'unit',
                        'label'                         => __('Custom Font Size', 'woopack'),
                        'description' 			        => 'px',
                        'preview'				        => array(
                            'type' 					        => 'css',
                            'selector'				        => '.woopack-product-title',
                            'property'      		        => 'font-size',
                            'unit'					        => 'px',
                        ),
                        'responsive' 			        => array(
                            'placeholder'                   => array(
                                'default'                       => '15',
                                'medium' 				        => '',
                                'responsive' 			        => '',
                            ),
                        ),
                    ),
                    'product_title_line_height'	    => array(
                        'type'                          => 'unit',
                        'label'                         => __('Line Height', 'woopack'),
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.woopack-product-title',
                            'property'                      => 'line-height',
                        ),
                        'responsive' 			        => array(
                            'placeholder'                   => array(
                                'default'                       => '',
                                'medium' 				        => '',
                                'responsive' 			        => '',
                            ),
                        ),
                    ),
                    'product_title_text_transform'  => array(
                        'type'                          => 'select',
                        'label' 		                => __('Text Transform', 'woopack'),
                        'default'                       => 'default',
                        'options'                       => array(
                            'default'                       => __('Default', 'woopack'),
                            'capitalize'                    => __('Capitalize', 'woopack'),
                            'lowercase'                     => __('lowercase', 'woopack'),
                            'uppercase'                     => __('UPPERCASE', 'woopack'),
                        ),
                        'preview' 		                => array(
                            'type'                          => 'css',
                            'selector'                      => '.woopack-product-title',
                            'property'                      => 'text-transform',
                        ),
                    ),
                    'product_title_color'           => array(
                        'type'                          => 'color',
                        'label'                         => __('Color', 'woopack'),
                        'show_reset'                    => true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.woopack-single-product.woocommerce .woopack-product-title',
                            'property'                      => 'color',
                        ),
                    ),
                    'product_title_margin'          => array(
                        'type'                          => 'unit',
                        'label'                         => __('Margin Bottom', 'woopack'),
                        'default'                       => '',
                        'description'                   => 'px',
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.woopack-single-product .woopack-product-title',
                            'property'                      => 'margin-bottom',
                            'unit'                          => 'px',
                        ),
                        'responsive'                    => array(
                            'placeholder'                   => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                ),
            ),
            'regular_price_fonts'   => array(
                'title'                 => __('Price', 'woopack'),
                'fields'                => array(
                    'regular_price_font'        => array(
                        'type'                      => 'font',
                        'default' 		            => array(
                            'family'                    => 'Default',
                            'weight'                    => 300
                        ),
                        'label'                     => __('Font', 'woopack'),
                        'preview'                       => array(
                            'type'                          => 'font',
                            'selector'                      => '.price .amount',
                            'property'		                => 'font-family',
                        ),
                    ),
                    'regular_price_font_size'  => array(
                        'type'                  => 'select',
                        'label'                 => __('Font Size', 'woopack'),
                        'default'               => 'default',
                        'options'               => array(
                            'default'               => __('Default', 'woopack'),
                            'custom'                => __('Custom', 'woopack')
                        ),
                        'toggle'                => array(
                            'custom'                => array(
                                'fields'                => array('regular_price_font_size_custom')
                            )
                        )
                    ),
                    'regular_price_font_size_custom'   => array(
                        'type'                      => 'unit',
                        'label'                     => __('Font Size', 'woopack'),
                        'description'               => 'px',
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'               => '15',
                                'medium'                => '',
                                'responsive'            => '',
                            ),
                        ),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.price .amount',
                            'property'                  => 'font-size',
                            'unit'                      => 'px',
                        ),
                    ),
                    'regular_price_line_height'	=> array(
                        'type'                      => 'unit',
                        'label'                     => __('Line Height', 'woopack'),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.price .amount',
                            'property'                  => 'line-height',
                        ),
                        'responsive' 			        => array(
                            'placeholder'                   => array(
                                'default'                       => '',
                                'medium' 				        => '',
                                'responsive' 			        => '',
                            ),
                        ),
                    ),
                    'regular_price_color'       => array(
                        'type'                      => 'color',
                        'label'                     => __('Color', 'woopack'),
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.price .amount',
                            'property'                  => 'color',
                        ),
                    ),
                    'product_price_margin'      => array(
                        'type'                      => 'unit',
                        'label'                     => __('Margin Bottom', 'woopack'),
                        'default'                   => '',
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woopack-single-product .price',
                            'property'                  => 'margin-bottom',
                            'unit'                      => 'px',
                        ),
                        'responsive' 			    => array(
                            'placeholder' 			    => array(
                                'default' 			        => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                ),
            ),
            'sale_price_fonts'      => array(
                'title'                 => __('Sale Price', 'woopack'),
                'fields'                => array(
                    'sale_price_font_size'  => array(
                        'type'                  => 'select',
                        'label'                 => __('Font Size', 'woopack'),
                        'default'               => 'default',
                        'options'               => array(
                            'default'               => __('Default', 'woopack'),
                            'custom'                => __('Custom', 'woopack')
                        ),
                        'toggle'                => array(
                            'custom'                => array(
                                'fields'                => array('sale_price_font_size_custom')
                            )
                        )
                    ),
                    'sale_price_font_size_custom' => array(
                        'type'                      => 'unit',
                        'label'                     => __('Font Size', 'woopack'),
                        'description'               => 'px',
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '15',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.price ins .amount',
                            'property'                  => 'font-size',
                            'unit'                      => 'px',
                        ),
                    ),
                    'sale_price_color'          => array(
                        'type'                      => 'color',
                        'label'                     => __('Color', 'woopack'),
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.price ins .amount',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'short_description'     => array(
                'title'                 => __('Short Description', 'woopack'),
                'fields'                => array(
                    'short_description_font'            => array(
                        'type'                              => 'font',
                        'label'                             => __('Font', 'woopack'),
                        'default' 		                    => array(
                            'family'                            => 'Default',
                            'weight'                            => 300
                        ),
                        'preview'                           => array(
                            'type'                              => 'font',
                            'selector'                          => '.woocommerce-product-details__short-description p',
                            'property'		                    => 'font-family',
                        ),
                    ),
                    'short_description_font_size'       => array(
                        'type'                              => 'select',
                        'label'  			                => __('Font Size', 'woopack'),
                        'default'                           => 'default',
                        'options' 		                    => array(
                            'default' 		                    => __('Default', 'woopack'),
                            'custom'                            => __('Custom', 'woopack'),
                        ),
                        'toggle'                            => array(
                            'custom'                            => array(
                                'fields'                            => array('short_description_font_size_custom')
                            ),
                        ),
                        'preview'               => array(
                            'type'                  => 'none'
                        )
                    ),
                    'short_description_font_size_custom'=> array(
                        'type'                              => 'unit',
                        'label'                             => __('Custom Font Size', 'woopack'),
                        'description'                       => 'px',
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '.woocommerce-product-details__short-description p',
                            'property'                          => 'font-size',
                            'unit'                              => 'px',
                        ),
                        'responsive'                        => array(
                            'placeholder'                       => array(
                                'default'                           => '15',
                                'medium'                            => '',
                                'responsive'                        => '',
                            ),
                        ),
                    ),
                    'short_description_line_height'	    => array(
                        'type'                              => 'unit',
                        'label'                             => __('Line Height', 'woopack'),
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '.woocommerce-product-details__short-description p',
                            'property'                          => 'line-height',
                        ),
                        'responsive' 			        => array(
                            'placeholder'                   => array(
                                'default'                       => '',
                                'medium' 				        => '',
                                'responsive' 			        => '',
                            ),
                        ),
                    ),
                    'short_description_color'           => array(
                        'type'                              => 'color',
                        'label'                             => __('Color', 'woopack'),
                        'show_reset'                        => true,
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '.woocommerce-product-details__short-description p',
                            'property'                          => 'color',
                        ),
                    ),
                    'product_description_margin_bottom'        => array(
                        'type'                              => 'unit',
                        'label'                             => __('Margin Bottom', 'woopack'),
                        'default'                           => '',
                        'description'                       => 'px',
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '.woopack-single-product .woocommerce-product-details__short-description p',
                            'property'                          => 'margin-bottom',
                            'unit'                              => 'px',
                        ),
                        'responsive' 			            => array(
                            'placeholder'                       => array(
                                'default'                           => '',
                                'medium'                            => '',
                                'responsive'                        => '',
                            ),
                        ),
                    ),
                ),
            ),
            'sale_badge_fonts'      => array(
                'title'                 => __('Sale Badge', 'woopack'),
                'fields'                => array(
                    'sale_badge_font'               => array(
                        'type'                          => 'font',
                        'default' 		                => array(
                            'family'                        => 'Default',
                            'weight'                        => 300
                        ),
                        'label'                         => __('Font', 'woopack'),
                        'preview'                           => array(
                            'type'                              => 'font',
                            'selector'                          => '.onsale',
                            'property'		                    => 'font-family',
                        ),
                    ),
                    'sale_badge_font_size'          => array(
                        'type'                          => 'select',
                        'label'  			            => __('Font Size', 'woopack'),
                        'default'                       => 'default',
                        'options' 		                => array(
                            'default' 		                => __('Default', 'woopack'),
                            'custom'                        => __('Custom', 'woopack'),
                        ),
                        'toggle'                        => array(
                            'custom'                        => array(
                                'fields'                        => array('sale_badge_font_size_custom')
                            ),
                        ),
                        'preview'               => array(
                            'type'                  => 'none'
                        )
                    ),
                    'sale_badge_font_size_custom'   => array(
                        'type'                          => 'unit',
                        'label'                         => __('Custom Font Size', 'woopack'),
                        'description'                   => 'px',
                        'responsive'                    => array(
                            'placeholder'                   => array(
                                'default'                       => '15',
                                'medium'                        => '',
                                'responsive'                    => '',
                            ),
                        ),
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.onsale',
                            'property'                      => 'font-size',
                            'unit'                          => 'px',
                        ),
                    ),
                    'sale_badge_line_height'        => array(
                        'type'                          => 'unit',
                        'label'                         => __('Line Height', 'woopack'),
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.onsale',
                            'property'                      => 'line-height',
                        ),
                        'responsive' 			        => array(
                            'placeholder'                   => array(
                                'default'                       => '',
                                'medium' 				        => '',
                                'responsive' 			        => '',
                            ),
                        ),
                    ),
                ),
            ),
            'meta_fonts'            => array(
                'title'                 => __('Product Taxonomy', 'woopack'),
                'fields'                => array(
                    'meta_font'             => array(
                        'type'                  => 'font',
                        'label'                 => __('Font', 'woopack'),
                        'default' 		        => array(
                            'family'                => 'Default',
                            'weight'                => 300
                        ),
                        'preview'               => array(
                            'type'                  => 'font',
                            'selector'              => '.woopack-single-product .product_meta',
                            'property'		        => 'font-family',
                        ),
                    ),
                    'meta_font_size'        => array(
                        'type'                  => 'select',
                        'label'  			    => __('Font Size', 'woopack'),
                        'default'               => 'default',
                        'options' 		        => array(
                            'default' 		        => __('Default', 'woopack'),
                            'custom'                => __('Custom', 'woopack'),
                        ),
                        'toggle'                => array(
                            'custom'                => array(
                                'fields'                => array('meta_font_size_custom')
                            ),
                        ),
                        'preview'               => array(
                            'type'                  => 'none'
                        )
                    ),
                    'meta_font_size_custom' => array(
                        'type'                  => 'unit',
                        'label'                 => __('Custom Font Size', 'woopack'),
                        'description' 			=> 'px',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '15',
                                'medium' 				=> '',
                                'responsive' 			=> '',
                            ),
                        ),
                        'preview'				=> array(
                            'type' 					=> 'css',
                            'selector'				=> '.woopack-single-product .product_meta',
                            'property'      		=> 'font-size',
                            'unit'					=> 'px',
                        ),
                    ),
                    'meta_text_transform'   => array(
                        'type'                          => 'select',
                        'label' 		                => __('Text Transform', 'woopack'),
                        'default'                       => 'default',
                        'options'                       => array(
                            'default'                       => __('Default', 'woopack'),
                            'capitalize'                    => __('Capitalize', 'woopack'),
                            'lowercase'                     => __('lowercase', 'woopack'),
                            'uppercase'                     => __('UPPERCASE', 'woopack'),
                        ),
                        'preview' 		                => array(
                            'type'                          => 'css',
                            'selector'                      => '.woopack-single-product .product_meta',
                            'property'                      => 'text-transform',
                        ),
                    )
                ),
            ),
        ),
    ),
));
