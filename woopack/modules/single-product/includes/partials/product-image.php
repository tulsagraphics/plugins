<?php do_action( 'woopack_single_product_before_image_wrap', $settings, $product ); ?>

<div class="single-product-image">
    <img src="<?php echo $image[0]; ?>" title="<?php echo get_the_title( $product_id ); ?>" alt="<?php echo get_the_title( $product_id ); ?>" class="woopack-product-featured-image">
	<?php if ( 'yes' == $settings->show_sale_badge && $product->is_on_sale() ) : ?>
		<?php
			$sale_badge = sprintf( '<span class="onsale">%s</span>', esc_html__('Sale!', 'woopack') );
			echo apply_filters( 'woocommerce_sale_flash', $sale_badge, $post, $product );
		?>
	<?php endif; ?>
</div>

<?php do_action( 'woopack_single_product_after_image_wrap', $settings, $product ); ?>