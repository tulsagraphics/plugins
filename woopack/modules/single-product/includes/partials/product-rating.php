<?php
$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();
$average      = $product->get_average_rating();

if ( $rating_count > 0 ) :
?>

<?php do_action( 'woopack_single_product_before_rating_wrap', $settings, $product ); ?>

<div class="woocommerce-product-rating">
	<?php echo wc_get_rating_html( $average, $rating_count ); ?>
	<?php if ( comments_open() ) : ?>
		<a href="#reviews" class="woocommerce-review-link" rel="nofollow">(<?php printf( _n( '%s customer review', '%s customer reviews', $review_count, 'woocommerce' ), '<span class="count">' . esc_html( $review_count ) . '</span>' ); ?>)</a>
	<?php endif ?>
</div>

<?php do_action( 'woopack_single_product_after_rating_wrap', $settings, $product ); ?>

<?php endif; ?>