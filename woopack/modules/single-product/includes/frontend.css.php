<?php $product_id = $settings->product_id; ?>
.fl-node-<?php echo $id; ?> .woopack-single-product {
	position: relative;
	<?php WooPack_Helper::print_css( 'padding-top', $settings->box_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->box_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->box_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->box_padding_right, 'px' ); ?>
	<?php if ( 'none' != $settings->box_border_style ) { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->box_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->box_border_top, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->box_border_bottom, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-left-width', $settings->box_border_left, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-right-width', $settings->box_border_right, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->box_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'border-radius', $settings->box_border_radius, 'px' ); ?>
	<?php if ( 'yes' == $settings->box_shadow ) { ?>
	-webkit-box-shadow: <?php echo $settings->box_shadow_h; ?>px <?php echo $settings->box_shadow_v; ?>px <?php echo $settings->box_shadow_blur; ?>px <?php echo $settings->box_shadow_spread; ?>px <?php echo WooPack_Helper::get_color_value( $settings->box_shadow_color ); ?>;
	-moz-box-shadow: <?php echo $settings->box_shadow_h; ?>px <?php echo $settings->box_shadow_v; ?>px <?php echo $settings->box_shadow_blur; ?>px <?php echo $settings->box_shadow_spread; ?>px <?php echo WooPack_Helper::get_color_value( $settings->box_shadow_color ); ?>;
	-o-box-shadow: <?php echo $settings->box_shadow_h; ?>px <?php echo $settings->box_shadow_v; ?>px <?php echo $settings->box_shadow_blur; ?>px <?php echo $settings->box_shadow_spread; ?>px <?php echo WooPack_Helper::get_color_value( $settings->box_shadow_color ); ?>;
	box-shadow: <?php echo $settings->box_shadow_h; ?>px <?php echo $settings->box_shadow_v; ?>px <?php echo $settings->box_shadow_blur; ?>px <?php echo $settings->box_shadow_spread; ?>px <?php echo WooPack_Helper::get_color_value( $settings->box_shadow_color ); ?>;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->content_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'text-align', $settings->content_alignment, '', 'default' != $settings->content_alignment ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->content_bg_color_hover ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product a {
	text-decoration: none;
	position: relative;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .product-img-wrapper {
	vertical-align: top;
}
<?php if ( 1 == $settings->product_layout ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-single-product .single-product-image {
		float: left;
		width: auto;
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating {
		display: flow-root;
	}
<?php } ?>
<?php if ( 2 == $settings->product_layout ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-single-product .single-product-image {
		float: right;
		width: auto;
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating {
		display: flow-root;
	}
<?php } ?>
.fl-node-<?php echo $id; ?> .woopack-single-product .onsale {
	position: absolute;
	<?php if ( 'top-left' == $settings->badge_position ) { ?>
		top: 0;
		left: 0;
	<?php } elseif ( 'top-center' == $settings->badge_position ) { ?>
		top: 0;
		left: 50%;
		transform: translateX(-50%);
	<?php } elseif ( 'top-right' == $settings->badge_position ) { ?>
		top: 0;
		right: 0;
	<?php } elseif ( 'bottom-left' == $settings->badge_position ) { ?>
		top: auto;
		bottom: 0;
		left: 0;
	<?php } elseif ( 'bottom-center' == $settings->badge_position ) { ?>
		top: auto;
		bottom: 0;
		left: 50%;
		transform: translateX(-50%);
	<?php } elseif ( 'bottom-right' == $settings->badge_position ) { ?>
		top: auto;
		bottom: 0;
		right: 0;
	<?php } ?>

	<?php WooPack_Helper::print_css( 'background-color', $settings->badge_bg_color ); ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->badge_padding_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->badge_padding_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->badge_padding_left_right, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->badge_padding_left_right, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'margin-top', $settings->badge_margin_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->badge_margin_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-left', $settings->badge_margin_left_right, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-right', $settings->badge_margin_left_right, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'color', $settings->badge_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->sale_badge_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->sale_badge_font_size_custom, 'px', 'custom' == $settings->sale_badge_font_size ); ?>

	<?php if ( 'none' != $settings->sale_badge_border_style ) { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->sale_badge_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->sale_badge_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->sale_badge_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>

	<?php WooPack_Helper::print_css( 'border-radius', $settings->sale_badge_border_radius, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .product-content {
	<?php WooPack_Helper::print_css( 'padding-top', $settings->content_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->content_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->content_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->content_padding_right, 'px' ); ?>
	<?php if ( 1 == $settings->product_layout ) { ?>
		float: left;
	<?php } elseif ( 2 == $settings->product_layout ) { ?>
		float: right;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woopack-product-title {
	<?php WooPack_Helper::print_css( 'color', $settings->product_title_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->product_title_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->product_title_font_size_custom, 'px', 'custom' == $settings->product_title_font_size ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->product_title_line_height ); ?>
	margin-top: 0;
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_title_margin, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->product_title_text_transform, '', 'default' != $settings->product_title_text_transform ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating {
	<?php WooPack_Helper::print_css( 'text-align', $settings->content_alignment ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->product_rating_size, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_rating_margin, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating .star-rating {
	float: none;
	<?php if ( 'center' == $settings->content_alignment ) { ?>
	margin: 0 auto;
	<?php } ?>
	<?php if ( 'right' == $settings->content_alignment ) { ?>
		float: right;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating .star-rating::before {
	<?php WooPack_Helper::print_css( 'color', $settings->product_rating_default_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating .star-rating span::before {
	<?php WooPack_Helper::print_css( 'color', $settings->product_rating_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .price {
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_price_margin, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .price .amount {
	<?php WooPack_Helper::print_css( 'font', $settings->regular_price_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->regular_price_font_size_custom, 'px', 'custom' == $settings->regular_price_font_size ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->regular_price_color ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->regular_price_line_height ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .price ins {
	text-decoration: none;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .price ins .amount {
	text-decoration: none;
	<?php WooPack_Helper::print_css( 'font-size', $settings->sale_price_font_size_custom, 'px', 'custom' == $settings->sale_price_font_size ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->sale_price_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-details__short-description {
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_description_margin_bottom, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-details__short-description,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-details__short-description p {
	<?php WooPack_Helper::print_css( 'color', $settings->short_description_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->short_description_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->short_description_font_size_custom, 'px', 'custom' == $settings->short_description_font_size ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->short_description_line_height ); ?>
}

/**************************
 * Variations
 **************************/
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations_form.cart,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart label {
	<?php if ( '' == $settings->short_description_color ) { ?>
		color: #000000;
	<?php } else {	
		WooPack_Helper::print_css( 'color', $settings->short_description_color ); ?>
	<?php } ?>	
	<?php WooPack_Helper::print_css( 'font', $settings->short_description_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->short_description_font_size_custom, 'px', $settings->short_description_font_size == 'custom' ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->short_description_line_height ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations {
	margin-top: 30px;
	margin-bottom: 15px;
	<?php if ( 'center' == $settings->content_alignment || 'default' == $settings->content_alignment ) { ?>
	margin-left: auto;
	margin-right: auto;
	<?php } ?>
	<?php if ( 'right' == $settings->content_alignment ) { ?>
	margin-left: auto;
	<?php } ?>
	<?php if ( 'left' == $settings->content_alignment ) { ?>
	margin-right: auto;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations td label {
	margin: 0;
	margin-right: 30px;
	vertical-align: middle;
	font-size: 15px;
    font-weight: 400;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations tr select {
	margin-right: 10px;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations tr:last-child select {
	margin-bottom: 5px;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart form.cart,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart form.cart .single_variation_wrap .woocommerce-variation-add-to-cart {
	width: 100%;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations_form.cart {
	<?php WooPack_Helper::print_css( 'text-align', $settings->content_alignment, '', $settings->content_alignment != 'default' ); ?>
	<?php WooPack_Helper::print_css( 'text-align', '-moz-'.$settings->content_alignment, '', $settings->content_alignment != 'default' ); ?>
	display: block;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations_form.cart .single_variation_wrap {
	margin-top: 5px;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .quantity {
	vertical-align: top;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations_form.cart .variations_button {
	<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_medium, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_medium, 'px' ); ?>
}

/**************************
 * Button
 **************************/
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .woocommerce-variation {
	margin-bottom: 10px;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart {
	<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart a:not(.reset_variations),
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .button,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .button.disabled,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .button.alt.disabled,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .button.single_add_to_cart_button.alt {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color ); ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->button_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->button_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->button_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->button_padding_right, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'color', $settings->button_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->button_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->button_font_size_custom, 'px', 'custom' == $settings->button_font_size ); ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->button_text_transform, '', 'default' != $settings->button_text_transform ); ?>

	<?php if ( 'default' != $settings->button_border_style ) { ?>
		<?php if ( 'none' == $settings->button_border_style ) { ?>
			border: none;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color ); ?>
		<?php } ?>
	<?php } ?>

	<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'width', '100', '%', 'full_width' == $settings->button_width ); ?>
	<?php WooPack_Helper::print_css( 'width', $settings->button_width_custom, '%', 'custom' == $settings->button_width ); ?>

	transition: 0.2s ease-in-out;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart a:not(.reset_variations):hover,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .button:hover,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .button.single_add_to_cart_button.alt:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color_hover ); ?>

	<?php if ( 'default' != $settings->button_border_style ) { ?>
		<?php if ( 'none' == $settings->button_border_style ) { ?>
			border: none;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color_hover, '', '' != $settings->button_border_color_hover ); ?>
		<?php } ?>
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .product_meta {
	<?php WooPack_Helper::print_css( 'padding-top', $settings->meta_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->meta_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->meta_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->meta_padding_right, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'color', $settings->meta_text_color ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->meta_border_color, '', 'yes' == $settings->meta_border ); ?>
	<?php WooPack_Helper::print_css( 'border', '0', '', 'no' == $settings->meta_border ); ?>

	<?php WooPack_Helper::print_css( 'font', $settings->meta_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->meta_font_size_custom, 'px', 'custom' == $settings->meta_font_size ); ?>

	<?php WooPack_Helper::print_css( 'text-transform', $settings->meta_text_transform, '', 'default' != $settings->meta_text_transform ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .product_meta .posted_in a {
	<?php WooPack_Helper::print_css( 'color', $settings->meta_link_color ); ?>
}


<?php
// *********************
// Media Query
// *********************
?>
@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woopack-single-product {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->box_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->box_padding_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->box_padding_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->box_padding_right_medium, 'px' ); ?>

		<?php if ( 'none' != $settings->box_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-top-width', $settings->box_border_top_medium, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->box_border_bottom_medium, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-left-width', $settings->box_border_left_medium, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-right-width', $settings->box_border_right_medium, 'px' ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->box_border_radius_medium, 'px' ); ?>

		<?php if ( 'yes' == $settings->box_shadow ) { ?>
			-webkit-box-shadow: <?php echo $settings->box_shadow_h_medium; ?>px <?php echo $settings->box_shadow_v_medium; ?>px <?php echo $settings->box_shadow_blur_medium; ?>px <?php echo $settings->box_shadow_spread_medium; ?>px <?php echo WooPack_Helper::get_color_value( $settings->box_shadow_color ); ?>;
			-moz-box-shadow: <?php echo $settings->box_shadow_h_medium; ?>px <?php echo $settings->box_shadow_v_medium; ?>px <?php echo $settings->box_shadow_blur_medium; ?>px <?php echo $settings->box_shadow_spread_medium; ?>px <?php echo WooPack_Helper::get_color_value( $settings->box_shadow_color ); ?>;
			-o-box-shadow: <?php echo $settings->box_shadow_h_medium; ?>px <?php echo $settings->box_shadow_v_medium; ?>px <?php echo $settings->box_shadow_blur_medium; ?>px <?php echo $settings->box_shadow_spread_medium; ?>px <?php echo WooPack_Helper::get_color_value( $settings->box_shadow_color ); ?>;
			box-shadow: <?php echo $settings->box_shadow_h_medium; ?>px <?php echo $settings->box_shadow_v_medium; ?>px <?php echo $settings->box_shadow_blur_medium; ?>px <?php echo $settings->box_shadow_spread_medium; ?>px <?php echo WooPack_Helper::get_color_value( $settings->box_shadow_color ); ?>;
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .onsale {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->badge_padding_top_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->badge_padding_top_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->badge_padding_left_right_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->badge_padding_left_right_medium, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'font-size', $settings->sale_badge_font_size_custom_medium, 'px', 'custom' == $settings->sale_badge_font_size ); ?>

		<?php if ( 'none' != $settings->sale_badge_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->sale_badge_border_width_medium, 'px' ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->sale_badge_border_radius_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .product-content {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->content_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->content_padding_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->content_padding_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->content_padding_right_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woopack-product-title {
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_title_font_size_custom_medium, 'px', 'custom' == $settings->product_title_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->product_title_line_height_medium, '', '' != $settings->product_title_line_height_medium ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_title_margin_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating {
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_rating_size_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_rating_margin_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .price {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_price_margin_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .price .amount {
		<?php WooPack_Helper::print_css( 'font-size', $settings->regular_price_font_size_custom_medium, 'px', 'custom' == $settings->regular_price_font_size ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->regular_price_line_height_medium, '', '' != $settings->regular_price_line_height_medium ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .price ins .amount {
		<?php WooPack_Helper::print_css( 'font-size', $settings->sale_price_font_size_custom_medium, 'px', 'custom' == $settings->sale_price_font_size ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-details__short-description {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_description_margin_bottom_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-details__short-description,
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-details__short-description p {
		<?php WooPack_Helper::print_css( 'font-size', $settings->short_description_font_size_custom_medium, 'px', 'custom' == $settings->short_description_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->short_description_line_height_medium ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart {
		<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart a,
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .button {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->button_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->button_padding_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->button_padding_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->button_padding_right_medium, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'font-size', $settings->button_font_size_custom_medium, 'px', 'custom' == $settings->button_font_size ); ?>

		<?php if ( 'none' != $settings->button_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width_medium, 'px' ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .product_meta {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->meta_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->meta_padding_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->meta_padding_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->meta_padding_right_medium, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'border', '0', '', 'no' == $settings->meta_border ); ?>

		<?php WooPack_Helper::print_css( 'font-size', $settings->meta_font_size_custom_medium, 'px', 'custom' == $settings->meta_font_size ); ?>
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woopack-single-product {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->box_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->box_padding_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->box_padding_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->box_padding_right_responsive, 'px' ); ?>

		<?php if ( 'none' != $settings->box_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-top-width', $settings->box_border_top_responsive, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->box_border_bottom_responsive, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-left-width', $settings->box_border_left_responsive, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-right-width', $settings->box_border_right_responsive, 'px' ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->box_border_radius_responsive, 'px' ); ?>

		<?php if ( 'yes' == $settings->box_shadow ) { ?>
		-webkit-box-shadow: <?php echo $settings->box_shadow_h_responsive; ?>px <?php echo $settings->box_shadow_v_responsive; ?>px <?php echo $settings->box_shadow_blur_responsive; ?>px <?php echo $settings->box_shadow_spread_responsive; ?>px <?php echo WooPack_Helper::get_color_value( $settings->box_shadow_color ); ?>;
		-moz-box-shadow: <?php echo $settings->box_shadow_h_responsive; ?>px <?php echo $settings->box_shadow_v_responsive; ?>px <?php echo $settings->box_shadow_blur_responsive; ?>px <?php echo $settings->box_shadow_spread_responsive; ?>px <?php echo WooPack_Helper::get_color_value( $settings->box_shadow_color ); ?>;
		-o-box-shadow: <?php echo $settings->box_shadow_h_responsive; ?>px <?php echo $settings->box_shadow_v_responsive; ?>px <?php echo $settings->box_shadow_blur_responsive; ?>px <?php echo $settings->box_shadow_spread_responsive; ?>px <?php echo WooPack_Helper::get_color_value( $settings->box_shadow_color ); ?>;
		box-shadow: <?php echo $settings->box_shadow_h_responsive; ?>px <?php echo $settings->box_shadow_v_responsive; ?>px <?php echo $settings->box_shadow_blur_responsive; ?>px <?php echo $settings->box_shadow_spread_responsive; ?>px <?php echo WooPack_Helper::get_color_value( $settings->box_shadow_color ); ?>;
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .single-product-image {
		float: none;
		display: block;
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .single-product-image img {
		width: 100%;
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .onsale {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->badge_padding_top_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->badge_padding_top_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->badge_padding_left_right_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->badge_padding_left_right_responsive, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'font-size', $settings->sale_badge_font_size_custom_responsive, 'px', 'custom' == $settings->sale_badge_font_size ); ?>

		<?php if ( 'none' != $settings->sale_badge_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->sale_badge_border_width_responsive, 'px' ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->sale_badge_border_radius_responsive, 'px' ); ?>

	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .product-content {
		width: 100% !important;
		float: none;
		<?php WooPack_Helper::print_css( 'padding-top', $settings->content_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->content_padding_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->content_padding_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->content_padding_right_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woopack-product-title {
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_title_font_size_custom_responsive, 'px', 'custom' == $settings->product_title_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->product_title_line_height_responsive, '', '' != $settings->product_title_line_height_responsive ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_title_margin_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating {
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_rating_size_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_rating_margin_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .price {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_price_margin_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .price .amount {
		<?php WooPack_Helper::print_css( 'font-size', $settings->regular_price_font_size_custom_responsive, 'px', 'custom' == $settings->regular_price_font_size ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->regular_price_line_height_responsive, '', '' != $settings->regular_price_line_height_responsive ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .price ins .amount {
		<?php WooPack_Helper::print_css( 'font-size', $settings->sale_price_font_size_custom_responsive, 'px', 'custom' == $settings->sale_price_font_size ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-details__short-description {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_description_margin_bottom_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-details__short-description,
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-details__short-description p {
		<?php WooPack_Helper::print_css( 'font-size', $settings->short_description_font_size_custom_responsive, 'px', 'custom' == $settings->short_description_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->short_description_line_height_responsive ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart {
		<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart a,
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .button {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->button_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->button_padding_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->button_padding_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->button_padding_right_responsive, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'font-size', $settings->button_font_size_custom_responsive, 'px', 'custom' == $settings->button_font_size ); ?>

		<?php if ( 'none' != $settings->button_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width_responsive, 'px' ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .product_meta {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->meta_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->meta_padding_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->meta_padding_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->meta_padding_right_responsive, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'border', '0', '', 'no' == $settings->meta_border ); ?>

		<?php WooPack_Helper::print_css( 'font-size', $settings->meta_font_size_custom_responsive, 'px', 'custom' == $settings->meta_font_size ); ?>
	}
}
