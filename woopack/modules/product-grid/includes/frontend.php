<?php
if ( ! isset( $settings->woopack ) ) {
	$settings->woopack = true;
}

if ( isset( $settings->product_source ) ) {
    if ( $settings->product_source == 'featured' ) {
        add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::featured_products', 10, 1 );
    }
    if ( $settings->product_source == 'best_selling' ) {
        add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::best_selling_products', 10, 1 );
    }
    if ( $settings->product_source == 'sale' ) {
        add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::sale_products', 10, 1 );
    }
    if ( $settings->product_source == 'top_rated' ) {
        add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::top_rated_products', 10, 1 );
    }
}

if ( isset( $_GET['orderby'] ) && ! empty( $_GET['orderby'] ) ) {
    $orderby_value = wc_clean( (string) $_GET['orderby'] );
    // Get order + orderby args from string
    $orderby_value = explode( '-', $orderby_value );
    $orderby       = esc_attr( $orderby_value[0] );
    $order         = ! empty( $orderby_value[1] ) ? $orderby_value[1] : '';
    WooPack_Helper::$order = $order;

    if ( is_callable( 'WooPack_Helper::order_by_' . $orderby ) ) {
        add_filter( 'pre_get_posts', 'WooPack_Helper::order_by_' . $orderby, 10, 1 );
    }
}

// Get the query data.
$query = FLBuilderLoop::query( $settings );
// Render the posts.
if ( $query->have_posts() ) :

    do_action( 'woopack_before_products_grid', $settings, $query );

    $paged = ( FLBuilderLoop::get_paged() > 0 ) ? ' woopack-paged-scroll-to' : '';

    do_action( 'woocommerce_before_shop_loop' );

    if ( $settings->enable_filter == 'yes' && 'acf_relationship' != $settings->data_source ) {
        include $module->dir . 'includes/post-filters.php';
    }

    ?>
    <div id="woopack-<?php echo $id; ?>" class="woopack-products-grid-wrap<?php echo $settings->enable_filter == 'yes' ? ' woopack-filters-enabled' : ''; ?> woocommerce">
        <ul class="woopack-products products">
            <?php
        	while( $query->have_posts() ) {

        		$query->the_post();

                $product = wc_get_product( get_the_ID() );

                if ( is_object( $product ) && $product->is_visible() ) {
                    $product_data = $product->get_data();
                    include apply_filters( 'woopack_products_grid_layout_path', $module->dir . 'includes/layout-' . $module->settings->product_layout . '.php', $settings );
                }
        	}
        	?>
        	<li class="woopack-grid-sizer"></li>
        </ul>

    </div>

    <?php

	do_action( 'woopack_after_products_grid', $settings, $query );
endif;

// Render the pagination.
if($settings->pagination != 'none' && $query->have_posts()) :

?>
<div class="fl-builder-pagination"<?php if($settings->pagination == 'scroll') echo ' style="display:none;"'; ?>>
    <?php
    if ( $settings->enable_filter == 'yes' && $settings->filter_type == 'dynamic' && $settings->pagination == 'numbers' ) {
        WooPack_Helper::pagination( $query );
    } else {
        FLBuilderLoop::pagination($query);
    }
    ?>
</div>
<?php endif; ?>
<?php

do_action( 'woopack_after_products_grid_pagination', $settings, $query );

// Render the empty message.
if(!$query->have_posts()) :

?>
<div class="woopack-grid-empty">
	<p><?php echo $settings->no_results_message; ?></p>
	<?php if ( $settings->show_search ) : ?>
    	<?php get_search_form(); ?>
	<?php endif; ?>
</div>

<?php

endif;

wp_reset_postdata();

if ( isset( $settings->product_source ) ) {
    if ( $settings->product_source == 'featured' ) {
        remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::featured_products', 10 );
    }
    if ( $settings->product_source == 'best_selling' ) {
        remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::best_selling_products', 10 );
    }
    if ( $settings->product_source == 'sale' ) {
        remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::sale_products', 10 );
    }
    if ( $settings->product_source == 'top_rated' ) {
        remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::top_rated_products', 10 );
    }
}
if ( isset( $_GET['orderby'] ) && ! empty( $_GET['orderby'] ) ) {
    WooPack_Helper::$order = '';
    remove_filter( 'pre_get_posts', 'WooPack_Helper::order_by_' . $orderby, 10 );
}
?>
