<?php
// Common
include WOOPACK_DIR . 'includes/product-common.css.php';
// Image
include WOOPACK_DIR . 'includes/product-image.css.php';
// Sale badge
include WOOPACK_DIR . 'includes/product-badge.css.php';
// Quick View
include WOOPACK_DIR . 'includes/product-quick-view.css.php';
// Content, price.
include WOOPACK_DIR . 'includes/product-content.css.php';
// Actions, button
include WOOPACK_DIR . 'includes/product-action.css.php';
?>

.fl-node-<?php echo $id; ?> .woopack-grid-sizer {
	<?php WooPack_Helper::print_css( 'width', $settings->product_spacing, '%' ); ?>
}

.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products.woopack-products {
	margin: 0 !important;
}
.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products.woopack-products li.product {
	<?php
		$space_desktop  = ( $settings->product_columns - 1 ) * $settings->product_spacing;
		$width_desktop  = ( 100 - $space_desktop ) / $settings->product_columns;
	?>
	<?php WooPack_Helper::print_css( 'width', $width_desktop, '% !important' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_spacing, '% !important' ); ?>
}
<?php if ( 'yes' == $settings->match_height ) { ?>
	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products.woopack-products li.product {
		<?php WooPack_Helper::print_css( 'margin-right', $settings->product_spacing, '% !important' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns; ?>n) {
		margin-right: 0 !important;
	}
<?php } else { ?>
	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products.woopack-products li.product {
		margin-right: 0 !important;
	}
<?php } ?>

.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap.woopack-filters-enabled ul.products.woopack-products li.product {
	margin-right: 0 !important;
}

.fl-node-<?php echo $id; ?> .fl-builder-pagination {
	width: 100%;
}
.fl-node-<?php echo $id; ?> .fl-builder-pagination ul.page-numbers {
	<?php if ( $default_align && 'default' == $settings->pagination_align ) { ?>
		<?php WooPack_Helper::print_css( 'text-align', $default_align ); ?>
	<?php } else { ?>
		<?php WooPack_Helper::print_css( 'text-align', $settings->pagination_align ); ?>
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .fl-builder-pagination li a.page-numbers,
.fl-node-<?php echo $id; ?> .fl-builder-pagination li span.page-numbers {
	<?php WooPack_Helper::print_css( 'background-color', $settings->pagination_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->pagination_color ); ?>
	<?php WooPack_Helper::print_css( 'padding-top', $settings->pagination_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->pagination_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->pagination_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->pagination_padding_right, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'margin-top', $settings->pagination_margin_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->pagination_margin_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-left', $settings->pagination_margin_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-right', $settings->pagination_margin_right, 'px' ); ?>

	<?php if ( 'none' != $settings->pagination_border_style ) { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->pagination_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->pagination_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->pagination_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'border-radius', $settings->pagination_border_radius, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->pagination_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->pagination_font_size_custom, 'px', 'custom' == $settings->pagination_font_size ); ?>
}
.fl-node-<?php echo $id; ?> .fl-builder-pagination li a.page-numbers:hover,
.fl-node-<?php echo $id; ?> .fl-builder-pagination li span.current {
	<?php WooPack_Helper::print_css( 'background-color', $settings->pagination_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->pagination_color ); ?>
	<?php if ( 'none' != $settings->pagination_color_hover ) { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->pagination_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->pagination_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->pagination_border_color_hover ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>
}

<?php if ( 3 == $settings->product_layout ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-product-wrapper {
		display: table;
	}
	.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products .woopack-product-wrapper .woopack-product-image {
		float: left;
		width: auto;
	}
	.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products .woopack-product-content {
		float: left;
	}
<?php } ?>

<?php if ( 4 == $settings->product_layout ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-product-wrapper {
		display: table;
	}
	.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products .woopack-product-wrapper .woopack-product-image {
		float: right;
		width: auto;
	}
	.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products .woopack-product-content {
		float: right;
	}
<?php } ?>

.fl-node-<?php echo $id; ?> .woopack-product-filters-wrap {
	text-align: <?php echo $settings->filter_alignment; ?>;
}
.fl-node-<?php echo $id; ?> .woopack-product-filters-toggle {
	<?php WooPack_Helper::print_css( 'color', $settings->filter_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-filters .woopack-product-filter {
	<?php WooPack_Helper::print_css( 'font', $settings->filter_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->filter_font_size_custom, 'px', 'custom' == $settings->filter_font_size ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->filter_line_height_custom, '', 'custom' == $settings->filter_line_height ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->filter_color ); ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->filter_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'border-style', $settings->filter_border, '', 'default' == $settings->filter_border_pos ); ?>
	<?php WooPack_Helper::print_css( 'border-top-style', $settings->filter_border, '', 'top' == $settings->filter_border_pos ); ?>
	<?php WooPack_Helper::print_css( 'border-bottom-style', $settings->filter_border, '', 'bottom' == $settings->filter_border_pos ); ?>
	<?php WooPack_Helper::print_css( 'border-width', $settings->filter_border_width, 'px', 'default' == $settings->filter_border_pos ); ?>
	<?php WooPack_Helper::print_css( 'border-top-width', $settings->filter_border_width, 'px', 'top' == $settings->filter_border_pos ); ?>
	<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->filter_border_width, 'px', 'bottom' == $settings->filter_border_pos ); ?>
	border-color: transparent;
	<?php if ( 'all' == $settings->filter_border_el ) { ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->filter_border_color ); ?>
	<?php } ?>
	<?php WooPack_Helper::print_css( 'padding-top', $settings->filter_padding_v, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->filter_padding_v, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->filter_padding_h, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->filter_padding_h, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-right', $settings->filter_margin_h, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->filter_margin_v, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'border-radius', $settings->filter_radius, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-filters .woopack-product-filter:hover,
.fl-node-<?php echo $id; ?> .woopack-product-filters .woopack-product-filter.woopack-filter-active {
	<?php WooPack_Helper::print_css( 'color', $settings->filter_hover_color ); ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->filter_bg_hover_color ); ?>
	<?php if ( 'all' == $settings->filter_border_el ) { ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->filter_border_hover_color ); ?>
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-filters .woopack-product-filter.woopack-filter-active {
	<?php if ( 'active' == $settings->filter_border_el ) { ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->filter_border_color ); ?>
	<?php } ?>
}

@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product.woopack-product-grid {
		<?php
		if ( '' != $settings->product_columns_medium ) {
			$space_medium  = ( $settings->product_columns_medium - 1 ) * $settings->product_spacing;
			$width_medium  = ( 100 - $space_medium ) / $settings->product_columns_medium;
		?>
		width: <?php echo $width_medium; ?>% !important;
		<?php } ?>
		float: left !important;
	}

	<?php if ( $settings->match_height == 'yes' ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns; ?>n) {
			margin-right: <?php echo $settings->product_spacing; ?>% !important;
		}
		.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns_medium; ?>n) {
			margin-right: 0 !important;
		}
	<?php } ?>

	.fl-node-<?php echo $id; ?> .fl-builder-pagination li a.page-numbers,
	.fl-node-<?php echo $id; ?> .fl-builder-pagination li span.page-numbers {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->pagination_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->pagination_padding_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->pagination_padding_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->pagination_padding_right_medium, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'margin-top', $settings->pagination_margin_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->pagination_margin_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-left', $settings->pagination_margin_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-right', $settings->pagination_margin_right_medium, 'px' ); ?>

		<?php if ( 'none' != $settings->pagination_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->pagination_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->pagination_border_width_medium, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->pagination_border_color ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->pagination_border_radius_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'font', $settings->pagination_font ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->pagination_font_size_custom_medium, 'px', 'custom' == $settings->pagination_font_size ); ?>
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product.woopack-product-grid {
		<?php
		if ( '' != $settings->product_columns_responsive ) {
			$space_responsive  = ( $settings->product_columns_responsive - 1 ) * $settings->product_spacing;
			$width_responsive  = ( 100 - $space_responsive ) / $settings->product_columns_responsive;
		?>
		width: <?php echo $width_responsive; ?>% !important;
		<?php } ?>
		float: left !important;
	}

	<?php if ( $settings->match_height == 'yes' ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns_medium; ?>n) {
			margin-right: <?php echo $settings->product_spacing; ?>% !important;
		}
		.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns_responsive; ?>n) {
			margin-right: 0 !important;
		}
	<?php } ?>

	.fl-node-<?php echo $id; ?> .fl-builder-pagination li a.page-numbers,
	.fl-node-<?php echo $id; ?> .fl-builder-pagination li span.page-numbers {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->pagination_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->pagination_padding_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->pagination_padding_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->pagination_padding_right_responsive, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'margin-top', $settings->pagination_margin_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->pagination_margin_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-left', $settings->pagination_margin_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-right', $settings->pagination_margin_right_responsive, 'px' ); ?>

		<?php if ( 'none' != $settings->pagination_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->pagination_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->pagination_border_width_responsive, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->pagination_border_color ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->pagination_border_radius_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'font', $settings->pagination_font ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->pagination_font_size_custom_responsive, 'px', 'custom' == $settings->pagination_font_size ); ?>
	}

	.fl-node-<?php echo $id; ?> .woopack-product-filters-wrap {
		text-align: left;
	}
	.fl-node-<?php echo $id; ?> .woopack-product-filters-toggle {
		display: block;
	}
	.fl-node-<?php echo $id; ?> .woopack-product-filters {
		display: none;
	}
	.fl-node-<?php echo $id; ?> .woopack-product-filters .woopack-product-filter {
		display: block;
		float: none;
		margin: 0 !important;
		text-align: left;
	}
}
