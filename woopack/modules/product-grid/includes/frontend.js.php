;(function($){

    var WooPackGridOptions = {
        id: '<?php echo $id ?>',
		layout: '<?php echo $settings->product_layout; ?>',
        layoutStyle: <?php echo $settings->product_layout; ?>,
		pagination: '<?php echo $settings->pagination; ?>',
        perPage: '<?php echo $settings->posts_per_page; ?>',
		matchHeight: '<?php echo $settings->match_height; ?>',
        filters: '<?php echo $settings->enable_filter; ?>',
        filterTax: '<?php echo $settings->filter_taxonomy; ?>',
        filterType: '<?php echo $settings->filter_type; ?>',
        fields: <?php echo json_encode($settings); ?>
    };

    <?php if ( isset( $_GET['orderby'] ) && ! empty( $_GET['orderby'] ) ) { ?>
    WooPackGridOptions.orderby = '<?php echo wc_clean( (string) $_GET['orderby'] ); ?>';
    <?php } ?>

    new WooPackGrid( WooPackGridOptions );

})(jQuery);
