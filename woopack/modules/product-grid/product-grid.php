<?php
/**
* @class WooPackProductGrid
*/
class WooPackProductGrid extends FLBuilderModule {
    /**
    * Constructor function for the module. You must pass the
    * name, description, dir and url in an array to the parent class.
    *
    * @method __construct
    */
    public function __construct()
    {
        parent::__construct(array(
            'name' 				=> __('Product Grid', 'woopack'),
            'description' 		=> __('Addon to display Product Grid.', 'woopack'),
            'group'             => WooPack_Helper::get_modules_group(),
            'category' 			=> WOOPACK_CAT,
            'dir' 				=> WOOPACK_DIR . 'modules/product-grid/',
            'url' 				=> WOOPACK_URL . 'modules/product-grid/',
            'editor_export' 	=> true, // Defaults to true and can be omitted.
            'enabled' 			=> true, // Defaults to true and can be omitted.
        ));

        wp_enqueue_style( 'woopack-modal-box' );
        wp_enqueue_script( 'woopack-modal-box' );
    }

    /**
    * @method enqueue_scripts
    */
    public function enqueue_scripts()
    {
        $this->add_js('jquery-imagesloaded');
        $this->add_js('jquery-isotope', WOOPACK_URL . 'assets/js/isotope.pkgd.min.js', array('jquery'), WOOPACK_VER, false);

        if(FLBuilderModel::is_builder_active() || $this->settings->pagination == 'scroll') {
            $this->add_js('jquery-infinitescroll');
        }

        // Jetpack sharing has settings to enable sharing on posts, post types and pages.
        // If pages are disabled then jetpack will still show the share button in this module
        // but will *not* enqueue its scripts and fonts.
        // This filter forces jetpack to enqueue the sharing scripts.
        add_filter( 'sharing_enqueue_scripts', '__return_true' );
    }


    public function get_layout()
    {
        //$layout = $this->layout;
        return 1;
    }

    /**
    * Renders the CSS class for each post item.
    *
    * @since 1.0.0
    * @return void
    */
    public function render_post_class( $product )
    {
        $settings   = $this->settings;
        $layout     = 'grid';
        $show_image = has_post_thumbnail() && $settings->show_image;
        $classes    = array( 'woopack-product-grid' );
        $classes[] = 'woopack-product-align-' . $settings->product_align;

        if ( $product ) {
            $classes[] = 'product';
            $classes[] = wc_get_loop_class();
            $classes[] = $product->get_stock_status();

            if ( $product->is_on_sale() ) {
                $classes[] = 'sale';
            }
            if ( $product->is_featured() ) {
                $classes[] = 'featured';
            }
            if ( $product->is_downloadable() ) {
                $classes[] = 'downloadable';
            }
            if ( $product->is_virtual() ) {
                $classes[] = 'virtual';
            }
            if ( $product->is_sold_individually() ) {
                $classes[] = 'sold-individually';
            }
            if ( $product->is_taxable() ) {
                $classes[] = 'taxable';
            }
            if ( $product->is_shipping_taxable() ) {
                $classes[] = 'shipping-taxable';
            }
            if ( $product->is_purchasable() ) {
                $classes[] = 'purchasable';
            }
            if ( $product->get_type() ) {
                $classes[] = "product-type-" . $product->get_type();
            }
            if ( $product->is_type( 'variable' ) ) {
                if ( ! $product->get_default_attributes() ) {
                    $classes[] = 'has-default-attributes';
                }
                if ( $product->has_child() ) {
                    $classes[] = 'has-children';
                }
            }
        }

        if ( false !== ( $key = array_search( 'hentry', $classes ) ) ) {
            unset( $classes[ $key ] );
        }

        post_class( apply_filters( 'woopack_product_grid_classes', $classes, $settings ) );
    }

    /**
     * Renders the product featured image.
     *
     * @since 1.0.0
     * @return void
     */
    public function render_featured_image()
    {
        $settings = $this->settings;

        if ( 'yes' == $settings->show_image && has_post_thumbnail() ) {
            global $product;
            include WOOPACK_DIR . 'includes/templates/loop-product-image.php';
        }
    }

    /**
     * Renders button.
     *
     * @since 1.0.0
     * @return void
     */
    public function render_button()
    {
        $settings = $this->settings;

        if ( 'none' != $settings->button_type ) {
            include WOOPACK_DIR . 'includes/templates/loop-product-button.php';
        }
    }

    /**
     * Renders meta.
     *
     * @since 1.0.0
     * @return void
     */
    public function render_meta()
    {
        $settings = $this->settings;

        if ( 'yes' == $settings->show_taxonomy ) {
            global $product;
            include WOOPACK_DIR . 'includes/templates/loop-product-meta.php';
        }
    }

    /**
    * Checks to see if a featured image exists for a position.
    *
    * @since 1.0.0
    * @param string|array $position
    * @return void
    */
    public function has_featured_image()
    {
        $settings = $this->settings;
        $result   = false;

        if ( has_post_thumbnail() && $settings->show_image ) {
            $result = true;
        }

        return $result;
    }
}

/**
* Register the module and its form settings.
*/
FLBuilder::register_module('WooPackProductGrid', array(
    'layouts'   => array(
        'title'     => __('Layout', 'woopack'),
        'sections'  => array(
            'layout'    => array(
                'title'     => '',
                'fields'    => array(
                    'product_layout'    => array(
                        'type'              => 'layout',
                        'label'             => __('', 'woopack'),
                        'default'           => 1,
                        'options'           => array(
                            1                    => WOOPACK_URL . 'modules/product-grid/images/layout-1.png',
                            2                    => WOOPACK_URL . 'modules/product-grid/images/layout-2.png',
                            3                    => WOOPACK_URL . 'modules/product-grid/images/layout-3.png',
                            4                    => WOOPACK_URL . 'modules/product-grid/images/layout-4.png',
                        ),
                    ),
                ),
            ),
            'config'    => array(
                'title'     => __('Structure', 'woopack'),
                'fields'    => array(
                    'posts_per_page'    => array(
                        'type'            => 'text',
                        'label'           => __('Posts Per Page', 'woopack'),
                        'default'         => '10',
                        'size'            => '4'
                    ),
                    'product_columns'   => array(
                        'type'              => 'unit',
                        'label'             => __('Columns', 'woopack'),
                        'default' 		    => '3',
                        'responsive'        => array(
                            'placeholder'       => array(
                                'default' 		    => '3',
                                'medium' 			=> '2',
                                'responsive'		=> '1',
                            ),
                        ),
                    ),
                    'product_spacing'   => array(
                        'type'              => 'text',
                        'label'             => __('Spacing', 'woopack'),
                        'default'           => '2',
                        'maxlength'         => '3',
                        'size'              => '4',
                        'description'       => '%'
                    ),
                    'product_align'     => array(
                        'type'              => 'select',
                        'label'             => __('Alignment', 'woopack'),
                        'default'           => 'default',
                        'options'           => array(
                            'default'           => __('Default', 'woopack'),
                            'left'              => __('Left', 'woopack'),
                            'center'            => __('Center', 'woopack'),
                            'right'             => __('Right', 'woopack')
                        ),
                    ),
                    'match_height'      => array(
                        'type'              => 'select',
                        'label'             => __('Equal Heights', 'woopack'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no'                => __('No', 'woopack'),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'content'   => array(
        'title'     => __('Content', 'woopack'),
        'file'      => WOOPACK_DIR . 'includes/loop-settings.php',
    ),
    'filter'    => array(
        'title'     => __('Filter', 'bb-powerpack'),
        'sections'  => array(
            'tax_filters'   => array(
                'title'         => '',
                'fields'        => array(
                    'enable_filter' => array(
                        'type'          => 'select',
                        'label'         => __('Enable Filters', 'woopack'),
                        'default'       => 'no',
                        'options'       => array(
                            'yes'           => __('Yes', 'woopack'),
                            'no'            => __('No', 'woopack')
                        ),
                        'toggle'        => array(
                            'yes'           => array(
                                'sections'      => array('filter_settings', 'filter_style', 'filter_typography')
                            )
                        )
                    ),
                ),
            ),
            'filter_settings'   => array(
                'title'             => __('Filter Settings', 'bb-powerpack'),
                'fields'            => array(
                    'filter_taxonomy'   => array(
                        'type'              => 'select',
                        'label'             => __('Select Taxonomy', 'woopack'),
                        'options'           => WooPack_Helper::get_taxonomies_list()
                    ),
                    'filter_type'       => array(
                        'type'              => 'select',
                        'label'             => __('Type', 'woopack'),
                        'default'           => 'static',
                        'options'           => array(
                            'static'            => __('Static', 'woopack'),
                            'dynamic'           => __('Dynamic', 'woopack')
                        )
                    ),
                    'filter_all_label'  => array(
                        'type'              => 'text',
                        'label'             => __('"All" Filter Label', 'woopack'),
                        'default'           => __('All', 'woopack'),
                    ),
                    'filter_alignment'  => array(
                        'type'              => 'select',
                        'label'             => __('Alignment', 'woopack'),
                        'default'           => 'left',
                        'options'           => array(
                            'left'              => __('Left', 'woopack'),
                            'center'            => __('Center', 'woopack'),
                            'right'             => __('Right', 'woopack'),
                        )
                    ),
                )
            ),
            'filter_style'  => array(
                'title'         => __('Filter Style', 'bb-powerpack'),
                'fields'        => array(
                    'filter_color' => array(
                        'type'          => 'color',
                        'label'         => __('Text Color', 'woopack'),
                        'default'       => '',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'property'      => 'color',
                            'selector'      => '.woopack-product-filters .woopack-product-filter'
                        )
                    ),
                    'filter_hover_color' => array(
                        'type'          => 'color',
                        'label'         => __('Text Hover Color', 'woopack'),
                        'default'       => '',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'none',
                        )
                    ),
                    'filter_bg_color' => array(
                        'type'          => 'color',
                        'label'         => __('Background Color', 'woopack'),
                        'default'       => '',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'property'      => 'background-color',
                            'selector'      => '.woopack-product-filters .woopack-product-filter'
                        )
                    ),
                    'filter_bg_hover_color' => array(
                        'type'          => 'color',
                        'label'         => __('Background Hover Color', 'woopack'),
                        'default'       => '',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'none',
                        )
                    ),
                    'filter_border'     => array(
                        'type'              => 'select',
                        'label'             => __('Border', 'woopack'),
                        'default'           => 'solid',
                        'options'           => array(
                            'solid'             => __('Solid', 'woopack'),
                            'double'            => __('Double', 'woopack'),
                            'dotted'            => __('Dotted', 'woopack'),
                        )
                    ),
                    'filter_border_pos'     => array(
                        'type'              => 'select',
                        'label'             => __('Border Position', 'woopack'),
                        'default'           => 'solid',
                        'options'           => array(
                            'default'           => __('Default', 'woopack'),
                            'top'               => __('Top', 'woopack'),
                            'bottom'            => __('Bottom', 'woopack'),
                        )
                    ),
                    'filter_border_el'      => array(
                        'type'                  => 'select',
                        'label'                 => __('Apply Border on', 'woopack'),
                        'default'               => 'active',
                        'options'               => array(
                            'active'                => __('Active Filter', 'woopack'),
                            'all'                   => __('All Filters', 'woopack')
                        )
                    ),
                    'filter_border_width'   => array(
                        'type'                  => 'text',
                        'label'                 => __('Border Width', 'woopack'),
                        'default'               => '0',
                        'description'           => 'px',
                        'size'                  => '5'
                    ),
                    'filter_border_color'   => array(
                        'type'                  => 'color',
                        'label'                 => __('Border Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true
                    ),
                    'filter_border_hover_color' => array(
                        'type'                      => 'color',
                        'label'                     => __('Border Hover Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true
                    ),
                    'filter_padding_v'    => array(
                        'type'              => 'text',
                        'label'             => __('Padding Top/Bottom', 'woopack'),
                        'default'           => '10',
                        'description'       => 'px',
                        'size'              => '5',
                        'preview'           => array(
                            'type'              => 'css',
                            'property'          => 'padding',
                            'selector'          => '.woopack-product-filters .woopack-product-filter',
                            'unit'              => 'px'
                        )
                    ),
                    'filter_padding_h'    => array(
                        'type'              => 'text',
                        'label'             => __('Padding Left/Right', 'woopack'),
                        'default'           => '10',
                        'description'       => 'px',
                        'size'              => '5',
                        'preview'           => array(
                            'type'              => 'css',
                            'property'          => 'padding',
                            'selector'          => '.woopack-product-filters .woopack-product-filter',
                            'unit'              => 'px'
                        )
                    ),
                    'filter_margin_h'     => array(
                        'type'              => 'text',
                        'label'             => __('Horizontal Spacing', 'woopack'),
                        'default'           => '20',
                        'description'       => 'px',
                        'size'              => '5',
                        'preview'           => array(
                            'type'              => 'css',
                            'property'          => 'margin-right',
                            'selector'          => '.woopack-product-filters .woopack-product-filter',
                            'unit'              => 'px'
                        )
                    ),
                    'filter_margin_v'     => array(
                        'type'              => 'text',
                        'label'             => __('Vertical Spacing', 'woopack'),
                        'default'           => '20',
                        'description'       => 'px',
                        'size'              => '5',
                        'preview'           => array(
                            'type'              => 'css',
                            'property'          => 'margin-bottom',
                            'selector'          => '.woopack-product-filters .woopack-product-filter',
                            'unit'              => 'px'
                        )
                    ),
                    'filter_radius'     => array(
                        'type'              => 'text',
                        'label'             => __('Round Corners', 'woopack'),
                        'default'           => '0',
                        'description'       => 'px',
                        'size'              => '5',
                        'preview'           => array(
                            'type'              => 'css',
                            'property'          => 'border-radius',
                            'selector'          => '.woopack-product-filters .woopack-product-filter',
                            'unit'              => 'px'
                        )
                    )
                )
            ),
            'filter_typography' => array(
                'title'             => __('Filter Typography', 'bb-powerpack'),
                'fields'            => array(
                    'filter_font'    => array(
                        'type'          => 'font',
						'label'         => __('Font', 'bb-powerpack'),
                        'default'		=> array(
                            'family'		=> 'Default',
                            'weight'		=> 400
                        ),
						'preview'       => array(
							'type'		    => 'font',
							'selector'      => '.woopack-product-filters .woopack-product-filter',
						),
                    ),
                    'filter_font_size'  => array(
                        'type'              => 'select',
                        'label'             => __('Font Size', 'bb-powerpack'),
                        'default'           => 'default',
                        'options'           => array(
                            'default'           => __('Default', 'bb-powerpack'),
                            'custom'            => __('Custom', 'bb-powerpack'),
                        ),
                        'toggle'            => array(
                            'custom'            => array(
                                'fields'            => array('filter_font_size_custom')
                            )
                        )
                    ),
                    'filter_font_size_custom'   => array(
                        'type'                      => 'unit',
                        'label'                     => __('Custom Font Size', 'bb-powerpack'),
                        'default'                   => '',
                        'preview' 			        => array(
                            'type'                      => 'css',
                            'selector'			        => '.woopack-product-filters .woopack-product-filter',
                            'property'      	        => 'font-size',
                            'unit'                      => 'px',
                        ),
                        'responsive' 			    => array(
                            'placeholder'               => array(
                                'default'               => '',
                                'medium'                => '',
                                'responsive'            => '',
                            ),
                        ),
                    ),
                    'filter_line_height'    => array(
                        'type'                  => 'select',
                        'label'                 => __('Line Height', 'bb-powerpack'),
                        'default'               => 'default',
                        'options'               => array(
                            'default'               => __('Default', 'bb-powerpack'),
                            'custom'                => __('Custom', 'bb-powerpack'),
                        ),
                         'toggle'            => array(
                            'custom'            => array(
                                'fields'            => array('filter_line_height_custom')
                            )
                        )
                    ),
                    'filter_line_height_custom' => array(
                        'type'                      => 'unit',
                        'label'                     => __('Custom Line Height', 'bb-powerpack'),
                        'default'                   => '',
                        'preview' 			        => array(
                            'type'                      => 'css',
                            'selector'			        => '.woopack-product-filters .woopack-product-filter',
                            'property'      	        => 'line-height',
                        ),
                        'responsive' 			    => array(
                            'placeholder'               => array(
                                'default'               => '',
                                'medium'                => '',
                                'responsive'            => '',
                            ),
                        ),
                    )
                )
            )
        )
    ),
    'pagination'=> array(
        'title'     => __( 'Pagination', 'woopack' ),
        'sections'  => array(
            'pagination'            => array(
                'title'                 => __('Pagination', 'woopack'),
                'fields'                => array(
                    'pagination'            => array(
                        'type'                  => 'select',
                        'label'                 => __('Pagination Style', 'woopack'),
                        'default'               => 'numbers',
                        'options'               => array(
                            'numbers'               => __('Numbers', 'woopack'),
                            'scroll'                => __('Scroll', 'woopack'),
                            'none'                  => _x( 'None', 'Pagination style.', 'woopack' ),
                        ),
                        'toggle'                =>  array(
                            'numbers'               => array(
                                'sections'          => array( 'pagination_font', 'pagination_padding', 'pagination_margin', 'pagination_border', 'pagination_color', 'pagination_hover', 'pagination_property' ),
                                'fields'            => array('pagination_align'),
                            ),
                        ),
                    ),
                    'no_results_message'    => array(
                        'type'                  => 'text',
                        'label'                 => __('No Results Message', 'woopack'),
                        'default'               => __("Sorry, we couldn't find any products. Please try a different search.", 'woopack'),
                        'connections'           => array('string'),
                    ),
                    'show_search'           => array(
                        'type'                  => 'select',
                        'label'                 => __('Show Search', 'woopack'),
                        'default'               => '1',
                        'options'               => array(
                            '1'                     => __('Show', 'woopack'),
                            '0'                     => __('Hide', 'woopack')
                        ),
                        'help'                  => __( 'Shows the search form if no products are found.', 'woopack' )
                    ),
                    'pagination_align'      => array(
                        'type'                  => 'select',
                        'label'                 => __('Alignment', 'woopack'),
                        'default'               => 'center',
                        'options'               => array(
                            'default'               => __('Default', 'woopack'),
                            'left'                  => __('Left', 'woopack'),
                            'center'                => __('Center', 'woopack'),
                            'right'                 => __('Right', 'woopack'),
                        ),
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.fl-builder-pagination ul.page-numbers',
                            'property'              => 'text-align',
                        ),
                    ),
                ),
            ),
            'pagination_color'      => array(
                'title'                 => __('Colors', 'woopack'),
                'fields'                => array(
                    'pagination_bg_color'       => array(
                        'type'                      => 'color',
                        'label'                     => __('Background Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.fl-builder-pagination ul.page-numbers li a.page-numbers, .fl-builder-pagination ul.page-numbers li span.page-numbers',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'pagination_bg_color_hover'	=> array(
                        'type'                      => 'color',
                        'label'                     => __('Background Hover Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.fl-builder-pagination ul.page-numbers li .page-numbers:hover, .fl-builder-pagination ul.page-numbers li .page-numbers.current',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'pagination_color'          => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.fl-builder-pagination ul.page-numbers li a.page-numbers, .fl-builder-pagination ul.page-numbers li span.page-numbers',
                            'property'                  => 'color',
                        ),
                    ),
                    'pagination_color_hover'    => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Hover Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.fl-builder-pagination ul.page-numbers li a.page-numbers:hover, .fl-builder-pagination ul.page-numbers li .page-numbers.current',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'pagination_border'     => array(
                'title'                 => __('Border', 'woopack'),
                'fields'                => array(
                    'pagination_border_style'       => array(
                        'type'                          => 'select',
                        'label'                         => __('Border Style', 'woopack'),
                        'default'                       => 'none',
                        'options'                       => array(
                            'none'                          => __('None', 'woopack'),
                            'solid'                         => __('Solid', 'woopack'),
                            'dotted'                        => __('Dotted', 'woopack'),
                            'dashed'                        => __('Dashed', 'woopack'),
                        ),
                        'toggle'                        => array(
                            'solid'                         => array(
                                'fields'                        => array( 'pagination_border_width', 'pagination_border_color', 'pagination_border_color_hover'),
                            ),
                            'dotted'                        => array(
                                'fields'                        => array( 'pagination_border_width', 'pagination_border_color', 'pagination_border_color_hover'),
                            ),
                            'dashed'                        => array(
                                'fields'                        => array( 'pagination_border_width', 'pagination_border_color', 'pagination_border_color_hover'),
                            ),
                        ),
                    ),
                    'pagination_border_width'       => array(
                        'type'                          => 'unit',
                        'default'                       => '0',
                        'label' 			            => __('Border Width', 'woopack'),
                        'description' 		            => 'px',
                        'preview' 			            => array(
                            'type'                          => 'css',
                            'selector'			            => '.fl-builder-pagination ul.page-numbers li .page-numbers',
                            'property'      	            => 'border-width',
                            'unit'                          => 'px',
                        ),
                        'responsive' 			        => array(
                            'placeholder'                   => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                    'pagination_border_color'       => array(
                        'type'                          => 'color',
                        'label'                         => __('Border Color', 'woopack'),
                        'show_reset'                    => true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.fl-builder-pagination ul.page-numbers li .page-numbers',
                            'property'                      => 'border-color',
                        ),
                    ),
                    'pagination_border_color_hover' => array(
                        'type'                          => 'color',
                        'label'                         => __('Border Hover Color', 'woopack'),
                        'show_reset'                    => true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.fl-builder-pagination ul.page-numbers li .page-numbers:hover, .fl-builder-pagination ul.page-numbers li .page-numbers.current',
                            'property'                      => 'border-color',
                        ),
                    ),
                    'pagination_border_radius'      => array(
                        'type'                          => 'unit',
                        'label'                         => __('Round Corners', 'woopack'),
                        'default'                       => '0',
                        'description'                   => 'px',
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.fl-builder-pagination ul.page-numbers li .page-numbers',
                            'property'                      => 'border-radius',
                            'unit'                          => 'px',
                        ),
                        'responsive' 			        => array(
                            'placeholder'                   => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                ),
            ),
            'pagination_padding'    => array(
                'title'                 => __('Padding', 'woopack'),
                'fields'                => array(
                    'pagination_padding'    => array(
                        'type'                      => 'dimension',
                        'label'                     => __('Padding', 'woopack'),
                        'description' 		        => 'px',
                        'preview' 			        => array(
                            'type'                      => 'css',
                            'selector'                  => '.fl-builder-pagination ul.page-numbers li .page-numbers',
                            'property'                  => 'padding',
                            'unit'                      => 'px'
                        ),
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                ),
            ),
            'pagination_margin'     => array(
                'title'                 => __('Margin', 'woopack'),
                'fields'                => array(
                    'pagination_margin'     => array(
                        'type'                      => 'dimension',
                        'label'                     => __('Margin', 'woopack'),
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.fl-builder-pagination ul.page-numbers li .page-numbers',
                            'property'                  => 'margin',
                            'unit'                      => 'px'
                        ),
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                ),
            ),
            'pagination_font'       => array(
                'title'                 => __('Typography', 'woopack'),
                'fields'                => array(
                    'pagination_font'               => array(
                        'type'                          => 'font',
                        'label'                         => __('Font', 'woopack'),
                        'default' 		                => array(
                            'family'                        => 'Default',
                            'weight'                        => 300
                        ),
                        'preview'                       => array(
                            'type'                          => 'font',
                            'selector'                      => '.fl-builder-pagination ul.page-numbers li .page-numbers',
                            'property'		                => 'font-family',
                        ),
                    ),
                    'pagination_font_size'          => array(
                        'type'                          => 'select',
                        'label'  			            => __('Font Size', 'woopack'),
                        'default'                       => 'default',
                        'options' 		                => array(
                            'default' 		                => __('Default', 'woopack'),
                            'custom'                        => __('Custom', 'woopack'),
                        ),
                        'toggle'                        => array(
                            'custom'                        => array(
                                'fields'                        => array('pagination_font_size_custom')
                            ),
                        ),
                    ),
                    'pagination_font_size_custom'   => array(
                        'type'                          => 'unit',
                        'label'                         => __('Custom Font Size', 'woopack'),
                        'description' 			        => 'px',
                        'responsive' 			        => array(
                            'placeholder'                   => array(
                                'default'                       => '15',
                                'medium'                        => '',
                                'responsive'                    => '',
                            ),
                        ),
                        'preview'				        => array(
                            'type'                          => 'css',
                            'selector'                      => '.fl-builder-pagination ul.page-numbers li .page-numbers',
                            'property'                      => 'font-size',
                            'unit'                          => 'px',
                        ),
                    ),
                ),
            ),
        ),
    ),
    'style'     => array(
        'title'     => __('Style', 'woopack'),
        'sections'  => apply_filters( 'woopack_product_grid_style_sections', woopack_product_style_fields() )
    ),
    'button'    => array(
        'title'     => __('Button', 'woopack'),
        'sections'  => woopack_product_button_fields()
    ),
    'typography'=> array(
        'title'     => __('Typography', 'woopack'),
        'sections'  => woopack_product_typography_fields()
    ),
));
