;(function($) {

    WooPackGrid = function(settings)
	{
		this.settings       = settings;
		this.nodeClass      = '.fl-node-' + settings.id;
		this.wrapperClass   = this.nodeClass + ' .woopack-products-grid-wrap ul.products';
		this.postClass      = this.wrapperClass + ' .woopack-product-grid';
        this.perPage        = settings.perPage;
		this.matchHeight	= settings.matchHeight === 'yes' ? true : false;
        this.layoutStyle	= settings.layoutStyle;
        this.masonry        = settings.matchHeight === 'no' ? true : false;
        this.filters        = settings.filters === 'yes' ? true : false;
        this.filterTax      = settings.filterTax;
        this.filterType     = settings.filterType;

		if(this._hasPosts()) {
			this._initInfiniteScroll();
            //this._matchWidth();
			this._gridLayout();
		}
	};
    WooPackGrid.prototype = {

		settings        : {},
		nodeClass       : '',
		wrapperClass    : '',
		postClass       : '',
        perPage         : '',
        matchHeight     : false,
        layoutStyle     : 1,
        masonry         : false,
        filters         : false,
        filterTax       : '',
		filterType      : '',
		cacheData		: {},

		_hasPosts: function()
		{
			return $(this.postClass).length > 0;
		},

        _matchWidth:function()
		{
            var wrap = $(this.wrapperClass);

			wrap.imagesLoaded( $.proxy( function() {
				if ( 3 == this.layoutStyle || 4 == this.layoutStyle ) {
				   var img_width = $( this.postClass + ' .woopack-product-image img').attr('width');
					$( this.postClass + ' .woopack-product-content').css({ 'width': 'calc(100% - ' + img_width+ 'px)' });
			    }
			}, this ) );
        },

        _gridLayout: function()
		{
			var wrap = $(this.wrapperClass);

            var postFilterData = {
				itemSelector: '.woopack-product-grid',
				percentPosition: true,
				transitionDuration: '0.4s',
			};

			if ( ! this.masonry ) {
				postFilterData = $.extend( {}, postFilterData, {
					layoutMode: 'fitRows',
					fitRows: {
						gutter: '.woopack-grid-sizer'
				  	},
				} );
			}

			if ( this.masonry ) {

				postFilterData = $.extend( {}, postFilterData, {
					masonry: {
						columnWidth: '.woopack-product-grid',
						gutter: '.woopack-grid-sizer'
					},
				} );
			}

			wrap.imagesLoaded( $.proxy( function() {

				var node = $(this.nodeClass);
                var base = this;

                if ( this.filters || this.masonry ) {

                    var postFilters = $(this.nodeClass).find('ul.products').isotope(postFilterData);
                    var filterWrap = $(this.nodeClass).find('.woopack-product-filters');
					var filterToggle = $(this.nodeClass).find('.woopack-product-filters-toggle');
					
					filterToggle.on('click', function() {
						filterWrap.slideToggle(function() {
							if ( $(this).is(':visible') ) {
								$(this).addClass('woopack-product-filters-open');
							}
							if ( ! $(this).is(':visible') ) {
								$(this).removeClass('woopack-product-filters-open');
							}
						});
					});

                    filterWrap.on('click', '.woopack-product-filter', function() {
                        if ( 'static' === base.filterType ) {
                            var filterVal = $(this).attr('data-filter');
                            postFilters.isotope({ filter: filterVal });
                        } else {
                            var term = $(this).data('term');
							base._getPosts(term, postFilterData);
						}

                        filterWrap.find('.woopack-product-filter').removeClass('woopack-filter-active');
						$(this).addClass('woopack-filter-active');
						
						filterToggle.find('span.toggle-text').html($(this).text());
						if (filterWrap.hasClass('woopack-product-filters-open')) {
							filterWrap.slideUp();
						}
					});
					
					if ('dynamic' === base.filterType) {
						$(base.nodeClass).find('.fl-builder-pagination a').off('click').on('click', function (e) {
							e.preventDefault();
							var pageNumber = $(this).attr('href').split('#page-')[1];
							base._getPosts('', postFilterData, pageNumber);
						});
					}
                }

                if( !this.masonry ) {
                    setTimeout( function() {
                        base._gridLayoutMatchHeight();
                    }, 1000 );
                }

                if ( this.filters || this.masonry ) {
                    setTimeout( function() {
                        if ( 'static' === base.filterType ) {
                            node.find('.woopack-filter-active').trigger('click');
                        }
						if ( ! base.masonry ) {
                        	base._gridLayoutMatchHeight();
						}
                        node.find('ul.products').isotope('layout');

                    }, 1000 );
                }

			}, this ) );
		},

		_getPosts: function (term, isotopeData, paged) {
			var processAjax = false,
				filter 		= term,
				paged 		= (!paged || 'undefined' === typeof paged) ? 1 : paged;

			if ('undefined' === typeof term || '' === filter) {
				filter = 'all';
			}

			var cacheData = this._getCacheData(filter);

			if ('undefined' === typeof cacheData) {
				processAjax = true;
			} else {
				var cachedResponse = cacheData.page[paged];
				if ('undefined' === typeof cachedResponse) {
					processAjax = true;
				} else {
					this._renderPosts(cachedResponse, {
						term: term,
						isotopeData: isotopeData,
						page: paged
					});
				}
			}

			if (processAjax) {
				this._getAjaxPosts(term, isotopeData, paged);
			}
		},

        _getAjaxPosts: function(term, isotopeData, paged)
        {
            var taxonomy    = this.filterTax,
				perPage     = this.perPage,
				layout      = this.layoutStyle,
				paged 		= 'undefined' === typeof paged ? false : paged,
                self        = this;

            var wrap        = $(this.wrapperClass),
				gridWrap    = $(this.nodeClass).find('.woopack-products-grid-wrap');
				
			var currentPage = woopack_config.current_page.split('?')[0];

            var data = {
                action: 'woopack_grid_get_posts',
                node_id: this.settings.id,
				page: !paged ? woopack_config.page : paged,
				current_page: currentPage,
                settings: this.settings.fields
            };

            if ( '' !== term ) {
                data['term'] = term;
			}
			if ( 'undefined' !== self.settings.orderby || '' !== self.settings.orderby ) {
				data['orderby'] = self.settings.orderby;
			}

			gridWrap.addClass('woopack-is-filtering');

			$.ajax({
				type: 'post',
				url: woopack_config.ajaxurl,
				data: data,
				success: function (response) {
					gridWrap.removeClass('woopack-is-filtering');
					self._setCacheData(term, response, paged);
					self._renderPosts(response, {
						term: term,
						isotopeData: isotopeData,
						page: paged
					});
				}
			});
		},
		
		_renderPosts: function (response, args)
		{
			var self = this,
				wrap = $(this.wrapperClass),
				gridWrap = $(this.nodeClass).find('.woopack-products-grid-wrap');

			wrap.isotope('remove', $(this.postClass));

			wrap.imagesLoaded($.proxy(function () {
				if (!this.masonry) {
					wrap.isotope('insert', $(response.data), $.proxy(this._gridLayoutMatchHeight, this));
					this._gridLayoutMatchHeight();
				} else {
					wrap.isotope('insert', $(response.data));
				}
				wrap.find('.woopack-grid-sizer').remove();
				wrap.append('<li class="woopack-grid-sizer"></li>');

				setTimeout(function() {
					wrap.isotope('layout');
				}, 200);
			}, this ));

			if (response.pagination) {
				$(self.nodeClass).find('.fl-builder-pagination').remove();
				$(self.nodeClass).find('.fl-module-content').append(response.pagination);
				$(self.nodeClass).find('.woopack-ajax-pagination a').off('click').on('click', function (e) {
					e.preventDefault();
					var pageNumber = $(this).attr('href').split('#page-')[1];
					self._getPosts(args.term, args.isotopeData, pageNumber);
				});
			} else {
				$(self.nodeClass).find('.fl-builder-pagination').remove();
			}

			var offsetTop = gridWrap.offset().top - 200;
			$('html, body').stop().animate({
				scrollTop: offsetTop
			}, 300);
		},

		_setCacheData: function (filter, response, paged)
		{
			if ('undefined' === typeof filter || '' === filter) {
				filter = 'all';
			}
			if ('undefined' === typeof paged || !paged) {
				paged = 1;
			}

			if ('undefined' === typeof this.cacheData.ajaxCache) {
				this.cacheData.ajaxCache = {};
			}
			if ('undefined' === typeof this.cacheData.ajaxCache[filter]) {
				this.cacheData.ajaxCache[filter] = {};
			}
			if ('undefined' === typeof this.cacheData.ajaxCache[filter].page) {
				this.cacheData.ajaxCache[filter].page = {};
			}

			return this.cacheData.ajaxCache[filter].page[paged] = response;
		},

		_getCacheData: function (filter)
		{
			var cacheData = this.cacheData;

			if ('undefined' === typeof cacheData.ajaxCache) {
				cacheData.ajaxCache = {};
			}
			//console.log(cacheData);

			return cacheData.ajaxCache[filter];
		},

		_gridLayoutMatchHeight: function()
		{
			var highestBox = 0;
			if ( false === this.matchHeight ) {
				return;
			}

            $(this.postClass).css('height', '').each(function(){

                if($(this).height() > highestBox) {
                	highestBox = $(this).height();
                }
            });

            $(this.postClass).height(highestBox);
		},

		_initInfiniteScroll: function()
		{
			if (this.settings.pagination == 'scroll' && typeof FLBuilder === 'undefined') {
				this._infiniteScroll();
			}
		},

		_infiniteScroll: function(settings)
		{
			$(this.wrapperClass).infinitescroll({
				navSelector     : this.nodeClass + ' .fl-builder-pagination',
				nextSelector    : this.nodeClass + ' .fl-builder-pagination a.next',
				itemSelector    : this.postClass,
				prefill         : true,
				bufferPx        : 200,
				animate			: false,
				loading         : {
					msgText         : 'Loading',
					finishedMsg     : '',
					img             : FLBuilderLayoutConfig.paths.pluginUrl + 'img/ajax-loader-grey.gif',
					speed           : 1
				}
			}, $.proxy(this._infiniteScrollComplete, this));

			setTimeout(function(){
				$(window).trigger('resize');
			}, 100);
		},

		_infiniteScrollComplete: function(elements)
		{
			var wrap = $(this.wrapperClass);

			elements = $(elements);

			wrap.imagesLoaded( $.proxy( function() {
				if( !this.masonry ) {
					this._gridLayoutMatchHeight();
					if( this.filters ) {
						wrap.isotope('insert', elements, $.proxy(this._gridLayoutMatchHeight, this));
					}
				} else {
					wrap.isotope('insert', elements);
				}

				elements.css('visibility', 'visible');
				wrap.find('.woopack-grid-sizer').remove();
				wrap.append('<li class="woopack-grid-sizer"></li>');
			}, this ) );
		}
	};

})(jQuery);
