<?php
/**
* @class WooPackCart
*/
class WooPackCart extends FLBuilderModule {
    /**
    * Constructor function for the module. You must pass the
    * name, description, dir and url in an array to the parent class.
    *
    * @method __construct
    */
    public function __construct()
    {
        parent::__construct(array(
            'name' 				=> __('Cart', 'woopack'),
            'description' 		=> __('Addon to display Cart.', 'woopack'),
            'group'             => WooPack_Helper::get_modules_group(),
            'category' 			=> WOOPACK_CAT,
            'dir' 				=> WOOPACK_DIR . 'modules/cart/',
            'url' 				=> WOOPACK_URL . 'modules/cart/',
            'editor_export' 	=> true, // Defaults to true and can be omitted.
            'enabled' 			=> true, // Defaults to true and can be omitted.
        ));
    }
}
/**
* Register the module and its form settings.
*/
FLBuilder::register_module('WooPackCart', array(
    'content'           => array(
        'title'             => __('Content', 'woopack'),
        'sections'          => array(
            'coupon'            => array(
                'title'             => __('Apply Coupon', 'woopack'),
                'fields'            => array(
                    'show_coupon'       => array(
                        'type'              => 'select',
                        'label' 			=> __('Show Coupon?', 'woopack'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no'                => __('No', 'woopack'),
                        ),
                        'toggle'            => array(
                            'yes'               => array(
                                'fields'            => array( 'coupon_text_color'),
                            ),
                        ),
                    ),
                    'coupon_text_color' => array(
                        'type'              => 'color',
                        'label'             => __('Input Text Color', 'woopack'),
                        'default'           => '',
                        'show_reset'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => 'table.shop_table.cart td.actions .coupon .input-text',
                            'property'          => 'color',
                        ),
                    ),
                ),
            ),
            'cross_sells'       => array(
                'title'             => __('Cross Sells', 'woopack'),
                'fields'            => array(
                    'show_cross_sells'  => array(
                        'type'              => 'select',
                        'label' 			=> __('Show Cross Sells?', 'woopack'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no'                => __('No', 'woopack'),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'cart_table_style'  => array(
        'title'             => __('Cart Table', 'woopack'),
        'sections'          => array(
            'box_style'         => array(
                'title'             => __('Box', 'woopack'),
                'fields'            => array(
                    'table_bg_color'        => array(
                        'type'                  => 'color',
                        'label'                 => __('Background Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true,
                        'show_alpha'            => true,
                        'preview'               => array(
                            'type' 				 => 'css',
                            'selector'			   => 'table.shop_table.cart',
                            'property'			   => 'background-color',
                        ),
                    ),
                    'table_border_style'    => array(
						'type'                => 'select',
						'label'               => __('Border Style', 'woopack'),
						'default'             => 'none',
						'options'             => array(
							'none'               => __('None', 'woopack'),
							'solid'              => __('Solid', 'woopack'),
							'dotted'             => __('Dotted', 'woopack'),
							'dashed'             => __('Dashed', 'woopack'),
						),
						'toggle'            => array(
							'solid'              => array(
                                'fields'                => array( 'table_border_width', 'table_border_color'),
							),
							'dotted'             => array(
                                'fields'                => array( 'table_border_width', 'table_border_color'),
							),
							'dashed'             => array(
                                'fields'                => array( 'table_border_width', 'table_border_color'),
							),
						),
					),
                    'table_border_width'    => array(
                        'type'                  => 'unit',
                        'label' 			    => __('Border Width', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'			    => 'table.shop_table.cart',
                            'property'              => 'border-width',
                            'unit'				    => 'px',
                        ),
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'table_border_color'    => array(
                        'type'                  => 'color',
                        'label' 			    => __('Border Color', 'woopack'),
                        'show_reset' 		    => true,
                        'show_alpha' 		    => true,
                        'default'               => '',
                        'preview' 			    => array(
                            'type' 				    => 'css',
                            'selector'		        => 'table.shop_table.cart',
                            'property'              => 'border-color',
                        ),
                    ),
                    'table_border_radius'   => array(
                        'type' 				    => 'unit',
                        'label' 		        => __('Round Corners', 'woopack'),
                        'default' 			    => '5',
                        'description' 		    => 'px',
                        'preview' 			    => array(
                            'type' 				    => 'css',
                            'selector'			    => 'table.shop_table.cart',
                            'property'      	    => 'border-radius',
                            'unit'				    => 'px',
                        ),
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'table_padding_t_b'     => array(
                        'type'                  => 'unit',
                        'label'                 => __('Top & Bottom Padding', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'rules' 	            => array(
                                array(
                                    'selector'          => 'table.shop_table.cart',
                                    'property'          => 'padding-top',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => 'table.shop_table.cart',
                                    'property'          => 'padding-bottom',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'            => array(
                            'placeholder' 		    => array(
                                'default'           => '',
                                'medium' 			=> '',
                                'responsive'        => '',
                            ),
                        ),
                    ),
                    'table_padding_r_l'     => array(
                        'type'                  => 'unit',
                        'label'                 => __('Right & Left Padding', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'rules' 	            => array(
                                array(
                                    'selector'          => 'table.shop_table.cart',
                                    'property'          => 'padding-right',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => 'table.shop_table.cart',
                                    'property'          => 'padding-left',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'            => array(
                            'placeholder'           => array(
                                'default'               => '',
                                'medium'                => '',
                                'responsive'            => '',
                            ),
                        ),
                    ),
                ),
            ),
            'table_header_style'=> array(
                'title'             => __('Table Heading', 'woopack'),
                'fields'            => array(
                    'table_header_padding'      => array(
                        'type'                      => 'unit',
                        'label' 				    => __('Vertical Spacing', 'woopack'),
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'rules'                     => array(
                                array(
                                    'selector'          => 'table.shop_table.cart thead th',
                                    'property'          => 'padding-top',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => 'table.shop_table.cart thead th',
                                    'property'          => 'padding-bottom',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                    'table_header_border_width' => array(
                        'type'                      => 'unit',
                        'label'                     => __('Separator Width', 'woopack'),
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => 'table.shop_table.cart thead th',
                            'property'                  => 'border-bottom-width',
                            'unit'                      => 'px',
                        ),
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                    'table_header_border_color' => array(
                        'type'                      => 'color',
                        'label'                     => __('Separator Color', 'woopack'),
                        'show_reset'                => true,
                        'default'                   => '',
                    ),
                ),
            ),
            'cart_item_style'   => array(
                'title'             => __('Cart Item', 'woopack'),
                'fields'            => array(
                    'cart_item_padding'      => array(
                        'type'                  => 'unit',
                        'label' 				=> __('Vertical Spacing', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'rules' 	            => array(
                                array(
                                    'selector'          => 'table.shop_table.cart td',
                                    'property'          => 'padding-top',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => 'table.shop_table.cart td',
                                    'property'          => 'padding-bottom',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                    'cart_item_odd_color'    => array(
                        'type' 				     => 'color',
                        'label' 			     => __('Odd Item Background Color', 'woopack'),
                        'show_reset' 		     => true,
                        'show_alpha' 		     => true,
                        'default'                => '',
                        'preview'                => array(
                            'type' 				     => 'css',
                            'selector'			     => 'tr.woocommerce-cart-form__cart-item.cart_item:nth-child(odd)',
                            'property'               => 'background',
                        ),
                    ),
                    'cart_item_even_color'   => array(
                        'type' 				     => 'color',
                        'label' 			     => __('Even Item Background Color', 'woopack'),
                        'show_reset' 		     => true,
                        'show_alpha' 		     => true,
                        'default'                => '',
                        'preview' 			     => array(
                            'type' 				     => 'css',
                            'selector'			     => 'tr.woocommerce-cart-form__cart-item.cart_item:nth-child(even)',
                            'property'      	     => 'background',
                        ),
                    ),
                    'cart_item_border_width' => array(
                        'type'                  => 'unit',
                        'label'                 => __('Separator Width', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => 'table.shop_table.cart td',
                            'property'              => 'border-top-width',
                            'unit'                  => 'px',
                        ),
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'cart_item_border_color' => array(
                        'type' 				     => 'color',
                        'label' 			     => __('Separator Color', 'woopack'),
                        'show_reset' 		     => true,
                        'show_alpha' 		     => true,
                        'default'                => '',
                    ),
                ),
            ),
            'image_style'       => array(
                'title'             => __('Image', 'woopack'),
                'fields'            => array(
                    'image_width'       => array(
                        'type'              => 'unit',
                        'label' 			=> __('Width', 'woopack'),
                        'description' 		=> 'px',
                        'preview' 			=> array(
                            'type' 				=> 'css',
                            'selector'			=> '.woocommerce table.cart .product-thumbnail img',
                            'property'			=> 'width',
                            'unit' 				=> 'px'
                        ),
                        'responsive' 		=> array(
                            'placeholder' 		=> array(
                                'default' 			=> '',
                                'medium' 			=> '',
                                'responsive'		=> '',
                            ),
                        ),
                    ),
                ),
            ),
            'product_remove_font'   => array(
                'title'                 => __('Product Remove Icon', 'woopack'),
                'fields'                => array(
                    'product_remove_font_size'          => array(
                        'type'                              => 'select',
                        'label'  			                => __('Size', 'woopack'),
                        'default'                           => 'default',
                        'options' 		                    => array(
                            'default' 		                    => __('Default', 'woopack'),
                            'custom'                            => __('Custom', 'woopack'),
                        ),
                        'toggle'                            => array(
                            'custom'                            => array(
                                'fields'                            => array('product_remove_font_size_custom')
                            ),
                        ),
                    ),
                    'product_remove_font_size_custom'   => array(
                        'type'                              => 'unit',
                        'label'                             => __('Custom Size', 'woopack'),
                        'description'                       => 'px',
                        'responsive' 			            => array(
                            'placeholder'                       => array(
                                'default'                           => '',
                                'medium' 				            => '',
                                'responsive' 			            => '',
                            ),
                        ),
                        'preview'                           => array(
                            'type' 					            => 'css',
                            'selector'                          => 'table.shop_table.cart td.product-remove a',
                            'property'                          => 'font-size',
                            'unit'                              => 'px',
                        ),
                    ),
                    'product_remove_color'              => array(
                        'type'                              => 'color',
                        'label'                             => __('Color', 'woopack'),
                        'default'                           => '',
                        'show_reset'                        => true,
                    ),
                    'product_remove_color_hover'        => array(
                        'type'                              => 'color',
                        'label'                             => __('Color on Hover', 'woopack'),
                        'default'                           => '',
                        'show_reset'                        => true,
                    ),
                ),
            ),
        ),
    ),
    'cart_total_style'  => array(
        'title'         => __('Cart Totals', 'woopack'),
        'sections'      => array(
            'cart_total_box_style' 	=> array(
                'title'                 => __('Box', 'woopack'),
                'fields'                => array(
                    'cart_total_bg_color'       => array(
                        'type'                      => 'color',
                        'label'                     => __('Background Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.cart_totals table.shop_table',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'cart_total_border_style'   => array(
                        'type'                      => 'select',
                        'label'                     => __('Border Style', 'woopack'),
                        'default'                   => 'none',
                        'options'                   => array(
                            'none'                      => __('None', 'woopack'),
                            'solid'                     => __('Solid', 'woopack'),
                            'dotted'                    => __('Dotted', 'woopack'),
                            'dashed'                    => __('Dashed', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'solid'                     => array(
                                'fields'                        => array( 'cart_total_border_width', 'cart_total_border_color'),
                            ),
                            'dotted'                    => array(
                                'fields'                        => array( 'cart_total_border_width', 'cart_total_border_color'),
                            ),
                            'dashed'                    => array(
                                'fields'                        => array( 'cart_total_border_width', 'cart_total_border_color'),
                            ),
                        ),
                    ),
                    'cart_total_border_width'   => array(
                        'type'                      => 'unit',
                        'label'                     => __('Border Width', 'woopack'),
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.cart_totals table.shop_table',
                            'property'                  => 'border-width',
                            'unit'                      => 'px',
                        ),
                        'responsive' 			    => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                    'cart_total_border_color'   => array(
                        'type'                      => 'color',
                        'label'                     => __('Border Color', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'default'                   => '',
                    ),
                    'cart_total_border_radius'  => array(
                        'type'                      => 'unit',
                        'label'                     => __('Round Corners', 'woopack'),
                        'default'                   => '5',
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.cart_totals table.shop_table',
                            'property'                  => 'border-radius',
                            'unit'                      => 'px',
                        ),
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium' 				    => '',
                                'responsive'			    => '',
                            ),
                        ),
                    ),
                    'cart_total_padding_t_b'    => array(
                        'type'                      => 'unit',
                        'label'                     => __('Top & Bottom Padding', 'woopack'),
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'rules'                     => array(
                                array(
                                    'selector'          => '.cart_totals table.shop_table',
                                    'property'          => 'padding-top',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => '.cart_totals table.shop_table',
                                    'property'          => 'padding-bottom',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'            => array(
                            'placeholder' 		    => array(
                                'default'           => '',
                                'medium' 			=> '',
                                'responsive'        => '',
                            ),
                        ),
                    ),
                    'cart_total_padding_r_l'    => array(
                        'type'                      => 'unit',
                        'label'                     => __('Right & Left Padding', 'woopack'),
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'rules' 	                => array(
                                array(
                                    'selector'          => '.cart_totals table.shop_table',
                                    'property'          => 'padding-right',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => '.cart_totals table.shop_table',
                                    'property'          => 'padding-left',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'            => array(
                            'placeholder'           => array(
                                'default'               => '',
                                'medium'                => '',
                                'responsive'            => '',
                            ),
                        ),
                    ),
                ),
            ),
            'cart_total_padding'    => array(
                'title'                 => __('Cart Totals', 'woopack'),
                'fields'                => array(
                    'cart_total_padding'    => array(
                        'type'                  => 'unit',
                        'label' 				=> __('Vertical Spacing', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'rules' 	            => array(
                                array(
                                    'selector'          => '.cart_totals table.shop_table tbody th, .cart_totals table.shop_table tbody td',
                                    'property'          => 'padding-top',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => '.cart_totals table.shop_table tbody th, .cart_totals table.shop_table tbody td',
                                    'property'          => 'padding-bottom',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'            => array(
                            'placeholder'           => array(
                                'default'               => '',
                                'medium'                => '',
                                'responsive'            => '',
                            ),
                        ),
                    ),
                ),
            ),
            'cart_total_separator'  => array(
                'title'                 => __('Separator', 'woopack'),
                'fields'                => array(
                    'cart_total_separator_width' => array(
                        'type'                      => 'unit',
                        'label'                     => __('Separator Width', 'woopack'),
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.cart_totals table.shop_table tbody th, .cart_totals table.shop_table tbody td',
                            'property'                  => 'border-top-width',
                            'unit'                      => 'px',
                        ),
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium' 				    => '',
                                'responsive'			    => '',
                            ),
                        ),
                    ),
                    'cart_total_separator_color' => array(
                        'type' 				         => 'color',
                        'label' 			         => __('Separator Color', 'woopack'),
                        'show_reset' 		         => true,
                        'show_alpha' 		         => true,
                        'default'                    => '',
                    ),
                ),
            ),
        ),
    ),
    'button_style'      => array(
        'title'             => __('Buttons', 'woopack'),
        'sections'          => array(
            'standard_button'       => array(
                'title'                 => __('Standard Button Colors', 'woopack'),
                'fields'                => array(
                    'standard_button_bg_color'          => array(
                        'type'                              => 'color',
                        'label'                             => __('Background Color', 'woopack'),
                        'default'                           => '',
                        'show_reset'                        => true,
                        'show_alpha'                        => true,
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '.woocommerce button.button, .woocommerce input.button',
                            'property'                          => 'background-color',
                        ),
                    ),
                    'standard_button_bg_color_hover'    => array(
                        'type'                              => 'color',
                        'label'                             => __('Background Hover Color', 'woopack'),
                        'default'                           => '',
                        'show_reset'                        => true,
                        'show_alpha'                        => true,
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '.woocommerce button.button, .woocommerce input.button:hover',
                            'property'                          => 'background-color',
                        ),
                    ),
                    'standard_button_color'             => array(
                        'type'                              => 'color',
                        'label'                             => __('Text Color', 'woopack'),
                        'default'                           => '',
                        'show_reset'                        => true,
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '.woocommerce button.button, .woocommerce input.button',
                            'property'                          => 'color',
                        ),
                    ),
                    'standard_button_color_hover'       => array(
                        'type'                              => 'color',
                        'label'                             => __('Text Color On Hover', 'woopack'),
                        'default'                           => '',
                        'show_reset'                        => true,
                        'show_alpha'                        => true,
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '.woocommerce button.button, .woocommerce input.button:hover',
                            'property'                          => 'color',
                        ),
                    ),
                ),
            ),
            'standard_button_border'=> array(
                'title'                 => __('Standard Button Border', 'woopack'),
                'fields'                => array(
                    'standard_button_border_style'  => array(
                        'type'                          => 'select',
                        'label'                         => __('Border Style', 'woopack'),
                        'default'                       => 'none',
                        'options'                       => array(
                            'none'                          => __('None', 'woopack'),
                            'solid'                         => __('Solid', 'woopack'),
                            'dotted'                        => __('Dotted', 'woopack'),
                            'dashed'                        => __('Dashed', 'woopack'),
                        ),
                        'toggle'                        => array(
                            'solid'                         => array(
                                'fields'                        => array( 'standard_button_border_width', 'standard_button_border_color', 'standard_button_border_color_h'),
                            ),
                            'dotted'                        => array(
                                'fields'                        => array( 'standard_button_border_width', 'standard_button_border_color', 'standard_button_border_color_h'),
                            ),
                            'dashed'                        => array(
                                'fields'                        => array( 'standard_button_border_width', 'standard_button_border_color', 'standard_button_border_color_h'),
                            ),
                        ),
                    ),
                    'standard_button_border_width'  => array(
                        'type'                          => 'unit',
                        'label'                         => __('Border Width', 'woopack'),
                        'description'                   => 'px',
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.woocommerce button.button, .woocommerce input.button',
                            'property'                      => 'border-width',
                            'unit'                          => 'px',
                        ),
                        'responsive'                    => array(
                            'placeholder'                   => array(
                                'default'                       => '',
                                'medium' 				        => '',
                                'responsive'			        => '',
                            ),
                        ),
                    ),
                    'standard_button_border_color'  => array(
                        'type'                          => 'color',
                        'label'                         => __('Border Color', 'woopack'),
                        'show_reset'                    => true,
                        'show_alpha'                    => true,
                        'default'                       => '',
                    ),
                    'standard_button_border_color_h'=> array(
                        'type'                          => 'color',
                        'label'                         => __('Border Color on Hover', 'woopack'),
                        'show_reset'                    => true,
                        'show_alpha'                    => true,
                        'default'                       => '',
                    ),
                    'standard_button_border_radius' => array(
                        'type'                          => 'unit',
                        'label'                         => __('Round Corners', 'woopack'),
                        'default'                       => '5',
                        'description'                   => 'px',
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.woocommerce button.button, .woocommerce input.button',
                            'property'                      => 'border-radius',
                            'unit'                          => 'px',
                            ),
                        'responsive'                    => array(
                            'placeholder'                   => array(
                            'default'                           => '',
                            'medium'                            => '',
                            'responsive'                        => '',
                            ),
                        ),
                    ),
                ),
            ),
            'checkout_button_str'   => array(
                'title'                 => __('Checkout Button Structure', 'woopack'),
                'fields'                => array(
                    'checkout_padding'      => array(
                        'type'                  => 'unit',
                        'label'                 => __('Padding', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.wc-proceed-to-checkout a.button.alt',
                            'property'              => 'padding',
                            'unit'                  => 'px'
                        ),
                        'responsive'            => array(
                            'placeholder'           => array(
                                'default' 			    => '',
                                'medium' 			    => '',
                                'responsive'            => '',
                            ),
                        ),
                    ),
                    'checkout_width'        => array(
                        'type'                  => 'select',
                        'label'                 => __('Width', 'woopack'),
                        'default'               => 'auto',
                        'options'               => array(
                            'auto'                  => __('Auto', 'woopack'),
                            'full_width'            => __('Full Width', 'woopack'),
                            'custom'                => __('Custom', 'woopack'),
                        ),
                        'toggle'                => array(
                            'custom'                => array(
                                'fields'                => array('checkout_width_custom')
                            ),
                        ),
                    ),
                    'checkout_width_custom' => array(
                        'type'                  => 'unit',
                        'label'                 => __('Custom Width', 'woopack'),
                        'description' 			=> '%',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive' 			=> '',
                            ),
                        ),
                        'preview'				=> array(
                            'type' 					=> 'css',
                            'selector'              => '.wc-proceed-to-checkout a.button.alt',
                            'property'      		=> 'width',
                            'unit'					=> '%',
                        ),
                        ),
                    'checkout_align'        => array(
                        'type'                  => 'select',
                        'label'                 => __('Alignment', 'woopack'),
                        'default'               => 'center',
                        'options'               => array(
                            'left'                  => __('Left', 'woopack'),
                            'center'                => __('Center', 'woopack'),
                            'right'                 => __('Right', 'woopack')
                        ),
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.wc-proceed-to-checkout',
                            'property'              => 'text-align',
                        ),
                    ),
                ),
            ),
            'checkout_button_color' => array(
                'title'                 => __('Checkout Button Colors', 'woopack'),
                'fields'                => array(
                    'checkout_bg_color'         => array(
                        'type'                      => 'color',
                        'label'                     => __('Background Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.wc-proceed-to-checkout a.button.alt',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'checkout_bg_color_hover'   => array(
                        'type'                      => 'color',
                        'label'                     => __('Background Hover Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.wc-proceed-to-checkout a.button.alt:hover',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'checkout_color'            => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.wc-proceed-to-checkout a.button.alt',
                            'property'                  => 'color',
                        ),
                    ),
                    'checkout_color_hover'      => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color On Hover', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.wc-proceed-to-checkout a.button.alt:hover',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'checkout_button_border'=> array(
                'title'                 => __('Checkout Button Border', 'woopack'),
                'fields'                => array(
                    'checkout_border_style'     => array(
                        'type'                      => 'select',
                        'label'                     => __('Border Style', 'woopack'),
                        'default'                   => 'none',
                        'options'                   => array(
                            'none'                      => __('None', 'woopack'),
                            'solid'                     => __('Solid', 'woopack'),
                            'dotted'                    => __('Dotted', 'woopack'),
                            'dashed'                    => __('Dashed', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'solid'                     => array(
                                'fields'                    => array( 'checkout_border_width', 'checkout_border_color', 'checkout_border_color_h'),
                            ),
                            'dotted'                    => array(
                                'fields'                    => array( 'checkout_border_width', 'checkout_border_color', 'checkout_border_color_h'),
                            ),
                            'dashed'                    => array(
                                'fields'                    => array( 'checkout_border_width', 'checkout_border_color', 'checkout_border_color_h'),
                            ),
                        ),
                    ),
                    'checkout_border_width'     => array(
                        'type'                      => 'unit',
                        'label' 			        => __('Border Width', 'woopack'),
                        'description' 		        => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'			        => '.wc-proceed-to-checkout a.button.alt',
                            'property'      	        => 'border-width',
                            'unit'				        => 'px',
                        ),
                        'responsive' 			    => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium' 				    => '',
                                'responsive'			    => '',
                            ),
                        ),
                    ),
                    'checkout_border_color'     => array(
                        'type'                      => 'color',
                        'label' 			        => __('Border Color', 'woopack'),
                        'show_reset' 		        => true,
                        'show_alpha'                => true,
                        'default'                   => '',
                    ),
                    'checkout_border_color_h'   => array(
                        'type'                      => 'color',
                        'label'                     => __('Border Color On Hover', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'default'                   => '',
                    ),
                    'checkout_border_radius'    => array(
                        'type'                      => 'unit',
                        'label' 			        => __('Round Corners', 'woopack'),
                        'default'                   => '5',
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.wc-proceed-to-checkout a.button.alt',
                            'property'                  => 'border-radius',
                            'unit'                      => 'px',
                        ),
                        'responsive' 			    => array(
                            'placeholder' 			    => array(
                                'default'                   => '',
                                'medium' 				    => '',
                                'responsive'			    => '',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'typography'        => array(
        'title'             => __('Typography', 'woopack'),
        'sections' 	        => array(
            'default_font'          => array(
                'title'                 => __('Default', 'woopack'),
                'fields'                => array(
                    'default_font'              => array(
                        'label'                     => __('Font', 'woopack'),
                        'type'                      => 'font',
                        'default' 		            => array(
                            'family'                    => 'Default',
                            'weight'                    => 300
                        ),
                        'preview'                   => array(
                            'type'                      => 'font',
                            'selector'                  => '.woocommerce, .cart_totals table.shop_table tbody th, .cart_totals table.shop_table tbody td',
                            'property'		            => 'font-family',
                        ),
                    ),
                    'default_font_size'         => array(
                        'type'                      => 'select',
                        'label'  			        => __('Font Size', 'woopack'),
                        'default'                   => 'default',
                        'options' 		            => array(
                            'default' 		            => __('Default', 'woopack'),
                            'custom'                    => __('Custom', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'custom'                    => array(
                                'fields'                    => array('default_font_size_custom')
                            ),
                        ),
                    ),
                    'default_font_size_custom'  => array(
                        'type'                      => 'unit',
                        'label'                     => __('Custom Font Size', 'woopack'),
                        'description'               => 'px',
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '15',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce, .cart_totals table.shop_table tbody th, .cart_totals table.shop_table tbody td',
                            'property'                  => 'font-size',
                            'unit'                      => 'px',
                        ),
                    ),
                    'default_color'             => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce, .cart_totals table.shop_table tbody th, .cart_totals table.shop_table tbody td',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'table_header_font'     => array(
                'title'                 => __('Table Header', 'woopack'),
                'fields'                => array(
                    'table_header_font'             => array(
                        'type'                          => 'font',
                        'default' 		                => array(
                            'family'                        => 'Default',
                            'weight'                        => 300
                        ),
                        'label'                     => __('Font', 'woopack'),
                        'preview'                       => array(
                            'type'                          => 'font',
                            'selector'                      => 'table.shop_table.cart thead th',
                            'property'		                => 'font-family',
                        ),
                    ),
                    'table_header_font_size'        => array(
                        'type'                          => 'select',
                        'label'  			            => __('Font Size', 'woopack'),
                        'default'                       => 'default',
                        'options' 		                => array(
                            'default' 		                => __('Default', 'woopack'),
                            'custom'                        => __('Custom', 'woopack'),
                        ),
                        'toggle'                        => array(
                            'custom'                        => array(
                                'fields'                        => array('table_header_font_size_custom')
                            ),
                        ),
                    ),
                    'table_header_font_size_custom' => array(
                        'type'                          => 'unit',
                        'label'                         => __('Custom Font Size', 'woopack'),
                        'description'                   => 'px',
                        'responsive'                    => array(
                            'placeholder'                   => array(
                                'default'                       => '15',
                                'medium'                        => '',
                                'responsive'                    => '',
                            ),
                        ),
                        'preview'                       => array(
                            'type' 					        => 'css',
                            'selector'                      => 'table.shop_table.cart thead th',
                            'property'                      => 'font-size',
                            'unit'                          => 'px',
                        ),
                    ),
                    'table_header_text_transform'   => array(
                        'type'                          => 'select',
                        'label' 		                => __('Text Transform', 'woopack'),
                        'default'                       => 'default',
                        'options'                       => array(
                            'default' 		                => __('Default', 'woopack'),
                            'capitalize'                    => __('Capitalize', 'woopack'),
                            'lowercase'                     => __('lowercase', 'woopack'),
                            'uppercase'                     => __('UPPERCASE', 'woopack'),
                        ),
                        'preview' 		                => array(
                            'type'                          => 'css',
                            'selector'                      => 'table.shop_table.cart thead th',
                            'property'                      => 'text-transform',
                        ),
                    ),
                    'table_header_color'            => array(
                        'type'                          => 'color',
                        'label'                         => __('Text Color', 'woopack'),
                        'default'                       => '',
                        'show_reset'                    => true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => 'table.shop_table.cart thead th',
                            'property'                      => 'color',
                        ),
                    ),
                ),
            ),
            'product_name_font'     => array(
                'title'                 => __('Product Name', 'woopack'),
                'fields'                => array(
                    'product_name_color'        => array(
                        'type'              => 'color',
                        'label'             => __('Text Color', 'woopack'),
                        'default'           => '',
                        'show_reset'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => 'table.shop_table.cart tbody td.product-name a',
                            'property'          => 'color',
                        ),
                    ),
                    'product_name_color_hover'  => array(
                        'type'              => 'color',
                        'label'             => __('Color on Hover', 'woopack'),
                        'default'           => '',
                        'show_reset'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => 'table.shop_table.cart tbody td.product-name a:hover',
                            'property'          => 'color',
                        ),
                    ),
                ),
            ),
            'cart_totals_title_font'=> array(
                'title'                 => __('Cart Totals Title', 'woopack'),
                'fields'                => array(
                    'cart_totals_title_font_family'     => array(
                        'type'                              => 'font',
                        'label'                             => __('Font', 'woopack'),
                        'default' 		                    => array(
                            'family'                            => 'Default',
                            'weight'                            => 300
                        ),
                        'preview'                           => array(
                            'type'                              => 'font',
                            'selector'                          => '.cart_totals h2',
                            'property'		                    => 'font-family',
                        ),
                    ),
                    'cart_totals_title_font_size'       => array(
                        'type'                              => 'select',
                        'label'  			                => __('Font Size', 'woopack'),
                        'default'                           => 'default',
                        'options' 		                    => array(
                            'default' 		                    => __('Default', 'woopack'),
                            'custom'                            => __('Custom', 'woopack'),
                        ),
                        'toggle'                            => array(
                            'custom'                            => array(
                                'fields'                            => array('cart_totals_title_font_size_custom')
                            ),
                        ),
                    ),
                    'cart_totals_title_font_size_custom'=> array(
                        'type'                              => 'unit',
                        'label'                             => __('Custom Font Size', 'woopack'),
                        'description'                       => 'px',
                        'responsive'                        => array(
                            'placeholder'                       => array(
                                'default'                       => '15',
                                'medium'                        => '',
                                'responsive'                    => '',
                            ),
                        ),
                        'preview'                           => array(
                            'type' 					            => 'css',
                            'selector'                          => '.cart_totals h2',
                            'property'                          => 'font-size',
                            'unit'                              => 'px',
                        ),
                    ),
                    'cart_totals_text_transform'    => array(
                        'type'                              => 'select',
                        'label'                             => __('Text Transform', 'woopack'),
                        'default'                           => 'default',
                        'options'                           => array(
                            'default' 		                    => __('Default', 'woopack'),
                            'capitalize'                        => __('Capitalize', 'woopack'),
                            'lowercase'                         => __('lowercase', 'woopack'),
                            'uppercase'                         => __('UPPERCASE', 'woopack'),
                        ),
                        'preview' 		                    => array(
                            'type'                              => 'css',
                            'selector'                          => '.cart_totals h2',
                            'property'                          => 'text-transform',
                        ),
                    ),
                    'cart_totals_title_color'           => array(
                        'type'                              => 'color',
                        'label'                             => __('Text Color', 'woopack'),
                        'default'                           => '',
                        'show_reset'                        => true,
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '.cart_totals h2',
                            'property'                          => 'color',
                        ),
                    ),
                ),
            ),
            'standard_button_font'  => array(
                'title'                 => __('Standard Button', 'woopack'),
                'fields'                => array(
                    'standard_button_font_family'       => array(
                        'type'                              => 'font',
                        'default' 		                    => array(
                            'family'                            => 'Default',
                            'weight'                            => 300
                        ),
                        'label'                             => __('Font', 'woopack'),
                        'preview'                               => array(
                            'type'                                  => 'font',
                            'selector'                              => '.woocommerce button.button, .woocommerce input.button',
                            'property'		                        => 'font-family',
                        ),
                    ),
                    'standard_button_font_size'         => array(
                        'type'                              => 'select',
                        'label'  			                => __('Font Size', 'woopack'),
                        'default'                           => 'default',
                        'options' 		                    => array(
                            'default' 		                    => __('Default', 'woopack'),
                            'custom'                            => __('Custom', 'woopack'),
                        ),
                        'toggle'                            => array(
                            'custom'                            => array(
                                'fields'                            => array('standard_button_font_size_custom')
                            ),
                        ),
                    ),
                    'standard_button_font_size_custom'  => array(
                        'type'                              => 'unit',
                        'label'                             => __('Custom Font Size', 'woopack'),
                        'description'                       => 'px',
                        'responsive'                        => array(
                            'placeholder'                       => array(
                                'default'                       => '15',
                                'medium'                        => '',
                                'responsive'                    => '',
                            ),
                        ),
                        'preview'                           => array(
                            'type' 					            => 'css',
                            'selector'                          => '.woocommerce button.button, .woocommerce input.button',
                            'property'                          => 'font-size',
                            'unit'                              => 'px',
                        ),
                    ),
                    'standard_button_text_transform'    => array(
                        'type'                              => 'select',
                        'label'                             => __('Text Transform', 'woopack'),
                        'default'                           => 'default',
                        'options'                           => array(
                            'default' 		                => __('Default', 'woopack'),
                            'capitalize'                    => __('Capitalize', 'woopack'),
                            'lowercase'                     => __('lowercase', 'woopack'),
                            'uppercase'                     => __('UPPERCASE', 'woopack'),
                        ),
                        'preview' 		                    => array(
                            'type'                              => 'css',
                            'selector'                          => '.woocommerce button.button, .woocommerce input.button',
                            'property'                          => 'text-transform',
                        ),
                    ),
                ),
            ),
            'checkout_button_font'  => array(
                'title'                 => __('Checkout Button', 'woopack'),
                'fields'                => array(
                    'checkout_button_font_family'       => array(
                        'type'                              => 'font',
                        'label'                             => __('Font', 'woopack'),
                        'default' 		                    => array(
                            'family'                            => 'Default',
                            'weight'                            => 300
                        ),
                        'preview'                           => array(
                            'type'                              => 'font',
                            'selector'                          => '.wc-proceed-to-checkout a.button.alt',
                            'property'		                    => 'font-family',
                        ),
                    ),
                    'checkout_button_font_size'         => array(
                        'type'                              => 'select',
                        'label'  			                => __('Font Size', 'woopack'),
                        'default'                           => 'default',
                        'options' 		                    => array(
                            'default' 		                    => __('Default', 'woopack'),
                            'custom'                            => __('Custom', 'woopack'),
                        ),
                        'toggle'                            => array(
                            'custom'                            => array(
                                'fields'                            => array('checkout_button_font_size_custom')
                            ),
                        ),
                    ),
                    'checkout_button_font_size_custom'  => array(
                        'type'                              => 'unit',
                        'label'                             => __('Custom Font Size', 'woopack'),
                        'description'                       => 'px',
                        'responsive'                        => array(
                            'placeholder'                       => array(
                                'default'                       => '15',
                                'medium'                        => '',
                                'responsive'                    => '',
                            ),
                        ),
                        'preview'                           => array(
                            'type' 					            => 'css',
                            'selector'                          => '.wc-proceed-to-checkout a.button.alt',
                            'property'                          => 'font-size',
                            'unit'                              => 'px',
                        ),
                    ),
                    'checkout_button_text_transform'    => array(
                        'type'                              => 'select',
                        'label'                             => __('Text Transform', 'woopack'),
                        'default'                           => 'default',
                        'options'                           => array(
                            'default' 		                    => __('Default', 'woopack'),
                            'capitalize'                        => __('Capitalize', 'woopack'),
                            'lowercase'                         => __('lowercase', 'woopack'),
                            'uppercase'                         => __('UPPERCASE', 'woopack'),
                        ),
                        'preview' 		                    => array(
                            'type'                              => 'css',
                            'selector'                          => '.wc-proceed-to-checkout a.button.alt',
                            'property'                          => 'text-transform',
                        ),
                    ),
                ),
            ),
        ),
    ),
));
