.fl-node-<?php echo $id; ?> .woocommerce table.cart th.product-quantity{
	text-align: left;
}
.fl-node-<?php echo $id; ?> .woocommerce,
.fl-node-<?php echo $id; ?> .cart_totals table.shop_table tbody th,
.fl-node-<?php echo $id; ?> .cart_totals table.shop_table tbody td {
	<?php WooPack_Helper::print_css( 'color', $settings->default_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->default_font ); ?>
	<?php if ( 'custom' == $settings->default_font_size && '' != $settings->default_font_size_custom ) { ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->default_font_size_custom, 'px' ); ?>
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.shop_table.cart {
	<?php WooPack_Helper::print_css( 'background-color', $settings->table_bg_color ); ?>
	<?php if ( 'none' != $settings->table_border_style ) { ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->table_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->table_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->table_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>

	<?php WooPack_Helper::print_css( 'border-radius', $settings->table_border_radius, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->table_padding_t_b, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_padding_t_b, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->table_padding_r_l, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->table_padding_r_l, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.shop_table.cart thead th {
	<?php WooPack_Helper::print_css( 'padding-top', $settings->table_header_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_header_padding, 'px' ); ?>

	<?php if ( '' != $settings->table_header_border_width ) { ?>
		<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->table_header_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-bottom-style', 'solid' ); ?>
	<?php } ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->table_header_border_color, ' !important' ); ?>

	<?php WooPack_Helper::print_css( 'color', $settings->table_header_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->table_header_font ); ?>

	<?php if ( 'custom' == $settings->table_header_font_size && '' != $settings->table_header_font_size_custom ) { ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->table_header_font_size_custom, 'px' ); ?>
	<?php } ?>

	<?php WooPack_Helper::print_css( 'text-transform', $settings->table_header_text_transform ); ?>
}
.fl-node-<?php echo $id; ?> tr.woocommerce-cart-form__cart-item.cart_item:nth-child(even) {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_item_even_color ); ?>
}
.fl-node-<?php echo $id; ?> tr.woocommerce-cart-form__cart-item.cart_item:nth-child(odd) {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_item_odd_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.shop_table.cart tbody tr:first-child td {
	border-top-width: 0!important;
}
.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.shop_table.cart td {
	<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_item_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_item_padding, 'px' ); ?>

	<?php if ( '' != $settings->cart_item_border_width ) { ?>
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_item_border_width, 'px' ); ?>
		border-top-style: solid;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->cart_item_border_color, ' !important' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce table.cart .product-thumbnail img {
	<?php WooPack_Helper::print_css( 'width', $settings->image_width, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.shop_table.cart td.product-remove a {
	<?php WooPack_Helper::print_css( 'color', $settings->product_remove_color, ' !important' ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->product_remove_font_size_custom, 'px', 'custom' == $settings->product_remove_font_size ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.shop_table.cart td.product-remove a:hover {
	<?php WooPack_Helper::print_css( 'color', $settings->product_remove_color_hover, ' !important' ); ?>
}
.fl-node-<?php echo $id; ?> table.shop_table.cart tbody td.product-name a {
	<?php WooPack_Helper::print_css( 'color', $settings->product_name_color ); ?>
	text-decoration: none;
}
.fl-node-<?php echo $id; ?> table.shop_table.cart tbody td.product-name a:hover {
	<?php WooPack_Helper::print_css( 'color', $settings->product_name_color_hover ); ?>
}
<?php if ( 'yes' == $settings->show_coupon ) { ?>
	.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.cart td.actions .coupon .input-text {
		<?php WooPack_Helper::print_css( 'color', $settings->coupon_text_color ); ?>
		margin: 0 4px 0 0;
	}
	.fl-node-<?php echo $id; ?> .woocommerce button.button,
	.fl-node-<?php echo $id; ?> .woocommerce input.button {
		<?php WooPack_Helper::print_css( 'background-color', $settings->standard_button_bg_color ); ?>
		<?php WooPack_Helper::print_css( 'color', $settings->standard_button_color, ' !important' ); ?>

		<?php if ( 'none' != $settings->standard_button_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->standard_button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->standard_button_border_width, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->standard_button_border_color ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->standard_button_border_radius, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'font', $settings->standard_button_font_family ); ?>

		<?php WooPack_Helper::print_css( 'font-size', $settings->standard_button_font_size_custom, 'px', 'custom' == $settings->standard_button_font_size ); ?>

		line-height: 1;
		<?php WooPack_Helper::print_css( 'text-transform', $settings->standard_button_text_transform ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce button.button:hover,
	.fl-node-<?php echo $id; ?> .woocommerce input.button:hover {
		<?php if ( 'none' != $settings->standard_button_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->standard_button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->standard_button_border_color_h ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>

		<?php WooPack_Helper::print_css( 'background-color', $settings->standard_button_bg_color_hover ); ?>
		<?php WooPack_Helper::print_css( 'color', $settings->standard_button_color_hover, ' !important' ); ?>
	}
<?php } // End if(). ?>
.fl-node-<?php echo $id; ?> .cart_totals table.shop_table {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_total_bg_color ); ?>

	<?php if ( 'none' != $settings->cart_total_border_style ) { ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->cart_total_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->cart_total_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->cart_total_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>

	<?php WooPack_Helper::print_css( 'border-radius', $settings->cart_total_border_radius, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_total_padding_t_b, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_total_padding_t_b, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->cart_total_padding_r_l, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->cart_total_padding_r_l, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .cart_totals tr.order-total th,
.fl-node-<?php echo $id; ?> .cart_totals tr.order-total td {
	border-bottom: none !important;
}
.fl-node-<?php echo $id; ?> .cart_totals table.shop_table tbody th,
.fl-node-<?php echo $id; ?> .cart_totals table.shop_table tbody td {
	<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_total_separator_width, 'px !important' ); ?>
	<?php WooPack_Helper::print_css( 'border-top-style', 'solid' ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->cart_total_separator_color, ' !important' ); ?>
	<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_total_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_total_padding, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .cart_totals h2 {
	<?php WooPack_Helper::print_css( 'color', $settings->cart_totals_title_color, ' !important' ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->cart_totals_title_font_family ); ?>
	<?php if ( 'custom' == $settings->cart_totals_title_font_size ) { ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->cart_totals_title_font_size_custom, 'px !important', '' != $settings->cart_totals_title_font_size_custom ); ?>
	<?php } ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->cart_totals_text_transform, '', 'default' != $settings->cart_totals_text_transform ); ?>
}
.fl-node-<?php echo $id; ?> .wc-proceed-to-checkout {
	<?php WooPack_Helper::print_css( 'text-align', $settings->checkout_align ); ?>
}
.fl-node-<?php echo $id; ?> .wc-proceed-to-checkout a.button.alt {
	<?php WooPack_Helper::print_css( 'background-color', $settings->checkout_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->checkout_color ); ?>
	<?php WooPack_Helper::print_css( 'padding', $settings->checkout_padding, 'px' ); ?>

	<?php if ( 'full_width' == $settings->checkout_width ) { ?>
		width: 100%;
	<?php } elseif ( 'custom' == $settings->checkout_width ) { ?>
		width: <?php echo $settings->checkout_width_custom; ?>%;
	<?php } ?>

	text-align: center;

	<?php if ( 'none' != $settings->checkout_border_style ) { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->checkout_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->checkout_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->checkout_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'border-radius', $settings->checkout_border_radius, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'font', $settings->checkout_button_font_family ); ?>
	<?php if ( 'custom' == $settings->checkout_button_font_size && '' != $settings->checkout_button_font_size_custom ) { ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->checkout_button_font_size_custom, 'px', '' != $settings->checkout_button_font_size_custom ); ?>
	<?php } ?>

	<?php WooPack_Helper::print_css( 'text-transform', $settings->checkout_button_text_transform, '', 'default' != $settings->checkout_button_text_transform ); ?>
}
.fl-node-<?php echo $id; ?> .wc-proceed-to-checkout a.button.alt:hover {
	<?php if ( 'none' != $settings->checkout_border_style ) { ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->checkout_border_color_h ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->checkout_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->checkout_color_hover ); ?>
}

<?php
// *********************
// Media Query
// *********************
?>
@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce,
	.fl-node-<?php echo $id; ?> .cart_totals table.shop_table tbody th,
	.fl-node-<?php echo $id; ?> .cart_totals table.shop_table tbody td {
		<?php if ( 'custom' == $settings->default_font_size && '' != $settings->default_font_size_custom_medium ) { ?>
			<?php WooPack_Helper::print_css( 'font-size', $settings->checkout_button_font_size_custom_medium, 'px' ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.shop_table.cart {
		<?php if ( 'none' != $settings->table_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->table_border_width_medium, 'px' ); ?>
		<?php } ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->table_border_radius_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-top', $settings->table_padding_t_b_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_padding_t_b_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->table_padding_r_l_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->table_padding_r_l_medium, 'px' ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.shop_table.cart thead th {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->table_header_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_header_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->table_header_border_width_medium, 'px' ); ?>

		<?php if ( 'custom' == $settings->table_header_font_size && '' != $settings->table_header_font_size_custom_medium ) { ?>
			<?php WooPack_Helper::print_css( 'font-size', $settings->table_header_font_size_custom_medium, 'px' ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.shop_table.cart td {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_item_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_item_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_item_border_width_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce table.cart .product-thumbnail img {
		<?php WooPack_Helper::print_css( 'width', $settings->image_width_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.shop_table.cart td.product-remove a {
		<?php if ( 'custom' == $settings->product_remove_font_size && '' != $settings->product_remove_font_size_custom_medium ) { ?>
			<?php WooPack_Helper::print_css( 'font-size', $settings->product_remove_font_size_custom_medium, 'px' ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .cart_totals table.shop_table {
		<?php WooPack_Helper::print_css( 'border-width', $settings->cart_total_border_width_medium, 'px', 'none' != $settings->cart_total_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->cart_total_border_radius_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_total_padding_t_b_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_total_padding_t_b_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->cart_total_padding_r_l_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->cart_total_padding_r_l_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .cart_totals table.shop_table tbody th,
	.fl-node-<?php echo $id; ?> .cart_totals table.shop_table tbody td {
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_total_separator_width_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_total_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_total_padding_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce button.button,
	.fl-node-<?php echo $id; ?> .woocommerce input.button {
		<?php WooPack_Helper::print_css( 'border-width', $settings->standard_button_border_width_medium, 'px', 'none' != $settings->standard_button_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->standard_button_border_radius_medium, 'px' ); ?>
		<?php if ( 'custom' == $settings->standard_button_font_size && '' != $settings->standard_button_font_size_custom_medium ) { ?>
			<?php WooPack_Helper::print_css( 'font-size', $settings->standard_button_font_size_custom_medium, 'px' ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .wc-proceed-to-checkout a.button.alt {
		<?php WooPack_Helper::print_css( 'padding', $settings->checkout_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'width', '100', '%', 'full_width' == $settings->checkout_width ); ?>
		<?php WooPack_Helper::print_css( 'width', $settings->checkout_width_custom_medium, '%', 'custom' == $settings->checkout_width ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->checkout_border_width_medium, 'px', 'none' != $settings->checkout_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->checkout_border_radius_medium, 'px' ); ?>

		<?php if ( 'custom' == $settings->checkout_button_font_size && '' != $settings->checkout_button_font_size_custom_medium ) { ?>
			<?php WooPack_Helper::print_css( 'font-size', $settings->checkout_button_font_size_custom_medium, 'px' ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .cart_totals h2 {
		<?php if ( 'custom' == $settings->cart_totals_title_font_size && '' != $settings->cart_totals_title_font_size_custom_medium ) { ?>
			<?php WooPack_Helper::print_css( 'font-size', $settings->cart_totals_title_font_size_custom_medium, 'px !important' ); ?>
		<?php } ?>
	}
}
@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce,
	.fl-node-<?php echo $id; ?> .cart_totals table.shop_table tbody th,
	.fl-node-<?php echo $id; ?> .cart_totals table.shop_table tbody td {
		<?php if ( 'custom' == $settings->default_font_size && '' != $settings->default_font_size_custom_responsive ) { ?>
			<?php WooPack_Helper::print_css( 'font-size', $settings->default_font_size_custom_responsive, 'px' ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.shop_table.cart {
		<?php if ( 'none' != $settings->table_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->table_border_width_responsive, 'px' ); ?>
		<?php } ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->table_border_radius_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-top', $settings->table_padding_t_b_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_padding_t_b_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->table_padding_r_l_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->table_padding_r_l_responsive, 'px' ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.shop_table.cart thead th {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->table_header_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_header_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->table_header_border_width_responsive, 'px' ); ?>

		<?php if ( 'custom' == $settings->table_header_font_size && '' != $settings->table_header_font_size_custom_responsive ) { ?>
			<?php WooPack_Helper::print_css( 'font-size', $settings->table_header_font_size_custom_responsive, 'px' ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.shop_table.cart td {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_item_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_item_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_item_border_width_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce table.cart .product-thumbnail img {
		<?php WooPack_Helper::print_css( 'width', $settings->image_width_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce-cart-form table.shop_table.cart td.product-remove a {
		<?php if ( 'custom' == $settings->product_remove_font_size && '' != $settings->product_remove_font_size_custom_responsive ) { ?>
			<?php WooPack_Helper::print_css( 'font-size', $settings->product_remove_font_size_custom_responsive, 'px' ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .cart_totals table.shop_table {
		<?php WooPack_Helper::print_css( 'border-width', $settings->cart_total_border_width_responsive, 'px', 'none' != $settings->cart_total_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->cart_total_border_radius_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_total_padding_t_b_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_total_padding_t_b_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->cart_total_padding_r_l_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->cart_total_padding_r_l_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .cart_totals table.shop_table tbody th,
	.fl-node-<?php echo $id; ?> .cart_totals table.shop_table tbody td {
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_total_separator_width_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_total_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_total_padding_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce button.button,
	.fl-node-<?php echo $id; ?> .woocommerce input.button {
		<?php WooPack_Helper::print_css( 'border-width', $settings->standard_button_border_width_responsive, 'px', 'none' != $settings->standard_button_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->standard_button_border_radius_responsive, 'px' ); ?>
		<?php if ( 'custom' == $settings->standard_button_font_size && '' != $settings->standard_button_font_size_custom_responsive ) { ?>
			<?php WooPack_Helper::print_css( 'font-size', $settings->standard_button_font_size_custom_responsive, 'px' ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .wc-proceed-to-checkout a.button.alt {
		<?php WooPack_Helper::print_css( 'padding', $settings->checkout_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'width', '100', '%', 'full_width' == $settings->checkout_width ); ?>
		<?php WooPack_Helper::print_css( 'width', $settings->checkout_width_custom_responsive, '%', 'custom' == $settings->checkout_width ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->checkout_border_width_responsive, 'px', 'none' != $settings->checkout_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->checkout_border_radius_responsive, 'px' ); ?>

		<?php if ( 'custom' == $settings->checkout_button_font_size && '' != $settings->checkout_button_font_size_custom_responsive ) { ?>
			<?php WooPack_Helper::print_css( 'font-size', $settings->checkout_button_font_size_custom_responsive, 'px' ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .cart_totals h2 {
		<?php if ( 'custom' == $settings->cart_totals_title_font_size && '' != $settings->cart_totals_title_font_size_custom_responsive ) { ?>
			<?php WooPack_Helper::print_css( 'font-size', $settings->cart_totals_title_font_size_custom_responsive, 'px !important' ); ?>
		<?php } ?>
	}
}
