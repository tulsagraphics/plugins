<?php
/**
* @class WooPackCheckout
*/
class WooPackCheckout extends FLBuilderModule {
    /**
    * Constructor function for the module. You must pass the
    * name, description, dir and url in an array to the parent class.
    *
    * @method __construct
    */
    public function __construct()
    {
        parent::__construct(array(
            'name' 				=> __('Checkout', 'woopack'),
            'description' 		=> __('Addon to display Checkout.', 'woopack'),
            'group'             => WooPack_Helper::get_modules_group(),
            'category' 			=> WOOPACK_CAT,
            'dir' 				=> WOOPACK_DIR . 'modules/checkout/',
            'url' 				=> WOOPACK_URL . 'modules/checkout/',
            'editor_export' 	=> true, // Defaults to true and can be omitted.
            'enabled' 			=> true, // Defaults to true and can be omitted.
        ));
    }
}
/**
* Register the module and its form settings.
*/
FLBuilder::register_module('WooPackCheckout', array(
    'checkout_sections' => array(
        'title'             => __('Sections', 'woopack'),
        'sections'          => array(
            'layout'            => array(
                'title'             => __('Layout', 'woopack'),
                'fields'            => array(
                    'checkout_layout'       => array(
                        'type'                  => 'select',
                        'label'                 => __('Layout', 'woopack'),
                        'default'               => '1column',
                        'options'               => array(
                            '1column'               => __('One Column', 'woopack'),
                            '2column'               => __('Two Column', 'woopack'),
                        ),
                        'toggle'                => array(
							'2column'                => array(
                                'fields'                    => array( 'first_column_width', 'second_column_width', 'space_bw_columns'),
							),
						),
                    ),
                    'first_column_width'    => array(
                        'type'                  => 'unit',
                        'label' 			    => __('First Column Width', 'woopack'),
                        'description'           => '%',
                        'default'               => '50',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '50',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'second_column_width'   => array(
                        'type'                  => 'unit',
                        'label' 			    => __('Second Column Width', 'woopack'),
                        'description'           => '%',
                        'default'               => '48',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '48',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'space_bw_columns'      => array(
                        'type'                  => 'unit',
                        'label' 			    => __('Space Between Columns', 'woopack'),
                        'description'           => '%',
                        'default'               => '2',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '2',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                ),
            ),
            'form_color'        => array(
                'title'             => __('Background', 'woopack'),
                'fields'            => array(
                    'form_bg_color'     => array(
                        'type'              => 'color',
                        'label'             => __('Background Color', 'woopack'),
                        'default'           => '',
                        'show_reset'        => true,
                        'show_alpha'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '#customer_details, table.shop_table, #payment, .woocommerce-info',
                            'property'          => 'background-color',
                        ),
                    ),
                ),
            ),
            'form_border'       => array(
                'title'             => __('Border', 'woopack'),
                'fields'            => array(
                    'form_border_style'    => array(
						'type'                => 'select',
						'label'               => __('Border Style', 'woopack'),
						'default'             => 'none',
						'options'             => array(
                            'default'            => __('Default', 'woopack'),
							'none'               => __('None', 'woopack'),
							'solid'              => __('Solid', 'woopack'),
							'dotted'             => __('Dotted', 'woopack'),
							'dashed'             => __('Dashed', 'woopack'),
						),
						'toggle'            => array(
							'solid'              => array(
                                'fields'                => array( 'form_border_width', 'form_border_color'),
							),
							'dotted'             => array(
                                'fields'                => array( 'form_border_width', 'form_border_color'),
							),
							'dashed'             => array(
                                'fields'                => array( 'form_border_width', 'form_border_color'),
							),
						),
					),
                    'form_border_width'    => array(
                        'type'                  => 'unit',
                        'label' 			    => __('Border Width', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '#customer_details, table.shop_table, #payment, .woocommerce-info',
                            'property'              => 'border-width',
                            'unit'				    => 'px',
                        ),
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'form_border_color'    => array(
                        'type'                  => 'color',
                        'label' 			    => __('Border Color', 'woopack'),
                        'show_reset' 		    => true,
                        'show_alpha' 		    => true,
                        'preview' 			    => array(
                            'type' 				    => 'css',
                            'selector'              => '#customer_details, table.shop_table, #payment, .woocommerce-info',
                            'property'              => 'border-color',
                        ),
                    ),
                    'form_border_radius'   => array(
                        'type' 				    => 'unit',
                        'label' 		        => __('Round Corners', 'woopack'),
                        'default' 			    => '5',
                        'description' 		    => 'px',
                        'preview' 			    => array(
                            'type' 				    => 'css',
                            'selector'              => '#customer_details, table.shop_table, #payment, .woocommerce-info',
                            'property'      	    => 'border-radius',
                            'unit'				    => 'px',
                        ),
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                ),
            ),
            'form_shadow'       => array(
                'title'             => __('Shadow', 'woopack'),
                'fields'        	=> array(
                    'form_shadow'    		=> array(
                        'type'          		=> 'select',
                        'label'         		=> __('Enable Shadow?', 'woopack'),
                        'default'       		=> 'no',
                        'options'       		=> array(
                            'yes'           		=> __('Yes', 'woopack'),
                            'no'            		=> __('No', 'woopack'),
                        ),
                        'toggle'        		=> array(
                            'yes'           		=> array(
                                'fields'        		=> array('form_shadow_h', 'form_shadow_v', 'form_shadow_blur', 'form_shadow_spread', 'form_shadow_color'),
                            ),
                        ),
                    ),
                    'form_shadow_h'  		=> array(
                        'type'          		=> 'unit',
                        'label'         		=> __('Horizontal', 'woopack'),
                        'default' 				=> '0',
                        'description' 			=> 'px',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'form_shadow_v'  		=> array(
                        'type'          		=> 'unit',
                        'label'         		=> __('Vertical', 'woopack'),
                        'default'       		=> '0',
                        'description' 			=> 'px',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'form_shadow_blur'		=> array(
                        'type'          		=> 'unit',
                        'label'         		=> __('Blur', 'woopack'),
                        'default'       		=> '0',
                        'description' 			=> 'px',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'form_shadow_spread'    => array(
                        'type'          		=> 'unit',
                        'label'         		=> __('Spread', 'woopack'),
                        'default'       		=> '0',
                        'description' 			=> 'px',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'form_shadow_color' 	=> array(
                        'type'              	=> 'color',
                        'label'             	=> __('Color', 'woopack'),
                        'show_reset'        	=> true,
                        'show_alpha'        	=> true,
                        'default'           	=> '000000',
                    ),
                ),
            ),
            'form_padding'      => array(
                'title'             => __('Padding', 'woopack'),
                'fields'        	=> array(
                    'form_padding_top_bottom'=> array(
                        'type'                  => 'unit',
                        'label'                 => __('Top & Bottom Padding', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'rules' 	            => array(
                                array(
                                    'selector'          => '#customer_details, table.shop_table, #payment, .woocommerce-info',
                                    'property'          => 'padding-top',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'          => '#customer_details, table.shop_table, #payment, .woocommerce-info',
                                    'property'          => 'padding-bottom',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'            => array(
                            'placeholder' 		    => array(
                                'default'           => '',
                                'medium' 			=> '',
                                'responsive'        => '',
                            ),
                        ),
                    ),
                    'form_padding_right_left'=> array(
                        'type'                  => 'unit',
                        'label'                 => __('Right & Left Padding', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'rules' 	            => array(
                                array(
                                    'selector'          => '#customer_details, table.shop_table, #payment, .woocommerce-info',
                                    'property'          => 'padding-right',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => '#customer_details, table.shop_table, #payment, .woocommerce-info',
                                    'property'          => 'padding-left',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'            => array(
                            'placeholder'           => array(
                                'default'               => '',
                                'medium'                => '',
                                'responsive'            => '',
                            ),
                        ),
                    ),
                ),
            ),
            'form_spacing'      => array(
                'title'             => __('Spacing', 'woopack'),
                'fields'        	=> array(
                    'form_space'        => array(
                        'type'                  => 'unit',
                        'label'                 => __('Space Between Sections', 'woopack'),
                        'description'           => 'px',
                        'responsive'            => array(
                            'placeholder' 		    => array(
                                'default'           => '',
                                'medium' 			=> '',
                                'responsive'        => '',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'inputs'            => array(
        'title'             => __('Inputs', 'woopack'),
        'sections'          => array(
            'input_color'       => array(
                'title'             => __('Color', 'woopack'),
                'fields'            => array(
                    'input_text_color'  => array(
                        'type'              => 'color',
                        'label'             => __('Text Color', 'woopack'),
                        'show_reset'        => true,
                        'show_alpha'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => 'form .form-row input.input-text, form .form-row textarea, form .form-row select',
                            'property'          => 'color',
                        ),
                    ),
                    'input_bg_color'  => array(
                        'type'              => 'color',
                        'label'             => __('Background Color', 'woopack'),
                        'show_reset'        => true,
                        'show_alpha'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => 'form .form-row input.input-text, form .form-row textarea, form .form-row select',
                            'property'          => 'background-color',
                        ),
                    ),
                    'placeholder_color'=> array(
                        'type'              => 'color',
                        'label'             => __('Placeholder Color', 'woopack'),
                        'show_reset'        => true,
                        'show_alpha'        => true,
                    ),
                ),
            ),
            'input_border'      => array(
                'title'             => __('Border', 'woopack'),
                'fields'            => array(
                    'input_border_style'    => array(
                        'type'                => 'select',
                        'label'               => __('Border Style', 'woopack'),
                        'default'             => 'none',
                        'options'             => array(
                            'default'            => __('Default', 'woopack'),
                            'none'               => __('None', 'woopack'),
                            'solid'              => __('Solid', 'woopack'),
                            'dotted'             => __('Dotted', 'woopack'),
                            'dashed'             => __('Dashed', 'woopack'),
                        ),
                        'toggle'            => array(
                            'solid'              => array(
                                'fields'                => array( 'input_border_width', 'input_border_color', 'input_border_focus', 'input_border_invalid'),
                            ),
                            'dotted'             => array(
                                'fields'                => array( 'input_border_width', 'input_border_color', 'input_border_focus', 'input_border_invalid'),
                            ),
                            'dashed'             => array(
                                'fields'                => array( 'input_border_width', 'input_border_color', 'input_border_focus', 'input_border_invalid'),
                            ),
                        ),
                    ),
                    'input_border_width'    => array(
                        'type'                  => 'unit',
                        'label' 			    => __('Border Width', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => 'form .form-row input.input-text, form .form-row textarea, form .form-row select',
                            'property'              => 'border-width',
                            'unit'				    => 'px',
                        ),
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'input_border_color'    => array(
                        'type'                  => 'color',
                        'label' 			    => __('Border Color', 'woopack'),
                        'show_reset' 		    => true,
                        'show_alpha' 		    => true,
                        'preview' 			    => array(
                            'type' 				    => 'css',
                            'selector'              => 'form .form-row input.input-text, form .form-row textarea, form .form-row select',
                            'property'              => 'border-color',
                        ),
                    ),
                    'input_border_focus'    => array(
                        'type'                  => 'color',
                        'label' 			    => __('Border Focus Color', 'woopack'),
                        'show_reset' 		    => true,
                        'show_alpha' 		    => true,
                        'preview' 			    => array(
                            'type' 				    => 'css',
                            'selector'              => 'form .form-row input.input-text:focus',
                            'property'              => 'border-color',
                        ),
                    ),
                    'input_border_invalid'  => array(
                        'type'                  => 'color',
                        'label' 			    => __('Border Invalid Color', 'woopack'),
                        'show_reset' 		    => true,
                        'show_alpha' 		    => true,
                        'preview' 			    => array(
                            'type' 				    => 'css',
                            'selector'              => '',
                            'property'              => 'border-color',
                        ),
                    ),
                    'input_border_radius'   => array(
                        'type' 				    => 'unit',
                        'label' 		        => __('Round Corners', 'woopack'),
                        'description' 		    => 'px',
                        'preview' 			    => array(
                            'type' 				    => 'css',
                            'selector'              => 'form .form-row input.input-text, form .form-row textarea, form .form-row select',
                            'property'      	    => 'border-radius',
                            'unit'				    => 'px',
                        ),
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                ),
            ),
            'inpur_shadow'      => array(
                'title'             => __('Shadow', 'woopack'),
                'fields'        	=> array(
                    'inpur_shadow'          => array(
                        'type'          		=> 'select',
                        'label'         		=> __('Enable Shadow?', 'woopack'),
                        'default'       		=> 'no',
                        'options'       		=> array(
                            'yes'           		=> __('Yes', 'woopack'),
                            'no'            		=> __('No', 'woopack'),
                        ),
                        'toggle'        		=> array(
                            'yes'           		=> array(
                                'fields'        		=> array('inpur_shadow_h', 'inpur_shadow_v', 'inpur_shadow_blur', 'inpur_shadow_spread', 'inpur_shadow_color', 'inpur_shadow_direction'),
                            ),
                        ),
                    ),
                    'inpur_shadow_direction'=> array(
                        'type'          		=> 'select',
                        'label'         		=> __('Shadow Direction', 'woopack'),
                        'default'       		=> 'out',
                        'options'       		=> array(
                            'out'           		=> __('Outside', 'woopack'),
                            'inset'            		=> __('Inside', 'woopack'),
                        ),
                    ),
                    'inpur_shadow_h'  		=> array(
                        'type'          		=> 'unit',
                        'label'         		=> __('Horizontal', 'woopack'),
                        'default' 				=> '0',
                        'description' 			=> 'px',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'inpur_shadow_v'  		=> array(
                        'type'          		=> 'unit',
                        'label'         		=> __('Vertical', 'woopack'),
                        'default'       		=> '0',
                        'description' 			=> 'px',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'inpur_shadow_blur'		=> array(
                        'type'          		=> 'unit',
                        'label'         		=> __('Blur', 'woopack'),
                        'default'       		=> '0',
                        'description' 			=> 'px',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'inpur_shadow_spread'    => array(
                        'type'          		=> 'unit',
                        'label'         		=> __('Spread', 'woopack'),
                        'default'       		=> '0',
                        'description' 			=> 'px',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive'			=> '',
                            ),
                        ),
                    ),
                    'inpur_shadow_color' 	=> array(
                        'type'              	=> 'color',
                        'label'             	=> __('Color', 'woopack'),
                        'show_reset'        	=> true,
                        'show_alpha'        	=> true,
                    ),
                ),
            ),
            'input_alignment'   => array(
                'title'             => __('Size & Alignment', 'woopack'),
                'fields'        	=> array(
                    'input_height'      => array(
                        'type'                  => 'unit',
                        'label'                 => __('Input Height', 'woopack'),
                        'default'               => '34',
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => 'form .form-row input.input-text, form .form-row select',
                            'property'              => 'height',
                            'unit'                  => 'px',
                        ),
                        'responsive'            => array(
                            'placeholder' 		    => array(
                                'default'           => '',
                                'medium' 			=> '',
                                'responsive'        => '',
                            ),
                        ),
                    ),
                    'textarea_height'   => array(
                        'type'                  => 'unit',
                        'label'                 => __('Textarea Height', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => 'form .form-row textarea',
                            'property'              => 'height',
                            'unit'                  => 'px',
                        ),
                        'responsive'            => array(
                            'placeholder'           => array(
                                'default'               => '',
                                'medium'                => '',
                                'responsive'            => '',
                            ),
                        ),
                    ),
                    'input_line_height'  => array(
                        'type'                  => 'unit',
                        'label'                 => __('Line Height', 'woopack'),
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => 'form .form-row textarea',
                            'property'              => 'line-height',
                        ),
                        'responsive'            => array(
                            'placeholder'           => array(
                                'default'               => '',
                                'medium'                => '',
                                'responsive'            => '',
                            ),
                        ),
                    ),
                    'input_text_align'  => array(
                        'type'          		=> 'select',
                        'label'         		=> __('Text Alignment', 'woopack'),
                        'default'       		=> 'left',
                        'options'       		=> array(
                            'left'           		=> __('Left', 'woopack'),
                            'center'                => __('Center', 'woopack'),
                            'right'                 => __('Right', 'woopack'),
                        ),
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => 'form .form-row input.input-text, form .form-row textarea, form .form-row select',
                            'property'              => 'text-align-last',
                        ),
                    ),
                ),
            ),
            'input_padding'     => array(
                'title'             => __('Padding', 'woopack'),
                'fields'        	=> array(
                    'input_padding'     	=> array(
                        'type'              		=> 'dimension',
                        'label' 					=> __('Padding', 'woopack'),
                        'description' 				=> 'px',
                        'preview' 					=> array(
                            'type' 						=> 'css',
                            'selector'					=> 'form .form-row input.input-text, form .form-row textarea, form .form-row select',
                            'property'					=> 'padding',
                            'unit' 						=> 'px'
                        ),
                        'responsive' 				=> array(
                            'placeholder' 				=> array(
                                'default' 					=> '',
                                'medium' 					=> '',
                                'responsive'				=> '',
                            ),
                        ),
                    ),
                ),
            ),
            'input_margin'      => array(
                'title'             => __('Margin Bottom', 'woopack'),
                'fields'        	=> array(
                    'input_margin_bottom'  	=> array(
                        'type'                  	=> 'unit',
                        'label'                 	=> __('Margin Bottom', 'woopack'),
                        'description' 				=> 'px',
                        'preview' 					=> array(
                            'type' 						=> 'css',
                            'selector'					=> 'form .form-row input.input-text, form .form-row textarea, form .form-row select',
                            'property'					=> 'margin-bottom',
                            'unit' 						=> 'px'
                        ),
                        'responsive'        		=> array(
                            'placeholder'       		=> array(
                                'default' 		    		=> '',
                                'medium' 					=> '',
                                'responsive'				=> '',
                            ),
                        ),
                    ),
                ),
            ),
        ),
        ),
    'contents'          => array(
        'title'             => __('Content', 'woopack'),
        'sections'          => array(
            'table_header_style'    => array(
                'title'             => __('Cart Heading', 'woopack'),
                'fields'            => array(
                    'table_header_padding'      => array(
                        'type'                      => 'unit',
                        'label' 				    => __('Vertical Spacing', 'woopack'),
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'rules'                     => array(
                                array(
                                    'selector'          => 'table.shop_table thead th',
                                    'property'          => 'padding-top',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => 'table.shop_table thead th',
                                    'property'          => 'padding-bottom',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                    'table_header_border_width' => array(
                        'type'                      => 'unit',
                        'label'                     => __('Separator Width', 'woopack'),
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => 'table.shop_table thead th',
                            'property'                  => 'border-bottom-width',
                            'unit'                      => 'px',
                        ),
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                    'table_header_border_color' => array(
                        'type'                      => 'color',
                        'label'                     => __('Separator Color', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'default'                   => '',
                    ),
                ),
            ),
            'cart_item_style'       => array(
                'title'             => __('Cart Item', 'woopack'),
                'fields'            => array(
                    'cart_item_padding'         => array(
                        'type'                      => 'unit',
                        'label' 				    => __('Vertical Spacing', 'woopack'),
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'rules'                     => array(
                                array(
                                    'selector'              => 'table.shop_table td, table.shop_table tfoot td, table.shop_table tfoot th',
                                    'property'              => 'padding-top',
                                    'unit'                  => 'px',
                                ),
                                array(
                                    'selector'	         => 'table.shop_table td, table.shop_table tfoot td, table.shop_table tfoot th',
                                    'property'              => 'padding-bottom',
                                    'unit'                  => 'px',
                                ),
                            ),
                        ),
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                    'cart_item_even_color'      => array(
                        'type'                      => 'color',
                        'label'                     => __('Even Item Background Color', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.cart_item:nth-child(even), table.shop_table tfoot tr:nth-child(even)',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'cart_item_odd_color'       => array(
                        'type'                      => 'color',
                        'label'                     => __('Odd Item Background Color', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.cart_item:nth-child(odd), table.shop_table tfoot tr:nth-child(odd)',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'cart_item_border_width'    => array(
                        'type'                      => 'unit',
                        'label'                     => __('Separator Width', 'woopack'),
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => 'table.shop_table td, table.shop_table tfoot td, table.shop_table tfoot th',
                            'property'                  => 'border-top-width',
                            'unit'                      => 'px',
                        ),
                        'responsive' 			    => array(
                            'placeholder' 			   => array(
                                'default' 				  => '',
                                'medium' 				   => '',
                                'responsive'			 => '',
                            ),
                        ),
                    ),
                    'cart_item_border_color'    => array(
                        'type'                      => 'color',
                        'label'                     => __('Separator Color', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                    ),
                ),
            ),
            'payment_method_style'  => array(
                'title'                 => __('Payment Method', 'woopack'),
                'fields'                => array(
                    'payment_method_label_color'            => array(
                        'type'                                  => 'color',
                        'label'                                 => __('Label Color', 'woopack'),
                        'default'                               => '',
                        'show_reset'                            => true,
                        'preview'                               => array(
                            'type'                                  => 'css',
                            'selector'                              => '#payment ul.payment_methods label',
                            'property'                              => 'color',
                        ),
                    ),
                    'payment_method_box_text_color'         => array(
                        'type'                                  => 'color',
                        'label'                                 => __('Message Text Color', 'woopack'),
                        'default'                               => '',
                        'show_reset'                            => true,
                        'preview'                               => array(
                            'type'                                  => 'css',
                            'selector'                              => '#payment div.payment_box p',
                            'property'                              => 'color',
                        ),
                    ),
                    'payment_method_box_bg_color'           => array(
                        'type'                                  => 'color',
                        'label'                                 => __('Message Background Color', 'woopack'),
                        'default'                               => '',
                        'show_reset'                            => true,
                        'preview'                               => array(
                            'type'                                  => 'css',
                            'selector'                              => '#payment div.payment_box',
                            'property'                              => 'background',
                        ),
                    ),
                    'payment_method_box_top_bottom_padding' => array(
                        'type'                                  => 'unit',
                        'label'                                 => __('Message Top & Bottom Padding', 'woopack'),
                        'description'                           => 'px',
                        'preview'                               => array(
                            'type'                                  => 'css',
                            'rules'                                 => array(
                                array(
                                    'selector'                          => '#payment div.payment_box',
                                    'property'                          => 'padding-top',
                                    'unit'                              => 'px',
                                ),
                                array(
                                    'selector'                          => '#payment div.payment_box',
                                    'property'                          => 'padding-bottom',
                                    'unit'                              => 'px',
                                ),
                            ),
                        ),
                        'responsive'                            => array(
                            'placeholder' 		                    => array(
                                'default'                           => '',
                                'medium' 			                => '',
                                'responsive'                        => '',
                            ),
                        ),
                    ),
                    'payment_method_box_right_left_padding' => array(
                        'type'                                  => 'unit',
                        'label'                                 => __('Message Right & Left Padding', 'woopack'),
                        'description'                           => 'px',
                        'preview'                               => array(
                            'type'                                  => 'css',
                            'rules' 	                            => array(
                                array(
                                    'selector'                          => '#payment div.payment_box',
                                    'property'                          => 'padding-right',
                                    'unit'                              => 'px',
                                ),
                                array(
                                    'selector'	                     => '#payment div.payment_box',
                                    'property'                          => 'padding-left',
                                    'unit'                              => 'px',
                                ),
                            ),
                        ),
                        'responsive'                            => array(
                            'placeholder'                           => array(
                                'default'                               => '',
                                'medium'                                => '',
                                'responsive'                            => '',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'messages'          => array(
        'title'             => __('Messages', 'woopack'),
        'sections'          => array(
            'errors'            => array(
                'title'             => __('Errors', 'woopack'),
                'fields'            => array(
                    'error_bg_color'    => array(
                        'type'              => 'color',
                        'label'             => __('Error Field Background Color', 'woopack'),
                        'show_reset'        => true,
                        'show_alpha'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.woocommerce-error',
                            'property'          => 'background-color',
                        ),
                    ),
                    'error_text_color'  => array(
                        'type'              => 'color',
                        'label'             => __('Error Field Message Text Color', 'woopack'),
                        'show_reset'        => true,
                        'show_alpha'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.woocommerce-error',
                            'property'          => 'color',
                        ),
                    ),
                    'error_border_color'=> array(
                        'type'              => 'color',
                        'label'             => __('Error Field Border Color', 'woopack'),
                        'show_reset'        => true,
                        'show_alpha'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.woocommerce-error',
                            'property'          => 'border-color',
                        ),
                    ),
                ),
            ),
        ),
    ),
    'button'            => array(
        'title'             => __('Button', 'woopack'),
        'sections'          => woopack_product_button_fields()

    ),
    'typography'        => array(
        'title'             => __('Typography', 'woopack'),
        'sections'          => array(
            'form_header_fonts'     => array(
                'title'                 => __('Customer Details Header', 'woopack'),
                'fields'                => array(
                    'form_header_font'              => array(
                        'label'                         => __('Font', 'woopack'),
                        'type'                          => 'font',
                        'default' 		                => array(
                            'family'                        => 'Default',
                            'weight'                        => 300
                        ),
                        'preview'                       => array(
                            'type'                          => 'font',
                            'selector'                      => '#customer_details h3, #customer_details h3 label',
                            'property'		                => 'font-family',
                        ),
                    ),
                    'form_header_font_size'         => array(
                        'type'                          => 'select',
                        'label'  			            => __('Font Size', 'woopack'),
                        'default'                       => 'default',
                        'options' 		                => array(
                            'default' 		                => __('Default', 'woopack'),
                            'custom'                        => __('Custom', 'woopack'),
                        ),
                        'toggle'                        => array(
                            'custom'                        => array(
                                'fields'                        => array('form_header_font_size_custom')
                            ),
                        ),
                    ),
                    'form_header_font_size_custom'  => array(
                        'type'                          => 'unit',
                        'label'                         => __('Custom Font Size', 'woopack'),
                        'description'                   => 'px',
                        'responsive'                    => array(
                            'placeholder'                   => array(
                                'default'                       => '15',
                                'medium'                        => '',
                                'responsive'                    => '',
                            ),
                        ),
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '#customer_details h3, #customer_details h3 label',
                            'property'                      => 'font-size',
                            'unit'                          => 'px',
                        ),
                    ),
                    'form_header_text_transform'    => array(
                        'type'                          => 'select',
                        'label' 		                => __('Text Transform', 'woopack'),
                        'default'                       => 'default',
                        'options'                       => array(
                            'default'                       => __('Default', 'woopack'),
                            'capitalize'                    => __('Capitalize', 'woopack'),
                            'lowercase'                     => __('lowercase', 'woopack'),
                            'uppercase'                     => __('UPPERCASE', 'woopack'),
                        ),
                        'preview' 		                => array(
                            'type'                          => 'css',
                            'selector'                      => '#customer_details h3, #customer_details h3 label',
                            'property'                      => 'text-transform',
                        ),
                    ),
                    'form_header_align'             => array(
                        'type'                          => 'select',
                        'label'                         => __('Alignment', 'woopack'),
                        'default'                       => 'left',
                        'options'                       => array(
                            'left'                          => __('Left', 'woopack'),
                            'center'                        => __('Center', 'woopack'),
                            'right'                         => __('Right', 'woopack')
                        ),
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '#customer_details h3, #customer_details h3 label',
                            'property'                      => 'text-align',
                        ),
                    ),
                    'form_header_line_height'       => array(
                        'type'                          => 'unit',
                        'label'                         => __('Line Height', 'woopack'),
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '#customer_details h3, #customer_details h3 label',
                            'property'                      => 'line-height',
                        ),
                        'responsive'                    => array(
                            'placeholder'                   => array(
                                'default'                       => '',
                                'medium'                        => '',
                                'responsive'                    => '',
                            ),
                        ),
                    ),
                    'form_header_color'             => array(
                        'type'                          => 'color',
                        'label'                         => __('Text Color', 'woopack'),
                        'default'                       => '',
                        'show_reset'                    => true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '#customer_details h3, #customer_details h3 label',
                            'property'                      => 'color',
                        ),
                    ),
                ),
            ),
            'form_label_fonts'      => array(
                'title'                 => __('Customer Details Label', 'woopack'),
                'fields'                => array(
                    'form_label_font'               => array(
                        'label'                     => __('Font', 'woopack'),
                        'type'                      => 'font',
                        'default' 		            => array(
                            'family'                    => 'Default',
                            'weight'                    => 300
                        ),
                        'preview'                   => array(
                            'type'                      => 'font',
                            'selector'                  => '.form-row label',
                            'property'		            => 'font-family',
                        ),
                    ),
                    'form_label_font_size'          => array(
                        'type'                      => 'select',
                        'label'  			        => __('Font Size', 'woopack'),
                        'default'                   => 'default',
                        'options' 		            => array(
                            'default' 		            => __('Default', 'woopack'),
                            'custom'                    => __('Custom', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'custom'                    => array(
                                'fields'                    => array('form_label_font_size_custom')
                            ),
                        ),
                    ),
                    'form_label_font_size_custom'   => array(
                        'type'                      => 'unit',
                        'label'                     => __('Custom Font Size', 'woopack'),
                        'description'               => 'px',
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '15',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.form-row label',
                            'property'                  => 'font-size',
                            'unit'                      => 'px',
                        ),
                    ),
                    'form_label_text_transform'     => array(
                        'type'                          => 'select',
                        'label' 		                => __('Text Transform', 'woopack'),
                        'default'                       => 'default',
                        'options'                       => array(
                            'default'                       => __('Default', 'woopack'),
                            'capitalize'                    => __('Capitalize', 'woopack'),
                            'lowercase'                     => __('lowercase', 'woopack'),
                            'uppercase'                     => __('UPPERCASE', 'woopack'),
                        ),
                        'preview' 		                => array(
                            'type'                          => 'css',
                            'selector'                      => '.form-row label',
                            'property'                      => 'text-transform',
                        ),
                    ),
                    'form_label_align'              => array(
                        'type'                      => 'select',
                        'label'                     => __('Alignment', 'woopack'),
                        'default'                   => 'left',
                        'options'                   => array(
                            'left'                      => __('Left', 'woopack'),
                            'center'                    => __('Center', 'woopack'),
                            'right'                     => __('Right', 'woopack')
                        ),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => 'form .form-row label',
                            'property'                  => 'text-align',
                        ),
                    ),
                    'form_label_line_height'        => array(
                        'type'                          => 'unit',
                        'label'                         => __('Line Height', 'woopack'),
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.form-row label',
                            'property'                      => 'line-height',
                        ),
                        'responsive'                    => array(
                            'placeholder'                   => array(
                                'default'                       => '',
                                'medium'                        => '',
                                'responsive'                    => '',
                            ),
                        ),
                    ),
                    'form_label_color'           => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.form-row label',
                            'property'                  => 'color',
                        ),
                    ),
                    'form_label_invalid_color'  => array(
                        'type'                      => 'color',
                        'label'                     => __('Invalid Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.form-row.woocommerce-invalid label',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'table_title_fonts'     => array(
                'title'                 => __('Review Order Title', 'woopack'),
                'fields'                => array(
                    'table_title_font'            => array(
                        'label'                     => __('Font', 'woopack'),
                        'type'                      => 'font',
                        'default' 		            => array(
                            'family'                    => 'Default',
                            'weight'                    => 300
                        ),
                        'preview'                   => array(
                            'type'                      => 'font',
                            'selector'                  => '#order_review_heading',
                            'property'		            => 'font-family',
                        ),
                    ),
                    'table_title_font_size'       => array(
                        'type'                      => 'select',
                        'label'  			        => __('Font Size', 'woopack'),
                        'default'                   => 'default',
                        'options' 		            => array(
                            'default' 		            => __('Default', 'woopack'),
                            'custom'                    => __('Custom', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'custom'                    => array(
                                'fields'                    => array('table_title_font_size_custom')
                            ),
                        ),
                    ),
                    'table_title_font_size_custom'=> array(
                        'type'                      => 'unit',
                        'label'                     => __('Custom Font Size', 'woopack'),
                        'description'               => 'px',
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '15',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '#order_review_heading',
                            'property'                  => 'font-size',
                            'unit'                      => 'px',
                        ),
                    ),
                    'table_title_text_transform'  => array(
                        'type'                          => 'select',
                        'label' 		                => __('Text Transform', 'woopack'),
                        'default'                       => 'default',
                        'options'                       => array(
                            'default'                       => __('Default', 'woopack'),
                            'capitalize'                    => __('Capitalize', 'woopack'),
                            'lowercase'                     => __('lowercase', 'woopack'),
                            'uppercase'                     => __('UPPERCASE', 'woopack'),
                        ),
                        'preview' 		                => array(
                            'type'                          => 'css',
                            'selector'                      => '#order_review_heading',
                            'property'                      => 'text-transform',
                        ),
                    ),
                    'table_title_align'           => array(
                        'type'                      => 'select',
                        'label'                     => __('Alignment', 'woopack'),
                        'default'                   => 'left',
                        'options'                   => array(
                            'left'                      => __('Left', 'woopack'),
                            'center'                    => __('Center', 'woopack'),
                            'right'                     => __('Right', 'woopack')
                        ),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '#order_review_heading',
                            'property'                  => 'text-align',
                        ),
                    ),
                    'table_title_line_height'     => array(
                        'type'                          => 'unit',
                        'label'                         => __('Line Height', 'woopack'),
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '#order_review_heading',
                            'property'                      => 'line-height',
                        ),
                        'responsive'                    => array(
                            'placeholder'                   => array(
                                'default'                       => '',
                                'medium'                        => '',
                                'responsive'                    => '',
                            ),
                        ),
                    ),
                    'table_title_color'           => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '#order_review_heading',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'table_header_fonts'    => array(
                'title'                 => __('Cart Header', 'woopack'),
                'fields'                => array(
                    'table_header_font'            => array(
                        'label'                     => __('Font', 'woopack'),
                        'type'                      => 'font',
                        'default' 		            => array(
                            'family'                    => 'Default',
                            'weight'                    => 300
                        ),
                        'preview'                   => array(
                            'type'                      => 'font',
                            'selector'                  => 'table.shop_table thead th',
                            'property'		            => 'font-family',
                        ),
                    ),
                    'table_header_font_size'       => array(
                        'type'                      => 'select',
                        'label'  			        => __('Font Size', 'woopack'),
                        'default'                   => 'default',
                        'options' 		            => array(
                            'default' 		            => __('Default', 'woopack'),
                            'custom'                    => __('Custom', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'custom'                    => array(
                                'fields'                    => array('table_header_font_size_custom')
                            ),
                        ),
                    ),
                    'table_header_font_size_custom'=> array(
                        'type'                      => 'unit',
                        'label'                     => __('Custom Font Size', 'woopack'),
                        'description'               => 'px',
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '15',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => 'table.shop_table thead th',
                            'property'                  => 'font-size',
                            'unit'                      => 'px',
                        ),
                    ),
                    'table_header_text_transform'  => array(
                        'type'                          => 'select',
                        'label' 		                => __('Text Transform', 'woopack'),
                        'default'                       => 'default',
                        'options'                       => array(
                            'default'                       => __('Default', 'woopack'),
                            'capitalize'                    => __('Capitalize', 'woopack'),
                            'lowercase'                     => __('lowercase', 'woopack'),
                            'uppercase'                     => __('UPPERCASE', 'woopack'),
                        ),
                        'preview' 		                => array(
                            'type'                          => 'css',
                            'selector'                      => 'table.shop_table thead th',
                            'property'                      => 'text-transform',
                        ),
                    ),
                    'table_header_line_height'     => array(
                        'type'                          => 'unit',
                        'label'                         => __('Line Height', 'woopack'),
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => 'table.shop_table thead th',
                            'property'                      => 'line-height',
                        ),
                        'responsive'                    => array(
                            'placeholder'                   => array(
                                'default'                       => '',
                                'medium'                        => '',
                                'responsive'                    => '',
                            ),
                        ),
                    ),
                    'table_header_color'           => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => 'table.shop_table thead th',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'table_fonts'           => array(
                'title'                 => __('Cart Items', 'woopack'),
                'fields'                => array(
                    'table_font'            => array(
                        'label'                     => __('Font', 'woopack'),
                        'type'                      => 'font',
                        'default' 		            => array(
                            'family'                    => 'Default',
                            'weight'                    => 300
                        ),
                        'preview'                   => array(
                            'type'                      => 'font',
                            'selector'                  => 'table.shop_table td, table.shop_table tfoot th',
                            'property'		            => 'font-family',
                        ),
                    ),
                    'table_font_size'       => array(
                        'type'                      => 'select',
                        'label'  			        => __('Font Size', 'woopack'),
                        'default'                   => 'default',
                        'options' 		            => array(
                            'default' 		            => __('Default', 'woopack'),
                            'custom'                    => __('Custom', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'custom'                    => array(
                                'fields'                    => array('table_font_size_custom')
                            ),
                        ),
                    ),
                    'table_font_size_custom'=> array(
                        'type'                      => 'unit',
                        'label'                     => __('Custom Font Size', 'woopack'),
                        'description'               => 'px',
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '15',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => 'table.shop_table td, table.shop_table tfoot th',
                            'property'                  => 'font-size',
                            'unit'                      => 'px',
                        ),
                    ),
                    'table_font_line_height'     => array(
                        'type'                          => 'unit',
                        'label'                         => __('Line Height', 'woopack'),
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => 'table.shop_table td, table.shop_table tfoot th',
                            'property'                      => 'line-height',
                        ),
                        'responsive'                    => array(
                            'placeholder'                   => array(
                                'default'                       => '',
                                'medium'                        => '',
                                'responsive'                    => '',
                            ),
                        ),
                    ),
                    'table_color'           => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => 'table.shop_table td, table.shop_table tfoot th',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'coupons_fonts'         => array(
                'title'                 => __('Coupons', 'woopack'),
                'fields'                => array(
                    'coupon_font'            => array(
                        'label'                     => __('Font', 'woopack'),
                        'type'                      => 'font',
                        'default' 		            => array(
                            'family'                    => 'Default',
                            'weight'                    => 300
                        ),
                        'preview'                   => array(
                            'type'                      => 'font',
                            'selector'                  => '.woocommerce-info',
                            'property'		            => 'font-family',
                        ),
                    ),
                    'coupon_font_size'       => array(
                        'type'                      => 'select',
                        'label'  			        => __('Font Size', 'woopack'),
                        'default'                   => 'default',
                        'options' 		            => array(
                            'default' 		            => __('Default', 'woopack'),
                            'custom'                    => __('Custom', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'custom'                    => array(
                                'fields'                    => array('coupon_font_size_custom')
                            ),
                        ),
                    ),
                    'coupon_font_size_custom'=> array(
                        'type'                      => 'unit',
                        'label'                     => __('Custom Font Size', 'woopack'),
                        'description'               => 'px',
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '15',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce-info',
                            'property'                  => 'font-size',
                            'unit'                      => 'px',
                        ),
                    ),
                    'coupon_font_line_height'     => array(
                        'type'                          => 'unit',
                        'label'                         => __('Line Height', 'woopack'),
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.woocommerce-info',
                            'property'                      => 'line-height',
                        ),
                        'responsive'                    => array(
                            'placeholder'                   => array(
                                'default'                       => '',
                                'medium'                        => '',
                                'responsive'                    => '',
                            ),
                        ),
                    ),
                    'coupon_color'           => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce-info',
                            'property'                  => 'color',
                        ),
                    ),
                    'coupon_link_color'           => array(
                        'type'                      => 'color',
                        'label'                     => __('Link Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce-info a',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'payment_method_fonts'  => array(
                'title'                 => __('Payment Method', 'woopack'),
                'fields'                => array(
                    'payment_method_font'               => array(
                        'label'                             => __('Font', 'woopack'),
                        'type'                              => 'font',
                        'default' 		                    => array(
                            'family'                            => 'Default',
                            'weight'                            => 300
                        ),
                        'preview'                           => array(
                            'type'                              => 'font',
                            'selector'                          => '#payment ul.payment_methods',
                            'property'		                    => 'font-family',
                        ),
                    ),
                    'payment_method_font_size'          => array(
                        'type'                              => 'select',
                        'label'  			                => __('Font Size', 'woopack'),
                        'default'                           => 'default',
                        'options' 		                    => array(
                            'default' 		                    => __('Default', 'woopack'),
                            'custom'                            => __('Custom', 'woopack'),
                        ),
                        'toggle'                            => array(
                            'custom'                            => array(
                                'fields'                            => array('payment_method_font_size_custom')
                            ),
                        ),
                    ),
                    'payment_method_font_size_custom'   => array(
                        'type'                              => 'unit',
                        'label'                             => __('Custom Font Size', 'woopack'),
                        'description'                       => 'px',
                        'responsive'                        => array(
                            'placeholder'                       => array(
                                'default'                           => '15',
                                'medium'                            => '',
                                'responsive'                        => '',
                            ),
                        ),
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '#payment ul.payment_methods',
                            'property'                          => 'font-size',
                            'unit'                              => 'px',
                        ),
                    ),
                    'payment_method_font_line_height'   => array(
                        'type'                              => 'unit',
                        'label'                             => __('Line Height', 'woopack'),
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '#payment ul.payment_methods li',
                            'property'                          => 'line-height',
                        ),
                        'responsive'                        => array(
                            'placeholder'                       => array(
                                'default'                           => '',
                                'medium'                            => '',
                                'responsive'                        => '',
                            ),
                        ),
                    ),
                    'payment_method_msg_font_size'      => array(
                        'type'                              => 'unit',
                        'label'                             => __('Message Font Size', 'woopack'),
                        'description'                       => 'px',
                        'responsive'                        => array(
                            'placeholder'                       => array(
                                'default'                           => '15',
                                'medium'                            => '',
                                'responsive'                        => '',
                            ),
                        ),
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '#payment div.payment_box, #payment div.payment_box p',
                            'property'                          => 'font-size',
                            'unit'                              => 'px',
                        ),
                    ),
                ),
            ),
        ),
    ),
));
