.fl-node-<?php echo $id; ?> #customer_details,
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table,
.fl-node-<?php echo $id; ?> .woocommerce #payment,
.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-info,
.fl-node-<?php echo $id; ?> .woocommerce .checkout_coupon {
	<?php if ( '' == $settings->form_bg_color ) { ?>
		background: none;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->form_bg_color ); ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->form_padding_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->form_padding_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->form_padding_right_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->form_padding_right_left, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space, 'px' ); ?>
	margin-top: 0;

	<?php if ( 'none' == $settings->form_border_style ) { ?>
		border: none;
	<?php } else { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->form_border_style, '', 'default' != $settings->form_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->form_border_width, 'px', 'default' != $settings->form_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->form_border_color, '', 'default' != $settings->form_border_style ); ?>
	<?php } ?>

	<?php WooPack_Helper::print_css( 'border-radius', $settings->form_border_radius, 'px' ); ?>

	<?php if ( 'yes' == $settings->form_shadow ) { ?>
		-webkit-box-shadow: <?php echo $settings->form_shadow_h; ?>px <?php echo $settings->form_shadow_v; ?>px <?php echo $settings->form_shadow_blur; ?>px <?php echo $settings->form_shadow_spread; ?>px <?php echo WooPack_Helper::get_color_value( $settings->form_shadow_color ); ?>;
		-moz-box-shadow: <?php echo $settings->form_shadow_h; ?>px <?php echo $settings->form_shadow_v; ?>px <?php echo $settings->form_shadow_blur; ?>px <?php echo $settings->form_shadow_spread; ?>px <?php echo WooPack_Helper::get_color_value( $settings->form_shadow_color ); ?>;
		-o-box-shadow: <?php echo $settings->form_shadow_h; ?>px <?php echo $settings->form_shadow_v; ?>px <?php echo $settings->form_shadow_blur; ?>px <?php echo $settings->form_shadow_spread; ?>px <?php echo WooPack_Helper::get_color_value( $settings->form_shadow_color ); ?>;
		box-shadow: <?php echo $settings->form_shadow_h; ?>px <?php echo $settings->form_shadow_v; ?>px <?php echo $settings->form_shadow_blur; ?>px <?php echo $settings->form_shadow_spread; ?>px <?php echo WooPack_Helper::get_color_value( $settings->form_shadow_color ); ?>;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woocommerce-NoticeGroup {
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> #order_review .shop_table {
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space, 'px !important' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce #payment {
	margin-bottom: 0px;
}
.fl-node-<?php echo $id; ?> #customer_details h3,
.fl-node-<?php echo $id; ?> #customer_details h3 label {
	<?php WooPack_Helper::print_css( 'color', $settings->form_header_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->form_header_font ); ?>
	<?php if ( 'custom' == $settings->form_header_font_size && '' != $settings->form_header_font_size_custom ) { ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->form_header_font_size_custom, 'px' ); ?>
	<?php } ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->form_header_text_transform, '', 'default' != $settings->form_header_text_transform ); ?>
	<?php WooPack_Helper::print_css( 'text-align', $settings->form_header_align ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->form_header_line_height ); ?>
}
.fl-node-<?php echo $id; ?> #ship-to-different-address-checkbox {
	margin-left: 0;
}
.fl-node-<?php echo $id; ?> #ship-to-different-address span {
	margin-left: 20px;
}
.fl-node-<?php echo $id; ?> .woocommerce form .form-row label {
	<?php WooPack_Helper::print_css( 'color', $settings->form_label_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->form_label_font ); ?>
	<?php if ( 'custom' == $settings->form_label_font_size && '' != $settings->form_label_font_size_custom ) { ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->form_label_font_size_custom, 'px' ); ?>
	<?php } ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->form_label_text_transform, '', 'default' != $settings->form_label_text_transform ); ?>
	<?php WooPack_Helper::print_css( 'text-align', $settings->form_label_align ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->form_label_line_height ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-invalid label {
	<?php WooPack_Helper::print_css( 'color', $settings->form_label_invalid_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce #order_review {
	background: transparent;
	padding: 0;
}
.fl-node-<?php echo $id; ?> .woocommerce .cart_item:nth-child(even),
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot tr:nth-child(even) {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_item_even_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .cart_item:nth-child(odd),
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot tr:nth-child(odd) {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_item_odd_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table thead th {
	<?php WooPack_Helper::print_css( 'padding-top', $settings->table_header_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_header_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->table_header_border_width, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'border-bottom-style', 'solid', '', '' != $settings->table_header_border_width ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->table_header_border_color, '', '' != $settings->table_header_border_width ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->table_header_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->table_header_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->table_header_font_size_custom, 'px', 'custom' == $settings->table_header_font_size ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->table_header_line_height ); ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->table_header_text_transform, '', 'default' != $settings->table_header_text_transform ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table td,
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot td,
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot th {
	<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_item_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_item_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_item_border_width, 'px !important' ); ?>
	<?php WooPack_Helper::print_css( 'border-top-style', 'solid', '', '' != $settings->cart_item_border_width ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->cart_item_border_color, ' !important' ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->table_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->table_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->table_font_size_custom, 'px', 'custom' == $settings->table_font_size ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->table_font_line_height ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tbody tr:first-child td {
	border-top-width: 0 !important;
}
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot td {
	font-weight: 600;
}
.fl-node-<?php echo $id; ?> .woocommerce #order_review_heading {
	<?php WooPack_Helper::print_css( 'color', $settings->table_title_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->table_title_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->table_title_font_size_custom, 'px', 'custom' == $settings->table_title_font_size ); ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->table_title_text_transform, '', 'default' != $settings->table_title_text_transform ); ?>
	<?php WooPack_Helper::print_css( 'text-align', $settings->table_title_align ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->table_title_line_height ); ?>
}

<?php
// *********************
// Inputs
// *********************
?>
.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text,
.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text:focus,
.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea,
.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea:focus,
.fl-node-<?php echo $id; ?> .woocommerce form .form-row select,
.fl-node-<?php echo $id; ?> .woocommerce .select2-container--default .select2-selection--single,
.fl-node-<?php echo $id; ?> .woocommerce .select2-container--default .select2-selection--single:focus,
.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-validated .select2-container--default .select2-selection--single,
.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-validated input.input-text {
	<?php WooPack_Helper::print_css( 'color', $settings->input_text_color ); ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->input_bg_color ); ?>

	<?php if ( 'none' == $settings->input_border_style ) { ?>
		border: none;
	<?php } else { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->input_border_style, '', 'default' != $settings->input_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->input_border_width, 'px', 'default' != $settings->input_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->input_border_color, '', 'default' != $settings->input_border_style ); ?>
	<?php } ?>

	<?php WooPack_Helper::print_css( 'border-radius', $settings->input_border_radius, 'px' ); ?>

	<?php if ( 'yes' == $settings->inpur_shadow ) { ?>
		-webkit-box-shadow: <?php if ( 'inset' == $settings->inpur_shadow_direction ) { echo $settings->inpur_shadow_direction; } ?> <?php echo $settings->inpur_shadow_h; ?>px <?php echo $settings->inpur_shadow_v; ?>px <?php echo $settings->inpur_shadow_blur; ?>px <?php echo $settings->inpur_shadow_spread; ?>px <?php echo WooPack_Helper::get_color_value( $settings->inpur_shadow_color ); ?>;
		-moz-box-shadow: <?php if ( 'inset' == $settings->inpur_shadow_direction ) { echo $settings->inpur_shadow_direction; } ?> <?php echo $settings->inpur_shadow_h; ?>px <?php echo $settings->inpur_shadow_v; ?>px <?php echo $settings->inpur_shadow_blur; ?>px <?php echo $settings->inpur_shadow_spread; ?>px <?php echo WooPack_Helper::get_color_value( $settings->inpur_shadow_color ); ?>;
		-o-box-shadow: <?php if ( 'inset' == $settings->inpur_shadow_direction ) { echo $settings->inpur_shadow_direction; } ?> <?php echo $settings->inpur_shadow_h; ?>px <?php echo $settings->inpur_shadow_v; ?>px <?php echo $settings->inpur_shadow_blur; ?>px <?php echo $settings->inpur_shadow_spread; ?>px <?php echo WooPack_Helper::get_color_value( $settings->inpur_shadow_color ); ?>;
		box-shadow: <?php if ( 'inset' == $settings->inpur_shadow_direction ) { echo $settings->inpur_shadow_direction; } ?> <?php echo $settings->inpur_shadow_h; ?>px <?php echo $settings->inpur_shadow_v; ?>px <?php echo $settings->inpur_shadow_blur; ?>px <?php echo $settings->inpur_shadow_spread; ?>px <?php echo WooPack_Helper::get_color_value( $settings->inpur_shadow_color ); ?>;
	<?php } ?>

	<?php WooPack_Helper::print_css( 'text-align-last', $settings->input_text_align ); ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->input_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->input_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->input_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->input_padding_right, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->input_margin_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->input_line_height ); ?>
}

.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-invalid input.input-text,
.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-invalid .select2-container--default .select2-selection--single {
	<?php if ( 'none' == $settings->input_border_style ) { ?>
		border: none;
	<?php } else { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->input_border_style, '', 'default' != $settings->input_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->input_border_width, 'px', 'default' != $settings->input_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->input_border_invalid, '', 'default' != $settings->input_border_style ); ?>
	<?php } ?>
}

.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text:focus,
.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea:focus {
	<?php if ( 'none' == $settings->input_border_style ) { ?>
		border: none;
	<?php } else { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->input_border_style, '', 'default' != $settings->input_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->input_border_width, 'px', 'default' != $settings->input_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->input_border_focus, '', 'default' != $settings->input_border_style ); ?>
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text,
.fl-node-<?php echo $id; ?> .woocommerce form .form-row select,
.fl-node-<?php echo $id; ?> .woocommerce .select2-container--default .select2-selection--single {
	<?php WooPack_Helper::print_css( 'height', $settings->input_height, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea {
	<?php WooPack_Helper::print_css( 'height', $settings->textarea_height, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text::-webkit-input-placeholder,
.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea::-webkit-input-placeholder {
	<?php WooPack_Helper::print_css( 'color', $settings->placeholder_color ); ?>
}

<?php
// *********************
// Button
// *********************
?>
.fl-node-<?php echo $id; ?> .woocommerce input.button,
.fl-node-<?php echo $id; ?> .woocommerce #place_order {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->button_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->button_font_size_custom, 'px', 'custom' == $settings->button_font_size ); ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->button_text_transform, '', 'default' != $settings->button_text_transform ); ?>
	<?php WooPack_Helper::print_css( 'padding-top', $settings->button_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->button_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->button_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->button_padding_right, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'width', '100', '%', 'full_width' == $settings->button_width ); ?>
	<?php WooPack_Helper::print_css( 'width', $settings->button_width_custom, '%', 'custom' == $settings->button_width ); ?>

	<?php if ( 'none' == $settings->button_border_style ) { ?>
		border: none;
	<?php } else { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style, '', 'default' != $settings->button_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width, 'px', 'default' != $settings->button_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color, '', 'default' != $settings->button_border_style ); ?>
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woocommerce #payment #place_order {
	<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom, 'px' ); ?>
	float: none;
}
.fl-node-<?php echo $id; ?> .woocommerce input.button:hover,
.fl-node-<?php echo $id; ?> .woocommerce #place_order:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color_hover ); ?>

	<?php if ( 'none' == $settings->button_border_style ) { ?>
		border: none;
	<?php } else { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style, '', 'default' != $settings->button_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width, 'px', 'default' != $settings->button_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color_hover, '', 'default' != $settings->button_border_style ); ?>
	<?php } ?>
	transition: all 0.2s ease-in-out;
}
.fl-node-<?php echo $id; ?> .woocommerce #payment .form-row.place-order,
.fl-node-<?php echo $id; ?> .woocommerce-page #payment .form-row.place-order {
	<?php WooPack_Helper::print_css( 'text-align', $settings->button_alignment ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-error {
	<?php WooPack_Helper::print_css( 'background-color', $settings->error_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->error_text_color ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->error_border_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-info {
	<?php WooPack_Helper::print_css( 'color', $settings->coupon_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->coupon_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->coupon_font_size_custom, 'px', 'custom' == $settings->coupon_font_size ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->coupon_font_line_height ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-info a {
	<?php WooPack_Helper::print_css( 'color', $settings->coupon_link_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce #payment ul.payment_methods {
	<?php WooPack_Helper::print_css( 'font', $settings->payment_method_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->payment_method_font_size_custom, 'px', 'custom' == $settings->payment_method_font_size ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce #payment ul.payment_methods label {
	<?php WooPack_Helper::print_css( 'color', $settings->payment_method_label_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce #payment ul.payment_methods li {
	<?php WooPack_Helper::print_css( 'line-height', $settings->payment_method_font_line_height ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce #payment div.payment_box {
	<?php WooPack_Helper::print_css( 'background', $settings->payment_method_box_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'padding-top', $settings->payment_method_box_top_bottom_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->payment_method_box_top_bottom_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->payment_method_box_right_left_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->payment_method_box_right_left_padding, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce #payment div.payment_box::before {
	<?php WooPack_Helper::print_css( 'border-color', $settings->payment_method_box_bg_color ); ?>
	border-right-color: transparent;
	border-left-color: transparent;
	border-top-color: transparent;
}
.fl-node-<?php echo $id; ?> .woocommerce #payment div.payment_box p {
	<?php WooPack_Helper::print_css( 'color', $settings->payment_method_box_text_color ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->payment_method_msg_font_size, 'px', 'custom' == $settings->payment_method_msg_font_size ); ?>
}

<?php
// *********************
// Media Query
// *********************
?>
@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> #customer_details,
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table,
	.fl-node-<?php echo $id; ?> .woocommerce #payment,
	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-info,
	.fl-node-<?php echo $id; ?> .woocommerce .checkout_coupon {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->form_padding_top_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->form_padding_top_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->form_padding_right_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->form_padding_right_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space_medium, 'px' ); ?>

		<?php if ( 'none' == $settings->form_border_style ) { ?>
			border: none;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->form_border_width_medium, 'px', 'default' != $settings->form_border_style ); ?>
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->form_border_radius_medium, 'px' ); ?>

		<?php if ( 'yes' == $settings->form_shadow ) { ?>
			-webkit-box-shadow: <?php echo $settings->form_shadow_h_medium; ?>px <?php echo $settings->form_shadow_v_medium; ?>px <?php echo $settings->form_shadow_blur_medium; ?>px <?php echo $settings->form_shadow_spread_medium; ?>px <?php echo WooPack_Helper::get_color_value( $settings->form_shadow_color ); ?>;
			-moz-box-shadow: <?php echo $settings->form_shadow_h_medium; ?>px <?php echo $settings->form_shadow_v_medium; ?>px <?php echo $settings->form_shadow_blur_medium; ?>px <?php echo $settings->form_shadow_spread_medium; ?>px <?php echo WooPack_Helper::get_color_value( $settings->form_shadow_color ); ?>;
			-o-box-shadow: <?php echo $settings->form_shadow_h_medium; ?>px <?php echo $settings->form_shadow_v_medium; ?>px <?php echo $settings->form_shadow_blur_medium; ?>px <?php echo $settings->form_shadow_spread_medium; ?>px <?php echo WooPack_Helper::get_color_value( $settings->form_shadow_color ); ?>;
			box-shadow: <?php echo $settings->form_shadow_h_medium; ?>px <?php echo $settings->form_shadow_v_medium; ?>px <?php echo $settings->form_shadow_blur_medium; ?>px <?php echo $settings->form_shadow_spread_medium; ?>px <?php echo WooPack_Helper::get_color_value( $settings->form_shadow_color ); ?>;
		<?php } ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce-NoticeGroup {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> #order_review .shop_table {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space_medium, 'px !important' ); ?>
	}
	.fl-node-<?php echo $id; ?> #customer_details h3,
	.fl-node-<?php echo $id; ?> #customer_details h3 label {
		<?php WooPack_Helper::print_css( 'font-size', $settings->form_header_font_size_custom_medium, 'px', 'custom' == $settings->form_header_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->form_header_line_height_medium ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce form .form-row label {
		<?php WooPack_Helper::print_css( 'font-size', $settings->form_label_font_size_custom_medium, 'px', 'custom' == $settings->form_label_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->form_label_line_height_medium ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table thead th {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->table_header_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_header_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->table_header_border_width_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->table_header_font_size_custom_medium, 'px', 'custom' == $settings->table_header_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->table_header_line_height_medium ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table td,
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot td,
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot th {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_item_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_item_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_item_border_width_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->table_font_size_custom_medium, 'px', 'custom' == $settings->table_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->table_font_line_height_medium ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #order_review_heading {
		<?php WooPack_Helper::print_css( 'font-size', $settings->table_title_font_size_custom_medium, 'px', 'custom' == $settings->table_title_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->table_title_line_height_medium ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text:focus,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea:focus,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row select,
	.fl-node-<?php echo $id; ?> .woocommerce .select2-container--default .select2-selection--single,
	.fl-node-<?php echo $id; ?> .woocommerce .select2-container--default .select2-selection--single:focus,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-validated .select2-container--default .select2-selection--single,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-validated input.input-text {
		<?php if ( 'none' == $settings->input_border_style ) { ?>
			border: none;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->input_border_width_medium, 'px', 'default' != $settings->input_border_style ); ?>
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->input_border_radius_medium, 'px' ); ?>

		<?php if ( 'yes' == $settings->inpur_shadow ) { ?>
			-webkit-box-shadow: <?php if ( 'inset' == $settings->inpur_shadow_direction ) { echo 'inset '; } ?><?php echo $settings->inpur_shadow_h_medium; ?>px <?php echo $settings->inpur_shadow_v_medium; ?>px <?php echo $settings->inpur_shadow_blur_medium; ?>px <?php echo $settings->inpur_shadow_spread_medium; ?>px <?php echo WooPack_Helper::get_color_value( $settings->inpur_shadow_color ); ?>;
			-moz-box-shadow: <?php if ( 'inset' == $settings->inpur_shadow_direction ) { echo 'inset '; } ?><?php echo $settings->inpur_shadow_h_medium; ?>px <?php echo $settings->inpur_shadow_v_medium; ?>px <?php echo $settings->inpur_shadow_blur_medium; ?>px <?php echo $settings->inpur_shadow_spread_medium; ?>px <?php echo WooPack_Helper::get_color_value( $settings->inpur_shadow_color ); ?>;
			-o-box-shadow: <?php if ( 'inset' == $settings->inpur_shadow_direction ) { echo 'inset '; } ?><?php echo $settings->inpur_shadow_h_medium; ?>px <?php echo $settings->inpur_shadow_v_medium; ?>px <?php echo $settings->inpur_shadow_blur_medium; ?>px <?php echo $settings->inpur_shadow_spread_medium; ?>px <?php echo WooPack_Helper::get_color_value( $settings->inpur_shadow_color ); ?>;
			box-shadow: <?php if ( 'inset' == $settings->inpur_shadow_direction ) { echo 'inset '; } ?><?php echo $settings->inpur_shadow_h_medium; ?>px <?php echo $settings->inpur_shadow_v_medium; ?>px <?php echo $settings->inpur_shadow_blur_medium; ?>px <?php echo $settings->inpur_shadow_spread_medium; ?>px <?php echo WooPack_Helper::get_color_value( $settings->inpur_shadow_color ); ?>;
		<?php } ?>

		<?php WooPack_Helper::print_css( 'padding-top', $settings->input_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->input_padding_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->input_padding_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->input_padding_right_medium, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->input_margin_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->input_line_height_medium ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-invalid input.input-text,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-invalid .select2-container--default .select2-selection--single {
		<?php if ( 'none' == $settings->input_border_style ) { ?>
			border: none;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->input_border_width_medium, 'px', 'default' != $settings->input_border_style ); ?>
		<?php } ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text:focus,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea:focus {
		<?php if ( 'none' == $settings->input_border_style ) { ?>
			border: none;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->input_border_width_medium, 'px', 'default' != $settings->input_border_style ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text,
	.fl-node-<?php echo $id; ?> .woocommerce .select2-container--default .select2-selection--single {
		<?php WooPack_Helper::print_css( 'height', $settings->input_height_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea {
		<?php WooPack_Helper::print_css( 'height', $settings->textarea_height_medium, 'px' ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce input.button,
	.fl-node-<?php echo $id; ?> .woocommerce #place_order {
		<?php WooPack_Helper::print_css( 'font-size', $settings->button_font_size_custom_medium, 'px', 'custom' == $settings->button_font_size ); ?>
		<?php WooPack_Helper::print_css( 'padding-top', $settings->button_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->button_padding_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->button_padding_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->button_padding_right_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius_medium, 'px' ); ?>

		<?php if ( 'none' == $settings->button_border_style ) { ?>
			border: none;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width_medium, 'px', 'default' != $settings->button_border_style ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment #place_order {
		<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce input.button:hover,
	.fl-node-<?php echo $id; ?> .woocommerce #place_order:hover {
		<?php if ( 'none' == $settings->button_border_style ) { ?>
			border: none;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width_medium, 'px', 'default' != $settings->button_border_style ); ?>
		<?php } ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-info {
		<?php WooPack_Helper::print_css( 'font-size', $settings->coupon_font_size_custom_medium, 'px', 'custom' == $settings->coupon_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->coupon_font_line_height_medium ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment ul.payment_methods {
		<?php WooPack_Helper::print_css( 'font-size', $settings->payment_method_font_size_custom_medium, 'px', 'custom' == $settings->payment_method_font_size ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment ul.payment_methods li {
		<?php WooPack_Helper::print_css( 'line-height', $settings->payment_method_font_line_height_medium ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment div.payment_box {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->payment_method_box_top_bottom_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->payment_method_box_top_bottom_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->payment_method_box_right_left_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->payment_method_box_right_left_padding_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment div.payment_box p {
		<?php WooPack_Helper::print_css( 'font-size', $settings->payment_method_msg_font_size_medium, 'px', 'custom' == $settings->payment_method_msg_font_size ); ?>
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> #customer_details,
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table,
	.fl-node-<?php echo $id; ?> .woocommerce #payment,
	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-info,
	.fl-node-<?php echo $id; ?> .woocommerce .checkout_coupon {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->form_padding_top_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->form_padding_top_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->form_padding_right_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->form_padding_right_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space_responsive, 'px' ); ?>

		<?php if ( 'none' == $settings->form_border_style ) { ?>
			border: none;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->form_border_width_responsive, 'px', 'default' != $settings->form_border_style ); ?>
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->form_border_radius_responsive, 'px' ); ?>

		<?php if ( 'yes' == $settings->form_shadow ) { ?>
			-webkit-box-shadow: <?php echo $settings->form_shadow_h_responsive; ?>px <?php echo $settings->form_shadow_v_responsive; ?>px <?php echo $settings->form_shadow_blur_responsive; ?>px <?php echo $settings->form_shadow_spread_responsive; ?>px <?php echo WooPack_Helper::get_color_value( $settings->form_shadow_color ); ?>;
			-moz-box-shadow: <?php echo $settings->form_shadow_h_responsive; ?>px <?php echo $settings->form_shadow_v_responsive; ?>px <?php echo $settings->form_shadow_blur_responsive; ?>px <?php echo $settings->form_shadow_spread_responsive; ?>px <?php echo WooPack_Helper::get_color_value( $settings->form_shadow_color ); ?>;
			-o-box-shadow: <?php echo $settings->form_shadow_h_responsive; ?>px <?php echo $settings->form_shadow_v_responsive; ?>px <?php echo $settings->form_shadow_blur_responsive; ?>px <?php echo $settings->form_shadow_spread_responsive; ?>px <?php echo WooPack_Helper::get_color_value( $settings->form_shadow_color ); ?>;
			box-shadow: <?php echo $settings->form_shadow_h_responsive; ?>px <?php echo $settings->form_shadow_v_responsive; ?>px <?php echo $settings->form_shadow_blur_responsive; ?>px <?php echo $settings->form_shadow_spread_responsive; ?>px <?php echo WooPack_Helper::get_color_value( $settings->form_shadow_color ); ?>;
		<?php } ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce-NoticeGroup {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> #order_review .shop_table {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space_responsive, 'px !important' ); ?>
	}
	.fl-node-<?php echo $id; ?> #customer_details h3,
	.fl-node-<?php echo $id; ?> #customer_details h3 label {
		<?php WooPack_Helper::print_css( 'font-size', $settings->form_header_font_size_custom_responsive, 'px', 'custom' == $settings->form_header_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->form_header_line_height_responsive ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce form .form-row label {
		<?php WooPack_Helper::print_css( 'font-size', $settings->form_label_font_size_custom_responsive, 'px', 'custom' == $settings->form_label_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->form_label_line_height_responsive ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table thead th {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->table_header_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_header_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->table_header_border_width_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->table_header_font_size_custom_responsive, 'px', 'custom' == $settings->table_header_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->table_header_line_height_responsive ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table td,
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot td,
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot th {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_item_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_item_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_item_border_width_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->table_font_size_custom_responsive, 'px', 'custom' == $settings->table_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->table_font_line_height_responsive ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #order_review_heading {
		<?php WooPack_Helper::print_css( 'font-size', $settings->table_title_font_size_custom_responsive, 'px', 'custom' == $settings->table_title_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->table_title_line_height_responsive ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text:focus,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea:focus,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row select,
	.fl-node-<?php echo $id; ?> .woocommerce .select2-container--default .select2-selection--single,
	.fl-node-<?php echo $id; ?> .woocommerce .select2-container--default .select2-selection--single:focus,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-validated .select2-container--default .select2-selection--single,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-validated input.input-text {
		<?php if ( 'none' == $settings->input_border_style ) { ?>
			border: none;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->input_border_width_responsive, 'px', 'default' != $settings->input_border_style ); ?>
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->input_border_radius_responsive, 'px' ); ?>

		<?php if ( 'yes' == $settings->inpur_shadow ) { ?>
			-webkit-box-shadow: <?php if ( 'inset' == $settings->inpur_shadow_direction ) { echo 'inset '; } ?><?php echo $settings->inpur_shadow_h_responsive; ?>px <?php echo $settings->inpur_shadow_v_responsive; ?>px <?php echo $settings->inpur_shadow_blur_responsive; ?>px <?php echo $settings->inpur_shadow_spread_responsive; ?>px <?php echo WooPack_Helper::get_color_value( $settings->inpur_shadow_color ); ?>;
			-moz-box-shadow: <?php if ( 'inset' == $settings->inpur_shadow_direction ) { echo 'inset '; } ?><?php echo $settings->inpur_shadow_h_responsive; ?>px <?php echo $settings->inpur_shadow_v_responsive; ?>px <?php echo $settings->inpur_shadow_blur_responsive; ?>px <?php echo $settings->inpur_shadow_spread_responsive; ?>px <?php echo WooPack_Helper::get_color_value( $settings->inpur_shadow_color ); ?>;
			-o-box-shadow: <?php if ( 'inset' == $settings->inpur_shadow_direction ) { echo 'inset '; } ?><?php echo $settings->inpur_shadow_h_responsive; ?>px <?php echo $settings->inpur_shadow_v_responsive; ?>px <?php echo $settings->inpur_shadow_blur_responsive; ?>px <?php echo $settings->inpur_shadow_spread_responsive; ?>px <?php echo WooPack_Helper::get_color_value( $settings->inpur_shadow_color ); ?>;
			box-shadow: <?php if ( 'inset' == $settings->inpur_shadow_direction ) { echo 'inset '; } ?><?php echo $settings->inpur_shadow_h_responsive; ?>px <?php echo $settings->inpur_shadow_v_responsive; ?>px <?php echo $settings->inpur_shadow_blur_responsive; ?>px <?php echo $settings->inpur_shadow_spread_responsive; ?>px <?php echo WooPack_Helper::get_color_value( $settings->inpur_shadow_color ); ?>;
		<?php } ?>

		<?php WooPack_Helper::print_css( 'padding-top', $settings->input_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->input_padding_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->input_padding_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->input_padding_right_responsive, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->input_margin_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->input_line_height_responsive ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-invalid input.input-text,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-invalid .select2-container--default .select2-selection--single {
		<?php if ( 'none' == $settings->input_border_style ) { ?>
			border: none;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->input_border_width_responsive, 'px', 'default' != $settings->input_border_style ); ?>
		<?php } ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text:focus,
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea:focus {
		<?php if ( 'none' == $settings->input_border_style ) { ?>
			border: none;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->input_border_width_responsive, 'px', 'default' != $settings->input_border_style ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text,
	.fl-node-<?php echo $id; ?> .woocommerce .select2-container--default .select2-selection--single {
		<?php WooPack_Helper::print_css( 'height', $settings->input_height_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea {
		<?php WooPack_Helper::print_css( 'height', $settings->textarea_height_responsive, 'px' ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce input.button,
	.fl-node-<?php echo $id; ?> .woocommerce #place_order {
		<?php WooPack_Helper::print_css( 'font-size', $settings->button_font_size_custom_responsive, 'px', 'custom' == $settings->button_font_size ); ?>
		<?php WooPack_Helper::print_css( 'padding-top', $settings->button_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->button_padding_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->button_padding_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->button_padding_right_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius_responsive, 'px' ); ?>

		<?php if ( 'none' == $settings->button_border_style ) { ?>
			border: none;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width_responsive, 'px', 'default' != $settings->button_border_style ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment #place_order {
		<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce input.button:hover,
	.fl-node-<?php echo $id; ?> .woocommerce #place_order:hover {
		<?php if ( 'none' == $settings->button_border_style ) { ?>
			border: none;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width_responsive, 'px', 'default' != $settings->button_border_style ); ?>
		<?php } ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-info {
		<?php WooPack_Helper::print_css( 'font-size', $settings->coupon_font_size_custom_responsive, 'px', 'custom' == $settings->coupon_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->coupon_font_line_height_responsive ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment ul.payment_methods {
		<?php WooPack_Helper::print_css( 'font-size', $settings->payment_method_font_size_custom_responsive, 'px', 'custom' == $settings->payment_method_font_size ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment ul.payment_methods li {
		<?php WooPack_Helper::print_css( 'line-height', $settings->payment_method_font_line_height_responsive ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment div.payment_box {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->payment_method_box_top_bottom_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->payment_method_box_top_bottom_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->payment_method_box_right_left_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->payment_method_box_right_left_padding_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment div.payment_box p {
		<?php WooPack_Helper::print_css( 'font-size', $settings->payment_method_msg_font_size_responsive, 'px', 'custom' == $settings->payment_method_msg_font_size ); ?>
	}
}

<?php if ( '2column' == $settings->checkout_layout ) {?>
	.fl-node-<?php echo $id; ?> .woocommerce .checkout_coupon .form-row,
	.fl-node-<?php echo $id; ?> .woocommerce #coupon_code {
		margin-bottom: 0;
	}
	.fl-node-<?php echo $id; ?> #customer_details {
		<?php WooPack_Helper::print_css( 'width', $settings->first_column_width, '%' ); ?>
		<?php WooPack_Helper::print_css( 'margin-right', $settings->space_bw_columns, '%' ); ?>
		float: left;
	}
	.fl-node-<?php echo $id; ?> #customer_details .col-1,
	.fl-node-<?php echo $id; ?> #customer_details .col-2 {
		width: 100%;
	}
	.fl-node-<?php echo $id; ?> .woocommerce #order_review_heading {
		<?php WooPack_Helper::print_css( 'width', $settings->second_column_width, '%' ); ?>
		float: left;
		margin-top: 0;
		margin-bottom: 10px;
		display: none;
	}
	.fl-node-<?php echo $id; ?> #order_review {
		<?php WooPack_Helper::print_css( 'width', $settings->second_column_width, '%' ); ?>
		float: left;
	}

	@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
		.fl-node-<?php echo $id; ?> #customer_details {
			<?php WooPack_Helper::print_css( 'width', $settings->first_column_width_medium, '%' ); ?>
			<?php WooPack_Helper::print_css( 'margin-right', $settings->space_bw_columns_medium, '%' ); ?>
		}
		.fl-node-<?php echo $id; ?> .woocommerce #order_review_heading,
		.fl-node-<?php echo $id; ?> #order_review {
			<?php WooPack_Helper::print_css( 'width', $settings->second_column_width_medium, '%' ); ?>
		}
	}

	@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
		.fl-node-<?php echo $id; ?> #customer_details {
			<?php WooPack_Helper::print_css( 'width', $settings->first_column_width_responsive, '%' ); ?>
			<?php WooPack_Helper::print_css( 'margin-right', $settings->space_bw_columns_responsive, '%' ); ?>
		}
		.fl-node-<?php echo $id; ?> .woocommerce #order_review_heading,
		.fl-node-<?php echo $id; ?> #order_review {
			<?php WooPack_Helper::print_css( 'width', $settings->second_column_width_responsive, '%' ); ?>
		}
	}
<?php } // End if().?>
