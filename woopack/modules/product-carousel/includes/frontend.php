<?php
if ( ! isset( $settings->woopack ) ) {
	$settings->woopack = true;
}

if ( isset( $settings->product_source ) ) {
    if ( $settings->product_source == 'featured' ) {
        add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::featured_products', 10, 1 );
    }
    if ( $settings->product_source == 'best_selling' ) {
        add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::best_selling_products', 10, 1 );
    }
    if ( $settings->product_source == 'sale' ) {
        add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::sale_products', 10, 1 );
    }
    if ( $settings->product_source == 'top_rated' ) {
        add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::top_rated_products', 10, 1 );
    }
}
// Get the query data.
$query = FLBuilderLoop::query( $settings );
// Render the posts.
if ( $query->have_posts() ) :

    do_action( 'woopack_before_products_carousel', $settings, $query );

    $paged = ( FLBuilderLoop::get_paged() > 0 ) ? ' woopack-paged-scroll-to' : '';

    do_action( 'woocommerce_before_shop_loop' );

    ?>
    <div class="woopack-products-carousel woopack-layout-<?php echo $module->settings->product_layout; ?> woocommerce">
        <div class="products owl-carousel owl-theme">
            <?php
        	while( $query->have_posts() ) {

        		$query->the_post();

                $product = wc_get_product( get_the_ID() );

                if ( is_object( $product ) && $product->is_visible() ) {
                    $product_data = $product->get_data();

        		    include apply_filters( 'woopack_products_carousel_layout_path', $module->dir . 'includes/layout-' . $module->settings->product_layout . '.php', $settings );
                }
        	}
        	?>
        </div>
    </div>
    <?php

	do_action( 'woopack_after_products_carousel', $settings, $query );
endif;


// Render the empty message.
if(!$query->have_posts()) :

?>
<div class="woopack-carousel-empty">
	<p><?php echo $settings->no_results_message; ?></p>
	<?php if ( $settings->show_search ) : ?>
	<?php get_search_form(); ?>
	<?php endif; ?>
</div>

<?php

endif;

wp_reset_postdata();

if ( isset( $settings->product_source ) ) {
    if ( $settings->product_source == 'featured' ) {
        remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::featured_products', 10 );
    }
    if ( $settings->product_source == 'best_selling' ) {
        remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::best_selling_products', 10 );
    }
    if ( $settings->product_source == 'sale' ) {
        remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::sale_products', 10 );
    }
    if ( $settings->product_source == 'top_rated' ) {
        remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::top_rated_products', 10 );
    }
}
?>
