<?php global $post, $product; ?>

<div id="woopack-product-<?php echo get_the_ID(); ?>"<?php echo $module->render_post_class( $product ); ?> data-product-id="<?php echo get_the_ID(); ?>">
    <div class="woopack-product-wrapper">
		<?php if ( 'yes' == $settings->show_image && has_post_thumbnail() ) {
        	include WOOPACK_DIR . 'includes/templates/loop-product-image.php';
    	} ?>

        <div class="woopack-product-content">
            <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                <?php if( 'yes' == $settings->product_title ) : ?>
                    <<?php echo $settings->product_title_heading_tag; ?> class="woopack-product-title"><?php echo get_the_title(); ?></<?php echo $settings->product_title_heading_tag; ?>>
                <?php endif; ?>

                <?php if ( 'yes' == $settings->product_rating ) : ?>
                    <?php echo wc_get_rating_html( $product->get_average_rating() ); ?>
                <?php endif; ?>

                <?php if( 'yes' == $settings->product_price ) : ?>
                    <?php wc_get_template( 'loop/price.php' ); ?>
                <?php endif; ?>
            </a>

            <?php if( 'yes' == $settings->product_short_description ) : ?>
                <div class="woopack-product-description">
                    <?php echo apply_filters( 'woocommerce_short_description', $product_data['short_description'] ); ?>
                </div>
            <?php endif; ?>

            <?php if ( 'none' != $settings->button_type ) {
				include WOOPACK_DIR . 'includes/templates/loop-product-button.php';
			} ?>

        </div>


    </div>
    <div class="woopack-product-meta-wrapper">
        <?php if ( 'yes' == $settings->show_taxonomy ) {
            include WOOPACK_DIR . 'includes/templates/loop-product-meta.php';
        } ?>
    </div>
</div>
