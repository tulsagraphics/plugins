<?php
if ( ! empty( $settings->category_columns ) ) {
	$column = $settings->category_columns;
} else {
	$column = '4';
}
	$spacing = $settings->category_spacing;
	$width   = ( 100 - ( $spacing * $column - 1 ) ) / $column;
	$height  = $settings->category_height;
	$margin_top = empty( $settings->category_margin_top ) ? 0 : $settings->category_margin_top;
	$margin_bottom = empty( $settings->category_margin_bottom ) ? 0 : $settings->category_margin_bottom;
	$margin_left = empty( $settings->category_margin_left ) ? 0 : $settings->category_margin_left;
	$margin_right = empty( $settings->category_margin_right ) ? 0 : $settings->category_margin_right;
	$speed   = $settings->transition_speed;
?>
.fl-node-<?php echo $id; ?> .woopack-last {
	clear: right;
	margin-right: 0 !important;
}
.fl-node-<?php echo $id; ?> .woopack-product-category {
	<?php WooPack_Helper::print_css( 'width', $width, '%' ); ?>
	<?php WooPack_Helper::print_css( 'height', $height, 'px' ); ?>
	float: left;
	background-repeat: no-repeat;
	background-size: cover;
	<?php WooPack_Helper::print_css( 'margin-right', $spacing, '%' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $spacing, '%' ); ?>
	<?php if ( 'none' != $settings->category_border_style ) { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->category_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->category_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->category_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'border-radius', $settings->category_border_radius, 'px' ); ?>
	<?php $_box_shadow = $settings->box_shadow_h . 'px ' . $settings->box_shadow_v . 'px ' . $settings->box_shadow_blur . 'px ' . $settings->box_shadow_spread . 'px ' . WooPack_Helper::get_color_value( $settings->box_shadow_color ); ?>
	<?php WooPack_Helper::print_css( 'box-shadow', $_box_shadow, '' ); ?>
	overflow: hidden;
}
.fl-node-<?php echo $id; ?> .woopack-product-category .woopack-product-category__link {
	<?php WooPack_Helper::print_css( 'background-color', $settings->category_bg_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-category:hover .woopack-product-category__link {
	<?php WooPack_Helper::print_css( 'background-color', $settings->category_bg_color_hover, '', 'style-2' != $settings->category_style ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-category .woopack-product-category__content {
	<?php WooPack_Helper::print_css( 'padding-top', $settings->category_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->category_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->category_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->category_padding_right, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-top', $settings->category_margin_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->category_margin_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-left', $settings->category_margin_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-right', $settings->category_margin_right, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'text-align', $settings->category_text_align, '', 'default' != $settings->category_text_align ); ?>
	height: <?php echo ( $height - ( $margin_top + $margin_bottom ) ); ?>px;
	width: calc( 100% - <?php echo ( $margin_left + $margin_right ); ?>px );
	display: table;
}
.fl-node-<?php echo $id; ?> .woopack-product-category .woopack-product-category__title {
	<?php WooPack_Helper::print_css( 'color', $settings->category_title_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->category_title_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->category_title_font_size_custom, 'px', 'custom' == $settings->category_title_font_size ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->category_title_line_height ); ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->category_title_text_transform, '', 'default' == $settings->category_title_text_transform ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-category .woopack-product-category__title span {
	<?php WooPack_Helper::print_css( 'font', $settings->category_title_font ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->category_title_line_height ); ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->category_title_text_transform, '', 'default' == $settings->category_title_text_transform ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->category_count_color ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->category_count_font_size_custom, 'px', 'custom' == $settings->category_count_font_size ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-category .woopack-product-category__description {
	<?php WooPack_Helper::print_css( 'margin-top', $settings->des_margin_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->category_description_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->category_description_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->category_description_font_size_custom, 'px', 'custom' == $settings->category_description_font_size ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->category_description_line_height ); ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->category_description_text_transform, '', 'default' == $settings->category_description_text_transform ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-category__button_wrapper {
	<?php if ( 'full_width' != $settings->button_width ) { ?>
		<?php WooPack_Helper::print_css( 'text-align', $settings->button_alignment, '', 'default' != $settings->button_alignment ); ?>
	<?php } ?>
	z-index: 9999;	
}
.fl-node-<?php echo $id; ?> .woopack-product-category__button_wrapper .woopack-product-category__button {
	<?php WooPack_Helper::print_css( 'width', '100', '%', 'full_width' == $settings->button_width ); ?>
	<?php WooPack_Helper::print_css( 'width', $settings->button_width_custom, '%', 'custom' == $settings->button_width ); ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->button_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->button_font_size_custom, 'px', 'custom' == $settings->button_font_size ); ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->button_text_transform ); ?>
	<?php if ( 'default' != $settings->button_border_style ) { ?>
		<?php if ( 'none' == $settings->button_border_style ) { ?>
			border: none;
		<?php } else { ?>
			border-style: <?php echo $settings->button_border_style; ?>;
			<?php if ( '' != $settings->button_border_width ) { ?>
				border-width: <?php echo $settings->button_border_width; ?>px;
			<?php } ?>
			<?php if ( '' != $settings->button_border_color ) { ?>
				border-color: <?php echo WooPack_Helper::get_color_value( $settings->button_border_color ); ?>;
			<?php } ?>
		<?php } ?>
	<?php } ?>
	<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-top', $settings->button_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->button_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->button_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->button_padding_right, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom, 'px' ); ?>
	text-align: center !important;
	float: none;
	z-index: 999999;
}
.fl-node-<?php echo $id; ?> .woopack-product-category__button_wrapper .woopack-product-category__button:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color_hover ); ?>

	<?php if ( 'none' != $settings->button_border_style ) { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color_hover ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-category__img {
	<?php WooPack_Helper::print_css( 'height', $height, 'px' ); ?>
	overflow: hidden;
}
.fl-node-<?php echo $id; ?> .woopack-product-category__img img {
	width: calc(100% + 0px);
}
.fl-node-<?php echo $id; ?> .woopack-product-category,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__link,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__content,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__title_wrapper,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__title,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__title span,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__img,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__img img,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__button_wrapper,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__button_wrapper .woopack-product-category__button,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__content::before,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__content::after,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__description_wrapper {
	-webkit-transition: all <?php echo $speed; ?>s ease-in-out;
		-moz-transition: all <?php echo $speed; ?>s ease-in-out;
			transition: all <?php echo $speed; ?>s ease-in-out;
}
.fl-node-<?php echo $id; ?> .woopack-product-category:hover,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__content,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__title_wrapper,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__title,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__title span,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__img,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__img img,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__button_wrapper,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__button_wrapper .woopack-product-category__button,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__content::before,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__content::after,
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__description_wrapper {
	-webkit-transition: all <?php echo $speed; ?>s ease-in-out;
		-moz-transition: all <?php echo $speed; ?>s ease-in-out;
			transition: all <?php echo $speed; ?>s ease-in-out;
}

.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__img img {
	<?php WooPack_Helper::print_css( 'opacity', $settings->category_bg_opacity, '', ( 'style-2' != $settings->category_style && ! empty( $settings->category_bg_color ) ) ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__img img {
	<?php WooPack_Helper::print_css( 'opacity', $settings->category_bg_opacity, '', ( 'style-2' != $settings->category_style && empty( $settings->category_bg_color ) && ! empty( $settings->category_bg_color_hover ) ) ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__title {
	<?php WooPack_Helper::print_css( 'color', $settings->category_title_hover_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__title span {
	<?php WooPack_Helper::print_css( 'color', $settings->category_count_hover_color ); ?>
}

<?php
// ========== Style - 1 ==========

if ( 'style-1' == $settings->category_style ) {
	if ( 'default' == $settings->category_text_align ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1 .woopack-product-category__content {
			text-align: left;
		}
	<?php } // End if().
	if ( 'top' == $settings->style_1_animation ) { ?>	
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1 .woopack-product-category__title_wrapper {
			transform: translate3d(0,0px,0);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1 .woopack-product-category__button_wrapper,
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1 .woopack-product-category__description_wrapper {
			transform: translate3d(0,-100px,0);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1:hover .woopack-product-category__title_wrapper {
			transform: translate3d(0,10px,0);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1:hover .woopack-product-category__description_wrapper,
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1:hover .woopack-product-category__button_wrapper {
			transform: translate3d(0,10px,0);
		}
	<?php } elseif ( 'bottom' == $settings->style_1_animation ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1 .woopack-product-category__title_wrapper {
		transform: translate3d(0,20px,0);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1 .woopack-product-category__button_wrapper,
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1 .woopack-product-category__description_wrapper {
			transform: translate3d(0,20px,0);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1:hover .woopack-product-category__title_wrapper,
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1:hover .woopack-product-category__description_wrapper,
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1:hover .woopack-product-category__button_wrapper {
			transform: translate3d(0,0,0);
		}
	<?php } elseif ( 'left' == $settings->style_1_animation ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1 .woopack-product-category__title_wrapper {
		transform: translate3d(-10px,0,0);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1 .woopack-product-category__button_wrapper,
		.woopack-product-category .product-category-style-1 .woopack-product-category__description_wrapper {
			transform: translate3d(-20px,0px,0);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1:hover .woopack-product-category__title_wrapper,
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1:hover .woopack-product-category__description_wrapper,
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1:hover .woopack-product-category__button_wrapper {
			transform: translate3d(0px,0,0);
		}
	<?php } elseif ( 'right' == $settings->style_1_animation ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1 .woopack-product-category__title_wrapper {
			transform: translate3d(10px,0px,0px);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1 .woopack-product-category__button_wrapper,
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1 .woopack-product-category__description_wrapper {
			transform: translate3d(20px,0px,0px);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1:hover .woopack-product-category__title_wrapper,
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1:hover .woopack-product-category__description_wrapper,
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-1:hover .woopack-product-category__button_wrapper {
			transform: translate3d(0px,0px,0px);
		}
	<?php } // End if().
} // End if().?> 

<?php
// ========== Style - 2 ==========

if ( 'style-2' == $settings->category_style ) {
	if ( 'default' == $settings->category_text_align ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-2 .woopack-product-category__content {
			text-align: left;
		}
	<?php } // End if().
	if ( 'top' == $settings->style_2_animation ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-2:hover .woopack-product-category__img {
			margin-top: <?php echo $settings->shutter_height; ?>px
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-2:hover .woopack-product-category__img img {
			opacity: 1 !important;
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-2 .woopack-product-category__content {
			position: absolute;
			top: -<?php echo $settings->shutter_height; ?>px;
			opacity: 0;
			visibility: hidden;
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-2:hover .woopack-product-category__content {
			opacity: 1;
			visibility: visible;
		}
	<?php } elseif ( 'bottom' == $settings->style_2_animation ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-2:hover .woopack-product-category__img {
			margin-top: -<?php echo $settings->shutter_height; ?>px;
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-2:hover .woopack-product-category__img img {
			opacity: 1 !important;
		}
	<?php } // End if().
} // End if(). ?>

<?php
// ========== Style - 3 ==========

if ( 'style-3' == $settings->category_style ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-3 .woopack-product-category__title::after {
		<?php WooPack_Helper::print_css( 'background-color', $settings->category_separator_color ); ?>
		<?php WooPack_Helper::print_css( 'height', $settings->category_separator_height, 'px' ); ?>
		-webkit-transition: opacity <?php echo $speed; ?>s, -webkit-transform <?php echo $speed; ?>s;
			transition: opacity <?php echo $speed; ?>s, transform <?php echo $speed; ?>s;
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-3 .woopack-product-category__title {
		<?php if ( 'default' == $settings->category_text_align ) { ?>
				text-align: right;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'text-align', $settings->category_text_align ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-3 .woopack-product-category__title::after {
		position: absolute;
		top: 100%;
		left: 0;
		width: 100%;
		content: '';
		opacity: 0;
		margin: 5px 0;
		-webkit-transform: translate3d(200px,0px,0);
			transform: translate3d(200px,0px,0);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-3 .woopack-product-category__description_wrapper,
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-3 .woopack-product-category__button_wrapper {
		position: relative;
		top: 0%;
		left: -24%;
		<?php if ( 'default' == $settings->category_text_align ) { ?>
				text-align: right;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'text-align', $settings->category_text_align, '', 'default' != $settings->category_text_align ); ?>
		<?php } ?>	
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-3 .woopack-product-category__img img {
		width: calc(100% + 12px);
		max-width: none;
		transform: translate3d(-10px,0px,0);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-3:hover .woopack-product-category__title::after {
		opacity: 1;
		-webkit-transform: translate3d(0,0px,0);
			transform: translate3d(0,0px,0);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-3:hover .woopack-product-category__img img {
		transform: translate3d(0px,0px,0);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-3:hover .woopack-product-category__description_wrapper,
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-3:hover .woopack-product-category__button_wrapper {
		opacity: 1;
		visibility: visible;
		position: relative;
		left: 0%;
		right: 0%;
	}
<?php
} // End if(). ?>

<?php
// ========== Style - 4 ==========

if ( 'style-4' == $settings->category_style ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-4 .woopack-product-category__title::after {
		<?php WooPack_Helper::print_css( 'background-color', $settings->category_separator_color ); ?>
		<?php WooPack_Helper::print_css( 'height', $settings->category_separator_height, 'px' ); ?>
		-webkit-transition: opacity <?php echo $speed; ?>s, -webkit-transform <?php echo $speed; ?>s;
			transition: opacity <?php echo $speed; ?>s, transform <?php echo $speed; ?>s;
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-4 .woopack-product-category__title {
		<?php if ( 'default' == $settings->category_text_align ) { ?>
				text-align: left;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'text-align', $settings->category_text_align ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-4 .woopack-product-category__title::after {
		position: absolute;
		top: 100%;
		left: 0;
		width: 100%;
		content: '';
		opacity: 0;
		margin: 5px 0;
		-webkit-transform: translate3d(-200px,0px,0);
			transform: translate3d(-200px,0px,0);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-4 .woopack-product-category__description_wrapper,
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-4 .woopack-product-category__button_wrapper {
		position: relative;
		top: 0%;
		right: -24%;
		<?php if ( 'default' == $settings->category_text_align ) { ?>
				text-align: left;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'text-align', $settings->category_text_align ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-4 .woopack-product-category__img img {
		transform: translate3d(0px,0px,0);
		width: calc(100% + 12px);
		max-width: none;
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-4:hover .woopack-product-category__title::after {
		opacity: 1;
		-webkit-transform: translate3d(0,0px,0);
			transform: translate3d(0,0px,0);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-4:hover .woopack-product-category__img img {
		transform: translate3d(-10px,0px,0);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-4:hover .woopack-product-category__description_wrapper,
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-4:hover .woopack-product-category__button_wrapper {
		opacity: 1;
		visibility: visible;
		position: relative;
		right: 0%;
	}		
<?php
} // End if().?>

<?php
// ========== Style - 5 ==========

if ( 'style-5' == $settings->category_style ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-5 .woopack-product-category__img {
		perspective: 2000px;
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-5 .woopack-product-category__img img {
		<?php WooPack_Helper::print_css( 'opacity', $settings->category_bg_opacity, ' !important'); ?>
		-webkit-transform: translate3d(0,0,300px);
			transform: translate3d(0,0,300px);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-5 .woopack-product-category__content {
		<?php if ( 'default' == $settings->category_text_align ) { ?>
				text-align: center;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'text-align', $settings->category_text_align ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-5 .woopack-product-category__content:before,
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-5 .woopack-product-category__content:after {
		<?php WooPack_Helper::print_css( 'background', $settings->category_separator_color ); ?>
		<?php WooPack_Helper::print_css( 'height', $settings->category_separator_height, 'px' ); ?>
		position: absolute;
		top: 50%;
		left: 50%;
		width: 80%;
		content: '';
		-webkit-transform: translate3d(-50%,-50%,0);
			transform: translate3d(-50%,-50%,0);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-5 .woopack-product-category__title_wrapper {
		position: absolute;
		top: 50%;
		left: 0;
		width: 100%;
		-webkit-transform: translate3d(0,0,0) translate3d(0,-120%,0);
			transform: translate3d(0,0,0) translate3d(0,-120%,0);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-5 .woopack-product-category__description_wrapper {
		position: absolute;
		top: 50%;
		left: 0;
		width: 100%;
		-webkit-transform: translate3d(0,-100%,0) translate3d(0,150%,0);
			transform: translate3d(0,-100%,0) translate3d(0,150%,0);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-5:hover .woopack-product-category__img img {
		<?php WooPack_Helper::print_css( 'opacity', $settings->category_bg_opacity ); ?>
		-webkit-transform: translate3d(0,0,0);
			transform: translate3d(0,0,0);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-5:hover .woopack-product-category__content:before {
		opacity: 0.5;
		<?php WooPack_Helper::print_css( 'height', $settings->category_separator_height, 'px' ); ?>	
		-webkit-transform: translate3d(-50%,-50%,0) rotate(45deg);
			transform: translate3d(-50%,-50%,0) rotate(45deg);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-5:hover .woopack-product-category__content:after {
		opacity: 0.5;
		<?php WooPack_Helper::print_css( 'height', $settings->category_separator_height, 'px' ); ?>
		-webkit-transform: translate3d(-50%,-50%,0) rotate(-45deg);
			transform: translate3d(-50%,-50%,0) rotate(-45deg);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-5:hover .woopack-product-category__title_wrapper {
		position: absolute;
		top: 50%;
		left: 0;
		width: 100%;	
		-webkit-transform: translate3d(0,0,0) translate3d(0,-100%,0);
			transform: translate3d(0,0,0) translate3d(0,-100%,0);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-5:hover .woopack-product-category__description_wrapper {
		-webkit-transform: translate3d(0,-100%,0) translate3d(0,130%,0);
			transform: translate3d(0,-100%,0) translate3d(0,130%,0);
	}
<?php
} // End if().?>

<?php
// ========== Style - 6 ==========

if ( 'style-6' == $settings->category_style ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-6 .woopack-product-category__content {
		position: absolute;
		top: 0;
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-6 .woopack-product-category__title_wrapper {
		<?php if ( 'default' == $settings->category_text_align ) { ?>
			text-align: left;
		<?php } else { ?>
			<?php WooPack_Helper::print_css( 'text-align', $settings->category_text_align ); ?>
		<?php } ?>
		width: 90%;
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-6 .woopack-product-category__button_wrapper,
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-6 .woopack-product-category__description_wrapper {
		display: none;
	}
	<?php if ( 'top' == $settings->style_6_animation ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-6 .woopack-product-category__title_wrapper {
			position: absolute;
			top: -100px;
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-6:hover .woopack-product-category__title_wrapper {
			position: absolute;
			top: 0;
		}
	<?php } elseif ( 'bottom' == $settings->style_6_animation ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-6 .woopack-product-category__title_wrapper {
			position: absolute;
			bottom: -100px;
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-6:hover .woopack-product-category__title_wrapper {
			position: absolute;
			bottom: 0;
		}
	<?php } // End if().
} // End if(). ?>


<?php
// ========== Style - 7 and Style - 8 ==========

if ( 'style-7' == $settings->category_style || 'style-8' == $settings->category_style ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__content {
		<?php WooPack_Helper::print_css( 'padding-top', '10', 'px', '' == $settings->category_padding_top ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', '10', 'px', '' == $settings->category_padding_bottom, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', '10', 'px', '' == $settings->category_padding_left, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', '10', 'px', '' == $settings->category_padding_right, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-top', '20', 'px', '' == $settings->category_margin_top ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', '20', 'px', '' == $settings->category_margin_bottom ); ?>
		<?php WooPack_Helper::print_css( 'margin-left', '20', 'px', '' == $settings->category_margin_left ); ?>
		<?php WooPack_Helper::print_css( 'margin-right', '20', 'px', '' == $settings->category_margin_right ); ?>
		<?php if ( '' == $settings->category_margin_top && '' == $settings->category_margin_bottom ) {?>
			height: <?php echo ( $height - '40' ); ?>px;
		<?php } ?>
		<?php if ( '' == $settings->category_margin_left && '' == $settings->category_margin_right ) {?>
			width: calc( 100% - 40px );
		<?php } ?>
	}
	<?php if ( 'in' == $settings->style_8_animation ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__button_wrapper,
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__description_wrapper {
			opacity: 0;
			visibility: hidden;
			display: table-row;
			vertical-align: middle;
			-webkit-transform: scale3d(0.5,0.5,1);
				transform: scale3d(0.5,0.5,1);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__button_wrapper,
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__description_wrapper {
			opacity: 1;
			visibility: visible;
			-webkit-transition: opacity 0.5s, -webkit-transform 0.5s;
				transition: opacity 0.5s, transform 0.5s;
			-webkit-transform: scale3d(1,1,1);
				transform: scale3d(1,1,1);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__img {
			perspective: 2000px;
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__img img {
			-webkit-transform: translate3d(0,0,300px);
				transform: translate3d(0,0,300px);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__img img {
			-webkit-transform: translate3d(0,0,0);
				transform: translate3d(0,0,0);
		}
	<?php } elseif ( 'out' == $settings->style_8_animation ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__button_wrapper,
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__description_wrapper {
			opacity: 0;
			visibility: hidden;
			position: relative;
			left: 5%;
			right: 5%;
			width: 90%;
			float: left;
			-webkit-transform: scale3d(1.5,1.5,1);
				transform: scale3d(1.5,1.5,1);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__button_wrapper,
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__description_wrapper {
			opacity: 1;
			visibility: visible;
			-webkit-transition: opacity 0.5s, -webkit-transform 0.5s;
				transition: opacity 0.5s, transform 0.5s;
			-webkit-transform: scale3d(1,1,1);
				transform: scale3d(1,1,1);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__img {
			perspective: 2000px;
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?>:hover .woopack-product-category__img img {
			-webkit-transform: translate3d(0,0,300px);
				transform: translate3d(0,0,300px);
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-<?php echo $settings->category_style; ?> .woopack-product-category__img img {
			-webkit-transform: translate3d(0,0,0);
				transform: translate3d(0,0,0);
		}
	<?php } // End if().
} ?>


<?php
// ========== Style - 7 ==========

if ( 'style-7' == $settings->category_style ) { ?>
	<?php if ( 'tr-bl' == $settings->style_7_animation ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-7 .woopack-product-category__content:after {
			content: '';
			opacity: 0;
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			border-style: solid;
			<?php WooPack_Helper::print_css( 'border-color', $settings->category_separator_color ); ?>
			<?php WooPack_Helper::print_css( 'border-top-width', $settings->category_separator_height, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->category_separator_height, 'px' ); ?>
			border-left: none;
			border-right: none;
			-webkit-transform: rotate3d(0,0,1,45deg) scale3d(1,0,1);
				transform: rotate3d(0,0,1,45deg) scale3d(1,0,1);
			-webkit-transform-origin: 50% 50%;
				transform-origin: 50% 50%;
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-7:hover .woopack-product-category__content:after {
			opacity: 1;
			-webkit-transform: rotate3d(0,0,1,45deg) scale3d(1,1.5,1);
				transform: rotate3d(0,0,1,45deg) scale3d(1,1.5,1);
		}
	<?php } elseif ( 'tl-br' == $settings->style_7_animation ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-7 .woopack-product-category__content:after {
			content: '';
			opacity: 0;
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			border-style: solid;
			<?php WooPack_Helper::print_css( 'border-color', $settings->category_separator_color ); ?>
			<?php WooPack_Helper::print_css( 'border-top-width', $settings->category_separator_height, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->category_separator_height, 'px' ); ?>
			border-left: none;
			border-right: none;
			-webkit-transform: rotate3d(0,0,1,135deg) scale3d(1,0,1);
				transform: rotate3d(0,0,1,135deg) scale3d(1,0,1);
			-webkit-transform-origin: 50% 50%;
				transform-origin: 50% 50%;
		}
		.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-7:hover .woopack-product-category__content:after {
			opacity: 1;
			-webkit-transform: rotate3d(0,0,1,135deg) scale3d(1,1.5,1);
				transform: rotate3d(0,0,1,135deg) scale3d(1,1.5,1);
		}
	<?php } // End if().
} // End if().?>


<?php
// ========== Style - 8 ==========

if ( 'style-8' == $settings->category_style ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-8 .woopack-product-category__content:before {
		<?php WooPack_Helper::print_css( 'border-color', $settings->category_separator_color ); ?>
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->category_separator_height, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->category_separator_height, 'px' ); ?>
		border-style: solid;
		border-left: none;
		border-right: none;
		-webkit-transform: scale(0,1);
			transform: scale(0,1);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-8 .woopack-product-category__content:after {
		<?php WooPack_Helper::print_css( 'border-color', $settings->category_separator_color ); ?>
		<?php WooPack_Helper::print_css( 'border-left-width', $settings->category_separator_height, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-right-width', $settings->category_separator_height, 'px' ); ?>
		border-style: solid;
		border-top: none;
		border-bottom: none;
		-webkit-transform: scale(1,0);
			transform: scale(1,0);
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-8:hover .woopack-product-category__content::before,
	.fl-node-<?php echo $id; ?> .woopack-product-category .product-category-style-8:hover .woopack-product-category__content::after {
		opacity: 1;
		visibility: visible;
		-webkit-transform: scale(1);
			transform: scale(1);
	}
<?php } // End if(). ?>



<?php
// *********************
// Media Query
// *********************
?>

@media only screen and ( max-width: <?php echo $global_settings->medium_breakpoint; ?>px ) {
	<?php
	$spacing_medium = empty( $settings->category_spacing_medium ) ? $settings->category_spacing : $settings->category_spacing_medium;
	$height_medium  = $settings->category_height_medium;
	?>
	.fl-node-<?php echo $id; ?> .woopack-product-category {
		<?php if ( ! empty( $settings->category_columns_medium ) ) {
			$column_medium = absint( $settings->category_columns_medium );
			$width_medium   = ( 100 - ( $spacing_medium * $column_medium - 1 ) ) / $column_medium;
			WooPack_Helper::print_css( 'width', $width_medium, '%' );
		} ?>
		<?php WooPack_Helper::print_css( 'height', $height_medium, 'px', '' != $height_medium ); ?>
		<?php WooPack_Helper::print_css( 'margin-right', $spacing_medium, '%' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $spacing_medium, '%' ); ?>
		<?php if ( 'none' != $settings->category_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->category_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->category_border_width_medium, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->category_border_color ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->category_border_radius_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .woopack-product-category__content {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->category_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->category_padding_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->category_padding_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->category_padding_right_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-top', $settings->category_margin_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->category_margin_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-left', $settings->category_margin_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-right', $settings->category_margin_right_medium, 'px' ); ?>
		<?php if ( ! empty( $height_medium ) ) { ?>
		height: <?php echo ( $height_medium - 40 ); ?>px;
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .woopack-product-category__title {
		<?php WooPack_Helper::print_css( 'font-size', $settings->category_title_font_size_custom_medium, 'px', 'custom' == $settings->category_title_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->category_title_line_height_medium ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .woopack-product-category__title span {
		<?php WooPack_Helper::print_css( 'line-height', $settings->category_title_line_height_medium ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->category_count_font_size_custom_medium, 'px', 'custom' == $settings->category_count_font_size ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .woopack-product-category__description {
		<?php WooPack_Helper::print_css( 'font-size', $settings->category_description_font_size_custom_medium, 'px', 'custom' == $settings->category_description_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->category_description_line_height_medium ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category__button_wrapper .woopack-product-category__button {
		<?php WooPack_Helper::print_css( 'width', '100', '%', 'full_width' == $settings->button_width ); ?>
		<?php WooPack_Helper::print_css( 'width', $settings->button_width_custom_medium, '%', 'custom' == $settings->button_width ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->button_font_size_custom_medium, 'px', 'custom' == $settings->button_font_size ); ?>

		<?php if ( 'none' != $settings->button_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style, '', 'default' == $settings->button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width_medium, 'px', 'default' == $settings->button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color, '', 'default' == $settings->button_border_style ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius_medium, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'padding-top', $settings->button_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->button_padding_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->button_padding_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->button_padding_right_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category__button_wrapper .woopack-product-category__button:hover {
		<?php if ( 'none' != $settings->button_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style, '', 'default' == $settings->button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width_medium, 'px', 'default' == $settings->button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color_hover, '', 'default' == $settings->button_border_style ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category__img {
		<?php WooPack_Helper::print_css( 'height', $height_medium, 'px' ); ?>
	}
}

@media only screen and ( max-width: <?php echo $global_settings->responsive_breakpoint; ?>px ) {
	<?php
	$spacing_responsive = empty( $settings->category_spacing_responsive ) ? $spacing_medium : $settings->category_spacing_responsive;
	$height_responsive  = $settings->category_height_responsive;
	?>
	.fl-node-<?php echo $id; ?> .woopack-product-category {
		<?php if ( ! empty( $settings->category_columns_responsive ) ) {
			$column_responsive = absint( $settings->category_columns_responsive );
			$width_responsive  = ( 100 - ( $spacing_responsive * $column_responsive - 1 ) ) / $column_responsive;
			WooPack_Helper::print_css( 'width', $width_medium, '%' );
		} ?>
		<?php WooPack_Helper::print_css( 'height', $height_responsive, 'px', '' != $height_responsive ); ?>
		<?php WooPack_Helper::print_css( 'margin-right', $spacing_responsive, '%' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $spacing_responsive, '%' ); ?>
		<?php if ( 'none' != $settings->category_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->category_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->category_border_width_responsive, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->category_border_color ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->category_border_radius_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .woopack-product-category__content {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->category_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->category_padding_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->category_padding_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->category_padding_right_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-top', $settings->category_margin_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->category_margin_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-left', $settings->category_margin_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-right', $settings->category_margin_right_responsive, 'px' ); ?>
		<?php if ( ! empty( $height_responsive ) ) { ?>
		height: <?php echo ( $height_responsive - 40 ); ?>px;
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .woopack-product-category__title {
		<?php WooPack_Helper::print_css( 'font-size', $settings->category_title_font_size_custom_responsive, 'px', 'custom' == $settings->category_title_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->category_title_line_height_responsive ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .woopack-product-category__title span {
		<?php WooPack_Helper::print_css( 'line-height', $settings->category_title_line_height_responsive ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->category_count_font_size_custom_responsive, 'px', 'custom' == $settings->category_count_font_size ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category .woopack-product-category__description {
		<?php WooPack_Helper::print_css( 'font-size', $settings->category_description_font_size_custom_responsive, 'px', 'custom' == $settings->category_description_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->category_description_line_height_responsive ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category__button_wrapper .woopack-product-category__button {
		<?php WooPack_Helper::print_css( 'width', '100', '%', 'full_width' == $settings->button_width ); ?>
		<?php WooPack_Helper::print_css( 'width', $settings->button_width_custom_responsive, '%', 'custom' == $settings->button_width ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->button_font_size_custom_responsive, 'px', 'custom' == $settings->button_font_size ); ?>

		<?php if ( 'none' != $settings->button_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style, '', 'default' == $settings->button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width_responsive, 'px', 'default' == $settings->button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color, '', 'default' == $settings->button_border_style ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius_responsive, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'padding-top', $settings->button_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->button_padding_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->button_padding_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->button_padding_right_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category__button_wrapper .woopack-product-category__button:hover {
		<?php if ( 'none' != $settings->button_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style, '', 'default' == $settings->button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width_responsive, 'px', 'default' == $settings->button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color_hover, '', 'default' == $settings->button_border_style ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-product-category__img {
		<?php WooPack_Helper::print_css( 'height', $height_responsive, 'px' ); ?>
	}
}
