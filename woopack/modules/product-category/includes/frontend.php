<?php
$cat_match    = $settings->tax_product_product_cat_matching;
$ids          = explode( ',', $settings->tax_product_product_cat );
$taxonomy     = 'product_cat';
$orderby      = 'name';
$show_count   = 1;      // 1 for yes, 0 for no
$pad_counts   = 1;      // 1 for yes, 0 for no
$hierarchical = 1;      // 1 for yes, 0 for no
$title        = '';
$empty        = 0;

$args = array(
	'taxonomy'     => $taxonomy,
	'orderby'      => $orderby,
	'show_count'   => $show_count,
	'pad_counts'   => $pad_counts,
	'hierarchical' => $hierarchical,
	'title_li'     => $title,
	'hide_empty'   => $empty,
);

if ( $cat_match && 'related' != $cat_match && ! empty( $ids ) ) {
	$args['include'] = $ids;
}
if ( ( ! $cat_match || 'related' == $cat_match ) && ! empty( $ids ) ) {
	$args['exclude'] = $ids;
}

$all_categories = get_categories( $args ); ?>

<div class="woopack-product-categories woopack-clear">
<?php
foreach ( $all_categories as $cat ) {
	$cat_thumb_id     = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
	$shop_catalog_img = wp_get_attachment_image_src( $cat_thumb_id, 'shop_catalog' );
	$term_link        = get_term_link( $cat, $taxonomy );

	include WOOPACK_DIR . 'modules/product-category/includes/layout-1.php';
}
?>
</div>
