<div class="fl-custom-query fl-loop-data-source" data-source="custom_query">
	<div id="fl-builder-settings-section-filter" class="fl-builder-settings-section">
		<h3 class="fl-builder-settings-title"><?php _e('Product Category Filter', 'woopack'); ?></h3>
		<table class="fl-form-table fl-custom-query-filter fl-custom-query-product-filter" style="display:table;" >
			<?php
			$taxonomies = FLBuilderLoop::taxonomies('product');
			foreach($taxonomies as $tax_slug => $tax) {
				if ( 'product_cat' == $tax->name ) {
					FLBuilder::render_settings_field( 'tax_product' . '_' . $tax_slug, array(
						'type'          => 'suggest',
						'action'        => 'fl_as_terms',
						'data'          => $tax_slug,
						'label'         => $tax->label,
						'help'          => sprintf( __( 'Enter a list of %1$s.', 'woopack' ), $tax->label ),
						'matching'      => true
					), $settings );
				}
			}
			?>
		</table>
	</div>
</div>