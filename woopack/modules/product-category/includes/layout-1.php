<div class="woopack-product-category woopack-clear" title="<?php echo $cat->name; ?>">
	<?php
		$style       = $settings->category_style;
		$category_id = $cat->term_id; ?>

		<div class="product-category-inner product-category-<?php echo $style; ?>">
			<a href="<?php echo $term_link; ?>" target="<?php echo $settings->category_link_target; ?>" class="woopack-product-category__link">
				<?php if ( is_array( $shop_catalog_img ) && ! empty( $shop_catalog_img ) ) { ?>
					<div class="woopack-product-category__img">
						<img src="<?php echo $shop_catalog_img[0]; ?>" alt="<?php echo $cat->name; ?>">
					</div>
				<?php } ?>
				<div class="woopack-product-category__content">
					<div class='woopack-product-category__title_wrapper'>
						<<?php echo $settings->category_title_tag; ?> class="woopack-product-category__title">
							<?php echo $cat->name; ?>
							<?php if ( 'yes' == $settings->category_show_counter ) { ?>
								<span class="count">(<?php echo $cat->count; ?>)</span>
							<?php }; ?>
						</<?php echo $settings->category_title_tag; ?>>
					</div>
					<div class='woopack-product-category__description_wrapper'>
						<p class="woopack-product-category__description"><?php echo $cat->category_description; ?></p>
					</div>		
					<?php if ( 'yes' == $settings->category_show_button ) : ?>
						<div class="woopack-product-category__button_wrapper">
							<button type="button" name="button" class="woopack-product-category__button">
								<?php
								if ( '' != $settings->category_button_text ) {
									echo $settings->category_button_text;
								} else {
									esc_html_e( 'Shop Now', 'woopack' );
								}
								?>
							</button>
						</div>
					<?php endif; ?>
				</div>
			</a>
		</div>
</div>
