<?php
/**
* @class WooPackProductCategory
*/
class WooPackProductCategory extends FLBuilderModule {
	/**
	* Constructor function for the module. You must pass the
	* name, description, dir and url in an array to the parent class.
	*
	* @method __construct
	*/
	public function __construct() {
		parent::__construct( array(
			'name'          => __( 'Product Category', 'woopack' ),
			'description'   => __( 'Addon to display Product Category.', 'woopack' ),
			'group'         => WooPack_Helper::get_modules_group(),
			'category'      => WOOPACK_CAT,
			'dir'           => WOOPACK_DIR . 'modules/product-category/',
			'url'           => WOOPACK_URL . 'modules/product-category/',
			'editor_export' => true, // Defaults to true and can be omitted.
			'enabled'       => true, // Defaults to true and can be omitted.
		));
	}
}
/**
* Register the module and its form settings.
*/
FLBuilder::register_module( 'WooPackProductCategory', array(
	'structure'  => array(
		'title'    => __( 'Structure', 'woopack' ),
		'sections' => array(
			'config'           => array(
				'title'  => __( 'Structure', 'woopack' ),
				'fields' => array(
					'category_style'      => array(
						'type'    => 'select',
						'label'   => __( 'Style', 'woopack' ),
						'default' => 'style-1',
						'options' => array(
							'style-1' => __( 'Style 1', 'woopack' ),
							'style-2' => __( 'Style 2', 'woopack' ),
							'style-3' => __( 'Style 3', 'woopack' ),
							'style-4' => __( 'Style 4', 'woopack' ),
							'style-5' => __( 'Style 5', 'woopack' ),
							'style-6' => __( 'Style 6', 'woopack' ),
							'style-7' => __( 'Style 7', 'woopack' ),
							'style-8' => __( 'Style 8', 'woopack' ),
						),
						'toggle'  => array(
							'style-1' => array(
								'fields'   => array( 'style_1_animation', 'category_bg_color_hover', 'category_bg_opacity', 'category_title_hover_color', 'category_count_hover_color', 'category_show_button' ),
								'sections' => array( 'padding' ),
							),
							'style-2' => array(
								'fields'   => array( 'style_2_animation', 'category_show_button' ),
								'sections' => array( 'sh_height', 'padding' ),
							),
							'style-3' => array(
								'fields'   => array( 'style_3_animation', 'category_bg_color_hover', 'category_bg_opacity', 'category_title_hover_color', 'category_count_hover_color', 'category_show_button' ),
								'sections' => array( 'separator', 'padding' ),
							),
							'style-4' => array(
								'fields'   => array( 'category_bg_color_hover', 'category_bg_opacity', 'category_title_hover_color', 'category_count_hover_color', 'category_show_button' ),
								'sections' => array( 'separator', 'padding' ),
							),
							'style-5' => array(
								'fields'   => array( 'category_bg_color_hover', 'category_bg_opacity' ),
								'sections' => array( 'separator' ),
							),
							'style-6' => array(
								'fields' => array( 'style_6_animation', 'category_bg_color_hover', 'category_bg_opacity' ),
								'sections' => array( 'padding' ),
							),
							'style-7' => array(
								'fields'   => array( 'style_7_animation', 'style_8_animation', 'category_bg_color_hover', 'category_bg_opacity', 'category_title_hover_color', 'category_count_hover_color', 'category_show_button' ),
								'sections' => array( 'separator', 'padding' ),
							),
							'style-8' => array(
								'fields'   => array( 'style_8_animation', 'category_bg_color_hover', 'category_bg_opacity', 'category_title_hover_color', 'category_count_hover_color', 'category_show_button' ),
								'sections' => array( 'separator', 'padding' ),
							),
						),
					),
					'category_columns'    => array(
						'type'       => 'unit',
						'label'      => __( 'Columns', 'woopack' ),
						'default'    => '3',
						'responsive' => array(
							'placeholder' => array(
								'default'    => '3',
								'medium'     => '2',
								'responsive' => '1',
							),
						),
					),
					'category_spacing'    => array(
						'type'        => 'unit',
						'label'       => __( 'Spacing', 'woopack' ),
						'default'     => '2',
						'description' => '%',
						'responsive'  => array(
							'placeholder' => array(
								'default'    => '',
								'medium'     => '',
								'responsive' => '',
							),
						),
						'preview'	=> array(
							'type'		=> 'css',
							'rules'		=> array(
								array(
									'selector'	=> '.woopack-product-category',
									'property'	=> 'margin-right',
									'unit'		=> '%'
								),
								array(
									'selector'	=> '.woopack-product-category',
									'property'	=> 'margin-bottom',
									'unit'		=> '%'
								)
							)
						)
					),
					'category_height'     => array(
						'type'        => 'unit',
						'label'       => __( 'Height', 'woopack' ),
						'default'     => '300',
						'description' => 'px',
						'responsive'  => array(
							'placeholder' => array(
								'default'    => '',
								'medium'     => '',
								'responsive' => '',
							),
						),
					),
					'category_text_align' => array(
						'type'    => 'select',
						'label'   => __( 'Content Alignment', 'woopack' ),
						'default' => 'default',
						'options' => array(
							'default' => __( 'Default', 'woopack' ),
							'center'  => __( 'Center', 'woopack' ),
							'left'    => __( 'Left', 'woopack' ),
							'right'   => __( 'Right', 'woopack' ),
						),
					),
					'style_1_animation'   => array(
						'type'    => 'select',
						'label'   => __( 'Animate content from (on hover)', 'woopack' ),
						'default' => 'left',
						'options' => array(
							'top'    => __( 'Top', 'woopack' ),
							'bottom' => __( 'Bottom', 'woopack' ),
							'left'   => __( 'Left', 'woopack' ),
							'right'  => __( 'Right', 'woopack' ),
						),
					),
					'style_2_animation'   => array(
						'type'    => 'select',
						'label'   => __( 'Animate content from (on hover)', 'woopack' ),
						'default' => 'bottom',
						'options' => array(
							'top'            => __( 'Top', 'woopack' ),
							'bottom'         => __( 'Bottom', 'woopack' ),
						),
					),
					'style_6_animation'   => array(
						'type'    => 'select',
						'label'   => __( 'Animate content from (on hover)', 'woopack' ),
						'default' => 'bottom',
						'options' => array(
							'top'    => __( 'Top', 'woopack' ),
							'bottom' => __( 'Bottom', 'woopack' ),
						),
					),
					'style_7_animation'   => array(
						'type'    => 'select',
						'label'   => __( 'Border on hover', 'woopack' ),
						'default' => 'tr-bl',
						'options' => array(
							'tr-bl' => __( 'Top-Right & Bottom-Left', 'woopack' ),
							'tl-br' => __( 'Top-Left & Bottom-Right', 'woopack' ),
						),
					),
					'style_8_animation'   => array(
						'type'    => 'select',
						'label'   => __( 'Animation Type', 'woopack' ),
						'default' => 'in',
						'options' => array(
							'in'  => __( 'Fade In', 'woopack' ),
							'out' => __( 'Fade Out', 'woopack' ),
						),
					),
					'transition_speed' => array(
						'type'        => 'text',
						'label'       => __( 'Transition Speed', 'woopack' ),
						'description' => __('second', 'woopack'),
						'size'        => '5',
						'default'     => '0.3',
					),
				),
			),
			'content'          => array(
				'title'  => __( 'Content', 'woopack' ),
				'fields' => array(
					'category_show_counter' => array(
						'type'    => 'select',
						'label'   => __( 'Show Product Counter?', 'woopack' ),
						'default' => 'yes',
						'options' => array(
							'yes' => __( 'Yes', 'woopack' ),
							'no'  => __( 'No', 'woopack' ),
						),
						'toggle'  => array(
							'yes' => array(
								'sections' => array( 'category_count_fonts' ),
							),
						),
					),
					'category_show_button'  => array(
						'type'    => 'select',
						'label'   => __( 'Show Button?', 'woopack' ),
						'default' => 'yes',
						'options' => array(
							'yes' => __( 'Yes', 'woopack' ),
							'no'  => __( 'No', 'woopack' ),
						),
						'toggle'  => array(
							'yes' => array(
								'tabs'   => array( 'button' ),
								'fields' => array( 'category_button_text' ),
							),
						),
					),
					'category_button_text'  => array(
						'type'    => 'text',
						'label'   => __( 'Button Text', 'woopack' ),
						'default' => __('Shop Now', 'woopack'),
						'preview'	=> array(
							'type'		=> 'text',
							'selector'	=> '.woopack-product-category .woopack-product-category__button'
						)
					),
					'category_link_target'  => array(
						'type'    => 'select',
						'label'   => __( 'Link Target', 'woopack' ),
						'options' => array(
							'_self'  => __( 'Same Window', 'woopack' ),
							'_blank' => __( 'New Window', 'woopack' ),
						),
					),
				),
			),
		),
	),
	'content'	=> array(
		'title'		=> __('Content', 'woopack'),
		'file'      => WOOPACK_DIR . 'modules/product-category/includes/settings-content.php',
	),
	'button'     => array(
		'title'    => __( 'Button', 'woopack' ),
		'sections' => woopack_product_button_fields(),
	),
	'style'      => array(
		'title'    => __( 'Style', 'woopack' ),
		'sections' => array(
			'color'      => array(
				'title'  => __( 'Overlay', 'woopack' ),
				'fields' => array(
					'category_bg_color'   => array(
						'type'       => 'color',
						'label'      => __( 'Background Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
						'preview'		=> array(
							'type'			=> 'css',
							'selector'		=> '.woopack-product-category .woopack-product-category__link',
							'property'		=> 'background-color'
						)
					),
					'category_bg_color_hover'   => array(
						'type'       	=> 'color',
						'label'      	=> __( 'Background Hover Color', 'woopack' ),
						'default'    	=> '',
						'show_reset' 	=> true,
						'preview'		=> array(
							'type'			=> 'none',
						)
					),
					'category_bg_opacity' => array(
						'type'        => 'text',
						'label'       => __( 'Image Opacity', 'woopack' ),
						'default'     => '0.5',
						'description' => __('between 0 to 1', 'woopack'),
						'size'        => '5',
						'preview'		=> array(
							'type'			=> 'css',
							'selector'		=> '.woopack-product-category .product-category-inner .woopack-product-category__img img',
							'property'		=> 'opacity'
						)
					),
				),
			),
			'separator'	 => array(
				'title'  => __( 'Separator', 'woopack' ),
				'fields' => array(
					'category_separator_color' => array(
						'type'       => 'color',
						'label'      => __( 'Color', 'woopack' ),
						'default'    => 'ffffff',
						'show_reset' => true,
						'show_alpha' => true,
					),
					'category_separator_height' => array(
						'type'        => 'text',
						'label'       => __( 'Height', 'woopack' ),
						'size'        => '5',
						'default'     => '2',
						'description' => 'px',
					),
				),
			),
			'border'     => array(
				'title'  => __( 'Border', 'woopack' ),
				'fields' => array(
					'category_border_style'  => array(
						'type'    => 'select',
						'label'   => __( 'Border Style', 'woopack' ),
						'default' => 'solid',
						'options' => array(
							'none'    => __( 'None', 'woopack' ),
							'solid'   => __( 'Solid', 'woopack' ),
							'dotted'  => __( 'Dotted', 'woopack' ),
							'dashed'  => __( 'Dashed', 'woopack' ),
						),
						'toggle'  => array(
							'solid'  => array(
								'fields' => array( 'category_border_width', 'category_border_color' ),
							),
							'dotted' => array(
								'fields' => array( 'category_border_width', 'category_border_color' ),
							),
							'dashed' => array(
								'fields' => array( 'category_border_width', 'category_border_color' ),
							),
						),
					),
					'category_border_width'  => array(
						'type'        => 'unit',
						'label'       => __( 'Border Width', 'woopack' ),
						'description' => 'px',
						'default'     => '1',
						'preview'     => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category',
							'property' => 'border-width',
							'unit'     => 'px',
						),
						'responsive'  => array(
							'placeholder' => array(
								'default'    => '',
								'medium'     => '',
								'responsive' => '',
							),
						),
					),
					'category_border_color'  => array(
						'type'       => 'color',
						'label'      => __( 'Border Color', 'woopack' ),
						'default'    => '000000',
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category',
							'property' => 'border-color',
						),
					),
					'category_border_radius' => array(
						'type'        => 'unit',
						'label'       => __( 'Round Corners', 'woopack' ),
						'description' => 'px',
						'default'     => '0',
						'preview'     => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category',
							'property' => 'border-radius',
							'unit'     => 'px',
						),
						'responsive'  => array(
							'placeholder' => array(
								'default'    => '',
								'medium'     => '',
								'responsive' => '',
							),
						),
					),
				),
			),
			'box_shadow'	=> array(
				'title'			=> __('Box Shadow', 'bb-powerpack'),
				'fields'		=> array(
					'box_shadow_h'	=> array(
						'type'			=> 'text',
						'label'			=> __('Horizontal', 'bb-powerpack'),
						'description'	=> 'px',
						'default'		=> '0',
						'size'			=> '5'
					),
					'box_shadow_v'	=> array(
						'type'			=> 'text',
						'label'			=> __('Vertical', 'bb-powerpack'),
						'description'	=> 'px',
						'default'		=> '0',
						'size'			=> '5'
					),
					'box_shadow_blur'	=> array(
						'type'			=> 'text',
						'label'			=> __('Blur', 'bb-powerpack'),
						'description'	=> 'px',
						'default'		=> '0',
						'size'			=> '5'
					),
					'box_shadow_spread'	=> array(
						'type'			=> 'text',
						'label'			=> __('Spread', 'bb-powerpack'),
						'description'	=> 'px',
						'default'		=> '0',
						'size'			=> '5'
					),
					'box_shadow_color'	=> array(
						'type'				=> 'color',
						'label'				=> __('Color', 'bb-powerpack'),
						'default'			=> 'rgba(0, 0, 0, 0.2)',
						'show_alpha'		=> true
					)
				)
			),
			'padding'    => array(
				'title'  => __( 'Padding', 'woopack' ),
				'fields' => array(
					'category_padding'    => array(
						'type'        => 'dimension',
						'label'       => __( 'Padding', 'woopack' ),
						'description' => 'px',
						'responsive'  => array(
							'placeholder' => array(
								'default'    => '',
								'medium'     => '',
								'responsive' => '',
							),
						),
						'preview'     => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__content',
							'property' => 'padding',
							'unit'     => 'px',
						),
					),
				),
			),
			'margin'     => array(
				'title'  => __( 'Margin', 'woopack' ),
				'fields' => array(
					'category_margin'    => array(
						'type'        => 'dimension',
						'label'       => __( 'Margin', 'woopack' ),
						'description' => 'px',
						'responsive'  => array(
							'placeholder' => array(
								'default'    => '',
								'medium'     => '',
								'responsive' => '',
							),
						),
						'preview'     => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__content',
							'property' => 'margin',
							'unit'     => 'px',
						),
					),
				),
			),
			'sh_height'  => array(
				'title'  => __( 'Shutter Height', 'woopack' ),
				'fields' => array(
					'shutter_height' => array(
						'type'        => 'unit',
						'label'       => __( 'Height', 'woopack' ),
						'default'     => '50',
						'description' => 'px',
						'responsive'  => array(
							'placeholder' => array(
								'default'    => '',
								'medium'     => '',
								'responsive' => '',
							),
						),
					),
				),
			),
			'des_margin' => array(
				'title'  => __( 'Description Margin', 'woopack' ),
				'fields' => array(
					'des_margin_top' => array(
						'type'        => 'unit',
						'label'       => __( 'Margin Top', 'woopack' ),
						'description' => 'px',
						'responsive'  => array(
							'placeholder' => array(
								'default'    => '',
								'medium'     => '',
								'responsive' => '',
							),
						),
						'preview'     => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__description',
							'property' => 'margin-top',
							'unit'     => 'px',
						),
					),
				),
			),
		),
	),
	'typography' => array(
		'title'    => __( 'Typography', 'woopack' ),
		'sections' => array(
			'category_title_fonts'       => array(
				'title'  => __( 'Category Title', 'woopack' ),
				'fields' => array(
					'category_title_tag'	=> array(
						'type'		=> 'select',
						'label'		=> __('Tag', 'woopack'),
						'default'	=> 'h3',
						'options'	=> array(
							'h1'		=> 'h1',
							'h2'		=> 'h2',
							'h3'		=> 'h3',
							'h4'		=> 'h4',
							'h5'		=> 'h5',
							'h6'		=> 'h6',
						)
					),
					'category_title_font'             => array(
						'type'    => 'font',
						'label'   => __( 'Font', 'woopack' ),
						'default' => array(
							'family' => 'Default',
							'weight' => 300,
						),
						'preview' => array(
							'type'     => 'font',
							'selector' => '.woopack-product-category .woopack-product-category__title',
							'property' => 'font-family',
						),
					),
					'category_title_font_size'        => array(
						'type'    => 'select',
						'label'   => __( 'Font Size', 'woopack' ),
						'default' => 'default',
						'options' => array(
							'default' => __( 'Default', 'woopack' ),
							'custom'  => __( 'Custom', 'woopack' ),
						),
						'toggle'  => array(
							'custom' => array(
								'fields' => array( 'category_title_font_size_custom' ),
							),
						),
					),
					'category_title_font_size_custom' => array(
						'type'        => 'unit',
						'label'       => __( 'Custom Font Size', 'woopack' ),
						'description' => 'px',
						'responsive'  => array(
							'placeholder' => array(
								'default'    => '15',
								'medium'     => '',
								'responsive' => '',
							),
						),
						'preview'     => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__title',
							'property' => 'font-size',
							'unit'     => 'px',
						),
					),
					'category_title_line_height'      => array(
						'type'       => 'unit',
						'label'      => __( 'Line Height', 'woopack' ),
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__title',
							'property' => 'line-height',
						),
						'responsive' => array(
							'placeholder' => array(
								'default'    => '',
								'medium'     => '',
								'responsive' => '',
							),
						),
					),
					'category_title_text_transform'   => array(
						'type'    => 'select',
						'label'   => __( 'Text Transform', 'woopack' ),
						'default' => 'default',
						'options' => array(
							'default'    => __( 'Default', 'woopack' ),
							'none'       => __( 'None', 'woopack' ),
							'capitalize' => __( 'Capitalize', 'woopack' ),
							'lowercase'  => __( 'lowercase', 'woopack' ),
							'uppercase'  => __( 'UPPERCASE', 'woopack' ),
						),
						'preview' => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__title',
							'property' => 'text-transform',
						),
					),
					'category_title_color'            => array(
						'type'       => 'color',
						'label'      => __( 'Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__title',
							'property' => 'color',
						),
					),
					'category_title_hover_color'      => array(
						'type'       => 'color',
						'label'      => __( 'Hover Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'none',
						),
					),
				),
			),
			'category_count_fonts'       => array(
				'title'  => __( 'Category Count', 'woopack' ),
				'fields' => array(
					'category_count_font_size'        => array(
						'type'    => 'select',
						'label'   => __( 'Font Size', 'woopack' ),
						'default' => 'default',
						'options' => array(
							'default' => __( 'Default', 'woopack' ),
							'custom'  => __( 'Custom', 'woopack' ),
						),
						'toggle'  => array(
							'custom' => array(
								'fields' => array( 'category_count_font_size_custom' ),
							),
						),
					),
					'category_count_font_size_custom' => array(
						'type'        => 'unit',
						'label'       => __( 'Custom Font Size', 'woopack' ),
						'description' => 'px',
						'responsive'  => array(
							'placeholder' => array(
								'default'    => '15',
								'medium'     => '',
								'responsive' => '',
							),
						),
						'preview'     => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__title span',
							'property' => 'font-size',
							'unit'     => 'px',
						),
					),
					'category_count_color'            => array(
						'type'       => 'color',
						'label'      => __( 'Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__title span',
							'property' => 'color',
						),
					),
					'category_count_hover_color'      => array(
						'type'       => 'color',
						'label'      => __( 'Hover Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'none',
						),
					),
				),
			),
			'category_description_fonts' => array(
				'title'  => __( 'Category Description', 'woopack' ),
				'fields' => array(
					'category_description_font'           => array(
						'type'    => 'font',
						'label'   => __( 'Font', 'woopack' ),
						'default' => array(
							'family' => 'Default',
							'weight' => 300,
						),
						'preview' => array(
							'type'     => 'font',
							'selector' => '.woopack-product-category .woopack-product-category__description',
							'property' => 'font-family',
						),
					),
					'category_description_font_size'      => array(
						'type'    => 'select',
						'label'   => __( 'Font Size', 'woopack' ),
						'default' => 'default',
						'options' => array(
							'default' => __( 'Default', 'woopack' ),
							'custom'  => __( 'Custom', 'woopack' ),
						),
						'toggle'  => array(
							'custom' => array(
								'fields' => array( 'category_description_font_size_custom' ),
							),
						),
					),
					'category_description_font_size_custom' => array(
						'type'        => 'unit',
						'label'       => __( 'Custom Font Size', 'woopack' ),
						'description' => 'px',
						'responsive'  => array(
							'placeholder' => array(
								'default'    => '15',
								'medium'     => '',
								'responsive' => '',
							),
						),
						'preview'     => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__description',
							'property' => 'font-size',
							'unit'     => 'px',
						),
					),
					'category_description_line_height'    => array(
						'type'       => 'unit',
						'label'      => __( 'Line Height', 'woopack' ),
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__description',
							'property' => 'line-height',
						),
						'responsive' => array(
							'placeholder' => array(
								'default'    => '',
								'medium'     => '',
								'responsive' => '',
							),
						),
					),
					'category_description_text_transform' => array(
						'type'    => 'select',
						'label'   => __( 'Text Transform', 'woopack' ),
						'default' => 'default',
						'options' => array(
							'default'    => __( 'Default', 'woopack' ),
							'none'       => __( 'None', 'woopack' ),
							'capitalize' => __( 'Capitalize', 'woopack' ),
							'lowercase'  => __( 'lowercase', 'woopack' ),
							'uppercase'  => __( 'UPPERCASE', 'woopack' ),
						),
						'preview' => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__description',
							'property' => 'text-transform',
						),
					),
					'category_description_color'          => array(
						'type'       => 'color',
						'label'      => __( 'Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__description',
							'property' => 'color',
						),
					),
				),
			),
		),
	),
));
