.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .add_to_cart_inline {
	border: none !important;
}
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .amount,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart del,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart ins {
	<?php if ( 'no' == $settings->product_price ) { ?>
		display: none !important;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .product {
	text-align: <?php echo $settings->align; ?>;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-product-price {
	margin-bottom: 10px;
	<?php if ( isset( $settings->qty_input ) && 'no' == $settings->qty_input ) { ?>
		display: inline-block;
	<?php } ?>
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-product-action {
	<?php if ( isset( $settings->qty_input ) && 'no' == $settings->qty_input ) { ?>
		display: inline-block;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-product-action .woopack-product-action-inner {
	display: inline-block;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-qty-input,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .variations_form .quantity {
	<?php if ( isset( $settings->qty_input ) ) { ?>
		<?php if ( 'no' == $settings->qty_input ) { ?>
			display: none !important;
		<?php } ?>
	<?php } ?>
	width: 60px;
	float: left;
}
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-qty-input input.qty,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .variations_form .quantity input.qty {
	width: 100%;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .variations_form table.variations {
	<?php if ( 'default' == $settings->align || 'center' == $settings->align ) { ?>
	margin: 0 auto;
	<?php } ?>
	<?php if ( 'right' == $settings->align ) { ?>
	margin: 0 0 0 auto;
	<?php } ?>
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .variations_form .label {
	color: inherit;
}
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .variations_form .label label {
	font-size: 12px;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .variations_form .reset_variations {
	margin-left: 5px;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woocommerce-variation-add-to-cart {
	display: inline-block;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woocommerce-variation {
	margin-top: 10px;
	margin-bottom: 10px;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart a.button,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart a.button.alt,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart a.added_to_cart,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart button,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .button,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart button.button,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart button.alt,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .button.alt,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart button.button.alt {
	<?php WooPack_Helper::print_css( 'color', $settings->button_color ); ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'padding-top', $settings->button_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->button_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->button_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->button_padding_right, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->button_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->button_font_size_custom, 'px', 'custom' == $settings->button_font_size ); ?>
	<?php if ( 'none' != $settings->button_border_style ) { ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius, 'px' ); ?>
	<?php if ( 'full_width' == $settings->button_width ) { ?>
		width: 100%;
	<?php } elseif ( 'custom' == $settings->button_width && '' != $settings->button_width_custom ) { ?>
		width: <?php echo $settings->button_width_custom; ?>%;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->button_text_transform, '', 'default' != $settings->button_text_transform ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->button_line_height ); ?>
	<?php if ( 'yes' == $settings->product_price ) { ?>
		<?php WooPack_Helper::print_css( 'margin-left', $settings->price_spacing, 'px' ); ?>
	<?php } ?>
	text-align: center;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart a.button:hover,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart a.button.alt:hover,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart a.added_to_cart:hover,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart button:hover,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .button:hover,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart button.button:hover,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart button.alt:hover,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .button.alt:hover,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart button.button.alt:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color_hover ); ?>

	<?php if ( 'none' != $settings->button_border_style ) { ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color_hover ); ?>
	<?php } else { ?>
		border: none;
<?php } ?>
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .single_variation_wrap button {
	float: left;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .product .amount {
	<?php WooPack_Helper::print_css( 'font', $settings->ragular_price_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->ragular_price_font_size, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->ragular_price_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .product del .amount {
	text-decoration: line-through;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .product ins {
	text-decoration: none;
}
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .product ins .amount {
	text-decoration: none;
	<?php WooPack_Helper::print_css( 'font-size', $settings->sale_price_font_size, 'px' ); ?>
<?php WooPack_Helper::print_css( 'color', $settings->sale_price_color ); ?>
}

@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart a.button
	.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart a.added_to_cart {
		<?php if ( 'none' != $settings->button_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width_medium, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius_medium, 'px' ); ?>
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart a.button
	.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart a.added_to_cart {
		<?php if ( 'none' != $settings->button_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width_responsive, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius_responsive, 'px' ); ?>
	}
}
