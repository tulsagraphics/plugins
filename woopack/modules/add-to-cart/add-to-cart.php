<?php
/**
* @class WooPackAddToCart
*/
class WooPackAddToCart extends FLBuilderModule {
    /**
    * Constructor function for the module. You must pass the
    * name, description, dir and url in an array to the parent class.
    *
    * @method __construct
    */
    public function __construct()
    {
        parent::__construct(array(
            'name' 				=> __('Add To Cart', 'woopack'),
            'description' 		=> __('Addon to display Add To Cart.', 'woopack'),
            'group'             => WooPack_Helper::get_modules_group(),
            'category' 			=> WOOPACK_CAT,
            'dir' 				=> WOOPACK_DIR . 'modules/add-to-cart/',
            'url' 				=> WOOPACK_URL . 'modules/add-to-cart/',
            'editor_export' 	=> true, // Defaults to true and can be omitted.
            'enabled' 			=> true, // Defaults to true and can be omitted.
        ));
    }
}
/**
* Register the module and its form settings.
*/
FLBuilder::register_module('WooPackAddToCart', array(
    'structure'     => array(
        'title'         => __('Structure', 'woopack'),
        'sections'      => array(
            'product'       => array(
                'title'         => __('Product', 'woopack'),
                'fields'        => array(
                    'product_id'    => array(
                        'type'          => 'select',
                        'label' 		=> __('Product', 'woopack'),
                        'options'       => WooPack_Helper::get_products_list()
					),
                ),
            ),
            'alignment'     => array(
                'title'         => __('Alignment', 'woopack'),
                'fields'        => array(
                    'align'          => array(
                        'type'          => 'select',
                        'label'         => __('Alignment', 'woopack'),
                        'default'       => 'center',
                        'options'       => array(
                            'left'          => __('Left', 'woopack'),
                            'center'        => __('Center', 'woopack'),
                            'right'         => __('Right', 'woopack'),
                        ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.woopack-product-add-to-cart .product',
                            'property'      => 'text-align',
                        ),
                    ),
                ),
            ),
            'product_price' => array(
                'title'         => __('Price', 'woopack'),
                'fields'        => array(
                    'product_price' => array(
                        'type'          => 'select',
                        'label'         => __('Show Price?', 'woopack'),
                        'default'       => 'yes',
                        'options'       => array(
                            'yes'           => __('Yes', 'woopack'),
                            'no'            => __('No', 'woopack'),
                        ),
                        'toggle'         => array(
                            'yes'           => array(
                                'sections'      => array( 'ragular_price_fonts', 'sale_price_fonts' ),
                            ),
                        ),
					),
					'variation_fields'	=> array(
						'type'          => 'select',
                        'label'         => __('Show Variation Fields?', 'woopack'),
                        'default'       => 'no',
                        'options'       => array(
                            'yes'           => __('Yes', 'woopack'),
                            'no'            => __('No', 'woopack'),
                        ),
					),
					'qty_input'		=> array(
						'type'			=> 'select',
						'label'			=> __('Show Quantity?', 'woopack'),
						'default'		=> 'no',
						'options'		=> array(
							'yes'           => __('Yes', 'woopack'),
                            'no'            => __('No', 'woopack'),
						)
					),
                    'price_spacing' => array(
                        'type'                  => 'unit',
                        'label'                 => __('Spacing', 'woopack'),
                        'default'               => '5',
                        'description' 			=> 'px',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '',
                                'medium' 				=> '',
                                'responsive' 			=> '',
                            ),
                        ),
                    )
                ),
            ),
        ),
    ),
    'style'         => array(
        'title'         => __('Style', 'woopack'),
        'sections'      => array(
            'button_property'   => array(
                'title'             => __('Structure', 'woopack'),
                'fields'            => array(
                    'button_width'          => array(
                        'type'                  => 'select',
                        'label'                 => __('Width', 'woopack'),
                        'default'               => 'auto',
                        'options'               => array(
                            'auto'                  => __('Auto', 'woopack'),
                            'full_width'            => __('Full Width', 'woopack'),
                            'custom'                => __('Custom', 'woopack'),
                        ),
                        'toggle'                => array(
                            'custom'                => array(
                                'fields'                => array('button_width_custom')
                            ),
                        ),
                    ),
                    'button_width_custom'   => array(
                        'type'                  => 'unit',
                        'label'                 => __('Custom Width', 'woopack'),
                        'description' 			=> '%',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '45',
                                'medium' 				=> '',
                                'responsive' 			=> '',
                            ),
                        ),
                    ),
                ),
            ),
            'button_color'      => array(
                'title'             => __('Colors', 'woopack'),
                'fields'            => array(
                    'button_bg_color'		=> array(
                        'type'                  => 'color',
                        'label'                 => __('Background Color', 'woopack'),
                        'show_reset'            => true,
                        'show_alpha'            => true,
                        'preview'               => array(
                            'type' 				 => 'css',
                            'selector'			   => '.woopack-product-add-to-cart .button',
                            'property'			   => 'background-color',
                        ),
                    ),
                    'button_bg_color_hover' => array(
                        'type'                  => 'color',
                        'label'                 => __('Background Hover Color', 'woopack'),
                        'show_reset'            => true,
                        'show_alpha'            => true,
                        'preview'               => array(
                            'type' 				 => 'css',
                            'selector'			   => '.woopack-product-add-to-cart .button:hover',
                            'property'			   => 'background-color',
                        ),
                    ),
                    'button_color'          => array(
                        'type'                  => 'color',
                        'label'                 => __('Text Color', 'woopack'),
                        'show_reset'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woopack-product-add-to-cart .button',
                            'property'              => 'color',
                        ),
                    ),
                    'button_color_hover'    => array(
                        'type'                  => 'color',
                        'label'                 => __('Text Hover Color', 'woopack'),
                        'show_reset'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woopack-product-add-to-cart .button:hover',
                            'property'              => 'color',
                        ),
                    ),
                ),
            ),
            'button_border'     => array(
                'title'             => __('Border', 'woopack'),
                'fields'            => array(
                    'button_border_style'       => array(
                        'type'                      => 'select',
                        'label'                     => __('Border Style', 'woopack'),
                        'default'                   => 'none',
                        'options'                   => array(
                            'none'                      => __('None', 'woopack'),
                            'solid'                     => __('Solid', 'woopack'),
                            'dotted'                    => __('Dotted', 'woopack'),
                            'dashed'                    => __('Dashed', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'solid'                     => array(
                                'fields'                        => array( 'button_border_width', 'button_border_color', 'button_border_color_hover'),
                            ),
                            'dotted'                    => array(
                                'fields'                        => array( 'button_border_width', 'button_border_color', 'button_border_color_hover' ),
                            ),
                            'dashed'                    => array(
                                'fields'                        => array( 'button_border_width', 'button_border_color', 'button_border_color_hover'),
                            ),
                        ),
                    ),
                    'button_border_width'       => array(
                        'type'                      => 'unit',
                        'default'                   => '1',
                        'label'                     => __('Border Width', 'woopack'),
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woopack-product-add-to-cart .button',
                            'property'                  => 'border-width',
                            'unit'                      => 'px',
                        ),
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                    ),
                    'button_border_color'       => array(
                        'type'                      => 'color',
                        'label'                     => __('Border Color', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woopack-product-add-to-cart .button',
                            'property'                  => 'border-color',
                        ),
                    ),
                    'button_border_color_hover' => array(
                        'type'                      => 'color',
                        'label'                     => __('Border Hover Color', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woopack-product-add-to-cart .button:hover',
                            'property'                  => 'border-color',
                        ),
                    ),
                    'button_border_radius'      => array(
                        'type'                      => 'unit',
                        'label'                     => __('Round Corners', 'woopack'),
                        'description'               => 'px',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woopack-product-add-to-cart .button',
                            'property'                  => 'border-radius',
                            'unit'                      => 'px',
                        ),
                        'responsive' 			    => array(
                            'placeholder'               => array(
                                'default'                   => '',
                                'medium' 				    => '',
                                'responsive'			    => '',
                            ),
                        ),
                    ),
                ),
            ),
            'button_padding'    => array(
                'title'             => __('Padding', 'woopack'),
                'fields'            => array(
                    'button_padding'    => array(
                        'type'                  => 'dimension',
                        'label'                 => __('Padding', 'woopack'),
                        'description'           => 'px',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woopack-product-add-to-cart .button',
                            'property'              => 'padding',
                            'unit'                  => 'px'
                        ),
                        'responsive'            => array(
                            'placeholder'           => array(
                                'default'               => '',
                                'medium'                => '',
                                'responsive'            => '',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'typography'    => array(
        'title'         => __('Typography', 'woopack'),
        'sections' 	    => array(
            'button_fonts'          => array(
                'title'                 => __('Button', 'woopack'),
                'fields'                => array(
                    'button_font'               => array(
                        'type'                      => 'font',
                        'label'                     => __('Font', 'woopack'),
                        'default' 		            => array(
                            'family'                    => 'Default',
                            'weight'                    => 300
                        ),
                        'preview'                   => array(
                            'type'                      => 'font',
                            'selector'                  => '.woopack-product-add-to-cart .button',
                            'property'		            => 'font-family',
                        ),
                    ),
                    'button_font_size'          => array(
                        'type'                      => 'select',
                        'label'  			        => __('Font Size', 'woopack'),
                        'default'                   => 'default',
                        'options' 		            => array(
                            'default' 		            => __('Default', 'woopack'),
                            'custom'                    => __('Custom', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'custom'                    => array(
                                'fields'                    => array('button_font_size_custom')
                            ),
                        ),
                    ),
                    'button_font_size_custom'   => array(
                        'type'                      => 'unit',
                        'label'                     => __('Custom Font Size', 'woopack'),
                        'description'               => 'px',
                        'responsive' 			    => array(
                            'placeholder'               => array(
                                'default'                   => '15',
                                'medium' 				    => '',
                                'responsive' 			    => '',
                            ),
                        ),
                        'preview'				=> array(
                            'type' 					=> 'css',
                            'selector'				=> '.woopack-product-add-to-cart .button',
                            'property'      		=> 'font-size',
                            'unit'					=> 'px',
                        ),
                    ),
                    'button_text_transform'     => array(
                        'type'                      => 'select',
                        'label' 		            => __('Text Transform', 'woopack'),
                        'default'                   => 'default',
                        'options'                   => array(
                            'default' 		            => __('Default', 'woopack'),
                            'capitalize' 		        => __('Capitalize', 'woopack'),
                            'lowercase'                 => __('Lowercase', 'woopack'),
                            'uppercase'                 => __('Uppercase', 'woopack'),
                        ),
                        'preview' 		            => array(
                            'type'                      => 'css',
                            'selector'                  => '.woopack-product-add-to-cart .button',
                            'property'                  => 'text-transform',
                        ),
                    ),
                    'button_line_height'        => array(
                        'type'                      => 'text',
                        'label'                     => __('Line Height', 'woopack'),
                        'size'                      => '5',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woopack-product-add-to-cart .button',
                            'property'                  => 'line-height',
                        ),
                    ),
                ),
            ),
            'ragular_price_fonts'   => array(
                'title'                 => __('Ragular Price', 'woopack'),
                'fields'                => array(
                    'ragular_price_font'        => array(
                        'type'                      => 'font',
                        'label'                     => __('Font', 'woopack'),
                        'default' 		            => array(
                            'family'                    => 'Default',
                            'weight'                    => 300
                        ),
                        'preview'                   => array(
                            'type'                      => 'font',
                            'selector'                  => '.product .amount',
                            'property'		            => 'font-family',
                        ),
                    ),
                    'ragular_price_font_size'   => array(
                        'type'                      => 'unit',
                        'label'                     => __('Font Size', 'woopack'),
                        'description'               => 'px',
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '15',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.product .amount',
                            'property'                  => 'font-size',
                            'unit'                      => 'px',
                        ),
                    ),
                    'ragular_price_color'       => array(
                        'type'                      => 'color',
                        'label'                     => __('Color', 'woopack'),
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.product .amount',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'sale_price_fonts'      => array(
                'title'                 => __('Sale Price', 'woopack'),
                'fields'                => array(
                    'sale_price_font_size'  => array(
                        'type'                  => 'unit',
                        'label'                 => __('Font Size', 'woopack'),
                        'description' 			=> 'px',
                        'responsive' 			=> array(
                            'placeholder' 			=> array(
                                'default' 				=> '15',
                                'medium' 				=> '',
                                'responsive' 			=> '',
                            ),
                        ),
                        'preview'				=> array(
                            'type' 					=> 'css',
                            'selector'				=> '.product ins .amount',
                            'property'      		=> 'font-size',
                            'unit'					=> 'px',
                        ),
                    ),
                    'sale_price_color'      => array(
                        'type'                  => 'color',
                        'label'                 => __('Color', 'woopack'),
                        'show_reset'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.product ins .amount',
                            'property'              => 'color',
                        ),
                    ),
                ),
            ),
        ),
    ),
));
