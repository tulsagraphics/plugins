<?php

define( 'WOOPACK_SL_URL', 'https://wpbeaveraddons.com' );

define( 'WOOPACK_ITEM_NAME', 'WooPack for Beaver Builder' );

if( !class_exists( 'WooPack_Plugin_Updater' ) ) {
	// load our custom updater
	include('class-plugin-updater.php' );
}

function woopack_get( $key ) {
	if ( is_network_admin() ) {
		return get_site_option( $key );
	} else {
		return get_option( $key );
	}
}

function woopack_update( $key, $value ) {
	if ( is_network_admin() ) {
		return update_site_option( $key, $value );
	} else {
		return update_option( $key, $value );
	}
}

function woopack_delete( $key ) {
	if ( is_network_admin() ) {
		return delete_site_option( $key );
	} else {
		return delete_option( $key );
	}
}

function woopack_plugin_updater() {

	// retrieve our license key from the DB
	$license_key = trim( woopack_get( 'woopack_license_key' ) );

	// setup the updater
	$edd_updater = new WooPack_Plugin_Updater( WOOPACK_SL_URL, WOOPACK_DIR . '/woopack.php', array(
			'version' 	=> WOOPACK_VER, 					// current version number
			'license' 	=> $license_key, 						// license key (used woopack_get above to retrieve from DB)
			'item_name' => WOOPACK_ITEM_NAME, 			// name of this plugin
			'author' 	=> 'Team IdeaBox - Beaver Addons' 	// author of this plugin
		)
	);

}
add_action( 'admin_init', 'woopack_plugin_updater', 0 );

/************************************
* this illustrates how to activate
* a license key
*************************************/

function woopack_activate_license() {

	// listen for our activate button to be clicked
	if( isset( $_POST['woopack_license_key'] ) ) {

		// run a quick security check
	 	if( ! isset( $_POST['woopack_nonce'] ) || ! wp_verify_nonce( $_POST['woopack_nonce'], 'woopack_nonce' ) )
			return; // get out if we didn't click the Activate button

		// retrieve the license from the database
		$license = trim( $_POST['woopack_license_key'] );

		// data to send in our API request
		$api_params = array(
			'edd_action'=> 'activate_license',
			'license' 	=> $license,
			'item_name' => urlencode( WOOPACK_ITEM_NAME ), // the name of our product in EDD
			'url'       => network_home_url()
		);

		// Call the custom API.
		$response = wp_remote_post( WOOPACK_SL_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

		// make sure the response came back okay
		if ( is_wp_error( $response ) )
			return false;

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		// $license_data->license will be either "valid" or "invalid"

		woopack_update( 'woopack_license_status', $license_data->license );

	}
}
add_action('admin_init', 'woopack_activate_license');

/***********************************************
* Illustrates how to deactivate a license key.
* This will descrease the site count
***********************************************/

function woopack_deactivate_license() {

	// listen for our activate button to be clicked
	if( isset( $_POST['woopack_license_deactivate'] ) && isset( $_POST['woopack_license_key'] ) ) {

		// run a quick security check
	 	if( ! isset( $_POST['woopack_nonce'] ) || ! wp_verify_nonce( $_POST['woopack_nonce'], 'woopack_nonce' ) )
			return; // get out if we didn't click the Activate button

		// retrieve the license from the database
		$license = trim( $_POST['woopack_license_key'] );

		// data to send in our API request
		$api_params = array(
			'edd_action'=> 'deactivate_license',
			'license' 	=> $license,
			'item_name' => urlencode( WOOPACK_ITEM_NAME ), // the name of our product in EDD
			'url'       => network_home_url()
		);

		// Call the custom API.
		$response = wp_remote_post( WOOPACK_SL_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

		// make sure the response came back okay
		if ( is_wp_error( $response ) )
			return false;

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		// $license_data->license will be either "deactivated" or "failed"
		if( $license_data->license == 'deactivated' )
			woopack_delete( 'woopack_license_status' );

	}
}
add_action('admin_init', 'woopack_deactivate_license');

/************************************
* check if
* a license key is still valid
* so this is only needed if we
* want to do something custom
*************************************/

function woopack_check_license() {

	global $wp_version;

	$license = trim( woopack_get( 'woopack_license_key' ) );
	$license_status = trim( woopack_get( 'woopack_license_status' ) );

	if ( '' == $license_status || 'invalid' == $license_status ) {
		//return 'invalid';
	}

	$api_params = array(
		'edd_action' => 'check_license',
		'license' 	=> $license,
		'item_name' => urlencode( WOOPACK_ITEM_NAME ),
		'url'       => network_home_url()
	);

	// Call the custom API.
	$response = wp_remote_post( WOOPACK_SL_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

	if ( is_wp_error( $response ) )
		return false;

	$license_data = json_decode( wp_remote_retrieve_body( $response ) );

	if( $license_data->license == 'valid' ) {
		return 'valid';
		// this license is still valid
	} else {
		return 'invalid';
		// this license is no longer valid
	}
}

 /**
  * Show update message on plugins page
  */
function woopack_update_message( $plugin_data, $response ) {
	if ( 'invalid' == woopack_check_license() ) {

		$message  = '';
		$message .= '<p style="padding: 5px 10px; margin-top: 10px; background: #d54e21; color: #fff;">';
		$message .= __( '<strong>UPDATE UNAVAILABLE!</strong>', 'woopack' );
		$message .= '&nbsp;&nbsp;&nbsp;';
		$message .= __( 'Please activate the license key to enable automatic updates for this plugin.', 'woopack' );

		$message .= ' <a href="' . WOOPACK_SL_URL . '" target="_blank" style="color: #fff; text-decoration: underline;">';
		$message .= __( 'Buy Now', 'woopack' );
		$message .= ' &raquo;</a>';

		$message .= '</p>';

		echo $message;
	}
}
add_action( 'in_plugin_update_message-' . WOOPACK_PATH, 'woopack_update_message', 1, 2 );
