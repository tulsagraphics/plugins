<?php

/**
 * Button display option for product.
 *
 * @since 1.0.0
 */

function woopack_product_button_fields()
{
	return apply_filters( 'woopack_product_button_fields', array(
		'button_structure'   => array(
			'title'             => __('Structure', 'woopack'),
			'fields'            => array(
				'button_alignment'		=> array(
					'type'              	=> 'select',
					'label'             	=> __('Alignment', 'woopack'),
					'default'           	=> 'default',
					'options'           	=> array(
						'default'              	=> __('Default', 'woopack'),
						'left'              	=> __('Left', 'woopack'),
						'center'            	=> __('Center', 'woopack'),
						'right'             	=> __('Right', 'woopack')
					),
					'preview'           	=> array(
						'type'              	=> 'css',
						'selector'          	=> '.woocommerce ul.products .woopack-product-action, .woocommerce div.products .woopack-product-action, #payment .form-row.place-order, .woopack-product-category .woopack-product-category__button',
						'property'          	=> 'text-align',
					),
				),
				'button_width'          => array(
					'type'                  => 'select',
					'label'                 => __('Width', 'woopack'),
					'default'               => 'auto',
					'options'               => array(
						'auto'                  => __('Auto', 'woopack'),
						'full_width'            => __('Full Width', 'woopack'),
						'custom'                => __('Custom', 'woopack'),
					),
					'toggle'                => array(
						'custom'                => array(
							'fields'                => array('button_width_custom')
						),
					),
				),
				'button_width_custom'   => array(
					'type'              => 'unit',
					'label'                 => __('Custom Width', 'woopack'),
					'description' 			=> '%',
					'responsive' 			=> array(
						'placeholder' 			=> array(
							'default' 				=> '30',
							'medium' 				=> '',
							'responsive' 			=> '',
						),
					),
					'preview'           	=> array(
						'type'              	=> 'css',
						'selector'          	=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button',
						'property'          	=> 'width',
						'unit'					=> '%'
					),
				),
				'button_margin_top'       => array(
					'type'                  => 'unit',
					'label'                 => __('Margin Top', 'woopack'),
					'default'				=> 10,
					'description' 			=> 'px',
					'responsive'        	=> array(
						'placeholder'       	=> array(
							'default' 		    	=> '',
							'medium' 				=> '',
							'responsive'			=> '',
						),
					),
					'preview'           	=> array(
						'type'              	=> 'css',
						'selector'          	=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button',
						'property'          	=> 'margin-top',
						'unit'					=> 'px'
					),
				),
				'button_margin_bottom'       => array(
					'type'                  => 'unit',
					'label'                 => __('Margin Bottom', 'woopack'),
					'default'				=> 15,
					'description' 			=> 'px',
					'responsive'        	=> array(
						'placeholder'       	=> array(
							'default' 		    	=> '',
							'medium' 				=> '',
							'responsive'			=> '',
						),
					),
					'preview'           	=> array(
						'type'              	=> 'css',
						'selector'          	=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button',
						'property'          	=> 'margin-bottom',
						'unit'					=> 'px'
					),
				),
				'button_border_radius'  	=> array(
					'type' 						=> 'unit',
					'label' 					=> __('Round Corners', 'woopack'),
					'description' 				=> 'px',
					'responsive' 				=> array(
						'placeholder' 				=> array(
							'default' 					=> '',
							'medium' 					=> '',
							'responsive'				=> '',
						),
					),
					'preview' 					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button',
						'property'      			=> 'border-radius',
						'unit'						=> 'px',
					),
				),
			),
		),
		'button_color'      => array(
			'title'             => __('Colors', 'woopack'),
			'fields'            => array(
				'button_bg_color'       => array(
					'type'              	=> 'color',
					'label'             	=> __('Background Color', 'woopack'),
					'show_reset'        	=> true,
					'show_alpha'        	=> true,
					'preview'           	=> array(
						'type' 					=> 'css',
						'selector'				=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button',
						'property'				=> 'background-color',
					),
				),
				'button_bg_color_hover'	=> array(
					'type'              	=> 'color',
					'label'             	=> __('Background Hover Color', 'woopack'),
					'show_reset'        	=> true,
					'show_alpha'        	=> true,
				),
				'button_color'          => array(
					'type'              	=> 'color',
					'label'             	=> __('Text Color', 'woopack'),
					'show_reset'        	=> true,
					'preview'           	=> array(
						'type'              	=> 'css',
						'selector'          	=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button',
						'property'          	=> 'color',
					),
				),
				'button_color_hover'    => array(
					'type'              	=> 'color',
					'label'             	=> __('Text Hover Color', 'woopack'),
					'show_reset'        	=> true,
				),
			),
		),
		'button_border'     => array(
			'title'             => __('Border', 'woopack'),
			'fields'            => array(
				'button_border_style'  		=> array(
					'type'              		=> 'select',
					'label'             		=> __('Border Style', 'woopack'),
					'default'           		=> 'default',
					'options'           		=> array(
						'default'					=> __('Default', 'woopack'),
						'none'              		=> __('None', 'woopack'),
						'solid'             		=> __('Solid', 'woopack'),
						'dotted'            		=> __('Dotted', 'woopack'),
						'dashed'            		=> __('Dashed', 'woopack'),
					),
					'toggle'            		=> array(
						'solid'						=> array(
							'fields'                	=> array('button_border_width', 'button_border_color', 'button_border_color_hover'),
						),
						'dotted'             		=> array(
							'fields'                	=> array('button_border_width', 'button_border_color', 'button_border_color_hover'),
						),
						'dashed'            	 	=> array(
							'fields'                	=> array('button_border_width', 'button_border_color', 'button_border_color_hover'),
						),
					),
				),
				'button_border_width'   	=> array(
					'type' 						=> 'unit',
					'default' 					=> '1',
					'label' 					=> __('Border Width', 'woopack'),
					'description' 				=> 'px',
					'preview' 					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button',
						'property'      			=> 'border-width',
						'unit'						=> 'px',
					),
					'responsive' 				=> array(
						'placeholder' 				=> array(
							'default' 					=> '',
							'medium' 					=> '',
							'responsive'				=> '',
						),
					),
				),
				'button_border_color'		=> array(
					'type' 						=> 'color',
					'label' 					=> __('Border Color', 'woopack'),
					'show_reset' 				=> true,
					'preview' 					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button',
						'property'      			=> 'border-color',
					),
				),
				'button_border_color_hover'	=> array(
					'type' 						=> 'color',
					'label' 					=> __('Border Hover Color', 'woopack'),
					'show_reset' 				=> true,
				),
			),
		),
		'button_padding'    => array(
			'title'             => __('Padding', 'woopack'),
			'fields'            => array(
				'button_padding'    => array(
					'type'              	=> 'dimension',
					'label' 				=> __('Padding', 'woopack'),
					'description' 			=> 'px',
					'preview' 				=> array(
						'type' 					=> 'css',
						'selector'				=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button',
						'property'				=> 'padding',
						'unit' 					=> 'px'
					),
					'responsive' 			=> array(
						'placeholder' 			=> array(
							'default' 				=> '',
							'medium' 				=> '',
							'responsive'			=> '',
						),
					),
				),
			),
		),
		'button_font'       => array(
			'title'             => __('Typography', 'woopack'),
			'fields'            => array(
				'button_font'     			=> array(
					'type'                  	=> 'font',
					'default' 		        	=> array(
						'family'                	=> 'Default',
						'weight'                	=> 300
					),
					'label'             		=> __('Font', 'woopack'),
					'preview'               	=> array(
						'type'                  	=> 'font',
						'selector'              	=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button',
						'property'		        	=> 'font-family',
					),
				),
				'button_font_size'			=> array(
					'type'                  	=> 'select',
					'label'  			    	=> __('Font Size', 'woopack'),
					'default'               	=> 'default',
					'options' 		        	=> array(
						'default' 		        	=> __('Default', 'woopack'),
						'custom'                	=> __('Custom', 'woopack'),
					),
					'toggle'                	=> array(
						'custom'                	=> array(
							'fields'                	=> array('button_font_size_custom'),
						),
					),
				),
				'button_font_size_custom' 	=> array(
					'type'              		=> 'unit',
					'label'                 	=> __('Custom Font Size', 'woopack'),
					'description' 				=> 'px',
					'responsive' 				=> array(
						'placeholder' 				=> array(
							'default' 					=> '',
							'medium' 					=> '',
							'responsive' 				=> '',
						),
					),
					'preview'					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button',
						'property'      			=> 'font-size',
						'unit'						=> 'px',
					),
				),
				'button_text_transform' 	=> array(
					'type'                  	=> 'select',
					'label'  			    	=> __('Text Transform', 'woopack'),
					'default'               	=> 'default',
					'options' 		        	=> array(
						'default' 		        	=> __('Default', 'woopack'),
						'none' 		        		=> __('None', 'woopack'),
						'capitalize' 		       	=> __('Capitalize', 'woopack'),
						'uppercase'             	=> __('UPPERCASE', 'woopack'),
						'lowercase'             	=> __('lowercase', 'woopack'),
					),
				),
			),
		),
	) );
}
