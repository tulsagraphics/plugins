.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .onsale,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .onsale,
.fl-node-<?php echo $id; ?> .woocommerce .product span.onsale {
	position: absolute;
	<?php if ( 'top-left' == $settings->badge_position ) { ?>
		top: 0 !important;
		left: 0 !important;
		bottom: auto !important;
		right: auto !important;
	<?php } elseif ( 'top-center' == $settings->badge_position ) { ?>
		top: 0 !important;
		left: 50% !important;
		bottom: auto !important;
		right: auto !important;
		transform: translateX(-50%);
	<?php } elseif ( 'top-right' == $settings->badge_position ) { ?>
		top: 0 !important;
		left: auto !important;
		bottom: auto !important;
		right: 0 !important;
	<?php } elseif ( 'bottom-left' == $settings->badge_position ) { ?>
		top: auto !important;
		left: 0 !important;
		bottom: 0 !important;
		right: auto !important;
	<?php } elseif ( 'bottom-center' == $settings->badge_position ) { ?>
		top: auto !important;
		left: 50% !important;
		bottom: 0 !important;
		right: auto !important;
		transform: translateX(-50%);
	<?php } elseif ( 'bottom-right' == $settings->badge_position ) { ?>
		top: auto !important;
		left: auto !important;
		bottom: 0 !important;
		right: 0 !important;
	<?php } ?>

	<?php WooPack_Helper::print_css( 'background-color', $settings->badge_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->badge_color ); ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->badge_padding_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->badge_padding_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->badge_padding_left_right, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->badge_padding_left_right, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'margin-top', $settings->badge_margin_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->badge_margin_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-left', $settings->badge_margin_left_right, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-right', $settings->badge_margin_left_right, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'font', $settings->sale_badge_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->sale_badge_font_size_custom, 'px', 'custom' == $settings->sale_badge_font_size ); ?>

	<?php if ( 'none' != $settings->sale_badge_border_style ) { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->sale_badge_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->sale_badge_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->sale_badge_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>

	<?php WooPack_Helper::print_css( 'border-radius', $settings->sale_badge_border_radius, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->sale_badge_line_height ); ?>
	min-width: 0;
	min-height: 0;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .product.outofstock .woopack-product-image .onsale,
.fl-node-<?php echo $id; ?> .woocommerce .products .product.outofstock .woopack-product-image img {
	opacity: 0.4;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock {
	position: absolute;
    display: flex;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span {
	<?php WooPack_Helper::print_css( 'background-color', $settings->out_of_stock_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->out_of_stock_color ); ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->out_of_stock_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->out_of_stock_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->out_of_stock_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->out_of_stock_padding_right, 'px' ); ?>

	<?php if ( 'none' != $settings->out_of_stock_border_style ) { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->out_of_stock_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->out_of_stock_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->out_of_stock_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'border-radius', $settings->out_of_stock_border_radius, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'font', $settings->out_of_stock_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->out_of_stock_font_size_custom, 'px', 'custom' == $settings->out_of_stock_font_size ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->out_of_stock_line_height ); ?>
}

@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .onsale,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .onsale,
	.fl-node-<?php echo $id; ?> .woocommerce .product span.onsale {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->badge_padding_top_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->badge_padding_top_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->badge_padding_left_right_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->badge_padding_left_right_medium, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'font-size', $settings->sale_badge_font_size_custom_medium, 'px', 'custom' == $settings->sale_badge_font_size ); ?>

		<?php if ( 'none' != $settings->sale_badge_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->sale_badge_border_width_medium, 'px' ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->sale_badge_border_radius_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->sale_badge_line_height_medium ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->out_of_stock_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->out_of_stock_padding_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->out_of_stock_padding_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->out_of_stock_padding_right_medium, 'px' ); ?>

		<?php if ( 'none' != $settings->out_of_stock_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->out_of_stock_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->out_of_stock_border_width_medium, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->out_of_stock_border_color ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>
		<?php WooPack_Helper::print_css( 'border-radius', $settings->out_of_stock_border_radius_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->out_of_stock_font_size_custom_medium, 'px', 'custom' == $settings->out_of_stock_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->out_of_stock_line_height_medium ); ?>
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .onsale,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .onsale,
	.fl-node-<?php echo $id; ?> .woocommerce .product span.onsale {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->badge_padding_top_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->badge_padding_top_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->badge_padding_left_right_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->badge_padding_left_right_responsive, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'font-size', $settings->sale_badge_font_size_custom_responsive, 'px', 'custom' == $settings->sale_badge_font_size ); ?>

		<?php if ( 'none' != $settings->sale_badge_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->sale_badge_border_width_responsive, 'px' ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->sale_badge_border_radius_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->sale_badge_line_height_responsive ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->out_of_stock_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->out_of_stock_padding_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->out_of_stock_padding_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->out_of_stock_padding_right_responsive, 'px' ); ?>

		<?php if ( 'none' != $settings->out_of_stock_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->out_of_stock_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->out_of_stock_border_width_responsive, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->out_of_stock_border_color ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->out_of_stock_border_radius_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->out_of_stock_font_size_custom_responsive, 'px', 'custom' == $settings->out_of_stock_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->out_of_stock_line_height_responsive ); ?>
	}

}
