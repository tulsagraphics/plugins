<?php
/**
 * Style option for products.
 *
 * @since 1.0.0
 */

function woopack_product_style_fields()
{
	return apply_filters( 'woopack_product_style_fields', array(
        'box'                  => array(
            'title'     			=> __('Box', 'woopack'),
            'fields'    			=> array(
                'content_bg_color'  	=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Background Color', 'woopack'),
                    'default'           	=> '',
                    'show_reset'        	=> true,
                    'show_alpha'        	=> true,
                    'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product, .woocommerce div.products div.product',
                        'property'				=> 'background-color',
                    ),
                ),
                'content_bg_color_hover'=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Background Hover Color', 'woopack'),
                    'default'           	=> '',
                    'show_reset'        	=> true,
                    'show_alpha'        	=> true,
                ),
                'box_border_style'  	=> array(
                    'type'              	=> 'select',
                    'label'             	=> __('Border Style', 'woopack'),
                    'default'           	=> 'default',
                    'options'           	=> array(
                        'default'              	=> __('Default', 'woopack'),
                        'none'              	=> __('None', 'woopack'),
                        'solid'             	=> __('Solid', 'woopack'),
                        'dotted'            	=> __('Dotted', 'woopack'),
                        'dashed'            	=> __('Dashed', 'woopack'),
                    ),
                    'toggle'            	=> array(
                        'solid'					=> array(
                            'fields'            	=> array( 'box_border', 'box_border_color'),
                        ),
                        'dotted'             	=> array(
                            'fields'                => array( 'box_border', 'box_border_color'),
                        ),
                        'dashed'             	=> array(
                            'fields'                => array( 'box_border', 'box_border_color'),
                        ),
                    ),
                ),
                'box_border'		=> array(
                    'type' 					=> 'dimension',
                    'label' 				=> __('Border', 'woopack'),
                    'description' 			=> 'px',
                    'default' 				=> '0',
                    'preview' 				=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product, .woocommerce div.products div.product',
                        'property'      		=> 'border-width',
                        'unit'					=> 'px',
                    ),
                    'responsive' 			=> array(
                        'placeholder' 			=> array(
                            'default' 				=> '',
                            'medium' 				=> '',
                            'responsive'			=> '',
                        ),
                    ),
                ),
                'box_border_color'  	=> array(
                    'type' 					=> 'color',
                    'label' 				=> __('Border Color', 'woopack'),
                    'show_reset' 			=> true,
                    'show_alpha' 			=> true,
                    'preview' 				=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product, .woocommerce div.products div.product',
                        'property'      		=> 'border-color',
                    ),
                ),
                'box_border_radius' 	=> array(
                    'type' 					=> 'unit',
                    'label' 				=> __('Round Corners', 'woopack'),
                    'description' 			=> 'px',
                    'default' 				=> '0',
                    'preview' 				=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product, .woocommerce div.products div.product',
                        'property'      		=> 'border-radius',
                        'unit'					=> 'px',
                    ),
                    'responsive' 			=> array(
                        'placeholder' 			=> array(
                            'default' 				=> '',
                            'medium' 				=> '',
                            'responsive'			=> '',
                        ),
                    ),
                ),
                'box_padding'   	=> array(
                    'type'              	=> 'dimension',
                    'label' 				=> __('Padding', 'woopack'),
                    'description' 			=> 'px',
                    'responsive' 			=> array(
                        'placeholder' 			=> array(
                            'default' 				=> '',
                            'medium' 				=> '',
                            'responsive'			=> '',
                        ),
                    ),
					'preview' 				=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product, .woocommerce div.products div.product',
                        'property'      		=> 'padding',
                        'unit'					=> 'px',
                    ),
                ),
            ),
        ),
        'box_shadow'           => array(
            'title'         		=> __('Box Shadow', 'woopack'),
            'fields'        		=> array(
                'box_shadow'    		=> array(
                    'type'          		=> 'select',
                    'label'         		=> __('Enable Box Shadow?', 'woopack'),
                    'default'       		=> 'no',
                    'options'       		=> array(
                        'yes'           		=> __('Yes', 'woopack'),
                        'no'            		=> __('No', 'woopack'),
                    ),
                    'toggle'        		=> array(
                        'yes'           		=> array(
                            'fields'        		=> array('box_shadow_h', 'box_shadow_v', 'box_shadow_blur', 'box_shadow_spread', 'box_shadow_color'),
                        ),
                    ),
                ),
                'box_shadow_h'  		=> array(
                    'type'          		=> 'unit',
                    'label'         		=> __('Horizontal', 'woopack'),
                    'default' 				=> '0',
                    'description' 			=> 'px',
                    'responsive' 			=> array(
                        'placeholder' 			=> array(
                            'default' 				=> '',
                            'medium' 				=> '',
                            'responsive'			=> '',
                        ),
                    ),
                ),
                'box_shadow_v'  		=> array(
                    'type'          		=> 'unit',
                    'label'         		=> __('Vertical', 'woopack'),
                    'default'       		=> '0',
                    'description' 			=> 'px',
                    'responsive' 			=> array(
                        'placeholder' 			=> array(
                            'default' 				=> '',
                            'medium' 				=> '',
                            'responsive'			=> '',
                        ),
                    ),
                ),
                'box_shadow_blur'		=> array(
                    'type'          		=> 'unit',
                    'label'         		=> __('Blur', 'woopack'),
                    'default'       		=> '0',
                    'description' 			=> 'px',
                    'responsive' 			=> array(
                        'placeholder' 			=> array(
                            'default' 				=> '',
                            'medium' 				=> '',
                            'responsive'			=> '',
                        ),
                    ),
                ),
                'box_shadow_spread'		=> array(
                    'type'          		=> 'unit',
                    'label'         		=> __('Spread', 'woopack'),
                    'default'       		=> '0',
                    'description' 			=> 'px',
                    'responsive' 			=> array(
                        'placeholder' 			=> array(
                            'default' 				=> '',
                            'medium' 				=> '',
                            'responsive'			=> '',
                        ),
                    ),
                ),
                'box_shadow_color' 		=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Color', 'woopack'),
                    'show_reset'        	=> true,
                    'show_alpha'        	=> true,
                    'default'           	=> 'rgba(0,0,0,0.2)',
                ),
            ),
        ),
        'box_shadow_hover'     => array(
            'title'         		=> __('Box Hover Shadow', 'woopack'),
            'fields'        		=> array(
                'box_shadow_hover'			=> array(
                    'type'          			=> 'select',
                    'label'         			=> __('Enable Box Hover Shadow?', 'woopack'),
                    'default'       			=> 'no',
                    'options'       			=> array(
                        'yes'           			=> __('Yes', 'woopack'),
                        'no'            			=> __('No', 'woopack'),
                    ),
                    'toggle'        			=> array(
                        'yes'           			=> array(
                            'fields'        		=> array('box_shadow_hover_h', 'box_shadow_hover_v', 'box_shadow_hover_blur', 'box_shadow_hover_spread', 'box_shadow_hover_color'),
                        ),
                    ),
                ),
                'box_shadow_hover_h'  		=> array(
                    'type'          			=> 'unit',
                    'label'         			=> __('Horizontal', 'woopack'),
                    'default' 					=> '0',
                    'description' 				=> 'px',
                    'responsive' 				=> array(
                        'placeholder' 				=> array(
                            'default' 					=> '',
                            'medium' 					=> '',
                            'responsive'				=> '',
                        ),
                    ),
                ),
                'box_shadow_hover_v'  		=> array(
                    'type'          			=> 'unit',
                    'label'         			=> __('Vertical', 'woopack'),
                    'default'       			=> '0',
                    'description' 				=> 'px',
                    'responsive' 				=> array(
                        'placeholder' 				=> array(
                            'default' 					=> '',
                            'medium' 					=> '',
                            'responsive'				=> '',
                        ),
                    ),
                ),
                'box_shadow_hover_blur'		=> array(
                    'type'          			=> 'unit',
                    'label'         			=> __('Blur', 'woopack'),
                    'default'       			=> '0',
                    'description' 				=> 'px',
                    'responsive' 				=> array(
                        'placeholder' 				=> array(
                            'default' 					=> '',
                            'medium' 					=> '',
                            'responsive'				=> '',
                        ),
                    ),
                ),
                'box_shadow_hover_spread'	=> array(
                    'type'          			=> 'unit',
                    'label'         			=> __('Spread', 'woopack'),
                    'default'       			=> '0',
                    'description' 				=> 'px',
                    'responsive' 				=> array(
                        'placeholder' 				=> array(
                            'default' 					=> '',
                            'medium' 					=> '',
                            'responsive'				=> '',
                        ),
                    ),
                ),
                'box_shadow_hover_color'	=> array(
                    'type'              		=> 'color',
                    'label'             		=> __('Color', 'woopack'),
                    'show_reset'        		=> true,
                    'show_alpha'        		=> true,
                    'default'           		=> 'rgba(0,0,0,0.2)',
                ),
            ),
        ),
        'content_style'        => array(
            'title'             	=> __('Content', 'woopack'),
            'fields'            	=> array(
                'content_padding'	=> array(
                    'type'              	=> 'dimension',
                    'label' 				=> __('Padding', 'woopack'),
                    'description' 			=> 'px',
                    'responsive' 			=> array(
                        'placeholder' 			=> array(
                            'default' 				=> '',
                            'medium' 				=> '',
                            'responsive'			=> '',
                        ),
                    ),
					'preview' 				=> array(
                        'type'              	=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product .woopack-product-content, .woocommerce div.products div.product .woopack-product-content',
                        'property'          	=> 'padding',
                        'unit' 					=> 'px'
                    ),
                ),
            ),
        ),
        'sale_badge_style'     => array(
            'title'             	=> __('Sale Badge', 'woopack'),
            'fields'            	=> array(
                'badge_position'        	=> array(
                    'type'                  	=> 'select',
                    'label'  			    	=> __('Sale Badge Position', 'woopack'),
                    'default'               	=> 'top-left',
                    'options' 		        	=> array(
                        'top-left'              	=> __('Top Left', 'woopack'),
                        'top-center'              	=> __('Top Center', 'woopack'),
                        'top-right'             	=> __('Top Right', 'woopack'),
                        'bottom-left'           	=> __('Bottom Left', 'woopack'),
                        'bottom-center'           	=> __('Bottom Center', 'woopack'),
                        'bottom-right'          	=> __('Bottom Right', 'woopack'),
                    ),
                ),
                'badge_bg_color'			=> array(
                    'type'              		=> 'color',
                    'label'             		=> __('Background Color', 'woopack'),
                    'show_reset'        		=> true,
                    'show_alpha'        		=> true,
                    'preview'           		=> array(
                        'type' 						=> 'css',
                        'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
                        'property'					=> 'background-color',
                    ),
                ),
                'badge_color'           	=> array(
                    'type'              		=> 'color',
                    'label'             		=> __('Text Color', 'woopack'),
                    'show_reset'        		=> true,
                    'preview'           		=> array(
                        'type'              		=> 'css',
                        'selector'          		=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
                        'property'          		=> 'color',
                    ),
                ),
				'badge_margin_left_right'     	=> array(
                    'type'              		=> 'unit',
                    'label' 					=> __('Horizontal Spacing', 'woopack'),
                    'description' 				=> 'px',
                    'preview' 					=> array(
                        'type' 						=> 'css',
                        'rules'						=> array(
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'margin-left',
		                        'unit' 						=> 'px'
							),
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'margin-right',
		                        'unit' 						=> 'px'
							),
						)
                    ),
                    'responsive' 				=> array(
                        'placeholder' 				=> array(
                            'default' 					=> '',
                            'medium' 					=> '',
                            'responsive'				=> '',
                        ),
                    ),
                ),
				'badge_margin_top_bottom'     	=> array(
                    'type'              		=> 'unit',
                    'label' 					=> __('Vertical Spacing', 'woopack'),
                    'description' 				=> 'px',
                    'preview' 					=> array(
                        'type' 						=> 'css',
                        'rules'						=> array(
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'margin-top',
		                        'unit' 						=> 'px'
							),
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'margin-bottom',
		                        'unit' 						=> 'px'
							),
						)
                    ),
                    'responsive' 				=> array(
                        'placeholder' 				=> array(
                            'default' 					=> '',
                            'medium' 					=> '',
                            'responsive'				=> '',
                        ),
                    ),
                ),
				'badge_padding_left_right'     	=> array(
                    'type'              		=> 'unit',
                    'label' 					=> __('Padding Left/Right', 'woopack'),
                    'description' 				=> 'px',
                    'preview' 					=> array(
                        'type' 						=> 'css',
                        'rules'						=> array(
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'padding-left',
		                        'unit' 						=> 'px'
							),
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'padding-right',
		                        'unit' 						=> 'px'
							),
						)
                    ),
                    'responsive' 				=> array(
                        'placeholder' 				=> array(
                            'default' 					=> '',
                            'medium' 					=> '',
                            'responsive'				=> '',
                        ),
                    ),
                ),
				'badge_padding_top_bottom'     	=> array(
                    'type'              		=> 'unit',
                    'label' 					=> __('Padding Top/Bottom', 'woopack'),
                    'description' 				=> 'px',
                    'preview' 					=> array(
                        'type' 						=> 'css',
                        'rules'						=> array(
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'padding-top',
		                        'unit' 						=> 'px'
							),
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'padding-bottom',
		                        'unit' 						=> 'px'
							),
						)
                    ),
                    'responsive' 				=> array(
                        'placeholder' 				=> array(
                            'default' 					=> '',
                            'medium' 					=> '',
                            'responsive'				=> '',
                        ),
                    ),
                ),
                'sale_badge_border_style'  	=> array(
                    'type'              		=> 'select',
                    'label'             		=> __('Border Style', 'woopack'),
                    'default'           		=> 'none',
                    'options'           		=> array(
                        'none'              		=> __('None', 'woopack'),
                        'solid'             		=> __('Solid', 'woopack'),
                        'dotted'            		=> __('Dotted', 'woopack'),
                        'dashed'            		=> __('Dashed', 'woopack'),
                    ),
                    'toggle'            		=> array(
                        'solid'              		=> array(
                            'fields'                	=> array( 'sale_badge_border_width', 'sale_badge_border_color'),
                        ),
                        'dotted'             		=> array(
                            'fields'                	=> array( 'sale_badge_border_width', 'sale_badge_border_color'),
                        ),
                        'dashed'             		=> array(
                            'fields'                	=> array( 'sale_badge_border_width', 'sale_badge_border_color'),
                        ),
                    ),
                ),
                'sale_badge_border_width'	=> array(
                    'type' 						=> 'unit',
                    'label' 					=> __('Border Width', 'woopack'),
                    'description' 				=> 'px',
                    'default' 					=> '1',
                    'preview' 					=> array(
                        'type' 						=> 'css',
                        'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
                        'property'      			=> 'border-width',
                        'unit'						=> 'px',
                    ),
                    'responsive' 			=> array(
                        'placeholder' 			=> array(
                            'default' 				=> '',
                            'medium' 				=> '',
                            'responsive'			=> '',
                        ),
                    ),
                ),
                'sale_badge_border_color'  	=> array(
                    'type' 						=> 'color',
                    'label' 					=> __('Border Color', 'woopack'),
                    'show_reset' 				=> true,
                    'preview' 					=> array(
                        'type' 						=> 'css',
                        'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
                        'property'      			=> 'border-color',
                    ),
                ),
                'sale_badge_border_radius' 	=> array(
                    'type' 						=> 'unit',
                    'label' 					=> __('Round Corners', 'woopack'),
                    'description' 				=> 'px',
                    'default' 					=> '0',
                    'preview' 					=> array(
                        'type' 						=> 'css',
                        'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
                        'property'      			=> 'border-radius',
                        'unit'						=> 'px',
                    ),
                    'responsive' 				=> array(
                        'placeholder' 				=> array(
                            'default' 					=> '',
                            'medium' 					=> '',
                            'responsive'				=> '',
                        ),
                    ),
                ),
            ),
        ),
        'out_of_stock_style'   => array(
            'title'             	=> __('Out of Stock', 'woopack'),
            'fields'            	=> array(
                'out_of_stock_bg_color'		 => array(
                    'type'              		=> 'color',
                    'label'             		=> __('Background Color', 'woopack'),
                    'show_reset'        		=> true,
					'show_alpha'        		=> true,
					'default'					=> 'rgba(0,0,0,.7)',
                    'preview'           		=> array(
                        'type' 						=> 'css',
                        'selector'					=> '.woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span',
                        'property'					=> 'background-color',
                    ),
                ),
                'out_of_stock_color'         => array(
                    'type'              		=> 'color',
                    'label'             		=> __('Text Color', 'woopack'),
					'show_reset'        		=> true,
					'default'					=> 'ffffff',
                    'preview'           		=> array(
                        'type'              		=> 'css',
                        'selector'          		=> '.woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span',
                        'property'          		=> 'color',
                    ),
				),
				'out_of_stock_padding'       => array(
					'type'        => 'dimension',
					'label'       => __( 'Padding', 'woopack' ),
					'description' => 'px',
					'default'     => '8',
					'preview'     => array(
						'type'     => 'css',
						'selector' => '.woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span',
						'property' => 'padding',
						'unit'     => 'px',
					),
					'responsive'  => true,
				),
                'out_of_stock_border_style'  => array(
                    'type'              		=> 'select',
                    'label'             		=> __('Border Style', 'woopack'),
                    'default'           		=> 'none',
                    'options'           		=> array(
                        'none'              		=> __('None', 'woopack'),
                        'solid'             		=> __('Solid', 'woopack'),
                        'dotted'            		=> __('Dotted', 'woopack'),
                        'dashed'            		=> __('Dashed', 'woopack'),
                    ),
                    'toggle'            		=> array(
                        'solid'              		=> array(
                            'fields'                	=> array( 'out_of_stock_border_width', 'out_of_stock_border_color'),
                        ),
                        'dotted'             		=> array(
                            'fields'                	=> array( 'out_of_stock_border_width', 'out_of_stock_border_color'),
                        ),
                        'dashed'             		=> array(
                            'fields'                	=> array( 'out_of_stock_border_width', 'out_of_stock_border_color'),
                        ),
                    ),
                ),
                'out_of_stock_border_width'	 => array(
                    'type' 						=> 'unit',
                    'label' 					=> __('Border Width', 'woopack'),
                    'description' 				=> 'px',
                    'default' 					=> '1',
                    'preview' 					=> array(
                        'type' 						=> 'css',
                        'selector'					=> '.woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span',
                        'property'      			=> 'border-width',
                        'unit'						=> 'px',
                    ),
                    'responsive' 			=> array(
                        'placeholder' 			=> array(
                            'default' 				=> '',
                            'medium' 				=> '',
                            'responsive'			=> '',
                        ),
                    ),
                ),
                'out_of_stock_border_color'  => array(
                    'type' 						=> 'color',
                    'label' 					=> __('Border Color', 'woopack'),
                    'show_reset' 				=> true,
                    'preview' 					=> array(
                        'type' 						=> 'css',
                        'selector'					=> '.woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span',
                        'property'      			=> 'border-color',
                    ),
                ),
                'out_of_stock_border_radius' => array(
                    'type' 						=> 'unit',
                    'label' 					=> __('Round Corners', 'woopack'),
                    'description' 				=> 'px',
                    'default' 					=> '4',
                    'preview' 					=> array(
                        'type' 						=> 'css',
                        'selector'					=> '.woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span',
                        'property'      			=> 'border-radius',
                        'unit'						=> 'px',
                    ),
                    'responsive' 				=> array(
                        'placeholder' 				=> array(
                            'default' 					=> '',
                            'medium' 					=> '',
                            'responsive'				=> '',
                        ),
                    ),
                ),
            ),
        ),
        'meta_style'           => array(
            'title'             	=> __('Product Taxonomy', 'woopack'),
            'fields'            	=> array(
                'meta_text_color'		=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Text Color', 'woopack'),
                    'show_reset'        	=> true,
                    'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product .woopack-product-meta .product_meta, .woocommerce div.products div.product .woopack-product-meta .product_meta',
                        'property'				=> 'color',
                    ),
                ),
                'meta_link_color'    	=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Link Color', 'woopack'),
                    'show_reset'        	=> true,
                    'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product .woopack-product-meta .product_meta .posted_in a, .woocommerce div.products div.product .woopack-product-meta .product_meta .posted_in a',
                        'property'				=> 'color',
                    ),
                ),
                'meta_border'	     	=> array(
                    'type'                  => 'select',
                    'label'  			    => __('Show Divider?', 'woopack'),
                    'default'               => 'yes',
                    'options' 		        => array(
                        'yes'              		=> __('Yes', 'woopack'),
                        'no'             		=> __('No', 'woopack'),
                    ),
                    'toggle'				=> array(
                        'yes'					=> array(
                            'fields'				=> array('meta_border_color')
                        )
                    )
                ),
                'meta_border_color'  	=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Divider Color', 'woopack'),
                    'default'           	=> 'eeeeee',
                    'show_reset'        	=> true,
                ),
                'meta_padding'   	=> array(
                    'type'                  => 'dimension',
                    'label'                 => __('Padding', 'woopack'),
                    'description' 		    => 'px',
                    'responsive' 			=> array(
                        'placeholder' 			=> array(
                            'default' 				=> '',
                            'medium' 				=> '',
                            'responsive'			=> '',
                        ),
                    ),
					'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product .woopack-product-meta .product_meta, .woocommerce div.products div.product .woopack-product-meta .product_meta',
                        'property'				=> 'padding',
						'unit'					=> 'px'
                    ),
                ),
            ),
        ),
        'product_rating_style' => array(
            'title'                 => __('Rating', 'woopack'),
            'fields'                => array(
                'product_rating_size'   		=> array(
                    'type'              			=> 'unit',
                    'label'                 		=> __('Size', 'woopack'),
                    'description' 					=> 'px',
                    'responsive' 					=> array(
                        'placeholder' 					=> array(
                            'default' 					=> '',
                            'medium' 					=> '',
                            'responsive' 				=> '',
                        ),
                    ),
					'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product .star-rating, .woocommerce div.products div.product .star-rating',
                        'property'				=> 'font-size',
						'unit'					=> 'px'
                    ),
                ),
                'product_rating_default_color'  => array(
                    'type'              			=> 'color',
                    'label'             			=> __('Rating Star Color', 'woopack'),
                    'show_reset'        			=> true,
					'preview'           			=> array(
                        'type' 							=> 'css',
                        'selector'						=> '.woocommerce ul.products li.product .star-rating:before, .woocommerce div.products div.product .star-rating:before',
                        'property'						=> 'color',
                    ),
                ),
                'product_rating_color'       	=> array(
                    'type'              			=> 'color',
                    'label'             			=> __('Rating Star Active Color', 'woopack'),
                    'show_reset'        			=> true,
					'preview'           			=> array(
                        'type' 							=> 'css',
                        'selector'						=> '.woocommerce ul.products li.product .star-rating span:before, .woocommerce div.products div.product .star-rating span:before',
                        'property'						=> 'color',
                    ),
                ),
                'product_rating_margin_bottom'  => array(
                    'type' 							=> 'unit',
                    'label' 						=> __('Margin Bottom', 'woopack'),
                    'default' 						=> '10',
                    'description' 					=> 'px',
                    'responsive' 					=> array(
                        'placeholder' 					=> array(
                            'default' 						=> '',
                            'medium' 						=> '',
                            'responsive'					=> '',
                        ),
                    ),
					'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product .star-rating, .woocommerce div.products div.product .star-rating',
                        'property'				=> 'margin-bottom',
						'unit'					=> 'px'
                    ),
                ),
            ),
        ),
        'quick_view_style' 	   => array(
            'title'             	=> __('Quick View', 'woopack'),
            'fields'            	=> array(
                'quick_view_text_color' => array(
                    'type'              	=> 'color',
                    'label'             	=> __('Text Color', 'woopack'),
                    'show_reset'        	=> true,
                    'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woopack-product-quick-view',
                        'property'				=> 'color',
                    ),
                ),
                'quick_view_bg_color'	=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Background Color', 'woopack'),
                    'show_reset'        	=> true,
                    'show_alpha'        	=> true,
                    'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woopack-product-quick-options',
                        'property'				=> 'background-color',
                    ),
                ),
                'quick_view_padding'    => array(
                    'type'                  => 'unit',
                    'label'                 => __('Padding', 'woopack'),
                    'description' 		    => 'px',
                    'preview'               => array(
                        'type'                  => 'css',
                        'selector'              => '.woopack-product-quick-view',
                        'property'              => 'padding',
                        'unit'                  => 'px'
                    ),
                    'responsive' 			=> array(
                        'placeholder' 			=> array(
                            'default' 				=> '',
                            'medium' 				=> '',
                            'responsive'			=> '',
                        ),
                    ),
                ),
				'quick_view_popup_width'	=> array(
					'type'		=> 'select',
					'label'		=> __('Popup Width', 'woopack'),
					'default'	=> 'auto',
					'options'	=> array(
						'auto'		=> __('Auto', 'woopack'),
						'custom'	=> __('Custom', 'woopack')
					),
					'toggle'	=> array(
						'custom'	=> array(
							'fields'	=> array('quick_view_popup_width_custom')
						)
					)
				),
				'quick_view_popup_width_custom'	=> array(
					'type'			=> 'text',
					'label'			=> __('Custom Popup Width', 'woopack'),
					'default'		=> 670,
					'description'	=> 'px',
					'size'			=> 5
				),
				'quick_view_popup_overlay_bg_color'	=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Popup Overlay Background Color', 'woopack'),
                    'show_reset'        	=> true,
                    'show_alpha'        	=> true,
                ),
            ),
        ),
	) );
}
