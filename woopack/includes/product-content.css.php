.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-content,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-content {
	<?php if ( $default_align && 'default' == $settings->product_align ) { ?>
		<?php WooPack_Helper::print_css( 'text-align', $default_align ); ?>
	<?php } else { ?>
		<?php WooPack_Helper::print_css( 'text-align', $settings->product_align ); ?>
	<?php } ?>
	<?php WooPack_Helper::print_css( 'padding-top', $settings->content_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->content_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->content_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->content_padding_right, 'px' ); ?>
	<?php if ( 3 == $settings->product_layout ) { ?>
		float: left;
	<?php } elseif ( 4 == $settings->product_layout ) { ?>
		float: right;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-title,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-title {
	<?php WooPack_Helper::print_css( 'color', $settings->product_title_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->product_title_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->product_title_font_size_custom, 'px', 'custom' == $settings->product_title_font_size ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->product_title_line_height ); ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->product_title_text_transform ); ?>
	<?php WooPack_Helper::print_css( 'margin-top', $settings->product_title_margin_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_title_margin_bottom, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .star-rating,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .star-rating {
	<?php WooPack_Helper::print_css( 'font-size', $settings->product_rating_size, 'px !important' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_rating_margin_bottom, 'px !important' ); ?>

	<?php if ( 'default' == $settings->product_align && $default_align ) { ?>
		<?php if ( 'center' == $default_align ) { ?>
			margin-left: auto !important;
			margin-right: auto !important;
		<?php } elseif ( 'left' == $default_align ) { ?>
			margin-right: auto !important;
			margin-left: 0 !important;
		<?php } elseif ( 'center' == $default_align ) { ?>
			margin-left: auto!important;
			margin-right: auto!important;
		<?php } elseif ( 'right' == $default_align ) { ?>
			margin-left: auto !important;
			margin-right: 0 !important;
		<?php } elseif ( 'left' == $default_align ) { ?>
			margin-right: auto !important;
			margin-left: 0 !important;
		<?php } ?>
	<?php } elseif ( 'right' == $settings->product_align ) { ?>
		margin-left: auto !important;
		margin-right: 0 !important;
	<?php } elseif ( 'left' == $settings->product_align ) { ?>
		margin-right: auto !important;
		margin-left: 0 !important;
	<?php } else { ?>
		margin-left: auto !important;
		margin-right: auto !important;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .star-rating:before,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .star-rating:before {
	<?php WooPack_Helper::print_css( 'color', $settings->product_rating_default_color, ' !important' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .star-rating span:before,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .star-rating span:before {
	<?php WooPack_Helper::print_css( 'color', $settings->product_rating_color, ' !important' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price {
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_price_margin_bottom, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price del,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price del {
	<?php WooPack_Helper::print_css( 'color', $settings->regular_price_color, ' !important' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price .amount,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price .amount {
	<?php WooPack_Helper::print_css( 'color', $settings->regular_price_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->regular_price_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->regular_price_font_size, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->regular_price_line_height ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price ins .amount,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price ins .amount {
	text-decoration: none;
	<?php WooPack_Helper::print_css( 'color', $settings->sale_price_color ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->sale_price_font_size, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->sale_price_line_height ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-description,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-description {
	<?php WooPack_Helper::print_css( 'color', $settings->short_description_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->short_description_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->short_description_font_size_custom, 'px', 'custom' == $settings->short_description_font_size ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->short_description_line_height ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_description_margin_bottom, 'px' ); ?>

	<?php if ( 2 == $settings->product_layout ) { ?>
		margin-top: 15px;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-meta .product_meta,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-meta .product_meta {
	<?php if ( $default_align && 'default' == $settings->product_align ) { ?>
		<?php WooPack_Helper::print_css( 'text-align', $default_align ); ?>
	<?php } else { ?>
		<?php WooPack_Helper::print_css( 'text-align', $settings->product_align ); ?>
	<?php } ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->meta_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->meta_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->meta_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->meta_padding_right, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->meta_text_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->meta_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->meta_font_size_custom, 'px', 'custom' == $settings->meta_font_size ); ?>
	<?php if ( 'yes' == $settings->meta_border && '' != $settings->meta_border_color ) { ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->meta_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-meta .product_meta .posted_in a,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-meta .product_meta .posted_in a {
	<?php WooPack_Helper::print_css( 'color', $settings->meta_link_color ); ?>
}

@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-title,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-title {
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_title_font_size_custom_medium, 'px', 'custom' == $settings->product_title_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->product_title_line_height_medium ); ?>
		<?php WooPack_Helper::print_css( 'margin-top', $settings->product_title_margin_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_title_margin_bottom_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .star-rating,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .star-rating {
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_rating_size_medium, 'px !important' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_rating_margin_bottom_medium, 'px !important' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-content,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-content {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->content_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->content_padding_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->content_padding_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->content_padding_right_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_price_margin_bottom_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price .amount,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price .amount {
		<?php WooPack_Helper::print_css( 'font-size', $settings->regular_price_font_size_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->regular_price_line_height_medium ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price ins .amount,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price ins .amount {
		<?php WooPack_Helper::print_css( 'font-size', $settings->sale_price_font_size_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->sale_price_line_height_medium ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-description,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-description {
		<?php WooPack_Helper::print_css( 'font-size', $settings->short_description_font_size_custom_medium, 'px', 'custom' == $settings->short_description_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->short_description_line_height_medium ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_description_margin_bottom_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-meta .product_meta,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-meta .product_meta {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->meta_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->meta_padding_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->meta_padding_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->meta_padding_right_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->meta_font_size_custom_medium, 'px', 'custom' == $settings->meta_font_size ); ?>
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-title,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-title {
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_title_font_size_custom_responsive, 'px', 'custom' == $settings->product_title_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->product_title_line_height_responsive ); ?>
		<?php WooPack_Helper::print_css( 'margin-top', $settings->product_title_margin_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_title_margin_bottom_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .star-rating,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .star-rating {
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_rating_size_responsive, 'px !important' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_rating_margin_bottom_responsive, 'px !important' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-content,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-content {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->content_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->content_padding_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->content_padding_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->content_padding_right_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_price_margin_bottom_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price .amount,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price .amount {
		<?php WooPack_Helper::print_css( 'font-size', $settings->regular_price_font_size_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->regular_price_line_height_responsive ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price ins .amount,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price ins .amount {
		<?php WooPack_Helper::print_css( 'font-size', $settings->sale_price_font_size_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->sale_price_line_height_responsive ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-description,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-description {
		<?php WooPack_Helper::print_css( 'font-size', $settings->short_description_font_size_custom_responsive, 'px', 'custom' == $settings->short_description_font_size ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->short_description_line_height_responsive ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_description_margin_bottom_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-meta .product_meta,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-meta .product_meta {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->meta_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->meta_padding_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->meta_padding_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->meta_padding_right_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->meta_font_size_custom_responsive, 'px', 'custom' == $settings->meta_font_size ); ?>
	}
}
