.woopack-product-quick-options {
	position: absolute;
	width: 100%;
	z-index: 90;
	background-color: rgba(255,255,255,.5);
	bottom: 0;
	text-align: center;
	opacity: 0;
	transition: 0.3s ease-in-out;
}
.woopack-product-quick-view {
	width: 100%;
	height: 100%;
	color: #434343;
	cursor: pointer;
	padding: 15px;
	position: relative;
}
.woopack-product-quick-view p {
	font-size: 14px;
	text-transform: capitalize;
	margin: 0;
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product:hover .woopack-product-quick-options,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product:hover .woopack-product-quick-options {
	opacity: 1;
}
.fl-node-<?php echo $id; ?> .woopack-product-quick-options {
	<?php WooPack_Helper::print_css( 'background-color', $settings->quick_view_bg_color ); ?>
	<?php if ( 'overlay' == $settings->quick_view_type ) { ?>
		top: 0;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-quick-view {
	<?php WooPack_Helper::print_css( 'color', $settings->quick_view_text_color ); ?>
	<?php WooPack_Helper::print_css( 'padding', $settings->quick_view_padding, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-quick-view p {
	<?php WooPack_Helper::print_css( 'color', $settings->quick_view_text_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->quick_view_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->quick_view_font_size_custom, 'px', 'custom' == $settings->quick_view_font_size ); ?>

	<?php if ( 'overlay' == $settings->quick_view_type ) { ?>
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translateX(-50%);
	<?php } ?>
}

<?php
// Quick View Modal
?>
div.woopack-modal-<?php echo $id; ?> .woopack-modal-overlay {
	<?php WooPack_Helper::print_css( 'background', $settings->quick_view_popup_overlay_bg_color ); ?>
}

/******************************************
 * Product Title
 ******************************************/
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product .product_title {
	<?php WooPack_Helper::print_css( 'color', $settings->product_title_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->product_title_font ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->product_title_line_height ); ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->product_title_text_transform ); ?>
	font-size: 26px;
	margin-top: 0;
	margin-bottom: 0;
}

/******************************************
 * Product Price
 ******************************************/
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product .price del {
	<?php WooPack_Helper::print_css( 'color', $settings->regular_price_color, ' !important' ); ?>
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product .price .amount {
	<?php WooPack_Helper::print_css( 'color', $settings->regular_price_color, ' !important' ); ?>
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product .price ins .amount {
	<?php WooPack_Helper::print_css( 'color', $settings->sale_price_color ); ?>
	text-decoration: none;
}

/******************************************
 * Product Rating
 ******************************************/
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce-product-rating .star-rating:before {
	<?php WooPack_Helper::print_css( 'color', $settings->product_rating_default_color, ' !important' ); ?>
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce-product-rating .star-rating span:before {
	<?php WooPack_Helper::print_css( 'color', $settings->product_rating_color, ' !important' ); ?>
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce-product-rating .woocommerce-review-link {
	display: none;
 }

/******************************************
 * Product Description
 ******************************************/
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product .woocommerce-product-details__short-description {
	<?php WooPack_Helper::print_css( 'color', $settings->short_description_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->short_description_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->short_description_font_size_custom, 'px', 'custom' == $settings->short_description_font_size ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->short_description_line_height ); ?>
}

/******************************************
 * Product Variations
 ******************************************/
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product form.cart table.variations {
	margin: 0 auto;
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product form.cart table.variations .reset_variations {
	float: right;
	padding: 0;
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product form.cart .woocommerce-variation-price {
	text-align: center;
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product form.cart .woocommerce-variation-add-to-cart {
	margin-top: 10px;
    text-align: center;
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product form.cart .woocommerce-variation-add-to-cart div.quantity {
	display: inline-block;
    float: none;
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product form.cart button.alt {
	display: inline-block;
	float: none;
	width: auto;
}

/******************************************
 * Button
 ******************************************/
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce button.button.alt {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color ); ?>
	<?php WooPack_Helper::print_css( 'font', $settings->button_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->button_font_size_custom, 'px', 'custom' == $settings->button_font_size ); ?>
	<?php WooPack_Helper::print_css( 'text-transform', $settings->button_text_transform ); ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->button_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->button_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->button_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->button_padding_right, 'px' ); ?>

	<?php if ( 'none' != $settings->button_border_style ) { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius, 'px' ); ?>

	<?php if ( 'full_width' == $settings->button_width ) { ?>
		width: 100%;
	<?php } elseif ( 'custom' == $settings->button_width ) { ?>
		<?php WooPack_Helper::print_css( 'width', $settings->button_width_custom, '%' ); ?>
	<?php } else { ?>
		width: auto;
	<?php } ?>
	-webkit-transition: 0.3s ease-in-out;
		-moz-transition: 0.3s ease-in-out;
			transition: 0.3s ease-in-out;
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce button.button.alt:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color_hover ); ?>

	<?php if ( 'none' != $settings->button_border_style ) { ?>
		<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style ); ?>
		<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color_hover ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>	
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce a.added_to_cart {
	background: none;
    font-weight: normal;
    color: inherit;
    border: 1px solid;
    margin-left: 4px;
}

/******************************************
 * Meta
 ******************************************/
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce .product_meta {
	<?php WooPack_Helper::print_css( 'font', $settings->meta_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->meta_font_size_custom, 'px', 'custom' == $settings->meta_font_size ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->meta_text_color ); ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->meta_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->meta_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->meta_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->meta_padding_right, 'px' ); ?>

	<?php if ( 'yes' == $settings->meta_border && '' != $settings->meta_border_color ) { ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->meta_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce .product_meta a {
	<?php WooPack_Helper::print_css( 'color', $settings->meta_link_color ); ?>
}

@media (min-width: 768px) {
	div.woopack-modal-<?php echo $id; ?> .woopack-modal-inner {
		<?php WooPack_Helper::print_css( 'width', $settings->quick_view_popup_width_custom, 'px', 'custom' == $settings->quick_view_popup_width ); ?>
	}
}
