<?php
remove_action('woocommerce_before_single_product', 'wc_print_notices', 10);

//  Removes Woocommerce Tabs
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);

// Remove up sells from after single product hook
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);

// Remove related products from after single product hook
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

$_product_id = get_the_ID();
?>

<?php do_action( 'woopack_before_quick_view_wrap', $_product_id ); ?>

<div class="woopack-product fl-node-<?php echo $node_id; ?>">
	<?php do_action( 'woopack_before_quick_view_content', $_product_id ); ?>

    <?php echo do_shortcode('[product_page id="'.$_product_id.'"]'); ?>

	<?php do_action( 'woopack_after_quick_view_content', $_product_id ); ?>
</div>

<?php do_action( 'woopacka_after_quick_view_wrap', $_product_id ); ?>