.fl-node-<?php echo $id; ?> .woocommerce ul.products .woopack-product-action,
.fl-node-<?php echo $id; ?> .woocommerce div.products .woopack-product-action {
	<?php if ( 'default' == $settings->button_alignment ) { ?>
		<?php WooPack_Helper::print_css( 'text-align', $default_align ); ?>
	<?php } else { ?>
		<?php WooPack_Helper::print_css( 'text-align', $settings->button_alignment ); ?>
	<?php }; ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .woopack-qty-input,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .variations_form .quantity {
	<?php if ( isset( $settings->qty_input ) ) { ?>
		<?php if ( 'no' == $settings->qty_input ) { ?>
			display: none;
		<?php } ?>
		<?php if ( 'above_button' == $settings->qty_input ) { ?>
			display: block;
			margin: 0 auto;
		<?php } else { ?>
			display: inline-block;
		<?php } ?>
		<?php if ( 'before_button' == $settings->qty_input ) { ?>
			margin-right: 2px;
		<?php } ?>
		<?php if ( 'after_button' == $settings->qty_input ) { ?>
			margin-left: 2px;
		<?php } ?>
		<?php if ( 'full_width' == $settings->button_width ) { ?>
			display: block;
			margin: 0 auto;
		<?php } ?>
	<?php } ?>
	width: 60px;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .woopack-qty-input input.qty,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .variations_form .quantity input.qty {
	width: 100%;
}

.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .variations_form table,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .grouped_form table {
	<?php if ( 'default' == $settings->product_align || 'center' == $settings->product_align ) { ?>
	margin: 0 auto;
	<?php } ?>
	<?php if ( 'right' == $settings->product_align ) { ?>
	margin: 0 0 0 auto;
	<?php } ?>
	margin-bottom: 10px;
}

.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .variations_form .label {
	color: inherit;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .variations_form .label label {
	font-size: 12px;
}

.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .variations_form .reset_variations {
	margin-left: 5px;
}

.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .variations_form .woocommerce-variation-price {
	margin-top: 5px;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .variations_form .woocommerce-variation-price .price {
	font-size: 14px;
}

.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .grouped_form div.quantity {
	margin-right: 10px;
    margin-bottom: 5px;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .grouped_form .woocommerce-grouped-product-list-item__label label {
	margin-right: 10px;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .grouped_form .woocommerce-grouped-product-list-item__price .amount {
	display: inline-block;
	margin-bottom: 5px;
}

.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.button,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.button.alt,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.add_to_cart_button,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.add_to_cart_button.alt,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.added_to_cart,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .button,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .button.alt,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action button,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action button.alt {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color ); ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->button_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->button_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->button_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->button_padding_right, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'font', $settings->button_font ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->button_font_size_custom, 'px', 'custom' == $settings->button_font_size ); ?>

	<?php if ( 'default' != $settings->button_border_style ) { ?>
		<?php if ( 'none' != $settings->button_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>
	<?php } ?>

	<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius, 'px' ); ?>

	<?php if ( 'full_width' == $settings->button_width ) { ?>
		width: 100%;
	<?php } elseif ( 'custom' == $settings->button_width ) { ?>
		width: <?php echo $settings->button_width_custom; ?>%;
	<?php } else { ?>
		width: auto;
	<?php } ?>

	<?php WooPack_Helper::print_css( 'text-transform', $settings->button_text_transform, '', 'default' != $settings->button_text_transform ); ?>
	text-align: center;
	-webkit-transition: 0.2s ease-in-out;
		-moz-transition: 0.2s ease-in-out;
			transition: 0.2s ease-in-out;
}
<?php if ( 'full_width' == $settings->button_width ) { ?>
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.button.alt.added,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .button.alt.added {
	width: 50%;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.added_to_cart,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.added_to_cart.alt {
	width: 48%;
	margin-left: 0;
}
<?php } ?>
.fl-node-<?php echo $id; ?> .woocommerce ul.products .woopack-product-action a.button:hover,
.fl-node-<?php echo $id; ?> .woocommerce div.products .woopack-product-action a.button:hover,
.fl-node-<?php echo $id; ?> .woocommerce ul.products .woopack-product-action .button:hover,
.fl-node-<?php echo $id; ?> .woocommerce div.products .woopack-product-action .button:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color_hover ); ?>

	<?php if ( 'default' != $settings->button_border_style ) { ?>
		<?php if ( 'none' != $settings->button_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color_hover ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>
	<?php } ?>
}

@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.button,
	.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.add_to_cart_button,
	.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.added_to_cart {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->button_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->button_padding_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->button_padding_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->button_padding_right_medium, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_medium, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'font', $settings->button_font ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->button_font_size_custom_medium, 'px', 'custom' == $settings->button_font_size ); ?>

		<?php if ( 'default' != $settings->button_border_style ) { ?>
			<?php if ( 'none' != $settings->button_border_style ) { ?>
				<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style ); ?>
				<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width_medium, 'px' ); ?>
				<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color ); ?>
			<?php } else { ?>
				border: none;
			<?php } ?>
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius_medium, 'px' ); ?>
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.button,
	.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.add_to_cart_button,
	.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.added_to_cart {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->button_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->button_padding_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->button_padding_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->button_padding_right_responsive, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_responsive, 'px' ); ?>

		<?php WooPack_Helper::print_css( 'font', $settings->button_font ); ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->button_font_size_custom_responsive, 'px', 'custom' == $settings->button_font_size ); ?>

		<?php if ( 'default' != $settings->button_border_style ) { ?>
			<?php if ( 'none' != $settings->button_border_style ) { ?>
				<?php WooPack_Helper::print_css( 'border-style', $settings->button_border_style ); ?>
				<?php WooPack_Helper::print_css( 'border-width', $settings->button_border_width_responsive, 'px' ); ?>
				<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color ); ?>
			<?php } else { ?>
				border: none;
			<?php } ?>
		<?php } ?>

		<?php WooPack_Helper::print_css( 'border-radius', $settings->button_border_radius_responsive, 'px' ); ?>
	}
}
