.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-image,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-image {
    position: relative;
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-image a,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-image a {
    cursor: default;
    display: block;
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-image a img,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-image a img {
    margin: auto;
    width: auto;
    border:none;
}
