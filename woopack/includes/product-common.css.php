<?php
if ( 'default' == $settings->product_align && 1 == $settings->product_layout ) {
	$default_align = 'center';
} elseif ( 'default' == $settings->product_align && 2 == $settings->product_layout ) {
	$default_align = 'center';
} elseif ( 'default' == $settings->product_align && 3 == $settings->product_layout ) {
	$default_align = 'left';
} elseif ( 'default' == $settings->product_align && 4 == $settings->product_layout ) {
	$default_align = 'left';
} else {
	$default_align = $settings->product_align;
}

	$product_col            = ! empty( $settings->product_columns ) ? $settings->product_columns : '3' ;
	$product_col_medium     = ! empty( $settings->product_columns_medium ) ? $settings->product_columns_medium : '2' ;
	$product_col_responsive = ! empty( $settings->product_columns_responsive ) ? $settings->product_columns_responsive : '1' ;
?>

.fl-node-<?php echo $id; ?> .woocommerce a {
	text-decoration: none;
}

.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products li.product,
.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-carousel div.products div.product {
	<?php WooPack_Helper::print_css( 'background-color', $settings->content_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'padding-top', $settings->box_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->box_padding_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->box_padding_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->box_padding_right, 'px' ); ?>
	<?php if ( 'default' != $settings->box_border_style ) { ?>
		<?php if ( 'none' != $settings->box_border_style ) { ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->box_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-top-width', $settings->box_border_top, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->box_border_bottom, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-left-width', $settings->box_border_left, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-right-width', $settings->box_border_right, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->box_border_color ); ?>
		<?php } else { ?>
			border: none;
		<?php } ?>
	<?php } ?>
	<?php WooPack_Helper::print_css( 'border-radius', $settings->box_border_radius, 'px' ); ?>
	<?php if ( 'yes' == $settings->box_shadow ) { ?>
		-webkit-box-shadow: <?php echo $settings->box_shadow_h; ?>px <?php echo $settings->box_shadow_v; ?>px <?php echo $settings->box_shadow_blur; ?>px <?php echo $settings->box_shadow_spread; ?>px <?php echo ( false === strpos( $settings->box_shadow_color, 'rgb' ) ) ? '#' . $settings->box_shadow_color : $settings->box_shadow_color; ?>;
		-moz-box-shadow: <?php echo $settings->box_shadow_h; ?>px <?php echo $settings->box_shadow_v; ?>px <?php echo $settings->box_shadow_blur; ?>px <?php echo $settings->box_shadow_spread; ?>px <?php echo ( false === strpos( $settings->box_shadow_color, 'rgb' ) ) ? '#' . $settings->box_shadow_color : $settings->box_shadow_color; ?>;
		-o-box-shadow: <?php echo $settings->box_shadow_h; ?>px <?php echo $settings->box_shadow_v; ?>px <?php echo $settings->box_shadow_blur; ?>px <?php echo $settings->box_shadow_spread; ?>px <?php echo ( false === strpos( $settings->box_shadow_color, 'rgb' ) ) ? '#' . $settings->box_shadow_color : $settings->box_shadow_color; ?>;
		box-shadow: <?php echo $settings->box_shadow_h; ?>px <?php echo $settings->box_shadow_v; ?>px <?php echo $settings->box_shadow_blur; ?>px <?php echo $settings->box_shadow_spread; ?>px <?php echo ( false === strpos( $settings->box_shadow_color, 'rgb' ) ) ? '#' . $settings->box_shadow_color : $settings->box_shadow_color; ?>;
	<?php } ?>
	max-width: none;
	clear: none !important;
	position: relative;
	text-align: <?php echo $default_align; ?>;
}

.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products li.product:hover,
.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-carousel div.products div.product:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->content_bg_color_hover ); ?>	
	<?php if ( 'yes' == $settings->box_shadow_hover ) { ?>
		-webkit-box-shadow: <?php echo $settings->box_shadow_hover_h; ?>px <?php echo $settings->box_shadow_hover_v; ?>px <?php echo $settings->box_shadow_hover_blur; ?>px <?php echo $settings->box_shadow_hover_spread; ?>px <?php echo ( false === strpos( $settings->box_shadow_hover_color, 'rgb' ) ) ? '#' . $settings->box_shadow_hover_color : $settings->box_shadow_hover_color; ?>;
		-moz-box-shadow: <?php echo $settings->box_shadow_hover_h; ?>px <?php echo $settings->box_shadow_hover_v; ?>px <?php echo $settings->box_shadow_hover_blur; ?>px <?php echo $settings->box_shadow_hover_spread; ?>px <?php echo ( false === strpos( $settings->box_shadow_hover_color, 'rgb' ) ) ? '#' . $settings->box_shadow_hover_color : $settings->box_shadow_hover_color; ?>;
		-o-box-shadow: <?php echo $settings->box_shadow_hover_h; ?>px <?php echo $settings->box_shadow_hover_v; ?>px <?php echo $settings->box_shadow_hover_blur; ?>px <?php echo $settings->box_shadow_hover_spread; ?>px <?php echo ( false === strpos( $settings->box_shadow_hover_color, 'rgb' ) ) ? '#' . $settings->box_shadow_hover_color : $settings->box_shadow_hover_color; ?>;
		box-shadow: <?php echo $settings->box_shadow_hover_h; ?>px <?php echo $settings->box_shadow_hover_v; ?>px <?php echo $settings->box_shadow_hover_blur; ?>px <?php echo $settings->box_shadow_hover_spread; ?>px <?php echo ( false === strpos( $settings->box_shadow_hover_color, 'rgb' ) ) ? '#' . $settings->box_shadow_hover_color : $settings->box_shadow_hover_color; ?>;
	<?php } ?>
}

@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products li.product,
	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-carousel div.products div.product {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->box_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->box_padding_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->box_padding_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->box_padding_right_medium, 'px' ); ?>
		<?php if ( 'default' != $settings->box_border_style ) { ?>
			<?php if ( 'none' != $settings->box_border_style ) { ?>
				<?php WooPack_Helper::print_css( 'border-style', $settings->box_border_style ); ?>
				<?php WooPack_Helper::print_css( 'border-top-width', $settings->box_border_top_medium, 'px' ); ?>
				<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->box_border_bottom_medium, 'px' ); ?>
				<?php WooPack_Helper::print_css( 'border-left-width', $settings->box_border_left_medium, 'px' ); ?>
				<?php WooPack_Helper::print_css( 'border-right-width', $settings->box_border_right_medium, 'px' ); ?>
				<?php WooPack_Helper::print_css( 'border-color', $settings->box_border_color ); ?>
			<?php } else { ?>
				border: none;
			<?php } ?>
		<?php } ?>	
		<?php WooPack_Helper::print_css( 'border-radius', $settings->box_border_radius_medium ); ?>
		<?php if ( 'yes' == $settings->box_shadow ) { ?>
			-webkit-box-shadow: <?php echo $settings->box_shadow_h_medium; ?>px <?php echo $settings->box_shadow_v_medium; ?>px <?php echo $settings->box_shadow_blur_medium; ?>px <?php echo $settings->box_shadow_spread_medium; ?>px <?php echo ( false === strpos( $settings->box_shadow_color, 'rgb' ) ) ? '#' . $settings->box_shadow_color : $settings->box_shadow_color; ?>;
			-moz-box-shadow: <?php echo $settings->box_shadow_h_medium; ?>px <?php echo $settings->box_shadow_v_medium; ?>px <?php echo $settings->box_shadow_blur_medium; ?>px <?php echo $settings->box_shadow_spread_medium; ?>px <?php echo ( false === strpos( $settings->box_shadow_color, 'rgb' ) ) ? '#' . $settings->box_shadow_color : $settings->box_shadow_color; ?>;
			-o-box-shadow: <?php echo $settings->box_shadow_h_medium; ?>px <?php echo $settings->box_shadow_v_medium; ?>px <?php echo $settings->box_shadow_blur_medium; ?>px <?php echo $settings->box_shadow_spread_medium; ?>px <?php echo ( false === strpos( $settings->box_shadow_color, 'rgb' ) ) ? '#' . $settings->box_shadow_color : $settings->box_shadow_color; ?>;
			box-shadow: <?php echo $settings->box_shadow_h_medium; ?>px <?php echo $settings->box_shadow_v_medium; ?>px <?php echo $settings->box_shadow_blur_medium; ?>px <?php echo $settings->box_shadow_spread_medium; ?>px <?php echo ( false === strpos( $settings->box_shadow_color, 'rgb' ) ) ? '#' . $settings->box_shadow_color : $settings->box_shadow_color; ?>;
		<?php } ?>
		text-align : center;
	}

	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products li.product:hover,
	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-carousel div.products div.product:hover {
		<?php if ( 'yes' == $settings->box_shadow_hover ) { ?>
			-webkit-box-shadow: <?php echo $settings->box_shadow_hover_h_medium; ?>px <?php echo $settings->box_shadow_hover_v_medium; ?>px <?php echo $settings->box_shadow_hover_blur_medium; ?>px <?php echo $settings->box_shadow_hover_spread_medium; ?>px <?php echo ( false === strpos( $settings->box_shadow_hover_color, 'rgb' ) ) ? '#' . $settings->box_shadow_hover_color : $settings->box_shadow_hover_color; ?>;
			-moz-box-shadow: <?php echo $settings->box_shadow_hover_h_medium; ?>px <?php echo $settings->box_shadow_hover_v_medium; ?>px <?php echo $settings->box_shadow_hover_blur_medium; ?>px <?php echo $settings->box_shadow_hover_spread_medium; ?>px <?php echo ( false === strpos( $settings->box_shadow_hover_color, 'rgb' ) ) ? '#' . $settings->box_shadow_hover_color : $settings->box_shadow_hover_color; ?>;
			-o-box-shadow: <?php echo $settings->box_shadow_hover_h_medium; ?>px <?php echo $settings->box_shadow_hover_v_medium; ?>px <?php echo $settings->box_shadow_hover_blur_medium; ?>px <?php echo $settings->box_shadow_hover_spread_medium; ?>px <?php echo ( false === strpos( $settings->box_shadow_hover_color, 'rgb' ) ) ? '#' . $settings->box_shadow_hover_color : $settings->box_shadow_hover_color; ?>;
			box-shadow: <?php echo $settings->box_shadow_hover_h_medium; ?>px <?php echo $settings->box_shadow_hover_v_medium; ?>px <?php echo $settings->box_shadow_hover_blur_medium; ?>px <?php echo $settings->box_shadow_hover_spread_medium; ?>px <?php echo ( false === strpos( $settings->box_shadow_hover_color, 'rgb' ) ) ? '#' . $settings->box_shadow_hover_color : $settings->box_shadow_hover_color; ?>;
		<?php } ?>
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products li.product,
	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-carousel div.products div.product {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->box_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->box_padding_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->box_padding_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->box_padding_right_responsive, 'px' ); ?>
		<?php if ( 'default' != $settings->box_border_style ) { ?>
			<?php if ( 'none' != $settings->box_border_style ) { ?>
				<?php WooPack_Helper::print_css( 'border-style', $settings->box_border_style ); ?>
				<?php WooPack_Helper::print_css( 'border-top-width', $settings->box_border_top_responsive, 'px' ); ?>
				<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->box_border_bottom_responsive, 'px' ); ?>
				<?php WooPack_Helper::print_css( 'border-left-width', $settings->box_border_left_responsive, 'px' ); ?>
				<?php WooPack_Helper::print_css( 'border-right-width', $settings->box_border_right_responsive, 'px' ); ?>
				<?php WooPack_Helper::print_css( 'border-color', $settings->box_border_color ); ?>
			<?php } else { ?>
				border: none;
			<?php } ?>
		<?php } ?>	
		<?php WooPack_Helper::print_css( 'border-radius', $settings->box_border_radius_responsive ); ?>
		<?php if ( 'yes' == $settings->box_shadow ) { ?>
			-webkit-box-shadow: <?php echo $settings->box_shadow_h_responsive; ?>px <?php echo $settings->box_shadow_v_responsive; ?>px <?php echo $settings->box_shadow_blur_responsive; ?>px <?php echo $settings->box_shadow_spread_responsive; ?>px <?php echo ( false === strpos( $settings->box_shadow_color, 'rgb' ) ) ? '#' . $settings->box_shadow_color : $settings->box_shadow_color; ?>;
			-moz-box-shadow: <?php echo $settings->box_shadow_h_responsive; ?>px <?php echo $settings->box_shadow_v_responsive; ?>px <?php echo $settings->box_shadow_blur_responsive; ?>px <?php echo $settings->box_shadow_spread_responsive; ?>px <?php echo ( false === strpos( $settings->box_shadow_color, 'rgb' ) ) ? '#' . $settings->box_shadow_color : $settings->box_shadow_color; ?>;
			-o-box-shadow: <?php echo $settings->box_shadow_h_responsive; ?>px <?php echo $settings->box_shadow_v_responsive; ?>px <?php echo $settings->box_shadow_blur_responsive; ?>px <?php echo $settings->box_shadow_spread_responsive; ?>px <?php echo ( false === strpos( $settings->box_shadow_color, 'rgb' ) ) ? '#' . $settings->box_shadow_color : $settings->box_shadow_color; ?>;
			box-shadow: <?php echo $settings->box_shadow_h_responsive; ?>px <?php echo $settings->box_shadow_v_responsive; ?>px <?php echo $settings->box_shadow_blur_responsive; ?>px <?php echo $settings->box_shadow_spread_responsive; ?>px <?php echo ( false === strpos( $settings->box_shadow_color, 'rgb' ) ) ? '#' . $settings->box_shadow_color : $settings->box_shadow_color; ?>;
		<?php } ?>

		text-align : center;
	}

	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products li.product:hover,
	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-carousel div.products div.product:hover {
		<?php if ( 'yes' == $settings->box_shadow_hover ) { ?>
			-webkit-box-shadow: <?php echo $settings->box_shadow_hover_h_responsive; ?>px <?php echo $settings->box_shadow_hover_v_responsive; ?>px <?php echo $settings->box_shadow_hover_blur_responsive; ?>px <?php echo $settings->box_shadow_hover_spread_responsive; ?>px <?php echo ( false === strpos( $settings->box_shadow_hover_color, 'rgb' ) ) ? '#' . $settings->box_shadow_hover_color : $settings->box_shadow_hover_color; ?>;
			-moz-box-shadow: <?php echo $settings->box_shadow_hover_h_responsive; ?>px <?php echo $settings->box_shadow_hover_v_responsive; ?>px <?php echo $settings->box_shadow_hover_blur_responsive; ?>px <?php echo $settings->box_shadow_hover_spread_responsive; ?>px <?php echo ( false === strpos( $settings->box_shadow_hover_color, 'rgb' ) ) ? '#' . $settings->box_shadow_hover_color : $settings->box_shadow_hover_color; ?>;
			-o-box-shadow: <?php echo $settings->box_shadow_hover_h_responsive; ?>px <?php echo $settings->box_shadow_hover_v_responsive; ?>px <?php echo $settings->box_shadow_hover_blur_responsive; ?>px <?php echo $settings->box_shadow_hover_spread_responsive; ?>px <?php echo ( false === strpos( $settings->box_shadow_hover_color, 'rgb' ) ) ? '#' . $settings->box_shadow_hover_color : $settings->box_shadow_hover_color; ?>;
			box-shadow: <?php echo $settings->box_shadow_hover_h_responsive; ?>px <?php echo $settings->box_shadow_hover_v_responsive; ?>px <?php echo $settings->box_shadow_hover_blur_responsive; ?>px <?php echo $settings->box_shadow_hover_spread_responsive; ?>px <?php echo ( false === strpos( $settings->box_shadow_hover_color, 'rgb' ) ) ? '#' . $settings->box_shadow_hover_color : $settings->box_shadow_hover_color; ?>;
		<?php } ?>
	}
}
