<div class="woopack-modal" style="display: none;">
    <div class="woopack-modal-overlay"></div>
    <div class="woopack-modal-inner">
        <div class="woopack-modal-close">×</div>
        <div class="woopack-modal-content" style="background-image: url(<?php echo WOOPACK_URL; ?>assets/images/loader.gif);"></div>
    </div>
</div>
