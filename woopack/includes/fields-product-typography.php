<?php

/**
 * Typography option for products.
 *
 * @since 1.0.0
 */
function woopack_product_typography_fields()
{
	return apply_filters( 'woopack_product_typography_fields', array(
		'product_title_fonts'   => array(
			'title'                 => __('Product Title', 'woopack'),
			'fields'                => array(
				'product_title_heading_tag'     => array(
					'type' 			              => 'select',
					'label' 		              => __('Heading Tag', 'woopack'),
					'default'                     => 'h2',
					'options'                     => array(
						'h1'                        => __('H1', 'woopack'),
						'h2'                        => __('H2', 'woopack'),
						'h3'                        => __('H3', 'woopack'),
						'h4'                        => __('H4', 'woopack'),
						'h5'                        => __('H5', 'woopack'),
						'h6'                        => __('H6', 'woopack'),
					),
				),
				'product_title_font'            => array(
					'type'                  		=> 'font',
					'label'             			=> __('Font', 'woopack'),
					'default' 		        		=> array(
						'family'                		=> 'Default',
						'weight'                		=> 300
					),
					'preview'               		=> array(
						'type'                  		=> 'font',
						'selector'              		=> '.woocommerce ul.products li.product .woopack-product-title, .woocommerce div.products div.product .woopack-product-title',
						'property'		        		=> 'font-family',
					),
				),
				'product_title_font_size'       => array(
					'type'                  		=> 'select',
					'label'  			    		=> __('Font Size', 'woopack'),
					'default'               		=> 'default',
					'options' 		        		=> array(
						'default' 		        		=> __('Default', 'woopack'),
						'custom'                		=> __('Custom', 'woopack'),
					),
					'toggle'                		=> array(
						'custom'                		=> array(
							'fields'                		=> array('product_title_font_size_custom')
						),
					),
				),
				'product_title_font_size_custom'=> array(
					'type'              			=> 'unit',
					'label'							=> __('Custom Font Size', 'woopack'),
					'description' 					=> 'px',
					'responsive' 					=> array(
						'placeholder' 					=> array(
							'default' 						=> '15',
							'medium' 						=> '',
							'responsive' 					=> '',
						),
					),
					'preview'               		=> array(
						'type'                  		=> 'css',
						'selector'              		=> '.woocommerce ul.products li.product .woopack-product-title, .woocommerce div.products div.product .woopack-product-title',
						'property'		        		=> 'font-size',
						'unit'                  		=> 'px',
					),
				),
				'product_title_line_height'	    => array(
					'type'              			=> 'unit',
					'label'							=> __('Line Height', 'woopack'),
					'preview'               		=> array(
						'type'                  		=> 'css',
						'selector'              		=> '.woocommerce ul.products li.product .woopack-product-title, .woocommerce div.products div.product .woopack-product-title',
						'property'		        		=> 'line-height',
					),
					'responsive' 					=> array(
						'placeholder' 					=> array(
							'default' 						=> '',
							'medium' 						=> '',
							'responsive' 					=> '',
						),
					),
				),
				'product_title_text_transform'  => array(
					'type'                  		=> 'select',
					'label'  			    		=> __('Text Transform', 'woopack'),
					'default'               		=> 'default',
					'options' 		        		=> array(
						'default' 		        		=> __('Default', 'woopack'),
						'none' 		        			=> __('None', 'woopack'),
						'uppercase'             		=> __('UPPERCASE', 'woopack'),
						'lowercase'             		=> __('lowercase', 'woopack'),
					),
					'preview' 		     			=> array(
						'type'              			=> 'css',
						'selector'          			=> '.woocommerce ul.products li.product .woopack-product-title, .woocommerce div.products div.product .woopack-product-title',
						'property'          			=> 'text-transform',
					),
				),
				'product_title_color'           => array(
					'type'              			=> 'color',
					'label'             			=> __('Color', 'woopack'),
					'show_reset'        			=> true,
					'preview'           			=> array(
						'type'              			=> 'css',
						'selector'          			=> '.woocommerce ul.products li.product .woopack-product-title, .woocommerce div.products div.product .woopack-product-title',
						'property'          			=> 'color',
					),
				),
				'product_title_margin_top'    => array(
					'type' 							=> 'unit',
					'label' 						=> __('Margin Top', 'woopack'),
					'default' 						=> '10',
					'description' 					=> 'px',
					'responsive' 						=> array(
						'placeholder' 						=> array(
							'default' 							=> '',
							'medium' 							=> '',
							'responsive'						=> '',
						),
					),
					'preview'           			=> array(
						'type'              			=> 'css',
						'selector'          			=> '.woocommerce ul.products li.product .woopack-product-title, .woocommerce div.products div.product .woopack-product-title',
						'property'          			=> 'margin-top',
						'unit'							=> 'px'
					),
				),
				'product_title_margin_bottom'    => array(
					'type' 							=> 'unit',
					'label' 						=> __('Margin Bottom', 'woopack'),
					'default' 						=> '10',
					'description' 					=> 'px',
					'responsive' 						=> array(
						'placeholder' 						=> array(
							'default' 							=> '',
							'medium' 							=> '',
							'responsive'						=> '',
						),
					),
					'preview'           			=> array(
						'type'              			=> 'css',
						'selector'          			=> '.woocommerce ul.products li.product .woopack-product-title, .woocommerce div.products div.product .woopack-product-title',
						'property'          			=> 'margin-bottom',
						'unit'							=> 'px'
					),
				),
			),
		),
		'product_price_fonts'   => array(
			'title'                 => __('Price', 'woopack'),
			'fields'                => array(
				'regular_price_font'        => array(
					'type'                      => 'font',
					'label'                 	=> __('Font', 'woopack'),
					'default' 		            => array(
						'family'                    => 'Default',
						'weight'                    => 300
					),
					'preview'                   => array(
						'type'                      => 'font',
						'selector'                  => '.woocommerce ul.products li.product .price .amount, .woocommerce div.products div.product .price .amount',
						'property'		            => 'font-family',
					),
				),
				'regular_price_font_size'   => array(
					'type'              		=> 'unit',
					'label'                 	=> __('Font Size', 'woopack'),
					'description' 				=> 'px',
					'responsive' 				=> array(
						'placeholder' 				=> array(
							'default' 					=> '',
							'medium' 					=> '',
							'responsive' 				=> '',
						),
					),
					'preview'					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce ul.products li.product .price .amount, .woocommerce div.products div.product .price .amount',
						'property'      			=> 'font-size',
						'unit'						=> 'px',
					),
				),
				'regular_price_line_height'	=> array(
					'type'              		=> 'unit',
					'label'                 	=> __('Line Height', 'woopack'),
					'responsive' 				=> array(
						'placeholder' 				=> array(
							'default' 					=> '',
							'medium' 					=> '',
							'responsive' 				=> '',
						),
					),
					'preview'					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce ul.products li.product .price .amount, .woocommerce div.products div.product .price .amount',
						'property'      			=> 'line-height',
					),
				),
				'regular_price_color'       => array(
					'type'              		=> 'color',
					'label'             		=> __('Color', 'woopack'),
					'show_reset'        		=> true,
					'preview'           		=> array(
						'type'              		=> 'css',
						'selector'          		=> '.woocommerce ul.products li.product .price .amount, .woocommerce div.products div.product .price .amount',
						'property'          		=> 'color',
					),
				),
				'product_price_margin_bottom'      => array(
					'type' 						=> 'unit',
					'label' 					=> __('Margin Bottom', 'woopack'),
					'default' 					=> '',
					'description' 				=> 'px',
					'responsive' 					=> array(
						'placeholder' 					=> array(
							'default' 						=> '',
							'medium' 						=> '',
							'responsive'					=> '',
						),
					),
					'preview'					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce ul.products li.product .price .amount, .woocommerce div.products div.product .price .amount',
						'property'      			=> 'margin-bottom',
						'unit'						=> 'px'
					),
				),
			),
		),
		'sale_price_fonts'      => array(
			'title'                 => __('Sale Price', 'woopack'),
			'fields'                => array(
				'sale_price_font_size'  => array(
					'type'              	=> 'unit',
					'label'                 	=> __('Font Size', 'woopack'),
					'description' 				=> 'px',
					'responsive' 				=> array(
						'placeholder' 				=> array(
							'default' 					=> '',
							'medium' 					=> '',
							'responsive' 				=> '',
						),
					),
					'preview'					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce ul.products li.product .price ins .amount, .woocommerce div.products div.product .price ins .amount',
						'property'      			=> 'font-size',
						'unit'						=> 'px',
					),
				),
				'sale_price_line_height'=> array(
					'type'              	=> 'unit',
					'label'                 	=> __('Line Height', 'woopack'),
					'responsive' 				=> array(
						'placeholder' 				=> array(
							'default' 					=> '',
							'medium' 					=> '',
							'responsive' 				=> '',
						),
					),
					'preview'					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce ul.products li.product .price ins .amount, .woocommerce div.products div.product .price ins .amount',
						'property'      			=> 'line-height',
					),
				),
				'sale_price_color'      => array(
					'type'              	=> 'color',
					'label'             	=> __('Color', 'woopack'),
					'show_reset'        	=> true,
					'preview'           	=> array(
						'type'              	=> 'css',
						'selector'          	=> '.woocommerce ul.products li.product .price ins .amount, .woocommerce div.products div.product .price ins .amount',
						'property'          	=> 'color',
					),
				),
			),
		),
		'short_description'=> array(
			'title'                 => __('Short Description', 'woopack'),
			'fields'                => array(
				'short_description_font'            => array(
					'type'                  			=> 'font',
					'label'             				=> __('Font', 'woopack'),
					'default' 		        			=> array(
						'family'                			=> 'Default',
						'weight'                			=> 300
					),
					'preview'               			=> array(
						'type'                  			=> 'font',
						'selector'              			=> '.woocommerce ul.products li.product .woopack-product-description, .woocommerce div.products div.product .woopack-product-description',
						'property'		        			=> 'font-family',
					),
				),
				'short_description_font_size'       => array(
					'type'                  			=> 'select',
					'label'  			    			=> __('Font Size', 'woopack'),
					'default'               			=> 'default',
					'options' 		        			=> array(
						'default' 		        			=> __('Default', 'woopack'),
						'custom'                			=> __('Custom', 'woopack'),
					),
					'toggle'                			=> array(
						'custom'                			=> array(
							'fields'                			=> array('short_description_font_size_custom')
						),
					),
				),
				'short_description_font_size_custom'=> array(
					'type'              				=> 'unit',
					'label'                 				=> __('Custom Font Size', 'woopack'),
					'description' 							=> 'px',
					'preview'								=> array(
						'type' 									=> 'css',
						'selector'								=> '.woocommerce ul.products li.product .woopack-product-description, .woocommerce div.products div.product .woopack-product-description',
						'property'      						=> 'font-size',
						'unit'									=> 'px',
					),
					'responsive' 							=> array(
						'placeholder' 							=> array(
							'default' 								=> '',
							'medium' 								=> '',
							'responsive' 							=> '',
						),
					),
				),
				'short_description_line_height'	    => array(
					'type'              				=> 'unit',
					'label'                 				=> __('Line Height', 'woopack'),
					'responsive' 							=> array(
						'placeholder' 							=> array(
							'default' 								=> '',
							'medium' 								=> '',
							'responsive' 							=> '',
						),
					),
					'preview'								=> array(
						'type' 									=> 'css',
						'selector'								=> '.woocommerce ul.products li.product .woopack-product-description, .woocommerce div.products div.product .woopack-product-description',
						'property'      						=> 'line-height',
					),
				),
				'short_description_color'           => array(
					'type'              				=> 'color',
					'label'             				=> __('Color', 'woopack'),
					'show_reset'        				=> true,
					'preview'           				=> array(
						'type'              				=> 'css',
						'selector'          				=> '.woocommerce ul.products li.product .woopack-product-description, .woocommerce div.products div.product .woopack-product-description',
						'property'          				=> 'color',
					),
				),
				'product_description_margin_bottom' => array(
					'type' 								=> 'unit',
					'label' 							=> __('Margin Bottom', 'woopack'),
					'default' 							=> '',
					'description' 						=> 'px',
					'responsive' 						=> array(
						'placeholder' 						=> array(
							'default' 							=> '',
							'medium' 							=> '',
							'responsive'						=> '',
						),
					),
					'preview'           				=> array(
						'type'              				=> 'css',
						'selector'          				=> '.woocommerce ul.products li.product .woopack-product-description, .woocommerce div.products div.product .woopack-product-description',
						'property'          				=> 'margin-bottom',
						'unit'								=> 'px'
					),
				),
			),
		),
		'sale_badge_fonts'      => array(
			'title'             	=> __('Sale Badge', 'woopack'),
			'fields'            	=> array(
				'sale_badge_font'            => array(
					'type'						=> 'font',
					'label'             		=> __('Font', 'woopack'),
					'default' 		        	=> array(
						'family'                	=> 'Default',
						'weight'                	=> 300
					),
					'preview'               	=> array(
						'type'                  	=> 'font',
						'selector'              	=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale, .woocommerce .product span.onsale',
						'property'		        	=> 'font-family',
					),
				),
				'sale_badge_font_size'       => array(
					'type'                  	=> 'select',
					'label'  			    	=> __('Font Size', 'woopack'),
					'default'               	=> 'default',
					'options' 		        	=> array(
						'default' 		        	=> __('Default', 'woopack'),
						'custom'                	=> __('Custom', 'woopack'),
					),
					'toggle'                	=> array(
						'custom'                	=> array(
							'fields'                	=> array('sale_badge_font_size_custom')
						),
					),
				),
				'sale_badge_font_size_custom'=> array(
					'type'              		=> 'unit',
					'label'                 	=> __('Custom Font Size', 'woopack'),
					'description' 				=> 'px',
					'responsive' 				=> array(
						'placeholder' 				=> array(
							'default' 					=> '',
							'medium' 					=> '',
							'responsive' 				=> '',
						),
					),
					'preview'					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale, .woocommerce .product span.onsale',
						'property'      			=> 'font-size',
						'unit'						=> 'px',
					),
				),
				'sale_badge_line_height'	 => array(
					'type'              		=> 'unit',
					'label'                 	=> __('Line Height', 'woopack'),
					'responsive' 				=> array(
						'placeholder' 				=> array(
							'default' 					=> '',
							'medium' 					=> '',
							'responsive' 				=> '',
						),
					),
					'preview'					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
						'property'      			=> 'line-height',
					),
				),
			),
		),
		'out_of_stock_fonts'    => array(
			'title'             	=> __('Out of Stock', 'woopack'),
			'fields'            	=> array(
				'out_of_stock_font'            => array(
					'type'						=> 'font',
					'label'             		=> __('Font', 'woopack'),
					'default' 		        	=> array(
						'family'                	=> 'Default',
						'weight'                	=> 300
					),
					'preview'               	=> array(
						'type'                  	=> 'font',
						'selector'              	=> '.woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span',
						'property'		        	=> 'font-family',
					),
				),
				'out_of_stock_font_size'       => array(
					'type'                  	=> 'select',
					'label'  			    	=> __('Font Size', 'woopack'),
					'default'               	=> 'default',
					'options' 		        	=> array(
						'default' 		        	=> __('Default', 'woopack'),
						'custom'                	=> __('Custom', 'woopack'),
					),
					'toggle'                	=> array(
						'custom'                	=> array(
							'fields'                	=> array('out_of_stock_font_size_custom')
						),
					),
				),
				'out_of_stock_font_size_custom'=> array(
					'type'              		=> 'unit',
					'label'                 	=> __('Custom Font Size', 'woopack'),
					'description' 				=> 'px',
					'responsive' 				=> array(
						'placeholder' 				=> array(
							'default' 					=> '',
							'medium' 					=> '',
							'responsive' 				=> '',
						),
					),
					'preview'					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span',
						'property'      			=> 'font-size',
						'unit'						=> 'px',
					),
				),
				'out_of_stock_line_height'	 => array(
					'type'              		=> 'unit',
					'label'                 	=> __('Line Height', 'woopack'),
					'responsive' 				=> array(
						'placeholder' 				=> array(
							'default' 					=> '',
							'medium' 					=> '',
							'responsive' 				=> '',
						),
					),
					'preview'					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span',
						'property'      			=> 'line-height',
					),
				),
			),
		),
		'meta_fonts'            => array(
			'title'                 => __('Product Taxonomy', 'woopack'),
			'fields'				=> array(
				'meta_font'             => array(
					'type'                  => 'font',
					'label'             	=> __('Font', 'woopack'),
					'default' 		        => array(
						'family'                => 'Default',
						'weight'                => 300
					),
					'preview'               => array(
						'type'                  => 'font',
						'selector'              => '.woocommerce ul.products li.product .woopack-product-meta .product_meta, .woocommerce div.products div.product .woopack-product-meta .product_meta',
						'property'		        => 'font-family',
					),
				),
				'meta_font_size'        => array(
					'type'                  => 'select',
					'label'  			    => __('Font Size', 'woopack'),
					'default'               => 'default',
					'options' 		        => array(
						'default' 		        => __('Default', 'woopack'),
						'custom'                => __('Custom', 'woopack'),
					),
					'toggle'                => array(
						'custom'                => array(
							'fields'                => array('meta_font_size_custom')
						),
					),
				),
				'meta_font_size_custom' => array(
					'type'              	=> 'unit',
					'label'                 	=> __('Custom Font Size', 'woopack'),
					'description' 				=> 'px',
					'responsive' 				=> array(
						'placeholder' 				=> array(
							'default' 					=> '',
							'medium' 					=> '',
							'responsive' 				=> '',
						),
					),
					'preview'					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce ul.products li.product .woopack-product-meta .product_meta, .woocommerce div.products div.product .woopack-product-meta .product_meta',
						'property'      			=> 'font-size',
						'unit'						=> 'px',
					),
				),
			),
		),
		'quick_view_fonts' 	    => array(
			'title'                 => __('Quick View', 'woopack'),
			'fields'                => array(
				'quick_view_font'           => array(
					'type'                  	=> 'font',
					'label'             		=> __('Font', 'woopack'),
					'default' 		        	=> array(
						'family'                	=> 'Default',
						'weight'                	=> 300
					),
					'preview'               	=> array(
						'type'                  	=> 'font',
						'selector'              	=> '.woopack-product-quick-view p',
						'property'		        	=> 'font-family',
					),
				),
				'quick_view_font_size'      => array(
					'type'                  	=> 'select',
					'label'  			    	=> __('Font Size', 'woopack'),
					'default'               	=> 'default',
					'options' 		        	=> array(
						'default' 		        	=> __('Default', 'woopack'),
						'custom'                	=> __('Custom', 'woopack'),
					),
					'toggle'                	=> array(
						'custom'                	=> array(
							'fields'                	=> array('quick_view_font_size_custom')
						),
					),
				),
				'quick_view_font_size_custom'=> array(
					'type'              		=> 'unit',
					'label'                 	=> __('Custom Font Size', 'woopack'),
					'description' 				=> 'px',
					'preview'					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woopack-product-quick-view p',
						'property'      			=> 'font-size',
						'unit'						=> 'px',
					),
					'responsive' 				=> array(
						'placeholder' 				=> array(
							'default' 					=> '',
							'medium' 					=> '',
							'responsive' 				=> '',
						),
					),
				),
			),
		),
	) );
}
