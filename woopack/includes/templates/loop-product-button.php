<?php global $product; ?>

<?php do_action( 'woopack_loop_before_product_action_wrap', $settings, $product ); ?>

<div class="woopack-product-action">
	<?php do_action( 'woopack_loop_product_action_wrap_start', $settings, $product ); ?>

    <?php if ( 'cart' == $settings->button_type ) : ?>

        <?php if ( ! $product->is_type( 'variable' ) && ! $product->is_type( 'grouped' ) && isset( $settings->qty_input ) && ( 'before_button' == $settings->qty_input || 'above_button' == $settings->qty_input ) ) : // Quantity input field before button. ?>
			
			<?php do_action( 'woopack_loop_before_product_quantity', $settings, $product ); ?>
            
			<span class="woopack-qty-input quantity">
                <input type="number" id="quantity_<?php echo $product->get_id(); ?>" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="<?php _e('Quantity', 'woopack'); ?>" size="4" pattern="[0-9]*" inputmode="numeric" />
            </span>

			<?php do_action( 'woopack_loop_after_product_quantity', $settings, $product ); ?>

        <?php endif; ?>

		<?php do_action( 'woopack_loop_before_product_button', $settings, $product ); ?>

		<?php

		if ( $product->is_type( 'variable' ) && isset( $settings->variation_fields ) && 'yes' == $settings->variation_fields ) {
			woocommerce_variable_add_to_cart();
		} elseif ( $product->is_type( 'grouped' ) && isset( $settings->variation_fields ) && 'yes' == $settings->variation_fields ) {
			woocommerce_grouped_add_to_cart();
		} else {
			// Add to Cart button.
			$class = implode( ' ', array_filter( array(
				'button',
				'product_type_' . $product->get_type(),
				$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
				$product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : '',
				'alt',
			) ) );
	
			echo sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s" aria-label="%s">%s</a>',
				esc_url( $product->add_to_cart_url() ),
				esc_attr( isset( $quantity ) ? $quantity : 1 ),
				esc_attr( $product->get_id() ),
				esc_attr( $product->get_sku() ),
				esc_attr( isset( $class ) ? $class : 'button' ),
				esc_attr( $product->add_to_cart_description() ),
				esc_html( $product->add_to_cart_text() )
			);
		}
        ?>

		<?php do_action( 'woopack_loop_after_product_button', $settings, $product ); ?>

        <?php if ( ! $product->is_type( 'variable' ) && ! $product->is_type( 'grouped' ) && isset( $settings->qty_input ) && 'after_button' == $settings->qty_input ) : // Quantity input field after button. ?>

			<?php do_action( 'woopack_loop_before_product_quantity', $settings, $product ); ?>

            <span class="woopack-qty-input quantity">
                <input type="number" id="quantity_<?php echo $product->get_id(); ?>" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="<?php _e('Quantity', 'woopack'); ?>" size="4" pattern="[0-9]*" inputmode="numeric" />
            </span>

			<?php do_action( 'woopack_loop_after_product_quantity', $settings, $product ); ?>

        <?php endif; ?>

    <?php endif; ?>

    <?php if ( 'custom' == $settings->button_type ) : // Custom button. ?>
		<?php do_action( 'woopack_loop_before_product_button', $settings, $product ); ?>
        <a href="<?php echo get_permalink(); ?>" rel="bookmark" class="button" target="<?php echo $settings->button_target; ?>" title="<?php echo get_the_title(); ?>"><?php echo $settings->button_text; ?></a>
		<?php do_action( 'woopack_loop_after_product_button', $settings, $product ); ?>
    <?php endif; ?>

	<?php do_action( 'woopack_loop_product_action_wrap_end', $settings, $product ); ?>
</div>

<?php do_action( 'woopack_loop_after_product_action_wrap', $settings, $product ); ?>
