<?php do_action( 'woopack_loop_before_product_image_wrap', $settings, $product ); ?>

<div class="woopack-product-image">
	<a href="<?php echo get_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
		<?php do_action( 'woopack_product_before_image', $settings, $product ); ?>
		<?php do_action( 'woopack_loop_before_product_image', $settings, $product ); ?>

	    <?php if ( $product->is_on_sale() && 'yes' == $settings->show_sale_badge ) : ?>
			<?php
				$sale_badge = sprintf( '<span class="onsale">%s</span>', esc_html__('Sale!', 'woopack') );
				echo apply_filters( 'woocommerce_sale_flash', $sale_badge, $post, $product );
			?>
	    <?php endif; ?>

	    <?php the_post_thumbnail( $settings->image_size, array( 'data-no-lazy' => '1', 'class' => 'woopack-product-featured-image' ) ); ?>

		<?php if ( 'yes' == $settings->show_quick_view ) : ?>
		    <div class="woopack-product-quick-options">
		        <div class="woopack-product-quick-view">
		            <p>
		            	<?php echo $settings->quick_view_custom_text; ?>
					</p>
		        </div>
		    </div>
		<?php endif; ?>
		
		<?php if ( ! $product->is_in_stock() ) { ?>
			<div class="woopack-out-of-stock">
				<span>
					<?php
					if ( isset( $settings->out_of_stock_text ) ) { 
						echo $settings->out_of_stock_text; 
					} else {
						echo esc_html__('Out of Stock', 'woopack');
					} ?>
				</span>
			</div>
		<?php }	?>

		<?php do_action( 'woopack_loop_after_product_image', $settings, $product ); ?>
		<?php do_action( 'woopack_product_after_image', $settings, $product ); ?>
	</a>
</div>

<?php do_action( 'woopack_loop_after_product_image_wrap', $settings, $product ); ?>