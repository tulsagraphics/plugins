<?php
FLBuilderModel::default_settings($settings, array(
	'select_taxonomy'		    => 'none',
	'show_taxonomy_custom_text'	=> 'no',
	'taxonomy_custom_text'	    => __('Category:', 'woopack'),
));
?>

<?php do_action( 'woopack_loop_before_product_meta_wrap', $settings, $product ); ?>

<div class="woopack-product-meta">
    <div class="product_meta">
        <?php do_action( 'woocommerce_product_meta_start' ); ?>

        <?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
            <span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'woopack' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woopack' ); ?></span></span>
        <?php endif; ?>

		<span class="posted_in">
		<?php
			if( $settings->show_taxonomy_custom_text == 'yes' ) {
				echo $settings->taxonomy_custom_text;
			} else {
				echo __('Category: ', 'woopack');
			}
			$pos_type_slug 	= $settings->post_type;
			$product_tax 	= $settings->select_taxonomy;
			$product_terms	= wp_get_post_terms( $product->get_id(), $product_tax, array('fields' => 'all') );

			$terms = '';

			if( !empty( $product_terms ) && $product_tax != 'none' ) {
				foreach ($product_terms as $term) {

					$term_link = get_term_link( $term );
					$terms .= '<a href="'.$term_link.'">'.$term->name.'</a>, ';

				}
				$terms = rtrim( $terms, ', ' );
				echo $terms;
			}
		?>
		</span>
        <?php do_action( 'woocommerce_product_meta_end' ); ?>
    </div>
</div>

<?php do_action( 'woopack_loop_after_product_meta_wrap', $settings, $product ); ?>
