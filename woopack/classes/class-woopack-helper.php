<?php

class WooPack_Helper {
    static public $order;
    /**
     * Get list of modules
     *
     * @since 1.1.0
     * @return array
     */
    static public function get_modules()
    {
        return array(
            'single-product',
            'add-to-cart',
            'product-grid',
            'product-carousel',
            'cart',
			'checkout',
			'product-category',
        );
    }

    /**
     * Get admin label from settings.
     *
     * @since 1.0.0
     * @return string $admin_label
     */ 
    static public function get_admin_label()
    {
        $admin_label = WooPack_Admin_Settings::get_option( 'woopack_admin_label' );
        $admin_label = trim( $admin_label ) !== '' ? trim( $admin_label ) : 'WooPack';

        return $admin_label;
    }

    /**
     * Get group name for modules from settings.
     *
     * @since 1.1.0
     * @return string $group_name
     */
    static public function get_modules_group()
    {
        $group_name = WooPack_Admin_Settings::get_option( 'woopack_builder_label' );
        $group_name = trim( $group_name ) !== '' ? trim( $group_name ) : 'WooPack ' . __('Modules', 'woopack');

        return $group_name;
    }

    /**
     * Get list of Products
     *
     * @since 1.0.0
     * @return array $options Array of products
     */
    static public function get_products_list()
    {
        global $wpdb;

        $type = 'product';
        $options = array();

        $posts = $wpdb->get_results( $wpdb->prepare( "
            SELECT ID, post_title FROM {$wpdb->posts}
            WHERE post_type = %s
            AND post_status = 'publish'
            ORDER BY post_date DESC
        ", $type ) );

        foreach ( $posts as $post ) {
            $options[$post->ID] = $post->post_title;
        }

        return $options;
    }

    /**
     * Get All Taxonomies
     *
     * @since 1.0.0
     * @return array $options Array of taxonomies
     */
    static public function get_taxonomies_list() {
        $options = array();

        $slug = 'product';
        $taxonomies = FLBuilderLoop::taxonomies($slug);

        foreach($taxonomies as $tax_slug => $tax) {
            $options[$tax_slug] = $tax->label;
        }

        return $options;
    }

    /**
     * Get color value either hex or rgb.
     *
     * @since 1.0.0
     * @return string $color
     */
    static public function get_color_value( $color )
    {
        if ( $color == '' || ! $color ) {
            return;
        }
        if ( false === strpos( $color, 'rgb' ) ) {
            return '#' . $color;
        } else {
            return $color;
        }
    }

    /**
    * Renders the CSS class for each post item.
    *
    * @since 1.0.0
    * @return void
    */
    static public function render_post_class( $product, $settings, $layout = 'grid' )
    {
        $show_image = has_post_thumbnail() && $settings->show_image;
        $classes    = array( 'woopack-product-' . $layout );
        $classes[] = 'woopack-product-align-' . $settings->product_align;

        if ( $product ) {
            $classes[] = 'product';
            $classes[] = wc_get_loop_class();
            $classes[] = $product->get_stock_status();

            if ( $product->is_on_sale() ) {
                $classes[] = 'sale';
            }
            if ( $product->is_featured() ) {
                $classes[] = 'featured';
            }
            if ( $product->is_downloadable() ) {
                $classes[] = 'downloadable';
            }
            if ( $product->is_virtual() ) {
                $classes[] = 'virtual';
            }
            if ( $product->is_sold_individually() ) {
                $classes[] = 'sold-individually';
            }
            if ( $product->is_taxable() ) {
                $classes[] = 'taxable';
            }
            if ( $product->is_shipping_taxable() ) {
                $classes[] = 'shipping-taxable';
            }
            if ( $product->is_purchasable() ) {
                $classes[] = 'purchasable';
            }
            if ( $product->get_type() ) {
                $classes[] = "product-type-" . $product->get_type();
            }
            if ( $product->is_type( 'variable' ) ) {
                if ( ! $product->get_default_attributes() ) {
                    $classes[] = 'has-default-attributes';
                }
                if ( $product->has_child() ) {
                    $classes[] = 'has-children';
                }
            }
        }

        if ( false !== ( $key = array_search( 'hentry', $classes ) ) ) {
            unset( $classes[ $key ] );
        }

        post_class( apply_filters( "woopack_product_{$layout}_classes", $classes, $settings ) );
    }

    /**
     * Output CSS conditionally.
     *
     * @since 1.0.0
     * @return void
     */
    static public function print_css( $property, $value, $unit = '', $print = true )
    {
        if ( ! $print || $value == '' ) {
            return;
        }

        $color_properties = array('background-color', 'background', 'color', 'border-color', 'fill');

        if ( 'font' == $property ) {
            if ( $value['family'] != 'Default' ) {
                FLBuilderFonts::font_css( $value );
            }
        }
        elseif ( in_array( $property, $color_properties ) ) {
            $color_value = self::get_color_value( $value );
            echo "{$property}: {$color_value}{$unit};\n"; // Here $unit maybe !important
        }
        else {
            echo "{$property}: {$value}{$unit};\n";
        }
    }

    /**
     * Build pagination.
     *
     * @since 1.1.0
     * @return void
     */
    static public function pagination( $query, $current_url = '', $paged = 1 ) {
		$total_pages = $query->max_num_pages;
        $permalink_structure = get_option( 'permalink_structure' );
        $current_url = empty( $current_url ) ? get_pagenum_link() : $current_url;
		$base = untrailingslashit( html_entity_decode( $current_url ) );

		if ( $total_pages > 1 ) {

			if ( ! $current_page = $paged ) { // @codingStandardsIgnoreLine
				$current_page = 1;
			}

			$base = FLBuilderLoop::build_base_url( $permalink_structure, $base );

			echo paginate_links(array(
				'base'	   => $base . '%_%',
				'format'   => '/#page-%#%',
				'current'  => $current_page,
				'total'	   => $total_pages,
				'type'	   => 'list',
			));
		}
    }
    
    static public function featured_products($query_args)
    {
        $query_args['tax_query'][] = array(
            'taxonomy'         => 'product_visibility',
            'terms'            => 'featured',
            'field'            => 'name',
            'operator'         => 'IN',
            'include_children' => false,
        );

        return $query_args;
    }

    static public function best_selling_products($query_args)
    {
        $query_args['meta_key'] = 'total_sales';
        $query_args['order']    = 'DESC';
        $query_args['orderby']  = 'meta_value_num';
        
        return $query_args;
    }

    static public function sale_products($query_args)
    {
        $final_ids = wc_get_product_ids_on_sale();
        if ( isset( $query_args['post__not_in'] ) && is_array( $query_args['post__not_in'] ) ) {
            $final_ids = array_diff( $final_ids, $query_args['post__not_in'] );
        }
        if ( isset( $query_args['post__in'] ) && is_array( $query_args['post__in'] ) ) {
            $query_args['post__in'] = array_merge( $query_args['post__in'], $final_ids );
        } else {
            $query_args['post__in'] = array_merge( array( 0 ), $final_ids );
        }

        return $query_args;
    }

    static public function top_rated_products($query_args)
    {
        $query_args['meta_key'] = '_wc_average_rating';
        $query_args['order']    = 'DESC';
        $query_args['orderby']  = 'meta_value_num';
        
        return $query_args;
    }

    static public function order_by_menu_order( $query )
    {
    }
    
    static public function order_by_rand( $query )
    {
        if ( $query->get('fl_builder_loop') ) {
            $query->set('orderby', 'rand');
        }
    }

    static public function order_by_date( $query )
    {
        if ( $query->get('fl_builder_loop') ) {
            $query->set('orderby', 'date ID');
            $query->set('order', ( 'asc' === self::$order ) ? 'ASC' : 'DESC');
        }
    }

    static public function order_by_price( $query )
    {
        if ( $query->get('fl_builder_loop') ) {
            $query->set('meta_key', '_price');
            $query->set('orderby', 'meta_value_num');
            if ( self::$order == 'desc' ) {
                $query->set('order', 'DESC');
            } else {
                $query->set('order', 'ASC');
            }
        }
    }

    static public function order_by_rating( $query )
    {
        if ( $query->get('fl_builder_loop') ) {
            $query->set('meta_key', '_wc_average_rating');
            $query->set('orderby', 'meta_value_num');
            $query->set('order', 'DESC');
        }
    }

    static public function order_by_popularity( $query )
    {
        if ( $query->get('fl_builder_loop') ) {
            $query->set('meta_key', 'total_sales');
            $query->set('orderby', 'meta_value_num');
            $query->set('order', 'DESC');
        }
    }
}
