<?php

class WooPack_Ajax {

    /**
     * Initializes actions.
     *
     * @since 1.0.0
     * @return void
     */
    static public function init()
    {
        add_action( 'wp_ajax_woopack_product_quick_view', __CLASS__ . '::get_product_quick_view' );
        add_action( 'wp_ajax_nopriv_woopack_product_quick_view', __CLASS__ . '::get_product_quick_view' );
        add_action( 'wp_ajax_woopack_grid_get_posts', __CLASS__ . '::get_ajax_posts' );
        add_action( 'wp_ajax_nopriv_woopack_grid_get_posts', __CLASS__ . '::get_ajax_posts' );
    }

    /**
     * Get Product Quick View.
     *
     * @since 1.0.0
     * @return void
     */
    static public function get_product_quick_view()
    {
        if ( ! isset( $_POST['product_id'] ) || empty( $_POST['product_id'] ) ) {
            return;
        }

        global $post;
        $product_id = intval( $_POST['product_id'] );

        $node_id = $_POST['node_id'];
        wp( 'p=' . $product_id . '&post_type=product' );

        ob_start();

        while( have_posts() ) : the_post();
            include WOOPACK_DIR . 'includes/product-quick-view.php';
        endwhile;

        echo ob_get_clean();

        die();
    }

    static public function get_ajax_posts()
    {
		$post_type = 'product';
        $settings = (object)$_POST['settings'];
        $response = array(
            'data'  => '',
            'pagination' => false,
        );

        global $wp_query;

        $args = array(
            'post_type'     => 'product',
            'post_status'   => 'publish',
            'ignore_sticky_posts'   => true,
            'woopack'       => true,
        );

        // posts filter.
        if ( isset( $settings->posts_product ) ) {
            
            $ids = $settings->posts_product;
            $arg = 'post__in';

            if ( isset( $settings->posts_product_matching ) ) {
                if ( ! $settings->posts_product_matching ) {
                    $arg = 'post__not_in';
                }
            }

            if ( ! empty( $ids ) ) {
                $args[$arg] = explode( ',', $ids );
            }
        }

        // author filter.
        if ( isset( $settings->users ) ) {
            
            $users = $settings->users;
            $arg = 'author__in';
            
            // Set to NOT IN if matching is present and set to 0.
			if ( isset( $settings->users_matching ) && ! $settings->users_matching ) {
				$arg = 'author__not_in';
            }

            if ( !empty( $users ) ) {
                if ( is_string( $users ) ) {
				    $users = explode( ',', $users );
                }
                
                $args[$arg] = $users;
            }
        }

        if ( isset( $settings->posts_per_page ) ) {
            $args['posts_per_page'] = $settings->posts_per_page;
        }

        if ( isset( $settings->filter_taxonomy ) && isset( $_POST['term'] ) ) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => $settings->filter_taxonomy,
                    'field'    => 'slug',
                    'terms'    => $_POST['term']
                )
            );
        } else if ( isset( $settings->offset ) ) {
            //$args['offset'] = $settings->offset;
		}
		
		$taxonomies = FLBuilderLoop::taxonomies( $post_type );

		foreach ( $taxonomies as $tax_slug => $tax ) {

			$tax_value = '';
			$term_ids  = array();
			$operator  = 'IN';

			// Get the value of the suggest field.
			if ( isset( $settings->{'tax_' . $post_type . '_' . $tax_slug} ) ) {
				// New style slug.
				$tax_value = $settings->{'tax_' . $post_type . '_' . $tax_slug};
			} elseif ( isset( $settings->{'tax_' . $tax_slug} ) ) {
				// Old style slug for backwards compat.
				$tax_value = $settings->{'tax_' . $tax_slug};
			}

			// Get the term IDs array.
			if ( ! empty( $tax_value ) ) {
				$term_ids = explode( ',', $tax_value );
			}

			// Handle matching settings.
			if ( isset( $settings->{'tax_' . $post_type . '_' . $tax_slug . '_matching'} ) ) {

				$tax_matching = $settings->{'tax_' . $post_type . '_' . $tax_slug . '_matching'};

				if ( ! $tax_matching ) {
					// Do not match these terms.
					$operator = 'NOT IN';
				} elseif ( 'related' === $tax_matching ) {
					// Match posts by related terms from the global post.
					global $post;
					$terms 	 = wp_get_post_terms( $post->ID, $tax_slug );
					$related = array();

					foreach ( $terms as $term ) {
						if ( ! in_array( $term->term_id, $term_ids ) ) {
							$related[] = $term->term_id;
						}
					}

					if ( empty( $related ) ) {
						// If no related terms, match all except those in the suggest field.
						$operator = 'NOT IN';
					} else {

						// Don't include posts with terms selected in the suggest field.
						$args['tax_query'][] = array(
							'taxonomy'	=> $tax_slug,
							'field'		=> 'id',
							'terms'		=> $term_ids,
							'operator'  => 'NOT IN',
						);

						// Set the term IDs to the related terms.
						$term_ids = $related;
					}
				}
			}// End if().

			if ( ! empty( $term_ids ) ) {

				$args['tax_query'][] = array(
					'taxonomy'	=> $tax_slug,
					'field'		=> 'id',
					'terms'		=> $term_ids,
					'operator'  => $operator,
				);
			}
		}// End foreach().

        $args['tax_query'][] = array(
			'taxonomy'	=> 'product_visibility',
			'field'	    => 'name',
			'terms'     => 'exclude-from-catalog',
            'operator'  => 'NOT IN',
		);

        if ( 'yes' == get_option( 'woocommerce_hide_out_of_stock_items' ) ) {
            $args['meta_query'][] = array(
                'key'       => '_stock_status',
                'value'     => 'instock',
                'compare'   => '='
            );
        }

        if ( isset( $settings->order ) ) {
            $args['order'] = $settings->order;
        }

        if ( isset( $settings->order_by ) ) {
            $args['orderby'] = $settings->order_by;
        }

        if ( isset( $settings->product_source ) ) {
            $args = self::get_conditional_args( $settings->product_source, $args );
        }

        if ( isset( $_POST['page'] ) ) {
            $args['paged'] = absint( $_POST['page'] );
        }

        if ( isset( $_POST['orderby'] ) ) {
            $orderby = esc_attr( $_POST['orderby'] );
            
            $args = self::get_conditional_args( $orderby, $args );
        }

        $query = new WP_Query( $args );

        if ( $query->have_posts() ) :

            // create pagination.
            if ( $query->max_num_pages > 1 ) {
                ob_start();
               
                echo '<div class="fl-builder-pagination woopack-ajax-pagination">';
                WooPack_Helper::pagination( $query, $_POST['current_page'], $_POST['page'] );
                echo '</div>';

                $response['pagination'] = ob_get_clean();
            }

            // posts query.
            ob_start();

            while( $query->have_posts() ) {

        		$query->the_post();

                $product = wc_get_product( get_the_ID() );

                if ( is_object( $product ) && $product->is_visible() ) {
                    $product_data = $product->get_data();
                    include apply_filters( 'woopack_products_grid_layout_path', WOOPACK_DIR . 'modules/product-grid/includes/layout-' . $settings->product_layout . '.php' );
                }
            }
            
            wp_reset_postdata();

            $response['data'] = ob_get_clean();

        else :
            $response['data'] = '<li>' . esc_html__('No posts found.', 'woopack') . '</li>';
        endif;

        wp_reset_query();

        wp_send_json( $response );
    }

    static public function get_conditional_args( $type, $args )
    {
        switch ( $type ) :
            case 'featured':
                $args['tax_query'][] = array(
                    'taxonomy'         => 'product_visibility',
                    'terms'            => 'featured',
                    'field'            => 'name',
                    'operator'         => 'IN',
                    'include_children' => false,
                );
                break;

            case 'popularity':
            case 'best_selling':
                $args = WooPack_Helper::best_selling_products( $args );
                break;

            case 'rating':
            case 'top_rated':
                $args = WooPack_Helper::top_rated_products( $args );
                break;

            case 'sale':
                $args = WooPack_Helper::sale_products( $args );
                break;

            case 'date':
                $args['orderby'] = 'date ID';
                $args['order'] = 'DESC';
                break;

            case 'price':
                $args['meta_key'] = '_price';
                $args['order'] = 'ASC';
                $args['orderby'] = 'meta_value_num';
                break;

            case 'price-desc':
                $args['meta_key'] = '_price';
                $args['order'] = 'DESC';
                $args['orderby'] = 'meta_value_num';
                break;

            default:
                break;

        endswitch;

        return $args;
    }
}

WooPack_Ajax::init();
