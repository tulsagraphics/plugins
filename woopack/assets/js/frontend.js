(function($) {
    // Product quick view.
    $('body').delegate('.woopack-product-quick-options', 'click', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var nodeId      = $(this).parents('.fl-module').attr('data-node');
        var product     = $(this).parents('.product'),
            productId   = product.data('product-id');

        new WooPackModal({
            source: 'ajax',
            ajaxUrl: woopack_config.ajaxurl,
            ajaxData: {
                action: 'woopack_product_quick_view',
                node_id: nodeId,
                product_id: productId
            },
            breakpoint: 767,
            cssClass: 'woopack-modal-' + nodeId,
            onAjaxLoad: function(wrapper) {
                wrapper.find('.woocommerce-product-gallery .woocommerce-product-gallery__image a').on('click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();

                    var imageEl = $(this).parents('.woocommerce-product-gallery__image');
                    var firstEl = imageEl.parents('.woocommerce-product-gallery').find('.woocommerce-product-gallery__image:first');
                    imageEl.insertBefore(firstEl);
				});
				setTimeout( function() {
					// Trigger variation form
					if ( typeof $.fn.wc_variation_form !== 'undefined' ) {
						if ( wrapper.find('form.variations_form').length > 0 ) {
							wrapper.find('form.variations_form').wc_variation_form();
						}
					}
				}, 100);
            },
            onClose: function(wrapper) {
                wrapper.removeClass('woopack-modal-' + nodeId);
            }
        });
    });

    $(window).on('resize', function() {
        if ( window.innerWidth > 767 ) {
            $('.woopack-modal .woopack-modal-inner').css({
                height: 'auto'
            });
        }
    });

    // Quanity input.
    $('body').delegate('.woopack-qty-input input.qty', 'change, keyup, blur', function() {
        var qty = parseInt($(this).val());
        
        // it should be above 0
        qty = ( 0 === qty || '' === qty ) ? 1 : qty;
        
        $(this).val(qty);
        $(this).parents('.woopack-product-action').find('a.button').attr('data-quantity', qty);
	});

	// Quick View add-to-cart AJAX
	$('body').delegate('.woopack-modal form.cart', 'submit', function(e) {

		if ( $(this).hasClass('variations_form') && typeof $.fn.wc_variation_form === 'undefined' ) {
			return;
		}

		e.preventDefault();
		e.stopPropagation();

		var variations = false;
		var grouped = $(this).hasClass('grouped_form');
		var $thisbutton = $(this).find('button[type="submit"]');

		if ( $(this).hasClass('variations_form') ) {
			variations = $(this).data('product_variations');
		}

		if ( grouped ) {
			$(this).find('.woocommerce-grouped-product-list tr').each(function() {
				var qty = $(this).find('input.qty').val();
				var productId = $(this).attr('id').split('-')[1];

				if ( qty > 0 ) {
					var data = {
						'quantity': qty,
						'product_id': productId,
					};

					$thisbutton.removeClass( 'added' );
					$thisbutton.addClass( 'loading' );

					// Trigger event.
					$( document.body ).trigger( 'adding_to_cart', [ $thisbutton, data ] );

					// Ajax action.
					addToCart( $thisbutton, data );
				}
			});
		} else {
			var data = {
				'quantity': $(this).find('.qty').val(),
				'product_id': $(this).find('.single_add_to_cart_button').val(),
			};

			if ( $(this).find('input[name="variation_id"]').length > 0 ) {
				data['add-to-cart'] = $(this).find('input[name="add-to-cart"]').val();
				data['product_id'] = $(this).find('input[name="variation_id"]').val();
				if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
					data['variation_id'] = $(this).find('input[name="variation_id"]').val();
				}
			}

			$thisbutton.removeClass( 'added' );
			$thisbutton.addClass( 'loading' );

			// Trigger event.
			$( document.body ).trigger( 'adding_to_cart', [ $thisbutton, data ] );

			addToCart( $thisbutton, data );
		}
	});

	
	// Variation Add to Cart AJAX - Loop
	$('body').delegate('.woopack-products .product .variations_form, .woopack-product-add-to-cart .variations_form', 'submit', function(e) {
		e.preventDefault();
		e.stopPropagation();

		var data = {
			'quantity': $(this).find('.qty').val(),
			'add-to-cart': $(this).find('input[name="add-to-cart"]').val(),
			'product_id': $(this).find('input[name="variation_id"]').val(),
		};

		if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
			data['variation_id'] = $(this).find('input[name="variation_id"]').val();
		}

		var $thisbutton = $(this).find('button[type="submit"]');

		$thisbutton.removeClass( 'added' );
		$thisbutton.addClass( 'loading' );

		// Trigger event.
		$( document.body ).trigger( 'adding_to_cart', [ $thisbutton, data ] );

		// Ajax action.
		$.post( wc_add_to_cart_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'add_to_cart' ), data, function( response ) {
			if ( ! response ) {
				return;
			}

			if ( response.error && response.product_url ) {
				window.location = response.product_url;
				return;
			}

			// Redirect to cart option
			if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
				window.location = wc_add_to_cart_params.cart_url;
				return;
			}

			// Trigger event so themes can refresh other areas.
			$( document.body ).trigger( 'added_to_cart', [ response.fragments, response.cart_hash, $thisbutton ] );
		});
	});

	// Grouped product - add to cart
	$('body').delegate('.woopack-products .product .grouped_form', 'submit', function(e) {
		e.preventDefault();
		e.stopPropagation();

		var $thisbutton = $(this).find('button[type="submit"]');

		$(this).find('.woocommerce-grouped-product-list tr').each(function() {
			var qty = $(this).find('input.qty').val();
			var productId = $(this).attr('id').split('-')[1];

			if ( qty > 0 ) {
				var data = {
					'quantity': qty,
					'product_id': productId,
				};

				$thisbutton.removeClass( 'added' );
				$thisbutton.addClass( 'loading' );

				// Trigger event.
				$( document.body ).trigger( 'adding_to_cart', [ $thisbutton, data ] );

				// Ajax action.
				addToCart( $thisbutton, data );
			}
		});
	});

	var addToCart = function( $thisbutton, data ) {
		// Ajax action.
		$.post( wc_add_to_cart_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'add_to_cart' ), data, function( response ) {
			if ( ! response ) {
				return;
			}

			if ( response.error && response.product_url ) {
				window.location = response.product_url;
				return;
			}

			// Redirect to cart option
			if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
				window.location = wc_add_to_cart_params.cart_url;
				return;
			}

			// Trigger event so themes can refresh other areas.
			$( document.body ).trigger( 'added_to_cart', [ response.fragments, response.cart_hash, $thisbutton ] );
		});
	};

	// Update product image on variation dropdown change
	$('body').delegate('.woopack-products .product .variations_form select', 'change', function() {
		var $this = $(this), attr = $this.attr('name'), val = $this.val(),
			form = $this.parents('.variations_form'),
			img = $this.parents('.woopack-product-grid.product').find('.woopack-product-image img'),
			variations = form.data('product_variations');

		variations.forEach(function (item) {
			if ( 'undefined' !== typeof item.attributes[ attr ] && val === item.attributes[ attr ] ) {
				if ( 'undefined' !== typeof item.image ) {
					img.attr('src', item.image.thumb_src);
					img.attr('srcset', item.image.srcset);
				}
			}
		});
	});

	// Fix View Cart button appearance
	$(document).on('wc_cart_button_updated', function(e, button) {
		button.parent().find('a.added_to_cart').addClass('button alt');
	});

})(jQuery);
