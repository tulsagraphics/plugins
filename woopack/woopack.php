<?php
/**
 * Plugin Name: WooPack for Beaver Builder
 * Plugin URI: https://wpbeaveraddons.com/woopack/
 * Description: A set of custom, creative, unique modules for WooCommerce to speed up your eCommerce web design and development process.
 * Version: 1.3.4
 * Author: Team IdeaBox - Beaver Addons
 * Author URI: https://wpbeaveraddons.com
 * Copyright: (c) 2016 IdeaBox Creations LLP
 * License: GNU General Public License v2.0
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: woopack
 * Domain Path: /languages
 * WC tested up to: 3.3.4
 */

if ( ! defined( 'ABSPATH' ) ) { exit; }  // Exit if accessed directly.

final class WooPack {
    /**
     * Holds the class object.
     *
     * @since 1.0.0
     * @var object $instance
     */
	public static $instance;

    /**
     * Holds error messages.
     *
     * @since 1.0.0
     * @var array $errors
     */
    public static $errors = array();

    /**
	 * Primary class constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct()
	{
		if ( ! function_exists( 'is_plugin_active' ) ) {
			include_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

        // Check whether Beaver Builder plugin is installed and activated or not.
        if ( ! class_exists( 'FLBuilder' ) ) {
            if ( ! is_plugin_active( 'beaver-builder-lite-version' . '/fl-builder.php' ) ) {
                self::$errors[] = esc_html__('Please install and activate Beaver Builder plugin to use WooPack for Beaver Builder.', 'woopack');
            }
        }

        // Check whether WooCommerce plugin is installed and activated or not.
        if ( ! class_exists( 'WooCommerce' ) || ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
            self::$errors[] = esc_html__('Please install and activate WooCommerce plugin to use WooPack for Beaver Builder.', 'woopack');
        }

        // If there are errors, print error messages and do not proceed further.
        if ( count( self::$errors ) ) {
            add_action( 'admin_notices', array( $this, 'admin_notices' ) );
            add_action( 'network_admin_notices', array( $this, 'admin_notices' ) );
            return;
        }

        // Define constants.
		$this->define_constants();

        // Hooks.
        $this->init_hooks();

        // Classes and updater.
		require_once WOOPACK_DIR . 'classes/class-woopack-helper.php';
		require_once WOOPACK_DIR . 'classes/class-woopack-wpml.php';
		require_once WOOPACK_DIR . 'classes/class-woopack-ajax.php';
		require_once WOOPACK_DIR . 'classes/class-admin-settings.php';
		require_once WOOPACK_DIR . 'includes/updater/update-config.php';

		// Includes.
		include WOOPACK_DIR . 'includes/fields-product-style.php';
		include WOOPACK_DIR . 'includes/fields-product-button.php';
		include WOOPACK_DIR . 'includes/fields-product-typography.php';
    }
    
    /**
	 * Initializes actions and filters.
	 *
	 * @since 1.0.0
	 * @return void
	 */
    public function init_hooks()
    {
        add_action( 'init', array( $this, 'init_modules' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
        add_action( 'wp_footer', array( $this, 'footer_scripts' ) );
        add_action( 'wp_footer', array( $this, 'render_modal_box' ) );
        add_action( 'fl_builder_loop_query_args', array( $this, 'hide_out_of_stock_products' ), 10, 1 );
    }

	/**
	 * Define WooPack constants.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	private function define_constants()
	{
		define( 'WOOPACK_VER', '1.3.4' );
		define( 'WOOPACK_DIR', plugin_dir_path( __FILE__ ) );
		define( 'WOOPACK_URL', plugins_url( '/', __FILE__ ) );
		define( 'WOOPACK_PATH', plugin_basename( __FILE__ ) );
		define( 'WOOPACK_CAT',  $this->register_wl_cat() );
	}

    /**
     * Custom scripts
     *
     * @since 1.0.0
     * @return void
     */
	public function enqueue_scripts()
	{
        
        $page_arg = is_front_page() ? 'page' : 'paged';
        $paged = get_query_var( $page_arg, 1 );

		wp_register_script( 'jquery-imagesloaded', WOOPACK_URL . 'assets/js/jquery.imagesloaded.js', array('jquery'), WOOPACK_VER, false );

        wp_register_style( 'owl-carousel-css', WOOPACK_URL . 'assets/css/owl.carousel.css', array(), WOOPACK_VER );
		wp_register_style( 'owl-carousel-theme-css', WOOPACK_URL . 'assets/css/owl.theme.css', array(), WOOPACK_VER );
		wp_register_script( 'owl-carousel-js', WOOPACK_URL . 'assets/js/owl.carousel.min.js', array('jquery'), WOOPACK_VER, false );

		wp_register_style( 'woopack-modal-box', WOOPACK_URL . 'assets/css/modal.css', array(), WOOPACK_VER );
		wp_register_script( 'woopack-modal-box', WOOPACK_URL . 'assets/js/modal.js', array('jquery'), WOOPACK_VER, true );

		wp_enqueue_script( 'woopack-frontend', WOOPACK_URL . 'assets/js/frontend.js', array('jquery'), WOOPACK_VER, true );
		wp_localize_script( 'woopack-frontend', 'woopack_config', array(
            'ajaxurl'		=> admin_url( 'admin-ajax.php' ),
            'page'     		=> $paged,
			'current_page' 	=> home_url($_SERVER['REQUEST_URI']),
			'woo_url' 		=> defined( 'WC_PLUGIN_FILE' ) ? plugins_url( '/', WC_PLUGIN_FILE ) : ''
		) );
	}

    /**
     * Scripts to render in footer.
     *
     * @since 1.0.0
     * @return void
     */
    public function footer_scripts()
    {
        if ( is_user_logged_in() && FLBuilderModel::is_builder_active() ) {
            $modules                = WooPack_Helper::get_modules();
            $form_title_selector    = array();
            $layout_field_selecor   = array();
            foreach ( $modules as $module ) {
                $form_title_selector[] = "form.fl-builder-{$module}-settings .fl-lightbox-header h1:before";
                $layout_field_selecor[] = ".fl-builder-{$module}-settings .fl-builder-settings-fields .fl-layout-field-option";
            }
            ?>
            <style type="text/css">
            <?php echo implode( ',', $form_title_selector ); ?> {
    			content: "<?php echo WooPack_Helper::get_admin_label(); ?>";
    			position: relative;
    			display: inline-block;
                margin-right: 5px;
    		}
            </style>
            <script type="text/javascript">
            (function($) {
                // Render preview on layout field change event.
                $('body').delegate('<?php echo implode( ',', $layout_field_selecor ); ?>', 'click', function() {
                    if ( 'undefined' !== typeof FLBuilder && 'function' === typeof FLBuilder.preview.preview ) {
                        setTimeout(function() {
                            FLBuilder.preview.preview();
                        }, 500);
                    }
                });
            })(jQuery);
            </script>
            <?php
        }
    }

    /**
	 * Render Modal Box.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function render_modal_box()
	{
		include WOOPACK_DIR . 'includes/modal-box.php';
	}

    /**
	 * Include modules.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function init_modules()
	{
        $modules 		= WooPack_Helper::get_modules();
		$modules_dir 	= WOOPACK_DIR . 'modules/';
		$theme_dir 		= '';

		if ( is_child_theme() ) {
			$theme_dir = get_stylesheet_directory();
		} else {
			$theme_dir = get_template_directory();
		}

        foreach ( $modules as $module ) {
			$module_path = $module . '/' . $module . '.php';

			if ( file_exists( $theme_dir . '/woopack/modules/' . $module_path ) ) {
				require_once $theme_dir . '/woopack/modules/' . $module_path;
			} else {
				if ( file_exists( $modules_dir . $module_path ) ) {
					require_once $modules_dir . $module_path;
				}
			}
        }
	}

    /**
     * Conditionally hide out of stock products.
     *
     * @since 1.0.0
     * @param $args array   List of query arguments.
     * @return array
     */
	public function hide_out_of_stock_products( $args )
	{
		if ( isset( $args['settings'] ) && ! isset( $args['settings']->woopack ) ) {
			return $args;
		}

        $hide = get_option('woocommerce_hide_out_of_stock_items');

        if ( 'no' == $hide ) {
            return $args;
        }

        if ( $outofstock_term = get_term_by( 'name', 'outofstock', 'product_visibility' ) ) {

            $args['tax_query'][] = array(
                'taxonomy'  => 'product_visibility',
                'field'     => 'term_taxonomy_id',
                'terms'     => array( $outofstock_term->term_taxonomy_id ),
                'operator'  => 'NOT IN'
            );
        }

        return $args;
    }

    /**
	 * Admin notices.
	 *
	 * @since 1.0.0
	 * @return void
	 */
    public function admin_notices()
    {
        if ( count( self::$errors ) ) {
            foreach ( self::$errors as $error ) {
                ?>
                <div class="notice notice-error">
					<p><?php echo $error; ?></p>
                </div>
                <?php
            }
        }
    }

    /**
	 * Returns the singleton instance of the class.
	 *
	 * @since 1.0.0
	 * @return object The WooPack object.
	 */
	public static function get_instance()
	{
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof WooPack ) ) {
			self::$instance = new WooPack();
		}

		return self::$instance;
	}

	/**
     * Register white label category
     *
     * @since 1.0.0
     * @return string $woopack
     */
	public function register_wl_cat()
	{
		$woopack = ( is_multisite() ) ? get_site_option( 'woopack_builder_label' ) : get_option( 'woopack_builder_label' );

		if ( '' == $woopack || false == $woopack ) {
			$woopack = esc_html__( 'WooPack Modules', 'woopack' );
		}

		return $woopack;
	}
}

// Load the WooPack class.
$woopack = WooPack::get_instance();


/**
 * Enable white labeling setting form after re-activating the plugin
 *
 * @since 1.0.0
 * @return void
 */
function woopack_plugin_activation() {
	delete_option( 'woopack_hide_form' );
	delete_option( 'woopack_hide_plugin' );
	if ( is_network_admin() ) {
		delete_site_option( 'woopack_hide_form' );
		delete_site_option( 'woopack_hide_plugin' );
	}
}
register_activation_hook( __FILE__, 'woopack_plugin_activation' );
