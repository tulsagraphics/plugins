!function ($) {
  $(function(){	
	$('.dropdown-toggle').dropdown();	
	$('.navbar').scrollspy();	
	$(".collapse").collapse();
	$('.accordion-toggle ').addClass('collapsed'); 
	
    // make code pretty
    window.prettyPrint && prettyPrint()
})
}(window.jQuery)