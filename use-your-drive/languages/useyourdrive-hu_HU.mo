��    G      T  a   �             $   &     K     S  "   e  	   �  
   �     �     �     �     �     �     �     �       )        B     `     s     �     �     �     �     �     �     �     
            
         +     ;  
   @     K     P     _     t  (   �  )   �     �     �     �     	     	      	  
   '	  !   2	     T	     \	     a	     h	     y	     �	  
   �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     %
     2
  '   ?
  *   g
  4   �
     �
  P  �
     ,  (   A     j     v     �     �     �     �     �                    4     C     L  2   b     �     �     �     �     �  +        ?      E  %   f  %   �     �     �     �     �     �     �          #     /     >     X     m  x   �                '     /     =     I     X     s  
   �     �  	   �     �     �     �  
   �  
   �     �     �       	        %     :     L     h     �     �     �  *   �  )   �  6   )     `        -          '                  9       $   0         8       2   B       	       #         ?      3       ;   %   /       7            F      "       >   &   <   6   +       G          :      !   .                
                                =          (   E                               )   ,   4   @   D   C             1          *   5   A        %d file(s) uploaded: ... or find documents on your device A guest Add a description Add a new folder in this directory Add files Add folder Allowed formats:  Back to our first folder Cancel Close Creating shared link... Date modified Delete Delete selected files Do you really want to delete these files? Do you really want to delete: Download all files Download file Download files as ZIP Download selected files Drag your files here Error Failed to add folder File type not allowed Filetype not allowed Folder Guest In queue Loading... Max file size:  Name New folder Next No description No file was uploaded No files or folders found Not succesfully uploaded to Google Drive Oops! This shouldn't happen... Try again! Preview Preview in new window Previous Previous folder Refresh Rename Rename to: Requested directory isn't allowed Results Save Saved! Search filenames Select folder Selected folder Share file Sharing link Size Start Start slideshow Success Upload files Upload folder Uploading failed Uploading to Cloud Uploading to Server View as grid View as list You are not authorized to rename folder You are not authorized to rename this file You are not authorized to rename this file or folder and within contents Project-Id-Version: Use-your-Drive v1.5.3
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-09-07 11:24+0200
PO-Revision-Date: 2017-09-07 11:56+0200
Last-Translator: 
Language-Team: 
Language: hu_HU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 2.0.3
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 %d fájl feltöltve: vagy tallózd ki a számítógépeden... Egy vendég Adhatsz hozzá leírást... Új mappa hozzáadása Fájl feltöltése Új mappa hozzáadása Megengedett formátumok:  Vissza a kezdő mappába Mégsem Bezár Megosztási link létrehozása Dátum szerint Törlés Kijelöltek törlése Valóban ki akarod törölni ezeket a fájlokat? : Valóban ki akarod törölni? : Összes letöltése Kép letöltése... ZIP fájl letöltése Csak  a kijelöltek letöltése Csak húzd ide a feltöltendő fájlokat... Hiba! Mappa hozzáadása nem sikerült Ez a fájl típus nem engedélyezett! Ez a fájl típus nem engedélyezett! Mappa Vendég Feltöltésre kész Betöltés... Maximális lméret / fájl:  Név szerint Új mappa hozzáadása Következő Nincs leírás Nem lett fájl feltöltve Hopsz! Ez itt üres! Nem sikerült feltölteni Hopsz! Valami váratlan hiba történt. Talán próbáld meg frissíteni a galériát! (jobb oldalon a frissítés gomb) Megtekintés Megtekintés új ablakban Előző Előző mappa Frissítés Átnevezés... Átnevezés erre a névre: A kért mappa nem elérhető Találatok Mentés Elmentve! Keresés Válassz mappát Választott mappa Megosztás Megosztás Méret szerint Kezdés Vetítés indítása Sikerült Fájlok feltöltése Mappa fetöltése A feltöltés nem sikerült Feltöltés folyamatban... Feltöltés a szerverre... Átváltás rács nézetre Átváltás lista nézetre Nincs jogosultságod átnevezni a mappát! Nincs jogosultságod átnevezni a fájlt! Nincs jogosultságod átnevezni a fájlt vagy mappát! ... 