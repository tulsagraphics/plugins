
(function($) {

	AstraPortfolio = {

		_ref			: null,

		/**
		 * _api_params = {
		 * 		'search'                  : '',
		 * 		'per_page'                : '',
		 * 		'astra-portfolio-categories'     : '',
		 * 		'astra-portfolio-other-categories' : '',
		 * 		'page'                    : '',
		 *   };
		 *
		 * E.g. per_page=<page-id>&astra-portfolio-categories=<category-ids>&astra-portfolio-other-categories=<page-builder-ids>&page=<page>
		 */
		_api_params           : {},
		_breakpoint           : 768,
		_iconUploader         : null,
	
		init: function()
		{
			this._showSiteOnLoad();
			this._masonry();
			this._resetPagedCount();
			this._bind();
			this._display();
		},

		/**
		 * Show Site On Load.
		 *
		 * @since 1.0.2
		 */
		_showSiteOnLoad: function()
		{
			if( AstraPortfolio._getParamFromURL('portfolio') )
			{
				var slug      = AstraPortfolio._getParamFromURL('portfolio');

				var api_params = {
					slug : slug,
				};

				// API Request.
				var api_url = astraPortfolio.apiEndpoint + 'astra-portfolio?' + decodeURIComponent( $.param( api_params ) );
				$.ajax({
					url   : api_url,
					cache : false
				})
				.done(function( items, status, XHR ) {

					if( 'success' === status && items.length && items[0] ) {
						console.log( JSON.stringify( items[0] ) );
						var portfolio_type = ( 'portfolio-type' in items[0] ) ? items[0]['portfolio-type'] : '';
						var site_url       = ( 'astra-site-url' in items[0] ) ? items[0]['astra-site-url'] + 'TB_iframe=true&width=600&height=550' : '';
						var title          = ( 'title' in items[0] ) ? items[0]['title']['rendered'] : '';
						var rel            = false;

						console.log('portfolio_type: ' + portfolio_type);

						switch( portfolio_type ) {
							case 'image':
											var lightbox_url = ( 'lightbox-image-url' in items[0] ) ? items[0]['lightbox-image-url'] : '';
											jQuery.magnificPopup.open({
												items: {
													src: lightbox_url
												},
												type: 'image'
											}, 0);
								break;
							case 'video':
											var video_url = ( 'portfolio-video-url' in items[0] ) ? items[0]['portfolio-video-url'] : '';
											jQuery.magnificPopup.open({
												items: {
													src: video_url
												},
												type: 'iframe'
											}, 0);
								break;
							case 'iframe':
										if( title && site_url ) {
											AstraPortfolio._showSingleSite( title, site_url, rel );
										}
								break;
						}
					}
				});
			} 
		},

		/**
		 * Preview Open
		 * 
		 * @param  {object} event Object.
		 */
		_showSingleSite: function( title, href, rel )
		{
			if( href )
			{
				$('html').addClass('processing');

				var location = astraPortfolio.settings["preview-bar-loc"] || 'bottom';

				tb_show( title, href, rel );

				var currentSiteTitle = jQuery("#TB_ajaxWindowTitle").text();

				var responseive_buttons = astraPortfolio.settings['responsive-button'] || true;
				if( responseive_buttons ) {
					var template = wp.template('astra-portfolio-responsive-view');
					jQuery('#TB_closeAjaxWindow').prepend( template );
				}
		
				jQuery('#TB_iframeContent').wrap('<div id="TB_iframeContent-wrapper"></div>');
		
				jQuery('#TB_window')
					.addClass( location )
					.addClass('desktop');
				
				if( astraPortfolio.siteLoadingEnabled ) {
					jQuery('#TB_window')
						.addClass('astra-portfolio-thickbox-loading')
						.append('<div class="site-loading"><h3>'+astraPortfolio.siteLoadingTitle+'</h3><p>'+astraPortfolio.siteLoadingMessage+'</p></div>');
				}
			}
		},

		/**
		 * Get URL param.
		 */
		_getParamFromURL: function(name, url)
		{
		    if (!url) url = window.location.href;
		    name = name.replace(/[\[\]]/g, "\\$&");
		    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		        results = regex.exec(url);
		    if (!results) return null;
		    if (!results[2]) return '';
		    return decodeURIComponent(results[2].replace(/\+/g, " "));
		},

		_display: function() {

			// Show portfolios, If Category (disabled), Other Category(disabled) & Search(enabled).
			if( 'no' === astraPortfolio.settings['show-other-categories'] &&
				'no' === astraPortfolio.settings['show-categories'] &&
				'yes' === astraPortfolio.settings['show-search']
			) {
				AstraPortfolio._showSites();
			}

			// Show portfolios, If Category (disabled), Other Category(disabled) & Search(disabled).
			if( ! $('.astra-portfolio-filters').length ) {
				AstraPortfolio._showSites();
				return;
			}

			/**
			 * Categories
			 */
			if( 'yes' === astraPortfolio.settings['show-categories'] ) {
				var category_data = 'categories';
				var category_slug = 'astra-portfolio-categories';
				var category = {
					slug          : category_slug + AstraPortfolio._getCategoryParams( category_data ),
					id            : category_slug + '-wrap',
					class         : category_slug,
					trigger       : 'astra-api-all-category-loaded',
					wrapper_class : 'filter-links',
					show_all      : AstraPortfolio._getCategoryAllSelectStatus( category_slug ),
				};
				AstraPortfolioAPI._api_request( category );
			}

			/**
			 * Other Categories
			 */
			if( 'yes' === astraPortfolio.settings['show-other-categories'] ) {
				var category_data = 'other-categories';
				var category_slug = 'astra-portfolio-other-categories';
				var category = {
					slug          : category_slug + AstraPortfolio._getCategoryParams( category_data ),
					id            : category_slug + '-wrap',
					class         : category_slug,
					trigger       : 'astra-api-category-loaded',
					wrapper_class : 'filter-links',
					show_all      : false,
				};

				AstraPortfolioAPI._api_request( category );
			}
		},
		
		/**
		 * Binds events for the Astra Portfolio.
		 *
		 * @since 1.0.0
		 * @access private
		 * @method _bind
		 */
		_bind: function()
		{
			$( window ).on('resize', 									AstraPortfolio._resize );

			$( document ).on('astra-portfolio-api-request-fail',		AstraPortfolio._apiFailed );
			$( document ).on('astra-api-post-loaded-on-scroll',			AstraPortfolio._reinitGridScrolled );
			$( document ).on('astra-api-post-loaded', 					AstraPortfolio._reinitGrid );
			$( document ).on('astra-api-category-loaded', 				AstraPortfolio._addFilters );
			$( document ).on('astra-api-all-category-loaded', 			AstraPortfolio._loadFirstGrid );

			$( document ).on('click', '.iframe:not(.open-in-new-tab) .site-preview', 			AstraPortfolio._previewOpen );
			$( document ).on('click', '.actions a', 					AstraPortfolio._previewResponsive );
			$( 'body' ).on('thickbox:removed', 							AstraPortfolio._previewClose );
			$( 'body' ).on('thickbox:iframe:loaded', 					AstraPortfolio._previewLoaded );

			// Event's for API request.
			$( document ).on('keyup input', '.astra-portfolio-search', 	AstraPortfolio._search );
			$( document ).on('scroll', 									AstraPortfolio._scroll );
			$( document ).on('click', '.filter-links a', 				AstraPortfolio._filterClick );

		},

		/**
		 * Remove thickbox loading class
		 * 
		 * @param  object event Event object.
		 * @return void.
		 */
		_previewLoaded: function( event ) {
			event.preventDefault();
			jQuery('#TB_window').removeClass('astra-portfolio-thickbox-loading');
		},

		/**
		 * Lightbox init.
		 */
		_lightboxInit: function( ) {

			$('.site-single.image').magnificPopup({
				delegate: 'a',
				type: 'image',
				tLoading: 'Loading image #%curr%...',
				mainClass: 'astra-portfolio-lightbox mfp-img-mobile',
				gallery: {
					enabled: true,
					navigateByImgClick: true,
					preload: [0,1] // Will preload 0 - before current, and 1 after the current image
				},
				image: {
					tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
					titleSrc: function(item) {
						return item.el.attr('title');
					}
				},
				callbacks: {
					change: function( item ) {
						console.log('change: ');
						console.log('item: ' + JSON.stringify( item ));
						var slug = jQuery( item.el ).parents('.site-single').attr('data-slug') || '';
						if( slug ) {

							AstraPortfolio._clean_url_params( 'portfolio' );

							var url_params = {
								'portfolio' : slug
							};

							// Change URL.
							if( ! AstraPortfolio._getParamFromURL('portfolio') ) {
								var current_url = window.location.href;
								var current_url_separator = ( window.location.href.indexOf( "?" ) === -1 ) ? "?" : "&";

								var new_url = current_url + current_url_separator + decodeURIComponent( $.param( url_params ) );
								AstraPortfolio._changeURL( new_url );
							}
						}
					},
					open: function( ) {
						var slug = jQuery( this.currItem.el ).parents('.site-single').attr('data-slug') || '';
						if( slug ) {
							var url_params = {
								'portfolio' : slug
							};

							// Change URL.
							if( ! AstraPortfolio._getParamFromURL('portfolio') ) {
								var current_url = window.location.href;
								var current_url_separator = ( window.location.href.indexOf( "?" ) === -1 ) ? "?" : "&";

								var new_url = current_url + current_url_separator + decodeURIComponent( $.param( url_params ) );
								AstraPortfolio._changeURL( new_url );
							}
						}
					},
					close: function( ) {
						AstraPortfolio._clean_url_params( 'portfolio' );
					}
				}
			});

			$('.site-single.video').magnificPopup({
				delegate: 'a',
				disableOn: 700,
				type: 'iframe',
				mainClass: 'astra-portfolio-popup mfp-fade',
				removalDelay: 160,
				preloader: false,

				fixedContentPos: false,
				callbacks: {
					open: function( ) {
						var slug = jQuery( this.currItem.el ).parents('.site-single').attr('data-slug') || '';
						if( slug ) {
							var url_params = {
								'portfolio' : slug
							};

							// Change URL.
							if( ! AstraPortfolio._getParamFromURL('portfolio') ) {
								var current_url = window.location.href;
								var current_url_separator = ( window.location.href.indexOf( "?" ) === -1 ) ? "?" : "&";

								var new_url = current_url + current_url_separator + decodeURIComponent( $.param( url_params ) );
								AstraPortfolio._changeURL( new_url );
							}
						}
					},
					close: function( ) {
						AstraPortfolio._clean_url_params( 'portfolio' );
					}
				}
			});

		},

		_clean_url_params: function( single_param )
		{
			var url_params = AstraPortfolio._getQueryStrings();
			delete url_params[ single_param ];
			delete url_params[''];		// Removed extra empty object.

			var current_url = window.location.href;
			var root_url = current_url.substr(0, current_url.indexOf('?')); 
			if( jQuery.isEmptyObject( url_params ) ) {
				var new_url = root_url + decodeURIComponent( $.param( url_params ) );
			} else {
				var current_url_separator = ( root_url.indexOf( "?" ) === -1 ) ? "?" : "&";
				var new_url = root_url + current_url_separator + decodeURIComponent( $.param( url_params ) );
			}

			AstraPortfolio._changeURL( new_url );
		},

		/**
		 * Responsive On Click.
		 */
		_previewResponsive: function( event ) {

			event.preventDefault();

			var icon = jQuery(this).find('.dashicons');

			var viewClass = icon.attr('data-view') || '';

			jQuery('#TB_window').removeClass( 'desktop' );
			jQuery('#TB_window').removeClass( 'tablet' );
			jQuery('#TB_window').removeClass( 'mobile' );
			jQuery('#TB_window').addClass( viewClass );

			jQuery('.actions .dashicons').removeClass('active');
			icon.addClass('active');

			jQuery('#TB_iframeContent').removeClass();
			jQuery('#TB_iframeContent').addClass( viewClass );

		},

		/**
		 * On Filter Clicked
		 */
		_filterClick: function( event ) {

			event.preventDefault();

			$(this).parents('.filter-links').find('a').removeClass('active');
			$(this).addClass('active');

			// Clean data before process request.
			$('.astra-portfolio').addClass('hide-me');
			$('.astra-portfolio-wrap').find('.spinner').removeClass('hide-me').addClass('is-active');
			$('.astra-portfolio-wrap').find('.astra-portfolio-not-found').addClass('hide-me');
			$('.no-more-demos').addClass('hide-me');
			$('.astra-portfolio-search').val('');

	        // Show sites.
			AstraPortfolio._showSites();

			if( $('.filters-wrap').length ) {
				
				$('html, body').animate({
					scrollTop: $('.filters-wrap').offset().top - 100
				});
			}
		},

		/**
		 * On Resize
		 */
		_resize: function() {

			if( AstraPortfolio._windowWidth() ) {

				jQuery('.astra-portfolio').masonry().masonry( 'destroy' );

				// Init masonry.
				AstraPortfolio._masonry();
			}
		},

		/**
		 * Preview Close
		 * 
		 * @param  {object} event Object.
		 */
		_previewClose: function( event )
		{
			event.preventDefault();
			$('html').removeClass('processing');

			var url_params = AstraPortfolio._getQueryStrings();
			delete url_params['portfolio'];

			var current_url = window.location.href;
			var root_url = current_url.substr(0, current_url.indexOf('?')); 
			if( jQuery.isEmptyObject( url_params ) ) {
				var new_url = root_url + decodeURIComponent( $.param( url_params ) );
			} else {
				var current_url_separator = ( root_url.indexOf( "?" ) === -1 ) ? "?" : "&";
				var new_url = root_url + current_url_separator + decodeURIComponent( $.param( url_params ) );
			}

			// Change URL.
			AstraPortfolio._changeURL( new_url );
		},

		/**
		 * Preview Open
		 * 
		 * @param  {object} event Object.
		 */
		_previewOpen: function( event ) {
			event.preventDefault();
			
			// Site Preview.
			var title 	 = jQuery(this).data('title') || jQuery(this).data('name') || null,
				href     = jQuery(this).data('href') || jQuery(this).data('alt'),
				rel      = jQuery(this).data('rel') || false,
				slug     = jQuery(this).parents('.site-single').attr('data-slug') || '';

			AstraPortfolio._showSingleSite( title, href, rel );

			var url_params = {
				'portfolio' : slug
			};

			// Change URL.
			if( ! AstraPortfolio._getParamFromURL('portfolio') ) {
				var current_url = window.location.href;
				var current_url_separator = ( window.location.href.indexOf( "?" ) === -1 ) ? "?" : "&";

				var new_url = current_url + current_url_separator + decodeURIComponent( $.param( url_params ) );
				
				AstraPortfolio._changeURL( new_url );
			}
		},

		/**
		 * Get query strings.
		 * 
		 * @param  string string Query string.
		 * @return string     	 Check and return query string.
		 */
		_getQueryStrings: function( string )
		{
			return ( string || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
		},

		/**
		 * Clean the URL.
		 * 
		 * @param  string url URL string.
		 * @return string     Change the current URL.
		 */
		_changeURL: function( url )
		{
			History.pushState(null, null, url);
		},

		/**
		 * Lazy Load Images
		 *
		 * @see  http://jquery.eisbehr.de/lazy/#features
		 */
		_lazyLoad: function()
		{
			$('.astra-portfolio img').Lazy({
				effect: 'fadeIn',
				onFinishedAll: function() {
					if( AstraPortfolio._windowWidth() ) {
						$('.astra-portfolio').masonry('reload');
					}
				}
			});
		},

		/**
		 * Init Masonry.
		 * 
		 * @see  /wp-includes/js/jquery/jquery.masonry.min.js (Source http://masonry.desandro.com).
		 */
		_masonry: function() {

			if( AstraPortfolio._windowWidth() ) {
				jQuery('.astra-portfolio').masonry({
					horizontalOrder : false,
					percentPosition : false
				});
			}
		},

		/**
		 * Show Sites
		 * 
		 * @param  {Boolean}
		 */
		_windowWidth: function() {

			if( jQuery( window ).outerWidth() > AstraPortfolio._breakpoint ) {
				return true;
			}
			
			return false;
		},

		// Add 'search'
		_apiAddParam_search: function() {
			var search_val = jQuery('.astra-portfolio-search').val() || '';
			if( '' !== search_val ) {
				AstraPortfolio._api_params['search'] = search_val;
			}
		},

		_apiAddParam_per_page: function() {
			// Add 'per_page'
			var per_page_val = 15;
			if( astraPortfolio.settings && astraPortfolio.settings["par-page"] ) {
				per_page_val = parseInt( astraPortfolio.settings["par-page"] );
			}
			AstraPortfolio._api_params['per_page'] = per_page_val;
		},

		_apiAddParam_astra_site_tags: function() {
			// Add 'astra-portfolio-tags'
			var tags = '' + $('.astra-portfolio-wrap').data( 'tags' ) || '';
			if( '' !== tags ) {
				if ( tags.indexOf(',') > -1) {
					tags = tags.replace(/^,|,$/g,'');;
				}

				AstraPortfolio._api_params['astra-portfolio-tags'] =  tags;
			}
		},

		_apiAddParam_astra_site_category: function()
		{
			if( $('.filter-links.astra-portfolio-categories').length ) {
				var selected_category_id = $('.filter-links.astra-portfolio-categories').find('.active').data('group') || '';
				if( '' !== selected_category_id && 'all' !== selected_category_id ) {
					AstraPortfolio._api_params['astra-portfolio-categories'] =  selected_category_id;
				} else {
					var categories = $('.astra-portfolio-wrap').data( 'categories' );
					if( '' !== categories ) {
						AstraPortfolio._api_params['astra-portfolio-categories'] =  categories;
					}
				}

			} else {
				
				var tags = '' + $('.astra-portfolio-wrap').data( 'categories' ) || '';
				if( '' !== tags ) {
					if ( tags.indexOf(',') > -1) {
						tags = tags.replace(/^,|,$/g,'');;
					}

					AstraPortfolio._api_params['astra-portfolio-categories'] =  tags;
				}
			}
		},

		_apiAddParam_astra_site_page_builder: function() {

			if( $('.filter-links.astra-portfolio-other-categories').length ) {
				var selected_category_id = $('.filter-links.astra-portfolio-other-categories').find('.active').data('group') || '';
				if( '' !== selected_category_id && 'all' !== selected_category_id ) {
					AstraPortfolio._api_params['astra-portfolio-other-categories'] =  selected_category_id;
				}

			} else {
				
				var tags = '' + $('.astra-portfolio-wrap').data( 'other-categories' ) || '';
				if( '' !== tags ) {
					if ( tags.indexOf(',') > -1) {
						tags = tags.replace(/^,|,$/g,'');;
					}

					AstraPortfolio._api_params['astra-portfolio-other-categories'] =  tags;
				}
			}
		},

		_apiAddParam_page: function() {
			// Add 'page'
			var page_val = parseInt(jQuery('body').attr('data-astra-demo-paged')) || 1;
			AstraPortfolio._api_params['page'] = page_val;
		},

		/**
		 * Show Sites
		 * 
		 * 	Params E.g. per_page=<page-id>&astra-portfolio-categories=<category-ids>&astra-portfolio-other-categories=<page-builder-ids>&page=<page>
		 *
		 * @param  {Boolean} resetPagedCount Reset Paged Count.
		 * @param  {String}  trigger         Filtered Trigger.
		 */
		_showSites: function( resetPagedCount, trigger ) {

			if( undefined === resetPagedCount ) {
				resetPagedCount = true
			}

			if( undefined === trigger ) {
				trigger = 'astra-api-post-loaded';
			}

			if( resetPagedCount ) {
				AstraPortfolio._resetPagedCount();
			}

			// Add Params for API request.
			AstraPortfolio._api_params = {};

			AstraPortfolio._apiAddParam_search();
			AstraPortfolio._apiAddParam_per_page();
			AstraPortfolio._apiAddParam_astra_site_tags( );
			AstraPortfolio._apiAddParam_astra_site_category();
			AstraPortfolio._apiAddParam_astra_site_page_builder();
			AstraPortfolio._apiAddParam_page();

			// API Request.
			var api_post = {
				slug: 'astra-portfolio' + astraPortfolio.ApiURLSep + decodeURIComponent( $.param( AstraPortfolio._api_params ) ),
				trigger: trigger,
			};

			AstraPortfolioAPI._api_request( api_post );

		},

		/**
		 * Get Category Params
		 * 
		 * @param  {string} category_data Category Slug.
		 * @return {mixed}               Add `include=<category-ids>` in API request.
		 */
		_getCategoryParams: function( category_data ) {

			// Con-cat to convert number into string.
			var categories = '' + $('.astra-portfolio-wrap').data( category_data ) || '';

			if( categories ) {
				if ( categories.indexOf(',') > -1) {
					return '?per_page=100&include='+categories.replace(/^,|,$/g,'');
				} else {
					return '?per_page=100&include='+categories;
				}
			}

			return '?per_page=100';
		},

		/**
		 * Get All Select Status
		 * 
		 * @param  {string} category_slug Category Slug.
		 * @return {boolean}              Return true/false.
		 */
		_getCategoryAllSelectStatus: function( category_slug ) {	

			// Has category?
			if( category_slug in astraPortfolio.settings ) {

				// Has `all` in stored list?
				if( $.inArray('all', astraPortfolio.settings[ category_slug ]) === -1 ) {
					return false;
				}
			}

			return true;
		},

		/**
		 * Load First Grid.
		 *
		 * This is triggered after all category loaded.
		 * 
		 * @param  {object} event Event Object.
		 */
		_loadFirstGrid: function( event, data ) {

			AstraPortfolio._addFilters( event, data );
			setTimeout(function() {
				AstraPortfolio._showSites();
			}, 100);
		},

		/**
		 * Append filters.
		 * 
		 * @param  {object} event Object.
		 * @param  {object} data  API response data.
		 */
		_addFilters: function( event, data ) {
			event.preventDefault();

			// Show portfolios, If Category (disabled), Other Category(enabled) & Search(enabled/disabled).
			if( ! astraPortfolio.settings['categories'] ) {
				setTimeout(function() {
					AstraPortfolio._showSites( );
				}, 100);
			}

			if( jQuery('.' + data.args.id).length ) {
				var template = wp.template('astra-portfolio-filters');
				jQuery('.' + data.args.id).html(template( data )).find('li:first a').addClass('active');
			}

		},

		/**
		 * Append sites on scroll.
		 * 
		 * @param  {object} event Object.
		 * @param  {object} data  API response data.
		 */
		_reinitGridScrolled: function( event, data ) {

			var template = wp.template('astra-portfolio-list');

			if( data.items.length > 0 ) {

				jQuery('.filter-count .count').text( data.items_count );

				setTimeout(function() {
					jQuery('.astra-portfolio').append(template( data ));

					AstraPortfolio._lazyLoad();
					AstraPortfolio._imagesLoaded();
				}, 800);
			} else {
				$('.astra-portfolio-wrap').find('.spinner').removeClass('is-active');
				$('.astra-portfolio-wrap').find('.no-more-demos').removeClass('hide-me');
				$('.astra-portfolio-wrap').find('.astra-portfolio-not-found').addClass('hide-me');
			}
		},

		/**
		 * Update Astra sites list.
		 * 
		 * @param  {object} event Object.
		 * @param  {object} data  API response data.
		 */
		_reinitGrid: function( event, data ) {

			if( data.items.length > 0 ) {
				var template = wp.template('astra-portfolio-list');

				jQuery('.filter-count .count').text( data.items_count );
				jQuery('body').attr('data-astra-demo-last-request', data.items_count);

				jQuery('.astra-portfolio').html(template( data ));
				AstraPortfolio._lazyLoad();
				AstraPortfolio._imagesLoaded();
			} else {
				$('.astra-portfolio-wrap').find('.spinner').removeClass('is-active').addClass('hide-me');
				$('.astra-portfolio-wrap').find('.astra-portfolio-not-found').removeClass('hide-me');
			}
		},

		/**
		 * Check image loaded with function `imagesLoaded()`
		 */
		_imagesLoaded: function() {

			$('.astra-portfolio').removeClass('hide-me');
			
			$('.astra-portfolio-grid').imagesLoaded()
			.always( function( instance ) {
				if( AstraPortfolio._windowWidth() ) {
					$('.astra-portfolio').masonry('reload');
					$('.astra-portfolio-wrap').find('.spinner').removeClass('is-active');
					AstraPortfolio._lightboxInit();
				}
			})
			.progress( function( instance, image ) {
				var result = image.isLoaded ? 'loaded' : 'broken';
			});

		},

		/**
		 * API Request Failed/Not found any demos.
		 */
		_apiFailed: function(e, args, jqXHR, textStatus) {

			var status = jqXHR.status || 0;

			$('.astra-portfolio-wrap').find('.spinner').removeClass('is-active');

			if( $('.astra-portfolio .site-single').length ) {
				$('.astra-portfolio-wrap').find('.no-more-demos').removeClass('hide-me');
			} else {
				$('.astra-portfolio-wrap').find('.astra-portfolio-not-found').removeClass('hide-me');			
			}
		},

		/**
		 * Search Site.
		 */
		_search: function() {

					
			$this = jQuery('.astra-portfolio-search').val();

			$('.filter-links.astra-portfolio-categories a').removeClass('active');

			// Clean data before process request.
			$('.astra-portfolio').addClass('hide-me');
			$('.astra-portfolio-wrap').find('.spinner').removeClass('hide-me').addClass('is-active');
			$('.astra-portfolio-wrap').find('.astra-portfolio-not-found').addClass('hide-me');
			$('.no-more-demos').addClass('hide-me');

			window.clearTimeout(AstraPortfolio._ref);
			AstraPortfolio._ref = window.setTimeout(function () {
				AstraPortfolio._ref = null;

				AstraPortfolio._resetPagedCount();
				jQuery('body').addClass('loading-content');
				jQuery('body').attr('data-astra-demo-search', $this);

				AstraPortfolio._showSites( );

			}, 500);

		},

		/**
		 * On Scroll
		 */
		_scroll: function(event) {

			if( ! $('.astra-portfolio').length ) {
				return;
			}

			var scrollDistance = jQuery(window).scrollTop();

			var themesBottom = Math.abs(jQuery(window).height() - jQuery('.astra-portfolio').offset().top - jQuery('.astra-portfolio').height());

			ajaxLoading = jQuery('body').data('scrolling');

			if (scrollDistance > themesBottom && ajaxLoading == false) {
				AstraPortfolio._updatedPagedCount();

				if( $('.no-more-demos').hasClass('hide-me') ) {
					$('.astra-portfolio-wrap').find('.spinner').addClass('is-active');
				}
				
				jQuery('body').data('scrolling', true);

				/**
				 * @see _reinitGridScrolled() which called in trigger 'astra-api-post-loaded-on-scroll'
				 */
				AstraPortfolio._showSites( false, 'astra-api-post-loaded-on-scroll' );

			}
		
		},
		
		/**
		 * Update Page Count.
		 */
		_updatedPagedCount: function() {
			paged = parseInt(jQuery('body').attr('data-astra-demo-paged'));
			jQuery('body').attr('data-astra-demo-paged', paged + 1);
			window.setTimeout(function () {
				jQuery('body').data('scrolling', false);
			}, 800);
		},

		/**
		 * Reset Page Count.
		 */
		_resetPagedCount: function() {

			jQuery('body').attr('data-astra-demo-last-request', '1');
			jQuery('body').attr('data-astra-demo-paged', '1');
			jQuery('body').attr('data-astra-demo-search', '');
			jQuery('body').attr('data-scrolling', false);

		}

	};

	/**
	 * Initialize AstraPortfolio
	 */
	$(function(){
		AstraPortfolio.init();
	});

})(jQuery);