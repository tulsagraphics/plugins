<?php
/**
 * Shortcode Markup
 *
 * @package Astra Portfolio
 * @since 1.0.0
 */

do_action( 'astra_portfolio_shortcode_top', $args );

?>
<div class="astra-portfolio-wrap"
	data-other-categories="<?php echo esc_attr( $args['data']['other-categories'] ); ?>"
	data-categories="<?php echo esc_attr( $args['data']['categories'] ); ?>"
	data-tags="<?php echo esc_attr( $args['data']['tags'] ); ?>"
>
	<?php
	if (
		'yes' === $args['data']['show-other-categories'] ||
		'yes' === $args['data']['show-categories'] ||
		'yes' === $args['data']['show-search']
	) {
		?>
	<div class="astra-portfolio-filters" class="wp-filter hide-if-no-js">

		<!-- All Filters -->
		<div class="filters-wrap">
			<?php
			if ( 'yes' === $args['data']['show-other-categories'] ) {
			?>
				<div class="astra-portfolio-other-categories-wrap"></div>
			<?php } ?>				
			<?php if ( 'yes' === $args['data']['show-categories'] ) { ?>
				<div class="astra-portfolio-categories-wrap"></div>
			<?php } ?>				
		</div>

		<?php if ( 'yes' === $args['data']['show-search'] ) { ?>
			<div class="search-form">
				<label class="screen-reader-text" for="astra-portfolio-search"><?php _e( 'Search', 'astra-portfolio' ); ?> </label>
				<input placeholder="<?php _e( 'Search...', 'astra-portfolio' ); ?>" type="search" aria-describedby="live-search-desc" class="astra-portfolio-search">
			</div>
		<?php } ?>

	</div>
	<?php } ?>

	<!-- All Astra Portfolio -->
	<div class="astra-portfolio-grid astra-portfolio <?php echo esc_attr( $args['row_class'] ); ?>"></div><!-- .astra-portfolio -->

	<span class="spinner is-active"></span>
	<span class="astra-portfolio-not-found hide-me">
		<p>
			<?php _e( 'No items found.', 'astra-portfolio' ); ?><br/>
		</p>
	</span>
	<span class="no-more-demos hide-me">
		<?php
		if ( isset( $args['data']['no-more-sites-message'] ) ) {
			echo do_shortcode( $args['data']['no-more-sites-message'] );
		}
		?>
	</span>

</div><!-- .astra-portfolio. -->

<?php do_action( 'astra_portfolio_shortcode_bottom', $args ); ?>
