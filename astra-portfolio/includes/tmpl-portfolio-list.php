<?php
/**
 * Portfolio list
 *
 * @package Astra Portfolio
 * @since 1.0.6
 */

?>
<# if ( data.items.length ) { #>
	<# for ( key in data.items ) {

		var has_single_item = '';
		if( 1 == data.items[ key ]['astra-site-open-in-new-tab'] )
		{
			has_single_item = 'open-in-new-tab';
		}
		#>

		<div class="site-single {{ has_single_item }} <?php echo esc_attr( $args['column_class'] ); ?> {{ data.items[ key ]['portfolio-type'] }}" data-slug="{{ data.items[ key ]['slug'] }}">
			<div class="inner">

				<#
				var type = data.items[ key ]['portfolio-type'] || '';

				switch( type ) {
					case 'page': 
									var permalink = data.items[ key ]['link'] || '';
									#>
									<a target="_blank" class="site-preview" title="{{ data.items[ key ].title.rendered }}" href="{{{permalink}}}">
										<# if( '' !== data.items[ key ]['thumbnail-image-url'] ) { #>
											<img class="lazy" data-src="{{ data.items[ key ]['thumbnail-image-url'] }}" alt="{{ data.items[ key ]['thumbnail-image-meta']['alt'] }}" title="{{ data.items[ key ]['thumbnail-image-meta']['title'] }}" />
											<noscript>
												<img src="{{ data.items[ key ]['thumbnail-image-url'] }}" alt="{{ data.items[ key ]['thumbnail-image-meta']['alt'] }}" title="{{ data.items[ key ]['thumbnail-image-meta']['title'] }}" />
											</noscript>
										<# } #>
										<span class="view-demo-wrap">
											<span class="view-demo"> <?php _e( 'Quick View', 'astra-portfolio' ); ?> </span>
										</span>
									</a>
									<#
						break;
					case 'video': 
									var video_url = data.items[ key ]['portfolio-video-url'] || '';
									#>										
									<a class="site-preview" title="{{ data.items[ key ].title.rendered }}" href="{{{video_url}}}">
										<# if( '' !== data.items[ key ]['thumbnail-image-url'] ) { #>
											<img class="lazy" data-src="{{ data.items[ key ]['thumbnail-image-url'] }}" alt="{{ data.items[ key ]['thumbnail-image-meta']['alt'] }}" title="{{ data.items[ key ]['thumbnail-image-meta']['title'] }}" />
											<noscript>
												<img src="{{ data.items[ key ]['thumbnail-image-url'] }}" alt="{{ data.items[ key ]['thumbnail-image-meta']['alt'] }}" title="{{ data.items[ key ]['thumbnail-image-meta']['title'] }}" />
											</noscript>
										<# } #>
										<span class="view-demo-wrap">
											<span class="view-demo"> <?php _e( 'Quick View', 'astra-portfolio' ); ?> </span>
										</span>
									</a>
									<#
						break;
					case 'image': 
									var image_url = data.items[ key ]['lightbox-image-url'] || '';
									if( '' === image_url ) {
										image_url = data.items[ key ]['thumbnail-image-url'] || '';
									}
									#>
									<a class="site-preview" title="{{ data.items[ key ].title.rendered }}" href="{{{image_url}}}">
										<# if( '' !== data.items[ key ]['thumbnail-image-url'] ) { #>
											<img class="lazy" data-src="{{ data.items[ key ]['thumbnail-image-url'] }}" alt="{{ data.items[ key ]['thumbnail-image-meta']['alt'] }}" title="{{ data.items[ key ]['thumbnail-image-meta']['title'] }}" />
											<noscript>
												<img src="{{ data.items[ key ]['thumbnail-image-url'] }}" alt="{{ data.items[ key ]['thumbnail-image-meta']['alt'] }}" title="{{ data.items[ key ]['thumbnail-image-meta']['title'] }}" />
											</noscript>
										<# } #>
										<span class="view-demo-wrap">
											<span class="view-demo"> <?php _e( 'Quick View', 'astra-portfolio' ); ?> </span>
										</span>
									</a>
									<#
						break;

					case 'iframe': 
					default:

									if( 1 == data.items[ key ]['astra-site-open-in-new-tab'] ) { #>
										<a class="site-preview" href="{{ data.items[ key ]['astra-site-url'] }}" target="_blank" data-title="{{ data.items[ key ].title.rendered }}">
									<# } else { #>
										<span class="site-preview" data-href="{{ data.items[ key ]['astra-site-url'] }}TB_iframe=true&width=600&height=550" data-title="{{ data.items[ key ].title.rendered }}">
									<# } #>
										<# if( '' !== data.items[ key ]['thumbnail-image-url'] ) { #>
											<img class="lazy" data-src="{{ data.items[ key ]['thumbnail-image-url'] }}" alt="{{ data.items[ key ]['thumbnail-image-meta']['alt'] }}" title="{{ data.items[ key ]['thumbnail-image-meta']['title'] }}" />
											<noscript>
												<img src="{{ data.items[ key ]['thumbnail-image-url'] }}" alt="{{ data.items[ key ]['thumbnail-image-meta']['alt'] }}" title="{{ data.items[ key ]['thumbnail-image-meta']['title'] }}" />
											</noscript>
										<# } #>
										<span class="view-demo-wrap">
											<span class="view-demo"> <?php _e( 'Quick View', 'astra-portfolio' ); ?> </span>
										</span>
									<# if( 1 == data.items[ key ]['astra-site-open-in-new-tab'] ) { #>
										</a>
									<# } else { #>
										</span>
									<# }
						break;
				}
				#>
				<div class="template-meta">
					<div class="item-title">
						{{{ data.items[ key ].title.rendered }}}
						<# if ( data.items[ key ]['astra-site-type'] ) { #>
							<span class="site-type {{data.items[ key ]['astra-site-type']}}">{{data.items[ key ]['astra-site-type']}}</span>
						<# } #>
					</div>
				</div>
			</div>
		</div>
	<# } #>
<# } else { #>
	<div class="astra-portfolio-not-found">
		<p>
			<?php _e( 'No items found.', 'astra-portfolio' ); ?><br/>
		</p>
	</div>
<# } #>
