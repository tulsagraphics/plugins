��    Z      �      �      �     �      �     �     �     	       	   $     .     2  -   A     o     �     �     �     �     �     �     �     �     �     �  \        k     w  #   �  	   �  
   �     �     �     �     �     �       	   	          (     0  
   9     D     R     `  2   p     �     �     �  J   �     	     .	  
   :	  (   E	     n	     {	     �	  	   �	     �	  #   �	     �	     �	  	   
     
     
     #
     (
     .
     2
     7
     >
     P
  	   V
     `
     m
     u
     �
     �
     �
     �
     �
               %     A      \  !   }      �  &   �     �  !     '   $     L  �   h     a  $   t     �     �     �     �     �          
  2        Q     e     i     z     �     �     �     �  	   �  )   �     �  P        X     i  )   �     �  
   �     �     �     �     �     �  	   �  	                  '     0     >     P     d  7   }     �     �     �  P   �     =     P  
   k  E   v     �     �  
   �            4   #  	   X  3   b  	   �     �     �     �     �     �     �     �     �     �               "     +     H  "   b     �  	   �  	   �     �      �     �     �  	   �  	   �     �               *     6     I   Activate License Activate your license to enable! Add New Add New Portfolio Add New Tags Add Portfolio Add image All All Portfolios All available extensions have been installed! Available Extensions Bottom Brainstorm Force Close Columns Deactivate License Debug Description. Display Display responsive buttons. Display sites search box. Display the portfolio of Astra Starter Sites & other portfolio items easily on your website. Downloading Enable sorting by categories. Enable sorting by other categories. Enter URL Extensions Extensions  Help Image Import Import Starter Sites Install Installed Installed Extensions License Licenses Loading... New Portfolio New Tags Name No items found. Number of items per row. Supports maximum 4 items. One moment please. Open in New Tab Page Builder Paste the shortcode on page where you need to display all portfolio items. Portfolio Image Preview Bar Quick View Quick create portfolio using below form. Rewrite Slug Rewrite portfolio url slug. Search Search... Select Type... Set portfolio preview bar location. Settings Settings saved successfully. Shortcode Single Page System Info Tags Title Top Type Update Validate Purchase Video Video URL WP Portfolio Website Your license is activated! Your license is active. Your license is not active! activate add new on admin barPortfolio admin menuPortfolio here http://www.brainstormforce.com http://www.wpastra.com/pro/ new portfolio itemAdd New post type general namePortfolio post type singular namePortfolio taxonomy general nameCategories taxonomy general nameOther Categories taxonomy general nameTags taxonomy singular nameCategories taxonomy singular nameOther Categories taxonomy singular nameTags PO-Revision-Date: 2018-04-05 20:39:05+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/2.3.1
Language: fr
Project-Id-Version: WP Portfolio
 Valider la License Validez votre licence pour activer ! Ajouter Ajouter un nouveau Portfolio Ajouter une nouvelle Étiquette Ajouter un Portfolio Ajouter une image Tous Tous les Portfolios Tous les addons disponibles ont été installés ! Add-ons Disponibles Bas Brainstorm Force Fermer Collones Désactiver la License Debug Description. Affichage Affiche les boutons de responsive design. Affiche la boite de recherche. Affiche le portfolio de Astra Stater Sites & d'autres facilement sur votre site. Téléchargement Active le tri par catégories. Active le tri via les autres catégories. URL Extensions Add-ons Aide Image Importer Importer les Starter Sites Installer Installé Add-ons Installés License Licenses Chargement... Nouveau Portfolio Nouvelle Étiquette Aucun résultat trouvé. Nombre de portfolio par rangée. Supporte au maximum 4. Un moment... Ouvrir dans un nouvel onglet Page Builder Coller le shortcode suivant sur la page ou vous souhaitez afficher le portfolio. Image du Portfolio Barre de prévisualisation Vue Rapide Ajoutez rapidement un portfolio en remplissant les champs ci-dessous. Permaliens personnalisés Personnalisation des permaliens Rechercher Recherche... Type de Portfolio... Défini la position de la barre de prévisualisation Réglages Les réglages ont étés correctement enregistrés. Shortcode Page classique Informations systeme Étiquettes Titre Haut Type Mise à jour Valider l'achat Vidéo URL de la Vidéo WP Portfolio Site Web Votre licence est validée ! Votre licence est valide. Votre licence n'est pas validée ! activer Portfolio Portfolio ici https://www.brainstormforce.com/ https://www.wpastra.com/pro/ Ajouter Portfolio Portfolio Catégories Autres Catégories Étiquettes Catégories Autres Catégories Étiquettes 