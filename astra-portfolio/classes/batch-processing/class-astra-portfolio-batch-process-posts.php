<?php
/**
 * Batch Processing
 *
 * @package Astra Portfolio
 * @since 1.0.0
 */

if ( ! class_exists( 'Astra_Portfolio_Batch_Process_Posts' ) ) :

	/**
	 * Astra_Portfolio_Batch_Process_Posts
	 *
	 * @since 1.0.0
	 */
	class Astra_Portfolio_Batch_Process_Posts {

		/**
		 * Instance
		 *
		 * @since 1.0.0
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Exclude portfolios IDs.
		 *
		 * @since 1.0.5
		 * @access private
		 * @var array Exclude IDs.
		 */
		private static $exclude_ids;

		/**
		 * Initiator
		 *
		 * @since 1.0.0
		 * @return object initialized object of class.
		 */
		public static function get_instance() {

			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * @since 1.0.0
		 */
		public function __construct() {
		}

		/**
		 * Import
		 *
		 * @since 1.0.0
		 * @return void
		 */
		public function import() {

			// Exclude post.
			self::$exclude_ids = (array) get_option( 'astra_portfolio_batch_excluded_sites', array() );

			$args = array(
				'per_page' => '100',
				'page'     => '1',
			);

			$response = Astra_Portfolio_API::get_instance()->get_sites( $args );

			if ( $response['sites_count'] ) {
				foreach ( $response['sites'] as $key => $single_site ) {
					Astra_Portfolio_Batch_Process_Posts::get_instance()->import_single_post( $single_site );
				}
			}

		}

		/**
		 * Create post.
		 *
		 * @param  array $single_site Single site API response.
		 * @return void
		 */
		public function import_single_post( $single_site = array() ) {
			$title              = ( isset( $single_site['title'] ) ) ? $single_site['title'] : '';
			$content            = ( isset( $single_site['content'] ) ) ? $single_site['content'] : '';
			$remote_post_id     = ( isset( $single_site['id'] ) ) ? absint( $single_site['id'] ) : '';
			$site_url           = ( isset( $single_site['astra_demo_url'] ) ) ? $single_site['astra_demo_url'] : '';
			$featured_image_url = ( isset( $single_site['featured_image_url'] ) ) ? $single_site['featured_image_url'] : '';
			$required_plugins   = ( isset( $single_site['required_plugins'] ) ) ? stripslashes( $single_site['required_plugins'] ) : '';

			$api_terms_site_category     = ( isset( $single_site['astra-site-category'] ) ) ? $single_site['astra-site-category'] : array();
			$api_terms_site_page_builder = ( isset( $single_site['astra-site-page-builder'] ) ) ? $single_site['astra-site-page-builder'] : array();

			$site_category     = Astra_Portfolio_Batch_Process_Terms::get_instance()->get_new_taxonomy_ids( 'astra-portfolio-categories', 'astra-site-category', $api_terms_site_category );
			$site_page_builder = Astra_Portfolio_Batch_Process_Terms::get_instance()->get_new_taxonomy_ids( 'astra-portfolio-other-categories', 'astra-site-page-builder', $api_terms_site_page_builder );

			// New post argument array.
			$args = array(
				'post_type'    => 'astra-portfolio',
				'post_status'  => 'draft',
				'post_title'   => $title,
				'post_content' => $content,
				'meta_input'   => array(
					'astra-remote-post-id' => $remote_post_id,
					'astra-site-url'       => $site_url,
					'astra-portfolio-type' => 'iframe',
				),
				'tax_input'    => array(
					'astra-portfolio-categories'       => $site_category,
					'astra-portfolio-other-categories' => $site_page_builder,
				),
			);

			if ( ! in_array( $remote_post_id, self::$exclude_ids ) ) {

				// Add to excludes list.
				self::$exclude_ids[] = $remote_post_id;
				self::$exclude_ids   = array_unique( self::$exclude_ids ); // Remove duplicates.

				update_option( 'astra_portfolio_batch_excluded_sites', self::$exclude_ids );

				// Create new post and get new post ID.
				$post_id = wp_insert_post( $args );

				error_log( $title );

				// Add post terms.
				wp_set_post_terms( $post_id, $site_category, 'astra-portfolio-categories' );
				wp_set_post_terms( $post_id, $site_page_builder, 'astra-portfolio-other-categories' );

				if ( ! empty( $featured_image_url ) ) {
					Astra_Portfolio_Batch_Process_Posts::get_instance()->set_featured_image_by_url( $featured_image_url, $post_id );
				}
			}
		}

		/**
		 * Set featured image to post ID by image URL.
		 *
		 * @since 1.0.1     Storing featured image ID in portfolio image meta.
		 *
		 * @since 1.0.0
		 *
		 * @param string $image_url Image URL.
		 * @param string $post_id   Post ID.
		 */
		public static function set_featured_image_by_url( $image_url = '', $post_id = '' ) {

			// $filename should be the path to a file in the upload directory.
			$file = Astra_Portfolio_Batch_Process_Posts::get_instance()->download_file( $image_url );

			if ( false === $file['success'] ) {
				error_log( $image_url );
				return;
			}

			$file_abs_url = $file['data']['file'];
			$file_url     = $file['data']['file'];
			$file_type    = $file['data']['type'];

			// Get the path to the upload directory.
			$wp_upload_dir = wp_upload_dir();

			// Prepare an array of post data for the attachment.
			$attachment = array(
				'guid'           => $wp_upload_dir['url'] . '/' . basename( $file_abs_url ),
				'post_mime_type' => $file_type,
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $file_abs_url ) ),
				'post_content'   => '',
				'post_status'    => 'inherit',
			);

			// Insert the attachment.
			$attach_id = wp_insert_attachment( $attachment, $file_abs_url );

			// Include image.php.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );

			// Define attachment metadata.
			$attach_data = wp_generate_attachment_metadata( $attach_id, $file_abs_url );

			// Assign metadata to attachment.
			wp_update_attachment_metadata( $attach_id, $attach_data );

			// And finally assign featured image to post.
			set_post_thumbnail( $post_id, $attach_id );

			// Add portfolio image meta.
			update_post_meta( $post_id, 'astra-portfolio-image-id', $attach_id );
		}

		/**
		 * Download File Into Uploads Directory
		 *
		 * @since 1.0.0
		 *
		 * @param  string $file Download File URL.
		 * @return array        Downloaded file data.
		 */
		public static function download_file( $file = '' ) {

			// Gives us access to the download_url() and wp_handle_sideload() functions.
			require_once( ABSPATH . 'wp-admin/includes/file.php' );

			$timeout_seconds = 5;

			// Download file to temp dir.
			$temp_file = download_url( $file, $timeout_seconds );

			// WP Error.
			if ( is_wp_error( $temp_file ) ) {
				return array(
					'success' => false,
					'data'    => $temp_file->get_error_message(),
				);
			}

			// Array based on $_FILE as seen in PHP file uploads.
			$file_args = array(
				'name'     => basename( $file ),
				'tmp_name' => $temp_file,
				'error'    => 0,
				'size'     => filesize( $temp_file ),
			);

			$overrides = array(

				// Tells WordPress to not look for the POST form
				// fields that would normally be present as
				// we downloaded the file from a remote server, so there
				// will be no form fields
				// Default is true.
				'test_form'   => false,

				// Setting this to false lets WordPress allow empty files, not recommended.
				// Default is true.
				'test_size'   => true,

				// A properly uploaded file will pass this test. There should be no reason to override this one.
				'test_upload' => true,

			);

			// Move the temporary file into the uploads directory.
			$results = wp_handle_sideload( $file_args, $overrides );

			if ( isset( $results['error'] ) ) {
				return array(
					'success' => false,
					'data'    => $results,
				);
			}

			// Success.
			return array(
				'success' => true,
				'data'    => $results,
			);
		}
	}

	/**
	 * Kicking this off by calling 'get_instance()' method
	 */
	Astra_Portfolio_Batch_Process_Posts::get_instance();

endif;
