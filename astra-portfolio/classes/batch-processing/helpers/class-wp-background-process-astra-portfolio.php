<?php
/**
 * Image Background Process
 *
 * @package Astra Portfolio
 * @since 1.0.0
 */

if ( class_exists( 'WP_Background_Process' ) ) :

	/**
	 * Image Background Process
	 *
	 * @since 1.0.0
	 */
	class WP_Background_Process_Astra_Portfolio extends WP_Background_Process {

		/**
		 * Image Process
		 *
		 * @var string
		 */
		protected $action = 'import_website_demos';

		/**
		 * Task
		 *
		 * Override this method to perform any actions required on each
		 * queue item. Return the modified item for further processing
		 * in the next pass through. Or, return false to remove the
		 * item from the queue.
		 *
		 * @since 1.0.0
		 *
		 * @param object $process Queue item object.
		 * @return mixed
		 */
		protected function task( $process ) {

			if ( method_exists( $process, 'import' ) ) {
				$process->import();
			}

			return false;
		}

		/**
		 * Complete
		 *
		 * Override if applicable, but ensure that the below actions are
		 * performed, or, call parent::complete().
		 *
		 * @since 1.0.0
		 */
		protected function complete() {

			error_log( 'Imported all Astra sites.' );

			// Mapping.
			delete_option( 'astra-portfolio-batch-api-astra-portfolio-categories' );
			delete_option( 'astra-portfolio-batch-api-astra-portfolio-other-categories' );

			// Taxonomy magging option.
			delete_option( 'astra-portfolio-batch-astra-site-category' );
			delete_option( 'astra-portfolio-batch-astra-site-page-builder' );
			delete_option( 'astra-portfolio-batch-astra-portfolio-categories' );
			delete_option( 'astra-portfolio-batch-astra-portfolio-other-categories' );

			// Update the current status.
			update_option( 'astra-portfolio-batch-process', 'complete' );

			parent::complete();

		}

	}

endif;
