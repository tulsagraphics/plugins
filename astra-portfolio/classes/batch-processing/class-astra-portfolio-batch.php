<?php
/**
 * Batch Processing
 *
 * @package Astra Portfolio
 * @since 1.0.0
 */

if ( ! class_exists( 'Astra_Portfolio_Batch' ) ) :

	/**
	 * Astra_Portfolio_Batch
	 *
	 * @since 1.0.0
	 */
	class Astra_Portfolio_Batch {

		/**
		 * Instance
		 *
		 * @since 1.0.0
		 * @var object Class object.
		 * @access private
		 */
		private static $instance;

		/**
		 * Process All
		 *
		 * @since 1.0.0
		 * @var object Class object.
		 * @access public
		 */
		public static $process_all;

		/**
		 * Initiator
		 *
		 * @since 1.0.0
		 * @return object initialized object of class.
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * @since 1.0.0
		 */
		public function __construct() {

			// Core Helpers - Batch Processing.
			require_once ASTRA_PORTFOLIO_DIR . 'classes/batch-processing/helpers/class-wp-async-request.php';
			require_once ASTRA_PORTFOLIO_DIR . 'classes/batch-processing/helpers/class-wp-background-process.php';
			require_once ASTRA_PORTFOLIO_DIR . 'classes/batch-processing/helpers/class-wp-background-process-astra-portfolio.php';

			// Process.
			require_once ASTRA_PORTFOLIO_DIR . 'classes/batch-processing/class-astra-portfolio-batch-process-terms.php';
			require_once ASTRA_PORTFOLIO_DIR . 'classes/batch-processing/class-astra-portfolio-batch-process-posts.php';

			self::$process_all = new WP_Background_Process_Astra_Portfolio();
			add_action( 'admin_head', array( $this, 'start_process' ) );
		}

		/**
		 * Start Image Import
		 *
		 * @since 1.0.0
		 *
		 * @return void
		 */
		public function start_process() {
			$status = get_option( 'astra-portfolio-batch-process', false );

			// Force fetch all the Astra Started Sites once again.
			// By default WP Portfolio does not duplicate the sites when importing.
			// Adding query parameter `force-fetch-all-sites` to the URL will just force fetch the Astra Sites once again.
			if ( isset( $_GET['force-fetch-all-sites'] ) ) {
				delete_option( 'astra_portfolio_batch_excluded_sites' );
				delete_option( 'astra-portfolio-batch-process' );
			}

			if ( 'complete' === $status ) {
				Astra_Portfolio_Notices::add_notice(
					array(
						'id'               => 'astra-portfolio-batch-process-complete',
						'type'             => 'success',
						'dismissible'      => false,
						'dismissible-meta' => 'transient',
						'show_if'          => true,
						/* translators: %1$s portfolio page url. */
						'message'          => sprintf( __( 'Astra Starter Sites have been imported as portfolio items! Please <a href="%1$s">take a look</a> and publish the ones that you like.', 'astra-portfolio' ), esc_url( admin_url() . 'edit.php?post_type=astra-portfolio' ) ),
					)
				);

				delete_option( 'astra-portfolio-batch-process' );

			} else {

				if ( ! isset( $_GET['_nonce'] ) || empty( $_GET['_nonce'] ) ) {
					return;
				}

				if ( wp_verify_nonce( $_GET['_nonce'], 'astra-portfolio-batch-process' ) ) {

					Astra_Portfolio_Notices::add_notice(
						array(
							'id'               => 'astra-portfolio-batch-process-start',
							'type'             => 'info',
							'dismissible'      => false,
							'dismissible-meta' => 'transient',
							'dismissible-time' => WEEK_IN_SECONDS,
							'show_if'          => true,
							'message'          => __( '<strong>Import Started!</strong><br/><br/>All Astra Starter Sites will be imported as Portfolio items in a while. It might take a few minutes for the import process to complete, but you may resume your work as this happens in the background.<br/><br/>We will display another notice as soon as all Astra Starter Sites are imported as portfolio items.', 'astra-portfolio' ),
						)
					);

					update_option( 'astra-portfolio-batch-process', 'in-process' );

					self::$process_all->push_to_queue( Astra_Portfolio_Batch_Process_Terms::get_instance() );
					self::$process_all->push_to_queue( Astra_Portfolio_Batch_Process_Posts::get_instance() );

					// Dispatch Queue.
					self::$process_all->save()->dispatch();
				}
			}
		}
	}

	/**
	 * Kicking this off by calling 'get_instance()' method
	 */
	Astra_Portfolio_Batch::get_instance();

endif;
