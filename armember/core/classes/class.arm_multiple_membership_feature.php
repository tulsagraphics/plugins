<?php
if (!class_exists('ARM_multiple_membership_feature')) {

    class ARM_multiple_membership_feature {

        var $isMultipleMembershipFeature;

        function __construct() {
           
            $is_multiple_membership_feature = get_option('arm_is_multiple_membership_feature');
            $this->isMultipleMembershipFeature = ($is_multiple_membership_feature == '1') ? true : false;
            add_action('arm_add_new_custom_add_on', array(&$this, 'arm_add_multiple_membership_addon'));
            add_action('arm_deactivate_feature_settings', array(&$this, 'arm_multiple_membership_update_feature_settings'), 10, 1);
           
         
        }
        
       
        function arm_multiple_membership_update_feature_settings($posted_data)
        {
            global $wp, $wpdb, $wp_rewrite, $ARMember;
            $features_options = $posted_data['arm_features_options'];
            $arm_features_status = (!empty($posted_data['arm_features_status'])) ? $posted_data['arm_features_status'] : 0;
            if ($features_options == 'arm_is_multiple_membership_feature') {
                
                $args = array(
                    'meta_query' => array(
                        array(
                            'key' => 'arm_user_plan_ids',
                            'value' => '',
                            'compare' => '!='
                        ),
                    )
                );

                $amTotalUsers = get_users($args);
                $morethanoneplan = 0;
                if (!empty($amTotalUsers)) {
                    foreach ($amTotalUsers as $usr) {
                        $user_id = $usr->ID;
                        $arm_user_plan = get_user_meta($user_id,'arm_user_plan_ids', true);
                        if(!empty($arm_user_plan) && is_array($arm_user_plan)){
                            $count = 0;
                            foreach($arm_user_plan as $plan_id)
                            {
                                if(!empty($plan_id)){
                                    $count++;
                                }
                                if($count > 1){
                                    $morethanoneplan = 1;
                                    break;
                                }
                            }
                        }
                    }
                }
              
                if($morethanoneplan == 1){
                    $response = array('type' => 'wocommerce_error', 'msg' => __("One or more users have multiple membership, so addon can't be deactivated.", 'ARMember'));
                            echo json_encode($response);
                            die();
                }
            }
        }
        
        function arm_add_multiple_membership_addon()
        {
            global $arm_members_activity, $armemberplugin;
            $multiple_membership_feature = get_option('arm_is_multiple_membership_feature');
            $hostname = $_SERVER["SERVER_NAME"];
            $setact = 0;
            $setact = $arm_members_activity->$armemberplugin();
            $featureActiveIcon = MEMBERSHIP_IMAGES_URL . '/feature_active_icon.png';
                ?>    <div class="arm_feature_list multiple_membership_enable <?php echo ($multiple_membership_feature == 1) ? 'active':'';?>">
                    <div class="arm_feature_active_icon"><img src="<?php echo $featureActiveIcon;?>"  alt="active icon" width="100%"/></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php _e('Multiple Membership/Plans','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php _e("Allow members to subscribe multiple plans simultaneously.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
						<div class="arm_feature_button_activate_wrapper <?php echo ($multiple_membership_feature == 1) ? 'hidden_section':'';?>">
							<a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="multiple_membership"><?php _e('Activate','ARMember'); ?></a>
							<a href="javascript:void(0)" class="arm_feature_configure_btn"><?php _e('Configure','ARMember'); ?></a>
                            <img src="<?php echo MEMBERSHIP_IMAGES_URL.'/arm_loader.gif' ?>" class="arm_addon_loader_img" width="24" height="24" />
						</div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($multiple_membership_feature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="multiple_membership"><?php _e('Activate','ARMember'); ?></a>
               
                            <img src="<?php echo MEMBERSHIP_IMAGES_URL.'/arm_loader.gif' ?>" class="arm_addon_loader_img" width="24" height="24" />
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($multiple_membership_feature == 1) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch" data-feature_val="0" data-feature="multiple_membership"><?php _e('Deactivate','ARMember'); ?></a>
                    
                            <img src="<?php echo MEMBERSHIP_IMAGES_URL.'/arm_loader.gif' ?>" class="arm_addon_loader_img" width="24" height="24" />
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/buddypress-support/"><?php _e('More Info', 'ARMember'); ?></a>
                </div><?php
        }
        
    }

}
global $is_multiple_membership_feature;
$is_multiple_membership_feature = new ARM_multiple_membership_feature();

