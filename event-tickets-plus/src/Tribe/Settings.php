<?php

class Tribe__Tickets_Plus__Settings {

	public function __construct() {
		add_filter( 'tribe_tickets_settings_tab_fields', array( $this, 'add_settings' ) );
	}

	/**
	 * Append global Event Tickets Plus settings section to tickets settings tab
	 *
	 * @param array $settings_fields
	 *
	 * @since 4.7.4
	 *
	 * @return array
	 */
	public function add_settings( array $settings_fields ) {
		$extra_settings = $this->additional_settings();
		return Tribe__Main::array_insert_before_key( 'tribe-form-content-end', $settings_fields, $extra_settings );
	}

	/**
	 * Return the actual settings
	 *
	 * @since 4.7.4
	 *
	 * @return array
	 */
	protected function additional_settings() {

		return array(
			'tickets-enable-qr-codes-heading' => array(
				'type' => 'html',
				'html' => '<h3>' . esc_html__( 'QR Codes', 'event-tickets-plus' ) . '</h3>',
			),
			'tickets-enable-qr-codes' => array(
				'type'            => 'checkbox_bool',
				'label'           => esc_html__( 'Enable QR codes for tickets?', 'event-tickets-plus' ),
				'default'         => true,
				'validation_type' => 'boolean',
			),
		);
	}
}