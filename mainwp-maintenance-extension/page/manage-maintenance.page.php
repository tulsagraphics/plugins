<?php

class Manage_Maintenance
{
	private static $instance = null;
	private $schedules = array();
	private $options = array();
	private $errors = array();

	static function get_instance() {

		if ( null == Manage_Maintenance::$instance ) {
			Manage_Maintenance::$instance = new Manage_Maintenance();
		}
		return Manage_Maintenance::$instance;
	}

		//Constructor
	function __construct() {

			 $this->schedules = array(
				 'daily'   => __( 'Day',   'mainwp-maintenance-extension' ),
			   'weekly'  => __( 'Week',  'mainwp-maintenance-extension' ),
			   'monthly' => __( 'Month', 'mainwp-maintenance-extension' ),
			   'yearly'  => __( 'Year',  'mainwp-maintenance-extension' )
			 );

			 $this->options = array(
			 	 'revisions'    => __( 'Delete all post revisions except for the last', 'mainwp-maintenance-extension' ),
				 'autodraft'    => __( 'Delete all auto draft posts',                   'mainwp-maintenance-extension' ),
				 'trashpost'    => __( 'Delete trash posts',                            'mainwp-maintenance-extension' ),
				 'spam'         => __( 'Delete spam comments',                          'mainwp-maintenance-extension' ),
				 'pending'      => __( 'Delete pending comments',                       'mainwp-maintenance-extension' ),
				 'trashcomment' => __( 'Delete trash comments',                         'mainwp-maintenance-extension' ),
				 'tags'         => __( 'Delete tags with 0 posts associated',           'mainwp-maintenance-extension' ),
				 'categories'   => __( 'Delete categories with 0 posts associated',     'mainwp-maintenance-extension' ),
				 'optimize'     => __( 'Optimize database tables',                      'mainwp-maintenance-extension' )
			 );
	}

	function gen_maintenance_task( $task = null ) {
		$task_options = array();
		$revisions = 5;
		if ( null != $task ) {
			$task_options = explode( ',', $task->options );
				$revisions = $task->revisions;
		}

		?>
		<div class="postbox" style="width: calc( 100% - 280px ); float: left;">
			<h2 class="hndle ui-sortable-handle"><span><i class="fa fa-cog"></i> <?php _e( 'Maintenance options', 'mainwp-maintenance-extension' ); ?></span></h2>
				<div class="mainwp-postbox-actions-top" id="maintenance-run-now-notice-box"><?php echo __( 'Attention! Before running a maintenance process, it is highly recommended to make a database backup.', 'mainwp-maintenance-extension' );?></div>
				<div class="inside">
					<ul class="maintenance_checkboxes">
					<?php foreach ( $this->options as $key => $option ) :
						if ( null == $task || in_array( $key, $task_options ) ) {
							$att_checked = ' checked="checked" ';
						} else {
							$att_checked = '';
						}
						?>
	          <li>
	          	<input type="checkbox" class="mainwp-checkbox2" name="maintenance_options[]" value="<?php echo $key; ?>" <?php echo $att_checked;?> id="mainte_option_<?php echo $key; ?>">
	            <label class="mainwp-label2" for="mainte_option_<?php echo $key; ?>"><?php echo $option; ?></label>
	            <?php
				if ( 'revisions' == $key ) { ?>
	               <input type="number" min="0" max="300" name="maintenance_options_revisions_count" id="maintenance_options_revisions_count" value="<?php echo $revisions; ?>">
				<?php } ?>
	          </li>
		     <?php endforeach; ?>
	       </ul>
      </div>
    </div>
    <?php
	}

	function gen_schedule_options( $task = null ) {
		$perform_number = array( 1,2,3,4,5,10,15 );
		$title = ! empty( $task ) ? $task->title : '';
		
		$recurringSchedule = $recurringDate = $recurringMonth = $recurringDay = $recurringTime = '';    
		$send_on_style = $send_on_day_of_week_style = $send_on_day_of_mon_style = $send_on_month_style = $monthly_style = 'style="display:none"';
		
		$day_of_week = array(
                    1 => __( 'Monday' ),
                    2 => __( 'Tuesday' ),			
                    3 => __( 'Wednesday' ),			
                    4 => __( 'Thursday' ),
                    5 => __( 'Friday' ),
                    6 => __( 'Saturday' ),
                    7 => __( 'Sunday' ),
                );
		
		if ( ! empty( $task ) ) {
			$title = $task->title;			
            $recurringSchedule = $task->schedule;
            $recurringDay = $task->recurring_day;			
			$recurringTime = $task->recurring_hour; 
			$task_perform = $task->perform; 
			
			
			
            if ( $task_perform == 1 && ($recurringSchedule == 'weekly' || $recurringSchedule == 'monthly' || $recurringSchedule == 'yearly') ) {
                $send_on_style = '';     
                if ($recurringSchedule == 'weekly') {
                    $send_on_day_of_week_style = '';
                } else if ($recurringSchedule == 'monthly') {
                    $send_on_day_of_mon_style = $monthly_style = '';
                    $recurringDate = $recurringDay;
                } else if ($recurringSchedule == 'yearly') {
                    list($recurringMonth, $recurringDate) = explode( '-', $recurringDay);
                    $send_on_day_of_mon_style = $send_on_month_style = '';
                }
            }
                        
		}
		
		?>
		<div class="postbox maint_custom_tbl" style="width: calc( 100% - 280px ); float: left;">
			<h2 class="hndle ui-sortable-handle"><span><i class="fa fa-cog"></i> <?php _e( 'Schedule options','mainwp-maintenance-extension' ); ?></span></h2>
			<div class="inside">
				<table class="form-table">
					<tbody>
						<tr>
							<th><?php _e( 'Schedule title:','mainwp-maintenance-extension' );?></th>
							<td>								
								<input type="text" size="50" name="managemaintenance_title"  value="<?php echo $title;?>" id="managemaintenance_title">
							</td>
						</tr>
						<tr>
							<th><?php _e( 'Perform maintenance:','mainwp-maintenance-extension' );?></th>
							<td>
								<select id="maintenance_perform_number">
								<?php
								foreach ( $perform_number as $val ) {
								?>
									<option value="<?php echo $val;?>" <?php echo ((null != $task && $val == $task->perform) ? 'selected' : '');?>><?php echo $val; ?></option>
				            <?php } ?>
								</select>
										<label><?php _e( ' time(s) per ', 'mainwp-maintenance-extension' );?></label>
								<select id="maintenance_perform_schedule">
								<?php
											foreach ( $this->schedules as $key => $val ) { ?>
									<option value="<?php echo $key;?>" <?php echo ((null != $task && $key == $task->schedule) ? 'selected' : '');?>><?php echo $val; ?></option>
								<?php } ?>
								</select>
										
								<span  id="mainwp_maint_send_on_wrap" <?php echo $send_on_style; ?>>
									<?php _e('Run on', 'mainwp-maintenance-extension'); ?>&nbsp;
										<span id="scheduled_send_on_day_of_week_wrap" <?php echo $send_on_day_of_week_style; ?>>
											<select name='mainwp_maint_schedule_day' id="mainwp_maint_schedule_day">                                                                                               
													<?php
													foreach ( $day_of_week as $value => $title ) {
															$_select = '';
															if ( $recurringDay == $value ) {
																	$_select = 'selected';                                             
															}
															echo '<option value="' . $value . '" ' . $_select . '>' . $title . '</option>';
													}
													?>
											</select> 
										</span> 
										<span id="scheduled_send_on_month_wrap" <?php echo $send_on_month_style; ?>>                                                 
											  <select name='mainwp_maint_schedule_month' id="mainwp_maint_schedule_month">                                      
												   <?php
												   $months_name = array(
													   1 => __('January'),
													   2 => __('February'),
													   3 => __('March'),
													   4 => __('April'),
													   5 => __('May'),
													   6 => __('June'),
													   7 => __('July'),
													   8 => __('August'),
													   9 => __('September'),
													   10 => __('October'),
													   11 => __('November'),
													   12 => __('December'),
												   );
												   for ( $x = 1; $x <= 12; $x++ ) {
														   $_select = '';
														   if ( $recurringMonth == $x ) {
																   $_select = 'selected';                                             
														   }
														   echo '<option value="' . $x . '" ' . $_select . '>' . $months_name[$x] . '</option>';
												   }
												   ?>
										   </select> 
									   </span>                                                
									   <span id="scheduled_send_on_day_of_month_wrap" <?php echo $send_on_day_of_mon_style; ?>>                                                               
											   <select name='mainwp_maint_schedule_day_of_month' id="mainwp_maint_schedule_day_of_month">                                       
													<?php
													$day_suffix = array(
														1 => 'st',
														2 => 'nd',
														3 => 'rd'
													);
													for ( $x = 1; $x <= 31; $x++ ) {
															$_select = '';
															if ( $recurringDate == $x ) {
																	$_select = 'selected';                                             
															}
															$remain = $x % 10;                                                
															$day_sf = isset($day_suffix[$remain]) ? $day_suffix[$remain] : 'th';
															echo '<option value="' . $x . '" ' . $_select . '>' . $x . $day_sf . '</option>';
													}
													?>
											</select>&nbsp; 
											<span class="show_if_monthly" <?php echo $monthly_style; ?>><?php _e('of the Month', 'mainwp-maintenance-extension' ); ?></span>
										</span> 
										<span id="scheduled_send_at_time">                                                               
												<span><?php _e('at hour', 'mainwp'); ?></span>
											   <select name='mainwp_maint_schedule_at_time' id="mainwp_maint_schedule_at_time">                                       
													<?php													
													for ( $x = 1; $x <= 24; $x++ ) {
															$_select = '';
															if ( $recurringTime == $x ) {
																	$_select = 'selected';                                             
															}														
															echo '<option value="' . $x . '" ' . $_select . '>' . $x . '</option>';
													}
													?>
											</select>										
										</span> 
									</span>
							</td>
						</tr>
					</tbody>
				</table>
      </div>
    </div>
  <?php
	}

	function gen_404_alerts_options() {
		$settings = get_option( 'mainwp_maintenance_settings' );
		$email = '';
		$enable_alert = 0;
		$sites = $groups = array();

		if ( is_array( $settings ) ) {
			$email = isset( $settings['email'] ) ? $settings['email'] : '';
			$enable_alert = isset( $settings['enable_alert'] ) ? intval( $settings['enable_alert'] ) : 0;
		}

		?>
		<div class="postbox" style="width: calc( 100% - 280px ); float: left; min-height: 337px;">
			<h2 class="hndle ui-sortable-handle"><span><i class="fa fa-cog"></i> <?php _e( '404 email alerts', 'mainwp-maintenance-extension' ); ?></span></h2>
			<div class="inside">
				<table class="form-table">
					<tbody>
						<tr>
							<th scope="row"><?php _e( 'Enable 404 email notifications', 'mainwp-maintenance-extension' );?></th>
							<td>
								<table>
									<tr>
										<td valign="top" style="padding-left: 0; padding-right: 5px; padding-top: 0px; padding-bottom: 0px; vertical-align: top;">
											<div class="mainwp-checkbox">
												<input type="checkbox" id="managemaintenance_enable_404_alert" name="managemaintenance_enable_404_alert" <?php echo (0 == $enable_alert ? '' : 'checked="checked"'); ?> value="1"/>
												<label for="managemaintenance_enable_404_alert"></label>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<th scope="row">
								<?php _e( 'Notification email address','mainwp-maintenance-extension' );?>
							</th>
							<td>
								<table>
									<tr>
										<td valign="top" style="padding-left: 0; padding-right: 5px; padding-top: 0px; padding-bottom: 0px; vertical-align: top;">
											<input type="text" size="50" name="managemaintenance_404_alert_email"  value="<?php echo $email;?>" id="managemaintenance_404_alert_email">
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
      </div>
    </div>
	<?php
	}

	function render_manage() {
		
		$maintenanceTask = null;
		if ( isset( $_GET['id'] ) ) {
			if ( ! empty( $_GET['id'] ) ) {
				$maintenanceTaskId = $_GET['id'];
				$maintenanceTask = Maintenance_Extension_DB::get_instance()->get_maintenance_task_by_id( $maintenanceTaskId );				
			} else {
					$this->errors[] = __( 'Incorrect maintenance task ID.', 'mainwp-maintenance-extension' );
			}
		}

		if ( null == $maintenanceTask ) {
			$this->render_tabs();
		} else {
            $this->render_tabs( $maintenanceTask );
		}
	}

	function render_tabs( $task = null ) {        
        
		$current_tab = '';
		
		if (isset($_GET['tab'])) {
			if ( $_GET['tab'] == 'new' ) {						
				$current_tab = 'new';
			} else if ( $_GET['tab'] == 'runnow' ) {						
				$current_tab = 'runnow';
			}
		}
        
        $notice_number = get_option('_mainwp_maintenance_notice_number', false);  
        
        $current_tab = (!empty( $notice_number ) && 4 != $notice_number && 6 != $notice_number && 7 != $notice_number || ! empty( $_GET['orderby'] )) ? 'schedule' : $current_tab; // visible tab schedules if added new maintenance task | update a maintenance task | or trigger run now | or sort orderby
        
        $selected_websites = $selected_groups = array();
		
		if ( null != $task ) {
			if ( $task->sites != '' ) {
					$selected_websites = explode( ',', $task->sites ); }
			if ( $task->groups != '' ) {
					$selected_groups = explode( ',', $task->groups ); }
			$current_tab = 'edit';
		}

		$selected_websites = is_array( $selected_websites ) ? $selected_websites : array();
		$selected_groups = is_array( $selected_groups ) ? $selected_groups : array();
		
		$style_select_sites = '';		
		if ( $current_tab == '') {
			$current_tab = 'runnow'; // hide tab run now
		}
		
		if ( $current_tab == 'schedule' ) { // display schedules tab
			$style_select_sites = ' style="display: none" '; 			
		}

        $str_info = '';
        
        if (!empty($notice_number)) {            
            delete_option('_mainwp_maintenance_notice_number');           
            if ( 1 == $notice_number ) {
                $str_info = __( 'Maintenance task has been added successfully!',   'mainwp-maintenance-extension' ); } else if ( 2 == $notice_number ) {
                $str_info = __( 'Maintenance task has been updated successfully!', 'mainwp-maintenance-extension' ); } else if ( 3 == $notice_number ) {
                $str_info = __( 'Site has been cleaned successfully!',             'mainwp-maintenance-extension' ); } else if ( 4 == $notice_number ) {
                $str_info = __( 'Site has been cleaned successfully!',             'mainwp-maintenance-extension' ); } else if ( 6 == $notice_number ) {
                $str_info = __( 'Sites have been cleaned successfully!',           'mainwp-maintenance-extension' ); } else if ( 5 == $notice_number ) {
                $str_info = '';                     
            } else if ( 7 == $notice_number ) {
                $str_info = __( '404 Email Alerts have been updated successfully!', 'mainwp-maintenance-extension' );                     
            }                
        }
	
        $error_str = $error_style = '';
        $error_style = 'style="display: none"';
		if ( count( $this->errors ) > 0 ) {
			$error_str = implode( '<br />', $this->errors );
			$error_str = rtrim( $error_str, '<br />' );
			$error_style = '';
		}
	?>
	<div class="mainwp-subnav-tabs">
		<a id="maintenance_run_now_lnk" href="admin.php?page=Extensions-Mainwp-Maintenance-Extension&tab=runnow" class="mainwp_action left <?php  echo ( $current_tab == 'runnow' ? 'mainwp_action_down' : ''); ?>"><?php _e( 'Run Now','mainwp-maintenance-extension' ); ?></a>
		<a id="maintenance_new_schedule_lnk" href="admin.php?page=Extensions-Mainwp-Maintenance-Extension&tab=new" class="mainwp_action mid <?php  echo ( $current_tab == 'new' ? 'mainwp_action_down' : ''); ?>"><?php _e( 'New Schedule','mainwp-maintenance-extension' ); ?></a>
		<a id="maintenance_schedules_lnk" href="#" class="mainwp_action mid <?php  echo ( ($current_tab == 'schedule') ? 'mainwp_action_down' : ''); ?> <?php echo 'mid';?>" ><?php _e( 'Schedules','mainwp-maintenance-extension' ); ?></a>
		<?php echo ($current_tab == 'edit' ? '<a id="maintenance_edit_daily_lnk" href="#" class="mainwp_action mid mainwp_action_down">' . __( 'Edit','mainwp-maintenance-extension' ) . ' ' . $task->title . '</a>' : ''); ?>
		<a id="maintenance_404_alerts_lnk" href="#" class="mainwp_action right"><?php _e( '404 Email Alerts','mainwp-maintenance-extension' ); ?></a>
		<div style="clear: both;"></div>
	</div>

	<div class="mainwp-notice mainwp-notice-green" id="maintenance-site-info-box"  <?php echo (empty( $str_info ) ? ' style="display: none" ' : ''); ?>><?php echo $str_info?></div>
	<div class="mainwp-notice mainwp-notice-red" id="maintenance-site-error-box" <?php echo $error_style;?>><?php echo $error_str;?></div>

	<div class="metabox-holder columns-1" id="mainwp-ap-option">
		<div class="meta-box-sortables ui-sortable" id="normal-sortables">

			<div id="maintenance_select_sites_box" class="mainwp_config_box_right" <?php echo $style_select_sites; ?>>
				<?php do_action( 'mainwp_select_sites_box', __( 'Select Sites', 'mainwp-maintenance-extension' ), 'checkbox', true, true, 'mainwp_select_sites_box_right', '', $selected_websites, $selected_groups ); ?>
			</div>

			<div id="maintenance-site-progress-box" title="<?php _e( 'Cleaning sites' ); ?>" style="display: none; text-align: center;">
				<div id="maintenance-site-progress"></div>
				<span id="maintenance-site-progress-current"></span>
				<div id="maintenance-site-progress-results" style="height: 200px; overflow: auto; margin-top: 20px; margin-bottom: 10px; text-align: left; padding: 10px;"></div>
				<input id="maintenance-site-progress-close" type="button" name="Close" value="Close" class="button" />
			</div>

			<div id="maintenance_option">

				<div id="maintenance_run_now_box">
					<?php if ($current_tab == 'runnow') { ?>
					<?php $this->gen_maintenance_task(); ?>
					<div style="clear:both"></div>
					<div id="maintenance_run_loading" class="mainwp-notice mainwp-notice-green"><i class="fa fa-spinner fa-pulse"></i> <em><?php _e( 'Getting selected sites...','mainwp-maintenance-extension' ) ?></em></div>
					<input id="maintenance_run_btn" type="button" value="<?php _e( 'Perform Maintenance','mainwp-maintenance-extension' );?>" class="button-primary button button-hero">
					<?php } ?>
				</div>				
				<div id="maintenance_new_schedule_box">
					<?php if ($current_tab == 'new') { ?>
					<?php $this->gen_schedule_options(); ?>
					<?php $this->gen_maintenance_task(); ?>
					<div style="clear:both"></div>
					<div id="maintenance_new_loading" class="mainwp-notice mainwp-notice-green"><i  class="fa fa-spinner fa-pulse"></i> <em><?php _e( 'Saving schedule...','mainwp-maintenance-extension' ) ?></em></div>
					<input type="button" value="<?php _e( 'Schedule Maintenance','mainwp-maintenance-extension' ); ?>" class="button-primary button button-hero" id="managemaintenance_add">
					<?php } ?>
				</div>								
				<div id="maintenance_schedules_box"  <?php echo $current_tab == 'schedule' ? '' : 'style="display: none"'; ?>>
					<?php echo $this->gen_schedules_table();?>
				</div>

				<?php if ( $current_tab == 'edit' ) : ?>
					<div id="maintenance_edit_daily_box">
					  <?php $this->gen_schedule_options( $task ); ?>
					  <?php $this->gen_maintenance_task( $task ); ?>
						<div style="clear:both"></div>
						<div id="maintenance_edit_loading" class="mainwp-notice mainwp-notice-green"><i  class="fa fa-spinner fa-pulse"></i> <em><?php _e( 'Updating schedule...','mainwp-maintenance-extension' ) ?></em></div>
						<input type="submit" value="<?php _e( 'Update Schedule','mainwp-maintenance-extension' ); ?>" class="button-primary button button-hero" id="managemaintenance_update" name="submit">
						<input type="hidden" name="edit_managemaintenance_id" id="edit_managemaintenance_id" value="<?php echo $task->id;?>">
					</div>
        <?php endif; ?>

				<div id="maintenance_404_alerts_box" style="display: none">
					<?php $this->gen_404_alerts_options(); ?>
					<div style="clear:both"></div>
					<div id="maintenance_save_loading" class="mainwp-notice mainwp-notice-green"><i  class="fa fa-spinner fa-pulse"></i> <em><?php _e( 'Saving settings to child sites...','mainwp-maintenance-extension' ) ?></em></div>
					<input id="maintenance_save_settings" type="button" value="<?php _e( 'Save Settings','mainwp-maintenance-extension' );?>" class="button-primary button button-hero">
				</div>

			</div>
		</div>
	</div>
  <?php
  
    if (!empty($str_info)) {
        ?>
        <script type="text/javascript">
            jQuery( document ).ready(function ($) {
                setTimeout(function(){ jQuery('#maintenance-site-info-box').fadeOut(1000); }, 5000);
            })
        </script>
        <?php
    }
}

	function gen_schedules_table() {

			$titleorder = '';
			$orderby = 'title';
		if ( isset( $_GET['orderby'] ) ) {
			if ( ('title' == $_GET['orderby']) ) {
				$orderby = 'title ' . ('asc' == $_GET['order'] ? 'asc' : 'desc');
				$titleorder = ( 'asc' == $_GET['order'] ) ? 'desc' : 'asc';
			}
		} else {
				$titleorder = 'desc';
		}
		?>
	  <table id="mainwp-table" class="wp-list-table widefat" cellspacing="0">
			<thead>
			  <tr>
				  <th scope="col" id="role" class="manage-column column-posts" style=""><a href="?page=Extensions-Mainwp-Maintenance-Extension&orderby=title&order=<?php echo ('' == $titleorder ? 'asc' : $titleorder); ?>"><?php _e( 'Title','mainwp-maintenance-extension' ); ?></a></th>
				  <th scope="col" id="role" class="manage-column column-posts" style=""><?php _e( 'Options','mainwp-maintenance-extension' ); ?></th>
				  <th scope="col" id="role" class="manage-column column-posts" style=""><?php _e( 'Schedule','mainwp-maintenance-extension' ); ?></th>
				  <th scope="col" id="role" class="manage-column column-posts" style=""><?php _e( 'Websites','mainwp-maintenance-extension' ); ?></th>
				  <th scope="col" id="role" class="manage-column column-posts" style=""><?php _e( 'Details','mainwp-maintenance-extension' ); ?></th>
				  <th scope="col" id="role" class="manage-column column-posts" style=""><?php _e( 'Trigger','mainwp-maintenance-extension' ); ?></th>
			  </tr>
			</thead>

			<tfoot>
			  <tr>
				  <th scope="col" id="role" class="manage-column column-posts" style=""><a href="?page=Extensions-Mainwp-Maintenance-Extension&orderby=title&order=<?php echo ('' == $titleorder ? 'asc' : $titleorder); ?>"><?php _e( 'Title','mainwp-maintenance-extension' ); ?></a></th>
				  <th scope="col" id="role" class="manage-column column-posts" style=""><?php _e( 'Options','mainwp-maintenance-extension' ); ?></th>
				  <th scope="col" id="role" class="manage-column column-posts" style=""><?php _e( 'Schedule','mainwp-maintenance-extension' ); ?></th>
				  <th scope="col" id="role" class="manage-column column-posts" style=""><?php _e( 'Websites','mainwp-maintenance-extension' ); ?></th>
				  <th scope="col" id="role" class="manage-column column-posts" style=""><?php _e( 'Details','mainwp-maintenance-extension' ); ?></th>
				  <th scope="col" id="role" class="manage-column column-posts" style=""><?php _e( 'Trigger','mainwp-maintenance-extension' ); ?></th>
			  </tr>
			</tfoot>

			<tbody id="the-sites-list" class="list:sites">
				<?php $this->get_table_content( $orderby ); ?>
			</tbody>
		</table>
	<?php
	}

	public function get_table_content( $orderby ) {
		$tasks = Maintenance_Extension_DB::get_instance()->get_maintenance_tasks( $orderby );
		if ( ! self::validate_maintenance_tasks( $tasks ) ) {
			$tasks = Maintenance_Extension_DB::get_instance()->get_maintenance_tasks( $orderby );
		}
		foreach ( $tasks as $task ) {
			 $sites = ($task->sites == '' ? array() : explode( ',', $task->sites ));
			 $groups = ($task->groups == '' ? array() : explode( ',', $task->groups ));

			 $websites = apply_filters( 'mainwp_getwebsitesbygroupids', $groups );
			if ( is_array( $websites ) ) {
				foreach ( $websites as  $website ) {
					if ( ! in_array( $website->id, $sites ) ) {
						$sites[] = $website->id;
					}
				}
			}

			$options = ($task->options == '' ? array() : explode( ',', $task->options ));
			$task_options = '';
			if ( count( $options ) == count( $this->options ) ) {
					$task_options = __( 'Full maintenance service','mainwp-maintenance-extension' ) . ',<br />';
				if ( $task->revisions != 0 ) {
					$task_options .= __( 'Delete all post revisions except for the last','mainwp-maintenance-extension' ) . ' ' . $task->revisions; } else {
					$task_options .= __( 'Delete all post revisions','mainwp-maintenance-extension' ); }
			} else {
				foreach ( $options as $option ) {
					if ( ! empty( $this->options[ $option ] ) ) {
						if ( 'revisions' == $option ) {
							if ( $task->revisions != 0 ) {
								$task_options .= __( 'Delete all post revisions except for the last','mainwp-maintenance-extension' ) . ' ' . $task->revisions . ',<br />'; } else {
								$task_options .= __( 'Delete all post revisions','mainwp-maintenance-extension' ) . ',<br />'; }
						} else {
							$task_options .= $this->options[ $option ] . ',<br />'; }
					}
				}
			}
			 $task_options = rtrim( $task_options, ',<br />' );
			 $do_optimize_db = (is_array( $options ) && in_array( 'optimize', $options )) ? 1 : 0;

				?>
			 <tr id="task-<?php echo $task->id; ?>" taskid="<?php echo $task->id; ?>">
				 <td scope="row" class="">
					 <strong><a href="admin.php?page=Extensions-Mainwp-Maintenance-Extension&id=<?php echo $task->id; ?>"><?php echo $task->title; ?></a></strong>
					 <br />
					 <div id="task-status-<?php echo $task->id; ?>" style="float: left; padding-right: 20px"></div>
					 <div class="row-actions">
						 <span class="edit"><a href="admin.php?page=Extensions-Mainwp-Maintenance-Extension&id=<?php echo $task->id; ?>"><i class="fa fa-pencil-square-o"></i> <?php _e( 'Edit','mainwp-maintenance-extension' ); ?></a> | </span>
						 <span class="delete"><a class="submitdelete" href="#" onClick="return managemaintenance_remove(<?php echo $task->id; ?>)"><i class="fa fa-trash-o"></i> <?php _e( 'Delete','mainwp-maintenance-extension' ); ?></a></span>
					 </div>
				 </td>
				 <td class=""><?php echo $task_options; ?></td>
				 <td class=""><?php echo $task->perform .' / ' . $this->schedules[ $task->schedule ]; ?></td>
				 <td class="" align="center">
						<?php if ( count( $sites ) == 0 ) {
									echo ('<span style="color: red; font-weight: bold; ">' . count( $sites ) . '</span>') ;
								} else {
									echo count( $sites );
								}
							?>
                            </td>                            
                            <td class="">
							<strong><?php _e( 'LAST RUN MANUALLY: ','mainwp-maintenance-extension' ); ?></strong> <?php echo ($task->last_run_manually == 0 ? '-' : self::format_timestamp( self::get_timestamp( $task->last_run_manually ) )); ?><br />
							<?php
							
							if ($task->perform == 1 && ( $task->schedule == 'weekly' || $task->schedule == 'monthly' || $task->schedule == 'yearly' )) {
								$next_run_time = self::format_timestamp( $task->schedule_nextsend ); 
								?>
								<strong><?php _e( 'LAST RUN: ','mainwp-maintenance-extension' ); ?></strong> <?php echo ($task->schedule_lastsend == 0 ? '-' : self::format_timestamp( self::get_timestamp( $task->schedule_lastsend ) )); ?><br />
								<strong><?php _e( 'NEXT RUN: ','mainwp-maintenance-extension' ); ?></strong> <?php echo ($task->schedule_nextsend == 0 ? __( 'Any minute','mainwp-maintenance-extension' ) : $next_run_time); ?>
							<?php } else { 								
								$next_run_time = self::format_timestamp( ($task->schedule == 'daily' ? (60 * 60 * 24) : ($task->schedule == 'weekly' ? (60 * 60 * 24 * 7) : ( $task->schedule == 'monthly' ?  (60 * 60 * 24 * 30) :  (60 * 60 * 24 * 365)))) / $task->perform + self::get_timestamp( $task->last_run ) );
								?>
								<strong><?php _e( 'LAST RUN: ','mainwp-maintenance-extension' ); ?></strong> <?php echo ($task->last_run == 0 ? '-' : self::format_timestamp( self::get_timestamp( $task->last_run ) )); ?><br />
								<strong><?php _e( 'NEXT RUN: ','mainwp-maintenance-extension' ); ?></strong> <?php echo ($task->last_run == 0 ? __( 'Any minute','mainwp-maintenance-extension' ) : $next_run_time); ?>								
							<?php } ?>
                            </td>
                            <td class="">
							<span class="maintenance_trigger_loading"><i  class="fa fa-spinner fa-pulse"></i></span>&nbsp;<a href="#" class="maintenance_run_now mainwp-run-now" optimize-db="<?php echo $do_optimize_db; ?>"><i class="fa fa-play"></i> <?php _e( 'Run Now','mainwp-maintenance-extension' ); ?></a>
                            </td>
                        </tr>
                        <?php
		}
	}

	public static function format_timestamp( $timestamp ) {
		return date_i18n( get_option( 'date_format' ) . ' ' . get_option( 'time_format' ), $timestamp );
	}

	public static function get_timestamp( $timestamp ) {
		$gmtOffset = get_option( 'gmt_offset' );

		return ( $gmtOffset ? ( $gmtOffset * HOUR_IN_SECONDS ) + $timestamp : $timestamp );
	}

	public static function validate_maintenance_tasks( $pMaintenanceTasks ) {

		if ( ! is_array( $pMaintenanceTasks ) ) { return true; }
			global $mainWPMaintenance_Extension_Activator;
		$nothingChanged = true;
		foreach ( $pMaintenanceTasks as $maintenanceTask ) {
			if ( $maintenanceTask->groups == '' ) {
				//Check if sites exist
				$newSiteIds = '';
				$siteIds = ($maintenanceTask->sites == '' ? array() : explode( ',', $maintenanceTask->sites ));
					$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPMaintenance_Extension_Activator->get_child_file(), $mainWPMaintenance_Extension_Activator->get_child_key(), $siteIds, array() );
					$exist_siteids = array();
				foreach ( $dbwebsites as $website ) {
					$exist_siteids[] = $website->id;
				}
				foreach ( $siteIds as $site_id ) {
					if ( ! in_array( $site_id, $exist_siteids ) ) {
						$nothingChanged = false;
					} else {
						$newSiteIds .= ',' . $site_id;
					}
				}
				if ( ! $nothingChanged ) {
					$newSiteIds = trim( $newSiteIds, ',' );
					Maintenance_Extension_DB::get_instance()->update_maintenance_task_values( $maintenanceTask->id, array( 'sites' => $newSiteIds ) );
				}
			} else {
				//Check if groups exist
				$newGroupIds = '';
				$groupIds = explode( ',', $maintenanceTask->groups );
				$groups = apply_filters( 'mainwp-getgroups', $mainWPMaintenance_Extension_Activator->get_child_file(), $mainWPMaintenance_Extension_Activator->get_child_key(), null );
				$exist_groupids = array();
				foreach ( $groups as $group ) {
					$exist_groupids[] = $group['id'];
				}
				foreach ( $groupIds as $groupId ) {
					if ( ! in_array( $groupId, $exist_groupids ) ) {
						$nothingChanged = false;
					} else {
						$newGroupIds .= ',' . $groupId;
					}
				}
				if ( ! $nothingChanged ) {
					$newGroupIds = trim( $newGroupIds, ',' );
					Maintenance_Extension_DB::get_instance()->update_maintenance_task_values( $maintenanceTask->id, array( 'groups' => $newGroupIds ) );
				}
			}
		}

		return $nothingChanged;
	}

	public static function save_maintenance() {

		$task_id = isset($_POST['taskid']) ? $_POST['taskid'] : false;
		$title = $_POST['title'];
		if ( empty( $title ) ) {
			die( __( 'Please enter a valid title for your maintenance task.', 'mainwp-maintenance-extension' ) );
		}
		$schedule = $_POST['schedule'];
		$options = implode( ',', $_POST['options'] );
		$perform = $_POST['perform'];
		$revisions = $_POST['revisions'];
		$recurring_day = isset( $_POST['recurring_day'] ) ? $_POST['recurring_day'] : '';
		$recurring_hour = isset( $_POST['recurring_hour'] ) ? $_POST['recurring_hour'] : '';
		
		if ($recurring_hour > 24 || $recurring_hour < 0) {
			$recurring_hour = 0;
		}
		
		if (!$perform == 1 || ($perform == 1 && $schedule == 'daily')) {
			$recurring_day = '';
		}
			
		
		
		$sites = '';
		$groups = '';
		
		if ( isset( $_POST['sites'] ) ) {
			foreach ( $_POST['sites'] as $site ) {
				if ( '' != $sites ) {
					$sites .= ','; }
				$sites .= $site;
			}
		}
		
		if ( isset( $_POST['groups'] ) ) {
			foreach ( $_POST['groups'] as $group ) {
				if ( '' != $groups ) {
					$groups .= ','; 					
				}
				$groups .= $group;
			}
		}

		$update = array( 			
			'title' => $title, 
			'schedule' => $schedule, 
			'options' => $options, 
			'sites' => $sites, 
			'groups' => $groups, 
			'perform' => $perform, 
			'revisions' => $revisions, 
			'recurring_day' => $recurring_day,
			'recurring_hour' => $recurring_hour
		);
		
		$recal = false;
		if ($task_id){
			$current_sched = Maintenance_Extension_DB::get_instance()->get_maintenance_task_by_id( $task_id );
			
			if ($current_sched ) {
				if ( $current_sched->perform != $perform || $current_sched->schedule != $schedule || $current_sched->recurring_day != $recurring_day || $current_sched->recurring_hour != $recurring_hour) {
					$recal = true;
				}
			} else {
				$recal = true;
			}
			
		} else {
			$recal = true;
		}
		
		if ( $recal && $perform == 1 && ( $schedule == 'weekly' || $schedule == 'monthly' || $schedule == 'yearly' )) {
			$cal_recurring = Maintenance_Extension::calc_scheduled_date( $schedule, $recurring_day, $recurring_hour );
			if (is_array($cal_recurring) && isset($cal_recurring['nextsend'])) {
				$update['schedule_nextsend'] = $cal_recurring['nextsend'];				
			}
		}
				
		$task = Maintenance_Extension_DB::get_instance()->update_task( $task_id, $update ); 							
		
		if ( $task_id && 0 == $task ) {
			die( 'NOAFFECTED' ); 			
		} else if ( ! $task ) {
			die( __( 'Unspecified error occured.', 'mainwp-maintenance-extension' ) );
		}
	}

	public static function save_settings() {
		
		$email = trim( $_POST['email'] );
		$enable = $_POST['enable'];

		if ( ! empty( $email ) && ! preg_match( '/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/is', $email ) ) {
			die( __( 'Please enter a valid email address.', 'mainwp-maintenance-extension' ) );
		}

		$sites = isset( $_POST['sites'] ) ? $_POST['sites'] : array();
		$groups = isset( $_POST['groups'] ) ? $_POST['groups'] : array();

		$settings = array(
			'email'        => $email,
			'enable_alert' => $enable,
			'sites'        => $sites,
			'groups'       => $groups,
		);

		if ( update_option( 'mainwp_maintenance_settings', $settings ) ) {}
			die( 'SUCCESS' );
			die( 'FAIL' );
		}

	public static function load_sites_to_save_settings() {
		global $mainWPMaintenance_Extension_Activator;
		$settings = get_option( 'mainwp_maintenance_settings' );
		$email = '';
		$enable_alert = 0;
		$sites = $groups = $dbwebsites_selected = $selected_sites = array();
		if ( is_array( $settings ) ) {
			$email = isset( $settings['email'] ) ? $settings['email'] : '';
			$enable_alert = isset( $settings['enable_alert'] ) ? intval( $settings['enable_alert'] ) : 0;
			$sites = isset( $settings['sites'] ) && is_array( $settings['sites'] ) ? $settings['sites'] : array();
			$groups = isset( $settings['groups'] ) && $settings['groups'] ? $settings['groups'] : array();

			$dbwebsites_selected = apply_filters( 'mainwp-getdbsites', $mainWPMaintenance_Extension_Activator->get_child_file(), $mainWPMaintenance_Extension_Activator->get_child_key(), $sites, $groups );
			if ( is_array( $dbwebsites_selected ) ) {
				foreach ( $dbwebsites_selected as $website ) {
					$selected_sites[] = $website->id;
				}
			}
		}

		?>
    <input type="hidden" id="mainwp_maintenance_settings_email" value="<?php echo $email;?>"/>
    <input type="hidden" id="mainwp_maintenance_settings_enable_alert" value="<?php echo $enable_alert;?>"/>
    <?php
		$dbwebsites = apply_filters( 'mainwp-getsites', $mainWPMaintenance_Extension_Activator->get_child_file(), $mainWPMaintenance_Extension_Activator->get_child_key(), null );
		if ( is_array( $dbwebsites ) && count( $dbwebsites ) > 0 ) {
			echo '<h3>' . __( 'Saving to child sites...' ). '</h3>';
			foreach ( $dbwebsites as $website ) {
				$_action = ' action="clear_settings" ';
				if ( in_array( $website['id'], $selected_sites ) ) {
					$_action = ' action="save_settings" ';
				}
				echo '<div><strong>' . stripslashes( $website['name'] ) .'</strong>: ';
				echo '<span class="mainwpMaintenanceSitesItem" ' . $_action. ' siteid="' . $website['id'] . '" status="queue"><span class="status">Queue</span>';
				echo '</div><br />';
			}
			?>
      <div id="mainwp_maintenance_apply_setting_ajax_message_zone" class="mainwp_info-box-yellow hidden"><?php _e( 'No child sites found.','mainwp-maintenance-extension' ); ?></div>
  		<?php
			return true;
		} else {
			return 'FAIL';
		}
	}

	public static function perform_save_settings() {
		$siteid = $_POST['siteId'];
		if ( empty( $siteid ) ) {
			die( json_encode( 'FAIL' ) ); }
		global $mainWPMaintenance_Extension_Activator;

		$post_data = array(
			'action'       => $_POST['do_action'],
			'email'        => $_POST['email'],
			'enable_alert' => intval( $_POST['enable'] ),
		);

		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPMaintenance_Extension_Activator->get_child_file(), $mainWPMaintenance_Extension_Activator->get_child_key(), $siteid, 'maintenance_site', $post_data );
		die( json_encode( $information ) );
	}

	public static function remove_maintenance() {

		if ( isset( $_POST['id'] ) && ! empty( $_POST['id'] ) ) {
			$task = Maintenance_Extension_DB::get_instance()->get_maintenance_task_by_id( $_POST['id'] );
			//Remove from DB
			Maintenance_Extension_DB::get_instance()->remove_maintenance_task( $task->id );
			die( 'SUCCESS' );
		}
			die( 'NOTASK' );
	}

	public static function render_progress_content( $taskid = null ) {

			$dbwebsites = array();
			$task_options = '';
			$task_revisions = 0;
			global $mainWPMaintenance_Extension_Activator;
			$dbwebsites = array();
		if ( null != $taskid ) {
			$task = Maintenance_Extension_DB::get_instance()->get_maintenance_task_by_id( $taskid );
			if ( $task ) {
				Maintenance_Extension_DB::get_instance()->update_last_manually_run_time( $taskid );
				$selected_websites = ($task->sites != '') ?  explode( ',', $task->sites ) : array();
				$selected_groups = ($task->groups != '') ? explode( ',', $task->groups ) : array();
				$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPMaintenance_Extension_Activator->get_child_file(), $mainWPMaintenance_Extension_Activator->get_child_key(), $selected_websites, $selected_groups );
				$task_options = $task->options;
				$task_revisions = $task->revisions;
			}
		} else {
			$selected_websites = isset( $_POST['sites'] ) ? $_POST['sites'] : array();
			$selected_groups = isset( $_POST['groups'] ) ? $_POST['groups'] : array();
			$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPMaintenance_Extension_Activator->get_child_file(), $mainWPMaintenance_Extension_Activator->get_child_key(), $selected_websites, $selected_groups );
		}

		if ( count( $dbwebsites ) > 0 ) {
			?>
			<table style="width: 100%">
			<?php
			$str_ids = '';
				foreach ( $dbwebsites as $website ) {
					echo '<tr><td>' . esc_url( $website->url ) . '</td><td style="width: 300px; text-align:right"><span class="maintenance-status-wp" siteid="'. $website->id .'">PENDING</span></td></tr>';
					$str_ids .= '<input type="hidden" name="maintenance_wp_ids[]"  value="'. $website->id .'" />';
				}
				$str_ids .= '<span id="mainwp_mainten_trigger_data" options="' . $task_options . '" revisions="' . $task_revisions . '"></span>';
			?>
      </table>
			<?php
				echo $str_ids;
				die();
		} else {
			die( 'FAIL' );
		}
	}

	public static function execute_maintenance_task( $task ) {

		if ( empty( $task ) ) {
			return; }

		Maintenance_Extension_DB::get_instance()->update_maintenance_run( $task->id );

		$sites = array();
		if ( $task->groups == '' ) {
			if ( $task->sites != '' ) { $sites = explode( ',', $task->sites ); }
		} else {
			$groups = explode( ',', $task->groups );

			$websites = apply_filters( 'mainwp_getwebsitesbygroupids', $groups );
			if ( is_array( $websites ) ) {
				foreach ( $websites as  $website ) {
					if ( ! in_array( $website->id, $sites ) ) {
						$sites[] = $website->id;
					}
				}
			}
		}
		foreach ( $sites as $siteid ) {
			try {
				$maintenanceResult = Manage_Maintenance::maintenance_site( $siteid, $task->options, $task->revisions );
			} catch (Exception $e) {
			}
		}
		Maintenance_Extension_DB::get_instance()->update_schedule_completed( $task->id );
		return;
	}

	public static function maintenance_site( $siteid, $options, $revisions ) {

		if ( empty( $siteid ) ) {
			return false; }

		if ( empty( $siteid ) ) { return false; }

		if ( ! is_array( $options ) ) {
			$options = explode( ',', $options ); }

		if ( count( $options ) <= 0 ) {
			return false; }

		global $mainWPMaintenance_Extension_Activator;

		$post_data = array( 'options' => $options , 'revisions' => $revisions );
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPMaintenance_Extension_Activator->get_child_file(), $mainWPMaintenance_Extension_Activator->get_child_key(), $siteid, 'maintenance_site', $post_data );

		if ( ! isset( $information['status'] ) || ('SUCCESS' != $information['status']) ) { return false; }
		return true;

	}

	public static function maintenance_run_site() {

		$options = $_POST['options'];
		$websiteId = $_POST['wp_id'];
		$revisions = $_POST['revisions'];
		$information = array( 'status' => 'FAIL' );

		if ( empty( $websiteId ) ) { die( json_encode( $information ) ); }

		if ( empty( $options ) ) {
			die( json_encode( $information ) ); }

		if ( ! is_array( $options ) ) {
			$options = explode( ',', $options );
		}
		global $mainWPMaintenance_Extension_Activator;
		$post_data = array( 'options' => $options , 'revisions' => $revisions );
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPMaintenance_Extension_Activator->get_child_file(), $mainWPMaintenance_Extension_Activator->get_child_key(), $websiteId, 'maintenance_site', $post_data );
		die( json_encode( $information ) );
	}
}
