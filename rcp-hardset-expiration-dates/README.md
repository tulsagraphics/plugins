# rcp-hardset-expiration-dates
Allows for a specific expiration date to be assigned to subscription levels. All members will expire on that date
