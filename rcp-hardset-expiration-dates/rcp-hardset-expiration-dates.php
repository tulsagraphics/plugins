<?php
/*
 * Plugin Name: Restrict Content Pro - Hard-set Expiration Dates
 * Description: Allows for a specific expiration date to be assigned to subscription levels. All members will expire on that date
 * Author: Restrict Content Pro Team
 * Author URL: http://restrictcontentpro.com
 * Plugin URL: http://restrictcontentpro.com/addons/hardset-expiration-dates
 * Version: 1.0.5
 */

class RCP_Hardset_Expiration_Dates {

	/**
	 * @access  public
	 * @since   1.0
	 */
	public $dir;

	/**
	 * @access  public
	 * @since   1.0
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Get things started
	 *
	 * @access  public
	 * @since   1.0
	 */
	public function init() {

		$this->dir = trailingslashit( plugin_dir_path( __FILE__ ) );

		if( class_exists( 'RCP_Add_On_Updater' ) ) {
			$updater = new RCP_Add_On_Updater( 285, __FILE__, '1.0.5' );
		}

		$this->includes();

		add_filter( 'rcp_member_get_expiration_date', array( $this, 'get_member_expiration_date' ), 10, 3 );
		add_filter( 'rcp_member_get_expiration_time', array( $this, 'get_member_expiration_time' ), 10, 3 );
		add_filter( 'rcp_member_calculated_expiration', array( $this, 'calculate_expiration' ), 10, 3 );
		add_filter( 'rcp_registration_is_recurring', array( $this, 'maybe_disable_auto_renew' ) );
		add_filter( 'rcp_get_levels', array( $this, 'maybe_deactivate_subscription_level' ) );

	}

	/**
	 * Load our additional files
	 *
	 * @access  public
	 * @since   1.0
	 */
	public function includes() {

		require_once $this->dir . 'includes/admin/level-edit.php';
		require_once $this->dir . 'includes/scripts.php';

	}

	/**
	 * Retrieve member's expiration date from subscription level
	 *
	 * @access  public
	 * @since   1.0
	 */
	public function get_member_expiration_date( $expiration = '', $member_id = 0, RCP_Member $member ) {

		$level_id = $member->get_subscription_id();

		if( ! empty( $level_id ) ) {

			$date = $this->get_subscription_expiration_date( $level_id );

			if( ! empty( $date ) ) {

				$expiration = $date;

			}
		}

		return $expiration;

	}

	/**
	 * Retrieve member's expiration time from subscription level
	 *
	 * @access  public
	 * @since   1.0
	 */
	public function get_member_expiration_time( $expiration = '', $member_id = 0, RCP_Member $member ) {

		$level_id = $member->get_subscription_id();

		if( ! empty( $level_id ) ) {

			$date = $this->get_subscription_expiration_date( $level_id );

			if( ! empty( $date ) ) {

				$expiration = strtotime( $date );

			}

		}

		return $expiration;

	}

	/**
	 * Calculate next expiration date
	 *
	 * @access  public
	 * @since   1.0
	 */
	public function calculate_expiration( $expiration = '', $member_id = 0, RCP_Member $member ) {

		$level_id = get_user_meta( $member_id, 'rcp_pending_subscription_level', true );

		if( empty( $level_id ) ) {

			$level_id = $member->get_subscription_id();

		}

		if( ! empty( $level_id ) ) {

			$date = $this->get_subscription_expiration_date( $level_id );

			if( ! empty( $date ) ) {

				$expiration = $date;

			}

		}

		return $expiration;

	}

	/**
	 * Retrieve expiration date for subscription level
	 *
	 * @access  public
	 * @since   1.0
	 */
	public function get_subscription_expiration_date( $level_id = 0 ) {
		return get_option( 'rcp_level_expiration_' . $level_id, '' );
	}

	/**
	 * Retrieve expiration date for subscription level
	 *
	 * @access  public
	 * @since   1.0
	 */
	public function maybe_disable_auto_renew( $auto_renew ) {

		$exp = $this->get_subscription_expiration_date( rcp_get_registration()->get_subscription() );

		if( ! empty( $exp ) ) {
			$auto_renew = false;
		}

		return $auto_renew;
	}

	/**
	 * Hides and deactivates the subscription level if its
	 * expiration date has passed.
	 *
	 * @access public
	 * @since 1.0.4
	 */
	public function maybe_deactivate_subscription_level( $levels ) {

		if ( ! rcp_is_registration_page() ) {
			return $levels;
		}

		foreach( $levels as $key => $level ) {

			if ( ! $expiration = get_option( 'rcp_level_expiration_' . $level->id ) ) {
				continue;
			}

			if ( strtotime( 'now' ) > strtotime( $expiration ) ) {
				unset( $levels[$key] );
				global $rcp_levels_db;
				$rcp_levels_db->update( $level->id, array( 'status' => 'inactive' ) );
			}
		}

		return $levels;
	}
}

/**
 * Load the plugin
 *
 * @access  public
 * @since   1.0
 */
function rcp_hardset_expiration_dates_load() {
	global $rcp_hsed;

	if( ! function_exists( 'rcp_is_active' ) ) {
		return;
	}

	$rcp_hsed = new RCP_Hardset_Expiration_Dates;
}
add_action( 'plugins_loaded', 'rcp_hardset_expiration_dates_load' );