<?php
class MainWP_Uploader_Utility
{
	static function ctype_digit( $str ) {

		return (is_string( $str ) || is_int( $str ) || is_float( $str )) && preg_match( '/^\d+\z/', $str );
	}
	static function get_short_file_name( $name ) {

		$info = pathinfo( $name );
		$short_name = $name;
		if ( strlen( $info['filename'] ) > 30 ) {
			$short_name = substr( $info['filename'], 0, 19 ) . '...' . substr( $info['filename'], -9 );
			$short_name .= '.' . $info['extension'];
		}
		return $short_name;
	}

	static function to_bytes( $str ) {
		$val = trim( $str );
		$last = strtolower( $str[ strlen( $str ) -1 ] );
		switch ( $last ) {
			case 'g': $val *= 1024;
			case 'm': $val *= 1024;
			case 'k': $val *= 1024;
		}
		return $val;
	}
}
