<?php
/*
Plugin Name: MainWP File Uploader Extension
Plugin URI: https://mainwp.com
Description: MainWP File Uploader Extension gives you an simple way to upload files to your child sites! Requires the MainWP Dashboard plugin.
Version: 1.4
Author: MainWP
Author URI: https://mainwp.com
Icon URI:
Documentation URI: http://docs.mainwp.com/category/mainwp-extensions/mainwp-file-uploader-extension/
*/

if ( ! defined( 'MAINWP_FILE_UPLOADER_PLUGIN_FILE' ) ) {
	define( 'MAINWP_FILE_UPLOADER_PLUGIN_FILE', __FILE__ );
}

class MainWP_Uploader_Extension
{
	public static $instance = null;
	public  $plugin_handle = 'mainwp-upload-extension';
	protected $plugin_url;
	private $plugin_slug;

	protected $mainWPUploader;

	static function get_instance() {

		if ( null == MainWP_Uploader_Extension::$instance ) { MainWP_Uploader_Extension::$instance = new MainWP_Uploader_Extension(); }
		return MainWP_Uploader_Extension::$instance;
	}

	public function __construct() {

		$this->plugin_url = plugin_dir_url( __FILE__ );
		$this->plugin_slug = plugin_basename( __FILE__ );
		if ( is_admin() ) {
			add_action( 'in_admin_header', array( &$this, 'in_admin_head' ) ); // Adds Help Tab in admin header
		}
		add_action( 'init', array( &$this, 'init' ) );
		add_filter( 'plugin_row_meta', array( &$this, 'plugin_row_meta' ), 10, 2 );
		
		MainWP_Uploader::get_instance()->init();
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
	}

	function in_admin_head() {
		if ( isset( $_GET['page'] ) && $_GET['page'] == 'Extensions-Mainwp-File-Uploader-Extension' ) {
			self::addHelpTabs(); // If page is the Extension then call this 'addHelpTabs' function
		}
	}
	/**
	 * This function add help tabs in header.
	 * @return void
	 */
	public static function addHelpTabs() {
		$screen = get_current_screen(); //This function returns an object that includes the screen's ID, base, post type, and taxonomy, among other data points.
		$i      = 1;
		$screen->add_help_tab( array(
			'id'      => 'mainwp_fu_helptabs_' . $i ++,
			'title'   => __( 'First Steps with Extensions', 'advanced-uptime-monitor-extension' ),
			'content' => self::getHelpContent( 1 ),
		) );
		$screen->add_help_tab( array(
			'id'      => 'mainwp_fu_helptabs_' . $i ++,
			'title'   => __( 'MainWP File Uploader Extension', 'advanced-uptime-monitor-extension' ),
			'content' => self::getHelpContent( 2 ),
		) );
	}
	/**
	 * Get help tab content.
	 *
	 * @param int $tabId
	 *
	 * @return string|bool
	 */
	public static function getHelpContent( $tabId ) {
		ob_start();
		if ( 1 == $tabId ) {
			?>
			<h3><?php echo __( 'First Steps with Extensions', 'mainwp-file-uploader-extension' ); ?></h3>
			<p><?php echo __( 'If you are having issues with getting started with the MainWP extensions, please review following help documents', 'mainwp-file-uploader-extension' ); ?></p>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'What are the MainWP Extensions', 'mainwp-file-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/order-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Order Extension(s)', 'mainwp-file-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/my-downloads-and-api-keys/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'My Downloads and API Keys', 'mainwp-file-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/install-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Install Extension(s)', 'mainwp-file-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/activate-extensions-api/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Activate Extension(s) API', 'mainwp-file-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/updating-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Updating Extension(s)', 'mainwp-file-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/remove-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Remove Extension(s)', 'mainwp-file-uploader-extension' ); ?></a><br/><br/>
			<a href="https://mainwp.com/help/category/mainwp-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Help Documenation for all MainWP Extensions', 'mainwp-file-uploader-extension' ); ?></a>
		<?php } else if ( 2 == $tabId ) { ?>
			<h3><?php echo __( 'MainWP File Uploader Extension', 'advanced-uptime-monitor-extension' ); ?></h3>
			<a href="https://mainwp.com/help/docs/file-uploader-extension/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'File Uploader Extension', 'mainwp-file-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/file-uploader-extension/upload-files-to-child-sites/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Upload Files to Child Sites', 'mainwp-file-uploader-extension' ); ?></a><br/>
		<?php }
		$output = ob_get_clean();
		return $output;
	}

	public function init() {

	}

	public function plugin_row_meta( $plugin_meta, $plugin_file ) {

		if ( $this->plugin_slug != $plugin_file ) { return $plugin_meta; }

		$slug = basename($plugin_file, ".php");
		$api_data = get_option( $slug. '_APIManAdder');		
		if (!is_array($api_data) || !isset($api_data['activated_key']) || $api_data['activated_key'] != 'Activated' || !isset($api_data['api_key']) || empty($api_data['api_key']) ) {
			return $plugin_meta;
		}
		
		$plugin_meta[] = '<a href="?do=checkUpgrade" title="Check for updates.">Check for updates now</a>';
		return $plugin_meta;
	}

	public function admin_init() {

		wp_enqueue_style( 'mainwp-uploader-extension', $this->plugin_url . 'css/mainwp-uploader.css' );
		wp_enqueue_script( 'mainwp-uploader-extension', $this->plugin_url . 'js/mainwp-uploader.js', array(), '1.4' );
		MainWP_Uploader::get_instance()->admin_init();
	}

	static function create_folders() {

		$dir = apply_filters( 'mainwp_getspecificdir', 'uploader/' );
		if ( ! file_exists( $dir ) ) {
			@mkdir( $dir, 0777, true );
		}
		//        if (!file_exists($dir . '/index.php'))
		//        {
		//            @touch($dir . '/index.php');
		//        }
	}
}


function mainwp_uploader_extension_autoload( $class_name ) {

	$allowedLoadingTypes = array( 'class', 'page' );
	$class_name = str_replace( '_', '-', strtolower( $class_name ) );
	foreach ( $allowedLoadingTypes as $allowedLoadingType ) {
		$class_file = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . str_replace( basename( __FILE__ ), '', plugin_basename( __FILE__ ) ) . $allowedLoadingType . DIRECTORY_SEPARATOR . $class_name . '.' . $allowedLoadingType . '.php';
		if ( file_exists( $class_file ) ) {
			require_once( $class_file );
		}
	}
}

if ( function_exists( 'spl_autoload_register' ) ) {
	spl_autoload_register( 'mainwp_uploader_extension_autoload' );
} else {
	function __autoload( $class_name ) {

		mainwp_uploader_extension_autoload( $class_name );
	}
}


register_activation_hook( __FILE__, 'uploader_extension_activate' );
register_deactivation_hook( __FILE__, 'uploader_extension_deactivate' );

function uploader_extension_activate() {

	update_option( 'mainwp_uploader_extension_activated', 'yes' );
	MainWP_Uploader_Extension::create_folders();
	$extensionActivator = new MainWP_Uploader_Extension_Activator();
	$extensionActivator->activate();	
}

function uploader_extension_deactivate() {

	$extensionActivator = new MainWP_Uploader_Extension_Activator();
	$extensionActivator->deactivate();
}

class MainWP_Uploader_Extension_Activator
{
	protected $mainwpMainActivated = false;
	protected $childEnabled = false;
	protected $childKey = false;
	protected $childFile;
	protected $plugin_handle = 'mainwp-file-uploader-extension';
	protected $product_id = 'MainWP File Uploader Extension';
	protected $software_version = '1.4';

	public function __construct() {

		$this->childFile = __FILE__;
		add_filter( 'mainwp-getextensions', array( &$this, 'get_this_extension' ) );
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', false );

		if ( $this->mainwpMainActivated !== false ) {
			$this->activate_this_plugin();
		} else {
			add_action( 'mainwp-activated', array( &$this, 'activate_this_plugin' ) );
		}
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_action( 'admin_notices', array( &$this, 'mainwp_error_notice' ) );
	}

	function admin_init() {
		if ( get_option( 'mainwp_uploader_extension_activated' ) == 'yes' ) {
			delete_option( 'mainwp_uploader_extension_activated' );
			wp_redirect( admin_url( 'admin.php?page=Extensions' ) );
			return;
		}
	}

	function get_this_extension( $pArray ) {

		$pArray[] = array( 'plugin' => __FILE__, 'api' => $this->plugin_handle, 'mainwp' => true, 'callback' => array( &$this, 'settings' ), 'apiManager' => true );
		return $pArray;
	}

	function settings() {
		do_action( 'mainwp-pageheader-extensions', __FILE__ );		
		MainWP_Uploader::render();		
		do_action( 'mainwp-pagefooter-extensions', __FILE__ );
	}

	function activate_this_plugin() {
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', $this->mainwpMainActivated );
		$this->childEnabled = apply_filters( 'mainwp-extension-enabled-check', __FILE__ );		
		$this->childKey = $this->childEnabled['key'];
		if ( function_exists( 'mainwp_current_user_can' )&& ! mainwp_current_user_can( 'extension', 'mainwp-file-uploader-extension' ) ) {
			return; }

		new MainWP_Uploader_Extension();
	}

	public function get_child_key() {

		return $this->childKey;
	}

	public function get_child_file() {

		return $this->childFile;
	}

	function mainwp_error_notice() {

		global $current_screen;
		if ( $current_screen->parent_base == 'plugins' && $this->mainwpMainActivated == false ) {
			echo '<div class="error"><p>MainWP File Uploader Extension ' . __( 'requires <a href="http://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> to be activated in order to work. Please install and activate <a href="http://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> first.' ) . '</p></div>';
		}
	}

	public function update_option( $option_name, $option_value ) {

		$success = add_option( $option_name, $option_value, '', 'no' );

		if ( ! $success ) {
			$success = update_option( $option_name, $option_value );
		}

		 return $success;
	}

	public function activate() {
		$options = array(
		'product_id' => $this->product_id,
							'activated_key' => 'Deactivated',
							'instance_id' => apply_filters( 'mainwp-extensions-apigeneratepassword', 12, false ),
							'software_version' => $this->software_version,
						);
		$this->update_option( $this->plugin_handle . '_APIManAdder', $options );
	}

	public function deactivate() {
		$this->update_option( $this->plugin_handle . '_APIManAdder', '' );
	}
}

global $mainWPUploaderExtensionActivator;
$mainWPUploaderExtensionActivator = new MainWP_Uploader_Extension_Activator();
