jQuery( document ).ready(function($) {
	jQuery( '.mainwp-uploader-tut-link' ).live('click', function() {
		var parent = jQuery( this ).closest( '.mainwp-uploader-tut-box' );
		parent.find( '.mainwp-uploader-tut-content' ).show();
		parent.find( '.mainwp-uploader-tut-dismiss' ).show();
		jQuery( this ).hide();
		return false;
	});

	jQuery( '.mainwp-uploader-tut-dismiss' ).live('click', function() {
		var parent = jQuery( this ).closest( '.mainwp-uploader-tut-box' );
		parent.find( '.mainwp-uploader-tut-content' ).hide();
		parent.find( '.mainwp-uploader-tut-link' ).show();
		jQuery( this ).hide();
		return false;
	});

	mainwp_uploader_upload_init = function () {
		jQuery( '#uploader-file-error-box' ).html( '' ).hide()
		jQuery( '#uploader-file-infor-box' ).html( '' ).hide();
		jQuery( '#selected_sites' ).removeClass( 'form-invalid' );
		jQuery( '#selected_groups' ).removeClass( 'form-invalid' );
	};
        
        jQuery( '.file_uploader_delete_file' ).live('click', function() {
                var parent = jQuery(this).closest('.uploader-setting-upload-new-files');
                
                var data = {
			action:'mainwp_uploader_delete_file',
			filename: parent.attr('filename'),
                        nonce: jQuery('#uploader_nonce').val(),
                    };
                    
		parent.find('i').show();
		jQuery.post(ajaxurl, data, function(response) {
			parent.find('i').hide();
                        if (response && response['error']) {
                            parent.find( '.status' ).html( '<font color="red">' + response['error'] + '</font>' );
                        } else if (response && response['ok']) {
                            parent.html('The file has been deleted!' );                            
                        } else {
                            parent.find( '.status' ).html( '<font color="red">' + __( "Undefined error" ) + '</font>' );
                        }			
		}, 'json');
		return false;
        });

	jQuery( '#mainwp_uploader_btn_upload' ).live('click', function() {
		var errors = [];
		var selected_sites = [];
		var selected_groups = [];
		var selected_files = [];

		mainwp_uploader_upload_init();

		var fileUpload = jQuery( '.uploader-setting-upload-file[status="queue"]' );
		if (fileUpload) {
			jQuery( '.uploader-setting-upload-file[status="queue"]' ).each(function (i) {
				_file = $( this ).attr( 'filename' );
				selected_files.push( _file );
			});
		}

		var newFileUpload = jQuery( '.uploader-setting-upload-new-files' );
		if (newFileUpload) {
			jQuery( '.uploader-setting-upload-new-files' ).each(function (i) {
				_file = $( this ).attr( 'filename' );
				selected_files.push( _file );
			});
		}

		if (selected_files.length == 0) {
			errors.push( __( 'There is no files to upload.','mainwp-file-uploader-extension' ) );
		}

		if (jQuery( '#select_by' ).val() == 'site') {
			jQuery( "input[name='selected_sites[]']:checked" ).each(function (i) {
				selected_sites.push( jQuery( this ).val() );
			});
			if (selected_sites.length == 0) {
				errors.push( __( 'Please select websites or groups','mainwp-file-uploader-extension' ) );
				jQuery( '#selected_sites' ).addClass( 'form-invalid' );
			}
		} else {
			jQuery( "input[name='selected_groups[]']:checked" ).each(function (i) {
				selected_groups.push( jQuery( this ).val() );
			});
			if (selected_groups.length == 0) {
				errors.push( __( 'Please select websites or groups','mainwp-file-uploader-extension' ) );
				jQuery( '#selected_groups' ).addClass( 'form-invalid' );
			}
		}

		if (errors.length > 0) {
			jQuery( '#uploader-file-error-box' ).html( "<span>" + errors.join( '<br />' ) + "<span>" );
			jQuery( '#uploader-file-error-box' ).show();
			return;
		}

		var path = $( '#mainwp_uploader_select_path' ).val();
		if (path !== '/') {
			path += '/';
		}

		if ($( '#mainwp_uploader_path_option' ).val().trim() !== '') {
			path += $( '#mainwp_uploader_path_option' ).val().trim();
		}

		var data = {
			action:'mainwp_uploader_load_sites',
			path: path,
			'files[]': selected_files,
			'sites[]': selected_sites,
			'groups[]': selected_groups
		};
		$( '.mainwp-uploader-loading' ).show();
		jQuery.post(ajaxurl, data, function(response) {
			$( '.mainwp-uploader-loading' ).hide();
			if (response === 'NOSITE') {
				$( '#uploader-file-error-box' ).html( '<p>No selected sites to upload.</p>' ).show();
			} else if (response === 'NOFILE') {
				$( '#uploader-file-error-box' ).html( '<p>No files to upload.</p>' ).show();
			} else {
				$( '#mainwp-uploader-settings-inside' ).html( response );
				uploader_setting_upload_file_next();
			}
		});
		return false;

	});

});

function mainwpUploaderCreateUploaderFile(){
	var uploader = new qq.FileUploader({
		element: document.getElementById( 'mainwp-uploader-file-uploader' ),
		action: location.href,
		template:   '<div class="qq-uploader">' +
					'<div class="qq-upload-drop-area"><span>or drop files here to upload</span></div>' + '<div id="mainwp-uploader-style"><div class="qq-upload-button button button-hero">Choose File</div>&nbsp;&nbsp;&nbsp;<em>or drop files here to upload</em></div>' +
					'<ul class="qq-upload-list"></ul>' +
					'</div>',
		onComplete: function(id, fileName, result){ uploader_setting_uploadbulk_oncomplete( id, fileName, result )},
		params: {uploader_do: 'UploaderInstallBulk-uploadfile'}
	});
}

uploader_setting_uploadbulk_oncomplete = function (id, fileName, result ) {
	if (result.success) {
		if (totalSuccess > 0) { // global variable
			jQuery( ".qq-upload-file" ).each(function (i) {
				if (jQuery( this ).parent().attr( 'class' ) && jQuery( this ).parent().attr( 'class' ).replace( /^\s+|\s+$/g, "" ) == 'qq-upload-success') {
					_file = jQuery( this ).attr( 'filename' );
					if (jQuery( this ).next().next().attr( 'class' ) != 'uploader-setting-upload-file') {
						jQuery( this ).next().after( '<span class="uploader-setting-upload-file" status="queue" id="' + id + '" filename ="' + _file + '">' + '<i class="fa fa-check" aria-hidden="true"></i>' + __( 'File is ready!' ) + '</span> ' );
					}
				}
			});
		}
	}
}

var uploader_CurrentThreads = 0;
var uploader_MaxThreads = 3;
var uploader_TotalThreads = 0;
var uploader_FinishedThreads = 0;

uploader_setting_upload_file_next = function(){
	if (uploader_TotalThreads == 0) {
		uploader_TotalThreads = jQuery( '.mainwpUploaderFileItem[status="queue"]' ).length;
	}
	while ((fileToUpload = jQuery( '.mainwpUploaderFileItem[status="queue"]:first' )) && (fileToUpload.length > 0)  && (uploader_CurrentThreads < uploader_MaxThreads)) {
		uploader_setting_upload_file_start_specific( fileToUpload );
	}
}

uploader_setting_upload_file_start_specific = function (pFileToUpload)
{
	uploader_CurrentThreads++;
	pFileToUpload.attr( 'status', 'progress' );
	pFileToUpload.html( '<i class="fa fa-spinner fa-pulse"></i> ' + __( 'Uploading...','mainwp-file-uploader-extension' ) );
	var data = {
		action:'mainwp_uploader_uploadbulk_file',
		siteId: pFileToUpload.closest( '.mainwpUploaderSiteItem' ).attr( 'siteid' ),
		filename: pFileToUpload.attr( 'filename' ),
		path: jQuery( '#mainwp_uploader_upload_file_path' ).val()
	};
	jQuery.post(ajaxurl, data, function(response) {
		var _error = false;
		if (response) {
			if (response == 'NOTEXIST') {
				pFileToUpload.html( '<i class="fa fa-exclamation-circle"></i> ' + __( 'Upload failed! File could not be found.','mainwp-file-uploader-extension' ) );
				_error = true;
			} else if (response == 'ERRORCREATEDIR') {
				pFileToUpload.html( '<i class="fa fa-exclamation-circle"></i> ' + __( 'Upload failed! Directory could not be created. Please check server permissions or contact the MainWP Support.','mainwp-file-uploader-extension' ) );
				_error = true;
			} else if (response['success']) {
				pFileToUpload.html( '<i class="fa fa-check"></i> ' + __( 'Upload successful!','mainwp-file-uploader-extension' ) );
			} else if (response['error']) {
				pFileToUpload.html( response['error'] );
				_error = true;
			} else {
				pFileToUpload.html( '<i class="fa fa-exclamation-circle"></i> ' + __( 'Upload failed! Please contact the MainWP Support for additional help.','mainwp-file-uploader-extension' ) );
				_error = true;
			}
		} else {
			pFileToUpload.html( '<i class="fa fa-exclamation-circle"></i> ' + __( 'Upload failed! Undefined error. Please contact the MainWP Support for additional help.','mainwp-file-uploader-extension' ) );
			_error = true;
		}
		if (_error) {
			pFileToUpload.css( 'color', '#a00' ); 
		} else {
			pFileToUpload.css( 'color', '#0073aa' ); 
		}

		uploader_CurrentThreads--;
		uploader_FinishedThreads++;
		if (uploader_FinishedThreads == uploader_TotalThreads && uploader_FinishedThreads != 0) {
			jQuery( '#mainwp_uploader_upload_file_ajax_message_zone' ).html( 'Files uploaded successfully!' ).fadeIn( 100 );
                        jQuery( '#mainwp-uploader-settings-inside' ).append( '<a class="button-primary button button-hero" href="admin.php?page=Extensions-Mainwp-File-Uploader-Extension">Upload New File(s)<a/>' );				
			var data = {
				action:'mainwp_uploader_delete_temp_file',
				tmp_files: jQuery( '#mainwp_uploader_tmp_files_name' ).val()
			};
			jQuery.post(ajaxurl, data, function(response) {
                            
			})
		}
		uploader_setting_upload_file_next();
	},'json');
};
