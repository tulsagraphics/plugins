<?php
class MainWP_Uploader
{
	public static $instance = null;
	private $allowedExtensions = array();

	static function get_instance() {

		if ( null === MainWP_Uploader::$instance ) { MainWP_Uploader::$instance = new MainWP_Uploader(); }
		return MainWP_Uploader::$instance;
	}
	public function init() {

		add_action( 'wp_ajax_mainwp_uploader_load_sites', array( $this, 'uploader_load_sites' ) );
        add_action( 'wp_ajax_mainwp_uploader_delete_file', array( $this, 'ajax_delete_file' ) );
		add_action( 'wp_ajax_mainwp_uploader_uploadbulk_file', array( $this, 'uploader_uploadbulk_file' ) );
		add_action( 'wp_ajax_mainwp_uploader_delete_temp_file', array( $this, 'uploader_delete_temp_file' ) );
		$this->allowedExtensions = array(
		'zip',
		'rar',
		'txt',
		'php',
		'xml',
		'bmp',
		'php',
		'html',
		'css',
		'ico',
		'jpg',
		'jpeg',
		'png',
		'gif',
		'pdf',
		'doc',
		'docx',
		'ppt',
		'pptx',
		'pps',
		'ppsx',
		'odt',
		'xls',
		'xlsx',
		'js',
		'mp3',
		'm4a',
		'ogg',
		'wav',
		'mp4',
		'm4v',
		'mov',
		'wmv',
		'avi',
		'mpg',
		'ogv',
		'3gp',
		'3g2',
		'po',
		'mo',
		'dat'
		);
	}

	public function admin_init() {

		if ( isset( $_REQUEST['uploader_do'] ) ) {
			if ( 'UploaderInstallBulk-uploadfile' == $_REQUEST['uploader_do'] ) {
				// list of valid extensions, ex. array("jpeg", "xml", "bmp")
				// $allowedExtensions = array('zip');
				// max file size in bytes
				 //$sizeLimit = 20 * 1024 * 1024; //20MB = max allowed

				$postSize = MainWP_Uploader_Utility::to_bytes( ini_get( 'post_max_size' ) );
				$uploadSize = MainWP_Uploader_Utility::to_bytes( ini_get( 'upload_max_filesize' ) );
				$sizeLimit = $postSize;
				if ( $postSize > $uploadSize ) {
					$sizeLimit = $uploadSize;
				}

				$uploader = apply_filters( 'mainwp_qq2fileuploader', $this->allowedExtensions, $sizeLimit );

				$path = apply_filters( 'mainwp_getspecificdir', 'uploader/' );

				$result = $uploader->handleUpload( $path, true );
				// to pass data through iframe you will need to encode all html tags
				die( htmlspecialchars( json_encode( $result ), ENT_NOQUOTES ) );
			}
		}
	}

	// to fix ajax request
	function sanitize_upload_files_name( $path, $files ) {
		if ( ! is_array( $files ) ) {
			return $files; }
		$new_files = array();
		foreach ( $files as $file ) {
			$san_name = sanitize_file_name( $file );
			if ( substr( $san_name, -3 ) == 'php' ) {
				$san_name = substr( $san_name, 0, - 3 ) . 'phpfile.txt'; //rename php file
			}
			if ( $file != $san_name && @rename( $path . $file, $path . $san_name ) ) {
				$new_files[] = $san_name; } else { 				$new_files[] = $file; }
		}
		return $new_files;
	}

	function uploader_load_sites() {
		global $mainWPUploaderExtensionActivator;
		$files = isset( $_POST['files'] ) ? $_POST['files'] : array();
		$selected_sites = isset( $_POST['sites'] ) ? $_POST['sites'] : array();
		$selected_groups = isset( $_POST['groups'] ) ? $_POST['groups'] : array();
		$path = trim( $_POST['path'] );
		if ( substr( $path, -1 ) != '/' ) {
			$path .= '/'; }

		if ( count( $files ) == 0 ) {
			die( 'NOFILE' );
		}

		$uploader_root = apply_filters( 'mainwp_getspecificdir', 'uploader/' );
		$files = $this->sanitize_upload_files_name( $uploader_root, $files );

		$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPUploaderExtensionActivator->get_child_file(), $mainWPUploaderExtensionActivator->get_child_key(), $selected_sites, $selected_groups );

		if ( ! is_array( $dbwebsites ) || count( $dbwebsites ) == 0 ) {
			die( 'NOSITE' );
		}
		?>
		<div id="mainwp_uploader_upload_file_ajax_message_zone" class="mainwp-notice mainwp-notice-green uploader-hidden"></div>
		<div class="postbox">
		<h3 class="mainwp_box_title"><span><i class="fa fa-upload"></i> <?php  _e( 'Uploading file(s) to child site(s)...','mainwp-file-uploader-extension' ); ?></span></h3>
		<?php
		$all_files = '';

		foreach ( $files as $file ) {
			$all_files .= '<strong>' . MainWP_Uploader_Utility::get_short_file_name( $file ) .'</strong> : <span class="mainwpUploaderFileItem" filename="' . $file . '" status="queue"><span class="status">Queue...</span></span><br />';
		}

		foreach ( $dbwebsites as $website ) {
			echo '<div class="mainwp-postbox-actions-top"><strong>' . stripslashes( $website->name ) .'</strong></div>';
			echo '<div class="inside">';
			echo '<span class="mainwpUploaderSiteItem" siteid="' . $website->id . '"><br />';
			echo $all_files;
			echo '</span>';
			echo '</div>';
		}
		?>
		</div>
        <input type="hidden" id="mainwp_uploader_upload_file_path" value="<?php echo $path; ?>"/>
        <input type="hidden" id="mainwp_uploader_tmp_files_name" value="<?php echo implode( ',', $files ); ?>"/>
        <?php
		die();
	}
    
    function ajax_delete_file() {
        if (!isset($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'uploader-nonce'))
                exit('Invalid request!');                
		$file_name = $_POST['filename'];
        
		if ( empty( $file_name ) ) {
			die( json_encode( array( 'error' => 'Invalid data.' )) );                         
        }
                    
		$local_file = apply_filters( 'mainwp_getspecificdir','uploader' ) . $file_name;
        
        $success = false;
		if ( file_exists( $local_file ) ) {
			 $success = @unlink($local_file);            
        }
        if ($success) {
            die( json_encode( array('ok' => 1) ) );
        } 
        die('Failed');
    }
    
	function uploader_uploadbulk_file() {

		$site_id = $_POST['siteId'];
		$file_name = $_POST['filename'];
		if ( empty( $site_id ) || empty( $file_name ) ) {
			die( json_encode( 'FAIL' ) ); }
		$local_file = apply_filters( 'mainwp_getspecificdir','uploader' ) . $file_name;
		if ( ! file_exists( $local_file ) ) {
			die( json_encode( 'NOTEXIST' ) ); }

		global $mainWPUploaderExtensionActivator;

		$file_url = apply_filters( 'mainwp_getdownloadurl','uploader', $file_name );
		$post_data = array(
			'url' => base64_encode( $file_url ),
			'filename' => $file_name,
			'path' => trim( $_POST['path'] ),
		);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPUploaderExtensionActivator->get_child_file(), $mainWPUploaderExtensionActivator->get_child_key(), $site_id, 'uploader_action', $post_data );
		die( json_encode( $information ) );
	}

	function uploader_delete_temp_file() {
		$tmp_files = $_POST['tmp_files'];
		$tmp_files = explode( ',', $tmp_files );
		$uploader_root = apply_filters( 'mainwp_getspecificdir', 'uploader/' );
		if ( is_array( $tmp_files ) ) {
			foreach ( $tmp_files as $file ) {
				$file_path = $uploader_root . $file;
				if ( file_exists( $file_path ) ) {
					@unlink( $file_path ); }
			}
		}
		die( 'SUCCESS' );
	}

	public function scan_upload_folder() {
		$uploader_root = apply_filters( 'mainwp_getspecificdir', 'uploader/' );
		$dir = @opendir( $uploader_root );
		$scan_files = array();
		if ( $dir ) {
			while ( ($file = readdir( $dir ) ) !== false ) {
				if ( substr( $file, 0, 1 ) == '.' ) {
					continue;                     
                }                
				$ext = pathinfo( $file, PATHINFO_EXTENSION );
				if ( ! in_array( $ext, $this->allowedExtensions ) ) {
					continue;                     
                }
				$scan_files[] = $file;
			}
			closedir( $dir );
		}
		return $scan_files;
	}

	public static function render() {

		$new_files = MainWP_Uploader::get_instance()->scan_upload_folder();
		?>   
	    <div id="mainwp_aup_ajax_message_zone" class="mainwp_info-box-yellow hidden"></div>
	    <div  class="mainwp-notice mainwp-notice-red uploader-hidden" id="uploader-file-error-box"></div>
	    <div class="mainwp-notice mainwp-notice-green uploader-hidden" id="uploader-file-infor-box"></div>
	    <div id="mainwp-uploader-settings-inside">
		    <div id="uploader_select_sites_box" class="mainwp_config_box_right">                                            
		        <?php do_action( 'mainwp_select_sites_box', __( 'Step 2: Select Sites', 'mainwp-file-uploader-extension' ), 'checkbox', true, true, 'mainwp_select_sites_box_right', '', array(), array() ); ?>													
		    </div>
		    <div class="postbox" id="mainwp-file-uploader-box">
		    <h3 class="mainwp_box_title"><span><i class="fa fa-upload"></i> <?php _e( 'Step 1: Upload File', 'mainwp-file-uploader-extension' ); ?></span></h3>
		    <div class="inside">  
			    <table class="form-table">
			        <tbody>
			            <tr>
			                <th scope="row"><?php _e( 'Upload File to Insert (Replace)','mainwp-file-uploader-extension' ); ?></th>
			                <td class="file-uploader-drop-zone">
			                    <div id="mainwp-uploader-file-uploader"></div>
			                    <?php
								if ( count( $new_files ) > 0 ) {
									echo __( 'Files ready to be uploaded to your child sites:','mainwp-file-uploader-extension' );
									?>
									<ul class="qq-upload-list">
									<?php
									foreach ( $new_files as $file ) {
										$short_name = MainWP_Uploader_Utility::get_short_file_name( $file );
										?>
										<li class="qq-upload-success">
                                            <span class="uploader-setting-upload-new-files qq-upload-file" filename="<?php echo $file; ?>"><?php echo $short_name; ?> <a href="#" class="file_uploader_delete_file"><?php _e('Delete', 'mainwp-file-uploader-extension'); ?></a><span class="working"> <span class="status"></span> <i style="display: none" class="fa fa-spinner fa-pulse "></i></span></span>
										</li>                            
			                            <?php
									}
										?>
			                            </ul>
			                        <?php
									}
								?>
			                </td>
			            </tr>
			            <tr>
			                <th scope="row"><?php _e( 'Enter Path to Insert (Replace)','mainwp-file-uploader-extension' ); ?></th>
			                <td>
			                    <select name="mainwp_uploader_select_path" id="mainwp_uploader_select_path">
				                    <option value="/">/</option>
				                    <option value="wp-admin">wp-admin</option>
				                    <option value="wp-content" selected="selected">wp-content</option>
				                    <option value="wp-includes">wp-includes</option>
			                    </select> / 
			                    <input type="text" id="mainwp_uploader_path_option" 
			                    	   name="mainwp_uploader_path_option" 
			                           placeholder="<?php _e( 'Optional', 'mainwp-file-uploader-extension' );?>"
			                           value="" /><br/>
			                    <em style="margin-left: 125px;">/directory/subdirectory/../..</em>
			                </td>
			            </tr>       
			        </tbody>
			    </table>
			    </div> 
			    <div class="mainwp-postbox-actions-top">
			        <strong ><?php _e( 'Note: ','mainwp-file-uploader-extension' ); ?></strong><?php _e( 'This Extension uploads files directly to your server so treat it with the care way you would when uploading a file via FTP.','mainwp-file-uploader-extension' ); ?>
			        <ul style="list-style-type:disc; margin-left: 2em;">
			            <li><?php _e( 'The Extension will overwrite files of the same name in the same folder','mainwp-file-uploader-extension' ); ?></li>
			            <li><?php _e( 'If you upload a corrupt file or a file with an error it could break your site','mainwp-file-uploader-extension' ); ?></li>
			            <li><?php _e( 'If you are having trouble uploading a file check this <a href="https://mainwp.com/help/docs/file-uploader-extension/upload-files-to-child-sites/" target="_blank">help document</a>.','mainwp-file-uploader-extension' ); ?></li>
			        </ul>
			        <strong><em><?php _e( 'MainWP is not responsible for the files that you upload to your sites.','mainwp-file-uploader-extension' ); ?></em></strong>
			    </div>
		    </div>
		    <div style="clear:both;"></div>
		    <p class="submit" style="">
		        <input type="button" value="<?php _e( 'Upload File','mainwp-file-uploader-extension' ); ?>" class="button-primary button button-hero" id="mainwp_uploader_btn_upload" name="submit">
		        <span class="mainwp-uploader-loading hidden"><i class="fa fa-spinner fa-pulse "></i></span>
		    </p>
            <input type="hidden" id="uploader_nonce" value="<?php echo wp_create_nonce('uploader-nonce'); ?>"/>
		    <script>
		        // in your app create uploader as soon as the DOM is ready
		        // don't wait for the window to load
		        mainwpUploaderCreateUploaderFile();      
		    </script>
	    
	    </div> <!-- id="mainwp-uploader-settings-inside" -->
    <?php
	}
}


