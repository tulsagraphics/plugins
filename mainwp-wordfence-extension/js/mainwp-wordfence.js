jQuery( document ).ready(function ($) {
	jQuery( '#mfc_redirectForm' ).submit();

        $( '.mainwp_wfc_tabs_lnk a' ).on('click', function () {            
            jQuery( '.mainwp_wfc_tabs_lnk a' ).removeClass( 'mainwp_action_down' );
            jQuery( this ).addClass( 'mainwp_action_down' );   
            
            if (typeof MWP_WFAD !== undefined)
                MWP_WFAD._tabHasFocus = false;
            
            if (jQuery( this ).attr('href') != '#') {            
                return;            
            }
            jQuery('.mainwp_wfc_tabs_content').hide();            
            jQuery('.mainwp_wfc_tabs_content[data-tab="' + jQuery( this ).data('tab') + '"]').show();
            return false;
	});
                
	$( '.wfc_plugin_upgrade_noti_dismiss' ).on('click', function () {
		var parent = $( this ).closest( '.ext-upgrade-noti' );
		parent.hide();
		var data = {
			action: 'mainwp_wfc_upgrade_noti_dismiss',
			siteId: parent.attr( 'id' ),
			new_version: parent.attr( 'version' ),
		}
		jQuery.post(ajaxurl, data, function (response) {

		});
		return false;
	});

	$( '.mwp_wfc_active_plugin' ).on('click', function () {
		mainwp_wfc_plugin_active_start_specific( $( this ), false );
		return false;
	});

	$( '.mwp_wfc_upgrade_plugin' ).on('click', function () {
		mainwp_wfc_plugin_upgrade_start_specific( $( this ), false );
		return false;
	});

	$( '.mwp_wfc_showhide_plugin' ).on('click', function () {
		mainwp_wfc_plugin_showhide_start_specific( $( this ), false );
		return false;
	});

	$( '#wfc_plugin_doaction_btn' ).on('click', function () {
		var bulk_act = $( '#mwp_wfc_plugin_action' ).val();
		mainwp_wfc_plugin_do_bulk_action( bulk_act );

	});

	$( '.mwp_wfc_scan_now_lnk' ).on('click', function () {
		mainwp_wfc_scan_start_specific( $( this ), false );
		return false;
	});

	$( '.wfc_metabox_scan_now_lnk' ).on('click', function () {
		var statusEl = $( '#wfc_metabox_working_row' ).find( '.status' );
		var loader = $( '#wfc_metabox_working_row' ).find( '.loading' );
		var data = {
			action: 'mainwp_wfc_scan_now',
			siteId: $( this ).attr( 'site-id' )
		}
		loader.show();
		statusEl.hide();
		jQuery.post(ajaxurl, data, function (response) {
			loader.hide();
			if (response) {
				if (response['error']) {
					if (response['error'] == 'SCAN_RUNNING') {
						statusEl.css( 'color', 'red' );
						statusEl.html( __( "A scan is already running" ) ).show();
					} else {
						statusEl.css( 'color', 'red' );
						statusEl.html( response['error'] ).show();
					}
				} else if (response['result'] == 'SUCCESS') {
					statusEl.css( 'color', '#21759B' );
					statusEl.html( __( 'Requesting a New Scan' ) ).show();
					setTimeout(function () {
						statusEl.fadeOut();
					}, 3000);
				} else {
					statusEl.css( 'color', 'red' );
					statusEl.html( __( "Undefined error" ) ).show();
				}
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( __( "Undefined error" ) ).show();
			}
		}, 'json');
		return false;
	});

	$( '.mainwp_wfc_postbox .handlediv' ).on('click', function () {
		var pr = $( this ).parent();
		if (pr.hasClass( 'closed' )) {
			pr.removeClass( 'closed' ); } else {
			pr.addClass( 'closed' ); }
	});

	$( '#mainwp-wfc-run-scan' ).on('click', function () {
                if (!confirm('You are about to scan all Child Sites?'))
                    return false;                		
                var selector = '#the-mwp-wordfence-list tr.active';                                
                jQuery( selector ).each(function(){
                    jQuery(this).find('.check-column input[type="checkbox"]:checked').closest('tr').addClass( 'queue' );
                });
                
                if( jQuery( selector + '.queue:first' ).length == 0 ) {
                    alert('Please select websites to scan');
                }
                
                mainwp_wfc_scan_start_next( selector );
		return false;
	});
        
        $( '#mainwp-wfc-kill-scan' ).on('click', function () {
                if (!confirm('You are about to Stop the Scan Process on Child Sites?'))
                    return false;                
		var selector = '#the-mwp-wordfence-list tr.active';                                
                jQuery( selector ).each(function(){
                    jQuery(this).find('.check-column input[type="checkbox"]:checked').closest('tr').addClass( 'queue' );
                });       
                
                if( jQuery( selector + '.queue:first' ).length == 0 ) {
                    alert('Please select websites to Stop the Scan Process');
                }
                
		mainwp_wfc_kill_start_next(selector);
		return false;
	});

	$( '#mainwp-wfc-widget-run-scan' ).on('click', function () {
		return false;
	});
        
        $( '#wfc_btn_savegeneral' ).on('click', function () {  
                if (!confirm('Are you sure?'))
                    return false;
                
                var statusEl = $( '#wfc_save_settings_status' );               
                statusEl.css( 'color', '#21759B' );
		statusEl.html('<i class="fa fa-spinner fa-pulse"></i> Saving ...');                
		var data = {
			action: 'mainwp_wfc_save_general_settings_to_child',
			siteId: $('#wfc_individual_settings_site_id').val(),
                        nonce: mainwp_WordfenceAdminVars.nonce
		}
                var me = this;
                jQuery(me).attr('disabled', true);
		jQuery.post(ajaxurl, data, function (response) {	
                        jQuery(me).attr('disabled', false);                        
                        statusEl.html('');
			if (response) {
				if (response['error']) {                                     
                                    statusEl.html( '<span style="color:red">' + response['error'] + '</span>').show();					
				} else if (response['ok']) {                                    
                                    statusEl.html( __( 'Saved' ) ).show();
                                    setTimeout(function () {
                                            statusEl.fadeOut();
                                    }, 5000);
				} else {                                    
                                    statusEl.html( '<span style="color:red">' + __( "Undefined error" ) + '</span>').show();                                    
				}
			} else {                            
                            statusEl.html( '<span style="color:red">' + __( "Undefined error" ) + '</span>').show();
			}
		}, 'json');
		return false;
		
	});      
        
         $( '#mainwp_wfc_override_global_setting' ).on('change', function () {
                var statusEl = $( '.wfc_change_override_working' );
                statusEl.css( 'color', '#21759B' );
		statusEl.html('<i class="fa fa-spinner fa-pulse"></i> Saving ...');
                
		var data = {
			action: 'mainwp_wfc_change_override_general_settings',
			siteId: $('#wfc_individual_settings_site_id').val(),
                        override: $( this ).is(':checked') ? 1 : 0
		}		
		jQuery.post(ajaxurl, data, function (response) {	
                        statusEl.html('');
			if (response) {
				if (response['error']) {
                                     statusEl.css( 'color', 'red' );
                                    statusEl.html( response['error'] ).show();					
				} else if (response['ok']) {
                                    statusEl.css( 'color', '#21759B' );
                                    statusEl.html( __( 'Saved' ) ).show();
                                    setTimeout(function () {
                                            statusEl.fadeOut();
                                    }, 5000);
				} else {
                                    statusEl.css( 'color', 'red' );
                                    statusEl.html( __( "Undefined error" ) ).show();
				}
			} else {
                            statusEl.css( 'color', 'red' );
                            statusEl.html( __( "Undefined error" ) ).show();
			}
		}, 'json');
		return false;
		
	}); 
        
         $( '#mwp_wordfence_general_use_premium' ).on('change', function () {
                var statusEl = $( '#wfc_change_use_premium_working' );
                statusEl.css( 'color', '#21759B' );
		statusEl.html('<i class="fa fa-spinner fa-pulse"></i> Saving ...');                
		var data = {
			action: 'mainwp_wfc_change_general_settings_use_premium',			
                        value: $( this ).is(':checked') ? 1 : 0,
                        nonce: mainwp_WordfenceAdminVars.nonce
		}		
                
		jQuery.post(ajaxurl, data, function (response) {	
                        statusEl.html('');
			if (response) {
				if (response['error']) {
                                     statusEl.css( 'color', 'red' );
                                    statusEl.html( response['error'] ).show();					
				} else if (response['ok']) {
                                    statusEl.css( 'color', '#21759B' );
                                    statusEl.html( __( 'Saved' ) ).show();
                                    setTimeout(function () {
                                        statusEl.fadeOut();                                                                                        
                                        window.location.href = 'admin.php?page=Extensions-Mainwp-Wordfence-Extension&tab=network_setting';
                                    }, 1000);
                                    
				} else {
                                    statusEl.css( 'color', 'red' );
                                    statusEl.html( __( "Undefined error" ) ).show();
				}
			} else {
                            statusEl.css( 'color', 'red' );
                            statusEl.html( __( "Undefined error" ) ).show();
			}
		}, 'json');
		return false;
		
	});       
});

// this function call from code on child plugin 
function mainwp_wfc_get_donwnloadlink(site_id, logfile) {    
    var location = 'admin-ajax.php?&action=wordfence_downloadLogFile&_mwpNoneName=nonce&_mwpNoneValue=wp-ajax&logfile=' + encodeURIComponent(logfile);    
    return 'admin.php?page=Extensions-Mainwp-Wordfence-Extension&action=open_site&websiteid=' + site_id + '&open_location=' + MWP_WFAD.utf8_to_b64(location);
}




var wfc_bulkMaxThreads = 3;
var wfc_bulkTotalThreads = 0;
var wfc_bulkCurrentThreads = 0;
var wfc_bulkFinishedThreads = 0;

mainwp_wfc_plugin_do_bulk_action = function (act) {
	var selector = '';
	switch (act) {
		case 'activate-selected':
			selector = '#the-mwp-wordfence-list tr.plugin-update-tr .mwp_wfc_active_plugin';
			jQuery( selector ).addClass( 'queue' );
			mainwp_wfc_plugin_active_start_next( selector );
			break;
		case 'update-selected':
			selector = '#the-mwp-wordfence-list tr.plugin-update-tr .mwp_wfc_upgrade_plugin';
			jQuery( selector ).addClass( 'queue' );
			mainwp_wfc_plugin_upgrade_start_next( selector );
			break;
		case 'hide-selected':
			selector = '#the-mwp-wordfence-list tr .mwp_wfc_showhide_plugin[showhide="hide"]';
			jQuery( selector ).addClass( 'queue' );
			mainwp_wfc_plugin_showhide_start_next( selector );
			break;
		case 'show-selected':
			selector = '#the-mwp-wordfence-list tr .mwp_wfc_showhide_plugin[showhide="show"]';
			jQuery( selector ).addClass( 'queue' );
			mainwp_wfc_plugin_showhide_start_next( selector );
			break;
	}
}

mainwp_wfc_plugin_showhide_start_next = function (selector) {
	while ((objProcess = jQuery( selector + '.queue:first' )) && (objProcess.length > 0) && (wfc_bulkCurrentThreads < wfc_bulkMaxThreads)) {
		objProcess.removeClass( 'queue' );
		if (objProcess.closest( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length == 0) {
			continue;
		}
		mainwp_wfc_plugin_showhide_start_specific( objProcess, true, selector );
	}
}

mainwp_wfc_plugin_showhide_start_specific = function (pObj, bulk, selector) {
	var parent = pObj.closest( 'tr' );
	var loader = parent.find( '.wfc-action-working .loading' );
	var statusEl = parent.find( '.wfc-action-working .status' );
	var showhide = pObj.attr( 'showhide' );
	if (bulk) {
		wfc_bulkCurrentThreads++; }

	var data = {
		action: 'mainwp_wfc_showhide_plugin',
		websiteId: parent.attr( 'website-id' ),
		showhide: showhide
	}
	statusEl.hide();
	loader.show();
	jQuery.post(ajaxurl, data, function (response) {
		loader.hide();
		pObj.removeClass( 'queue' );
		if (response && response['error']) {
			statusEl.css( 'color', 'red' );
			statusEl.html( response['error'] ).show();
		} else if (response && response['result'] == 'SUCCESS') {
			if (showhide == 'show') {
				pObj.text( __( "Hide Wordfence Plugin" ) );
				pObj.attr( 'showhide', 'hide' );
				parent.find( '.wordfence_hidden_title' ).html( __( 'No' ) );
			} else {
				pObj.text( __( "Show Wordfence Plugin" ) );
				pObj.attr( 'showhide', 'show' );
				parent.find( '.wordfence_hidden_title' ).html( __( 'Yes' ) );
			}

			statusEl.css( 'color', '#21759B' );
			statusEl.html( __( 'Successful' ) ).show();
			statusEl.fadeOut( 3000 );
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( __( "Undefined error" ) ).show();
		}

		if (bulk) {
			wfc_bulkCurrentThreads--;
			wfc_bulkFinishedThreads++;
			mainwp_wfc_plugin_showhide_start_next( selector );
		}

	}, 'json');
	return false;
}

mainwp_wfc_plugin_upgrade_start_next = function (selector) {
	while ((objProcess = jQuery( selector + '.queue:first' )) && (objProcess.length > 0) && (objProcess.closest( 'tr' ).prev( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length > 0) && (wfc_bulkCurrentThreads < wfc_bulkMaxThreads)) {
		objProcess.removeClass( 'queue' );
		if (objProcess.closest( 'tr' ).prev( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length == 0) {
			continue;
		}
		mainwp_wfc_plugin_upgrade_start_specific( objProcess, true, selector );
	}
}

mainwp_wfc_plugin_upgrade_start_specific = function (pObj, bulk, selector) {
	var parent = pObj.closest( '.ext-upgrade-noti' );
	var workingRow = parent.find( '.mwp-wfc-row-working' );
	var slug = parent.attr( 'plugin-slug' );
	var data = {
		action: 'mainwp_wfc_upgrade_plugin',
		websiteId: parent.attr( 'website-id' ),
		type: 'plugin',
		'slugs[]': [slug]
	}

	if (bulk) {
		wfc_bulkCurrentThreads++; }

	workingRow.find( 'i' ).show();
	jQuery.post(ajaxurl, data, function (response) {
		workingRow.find( 'i' ).hide();
		pObj.removeClass( 'queue' );
		if (response && response['error']) {
			workingRow.find( '.status' ).html( '<font color="red">' + response['error'] + '</font>' );
		} else if (response && response['upgrades'][slug]) {
			pObj.after( 'Wordfence plugin has been updated' );
			pObj.remove();
		} else {
			workingRow.find( '.status' ).html( '<font color="red">' + __( "Undefined error" ) + '</font>' );
		}

		if (bulk) {
			wfc_bulkCurrentThreads--;
			wfc_bulkFinishedThreads++;
			mainwp_wfc_plugin_upgrade_start_next( selector );
		}

	}, 'json');
	return false;
}

mainwp_wfc_plugin_active_start_next = function (selector) {
	while ((objProcess = jQuery( selector + '.queue:first' )) && (objProcess.length > 0) && (objProcess.closest( 'tr' ).prev( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length > 0) && (wfc_bulkCurrentThreads < wfc_bulkMaxThreads)) {
		objProcess.removeClass( 'queue' );
		if (objProcess.closest( 'tr' ).prev( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length == 0) {
			continue;
		}
		mainwp_wfc_plugin_active_start_specific( objProcess, true, selector );
	}
}

mainwp_wfc_plugin_active_start_specific = function (pObj, bulk, selector) {
	var parent = pObj.closest( '.ext-upgrade-noti' );
	var workingRow = parent.find( '.mwp-wfc-row-working' );
	var slug = parent.attr( 'plugin-slug' );
	var data = {
		action: 'mainwp_wfc_active_plugin',
		websiteId: parent.attr( 'website-id' ),
		'plugins[]': [slug]
	}

	if (bulk) {
		wfc_bulkCurrentThreads++; }

	workingRow.find( 'i' ).show();
	jQuery.post(ajaxurl, data, function (response) {
		workingRow.find( 'i' ).hide();
		pObj.removeClass( 'queue' );
		if (response && response['error']) {
			workingRow.find( '.status' ).html( '<font color="red">' + response['error'] + '</font>' );
		} else if (response && response['result']) {
			pObj.after( 'Wordfence plugin has been activated' );
			pObj.remove();
		}
		if (bulk) {
			wfc_bulkCurrentThreads--;
			wfc_bulkFinishedThreads++;
			mainwp_wfc_plugin_active_start_next( selector );
		}

	}, 'json');
	return false;
}


jQuery( document ).ready(function ($) {
	jQuery( '.mainwp-show-tut' ).on('click', function () {
		jQuery( '.mainwp-wfc-tut' ).hide();
		var num = jQuery( this ).attr( 'number' );		
		jQuery( '.mainwp-wfc-tut[number="' + num + '"]' ).show();
		mainwp_setCookie( 'wordfence_quick_tut_number', jQuery( this ).attr( 'number' ) );
		return false;
	});

	jQuery( '#mainwp-wordfence-quick-start-guide' ).on('click', function () {
		if (mainwp_getCookie( 'wordfence_quick_guide' ) == 'on') {
			mainwp_setCookie( 'wordfence_quick_guide', '' ); } else {
			mainwp_setCookie( 'wordfence_quick_guide', 'on' ); }
			wordfence_showhide_quick_guide();
			return false;
	});
	jQuery( '#mainwp-wfc-tips-dismiss' ).on('click', function () {
		mainwp_setCookie( 'wordfence_quick_guide', '' );
		wordfence_showhide_quick_guide();
		return false;
	});

	wordfence_showhide_quick_guide();

	jQuery( '#mainwp-wordfence-dashboard-tips-dismiss' ).on('click', function () {
		$( this ).closest( '.mainwp_info-box-yellow' ).hide();
		mainwp_setCookie( 'wordfence_dashboard_notice', 'hide', 2 );
		return false;
	});

});

wordfence_showhide_quick_guide = function () {
	var show = mainwp_getCookie( 'wordfence_quick_guide' );
	if (show == 'on') {
		jQuery( '#mainwp-wfc-tips' ).show();
		jQuery( '#mainwp-wordfence-quick-start-guide' ).hide();
		wordfence_showhide_quick_tut();
	} else {
		jQuery( '#mainwp-wfc-tips' ).hide();
		jQuery( '#mainwp-wordfence-quick-start-guide' ).show();
	}

	if ('hide' == mainwp_getCookie( 'wordfence_dashboard_notice' )) {
		jQuery( '#mainwp-wordfence-dashboard-tips-dismiss' ).closest( '.mainwp_info-box-yellow' ).hide();
	}
}

wordfence_showhide_quick_tut = function () {
	var tut = mainwp_getCookie( 'wordfence_quick_tut_number' );
	jQuery( '.mainwp-wfc-tut' ).hide();
	jQuery( '.mainwp-wfc-tut[number="' + tut + '"]' ).show();
}

mainwp_wfc_scan_start_next = function (selector) {
	while ((objProcess = jQuery( selector + '.queue:first' )) && (objProcess.length > 0) && (wfc_bulkCurrentThreads < wfc_bulkMaxThreads)) {
		objProcess.removeClass( 'queue' );		
		mainwp_wfc_scan_start_specific( objProcess, true, selector );
	}
}

mainwp_wfc_scan_start_specific = function (pObj, bulk, selector) {
	var parent = pObj.closest( 'tr' );
	var statusEl = parent.find( '.wfc-scan-working .status' );
	var loader = parent.find( '.wfc-scan-working i' );
	var data = {
		action: 'mainwp_wfc_scan_now',
		siteId: parent.attr( 'website-id' )
	}

	if (bulk) {
            wfc_bulkCurrentThreads++; 
        }
        statusEl.css( 'color', '#21759B' );
	loader.show();
	statusEl.html('Running ...').show();
	jQuery.post(ajaxurl, data, function (response) {
		loader.hide();
                statusEl.hide();
                
		if (response) {
			if (response['error']) {
				if (response['error'] == 'SCAN_RUNNING') {
					statusEl.css( 'color', 'red' );
					statusEl.html( __( "A scan is already running" ) ).show();
				} else {
					statusEl.css( 'color', 'red' );
					statusEl.html( response['error'] ).show();
				}
			} else if (response['result'] == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( __( 'Requesting a New Scan' ) ).show();
				setTimeout(function () {
					statusEl.fadeOut();
				}, 3000);
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( __( "Undefined error" ) ).show();
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( __( "Undefined error" ) ).show();
		}

		if (bulk) {
			wfc_bulkCurrentThreads--;
			wfc_bulkFinishedThreads++;
			mainwp_wfc_scan_start_next( selector );
		}

	}, 'json');
	return false;
}



mainwp_wfc_kill_start_next = function (selector) {    
    while ((objProcess = jQuery( selector + '.queue:first' )) && (objProcess.length > 0) && (wfc_bulkCurrentThreads < wfc_bulkMaxThreads)) {
        objProcess.removeClass( 'queue' );
        mainwp_wfc_kill_start_specific( objProcess, true, selector );
    }
}

mainwp_wfc_kill_start_specific = function (pObj, bulk, selector) {
	//var parent = pObj.closest( 'tr' );
        
	var statusEl = pObj.find( '.wfc-scan-working .status' );
	var loader = pObj.find( '.wfc-scan-working i' );
        console.log(loader);
	var data = {
            action: 'mainwp_wfc_kill_scan_now',
            siteId: pObj.attr( 'website-id' )
	}

	if (bulk) {
            wfc_bulkCurrentThreads++; 
        }

        statusEl.css( 'color', '#21759B' );
	loader.show();        
	statusEl.html('Stopping the Scan process...').show();
	jQuery.post(ajaxurl, data, function (response) {
		loader.hide();
                statusEl.hide();
                
		if (response) {
			if (response['error']) {				
                            statusEl.css( 'color', 'red' );
                            statusEl.html( response['error'] ).show();				
			} else if (response['ok']) {
                            statusEl.css( 'color', '#21759B' );
                            statusEl.html( __( 'Stop the Scan process requested' ) ).show();
                            setTimeout(function () {
                                    statusEl.fadeOut();
                            }, 3000);
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( __( "Undefined error" ) ).show();
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( __( "Undefined error" ) ).show();
		}

		if (bulk) {
			wfc_bulkCurrentThreads--;
			wfc_bulkFinishedThreads++;
			mainwp_wfc_kill_start_next( selector );
		}

	}, 'json');
	return false;
}


mainwp_wfc_save_setting_start_next = function () {
    if (wfc_bulkTotalThreads == 0) {
        wfc_bulkTotalThreads = jQuery( '.itemToProcess[status="queue"]' ).length;
    }
    while ((itemProcess = jQuery( '.itemToProcess[status="queue"]:first' )) && (itemProcess.length > 0) && (wfc_bulkCurrentThreads < wfc_bulkMaxThreads)) {
        mainwp_wfc_save_setting_start_specific( itemProcess );
    }
        
    if (wfc_bulkFinishedThreads == wfc_bulkTotalThreads && wfc_bulkFinishedThreads != 0) {
        var _section = jQuery('#_post_saving_section').val();
        var _tab = 'network_setting';
        switch(_section) {             
            case 'firewall':
                _tab = 'network_firewall';
                break;
            case 'blocking':
                _tab = 'network_blocking';
                break;
            case 'scanner':
                _tab = 'network_scan';
                break;
            case 'livetraffic':
                _tab = 'network_traffic';
                break;                
            case 'diagnostics':
                _tab = 'diagnostics';
                break;
        }
        jQuery( '#mainwp_wfc_perform_bulk_actions_ajax_message' ).html( 'Saved Settings to child sites.' ).fadeIn( 100 );
        jQuery( '#mainwp_wfc_perform_bulk_actions_ajax_message' ).after('<p><a class="button-primary" href="admin.php?page=Extensions-Mainwp-Wordfence-Extension&tab=' + _tab + '">Return to Settings</a></p>');
    }
};

mainwp_wfc_bulk_import_start_next = function ( pToken ) {
    if (wfc_bulkTotalThreads == 0) {
        wfc_bulkTotalThreads = jQuery( '.itemToProcess[status="queue"]' ).length;
    }
    while ((itemProcess = jQuery( '.itemToProcess[status="queue"]:first' )) && (itemProcess.length > 0) && (wfc_bulkCurrentThreads < wfc_bulkMaxThreads)) {
            mainwp_wfc_bulk_import_start_specific( itemProcess, pToken );
    }
    if (wfc_bulkFinishedThreads == wfc_bulkTotalThreads && wfc_bulkFinishedThreads != 0) {
        jQuery( '#mainwp_wfc_perform_bulk_actions_ajax_message' ).html( 'Imported Settings to child sites.' ).fadeIn( 100 );
        jQuery( '#mainwp_wfc_perform_bulk_actions_ajax_message').after('<p><a class="button-primary" href="admin.php?page=Extensions-Mainwp-Wordfence-Extension&tab=network_setting">Return to Settings</a></p>');
    }
};

mainwp_wfc_bulk_import_start_specific = function (pItemProcess, pToken) {
	wfc_bulkCurrentThreads++;
	pItemProcess.attr( 'status', 'progress' );
	var statusEl = pItemProcess.find( '.status' ).html( 'Running ...' );	
        var loaderEl = pItemProcess.find( '.loading' );

	var data = {
		action: 'mainwp_wfc_importSettings',
        token: pToken,
        site_id: pItemProcess.attr( 'siteid' ),
        bulk_import: 1,
        nonce: mainwp_WordfenceAdminVars.firstNonce
	};
	loaderEl.show();
    jQuery.post(ajaxurl, data, function (response) {
		loaderEl.hide();
		pItemProcess.attr( 'status', 'done' );
		if (response) {
			if (response['result'] == 'OVERRIDED') {
				statusEl.html( 'Not Updated - Individual site settings are in use' ).show();
				statusEl.css( 'color', 'red' );
			} else {
				if (response.ok) {
					statusEl.html( 'You successfully imported ' + response.totalSet + ' options.' ).show();
				} else if (response.errorImport) {
					statusEl.html( 'Error during Import ' + response.errorImport ).show();
					statusEl.css( 'color', 'red' );
				} else {
					statusEl.html( 'An unknown error occurred during the import').show();
					statusEl.css( 'color', 'red' );
				}
			}
		} else {
			statusEl.html( __( 'Undefined Error' ) ).show();
			statusEl.css( 'color', 'red' );
		}
		wfc_bulkCurrentThreads--;
		wfc_bulkFinishedThreads++;		
		mainwp_wfc_bulk_import_start_next(pToken);
	}, 'json');
};

// saving after save general settings
mainwp_wfc_save_setting_start_specific = function (pItemProcess) {
	wfc_bulkCurrentThreads++;
	pItemProcess.attr( 'status', 'progress' );
	var statusEl = pItemProcess.find( '.status' ).html( 'Running ...' );
	var loaderEl = pItemProcess.find( '.loading' );
	var detailedEl = pItemProcess.find( '.detailed' );

	var data = {
		action: 'mainwp_wfc_save_settings',
		siteId: pItemProcess.attr( 'siteid' ),
                _ajax_saving_section: jQuery('#_post_saving_section').val()
	};
	loaderEl.show();
	jQuery.post(ajaxurl, data, function (response) {
		loaderEl.hide();
		pItemProcess.attr( 'status', 'done' );
		var delay = 3000;
		var detail = '';
		if (response) {
			if (response['result'] == 'OVERRIDED') {
				statusEl.html( 'Not Updated - Individual site settings are in use' ).show();
				statusEl.css( 'color', 'red' );
			} else {
				if (response.ok) {
					if (response['paidKeyMsg']) {
						delay = 9000;
						statusEl.html( 'Congratulations! You have been upgraded to Premium Scanning. You have upgraded to a Premium API key. Once this page reloads, you can choose which premium scanning options you would like to enable and then click save.' ).show();
					} else {
						statusEl.html( 'Successful' ).show();
					}
				} else if (response['error']) {
					statusEl.html( response['error'] ).show();
					statusEl.css( 'color', 'red' );
				} else {
					statusEl.html( __( 'Undefined Error' ) ).show();
					statusEl.css( 'color', 'red' );
				}
				if (response['invalid_users']) {
					delay = 9000;
					detail += __( "The following users you selected to ignore in live traffic reports are not valid on the child site: " ) + response['invalid_users'];
				}
				if (detail !== '') {
					detailedEl.css( 'color', 'red' );
					detailedEl.html( detail ).show();
				}
			}
		} else {
			statusEl.html( __( 'Undefined Error' ) ).show();
			statusEl.css( 'color', 'red' );
		}

		wfc_bulkCurrentThreads--;
		wfc_bulkFinishedThreads++;		
		mainwp_wfc_save_setting_start_next();
	}, 'json');
};

// saving after save individual settings
mainwp_wfc_save_site_settings = function (site_id) {
	var process = jQuery( '#mwp_wfc_ajax_message' );
	var statusEl = process.find( '.status' );
	var loaderEl = process.find( '.loading' );
	var detailedEl = process.find( '.detailed' );

	var data = {
		action: 'mainwp_wfc_save_settings',
		siteId: site_id,
		individual: 1,
                _ajax_saving_section: jQuery('#_post_saving_section').val()
	};
	loaderEl.show();
	jQuery.post(ajaxurl, data, function (response) {
		loaderEl.hide();
		if (response) {
			if (response['result'] == 'OVERRIDED') {
				statusEl.html( 'Not Updated - Individual site settings are in use' ).show();
				statusEl.css( 'color', 'red' );
			} else {
				var detail = '';
				if (response.ok) {
					if (response['paidKeyMsg']) {
						delay = 9000;
						statusEl.html( 'Congratulations! You have been upgraded to Premium Scanning. You have upgraded to a Premium API key. Once this page reloads, you can choose which premium scanning options you would like to enable and then click save.' ).show();
					} else {
						statusEl.html( 'Successful' ).show();
					}
					if (response['reload'] == 'reload') {
						mainwp_wfc_save_site_settings_reload( site_id );
					}
				} else if (response['error']) {
					statusEl.html( response['error'] ).show();
					statusEl.css( 'color', 'red' );
				} else {
					statusEl.html( __( 'Undefined Error' ) ).show();
					statusEl.css( 'color', 'red' );
				}
				if (response['invalid_users']) {
					delay = 9000;
					detail += __( "The following users you selected to ignore in live traffic reports are not valid on the child site: " ) + response['invalid_users'];
				}
				if (detail !== '') {
					detailedEl.css( 'color', 'red' );
					detailedEl.html( detail ).show();
				}
			}
		} else {
			statusEl.html( __( 'Undefined Error' ) ).show();
			statusEl.css( 'color', 'red' );
		}
	}, 'json');
};

mainwp_wfc_save_site_settings_reload = function (site_id) {
	var data = {
		action: 'mainwp_wfc_save_settings_reload',
		siteId: site_id
	};
	var reload = jQuery( '#mwp_wfc_license_body' );
	reload.html( '<i class="fa fa-spinner fa-pulse"></i> ' + __( 'Reloading ...' ) );
	jQuery.post(ajaxurl, data, function (response) {
		reload.html( response );
	})
}

mainwp_wfc_bulk_performance_setup_start_next = function (what) {
    if (wfc_bulkTotalThreads == 0) {
            wfc_bulkTotalThreads = jQuery( '.itemToProcess[status="queue"]' ).length;
    }
    while ((itemProcess = jQuery( '.itemToProcess[status="queue"]:first' )) && (itemProcess.length > 0) && (wfc_bulkCurrentThreads < wfc_bulkMaxThreads)) {
            switch(what) {
                case 'save_caching_type':
                    mainwp_wfc_bulk_savecachingtype_start_specific( itemProcess, what);
                    break;                
                case 'save_cache_options':
                    mainwp_wfc_bulk_savecacheoptions_start_specific( itemProcess, what);
                    break;                
                case 'clear_page_cache':
                    mainwp_wfc_bulk_clearpagecache_start_specific( itemProcess, what);
                    break;                
                case 'get_cache_stats':
                    mainwp_wfc_bulk_getcachestats_start_specific( itemProcess, what);
                    break;                
                case 'add_cache_exclusion':
                    mainwp_wfc_bulk_addcacheexclusion_start_specific( itemProcess, what);
                    break;                            
                case 'remove_cache_exclusion':
                    mainwp_wfc_bulk_removecacheexclusion_start_specific( itemProcess, what);
                    break;                            
            } 
    }
    if (wfc_bulkFinishedThreads == wfc_bulkTotalThreads && wfc_bulkFinishedThreads != 0) {
        jQuery( '#mainwp_wfc_perform_bulk_actions_ajax_message' ).html( 'Finished' ).fadeIn( 100 );
        jQuery( '#mainwp_wfc_perform_bulk_actions_ajax_message').after('<p><a class="button-primary" href="admin.php?page=Extensions-Mainwp-Wordfence-Extension&tab=performance">Return to Performance Setup</a></p>');
    }
}

mainwp_wfc_bulk_savecachingtype_start_specific = function (pItemProcess, pWhat) {
	wfc_bulkCurrentThreads++;
	pItemProcess.attr( 'status', 'progress' );
	var statusEl = pItemProcess.find( '.status' ).html( 'Running ...' );	var loaderEl = pItemProcess.find( '.loading' );
	
        var data = {
		action: 'mainwp_wfc_saveCacheConfig',                
                site_id: pItemProcess.attr( 'siteid' ),                 
                individual: 0,
                nonce: mainwp_WordfenceAdminVars.firstNonce
	};
        
	loaderEl.show();
        
        jQuery.post(ajaxurl, data, function (response) {
                loaderEl.hide();
                pItemProcess.attr( 'status', 'done' );
                if (response) {
                        if (response['result'] == 'OVERRIDED') {
                                statusEl.html( 'Not Updated - Individual site settings are in use' ).show();
                                statusEl.css( 'color', 'red' );
                        } else {
                                if (response.ok) {                                    
                                        if (response.heading)
                                            statusEl.html( response.heading ).show();
                                        else
                                            statusEl.html( 'Successful' ).show();
                                } else if (response.error) {
                                        statusEl.html( response.error ).show();
                                        statusEl.css( 'color', 'red' );                                        
                                }  else if (response.errorMsg) {
                                        statusEl.html( response.errorMsg ).show();
                                        statusEl.css( 'color', 'red' );                                        
                                } else {
                                        statusEl.html( 'An unknown error').show();
                                        statusEl.css( 'color', 'red' );
                                }
                        }
                } else {
                        statusEl.html( __( 'Undefined Error' ) ).show();
                        statusEl.css( 'color', 'red' );
                }
                wfc_bulkCurrentThreads--;
                wfc_bulkFinishedThreads++;                
                mainwp_wfc_bulk_performance_setup_start_next(pWhat);
        }, 'json');
};


mainwp_wfc_bulk_savecacheoptions_start_specific = function (pItemProcess, pWhat) {
	wfc_bulkCurrentThreads++;
	pItemProcess.attr( 'status', 'progress' );
	var statusEl = pItemProcess.find( '.status' ).html( 'Running ...' );	var loaderEl = pItemProcess.find( '.loading' );
	
        var data = {
		action: 'mainwp_wfc_saveCacheOptions',                
                site_id: pItemProcess.attr( 'siteid' ),                 
                individual: 0,
                nonce: mainwp_WordfenceAdminVars.firstNonce
	};
        
	loaderEl.show();
        
        jQuery.post(ajaxurl, data, function (response) {
                loaderEl.hide();
                pItemProcess.attr( 'status', 'done' );
                if (response) {
                        if (response['result'] == 'OVERRIDED') {
                                statusEl.html( 'Not Updated - Individual site settings are in use' ).show();
                                statusEl.css( 'color', 'red' );
                        } else {
                                if (response.ok) {                                    
                                        statusEl.html( 'Successful' ).show();
                                } else if (response.error) {
                                        statusEl.html( response.error ).show();
                                        statusEl.css( 'color', 'red' );                                        
                                }  else if (response.errorMsg) {
                                        statusEl.html( response.errorMsg ).show();
                                        statusEl.css( 'color', 'red' );                                        
                                } else {
                                        statusEl.html( 'An unknown error').show();
                                        statusEl.css( 'color', 'red' );
                                }
                        }
                } else {
                        statusEl.html( __( 'Undefined Error' ) ).show();
                        statusEl.css( 'color', 'red' );
                }
                wfc_bulkCurrentThreads--;
                wfc_bulkFinishedThreads++;                
                mainwp_wfc_bulk_performance_setup_start_next(pWhat);
        }, 'json');
};


mainwp_wfc_bulk_clearpagecache_start_specific = function (pItemProcess, pWhat) {
	wfc_bulkCurrentThreads++;
	pItemProcess.attr( 'status', 'progress' );
	var statusEl = pItemProcess.find( '.status' ).html( 'Running ...' );	var loaderEl = pItemProcess.find( '.loading' );
	
        var data = {
		action: 'mainwp_wfc_clearPageCache',                
                site_id: pItemProcess.attr( 'siteid' ),                                 
                nonce: mainwp_WordfenceAdminVars.firstNonce
	};
        
	loaderEl.show();
        
        jQuery.post(ajaxurl, data, function (response) {
                loaderEl.hide();
                pItemProcess.attr( 'status', 'done' );
                if (response) {
                        if (response['result'] == 'OVERRIDED') {
                                statusEl.html( 'Not Updated - Individual site settings are in use' ).show();
                                statusEl.css( 'color', 'red' );
                        } else {
                                if (response.ok) {  
                                        var msg = '';
                                        if (response.heading) {
                                            msg = response.heading + '. ';
                                        }
                                        if (response.body) {
                                            msg += response.body;
                                        }
                                        if (msg == '')
                                            msg = 'Successful';
                                        statusEl.html( msg ).show();
                                } else if (response.error) {
                                        statusEl.html( response.error ).show();
                                        statusEl.css( 'color', 'red' );                                        
                                }  else if (response.errorMsg) {
                                        statusEl.html( response.errorMsg ).show();
                                        statusEl.css( 'color', 'red' );                                        
                                } else {
                                        statusEl.html( 'An unknown error').show();
                                        statusEl.css( 'color', 'red' );
                                }
                        }
                } else {
                        statusEl.html( __( 'Undefined Error' ) ).show();
                        statusEl.css( 'color', 'red' );
                }
                wfc_bulkCurrentThreads--;
                wfc_bulkFinishedThreads++;                
                mainwp_wfc_bulk_performance_setup_start_next(pWhat);
        }, 'json');
};

mainwp_wfc_bulk_getcachestats_start_specific = function (pItemProcess, pWhat) {
	wfc_bulkCurrentThreads++;
	pItemProcess.attr( 'status', 'progress' );
	var statusEl = pItemProcess.find( '.status' ).html( 'Running ...' );	var loaderEl = pItemProcess.find( '.loading' );
	
        var data = {
		action: 'mainwp_wfc_getCacheStats',                
                site_id: pItemProcess.attr( 'siteid' ),                                 
                nonce: mainwp_WordfenceAdminVars.firstNonce
	};
        
	loaderEl.show();
        
        jQuery.post(ajaxurl, data, function (response) {
                loaderEl.hide();
                pItemProcess.attr( 'status', 'done' );
                if (response) {
                        if (response['result'] == 'OVERRIDED') {
                                statusEl.html( 'Not Updated - Individual site settings are in use' ).show();
                                statusEl.css( 'color', 'red' );
                        } else {
                                if (response.ok) {  
                                        var msg = '';
                                        if (response.heading) {
                                            msg = response.heading + '. ';
                                        }
                                        if (response.body) {
                                            msg += response.body;
                                        }
                                        if (msg == '')
                                            msg = 'Successful';
                                        statusEl.html( msg ).show();
                                } else if (response.error) {
                                        statusEl.html( response.error ).show();
                                        statusEl.css( 'color', 'red' );                                        
                                }  else if (response.errorMsg) {
                                        statusEl.html( response.errorMsg ).show();
                                        statusEl.css( 'color', 'red' );                                        
                                } else {
                                        statusEl.html( 'An unknown error').show();
                                        statusEl.css( 'color', 'red' );
                                }
                        }
                } else {
                        statusEl.html( __( 'Undefined Error' ) ).show();
                        statusEl.css( 'color', 'red' );
                }
                wfc_bulkCurrentThreads--;
                wfc_bulkFinishedThreads++;                
                mainwp_wfc_bulk_performance_setup_start_next(pWhat);
        }, 'json');
};

mainwp_wfc_bulk_addcacheexclusion_start_specific = function (pItemProcess, pWhat) {
	wfc_bulkCurrentThreads++;
	pItemProcess.attr( 'status', 'progress' );
	var statusEl = pItemProcess.find( '.status' ).html( 'Running ...' );	var loaderEl = pItemProcess.find( '.loading' );
	
        var data = {
		action: 'mainwp_wfc_addCacheExclusion',                
                site_id: pItemProcess.attr( 'siteid' ),
                id: jQuery('#mainwp_wfc_bulk_cache_exclusion_id').attr('value'),
                individual: 0,                               
                nonce: mainwp_WordfenceAdminVars.firstNonce
	};
        
	loaderEl.show();
        
        jQuery.post(ajaxurl, data, function (response) {
                loaderEl.hide();
                pItemProcess.attr( 'status', 'done' );
                if (response) {
                        if (response['result'] == 'OVERRIDED') {
                                statusEl.html( 'Not Updated - Individual site settings are in use' ).show();
                                statusEl.css( 'color', 'red' );
                        } else {
                                if (response.ok) {  
                                        statusEl.html( 'Successful' ).show();
                                } else if (response.error) {
                                        statusEl.html( response.error ).show();
                                        statusEl.css( 'color', 'red' );                                        
                                }  else if (response.errorMsg) {
                                        statusEl.html( response.errorMsg ).show();
                                        statusEl.css( 'color', 'red' );                                        
                                } else {
                                        statusEl.html( 'An unknown error').show();
                                        statusEl.css( 'color', 'red' );
                                }
                        }
                } else {
                        statusEl.html( __( 'Undefined Error' ) ).show();
                        statusEl.css( 'color', 'red' );
                }
                wfc_bulkCurrentThreads--;
                wfc_bulkFinishedThreads++;                
                mainwp_wfc_bulk_performance_setup_start_next(pWhat);
        }, 'json');
};

mainwp_wfc_bulk_removecacheexclusion_start_specific = function (pItemProcess, pWhat) {
	wfc_bulkCurrentThreads++;
	pItemProcess.attr( 'status', 'progress' );
	var statusEl = pItemProcess.find( '.status' ).html( 'Running ...' );	var loaderEl = pItemProcess.find( '.loading' );
	
        var data = {
		action: 'mainwp_wfc_removeCacheExclusion',                
                site_id: pItemProcess.attr( 'siteid' ),
                id: jQuery('#mainwp_wfc_bulk_cache_exclusion_id').attr('value'),
                individual: 0,                               
                nonce: mainwp_WordfenceAdminVars.firstNonce
	};
        
	loaderEl.show();
        
        jQuery.post(ajaxurl, data, function (response) {
                loaderEl.hide();
                pItemProcess.attr( 'status', 'done' );
                if (response) {
                        if (response['result'] == 'OVERRIDED') {
                                statusEl.html( 'Not Updated - Individual site settings are in use' ).show();
                                statusEl.css( 'color', 'red' );
                        } else {
                                if (response.ok) {                                          
                                        statusEl.html( 'Successful' ).show();
                                } else if (response.error) {
                                        statusEl.html( response.error ).show();
                                        statusEl.css( 'color', 'red' );                                        
                                }  else if (response.errorMsg) {
                                        statusEl.html( response.errorMsg ).show();
                                        statusEl.css( 'color', 'red' );                                        
                                } else {
                                        statusEl.html( 'An unknown error').show();
                                        statusEl.css( 'color', 'red' );
                                }
                        }
                } else {
                        statusEl.html( __( 'Undefined Error' ) ).show();
                        statusEl.css( 'color', 'red' );
                }
                wfc_bulkCurrentThreads--;
                wfc_bulkFinishedThreads++;                
                mainwp_wfc_bulk_performance_setup_start_next(pWhat);
        }, 'json');
};

jQuery(document).on('change', '#mainwp_wfc_diagnostic_info', function()
{
    var siteId = jQuery(this).val();    
    jQuery('#mwp_wfc_other_test_box').hide();
    if (siteId == '-1')
    {
        jQuery('#mainwp_diagnostics_child_resp').hide();        
        return;
    }
    jQuery('#mainwp_wfc_diagnostics_child_loading').show();
    jQuery('#mainwp_diagnostics_child_resp').hide();   
    jQuery('#mwp_wfc_other_test_box').hide();
    MWP_WFAD.getDiagnostics(siteId);    
});

mainwp_wfc_bulk_diagnostics_start_next = function (what) {
    if (wfc_bulkTotalThreads == 0) {
        wfc_bulkTotalThreads = jQuery( '.itemToProcess[status="queue"]' ).length;
    }
    while ((itemProcess = jQuery( '.itemToProcess[status="queue"]:first' )) && (itemProcess.length > 0) && (wfc_bulkCurrentThreads < wfc_bulkMaxThreads)) {
        switch(what) {
            case 'waf_update_rules':
                mainwp_wfc_bulk_wafupdaterules_start_specific( itemProcess, what);
                break;                
            case 'save_debugging_options':
                mainwp_wfc_bulk_savedebuggingoptions_start_specific( itemProcess, what);
                break;                                                       
        } 
    }
    if (wfc_bulkFinishedThreads == wfc_bulkTotalThreads && wfc_bulkFinishedThreads != 0) {
        jQuery( '#mainwp_wfc_perform_bulk_actions_ajax_message' ).html( 'Finished' ).fadeIn( 100 );
        jQuery( '#mainwp_wfc_perform_bulk_actions_ajax_message').after('<p><a class="button-primary" href="admin.php?page=Extensions-Mainwp-Wordfence-Extension&tab=network_firewall">Return to Firewall</a></p>');
    }
}

mainwp_wfc_bulk_wafupdaterules_start_specific = function (pItemProcess, pWhat) {
	wfc_bulkCurrentThreads++;
	pItemProcess.attr( 'status', 'progress' );
	var statusEl = pItemProcess.find( '.status' ).html( 'Running ...' );	
        var loaderEl = pItemProcess.find( '.loading' );
	
        var data = {
		action: 'mainwp_wfc_updateWAFRules',                
                site_id: pItemProcess.attr( 'siteid' ),                
                individual: 0,                               
                nonce: mainwp_WordfenceAdminVars.firstNonce
	};
        
	loaderEl.show();
        
        jQuery.post(ajaxurl, data, function (response) {
                loaderEl.hide();
                pItemProcess.attr( 'status', 'done' );
                if (response) {
                        if (response['result'] == 'OVERRIDED') {
                                statusEl.html( 'Not Updated - Individual site settings are in use' ).show();
                                statusEl.css( 'color', 'red' );
                        } else {
                                if (response.ok) {       
                                    if (!response.isPaid)
                                        statusEl.html( 'Your rules have been updated successfully. You are currently using the the free version of Wordfence.' ).show();
                                    else 
                                        statusEl.html( 'Your rules have been updated successfully.' ).show();
                                } else if (response.error) {
                                        statusEl.html( response.error ).show();
                                        statusEl.css( 'color', 'red' );                                        
                                } else {
                                        statusEl.html( 'An unknown error').show();
                                        statusEl.css( 'color', 'red' );
                                }
                        }
                } else {
                        statusEl.html( __( 'Undefined Error' ) ).show();
                        statusEl.css( 'color', 'red' );
                }
                wfc_bulkCurrentThreads--;
                wfc_bulkFinishedThreads++;                
                mainwp_wfc_bulk_diagnostics_start_next(pWhat);
        }, 'json');
};

mainwp_wfc_bulk_savedebuggingoptions_start_specific = function (pItemProcess, pWhat) {
	wfc_bulkCurrentThreads++;
	pItemProcess.attr( 'status', 'progress' );
	var statusEl = pItemProcess.find( '.status' ).html( 'Running ...' );	
        var loaderEl = pItemProcess.find( '.loading' );
	
        var data = {
		action: 'mainwp_wfc_saveDebuggingSettingsToSite',                
                site_id: pItemProcess.attr( 'siteid' ),                
                individual: 0,                               
                nonce: mainwp_WordfenceAdminVars.firstNonce
	};
        
	loaderEl.show();
        
        jQuery.post(ajaxurl, data, function (response) {
                loaderEl.hide();
                pItemProcess.attr( 'status', 'done' );
                if (response) {
                        if (response['result'] == 'OVERRIDED') {
                                statusEl.html( 'Not Updated - Individual site settings are in use' ).show();
                                statusEl.css( 'color', 'red' );
                        } else {
                                if (response.ok) {       
                                    statusEl.html( 'Successful' ).show();
                                } else if (response.error) {
                                        statusEl.html( response.error ).show();
                                        statusEl.css( 'color', 'red' );                                        
                                } else {
                                        statusEl.html( 'Undefined Error').show();
                                        statusEl.css( 'color', 'red' );
                                }
                        }
                } else {
                        statusEl.html( __( 'Undefined Error' ) ).show();
                        statusEl.css( 'color', 'red' );
                }
                wfc_bulkCurrentThreads--;
                wfc_bulkFinishedThreads++;                
                mainwp_wfc_bulk_diagnostics_start_next(pWhat);
        }, 'json');
};


mainwp_wfc_save_firewall_start_next = function () {
    if (wfc_bulkTotalThreads == 0) {
        wfc_bulkTotalThreads = jQuery( '.itemToProcess[status="queue"]' ).length;
    }
    while ((itemProcess = jQuery( '.itemToProcess[status="queue"]:first' )) && (itemProcess.length > 0) && (wfc_bulkCurrentThreads < wfc_bulkMaxThreads)) {
        mainwp_wfc_save_firewall_start_specific( itemProcess );
    }
    if (wfc_bulkFinishedThreads == wfc_bulkTotalThreads && wfc_bulkFinishedThreads != 0) {
        jQuery( '#mainwp_wfc_perform_bulk_actions_ajax_message' ).html( 'Saved Settings to child sites.' ).fadeIn( 100 );
        jQuery( '#mainwp_wfc_perform_bulk_actions_ajax_message' ).after('<p><a class="button-primary" href="admin.php?page=Extensions-Mainwp-Wordfence-Extension&tab=network_firewall">Return to Firewall Settings</a></p>');
    }
};


mainwp_wfc_save_firewall_start_specific = function (pItemProcess) {
	wfc_bulkCurrentThreads++;
	pItemProcess.attr( 'status', 'progress' );
	var statusEl = pItemProcess.find( '.status' ).html( 'Running ...' );
	var loaderEl = pItemProcess.find( '.loading' );
	
	var data = {
		action: 'mainwp_wfc_save_firewall_settings',
		siteId: pItemProcess.attr( 'siteid' ),
                nonce: mainwp_WordfenceAdminVars.nonce
	};
	loaderEl.show();
	jQuery.post(ajaxurl, data, function (res) {              
		loaderEl.hide();
		pItemProcess.attr( 'status', 'done' );		
		if (res) {                    
                    if ( res.success) {
                        statusEl.html( 'The Wordfence Web Application Firewall configuration was saved successfully.' ).show();                                                                     
                    } else if ( res.error) {
                        statusEl.html( res.error ).show();
                        statusEl.css( 'color', 'red' );                                
                    } else if ( res.errorMsg) {
                        statusEl.html( res.errorMsg ).show();
                        statusEl.css( 'color', 'red' );                                
                    } else {
                        statusEl.html( 'There was an error saving the Web Application Firewall configuration settings.' ).show();
                        statusEl.css( 'color', 'red' );                                
                    }
		} else {
                    statusEl.html( __( 'Undefined Error' ) ).show();
                    statusEl.css( 'color', 'red' );
		}
		wfc_bulkCurrentThreads--;
		wfc_bulkFinishedThreads++;		
		mainwp_wfc_save_firewall_start_next();
	}, 'json');
};
