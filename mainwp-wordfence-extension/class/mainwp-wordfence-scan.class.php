<?php

class MainWP_Wordfence_Scan {
     
    
    public static function gen_scans_scheduling(  $post, $metabox = null ) 
    {
        $current_site_id = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : 0; 
        $w = isset($metabox['args']['w']) ? $metabox['args']['w'] : null;
        if (empty($w))
            return;
        
        if ($current_site_id) {
            $is_Paid = MainWP_Wordfence_Config_Site::get( 'isPaid', 0, $current_site_id );            
        } else {
            $is_Paid = get_option('mainwp_wordfence_use_premium_general_settings');
        }
        
        
        $schedEnabled = $w->get('scheduledScansEnabled', true);  
        $schedMode = $w->get('schedMode', 'auto');  
        
        ?>
        <table class="mwp-wf-form-table">
            <tbody>   
                <tr>
                           <th class="wfConfigEnable">Schedule Wordfence Scans</th>
                           <td><input type="checkbox" id="scheduledScansEnabled" class="wfConfigElem"
                                      name="scheduledScansEnabled" onclick="onclick_scheduledScansEnabled(this);"
                                                                  value="1" <?php $w->cb( 'scheduledScansEnabled' ); ?> />
                           </td>
                       </tr>  
                  <tr>
                        <th class="wfConfigEnable"><label class="wf-control-label" for="wf-scheduling-mode-automatic">Let Wordfence choose when to scan my site (recommended)</label></th>     
                        <td>
<input type="radio" name="schedMode" id="wf-scheduling-mode-automatic" value="auto" <?php echo ($schedMode == 'auto' || !$is_Paid) ? 'checked' : ''; ?> <?php echo !$schedEnabled ? 'disabled' : ''; ?>> 
                        </td>
                    </tr>  
                    <tr>
                        <th class="wfConfigEnable"><label class="wf-control-label" for="wf-scheduling-mode-manually">Manually schedule scans</label> <span style="color: #F00;">Premium Feature</span></th>     
                        <td>
                            <input type="radio" name="schedMode" id="wf-scheduling-mode-manually" value="manual"  <?php echo ($schedMode == 'manual' && $is_Paid) ? 'checked' : ''; ?> <?php echo !$schedEnabled ? 'disabled' : ''; ?> <?php if ( ! $is_Paid ) {  ?>onclick="alert('This is a paid feature because it places significant additional load on our servers.'); return false;" <?php } ?>>
                        </td>
                    </tr>  
            </tbody>
        </table>
                
        <script type="text/javascript">		
            function onclick_scheduledScansEnabled(me) {
                if (jQuery(me).is(":checked")){                    
                    jQuery('#wf-scheduling-mode-automatic').removeAttr('disabled');
                    jQuery('#wf-scheduling-mode-manually').removeAttr('disabled');                    
                } else {                    
                    jQuery('#wf-scheduling-mode-automatic').attr('disabled', 'true');
                    jQuery('#wf-scheduling-mode-manually').attr('disabled', 'true');
                }
            };
        </script>            
        <?php
    }
            
    public static function gen_scans_general_settings(  $post, $metabox = null ) 
    {   
            $current_site_id = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : 0; 
            $w = isset($metabox['args']['w']) ? $metabox['args']['w'] : null;
            if (empty($w))
                return;
            
            if ($current_site_id) {
                $is_Paid = MainWP_Wordfence_Config_Site::get( 'isPaid', 0 );
            } else {
                $is_Paid = get_option('mainwp_wordfence_use_premium_general_settings');
            }
               
            ?>
            <table class="mwp-wf-form-table">
                    <tbody>             
                        <tr>
                            <th class="wfConfigEnable">Check if this website is on a domain blacklist</th>
                            <td>
                                <input type="checkbox" id="scansEnabled_checkGSB" class="wfConfigElem" name="scansEnabled_checkGSB" value="1" <?php $w->cbp( 'scansEnabled_checkGSB', $is_Paid ); if (!$is_Paid) { ?>onclick="alert('This is a paid feature because it places significant additional load on our servers.'); jQuery('#scansEnabled_checkGSB').attr('checked', false); return false;" <?php } ?>>
                                <span style="color: #F00;">Premium Feature</span><br/> When doing a scan, Wordfence will check with multiple domain blacklists to see if your site is listed.

                            </td>
                        </tr>
                        <tr>
                            <th class="wfConfigEnable">Check if this website is being "Spamvertised"</th>
                            <td><input type="checkbox" id="spamvertizeCheck" class="wfConfigElem"
                                name="spamvertizeCheck" value="1" <?php $w->cbp( 'spamvertizeCheck', $is_Paid );
                                             if ( ! $is_Paid ) {  ?>onclick="alert('This is a paid feature because it places significant additional load on our servers.'); jQuery('#spamvertizeCheck').attr('checked', false); return false;" <?php } ?> />&nbsp;<span
                                    style="color: #F00;">Premium Feature</span> <br/>When doing a scan, Wordfence will check
                                with spam services if your site domain name is appearing as a link in spam emails.
                            </td>
                        </tr>
                        <tr>
                            <th class="wfConfigEnable">Check if this website IP is generating spam</th>
                            <td><input type="checkbox" id="checkSpamIP" class="wfConfigElem"
                                name="checkSpamIP" value="1" <?php $w->cbp( 'checkSpamIP', $is_Paid );
                                             if ( ! $is_Paid ) {  ?>onclick="alert('This is a paid feature because it places significant additional load on our servers.'); jQuery('#checkSpamIP').attr('checked', false); return false;" <?php } ?> />&nbsp;<span
                                    style="color: #F00;">Premium Feature</span>
                            </td>
                        </tr>
                        <tr>
                            <th>Scan for misconfigured How does Wordfence get IPs</th>
                            <td><input type="checkbox" id="scansEnabled_checkHowGetIPs" class="wfConfigElem"
                                       name="scansEnabled_checkHowGetIPs"
                                            value="1" <?php $w->cb( 'scansEnabled_checkHowGetIPs' ); ?></td>
                        </tr>                        
                        <tr>
                            <th>Scan for publicly accessible configuration, backup, or log files</th>
                                <td><input type="checkbox" id="scansEnabled_checkReadableConfig" class="wfConfigElem"
                                                   name="scansEnabled_checkReadableConfig" value="1" <?php $w->cb( 'scansEnabled_checkReadableConfig' ); ?> />
                                </td>
                        </tr>
                        <tr>
                                <th>Scan for publicly accessible quarantined files</th>
                                <td><input type="checkbox" id="scansEnabled_suspectedFiles" class="wfConfigElem"
                                                   name="scansEnabled_suspectedFiles" value="1" <?php $w->cb( 'scansEnabled_suspectedFiles' ); ?> />
                                </td>
                        </tr>
                        <tr>
                            <th>Scan core files against repository versions for changes</th>
                            <td><input type="checkbox" id="scansEnabled_core" class="wfConfigElem"
							           name="scansEnabled_core" value="1" <?php $w->cb( 'scansEnabled_core' ); ?>/></td>
                        </tr>

                        <tr>
                            <th>Scan theme files against repository versions for changes</th>
                            <td><input type="checkbox" id="scansEnabled_themes" class="wfConfigElem"
							           name="scansEnabled_themes" value="1" <?php $w->cb( 'scansEnabled_themes' ); ?>/>
                            </td>
                        </tr>
                        <tr>
                            <th>Scan plugin files against repository versions for changes</th>
                            <td><input type="checkbox" id="scansEnabled_plugins" class="wfConfigElem"
                                       name="scansEnabled_plugins"
							           value="1" <?php $w->cb( 'scansEnabled_plugins' ); ?>/></td>
                        </tr>
                        <tr>
                            <th>Scan wp-admin and wp-includes for files not bundled with WordPress</th>
                            <td><input type="checkbox" id="scansEnabled_coreUnknown" class="wfConfigElem"
                                       name="scansEnabled_coreUnknown"
                                                value="1" <?php $w->cb( 'scansEnabled_coreUnknown' ); ?>/></td>
                        </tr>
                        <tr>
                            <th>Scan for signatures of known malicious files</th>
                            <td><input type="checkbox" id="scansEnabled_malware" class="wfConfigElem"
                                       name="scansEnabled_malware"
							           value="1" <?php $w->cb( 'scansEnabled_malware' ); ?>/></td>
                        </tr>
                        <tr>
                            <th>Scan file contents for backdoors, trojans and suspicious code</th>
                            <td><input type="checkbox" id="scansEnabled_fileContents" class="wfConfigElem"
                                       name="scansEnabled_fileContents"
							           value="1" <?php $w->cb( 'scansEnabled_fileContents' ); ?>/>							
							</td>
                        </tr>                       
                        <tr>
                            <th>Scan file contents for malicious URLs </th>
                            <td><input type="checkbox" id="scansEnabled_fileContentsGSB" class="wfConfigElem"
                                       name="scansEnabled_fileContentsGSB"
							           value="1" <?php $w->cb( 'scansEnabled_fileContentsGSB' ); ?>/>
							</td>
                        </tr>                        
                        <tr>
                            <th>Scan posts for known dangerous URLs and suspicious content</th>
                            <td><input type="checkbox" id="scansEnabled_posts" class="wfConfigElem"
							           name="scansEnabled_posts" value="1" <?php $w->cb( 'scansEnabled_posts' ); ?>/>
                            </td>
                        </tr>
                        <tr>
                            <th>Scan comments for known dangerous URLs and suspicious content</th>
                            <td><input type="checkbox" id="scansEnabled_comments" class="wfConfigElem"
                                       name="scansEnabled_comments"
							           value="1" <?php $w->cb( 'scansEnabled_comments' ); ?>/></td>
                        </tr>
                        <tr>
                            <th>Scan WordPress core, plugin, and theme options for known dangerous URLs and suspicious content</th>
                            <td><input type="checkbox" id="scansEnabled_suspiciousOptions" class="wfConfigElem"
                                       name="scansEnabled_suspiciousOptions"
							           value="1" <?php $w->cb( 'scansEnabled_suspiciousOptions' ); ?>/></td>
                        </tr>
                        <tr>
                            <th>Scan for out of date, abandoned, and vulnerable plugins, themes, and WordPress versions</th>
                            <td><input type="checkbox" id="scansEnabled_oldVersions" class="wfConfigElem"
                                       name="scansEnabled_oldVersions"
							           value="1" <?php $w->cb( 'scansEnabled_oldVersions' ); ?>/></td>
                        </tr>
						<tr>
							<th>Scan for admin users created outside of WordPress</th>
							<td><input type="checkbox" id="scansEnabled_suspiciousAdminUsers" class="wfConfigElem"
									   name="scansEnabled_suspiciousAdminUsers"
									   value="1" <?php $w->cb( 'scansEnabled_suspiciousAdminUsers' ); ?>/></td>
						</tr>
                        <tr>
                            <th>Check the strength of passwords</th>
                            <td><input type="checkbox" id="scansEnabled_passwds" class="wfConfigElem"
                                       name="scansEnabled_passwds"
							           value="1" <?php $w->cb( 'scansEnabled_passwds' ); ?>/></td>
                        </tr>
                        <tr>
                            <th>Monitor disk space</th>
                            <td><input type="checkbox" id="scansEnabled_diskSpace" class="wfConfigElem"
                                       name="scansEnabled_diskSpace"
							           value="1" <?php $w->cb( 'scansEnabled_diskSpace' ); ?>/></td>
                        </tr>
                        <tr>
                            <th>Scan for unauthorized DNS changes</th>
                            <td><input type="checkbox" id="scansEnabled_dns" class="wfConfigElem"
							           name="scansEnabled_dns" value="1" <?php $w->cb( 'scansEnabled_dns' ); ?>/></td>
                        </tr>
                        <tr>
                            <th>Scan files outside your WordPress installation</th>
                            <td><input type="checkbox" id="other_scanOutside" class="wfConfigElem"
							           name="other_scanOutside" value="1" <?php $w->cb( 'other_scanOutside' ); ?> />
                            </td>
                        </tr>
                        <tr>
                            <th>Scan images, binary, and other files as if they were executable</th>
                            <td><input type="checkbox" id="scansEnabled_scanImages" class="wfConfigElem"
                                       name="scansEnabled_scanImages"
							           value="1" <?php $w->cb( 'scansEnabled_scanImages' ); ?> /></td>
                        </tr>
                        <tr>
                            <th>Enable HIGH SENSITIVITY scanning (may give false positives)</th>
                            <td><input type="checkbox" id="scansEnabled_highSense" class="wfConfigElem"
                                       name="scansEnabled_highSense"
							           value="1" <?php $w->cb( 'scansEnabled_highSense' ); ?> /></td>
                        </tr>                          
                        
                        </tbody>
                    </table>            
            <?php
        }
        
    public static function gen_scans_basic_settings(  $post, $metabox = null ) 
    {
        
        $current_site_id = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : 0; 
        $w = isset($metabox['args']['w']) ? $metabox['args']['w'] : null;
        
        if (empty($w))
            return;
        
        $scan_type = $w->get('scanType', MainWP_Wordfence_Config::SCAN_TYPE_STANDARD);
        
        if ($current_site_id) {
            $is_Paid = MainWP_Wordfence_Config_Site::get( 'isPaid', 0 );
        } else {
            $is_Paid = get_option('mainwp_wordfence_use_premium_general_settings');
        }
        
        $is_Paid = $is_Paid ? true : false;
        
        function _wfAllowOnlyBoolean($value) {
            return ($value === false || $value === true);
        }


        $limitedOptions = array_filter(MainWP_wfScanner::limitedScanTypeOptions(), '_wfAllowOnlyBoolean');
        $standardOptions = array_filter(MainWP_wfScanner::standardScanTypeOptions($is_Paid), '_wfAllowOnlyBoolean');
        $highSensitivityOptions = array_filter(MainWP_wfScanner::highSensitivityScanTypeOptions($is_Paid), '_wfAllowOnlyBoolean');

        ?>
        <table class="mwp-wf-form-table">
            <tbody>  
                <tr>
                        <th class="wfConfigEnable" style="min-width:150px">Scan Type</th>     
                        <td>
                            <ul>
                                <li>
                                <label class="wf-control-label"><input type="radio" class="mwp-wf-scan-type-option" name="scanType" <?php echo ($scan_type  == MainWP_Wordfence_Config::SCAN_TYPE_LIMITED) ? 'checked' : ''; ?>  value="<?php echo MainWP_Wordfence_Config::SCAN_TYPE_LIMITED; ?>" data-selected-options="<?php echo esc_attr(json_encode($limitedOptions)); ?>"> Limited Scan (For entry-level hosting plans. Provides limited detection capability with very low resource utilization.)</label>
                                </li>
                                <li>
                                <label class="wf-control-label"><input type="radio" class="mwp-wf-scan-type-option" name="scanType" <?php echo ($scan_type  == MainWP_Wordfence_Config::SCAN_TYPE_STANDARD) ? 'checked' : ''; ?> value="<?php echo MainWP_Wordfence_Config::SCAN_TYPE_STANDARD; ?>" data-selected-options="<?php echo esc_attr(json_encode($standardOptions)); ?>"> Standard Scan (Our recommendation for all websites. Provides the best detection capability in the industry.)</label>
                                </li>
                                <li>
                                <label class="wf-control-label"><input type="radio" class="mwp-wf-scan-type-option" name="scanType" <?php echo ($scan_type  == MainWP_Wordfence_Config::SCAN_TYPE_HIGH_SENSITIVITY) ? 'checked' : ''; ?> value="<?php echo MainWP_Wordfence_Config::SCAN_TYPE_HIGH_SENSITIVITY; ?>" data-selected-options="<?php echo esc_attr(json_encode($highSensitivityOptions)); ?>"> High Sensitivity (For site owners who think they may have been hacked. More thorough but may produce false positives.)</label>
                                </li>
                                <li>
                                <label class="wf-control-label"><input type="radio" class="mwp-wf-scan-type-option mwp-wf-scan-type-option-custom" name="scanType" <?php echo ($scan_type  == MainWP_Wordfence_Config::SCAN_TYPE_CUSTOM) ? 'checked' : 'disabled'; ?> value="<?php echo MainWP_Wordfence_Config::SCAN_TYPE_CUSTOM; ?>"> Custom Scan (Selected automatically when General Options have been customized for this website.)</label>                           
                                </li>
                            </ul>                                
                        </td>
                    </tr> 
                </tbody>
        </table>
        
        <script type="application/javascript">
			(function($) {
				$(function() {
                        //Set initial state                        
                        var currentScanType = $('.mwp-wf-scan-type-option:checked');
                        if (!currentScanType.hasClass('mwp-wf-scan-type-option-custom')) {
                            var selectedOptions = currentScanType.data('selectedOptions');
                            var keys = Object.keys(selectedOptions);
                            for (var i = 0; i < keys.length; i++) {
                                $('input[name="' + keys[i] + '"]').attr("checked", selectedOptions[keys[i]]); //Currently all checkboxes
                            }
                        }
                    
                        $('.mwp-wf-scan-type-option').each(function(index, element) {
                               $(element).on('click', function(e) {
                                   if ($(element).hasClass('mwp-wf-scan-type-option-custom')) {
                                       return;
                                   }
                                   $('.mwp-wf-scan-type-option.mwp-wf-scan-type-option-custom').attr('disabled', true);
                                   var selectedOptions = $(this).data('selectedOptions');
                                   var keys = Object.keys(selectedOptions);
                                   for (var i = 0; i < keys.length; i++) {								
                                       $('input[name="' + keys[i] + '"]').attr("checked", selectedOptions[keys[i]]); //Currently all checkboxes
                                   }
                               });
                           });
                           
                            //Hook up change events on individual checkboxes
                            var availableOptions = <?php echo json_encode(array_keys($highSensitivityOptions)); ?>;
                            for (var i = 0; i < availableOptions.length; i++) {
                                $('input[name="' + availableOptions[i] + '"]').on('change', function(e) { //Currently all checkboxes                                                                       
                                    var currentScanType = $('.mwp-wf-scan-type-option:checked');
                                    if (!currentScanType.hasClass('mwp-wf-scan-type-option-custom')) {
                                        $('.mwp-wf-scan-type-option.mwp-wf-scan-type-option-custom').prop('checked', true);                                        
                                        $('.mwp-wf-scan-type-option.mwp-wf-scan-type-option-custom').removeAttr('disabled');
                                    }                                    
                                });
                            }
                    
                });
			})(jQuery);
                    
        </script>
        <?php
    }
    
    public static function gen_scans_performace_settings(  $post, $metabox = null ) 
    {
        
        $w = isset($metabox['args']['w']) ? $metabox['args']['w'] : null;
        
        if (empty($w))
            return;
            
        ?>
        <table class="mwp-wf-form-table">
            <tbody>               
                <tr>
                    <th>Use low resource scanning (reduces server load by lengthening the scan duration)</th>
                        <td><input type="checkbox" id="lowResourceScansEnabled" class="wfConfigElem"
                                       name="lowResourceScansEnabled"
                                    value="1" <?php $w->cb( 'lowResourceScansEnabled' ); ?> /></td>
                </tr>                             
                <tr>
                        <th>Limit the number of issues sent in the scan results email</th>
                        <td>
                                <input type="text" class="wfConfigElem mwp-wf-form-control" name="scan_maxIssues" id="scan_maxIssues" value="<?php $w->f( 'scan_maxIssues' ); ?>">
                                <span class="wf-help-block">0 or empty means unlimited issues will be sent.</span>
                        </td>
                </tr>  
                <tr>
                            <th>Time limit that a scan can run in seconds </th>
                            <td>
                                    <input type="text" class="wfConfigElem mwp-wf-form-control" name="scan_maxDuration" id="scan_maxDuration" value="<?php $w->f( 'scan_maxDuration' ); ?>">
                                    <span class="wf-help-block">0 or empty means the default value will be used.</span>
                            </td>
                </tr> 
                <tr>
                    <th>How much memory should Wordfence request when scanning</th>
                    <td><input type="text" id="maxMem" name="maxMem" value="<?php $w->f( 'maxMem' ); ?>"
                               size="4"/>Megabytes
                    </td>
                </tr>

                <tr>
                    <th>Maximum execution time for each scan stage</th>
                    <td><input type="text" id="maxExecutionTime" name="maxExecutionTime"
                               value="<?php $w->f( 'maxExecutionTime' ); ?>" size="4"/>0 for default. Must be greater than 7 and 10-20 or higher is recommended for most servers
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
        
    }
     
    public static function gen_scans_advanced_settings(  $post, $metabox = null ) 
    {        
        $w = isset($metabox['args']['w']) ? $metabox['args']['w'] : null;        
        ?>
        <table class="mwp-wf-form-table">
            <tbody>               
                <tr>
                    <th>Exclude files from scan that match these wildcard patterns (one per line).</th>
                            <td>
                                    <textarea id="scan_exclude" class="wfConfigElem mwp-wf-form-control" cols="40" rows="4"
                                            name="scan_exclude"><?php echo MainWP_Wordfence_Utility::cleanupOneEntryPerLine($w->getHTML( 'scan_exclude' )); ?></textarea>
                            </td>
                    </tr>
                <tr id="scan_include_extra">
                        <th style="vertical-align: top;">Additional scan signatures (one per line)</th>
                        <td><textarea class="wfConfigElement mwp-wf-form-control" cols="40" rows="4"
                                      name="scan_include_extra"><?php echo $w->getHTML('scan_include_extra'); ?></textarea>
                        </td>
                    </tr>                        
            </tbody>
        </table>
        <?php
    }
        
}
