<?php

class MainWP_Wordfence_Setting {
	//Singleton
	private static $instance = null;
	private $configs = null;
		
	static function get_instance() {
		if ( null == MainWP_Wordfence_Setting::$instance ) {
			MainWP_Wordfence_Setting::$instance = new MainWP_Wordfence_Setting();
		}

		return MainWP_Wordfence_Setting::$instance;
	}

	public function load_configs($websites = array(), $individual = false) {
		if ($this->configs == null) {
			if ($individual) {
				$website = current($websites);
				if ( is_object( $website ) ) {					
					$this->configs = new MainWP_Wordfence_Config_Site( $website->id );
				}
			} else {
				$website_ids = array();
				foreach ( $websites as $site_id => $site_name ) {
					$website_ids[] = $site_id;
				}
				$this->configs = new MainWP_Wordfence_Config( $website_ids );
			}
		}		
		return $this->configs;		
	}	
	
	public function __construct() {
		
	}
    
    public static function init() {
        if ( isset( $_POST['submit'] ) && isset($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], 'mwp-wfc-nonce') && isset( $_POST['wfc_individual_settings_site_id'] ) && ! empty( $_POST['wfc_individual_settings_site_id'] ) ) {            			
            $return = MainWP_Wordfence::handlePostSettings( $_POST['wfc_individual_settings_site_id'] );            
            if (is_array($return)) {
                if (isset($return['ok'])) {
                    update_option( 'mainwp_wfc_do_save_individual_setting', 'yes' );                
                } else if (isset($return['errorMsg'])) {
                    update_option( 'mainwp_wfc_save_individual_setting_error', $return['errorMsg'] );                
                }
            } else {
                update_option( 'mainwp_wfc_save_individual_setting_error', 'Undefined error.' );                
            }
		}
    }
                
    public static function on_load_general_page() {
                
                $websites = MainWP_Wordfence::get_wfc_sites();
                $w = self::get_instance()->load_configs($websites);
                
                $current_site_id =  isset( $_GET['site_id'] ) && ! empty( $_GET['site_id'] ) ? $_GET['site_id'] : 0;
                $site_url = '';
                
                if( $current_site_id ) {
                    global $mainWPWordfenceExtensionActivator;                    
                    $dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPWordfenceExtensionActivator->get_child_file(), $mainWPWordfenceExtensionActivator->get_child_key(), array( $current_site_id ), array() );                                
                    if ( is_array( $dbwebsites ) && ! empty( $dbwebsites ) ) {
                        $website = current( $dbwebsites );
                        $site_url = $website->url;
                    }
                }
        
                $current_tab = $current_action = '';
                                
                if (isset($_GET['action'])) 
                {                                 
                    $current_action = $_GET['action'];                    
                }
                
                
                $i = 1;	
                if (isset($_GET['tab'])) {  
                    do_action('mainwp_enqueue_meta_boxes_scripts');
                    
                    if ($_GET['tab'] == 'network_setting') {                        
                        add_meta_box(
                                'wfc-settings-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Licenses <span class="wfc_postbox_title">(click right arrow to open or close section)</span>', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Setting', 'gen_settings_licenses' ),
                                'mainwp_wfc_postboxes_settings',
                                'normal',
                                'core',
                                array( 'websites' => $websites, 'w' => $w )
                        );
                        add_meta_box(
                                'wfc-settings-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Scan Schedule', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Setting', 'gen_settings_scan_schedule' ),
                                'mainwp_wfc_postboxes_settings',
                                'normal',
                                'core',
                                array( 'w' => $w )
                        );
                        add_meta_box(
                                'wfc-settings-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'General Wordfence Options', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Setting', 'gen_settings_basic' ),
                                'mainwp_wfc_postboxes_settings',
                                'normal',
                                'core',
                                array( 'w' => $w )
                        );
                        add_meta_box(
                                'wfc-settings-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Alert Preferences', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Setting', 'gen_settings_alerts' ),
                                'mainwp_wfc_postboxes_settings',
                                'normal',
                                'core',
                                array( 'w' => $w )
                        );
                        add_meta_box(
                                'wfc-settings-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Activity Report', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Setting', 'gen_settings_email' ),
                                'mainwp_wfc_postboxes_settings',
                                'normal',
                                'core',
                                array( 'w' => $w )
                        );
                        
                        add_meta_box(
                            'wfc-settings-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Dashboard Options', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Setting', 'gen_dashboard_notification_options' ),
                            'mainwp_wfc_postboxes_settings',
                            'normal',
                            'core',
                            array( 'w' => $w )
                        );
                         
                        add_meta_box(
                                'wfc-settings-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Comment Spam Filter Options', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Setting', 'gen_comment_spam_filter_settings' ),
                                'mainwp_wfc_postboxes_settings',
                                'normal',
                                'core',
                                array( 'w' => $w )
                        );
                        add_meta_box(
                                'wfc-settings-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Importing Wordfence Settings', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Setting', 'gen_settings_import_settings' ),
                                'mainwp_wfc_postboxes_settings',
                                'normal',
                                'core'
                        );
                        
                    } 
                    else if ($_GET['tab'] == 'network_scan') {                        
                        add_meta_box(
                                'wfc-settings-scan-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Scan Scheduling', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Scan', 'gen_scans_scheduling' ),
                                'mainwp_wfc_postboxes_general_scans',
                                'normal',
                                'core',
                                array( 'w' => $w )
                        );
                        
                        add_meta_box(
                                'wfc-settings-scan-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Wordfence Basic Scan Type Options', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Scan', 'gen_scans_basic_settings' ),
                                'mainwp_wfc_postboxes_general_scans',
                                'normal',
                                'core',
                                array( 'w' => $w )
                        );
                        
                        add_meta_box(
                                'wfc-settings-scan-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'General Options', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Scan', 'gen_scans_general_settings' ),
                                'mainwp_wfc_postboxes_general_scans',
                                'normal',
                                'core',
                                array( 'w' => $w )
                        );
                        add_meta_box(
                                'wfc-settings-scan-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Performance Options', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Scan', 'gen_scans_performace_settings' ),
                                'mainwp_wfc_postboxes_general_scans',
                                'normal',
                                'core',
                                array( 'w' => $w )
                        );
                        add_meta_box(
                                'wfc-settings-scan-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Advanced Scan Options', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Scan', 'gen_scans_advanced_settings' ),
                                'mainwp_wfc_postboxes_general_scans',
                                'normal',
                                'core',
                                array( 'w' => $w )
                        );
                    }
                    else if ( $_GET['tab'] == 'network_traffic' ) {
                        $current_tab = 'network_traffic';
                    }                     
                    else if ( $_GET['tab'] == 'network_blocking' ) {     
                        $current_tab = 'network_blocking';
                    }
                    else if ( $_GET['tab'] == 'network_firewall' ) {     
                        
                        add_meta_box(
                                'wfc-general-firewall-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Wordfence Basic WordPress Protection', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Firewall', 'gen_general_firewall_basic' ),
                                'mainwp_wfc_postboxes_general_firewall',
                                'normal',
                                'core',
                                array( 'w' => $w )
                        ); 

                       add_meta_box(
                                'wfc-firewall-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Advanced Firewall Options', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Firewall', 'gen_advanced_firewall_options' ),
                                'mainwp_wfc_postboxes_general_firewall',
                                'normal',
                                'core',
                                array( 'w' => $w )
                       );

                       add_meta_box(
                                'wfc-firewall-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Network Firewall Rules', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Firewall', 'gen_whitelisted_url' ),
                                'mainwp_wfc_postboxes_general_firewall',
                                'normal',
                                'core',
                                array()
                       );

//                       add_meta_box(
//                                'wfc-firewall-contentbox-' . $i++,
//                                '<i class="fa fa-cog"></i> ' . __( 'Firewall Rules', 'mainwp-wordfence-extension' ),
//                                array( 'MainWP_Wordfence_Firewall', 'gen_firewall_rules' ),
//                                'mainwp_wfc_postboxes_general_firewall',
//                                'normal',
//                                'core',
//                                array( 'w' => $w )
//                       );

                        add_meta_box(
                                'wfc-general-firewall-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Wordfence Rate Limiting', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Firewall', 'gen_settings_rate_limiting_rules' ),
                                'mainwp_wfc_postboxes_general_firewall',
                                'normal',
                                'core',
                                array( 'w' => $w )
                        );

                        add_meta_box(
                                'wfc-general-firewall-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Wordfence Brute Force Protection', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Firewall', 'gen_settings_login_security' ),
                                'mainwp_wfc_postboxes_general_firewall',
                                'normal',
                                'core',
                                array( 'w' => $w )
                        );

                   } else if ($_GET['tab'] == 'diagnostics') {                        
                        add_meta_box(
                                'wfc-diagnostics-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Diagnostics', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Setting', 'gen_diagnostics_result' ),
                                'mainwp_wfc_postboxes_diagnostics',
                                'normal',
                                'core',
                                array( 'websites' => $websites )
                        );                        

                        add_meta_box(
                                'wfc-diagnostics-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Debugging Options', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Setting', 'gen_diagnostics_debugging_options' ),
                                'mainwp_wfc_postboxes_diagnostics',
                                'normal',
                                'core',
                                array( 'w' => $w )
                        );
                    }                
                }  
                
                if ($current_tab == 'network_traffic' || $current_action == 'traffic') {
                    
                        if ($current_tab == 'network_traffic')
                        {
                                add_meta_box(
                                        'wfc-general-traffic-contentbox-' . $i++,
                                        '<i class="fa fa-cog"></i> ' . __( 'Wordfence Live Traffic Options', 'mainwp-wordfence-extension' ),
                                        array( 'MainWP_Wordfence_Live_Traffic', 'gen_live_traffic_options' ),
                                        'mainwp_wfc_postboxes_general_traffic',
                                        'normal',
                                        'core',
                                        array( 'w' => $w )
                                ); 
                        }
                        
                        add_meta_box(
                                'wfc-general-traffic-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . ($current_site_id ? __( 'Wordfence Live Traffic for', 'mainwp-wordfence-extension' ) . ' ' . $site_url : __( 'Network Wordfence Live Traffic', 'mainwp-wordfence-extension' )),
                                array( 'MainWP_Wordfence_Live_Traffic', 'gen_live_traffic_tab' ),
                                'mainwp_wfc_postboxes_general_traffic',
                                'normal',
                                'core',
                                array( 'websites' => $websites, 'websiteid' => $current_site_id )
                        ); 
                }   
                
                if ($current_tab == 'network_blocking') {
                    
                    add_meta_box(
                           'wfc-contentbox-' . $i++,
                           '<i class="fa fa-cog"></i> ' . __( 'Wordfence Blocking Options', 'mainwp-wordfence-extension' ),
                           array( 'MainWP_Wordfence_Blocking', 'gen_blocking_general_settings_tab' ),
                           'mainwp_wfc_postboxes_blocking',
                           'normal',
                           'core',
                           array( 'w' => $w )
                   );
                    
                    add_meta_box(
                           'wfc-contentbox-' . $i++,
                           '<i class="fa fa-cog"></i> ' . ($_GET['tab'] == 'network_blocking' ? __( 'WordFence Network Blocking Rule - Custom Pattern', 'mainwp-wordfence-extension' ) : __( 'WordFence Create a Blocking Rule - Custom Pattern', 'mainwp-wordfence-extension' )),
                           array( 'MainWP_Wordfence_Blocking', 'gen_blocking_custom_rules_tab' ),
                           'mainwp_wfc_postboxes_blocking',
                           'normal',
                           'core',
                           array( 'websiteid' => $current_site_id )
                   );

                   add_meta_box(
                           'wfc-contentbox-' . $i++,
                           '<i class="fa fa-cog"></i> ' . ($_GET['tab'] == 'network_blocking' ? __( 'WordFence Network Blocking Rule - IP Address', 'mainwp-wordfence-extension' ) : __( 'WordFence Create a Blocking Rule - IP Address', 'mainwp-wordfence-extension' )),
                           array( 'MainWP_Wordfence_Blocking', 'gen_blocking_rules_ip_address_tab' ),
                           'mainwp_wfc_postboxes_blocking',
                           'normal',
                           'core',
                           array( 'websiteid' => $current_site_id )
                   );
                }
                
                if ($current_action == 'blocking' && $current_site_id) {
                    add_meta_box(
                            'wfc-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' .__( 'Wordfence All Blocks for ' . $site_url, 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Blocking', 'gen_all_blocking_ips_tab' ),
                            'mainwp_wfc_postboxes_blocking',
                            'normal',
                            'core',
                            array( 'websiteid' => $current_site_id )
                    );	
                }
                
        }     
        
    public static function on_load_individual_page() 
    {
                $current_site_id = isset($_GET['id']) ? $_GET['id'] : 0;
                if (empty($current_site_id ))
                    return;                
                
                MainWP_Wordfence_Extension::activity_enqueue_style();
                
                $website = null;
                global $mainWPWordfenceExtensionActivator;
                $option = array(
                        'plugin_upgrades' => true,
                        'plugins' => true,
                );
                $site_url = '';
                $dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPWordfenceExtensionActivator->get_child_file(), $mainWPWordfenceExtensionActivator->get_child_key(), array( $current_site_id ), array(), $option );                                
                if ( is_array( $dbwebsites ) && ! empty( $dbwebsites ) ) {
                    $website = current( $dbwebsites );
                    $site_url = $website->url;
                }
                
                $w = null;                
                if ($website) { 
                    $w = self::get_instance()->load_configs(array($website), true);                      
                }

                do_action('mainwp_enqueue_meta_boxes_scripts');                
                $i = 0;
              
                if (!isset($_GET['tab']) || $_GET['tab'] == 'settings') {
                    
                    add_meta_box(
                            'wfc-settings-individual-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Wordfence Settings', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Setting', 'gen_settings_scan_schedule' ),
                            'mainwp_wfc_postboxes_individual_settings',
                            'normal',
                            'core',
                            array( 'websiteid' => $current_site_id, 'w' => $w)
                    );
                    
                    add_meta_box(
                            'wfc-settings-individual-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Wordfence License', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Setting', 'gen_settings_individual_licenses' ),
                            'mainwp_wfc_postboxes_individual_settings',
                            'normal',
                            'core',
                            array( 'websiteid' => $current_site_id )
                    );                    
                    add_meta_box(
                            'wfc-settings-individual-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'General Wordfence Options', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Setting', 'gen_settings_basic' ),
                            'mainwp_wfc_postboxes_individual_settings',
                            'normal',
                            'core',
                            array( 'websiteid' => $current_site_id, 'w' => $w )
                    );
                    add_meta_box(
                            'wfc-settings-individual-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Wordfence Alert Preferences', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Setting', 'gen_settings_alerts' ),
                            'mainwp_wfc_postboxes_individual_settings',
                            'normal',
                            'core',
                            array( 'websiteid' => $current_site_id, 'w' => $w )
                    );
                    add_meta_box(
                            'wfc-settings-individual-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Wordfence Activity Report', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Setting', 'gen_settings_email' ),
                            'mainwp_wfc_postboxes_individual_settings',
                            'normal',
                            'core',
                            array( 'websiteid' => $current_site_id, 'w' => $w )
                    );
 
                    add_meta_box(
                            'wfc-settings-individual-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Wordfence Dashboard Options', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Setting', 'gen_dashboard_notification_options' ),
                            'mainwp_wfc_postboxes_individual_settings',
                            'normal',
                            'core',
                            array( 'websiteid' => $current_site_id, 'w' => $w )
                    );

                    add_meta_box(
                            'wfc-settings-individual-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Comment Spam Filter Options', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Setting', 'gen_comment_spam_filter_settings' ),
                            'mainwp_wfc_postboxes_individual_settings',
                            'normal',
                            'core',
                            array( 'websiteid' => $current_site_id, 'w' => $w )
                    );
                    add_meta_box(
                            'wfc-settings-individual-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Exporting and Importing Wordfence Settings', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Setting', 'gen_settings_import_settings' ),
                            'mainwp_wfc_postboxes_individual_settings',
                            'normal',
                            'core',
                            array( 'websiteid' => $current_site_id)
                    );
                } 
                else if ($_GET['tab'] == 'scan') {
                    add_meta_box(
                            'wfc-settings-individual-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'General Options', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Scan', 'gen_scans_general_settings' ),
                            'mainwp_wfc_postboxes_scans',
                            'normal',
                            'core',
                            array( 'websiteid' => $current_site_id, 'w' => $w )
                    );
                    
                    add_meta_box(
                            'wfc-settings-individual-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Wordfence Basic Scan Type Options', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Scan', 'gen_scans_basic_settings' ),
                            'mainwp_wfc_postboxes_scans',
                            'normal',
                            'core',
                            array( 'websiteid' => $current_site_id, 'w' => $w )
                    );
                    
                    add_meta_box(
                            'wfc-settings-individual-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Scan Scheduling', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Scan', 'gen_scans_scheduling' ),
                            'mainwp_wfc_postboxes_scans',
                            'normal',
                            'core',
                            array( 'websiteid' => $current_site_id, 'w' => $w )
                    );
                    add_meta_box(
                            'wfc-settings-individual-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Performance Options', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Scan', 'gen_scans_performace_settings' ),
                            'mainwp_wfc_postboxes_scans',
                            'normal',
                            'core',
                            array( 'w' => $w )
                    );
                    add_meta_box(
                            'wfc-settings-individual-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Advanced Scan Options', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Scan', 'gen_scans_advanced_settings' ),
                            'mainwp_wfc_postboxes_scans',
                            'normal',
                            'core',
                            array( 'w' => $w )
                    );
                }
                else if ($_GET['tab'] == 'diagnostics') {                    
                        add_meta_box(
                                'wfc-diagnostics-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Diagnostics', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Setting', 'gen_diagnostics_result' ),
                                'mainwp_wfc_postboxes_individual_diagnostics',
                                'normal',
                                'core',
                                array( 'websiteid' => $current_site_id, 'websites' => array($current_site_id => $website->name))
                        );
                        add_meta_box(
                                'wfc-diagnostics-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Other Tests', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Setting', 'gen_diagnostics_other_tests' ),
                                'mainwp_wfc_postboxes_individual_diagnostics',
                                'normal',
                                'core',
                                array( 'websiteid' => $current_site_id )
                        );

                        add_meta_box(
                                'wfc-diagnostics-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Debugging Options', 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Setting', 'gen_diagnostics_debugging_options' ),
                                'mainwp_wfc_postboxes_individual_diagnostics',
                                'normal',
                                'core',
                                array( 'websiteid' => $current_site_id , 'w' => $w )
                        );                      
                }                
                else if ( $_GET['tab'] == 'traffic') {                     
                    add_meta_box(
                            'wfc-general-traffic-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Live Traffic Options', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Live_Traffic', 'gen_live_traffic_options' ),
                            'mainwp_wfc_postboxes_traffic',
                            'normal',
                            'core',
                            array( 'w' => $w , 'is_individual' => true )
                    ); 

                    add_meta_box(
                            'wfc-general-traffic-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Live Traffic for', 'mainwp-wordfence-extension' ) . ' ' . $site_url,
                            array( 'MainWP_Wordfence_Live_Traffic', 'gen_live_traffic_tab' ),
                            'mainwp_wfc_postboxes_traffic',
                            'normal',
                            'core',
                            array( 'websiteid' => $current_site_id , )
                    );                        
                } 
                else if ( $_GET['tab'] == 'blocking') {   
                        add_meta_box(
                               'wfc-contentbox-' . $i++,
                               '<i class="fa fa-cog"></i> ' . __( 'Wordfence Blocking Options', 'mainwp-wordfence-extension' ),
                               array( 'MainWP_Wordfence_Blocking', 'gen_blocking_general_settings_tab' ),
                               'mainwp_wfc_postboxes_blocking',
                               'normal',
                               'core',
                               array( 'w' => $w )
                       );
                     
                        add_meta_box(
                               'wfc-contentbox-' . $i++,
                               '<i class="fa fa-cog"></i> ' . __( 'WordFence Create a Blocking Rule - Custom Pattern', 'mainwp-wordfence-extension' ),
                               array( 'MainWP_Wordfence_Blocking', 'gen_blocking_custom_rules_tab' ),
                               'mainwp_wfc_postboxes_blocking',
                               'normal',
                               'core',
                               array( 'websiteid' => $current_site_id )
                       );
                       
                       add_meta_box(
                               'wfc-contentbox-' . $i++,
                               '<i class="fa fa-cog"></i> ' . __( 'WordFence Create a Blocking Rule - IP Address', 'mainwp-wordfence-extension' ),
                               array( 'MainWP_Wordfence_Blocking', 'gen_blocking_rules_ip_address_tab' ),
                               'mainwp_wfc_postboxes_blocking',
                               'normal',
                               'core',
                               array( 'websiteid' => $current_site_id )
                       );
                                              
                        add_meta_box(
                                'wfc-contentbox-' . $i++,
                                '<i class="fa fa-cog"></i> ' . __( 'Wordfence All Blocks for ' . $site_url, 'mainwp-wordfence-extension' ),
                                array( 'MainWP_Wordfence_Blocking', 'gen_all_blocking_ips_tab' ),
                                'mainwp_wfc_postboxes_blocking',
                                'normal',
                                'core',
                                array( 'websiteid' => $current_site_id )
                        );	
                                            
                } 
                else if ( $_GET['tab'] == 'firewall') {                     
                    add_meta_box(
                             'wfc-firewall-contentbox-' . $i++,
                             '<i class="fa fa-cog"></i> ' . __( 'Basic Firewall Options', 'mainwp-wordfence-extension' ),
                             array( 'MainWP_Wordfence_Firewall', 'gen_individual_firewall_basic' ),
                             'mainwp_wfc_postboxes_firewall',
                             'normal',
                             'core',
                             array( 'websiteid' => $current_site_id , 'w' => $w )
                     );

                    add_meta_box(
                             'wfc-firewall-contentbox-' . $i++,
                             '<i class="fa fa-cog"></i> ' . __( 'Advanced Firewall Options', 'mainwp-wordfence-extension' ),
                             array( 'MainWP_Wordfence_Firewall', 'gen_advanced_firewall_options' ),
                             'mainwp_wfc_postboxes_firewall',
                             'normal',
                             'core',
                             array( 'websiteid' => $current_site_id, 'w' => $w  )
                    );
                  
//                    add_meta_box(
//                             'wfc-firewall-contentbox-' . $i++,
//                             '<i class="fa fa-cog"></i> ' . __( 'Firewall Rules', 'mainwp-wordfence-extension' ),
//                             array( 'MainWP_Wordfence_Firewall', 'gen_firewall_rules' ),
//                             'mainwp_wfc_postboxes_firewall',
//                             'normal',
//                             'core',
//                             array( 'websiteid' => $current_site_id )
//                    );
                    
                    add_meta_box(
                            'wfc-firewall-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Rate Limiting', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Firewall', 'gen_settings_rate_limiting_rules' ),
                            'mainwp_wfc_postboxes_firewall',
                            'normal',
                            'core',
                            array( 'w' => $w )
                    ); 
                    add_meta_box(
                            'wfc-firewall-contentbox-' . $i++,
                            '<i class="fa fa-cog"></i> ' . __( 'Brute Force Protection', 'mainwp-wordfence-extension' ),
                            array( 'MainWP_Wordfence_Firewall', 'gen_settings_login_security' ),
                            'mainwp_wfc_postboxes_firewall',
                            'normal',
                            'core',
                            array( 'w' => $w )
                    ); 
                      
                    add_meta_box(
                             'wfc-firewall-contentbox-' . $i++,
                             '<i class="fa fa-cog"></i> ' . __( 'Firewall Rules and Whitelisted URLs for', 'mainwp-wordfence-extension' ) . ' ' . $site_url,
                             array( 'MainWP_Wordfence_Firewall', 'gen_whitelisted_url' ),
                             'mainwp_wfc_postboxes_firewall',
                             'normal',
                             'core',
                             array('websiteid' => $current_site_id ,)
                    );
                }
                    
        }
                
	public function admin_init() {
		add_action( 'wp_ajax_mainwp_wfc_save_settings', array( $this, 'ajax_save_settings' ) );	
        add_action( 'wp_ajax_mainwp_wfc_save_firewall_settings', array( $this, 'ajax_save_firewall_settings' ) );	        
		add_action( 'wp_ajax_mainwp_wfc_save_settings_reload', array( $this, 'ajax_save_settings_reload' ) );		
        add_action( 'wp_ajax_mainwp_wfc_change_override_general_settings', array( $this, 'ajax_change_override_general_settings' ) );		
        add_action( 'wp_ajax_mainwp_wfc_change_general_settings_use_premium', array( $this, 'ajax_change_general_settings_use_premium' ) );		
        add_action( 'wp_ajax_mainwp_wfc_save_general_settings_to_child', array( $this, 'ajax_save_general_settings_to_child' ) );                        
	}
       
	function ajax_save_settings() {
		$siteid = $_POST['siteId'];
		if ( empty( $siteid ) ) {
			die( json_encode( 'FAIL' ) );
		}
        $selected_section = isset($_POST['_ajax_saving_section']) ? $_POST['_ajax_saving_section'] : '';
		$information = $this->perform_save_settings($siteid, false, $selected_section);					
		die( json_encode( $information ) );
	}

    function ajax_save_firewall_settings() {
        
        if ( !isset($_POST['nonce']) || !wp_verify_nonce( $_POST['nonce'], 'wfc-nonce' ) ) {
            wp_send_json( array( 'error' => esc_html( __("Invalid request") ) ) );
        }

		$siteid = $_POST['siteId'];
		if ( empty( $siteid ) ) {
			wp_send_json( array( 'error' => esc_html( __("Empty Site ID") ) ) );
		}
        
        $w         = new MainWP_Wordfence_Config_Site( $siteid ); // new: to load data
		$override = $w->is_override();        
        if ($override) {
			die( json_encode( array( 'error' => 'Not Updated - Individual site settings are in use' ) ) );
            return;
		}
        
        $settings = array();
        $ext_settings = get_option('mainwp_wfc_general_extra_settings', array());  
        if (is_array($ext_settings) && isset($ext_settings['general_firewall']))
            $settings = $ext_settings['general_firewall'];  
        	
         $check_values = array('wafStatus', 'learningModeGracePeriodEnabled', 'learningModeGracePeriod');
         
        $post_data = array();    
        foreach($check_values as $value) {
            if (isset($settings[$value])) {
                $post_data[$value] = $settings[$value];
            }
        }   
            
        global $mainWPWordfenceExtensionActivator;
        
        $post_data['mwp_action'] = 'save_waf_config';
        $post_data['wafConfigAction'] = 'config';
        
        $information = apply_filters( 'mainwp_fetchurlauthed', $mainWPWordfenceExtensionActivator->get_child_file(), $mainWPWordfenceExtensionActivator->get_child_key(), $siteid, 'wordfence', $post_data );            
        if (is_array($information) && isset($information['data'])) {                              
            $update = array(
                'wafData' => $information['data'],
                'wafStatus' => $post_data['wafStatus'],
                'learningModeGracePeriodEnabled' => $post_data['learningModeGracePeriodEnabled']                    
            );                                
            if (isset($information['learningModeGracePeriod'])) {
                $update['learningModeGracePeriod'] = $information['learningModeGracePeriod'];
            }
            MainWP_Wordfence_DB::get_instance()->update_extra_settings_fields_values_by( $siteid, $update );   

        }           
        die( json_encode( $information ) );   

		die( json_encode( $information ) );
	}
    
	function mainwp_apply_plugin_settings($siteid) {	
        // forced apply settings to child so save ALL settings
        $all_section = MainWP_Wordfence_Config::OPTIONS_TYPE_ALL;
		$information = $this->perform_save_settings($siteid, true, $all_section);		
		$result = array();
		if (is_array($information)) {
			if (isset($information['ok'])) {
				$result = array('result' => 'success');
			} else if ($information['error']) {
				$result = array('error' => $information['error']);				
			} else {
				$result = array('result' => 'failed');
			}			
		} else {
			$result = array('result' => 'failed');
		}			
		die( json_encode( $result ) );		
	}
	
	function simple_crypt($key, $data, $action = 'encrypt'){
            $res = '';
            if($action == 'encrypt'){
				$string = base64_encode( serialize($data) );                
            } else {
				$string = $data;
			}
            for( $i = 0; $i < strlen($string); $i++){
                    $c = ord(substr($string, $i));
                    if($action == 'encrypt'){
                        $c += ord(substr($key, (($i + 1) % strlen($key))));
                        $res .= chr($c & 0xFF);
                    }else{
                        $c -= ord(substr($key, (($i + 1) % strlen($key))));
                        $res .= chr(abs($c) & 0xFF);
                    }
            }
			
            if($action !== 'encrypt'){
                $res = unserialize( base64_decode($res) );
            } 
            return $res;
    }
	
	public function perform_save_settings($siteid, $forced_global_setting = false, $pSection = '') {		
        
        $saving_opts = MainWP_Wordfence_Config::getSectionSettings($pSection);
        if (empty($saving_opts))
            return array( 'error' => 'Invalid fields number' );
		$w         = new MainWP_Wordfence_Config_Site( $siteid ); // new: to load data
		$cacheType = $w->get_cacheType();
		$apiKey    = MainWP_Wordfence_Config_Site::$apiKey;

		if ( !$forced_global_setting && $override = $w->is_override() ) {
			$options = MainWP_Wordfence_Config_Site::load_settings();
		} else {            
            // if forced save general settings 
            // or saving general settings to child  
            // or saving to child site from general settings
            // then get general settings to save
			$options = MainWP_Wordfence_Config::load_settings();
		}
		
		if (!$forced_global_setting) {
			$individual = isset( $_POST['individual'] ) && $_POST['individual'] ? true : false;
			if ( $individual && !$override ) {
				return array( 'error' => 'Update Failed: Override General Settings need to be set to Yes.' );
			} else if ( !$individual && $override ) {
				return array( 'result' => 'OVERRIDED' );
			}
		}

		global $mainWPWordfenceExtensionActivator;
		$post_data             = array(
			'mwp_action' => 'save_settings_new', // new version of saving settings
            'savingSection' => $pSection,
			'apiKey'     => $apiKey,
		);
        
        foreach($options as $key => $val) {
            if (!in_array($key, $saving_opts)) {
                unset($options[$key]);
            }
        }
                
        if (isset($options['apiKey']))
            unset($options['apiKey']);
        
//		$post_data['settings'] = base64_encode( serialize( $options ) );
		$post_data['settings'] = $this->simple_crypt('thisisakey', $options, 'encrypt' ); // to fix pass through sec rules of Dreamhost 	 	
		$post_data['encrypted'] = 1;
		
		$information           = apply_filters( 'mainwp_fetchurlauthed', $mainWPWordfenceExtensionActivator->get_child_file(), $mainWPWordfenceExtensionActivator->get_child_key(), $siteid, 'wordfence', $post_data );
		//print_r($information);
		if ( is_array( $information ) ) {
			$update  = array( 'site_id' => $siteid );
			$perform = false;
			if ( isset( $information['isPaid'] ) ) {
				$perform          = true;
				$update['isPaid'] = $information['isPaid'];
				$update['apiKey'] = $information['apiKey'];
			}
			if ( isset( $information['cacheType'] ) && $cacheType != $information['cacheType'] ) {
				$perform             = true;
				$update['cacheType'] = $information['cacheType'];
			}
			if ( $perform ) {
				MainWP_Wordfence_DB::get_instance()->update_setting( $update );
			}
		}
		return $information;
	}
    
	public function ajax_save_settings_reload() {
		$siteid = $_POST['siteId'];
		if ( empty( $siteid ) ) {
			die( 'Error reload.' );
		}

		new MainWP_Wordfence_Config_Site( $siteid );
		$is_Paid = MainWP_Wordfence_Config_Site::get( 'isPaid' );
		$api_Key = MainWP_Wordfence_Config_Site::get( 'apiKey' );
		?>
        <tr>
            <th>Wordfence API Key:</th>
			<td><input type="text" class="apiKey" name="apiKey[<?php echo $siteid; ?>]"
			           value="<?php echo esc_attr( $api_Key ); ?>" size="80"/>&nbsp;
				<?php if ( $is_Paid ) {  ?>
                    License Status: Premium Key. <span style="font-weight: bold; color: #0A0;">Premium scanning enabled!</span>
				<?php } else { ?>
                License Status: <span style="color: #F00; font-weight: bold;">Free Key</span>.
					<?php } ?>
            </td>
        </tr>
        <tr>
            <th>&nbsp;</th>
            <td>
				<?php if ( $is_Paid ) { ?>
                    <table border="0">
                        <tr>
                            <td><a href="https://www.wordfence.com/manage-wordfence-api-keys/" target="_blank"><input
                                        type="button" value="Renew your premium license"/></a></td>
                            <td>&nbsp;</td>
                            <td><input type="button" value="Downgrade to a free license"
							           onclick="MWP_WFAD.downgradeLicense(<?php echo $siteid; ?>);"/></td>
                        </tr>
                    </table>
				<?php } ?>
            </td>
        </tr>
		<?php
		die();
	}
        
        public function ajax_change_override_general_settings() {
            $siteid = isset( $_POST['siteId'] ) ? $_POST['siteId'] : false;
            if (empty($siteid))
                wp_send_json( array( 'error' => esc_html( __("Invalid data.") ) ) );
                
            		
            $override = isset( $_POST['override'] ) && $_POST['override'] ? 1 : 0;						
            MainWP_Wordfence_DB::get_instance()->update_setting( array(
                    'site_id'  => $siteid,
                    'override' => $override,

            ) );
            
            die( json_encode( array('ok' => 1) ) );
        }
        
        public function ajax_change_general_settings_use_premium() {                           
            $nonce = isset( $_POST['nonce'] ) ? $_POST['nonce'] : null;
            if ( empty($nonce) || ! wp_verify_nonce( $nonce, 'wfc-nonce' ) ) {
                die( json_encode( array('error' => 'Invalid request!') ) );
            }
            $value = isset( $_POST['value'] ) && $_POST['value'] ? 1 : 0;						
            update_option('mainwp_wordfence_use_premium_general_settings', $value);
            
            die( json_encode( array('ok' => 1) ) );
        }
                

         public function ajax_save_general_settings_to_child() {
            if ( !isset($_POST['nonce']) || !wp_verify_nonce( $_POST['nonce'], 'wfc-nonce' ) ) {
                wp_send_json( array( 'error' => esc_html( __("Invalid request") ) ) );
            }
            
            $siteid = isset( $_POST['siteId'] ) ? $_POST['siteId'] : false;
            if (empty($siteid))
                wp_send_json( array( 'error' => esc_html( __("Invalid data") ) ) );
            
            // to saving general settings from individual page, so save ALL settings
            $all_section = MainWP_Wordfence_Config::OPTIONS_TYPE_ALL; 
            $information = $this->perform_save_settings($siteid, true, $all_section);		
            die( json_encode( $information ) );            
        }
        
	public static function gen_listing_sites($do_action, $tab = '', $pSection = '') {
        
		global $mainWPWordfenceExtensionActivator;
		$websites  = apply_filters( 'mainwp-getsites', $mainWPWordfenceExtensionActivator->get_child_file(), $mainWPWordfenceExtensionActivator->get_child_key(), null );
		$sites_ids = array();
		if ( is_array( $websites ) ) {
			foreach ( $websites as $website ) {
				$sites_ids[] = $website['id'];
			}
		}

		$option               = array(
			'plugin_upgrades' => true,
			'plugins'         => true,
		);
		$dbwebsites           = apply_filters( 'mainwp-getdbsites', $mainWPWordfenceExtensionActivator->get_child_file(), $mainWPWordfenceExtensionActivator->get_child_key(), $sites_ids, array(), $option );
		$all_the_plugin_sites = array();
		foreach ( $dbwebsites as $website ) {
			if ( $website && $website->plugins != '' ) {
				$plugins = json_decode( $website->plugins, 1 );
				if ( is_array( $plugins ) && count( $plugins ) != 0 ) {
					foreach ( $plugins as $plugin ) {
						if ( 'wordfence/wordfence.php' == $plugin['slug'] ) {
							if ( $plugin['active'] ) {
								$all_the_plugin_sites[] = MainWP_Wordfence_Utility::map_site( $website, array(
									'id',
									'name',
								) );
								break;
							}
						}
					}
				}
			}
		}		
		if ( count( $all_the_plugin_sites ) > 0 ) {                    
                    echo '<div class="poststuff">';				
                        echo '<div class="postbox">';				
                            echo '<h3 class="mainwp_box_title"><span><i class="fa fa-cog"></i> ';
                                    
                                if ($do_action == 'save_settings') {
                                        echo __( 'Saving Settings to child sites ...', 'mainwp-wordfence-extension' );
                                } else if ($do_action == 'bulk_import') {
                                        echo __( 'Import Settings to child sites ...', 'mainwp-wordfence-extension' );
                                } else if ($do_action == 'save_firewall') {
                                        echo __( 'Saving Firewall Settings to child sites ...', 'mainwp-wordfence-extension' );
                                } else if ($do_action == 'save_caching_type') {				
                                        echo __( 'Saving type of caching to child sites ...', 'mainwp-wordfence-extension' );
                                } else if ($do_action == 'save_cache_options') {				
                                        echo __( 'Saving cache options to child sites ...', 'mainwp-wordfence-extension' );
                                }  else if ($do_action == 'clear_page_cache') {				
                                        echo __( 'Clearing page Cache on child sites ...', 'mainwp-wordfence-extension' );
                                }  else if ($do_action == 'get_cache_stats') {				
                                        echo __( 'Getting Cache Stats on child sites ...', 'mainwp-wordfence-extension' );
                                } else if ($do_action == 'add_cache_exclusion') {				
                                        echo __( 'Adding Cache Exclusion to child sites ...', 'mainwp-wordfence-extension' );				
                                        if (!isset($_GET['id']) || empty($_GET['id'])) {
                                                echo '<div class="mainwp_info-box-red">Error: empty cache exclution id.</div>';
                                                return;					
                                        }
                                        ?>
                                        <span id="mainwp_wfc_bulk_cache_exclusion_id" value="<?php echo esc_attr($_GET['id']); ?>"></span>
                                        <?php
                                } else if ($do_action == 'remove_cache_exclusion') {				
                                        echo __( 'Removing Cache Exclusion on child sites ...', 'mainwp-wordfence-extension' );				
                                        if (!isset($_GET['id']) || empty($_GET['id'])) {
                                                echo '<div class="mainwp_info-box-red">Error: empty cache exclution id.</div>';
                                                return;					
                                        }
                                        ?>
                                        <span id="mainwp_wfc_bulk_cache_exclusion_id" value="<?php echo esc_attr($_GET['id']); ?>"></span>
                                        <?php
                                } else if ($do_action == 'waf_update_rules') {				
                                        echo __( 'Refreshing firewall rules on child sites ...', 'mainwp-wordfence-extension' );
                                } else if ($do_action == 'save_debugging_options') {				
                                        echo __( 'Saving Debugging Options to child sites ...', 'mainwp-wordfence-extension' );
                                }  	

                                echo '</span></h3>';
                                echo '<div class="inside">';

                                foreach ( $all_the_plugin_sites as $website ) {
                                        echo '<div><strong>' . stripslashes( $website['name'] ) . '</strong>: ';
                                        echo '<span class="itemToProcess" siteid="' . $website['id'] . '" status="queue"><span class="loading" style="display: none"><i class="fa fa-spinner fa-pulse"></i></span> <span class="status">Queue</span><br />';
                                        echo '<div style="display: none" class="detailed"></div>';
                                        echo '</div><br />';
                                }
                        
                        echo '</div>';
                        echo '</div>';
			?>
            <div id="mainwp_wfc_perform_bulk_actions_ajax_message" class="mainwp_info-box-yellow hidden"></div>
            
            <input type="hidden" name="_post_saving_section" id="_post_saving_section" value="<?php echo $pSection; ?>"  />
            
            <script>
                jQuery(document).ready(function ($) {
	                <?php if ($do_action == 'save_settings') { ?>
                                    mainwp_wfc_save_setting_start_next();
	                <?php } else if ($do_action == 'bulk_import') { ?>
                                    mainwp_wfc_bulk_import_start_next('<?php echo esc_attr($_GET['token']); ?>');
	                <?php } else if ($do_action == 'save_firewall') { ?>
                                    mainwp_wfc_save_firewall_start_next();
	                <?php } else if ($tab == 'performance') { 
                                    // seem not used any more
                                ?>
                                    mainwp_wfc_bulk_performance_setup_start_next('<?php echo $do_action; ?>');
                        <?php } else if ($tab == 'diagnostics') { ?>
                                    mainwp_wfc_bulk_diagnostics_start_next('<?php echo $do_action; ?>');
                        <?php } ?>
						
                })
            </script>
			<?php
			return true;
		} else {
			echo '<div class="mainwp_info-box-yellow">' . __( 'No child sites with the Wordfence Security plugin installed.', 'mainwp-wordfence-extension' ) . '</div>';
			?>
            <script>
                jQuery(document).ready(function ($) {
                    setTimeout(function () {
                        location.href = 'admin.php?page=Extensions-Mainwp-Wordfence-Extension&tab=network_setting';
                    }, 3000);
                })
            </script>
			<?php
		}
	}

	public static function gen_settings_tab( $is_individual = false ) {            
           ?>
            
            <div class="mwp_wordfenceModeElem" id="mwp_wordfenceMode_settings"></div>            
            <?php
            if ($is_individual) {
                do_action( 'mainwp_do_meta_boxes', 'mainwp_wfc_postboxes_individual_settings'); 
            } else {
                do_action( 'mainwp_do_meta_boxes', 'mainwp_wfc_postboxes_settings'); 
            }                
            ?>
            <?php 
            
            // individual page have own Saving button
            if ( ! $is_individual ) {                
                self::gen_save_general_button();
            }            
            //self::gen_template_scripts();
	}
    
    public static function gen_save_general_button() {
        ?>
            <div>
                <input type="button" onclick="MWP_WFAD.saveConfig();" class="button-primary"
                        value="<?php _e( 'Save Settings', 'mainwp-wordfence-extension' ); ?>">					
                 <i class="fa fa-spinner fa-pulse wfcSaveOpts" style="display:none"></i>
                 <span class="wfSavedMsg">&nbsp;Your changes have been saved!</span>
            </div>
        <?php 
    }
    
    public static function gen_firewall_settings_tab( $is_individual = false ) {            
           ?>
            
            <div class="mwp_wordfenceModeElem" id="mwp_wordfenceMode_settings"></div>
            <div class="mwp_wfc_firewall_settings_form_content">
                <?php
                if ($is_individual) {
                    do_action( 'mainwp_do_meta_boxes', 'mainwp_wfc_postboxes_firewall'); 
                } else {
                    do_action( 'mainwp_do_meta_boxes', 'mainwp_wfc_postboxes_general_firewall'); 
                }

                // individual page have own Saving button
                if ( ! $is_individual ) {                
                    self::gen_save_general_button();
                }    
                ?>
            </div>
            <?php		
            //self::gen_template_scripts();
	}
    
    public static function gen_live_traffic_settings_tab( $is_individual = false )
    {            
           ?>
            
            <div class="mwp_wordfenceModeElem" id="mwp_wordfenceMode_settings"></div>
            <div class="mwp_wfc_scan_settings_form_content">
            <?php
            if ($is_individual) {                 
                do_action( 'mainwp_do_meta_boxes', 'mainwp_wfc_postboxes_traffic');
            } else {                
                do_action( 'mainwp_do_meta_boxes', 'mainwp_wfc_postboxes_general_traffic');  
            }
            
            // individual page have own Saving button
//            if ( ! $is_individual ) { 
//                 if (!isset($_GET['action'])) {
//                    self::gen_save_general_button();
//                 }
//            }    
            ?>               
            </div>
            <?php		
            //self::gen_template_scripts();
	}
    
    public static function gen_blocking_settings_tab( $is_individual = false )
    {
        do_action( 'mainwp_do_meta_boxes', 'mainwp_wfc_postboxes_blocking');
        
        // individual page have own Saving button
        if ( ! $is_individual ) {    
            if (!isset($_GET['action'])) {
                self::gen_save_general_button();
            }
        }         
    }
        
    public static function gen_scan_settings_tab( $is_individual = false ) {            
           ?>
            
            <div class="mwp_wordfenceModeElem" id="mwp_wordfenceMode_settings"></div>
            <div class="mwp_wfc_scan_settings_form_content">
            <?php
            if ($is_individual) {                 
                do_action( 'mainwp_do_meta_boxes', 'mainwp_wfc_postboxes_scans');
            } else {                
                do_action( 'mainwp_do_meta_boxes', 'mainwp_wfc_postboxes_general_scans');  
            }
          
            // individual page have own Saving button
            if ( ! $is_individual ) {                
                self::gen_save_general_button();
            }    
            ?>                   
            </div>
            <?php		
            //self::gen_template_scripts();
	}
    
    public static function gen_settings_licenses($post, $metabox = null) {                  
            $websites = isset($metabox['args']['websites']) ? $metabox['args']['websites'] : true;  
            $w =  isset($metabox['args']['w']) ? $metabox['args']['w'] : null;
            if (empty($w))
                return;
            
             $current_site_id = isset($_GET['id']) ? $_GET['id'] : 0;
                if (empty($current_site_id )) {
                    
                }
                            
            $is_Paids = $api_Keys = array();            
            if ($w) {
                $is_Paids = $w->get_isPaids();
                $api_Keys = $w->get_apiKeys();
            }
            
            $is_premium = get_option('mainwp_wordfence_use_premium_general_settings');
            
          ?>            
                <table>
                    <tbody>
                        <tr valign="top">
								<th scope="row" class="settinglabel">
									<?php _e( 'Use premium version', 'mainwp-wordfence-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Set to YES if you want to use with the Wordfence premium version.', 'mainwp-wordfence-extension' ) ); ?>
								</th>
								<td class="settingfield" id="wfc_row_use_premium_setting">
									<div class="mainwp-checkbox">                                      
										<input type="checkbox" id="mwp_wordfence_general_use_premium" name="mwp_wordfence_general_use_premium"  <?php echo ($is_premium ? 'checked="checked"' : ''); ?> value="yes"/>
										<label for="mwp_wordfence_general_use_premium"></label>
									</div>&nbsp;&nbsp;  
                                    <span id="wfc_change_use_premium_working"></div>
								</td>
							</tr>  
                            
            <?php if ( is_array( $websites ) && count( $websites ) > 0 ) {
                ?>
                        <tr>
                            <td colspan="2">License Key <a
                                    href="https://www.wordfence.com/wordfence-signup/" target="_blank">Click
                                    Here to Upgrade to Wordfence Premium now.</a>
                            </td>
                        </tr>
                        <?php
                        foreach ( $websites as $site_id => $site_name ) {
                                $is_Paid = isset( $is_Paids[ $site_id ] ) ? $is_Paids[ $site_id ] : 0;
                                $api_Key = isset( $api_Keys[ $site_id ] ) ? $api_Keys[ $site_id ] : '';
                                ?>
                                    <tr>
                                    <th><?php echo stripslashes( $site_name ); ?></th>
                                    <td><input type="text" class="apiKey" name="apiKey[<?php echo $site_id; ?>]"
                                               value="<?php echo esc_attr( $api_Key ); ?>" size="80"/>&nbsp;&nbsp;
                                    <?php if ( $is_Paid ) {  ?>
                                                License Status: Premium Key. <span
                                                    style="font-weight: bold; color: #0A0;">Premium scanning enabled!</span>
                                    <?php } else { ?>
                                            License Status: <span style="color: #F00; font-weight: bold;">Free Key</span>.
                                    <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                    <th>&nbsp;</th>
                                    <td>
                                    <?php if ( $is_Paid ) { ?>
                                                <table border="0">
                                                    <tr>
                                                        <td>
                                                            <a href="https://www.wordfence.com/manage-wordfence-api-keys/"
                                                               target="_blank"><input type="button"
                                                                                      value="Renew your premium license"/></a>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td><input type="button" value="Downgrade to a free license"
                                            onclick="MWP_WFAD.downgradeLicense(<?php echo $site_id; ?>);"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                    <?php } ?>
                                    </td>
                                </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                                <td colspan="2"><?php _e('No websites were found with the Wordfence plugin installed.', 'mainwp-wordfence-extension'); ?></td>
                        </tr>
                        <?php
                    }
                ?>
                    </tbody>
                </table>
                   
         <?php               
        }
                
    public static function gen_settings_individual_licenses($post, $metabox = null) {
            $current_site_id = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : 0; 
            
            $is_Paid = MainWP_Wordfence_Config_Site::get( 'isPaid', 0 );
            $api_Key = MainWP_Wordfence_Config_Site::get( 'apiKey', '' );
            
            ?>
            <table class="mwp-wf-form-table">
                   <tbody id="mwp_wfc_license_body">
                   <tr>
                       <th>Wordfence API Key:</th>
                        <td><input type="text" class="apiKey" name="apiKey[<?php echo $current_site_id; ?>]"
                            value="<?php echo esc_attr( $api_Key ); ?>" size="80"/>&nbsp;&nbsp;
                        <?php if ( $is_Paid ) {  ?>
                               License Status: Premium Key. <span
                                   style="font-weight: bold; color: #0A0;">Premium scanning enabled!</span>
                        <?php } else { ?>
                           License Status: <span style="color: #F00; font-weight: bold;">Free Key</span>                               .
                        <?php } ?>
                       </td>
                   </tr>
                   <tr>
                       <th>&nbsp;</th>
                       <td>
                    <?php if ( $is_Paid ) { ?>
                       <table border="0">
                           <tr>
                               <td><a href="https://www.wordfence.com/manage-wordfence-api-keys/"
                                      target="_blank"><input type="button"
                                                             value="Renew your premium license"/></a>
                               </td>
                               <td>&nbsp;</td>
                               <td><input type="button" value="Downgrade to a free license"
                                    onclick="MWP_WFAD.downgradeLicense(<?php echo $current_site_id; ?>);"/>
                               </td>
                           </tr>
                       </table>
                    <?php } ?>
                       </td>
                   </tr>
                   </tbody>
            </table>                    
            <?php			
        }
        
        public static function gen_settings_scan_schedule($post, $metabox = null) {
            $current_site_id = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : 0;                          
            $w = isset($metabox['args']['w']) ? $metabox['args']['w'] : null;   
            if (empty($w))
                return;
            
            $override = 0;
            if ($current_site_id) {
                $override = $w->is_override();                
            }                        
            ?>            
            <table class="mwp-wf-form-table">
                <tbody>  
                <?php if ($current_site_id) { ?>     
                    <tr>
                         <th><?php _e( 'Override General Settings', 'mainwp-wordfence-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Set to YES if you want to overwrite global wordfence options.', 'mainwp-wordfence-extension' ) ); ?></th>
                        <td>
                            <div class="mainwp-checkbox">
                                <input type="checkbox" id="mainwp_wfc_override_global_setting"
                                        name="mainwp_wfc_override_global_setting" <?php echo( 0 == $override ? '' : 'checked="checked"' ); ?>
                                       value="1"/>
                                <label for="mainwp_wfc_override_global_setting"></label>
                            </div>
                            <span class="wfc_change_override_working"></div>
                        </td>
                    </tr>
                    <tr>
                        <th>&nbsp;</th>
                        <td>&nbsp;</td>
                    </tr>       
                <?php } ?>  
                <tr>
                    <th>Scan Schedule</th>
                    <td>
                        <select id="scheduleScan" name="scheduleScan">
                                                                <option value=""<?php $w->sel( 'scheduleScan', '' ); ?>>N/A</option>
                                                                <option value="twicedaily"<?php $w->sel( 'scheduleScan', 'twicedaily' ); ?>>Twice a
                                Day
                            </option>
                                                                <option value="daily"<?php $w->sel( 'scheduleScan', 'daily' ); ?>>Once a Day
                            </option>
                                                                <option value="weekly"<?php $w->sel( 'scheduleScan', 'weekly' ); ?>>Once a Week
                            </option>
                                                                <option value="monthly"<?php $w->sel( 'scheduleScan', 'monthly' ); ?>>Once a Month
                            </option>
                        </select>                       
                    </td>
                </tr>
                </tbody>
            </table>                	
        <?php            
        }
        
        
        public static function gen_settings_basic($post, $metabox = null) {
            $current_site_id = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : 0; 
            $w = isset($metabox['args']['w']) ? $metabox['args']['w'] : null;
            if (empty($w))
                return;
            if ($current_site_id) {
                $is_Paid = MainWP_Wordfence_Config_Site::get( 'isPaid', 0 );
            } else {
                $is_Paid = $is_premium = get_option('mainwp_wordfence_use_premium_general_settings');
            }
                
            
            ?>               
         
            <table class="mwp-wfc-settings mwp-wf-form-table">
                <tbody>
                <tr>
                    <th class="wfConfigEnable">Update Wordfence automatically when a new version is released?
                    </th>
                    <td><input type="checkbox" id="autoUpdate" class="wfConfigElem" name="autoUpdate"
                                                           value="1" <?php $w->cb( 'autoUpdate' ); ?> />&nbsp;Automatically updates
                        Wordfence to the newest version within 24 hours of a new release.
                    </td>
                </tr>
                <tr>
                    <th>Where to email alerts:</th>
                    <td><input type="text" id="alertEmails" class="mwp-wf-form-control" name="alertEmails"
                                                           value="<?php $w->f( 'alertEmails' ); ?>" size="50"/><span
                            class="wfTipText">Separate multiple emails with commas</span></td>
                </tr>                
                <tr>
                    <th>How does Wordfence get IPs:</th>
                    <td>                               
                    <select id="howGetIPs" name="howGetIPs" class="mwp-wf-form-control">
                            <option value="">Let Wordfence use the most secure method to get visitor IP addresses. Prevents
                                    spoofing and works with most sites.
                            </option>
                            <option value="REMOTE_ADDR"<?php $w->sel( 'howGetIPs', 'REMOTE_ADDR' ); ?>>Use PHP's built in
                                    REMOTE_ADDR and don't use anything else. Very secure if this is compatible with your site.
                            </option>
                            <option value="HTTP_X_FORWARDED_FOR"<?php $w->sel( 'howGetIPs', 'HTTP_X_FORWARDED_FOR' ); ?>>Use
                                    the X-Forwarded-For HTTP header. Only use if you have a front-end proxy or spoofing may
                                    result.
                            </option>
                            <option value="HTTP_X_REAL_IP"<?php $w->sel( 'howGetIPs', 'HTTP_X_REAL_IP' ); ?>>Use the
                                    X-Real-IP HTTP header. Only use if you have a front-end proxy or spoofing may result.
                            </option>
                            <option value="HTTP_CF_CONNECTING_IP"<?php $w->sel( 'howGetIPs', 'HTTP_CF_CONNECTING_IP' ); ?>>
                                    Use the Cloudflare "CF-Connecting-IP" HTTP header to get a visitor IP. Only use if you're
                                    using Cloudflare.
                            </option>
                    </select>
                    <span class="wf-help-block"><a href="#" class="mwp-do-show" data-selector="#howGetIPs_trusted_proxies">+ Edit trusted proxies</a></span>
                    </td>
                </tr>
                <tr class="hidden" id="howGetIPs_trusted_proxies">
                    <th>Trusted proxies</th>
                    <td>
                        <div class="wf-col-sm-7">
                            <textarea class="mwp-wf-form-control" rows="4" name="howGetIPs_trusted_proxies" id="howGetIPs_trusted_proxies_field"><?php echo $w->getHTML('howGetIPs_trusted_proxies'); ?></textarea>
                            <span class="wf-help-block">These IPs (or CIDR ranges) will be ignored when determining the requesting IP via the X-Forwarded-For HTTP header. Enter one IP or CIDR range per line.</span>

                            <?php if (false && $current_site_id ) { ?>
                                <script type="application/javascript">
                                    (function($) {
                                        var updateIPPreview = function() {
                                            MWP_WFAD.updateIPPreview({howGetIPs: $('#howGetIPs').val(), 'howGetIPs_trusted_proxies': $('#howGetIPs_trusted_proxies_field').val()}, function(ret) {
                                                if (ret && ret.ok) {
                                                    $('#howGetIPs-preview-all').html(ret.ipAll);
                                                    $('#howGetIPs-preview-single').html(ret.ip);
                                                }
                                                else {
                                                    //TODO: implementing testing whether or not this setting will lock them out and show the error saying that they'd lock themselves out
                                                }
                                            });
                                        };

                                        $('#howGetIPs').on('change', function() {
                                            updateIPPreview();
                                        });

                                        var coalescingUpdateTimer;
                                        $('#howGetIPs_trusted_proxies_field').on('keyup', function() {
                                            clearTimeout(coalescingUpdateTimer);
                                            coalescingUpdateTimer = setTimeout(updateIPPreview, 1000);
                                        });
                                    })(jQuery);
                                </script>
                            <?php } ?>
                        </div>
                    </td>
                   </tr>
               <tr>
                    <th>Hide WordPress version</th>
                    <td><input type="checkbox" id="other_hideWPVersion" class="wfConfigElem"
                               name="other_hideWPVersion" value="1" <?php $w->cb( 'other_hideWPVersion' ); ?> />
                    </td>
                </tr>  
                <tr>
                    <th><label for="disableCodeExecutionUploads">Disable Code Execution for Uploads directory</label></th>
                    <td><input type="checkbox" id="disableCodeExecutionUploads" class="wfConfigElem"
                               name="disableCodeExecutionUploads"
                               value="1" <?php $w->cb( 'disableCodeExecutionUploads' ); ?> /></td>
                </tr>
                <tr>
                            <th>Disable Wordfence Cookies</th>
                            <td><input type="checkbox" id="disableCookies" class="wfConfigElem" name="disableCookies"
							           value="1" <?php $w->cb( 'disableCookies' ); ?> />(when enabled all visits in live
                                traffic will appear to be new visits)
                            </td>
                        </tr>       
					<tr>
                            <th>Pause live updates when window loses focus</th>
                            <td><input type="checkbox" id="liveActivityPauseEnabled" class="wfConfigElem" name="liveActivityPauseEnabled"
							           value="1" <?php $w->cb( 'liveActivityPauseEnabled' ); ?> />
                            </td>
                        </tr>  	
                <tr>
                    <th>Update interval in seconds (2 is default)</th>
                    <td><input type="text" id="actUpdateInterval" name="actUpdateInterval"
                               value="<?php $w->f( 'actUpdateInterval' ); ?>" size="4"/>Setting higher will
                        reduce browser traffic but slow scan starts, live traffic &amp; status updates.
                    </td>
                </tr>  
                
                <tr>
                    <th>Bypass the LiteSpeed "noabort" check </th>
                    <td><input type="checkbox" id="other_bypassLitespeedNoabort" class="wfConfigElem" name="other_bypassLitespeedNoabort"
                               value="1" <?php $w->cb( 'other_bypassLitespeedNoabort' ); ?> />
                    </td>
                </tr>      
                <tr>
                    <th>Delete Wordfence tables and data on deactivation?</th>
                    <td><input type="checkbox" id="deleteTablesOnDeact" class="wfConfigElem"
                               name="deleteTablesOnDeact" value="1" <?php $w->cb( 'deleteTablesOnDeact' ); ?> />
                    </td>
                </tr>                       
                </tbody>
            </table>               
            <?php
        }
        
        public static function gen_settings_alerts($post, $metabox = null) {
             $current_site_id = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : 0;     
             $w = isset($metabox['args']['w']) ? $metabox['args']['w'] : null;
             if (empty($w))
                return;
            ?>
            <table class="mwp-wf-form-table">
                        <tbody>
						<?php
						$emails = $w->get_AlertEmails();
						if ( sizeof( $emails ) < 1 ) {
							echo "<tr><th colspan=\"2\" style=\"color: #F00;\">You have not configured an email to receive alerts yet. Set this up under \"Basic Options\" above.</th></tr>\n";
						}
						?>
                        <tr>
                            <th>Email me when Wordfence is automatically updated</th>
                            <td><input type="checkbox" id="alertOn_update" class="wfConfigElem" name="alertOn_update"
							           value="1" <?php $w->cb( 'alertOn_update' ); ?>/>&nbsp;If you have automatic
                                updates enabled (see above), you'll get an email when an update occurs.
                            </td>
                        </tr>
                        <tr>
                                <th>Email me if Wordfence is deactivated</th>
                                <td><input type="checkbox" id="alertOn_wordfenceDeactivated" class="wfConfigElem" name="alertOn_wordfenceDeactivated"
                                                   value="1" <?php $w->cb( 'alertOn_wordfenceDeactivated' ); ?>/>
                                </td>
                        </tr>
                        <tr>
                            <th>Alert on critical problems</th>
                            <td><input type="checkbox" id="alertOn_critical" class="wfConfigElem"
							           name="alertOn_critical" value="1" <?php $w->cb( 'alertOn_critical' ); ?>/></td>
                        </tr>
                        <tr>
                            <th>Alert on warnings</th>
                            <td><input type="checkbox" id="alertOn_warnings" class="wfConfigElem"
							           name="alertOn_warnings" value="1" <?php $w->cb( 'alertOn_warnings' ); ?>/></td>
                        </tr>
                        <tr>
                            <th>Alert when an IP address is blocked</th>
                            <td><input type="checkbox" id="alertOn_block" class="wfConfigElem" name="alertOn_block"
							           value="1" <?php $w->cb( 'alertOn_block' ); ?>/></td>
                        </tr>
                        <tr>
                            <th>Alert when someone is locked out from login</th>
                            <td><input type="checkbox" id="alertOn_loginLockout" class="wfConfigElem"
                                       name="alertOn_loginLockout"
							           value="1" <?php $w->cb( 'alertOn_loginLockout' ); ?>/></td>
                        </tr>
						 <tr>
                            <th>Alert when someone is blocked from logging in for using a password found in a breach</th>
                            <td><input type="checkbox" id="alertOn_breachLogin" class="wfConfigElem"
                                       name="alertOn_breachLogin"
							           value="1" <?php $w->cb( 'alertOn_breachLogin' ); ?>/></td>
                        </tr>			
                        <tr>
                            <th>Alert when the "lost password" form is used for a valid user</th>
                            <td><input type="checkbox" id="alertOn_lostPasswdForm" class="wfConfigElem"
                                       name="alertOn_lostPasswdForm"
							           value="1" <?php $w->cb( 'alertOn_lostPasswdForm' ); ?>/></td>
                        </tr>
                        <tr>
                            <th>Alert me when someone with administrator access signs in</th>
                            <td><input type="checkbox" id="alertOn_adminLogin" class="wfConfigElem"
							           name="alertOn_adminLogin" value="1" <?php $w->cb( 'alertOn_adminLogin' ); ?>/>
                            </td>
                        </tr>
                        <tr>
                                <th style="color: #666666;padding-left: 20px;">Only alert me when that administrator signs in from a new device or location</th>
                                <td><input type="checkbox" id="alertOn_firstAdminLoginOnly" class="wfConfigElem" name="alertOn_firstAdminLoginOnly"
                                                   value="1" <?php $w->cb( 'alertOn_firstAdminLoginOnly' ); ?>/></td>
                        </tr>
                        <tr>
                            <th>Alert me when a non-admin user signs in</th>
                            <td><input type="checkbox" id="alertOn_nonAdminLogin" class="wfConfigElem"
                                       name="alertOn_nonAdminLogin"
							           value="1" <?php $w->cb( 'alertOn_nonAdminLogin' ); ?>/></td>
                        </tr>
                        <tr>
                                <th style="color: #666666;padding-left: 20px;">Only alert me when that user signs in from a new device or location</th>
                                <td><input type="checkbox" id="alertOn_firstNonAdminLoginOnly" class="wfConfigElem" name="alertOn_firstNonAdminLoginOnly"
                                                   value="1" <?php $w->cb( 'alertOn_firstNonAdminLoginOnly' ); ?>/></td>  
                        </tr>
                        <tr>
                                <th>Alert me when there's a large increase in attacks detected on my site</th>
                                <td><input type="checkbox" id="wafAlertOnAttacks" class="wfConfigElem"
                                                   name="wafAlertOnAttacks" value="1" <?php $w->cb( 'wafAlertOnAttacks' ); ?>/></td>
                        </tr>
                        <tr>
                            <th>Maximum email alerts to send per hour</th>
                            <td>&nbsp;<input type="text" id="alert_maxHourly" name="alert_maxHourly"
							                 value="<?php $w->f( 'alert_maxHourly' ); ?>" size="4"/> 0 means unlimited alerts will be sent.
                            </td>
                        </tr>
                        </tbody>
                    </table>               
            <?php
        }
        
        public static function gen_settings_email($post, $metabox = null) {
            $current_site_id = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : 0;   
            $w = isset($metabox['args']['w']) ? $metabox['args']['w'] : null;
            if (empty($w))
                return;
        ?>

            <table class="mwp-wf-form-table">
                        <tbody>
                        <tr>
                            <th><?php _e( 'Enable email summary:', 'mainwp-wordfence-extension' ); ?></th>
                            <td>&nbsp;<input type="checkbox" id="email_summary_enabled" name="email_summary_enabled"
							                 value="1" <?php $w->cb( 'email_summary_enabled' ); ?> />
                            </td>
                        </tr>
                        <tr>
                            <th>Email summary frequency:</th>
                            <td>
                                <select id="email_summary_interval" class="wfConfigElem" name="email_summary_interval">
									<option value="daily"<?php $w->sel( 'email_summary_interval', 'daily' ); ?>>Once a
                                        day
                                    </option>
									<option value="weekly"<?php $w->sel( 'email_summary_interval', 'weekly' ); ?>>
                                        Once a weeks
                                    </option>
									<option value="monthly"<?php $w->sel( 'email_summary_interval', 'monthly' ); ?>>Once
                                        a month
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>List of directories to exclude from recently modified file list:</th>
                            <td>                                
                                <textarea id="email_summary_excluded_directories" name="email_summary_excluded_directories" class="mwp-wf-form-control" rows="4"><?php echo esc_html(MainWP_Wordfence_Utility::cleanupOneEntryPerLine($w->get('email_summary_excluded_directories', ''))); ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th>Enable activity report widget on the WordPress dashboard:</th>
                            <td>&nbsp;<input type="checkbox" id="email_summary_dashboard_widget_enabled"
                                             name="email_summary_dashboard_widget_enabled"
							                 value="1" <?php $w->cb( 'email_summary_dashboard_widget_enabled' ); ?> />
                            </td>
                        </tr>
                        </tbody>
                    </table>                
        <?php
        }
        
        public static function gen_dashboard_notification_options($post, $metabox = null) {
            $current_site_id = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : 0;  
            $w = isset($metabox['args']['w']) ? $metabox['args']['w'] : null;
            if (empty($w))
                return;
            
            if ($current_site_id) {
                $is_Paid = MainWP_Wordfence_Config_Site::get( 'isPaid', 0 );
            } else {
                $is_Paid = $is_premium = get_option('mainwp_wordfence_use_premium_general_settings');
            }
            
            ?>
             <table class="mwp-wf-form-table">
                <tbody>
                    <tr>
                        <th>Updates Needed (Plugin, Theme, or Core)</th>
                        <td>
                           <input type="checkbox" id="notification_updatesNeeded" name="notification_updatesNeeded" value="1" <?php $w->cb('notification_updatesNeeded'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <th>Security Alerts</th>
                        <td>
                           <input type="checkbox" id="notification_securityAlerts"<?php if ($is_Paid) { echo ' name="notification_securityAlerts"'; } ?> value="1" <?php if ($is_Paid) { $w->cb('notification_securityAlerts'); } else { echo ' checked disabled'; } ?>></div>
                    <?php if (!$is_Paid): ?>
                        <span class="wf-help-block"><span style="color: #F00;">Premium Option</span> This option requires a Wordfence Premium Key.</span>
                        <?php if ($w->get('notification_securityAlerts')): ?><input type="hidden" name="notification_securityAlerts" value="<?php $w->f('notification_securityAlerts'); ?>"><?php endif; ?>
                    <?php endif; ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <th>Promotions</th>
                        <td>
                           <input type="checkbox" id="notification_promotions"<?php if ($is_Paid) { echo ' name="notification_promotions"'; } ?> value="1" <?php if ($is_Paid) { $w->cb('notification_promotions'); } else { echo ' checked disabled'; } ?>></div>
                    <?php if (!$is_Paid): ?>
                        <span class="wf-help-block"><span style="color: #F00;">Premium Option</span> This option requires a Wordfence Premium Key.</span>
                        <?php if ($w->get('notification_promotions')): ?><input type="hidden" name="notification_promotions" value="<?php $w->f('notification_promotions'); ?>"><?php endif; ?>
                    <?php endif; ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <th>Blog Highlights</th>
                        <td>
                           <input type="checkbox" id="notification_blogHighlights"<?php if ($is_Paid) { echo ' name="notification_blogHighlights"'; } ?> value="1" <?php if ($is_Paid) { $w->cb('notification_blogHighlights'); } else { echo ' checked disabled'; } ?>></div>
                    <?php if (!$is_Paid): ?>
                        <span class="wf-help-block"><span style="color: #F00;">Premium Option</span> This option requires a Wordfence Premium Key.</span>
                        <?php if ($w->get('notification_blogHighlights')): ?><input type="hidden" name="notification_blogHighlights" value="<?php $w->f('notification_blogHighlights'); ?>"><?php endif; ?>
                    <?php endif; ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <th>Product Updates</th>
                        <td>
                           <input type="checkbox" id="notification_productUpdates"<?php if ($is_Paid) { echo ' name="notification_productUpdates"'; } ?> value="1" <?php if ($is_Paid) { $w->cb('notification_productUpdates'); } else { echo ' checked disabled'; } ?>></div>
                    <?php if (!$is_Paid): ?>
                        <span class="wf-help-block"><span style="color: #F00;">Premium Option</span> This option requires a Wordfence Premium Key.</span>
                        <?php if ($w->get('notification_productUpdates')): ?><input type="hidden" name="notification_productUpdates" value="<?php $w->f('notification_productUpdates'); ?>"><?php endif; ?>
                    <?php endif; ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <th>Scan Status</th>
                        <td>
                           <input type="checkbox" id="notification_scanStatus" name="notification_scanStatus" value="1" <?php $w->cb('notification_scanStatus'); ?>>
                        </td>
                    </tr>
                    
                </tbody>
            </table>
            <?php
        }
        public static function gen_comment_spam_filter_settings( $post, $metabox = null ) {
            $current_site_id = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : 0;  
            $w = isset($metabox['args']['w']) ? $metabox['args']['w'] : null;
            if (empty($w))
                return;            

            if ($current_site_id) {
                $is_Paid = MainWP_Wordfence_Config_Site::get( 'isPaid', 0 );
            } else {
                $is_Paid = $is_premium = get_option('mainwp_wordfence_use_premium_general_settings');
            }
            
            ?>
         
                    <table class="mwp-wfc-settings mwp-wf-form-table">
                        <tbody>                        
                        <tr>
                            <th>Hold anonymous comments using member emails for moderation</th>
                            <td><input type="checkbox" id="other_noAnonMemberComments" class="wfConfigElem"
                                       name="other_noAnonMemberComments"
							           value="1" <?php $w->cb( 'other_noAnonMemberComments' ); ?> />
                            <br/>Blocks when the comment is posted without being logged in, but provides an email address for the registered user
                            </td>
                        </tr>
                        <tr>
                            <th>Filter comments for malware and phishing URL's</th>
                            <td><input type="checkbox" id="other_scanComments" class="wfConfigElem"
							           name="other_scanComments" value="1" <?php $w->cb( 'other_scanComments' ); ?> />
                                <br/>Blocks when a comment contains a URL on a domain blacklist
                            </td>
                        </tr>                        
                        <tr>
                            <th class="wfConfigEnable">Advanced Comment Spam Filter</th>
                            <td><input type="checkbox" id="advancedCommentScanning" class="wfConfigElem"
                                       name="advancedCommentScanning" value="1"
                                        <?php $w->cbp( 'advancedCommentScanning' , $is_Paid );
                                        if ( ! $is_Paid ) {  ?>onclick="alert('This is a paid feature because it places significant additional load on our servers.'); jQuery('#advancedCommentScanning').attr('checked', false); return false;" <?php } ?> />&nbsp;<span
                                    style="color: #F00;">Premium Feature</span><br/> In addition to free comment filtering
                                (see below) this option filters comments against several additional real-time lists of
                                known spammers and infected hosts.
                            </td>
                        </tr>
                        </tbody>
                    </table>
                
        <?php
        }
        
        public static function gen_settings_import_settings($post, $metabox = null) {
            $current_site_id = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : 0;  
            
            ?>
            <table class="mwp-wf-form-table">
		                <tbody>
		                <?php if ( $current_site_id ) { ?>
		                    <tr>
		                        <th>Export this site's Wordfence settings for import on another site:</th>
		                        <td><input type="button" class="button" id="exportSettingsBut" value="Export Wordfence Settings"
                                            onclick="MWP_WFAD.exportSettings(this, <?php echo $current_site_id; ?>); return false;"/></td>
		                    </tr>
		                <?php } ?>
		                    <tr>
		                        <th>Import Wordfence settings from another site using a token:</th>
			                    <td>
                                            <input type="text" size="20" value="" id="importToken"/>&nbsp;
                                        <?php if ($current_site_id) { ?>
                                            <input type="button" class="button"
                                             name="importSettingsButton"
                                             value="Import Settings"
                                             onclick="MWP_WFAD.importSettings(this, jQuery('#importToken').val(), <?php echo $current_site_id; ?>); return false;"/>
	                            <?php } else { ?>
			                    <input type="button" class="button"
	                                     name="importSettingsButton"
	                                     value="Bulk Import Settings"
	                                     onclick="MWP_WFAD.onBulkImportSettings(jQuery('#importToken').val()); return false;"/>
	                            <?php } ?>
		                        </td>
		                    </tr>
		                </tbody>
                    </table>
               
            <?php
        }
           
        public static function gen_diagnostics_tab( $pIndividual = false ) {
            
        ?>		
            <style type="text/css">	
                        table.wf-table {
                                width: 100%;
                                max-width: 100%;
                                border-collapse: collapse;
                        }
                        table.wf-table th,
                        table.wf-table td {
                                padding: 6px 4px;
                                border: 1px solid #ccc;
                        }
                        table.wf-table thead th,
                        table.wf-table thead td,
                        table.wf-table tfoot th,
                        table.wf-table tfoot td,
                        table.wf-table tbody.thead th,
                        table.wf-table tbody.thead td {
                                background-color: #222;
                                color: #fff;
                                font-weight: bold;
                                border-color: #474747;
                                text-align: left;
                        }
                        table.wf-table tbody tr.even td,
                        table.wf-table tbody tr:nth-child(2n) td {
                                background-color: #eee;
                        }
                        table.wf-table tbody tr td,
                        table.wf-table tbody tr.odd td {
                                background-color: #fff;
                        }
                        table.wf-table tbody tr:hover > td {
                                background-color: #fffbd8;
                        }
                        table.wf-table tbody.empty-row tr td {
                                border-width: 0;
                                padding: 8px 0;
                                background-color: transparent;
                        }


                        .wf-table td.error {
                                color: #d0514c;
                                font-weight: bold;
                        }
                        .wf-table td.success:before,
                        .wf-table td.error:before {
                                font-size: 16px;
                                display: inline-block;
                                margin: 0px 8px 0px 0px;
                        }
                        .wf-table td.error:before {
                                content: "\2718";
                        }
                        .wf-table td.success {
                                color: #008c10;
                                font-weight: bold;
                                max-width: 20%;
                        }
                        .wf-table td.success:before {
                                content: "\2713";
                        }
                        .wf-table td.inactive {
                                font-weight: bold;
                                color: #666666;
                        }

            </style>				
            <?php
            
            if ($pIndividual) {
                do_action( 'mainwp_do_meta_boxes', 'mainwp_wfc_postboxes_individual_diagnostics'); 
            } else {
                do_action( 'mainwp_do_meta_boxes', 'mainwp_wfc_postboxes_diagnostics'); 
            }
            // individual page have own Saving button
            if ( ! $pIndividual ) {                
                self::gen_save_general_button();
            } 
            
	}
        
        public static function gen_diagnostics_result($post, $metabox = null) {
            $current_site_id = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : 0;  
            $websites =  isset($metabox['args']['websites']) ? $metabox['args']['websites'] : 0;  
            ?>
            <?php if (!$current_site_id) { 					
                        if (!is_array($websites) || count($websites) == 0){
                                echo __( 'No websites were found with the Wordfence plugin installed', 'mainwp-wordfence-extension' );
                        } else {
                        ?>
                        <?php _e( 'Select Child Site: ', 'mainwp' ); ?>
                        <select name="" id="mainwp_wfc_diagnostic_info" style="margin-right: 2em">
                                <option value="-1"><?php _e( 'Select Child Site', 'mainwp' ); ?></option>
                                <?php
                                foreach($websites as $_id => $_site)
                                {
                                        echo '<option value="'.$_id.'">' . stripslashes($_site) . '</option>';
                                }					
                                ?>
                        </select>
                    <?php	} ?>
                    <br/><br/>
            <?php } else { ?>				
                     <script>
                        jQuery(document).ready(function ($) {
                            MWP_WFAD.getDiagnostics(<?php echo $current_site_id; ?>);  
                        });
                     </script>   
            <?php } ?>
            <div id="mainwp_wfc_diagnostics_child_loading" style="display: none"><i class="fa fa-spinner fa-pulse"></i> <?php _e( 'Loading ...', 'mainwp' ); ?></div>
            <div id="mainwp_diagnostics_child_resp"></div>
            
	<?php    
        }
        
        public static function gen_diagnostics_other_tests($post, $metabox = null) {
                $current_site_id = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : 0;  
            	if (empty($current_site_id))
                    return;		
                $open_url = 'admin.php?page=Extensions-Mainwp-Wordfence-Extension&action=open_site';					
                $conf_href = $open_url . "&websiteid=" . $current_site_id . "&open_location=" . base64_encode( "?_wfsf=sysinfo&nonce=child_temp_nonce" );			
                $test_href = $open_url . "&websiteid=" . $current_site_id . "&open_location=" . base64_encode( "?_wfsf=testmem&nonce=child_temp_nonce" );

                ?>
				
                <ul>
                        <li>
                                <a id="mwp_wfc_system_conf_lnk" href="<?php echo $open_url . $conf_href; ?>"
                                   target="_blank">Click to view your system's configuration in a new window</a>
                                </li>
                        <li>
                                <a id="mwp_wfc_test_mem_lnk" href="<?php echo $open_url . $test_href; ?>"
                                   target="_blank">Test your WordPress host's available memory</a>
                                </a>
                        </li>			
                </ul>
		
            <?php
        }
        
        public static function gen_diagnostics_debugging_options($post, $metabox = null) {            
            $current_site_id = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : 0;  
            $w = isset($metabox['args']['w']) ? $metabox['args']['w'] : null;
            if (empty($w))
                return;
        ?>
                    <form method="post" id="wfDebuggingConfigForm"
                            action="admin.php?page=Extensions-Mainwp-Wordfence-Extension&tab=diagnostics">
                                    <table class="mwp_wfConfigForm">                                         
                                            <tr>
                                                    <th>Enable debugging mode (increases database load)</th>
                                                    <td><input type="checkbox" id="debugOn" class="wfConfigElem" name="debugOn"
                                                                       value="1" <?php $w->cb( 'debugOn' ); ?> /></td>
                                            </tr>
                                            <tr>
                                                    <th>Start all scans remotely</th>
                                                    <td><input type="checkbox" id="startScansRemotely" class="wfConfigElem"
                                                                       name="startScansRemotely" value="1" <?php $w->cb( 'startScansRemotely' ); ?> />(Try
                                                            this if your scans aren't starting and your site is publicly accessible)
                                                    </td>
                                            </tr>          
                                            <tr>
                                                    <th><label for="ssl_verify">Enable SSL Verification</label>
                                                    </th>
                                                    <td style="vertical-align: top;"><input type="checkbox" id="ssl_verify" class="wfConfigElem"
                                                            name="ssl_verify"
                                                            value="1" <?php $w->cb( 'ssl_verify' ); ?> />
                                                            (Disable this if you are <strong><em>consistently</em></strong> unable to connect to the
                                                            Wordfence servers.)
                                                    </td>
                                            </tr>
                                            <tr>
                                                    <th><label for="betaThreatDefenseFeed">Enable beta threat defense feed</label></th>
                                                    <td style="vertical-align: top;"><input type="checkbox" id="betaThreatDefenseFeed"
                                                        class="wfConfigElem"
                                                        name="betaThreatDefenseFeed"
                                                        value="1" <?php $w->cb('betaThreatDefenseFeed'); ?> />
                                                    </td>
                                            </tr>

                            </table>
                            <br>
<!--                            <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                            <td><input type="button" id="button1" name="button1" class="button-primary" value="Save Changes"
                                                               onclick="MWP_WFAD.saveDebuggingConfig(<?php echo $current_site_id; ?>, <?php echo $current_site_id ? 1 : 0; ?>);"/></td>
                                            <td style="height: 24px;">
                                                    <div class="wfAjax24"></div>
                                                    <span class="wfSavedMsg">&nbsp;Your changes have been saved!</span></td>
                                    </tr>
                            </table>-->
            </form>
		
	<?php
        }
   
        static function gen_template_scripts() {
		?>
        <script type="text/x-jquery-template" id="wfContentBasicOptions">
            <div>
                <h3>Basic Options</h3>

                <p>
                    Using Wordfence is simple. Install Wordfence, enter an email address on this page to send alerts to,
                    and then do your first scan and work through the security alerts we provide.
                    We give you a few basic security levels to choose from, depending on your needs. Remember to hit the
                    "Save" button to save any changes you make.
                </p>

                <p>
                    If you use the free edition of Wordfence, you don't need to worry about entering an API key in the
                    "API Key" field above. One is automatically created for you. If you choose to <a
                        href="https://www.wordfence.com/wordfence-signup/" target="_blank">upgrade to Wordfence Premium
                        edition</a>, you will receive an API key. You will need to copy and paste that key into the "API
                    Key"
                    field above and hit "Save" to activate your key.
                </p>
            </div>
        </script>
        <script type="text/x-jquery-template" id="wfContentScansToInclude">
            <div>
                <h3>Scans to Include</h3>

                <p>
                    This section gives you the ability to fine-tune what we scan.
                    If you use many themes or plugins from the public WordPress directory we recommend you
                    enable theme and plugin scanning. This will verify the integrity of all these themes and plugins and
                    alert you of any changes.

                <p>

                <p>
                    The option to "scan files outside your WordPress installation" will cause Wordfence to do a much
                    wider security scan
                    that is not limited to your base WordPress directory and known WordPress subdirectories. This scan
                    may take longer
                    but can be very useful if you have other infected files outside this WordPress installation that you
                    would like us to look for.
                </p>
            </div>
        </script>

        <script type="text/x-jquery-template" id="wfContentLoginSecurity">
            <div>
                <h3>Login Security</h3>

                <p>
                    We have found that real brute force login attacks make hundreds or thousands of requests trying to
                    guess passwords or user login names.
                    So in general you can leave the number of failed logins before a user is locked out as a fairly high
                    number.
                    We have found that blocking after 20 failed attempts is sufficient for most sites and it allows your
                    real site users enough
                    attempts to guess their forgotten passwords without getting locked out.
                </p>
            </div>
        </script>
		<?php
	}
	
}
