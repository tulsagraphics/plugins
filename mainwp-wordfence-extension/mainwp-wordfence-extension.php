<?php
/*
Plugin Name: MainWP Wordfence Extension
Plugin URI: https://mainwp.com
Description: The Wordfence Extension combines the power of your MainWP Dashboard with the popular WordPress Wordfence Plugin. It allows you to manage Wordfence settings, Monitor Live Traffic and Scan your child sites directly from your dashboard. Requires MainWP Dashboard plugin.
Version: 2.1
Author: MainWP
Author URI: https://mainwp.com
Documentation URI: https://mainwp.com/help/category/mainwp-extensions/wordfence/
Icon URI:
*/
if ( ! defined( 'MAINWP_WORDFENCE_EXT_PLUGIN_FILE' ) ) {
	define( 'MAINWP_WORDFENCE_EXT_PLUGIN_FILE', __FILE__ );
}

if ( ! defined( 'MAINWP_WORDFENCE_PATH' ) ) {
	define( 'MAINWP_WORDFENCE_PATH', plugin_dir_path( MAINWP_WORDFENCE_EXT_PLUGIN_FILE ) );
}



class MainWP_Wordfence_Extension {
	public static $instance = null;
	public static $plugin_url;
	public static $plugin_translate = 'mainwp-wordfence-extension';
	public $plugin_slug;
	public static $plugin_dir;
	protected $option;
	protected $option_handle = 'mainwp_wordfence_extension';
	public static $update_version = '1.1';		
    private static $script_version = '1.6';
     
	static function get_instance() {
		if ( null == MainWP_Wordfence_Extension::$instance ) {
			MainWP_Wordfence_Extension::$instance = new MainWP_Wordfence_Extension();
		}

		return MainWP_Wordfence_Extension::$instance;
	}

	public function __construct() {
		self::$plugin_dir  = plugin_dir_path( __FILE__ );
		self::$plugin_url  = plugin_dir_url( __FILE__ );
		$this->plugin_slug = plugin_basename( __FILE__ );		
		$this->option      = get_option( $this->option_handle );        
        $this->include_files();        
        add_action( 'init', array( &$this, 'localization' ) );
		add_filter( 'plugin_row_meta', array( &$this, 'plugin_row_meta' ), 10, 2 );
		add_action( 'after_plugin_row', array( &$this, 'after_plugin_row' ), 10, 3 );
        add_action( 'in_admin_header', array( &$this, 'in_admin_head' ) ); // Adds Help Tab in admin header
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_action( 'init', array( &$this, 'check_update' ) );
        add_action( 'init', array( &$this, 'init' ) );
		add_filter( 'mainwp-getsubpages-sites', array( &$this, 'managesites_subpage' ), 10, 1 );
		add_filter( 'mainwp-sync-extensions-options', array( &$this, 'mainwp_sync_extensions_options' ), 10, 1 );		
		add_action( 'mainwp_applypluginsettings_mainwp-wordfence-extension', array( MainWP_Wordfence_Setting::get_instance(), 'mainwp_apply_plugin_settings' ) );
		
		MainWP_Wordfence_DB::get_instance()->install();
	}

	public function localization() {
		load_plugin_textdomain( 'mainwp-wordfence-extension', false,  dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}
    
    public function include_files() {        
        require_once MAINWP_WORDFENCE_PATH . '/libs/wfUtils.php';
        require_once MAINWP_WORDFENCE_PATH . '/libs/wfPersistenceController.php';        
        require_once MAINWP_WORDFENCE_PATH . '/libs/wfView.php';          
        require_once MAINWP_WORDFENCE_PATH . '/models/scanner/wfScanner.php';
        require_once MAINWP_WORDFENCE_PATH . '/models/common/wfTab.php';
    }
    
    public function init() {
        MainWP_Wordfence_Setting::init();
	}

         /**
	 * This function check if current page is Client Reports Extension page.
	 * @return void
	 */
	function in_admin_head() {
		if ( isset( $_GET['page'] ) && $_GET['page'] == 'Extensions-Mainwp-Wordfence-Extension' ) {
			self::addHelpTabs(); // If page is Extension then call this 'addHelpTabs' function
		}
	}

	/**
	 * This function add help tabs in header.
	 * @return void
	 */
	public static function addHelpTabs() {
		$screen = get_current_screen(); //This function returns an object that includes the screen's ID, base, post type, and taxonomy, among other data points.
		$i      = 1;

		$screen->add_help_tab( array(
			'id'      => 'mainwp_wfc_ext_helptabs_' . $i ++,
			'title'   => __( 'First Steps with Extensions', 'mainwp-wordfence-extension' ),
			'content' => self::getHelpContent( 1 ),
		) );
		$screen->add_help_tab( array(
			'id'      => 'mainwp_wfc_ext_helptabs_' . $i ++,
			'title'   => __( 'MainWP Wordfence Extension', 'mainwp-wordfence-extension' ),
			'content' => self::getHelpContent( 2 ),
		) );
	}
        
        /**
	 * Get help tab content.
	 *
	 * @param int $tabId
	 *
	 * @return string|bool
	 */
	public static function getHelpContent( $tabId ) {
		ob_start();
		if ( 1 == $tabId ) {
			?>
			<h3><?php echo __( 'First Steps with Extensions', 'mainwp-client-reports-extension' ); ?></h3>
			<p><?php echo __( 'If you are having issues with getting started with the MainWP extensions, please review following help documents', 'mainwp-wordfence-extension' ); ?></p>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'What are the MainWP Extensions', 'mainwp-wordfence-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/order-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Order Extension(s)', 'mainwp-wordfence-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/my-downloads-and-api-keys/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'My Downloads and API Keys', 'mainwp-wordfence-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/install-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Install Extension(s)', 'mainwp-wordfence-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/activate-extensions-api/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Activate Extension(s) API', 'mainwp-wordfence-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/updating-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Updating Extension(s)', 'mainwp-wordfence-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/remove-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Remove Extension(s)', 'mainwp-wordfence-extension' ); ?></a><br/><br/>
			<a href="https://mainwp.com/help/category/mainwp-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Help Documenation for all MainWP Extensions', 'mainwp-wordfence-extension' ); ?></a>
		<?php } else if ( 2 == $tabId ) { ?>
			<h3><?php echo __( 'MainWP Wordfence Extension', 'mainwp-wordfence-extension' ); ?></h3>
			<a href="https://mainwp.com/help/docs/how-to-install-the-mainwp-wordfence-extension/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'How to Install the MainWP Wordfence Extension', 'mainwp-wordfence-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/mainwp-wordfence-setup/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'MainWP Wordfence Setup', 'mainwp-wordfence-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/wordfence-settings/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Wordfence Settings', 'mainwp-wordfence-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/wordfence-dashboard/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Wordfence Dashboard', 'mainwp-wordfence-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/how-to-scan-child-sites-with-the-mainwp-wordfence-extension/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'How to Scan Child Sites with the MainWP Wordfence Extension', 'mainwp-wordfence-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/monitor-child-sites-live-traffic/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Monitor Child Sites Live Traffic', 'mainwp-wordfence-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/monitor-network-live-traffic/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Monitor Network Live Traffic', 'mainwp-wordfence-extension' ); ?></a><br/>
		<?php }
		$output = ob_get_clean();

		return $output;
	}
        
	function check_update() {

		$update_version = get_option( 'mainwp_wordfence_update_version', false );
		
		if ( $update_version == self::$update_version ) { return; }
		
		global $mainWPWordfenceExtensionActivator;
		$websites = apply_filters( 'mainwp-getsites', $mainWPWordfenceExtensionActivator->get_child_file(), $mainWPWordfenceExtensionActivator->get_child_key(), null );
		$site_ids = array();
		if ( is_array( $websites ) ) {
			foreach ( $websites as $website ) {
				$site_ids[] = $website['id'];
			}
		}
		
		// update new setting loginSec_strongPasswds_enabled
		if ( $update_version == '1.0' && (count($site_ids) > 0) ) {
			// update general setings
			$wfc_option = get_option( MainWP_Wordfence_Config::$option_handle, false );				
			if ( isset($wfc_option['loginSec_strongPasswds'])) {
				if ($wfc_option['loginSec_strongPasswds'] == '') {
					$wfc_option['loginSec_strongPasswds_enabled'] = 0;
				} else {
					$wfc_option['loginSec_strongPasswds_enabled'] = 1;
				}
				
				update_option( MainWP_Wordfence_Config::$option_handle, $wfc_option );
			}			
			
			// update individual setings
			$wfc_settings = MainWP_Wordfence_DB::get_instance()->get_settings();
			
			$settings_data = array();			
			foreach ( $wfc_settings as $set ) {
				if ( in_array( $set->site_id, $site_ids ) ) {						
					$settings_data[ $set->site_id ] = $set->settings; 						
				}
			}

			if ( count( $settings_data ) > 0 ) {				
				foreach ( $settings_data as $siteid => $settings ) {
					$option = unserialize( $settings );
					if ( is_array( $option ) ) {						
						if (isset($option['loginSec_strongPasswds'])) {
							if ($option['loginSec_strongPasswds'] == '') {
								$option['loginSec_strongPasswds_enabled'] = 0;
							} else {
								$option['loginSec_strongPasswds_enabled'] = 1;
							}
							$update = array(
								'site_id' => $siteid,
								'settings'  => serialize( $option ),
							);
							MainWP_Wordfence_DB::get_instance()->update_setting( $update );								
						}
					}
				}
			}
		}
			
		update_option( 'mainwp_wordfence_update_version', self::$update_version, 'yes' );
	}

	function mainwp_sync_extensions_options($values = array()) {
		$values['mainwp-wordfence-extension'] = array(
				'plugin_slug' => 'wordfence/wordfence.php',	
				'plugin_name' => 'Wordfence Security'
		);
		return $values;
	}	
	
	public function plugin_row_meta( $plugin_meta, $plugin_file ) {
		if ( $this->plugin_slug != $plugin_file ) {
			return $plugin_meta;
		}
		
		$slug = basename($plugin_file, ".php");
		$api_data = get_option( $slug. '_APIManAdder');		
		if (!is_array($api_data) || !isset($api_data['activated_key']) || $api_data['activated_key'] != 'Activated' || !isset($api_data['api_key']) || empty($api_data['api_key']) ) {
			return $plugin_meta;
		}
		
		$plugin_meta[] = '<a href="?do=checkUpgrade" title="Check for updates.">Check for updates now</a>';

		return $plugin_meta;
	}

	public function after_plugin_row( $plugin_file, $plugin_data, $status ) {	
		if ( $this->plugin_slug != $plugin_file ) {
			return ;
		}	
		$slug = basename($plugin_file, ".php");
		$api_data = get_option( $slug. '_APIManAdder');
		
		if (!is_array($api_data) || !isset($api_data['activated_key']) || $api_data['activated_key'] != 'Activated'){
			if (!isset($api_data['api_key']) || empty($api_data['api_key'])) {
				?>
				<style type="text/css">
					tr#<?php echo $slug;?> td, tr#<?php echo $slug;?> th{
						box-shadow: none;
					}
				</style>
				<tr class="plugin-update-tr active"><td colspan="3" class="plugin-update colspanchange"><div class="update-message api-deactivate">
				<?php echo (sprintf(__("API not activated check your %sMainWP account%s for updates. For automatic update notification please activate the API.", "mainwp"), '<a href="https://mainwp.com/my-account" target="_blank">', '</a>')); ?>
				</div></td></tr>
				<?php
			}
		}		
	}	
	
	function managesites_subpage( $subPage ) {
		$subPage[] = array(
			'title' => __( 'Wordfence', 'mainwp-wordfence-extension' ),
			'slug' => 'Wordfence',
			'sitetab' => true,
			'menu_hidden' => true,
			'callback' => array( 'MainWP_Wordfence', 'render' ),
            'on_load_callback' => array( 'MainWP_Wordfence_Setting', 'on_load_individual_page' ),
		);
		return $subPage;
	}


	public function admin_init() {
		wp_enqueue_style( 'mainwp-wordfence-extension', self::getBaseURL() . 'css/mainwp-wordfence.css',  '', self::$script_version );
		wp_enqueue_script( 'mainwp-wordfence-extension', self::getBaseURL() . 'js/mainwp-wordfence.js', array( 'jquery' ), self::$script_version);

		if ( isset( $_GET['page'] ) && ( 'Extensions-Mainwp-Wordfence-Extension' == $_GET['page'] || ( 'managesites' == $_GET['page'] && isset($_GET['scanid']) ) || 'ManageSitesWordfence' == $_GET['page'] ) ) {                        
            wp_enqueue_style('wp-pointer');
			wp_enqueue_script('wp-pointer');
			//wp_enqueue_style('mainwp-wordfence-main-style',  self::getBaseURL() . 'css/main.css', '', self::$script_version);                                    
            wp_enqueue_style('mainwp-wordfence-main-style',  self::getBaseURL() . 'css/wf-main.css', '', self::$script_version);                        
            wp_enqueue_style('mainwp-wordfence-adminbar-style',  self::getBaseURL() . 'css/wf-adminbar.css', '', self::$script_version);                        
            wp_enqueue_style('mainwp-wordfence-ionicons-style', self::getBaseURL() . 'css/wf-ionicons.css', '', self::$script_version);
            
			//wp_enqueue_style( 'mainwp-wordfence-extension-colorbox-style', self::getBaseURL() . 'css/wf-colorbox.css' );
			wp_enqueue_style( 'mainwp-wordfence-extension-dttable-style', self::getBaseURL() . 'css/dt_table.css' );            
            wp_enqueue_style( 'mainwp-wordfence-colorbox-style', self::getBaseURL() . 'css/wf-colorbox.css', '', self::$script_version );
			wp_enqueue_script( 'mainwp-wordfence-extension-admin-log', self::getBaseURL() . 'js/mainwp-wfc-log.js', array(), self::$script_version );
			wp_enqueue_script( 'mainwp-wordfence-extension-jquery-tmpl', self::getBaseURL() . 'js/jquery.tmpl.min.js', array( 'jquery' ) );
			//wp_enqueue_script( 'mainwp-wordfence-extension-jquery-colorbox', self::getBaseURL() . 'js/jquery.colorbox-min.js', array( 'jquery' ) );                        
            wp_enqueue_script( 'mainwp-wordfence-jquery-colorbox', self::getBaseURL() . 'js/jquery.colorbox.1517414961.js', array('jquery'), self::$script_version );            
			wp_enqueue_script( 'mainwp-wordfence-extension-jquery-dataTables', self::getBaseURL() . 'js/jquery.dataTables.min.js', array( 'jquery' ) );
		}

		if ( isset( $_GET['page'] ) && ( 'managesites' == $_GET['page'] || 'ManageSitesWordfence' == $_GET['page'] ) ) {
			wp_enqueue_script( 'mainwp-wordfence-extension-admin-log', self::getBaseURL() . 'js/mainwp-wfc-log.js', array(), self::$script_version );            
		}
        
		$wfc = new MainWP_Wordfence();
		$wfc->admin_init();
		$wfc_plugin = new MainWP_Wordfence_Plugin();
		$wfc_plugin->admin_init();
		$wfc_setting = new MainWP_Wordfence_Setting();
		$wfc_setting->admin_init();
	}

    public static function activity_enqueue_style() {
            
            $current_page = '';
            
            if (isset($_GET['action']) && $_GET['action'] == 'traffic')
                $current_page = 'traffic';
            
            if (isset($_GET['tab'])) {
                if ( $_GET['tab'] == 'network_traffic' || $_GET['tab'] == 'traffic' ) {
                    $current_page = 'traffic';
                } 
                else if ( $_GET['tab'] == 'firewall' || $_GET['tab'] == 'network_firewall' ) {
                    $current_page = 'firewall';
                }
            }
            
        
            if ($current_page == 'traffic') {                
                wp_enqueue_style('mainwp-wfc-jquery-ui-theme-css', MainWP_Wordfence_Extension::getBaseURL() . 'css/jquery-ui.theme.min.css', array(), self::$script_version);
                wp_enqueue_style('mainwp-wfc-jquery-ui-timepicker-css', MainWP_Wordfence_Extension::getBaseURL() . 'css/jquery-ui-timepicker-addon.css', array(), self::$script_version);
                wp_enqueue_script('mainwp-wfc-timepicker-js', MainWP_Wordfence_Extension::getBaseURL() . 'js/jquery-ui-timepicker-addon.js', array('jquery', 'jquery-ui-datepicker', 'jquery-ui-slider'), self::$script_version);
                wp_enqueue_script('mainwp-wfc-knockout-js', MainWP_Wordfence_Extension::getBaseURL() . 'js/knockout-3.3.0.js', array(), self::$script_version);
                //wp_enqueue_script('mainwp-wfc-live-traffic-js', MainWP_Wordfence_Extension::getBaseURL() . 'js/admin.liveTraffic.js', array('jquery'), self::$script_version);
                wp_enqueue_script('mainwp-wordfence-live-traffic-js', self::getBaseURL() . 'js/admin.liveTraffic.1517414961.js', array('jquery', 'jquery-ui-tooltip'), self::$script_version);                    
            } 
            else if ( $current_page == 'firewall' ) {
                wp_enqueue_style('mainwp-wfc-jquery-ui-timepicker-css', MainWP_Wordfence_Extension::getBaseURL() . 'css/jquery-ui-timepicker-addon.css', array(), self::$script_version);
                wp_enqueue_script('mainwp-wfc-timepicker-js', MainWP_Wordfence_Extension::getBaseURL() . 'js/jquery-ui-timepicker-addon.js', array('jquery', 'jquery-ui-datepicker', 'jquery-ui-slider'), self::$script_version);
                wp_enqueue_style('select2', MainWP_Wordfence_Extension::getBaseURL() . 'css/select2.min.css', array(), self::$script_version);
                wp_enqueue_script('select2', MainWP_Wordfence_Extension::getBaseURL() . 'js/select2.min.js', array('jquery'), self::$script_version);                
            }
	}
    
	public function get_option( $key, $default = '' ) {
		if ( isset( $this->option[ $key ] ) ) {
			return $this->option[ $key ];
		}

		return $default;
	}

	public function set_option( $key, $value ) {
		$this->option[ $key ] = $value;

		return update_option( $this->option_handle, $this->option );
	}
        
        public static function getBaseURL() {
		return self::$plugin_url;
	}
}


function mainwp_wfc_extension_autoload( $class_name ) {
	$allowedLoadingTypes = array( 'class' );
	$class_name = str_replace( '_', '-', strtolower( $class_name ) );
	foreach ( $allowedLoadingTypes as $allowedLoadingType ) {
		$class_file = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . str_replace( basename( __FILE__ ), '', plugin_basename( __FILE__ ) ) . $allowedLoadingType . DIRECTORY_SEPARATOR . $class_name . '.' . $allowedLoadingType . '.php';
		if ( file_exists( $class_file ) ) {
			require_once( $class_file );
		}
	}
}


	spl_autoload_register( 'mainwp_wfc_extension_autoload' );


register_activation_hook( __FILE__, 'mainwp_wordfence_extension_activate' );
register_deactivation_hook( __FILE__, 'mainwp_wordfence_extension_deactivate' );
function mainwp_wordfence_extension_activate() {
	update_option( 'mainwp_wordfence_extension_activated', 'yes' );
	$extensionActivator = new MainWP_Wordfence_Extension_Activator();
	$extensionActivator->activate();	
}

function mainwp_wordfence_extension_deactivate() {
	$extensionActivator = new MainWP_Wordfence_Extension_Activator();
	$extensionActivator->deactivate();
}

class MainWP_Wordfence_Extension_Activator {
	protected $mainwpMainActivated = false;
	protected $childEnabled = false;
	protected $childKey = false;
	protected $childFile;
	protected $plugin_handle = 'mainwp-wordfence-extension';
	protected $product_id = 'MainWP Wordfence Extension';
	protected $software_version = '2.1';

	public function __construct() {
		$this->childFile = __FILE__;
		add_filter( 'mainwp-getextensions', array( &$this, 'get_this_extension' ) );
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', false );

		if ( $this->mainwpMainActivated !== false ) {
			$this->activate_this_plugin();
		} else {
			add_action( 'mainwp-activated', array( &$this, 'activate_this_plugin' ) );
		}
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_action( 'admin_notices', array( &$this, 'mainwp_error_notice' ) );
	}

	function admin_init() {
		if ( get_option( 'mainwp_wordfence_extension_activated' ) == 'yes' ) {
			delete_option( 'mainwp_wordfence_extension_activated' );
			wp_redirect( admin_url( 'admin.php?page=Extensions' ) );

			return;
		}
	}

	function get_this_extension( $pArray ) {
		$pArray[] = array(
                        'plugin'     => __FILE__,
                        'api'        => $this->plugin_handle,
                        'mainwp'     => true,
                        'callback'   => array( &$this, 'settings' ),
                        'apiManager' => true,
                        'on_load_callback' => array('MainWP_Wordfence' , 'on_load_general_page')
                );

		return $pArray;
	}

	public function get_metaboxes( $metaboxes ) {
		if ( ! is_array( $metaboxes ) ) {
			$metaboxes = array();
		}
		if ( isset( $_GET['page'] ) && 'managesites' == $_GET['page'] ) {
			$metaboxes[] = array(
				'plugin'        => $this->childFile,
				'key'           => $this->childKey,
				'metabox_title' => __( 'Wordfence Status', 'mainwp-wordfence-extension' ),
				'callback'      => array( 'MainWP_Wordfence', 'render_metabox' ),
			);
		}

		return $metaboxes;
	}

	function settings() {
		do_action( 'mainwp-pageheader-extensions', __FILE__ );		
		MainWP_Wordfence::render();		
		do_action( 'mainwp-pagefooter-extensions', __FILE__ );
	}

	function activate_this_plugin() {
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', $this->mainwpMainActivated );
		$this->childEnabled = apply_filters( 'mainwp-extension-enabled-check', __FILE__ );
		$this->childKey = $this->childEnabled['key'];
		if ( function_exists( 'mainwp_current_user_can' ) && ! mainwp_current_user_can( 'extension', 'mainwp-wordfence-extension' ) ) {
			return;
		}
		add_filter( 'mainwp-getmetaboxes', array( &$this, 'get_metaboxes' ) );
		new MainWP_Wordfence_Extension();
	}

	public function get_child_key() {
		return $this->childKey;
	}

	public function get_child_file() {
		return $this->childFile;
	}

	function mainwp_error_notice() {
		global $current_screen;
		if ( $current_screen->parent_base == 'plugins' && $this->mainwpMainActivated == false ) {
			echo '<div class="error"><p>MainWP Wordfence Extension ' . __( 'requires <a href="https://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> to be activated in order to work. Please install and activate <a href="https://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> first.', 'mainwp-wordfence-extension' ) . '</p></div>';
		}
	}

	public function update_option( $option_name, $option_value ) {
		$success = add_option( $option_name, $option_value, '', 'no' );

		if ( ! $success ) {
			$success = update_option( $option_name, $option_value );
		}

		return $success;
	}

	public function activate() {
		$options = array(
			'product_id'       => $this->product_id,
			'activated_key'    => 'Deactivated',
			'instance_id'      => apply_filters( 'mainwp-extensions-apigeneratepassword', 12, false ),
			'software_version' => $this->software_version,
		);
		$this->update_option( $this->plugin_handle . '_APIManAdder', $options );
	}

	public function deactivate() {
		$this->update_option( $this->plugin_handle . '_APIManAdder', '' );
	}
}

global $mainWPWordfenceExtensionActivator;
$mainWPWordfenceExtensionActivator = new MainWP_Wordfence_Extension_Activator();
