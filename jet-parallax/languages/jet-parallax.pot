#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Jet Elements for Elementor\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-03-12 14:58+0200\n"
"PO-Revision-Date: 2018-01-03 12:10+0200\n"
"Last-Translator: Vladimir Anokhin <ano.vladimir@gmail.com>\n"
"Language-Team: JetImpex\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: _e;__;__;_x;esc_html_e;esc_html__;esc_attr_e;"
"esc_attr__;_x:1,2c;_n:1,2\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Generator: Poedit 1.8.6\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: cherry-framework\n"
"X-Poedit-SearchPathExcluded-1: node_modules\n"

#: includes/ext/element-extension.php:60
msgid "Jet Parallax"
msgstr ""

#: includes/ext/element-extension.php:70
msgid "Image"
msgstr ""

#: includes/ext/element-extension.php:78
msgid "Parallax Speed(%)"
msgstr ""

#: includes/ext/element-extension.php:97
msgid "Parallax Type"
msgstr ""

#: includes/ext/element-extension.php:101
msgid "None"
msgstr ""

#: includes/ext/element-extension.php:102
msgid "Scroll"
msgstr ""

#: includes/ext/element-extension.php:103
msgid "Mouse Move"
msgstr ""

#: includes/ext/element-extension.php:111
msgid "z-Index"
msgstr ""

#: includes/ext/element-extension.php:123
msgid "Background X Position(%)"
msgstr ""

#: includes/ext/element-extension.php:135
msgid "Background Y Position(%)"
msgstr ""

#: includes/ext/element-extension.php:147
msgid "Background Size"
msgstr ""

#: includes/ext/element-extension.php:151
msgid "Auto"
msgstr ""

#: includes/ext/element-extension.php:152
msgid "Cover"
msgstr ""

#: includes/ext/element-extension.php:153
msgid "Contain"
msgstr ""

#: includes/ext/element-extension.php:161
msgid "Animation Property"
msgstr ""

#: includes/ext/element-extension.php:165
msgid "Background Position"
msgstr ""

#: includes/ext/element-extension.php:166
msgid "Transform"
msgstr ""

#: includes/ext/element-extension.php:167
msgid "Transform 3D"
msgstr ""

#: includes/integration.php:202
msgid "Jet Elements"
msgstr ""
