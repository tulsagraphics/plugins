<?php
/**
 * Class description
 *
 * @package   package_name
 * @author    Cherry Team
 * @license   GPL-2.0+
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! class_exists( 'Jet_Parallax_Assets' ) ) {

	/**
	 * Define Jet_Parallax_Assets class
	 */
	class Jet_Parallax_Assets {

		/**
		 * Localize data array
		 *
		 * @var array
		 */
		public $localize_data = array();

		/**
		 * A reference to an instance of this class.
		 *
		 * @since 1.0.0
		 * @var   object
		 */
		private static $instance = null;

		/**
		 * Constructor for the class
		 */
		public function init() {
			add_action( 'elementor/frontend/after_enqueue_styles', array( $this, 'enqueue_styles' ) );
			add_action( 'elementor/frontend/before_enqueue_scripts', array( $this, 'enqueue_scripts' ), 10 );
		}

		/**
		 * Enqueue public-facing stylesheets.
		 *
		 * @since 1.0.0
		 * @access public
		 * @return void
		 */
		public function enqueue_styles() {

			wp_enqueue_style(
				'jet-parallax-frontend',
				jet_parallax()->plugin_url( 'assets/css/jet-parallax-frontend.css' ),
				false,
				jet_parallax()->get_version()
			);
		}

		/**
		 * Enqueue plugin scripts only with elementor scripts
		 *
		 * @return void
		 */
		public function enqueue_scripts() {
			wp_enqueue_script( 'jet-ease-pack', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/easing/EasePack.min.js', null, null, true );
			wp_enqueue_script( 'jet-tween-max', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js', null, null, true );

			wp_enqueue_script(
				'jet-parallax-frontend',
				jet_parallax()->plugin_url( 'assets/js/jet-parallax-frontend.js' ),
				array( 'jquery', 'elementor-frontend' ),
				jet_parallax()->get_version(),
				true
			);

			wp_localize_script(
				'jet-parallax-frontend',
				'jetParallax',
				apply_filters( 'jet-parallax/frontend/localize-data', $this->localize_data )
			);
		}

		/**
		 * Prints editor templates
		 *
		 * @return void
		 */
		public function print_templates() {}

		/**
		 * Returns the instance.
		 *
		 * @since  1.0.0
		 * @return object
		 */
		public static function get_instance() {

			// If the single instance hasn't been set, set it now.
			if ( null == self::$instance ) {
				self::$instance = new self;
			}
			return self::$instance;
		}
	}

}

function jet_parallax_assets() {
	return Jet_Parallax_Assets::get_instance();
}
