# Jet Parallax For Elementor

JetParallax is a plugin for Elementor live page builder allowing to create mesmerizing parallax effects for multiple objects and layering them in order to get the effect of depth and 3D movement.

# ChangeLog

## [1.0.4](https://github.com/ZemezLab/jet-parallax/releases/tag/1.0.4)
* Added: Elementor 2.1 compatibility

## [1.0.3](https://github.com/ZemezLab/jet-parallax/releases/tag/1.0.3)
* Added: multiple performance improvements and bug fixes

## [1.0.1](https://github.com/ZemezLab/jet-parallax/releases/tag/1.0.1)
* Fixed: Elementor 2.0 compatibility


## [1.0.0](https://github.com/ZemezLab/jet-parallax/releases/tag/1.0.0)
* Init
