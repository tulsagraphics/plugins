<?php

namespace Common\Files\Actions\Storage;

use Storage;
use Common\Files\FileEntry;
use Illuminate\Http\UploadedFile;

class StorePublicUpload
{
    /**
     * @param FileEntry $entry
     * @param UploadedFile $contents
     */
    public function execute(FileEntry $entry, $contents)
    {
        Storage::disk('public')->putFileAs($entry->public_path, $contents, $entry->file_name);
    }
}