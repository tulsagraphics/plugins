<?php

namespace Common\Files\Actions\Storage;

use Illuminate\Http\UploadedFile;
use Storage;
use Common\Files\FileEntry;

class StorePrivateUpload
{
    /**
     * @param FileEntry $entry
     * @param UploadedFile $contents
     */
    public function execute(FileEntry $entry, $contents)
    {
        Storage::disk(config('common.site.uploads_disk'))->putFileAs($entry->file_name, $contents, $entry->file_name);
    }
}