<?php

namespace Common\Files\Actions;

use Common\Files\FileEntry;
use Common\Files\Traits\GetsEntryTypeFromMime;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;

class UploadedFileToArray
{
    use GetsEntryTypeFromMime;

    /**
     * @var FileEntry
     */
    private $entry;

    /**
     * @param FileEntry $entry
     */
    public function __construct(FileEntry $entry)
    {
        $this->entry = $entry;
    }

    /**
     * @param UploadedFile $file
     * @param array $extra
     * @return array
     */
    public function execute(UploadedFile $file, $extra)
    {
        // TODO: move mime/extension/type guessing into separate class
        $originalMime = $file->getMimeType();

        if ($originalMime === 'application/octet-stream') {
            $originalMime = $file->getClientMimeType();
        }

        $data = [
            'name' => $file->getClientOriginalName(),
            'file_name' => str_random(40),
            'mime' => $originalMime,
            'type' => $this->getTypeFromMime($originalMime),
            'file_size' => $file->getClientSize(),
            'extension' => $this->getExtension($file, $originalMime),
        ];

        // merge extra data specified by user
        $data = array_merge($data, [
            'path' => Arr::get($extra, 'path'),
            'parent_id' => Arr::get($extra, 'parent_id'),
            'public_path' => Arr::get($extra, 'public_path'),
            'public' => Arr::get($extra, 'public_path') ? 1 : 0
        ]);

        // public files will be stored with extension
        if ($data['public']) {
            $data['file_name'] = $data['file_name'] . '.' . $data['extension'];
        }

        return $data;
    }

    /**
     * Extract file extension from specified file data.
     *
     * @param UploadedFile $file
     * @param string $mime
     * @return string
     */
    private function getExtension(UploadedFile $file, $mime)
    {
        if ($extension = $file->getClientOriginalExtension()) {
            return $extension;
        }

        $pathinfo = pathinfo($file->getClientOriginalName());

        if (isset($pathinfo['extension'])) return $pathinfo['extension'];

        return explode('/', $mime)[1];
    }
}