<?php

namespace Common\Files\Actions;

use Auth;
use Common\Auth\User;
use Common\Files\FileEntry;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;

class CreateFileEntry
{
    /**
     * @var FileEntry
     */
    private $entry;

    /**
     * @param FileEntry $entry
     */
    public function __construct(FileEntry $entry)
    {
        $this->entry = $entry;
    }

    /**
     * @param UploadedFile $file
     * @param $extra
     * @return FileEntry
     */
    public function execute(UploadedFile $file, $extra)
    {
        $data = app(UploadedFileToArray::class)->execute($file, $extra);

        $userId = Arr::get($extra, 'user_id', Auth::id());
        $entries = collect();

        if (Arr::get($data, 'path')) {
            $entries = $entries->merge($this->createPath($data['path'], $data['parent_id'], $userId));
            $parent = $entries->last();
            if ($parent) $data['parent_id'] = $parent->id;
        }

        $file = $this->entry->create($data);

        if ( ! Arr::get($data, 'public')) {
            $file->generatePath();
        }

        $entries->push($file);

        $entryIds = $entries->mapWithKeys(function($entry) {
            return [$entry->id => ['owner' => 1]];
        })->toArray();

        User::find($userId)->files()->syncWithoutDetaching($entryIds);

        if (isset($parent) && $parent) {
            $file->setRelation('parent', $parent);
        } else {
            $file->load('parent');
        }

        return $file;
    }

    /**
     * @param string $path
     * @param integer|null $parentId
     * @param integer $userId
     * @return \Illuminate\Support\Collection
     */
    private function createPath($path, $parentId, $userId)
    {
        $path = collect(explode('/', $path));
        $path = $path->filter(function($name) {
            return $name && ! str_contains($name, '.');
        });

        if ($path->isEmpty()) return $path;

        return $path->reduce(function($parents, $name) use($parentId, $userId) {
            if ( ! $parents) $parents = collect();
            $parent = $parents->last();

            $values = [
                'type' => 'folder',
                'name' => $name,
                'file_name' => $name,
                'parent_id' => $parent ? $parent->id : $parentId
            ];

            // check if user already has a folder with that name and parent
            $folder = $this->entry->where($values)
                ->whereOwner($userId)
                ->first();

            if ( ! $folder) {
                $folder = $this->entry->create($values);
                $folder->generatePath();
            }

            return $parents->push($folder);
        });
    }
}