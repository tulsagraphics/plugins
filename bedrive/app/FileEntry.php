<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Common\Files\FileEntry as CommonFileEntry;

class FileEntry extends CommonFileEntry
{
    protected $table = 'file_entries';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function labels()
    {
        return $this->tags()->where('tags.type', 'label');
    }

    /**
     * Get only entries that are not children of another entry.
     *
     * @param Builder $builder
     * @return Builder
     */
    public function scopeRootOnly(Builder $builder) {
        return $builder->where('parent_id', null);
    }

    /**
     * Get only entries that are starred.
     *
     * @param Builder $builder
     * @return Builder
     */
    public function scopeOnlyStarred(Builder $builder) {
        return $builder->whereHas('labels', function($query) {
            return $query->where('tags.name', 'starred');
        });
    }
}
