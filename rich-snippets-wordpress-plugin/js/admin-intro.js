var rich_snippets_admin_intro;

(function () {
	"use strict";

	rich_snippets_admin_intro = function () {
		this.code_field              = null;
		this.message_div             = null;
		this.edit_purchase_code_link = null;
		this.verify_button           = null;
		this.privacy_aggreement      = null;

		this.init = function () {
			var self = this;

			this.code_field              = jQuery( '.wpb-rs-main-cc-code' );
			this.message_div             = jQuery( '.wpb-rs-main-message' );
			this.edit_purchase_code_link = jQuery( '.wpb-rs-main-edit-purchase-code  a' );
			this.verify_button           = jQuery( '.wpb-rs-main-step[data-step="2"]' );
			this.privacy_aggreement      = jQuery( '.wpb-rs-privaca-agree' );

			this.code_field.on( 'keyup focusout', function () {
				self.toggle_verify_button();
			} );

			this.privacy_aggreement.on( 'click', function () {
				self.toggle_verify_button();
			} );

			this.edit_purchase_code_link.on( 'click', function ( e ) {
				e.preventDefault();
				self.purchase_code_edit();
			} );

			this.toggle_verify_button();

			jQuery( document ).on( 'click', '.wpb-rs-main-step', function ( e ) {
				e.preventDefault();
				var step           = parseInt( jQuery( this ).data( 'step' ) );
				var next_step      = step + 1;
				var $next_step_div = jQuery( '.wpb-rs-main-text-step' + next_step );

				if ( !$next_step_div.length ) {
					return false;
				}

				if ( 3 === next_step && 1 !== parseInt( jQuery( this ).data( 'activated' ) ) ) {
					self.verify();
					return false;
				}

				jQuery( '.wpb-rs-main-text-step' + step ).fadeOut( 400, function () {
					if ( $next_step_div.length >= 1 ) {
						$next_step_div.fadeIn();
					}
				} );
			} );
		};

		this.purchase_code_edit = function () {
			this.edit_purchase_code_link.parent().addClass( 'hidden' );
			jQuery( '.wpb-rs-main-help-link' ).removeClass( 'hidden' );
			this.code_field.attr( 'type', 'text' );
		};

		this.verify = function () {
			var self = this;

			if ( self.verify_button.hasClass( 'disabled' ) ) {
				return false;
			}

			var purchase_code = this.code_field.val();

			var on_error = function ( jqXHR, text_status, thrown_error ) {
				window.console.error( text_status, thrown_error, jqXHR );

				self.message_div.html( '<div class="notice notice-error"><p>' + jqXHR.responseJSON.message + '</p></div>' ).show();

				setTimeout( function () {
					self.message_div.find( '.notice' ).hide( 1000, function () {
						jQuery( this ).remove();
					} );
				}, 15000 );

			};

			jQuery.ajax( {
				'url':        WPB_RS_ADMIN.rest_url + '/admin/verify/',
				'dataType':   'json',
				'beforeSend': function ( xhr ) {
					xhr.setRequestHeader( 'X-WP-Nonce', WPB_RS_ADMIN.nonce );
					self.verify_button.addClass( 'installing' );
				},
				'data':       {
					'purchase_code': purchase_code
				},
				'method':     'GET'
			} )
				.fail( on_error )
				.always( function () {
					self.verify_button.removeClass( 'installing' );
				} )
				.done( function ( response, textStatus, jqXHR ) {

					if ( !response ) {
						jqXHR.responseJSON = {
							'message': WPB_RS_ADMIN.translations.activation_no_content_err.replace( '%d', '1' )
						};
						on_error( jqXHR, textStatus, '' );
						return false;
					}

					if ( !response.hasOwnProperty( 'verified' ) ) {
						jqXHR.responseJSON = {
							'message': WPB_RS_ADMIN.translations.activation_no_content_err.replace( '%d', '2' )
						};
						on_error( jqXHR, textStatus, '' );
						return false;
					}

					self.message_div.html( '<div class="notice notice-success"><p>' + WPB_RS_ADMIN.translations.activated + '</p></div>' ).show();

					jQuery( '.wpb-rss-not-active-info' ).hide();

					self.verify_button.data( 'activated', 1 );

					setTimeout( function () {
						// window.location = WPB_RS_ADMIN.redirect_url;
						self.verify_button.trigger( 'click' );
					}, 3000 );
				} );
		};

		this.toggle_verify_button = function () {
			if ( '' === this.code_field.val() || !this.privacy_aggreement.prop( 'checked' ) ) {
				if ( !this.verify_button.hasClass( 'disabled' ) ) {
					this.verify_button.addClass( 'disabled' );
				}
			} else {
				if ( this.verify_button.hasClass( 'disabled' ) ) {
					this.verify_button.removeClass( 'disabled' );
				}
			}
		};
	};

	jQuery( document ).ready( function () {
		new rich_snippets_admin_intro().init();
	} );

})();



