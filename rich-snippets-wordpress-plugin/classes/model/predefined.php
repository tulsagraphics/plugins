<?php

namespace wpbuddy\rich_snippets;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


/**
 * Class Predefined.
 *
 * Functions to install/update predefined snippets.
 *
 * @package wpbuddy\rich_snippets
 *
 * @since   2.0.0
 */
final class Predefined_Model {

	/**
	 * @since 2.0.0
	 *
	 * @return array
	 */
	public static function article() {

		$position = unserialize( 'O:38:"wpbuddy\rich_snippets\Position_Ruleset":1:{s:50:" wpbuddy\rich_snippets\Position_Ruleset rulegroups";a:2:{i:0;O:41:"wpbuddy\rich_snippets\Position_Rule_Group":1:{s:48:" wpbuddy\rich_snippets\Position_Rule_Group rules";a:1:{i:0;O:35:"wpbuddy\rich_snippets\Position_Rule":3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:4:"post";}}}i:1;O:41:"wpbuddy\rich_snippets\Position_Rule_Group":1:{s:48:" wpbuddy\rich_snippets\Position_Rule_Group rules";a:1:{i:0;O:35:"wpbuddy\rich_snippets\Position_Rule":3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:4:"page";}}}}}' );

		$snippet = unserialize( 'a:1:{s:19:"snip-global-article";O:34:"wpbuddy\rich_snippets\Rich_Snippet":13:{s:2:"id";s:19:"snip-global-article";s:7:"context";s:17:"http://schema.org";s:4:"type";s:7:"Article";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;s:24:"image-prop-599be31c45dba";a:4:{i:0;s:29:"http://schema.org/ImageObject";i:1;O:34:"wpbuddy\rich_snippets\Rich_Snippet":7:{s:2:"id";s:18:"snip-599be1455dd51";s:7:"context";s:17:"http://schema.org";s:4:"type";s:11:"ImageObject";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;s:22:"url-prop-599be31c45c95";a:4:{i:0;s:26:"current_post_thumbnail_url";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:25:"height-prop-599be31c45cc2";a:4:{i:0;s:29:"current_post_thumbnail_height";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:24:"width-prop-599be31c45ce8";a:4:{i:0;s:28:"current_post_thumbnail_width";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}}s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:28:"publisher-prop-599be31c45de2";a:4:{i:0;s:39:"global_snippet_snip-global-organization";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:31:"dateModified-prop-599be31c45e07";a:4:{i:0;s:26:"current_post_modified_date";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:25:"author-prop-599be31c45e2d";a:4:{i:0;s:24:"http://schema.org/Person";i:1;O:34:"wpbuddy\rich_snippets\Rich_Snippet":6:{s:2:"id";s:18:"snip-599be16a975e1";s:7:"context";s:17:"http://schema.org";s:4:"type";s:6:"Person";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;s:22:"url-prop-599be31c45d55";a:4:{i:0;s:23:"current_post_author_url";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:23:"name-prop-599be31c45d7d";a:4:{i:0;s:24:"current_post_author_name";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}}s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:35:"mainEntityOfPage-prop-599be31c45e4f";a:4:{i:0;s:16:"current_post_url";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:30:"description-prop-599be31c45e70";a:4:{i:0;s:19:"yoast_seo_meta_desc";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:32:"datePublished-prop-599be31c45e97";a:4:{i:0;s:17:"current_post_date";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:27:"headline-prop-599be31c45f10";a:4:{i:0;s:18:"current_post_title";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:30:"articleBody-prop-599be31c45f37";a:4:{i:0;s:20:"current_post_content";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}}}' );

		return array(
			'position' => $position,
			'schema'   => $snippet,
		);
	}

	/**
	 * @since 2.0.0
	 *
	 * @return array
	 */
	public static function organization() {

		$snippet = unserialize( 'a:1:{s:24:"snip-global-organization";O:34:"wpbuddy\rich_snippets\Rich_Snippet":7:{s:2:"id";s:24:"snip-global-organization";s:7:"context";s:17:"http://schema.org";s:4:"type";s:12:"Organization";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;s:22:"url-prop-599be6521135e";a:2:{i:0;s:8:"blog_url";i:1;N;}s:23:"name-prop-599be652113e9";a:2:{i:0;s:10:"blog_title";i:1;N;}s:23:"logo-prop-599be65211453";a:2:{i:0;s:29:"http://schema.org/ImageObject";i:1;O:34:"wpbuddy\rich_snippets\Rich_Snippet":7:{s:2:"id";s:18:"snip-599be6401df71";s:7:"context";s:17:"http://schema.org";s:4:"type";s:11:"ImageObject";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;s:22:"url-prop-599be652112f6";a:2:{i:0;s:13:"site_icon_url";i:1;N;}s:25:"height-prop-599be65211318";a:2:{i:0;s:16:"site_icon_height";i:1;N;}s:24:"width-prop-599be65211337";a:2:{i:0;s:15:"site_icon_width";i:1;N;}}}}}' );

		return array(
			'position' => '',
			'schema'   => $snippet,
		);
	}

	/**
	 * @since 2.3.1
	 *
	 * @return array
	 */
	public static function review_of_product() {

		$snippet = unserialize( 'a:1:{s:26:"snip-global-review-product";O:34:"wpbuddy\rich_snippets\Rich_Snippet":12:{s:2:"id";s:26:"snip-global-review-product";s:7:"context";s:17:"http://schema.org";s:4:"type";s:6:"Review";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;s:28:"publisher-prop-5a97b109a0527";a:4:{i:0;s:39:"global_snippet_snip-global-organization";i:1;O:34:"wpbuddy\rich_snippets\Rich_Snippet":4:{s:2:"id";s:18:"snip-5a97ca6e75e69";s:7:"context";s:17:"http://schema.org";s:4:"type";s:5:"Thing";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;}s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:31:"itemReviewed-prop-5a97b109a9106";a:4:{i:0;s:25:"http://schema.org/Product";i:1;O:34:"wpbuddy\rich_snippets\Rich_Snippet":13:{s:2:"id";s:18:"snip-5a97b11b0288c";s:7:"context";s:17:"http://schema.org";s:4:"type";s:7:"Product";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;s:22:"mpn-prop-5a97b11b1a1a6";a:4:{i:0;s:9:"textfield";i:1;s:0:"";s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}s:24:"gtin8-prop-5a97b11b24fd1";a:4:{i:0;s:9:"textfield";i:1;s:0:"";s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}s:24:"image-prop-5a97b11b2ed96";a:4:{i:0;s:29:"http://schema.org/ImageObject";i:1;O:34:"wpbuddy\rich_snippets\Rich_Snippet":7:{s:2:"id";s:18:"snip-5a97c8ebb759d";s:7:"context";s:17:"http://schema.org";s:4:"type";s:11:"ImageObject";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;s:25:"height-prop-5a97c8ebc0b33";a:4:{i:0;s:29:"current_post_thumbnail_height";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:24:"width-prop-5a97c8ebcdc54";a:4:{i:0;s:28:"current_post_thumbnail_width";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:22:"url-prop-5a97c8ebd823c";a:4:{i:0;s:26:"current_post_thumbnail_url";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}}s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:23:"name-prop-5a97b11b38b83";a:4:{i:0;s:9:"textfield";i:1;s:0:"";s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}s:25:"offers-prop-5a97b11b535bf";a:4:{i:0;s:23:"http://schema.org/Offer";i:1;O:34:"wpbuddy\rich_snippets\Rich_Snippet":8:{s:2:"id";s:18:"snip-5a97c9103796a";s:7:"context";s:17:"http://schema.org";s:4:"type";s:5:"Offer";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;s:32:"priceCurrency-prop-5a97c91050a3e";a:4:{i:0;s:9:"textfield";i:1;s:3:"EUR";s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}s:24:"price-prop-5a97c91068fd0";a:4:{i:0;s:0:"";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:31:"availability-prop-5a97c91077fd1";a:4:{i:0;s:36:"descendant-http://schema.org/InStock";i:1;O:34:"wpbuddy\rich_snippets\Rich_Snippet":4:{s:2:"id";s:18:"snip-5a97ca6e76647";s:7:"context";s:17:"http://schema.org";s:4:"type";s:5:"Thing";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;}s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:22:"url-prop-5a97ca2ec7f53";a:4:{i:0;s:9:"textfield";i:1;s:0:"";s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}}s:11:"overridable";b:1;s:20:"overridable_multiple";b:1;}s:25:"gtin14-prop-5a97b11b6330c";a:4:{i:0;s:9:"textfield";i:1;s:0:"";s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}s:25:"gtin13-prop-5a97b11b70b67";a:4:{i:0;s:9:"textfield";i:1;s:0:"";s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}s:30:"description-prop-5a97b11b8b265";a:4:{i:0;s:9:"textfield";i:1;s:0:"";s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}s:24:"brand-prop-5a97b11b96a34";a:4:{i:0;s:23:"http://schema.org/Brand";i:1;O:34:"wpbuddy\rich_snippets\Rich_Snippet":5:{s:2:"id";s:18:"snip-5a97c9ca704f4";s:7:"context";s:17:"http://schema.org";s:4:"type";s:5:"Brand";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;s:23:"name-prop-5a97c9ca7ac67";a:4:{i:0;s:9:"textfield";i:1;s:0:"";s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}}s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}}s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}s:25:"author-prop-5a97b109b22c3";a:4:{i:0;s:24:"http://schema.org/Person";i:1;O:34:"wpbuddy\rich_snippets\Rich_Snippet":6:{s:2:"id";s:18:"snip-5a97bf5c0ced0";s:7:"context";s:17:"http://schema.org";s:4:"type";s:6:"Person";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;s:23:"name-prop-5a97bf5c163fb";a:4:{i:0;s:24:"current_post_author_name";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:22:"url-prop-5a97bf5c23b4a";a:4:{i:0;s:23:"current_post_author_url";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}}s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:30:"description-prop-5a97b109c6003";a:4:{i:0;s:20:"current_post_excerpt";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:32:"datePublished-prop-5a97b109d0961";a:4:{i:0;s:17:"current_post_date";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:31:"reviewRating-prop-5a97bde8d91fc";a:4:{i:0;s:18:"misc_rating_5_star";i:1;i:4;s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}s:29:"reviewBody-prop-5a97bebcecb07";a:4:{i:0;s:20:"current_post_content";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:31:"dateModified-prop-5a97bfb128a9c";a:4:{i:0;s:26:"current_post_modified_date";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}}}' );

		return array(
			'title'    => __( 'Review of Product', 'rich-snippets-schema' ),
			'status'   => 'draft',
			'position' => '',
			'schema'   => $snippet,
		);
	}


	/**
	 * @since 2.3.1
	 *
	 * @return array
	 */
	public static function product_woocommerce() {

		$position = unserialize( 'O:38:"wpbuddy\rich_snippets\Position_Ruleset":1:{s:50:" wpbuddy\rich_snippets\Position_Ruleset rulegroups";a:1:{i:0;O:41:"wpbuddy\rich_snippets\Position_Rule_Group":1:{s:48:" wpbuddy\rich_snippets\Position_Rule_Group rules";a:1:{i:0;O:35:"wpbuddy\rich_snippets\Position_Rule":3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:7:"product";}}}}}' );

		$snippet = unserialize( 'a:1:{s:23:"snip-global-product-woo";O:34:"wpbuddy\rich_snippets\Rich_Snippet":15:{s:2:"id";s:23:"snip-global-product-woo";s:7:"context";s:17:"http://schema.org";s:4:"type";s:7:"Product";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;s:22:"sku-prop-5a90e4c190ec5";a:4:{i:0;s:15:"woocommerce_sku";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:22:"mpn-prop-5a90e4c1a707e";a:4:{i:0;s:9:"textfield";i:1;s:0:"";s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}s:24:"gtin8-prop-5a90e4c1bc50c";a:4:{i:0;s:9:"textfield";i:1;s:0:"";s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}s:24:"image-prop-5a90e4c1cf2ec";a:4:{i:0;s:29:"http://schema.org/ImageObject";i:1;O:34:"wpbuddy\rich_snippets\Rich_Snippet":7:{s:2:"id";s:18:"snip-5a97cc0643099";s:7:"context";s:17:"http://schema.org";s:4:"type";s:11:"ImageObject";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;s:25:"height-prop-5a97cc064cf42";a:4:{i:0;s:29:"current_post_thumbnail_height";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:24:"width-prop-5a97cc065a42e";a:4:{i:0;s:28:"current_post_thumbnail_width";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:22:"url-prop-5a97cc066871e";a:4:{i:0;s:26:"current_post_thumbnail_url";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}}s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:23:"name-prop-5a90e4c1e3c44";a:4:{i:0;s:18:"current_post_title";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:34:"aggregateRating-prop-5a90e4c20773c";a:4:{i:0;s:18:"woocommerce_rating";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:25:"offers-prop-5a90e4c21d251";a:4:{i:0;s:18:"woocommerce_offers";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:25:"gtin14-prop-5a90e4c23b1a4";a:4:{i:0;s:9:"textfield";i:1;s:0:"";s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}s:25:"gtin13-prop-5a90e4c261b5e";a:4:{i:0;s:9:"textfield";i:1;s:0:"";s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}s:30:"description-prop-5a90e4c297536";a:4:{i:0;s:20:"current_post_excerpt";i:1;N;s:11:"overridable";b:0;s:20:"overridable_multiple";b:0;}s:24:"brand-prop-5a90e4c2b5e88";a:4:{i:0;s:23:"http://schema.org/Brand";i:1;O:34:"wpbuddy\rich_snippets\Rich_Snippet":5:{s:2:"id";s:18:"snip-5a97cc562cbb3";s:7:"context";s:17:"http://schema.org";s:4:"type";s:5:"Brand";s:45:" wpbuddy\rich_snippets\Rich_Snippet _is_ready";b:0;s:23:"name-prop-5a97cc563c122";a:4:{i:0;s:9:"textfield";i:1;s:0:"";s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}}s:11:"overridable";b:1;s:20:"overridable_multiple";b:0;}}}' );

		return array(
			'title'    => __( 'Product (WooCommerce)', 'rich-snippets-schema' ),
			'position' => $position,
			'status'   => 'draft',
			'schema'   => $snippet,
		);
	}
}
