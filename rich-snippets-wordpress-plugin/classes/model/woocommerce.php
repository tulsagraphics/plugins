<?php

namespace wpbuddy\rich_snippets;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


/**
 * Class WooCommerce_Model.
 *
 * Recognizes the WooCommerce plugin and provides new fields.
 *
 * @package wpbuddy\rich_snippets
 *
 * @since   2.2.0
 */
final class WooCommerce_Model {

	/**
	 * @param $values
	 *
	 * @return mixed
	 */
	public static function internal_subselect( $values ) {

		if ( ! function_exists( 'WC' ) ) {
			return $values;
		}

		$values['http://schema.org/AggregateRating'][] = array(
			'id'     => 'woocommerce_rating',
			'label'  => esc_html_x( 'Current Product Rating (WooCommerce)', 'subselect field', 'rich-snippets-schema' ),
			'method' => array( 'wpbuddy\rich_snippets\WooCommerce_Model', 'rating' ),
		);

		$values['http://schema.org/Rating'][] = array(
			'id'     => 'woocommerce_rating',
			'label'  => esc_html_x( 'Current Product Rating (WooCommerce)', 'subselect field', 'rich-snippets-schema' ),
			'method' => array( 'wpbuddy\rich_snippets\WooCommerce_Model', 'rating' ),
		);

		$values['http://schema.org/Text'][] = array(
			'id'     => 'woocommerce_sku',
			'label'  => esc_html_x( 'Stock Keeping Unit (WooCommerce)', 'subselect field', 'rich-snippets-schema' ),
			'method' => array( 'wpbuddy\rich_snippets\WooCommerce_Model', 'sku' ),
		);

		$values['http://schema.org/Offer'][] = array(
			'id'     => 'woocommerce_offers',
			'label'  => esc_html_x( 'Offers (WooCommerce)', 'subselect field', 'rich-snippets-schema' ),
			'method' => array( 'wpbuddy\rich_snippets\WooCommerce_Model', 'offers' ),
		);

		$values['http://schema.org/QuantitativeValue'][] = array(
			'id'     => 'woocommerce_height',
			'label'  => esc_html_x( 'Product Height (WooCommerce)', 'subselect field', 'rich-snippets-schema' ),
			'method' => array( 'wpbuddy\rich_snippets\WooCommerce_Model', 'height' ),
		);

		$values['http://schema.org/QuantitativeValue'][] = array(
			'id'     => 'woocommerce_width',
			'label'  => esc_html_x( 'Product Width (WooCommerce)', 'subselect field', 'rich-snippets-schema' ),
			'method' => array( 'wpbuddy\rich_snippets\WooCommerce_Model', 'width' ),
		);

		$values['http://schema.org/QuantitativeValue'][] = array(
			'id'     => 'woocommerce_weight',
			'label'  => esc_html_x( 'Product Weight (WooCommerce)', 'subselect field', 'rich-snippets-schema' ),
			'method' => array( 'wpbuddy\rich_snippets\WooCommerce_Model', 'weight' ),
		);

		return $values;
	}


	/**
	 * Returns the value of the current rating.
	 *
	 * @param                                     $val
	 * @param \wpbuddy\rich_snippets\Rich_Snippet $rich_snippet
	 * @param array                               $meta_info
	 *
	 * @since 2.2.0
	 *
	 * @return string
	 */
	public static function rating( $val, Rich_Snippet $rich_snippet, array $meta_info ) {

		$product = wc_get_product( $meta_info['current_post_id'] );

		if ( ! $product instanceof \WC_Product ) {
			$rating_value = 0;
			$rating_count = 0;
		} else {
			$rating_value = floatval( $product->get_average_rating( 'raw' ) );
			$rating_count = floatval( $product->get_rating_count( 'raw' ) );
		}

		$rating_snippet       = new Rich_Snippet();
		$rating_snippet->type = 'AggregateRating';


		$rating_snippet->set_props( array(
			array(
				'name'  => 'ratingCount',
				'value' => max( 1, $rating_count ), # ratingCount must not be 0
			),
			array(
				'name'  => 'bestRating',
				'value' => 5,
			),
			array(
				'name'  => 'ratingValue',
				'value' => $rating_value,
			),
			array(
				'name'  => 'worstRating',
				'value' => $rating_count <= 0 ? 0 : 1, # worstRating must be 0 if ratingCount is 0
			),
		) );

		$rating_snippet->prepare_for_output();

		return $rating_snippet;
	}


	/**
	 * Returns the value of the current SKU.
	 *
	 * @param                                     $val
	 * @param \wpbuddy\rich_snippets\Rich_Snippet $rich_snippet
	 * @param array                               $meta_info
	 *
	 * @since 2.2.0
	 *
	 * @return string
	 */
	public static function sku( $val, Rich_Snippet $rich_snippet, array $meta_info ) {

		$product = wc_get_product( $meta_info['current_post_id'] );

		if ( ! $product instanceof \WC_Product ) {
			return '';
		}

		return (string) $product->get_sku( 'raw' );
	}


	/**
	 * Returns the offer for a WooCommerce product.
	 *
	 * @since 2.2.0
	 *
	 * @param int $post_id
	 *
	 * @return \stdClass
	 */
	private static function offer( $post_id ) {

		/**
		 * @var \WC_Product_Variation
		 */
		$product = wc_get_product( $post_id );
		$helper  = Helper_Model::instance();

		$obj                = new \stdClass();
		$obj->{'@context'}  = 'http://schema.org';
		$obj->{'@type'}     = 'Offer';
		$obj->availability  = 'https://schema.org/' . ( $product->is_in_stock() ? 'InStock' : 'OutOfStock' );
		$obj->priceCurrency = get_woocommerce_currency();
		$obj->price         = wc_format_decimal( $product->get_price(), wc_get_price_decimals() );

		$item_offered               = new \stdClass();
		$item_offered->{'@context'} = 'http://schema.org';
		$item_offered->{'@type'}    = 'IndividualProduct';
		$item_offered->url          = $product->get_permalink();
		$item_offered->sku          = $product->get_sku();

		if ( wc_product_dimensions_enabled() ) {
			$item_offered->width  = self::get_product_quantitive_snippet( $post_id, 'width' );
			$item_offered->height = self::get_product_quantitive_snippet( $post_id, 'height' );
		}

		if ( wc_product_weight_enabled() ) {
			$item_offered_weight               = new \stdClass();
			$item_offered_weight->{'@context'} = 'http://schema.org';
			$item_offered_weight->{'@type'}    = 'QuantitativeValue';
			$item_offered_weight->value        = $product->get_weight();
			$item_offered_weight->unitCode     = get_option( 'woocommerce_weight_unit' );
			$item_offered->weight              = $item_offered_weight;
		}

		$item_offered->name        = $product->get_title();
		$item_offered->description = $product->get_description();

		$image               = new \stdClass();
		$image->{'@context'} = 'http://schema.org';
		$image->{'@type'}    = 'ImageObject';
		$image->url          = $helper->get_media_meta( 'url', (int) $product->get_image_id() );
		$image->width        = $helper->get_media_meta( 'width', (int) $product->get_image_id() );
		$image->height       = $helper->get_media_meta( 'height', (int) $product->get_image_id() );
		$item_offered->image = $image;


		$obj->itemOffered = $item_offered;

		return $obj;
	}

	/**
	 * Returns a snippet of all offers.
	 *
	 * @param                                     $val
	 * @param \wpbuddy\rich_snippets\Rich_Snippet $rich_snippet
	 * @param array                               $meta_info
	 *
	 * @since 2.2.0
	 *
	 * @return array
	 */
	public static function offers( $val, Rich_Snippet $rich_snippet, array $meta_info ) {

		$offers = [];

		$product = wc_get_product( $meta_info['current_post_id'] );

		if ( $product instanceof \WC_Product_Variable ) {
			foreach ( $product->get_visible_children() as $child_product_id ) {
				$offers[] = self::offer( $child_product_id );
			}

		} elseif ( $product instanceof \WC_Product ) {
			$offers[] = self::offer( $meta_info['current_post_id'] );
		}

		return $offers;
	}


	/**
	 * Returns the height of a product.
	 *
	 * @param                                     $val
	 * @param \wpbuddy\rich_snippets\Rich_Snippet $rich_snippet
	 * @param array                               $meta_info
	 *
	 * @since 2.2.0
	 *
	 * @return \stdClass
	 */
	public static function height( $val, Rich_Snippet $rich_snippet, array $meta_info ) {

		return self::get_product_quantitive_snippet( $meta_info['current_post_id'], 'height' );
	}


	/**
	 * Returns the width of a product.
	 *
	 * @param                                     $val
	 * @param \wpbuddy\rich_snippets\Rich_Snippet $rich_snippet
	 * @param array                               $meta_info
	 *
	 * @since 2.2.0
	 *
	 * @return \stdClass
	 */
	public static function width( $val, Rich_Snippet $rich_snippet, array $meta_info ) {

		return self::get_product_quantitive_snippet( $meta_info['current_post_id'], 'width' );
	}


	/**
	 * Get WooCommerce product dimension as a snippet.
	 *
	 * @param int    $product_id
	 * @param string $prop width|height|weight
	 *
	 * since 2.2.0
	 *
	 * @return \stdClass
	 */
	private static function get_product_quantitive_snippet( $product_id, $prop ) {

		$product = wc_get_product( $product_id );

		$item = new \stdClass();

		if ( $product instanceof \WC_Product ) {
			return $item;
		}

		$item->{'@context'} = 'http://schema.org';
		$item->{'@type'}    = 'QuantitativeValue';
		$item->value        = method_exists( $product, $prop ) ? $product->{$prop}() : '';
		$item->unitCode     = 'weight' === $prop ? get_option( 'woocommerce_weight_unit' ) : get_option( 'woocommerce_dimension_unit' );

		return $item;
	}


	/**
	 * Returns the eight of a product.
	 *
	 * @param                                     $val
	 * @param \wpbuddy\rich_snippets\Rich_Snippet $rich_snippet
	 * @param array                               $meta_info
	 *
	 * @since 2.2.0
	 *
	 * @return \stdClass
	 */
	public static function weight( $val, Rich_Snippet $rich_snippet, array $meta_info ) {

		return self::get_product_quantitive_snippet( $meta_info['current_post_id'], 'weight' );
	}
}
