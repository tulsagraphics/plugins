<?php

namespace wpbuddy\rich_snippets;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * @var \WP_Post $post
 */
$post           = $this->arguments[0];
$snippets       = Snippets_Model::get_snippets( $post->ID );
$snippets_count = count( $snippets );
?>

<div class="wpb-rs-loader updating-message"><p class="updating-message"></p></div>

<div id="wpb_rs_snippets" data-snippets-loaded="<?php echo intval( ! ( $snippets_count > 0 ) ); ?>">
	<?php
	if ( $snippets_count > 0 ) {
		?>
		<p class="wpb-rs-available-schemas-text">
			<?php
			printf( _n(
				'One snippet is attached to this post. Want to edit it now?',
				'%s snippets are attached to this post. Want to edit them now?',
				$snippets_count,
				'rich-snippets-schema'
			), $snippets_count );
			?>
			<button class="button wpb-rs-load-snippets">
				<?php _e( 'Yes' ); ?>
			</button>
		</p>
		<?php
	}
	?>
</div>
<button class="button button-big wpb-rs-add-snippet">
	<span class="dashicons dashicons-plus-alt"></span>
	<?php _e( 'Add a Rich Snippet', 'rich-snippets-schema' ); ?>
</button>
