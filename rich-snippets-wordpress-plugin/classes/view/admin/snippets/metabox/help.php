<p><?php _e( 'Need some help? Let\'s start with this introduction video:' ); ?></p>
<a href="<?php echo esc_attr( _x( 'https://rich-snippets.io/how-to-add-rich-snippets-en/', 'Url to how-to video', 'rich-snippets-schema' ) ); ?>"
   target="_blank"><img
			src="<?php echo esc_attr( _x( 'https://rich-snippets.io/wp-content/uploads/2017/08/create-a-rich-snippet-mockup-english.jpg', 'Url to how-to video image', 'rich-snippets-schema' ) ); ?>"/></a>
