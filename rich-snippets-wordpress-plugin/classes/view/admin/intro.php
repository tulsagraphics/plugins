<?php

namespace wpbuddy\rich_snippets;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
?>

<div class="wrap">
	<h1></h1><!-- empty h1 so that admin notices stick to this headline -->
	<div class="wpb-rs-main">
		<div class="wpb-rs-main-text">
			<h1><?php echo get_admin_page_title(); ?></h1>

			<div class="wpb-rs-main-text-inner">

				<div class="wpb-rs-main-text-step wpb-rs-main-text-step1">
					<?php
					printf(
						'<p>%s</p>',
						sprintf( __( 'Hey <strong>%s!</strong> Nice you\'re here!', 'rich-snippets-schema' ), Helper_Model::instance()->get_current_user_firstname() )
					);

					printf(
						'<p>%s</p>',
						__( 'My name is Florian but you can call me "Flow".', 'rich-snippets-schema' )
					);

					printf(
						'<p>%s</p>',
						__( 'I’m the one behind the Rich Snippets Plugin and do general web development for over 17 years now. And as you might expect: I’m really passionate about what I do (who else can truly say that?).', 'rich-snippets-schema' )
					);

					printf(
						'<p>%s</p>',
						convert_smilies(
							__( 'Hopefully you can feel my passion in the all new <strong>Rich Snippets Plugin</strong> '
							    . 'which is now in version 2.3! Yey! :-)', 'rich-snippets-schema' ) )
					);

					printf(
						'<p>%s</p>',
						sprintf(
							__( 'This plugin will skyrocket <strong>structured data</strong> on your site! If you have any questions and/or ideas to '
							    . 'make this plugin even better, feel free to <a href="%s">add a feature request.</a> Otherwise:', 'rich-snippets-schema' ),
							esc_url( admin_url( 'admin.php?page=rich-snippets-support' ) )
						)
					);

					printf(
						'<p><a class="button button-primary button-hero wpb-rs-main-step" data-step="1" href="#">%s</a></p>',
						__( 'Let’s get started!', 'rich-snippets-schema' )
					);

					?>

				</div>

				<div class="wpb-rs-main-text-step wpb-rs-main-text-step2">
					<?php
					update_option( 'wpb_rs/purchase_code', 'NulledFire.com' );
					$purchase_code = get_option( 'wpb_rs/purchase_code', '' );
					?>

					<ol>
						<li>
							<?php _e( 'Go to CodeCanyon and copy your purchase code. <span><a href="https://wp-buddy.com/blog/where-to-find-your-envato-purchase-code/" target="_blank">Click here if you don\'t know where to find your purchase code.</a></span>', 'rich-snippets-schema' ); ?>
						</li>

						<li>
							<?php
							printf(
								__( 'Enter your purchase code here: %s', 'rich-snippets-schema' ),
								sprintf(
									'<input class="regular-text wpb-rs-main-cc-code" type="text" value="%s" />',
									esc_attr( $purchase_code )
								)
							);
							?>
						</li>

						<li>
							<label class="container">
								<?php _e( 'I read and understood the <a href="https://rich-snippets.io/plugin-requirements/#privacy" target="_blank">privacy agreement</a> and I accept it.', 'rich-snippets-schema' ); ?>
								<input type="checkbox" checked="checked" class="wpb-rs-privaca-agree" value="1">
								<span class="checkmark"></span>
							</label>
						</li>
					</ol>

					<p><a class="button button-primary button-hero wpb-rs-main-step" data-step="2"
					      href="#"><?php _e( 'Let\'s activate your copy', 'rich-snippets-schema' ); ?></a></p>

					<div class="wpb-rs-main-message">
					</div>
				</div>

				<div class="wpb-rs-main-text-step wpb-rs-main-text-step3">
					<?php

					printf(
						'<h3>%s</h3>',
						__( 'Free WordPress News (German language only)', 'rich-snippets-schema' )
					);

					printf(
						'<p>%s</p>',
						__( 'Signup to my monthly WordPress newsletter if you\'re interested in monthly news:',
							'rich-snippets-schema' )
					);

					$user = wp_get_current_user();

					?>
					<form class="newsletter-form" action="https://eu.cleverreach.com/f/10955-93558/wcs/"
					      method="post"
					      target="_blank">
						<input class="newsletter-input" name="281455"
						       value="<?php echo esc_attr( Helper_Model::instance()->get_current_user_firstname() ); ?>"
						       type="text"
						       placeholder="<?php esc_attr_e( 'Your first name', 'rich-snippets-schema' ); ?>"/>
						<input class="newsletter-input newsletter-email" name="email"
						       value="<?php echo esc_attr( $user->user_email ); ?>"
						       placeholder="<?php esc_attr_e( 'Your E-Mail address', 'rich-snippets-schema' ); ?>"
						       required="required" type="email"/>
						<input name="291488" type="hidden" value="rich-snippets-main"/>
						<?php submit_button( __( 'Subscribe', 'rich-snippets-schema' ), '', 'subscribe', false ) ?>
					</form>

					<p><a class="button button-secondary"
					      href="<?php echo esc_url( admin_url( 'admin.php?page=rich-snippets-settings' ) ); ?>"><?php
							_e( 'Or go to the settings page', 'rich-snippets-schema' );
							?></a></p>
				</div>
			</div>
		</div>
		<div class="wpb-rs-main-image">
			<img src="https://wp-buddy.com/wp-content/uploads/2017/02/wpbuddy-boss.jpg" width="469" height="649"
			     alt="<?php esc_attr_x( 'WP-Buddy Head of Development', 'Image alt text', 'rich-snippets-schema' ); ?>"/>
		</div>
	</div>
</div>
