<?php

namespace wpbuddy\rich_snippets;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


/**
 * Class Rich_Snippets.
 *
 * Starts up all the good things.
 *
 * @package wpbuddy\rich_snippets
 *
 * @since   2.0.0
 */
final class Rich_Snippets_Plugin {

	/**
	 * The instance.
	 *
	 * @var Rich_Snippets_Plugin
	 *
	 * @since 2.0.0
	 */
	protected static $_instance = null;


	/**
	 * If the init method has been called.
	 *
	 * @var bool
	 *
	 * @since 2.0.0
	 */
	private $initialized = false;


	/**
	 * The plugin file path.
	 *
	 * @since 2.0.0
	 *
	 * @var string
	 */
	private $plugin_file = '';


	/**
	 * Activates debugging.
	 *
	 * @since 2.0.0
	 *
	 * @var bool
	 */
	private $debug = false;


	/**
	 * Get the singleton instance.
	 *
	 * Creates a new instance of the class if it does not exists.
	 *
	 * @return   Rich_Snippets_Plugin
	 *
	 * @since 2.0.0
	 */
	public static function instance() {

		if ( null === self::$_instance ) {
			self::$_instance = new self;
		}

		return self::$_instance;
	}


	/**
	 * Magic function for cloning.
	 *
	 * Disallow cloning as this is a singleton class.
	 *
	 * @since 2.0.0
	 */
	protected function __clone() {
	}


	/**
	 * Magic method for setting upt the class.
	 *
	 * Disallow external instances.
	 *
	 * @since 2.0.0
	 */
	protected function __construct() {
	}


	/**
	 * Init everything needed.
	 *
	 * @since 2.0.0
	 */
	public function init() {

		# Upps. This seems to be initialized already.
		if ( $this->initialized ) {
			return;
		}

		$this->debug = defined( 'WPB_RS_DEBUG' ) && WPB_RS_DEBUG;

		# set plugin path to file
		$this->plugin_file = WPB_RS_FILE;

		# load deprecated version of the plugin if the user wants to see it active
		$this->load_deprecated();

		register_activation_hook( $this->get_plugin_file(), array( self::instance(), 'on_activation' ) );
		register_deactivation_hook( $this->get_plugin_file(), array( self::instance(), 'on_deactivation' ) );

		add_action( 'init', array( 'wpbuddy\rich_snippets\Posttypes_Model', 'create_post_types' ) );

		if ( is_admin() ) {
			Admin_Controller::instance()->init();
		} else {
			new Frontend_Controller();
		}

		add_action( 'rest_api_init', array( 'wpbuddy\rich_snippets\Rest_Controller', 'init' ) );

		$this->third_party_init();

		# allow other plugins to hook into
		do_action_ref_array( 'wpbuddy/rich_snippets/init', self::$_instance );

		# After plugins have been updated
		add_action( 'upgrader_process_complete', function ( $upgrader, $args ) {

			/**
			 * @var \WP_Upgrader $upgrader
			 * @var array        $args
			 */

			if ( ! isset( $args['type'] ) ) {
				return;
			}

			if ( 'plugin' !== $args['type'] ) {
				return;
			}

			if ( ! isset( $args['plugins'] ) ) {
				return;
			}

			if ( ! is_array( $args['plugins'] ) ) {
				return;
			}

			if ( in_array( plugin_basename( WPB_RS_FILE ), $args['plugins'] ) ) {
				update_option( 'wpb_rs/upgraded', false, 'yes' );
			}

		}, 10, 2 );

		Cron_Model::add_cron_hooks();

		# done.
		$this->initialized = true;
	}


	/**
	 * Load deprecated stuff.
	 *
	 * @since 2.0.0
	 * @deprecated 2.0.0
	 */
	private function load_deprecated() {

		$is_active = (bool) get_option( 'wpb_rs/setting/deprecated_version', false );

		if ( ! $is_active ) {
			return;
		}

		add_action( 'admin_notices', array( $this, 'deprecated_plugin_message' ) );

		require_once( plugin_dir_path( $this->plugin_file ) . 'deprecated/1.6.3/rich-snippets-wordpress-plugin.php' );
	}


	/**
	 * Prints a deprecated message for users who are still using the old version.
	 *
	 * @since 2.0.0
	 * @deprecated 2.0.0
	 */
	public function deprecated_plugin_message() {

		$screen = get_current_screen();

		if ( false === stripos( $screen->id, 'rswp' ) ) {
			return;
		}

		printf(
			'<div class="notice notice-info"><p>%s</p><p><a class="button" href="%s" target="_blank">%s</a> <a class="button" href="%s">%s</a> <a class="button button-primary" href="%s" onclick="return confirm(\'%s\')">%s</a></p></div>',
			sprintf(
				__( 'Hey there! This is an old part of the Rich Snippets Plugin. It\'s deprecated and should no longer be used. It will be removed in a future version.', 'rich-snippets-schema' ),
				esc_html( PHP_VERSION )
			),
			'https://rich-snippets.io/deprecated-1-x-x/',
			_x( 'Read more', 'Read more about the fact that an old part of the plugin will soon be deprecated.', 'rich-snippets-schema' ),
			admin_url( 'admin.php?page=rich-snippets-schema' ),
			_x( 'Go to the new part', 'Button that redirects to version 2.0 settings page.', 'rich-snippets-schema' ),
			admin_url( 'admin.php?page=rich-snippets-settings&action=wpb_rs_uninstall_deprecated' ),
			__( 'Are you sure? This will uninstall the old part and remove all data including your existing shortcodes.', 'rich-snippets-schema' ),
			_x( 'Ok, I no longer need this part.', 'If a user no longer needs the deprecated version of the plugin.', 'rich-snippets-schema' )
		);
	}


	/**
	 * Returns the plugin file path.
	 *
	 * @since 2.0.0
	 *
	 * @return string
	 */
	public function get_plugin_file() {

		return $this->plugin_file;
	}


	/**
	 * Checks if debugging is on.
	 *
	 * @since 2.0.0
	 *
	 * @return bool
	 */
	public function debug(): bool {

		return $this->debug;
	}


	/**
	 * Performs actions on plugin activation.
	 *
	 * @since 2.0.0
	 */
	public function on_activation() {

		Admin_Settings_Controller::prepare_settings();

		Cron_Model::add_cron();
	}


	/**
	 * Performs actions on plugin deactivation.
	 *
	 * @since 2.0.0
	 */
	public function on_deactivation() {

		Cron_Model::remove_cron();
	}


	/**
	 * Included third party stuff.
	 *
	 * @since 2.2.0
	 */
	public function third_party_init() {

		add_filter( 'wpbuddy/rich_snippets/fields/internal_subselect/values', array(
			'wpbuddy\rich_snippets\YoastSEO_Model',
			'internal_subselect',
		) );

		add_filter( 'wpbuddy/rich_snippets/fields/internal_subselect/values', array(
			'wpbuddy\rich_snippets\WooCommerce_Model',
			'internal_subselect',
		) );

		add_filter( 'wpbuddy/rich_snippets/fields/internal_subselect/values', array(
			'wpbuddy\rich_snippets\MiscFields_Model',
			'internal_subselect',
		) );

		add_action(
			'wpbuddy/rich_snippets/rest/property/html/fields',
			array(
				'wpbuddy\rich_snippets\MiscFields_Model',
				'fields',
			),
			10, 1
		);
	}

}
