<?php

namespace wpbuddy\rich_snippets;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


/**
 * Json object.
 *
 * @package wpbuddy\rich_snippets
 *
 * @since   2.0.0
 */
class Rich_Snippet {

	/**
	 * The snippet ID.
	 *
	 * @since 2.0.0
	 *
	 * @var string
	 */
	public $id = '';


	/**
	 * The context.
	 *
	 * @since 2.0.0
	 *
	 * @var string
	 */
	public $context = 'http://schema.org';


	/**
	 * The type.
	 *
	 * @since 2.0.0
	 *
	 * @var string
	 */
	public $type = 'Thing';


	/**
	 * If the object has been prepared for output.
	 *
	 * @since 2.0,0
	 *
	 * @var bool
	 */
	private $_is_ready = false;


	/**
	 * Rich_Snippet constructor.
	 *
	 * @since 2.0.0
	 *
	 * @param array
	 */
	public function __construct() {

		$this->id = uniqid( 'snip-' );
	}


	/**
	 * Sets properties.
	 *
	 * @since 2.0.0
	 *
	 * @param array $props
	 */
	public function set_props( $props = array() ) {

		foreach ( $props as $prop ) {
			$this->set_prop( $prop['name'], $prop['value'], isset( $prop['id'] ) ? $prop['id'] : null );
		}
	}


	/**
	 * Sets a single property.
	 *
	 * Will add a '-prop-xxx' unique ID to each property that is not a class var.
	 *
	 * @since 2.0.0
	 *
	 * @param string      $name
	 * @param mixed       $value
	 * @param string|null $id A unique ID for this prop (without the '-prop-' prefix)
	 */
	public function set_prop( $name, $value, $id = null ) {

		if ( empty( $id ) ) {
			$id = uniqid( '-prop-' );
		} else {
			if ( false !== stripos( $id, 'prop-' ) ) {
				$id = '-' . $id;
			} else {
				$id = '-prop-' . $id;
			}
		}

		$this->{$name . $id} = $value;
	}


	/**
	 * Returns an array of properties.
	 *
	 * @since 2.0.0
	 *
	 * @return Schema_Property[]
	 */
	public function get_properties() {

		$object_vars = get_object_vars( $this );

		$class_vars = get_class_vars( __CLASS__ );

		$object_props = array_diff_key( $object_vars, $class_vars );

		$props = array();

		foreach ( $object_props as $k => $v ) {
			$prop_id  = $this->normalize_property_name( $k );
			$prop_uid = str_replace( $prop_id . '-prop-', '', $k );
			$prop_id  = 'http://schema.org/' . $prop_id;

			$prop = Schemas_Model::get_property_by_id( $prop_id );

			if ( $prop instanceof Schema_Property ) {
				$prop->value                = $v;
				$prop->overridable          = $v['overridable'] ?? false;
				$prop->overridable_multiple = $v['overridable_multiple'] ?? false;
				$prop->uid                  = $prop_uid;

				$props[] = $prop;
			}
		}

		return $props;
	}


	/**
	 * Removes "-prop-*****" names from property names.
	 *
	 * @since 2.0.0
	 *
	 * @param string $prop
	 *
	 * @return string
	 */
	private function normalize_property_name( $prop ) {

		$prop_id = strstr( $prop, '-prop-', true );

		return str_replace( '-prop-', '', $prop_id );
	}


	/**
	 * Returns a property value for a given full url (e.g. https://schema.org/image )
	 *
	 * @since 2.0.0
	 *
	 * @param string $url
	 *
	 * @return mixed|null Null if value does not exist.
	 */
	public function get_property_value_by_path( $url ) {

		$url      = untrailingslashit( $url );
		$val_name = Helper_Model::instance()->remove_schema_url( $url );

		if ( isset( $this->{$val_name} ) ) {
			return $this->{$val_name};
		}

		return null;
	}


	/**
	 * Outputs a JSON-String of the object.
	 *
	 * @since 2.0.0
	 *
	 * @return string
	 */
	public function __toString(): string {

		if ( ! $this->_is_ready ) {
			return sprintf( '<!--%s-->',
				__( 'Object is not ready for output, yet. Please call \wpbuddy\rich_snippets\Rich_Snippet::prepare_for_output() first.', 'rich-snippets-schema' )
			);
		}

		return json_encode( $this );
	}


	/**
	 * Prepares object for output.
	 *
	 * @since 2.0.0
	 *
	 * @param array $meta_info
	 *
	 * @return \wpbuddy\rich_snippets\Rich_Snippet
	 *
	 */
	public function prepare_for_output( array $meta_info = array() ): Rich_Snippet {

		$meta_info = wp_parse_args( $meta_info, array(
			'current_post_id' => 0,
			'snippet_post_id' => 0,
		) );

		if ( $this->_is_ready ) {
			return $this;
		}

		# overwrite values, if any
		$this->overwrite_values( $meta_info );

		# merge multiple properties together
		$this->merge_multiple_props();

		# fill all values
		$this->fill_values( $meta_info );

		# rename some properties
		$this->{'@context'} = $this->context;
		$this->{'@type'}    = $this->type;

		# delete all internal object vars
		foreach ( array_keys( get_class_vars( __CLASS__ ) ) as $k ) {
			unset( $this->{$k} );
		}

		# filter empty props if they are not integers or floats
		foreach ( array_keys( get_object_vars( $this ) ) as $k ) {
			if ( ! ( is_int( $this->{$k} ) || is_float( $this->{$k} ) ) && empty( $this->{$k} ) ) {
				unset( $this->{$k} );
			}

			/**
			 * Workaround: Scalar values need to be transformed to strings.
			 * This is because the structured data test tools don't like integer values.
			 */
			if ( isset( $this->{$k} ) && is_scalar( $this->{$k} ) ) {
				$this->{$k} = (string) $this->{$k};
			}
		}

		do_action_ref_array(
			'wpbuddy/rich_snippets/rich_snippet/prepare',
			array( &$this )
		);

		$this->_is_ready = true;

		return $this;
	}


	/**
	 * Returns the value.
	 *
	 * @param mixed $var A key-value pair where the first is the field type and the second is the value itself.
	 * @param array $meta_info
	 *
	 * @see \wpbuddy\rich_snippets\Admin_Snippets_Controller::search_value_by_id()
	 *
	 * @since 2.0.0
	 *
	 * @return mixed
	 */
	private function get_the_value( $var, array $meta_info ) {

		if ( is_array( $var ) ) {
			if ( isset( $var[1] ) && ( $var[1] instanceof Rich_Snippet ) ) {
				$var = $var[1]->prepare_for_output( $meta_info );
			} else {
				$field_type = $var[0];

				if ( empty( $field_type ) ) {
					return '';
				}

				$var = $var[1];

				/**
				 * Rich_Snippet value filter.
				 *
				 * Allows plugins to hook into the value that will be outputted later.
				 *
				 * @since 2.0.0
				 *
				 * @param mixed        $value The value.
				 * @param string       $field_type The field type (ie. textfield).
				 * @param Rich_Snippet $object The current Rich_Snippet object.
				 *
				 */
				$var = apply_filters( 'wpbuddy/rich_snippets/rich_snippet/value', $var, $field_type, $this, $meta_info );

				/**
				 * Rich_Snippet value type filter.
				 *
				 * Allows plugins to hook into the value. The last parameter is the $field_type (ie. textfield).
				 *
				 * @since 2.0.0
				 *
				 * @param mixed        $value The value.
				 * @param Rich_Snippet $object The current Rich_Snippet object.
				 *
				 */
				$var = apply_filters( 'wpbuddy/rich_snippets/rich_snippet/value/' . $field_type, $var, $this, $meta_info );

			}
		}

		return $var;

	}


	/**
	 * Gets the main type i.e. http://schema.org/Thing
	 *
	 * @since 2.0.0
	 *
	 * @return string
	 */
	public function get_type(): string {

		return trailingslashit( $this->context ) . $this->type;
	}


	/**
	 * Checks if a snippet has properties.
	 *
	 * @since 2.0.0
	 *
	 * @return bool
	 */
	public function has_properties(): bool {

		$object_vars = get_object_vars( $this );

		$class_vars = get_class_vars( __CLASS__ );

		$object_props = array_diff_key( $object_vars, $class_vars );

		return count( $object_props ) > 1;
	}


	/**
	 * Merges multiple props together.
	 *
	 * @since 2.0.0
	 */
	private function merge_multiple_props() {

		$vars = get_object_vars( $this );

		$class_vars = get_class_vars( __CLASS__ );

		$props = array_diff_key( $vars, $class_vars );

		foreach ( $props as $prop_key => $prop_value ) {
			$real_prop_name = $this->normalize_property_name( $prop_key );

			if ( ! isset( $this->{$real_prop_name} ) ) {
				$this->{$real_prop_name} = $prop_value;
				unset( $this->{$prop_key} );
				continue;
			}

			if ( ! $this->{$real_prop_name} instanceof Multiple_Property ) {
				# create new Multiple_Property
				$mp = new Multiple_Property();

				# copy the previous value
				$mp[] = $this->{$real_prop_name};

				# replace the previous value
				$this->{$real_prop_name} = $mp;
			}

			$this->{$real_prop_name}[] = $prop_value;

			unset( $this->{$prop_key} );
		}
	}


	/**
	 * Fills property values.
	 *
	 * @param array $meta_info
	 */
	private function fill_values( $meta_info ) {

		$vars = get_object_vars( $this );

		$class_vars = get_class_vars( __CLASS__ );

		$props = array_diff_key( $vars, $class_vars );

		foreach ( $props as $name => $var ) {
			if ( ! $this->{$name} instanceof Multiple_Property ) {
				$this->{$name} = $this->get_the_value( $var, $meta_info );
			} else {
				$sub_props = array();
				foreach ( $this->{$name} as $sub_prop_key => $sub_prop ) {
					$sub_props[ $sub_prop_key ] = $this->get_the_value( $sub_prop, $meta_info );
				}
				$this->{$name} = $sub_props;
			}
		}
	}


	/**
	 * Checks if the snippet has properties that can be overwritten.
	 *
	 * @since 2.2.0
	 *
	 * @return bool
	 */
	public function has_overridable_props() {

		$vars = get_object_vars( $this );

		$class_vars = get_class_vars( __CLASS__ );

		$props = array_diff_key( $vars, $class_vars );

		foreach ( $props as $name => $var ) {
			if ( ! isset( $var['overridable'] ) ) {
				continue;
			}

			if ( $var['overridable'] ) {
				return true;
			}
		}

		return false;
	}


	/**
	 * Overwrites values, if any.
	 *
	 * @since 2.2.0
	 *
	 * @param array $meta_info
	 *
	 */
	public function overwrite_values( $meta_info ) {

		if ( empty( $meta_info['current_post_id'] ) ) {
			return;
		}

		$overwrite_data = get_post_meta( $meta_info['current_post_id'], '_wpb_rs_overwrite_data', true );

		$vars = get_object_vars( $this );

		$class_vars = get_class_vars( __CLASS__ );

		$props = array_diff_key( $vars, $class_vars );

		foreach ( $props as $prop_name_with_id => $prop_value ) {
			if ( ! isset( $prop_value['overridable'] ) ) {
				continue;
			}

			if ( ! $prop_value['overridable'] ) {
				continue;
			}

			$prop_id = str_replace( 'prop-', '', strstr( $prop_name_with_id, 'prop-' ) );

			if ( ! isset( $overwrite_data[ $this->id ]['properties'][ $prop_id ] ) ) {
				continue;
			}

			$overwrite_value = $overwrite_data[ $this->id ]['properties'][ $prop_id ];

			if ( isset( $prop_value['overridable_multiple'] ) && $prop_value['overridable_multiple'] && is_array( $overwrite_value ) ) {
				# Add the first value to the current property
				$this->{$prop_name_with_id}[1] = array_shift( $overwrite_value );

				# Create new properties for the rest of the values.
				foreach ( $overwrite_value as $k => $ov ) {
					$new_prop_name_with_id             = $prop_name_with_id . '-' . $k;
					$this->{$new_prop_name_with_id}    = $this->{$prop_name_with_id};
					$this->{$new_prop_name_with_id}[1] = $ov;
				}

			} else {
				$this->{$prop_name_with_id}[1] = $overwrite_value;
			}
		}
	}
}
