<?php
/*
Plugin Name: MainWP Url Extractor Extension
Plugin URI: https://mainwp.com
Description: MainWP URL Extractor allows you to search your child sites post and pages and export URLs in customized format. Requires MainWP Dashboard plugin.
Version: 1.0
Author: MainWP
Author URI: https://mainwp.com
Documentation URI: http://docs.mainwp.com/category/mainwp-extensions/mainwp-url-extractor-extension/
Support Forum URI: https://mainwp.com/forum/forumdisplay.php?93-Sucuri-Extension
Icon URI:
*/

if ( ! defined( 'MAINWP_URL_EXTRACTOR_PLUGIN_FILE' ) ) {
	define( 'MAINWP_URL_EXTRACTOR_PLUGIN_FILE', __FILE__ );
}

class MainWP_Url_Extractor_Extension
{
	public static $instance = null;
	public  $plugin_handle = 'mainwp-url-extractor-extension';
	protected $plugin_url;
	private $plugin_slug;
	protected $plugin_dir;
	private $mainWPExtractUrls = null;

	static function get_instance() {

		if ( null == MainWP_Url_Extractor_Extension::$instance ) { MainWP_Url_Extractor_Extension::$instance = new MainWP_Url_Extractor_Extension(); }
		return MainWP_Url_Extractor_Extension::$instance;
	}

	public function __construct() {

		$this->plugin_dir = plugin_dir_path( __FILE__ );
		$this->plugin_url = plugin_dir_url( __FILE__ );
		$this->plugin_slug = plugin_basename( __FILE__ );

		add_action( 'init', array( &$this, 'init' ) );
		add_filter( 'plugin_row_meta', array( &$this, 'plugin_row_meta' ), 10, 2 );
		add_action( 'after_plugin_row', array( &$this, 'after_plugin_row' ), 10, 3 );
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_action( 'mainwp_admin_menu', array( &$this, 'init_menu' ), 9 );
		MainWP_Extract_Urls_DB::get_instance()->install();
		$this->mainWPExtractUrls = new MainWP_Extract_Urls();
	}

	public function init() {

		$this->mainWPExtractUrls->init();
	}

	public function init_menu() {
		$this->mainWPExtractUrls->init_menu();
	}

	public function plugin_row_meta( $plugin_meta, $plugin_file ) {

		if ( $this->plugin_slug != $plugin_file ) { return $plugin_meta; }

		$slug = basename($plugin_file, ".php");
		$api_data = get_option( $slug. '_APIManAdder');		
		if (!is_array($api_data) || !isset($api_data['activated_key']) || $api_data['activated_key'] != 'Activated' || !isset($api_data['api_key']) || empty($api_data['api_key']) ) {
			return $plugin_meta;
		}
		
		$plugin_meta[] = '<a href="?do=checkUpgrade" title="Check for updates.">Check for updates now</a>';
		return $plugin_meta;
	}
	
	public function after_plugin_row( $plugin_file, $plugin_data, $status ) {	
		if ( $this->plugin_slug != $plugin_file ) {
			return ;
		}	
		$slug = basename($plugin_file, ".php");
		$api_data = get_option( $slug. '_APIManAdder');
		
		if (!is_array($api_data) || !isset($api_data['activated_key']) || $api_data['activated_key'] != 'Activated'){
			if (!isset($api_data['api_key']) || empty($api_data['api_key'])) {
				?>
				<style type="text/css">
					tr#<?php echo $slug;?> td, tr#<?php echo $slug;?> th{
						box-shadow: none;
					}
				</style>
				<tr class="plugin-update-tr active"><td colspan="3" class="plugin-update colspanchange"><div class="update-message api-deactivate">
				<?php echo (sprintf(__("API not activated check your %sMainWP account%s for updates. For automatic update notification please activate the API.", "mainwp"), '<a href="https://mainwp.com/my-account" target="_blank">', '</a>')); ?>
				</div></td></tr>
				<?php
			}
		}		
	}	

	public function admin_init() {

		wp_enqueue_style( 'mainwp-extract-urls-extension', $this->plugin_url . 'css/mainwp-extract.css' );
		if ( isset( $_REQUEST['page'] ) && 'Extensions-Mainwp-Url-Extractor-Extension' == $_REQUEST['page'] ) {
			wp_enqueue_script( 'mainwp-extract-urls-extension', $this->plugin_url . 'js/mainwp-extract.js' );
		}
		$this->mainWPExtractUrls->admin_init();
	}
}


function mainwp_extract_urls_extension_autoload( $class_name ) {

	$allowedLoadingTypes = array( 'class' );
	$class_name = str_replace( '_', '-', strtolower( $class_name ) );
	foreach ( $allowedLoadingTypes as $allowedLoadingType ) {
		$class_file = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . str_replace( basename( __FILE__ ), '', plugin_basename( __FILE__ ) ) . $allowedLoadingType . DIRECTORY_SEPARATOR . $class_name . '.' . $allowedLoadingType . '.php';
		if ( file_exists( $class_file ) ) {
			require_once( $class_file );
		}
	}
}

if ( function_exists( 'spl_autoload_register' ) ) {
	spl_autoload_register( 'mainwp_extract_urls_extension_autoload' );
} else {
	function __autoload( $class_name ) {

		mainwp_extract_urls_extension_autoload( $class_name );
	}
}

register_activation_hook( __FILE__, 'extract_extension_activate' );
register_deactivation_hook( __FILE__, 'extract_extension_deactivate' );
function extract_extension_activate() {
	update_option( 'mainwp_extract_extension_activated', 'yes' );
	$extensionActivator = new MainWP_Url_Extractor_Extension_Activator();
	$extensionActivator->activate();	
}
function extract_extension_deactivate() {

	$extensionActivator = new MainWP_Url_Extractor_Extension_Activator();
	$extensionActivator->deactivate();
}

class MainWP_Url_Extractor_Extension_Activator
{
	protected $mainwpMainActivated = false;
	protected $childEnabled = false;
	protected $childKey = false;
	protected $childFile;
	protected $plugin_handle = 'mainwp-url-extractor-extension';
	protected $product_id = 'MainWP Url Extractor Extension';
	protected $software_version = '1.0';

	public function __construct() {

		$this->childFile = __FILE__;
		add_filter( 'mainwp-getextensions', array( &$this, 'get_this_extension' ) );
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', false );

		if ( $this->mainwpMainActivated !== false ) {
			$this->activate_this_plugin();
		} else {
			add_action( 'mainwp-activated', array( &$this, 'activate_this_plugin' ) );
		}
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_action( 'admin_notices', array( &$this, 'mainwp_error_notice' ) );
	}

	function admin_init() {
		if ( get_option( 'mainwp_extract_extension_activated' ) == 'yes' ) {
			delete_option( 'mainwp_extract_extension_activated' );
			wp_redirect( admin_url( 'admin.php?page=Extensions' ) );
			return;
		}
	}

	function get_this_extension( $pArray ) {

		$pArray[] = array( 'plugin' => __FILE__, 'api' => $this->plugin_handle,  'mainwp' => true, 'callback' => array( &$this, 'settings' ), 'apiManager' => true );
		return $pArray;
	}

	function settings() {

		do_action( 'mainwp-pageheader-extensions', __FILE__ );
		if ( $this->childEnabled ) {
			MainWP_Extract_Urls::render();
		} else {
			?><div class="mainwp_info-box-yellow"><strong><?php _e( 'The Extension has to be enabled to change the settings.' ); ?></strong></div><?php
		}
		do_action( 'mainwp-pagefooter-extensions', __FILE__ );
	}

	function activate_this_plugin() {
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', $this->mainwpMainActivated );
		$this->childEnabled = apply_filters( 'mainwp-extension-enabled-check', __FILE__ );
		$this->childKey = $this->childEnabled['key'];
		if ( function_exists( 'mainwp_current_user_can' )&& ! mainwp_current_user_can( 'extension', 'mainwp-url-extractor-extension' ) ) {
			return; 			
		}
		new MainWP_Url_Extractor_Extension();
	}

	public function get_child_key() {

		return $this->childKey;
	}

	public function get_child_file() {

		return $this->childFile;
	}

	function mainwp_error_notice() {

		global $current_screen;
		if ( $current_screen->parent_base == 'plugins' && $this->mainwpMainActivated == false ) {
			echo '<div class="error"><p>MainWP Url Extractor Extension ' . __( 'requires <a href="http://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> to be activated in order to work. Please install and activate <a href="http://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> first.' ) . '</p></div>';
		}
	}
	public function update_option( $option_name, $option_value ) {

		$success = add_option( $option_name, $option_value, '', 'no' );

		if ( ! $success ) {
			$success = update_option( $option_name, $option_value );
		}

		 return $success;
	}

	public function activate() {
		$options = array(
		'product_id' => $this->product_id,
							'activated_key' => 'Deactivated',
							'instance_id' => apply_filters( 'mainwp-extensions-apigeneratepassword', 12, false ),
							'software_version' => $this->software_version,
						);
		$this->update_option( $this->plugin_handle . '_APIManAdder', $options );
	}

	public function deactivate() {
		$this->update_option( $this->plugin_handle . '_APIManAdder', '' );
	}
}

global $mainwpUrlExtractorExtensionActivator;
$mainwpUrlExtractorExtensionActivator = new MainWP_Url_Extractor_Extension_Activator();
