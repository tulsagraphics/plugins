jQuery( document ).ready(function($) {
	jQuery( '.mainwp_extractor_datepicker' ).datepicker( {dateFormat:"yy-mm-dd"} );

	$( '.ext_opt_link' ).bind('click', function() {
		var box = $( '#' + $( this ).attr( 'box' ) );
		$( '.ext-urls-option-box' ).hide();
		box.show();
		return false;
	});

	$( '.ext_box_dismiss' ).bind('click', function() {
		$( this ).closest( '.ext-urls-option-box' ).hide();
		return false;
	});

	$( '#mainwp_extract_btn_preview_ouput' ).live('click', function () {
		mainwp_extract_preview_ouput();
		return false;
	});

	$( '#mainwp_extract_btn_export' ).live('click', function () {
		if ($( '#mainwp_extract_enable_export' ).val() == 1) {
			var extract_data = jQuery( '#mainwp_extract_preview_output' ).val();
			var blob = new Blob( [extract_data], {type: "text/plain;charset=utf-8"} );
			var file_name = "extract_urls_data.txt";
			if ($( 'input[name="mainwp_extract_export_type"]:checked' ).val() == 'csv') {
				file_name = "extract_urls_data.csv";
			}
			saveAs( blob, file_name );
		}
		return false;
	});

	$( '#ext_save_template_btn_save' ).live('click', function () {
		var errors = [];
		if (jQuery( '#mainwp_extract_format_output' ).val().trim() == '') {
			errors.push( 'Format output can not be empty.' );
		}
		if (jQuery( '#ext_save_template_title' ).val().trim() == '') {
			errors.push( 'Template title can not be empty.' );
		}
		if (errors.length > 0) {
			jQuery( '#mainwp_ext_url_error' ).html( errors.join( '<br />' ) );
			jQuery( '#mainwp_ext_url_error' ).show();
			return false;
		} else {
			jQuery( '#mainwp_ext_url_error' ).html( "" );
			jQuery( '#mainwp_ext_url_error' ).hide();
		}
	})

	$( '.ext_delete_template' ).live('click', function () {
		if ( ! confirm( "Are you sure?" )) {
			return false; }
		$( '#ext_detele_selected_template' ).val( $( '#ext_urls_template_select' ).val() );
		$( '#ext_form_delete_template' ).submit();
		return false;
	})

	$( '#ext_template_btn_use' ).live('click', function () {
		var data = {
			action: 'mainwp_extract_urls_load_template',
			tempId: $( '#ext_urls_template_select' ).val()
		}
		jQuery( '#mainwp_ext_url_error' ).hide();
		jQuery( '#ext_url_loading' ).show();
		jQuery.post(ajaxurl, data, function (response) {
			jQuery( '#ext_url_loading' ).hide();
			if (response && response['status'] == 'success') {
				if (response['format_output']) {
					$( '#mainwp_extract_format_output' ).val( response['format_output'] ); }
				if (response['separator']) {
					$( '#mainwp_extract_separator' ).val( response['separator'] ); }
			} else {
				jQuery( '#mainwp_ext_url_error' ).html( 'Error loading template.' );
				jQuery( '#mainwp_ext_url_error' ).show();
			}
		},'json')
	});

	$( 'a.ext_url_add_token' ).on( 'click', function( e ) {
		var replace_text = jQuery( this ).html();
		var formatObj = $( '#mainwp_extract_format_output' );
		var str = formatObj.val();
		var pos = ext_getPos( formatObj[0] );
		str = str.substring( 0,pos ) + replace_text + str.substring( pos, str.length )
		formatObj.val( str );
		return false;
	});

});

function ext_getPos(obj) {
		var pos = 0;	// IE Support
	if (document.selection) {
		obj.focus();
		var range = document.selection.createRange();
		range.moveStart( 'character', -obj.value.length );
		pos = range.text.length;
	} // Firefox support
		else if (obj.selectionStart || obj.selectionStart == '0') {
			pos = obj.selectionStart; }
		return (pos);
}

mainwp_extract_preview_ouput = function (postId, userId) {
	var errors = [];
	var selected_sites = [];
	var selected_groups = [];

	if (jQuery( '#select_by' ).val() == 'site') {
		jQuery( "input[name='selected_sites[]']:checked" ).each(function (i) {
			selected_sites.push( jQuery( this ).val() );
		});
		if (selected_sites.length == 0) {
			errors.push( 'Please select websites or groups.' );
			jQuery( '#selected_sites' ).addClass( 'form-invalid' );
		} else {
			jQuery( '#selected_sites' ).removeClass( 'form-invalid' );
		}
	} else {
		jQuery( "input[name='selected_groups[]']:checked" ).each(function (i) {
			selected_groups.push( jQuery( this ).val() );
		});
		if (selected_groups.length == 0) {
			errors.push( 'Please select websites or groups.' );
			jQuery( '#selected_groups' ).addClass( 'form-invalid' );
		} else {
			jQuery( '#selected_groups' ).removeClass( 'form-invalid' );
		}
	}

	var status = "";
	var statuses = ['publish', 'pending', 'private', 'future', 'draft', 'trash'];
	for (var i = 0; i < statuses.length; i++) {
		if (jQuery( '#mainwp_post_search_type_' + statuses[i] ).attr( 'checked' )) {
			if (status != "") { status += ","; }
			status += statuses[i];
		}
	}
	if (status == "") {
		errors.push( 'Please select a post status.' );
	}

	if (jQuery( '#mainwp_extract_format_output' ).val().trim() == '') {
		errors.push( 'Format output can not be empty.' );
	}

	var post_type = 0;
	if (jQuery( '#mainwp_post_search_post' ).attr( 'checked' )) {
		post_type += 1; }
	if (jQuery( '#mainwp_post_search_page' ).attr( 'checked' )) {
		post_type += 2; }
	if (post_type == 0) {
		errors.push( 'Please select post type.' ); }

	if (errors.length > 0) {
		jQuery( '#mainwp_posts_error' ).html( errors.join( '<br />' ) );
		jQuery( '#mainwp_posts_error' ).show();
		return false;
	} else {
		jQuery( '#mainwp_posts_error' ).html( "" );
		jQuery( '#mainwp_posts_error' ).hide();
	}

	var data = {
		action:'mainwp_extract_preview_ouput',
		keyword:jQuery( '#mainwp_post_search_by_keyword' ).val(),
		dtsstart:jQuery( '#mainwp_post_search_by_dtsstart' ).val(),
		dtsstop:jQuery( '#mainwp_post_search_by_dtsstop' ).val(),
		post_type: post_type,
		status:status,
		'groups[]':selected_groups,
		'sites[]':selected_sites,
		postId: (postId == undefined ? '' : postId),
		userId: (userId == undefined ? '' : userId),
		format_output: jQuery( '#mainwp_extract_format_output' ).val(),
		separator: jQuery( '#mainwp_extract_separator' ).val(),
	};
	jQuery( '#mainwp_extract_preview_output' ).val( '' );
	jQuery( '#mainwp_extract_enable_export' ).val( 0 );
	jQuery( '#mainwp_posts_loading' ).show();
	jQuery.post(ajaxurl, data, function (response) {
		jQuery( '#mainwp_posts_loading' ).hide();
		if (response) {
			if (response['error']) {
				jQuery( '#mainwp_extract_preview_output' ).html( response['error'] );
			} else if (response['result'] != undefined) {
				jQuery( '#mainwp_extract_preview_output' ).val( response['result'] );
				jQuery( '#mainwp_extract_enable_export' ).val( 1 );
			} else {
				jQuery( '#mainwp_extract_preview_output' ).html( "Undefined error." );
			}
		} else {
			jQuery( '#mainwp_extract_preview_output' ).html( "Undefined error." );
		}
	}, 'json');
};


jQuery( document ).ready(function($) {
	jQuery( '.mainwp-show-url-tut' ).on('click', function(){
		jQuery( '.mainwp-urlext-tut' ).hide();
		var num = jQuery( this ).attr( 'number' );
		console.log( num );
		jQuery( '.mainwp-urlext-tut[number="' + num + '"]' ).show();
		urlext_setCookie( 'urlext_quick_tut_number', jQuery( this ).attr( 'number' ) );
		return false;
	});

	jQuery( '#mainwp-urlext-quick-start-guide' ).on('click', function () {
		if (urlext_getCookie( 'urlext_quick_guide' ) == 'on') {
			urlext_setCookie( 'urlext_quick_guide', '' ); } else {
			urlext_setCookie( 'urlext_quick_guide', 'on' ); }
			urlext_showhide_quick_guide();
			return false;
	});
	jQuery( '#mainwp-urlext-tips-dismiss' ).on('click', function () {
		urlext_setCookie( 'urlext_quick_guide', '' );
		urlext_showhide_quick_guide();
		return false;
	});

});

urlext_showhide_quick_guide = function(show, tut) {
	var show = urlext_getCookie( 'urlext_quick_guide' );
	var tut = urlext_getCookie( 'urlext_quick_tut_number' );
	if (typeof tut == "undefined" || ! tut) {
		tut = 1; }

	if (typeof show == "undefined" || ! tut) {
		show = 'on'; }

	if (show == 'on') {
		jQuery( '#mainwp-urlext-tips' ).show();
		jQuery( '#mainwp-urlext-quick-start-guide' ).hide();
		urlext_showhide_quick_tut();
	} else {
		jQuery( '#mainwp-urlext-tips' ).hide();
		jQuery( '#mainwp-urlext-quick-start-guide' ).show();
	}
}

urlext_showhide_quick_tut = function() {
	var tut = urlext_getCookie( 'urlext_quick_tut_number' );
	jQuery( '.mainwp-urlext-tut' ).hide();
	jQuery( '.mainwp-urlext-tut[number="' + tut + '"]' ).show();
}


function urlext_setCookie(c_name, value, expiredays)
{
	var exdate = new Date();
	exdate.setDate( exdate.getDate() + expiredays );
	document.cookie = c_name + "=" + escape( value ) + ((expiredays == null) ? "" : ";expires=" + exdate.toUTCString());
}
function urlext_getCookie(c_name)
{
	if (document.cookie.length > 0) {
		var c_start = document.cookie.indexOf( c_name + "=" );
		if (c_start != -1) {
			c_start = c_start + c_name.length + 1;
			var c_end = document.cookie.indexOf( ";", c_start );
			if (c_end == -1) {
				c_end = document.cookie.length; }
			return unescape( document.cookie.substring( c_start, c_end ) );
		}
	}
	return "";
}
