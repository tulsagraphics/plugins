<?php

class MainWP_Extract_Urls
{
	public static $availableTokens = array(
	'[post.title]',
	'[post.url]',
	'[post.date]',
	'[post.status]',
	'[post.author]',
	'[post.website.url]',
	'[post.website.name]',
	);
	public function __construct() {

	}

	public function init() {

	}

	public function admin_init() {
		add_action( 'wp_ajax_mainwp_extract_preview_ouput', array( &$this, 'extract_preview_ouput' ) ); //ok
		add_action( 'wp_ajax_mainwp_extract_urls_load_template', array( &$this, 'load_template' ) ); //ok
	}

	public function init_menu() {

	}

	public function extract_preview_ouput() {
		$return = array();
		try {
			$return['result'] = MainWP_Extract_Urls::render_preview_ouput( $_POST['keyword'], $_POST['dtsstart'], $_POST['dtsstop'], $_POST['status'], (isset( $_POST['groups'] ) ? $_POST['groups'] : ''), (isset( $_POST['sites'] ) ? $_POST['sites'] : ''), $_POST['postId'], $_POST['userId'], $_POST['post_type'] );
		} catch (Exception $e) {
			$return['error'] = $e->getMessage();
		}
		die( json_encode( $return ) );
	}

	public function load_template() {
		$id = $_POST['tempId'];
		$return = array();
		if ( $id ) {
			$template = MainWP_Extract_Urls_DB::get_instance()->get_template_by( 'id', $id );
			if ( is_object( $template ) ) {
				$return = array(
				'format_output' => stripslashes( $template->format_output ),
								'separator' => stripslashes( $template->separator ),
								'status' => 'success',
				);
				die( json_encode( $return ) );
			}
		}
		die( json_encode( $return ) );
	}

	public static function save_template() {
		if ( isset( $_POST['ext_save_template_btn_save'] ) ) {
			$template = array(
			'title' => trim( $_POST['ext_save_template_title'] ),
							   'separator' => $_POST['mainwp_extract_separator'],
							   'format_output' => $_POST['mainwp_extract_format_output'],
						   );

			if ( MainWP_Extract_Urls_DB::get_instance()->update_template( $template ) ) {
				return true; }
			return false;
		}
	}

	public static function delete_template() {
		if ( isset( $_POST['ext_detele_selected_template'] ) ) {
			return MainWP_Extract_Urls_DB::get_instance()->delete_template( intval( $_POST['ext_detele_selected_template'] ) );
		}
	}

	public static function render_preview_ouput( $keyword, $dtsstart, $dtsstop, $status, $groups, $sites, $postId, $userId, $post_type ) {
		global $mainwpUrlExtractorExtensionActivator;

		$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainwpUrlExtractorExtensionActivator->get_child_file(), $mainwpUrlExtractorExtensionActivator->get_child_key(), $sites, $groups );

		$output = new stdClass();
		$output->errors = array();
		$output->posts = 0;
		$output->result = '';

		if ( count( $dbwebsites ) > 0 ) {
			$post_data = array(
				'keyword' => $keyword,
				'dtsstart' => $dtsstart,
				'dtsstop' => $dtsstop,
				'status' => $status,
				'where_post_date' => 1,
				'extract_post_type' => $post_type,
				'maxRecords' => ((get_option( 'mainwp_maximumPosts' ) === false) ? 50 : get_option( 'mainwp_maximumPosts' )),
			);
			if ( isset( $postId ) && ('' != $postId) ) {
				$post_data['postId'] = $postId;
			} else if ( isset( $userId ) && ('' != $userId) ) {
				$post_data['userId'] = $userId;
			}

			$format_output = isset( $_POST['format_output'] ) ? trim( $_POST['format_output'] ) : '';

			$matches = array();
			if ( preg_match_all( '/(\[[^\]]+\])/is' , $format_output, $matches ) ) {
				$matches = $matches[1];
			}

			$tokens = array();
			foreach ( $matches as $token ) {
				if ( in_array( trim( $token ), self::$availableTokens ) ) {
					$tokens[] = $token;
				}
			}

			if ( ! is_array( $tokens ) ||  count( $tokens ) == 0 ) {
				throw new Exception( 'Error: Format output empty.' ); }

			$post_data['extract_tokens'] = base64_encode( serialize( $tokens ) );
			do_action( 'mainwp_fetchurlsauthed', $mainwpUrlExtractorExtensionActivator->get_child_file(), $mainwpUrlExtractorExtensionActivator->get_child_key(), $dbwebsites, 'get_all_posts', $post_data, array( 'MainWP_Extract_Urls', 'posts_extract_handler' ), $output );
		}

		//Sort if required
		if ( $output->posts == 0 ) {
			throw new Exception( 'No posts found.' ); }
		return $output->result;
	}

	public static function posts_extract_handler( $data, $website, &$output ) {

		$separator = isset( $_POST['separator'] ) ? $_POST['separator'] : '';
		$format_output = isset( $_POST['format_output'] ) ? $_POST['format_output'] : '';

		$matches = array();
		if ( preg_match_all( '/(\[[^\]]+\])/is' , $format_output, $matches ) ) {
			$matches = $matches[1];
		}

		$tokens = array();
		foreach ( $matches as $token ) {
			if ( in_array( trim( $token ), self::$availableTokens ) ) {
				$tokens[] = $token;
			}
		}

		if ( preg_match( '/<mainwp>(.*)<\/mainwp>/', $data, $results ) > 0 ) {
			$posts = unserialize( base64_decode( $results[1] ) );
			unset( $results );
			foreach ( $posts as $post ) {
				if ( isset( $post['dts'] ) ) {
					if ( ! stristr( $post['dts'], '-' ) ) {
						$post['dts'] = MainWP_Extract_Urls_Utility::format_timestamp( MainWP_Extract_Urls_Utility::get_timestamp( $post['dts'] ) );
					}
				}

				if ( ! isset( $post['title'] ) || ('' == $post['title']) ) {
					$post['title'] = '(No Title)';
				}
				$str = $format_output;

				foreach ( $tokens as $token ) {
					if ( in_array( $token, array( '[post.url]', '[post.website.url]', '[post.website.name]' ) ) ) {
						if ( isset( $post[ $token ] ) ) {
							//if (strpos($post[$token], ',') !== false)
							//    $post[$token] = '"' . $post[$token] . '"';
							$str = str_replace( $token, $post[ $token ], $str );
						}
					} else {
						switch ( $token ) {
							case '[post.title]':
								if ( isset( $post['title'] ) ) {
									// if (strpos($post['title'], ',') !== false)
									//     $post['title'] = '"' . $post['title'] . '"';
									$str = str_replace( $token, $post['title'], $str );
								}
								break;
							case '[post.date]':
								if ( isset( $post['dts'] ) ) {
									// if (strpos($post['dts'], ',') !== false)
									//     $post['dts'] = '"' . $post['dts'] . '"';
									$str = str_replace( $token, $post['dts'], $str );
								}
								break;
							case '[post.status]':
								if ( isset( $post['status'] ) ) {
									$str = str_replace( $token, $post['status'], $str );
								}
								break;
							case '[post.author]':
								if ( isset( $post['author'] ) ) {
									//if (strpos($post['author'], ',') !== false)
									//    $post['author'] = '"' . $post['author'] . '"';
									$str = str_replace( $token, $post['author'] , $str );
								}
								break;
						}
					}
				}
				//$str = trim($str);
				$str = stripslashes( $str );
				if ( empty( $separator ) ) {
					$output->result .= $str . "\n"; } else {
					$output->result .= $str . $separator; }

					$output->posts++;
			}
			$output->result = rtrim( $output->result, $separator );
			unset( $posts );
		} else {
			$output->errors[ $website->id ] = MainWPErrorHelper::getErrorMessage( new MainWPException( 'NOMAINWP', $website->url ) );
		}
	}


	public static function render() {
		$str_info = array();
		$info_class = ' hidden-field ';

		if ( isset( $_POST['ext_save_template_btn_save'] ) ) {
			 $info_class = '';
			$save_template = MainWP_Extract_Urls::save_template();
			if ( $save_template ) {
				$str_info[] = __( 'Template saved.' );
			} else {
				$str_info[] = __( 'Saving template failed.' );
			}
		}

		if ( isset( $_POST['ext_detele_selected_template'] ) ) {
			$info_class = '';
			$deleted = MainWP_Extract_Urls::delete_template();
			if ( $deleted ) {
				$str_info[] = __( 'Template deleted succesfull.' );
			} else {
				$str_info[] = __( 'Deleting template failed.' );
			}
		}

	?>
    <?php self::render_qsg(); ?>
        <div class="mainwp-search-form">
             <div id="mainwp_posts_error" class="mainwp_error mainwp_info-box-red"></div>             
             <div class="mainwp_info-box-yellow <?php echo $info_class; ?>"><?php echo count( $str_info ) > 0 ? implode( '<br />', $str_info ) : ''; ?></div>
             <br/>
            <?php do_action( 'mainwp_select_sites_box', __( 'Select Sites', 'mainwp' ), 'checkbox', true, true, 'mainwp_select_sites_box_right', '', array(), array() ); ?>
            <div class="postbox" style="float: left; min-height: 335px; width: -moz-calc(100% - 300px);width: -webkit-calc(100% - 300px);width: -o-calc(100% - 300px);width: calc(100% - 300px);">
            <h3 class="mainwp_box_title"><span><?php _e( 'Extract URLs','mainwp' ); ?></span></h3>
            <div class="inside">
            <div class="ext-urls-option">
                <label><?php _e( 'Post Type:', 'mainwp' ); ?></label>
                <ul class="mainwp_checkboxes">
                    <li>
                        <input type="checkbox" id="mainwp_post_search_post"  checked="checked" value="1" class="mainwp-checkbox2"/>
                        <label for="mainwp_post_search_post" class="mainwp-label2">Post</label>
                    </li>
                    <li>
                        <input type="checkbox" id="mainwp_post_search_page" checked="checked" value="2" class="mainwp-checkbox2" />
                        <label for="mainwp_post_search_page" class="mainwp-label2">Page</label>
                    </li>
                </ul>  
            </div>            
            <div style="clear: both;"></div>
            <div class="ext-urls-option">
                <label><?php _e( 'Status:', 'mainwp' ); ?></label>
                <ul class="mainwp_checkboxes">
                    <li>
                        <input type="checkbox" id="mainwp_post_search_type_publish" checked="checked" class="mainwp-checkbox2"/>
                        <label for="mainwp_post_search_type_publish" class="mainwp-label2">Published</label>
                    </li>
                    <li>
                        <input type="checkbox" id="mainwp_post_search_type_pending" class="mainwp-checkbox2" />
                        <label for="mainwp_post_search_type_pending" class="mainwp-label2">Pending</label>
                    </li>
                    <li>
                        <input type="checkbox" id="mainwp_post_search_type_private"  class="mainwp-checkbox2" />
                        <label for="mainwp_post_search_type_private" class="mainwp-label2">Private</label>
                    </li>
                    <li>
                        <input type="checkbox" id="mainwp_post_search_type_future"  class="mainwp-checkbox2" />
                        <label for="mainwp_post_search_type_future" class="mainwp-label2">Future</label>
                    </li>
                    <li>
                        <input type="checkbox" id="mainwp_post_search_type_draft"  class="mainwp-checkbox2" />
                        <label for="mainwp_post_search_type_draft" class="mainwp-label2">Draft</label>
                    </li>
                    <li>
                        <input type="checkbox" id="mainwp_post_search_type_trash"  class="mainwp-checkbox2" />
                        <label for="mainwp_post_search_type_trash" class="mainwp-label2">Trash</label>
                    </li>
                </ul>
            </div>            

            <div class="ext-urls-option" style="clear: both; float: none; margin-top: 2em;">
                <?php _e( 'Containing Keyword:','mainwp' ); ?><br />
                <input type="text" id="mainwp_post_search_by_keyword" size="50" value="" style="clear: both;"/>
            </div>            
            <div class="ext-urls-option" style="clear: both; float: none;">
                <?php _e( 'Date Range:','mainwp' ); ?><br />
                <input type="text" id="mainwp_post_search_by_dtsstart" class="mainwp_extractor_datepicker" size="12" value=""/> <?php _e( 'to','mainwp' ); ?> <input type="text" id="mainwp_post_search_by_dtsstop" class="mainwp_extractor_datepicker" size="12" value=""/>
            </div>                                    
        </div>
            </div>
        </div>        
        <div style="clear: both;"></div>
        <br/> 
        <div class="postbox">
        <div class="inside">
        <div id="mainwp_ext_url_error" class="mainwp_error mainwp_info-box-red"></div>
        <h3><?php _e( 'Format Output','mainwp' ); ?></h3>
        
        <form method="post" action="">
            <input type="text" name="mainwp_extract_format_output" id="mainwp_extract_format_output" value="" />        
            <p class="ext-urls-show-opt-links">
                <a href="#" class="ext_opt_link" box="ext_box1" title="<?php _e( 'Show Available Tokens', 'mainwp' ); ?>"><?php _e( 'Show Available Tokens', 'mainwp' ); ?></a>
                <a href="#" class="ext_opt_link" box="ext_box2" title="<?php _e( 'Templates', 'mainwp' ); ?>"><?php _e( 'Templates', 'mainwp' ); ?></a>
                <a href="#" class="ext_opt_link" box="ext_box3" title="<?php _e( 'Save Output Format', 'mainwp' ); ?>"><?php _e( 'Save Output Format', 'mainwp' ); ?></a>
            </p>    
            <div id="ext_box1" class="ext-urls-option-box hidden-field">                   
                <span><a href="#" class="ext_box_dismiss"><?php _e( 'Dismiss' ); ?></a></span>
                <p class="ext-token"><strong><a class="ext_url_add_token" href="#" style="color: green">[post.title]</a></strong> - <em>Extracts Post/Page Title</em></p>
                <p class="ext-token"><strong><a class="ext_url_add_token" href="#" style="color: green">[post.url]</a></strong> - <em>Extracts Post/Page URL</em></p>
                <p class="ext-token"><strong><a class="ext_url_add_token" href="#" style="color: green">[post.date]</a></strong> - <em>Extracts Post/Page Publishing Date</em></p>
                <p class="ext-token"><strong><a class="ext_url_add_token" href="#" style="color: green">[post.status]</a></strong> - <em>Extracts Post/Page Status</em></p>
                <p class="ext-token"><strong><a class="ext_url_add_token" href="#" style="color: green">[post.author]</a></strong> - <em>Extracts Post/Page Author</em></p>
                <p class="ext-token"><strong><a class="ext_url_add_token" href="#" style="color: green">[post.website.url]</a></strong> - <em>Extracts Post/Page Website URL</em></p>
                <p class="ext-token"><strong><a class="ext_url_add_token" href="#" style="color: green">[post.website.name]</a></strong> - <em>Extracts Post/Page Website Name</em></p>            
            </div>
            <h3><?php _e( 'Separator','mainwp' ); ?></h3>
            <input type="text" name="mainwp_extract_separator" id="mainwp_extract_separator" value=""/>             
            <div id="ext_box2" class="ext-urls-option-box hidden-field">     
                <span><a href="#" class="ext_box_dismiss"><?php _e( 'Dismiss' ); ?></a></span>
                <span id="ext_url_loading" class="hidden-field"><img src="<?php echo plugins_url( 'images/loader.gif', dirname( __FILE__ ) ); ?>"/></span>
                <br/>                
                <?php
				$templates = MainWP_Extract_Urls_DB::get_instance()->get_template_by( 'all' );
				if ( is_array( $templates ) && count( $templates ) > 0 ) {
				?>
                    <label><?php _e( 'Select Template:' ); ?></label>
                    &nbsp;<?php echo self::gen_template_select( $templates ); ?>
                    &nbsp;<input type="button" class="button-primary" name="ext_template_btn_use" id="ext_template_btn_use" value="<?php _e( 'Use Template', 'mainwp' ); ?>" />
                    &nbsp;<a href="#" class="ext_delete_template"><?php _e( 'Delete' );?></a>
                <?php
				} else {
					echo 'No Saved Templates';
				}
				?>
                <br/>
                <br/>
            </div>         
            <div id="ext_box3" class="ext-urls-option-box hidden-field">     
                <span><a href="#" class="ext_box_dismiss"><?php _e( 'Dismiss' ); ?></a></span>
                <br/>
                <label><?php _e( 'Format Template Title:' ); ?></label>
                &nbsp;<input type="text" name="ext_save_template_title" id="ext_save_template_title" />
                &nbsp;<input type="submit" class="button-primary" name="ext_save_template_btn_save" id="ext_save_template_btn_save" value="<?php _e( 'Save Template', 'mainwp' ); ?>" />
                <br/>
                <br/>
            </div>   
        </form>   
        
        <form method="post" action="" id="ext_form_delete_template">
            <input type="hidden" name="ext_detele_selected_template" id="ext_detele_selected_template" value="0"/>            
        </form>
        <br/>
        <br/>
        <p>
            <input type="button" name="mainwp_extract_btn_preview_ouput" id="mainwp_extract_btn_preview_ouput" class="button-primary" value="<?php _e( 'Preview Ouput','mainwp' ); ?>"/>
            <span id="mainwp_posts_loading">&nbsp;<em><?php _e( 'Grabbing information from Child Sites','mainwp' ) ?></em>&nbsp;&nbsp;<img src="<?php echo plugins_url( 'images/loader.gif', dirname( __FILE__ ) ); ?>"/></span>
        </p>        
        </div>
        </div> 
        <br/>
        <br/>
        <textarea name="mainwp_extract_preview_output" wrap="off" id="mainwp_extract_preview_output"></textarea>
        <br/>
        <input type="hidden" name="mainwp_extract_enable_export" id="mainwp_extract_enable_export" value="0"/>
        <p>
            <input type="button" name="mainwp_extract_btn_export" id="mainwp_extract_btn_export" class="button-primary right" value="<?php _e( 'Export','mainwp' ); ?>"/>            
            <span class="mainwp_extract_export_type">
                <input type="radio" value="txt" id="type_txt" name="mainwp_extract_export_type" />
                <label for="type_txt"><?php echo 'TXT'; ?></label>
            </span>    
            <span class="mainwp_extract_export_type">
                <input type="radio" value="csv" id="type_csv" name="mainwp_extract_export_type"  checked />
                <label for="type_csv"><?php echo 'CSV'; ?></label>
            </span>
        </p>
         <div class="clear"></div>    
        <?php
	}

	public static function gen_template_select( $templates ) {
		$str = '';
		if ( is_array( $templates ) && count( $templates ) > 0 ) {
			$str = '<select name="ext_urls_template_select" id="ext_urls_template_select">';
			foreach ( $templates as $template ) {
				if ( ! empty( $template ) ) {
					$str .= '<option value="' . $template->id . '">' . $template->title . '</option>'; }
			}
			$str .= '</select>';
		}
		return $str;
	}

	public static function render_qsg() {
		$plugin_data = get_plugin_data( MAINWP_URL_EXTRACTOR_PLUGIN_FILE, false );
		$description = $plugin_data['Description'];
		$extraHeaders = array( 'DocumentationURI' => 'Documentation URI' );
		$file_data = get_file_data( MAINWP_URL_EXTRACTOR_PLUGIN_FILE, $extraHeaders );
		$documentation_url  = $file_data['DocumentationURI'];
		?>
        <div  class="mainwp_ext_info_box" id="urlext-notice-box">
		   <div class="mainwp-ext-description"><?php echo $description; ?></div><br/>
		   <b><?php echo __( 'Need Help?' ); ?></b> <?php echo __( 'Review the Extension' ); ?> <a href="<?php echo $documentation_url; ?>" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Documentation' ); ?></a>. 
				   <a href="#" id="mainwp-urlext-quick-start-guide"><i class="fa fa-info-circle"></i> <?php _e( 'Show Quick Start Guide','mainwp' ); ?></a></div>
				   <div  class="mainwp_ext_info_box" id="mainwp-urlext-tips">
					 <span><a href="#" class="mainwp-show-url-tut" number="1"><i class="fa fa-book"></i> <?php _e( 'Extract Posts and Pages URLs','mainwp' ) ?></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="mainwp-show-url-tut"  number="2"><i class="fa fa-book"></i> <?php _e( 'Saving URL Extractor Templates','mainwp' ) ?></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="mainwp-show-url-tut"  number="3"><i class="fa fa-book"></i> <?php _e( 'Deleting URL Extractor Templates','mainwp' ) ?></a></span><span><a href="#" id="mainwp-urlext-tips-dismiss" style="float: right;"><i class="fa fa-times-circle"></i> <?php _e( 'Dismiss','mainwp' ); ?></a></span>
					 <div class="clear"></div>
					 <div id="mainwp-urlext-tuts">
					   <div class="mainwp-urlext-tut" number="1">
						   <h3>Extract Posts and Pages URLs</h3>
						   <p>
							   <ol>
								   <li>Select Post Type (you can extract Posts, Pages or Post and Pages both);</li>
								   <li>Select Post/Page Status;</li>
								   <li>Optionally, refine your search with Keywords and/or Date Range;</li>
								   <li>Select your Child Sites;</li><br/>
								   <img src="http://docs.mainwp.com/wp-content/uploads/2014/05/url-1-4.png" alt="screenshot"/>
								   <li>
									   Format the Output; In this field you can use provided tokens:
									   <ul>
										   <li><strong>[post.title]</strong> Ã¢â‚¬â€œ <em>Extracts Post/Page Title</em></li>
										   <li><strong>[post.url]</strong> Ã¢â‚¬â€œ <em>Extracts Post/Page URL</em></li>
										   <li><strong>[post.status]</strong> Ã¢â‚¬â€œ <em>Extracts Post/Page Status</em></li>
										   <li><strong>[post.author]</strong> Ã¢â‚¬â€œ <em>Extracts Post/Page Author</em></li>
										   <li><strong>[post.date]</strong> Ã¢â‚¬â€œ <em>Extracts Post/Page Date</em></li>
										   <li><strong>[post.website.url]</strong> Ã¢â‚¬â€œ <em>Extracts Post/Page Website URL</em></li>
										   <li><strong>[post.website.name]</strong> Ã¢â‚¬â€œ <em>Extracts Post/Page Website Name</em></li>
									   </ul>
									   Tokens can be added by typing in or by clicking on the token in the Available Tokens Box. To open the box to see and use available tokens, click the Show Available Tokens link.
								   </li>
								   <li>Optionally you can specify a Separator. In case you leave the field blank, the extension will use Line Break as the separator. This means that each of the search results will displayed in separate line. In case you use some special character or a set of characters it will be used to separate each query result and the results will be displayed in the same line.</li>
								   <li>Click the Preview Output button;</li>
								   <li>Select the file type you want to export (CSV or TXT) and click the Export button.</li><br/>
								   <img src="http://docs.mainwp.com/wp-content/uploads/2014/05/url-5-8.png" style="width: 100% !important;" alt="screenshot"/>
							   </ol>
						   </p>
					   </div>
					   <div class="mainwp-urlext-tut"  number="2">
						   <h3>Saving URL Extractor Templates</h3>
						   <p>
							   <ol>
								   <li>Click the Save Output Format link, the box with saving options will appear;</li>
								   <li>
									   Enter a Template Title; <br/><br/>
									   <img src="http://docs.mainwp.com/wp-content/uploads/2014/05/url-saving-templates-1024x257.png" style="width: 100% !important;" alt="screenshot"/>
								   </li>
								   <li>Click the Save Template button.</li>
							   </ol>
							   Saved Templates will contain only the Format Output and Separator data! Search options will not be saved.
						   </p>
					   </div>
					   <div class="mainwp-urlext-tut"  number="3">
						   <h3>Delete URL Extractor Templates</h3>
						   <p>
							   <ol>
								   <li>
									   Click the Templates link;
								   </li>
								   <li>
									   Select the template you want to delete;<br /><br/>
									   <img src="http://docs.mainwp.com/wp-content/uploads/2014/05/url-select-template-1024x116.png" style="width: 100% !important;" alt="screenshot"/>
								   </li>
								   <li>
									   Click the Delete link.
								   </li>
							   </ol>
						   </p>  
					   </div>
					 </div>
				   </div>
        <?php
	}
}
