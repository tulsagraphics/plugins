=== Live Population for Gravity Forms ===
Contributors: travislopes
Tags: population, merge tags, replacements, gravity forms
Requires at least: 4.2
Tested up to: 4.8
License: GPL-3.0+
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Use merge tags to populate field values, labels and more without reloading the page.

== Description ==

Live Population makes it possible to use merge tags within form field labels, default values, placeholders, descriptions and HTML content.

When building out a form, a new Live Population tab is added under field settings. Inside this tab, you can select what field causes Live Population to occur and which field properties should be updated. Merge tags will be automatically replaced when the form is initially loaded and when the user makes changes when filling out a form.

Live Population requires [Gravity Forms](https://forgravity.com/gravityforms).

= Requirements =

1. [Purchase and install Gravity Forms](https://forgravity.com/gravityforms)
2. WordPress 4.2+
3. Gravity Forms 1.9.14+

= Support =

If you have any problems, please contact support: https://forgravity.com/support/

== Installation ==

1.  Download the zipped file.
1.  Extract and upload the contents of the folder to your /wp-contents/plugins/ folder.
1.  Navigate to the WordPress admin Plugins page and activate the "Live Population for Gravity Forms" plugin.

== ChangeLog ==

= 1.4.2 =
- Add "fg_livepopulation_replace_variables_{field_property}" filter to toggle HTML escaping, line break conversion and URL encoding when processing merge tags.

= 1.4.1 =
- Add support when editing entry with GravityView.
- Fix error when target field cannot be found.

= 1.4 =
- Add "fg_livepopulation_pre_population" and "fg_livepopulation_post_population" Javascript actions.
- Add "fg_livepopulation_suppress_empty_choices" filter to disable field choice removal where merge tags are used and the choice text or value is empty.
- Add support for default values when initially populating form.
- Add support for label population on Gravity Flow workflow page.
- Add support for List field descriptions and labels.
- Add support for Members version 2.0+.
- Add support for populating merge tags in field choices from non-List fields.
- Add support for Post fields.
- Fix a fatal Javascript error when using an illegal merge tag.
- Fix duplicate field appearing in Live Population target field setting.
- Fix field choices replacement type setting being displayed when field choices replacement is disabled.
- Fix field choices not appearing as available Live Population replacement for fields with choices.
- Fix field settings tabs being too wide when Gravity Perks are installed.
- Fix incorrect slug in automatic updater.
- Fix List field values incorrectly populating as comma separated list when replacing choices.
- Fix Live Population not triggering on Select fields using enhanced user interface.
- Fix merge tags not being replaced when using Gravity Flow Form Connector.
- Fix PHP notice when Gravitate Encryption plugin is not installed.
- Fix special characters converting to HTML entities.
- Update field choices to be removed if merge tags are used and the choice text or value is empty.
- Update field replacement target choices to display admin label if set. If admin label and label are empty, field ID will be displayed.
- Update license API requests to support upcoming ForGravity bundle.

= 1.3 =
- Add support for Gravitate Encryption plugin.
- Add support for review pages.
- Add ability to select multiple target fields to trigger Live Population.

= 1.2 =
- Add "fg_livepopulation_delay" Javascript filter to change delay time when using "keyup" Javascript event.
- Add "fg_livepopulation_event_type" Javascript filter to change Javascript event for non-checkbox/radio input fields.
- Add plugin capabilities.
- Add support for AJAX enabled forms.
- Add support for {all_fields} merge tag in HTML fields.
- Fix a fatal error enqueuing front-end script when using versions of PHP prior to 5.5.
- Fix fields being included for Live Population when no target field was selected.
- Fix Live Population not running due to asynchronous AJAX requests being disabled.
- Fix Live Population not taking place when selecting checkbox or radio choices.
- Update population on form render to replace merge tags even when a target field is not selected.
- Update Live Population field settings tab to more clearly explain functionality.

= 1.1 =
- Add population of checkbox, multi select, radio and select field choices from list fields.
- Add support for populating section field labels.
- Add support for shortcodes in field content and descriptions.
- Fix description not populating if description was blank prior to population.
- Fix missing Live Population field settings for file upload fields.
- Improve support for placeholder population.

= 1.0 =
- It's all new!