=== YITH WooCommerce Popup Premium ===

Contributors: yithemes
Tags: popoup
Requires at least: 4.0.0
Tested up to: 4.9.6
Stable tag: 1.2.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Installation ==
Important: First of all, you have to download and activate WooCommerce plugin, which is mandatory for YITH WooCommerce Popup to be working.

1. Unzip the downloaded zip file.
2. Upload the plugin folder into the `wp-content/plugins/` directory of your WordPress site.
3. Activate `YITH WooCommerce Popup` from Plugins page.

= Configuration =
YITH WooCommerce Popup will add a new tab called "WC Popup" in "YIT Plugins" menu item. There, you will find all Yithemes plugins with quick access to plugin setting page.

== Changelog ==
*YITH WooCommerce Popup Premium*
= 1.2.4 - Released on May 25, 2018 =
New: Privacy Policy Guide
Update: Plugin Core 3.0.16
Update: Language files

= 1.2.3 - Released on May 18, 2018 =
New: Support to WordPress 4.9.6
New: Support to WooCommerce 3.4.0 RC1
New: Privacy options in Newsletter tab
Dev: Added filter ywpop_get_background_image
Dev: Added nl2br function for the header and footer content
Update: Plugin Core 3.0.15
Update: Language files

= 1.2.2 - Released on Jan 30, 2018 =
New: Support to WooCommerce 3.3.0 RC2
New: Dutch Translation
New: Spanish Translation
Fix: Icon type
Fix: Issue with Ultimate Addon
Update: Plugin Core 3.0.10

= 1.2.1 - Released on Dec 12, 2017 =
Remove: Removed Alert message when users try to leave the page
Update: Plugin Core 3.0.1
Fix: Icon option

= 1.2.0 - Released on Dec 11, 2017 =
* Update: Plugin Core 3.0

= 1.1.2 - Released on Oct 12, 2017 =
New: Support to WooCommerce 3.2
New: Mailchimp Double Opt-in option in Newsletter Tab
New: Italian Translation
New: Dutch Translation
Dev: New "ypop_alter_popup" filter
Update: Plugin Core
Fix: Hide popup for session
Fix: issue with 'show only one time for session' option

= 1.1.1 - Released on May 09, 2017 =
New: Support to WooCommerce 3.0.5
Update: Plugin Core 2.9.65
Fix: popup showed after click on internal link

= 1.1.0 - Released on Mar 31, 2017 =
New: Support to WooCommerce 3.0 RC 2
Update: Plugin Core 2.9.62
Fix: popup showed after click on close button

= 1.0.7 - Released on Dec 07, 2016 =
* Added: Support to Wordpress 4.7
* Fixed: Cookie javascript errors

= 1.0.6 - Released on Jun 13, 2016 =
* Added: Support to WooCommerce 2.6 RC1
* Updated: Plugin Core Framework

= 1.0.5 - Released on Mar 28, 2016 =
* Added: Option to display the popup if click a link with #yithpopup

= 1.0.4 - Released on Mar 18, 2016 =
* Fixed: Mailchimp Newsletter Button
* Fixed: Minor Bugs
* Updated: Plugin Core Framework
* Updated: Changed Text Domain from 'ypop' to 'yith-woocommerce-popup'

= 1.0.3 - Released on Jan 26, 2016  =
* Tweak: Popup Add-to-cart via ajax
* Fixed: Registration newsletter issue
* Fixed: Black overlay when popup are disabled
* Fixed: Other minor bugs

= 1.0.2 - Released on Jan 25, 2016 =
* Added : Support to WooCommerce 2.5
* Fixed: Minor Bugs

= 1.0.1 - Released on Aug 13, 2015 =
Added: Support to WooCommerce 2.4.2
Updated: Plugin Core Framework

= 1.0.0 =
Initial release

== Suggestions ==
If you have any suggestions concerning how to improve YITH WooCommerce Popup, you can [write to us](mailto:plugins@yithemes.com "Your Inspiration Themes"), so that we can improve YITH WooCommerce Popup.

== Upgrade notice ==

= 1.0.3 =

* Tweak: Popup Add-to-cart via ajax
* Fixed: Registration newsletter issue
* Fixed: Black overlay when popup are disabled
* Fixed: Other minor bugs