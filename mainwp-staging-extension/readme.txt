=== MainWP Staging Extension ===
Plugin Name: MainWP Staging Extension
Plugin URI: https://mainwp.com
Description: MainWP Staging Extension allows you to create and manage Staging sites directly from your MainWP Dashboard.
Version: 1.1
Author: MainWP
Author URI: https://mainwp.com

== Installation ==
1. Please install plugin "MainWP Dashboard" and active it before install Staging Extension plugin (get the MainWP Dashboard plugin from url:https://mainwp.com/)
2. Upload the `mainwp-staging-extension` folder to the `/wp-content/plugins/` directory
3. Activate the MainWP Staging Extension plugin through the 'Plugins' menu in WordPress

== Screenshots ==
1. Enable or Disable extension on the "Extensions" page in the dashboard

== Changelog ==

= 1.1 - 8-10-2018 =
* Fixed: MainWP Time Capsule Extension compatibility issue
* Fixed: an issue with a deprecated hook

= 1.0 - 4-19-2018 =
* Beta 2 - Fixed WP Staging 2.2.5 plugin compatibility 

= 1.0 - 12-21-2017 =
* Beta 1 release
