<?php

class MainWP_Staging {
	protected static $_instance = null;
    protected $settings = null;

    public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	public function admin_init() {
        add_action( 'wp_ajax_mainwp_staging_site_override_settings', array( $this, 'ajax_override_settings' ) );
        add_action( 'wp_ajax_mainwp_staging_save_settings', array( $this, 'ajax_save_settings' ) );
        add_action( 'wp_ajax_mainwp_staging_overview', array( $this, 'ajax_get_overview' ) );
        add_action( 'wp_ajax_mainwp_staging_scanning', array( $this, 'ajax_get_scanning' ) );
        add_action( 'wp_ajax_mainwp_staging_check_disk_space', array( $this, 'ajax_check_disk_space' ) );
        add_action( 'wp_ajax_mainwp_staging_check_clone', array( $this, 'ajax_check_clone' ) );

        add_action( 'wp_ajax_mainwp_staging_cloning', array( $this, 'ajax_cloning' ) );
        add_action( 'wp_ajax_mainwp_staging_clone_database', array( $this, 'ajax_clone_database' ) );
        add_action( 'wp_ajax_mainwp_staging_clone_prepare_directories', array( $this, 'ajax_clone_prepare_directories' ) );
        add_action( 'wp_ajax_mainwp_staging_clone_files', array( $this, 'ajax_clone_files' ) );
        add_action( 'wp_ajax_mainwp_staging_clone_replace_data', array( $this, 'ajax_clone_replace_data' ) );
        add_action( 'wp_ajax_mainwp_staging_clone_finish', array( $this, 'ajax_clone_finish' ) );
        add_action( 'wp_ajax_mainwp_staging_cancel_clone', array( $this, 'ajax_cancel_clone' ) );
		add_action( 'wp_ajax_mainwp_staging_update', array( $this, 'ajax_staging_update' ) );
		add_action( 'wp_ajax_mainwp_staging_cancel_update', array( $this, 'ajax_cancel_update' ) );
        add_action( 'wp_ajax_mainwp_staging_confirm_delete_clone', array( $this, 'ajax_confirm_delete_clone' ) );
        add_action( 'wp_ajax_mainwp_staging_delete_clone', array( $this, 'ajax_delete_clone' ) );
        add_action( 'wp_ajax_mainwp_staging_add_clone_website', array( $this, 'ajax_staging_addclonewebsite' ) );
        add_action( 'wp_ajax_mainwp_staging_delete_clone_website', array( $this, 'ajax_staging_deletewebsite' ) );
	}

    public static function verify_nonce() {
        if ( ! isset( $_REQUEST['_stagingNonce'] ) || ! wp_verify_nonce( $_REQUEST['_stagingNonce'], '_wpnonce_staging' ) ) {
	      die(json_encode( array( 'error' => __( 'Invalid request', 'mainwp-staging-extension' ) ) ) );
        }

        $site_id = isset($_POST['stagingSiteID']) ? intval($_POST['stagingSiteID']) : 0;
        if (empty($site_id)) {
	    die(json_encode( array( 'error' => __( 'Empty site id', 'mainwp-staging-extension' ) ) ) );
        }
    }

    public static function on_load_settings_page() {
        $i = 0;
        do_action('mainwp_enqueue_meta_boxes_scripts');
        add_meta_box(
                'staging-settings-contentbox-' . $i++,
                '<i class="fa fa-cog"></i> ' . __( 'WP Staging settings', 'mainwp-staging-extension' ),
                array( 'MainWP_Staging', 'render_settings_tab' ),
                'mainwp_staging_postboxes_settings',
                'normal',
                'core',
                array()
        );
    }

    public static function on_load_individual_settings_page() {
        $site_id = 0;
        $override = false;
		if ( self::is_manage_site() ) {
            if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
                $site_id = $_GET['id'];
                $site_data = MainWP_Staging_DB::instance()->get_setting_by( 'site_id', $site_id );
                $cpu_load = '';
                if ( $site_data ) {
                    $override = $site_data->override ? true : false;
                    $settings = unserialize($site_data->settings);
                    $cpu_load = is_array($settings) && isset($settings['cpuLoad']) ? $settings['cpuLoad'] : '';
                }
            }
        }

        $show_settings_tab = $show_stagings_tab = $show_new_tab = false;

        if (isset($_GET['tab'])) {
           if ($_GET['tab'] == 'settings') {
               $show_settings_tab = true;
           } else if ($_GET['tab'] == 'new') {
               $show_new_tab = true;
      } else {
               $show_stagings_tab = true;
           }
        } else {
            $show_stagings_tab = true;
        }

        do_action('mainwp_enqueue_meta_boxes_scripts');
        $i = 0;
        if ($show_settings_tab) {
            add_meta_box(
                    'staging-individual-settings-contentbox-' . $i++,
                    '<i class="fa fa-cog"></i> ' . __( 'Settings', 'mainwp-staging-extension' ),
                    array( 'MainWP_Staging', 'render_site_settings_box' ),
                    'mainwp_staging_postboxes_individual_settings',
                    'normal',
                    'core',
                    array('override' => $override, 'site_id' => $site_id)
            );

            add_meta_box(
                    'staging-individual-settings-contentbox-' . $i++,
                    '<i class="fa fa-cog"></i> ' . __( 'WP Staging Settings', 'mainwp-staging-extension' ),
                    array( 'MainWP_Staging', 'render_settings_tab' ),
                    'mainwp_staging_postboxes_individual_settings',
                    'normal',
                    'core',
                    array('override' => $override, 'site_id' => $site_id)
            );
        }

        if ($show_stagings_tab) {
            add_meta_box(
                    'staging-listing-contentbox-' . $i++,
                    '<i class="fa fa-cog"></i> ' . __( 'Staging Sites', 'mainwp-staging-extension' ),
                    array( 'MainWP_Staging', 'render_stagings_tab'),
                    'mainwp_staging_postboxes_individual_listing',
                    'normal',
                    'core',
                    array('override' => $override, 'site_id' => $site_id, 'cpu_load' => $cpu_load, 'tab' => 'stagings')
            );
        } else if ($show_new_tab) {
            add_meta_box(
                    'staging-listing-contentbox-' . $i++,
                    '<i class="fa fa-cog"></i> ' . __( 'Create Staging Site', 'mainwp-staging-extension' ),
                    array( 'MainWP_Staging', 'render_stagings_tab'),
                    'mainwp_staging_postboxes_individual_listing',
                    'normal',
                    'core',
                    array('override' => $override, 'site_id' => $site_id, 'cpu_load' => $cpu_load, 'tab' => 'new')
            );
        }
    }

    public function ajax_save_settings() {
        self::verify_nonce();
        $individual = isset( $_POST['individual'] ) && $_POST['individual'] ? true : false;

        $site_id = $_POST['stagingSiteID'];

        $individual_data = MainWP_Staging_DB::instance()->get_setting_by( 'site_id', $site_id );
        $override = $individual_data->override;

        if ($individual) {
            if (!$override) {
        die(json_encode( array( 'error' => __( 'Update failed! Override General Settings need to be set to Yes.', 'mainwp-staging-extension' ) ) ) );
            }
            $data = $individual_data;
        } else {
            if ($override) {
        die(json_encode( array( 'error' => __( 'Update failed! Individual site settings are in use.', 'mainwp-staging-extension' ) ) ) );
            }
            $data = MainWP_Staging_DB::instance()->get_setting_by( 'site_id', 0 ); // general settings
        }

        $settings = unserialize($data->settings);
        if (empty($settings) || !is_array($settings)) {
            die(json_encode( array( 'error' => 'Empty settings' ) ) );
        }

        $filters = array(
            'queryLimit',
            'fileLimit',
            'batchSize',
            'cpuLoad',
            'disableAdminLogin',
            'wpSubDirectory',
            'debugMode',
            'unInstallOnDelete',
            'checkDirectorySize',
			'optimizer',
			'loginSlug'
        );
        $save_fields = array();
        foreach($filters as $field) {
            if (isset($settings[$field])) {
                $save_fields[$field]  = $settings[$field];
            }
        }
		global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action' => 'save_settings',
            'settings'   => $save_fields
		);

		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data );
        die(json_encode($information));
    }

    public function ajax_staging_addclonewebsite() {
        self::verify_nonce();

        $site_id = $_POST['stagingSiteID'];
        $cloneID = isset($_POST['clone']) ? $_POST['clone'] : '';
        $clone_url = isset($_POST['clone_url']) ? $_POST['clone_url'] : '';

        if (empty($cloneID) || empty($clone_url)) {
      die(json_encode( array( 'error' => __( 'Empty clone data', 'mainwp-staging-extension' ) ) ) );
        }

		global $mainwp_StagingExtensionActivator;

        $information = apply_filters( 'mainwp_clonesite', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, $cloneID, $clone_url, $force_update = true );
    if ( is_array( $information ) && isset( $information['siteid'] ) && !empty( $information['siteid'] ) ) {
            $update = array(
                'site_id'         => $site_id,
                'clone_id'        => $cloneID,
                'clone_url'       => $clone_url,
                'staging_site_id' => $information['siteid']
            );
            MainWP_Staging_DB::instance()->update_staging_site( $update );
        }
        die( json_encode( $information ) );
    }

    public function ajax_staging_deletewebsite() {
        self::verify_nonce();

        $site_id = $_POST['stagingSiteID'];
        $cloneID = isset($_POST['clone']) ? $_POST['clone'] : '';

        if (empty($cloneID)) {
      die(json_encode( array( 'error' => __( 'Empty clone id', 'mainwp-staging-extension' ) ) ) );
        }

        $staging_site = MainWP_Staging_DB::instance()->get_staging_site( $site_id, $cloneID );
        $information = array();
		global $mainwp_StagingExtensionActivator;
        if ( $staging_site && property_exists($staging_site, 'clone_url') ) {
            // $information = apply_filters( 'mainwp_deleteclonesite', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, $staging_site->clone_url );
            // will release when dashboard 3.4.9 released - did it
            $information = apply_filters( 'mainwp_delete_clonesite', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $staging_site->clone_url );
            MainWP_Staging_DB::instance()->delete_staging_site( $site_id, $cloneID);
        }
        die( json_encode( $information ) );
    }

    public function ajax_get_overview() {
        self::verify_nonce();

        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action' => 'get_overview'
		);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data );

        if(is_array($information) && isset($information['availableClones'])) {
            $available_clones = $information['availableClones'];
            if ( !is_array( $available_clones ) )
                $available_clones = array();

            MainWP_Staging::instance()->sync_staging_site_data( $site_id, $available_clones);
        } else {
            die(json_encode($information));
        }
        ob_start();

			// Existing Staging Sites
			?>

			<table class="wp-list-table widefat fixed striped wpstg-existing-clones" id="wpstg-existing-clones">
				<thead>
					<tr>
						<th scope="col" class="manage-column column-staging-site"><?php _e( 'Site', 'mainwp-staging-extension' ); ?></th>
						<th scope="col" class="manage-column column-staging-url"><?php _e( 'URL', 'mainwp-staging-extension' ); ?></th>
						<th scope="col" class="manage-column column-staging-slug"><?php _e( 'Slug', 'mainwp-staging-extension' ); ?></th>
						<th scope="col" class="manage-column column-staging-db-prefix"><?php _e( 'DB Prefix', 'mainwp-staging-extension' ); ?></th>
						<th scope="col" class="manage-column column-staging-actions"><?php _e( 'Actions', 'mainwp-staging-extension' ); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if ( !empty($available_clones)) : ?>
                            <?php
						foreach ( $available_clones as $name => $data ):
							$urlLogin = $data["url"] . "/wp-login.php";
							$prefix = isset ( $data['prefix'] ) ?  $data['prefix'] : '';
                            ?>
							<tr id="<?php echo $data["directoryName"]; ?>" directory-name="<?php echo $data["directoryName"]; ?>" class="mwp-wpstg-clone">
								<td><strong><a href="<?php echo $urlLogin?>" target="_blank"><?php echo $data["directoryName"]; ?></a></strong></td>
								<td><a href="<?php echo $data["url"]; ?>" target="_blank"><?php echo $data["url"]; ?></a></td>
								<td><?php echo '/' . $data["directoryName"] . '/'; ?></td>
								<td><?php echo $prefix; ?></td>
								<td>
									<a href="<?php echo $urlLogin?>" class="wpstg-open-clone mwp-wpstg-clone-action" target="_blank"><?php _e("Open", "mainwp-staging-extension")?></a> |
					        <a href="#" class="wpstg-execute-clone mwp-wpstg-clone-action" data-clone="<?php echo $name?>"><?php _e("Update", "mainwp-staging-extension")?></a> |
					        <a href="#" class="wpstg-remove-clone mwp-wpstg-clone-action mainwp-red" data-clone="<?php echo $name?>"><?php _e("Delete", "mainwp-staging-extension")?></a>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php else: ?>
						<tr>
							<td colspan="5"><?php _e( 'No existing staging sites found.', "mainwp-staging-extension" ); ?></td>
						</tr>
					<?php endif; ?>
				</tbody>
				<tfoot>
					<tr>
						<th scope="col" class="manage-column column-staging-site"><?php _e( 'Site', 'mainwp-staging-extension' ); ?></th>
						<th scope="col" class="manage-column column-staging-url"><?php _e( 'URL', 'mainwp-staging-extension' ); ?></th>
						<th scope="col" class="manage-column column-staging-slug"><?php _e( 'Slug', 'mainwp-staging-extension' ); ?></th>
						<th scope="col" class="manage-column column-staging-db-prefix"><?php _e( 'DB Prefix', 'mainwp-staging-extension' ); ?></th>
						<th scope="col" class="manage-column column-staging-actions"><?php _e( 'Actions', 'mainwp-staging-extension' ); ?></th>
					</tr>
				</tfoot>
			</table>
			<i class="fa fa-spinner fa-pulse" style="display:none;"></i> <div class="status" style="display:none;"></div>
			<div id="wpstg-removing-clone"></div>
       <?php
			// End Existing Clones

        $html = ob_get_clean();
        die( json_encode(array('result' => $html)) );
   }

   public function sync_staging_site_data($site_id, $available_clones) {
            if (empty($site_id))
                return false;

            $stagings_sites = MainWP_Staging_DB::instance()->get_stagings_of_site( $site_id );
            $current_clone_ids = array();
            if (is_array($stagings_sites)) {
                 foreach ($stagings_sites as $val) {
                    $current_clone_ids[$val->clone_id] = $val->clone_url;
                 }
            }

            $clone_ids = array();
            if (is_array($available_clones)) {
                foreach ($available_clones as $name => $data) {
                    $clone_ids[$data['directoryName']] = $data['url'];
                }
            }

            global $mainwp_StagingExtensionActivator;

            foreach($clone_ids as $clone_id => $clone_url) {
                if (!isset($current_clone_ids[$clone_id])) {
                    // add website into dashboard database
                    $result = apply_filters( 'mainwp_clonesite', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, $clone_id, $clone_url ); // no need to force update clone site
                    if (is_array($result) && isset($result['siteid']) && !empty($result['siteid'])) {
                        $update = array(
                            'site_id' 				=> $site_id,
                            'clone_id' 				=> $clone_id,
                            'clone_url' 			=> $clone_url,
                            'staging_site_id' => $result['siteid']
                        );
                        MainWP_Staging_DB::instance()->update_staging_site( $update );
                    }
                }
            }

            foreach($current_clone_ids as $current_clone_id => $current_clone_url) {
                if (!isset($clone_ids[$current_clone_id])) {
                    // remove website on dashboard database
                    apply_filters( 'mainwp_deleteclonesite', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, $current_clone_url );
                    // will use new hook when dashboard 3.4.9 released
                    //apply_filters( 'mainwp_delete_clonesite', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $current_clone_url );
                    MainWP_Staging_DB::instance()->delete_staging_site( $site_id, $current_clone_id);
                }
            }
   }

   public function ajax_get_scanning() {
        self::verify_nonce();
        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action' => 'get_scan'
		);
        if (isset($_POST['clone'])) {
            $post_data['clone'] = $_POST['clone'];
        }
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data );

        if( is_array( $information ) && isset( $information['options'] ) ) {
            $options = unserialize($information['options']);
            $directoryListing = $information['directoryListing'];
            $prefix = $information['prefix'];
        } else {
                die( json_encode( $information ) );
        }

        global $mainwp_StagingExtensionActivator;
        $dbwebsites = apply_filters( 'mainwp-getdbsites', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), array( $site_id ), array() );
        if ( is_array( $dbwebsites ) && ! empty( $dbwebsites ) ) {
            $website = current( $dbwebsites );
        }
        ob_start();
        ?>
		<div class="mainwp-notice mainwp-notice-yellow" id="mwp-wpstg-clone-id-error" style="display:none;">
	    <?php echo __( "Probably not enough free disk space to create a staging site. You can continue but its likely that the copying process will fail.", "mainwp-staging-extension" ); ?>
    </div>

		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row"><?php echo __( 'Staging site name', 'mainwp-staging-extension' ); ?></th>
					<td><input type="text" id="wpstg-new-clone-id"  value="<?php echo $options->current; ?>"<?php if (null !== $options->current) echo " disabled='disabled'"?>></td>
				</tr>
				<tr>
					<th scope="row"><?php echo __( 'Staging site URL', 'mainwp-staging-extension' ); ?></th>
					<td><?php echo ($website ? $website->url : '')?><span id="wpstg_site_url"><?php echo $options->current; ?></span></td>
				</tr>
			</tbody>
		</table>

         <div class="mwp-wpstg-tabs-wrapper">
			<h3 class="mainwp_box_title" style="border-top: 1px solid #eee; margin: 0 -2px;"><a href="#" class="mwp-wpstg-tab-header active expand" data-id="#wpstg-scanning-db"><?php echo __("DB Tables", "mainwp-staging-extension")?><span class="mwp-wpstg-tab-triangle"><i class="fa fa-caret-down" aria-hidden="true"></i></span></a></h3>
			<div class="mainwp-postbox-actions-top" style="margin: 0 -2px;">
				<?php echo __( "Uncheck the tables you do not want to copy. (If the copy process was previously interrupted, successfully copied tables are greyed out and copy process will skip these ones)", "mainwp-staging-extension" ); ?>
			</div>
             <div class="mwp-wpstg-tab-section" id="wpstg-scanning-db">
                 <ul class="mwp_wpstg_checkboxes">
                 <?php
                 foreach ($options->tables as $table):
                     $attributes = in_array($table->name, $options->excludedTables) ? '' : "checked";
                     $attributes .= in_array($table->name, $options->clonedTables) ? " disabled" : '';
                     ?>
          <li>
						<div class="wpstg-db-table">
                         <label>
                             <input class="wpstg-db-table-checkboxes" type="checkbox" name="<?php echo $table->name?>" <?php echo $attributes?>>
                             <?php echo $table->name?>
                         </label>
              <span class="mwp-wpstg-size-info"><?php echo $this->formatSize($table->size); ?></span>
            </div>
					</li>
                 <?php endforeach ?>
                 </ul>
                 <div>
				   <a href="#" class="wpstg-button-unselect button"><?php _e( 'Un-check all', 'mainwp-staging-extension' ); ?></a>
                     <a href="#" class="wpstg-button-select button" tblprefix="<?php echo $prefix; ?>"><?php echo $prefix; ?></a>
                 </div>
             </div>
			<h3 class="mainwp_box_title" style="border-top: 1px solid #eee; margin: 0 -2px;"><a href="#" class="mwp-wpstg-tab-header expand" data-id="#wpstg-scanning-files"><?php echo __("Files", "mainwp-staging-extension")?><span class="mwp-wpstg-tab-triangle"><i class="fa fa-caret-down" aria-hidden="true"></i></span></a></h3>
			<div class="mainwp-postbox-actions-top" style="margin: 0 -2px;">
				<?php echo __("Uncheck the folders you do not want to copy. Click on them for expanding!", "mainwp-staging-extension")?>
			</div>
             <div class="mwp-wpstg-tab-section" id="wpstg-scanning-files">
                 <?php echo $directoryListing; ?>

			  <h3><?php echo __("Extra directories to copy", "mainwp-staging-extension")?></h3>
                 <textarea id="wpstg_extraDirectories" name="wpstg_extraDirectories" style="width:100%; height:120px;"></textarea>
			  <br/><em><?php echo __( "Enter one folder path per line. Folders must start with absolute path: " . $options->root, "mainwp-staging-extension" ); ?></em>
                 <p>
                         <?php
                         if (isset($options->clone)){
                         echo __("All files are copied into: ", "mainwp-staging-extension") . $options->root . $options->clone;
                         }
                         ?>
                 </p>
             </div>
         </div>
        <br>

        <?php if (null !== $options->current) {  ?>
    <button type="button" class="wpstg-prev-step-link wpstg-link-btn button button-hero"><?php _e("Back", "mainwp-staging-extension")?></button>
        <?php }

		 if (null !== $options->current)
		{
			$label =  __("Update Clone", "mainwp-staging-extension");
			$action = 'staging_update';
		} else {
			$label =  __("Create Staging Site", "mainwp-staging-extension");
			$action = 'staging_cloning';
		}


		?>

		<button type="button" id="wpstg-start-cloning" class="wpstg-next-step-link wpstg-link-btn button button-hero button-primary" data-action="<?php echo $action; ?>">
            <?php echo $label; ?>
        </button>

    <a href="#" id="mwp-wpstg-check-space" class="button button-hero"><?php _e('Check Disk Space', 'mainwp-staging-extension'); ?></a>
        <span id="mwp-wpstg-working-overview">
      <i class="fa fa-spinner fa-pulse fa-3x" style="display:none;"></i> <span class="status" style="display:none;"></span>
        </span>
         <?php
        $html = ob_get_clean();
        die( json_encode(array('result' => $html)) );
    }

  public function formatSize($bytes, $precision = 2) {
  	if ( (double) $bytes < 1 ) {
            return '';
        }
        $units  = array('B', "KB", "MB", "GB", "TB");

        $bytes  = (double) $bytes;
        $base   = log($bytes) / log(1000); // 1024 would be for MiB KiB etc
        $pow    = pow(1000, $base - floor($base)); // Same rule for 1000

        return round($pow, $precision) . ' ' . $units[(int) floor($base)];
    }

   public function ajax_check_disk_space() {
        self::verify_nonce();
        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action' => 'check_disk_space'
		);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data );
        die( json_encode( $information ) );
    }

   public function ajax_check_clone() {
        self::verify_nonce();
        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action' => 'check_clone',
            'cloneID' 	 => $_POST["cloneID"]
		);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data );
      die( json_encode( $information ) );
    }

    public function ajax_cloning() {
        self::verify_nonce();
        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action'          => 'start_clone',
            'cloneID'             => $_POST["cloneID"],
            'includedTables'      => $_POST["includedTables"],
			'excludedTables'      => $_POST["excludedTables"],
            'includedDirectories' => $_POST["includedDirectories"],
            'excludedDirectories' => $_POST["excludedDirectories"],
            'extraDirectories'    => $_POST["extraDirectories"]
		);

		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data );
        die( $information );
    }

    public function ajax_clone_database() {
        self::verify_nonce();
        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action' => 'clone_database'
		);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data );
		die( json_encode( $information ) );
    }

    public function ajax_clone_prepare_directories() {
        self::verify_nonce();
        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action' => 'prepare_directories'
		);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data );
    die( json_encode( $information ) );
    }

    public function ajax_clone_files() {
        self::verify_nonce();
        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action' => 'copy_files'
		);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data );
	  die( json_encode( $information ) );
    }

    public function ajax_clone_replace_data() {
        self::verify_nonce();
        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action' => 'replace_data'
		);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data );
    die( json_encode( $information ) );
    }

    public function ajax_clone_finish() {
        self::verify_nonce();
        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action' => 'clone_finish'
		);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data );
		die( json_encode( $information ) );
    }

    public function ajax_confirm_delete_clone() {
        self::verify_nonce();
        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action' => 'delete_confirmation',
		    'clone'      => $_POST["clone"]
		);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data );

        if(is_array($information) && isset($information['clone'])) {
            $clone = $information['clone'];
            $delete_getTables = $information['deleteTables'];
        } else {
            die(json_encode($information));
        }

        ob_start();

        ?>
    <div class="mainwp-notice mainwp-notice-red">
      <strong><?php _e("Attention: Check carefully if these database tables and files are safe to delete and do not belong to your live site!", "mainwp-staging-extension"); ?></strong><br/>
      <?php _e('Clone name:', 'mainwp-staging-extension' ); ?> <?php echo $clone->directoryName; ?><br/>
      <?php _e( 'Usually the preselected data can be deleted without any risk. But in case something goes wrong you better check it first.', 'mainwp-staging-extension' ); ?>
        </div>
        <div class="mwp-wpstg-tabs-wrapper">
			<h3 class="mainwp_box_title" style="border-top: 1px solid #eee;"><a href="#" class="mwp-wpstg-tab-header active" data-id="#wpstg-scanning-db"><?php echo __("DB tables to remove", "mainwp-staging-extension")?><span class="mwp-wpstg-tab-triangle"><i class="fa fa-caret-down" aria-hidden="true"></i></span></a></h3>

            <!-- Database -->
            <div class="mwp-wpstg-tab-section" id="wpstg-scanning-db">
        <div class="mainwp-postbox-actions-top" style="margin: -10px -10px 10px -10px;"><?php _e("Unselect database tables you do not want to delete", "mainwp-staging-extension")?></div>
                <?php foreach ($delete_getTables as $table):?>
                    <div class="wpstg-db-table">
                        <label>
                            <input class="wpstg-db-table-checkboxes" type="checkbox" name="<?php echo $table->name?>" checked>
                            <?php echo $table->name?>
                        </label>
                        <span class="mwp-wpstg-size-info">
                        <?php echo $table->size?>
                    </span>
                    </div>
                <?php endforeach ?>
        <div style="margin-top: 10px;">
          <a href="#" class="wpstg-button-unselect button"><?php _e( 'Un-check all','mainwp-staging-extension' ); ?></a>
                </div>
            </div>
            <!-- /Database -->

      <h3 class="mainwp_box_title" style="border-top: 1px solid #eee;"><a href="#" class="mwp-wpstg-tab-header" data-id="#wpstg-scanning-files"><?php echo __("Files to remove", "mainwp-staging-extension")?><span class="mwp-wpstg-tab-triangle"><i class="fa fa-caret-down" aria-hidden="true"></i></span></a></h3>

            <!-- Files -->
            <div class="mwp-wpstg-tab-section" id="wpstg-scanning-files">
        <div class="mainwp-postbox-actions-top" style="margin: -10px -10px 10px -10px;"><?php _e("The folder below and all of its subfolders will be deleted. Unselect the checkbox for not deleting the files.", "mainwp-staging-extension") ?></div>
                <div class="wpstg-dir">
                    <label>
                        <input id="deleteDirectory" type="checkbox" class="wpstg-check-dir" name="deleteDirectory" value="1" checked>
                        <?php echo $clone->path; ?>
                        <span class="mwp-wpstg-size-info"></span>
                    </label>
                </div>
            </div>
            <!-- /Files -->
        </div>

		<div style="margin-top: 10px;">
    	<a href="#" class="wpstg-link-btn button" id="wpstg-cancel-removing"><?php _e("Cancel", "mainwp-staging-extension")?></a>
			<a href="#" class="wpstg-link-btn button button-primary" id="wpstg-remove-clone" data-clone="<?php echo $clone->name?>"><?php echo __("Remove", "mainwp-staging-extension")?></a>
		</div>
        <?php
        $html = ob_get_clean();
        die( $html );
    }

    public function ajax_delete_clone() {
        self::verify_nonce();
        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action'     => 'delete_clone',
            'clone'          => $_POST["clone"],
            'excludedTables' => $_POST["excludedTables"],
            'deleteDir'      => $_POST["deleteDir"]
		);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data, $raw = true );  // $raw get raw response from the child site
        die($information);
    }

     public function ajax_cancel_clone() {
        self::verify_nonce();
        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action' => 'cancel_clone',
            'clone'      => $_POST["clone"]
		);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data, $raw = true ); // $raw get raw response from the child site
        die($information);
    }

	public function ajax_staging_update() {
        self::verify_nonce();
        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action'          => 'staging_update',
            'cloneID'             => $_POST["cloneID"],
			'includedTables'      => $_POST["includedTables"],
            'excludedTables'      => $_POST["excludedTables"],
            'includedDirectories' => $_POST["includedDirectories"],
            'excludedDirectories' => $_POST["excludedDirectories"],
            'extraDirectories'    => $_POST["extraDirectories"]
		);

		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data );
        die( $information );
    }

	public function ajax_cancel_update() {
        self::verify_nonce();
        $site_id = $_POST['stagingSiteID'];

        global $mainwp_StagingExtensionActivator;
		$post_data = array(
			'mwp_action' => 'cancel_update',
            'clone'      => $_POST["clone"]
		);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $site_id, 'wp_staging', $post_data, $raw = true ); // $raw get raw response from the child site
        die($information);
    }


    function ajax_override_settings() {
        $websiteId = $_POST['stagingSiteID'];
		if ( empty( $websiteId ) ) {
            die( json_encode( array( 'error' => 'Empty site id.' ) ) );
		}
        $update = array(
            'site_id'  => $websiteId,
            'override' => $_POST['override'],
        );

        MainWP_Staging_DB::instance()->update_setting( $update );
        die( json_encode( array( 'ok' => 1 ) ) );
	}

    public static function ajax_load_sites($what = '', $echo = false) {
        $all_sites = MainWP_Staging_Plugin::get_instance()->get_websites_installed_the_plugin();
        $error = '';

        if (count($all_sites) == 0) {
            $error = __( 'No websites were found with the WP Staging plugin installed.', 'mainwp-staging-extension' );
        }

        if ($what == '') {
            $what = isset( $_POST['what'] ) ? $_POST['what'] : '';
        }

        $html = '';
	  if ( empty( $error ) ) {
                if ($what == 'save_settings')
                    $title = __( 'Saving settings to child sites ...', 'mainwp-staging-extension' );

                ob_start();
                ?>
                <div class="poststuff" id="mainwp_staging_load_sites_box">
                    <div class="postbox">
                        <h3 class="mainwp_box_title"><span><i class="fa fa-cog"></i><?php echo !empty( $title ) ? $title : '&nbsp;'; ?></span></h3>
                        <div class="inside">
                                <?php
                                foreach ( $all_sites as $website ) {
                                    echo '<div><strong>' . $website['name'] . '</strong>: ';
                                    echo '<span class="siteItemProcess" action="" site-id="' . $website['id'] . '" status="queue"><span class="status">Queue ...</span> <i style="display: none;" class="fa fa-spinner fa-pulse"></i></span>';
                                    echo '</div><br />';
                                }
                                ?>
                        </div>
                    </div>
                </div>

            <?php
            if ($what != '') {
                ?>
                <script type="text/javascript">
                    jQuery( document ).ready(function ($) {
                        staging_bulkTotalThreads = jQuery('.siteItemProcess[status=queue]').length;
                        mainwp_staging_save_settings_start_next('<?php echo $what; ?>');
                    })
                </script>
                <?php
            }
            $html = ob_get_clean();

        }

        if ($echo) {
            if (!empty( $error )) {
                echo '<div class="mainwp-notice mainwp-notice-red" >' . $error . '</div>';
                return;
            }
            echo $html;
        } else {
            if (!empty( $error )) {
                die(json_encode(array( 'error' => $error)));
            }
            die(json_encode(array('result' => $html )));
        }
	}

	public static function render() {
        $website = null;
		if ( self::is_manage_site() ) {
            if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
                global $mainwp_StagingExtensionActivator;
                $option = array(
                    'plugin_upgrades' => true,
                    'plugins'         => true,
                );
                $dbwebsites = apply_filters( 'mainwp-getdbsites', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), array( $_GET['id'] ), array(), $option );

                if ( is_array( $dbwebsites ) && ! empty( $dbwebsites ) ) {
                    $website = current( $dbwebsites );
                }
            }

			$error = '';
			if ( empty( $website ) || empty( $website->id ) ) {
				$error = __( 'Error! Site not found.', 'mainwp-staging-extension' );
			} else {
				$activated = false;
				if ( $website && $website->plugins != '' ) {
					$plugins = json_decode( $website->plugins, 1 );
					if ( is_array( $plugins ) && count( $plugins ) > 0 ) {
						foreach ( $plugins as $plugin ) {
							if ( ('wp-staging/wp-staging.php' == strtolower($plugin['slug'])) ) {
								if ( $plugin['active'] ) {
									$activated = true;
								}
								break;
							}
						}
					}
				}
				if ( ! $activated ) {
					$error = __( 'WP Staging plugin is not installed or activated on the site.', 'mainwp-staging-extension' );
				}
			}

			if ( ! empty( $error ) ) {
				do_action( 'mainwp-pageheader-sites', 'Staging' );
				echo '<div class="mainwp-notice mainwp-notice-red">' . $error . '</div>';
				do_action( 'mainwp-pagefooter-sites', 'Staging' );
				return;
			}
		}
		self::render_tabs();
	}

	public static function render_tabs() {
        $is_manager_site = self::is_manage_site();
        $site_id = isset($_GET['id']) ? $_GET['id'] : 0;

        $show_dashboard_tab = $show_settings_tab = $show_stagings_tab = $show_new_tab = false;
        $staging_list_link = $dashboard_link = $new_staging_link = '';
        if ( $is_manager_site ) {
            if (isset($_GET['tab'])) {
                if ($_GET['tab'] == 'settings') {
                    $show_settings_tab = true;
                } else if ($_GET['tab'] == 'new') {
                    $show_new_tab = true;
                } else {
                    $show_stagings_tab = true;
                }
            } else {
                $show_stagings_tab = true;
            }

            $staging_list_link = '<a href="admin.php?page=ManageSitesStaging&id=' . $site_id . '" class="mainwp_action left ' . ($show_stagings_tab ? 'mainwp_action_down selected' : '') . '">' . __( 'Staging sites', 'mainwp-staging-extension' ) . '</a>';
            $new_staging_link = '<a href="admin.php?page=ManageSitesStaging&tab=new&id=' . $site_id . '" class="mainwp_action left ' . ($show_new_tab ? 'mainwp_action_down selected' : '') . '">' . __( 'Create New', 'mainwp-staging-extension' ) . '</a>';
			$setings_link = '<a href="admin.php?page=ManageSitesStaging&tab=settings&id=' . $site_id . '" class="mainwp_action right ' . ($show_settings_tab ? 'mainwp_action_down selected' : '') . '">' . __( 'WP Staging Settings', 'mainwp-staging-extension' ) . '</a>';

		} else {
            if (isset($_GET['tab'])) {
                if ($_GET['tab'] == 'settings') {
                    $show_settings_tab = true;
                } else {
                    $show_dashboard_tab = true;
                }
            } else {
                $show_dashboard_tab = true;
            }
            $dashboard_link = '<a href="admin.php?page=Extensions-Mainwp-Staging-Extension" class="mainwp_action left ' . ($show_dashboard_tab? 'mainwp_action_down selected' : '') . '">' . __( 'WP Staging Dashboard', 'mainwp-staging-extension' ) . '</a>';
            $setings_link = '<a href="admin.php?page=Extensions-Mainwp-Staging-Extension&tab=settings" class="mainwp_action right ' . ($show_settings_tab ? 'mainwp_action_down selected' : '') . '">' . __( 'Settings', 'mainwp-staging-extension' ) . '</a>';
		}

		if ($is_manager_site) {
			do_action( 'mainwp-pageheader-sites', 'Staging' );
		}
		?>
        <div class="mainwp-subnav-tabs">
            <div class="staging_tabs_lnk"><?php echo $dashboard_link . $staging_list_link . $new_staging_link . $setings_link; ?></div>
					<div style="clear:both;"></div>
        </div>
        <div id="mwp_staging_message_zone" class="mainwp-notice mainwp-notice-green" style="display: none"></div>
		<div id="mwp_staging_error_zone" class="mainwp-notice mainwp-notice-red" style="display: none"></div>
		<?php
        $perform_settings_update = $perform_what = false;
        $updated = MainWP_Staging::instance()->handlePosting();
        if ($updated ) {
            ?>
            <div class="mainwp-notice mainwp-notice-yellow"><?php _e('Settings saved', 'mainwp-staging-extension'); ?></div>
            <?php
            if ($is_manager_site) {
                update_option( 'mainwp_staging_perform_individual_setting', 'yes');
            } else {
                $perform_settings_update = true;
                $perform_what = 'save_settings';
            }
        }

        if ( ! $is_manager_site ) {
                global $mainwp_StagingExtensionActivator;
                $websites = apply_filters( 'mainwp-getsites', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), null );
                $sites_ids = array();
                if ( is_array( $websites ) ) {
                    foreach ( $websites as $site ) {
                        $sites_ids[] = $site['id'];
                    }
                    unset( $websites );
                }
                $option = array(
                    'plugin_upgrades' => true,
                    'plugins'         => true,
                );

                $dbwebsites = apply_filters( 'mainwp-getdbsites', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $sites_ids, array(), $option );
                $selected_group = 0;

                if ( isset( $_POST['mainwp_staging_plugin_groups_select'] ) ) {
                    $selected_group = intval( $_POST['mainwp_staging_plugin_groups_select'] );
                }

                $pluginDataSites = array();
                if ( count( $sites_ids ) > 0 ) {
                    $pluginDataSites = MainWP_Staging_DB::instance()->get_staging_data( $sites_ids );
                }

                $dbwebsites_data = MainWP_Staging_Plugin::get_instance()->get_websites_with_the_plugin( $dbwebsites, $selected_group, $pluginDataSites );

                unset( $dbwebsites );
                unset( $pluginDataSites );

                if ( $show_dashboard_tab ) {
                    ?>
                    <div class="tablenav top">
                       <?php MainWP_Staging_Plugin::gen_select_sites( $dbwebsites_data, $selected_group ); ?>
                                <input type="button"
								 class="mainwp-upgrade-button button-primary button"
                                value="<?php _e( 'Sync Data', 'mainwp-staging-extension' ); ?>"
								 id="dashboard_refresh"
								 style="background-image: none!important; float:right; padding-left: .6em !important;">
                   </div>
                   <?php
                   MainWP_Staging_Plugin::gen_dashboard_tab($dbwebsites_data);
                }
        }

        if ( $perform_settings_update && !$is_manager_site) {
                self::ajax_load_sites( $perform_what, true );
        } else {
            if ( $is_manager_site ) {
                if ($show_settings_tab) {
                    do_action( 'mainwp_do_meta_boxes', 'mainwp_staging_postboxes_individual_settings');
                } else if ( $show_stagings_tab || $show_new_tab) {
                    do_action( 'mainwp_do_meta_boxes', 'mainwp_staging_postboxes_individual_listing');
                }
            } else {
                if ($show_settings_tab) {
                    do_action( 'mainwp_do_meta_boxes', 'mainwp_staging_postboxes_settings');
                }
            }
        }

			if ( $is_manager_site ) {
			do_action( 'mainwp-pagefooter-sites', 'Staging' );
		}
	}

    public static function render_site_settings_box($post, $metabox = null) {
            $override = isset($metabox['args']['override']) ? $metabox['args']['override'] : 0;
            $site_id = isset($metabox['args']['site_id']) ? $metabox['args']['site_id'] : 0;
            ?>
            <script type="text/javascript">
            <?php
                if ( $site_id ) {
                    if ( get_option( 'mainwp_staging_perform_individual_setting' ) == 'yes' ) {
                        delete_option( 'mainwp_staging_perform_individual_setting');
                        ?>
                        jQuery(document).ready(function ($) {
                            mainwp_staging_save_individual_settings(<?php echo $site_id; ?>);
                        })
                        <?php
                    }
                }
            ?>
            </script>
            <input type="hidden" name="mainwp_staging_site_id" value="<?php echo $site_id; ?>">
            <div id="mwp_staging_setting_ajax_message">
                <div class="mainwp-notice status" style="display:none;"></div>
                <span class="loading" style="display:none;"><i class="fa fa-spinner fa-pulse"></i> <?php _e( 'Saving ...', 'mainwp-wordfence-extension' ); ?></span>
                <div style="display: none" class="detailed"></div>
            </div>
      <table class="form-table">
                <tbody>
                    <tr>
            <th scope="row"><?php _e( 'Override General Settings', 'mainwp-staging-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Set to YES if you want to overwrite global WP Staging options.', 'mainwp-staging-extension' ) ); ?></th>
                        <td>
                            <div class="mainwp-checkbox">
                <input type="checkbox"
											 id="mainwp_staging_override_general_settings"
                       name="mainwp_staging_override_general_settings"
											 <?php echo( $override ? 'checked="checked"' : '' ); ?>
                                       value="1"/>
                                <label for="mainwp_staging_override_general_settings"></label>
                            </div>
                            <span class="staging_change_override_working"></div>
                        </td>
                    </tr>
                </tbody>
            </table>
        <?php
    }

    public static function getCPULoadSetting($setting) {
      switch ( $setting ) {
         case "high":
            $cpuLoad = 0;
            break;

         case "medium":
            $cpuLoad = 1000;
            break;

         case "low":
            $cpuLoad = 3000;
            break;

         case "default":
         default:
            $cpuLoad = 1000;
      }
      return $cpuLoad;
   }

    private function handlePosting() {
        if (isset($_POST['_wpnonce_mainwp_wpstg_settings']) && wp_verify_nonce($_POST['_wpnonce_mainwp_wpstg_settings'], '_wpnonce_mainwp_wpstg_settings')) {
            $site_id = intval($_POST['mainwp_staging_site_id']); // site_id = 0 is general settings
            $settings = $this->sanitizeData($_POST['mainwp_wp_stg_settings']);
            $update = array(
                    'site_id'  => $site_id,
                    'settings' => serialize( $settings ),
            );
            MainWP_Staging_DB::instance()->update_setting( $update );
            return true;
        }
        return false;
    }

    private function sanitizeData( $data = array() ) {
      $sanitized = array();
      foreach ( $data as $key => $value ) {
         $sanitized[$key] = (is_array( $value )) ? $this->sanitizeData( $value ) : htmlspecialchars( $value );
      }
      return $sanitized;
   }

    public static function render_settings_tab($post, $metabox = null) {
        //$override = isset($metabox['args']['override']) ? $metabox['args']['override'] : 0;
        $site_id = isset($metabox['args']['site_id']) ? $metabox['args']['site_id'] : 0;  // site_id = 0 is general settings

        $site_data = MainWP_Staging_DB::instance()->get_setting_by( 'site_id', $site_id );
        if ($site_data) {
            $settings = unserialize($site_data->settings);
        }

        if (!is_array($settings))
            $settings = array();
    ?>
     <form method="post" action="">
        <input type="hidden" name="mainwp_staging_site_id" value="<?php echo $site_id; ?>">
		<!-- WP Staging Settings -->

                <table class="form-table">
                            <tbody>
				<tr>
					<th scope="row"><?php _e('DB copy query limit', 'mainwp-staging-extension'); ?></th>
					<td>
						<input id="mainwp_wp_stg_settings[queryLimit]" name="mainwp_wp_stg_settings[queryLimit]" class="medium-text" step="1" max="999999" min="0" value="<?php echo isset($settings['queryLimit']) ? $settings['queryLimit'] : 5000; ?>" type="number"> <?php _e( 'Default: 5000', 'mainwp-staging-extension' ); ?>
							<br/><br/>
							<em><?php _e( 'Number of DB rows, that will be copied within one ajax request. The higher the value the faster the database copy process. To find out the highest possible values try a high value like 1.000 or more. If you get timeout issues, lower it until you get no more errors during copying process.', 'mainwp-staging-extension'); ?></em>
						</td>
					</tr>
				<?php $fileLimit = isset($settings['fileLimit']) ? $settings['fileLimit'] : 1; ?>
				<tr>
					<th scope="row"><?php _e( 'File copy limit', 'mainwp-staging-extension' ); ?></th>
                                    <td>
                                        <select id="mainwp_wp_stg_settings[fileLimit]" name="mainwp_wp_stg_settings[fileLimit]" class="medium-text" step="1" max="999999" min="0">
											<option value="1" <?php echo $fileLimit == 1 ? 'selected="selected"' : ''; ?>>1</option>
											<option value="10" <?php echo $fileLimit == 10 ? 'selected="selected"' : ''; ?>>10</option>
											<option value="50" <?php echo $fileLimit == 50 ? 'selected="selected"' : ''; ?>>50</option>
                                            <option value="250" <?php echo $fileLimit == 250 ? 'selected="selected"' : ''; ?>>250</option>
                                            <option value="500" <?php echo $fileLimit == 500 ? 'selected="selected"' : ''; ?>>500</option>
                                            <option value="1000" <?php echo $fileLimit == 1000 ? 'selected="selected"' : ''; ?>>1000</option>
            </select> <?php _e( 'Default: 1', 'mainwp-staging-extension' ); ?>
						<br/><br/>
						<em><?php _e( 'Number of files to copy that will be copied within one ajax request. The higher the value the faster the file copy process. To find out the highest possible values try a high value like 500 or more. If you get timeout issues, lower it until you get no more errors during copying process. ', 'mainwp-staging-extension'); ?></em>
                                    </td>
                                </tr>
				<tr>
					<th scope="row"><?php _e('File copy batch size', 'mainwp-staging-extension'); ?></th>
                                    <td>
						<input id="mainwp_wp_stg_settings[batchSize]" name="mainwp_wp_stg_settings[batchSize]" class="medium-text" step="1" max="999999" min="0" value="<?php echo isset($settings['batchSize']) ? $settings['batchSize'] : 2; ?>" type="number"> <?php _e( 'Default: 2', 'mainwp-staging-extension' ); ?>
						<br/><br/>
						<em><?php _e( 'Buffer size for the file copy process in megabyte. The higher the value the faster large files will be copied. To find out the highest possible values try a high one and lower it until you get no errors during file copy process. Usually this value correlates directly with the memory consumption of php so make sure that it does not exceed any php.ini max_memory limits. ', 'mainwp-staging-extension'); ?></em>
                                    </td>
                                </tr>
                                    <?php
					$cpuLoad = isset( $settings['cpuLoad'] ) ? $settings['cpuLoad'] : 'medium';
					if ( !in_array( $cpuLoad, array( 'high', 'medium', 'low' ) ) ) {
                                        $cpuLoad = 'medium';
                                    }
                                    ?>
				<tr>
					<th scope="row"><?php _e('CPU load priority', 'mainwp-staging-extension'); ?></th>
                                    <td>
                                        <select id="mainwp_wp_stg_settings[cpuLoad]" name="mainwp_wp_stg_settings[cpuLoad]">
							<option value="high" <?php echo $cpuLoad == 'high' ? 'selected="selected"' : ''; ?>><?php _e( 'High (fast)', 'mainwp-staging-extension' ); ?></option>
							<option value="medium" <?php echo $cpuLoad == 'medium' ? 'selected="selected"' : ''; ?>><?php _e( 'Medium (average)', 'mainwp-staging-extension' ); ?></option>
							<option value="low" <?php echo $cpuLoad == 'low' ? 'selected="selected"' : ''; ?>><?php _e( 'Low (slow)', 'mainwp-staging-extension' ); ?></option>
						</select> <?php _e( 'Default: Medium', 'mainwp-staging-extension' ); ?>
						<br/><br/>
						<em><?php _e( 'Using high will result in fast as possible processing but the cpu load increases and it is also possible that staging process gets interrupted because of too many ajax requests (e.g. authorization error). Using a lower value results in lower cpu load on your server but also slower staging site creation.', 'mainwp-staging-extension'); ?></em>
                                    </td>
                                </tr>
				<tr>
					<th scope="row"><?php _e('Disable admin authorization', 'mainwp-staging-extension'); ?></th>
                                    <td>
                                        <input name="mainwp_wp_stg_settings[disableAdminLogin]" id="mainwp_wp_stg_settings[disableAdminLogin]_1" value="1" <?php echo isset($settings['disableAdminLogin']) && $settings['disableAdminLogin'] ? 'checked="checked"' : ''; ?> type="checkbox">
						<br/><br/>
						<em><?php _e( 'If you want to remove the requirement to login to the staging site you can deactivate it here. If you disable authentication everyone can see your staging sites including search engines and this can lead to "duplicate content" in search engines.', 'mainwp-staging-extension'); ?></em>
                                    </td>
                                </tr>
				<tr>
					<th scope="row"><?php _e('Wordpress in subdirectory', 'mainwp-staging-extension'); ?></th>
                                    <td>
                                        <input name="mainwp_wp_stg_settings[wpSubDirectory]" id="mainwp_wp_stg_settings[wpSubDirectory]_1" value="1" <?php echo isset($settings['wpSubDirectory']) && $settings['wpSubDirectory'] ? 'checked="checked"' : ''; ?> type="checkbox">
						<br/><br/>
						<em><?php _e( 'Use this option when you gave wordpress its own subdirectory. If you enable this, WP Staging will reset the index.php of the clone site to the originally one.', 'mainwp-staging-extension'); ?></em>
                                    </td>
                                </tr>
				<tr>
					<th scope="row"><?php _e('Debug mode', 'mainwp-staging-extension'); ?></th>
                                    <td>
                                        <input name="mainwp_wp_stg_settings[debugMode]" id="mainwp_wp_stg_settings[debugMode]_1" value="1" <?php echo isset($settings['debugMode']) && $settings['debugMode'] ? 'checked="checked"' : ''; ?> type="checkbox">
						<br/><br/>
						<em><?php _e( 'This will enable an extended debug mode which creates additional entries in <strong>wp-content/uploads/wp-staging/logs/logfile.log</strong>. Please enable this when we ask you to do so.', 'mainwp-staging-extension'); ?></em>
                                    </td>
				</tr>
				<tr>
					<th scope="row"><?php _e('Optimizer', 'mainwp-staging-extension'); ?></th>
						<td>
							<input name="mainwp_wp_stg_settings[optimizer]" id="mainwp_wp_stg_settings[optimizer]_1" value="1" <?php echo isset($settings['optimizer']) && $settings['optimizer'] ? 'checked="checked"' : ''; ?> type="checkbox">
							<br/><br/>
							<em><?php _e( 'The Optimizer is a mu plugin which disables all other plugins during WP Staging processing. Usually this makes the cloning process more reliable. If you experience issues, disable the Optimizer.', 'mainwp-staging-extension'); ?></em>
						</td>
					</tr>
				<tr>
					<th scope="row"><?php _e('Remove data on uninstall?', 'mainwp-staging-extension'); ?></th>
                                    <td>
                                        <input name="mainwp_wp_stg_settings[unInstallOnDelete]" id="mainwp_wp_stg_settings[unInstallOnDelete]_1" value="1" <?php echo isset($settings['unInstallOnDelete']) && $settings['unInstallOnDelete'] ? 'checked="checked"' : ''; ?> type="checkbox">
						<br/><br/>
						<em><?php _e( 'Check this box if you like WP Staging to completely remove all of its data when the plugin is deleted. This will not remove staging sites files or database tables.', 'mainwp-staging-extension'); ?></em>
                                    </td>
                                </tr>
							<tr>
								<th scope="row"><?php _e('Check directory size', 'mainwp-staging-extension'); ?></th>
									<td>
									<input name="mainwp_wp_stg_settings[checkDirectorySize]" id="mainwp_wp_stg_settings[checkDirectorySize]_1" value="1" <?php echo isset($settings['checkDirectorySize']) && $settings['checkDirectorySize'] ? 'checked="checked"' : ''; ?> type="checkbox">
									<br/><br/>
									<em><?php _e( 'Check this box if you like WP Staging to check sizes of each directory on scanning process.', 'mainwp-staging-extension'); ?></em><br/>
									<strong><em><?php _e( 'Warning this might cause timeout problems in big directory / file structures.', 'mainwp-staging-extension'); ?></em></strong>
                                    </td>
                                </tr>

								 <tr class="row">
									 <th scope="row"><label for="mainwp_wp_stg_settings[loginSlug]"><?php _e('Login Custom Link', 'mainwp-staging-extension'); ?></label></th>
									<td>
										<?php echo 'http://childsite.com/?'; ?>
									   <input id="mainwp_wp_stg_settings[loginSlug]" name="mainwp_wp_stg_settings[loginSlug]" value="<?php echo isset($settings['loginSlug']) ? esc_attr( $settings['loginSlug'] ) : ''; ?>" type="text">
									<br/><br/>
									<em><?php _e( 'Enter the string which links to your login page if you are using a custom login page instead the default WordPress login. <br/><strong>This does not affect already existing staging sites.</strong><br/> You need to create a new staging site if you like to change the login url of a staging site.', 'mainwp-staging-extension' ); ?></em>
                                    </td>
                               </tr>
                            </tbody>
                        </table>

		<!-- End of WP Staging Settings -->

		<input type="submit" class="button-primary button button-hero" name="submit_staging" value="<?php _e( 'Save Settings', 'mainwp-staging-extension' ); ?>"/>
                <input type="hidden" name="_wpnonce_mainwp_wpstg_settings" value="<?php echo wp_create_nonce( '_wpnonce_mainwp_wpstg_settings' ); ?>"/>
        </form>
           <?php
    }

    public static function render_stagings_tab($post, $metabox = null) {
        $site_id = isset($metabox['args']['site_id']) ? $metabox['args']['site_id'] : 0;
        $cpu_load = isset($metabox['args']['cpu_load']) ? $metabox['args']['cpu_load'] : '';
        $cpu_load = self::getCPULoadSetting($cpu_load);
        $tab = isset($metabox['args']['tab']) ? $metabox['args']['tab'] : false;
    ?>
		<div id="mwp-wpstg-working-clone">
            <div class="status mainwp-notice mainwp-notice-yellow" style="display:none;"></div>
			<div class="mwp-loading" style="display:none;"><i class="fa fa-spinner fa-pulse"></i> <?php _e( "Loading...", "mainwp-staging-extension" ); ?></div>
		</div>

<!--		<ul id="wpstg-steps">
			<li class="wpstg-current-step">
				<span class="wpstg-step-num">1</span>
				<?php //echo __("Overview", "wpstg")?>
			</li>
			<li>
				<span class="wpstg-step-num">2</span>
				<?php //echo __("Scanning", "wpstg")?>
			</li>
			<li>
				<span class="wpstg-step-num">3</span>
				<?php //echo __("Cloning", "wpstg")?>
			</li>
			<li>
				<span id="wpstg-loader" style="display:none;"></span>
			</li>
		</ul>-->

        <div id="mwp-wpstg-workflow" class="" site-id="<?php echo $site_id; ?>"></div>
        <script type="text/javascript">
            var mainwp_staging_$workFlow = null, mainwp_staging_$working = null;
            jQuery(document).ready(function ($) {
                staging_elementsCache.site_id = <?php echo $site_id; ?>;
                staging_elementsCache.cpuLoad = <?php echo $cpu_load; ?>;
                mainwp_staging_$workFlow = jQuery('#mwp-wpstg-workflow');
                mainwp_staging_$working = jQuery('#mwp-wpstg-working-clone');
				MainWP_WPStaging.init();
                <?php if ($tab == 'new') {  ?>
                        mainwp_staging_scanning();
                <?php } else {  ?>
                        mainwp_staging_loadOverview();
                <?php }   ?>
            })
        </script>
        <?php
    }

    public function get_site_settings($site_id) {
        if (empty($site_id))
            return false;

        $settings = array();
        $override = false;
        $site_data = MainWP_Staging_DB::instance()->get_setting_by( 'site_id', $site_id );
        if ($site_data) {
            $settings = unserialize($site_data->settings);
            if ( is_array($settings) && $settings['override'] )
                $override = true;
        }

        if ( empty($settings) || !$override ) {
            $site_data = MainWP_Staging_DB::instance()->get_setting_by( 'site_id', 0 ); // general settings
            if ($site_data) {
                $settings = unserialize($site_data->settings);
            }
        }

        return $settings;
    }

    public static function map_site( &$website, $keys ) {
		$outputSite = array();
		foreach ( $keys as $key ) {
			$outputSite[ $key ] = $website->$key;
		}
		return $outputSite;
	}

    public static function is_manage_site() {
		if ( isset( $_POST['isIndividual'] ) && ! empty( $_POST['isIndividual'] ) && isset( $_POST['stagingSiteID'] ) && ! empty( $_POST['stagingSiteID'] ) ) {
			return true;
		} else if ( isset( $_GET['page'] ) && ('ManageSitesStaging' == $_GET['page']) ) {
			return true;
		}
		return false;
	}

    public static function formatTimestamp( $timestamp ) {
		return date_i18n( get_option( 'date_format' ) . ' ' . get_option( 'time_format' ), $timestamp );
	}

    public static function showMainWPMessage( $type, $notice_id ) {
                if ($type == 'tour') {
                    $status = get_user_option( 'mainwp_tours_status' );
                } else {
                    $status = get_user_option( 'mainwp_notice_saved_status' );
                }

                if ( ! is_array( $status ) ) {
                        $status = array();
                }
                if ( isset( $status[ $notice_id ] ) ) {
                    return false;
                }
		return true;
	}

} // End of class
