<?php

class MainWP_Staging_Plugin {

	private $option_handle = 'mainwp_staging_plugin_option';
	private $option = array();
	private static $order = '';
	private static $orderby = '';
	//Singleton
	private static $instance = null;

	static function get_instance() {
		if ( null == MainWP_Staging_Plugin::$instance ) {
			MainWP_Staging_Plugin::$instance = new MainWP_Staging_Plugin();
		}
		return MainWP_Staging_Plugin::$instance;
	}

	public function __construct() {
		$this->option = get_option( $this->option_handle );
	}

	public function admin_init() {
		add_action( 'wp_ajax_mainwp_staging_upgrade_noti_dismiss', array( $this, 'ajax_dismiss_notice' ) );
		add_action( 'wp_ajax_mainwp_staging_active_plugin', array( $this, 'ajax_active_plugin' ) );
		add_action( 'wp_ajax_mainwp_staging_upgrade_plugin', array( $this, 'ajax_upgrade_plugin' ) );
		add_action( 'wp_ajax_mainwp_staging_showhide_plugin', array( $this, 'ajax_showhide_plugin' ) );
	}

	public function get_option( $key = null, $default = '' ) {
		if ( isset( $this->option[ $key ] ) ) {
			return $this->option[ $key ]; }
		return $default;
	}

	public function set_option( $key, $value ) {
		$this->option[ $key ] = $value;
		return update_option( $this->option_handle, $this->option );
	}

	public static function gen_dashboard_tab( $websites ) {		
		$orderby = 'name';
		$_order = 'desc';
		if ( isset( $_GET['staging_orderby'] ) && ! empty( $_GET['staging_orderby'] ) ) {
			$orderby = $_GET['staging_orderby'];
		}
		if ( isset( $_GET['staging_order'] ) && ! empty( $_GET['staging_order'] ) ) {
			$_order = $_GET['staging_order'];
		}

		$name_order = $version_order = $status_order = $last_scan_order = $time_order = $url_order = $hidden_order = '';

		if ( isset( $_GET['staging_orderby'] ) ) {
			if ( 'name' == $_GET['staging_orderby'] ) {
				$name_order = ('desc' == $_order) ? 'asc' : 'desc';
			} else if ( 'version' == $_GET['staging_orderby'] ) {
				$version_order = ('desc' == $_order) ? 'asc' : 'desc';
			} else if ( 'url' == $_GET['staging_orderby'] ) {
				$url_order = ('desc' == $_order) ? 'asc' : 'desc';
			} else if ( 'hidden' == $_GET['staging_orderby'] ) {
				$hidden_order = ('desc' == $_order) ? 'asc' : 'desc';
			}
		}

		self::$order = $_order;
		self::$orderby = $orderby;
		usort( $websites, array( 'MainWP_Staging_Plugin', 'data_sort' ) );
		?>
      <table id="mainwp-table-plugins" class="wp-list-table widefat plugins" cellspacing="0">
        <thead>
          <tr>
          	<th class="check-column">
              <input type="checkbox"  id="cb-select-all-1" >
            </th>
						<th scope="col" class="manage-column sortable <?php echo $name_order; ?>">
							<a href="?page=Extensions-Mainwp-Staging-Extension&tab=dashboard&staging_orderby=name&staging_order=<?php echo (empty( $name_order ) ? 'asc' : $name_order); ?>"><span><?php _e( 'Site', 'mainwp-staging-extension' ); ?></span><span class="sorting-indicator"></span></a>
            </th>
						<th scope="col" class="manage-column sortable <?php echo $url_order; ?>">
							<a href="?page=Extensions-Mainwp-Staging-Extension&tab=dashboard&staging_orderby=url&staging_order=<?php echo (empty( $url_order ) ? 'asc' : $url_order); ?>"><span><?php _e( 'URL', 'mainwp-staging-extension' ); ?></span><span class="sorting-indicator"></span></a>
            </th>
						<th scope="col" class="manage-column sortable <?php echo $version_order; ?>">
							<a href="?page=Extensions-Mainwp-Staging-Extension&tab=dashboard&staging_orderby=version&staging_order=<?php echo (empty( $version_order ) ? 'asc' : $version_order); ?>"><span><?php _e( 'Version', 'mainwp-staging-extension' ); ?></span><span class="sorting-indicator"></span></a>
            </th>
						<th scope="col" class="manage-column sortable <?php echo $hidden_order; ?>">
							<a href="?page=Extensions-Mainwp-Staging-Extension&tab=dashboard&staging_orderby=hidden&staging_order=<?php echo (empty( $hidden_order ) ? 'asc' : $hidden_order); ?>"><span><?php _e( 'Hidden', 'mainwp-staging-extension' ); ?></span><span class="sorting-indicator"></span></a>
            </th>
            <th scope="col" class="manage-column">
              <span><?php _e( 'Settings', 'mainwp-staging-extension' ); ?></span>
            </th>
            <th scope="col" class="manage-column">
							<span><?php _e( 'Staging', 'mainwp-staging-extension' ); ?></span>
            </th>
            </tr>
          </thead>
          <tfoot>
  					<tr>
              <th class="check-column">
                <input type="checkbox"  id="cb-select-all-2" >
          		</th>
							<th scope="col" class="manage-column sortable <?php echo $name_order; ?>">
								<a href="?page=Extensions-Mainwp-Staging-Extension&tab=dashboard&staging_orderby=name&staging_order=<?php echo (empty( $name_order ) ? 'asc' : $name_order); ?>"><span><?php _e( 'Site' ); ?></span><span class="sorting-indicator"></span></a>
              </th>
							<th scope="col" class="manage-column sortable <?php echo $url_order; ?>">
								<a href="?page=Extensions-Mainwp-Staging-Extension&tab=dashboard&staging_orderby=url&staging_order=<?php echo (empty( $url_order ) ? 'asc' : $url_order); ?>"><span><?php _e( 'URL', 'mainwp-staging-extension' ); ?></span><span class="sorting-indicator"></span></a>
              </th>
							<th scope="col" class="manage-column sortable <?php echo $version_order; ?>">
								<a href="?page=Extensions-Mainwp-Staging-Extension&tab=dashboard&staging_orderby=version&staging_order=<?php echo (empty( $version_order ) ? 'asc' : $version_order); ?>"><span><?php _e( 'Version', 'mainwp-staging-extension' ); ?></span><span class="sorting-indicator"></span></a>
            	</th>
							<th scope="col" class="manage-column sortable <?php echo $hidden_order; ?>">
								<a href="?page=Extensions-Mainwp-Staging-Extension&tab=dashboard&staging_orderby=hidden&staging_order=<?php echo (empty( $hidden_order ) ? 'asc' : $hidden_order); ?>"><span><?php _e( 'Hidden', 'mainwp-staging-extension' ); ?></span><span class="sorting-indicator"></span></a>
            	</th>
              <th scope="col" class="manage-column sortable">
								<span><?php _e( 'Settings', 'mainwp-staging-extension' ); ?></span>
              </th>
              <th scope="col" class="manage-column">
								<span><?php _e( 'Staging', 'mainwp-staging-extension' ); ?></span>
              </th>
        		</tr>
    			</tfoot>
          <tbody id="the-mwp-staging-list" class="list:sites">
					<?php
						if ( is_array( $websites ) && count( $websites ) > 0 ) {
							self::get_dashboard_table_row( $websites );
						} else {
							echo '<tr><td colspan="8">' . __( 'No websites were found with the WP Staging plugin installed.', 'mainwp-staging-extension' ) . '</td></tr>';
						}
					?>
          </tbody>
        </table>
		<?php
	}

	public static function get_dashboard_table_row( $websites ) {
		$dismiss = array();
		if ( session_id() == '' ) {
			session_start();             
        }
        
		if ( isset( $_SESSION['mainwp_staging_dismiss_upgrade_plugin_notis'] ) ) {
			$dismiss = $_SESSION['mainwp_staging_dismiss_upgrade_plugin_notis'];
		}

		if ( ! is_array( $dismiss ) ) {
			$dismiss = array();             
        }

		$plugin_name = 'WP Staging';
        $staging_sites_info = MainWP_Staging_DB::instance()->get_count_stagings_of_sites();
        
        
		foreach ( $websites as $website ) {
			$location = 'admin.php?page=wpstg_clone';
			$website_id = $website['id'];
			$cls_active = (isset( $website['staging_active'] ) && ! empty( $website['staging_active'] )) ? 'active' : 'inactive';
			$cls_update = (isset( $website['staging_upgrade'] )) ? 'update' : '';
			$cls_update = ('inactive' == $cls_active) ? 'update' : $cls_update;
			$showhide_action = (1 == $website['hide_staging']) ? 'show' : 'hide';
			$showhide_link = '<a href="#" class="mwp_staging_showhide_plugin" showhide="' . $showhide_action . '"><i class="fa fa-eye-slash"></i> ' . ('show' === $showhide_action ? 'Show ' . $plugin_name . ' plugin' : 'Hide ' . $plugin_name . ' plugin') . '</a>';
			?>
			<tr class="<?php echo $cls_active . ' ' . $cls_update; ?>" website-id="<?php echo $website_id; ?>" plugin-name="<?php echo $plugin_name; ?>">
                <th class="check-column">
                    <input type="checkbox"  name="checked[]">
                </th>
                <td>
					<a href="admin.php?page=managesites&dashboard=<?php echo $website_id; ?>"><?php echo $website['name']; ?></a><br/>
					<div class="row-actions"><span class="dashboard"><a href="admin.php?page=managesites&dashboard=<?php echo $website_id; ?>"><i class="fa fa-tachometer"></i> <?php _e( 'Overview', 'mainwp-staging-extension' ); ?></a></span> |  <span class="edit"><a href="admin.php?page=managesites&id=<?php echo $website_id; ?>"><i class="fa fa-pencil-square-o"></i> <?php _e( 'Edit' ); ?></a> | <?php echo $showhide_link; ?></span></div>                    
					<div class="its-action-working"><span class="status" style="display:none;"></span><span class="loading" style="display:none;"><i class="fa fa-spinner fa-pulse"></i> <?php _e( 'Please wait...' ); ?></span></div>
                </td>               
                <td>
					<a href="<?php echo $website['url']; ?>" target="_blank"><?php echo $website['url']; ?></a><br/>
					<div class="row-actions"><span class="edit"><a target="_blank" href="admin.php?page=SiteOpen&newWindow=yes&websiteid=<?php echo $website_id; ?>"><i class="fa fa-external-link"></i> <?php _e( 'Open WP-Admin', 'mainwp-staging-extension' ); ?></a></span> | <span class="edit"><a href="admin.php?page=SiteOpen&newWindow=yes&websiteid=<?php echo $website_id; ?>&location=<?php echo base64_encode( $location ); ?>" target="_blank"><i class="fa fa-cog"></i> <?php _e( 'Open WP Staging', 'mainwp-staging-extension' ); ?></a></span></div>                    
                </td>
                <td>
					<?php
					if ( isset( $website['staging_plugin_version'] ) ) {
						echo $website['staging_plugin_version'];					
					} else {
						echo '&nbsp;'; }
					?>
                </td>     
                <td>
                    <span class="staging_hidden_title"><?php
                            echo (1 == $website['hide_staging']) ? __( 'Yes' ) : __( 'No' );
                            ?>
                    </span>
                </td>
                <td>
                        <span><a href="admin.php?page=ManageSitesStaging&tab=settings&id=<?php echo $website_id; ?>"><?php echo ($website['isOverride'] ? __( 'Individual', 'mainwp-staging-extension' ) : __( 'General' )); ?></a></span>                                    
                </td> 
                <td>
                <?php
                    if ($staging_sites_info && isset($staging_sites_info[$website_id]) && $staging_sites_info[$website_id] > 0) {
                        echo '<a class="button button-secondary" href="admin.php?page=ManageSitesStaging&tab=stagings&id=' . $website_id . '">' . __('Manage Staging Sites', 'mainwp-staging-extension') . '</a>';
                    } else {
                        echo '<a class="button button-primary" href="admin.php?page=ManageSitesStaging&tab=new&id=' . $website_id . '">' . __('Create Staging Site', 'mainwp-staging-extension') . '</a>';                        
                    }  
                ?>
                </td> 
            </tr>        
			<?php
			$active_link = $update_link = '';
			$version = '';
			$plugin_slug = $website['plugin_slug'];
			if ( isset( $website['staging_active'] ) && empty( $website['staging_active'] ) ) {
				$active_link = '<a href="#" class="mwp_staging_active_plugin" >Activate ' . $plugin_name . ' plugin</a>'; }

			if ( isset( $website['staging_upgrade'] ) ) {
				if ( isset( $website['staging_upgrade']['new_version'] ) ) {
					$version = $website['staging_upgrade']['new_version']; }
				$update_link = '<a href="#" class="mwp_staging_upgrade_plugin" >Update ' . $plugin_name . ' plugin</a>';
				if ( isset( $website['staging_upgrade']['plugin'] ) ) {
					$plugin_slug = $website['staging_upgrade']['plugin']; }
			}

			$hide_update = false;
			if ( isset( $dismiss[ $website_id ] ) ) {
				$hide_update = true;
			}

			if ( ! empty( $active_link ) || ! empty( $update_link ) ) {
				$location = 'plugins.php';
				$link_row = $active_link . ' | ' . $update_link;
				$link_row = rtrim( $link_row, ' | ' );
				$link_row = ltrim( $link_row, ' | ' );
				?>
				<tr class="plugin-update-tr" <?php echo ( $hide_update ? 'style="display: none"' : ''); ?>>
                        <td colspan="8" class="plugin-update">
                           <div class="ext-upgrade-noti update-message" plugin-slug="<?php echo $plugin_slug; ?>" website-id="<?php echo $website_id; ?>" version="<?php echo $version; ?>"><p>
                                    <?php if ( ! $hide_update ) { ?>
                                            <span style="float:right"><a href="#" class="staging_plugin_upgrade_noti_dismiss"><?php _e( 'Dismiss' ); ?></a></span>                    
                                    <?php } ?>
                                    <?php echo $link_row; ?>
                                    <span class="mwp-staging-row-working"><i class="fa fa-spinner fa-pulse" style="display: none"></i>&nbsp;<span class="status"></span></span>
                            </p></div>
                        </td>
                    </tr>
				<?php
			}
		}
	}

	public static function data_sort( $a, $b ) {
		if ( 'version' == self::$orderby ) {
			$a = $a['staging_plugin_version'];
			$b = $b['staging_plugin_version'];
			$cmp = version_compare( $a, $b );
		} else if ( 'url' == self::$orderby ) {
			$a = $a['url'];
			$b = $b['url'];
			$cmp = strcmp( $a, $b );
		} else if ( 'hidden' == self::$orderby ) {
			$a = $a['hide_staging'];
			$b = $b['hide_staging'];
			$cmp = $a - $b;
		} else {
			$a = $a['name'];
			$b = $b['name'];
			$cmp = strcmp( $a, $b );
		}
		if ( 0 == $cmp ) {
			return 0; }

		if ( 'desc' == self::$order ) {
			return ($cmp > 0) ? -1 : 1;
		} else {
			return ($cmp > 0) ? 1 : -1;
		}
	}

	public function get_websites_installed_the_plugin() {
		global $mainwp_StagingExtensionActivator;
		$websites = apply_filters( 'mainwp-getsites', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), null );
		$sites_ids = array();
		if ( is_array( $websites ) ) {
			foreach ( $websites as $site ) {
				$sites_ids[] = $site['id'];
			}
			unset( $websites );
		}
		$option = array(
            'plugin_upgrades' => true,
			'plugins' => true,
		);
		$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $sites_ids, array(), $option );
		$pluginSites = array();
		if ( count( $sites_ids ) > 0 ) {
			$pluginSites = MainWP_Staging_DB::instance()->get_staging_data( $sites_ids );
		}
		return MainWP_Staging_Plugin::get_instance()->get_websites_with_the_plugin_data( $dbwebsites, $pluginSites );
	}

	public function get_websites_with_the_plugin_data( $websites, $plugin_data_sites = array() ) {
		$sites_data = array();
		if ( is_array( $websites ) ) {
			foreach ( $websites as $website ) {
				if ( $website && $website->plugins != '' ) {
					$plugins = json_decode( $website->plugins, 1 );
					if ( is_array( $plugins ) && count( $plugins ) != 0 ) {
						foreach ( $plugins as $plugin ) {
							if ( ('wp-staging/wp-staging.php' == strtolower($plugin['slug'])) ) {								                             
								if ( ! $plugin['active'] ) {
									continue;                                     
                                }                                
								$data = array( 'id' => $website->id, 'name' => $website->name );
								$plugintDS = isset( $plugin_data_sites[ $website->id ] ) ? $plugin_data_sites[ $website->id ] : array();
								if ( ! is_array( $plugintDS ) ) {
									$plugintDS = array();                                     
                                }								
								$data['isOverride'] = isset( $plugintDS['override'] ) ? $plugintDS['override'] : 0;
								$sites_data[ $website->id ] = $data;
								break;
							}
						}
					}
				}
			}
		}

	return $sites_data;
	}

	public function get_websites_with_the_plugin( $websites, $selected_group = 0, $plugin_data_sites = array() ) {
		$websites_plugin = array();

		$pluginHide = $this->get_option( 'hide_the_plugin' );

		if ( ! is_array( $pluginHide ) ) {
			$pluginHide = array();
		}
		$_text = __( 'Nothing currently scheduled', 'mainwp-staging-extension' );
		if ( is_array( $websites ) && count( $websites ) ) {
			if ( empty( $selected_group ) ) {
				foreach ( $websites as $website ) {
					if ( $website && $website->plugins != '' ) {
						$plugins = json_decode( $website->plugins, 1 );
						if ( is_array( $plugins ) && count( $plugins ) != 0 ) {
							foreach ( $plugins as $plugin ) {
								if ( ('wp-staging/wp-staging.php' == strtolower($plugin['slug'])) ) {
									$site = self::map_site( $website, array( 'id', 'name', 'url' ) );
									if ( $plugin['active'] ) {
										$site['staging_active'] = 1;                                         
                                    } else {
										$site['staging_active'] = 0;                                         
                                    }
                                    // get upgrade info
                                    $site['staging_plugin_version'] = $plugin['version'];
                                    $site['plugin_slug'] = $plugin['slug'];
                                    $plugin_upgrades = json_decode( $website->plugin_upgrades, 1 );
                                    if ( is_array( $plugin_upgrades ) && count( $plugin_upgrades ) > 0 ) {
                                        $upgrade = array();
                                        if ( isset( $plugin_upgrades['wp-staging/wp-staging.php'] ) ) {
                                            $upgrade = $plugin_upgrades['wp-staging/wp-staging.php'];												
                                        } 
                                        if ( isset( $upgrade['update'] ) ) {
                                            $site['staging_upgrade'] = $upgrade['update'];
                                        }
                                    }
                                    $site['hide_staging'] = 0;
                                    if ( isset( $pluginHide[ $website->id ] ) && $pluginHide[ $website->id ] ) {
                                        $site['hide_staging'] = 1;
                                    }
                                    $plugintDS = isset( $plugin_data_sites[ $site['id'] ] ) ? $plugin_data_sites[ $site['id'] ] : array();
                                    if ( ! is_array( $plugintDS ) ) {
                                        $plugintDS = array();                                         
                                    }

                                    $site['isOverride'] = isset( $plugintDS['override'] ) ? $plugintDS['override'] : 0;
                                    $websites_plugin[] = $site;
                                    break;
								}
							}
						}
					}
				}
			} else {
				global $mainwp_StagingExtensionActivator;

				$group_websites = apply_filters( 'mainwp-getdbsites', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), array(), array( $selected_group ) );
				$sites = array();
				foreach ( $group_websites as $site ) {
					$sites[] = $site->id;
				}
				foreach ( $websites as $website ) {
					if ( $website && $website->plugins != '' && in_array( $website->id, $sites ) ) {
						$plugins = json_decode( $website->plugins, 1 );
						if ( is_array( $plugins ) && count( $plugins ) != 0 ) {
							foreach ( $plugins as $plugin ) {
								if ( ('wp-staging/wp-staging.php' == strtolower($plugin['slug'])) ) {
									$site = self::map_site( $website, array( 'id', 'name', 'url' ) );
									if ( $plugin['active'] ) {
										$site['staging_active'] = 1; } else {
										$site['staging_active'] = 0; }
										$site['staging_plugin_version'] = $plugin['version'];
										// get upgrade info
										$plugin_upgrades = json_decode( $website->plugin_upgrades, 1 );
										if ( is_array( $plugin_upgrades ) && count( $plugin_upgrades ) > 0 ) {
                                            $upgrade = array();
											if ( isset( $plugin_upgrades['wp-staging/wp-staging.php'] ) ) {
												$upgrade = $plugin_upgrades['wp-staging/wp-staging.php'];												
											} 
                                            if ( isset( $upgrade['update'] ) ) {
                                                    $site['staging_upgrade'] = $upgrade['update'];
                                            }
										}
										$site['hide_staging'] = 0;
										if ( isset( $pluginHide[ $website->id ] ) && $pluginHide[ $website->id ] ) {
											$site['hide_staging'] = 1;
										}

										$plugintDS = isset( $plugin_data_sites[ $site['id'] ] ) ? $plugin_data_sites[ $site['id'] ] : array();
										if ( ! is_array( $plugintDS ) ) {
											$plugintDS = array();                                             
                                        }
										$site['isOverride'] = $plugintDS['override'];
										$websites_plugin[] = $site;
										break;
								}
							}
						}
					}
				}
			}
		}

		// if search action
		$search_sites = array();
		if ( isset( $_GET['s'] ) && ! empty( $_GET['s'] ) ) {
			$find = trim( $_GET['s'] );
			foreach ( $websites_plugin as $website ) {
				if ( stripos( $website['name'], $find ) !== false || stripos( $website['url'], $find ) !== false ) {
					$search_sites[] = $website;
				}
			}
			$websites_plugin = $search_sites;
		}
		unset( $search_sites );

		return $websites_plugin;
	}

	public static function gen_select_sites( $websites, $selected_group ) {
		global $mainwp_StagingExtensionActivator;
		$groups = apply_filters( 'mainwp-getgroups', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), null );
		$search = (isset( $_GET['s'] ) && ! empty( $_GET['s'] )) ? trim( $_GET['s'] ) : '';
		?> 
        <div class="alignleft actions bulkactions">
            <select id="mwp_staging_plugin_action">
					<option selected="selected" value="-1"><?php _e( 'Bulk Actions', 'mainwp-staging-extension' ); ?></option>
					<option value="activate-selected"><?php _e( 'Active', 'mainwp-staging-extension' ); ?></option>
					<option value="update-selected"><?php _e( 'Update', 'mainwp-staging-extension' ); ?></option>
					<option value="hide-selected"><?php _e( 'Hide', 'mainwp-staging-exetnsion' ); ?></option>
					<option value="show-selected"><?php _e( 'Show', 'mainwp-staging-extension' ); ?></option>
            </select>
			<input type="button" value="<?php _e( 'Apply' ); ?>" class="button action" id="staging_plugin_doaction_btn" name="">
        </div>

        <div class="alignleft actions">
            <form action="" method="GET">
                <input type="hidden" name="page" value="Extensions-Mainwp-Staging-Extension">
				<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"><?php _e( 'No search results.', 'mainwp-staging-extension' ); ?></span>
				<input type="text" class="mainwp_autocomplete ui-autocomplete-input" name="s" autocompletelist="sites" value="<?php echo stripslashes( $search ); ?>" autocomplete="off">
                <datalist id="sites">
					<?php
					if ( is_array( $websites ) && count( $websites ) > 0 ) {
						foreach ( $websites as $website ) {
							echo '<option>' . $website['name'] . '</option>';
						}
					}
					?>                
                </datalist>
                <input type="submit" name="" class="button" value="Search Sites">
            </form>
        </div>    
        <div class="alignleft actions">
            <form method="post" action="admin.php?page=Extensions-Mainwp-Staging-Extension">
                <select name="mainwp_staging_plugin_groups_select">
					<option value="0"><?php _e( 'Select a group' ); ?></option>
					<?php
					if ( is_array( $groups ) && count( $groups ) > 0 ) {
						foreach ( $groups as $group ) {
							$_select = '';
							if ( $selected_group == $group['id'] ) {
								$_select = 'selected '; }
							echo '<option value="' . $group['id'] . '" ' . $_select . '>' . $group['name'] . '</option>';
						}
					}
					?>
          </select>&nbsp;
				<input class="button" type="submit" name="mwp_staging_plugin_btn_display" value="<?php _e( 'Display', 'mainwp-staging-extension' ); ?>">
            </form>  
        </div>    
		<?php
		return;
	}

	public function ajax_dismiss_notice() {
            if (isset($_POST['dismiss'])) {
                if ('override_notice' == $_POST['dismiss']) {
                    update_option('mainwp_staging_override_notice_dismiss', 1);                    
                    die('dismissed');
                }               
            } else if (isset($_POST['stagingSiteID'])) {            
                $website_id = $_POST['stagingSiteID'];
                $version = $_POST['new_version'];
                if ( $website_id ) {
                        session_start();
                        $dismiss = $_SESSION['mainwp_staging_dismiss_upgrade_plugin_notis'];
                        if ( is_array( $dismiss ) && count( $dismiss ) > 0 ) {
                                $dismiss[ $website_id ] = 1;
                        } else {
                                $dismiss = array();
                                $dismiss[ $website_id ] = 1;
                        }
                        $_SESSION['mainwp_staging_dismiss_upgrade_plugin_notis'] = $dismiss;
                        die( 'updated' );
                }
            }
            die( 'nochange' );
	}

	public function ajax_active_plugin() {
		$_POST['websiteId'] = $_POST['stagingSiteID'];
		do_action( 'mainwp_activePlugin' );
		die();
	}

	public function ajax_upgrade_plugin() {
		$_POST['websiteId'] = $_POST['stagingSiteID'];
		do_action( 'mainwp_upgradePluginTheme' );
		die();
	}

	public function ajax_showhide_plugin() {
		$siteid = isset( $_POST['stagingSiteID'] ) ? $_POST['stagingSiteID'] : null;
		$showhide = isset( $_POST['showhide'] ) ? $_POST['showhide'] : null;
		if ( null !== $siteid && null !== $showhide ) {
			global $mainwp_StagingExtensionActivator;
			$post_data = array(
                'mwp_action' => 'set_showhide',
				'showhide' => $showhide,
			);
			$information = apply_filters( 'mainwp_fetchurlauthed', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $siteid, 'wp_staging', $post_data );

			if ( is_array( $information ) && isset( $information['result'] ) && 'SUCCESS' === $information['result'] ) {
				$hide_staging = $this->get_option( 'hide_the_plugin' );
				if ( ! is_array( $hide_staging ) ) {
					$hide_staging = array(); }
				$hide_staging[ $siteid ] = ('hide' === $showhide) ? 1 : 0;
				$this->set_option( 'hide_the_plugin', $hide_staging );
			}
			die( json_encode( $information ) );
		}
		die();
	}
    
    public static function map_site( &$website, $keys ) {
		$outputSite = array();
		foreach ( $keys as $key ) {
			$outputSite[ $key ] = $website->$key;
		}
		return $outputSite;
	}
    
}
