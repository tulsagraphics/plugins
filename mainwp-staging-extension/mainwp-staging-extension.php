<?php
/*
Plugin Name: MainWP Staging Extension
Plugin URI: https://mainwp.com
Description: MainWP Staging Extension allows you to create and manage Staging sites directly from your MainWP Dashboard.
Version: 1.1
Author: MainWP
Author URI: https://mainwp.com
Documentation URI: https://mainwp.com/help/category/mainwp-extensions/staging/
*/

// Based on the version 2.1.1

if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct access allowed' ); }

if ( ! defined( 'MAINWP_STAGING_PLUGIN_FILE' ) ) {
	define( 'MAINWP_STAGING_PLUGIN_FILE', __FILE__ );
}

define( 'MAINWP_STAGING_PLUGIN_DIR', dirname( __FILE__ ) );
define( 'MAINWP_STAGING_PLUGIN_URL', plugins_url( '', __FILE__ ) );

class MainWP_Staging_Extension
{
	public static $instance = null;
    protected $staging_sites_info = null;    
        
	static function instance() {
		if ( null == self::$instance ) { 
                    self::$instance = new self();                     
                }
		return self::$instance;
	}

	public function __construct() {                      
            add_action( 'admin_init', array( &$this, 'admin_init' ) );
            add_action( 'init', array( &$this, 'localization' ) );                
            add_filter( 'mainwp-sitestable-getcolumns', array( $this, 'sitestable_getcolumns' ), 10 );
            add_filter( 'mainwp-sitestable-item', array( $this, 'sitestable_item' ), 10 );   
       
            add_action( 'in_admin_header', array( &$this, 'in_admin_head' ) ); // Adds Help Tab in admin header
            add_filter( 'mainwp-getsubpages-sites', array( &$this, 'managesites_subpage' ), 10, 1 );            
            add_action( 'mainwp_delete_site', array( &$this, 'on_delete_site' ), 10, 1 );            
            add_filter( 'mainwp_widgetupdates_actions_top', array( &$this, 'hook_widgetupdates_actions_top' ), 10, 1 );            
                     
            add_filter( 'mainwp-sync-others-data', array( $this, 'sync_others_data' ), 10, 2 );
        	add_action( 'mainwp-site-synced', array( $this, 'synced_site' ), 10, 2 );

            MainWP_Staging::instance();            
            
            MainWP_Staging_DB::instance()->install();		            
	}

    public function admin_init() {
            wp_enqueue_style( 'mainwp-staging-extension', MAINWP_STAGING_PLUGIN_URL . '/css/mainwp-staging.css' );
            if ( isset( $_REQUEST['page'] ) && ( 'Extensions-Mainwp-Staging-Extension' == $_REQUEST['page'] || 'ManageSitesStaging' == $_REQUEST['page'] )) {                    
                wp_enqueue_script( 'mainwp-staging-extension', MAINWP_STAGING_PLUGIN_URL . '/js/mainwp-staging.js', array(), '1.1' );
            }
            
            wp_localize_script(
                    'mainwp-staging-extension', 'mainwp_staging_loc', array(			
                        'nonce' => wp_create_nonce( '_wpnonce_staging' )
                    )
            );       
            
            // create staging group if needed
            if (false === get_option('mainwp_stagingsites_group_id', false)) {
                global $mainwp_StagingExtensionActivator;                
                $staging_group_id = apply_filters( 'mainwp_addgroup', $mainwp_StagingExtensionActivator->get_child_file(), $mainwp_StagingExtensionActivator->get_child_key(), $group = 'Staging Sites' );                
                if ($staging_group_id) {
                    self::update_option('mainwp_stagingsites_group_id', intval($staging_group_id), 'yes');
                }
            }
            
            if ( isset( $_POST['select_mainwp_staging_options_siteview'] ) ) {
                global $current_user; 
                update_user_option($current_user->ID, 'mainwp_staging_options_updates_view', $_POST['select_mainwp_staging_options_siteview']);
            }
        
            if (isset( $_GET['page'] ) && 'managesites' == $_GET['page'] && !isset( $_GET['id'] ) && !isset( $_GET['do']) && !isset( $_GET['dashboard'])) {		
                if ($this->staging_sites_info === null) {
                    $this->staging_sites_info = MainWP_Staging_DB::instance()->get_count_stagings_of_sites();
                } 
            }
    }

    public static function update_option( $option_name, $option_value, $autoload = 'no' ) {
		$success = add_option( $option_name, $option_value, '', $autoload );

		if ( ! $success ) {
			$success = update_option( $option_name, $option_value );
		}

		return $success;
	}
    
    public function localization() {
        load_plugin_textdomain( 'mainwp-staging-extension', false,  dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    }

    function managesites_subpage( $subPage ) {
        $subPage[] = array(
            'title'            => __( 'WP Staging','mainwp-staging-extension' ),
            'slug'             => 'Staging',
            'sitetab'          => true,
            'menu_hidden'      => true,
            'callback'         => array( 'MainWP_Staging', 'render' ),
            'on_load_callback' => array('MainWP_Staging', 'on_load_individual_settings_page')
        );
        return $subPage;
    }     
    
    public function sitestable_getcolumns( $columns ) {
    $columns['wp_staging'] = __( 'Staging', 'mainwp-staging-extension' );
        return $columns;
	}
    
    public function sitestable_item( $item ) {
         // do not add links for staging sites
        if ($item['is_staging'])
            return $item;
            
        $site_id = $item['id'];     
        if ($this->staging_sites_info && isset($this->staging_sites_info[$site_id]) && $this->staging_sites_info[$site_id] > 0) {
            $item['wp_staging'] = '<a href="admin.php?page=ManageSitesStaging&tab=stagings&id=' . $site_id . '">' . __('Manage', 'mainwp-staging-extension') . '</a>';
        } else {
            $item['wp_staging'] = '<a href="admin.php?page=ManageSitesStaging&tab=new&id=' . $site_id . '">' . __('Create', 'mainwp-staging-extension') . '</a>';
        }                            

        return $item;
	}
    
    function hook_widgetupdates_actions_top( $input ) {
        // added the selection
        if ($input != '')
            return $input;  
        
        $view_stagings = get_user_option('mainwp_staging_options_updates_view') == 'staging' ? true : false;                
        $input .= '<div class="mainwp-cols-s mainwp-left mainwp-t-align-left">
                    <form method="post" action="">
                        <label for="mainwp_staging_select_options_siteview">' . __( 'View updates in: ', 'mainwp' ) . '</label>
                        <select class="mainwp-select2-mini" id="mainwp_staging_select_options_siteview" name="select_mainwp_staging_options_siteview">
                            <option value="live" ' . ( $view_stagings ? '' : 'selected' ) . '>' .  esc_html__( 'Live Sites', 'mainwp')  . '</option>
                            <option value="staging" ' . ( $view_stagings ? 'selected' : '' ) . '>' .  esc_html__( 'Staging Sites', 'mainwp' ) . '</option>                            
                        </select>
                    </form>
                </div>
                <script type="text/javascript">			
                    jQuery(document).ready(function ($) {
                        jQuery("#mainwp_staging_select_options_siteview").change(function() {
                            jQuery(this).closest("form").submit();
                        });
                    })
            	</script>';

        return $input;
    }
        
    public function sync_others_data( $data, $pWebsite = null ) {
        // not sync staging with staging site
        if ($pWebsite->is_staging) 
            return;
		if ( ! is_array( $data ) ) {
			$data = array(); }
		$data['syncWPStaging'] = 1;
		return $data;
	}
    
	public function synced_site( $pWebsite, $information = array() ) {
        // do not sync staging site
        if ($pWebsite->is_staging) 
            return;
        
		if ( is_array( $information ) && isset( $information['syncWPStaging'] ) ) {            
			$data = $information['syncWPStaging'];            
            
			if ( is_array( $data ) && isset($data['availableClones']) ) {   
                $available_clones = $data['availableClones'];
                unset($data['availableClones']);
                
                if (!is_array($available_clones))
                    $available_clones = array();                
                
                $site_id = $pWebsite->id;                
                MainWP_Staging::instance()->sync_staging_site_data( $site_id, $available_clones);
			}
			unset( $information['syncWPStaging'] );
		}
	}
    
           
    public function on_delete_site( $website ) {
        if ( $website ) {
            if ($website->is_staging == 0) {
                MainWP_Staging_DB::instance()->delete_settings_by( 'site_id', $website->id );
                MainWP_Staging_DB::instance()->delete_staging_site( $website->id );
            } else { // the site is staging site                
                MainWP_Staging_DB::instance()->delete_staging_site( false, false, $website->id );
            }
        }
	}        
    /**
 * This function check if current page is Sstaging Extension page.
	 * @return void
	 */
	function in_admin_head() {
		if ( isset( $_GET['page'] ) && $_GET['page'] == 'Extensions-Mainwp-Staging-Extension' ) {
			self::addHelpTabs(); // If page is Staging Extension then call this 'addHelpTabs' function
		}
	}

	/**
	 * This function add help tabs in header.
	 * @return void
	 */
	public static function addHelpTabs() {
		$screen = get_current_screen(); //This function returns an object that includes the screen's ID, base, post type, and taxonomy, among other data points.
		$i      = 1;

		$screen->add_help_tab( array(
			'id'      => 'mainwp_staging_helptabs_' . $i ++,
			'title'   => __( 'First Steps with Extensions', 'mainwp-staging-extension' ),
			'content' => self::getHelpContent( 1 ),
		) );
		$screen->add_help_tab( array(
			'id'      => 'mainwp_staging_helptabs_' . $i ++,
			'title'   => __( 'MainWP Staging Extension', 'mainwp-staging-extension' ),
			'content' => self::getHelpContent( 2 ),
		) );
	}

	/**
	 * Get help tab content.
	 *
	 * @param int $tabId
	 *
	 * @return string|bool
	 */
	public static function getHelpContent( $tabId ) {
		ob_start();
		if ( 1 == $tabId ) {
			?>
			<h3><?php echo __( 'First Steps with Extensions', 'mainwp-staging-extension' ); ?></h3>
			<p><?php echo __( 'If you are having issues with getting started with the MainWP extensions, please review following help documents', 'mainwp-staging-extension' ); ?></p>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'What are the MainWP Extensions', 'mainwp-staging-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/order-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Order Extension(s)', 'mainwp-staging-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/my-downloads-and-api-keys/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'My Downloads and API Keys', 'mainwp-staging-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/install-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Install Extension(s)', 'mainwp-staging-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/activate-extensions-api/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Activate Extension(s) API', 'mainwp-staging-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/updating-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Updating Extension(s)', 'mainwp-staging-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/remove-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Remove Extension(s)', 'mainwp-staging-extension' ); ?></a><br/><br/>
			<a href="https://mainwp.com/help/category/mainwp-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Help Documenation for all MainWP Extensions', 'mainwp-staging-extension' ); ?></a>
		<?php } else if ( 2 == $tabId ) { ?>
			<h3><?php echo 'MainWP Staging Extension'; ?></h3>
			<a href="https://mainwp.com/help/docs/staging-extension/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'MainWP Staging Extension', 'mainwp-staging-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/install-and-set-the-mainwp-staging-extension/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Install and Set the MainWP Staging Extension', 'mainwp-staging-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/create-staging-site/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Create Staging Site', 'mainwp-staging-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/update-a-staging-site/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Update a Staging Site', 'mainwp-staging-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/run-updates-on-staging-sites/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Run Updates on Staging Sites', 'mainwp-staging-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/delete-a-staging-site/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Delete a Staging Site', 'mainwp-staging-extension' ); ?></a><br/>
			<?php }
		$output = ob_get_clean();

		return $output;
	}
        
}


function mainwp_staging_extension_autoload( $class_name ) {
        $autoload_types = array( 'class' );

        foreach ( $autoload_types as $type ) {
                $autoload_dir  = \trailingslashit( dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $type );

                $autoload_path = sprintf( '%s%s-%s.php', $autoload_dir, $type, strtolower( str_replace( '_', '-', $class_name ) ) );

                if ( file_exists( $autoload_path ) ) {
                        require_once( $autoload_path );
                }
        }
}

if ( function_exists( 'spl_autoload_register' ) ) {
    spl_autoload_register( 'mainwp_staging_extension_autoload' );
} else {
    function __autoload( $class_name ) {
        mainwp_staging_extension_autoload( $class_name );
    }
}

register_activation_hook( __FILE__, 'wp_staging_extension_activate' );
register_deactivation_hook( __FILE__, 'wp_staging_extension_deactivate' );

function wp_staging_extension_activate() {
	update_option( 'mainwp_staging_extension_activated', 'yes' );
	$extensionActivator = new MainWP_Staging_Extension_Activator();
	$extensionActivator->activate();	
}

function wp_staging_extension_deactivate() {
	$extensionActivator = new MainWP_Staging_Extension_Activator();
	$extensionActivator->deactivate();
}

class MainWP_Staging_Extension_Activator
{
	protected $mainwpMainActivated = false;
	protected $childEnabled = false;
	protected $childKey = false;
	protected $childFile;
	protected $plugin_handle = 'mainwp-staging-extension';
	protected $product_id = 'MainWP Staging Extension';
	protected $software_version = '1.1';

	public function __construct() {

		$this->childFile = __FILE__;
		add_filter( 'mainwp-getextensions', array( &$this, 'get_this_extension' ) );
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', false );

		if ( $this->mainwpMainActivated !== false ) {
			$this->activate_this_plugin();
		} else {
			add_action( 'mainwp-activated', array( &$this, 'activate_this_plugin' ) );
		}
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_action( 'admin_notices', array( &$this, 'mainwp_error_notice' ) );
	}

	function admin_init() {
            if ( get_option( 'mainwp_staging_extension_activated' ) == 'yes' ) {
                delete_option( 'mainwp_staging_extension_activated' );
                wp_redirect( admin_url( 'admin.php?page=Extensions' ) );
                return;
            }            
            MainWP_Staging::instance()->admin_init();   
            MainWP_Staging_Plugin::get_instance()->admin_init();
	}

	function get_this_extension( $pArray ) {
		$pArray[] = array( 
                    'plugin'           => __FILE__,
                    'api'              => $this->plugin_handle,
                    'mainwp'           => true,
                    'callback'         => array( &$this, 'settings' ),
                    'apiManager'       => true,
                    'on_load_callback' => array('MainWP_Staging', 'on_load_settings_page')
                );
		return $pArray;
	}
        
	function settings() {
		do_action( 'mainwp-pageheader-extensions', __FILE__ );
		if ( $this->childEnabled ) {			
            MainWP_Staging::instance()->render();		
		} 
		do_action( 'mainwp-pagefooter-extensions', __FILE__ );
	}

	function activate_this_plugin() {                
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', $this->mainwpMainActivated );
		$this->childEnabled = apply_filters( 'mainwp-extension-enabled-check', __FILE__ );
		$this->childKey = $this->childEnabled['key'];
		if ( function_exists( 'mainwp_current_user_can' )&& ! mainwp_current_user_can( 'extension', 'mainwp-staging-extension' ) ) {
            return; 			
		}
		new MainWP_Staging_Extension();
	}

	public function get_child_key() {
		return $this->childKey;
	}

	public function get_child_file() {
		return $this->childFile;
	}

	function mainwp_error_notice() {
            global $current_screen;
            if ( $current_screen->parent_base == 'plugins' && $this->mainwpMainActivated == false ) {
                    echo '<div class="error"><p>MainWP Staging Extension ' . __( 'requires <a href="https://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> to be activated in order to work. Please install and activate <a href="https://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> first.' ) . '</p></div>';
            }
	}
       
	public function activate() {
		$options = array(
                        'product_id'       => $this->product_id,
                        'activated_key'    => 'Deactivated',
                        'instance_id'      => apply_filters( 'mainwp-extensions-apigeneratepassword', 12, false ),
                        'software_version' => $this->software_version,
                );
		update_option( $this->plugin_handle . '_APIManAdder', $options );
	}

	public function deactivate() {
		update_option( $this->plugin_handle . '_APIManAdder', '' );
	}
}

global $mainwp_StagingExtensionActivator;
$mainwp_StagingExtensionActivator = new MainWP_Staging_Extension_Activator();
