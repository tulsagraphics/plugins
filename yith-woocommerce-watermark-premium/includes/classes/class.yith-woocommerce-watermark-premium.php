<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
/**
 * Implements premium features of YIT WooCommerce Watermark plugin
 *
 * @class   YITH_WC_Watermark
 * @package YITHEMES
 * @since   1.0.0
 * @author  Your Inspiration Themes
 */
if( !class_exists( 'YITH_WC_Watermark_Premium' ) ) {

    class YITH_WC_Watermark_Premium extends YITH_WC_Watermark
    {

        /**
         * @var YITH_WC_Watermark_Premium single instance of class
         */
        protected static $_instance;


        public function __construct()
        {

            parent::__construct();

            add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
            add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );
            //add premium tabs
            add_filter( 'ywcwat_add_premium_tab', array( $this, 'add_premium_tab' ) );


            // add_action( 'wp_ajax_nopriv_add_new_watermark_admin', array( $this, 'add_new_watermark_admin' ) );
            // add_action( 'wp_ajax_nopriv_add_new_product_watermark_admin', array( $this, 'add_new_product_watermark_admin' ) );

            //add metaboxes in edit product
            add_filter( 'product_type_options', array( $this, 'add_product_watermark_option' ) );
            add_filter( 'woocommerce_product_write_panel_tabs', array( $this, 'print_watermark_panels' ), 98 );
            add_action( 'woocommerce_process_product_meta', array( $this, 'save_product_watermark_meta' ), 20, 2 );

            add_filter( 'ywcwat_max_item_task', array( $this, 'remove_max_item_task' ) );

            if( is_admin() ) {
                //include admin style and script
                $this->include_premium_file();
                add_action( 'woocommerce_admin_field_watermark-insert-new', 'YITH_Watermark_Insert_New::output' );
                add_action( 'woocommerce_admin_field_watermark-apply', 'YITH_Watermark_Apply::output' );
                //  add_action( 'admin_enqueue_scripts', array( $this, 'include_premium_style_script' ) );
                add_filter( 'ywcwat_admin_panel_script_parameters', array( $this, 'add_premium_parameters' ), 10, 1 );
                add_filter( 'ywcwat_product_admin_script_parameters', array( $this, 'add_premium_parameters' ), 10, 1 );

                /** === AJAX ACTIONS  === */


                //add ajax action for add new watermark (in single product )
                add_action( 'wp_ajax_add_new_product_watermark_admin', array( $this, 'add_new_product_watermark_admin' ) );

                //add ajax action for load admin template
                add_action( 'wp_ajax_add_new_watermark_admin', array( $this, 'add_new_watermark_admin' ) );

                //remove single watermark field
                add_action( 'wp_ajax_remove_watermark', array( $this, 'remove_watermark_admin' ) );

                //preview watermark
                add_action( 'wp_ajax_preview_watermark', array( $this, 'preview_watermark' ) );


                /** === END AJAX ACTIONS */

                add_filter( 'ywcwat_build_watermark_repeat_image', array( $this, 'repeat_watermark_image' ), 15, 3 );

                //adjust watermark position
                add_filter( 'ywcwat_watermark_position', array( $this, 'get_watermark_position' ), 10, 6 );

                //apply text watermark
                add_action( 'ywcwat_build_watermark_text', array( $this, 'build_watermark_text' ), 10, 2 );

                add_filter( 'ywcwat_product_attach_ids', array( $this, 'add_product_attach_id' ), 10, 2 );

            }
        }

        /** return single instance of class
         * @author YITHEMES
         * @since 1.0.0
         * @return YITH_WC_Watermark_Premium
         */
        public static function get_instance()
        {

            if( is_null( self::$_instance ) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        /** Register plugins for activation tab
         * @return void
         * @since    1.0.0
         * @author   Andrea Grillo <andrea.grillo@yithemes.com>
         */
        public function register_plugin_for_activation()
        {
            if( !class_exists( 'YIT_Plugin_Licence' ) ) {
                require_once YWCWAT_DIR . 'plugin-fw/licence/lib/yit-licence.php';
                require_once YWCWAT_DIR . 'plugin-fw/licence/lib/yit-plugin-licence.php';
            }
            YIT_Plugin_Licence()->register( YWCWAT_INIT, YWCWAT_SECRET_KEY, YWCWAT_SLUG );
        }

        /**
         * Register plugins for update tab
         *
         * @return void
         * @since    1.0.0
         * @author   Andrea Grillo <andrea.grillo@yithemes.com>
         */
        public function register_plugin_for_updates()
        {
            if( !class_exists( 'YIT_Upgrade' ) ) {
                require_once( YWCWAT_DIR . 'plugin-fw/lib/yit-upgrade.php' );
            }
            YIT_Upgrade()->register( YWCWAT_SLUG, YWCWAT_INIT );
        }

        /** add premium tab
         * @author YITHEMES
         * @since 1.0.0
         * @param $tabs
         * @return mixed
         */
        public function add_premium_tab( $tabs )
        {

            unset( $tabs['premium-landing'] );

            $tabs['watermark-list'] = __( 'Active Watermark', 'yith-woocommerce-watermark' );
            return $tabs;
        }

        /** include custom admin fields
         * @author YITHEMES
         * @since 1.0.0
         */
        public function include_premium_file()
        {

            include_once( YWCWAT_TEMPLATE_PATH . '/admin/watermark-insert-new.php' );
            include_once( YWCWAT_TEMPLATE_PATH . '/admin/watermark-apply.php' );
        }

        /** remove limit item for task
         * @author YITHEMES
         * @since 1.0.0
         * @param $max_item_task
         * @return string
         */
        public function remove_max_item_task( $max_item_task )
        {
            return '';
        }


        public function add_premium_parameters( $params )
        {

            if( isset( $params['perc_w'] ) ) {
                unset( $params['perc_w'] );
            }

            if( isset( $params['perc_h'] ) ) {
                unset( $params['perc_h'] );
            }

            $params['label_position'] = array(
                'top_left' => __( 'TOP LEFT', 'yith-woocommerce-watermark' ),
                'top_center' => __( 'TOP CENTER', 'yith-woocommerce-watermark' ),
                'top_right' => __( 'TOP RIGHT', 'yith-woocommerce-watermark' ),
                'middle_left' => __( 'LEFT CENTER', 'yith-woocommerce-watermark' ),
                'middle_center' => __( 'CENTER', 'yith-woocommerce-watermark' ),
                'middle_right' => __( 'RIGHT CENTER', 'yith-woocommerce-watermark' ),
                'bottom_left' => __( 'BOTTOM LEFT', 'yith-woocommerce-watermark' ),
                'bottom_center' => __( 'BOTTOM CENTER', 'yith-woocommerce-watermark' ),
                'bottom_right' => __( 'BOTTOM RIGHT', 'yith-woocommerce-watermark' )
            );

            $params['delete_single_watermark'] = array(
                'confirm_delete_watermark' => __( 'Do you want to delete this watermark ?', 'yith-woocommerce-watermark' )
            );

            $params['gd_version'] = $this->get_gd_version();
            $params['block_loader'] = YWCWAT_ASSETS_URL . '/images/block-loader.gif';

            $current_filter = current_filter();

            if( 'ywcwat_admin_panel_script_parameters' === $current_filter ) {

                $params['actions']['remove_watermark'] = 'remove_watermark';
                $params['actions']['preview_watermark'] = 'preview_watermark';
            }
            else {
                $params['actions']['remove_watermark'] = 'remove_product_watermark';
                $params['actions']['preview_watermark'] = 'preview_watermark';
            }

            return $params;
        }

        /** include style and script
         * @author YITHEMES
         * @since 1.0.0
         */
        public function include_premium_style_script()
        {


            if( isset( $_GET['page'] ) && 'yith_ywcwat_panel' == $_GET['page'] ) {

                wp_enqueue_script( 'ywcwat_premium_admin_script', YWCWAT_ASSETS_URL . 'js/ywcwat_premium_admin' . $this->_suffix . '.js', array( 'jquery', 'wp-color-picker' ), YWCWAT_VERSION, true );
                wp_enqueue_script( 'ywcwat_premium_enhanceselect', YWCWAT_ASSETS_URL . 'js/ywcwat_enhancedcselect' . $this->_suffix . '.js', array( 'jquery', 'select2' ), YWCWAT_VERSION, true );

                $ywcwat_localize_script = array(
                    'i18n_matches_1' => _x( 'One result is available, press enter to select it.', 'enhanced select', 'woocommerce' ),
                    'i18n_matches_n' => _x( '%qty% results are available, use up and down arrow keys to navigate.', 'enhanced select', 'woocommerce' ),
                    'i18n_no_matches' => _x( 'No matches found', 'enhanced select', 'woocommerce' ),
                    'i18n_ajax_error' => _x( 'Loading failed', 'enhanced select', 'woocommerce' ),
                    'i18n_input_too_short_1' => _x( 'Please enter 1 or more characters', 'enhanced select', 'woocommerce' ),
                    'i18n_input_too_short_n' => _x( 'Please enter %qty% or more characters', 'enhanced select', 'woocommerce' ),
                    'i18n_input_too_long_1' => _x( 'Please delete 1 character', 'enhanced select', 'woocommerce' ),
                    'i18n_input_too_long_n' => _x( 'Please delete %qty% characters', 'enhanced select', 'woocommerce' ),
                    'i18n_selection_too_long_1' => _x( 'You can only select 1 item', 'enhanced select', 'woocommerce' ),
                    'i18n_selection_too_long_n' => _x( 'You can only select %qty% items', 'enhanced select', 'woocommerce' ),
                    'i18n_load_more' => _x( 'Loading more results&hellip;', 'enhanced select', 'woocommerce' ),
                    'i18n_searching' => _x( 'Searching&hellip;', 'enhanced select', 'woocommerce' ),
                    'ajax_url' => admin_url( 'admin-ajax.php', is_ssl() ? 'https' : 'http' ),
                    'search_categories_nonce' => wp_create_nonce( YWCWAT_SLUG . '_search-categories' ),
                    'plugin_nonce' => '' . YWCWAT_SLUG . '',


                );
                $shop_single = wc_get_image_size( 'shop_single' );
                $shop_catalog = wc_get_image_size( 'shop_catalog' );
                $shop_thumbnail = wc_get_image_size( 'shop_thumbnail' );
                $ywcwat_label_position = array(
                    'label_position' => array(
                        'top_left' => __( 'TOP LEFT', 'yith-woocommerce-watermark' ),
                        'top_center' => __( 'TOP CENTER', 'yith-woocommerce-watermark' ),
                        'top_right' => __( 'TOP RIGHT', 'yith-woocommerce-watermark' ),
                        'middle_left' => __( 'LEFT CENTER', 'yith-woocommerce-watermark' ),
                        'middle_center' => __( 'CENTER', 'yith-woocommerce-watermark' ),
                        'middle_right' => __( 'RIGHT CENTER', 'yith-woocommerce-watermark' ),
                        'bottom_left' => __( 'BOTTOM LEFT', 'yith-woocommerce-watermark' ),
                        'bottom_center' => __( 'BOTTOM CENTER', 'yith-woocommerce-watermark' ),
                        'bottom_right' => __( 'BOTTOM RIGHT', 'yith-woocommerce-watermark' )
                    ),
                    'delete_single_watermark' => array(
                        'confirm_delete_watermark' => __( 'Do you want to delete this watermark ?', 'yith-woocommerce-watermark' )
                    ),
                    'messages' => array(
                        'singular_success_image' => __( 'Image has been created', 'yith-woocommerce-watermark' ),
                        'plural_success_image' => __( 'Images have been created', 'yith-woocommerce-watermark' ),
                        'singular_error_image' => __( 'Image has not been created', 'yith-woocommerce-watermark' ),
                        'plural_error_image' => __( 'Images have not been created', 'yith-woocommerce-watermark' ),
                    ),
                    'attach_id' => $this->get_watermark_ids(),
                    'shop_single' => $shop_single,
                    'shop_catalog' => $shop_catalog,
                    'shop_thumbnail' => $shop_thumbnail,
                    'gd_version' => $this->get_gd_version()
                );

                wp_localize_script( 'ywcwat_premium_admin_script', 'ywcwat_premium', $ywcwat_label_position );
                wp_localize_script( 'ywcwat_premium_enhanceselect', 'ywcwat_enhanceselect', $ywcwat_localize_script );

                wp_enqueue_script( 'wc-enhanced-select' );
                wp_enqueue_style( 'ywcwat_load_font', YWCWAT_ASSETS_URL . '/fonts/ywcwat_load_fonts.css', array(), YWCWAT_VERSION );
            }


            global $post;
            if( isset( $post ) && ( 'product' == $post->post_type || 'product_variation' == $post->post_type ) ) {
                $shop_single = wc_get_image_size( 'shop_single' );
                $shop_catalog = wc_get_image_size( 'shop_catalog' );
                $shop_thumbnail = wc_get_image_size( 'shop_thumbnail' );
                $ywcwat_label_position = array(
                    'label_position' => array(
                        'top_left' => __( 'TOP LEFT', 'yith-woocommerce-watermark' ),
                        'top_center' => __( 'TOP CENTER', 'yith-woocommerce-watermark' ),
                        'top_right' => __( 'TOP RIGHT', 'yith-woocommerce-watermark' ),
                        'middle_left' => __( 'LEFT CENTER', 'yith-woocommerce-watermark' ),
                        'middle_center' => __( 'CENTER', 'yith-woocommerce-watermark' ),
                        'middle_right' => __( 'RIGHT CENTER', 'yith-woocommerce-watermark' ),
                        'bottom_left' => __( 'BOTTOM LEFT', 'yith-woocommerce-watermark' ),
                        'bottom_center' => __( 'BOTTOM CENTER', 'yith-woocommerce-watermark' ),
                        'bottom_right' => __( 'BOTTOM RIGHT', 'yith-woocommerce-watermark' )
                    ),
                    'delete_single_watermark' => array(
                        'confirm_delete_watermark' => __( 'Do you want to delete this watermark ?', 'yith-woocommerce-watermark' )
                    ),
                    'messages' => array(
                        'singular_success_image' => __( 'Image has been created', 'yith-woocommerce-watermark' ),
                        'plural_success_image' => __( 'Images have been created', 'yith-woocommerce-watermark' ),
                        'singular_error_image' => __( 'Image has not been created', 'yith-woocommerce-watermark' ),
                        'plural_error_image' => __( 'Images have not been created', 'yith-woocommerce-watermark' ),
                    ),
                    'attach_id' => $this->get_watermark_ids(),
                    'shop_single' => $shop_single,
                    'shop_catalog' => $shop_catalog,
                    'shop_thumbnail' => $shop_thumbnail,
                    'gd_version' => $this->get_gd_version()
                );


                wp_enqueue_style( 'ywcwat_free_admin_style', YWCWAT_ASSETS_URL . 'css/ywcwat_admin.css', array(), YWCWAT_VERSION );
                wp_enqueue_script( 'ywcwat_premium_product_admin_script', YWCWAT_ASSETS_URL . 'js/ywcwat_admin_single_product' . $this->_suffix . '.js', array( 'jquery', 'wp-color-picker' ), YWCWAT_VERSION, true );
                wp_localize_script( 'ywcwat_premium_product_admin_script', 'ywcwat_premium', $ywcwat_label_position );
            }
        }

        /** return new watermark metabox in admin
         * @author YITHEMES
         * @since 1.0.0
         */
        public function add_new_watermark_admin()
        {

            if( isset( $_REQUEST['ywcwat_addnewwat'] ) && isset( $_REQUEST['ywcwat_unique_id'] ) ) {

                $params = array(
                    'option_id' => 'ywcwat_watermark_select',
                    'current_row' => $_REQUEST['ywcwat_addnewwat'],
                    'unique_id' => $_REQUEST['ywcwat_unique_id'],

                );

                $params['params'] = $params;
                ob_start();
                wc_get_template( 'single-watermark-template.php', $params, YWCWAT_TEMPLATE_PATH, YWCWAT_TEMPLATE_PATH );
                $template = ob_get_contents();
                ob_end_clean();
                wp_send_json( array( 'result' => $template ) );
            }
        }


        /**
         * delete a single watermark field
         * @author Salvatore Strano
         * @since 1.1.0
         */

        public function remove_watermark_admin()
        {

            $update = false;
            if( isset( $_REQUEST['ywcwat_unique_id'] ) ) {

                $all_watermarks = get_option( 'ywcwat_watermark_select', array() );
                $watermark_to_delete = $_REQUEST['ywcwat_unique_id'];


                foreach ( $all_watermarks as $key => $watermark ) {

                    if( $watermark['ywcwat_id'] == $watermark_to_delete ) {
                        unset( $all_watermarks[$key] );
                        break;
                    }
                }

                $update = update_option( 'ywcwat_watermark_select', $all_watermarks );


            }

            wp_send_json( array( 'result' => $update ) );
        }

        /** return new watermark metabox in edit product
         * @author YITHEMES
         * @since 1.0.0
         */
        public function add_new_product_watermark_admin()
        {

            if( isset( $_REQUEST['ywcwat_product_addnewwat'] ) ) {

                $optionid = $_REQUEST['ywcwat_product_option_id'];
                $current_row = $_REQUEST['ywcwat_product_addnewwat'];

                $params = array(
                    'option_id' => $optionid,
                    'current_row' => $current_row
                );

                $params['params'] = $params;
                ob_start();
                wc_get_template( 'metaboxes/single-product-watermark-template.php', $params, YWCWAT_TEMPLATE_PATH, YWCWAT_TEMPLATE_PATH );
                $template = ob_get_contents();
                ob_end_clean();

                wp_send_json( array( 'result' => $template ) );

            }
        }

        /** return attachment ids
         * @author YITHEMES
         * @since 1.0.0
         * @return mixed|string|void
         */
        public function get_ids_attach()
        {

            $attach_ids = ywcwat_get_all_product_attach();

            $ids = array();

            foreach ( $attach_ids as $attach_id ) {
                $ids[] = $attach_id->ID;
            }

            $gallery_ids = ywcwat_get_all_product_img_gallery();

            foreach ( $gallery_ids as $gallery_id ) {

                $attach_ids = explode( ',', $gallery_id->ID );


                foreach ( $attach_ids as $attach_id ) {


                    if( !in_array( $attach_id, $ids ) ) {
                        $ids[] = $attach_id;
                    }
                }
            }


            return json_encode( $ids );

        }


        /**
         * add the repeat feature in type image
         * @author Strano Salvatore
         * @since 1.1.0
         * @param $original_image
         * @param $overlay
         */
        public function repeat_watermark_image( $original_image, $watermark )
        {

            $ww = imagesx( $watermark );
            $hh = imagesy( $watermark );
            $w = imagesx( $original_image );
            $h = imagesy( $original_image );

            $cur_w = 0;

            while ( $cur_w<$w ) {

                $cur_h = 0;
                while ( $cur_h<$h ) {
                    imagecopyresampled( $original_image, $watermark, $cur_w, $cur_h, 0, 0, $ww, $hh, $ww, $hh );
                    $cur_h += $hh;
                }
                $cur_w += $ww;
            }
        }


        /**
         * return watermark position
         * @author Strano Salvatore
         * @since 1.1.0
         *
         * @param array $watermark_position
         * @param float $image_width
         * @param float $image_height
         * @param float $overlay_width
         * @param float $overlay_height
         * @param array $watermark
         * @return array
         */
        public function get_watermark_position( $watermark_position, $image_width, $image_height, $overlay_width, $overlay_height, $watermark )
        {

            $position = $watermark['ywcwat_watermark_position'];
            $margin_x_watermark = $watermark['ywcwat_watermark_margin_x'];
            $margin_y_watermark = $watermark['ywcwat_watermark_margin_y'];
            switch ( $position ) {

                case 'top_left':
                    $watermark_start_x = $margin_x_watermark;
                    $watermark_start_y = $margin_y_watermark;
                    break;
                case 'top_center':
                    $watermark_start_x = ( $image_width / 2 )-( $overlay_width / 2 )+$margin_x_watermark;
                    $watermark_start_y = $margin_y_watermark;
                    break;
                case 'top_right':
                    $watermark_start_x = $image_width-$overlay_width+$margin_x_watermark;
                    $watermark_start_y = $margin_y_watermark;
                    break;
                case 'middle_left':
                    $watermark_start_x = $margin_x_watermark;
                    $watermark_start_y = ( $image_height / 2 )-( $overlay_height / 2 )+$margin_y_watermark;
                    break;
                case 'middle_center':
                    $watermark_start_x = ( $image_width / 2 )-( $overlay_width / 2 )+$margin_x_watermark;
                    $watermark_start_y = ( $image_height / 2 )-( $overlay_height / 2 )+$margin_y_watermark;
                    break;
                case 'middle_right':
                    $watermark_start_x = $image_width-$overlay_width+$margin_x_watermark;
                    $watermark_start_y = ( $image_height / 2 )-( $overlay_height / 2 )+$margin_y_watermark;
                    break;
                case 'bottom_left':
                    $watermark_start_x = $margin_x_watermark;
                    $watermark_start_y = $image_height-$overlay_height-$margin_y_watermark;
                    break;
                case 'bottom_center':
                    $watermark_start_x = ( $image_width / 2 )-( $overlay_width / 2 )+$margin_x_watermark;
                    $watermark_start_y = $image_height-$overlay_height-$margin_y_watermark;
                    break;

                default:
                    /*position button right*/
                    $watermark_start_x = $image_width-$overlay_width-$margin_x_watermark;
                    $watermark_start_y = $image_height-$overlay_height-$margin_y_watermark;
                    break;

            }

            return array( $watermark_start_x, $watermark_start_y );
        }


        /**
         * @param $original_image
         * @param $watermark
         */
        public function build_watermark_text( $original_image, $watermark )
        {

            $image_width = imagesx( $original_image );
            $image_height = imagesy( $original_image );


            $watermark_content = $this->imagecreatefromtext( $original_image, $watermark );
            $watermark_width = imagesx( $watermark_content );
            $watermark_height = imagesy( $watermark_content );

            if( $watermark_width>$image_width ) {

                $coeff_ratio = $image_width / $watermark_width;


                $width = intval( round( $watermark_width * $coeff_ratio ) );
                $height = intval( round( $watermark_height * $coeff_ratio ) );

            }
            else {
                $width = $watermark_width;
                $height = $watermark_height;
            }


            list( $watermark_start_x, $watermark_start_y ) = $this->compute_watermark_position( $image_width, $image_height, $width, $height, $watermark );

            imagesavealpha( $watermark_content, true );
            imagealphablending( $watermark_content, true );

            imagecopyresampled( $original_image, $watermark_content, $watermark_start_x, $watermark_start_y, 0, 0, $width, $height, $watermark_width, $watermark_height );
        }

        /**@author YITHEMES
         * @since 1.0.0
         * @param $watermark
         * @return resource
         */
        public function imagecreatefromtext( $original, $watermark )
        {

            $text = $watermark['ywcwat_watermark_text'];
            $font_name = YWCWAT_DIR . 'assets/fonts/' . $watermark['ywcwat_watermark_font'];

            $width = $watermark['ywcwat_watermark_width'];
            $height = $watermark['ywcwat_watermark_height'];
            $font_color = $watermark['ywcwat_watermark_font_color'];
            $bg_color = $watermark['ywcwat_watermark_bg_color'];
            $bg_opacity = $watermark['ywcwat_watermark_opacity'];

            $font_size = $watermark['ywcwat_watermark_font_size'];

            $width = round( imagesx( $original ) * ( $width / 100 ) );
            $height = round( imagesy( $original ) * ( $height / 100 ) );
            $line_height = $watermark['ywcwat_watermark_line_height'] == -1 ? $height / 2 : $watermark['ywcwat_watermark_line_height'];
            $angle = isset( $watermark['ywcwat_watermark_angle'] ) ? $watermark['ywcwat_watermark_angle'] : 0;
            $text_box_info = $this->calculateTextBox( $text, $font_name, $font_size, $angle );

            $img_only_text = imagecreatetruecolor( $width, $height );
            $bg_color = ywcwat_Hex2RGB( $bg_color );

            $bg_opacity = round( abs( ( ( (int)$bg_opacity-100 ) / 0.78740 ) ) );

            imagefill( $img_only_text, 0, 0, imagecolorallocatealpha( $img_only_text, $bg_color[0], $bg_color[1], $bg_color[2], $bg_opacity ) );

            $font_color = ywcwat_Hex2RGB( $font_color );

            $color = imagecolorallocate( $img_only_text, $font_color[0], $font_color[1], $font_color[2] );
            $this->write_multiline_text( $img_only_text, $font_name, $font_size, $color, $text, $line_height, $angle );

            return $img_only_text;
        }

        private function calculateTextBox( $text, $fontFile, $fontSize, $fontAngle )
        {
            /************
             * simple function that calculates the *exact* bounding box (single pixel precision).
             * The function returns an associative array with these keys:
             * left, top:  coordinates you will pass to imagettftext
             * width, height: dimension of the image you have to create
             *************/
            $rect = imagettfbbox( $fontSize, $fontAngle, $fontFile, $text );
            $minX = min( array( $rect[0], $rect[2], $rect[4], $rect[6] ) );
            $maxX = max( array( $rect[0], $rect[2], $rect[4], $rect[6] ) );
            $minY = min( array( $rect[1], $rect[3], $rect[5], $rect[7] ) );
            $maxY = max( array( $rect[1], $rect[3], $rect[5], $rect[7] ) );


            return array(
                "left" => abs( $minX )-1,
                "top" => abs( $minY )-1,
                "width" => $maxX-$minX,
                "height" => $maxY-$minY,
                "box" => $rect
            );
        }

        /** split text in line
         * @author YITHEMES
         * @since 1.0.0
         * @param $font_size
         * @param $font
         * @param $text
         * @param $max_width
         * @return array
         */
        public function get_multiline_text( $font_size, $font, $text, $max_width, $angle )
        {
            $words = explode( " ", $text );
            $lines = array( $words[0] );
            $current_line = 0;
            for ( $i = 1; $i<count( $words ); $i++ ) {

                $dimension = $this->calculateTextBox( $lines[$current_line] . " " . $words[$i], $font, $font_size, $angle );

                $string_lenght = $dimension['width'];

                if( $string_lenght<$max_width ) {

                    $lines[$current_line] .= ' ' . $words[$i];
                }
                else {
                    $current_line++;
                    $lines[$current_line] = $words[$i];
                }
            }

            return $lines;
        }

        /** print single line in image
         * @author YITHEMES
         * @since 1.0.0
         * @param $image
         * @param $font
         * @param $font_size
         * @param $color
         * @param $text
         * @param $start_y
         * @param $line_height
         */
        public function write_multiline_text( $image, $font, $font_size, $color, $text, $line_height, $angle )
        {

            $image_w = imagesx( $image );
            $image_h = imagesy( $image );
            $lines = $this->get_multiline_text( $font_size, $font, $text, $image_w, $angle );
            $tot_line = count( $lines );

            foreach ( $lines as $line ) {

                $dim = $this->calculateTextBox( $line, $font, $font_size, $angle );

                if( $angle == 0 ) {

                    $text_width = $dim['width'];
                    $text_height = $dim['height'];
                    $text_height = $tot_line == 1 ? $text_height : $line_height;
                    $x = ceil( ( $image_w-$text_width ) / 2 );
                    $y = ceil( ( $image_h+$text_height ) / 2 )-( $tot_line-1 ) * $text_height;
                }
                else {

                    $dim = $dim['box'];
                    $x = ( $image_w / 2 )-( $dim[4]-$dim[0] ) / 2;
                    $y = ( ( $image_h / 2 )-( $dim[5]-$dim[1] ) / 2 );

                }
                $tot_line--;
                imagettftext( $image, $font_size, $angle, $x, $y, $color, $font, $line );


            }
        }

        /**@author YITHEMES
         * @since 1.0.0
         * @return array with woocommerce size name
         */
        public function get_woocommerce_size()
        {

            $watermark_size = yith_watermark_get_image_size();

            return array_keys( $watermark_size );
        }

        //single product : manage custom watermark

        /** add checkbox in product data header
         * @author YITHEMES
         * @since 1.0.0
         * @param $type_options
         * @return array
         */
        public function add_product_watermark_option( $type_options )
        {

            $watermark_option = array(
                'enable_watermark' => array(
                    'id' => '_ywcwat_product_enabled_watermark',
                    'wrapper_class' => '',
                    'label' => __( 'Watermark', 'yith-woocommerce-watermark' ),
                    'description' => __( 'Add custom watermark for this product', 'yith-woocommerce-watermark' ),
                    'default' => 'no'
                )
            );

            return array_merge( $type_options, $watermark_option );
        }

        /** print watermark tab in product data
         * @author YITHEMES
         * @since 1.0.0
         */
        public function print_watermark_panels()
        {

            ?>
            <style type="text/css">
                #woocommerce-product-data ul.wc-tabs .ywcwat_watermark_data_tab a:before {
                    content: '\e00c';
                    font-family: 'WooCommerce';
                    padding-right: 5px;

                }

            </style>
            <li class="ywcwat_watermark_data_tab show_if_custom_watermark_enabled">
                <a href="#ywcwat_watermark_data">
                    <?php _e( 'Watermark', 'yith-woocommerce-watermark' ); ?>
                </a>
            </li>


            <?php
            add_action( 'woocommerce_product_data_panels', array( $this, 'write_watermark_panels' ) );

        }

        /**include the watermark tab content
         * @author YITHEMES
         * @since 1.0.0
         */
        public function write_watermark_panels()
        {

            include_once( YWCWAT_TEMPLATE_PATH . 'metaboxes/product_watermark.php' );
        }

        /** save the watermark product meta
         * @author YITHEMES
         * @since 1.0.0
         * @param $post_id
         * @param $post
         */
        public function save_product_watermark_meta( $post_id, $post )
        {

            $product = wc_get_product( $post_id );
            if( isset( $_REQUEST['ywcwat_custom_watermark'] ) ) {

                $custom_watermark = $_REQUEST['ywcwat_custom_watermark'];
                yit_save_prop( $product, '_ywcwat_product_watermark', $custom_watermark );

            }
            else {
                yit_delete_prop( $product, '_ywcwat_product_watermark' );
            }

            if( isset( $_REQUEST['_ywcwat_product_enabled_watermark'] ) ) {

                yit_save_prop( $product, '_enable_watermark', 'yes' );
            }
            else {
                yit_delete_prop( $product, '_enable_watermark', 'no' );
            }


        }

        /**return gd version
         * @author YITHEMES
         * @since 1.0.0
         * @return mixed
         */
        public function get_gd_version()
        {
            $gd_version = gd_info();
            preg_match( '/\d/', $gd_version['GD Version'], $match );
            $gd_ver = $match[0];

            return $gd_ver;
        }

        /**
         * get the preview watermark
         * @author Strano Salvatore
         * @since 1.1.0
         */
        public function preview_watermark()
        {

            $message = __( 'No preview is available for this watermark', 'yith-woocommerce-watermark' );
            $result = false;


            if( isset( $_REQUEST['ywcwat_args'] ) ) {
                $watermark_id = $_REQUEST['ywcwat_id'];
                $watermark_args = array();
                $watermark_arg = ( $_REQUEST['ywcwat_args'] );
                parse_str( $watermark_arg, $watermark_args );

                $context = isset( $_REQUEST['context'] ) ? 'ywcwat_custom_watermark' : 'ywcwat_watermark_select';

                $watermark = isset( $watermark_args[ $context ] ) ? array_shift( $watermark_args[ $context ] ) : false;

                $image_size = $watermark['ywcwat_watermark_sizes'];

                $size = wc_get_image_size( $image_size );

                $preview_width = apply_filters( 'ywcwat_preview_width', !empty( $size['width'] ) ? $size['width'] : 300, $image_size );
                $preview_height = apply_filters( 'ywcwat_preview_height', !empty( $size['height'] ) ? $size['height'] : 300, $image_size );

                $preview_width = $preview_width > 500 ? 500 : $preview_width;
	            $preview_height = $preview_height > 500 ? 500 : $preview_height;


	            if( $watermark ) {
                    $original_image = imagecreatetruecolor( $preview_width, $preview_height );

                    $color = imagecolorallocate( $original_image, 224, 224, 224 );
                    imagefill( $original_image, 0, 0, $color );

                    if( $watermark['ywcwat_watermark_type'] == 'type_text' ) {

                        $this->build_watermark_text( $original_image, $watermark );
                    }
                    else {

                        $this->build_watermark_image( $original_image, $watermark );
                    }

                    $res = $this->generateimagefrom( $original_image, YWCWAT_ASSETS_PATH . '/images/preview.jpg', 'jpg', 100 );

                    if( $res ) {
                        $result = true;
                        $message = YWCWAT_ASSETS_URL . 'images/preview.jpg?now=' . time();
                    }

                    imagedestroy( $original_image );


                }
            }

            wp_send_json( array( 'result' => $result, 'message' => $message ) );
        }

        /**
         * add to attach_ids also the gallery images
         * @author Salvatore Strano
         * @since 1.1.0
         * @param array $attach_ids
         * @param WC_Product $product
         * @return array
         */
        public function add_product_attach_id( $attach_ids, $product ){

            $gallery_ids = $product->get_gallery_image_ids();

            if( count( $gallery_ids ) > 0 ){

                $attach_ids = array_merge( $attach_ids, $gallery_ids );
            }

            return $attach_ids;

        }
    }
}