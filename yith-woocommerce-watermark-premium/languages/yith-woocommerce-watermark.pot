#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: YITH WooCommerce Watermark\n"
"POT-Creation-Date: 2017-09-21 16:37+0200\n"
"PO-Revision-Date: 2016-03-16 16:41+0100\n"
"Last-Translator: \n"
"Language-Team: Yithemes <plugins@yithemes.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;_ngettext_noop:1,2;"
"_n_nooop:1,2;_c,_nc:4c,1,2;_x:1,2c\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-SearchPathExcluded-0: ../plugin-fw\n"

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:139
#: ../plugin-options/watermark-list-options.php:11
msgid "Active Watermark"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:178
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:248
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:289
msgid "TOP LEFT"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:179
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:249
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:290
msgid "TOP CENTER"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:180
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:250
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:291
msgid "TOP RIGHT"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:181
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:251
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:292
msgid "LEFT CENTER"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:182
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:252
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:293
msgid "CENTER"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:183
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:253
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:294
msgid "RIGHT CENTER"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:184
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:254
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:295
msgid "BOTTOM LEFT"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:185
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:255
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:296
msgid "BOTTOM CENTER"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:186
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:256
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:297
msgid "BOTTOM RIGHT"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:190
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:259
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:300
msgid "Do you want to delete this watermark ?"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:225
msgctxt "enhanced select"
msgid "One result is available, press enter to select it."
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:226
msgctxt "enhanced select"
msgid "%qty% results are available, use up and down arrow keys to navigate."
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:227
msgctxt "enhanced select"
msgid "No matches found"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:228
msgctxt "enhanced select"
msgid "Loading failed"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:229
msgctxt "enhanced select"
msgid "Please enter 1 or more characters"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:230
msgctxt "enhanced select"
msgid "Please enter %qty% or more characters"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:231
msgctxt "enhanced select"
msgid "Please delete 1 character"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:232
msgctxt "enhanced select"
msgid "Please delete %qty% characters"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:233
msgctxt "enhanced select"
msgid "You can only select 1 item"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:234
msgctxt "enhanced select"
msgid "You can only select %qty% items"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:235
msgctxt "enhanced select"
msgid "Loading more results&hellip;"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:236
msgctxt "enhanced select"
msgid "Searching&hellip;"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:262
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:303
msgid "Image has been created"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:263
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:304
msgid "Images have been created"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:264
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:305
msgid "Image has not been created"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:265
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:306
msgid "Images have not been created"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:746
#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:774
#: ../includes/classes/class.yith-woocommerce-watermark.php:252
#: ../includes/classes/class.yith-woocommerce-watermark.php:253
msgid "Watermark"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:747
msgid "Add custom watermark for this product"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark-premium.php:847
msgid "No preview is available for this watermark"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:160
#: ../includes/classes/class.yith-woocommerce-watermark.php:245
msgid "Settings"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:162
msgid "Premium live demo"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:162
msgid "Live demo"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:167
#: ../includes/classes/class.yith-woocommerce-watermark.php:246
msgid "Premium Version"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:194
msgid "Plugin Documentation"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:311
msgid "The watermark has been applied to"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:312
msgid "Product"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:313
msgid "on"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:314
msgid "Products"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:315
msgid "Completed"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:316
msgid "You can't use images bigger than"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:319
msgid "of the size of the \"Single Product image\", that is"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:322
msgid "Attach id "
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:323
msgid "Images will be restored, are you sure? "
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:324
msgid "Image has been deleted"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:325
msgid "Images have been deleted"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:326
msgid "Image has not been deleted"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:327
msgid "Images have not been deleted"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:330
#: ../templates/metaboxes/single-product-watermark-template.php:186
#: ../templates/single-watermark-template.php:191
msgid "Shop Single"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:331
#: ../templates/metaboxes/single-product-watermark-template.php:187
#: ../templates/single-watermark-template.php:192
msgid "Shop Thumbnail"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:332
#: ../templates/metaboxes/single-product-watermark-template.php:188
#: ../templates/single-watermark-template.php:193
msgid "Shop Catalog"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:333
msgid "Full Size"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:555
msgid "Watermark Created"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:556
msgid "Empty Path"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:557
msgid "Error when saving resize image"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:558
msgid "Can't load the image editor"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:559
msgid "Error when creating watermark"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:560
msgid "Image size doesn't exist"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:936
msgid "From version 1.0.7 all your product backed up images are available at"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:949
msgid "Backup completed!"
msgstr ""

#: ../includes/classes/class.yith-woocommerce-watermark.php:990
#: ../templates/admin/apply-single-watermark.php:4
msgid "Apply Watermark"
msgstr ""

#: ../init.php:47
msgid ""
"YITH WooCommerce Watermark Premium is enabled but not effective. It requires "
"WooCommerce in order to work."
msgstr ""

#: ../plugin-options/general-settings-options.php:11
msgid "General Settings"
msgstr ""

#: ../plugin-options/general-settings-options.php:27
msgid "Jpeg Quality"
msgstr ""

#: ../plugin-options/watermark-list-options.php:17
msgid "Add New Watermark"
msgstr ""

#: ../templates/admin/custom-button.php:15
msgid "Generate a complete backup"
msgstr ""

#: ../templates/admin/custom-button.php:17
msgid "Backup"
msgstr ""

#: ../templates/admin/watermark-apply.php:16
msgid "Apply all watermarks"
msgstr ""

#: ../templates/admin/watermark-apply.php:18
msgid "Apply All Watermarks"
msgstr ""

#: ../templates/admin/watermark-apply.php:19
msgid "Apply all watermarks created to all your product images"
msgstr ""

#: ../templates/admin/watermark-apply.php:23
#: ../templates/admin/watermark-apply.php:25
msgid "Reset"
msgstr ""

#: ../templates/admin/watermark-apply.php:26
msgid ""
"Delete all product images with watermark (once completed, you have to "
"deactivate the plugin and regenerate image thumbnails)."
msgstr ""

#: ../templates/admin/watermark-apply.php:48
msgid "Show Log"
msgstr ""

#: ../templates/admin/watermark-apply.php:49
msgid "Hide Log"
msgstr ""

#: ../templates/admin/watermark-insert-new.php:16
#: ../templates/metaboxes/product_watermark.php:14
msgid "Add Watermark"
msgstr ""

#: ../templates/metaboxes/product_watermark.php:15
msgid "Add"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:37
#: ../templates/single-watermark-template.php:40
msgid "( in pt )"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:37
#: ../templates/single-watermark-template.php:40
msgid "( in px )"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:44
#: ../templates/single-watermark-template.php:47
msgid "Remove"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:46
#: ../templates/single-watermark-template.php:49
msgid "Expand"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:47
#: ../templates/single-watermark-template.php:50
msgid "Collapse"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:52
msgid "Create Watermark"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:55
msgid "Starting from a text"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:56
msgid "Starting from an image"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:62
msgid "Text"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:66
msgid "Write a text"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:69
msgid "Font Type"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:85
msgid "Font Size"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:92
msgid "Font Color"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:99
msgid "Background Color"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:106
msgid "Opacity"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:111
msgid ""
"Set background opacity, set to 0 for complete transparency, or to\n"
"  100 for complete opacity"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:115
msgid "Box Width"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:119
msgid ""
"Set the width of the box by percentage compared to the selected \"Shop Size"
"\" option"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:122
msgid "Box Height"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:126
msgid ""
"Set the height of the box by percentage compared to the selected \"Shop Size"
"\" option"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:129
#: ../templates/single-watermark-template.php:124
msgid "Rotate text to"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:133
#: ../templates/single-watermark-template.php:127
msgid "Specify an angle between 0 and 360° to rotate your text"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:136
#: ../templates/single-watermark-template.php:132
msgid "Line Height"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:140
#: ../templates/single-watermark-template.php:135
msgid "Set -1 to set it equal to the height of the watermark"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:146
#: ../templates/metaboxes/single-product-watermark-template.php:150
#: ../templates/metaboxes/single-product-watermark-template.php:151
#: ../templates/single-watermark-template.php:161
msgid "Select Watermark"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:158
msgid "Repeat image"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:162
#: ../templates/single-watermark-template.php:172
msgid "If enabled, the watermark is replicated on the whole image"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:167
#: ../templates/single-watermark-template.php:178
msgid "Margin X"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:173
#: ../templates/single-watermark-template.php:180
msgid "Margin Y"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:181
#: ../templates/single-watermark-template.php:186
msgid "Shop Size"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:189
#: ../templates/single-watermark-template.php:194
msgid "Full Size (visible in modal)"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:196
msgid "Select the images you want to apply the watermark on"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:202
#: ../templates/single-watermark-template.php:227
msgid "Watermark Position"
msgstr ""

#: ../templates/metaboxes/single-product-watermark-template.php:224
#: ../templates/single-watermark-template.php:250
#: ../templates/watermark-preview.php:13
msgid "Preview"
msgstr ""

#: ../templates/single-watermark-template.php:58
msgid "Create Watermark from"
msgstr ""

#: ../templates/single-watermark-template.php:61
msgid "Select an option"
msgstr ""

#: ../templates/single-watermark-template.php:62
msgid "From Text"
msgstr ""

#: ../templates/single-watermark-template.php:63
msgid "From Image"
msgstr ""

#: ../templates/single-watermark-template.php:73
msgid "Watermark Text"
msgstr ""

#: ../templates/single-watermark-template.php:79
msgid "Watermark Font"
msgstr ""

#: ../templates/single-watermark-template.php:94
msgid "Watermark Font Color"
msgstr ""

#: ../templates/single-watermark-template.php:101
msgid "Watermark Font Size "
msgstr ""

#: ../templates/single-watermark-template.php:108
msgid "Box Width ( % ) "
msgstr ""

#: ../templates/single-watermark-template.php:111
msgid "Set the width of the box in percent compared to the shop size"
msgstr ""

#: ../templates/single-watermark-template.php:116
msgid "Box Height ( % ) "
msgstr ""

#: ../templates/single-watermark-template.php:119
msgid "Set the height of the box in percent compared to the shop size"
msgstr ""

#: ../templates/single-watermark-template.php:139
msgid "Watermark Background Color"
msgstr ""

#: ../templates/single-watermark-template.php:146
msgid "Watermark Opacity"
msgstr ""

#: ../templates/single-watermark-template.php:149
msgid ""
"Set background opacity, set to 0 for complete transparency, or set to 100 "
"for complete opacity"
msgstr ""

#: ../templates/single-watermark-template.php:158
msgid "Watermark Image"
msgstr ""

#: ../templates/single-watermark-template.php:168
msgid "Repeat Image"
msgstr ""

#: ../templates/single-watermark-template.php:176
msgid "Watermark Margin"
msgstr ""

#: ../templates/single-watermark-template.php:201
msgid "Select the images to which you want to apply the watermark"
msgstr ""

#: ../templates/single-watermark-template.php:206
msgid "Product Categories"
msgstr ""

#: ../templates/single-watermark-template.php:220
msgid "Choose Category"
msgstr ""

#: ../templates/single-watermark-template.php:223
msgid ""
"Select the categories to which you want to apply the watermark, leave empty "
"to select all"
msgstr ""
