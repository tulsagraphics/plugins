��    U      �  q   l      0     1     ?     S     c     p          �     �     �  �   �     I     M     \     i     u  	   �     �     �  	   �  5   �     �     �     �     	     &	     9	     B	     K	     \	     a	  	   i	     s	  9   {	     �	     �	     �	     �	     �	      
     
     
  	   )
     3
     I
     R
     g
     p
     
     �
     �
     �
  !   �
     �
     	     $     D     `     u     �     �  	   �     �     �     �     �     �  	         
               "  	   @     J     Q     b     f     �     �  	   �  %   �     �  8   �  ;     
   V  �  a     �          %  
   4     ?     L     f     u     �  �   �     -     3     J     h     t  
   �  	   �     �  	   �  <   �     �                &     A     \     h     u     �     �     �  	   �  F   �     �                    3     E     J     X  	   o     y  
   �     �     �     �  "   �     �            -   3     a     v  -   �     �     �     �          	  
             9  
   U     `     z     �     �     �     �  "   �     �     �     �       !        2     :     N  )   `     �     �     �     �     $   P                 D   4   E   %             I   
   >           *   	   9   A   <      L           3          #          J   .          S   M                  C       ,   /   7          2   8          ;   "                      @   (       G   O              :      0       !             H       6   )       T   ?          -   5   '          U       K              &   +   R      B   F   1   Q   =      N             + Add account + Add new condition Account details Account name Account number Add New Rule Add new Add new rule Admin menu nameYITH Rules Alert Message: WooCommerce requiresYITH Payment Method Restrictions is enabled but not effective. It requires WooCommerce in order to work. All All YITH Rules BACS account BIC / Swift Back to rules Bank name Category Change BACS account Changelog Check this option to disable payment restriction rule Conditions: Delete Delete %s permanently Delete Permanently Delete permanently Disable: Disabled Does not contain Edit Edit %s Edit Rule Enabled Enter here the reason why the payment gateway is disabled Equal to Geolocalization Greater than Greater than or equal to Help Center IBAN Include all Include at least one of Less than Less than or equal to Message: Move %s to the Trash New Rule No Rules found No Rules found in trash Parent Rules Payment Method Payment Method Restrictions Payment Method Restrictions Rules Payment method gateway: Payment method restriction Payment method restriction rule Payment method restrictions Plugin documentation Premium Version Price Product Published Remove payment method Remove selected account(s) Restore Restore %s from the Trash Restriction by: Rule name Rules Save Search Rules Select bank transfer account: Sort code Status Support platform Tag This is where rules are stored. Trash Type of restriction: View Rule What to do with this payment gateway? current version plugin name in admin WP menuPayment Method Restrictions plugin name in admin page titlePayment Method Restrictions verbTrash Project-Id-Version: YITH Payment Method Restrictions for WooCommerce
POT-Creation-Date: 2017-08-16 13:12+0200
PO-Revision-Date: 2017-08-16 13:33+0200
Last-Translator: 
Language-Team: YITH <plugins@yithemes.com>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.3
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=n!=1;
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: plugin-fw
 + Aggiungi un conto + Aggiungi una condizione Dettagli conto Nome conto Numero conto Aggiungi una nuova regola Aggiungi nuova Aggiungi una nuova regola Regole YITH YITH Payment Method Restrictions è abilitato ma non operativo. È necessario aver installato WooCommerce perché possa funzionare correttamente. Tutte Tutte le regole (YITH) Conto bonifici bancari (BACS) BIC / Swift Torna alle regole Nome banca Categoria Cambia conto BACS Changelog Metti una spunta a questa opzione per disabilitare la regola Condizioni: Rimuovi Rimuovi %s in modo permanente Rimuovi in modo permanente Rimuovi in modo permanente Disabilita: Disabilitato Non contiene Modifica Modifica %s Modifica regola Abilitato Inserisci qui il motivo per cui il metodo di pagamento è disabilitato Uguale a Geolocalizzazione Maggiore di Maggiore o uguale a Centro assistenza IBAN Includi tutti Includi almeno uno tra Minore di Minore o uguale a Messaggio: Sposta %s nel cestino Nuova regola Nessuna regola trovata Nessuna regola trovata nel cestino Regole genitore Metodo di pagamento Payment Method Restrictions Regole di restrizione sul metodo di pagamento Metodo di pagamento: Payment Method Restriction Regola di restrizione sul metodo di pagamento Payment Method Restrictions Documentazione plugin Versione premium Prezzo Prodotto Pubblicate Rimuovi metodo di pagamento Rimuovi i conti selezionati Ripristina Ripristina %s dal cestino Restrizione per: Nome regola Regole Salva Cerca tra le regole Seleziona il conto per i bonifici: Ordina per filiale Stato Piattaforma di supporto Tag Qui sono salvate tutte le regole. Cestino Tipo di restrizione Visualizza regola Cosa fare con questo metodo di pagamento? versione attuale Payment Method Restrictions Payment Method Restrictions Cestina 