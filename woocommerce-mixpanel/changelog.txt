*** WooCommerce Mixpanel Changelog ***

2018.03.28 - version 1.12.1
 * Fix - Ensure orders processed with the Cash on Delivery gateway are fully tracked

2018.01.26 - version 1.12.0
 * Misc - Add support for WooCommerce 3.3
 * Misc - Remove support for WooCommerce 2.5

2017.10.24 - version 1.11.1
 * Fix - Fix issue with sending subscription renewal events
 * Fix - Correctly alias users when completing a purchase
 * Fix - Fix deprecated notices with WooCommerce Subscriptions

2017.05.02 - version 1.11.0
 * Tweak - Send user properties to Mixpanel on every page load
 * Fix - Properly escape event names and properties when tracking via JavaScript

2017.03.28 - version 1.10.0
 * Misc - Added support for WooCommerce 3.0
 * Misc - Removed support for WooCommerce 2.4

2016.11.15 - version 1.9.3
 * Fix - Fixes revenue tracking for automatic gateways

2016.08.31 - version 1.9.2
 * Fix - Fix a rare PHP error that can occur when an error occurs receiving a response from Mixpanel's API

2016.08.02 - version 1.9.1
 * Misc - Made disable tracking method public to help third party developers to determine if they should run their own custom Mixpanel tracking or not

2016.06.02 - version 1.9.0
 * Misc - Added support for WooCommerce 2.6
 * Misc - Removed support for WooCommerce 2.3

2016.05.03 - version 1.8.0
 * Feature - Greatly improve the reliability of purchase tracking
 * Tweak - Use the WooCommerce order number as the value of the Order ID property when tracking purchases and other order processes

2016.01.14 - version 1.7.0
 * Misc - Added support for WooCommerce 2.5
 * Misc - Removed support for WooCommerce 2.2

2015.10.06 - version 1.6.1
 * Tweak - Track the product price when adding to the cart
 * Misc - WooCommerce Subscriptions 2.0 Compatibility

2015.07.28 - version 1.6.0
 * Feature - You can now track custom user properties
 * Misc - WooCommerce 2.4 Compatibility

2015.02.09 - version 1.5.0
 * Misc - WooCommerce 2.3 Compatibility

2015.01.20 - version 1.4.5
 * Fix - Use mixpanel.alias to track registered users

2014.10.06 - version 1.4.4
 * Fix - Track the 'Changed Password' event

2014.09.30 - version 1.4.3
 * Fix - Fix a fatal error when placing a subscription order

2014.09.07 - version 1.4.2
 * Misc - WooCommerce 2.2 Compatibility

2014.08.19 - version 1.4.1
 * Tweak - More geocoding IP addresses improvements

2014.07.15 - version 1.4
 * Feature - Track additional properties for new user sign ups
 * Fix - Fix issue with tracking People revenue at checkout

2014.05.27 - version 1.3.2
 * Fix - Really fix issue with geocoding IP address :)

2014.02.24 - version 1.3.1
 * Fix - Fix potential issue with geocoding IP addresses

2014.01.20 - version 1.3
 * Feature - Subscription free trials that convert into paying customers now send the 'subscription trial converted' event
 * Fix - Fixed possible duplicate 'completed purchase' events
 * Misc - WooCommerce 2.1 compatibility
 * Misc - Uses SkyVerge Plugin Framework
 * Localization - Text domain changed from `wc-mixpanel` to `woocommerce-mixpanel`

2013.08.23 - version 1.2.1
 * Tweak - User charges are now tracked at checkout
 * Fix - Alias user when they create an account during checkout

2013.07.26 - version 1.2
 * Tweak - Rewritten codebase for improved performance and reliability
 * Localization - Text domain changed from `wc_mixpanel` to `wc-mixpanel`
 * Misc - Rebrand to SkyVerge

2013.01.23 - version 1.1.1
 * Feature - WooCommerce 2.0 Compatibility
 * Tweak - Minor performance improvements

2012.12.07 - version 1.1.0
 * Feature - Add support for WooCommerce Subscriptions! Track subscription-related events with one click, see the Docs for additional info.
 * Feature - Log errors and queries to the WooCommerce log instead of the PHP error log for easier access when on shared or cloud hosting
 * Tweak - Improve code styling and documentation

2012.12.04 - version 1.0.1
 * Feature - New WooThemes Updater

2012.09.06 - version 1.0
 * Initial Release :)
