=== WooCommerce Mixpanel ===
Author: skyverge, woocommerce
Tags: woocommerce
Requires at least: 4.4
Tested up to: 4.9.4

Adds Mixpanel tracking to WooCommerce with one click!

See http://docs.woothemes.com/document/mixpanel/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-mixpanel' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
