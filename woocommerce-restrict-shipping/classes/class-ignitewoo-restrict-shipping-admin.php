<?php 

class IGN_Restrict_Shipping_Admin {

	var $do_not_show; 
	
	function __construct() { 
	
		//add_action( 'add_meta_boxes', array( &$this, 'add_meta_box' ) );
		add_action( 'save_post', array( &$this, 'save_post' ), 1, 1 );
		add_action( 'edit_post', array( &$this, 'save_post' ), 1, 1 );
		
		add_action( 'woocommerce_product_write_panel_tabs', array( $this, 'add_tab' ), 15 );
		add_action( 'woocommerce_product_write_panels', array( $this, 'data_panels' ), 15 );
		
		add_action( 'admin_enqueue_scripts', array( $this,'enqueue_scripts' ) );
		add_action( 'admin_print_footer_scripts', array( $this,'footer_scripts' ) );
		
		add_action( 'wp_ajax_ign_saw_new_location_pointer', array( $this,'saw_rs_location_pointer' ) );
	}
	
	function saw_rs_location_pointer() { 
		global $user_ID;
		
		update_user_meta( $user_ID, 'saw_rs_location_pointer', 1 );
	
		die( 'done' );
		
	}

	function enqueue_scripts() {
		global $user_ID;
		
		if ( get_user_meta( $user_ID, 'saw_rs_location_pointer', true ) ) {
			$this->do_not_show = true;
			return;
		}
			
		wp_enqueue_style( 'wp-pointer' );
		wp_enqueue_script( 'wp-pointer' );
	}

	function footer_scripts() {
		if ( $this->do_not_show )
			return;
			
		$pointer_content = '<h3>' . __( 'Restrict Shipping', 'ignitewoo_restrict_shipping' ) . '</h3>';
		$pointer_content .= '<p>' . __( 'The settings for restricting shipping for this product are here in the Shipping Restrictions tab.', 'ignitewoo_restrict_shipping' ) . '</p>';
		?>
		<script type="text/javascript">
			jQuery(document).ready( function($) {
				setTimeout( function() {
					$('.shipping_restrictions_tab').pointer({
						content: '<?php echo $pointer_content; ?>',
						position: 'left',
						pointerClass: 'ign_rs_pointer',
						close: function() {
							$.post( ajaxurl, { action:'ign_saw_new_location_pointer' } );
						}
					}).pointer('open');
				}, 1000 );
			});
		</script>
		<?php
	}

	function add_tab() { 
		?>
		<style>
		#woocommerce-coupon-data ul.wc-tabs li.shipping_restrictions_tab a:before, #woocommerce-product-data ul.wc-tabs li.shipping_restrictions_tab a:before, .woocommerce ul.wc-tabs li.shipping_restrictions_tab a:before {
			content: "";
		}		
		</style>
		<li class="shipping_restrictions_tab show_if_simple show_if_variable"><a href="#shipping_restrictions_tab"><?php _e('Shipping Restrictions', 'ignitewoo_restrict_shipping' ); ?></a></li>
		<?php
	}
	
	function data_panels() {
		?>
		<div id="shipping_restrictions_tab" class="woocommerce_options_panel panel wc-metaboxes-wrapper">
		<?php
		
		$this->meta_box_content();
		
		?>
		</div>
		<?php
	}
	
	/*
	function add_meta_box() {
	
		add_meta_box( 'woocommerce-restrict-shipping', __( 'Restricted Countries / States', 'ignitewoo_restrict_shipping' ), array( &$this, 'meta_box_content' ), 'product', 'side', 'default' );

	}
	*/
	
	function meta_box_content() {
		global $woocommerce, $post;

		$c = new WC_Countries();

		$s = get_post_meta( $post->ID, 'restrict_states', true );

		if ( !isset( $s ) )
			$s = array();

		wp_enqueue_script( 'rst-chosen', $GLOBALS['ignitewoo_restrict_shipping']->plugin_url . '/assets/chosen.jquery.min.js' );
		wp_enqueue_style( 'rst-chosen-css', $GLOBALS['ignitewoo_restrict_shipping']->plugin_url. '/assets/chosen.css' );
		?>
		
		<style>
			table tbody tr.alt { background-color: #F9F9F9; }
			.restrict_states_box .chzn-container, .restrict_states_box .chosen-container { width: 100% !important; }
			.restrict_states_box .chosen-container-multi .chosen-choices li.search-field input[type="text"] { height: 26px !important; }
			.restrict_states_box .chosen-container-multi .chosen-choices li.search-choice {
				background-image: none !important;
				box-shadow: none !important;
				border: none !important;
				background-color: #e4e4e4 !important;
				padding: 5px 20px 5px 10px !important;
			}
			.restrict_states_box .chosen-container-multi .chosen-choices li.search-choice .search-choice-close {
				top: 7px !important;
			}
			.add_all_states {
				float:none !important; 
				vertical-align:top !important;
				margin-left: 20px !important;
			}
		</style>
		
		<div class="restrict_states_box country_states_wrap">
			<p style="font-weight:bold">
				<?php _e( 'Select areas where this product cannot be shipped. Leave this blank to allow shipping to anywhere', 'ignitewoo_restrict_shipping' )?>
			</p>
			<table><tr><td>
			<p>
			<select name="restrict_states[]" multiple="multiple" class="chosen_selects" style="width:200px">
			<?php
			
			$state_groups = array();
			
			foreach( $c->countries as $k => $v ) {

				if ( !empty( $c->states[ $k ] ) && count( $c->states[ $k ] ) > 0 ) { 
				
					echo '<optgroup label="' . $v . '">'; 
					
					foreach( $c->states[ $k ] as $kk => $vv ) { 

						if ( in_array( $k . ':' . $kk, (array)$s ) )
							$selected = ' selected="selected"';
						else
							$selected = '';
							
						echo '<option value="' . $k . ':' . $kk . '" ' . $selected . '>' . $vv . '</option>';
					}
					
					echo '</optgroup>';
					
					if ( 'US' !== $k )
						$state_groups[] = array( 'name' => $k, 'label' => $v, 'states' => $c->states[ $k ] );
				
				} else { 
				
					if ( in_array( $k, (array)$s ) )
						$selected = ' selected="selected"';
					else
						$selected = '';
						
					echo '<option value="' . $k . '"' . $selected . '>' . $v . '</option>';
					
				}
			
			}
		
			$setting = get_post_meta( $post->ID, 'apply_global_shipping_restrictions', true );

			if ( empty( $setting ) )
				$setting = 'merge';

			?>
			</select>
			</p>

			<p>
				<button class="select_all button"><?php _e('All', 'ignitewoo_table_rate'); ?></button>
				<button class="select_none button"><?php _e('None', 'ignitewoo_table_rate'); ?></button>
				
				<select class="add_all_states" name="add_all_states" >
				<?php 

				$state_groups[] = array( 'label' => __('US States', 'ignitewoo_table_rate') );
				
				$state_groups[] = array( 'label' => __('EU States', 'ignitewoo_table_rate') );
			
				usort( $state_groups, array( &$this, 'sort_states' ) );
				
				for ( $i = 0; $i < count( $state_groups ); $i++ )  { ?>
				
					<option id="<?php echo str_replace( ' ', '-', $state_groups[$i]['label'] )?>" value="<?php echo str_replace( ' ', '-', $state_groups[$i]['label'] )?>"><?php _e( $state_groups[ $i ]['label'], 'ignitewoo_table_rate'); ?></option>
				
				<?php } ?>
				</select> 
				<button class="select_all_states button"><?php _e('Add States/Provinces', 'ignitewoo_table_rate'); ?></button>
			</p>
			
			</td></tr></table>
			
			<?php /*
			<p><button class="select_all button"><?php _e('All', 'ignitewoo_table_rate'); ?></button><button class="select_none button"><?php _e('None', 'ignitewoo_table_rate'); ?></button><button class="button select_us_states"><?php _e('US States', 'ignitewoo_table_rate'); ?></button><button class="button select_europe"><?php _e('EU States', 'ignitewoo_table_rate'); ?></button><button class="button select_aus"><?php _e('AUS States', 'ignitewoo_table_rate'); ?></button></p>
			*/ ?>

			
			<p style="line-height: 1.2em">
				<input type="radio" name="apply_global_shipping_restrictions" value="override" <?php checked( $setting, 'override', true ) ?>> <?php _e( 'Override global restrictions', 'ignitewoo_restrict_shipping' )?>
			</p>
			<p>
				<input type="radio" name="apply_global_shipping_restrictions" value="merge" <?php checked( $setting, 'merge', true ) ?>> <?php _e( 'Merge with global restrictions', 'ignitewoo_restrict_shipping' )?>
			</p>
		</div>
		
		<?php
		
		$this->add_inline_js();
		
		$this->variation_rules( $c );
	}
	
	
	function variation_rules( $c ) { 
		global $post;
		
		if ( function_exists( 'wc_get_product' ) )
			$_product = wc_get_product();
		else 
			$_product = get_product();
			
		if ( !$_product->is_type( 'variable' ) )
			return;
			
		?>
		
		<div class="restrict_states_box variation_restrictions">
			<p class="form-field" style="font-weight:bold">
				<label><?php _e( 'Restriction by variation', 'ignitewoo_restrict_shipping' ) ?></label>
			</p>
			<table class="widefat" style="margin-bottom: 15px">
				<thead>
					<tr>
						
						<th style="width:20%"><?php _e( 'Variation', 'ignitewoo_restrict_shipping' )?></th>
						<th><?php _e( 'Restrictions', 'ignitewoo_restrict_shipping' )?></th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; foreach( $_product->get_children() as $child_id ) { 
						
						if ( 0 == $i % 2 )
							$alt = '';
						else 
							$alt = 'alt';
						
						$i++;
						
						$s = get_post_meta( $child_id, '_variable_restrict_states', true );
						
						$variation = $_product->get_child( $child_id );
						$variation_attributes = $variation->get_variation_attributes();
						$attrs = array();
						
						foreach( $variation_attributes as $id => $name )
							$variation_attributes[ $id ] = ucwords( $name );
						
					?>
					<tr class="<?php echo $alt ?>">
						<td><?php echo $child_id . ' &ndash; ' . implode( ', ', $variation_attributes ) ?></td>
						
						<td style="overflow:auto">
							<p class="country_states_wrap">
								<select name="variable_restrict_states[<?php echo $child_id ?>][]" multiple="multiple" class="chosen_selects" style="width:200px">
								<?php
								
								$state_groups = array();
								
								foreach( $c->countries as $k => $v ) {

									if ( !empty( $c->states[ $k ] ) && count( $c->states[ $k ] ) > 0 ) { 
									
										echo '<optgroup label="' . $v . '">'; 
										
										foreach( $c->states[ $k ] as $kk => $vv ) { 

											if ( in_array( $k . ':' . $kk, (array)$s ) )
												$selected = ' selected="selected"';
											else
												$selected = '';
												
											echo '<option value="' . $k . ':' . $kk . '" ' . $selected . '>' . $vv . '</option>';
										}
										
										echo '</optgroup>';
										
										if ( 'US' !== $k )
											$state_groups[] = array( 'name' => $k, 'label' => $v, 'states' => $c->states[ $k ] );
									
									} else { 
									
										if ( in_array( $k, (array)$s ) )
											$selected = ' selected="selected"';
										else
											$selected = '';
											
										echo '<option value="' . $k . '"' . $selected . '>' . $v . '</option>';
										
									}
								
								}
							
								$setting = get_post_meta( $post->ID, 'apply_global_shipping_restrictions', true );

								if ( empty( $setting ) )
									$setting = 'override';

								?>
								</select>
							</p>
							<p style="clear:both">
								
								<button class="select_all button"><?php _e('All', 'ignitewoo_table_rate'); ?></button>
								<button class="select_none button"><?php _e('None', 'ignitewoo_table_rate'); ?></button>
								
								<select class="add_all_states" name="add_all_states" >
								<?php 

								$state_groups[] = array( 'label' => __('US States', 'ignitewoo_table_rate') );
								
								$state_groups[] = array( 'label' => __('EU States', 'ignitewoo_table_rate') );
							
								usort( $state_groups, array( &$this, 'sort_states' ) );
								
								for ( $i = 0; $i < count( $state_groups ); $i++ )  { ?>
								
									<option id="<?php echo str_replace( ' ', '-', $state_groups[$i]['label'] )?>" value="<?php echo str_replace( ' ', '-', $state_groups[$i]['label'] )?>"><?php _e( $state_groups[ $i ]['label'], 'ignitewoo_table_rate'); ?></option>
								
								<?php } ?>
								</select> 
								<button class="select_all_states button"><?php _e('Add States/Provinces', 'ignitewoo_table_rate'); ?></button>

							</p>
						
						</td>
					</tr>
					<?php } ?>
				</tbody> 
			</table>
		</div>
		
		<?php 
	}

	function sort_states( $a, $b ) { 

		if ( $a['label'] == $b['label'] )
			return;
	
		return strcmp( $a["label"], $b["label"] );
	}
	
	function add_inline_js() { 
		global $woocommerce;
				 
		$js = "
			jQuery( document ).ready( function() { 
				jQuery( '.chosen_selects' ).chosen();
			})
			jQuery( '.select_all_states' ).click( function(e) { 
			
				e.preventDefault();
				
				var id = jQuery( this ).closest( 'p' ).find( '.add_all_states :selected' ).attr('id');	
				
				ids = id.replace( /-/g, ' ' );
				
				if ( 'EU States' == ids ) { 
					jQuery( this ).closest('td').find('select').find('option[value=\"AL\"], option[value=\"AD\"], option[value=\"AM\"], option[value=\"AT\"], option[value=\"BY\"], option[value=\"BE\"], option[value=\"BA\"], option[value=\"BG\"], option[value=\"CH\"], option[value=\"CY\"], option[value=\"CZ\"], option[value=\"DE\"], option[value=\"DK\"], option[value=\"EE\"], option[value=\"ES\"], option[value=\"FO\"], option[value=\"FI\"], option[value=\"FR\"], option[value=\"GB\"], option[value=\"GE\"], option[value=\"GI\"], option[value=\"GR\"], option[value=\"HU\"], option[value=\"HR\"], option[value=\"IE\"], option[value=\"IS\"], option[value=\"IT\"], option[value=\"LT\"], option[value=\"LU\"], option[value=\"LV\"], option[value=\"MC\"], option[value=\"MK\"], option[value=\"MT\"], option[value=\"NO\"], option[value=\"NL\"], option[value=\"PO\"], option[value=\"PT\"], option[value=\"RO\"], option[value=\"RU\"], option[value=\"SE\"], option[value=\"SI\"], option[value=\"SK\"], option[value=\"SM\"], option[value=\"TR\"], option[value=\"UA\"], option[value=\"VA\"]').attr(\"selected\",\"selected\");
					
					jQuery(this).closest('div').find('select').trigger('chosen:updated');
					
					return false;
					
				} else if ( 'US States' == ids ) { 
					jQuery( this ).closest('td').find('select').find('option[value=\"US:AK\"], option[value=\"US:AL\"], option[value=\"US:AZ\"], option[value=\"US:AR\"], option[value=\"US:CA\"], option[value=\"US:CO\"], option[value=\"US:CT\"], option[value=\"US:DE\"], option[value=\"US:DC\"], option[value=\"US:FL\"], option[value=\"US:GA\"], option[value=\"US:HI\"], option[value=\"US:ID\"], option[value=\"US:IL\"], option[value=\"US:IN\"], option[value=\"US:IA\"], option[value=\"US:KS\"], option[value=\"US:KY\"], option[value=\"US:LA\"], option[value=\"US:ME\"], option[value=\"US:MD\"], option[value=\"US:MA\"], option[value=\"US:MI\"], option[value=\"US:MN\"], option[value=\"US:MS\"], option[value=\"US:MO\"], option[value=\"US:MT\"], option[value=\"US:NE\"], option[value=\"US:NV\"], option[value=\"US:NH\"], option[value=\"US:NJ\"], option[value=\"US:NM\"], option[value=\"US:NY\"], option[value=\"US:NC\"], option[value=\"US:ND\"], option[value=\"US:OH\"], option[value=\"US:OK\"], option[value=\"US:OR\"], option[value=\"US:PA\"], option[value=\"US:RI\"], option[value=\"US:SC\"], option[value=\"US:SD\"], option[value=\"US:TN\"], option[value=\"US:TX\"], option[value=\"US:UT\"], option[value=\"US:VT\"], option[value=\"US:VA\"], option[value=\"US:WA\"], option[value=\"US:WV\"], option[value=\"US:WI\"], option[value=\"US:WY\"]').attr(\"selected\",\"selected\");
					
					jQuery(this).closest('div').find('select').trigger('chosen:updated');
					
					return false;
				
				}

				jQuery( this ).closest('td').find('select').find( 'optgroup[label=\"' + ids + '\"]' ).find('option').each( function() { 
					jQuery( this ).attr(\"selected\",\"selected\");
				})
				
				jQuery( this ).closest('td').find('select').trigger('chosen:updated');
				
				return false;
			})
		
			jQuery('.select_all').on('click', function(){
				jQuery(this).closest('td').find('select option').attr(\"selected\",\"selected\");
				jQuery(this).closest('td').find('select').trigger('chosen:updated');
				return false;
			});

			jQuery('.select_none').on('click', function(){
				jQuery(this).closest('td').find('select option').removeAttr(\"selected\");
				jQuery(this).closest('td').find('select').trigger('chosen:updated');
				return false;
			});
		
			
		";

		
		if ( function_exists( 'wc_enqueue_js' ) )
			wc_enqueue_js( $js );
		else 
			$woocommerce->add_inline_js( $js );
	}
	
	function save_post( $post_id ) {
		global $post;
		
		if ( empty( $post ) )
			return;

		if ( !$_POST ) return $post_id;
		if ( is_int( wp_is_post_revision( $post_id ) ) ) return;
		if ( is_int( wp_is_post_autosave( $post_id ) ) ) return;
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
		if ( !current_user_can( 'edit_post', $post_id )) return $post_id;
		if ( $post->post_type != 'product' ) return $post_id;

		$rs = isset( $_POST['restrict_states'] ) ?  $_POST['restrict_states'] : '';
		
		update_post_meta( $post_id, 'restrict_states', $rs );
		
		update_post_meta( $post_id, 'apply_global_shipping_restrictions', $_POST['apply_global_shipping_restrictions'] );
		
		if ( !empty( $_POST['variable_restrict_states'] ) && is_array( $_POST['variable_restrict_states'] ) ) {
			foreach( $_POST['variable_restrict_states'] as $vid => $data ) { 
				if ( !empty( $data ) )
					update_post_meta( $vid, '_variable_restrict_states', $data );
			} 
		}
	}
}

$GLOBALS['ignitewoo_restrict_shipping_admin'] = new IGN_Restrict_Shipping_Admin();
	