<?php
/*
Plugin Name: WooCommerce Restrict Shipping
Plugin URI: http://ignitewoo.com
Description: Prevents customers from purchasing items that cannot be shipped to selected countries / states
Version: 1.7.4
Author: IgniteWoo
Author URI: http://ignitewoo.com
*/


// Add the plugin settings interface
add_action( 'admin_init', 'ign_restrict_shipping_admin_init', 99 );

function ign_restrict_shipping_admin_init() {
	global $ignitewoo_integrations;
	
	if ( !class_exists( 'Woocommerce' ) && !class_exists( 'WC' ) )
		return;

	// Add the primary tab
	add_action( 'woocommerce_settings_tabs', 'ignitewoo_add_tab', 10 );
	
	// Add the integration sections
	add_action( 'woocommerce_settings_tabs_ignitewoo', 'ignitewoo_settings_tab_action', 10 );
	
	if ( !class_exists( 'IGN_Integration' ) || !class_exists( 'IGN_Integrations' ) ) { 

		require_once( dirname( __FILE__ ) . '/classes/class-ignitewoo-integration.php' );
			
		require_once( dirname( __FILE__ ) . '/classes/class-ignitewoo-integrations.php' );
		
	}
	
	require_once( dirname( __FILE__ ) . '/classes/class-ignitewoo-restrict-shipping-integration.php' );

	$ignitewoo_integrations->init();
	
	if ( !function_exists( 'ignitewoo_add_tab' ) ) { 
	
		function ignitewoo_add_tab() {
			global $ignitewoo_integrations; 

			$current_tab = ( isset($_GET['tab'] ) ) ? $_GET['tab'] : 'general';
			
			$ignitewoo_integrations->ignitewoo_integrations_tab( $current_tab );

		}
	}

	if ( !function_exists( 'ignitewoo_settings_tab_action' ) ) { 
	
		function ignitewoo_settings_tab_action() {
			global $ignitewoo_integrations; 

			$ignitewoo_integrations->ignitewoo_integrations_sections();

		}
	}
}


class IgniteWoo_Restrict_Shipping {
	
	var $settings;

	function __construct() {

		add_action( 'init', array( &$this, 'init' ), 10 );
		add_action( 'woocommerce_after_checkout_validation', array( &$this, 'check_restricted_states' ) );
		//add_action( 'woocommerce_before_checkout_form', array( &$this, 'before_checkout_form' ), 1 );
		add_action( 'wp_ajax_woocommerce_update_order_review', array( &$this, 'before_checkout_form' ), 1 );
		add_action( 'wp_ajax_nopriv_woocommerce_update_order_review', array( &$this, 'before_checkout_form' ), 1 );
		
		$this->plugin_url = plugins_url( '', __FILE__ );

	}

	function init() {
	
		$this->settings = get_option( 'woocommerce_ign_restrict_shipping_settings', false );

		if ( !is_admin() )
			return;
			
		require_once( dirname( __FILE__ ) . '/classes/class-ignitewoo-restrict-shipping-admin.php' );
		
	}
	
	function load_plugin_textdomain() {

		$locale = apply_filters( 'plugin_locale', get_locale(), 'ignitewoo_restrict_shipping' );

		load_textdomain( 'ignitewoo_restrict_shipping', WP_LANG_DIR.'/woocommerce/ignitewoo_restrict_shipping-'.$locale.'.mo' );

		$plugin_rel_path = apply_filters( 'ignitewoo_translation_file_rel_path', dirname( plugin_basename( __FILE__ ) ) . '/languages' );

		load_plugin_textdomain( 'ignitewoo_restrict_shipping', false, $plugin_rel_path );

	}

	function before_checkout_form() { 
		global $user_ID, $woocommerce;

		if ( empty( $this->settings['enable'] ) || 'yes' != $this->settings['enable'] )
			return;
		
		if ( empty( $this->settings['live_js'] ) || 'yes' != $this->settings['live_js'] ) { 
		
			if ( empty( $_POST['post_data'] ) && empty( $_POST ) && !is_user_logged_in() ) 
				return;

			if ( !empty( $_POST['post_data'] ) ) { 
			
				$posted = wp_parse_args( $_POST['post_data'], array() );

				$this->check_restricted_states( $posted, 'error' );
		
			} else { 
			
				$user = get_user_meta( $user_ID );
				
				if ( empty( $user ) )
					return; 
					
				$posted['billing_country'] = $user['billing_country'][0];
				$posted['billing_state'] = $user['billing_state'][0];
				$posted['shipping_country'] = $user['shipping_country'][0];
				$posted['shipping_state'] = $user['shipping_state'][0];
			
				$this->check_restricted_states( $posted, 'error' );
			
				wc_print_notices();
			
			}
			
			return;
		}
			
		if ( empty( $_POST['post_data'] ) && empty( $_POST ) && !is_user_logged_in() ) 
			return;

		if ( !empty( $_POST['post_data'] ) ) { 
		
			$posted = wp_parse_args( $_POST['post_data'], array() );

			$this->check_restricted_states( $posted, 'error' );

			ob_start();
			
			if ( function_exists( 'wc_print_notices' ) )
				wc_print_notices();
			else 
				$woocommerce->show_messages();
			
			$messages = ob_get_clean();
			
			$messages = str_replace( array( "\n", "\r" ), '', $messages );
			
			// WC 2.3.x handles checkout JS stuff differently.
			if ( version_compare( WOOCOMMERCE_VERSION, '2.3', '>=' ) ) { 
				
				// Get order review fragment
				ob_start();
				woocommerce_order_review();
				$woocommerce_order_review = ob_get_clean();

				// Get checkout payment fragment
				ob_start();
				woocommerce_checkout_payment();
				$woocommerce_checkout_payment = ob_get_clean();
				
				if ( !empty( $this->error_msg ) ) { 

					$out = array(
						'result' => 'failure',
						'messages' => $messages,
						'reload' => isset( WC()->session->reload_checkout ) ? 'true' : 'false',
						'fragments' => apply_filters( 'woocommerce_update_order_review_fragments', array(
							'.woocommerce-checkout-review-order-table' => $woocommerce_order_review,
							'.woocommerce-checkout-payment'            => $woocommerce_checkout_payment
						) )
					);
					
					wp_send_json( $out );

					die();
					
				} else { 
				
					// Add a script to remove an error messages, prepend to order review HTML
					$script = "<script>jQuery( '.woocommerce-error, .woocommerce-message' ).remove();</script>";
					
					$out = array(
						'result' => 'success',
						'messages' => "",
						'reload' => isset( WC()->session->reload_checkout ) ? 'true' : 'false',
						'fragments' => apply_filters( 'woocommerce_update_order_review_fragments', array(
							'.woocommerce-checkout-review-order-table' => $script . $woocommerce_order_review,
							'.woocommerce-checkout-payment'            => $woocommerce_checkout_payment
						) )
					);
					
					wp_send_json( $out );

					die();;
				}
				
			} else {
			
				if ( !empty( $this->error_msg ) ) {
					echo "
					<script>
						jQuery( '.woocommerce-error, .woocommerce-message' ).remove();
						jQuery( 'form.checkout' ).prepend( '{$messages}' );
						jQuery( 'form.checkout' ).find( '.input-text, select' ).blur();
						jQuery( 'html, body' ).animate({
							scrollTop: ( jQuery( 'form.checkout' ).offset().top - 100 )
						}, 1000 );
						jQuery( 'body' ).trigger( 'checkout_error' );
					</script>";
				} else { 
					echo "
					<script>
						jQuery( '.woocommerce-error, .woocommerce-message' ).remove();
					</script>";
				}	
			}
			
		} else { 
		
			$user = get_user_meta( $user_ID );
			
			if ( empty( $user ) )
				return; 
				
			$posted['billing_country'] = $user['billing_country'][0];
			$posted['billing_state'] = $user['billing_state'][0];
			$posted['shipping_country'] = $user['shipping_country'][0];
			$posted['shipping_state'] = $user['shipping_state'][0];
		
			$this->check_restricted_states( $posted, 'error' );
		
			if ( function_exists( 'wc_print_notices' ) )
				wc_print_notices();
			else 
				$woocommerce->show_messages();
		
		}
		
	}
	
	function check_restricted_states( $posted, $notice_type = 'error' ) {
		global $woocommerce;

		if ( 0 == sizeof( $woocommerce->cart->get_cart() ) )
			return;

		if ( !isset( $posted['billing_state'] ) )
			return;

		$opts = $this->settings; // get_option( 'woocommerce_ign_restrict_shipping_settings', false );

		if ( empty( $opts['enable'] ) || 'yes' != $opts['enable'] )
			return;
			
		$cart = $woocommerce->cart->get_cart();

		if ( count( $cart ) <= 0 )
			return;
			
		$location = '';

		$c = new WC_Countries();
		
		if ( version_compare( WOOCOMMERCE_VERSION, '2.1', '>=' ) )
			$ship_checkbox_name = 'ship_to_different_address';
		else 
			$ship_checkbox_name = 'shiptobilling';

		if ( 'yes' == get_option( 'woocommerce_ship_to_billing_address_only' ) ) { 

			if ( isset( $posted['billing_country'] ) && !empty( $posted['billing_country'] ) ) 
				$location = $posted['billing_country'];
				
			$location_name = $c->countries[ $location ];
				
			if ( isset( $posted['billing_state'] ) && !empty( $posted['billing_state'] ) ) {
				
				if ( count( $c->states[ $location ] ) > 0 ) { 
					$location .= ':' . $posted['billing_state'];
					$location_name = $c->states[ $posted['billing_country'] ][ $posted['billing_state'] ] . ', ' . $location_name;
				} else 
				$location_name = $c->states[ $posted['billing_country'] ][ $posted['billing_state'] ] . $location_name;
				
			}
		
		} else if ( ( isset( $posted[ $ship_checkbox_name ] ) && '1' != $posted[ $ship_checkbox_name ] ) || isset( $posted[ $ship_checkbox_name] ) && empty( $posted[ $ship_checkbox_name ] ) ) {

			if ( isset( $posted['billing_country'] ) && !empty( $posted['billing_country'] ) ) 
				$location = $posted['billing_country'];
				
			$location_name = $c->countries[ $location ];
				
			if ( isset( $posted['billing_state'] ) && !empty( $posted['billing_state'] ) ) {
				
				if ( count( $c->states[ $location ] ) > 0 ) { 
					$location .= ':' . $posted['billing_state'];
					$location_name = $c->states[ $posted['billing_country'] ][ $posted['billing_state'] ] . ', ' . $location_name;
				} else
					$location_name = $c->states[ $posted['billing_country'] ][ $posted['billing_state'] ] . $location_name;
				
			}
			
		} else if ( ( !isset( $posted[ $ship_checkbox_name ] ) || '' == $posted[ $ship_checkbox_name ] ) || isset( $posted[ $ship_checkbox_name ] ) && empty( $posted[ $ship_checkbox_name ] ) ) {

			if ( isset( $posted['billing_country'] ) && !empty( $posted['billing_country'] ) ) 
				$location = $posted['billing_country'];
				
			$location_name = $c->countries[ $location ];
				
			if ( isset( $posted['billing_state'] ) && !empty( $posted['billing_state'] ) ) {
				
				if ( count( $c->states[ $location ] ) > 0 ) { 
					$location .= ':' . $posted['billing_state'];
					$location_name = $c->states[ $posted['billing_country'] ][ $posted['billing_state'] ] . ', ' . $location_name;
				} else
					$location_name = $c->states[ $posted['billing_country'] ][ $posted['billing_state'] ] . $location_name;
				
			}

		} else if ( isset( $posted['shipping_state'] ) /* && !empty( $posted['shipping_state'] )*/ ) {

			if ( isset( $posted['shipping_country'] ) && !empty( $posted['shipping_country'] ) ) 
				$location = $posted['shipping_country'];

			$location_name = $c->countries[ $location ];
			
			if ( isset( $posted['shipping_state'] ) && !empty( $posted['shipping_state'] ) ) {
			
				if ( count( $c->states[ $location ] ) > 0 ) { 
					$location .= ':' . $posted['shipping_state'];
					$location_name = $c->states[ $posted['shipping_country'] ][ $posted['shipping_state'] ] . ', ' . $location_name;
				} else 
					$location_name = $c->states[ $posted['shipping_country'] ][ $posted['shipping_state'] ] . $location_name;
				
			}

		}

		$global_restrictions = get_option( 'woocommerce_ign_restrict_shipping_settings', array() );

		if ( function_exists( 'wc_get_page_id' ) )
			$cpid = wc_get_page_id( 'cart' );
		else 
			$cpid = woocommerce_get_page_id( 'cart' );
						
		$cpid = get_permalink( $cpid );
		
		/*
		if ( function_exists( 'wc_clear_notices' ) ) 
			wc_clear_notices();
		else 
			$woocommerce->clear_messages();
		*/
		
		foreach( $cart as $k => $i ) {

			// action setting for product restrictions
			$setting = get_post_meta( $i['product_id'], 'apply_global_shipping_restrictions', true );

			if ( empty( $setting ) )
				$setting = 'merge';

			$restricted_states = null;
			
			// Variation? if so check for specific restrictions
			if ( $i['data']->is_type('variation') ) {
				$restricted_states = get_post_meta( $i['variation_id'] , '_variable_restrict_states', true );
			}
			
			// product restrictions
			if ( empty( $restricted_states ) )
				$restricted_states = get_post_meta( $i['product_id'], 'restrict_states', true );
				
			if ( empty( $restricted_states ) )
				$restricted_states = array();
				
			// If override is set and the product settings are empty, then do not process restriction checking
			if ( 'override' == $setting && empty( $restricted_states ) )
				continue;
	
			if ( empty( $restricted_states ) )
				$restricted_states = array();
				
			// If merge is set then merge global restrictions ( if any ) with the product restrictions, and process if not empty			
			if ( 'merge' == $setting ) { 
			
				// Merge with global settings				
				$restricted_areas = (array)$global_restrictions['restricted_areas'];
			
				$restricted_states = array_unique( array_merge( $restricted_areas, $restricted_states ) );
			}

			// No restriction at all?  Continue to next product in the cart
			if ( empty( $restricted_states ) )
				continue;

			$show_message = false;
			
			// if there are no product based restrictions check the global restricted categories
			// but only if there are categories set, otherwise the global restrictions apply to all products
			// If there are restricted cats then restrictions ONLY apply to products in those categories
			
			$r = get_post_meta( $i['product_id'], 'restrict_states', true );
			
			if ( !empty( $global_restrictions['product_categories'] ) && empty( $r ) ) {
			
				// Is the product in a restricted category? 
				if ( empty( $i['product_id'] ) )
					continue;
					
				$terms = wp_get_post_terms( $i['product_id'], 'product_cat' );
				
				if ( empty( $terms ) || is_wp_error( $terms ) )
					continue;
				
				$restricted_cat = false;
				
				foreach( $terms as $t ) {

					if ( !in_array( $t->term_id, $global_restrictions['product_categories' ] ) )
						continue;

					$restricted_cat = true;
					
					break;
				}
				
				if ( $restricted_cat && in_array( $location, $restricted_states ) )
					$show_message = true;
				
			}

			if ( in_array( $location, $restricted_states ) ) {
			
				$show_message = true;
				
			}

			if ( $show_message ) {

				$title = get_the_title( $i['product_id'] );
				
				if ( $i['data']->is_type('variation') ) {
					$variation_attributes = $i['data']->get_variation_attributes();
					
					foreach( $variation_attributes as $id => $name )
						$variation_attributes[ $id ] = ucwords( $name );
						
					$title .= ' &ndash; ' . implode( ', ', $variation_attributes ); 
				}


				$settings = get_option( 'woocommerce_ign_restrict_shipping_settings' );
		
				if ( empty( $settings['message'] ) )
					$msg = '<strong>We cannot ship "%p" to %d. Please <a href="%u">return to your cart</a> and remove the item, or change your shipping address</strong>';
				else
					$msg = $settings['message'];
					
				$msg = str_replace( '%p', $title, $msg );
				$msg = str_replace( '%d', $location_name, $msg );
				$msg = str_replace( '%u', $cpid, $msg );

				if ( function_exists( 'wc_add_notice' ) ) 
					wc_add_notice( $msg, $notice_type );
				else 
					$woocommerce->add_error( $msg );
					
				$this->error_msg = $msg;
			}

		}

	}

}

$GLOBALS['ignitewoo_restrict_shipping'] = new IgniteWoo_Restrict_Shipping();
















if ( ! function_exists( 'ignitewoo_queue_update' ) )
	require_once( dirname( __FILE__ ) . '/ignitewoo_updater/ignitewoo_update_api.php' );

$this_plugin_base = plugin_basename( __FILE__ );

add_action( "after_plugin_row_" . $this_plugin_base, 'ignite_plugin_update_row', 1, 2 );

ignitewoo_queue_update( plugin_basename( __FILE__ ), 'e4e2a2d0d047af189cda5e026c318eb4', '4867' );