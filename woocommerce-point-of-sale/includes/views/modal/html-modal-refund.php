<div class="md-modal md-dynamicmodal md-close-by-overlay md-register" id="modal-refund">
    <div class="md-content">
        <h1><?php _e('Refund', 'wc_point_of_sale'); ?><span class="md-close"></span></h1>
        <div class="full-height">
            <div class="two_cols">
                <div class="">
                    <form action="" method="post" id="put_wc_pos_barcode">
                        <p class="form-row form-row-wide">
                            <label for="product_barcode"><?php _e("Search Order Number", 'wc_point_of_sale'); ?></label>
                            <input type="text" id="product_barcode" name="product_barcode" value="" minlength="1" >
                            <input type="submit" value="<?php _e("Find Order Number", 'wc_point_of_sale'); ?>" class="button" id="find_product_by_barcode">
                        </p>
                    </form>
                </div>
                <div id="poststuff_stock" style="display: none">
                    <input type="hidden" name="order_id" id="order_id" value="">
                    <table class="wp-list-table widefat striped posts" id="barcode_options_table">
                        <thead>
                        <tr>
                            <th scope="col" id="thumb" class="manage-column column-thumb"><span class="stock_page_image tips" data-tip="<?php _e("Image", 'wc_point_of_sale'); ?>"></span></th>
                            <th scope="col" id="name" class="manage-column column-name"><?php _e("Name", 'wc_point_of_sale'); ?></th>
                            <th scope="col" id="is_in_stock" class="manage-column column-is_in_stock"><?php _e("Stock", 'wc_point_of_sale'); ?></th>
                            <th scope="col" id="price" class="manage-column column-price"><?php _e("Price", 'wc_point_of_sale'); ?></th>
                            <th scope="col" id="stock_val" class="manage-column column-price"><?php _e("Update Stock", 'wc_point_of_sale'); ?></th>
                            <th scope="col" id="increase" class="manage-column column-price"><?php _e("Action", 'wc_point_of_sale'); ?></th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
            <div id="refund_message">

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="wrap-button">
            <button class="button button-primary wp-button-large alignright" type="button" id="load_refund_products"><?php _e('Refund Product', 'wc_point_of_sale'); ?></button>
        </div>
    </div>
</div>

<script>

    var pos_ready = function () {
        jQuery(document).ready(function($){
            var container = $("#poststuff_stock");
            $(document).anysearch({
                searchSlider: false,
                isBarcode: function(barcode) {
                    filter_product(barcode);
                },
                searchFunc: function(search) {
                    filter_product(search);
                },
            });
            $("#message").fadeOut();
            container.fadeOut();
            $("#put_wc_pos_barcode").on('submit', function(e) {
                if( $('#product_barcode').val() != '' ) {
                    $("#message").fadeOut();
                    var barcode = $('#product_barcode').val();
                    filter_order(barcode);
                }
                return false;
            });

            container.on('click', '.increase_stock, .decrease_stock', function(e) {
                e.preventDefault();
                var id = $(this).parents('tr').data('product');
                var operation = 'increase';
                var value = $('.stock_value_' + id).val();
                var order_id = $("#order_id").val();

                if($(this).is('.decrease_stock')) operation = 'decrease';

                if( value > 0 ) {
                    $("#message").fadeOut();
                    $('#wc_pos_stock_controller').block({
                        message: null,
                        overlayCSS: {
                            background: '#fff',
                            opacity: 0.6
                        }
                    });

                    data = {
                        action: 'wc_pos_change_stock',
                        id: id,
                        value: value,
                        operation: operation,
                        order_id: order_id
                    };
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        url: ajaxurl,
                        data: data,
                        success: function (data, textStatus, XMLHttpRequest) {
                            if( data.status == 'success' && data.response ) {
                                update_sku_controller_table (data.response);
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            APP.showNotice(errorThrown, 'error');
                        },
                        complete : function (argument) {
                            $('#wc_pos_stock_controller').unblock();
                        }
                    });
                }
            });

            function decrease_stock () {
                var id = $('#product_id').val();
                var operation = 'decrease';
                var value = 1;

                if (value > 0) {
                    $("#message").fadeOut();
                    $('#wc_pos_stock_controller').block({
                        message: null, overlayCSS: {
                            background: '#fff', opacity: 0.6
                        }
                    });

                    data = {
                        action: 'wc_pos_change_stock', id: id, value: value, operation: operation
                    };
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        url: ajaxurl,
                        data: data,
                        success: function (data, textStatus, XMLHttpRequest) {
                            if (data.status == 'success' && data.response) {
                                update_sku_controller_table(data.response);
                            }
                        },
                        error: function (MLHttpRequest, textStatus, errorThrown) {
                        },
                        complete: function (argument) {
                            $('#wc_pos_stock_controller').unblock();
                        }
                    });
                }
            }

            function filter_product(barcode)
            {
                $('#wc_pos_stock_controller').block({
                    message: null,
                    overlayCSS: {
                        background: '#fff',
                        opacity: 0.6
                    }
                });
                data = {
                    action: 'wc_pos_filter_product_barcode',
                    barcode: barcode
                };
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: ajaxurl,
                    data: data,
                    success: function (data, textStatus, XMLHttpRequest) {
                        if( data.status == 'success' && data.response ) {
                            update_sku_controller_table (data.response);
                        } else {
                            $("#message").html("Product wasn't found.");
                            $("#message").fadeIn();
                            $("#poststuff_stock").fadeOut();
                        }
                        decrease_stock();
                    },
                    error: function (MLHttpRequest, textStatus, errorThrown) {
                    },
                    complete : function (argument) {
                        $('#wc_pos_stock_controller').unblock();
                    }
                });
                $('#product_barcode').val('');
            }

            function update_sku_controller_table (object) {
                //#decrease_stock
                if($.isArray(object)){
                    $.each(object, function (i, product) {
                        var stock_status = "In stock";
                        if(product.stock_status == "outofstock")
                            stock_status = "Out of stock";

                        var stock_html = '<mark class="'+product.stock_status+'">'+stock_status+'</mark>';
                        if(product.stock_status == "instock")
                            stock_html += ' &times; ' + (!product.stock_quantity ? 0 : product.stock_quantity );

                        var $html = "";
                        $html += '<tr data-product="' + product.id + '">';
                        $html += '<td class="thumb column-thumb" data-colname="Image">' +
                            '<div class="product_image product_image_' + product.id + '">' + product.image + '</div>' +
                            '</td>';
                        $html += '<td class="column-name name" data-colname="Name">' +
                            '<span class="product_name product_name_' + product.id + '">' + product.name + '</span><br/>' +
                            '<small class="product_sku product_sku_' + product.id + '">' + product.sku + '</small><br/>' +
                            '</td>';
                        $html += '<td class="stock column-stock" data-colname="Stock">' +
                            '<div class="product_stock product_stock_' + product.id + '">' + stock_html + '</div>' +
                            '</td>';
                        $html += '<td class="price column-price" data-colname="Price">' +
                            '<div class="product_price product_price_' + product.id + '">' + product.price + '</div>' +
                            '</td>';
                        $html += '<td class="price column-stock-value" data-colname="Stock Value">' +
                            '<input class="stock_value stock_value_' + product.id + '">' +
                            '</td>';
                        $html += '<td class="price column-actions" data-colname="Increase">' +
                            '<input type="submit" value="+" class="increase_stock">' +
                            '<input type="submit" value="-" class="decrease_stock">' +
                            '</td>';
                        container.find('tbody').append($html);
                    });
                }else{
                    $(".product_name_" + object.id).html(object.name);
                    $(".product_sku_" + object.id).html(object.sku);
                    $(".product_image_" + object.id).html(object.image);
                    $(".product_price_" + object.id).html(object.price);
                    $(".product_stock_" + object.id).html(object.stock_status);
                    $('.stock_value_' + object.id).val('');
                }
                container.fadeIn();
            }

            function filter_order(order_id){
                container.find('tbody').html("");
                $('#wc_pos_stock_controller').block({
                    message: null,
                    overlayCSS: {
                        background: '#fff',
                        opacity: 0.6
                    }
                });
                data = {
                    action: 'wc_pos_filter_order_barcode',
                    barcode: order_id
                };
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: ajaxurl,
                    data: data,
                    success: function (data, textStatus, XMLHttpRequest) {
                        if( data.success && data.data.length > 0 ) {
                            update_sku_controller_table (data.data);
                            $("#order_id").val(order_id);
                        } else {
                            $("#message").html("Order wasn't found.");
                            $("#message").fadeIn();
                            $("#poststuff_stock").fadeOut();
                        }
                        decrease_stock();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $("#order_id").val("");
                        APP.showNotice(jqXHR.responseJSON.data, 'error');
                    },
                    complete : function (argument) {
                        $('#wc_pos_stock_controller').unblock();
                    }
                });
                $('#product_barcode').val('');
            }

        });

    }

    document.addEventListener("DOMContentLoaded", pos_ready);

</script>
