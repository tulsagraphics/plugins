<?php
/**
 * Functions for updating data, used by the background updater.
 *
 * @author      WooThemes
 * @package     WC_OD
 * @since       1.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Add the shipping date to the not completed orders with delivery date.
 *
 * @global wpdb $wpdb The WordPress Database Access Abstraction Object.
 */
function wc_od_update_140_shipping_dates() {
	global $wpdb;

	$results = $wpdb->get_results( $wpdb->prepare( "
		SELECT meta1.post_id AS order_id, meta1.meta_value AS delivery_date
		FROM {$wpdb->posts} AS posts, {$wpdb->postmeta} AS meta1
		WHERE posts.id = meta1.post_id
			AND post_type = 'shop_order'
			AND post_status IN ( 'wc-pending', 'wc-on-hold', 'wc-processing' )
			AND meta1.meta_key = '_delivery_date'
			AND meta1.meta_value >= %s
			AND NOT EXISTS (
				SELECT * FROM {$wpdb->postmeta} AS meta2
				WHERE meta1.post_id = meta2.post_id
				AND meta2.meta_key = '_shipping_date'
			)
	", wc_od_get_local_date( false ) ) );

	foreach ( $results as $order_data ) {
		$shipping_timestamp = wc_od_get_last_shipping_date( array(
			'delivery_date' => $order_data->delivery_date,
			'disabled_delivery_days_args' => array(
				'type'    => 'delivery',
				'country' => get_post_meta( $order_data->order_id, '_shipping_country', true ),
				'state'   => get_post_meta( $order_data->order_id, '_shipping_state', true ),
			),
		) );

		if ( $shipping_timestamp ) {
			$shipping_date = wc_od_localize_date( $shipping_timestamp, 'Y-m-d' );
			update_post_meta( $order_data->order_id, '_shipping_date', $shipping_date, true );
		}
	}
}

/**
 * Update DB Version.
 */
function wc_od_update_140_db_version() {
	WC_OD_Install::update_db_version( '1.4.0' );
}

/**
 * Execute the migration 1.4.0 again to calculate the missing shipping dates from the renewal orders.
 */
function wc_od_update_141_shipping_dates() {
	wc_od_update_140_shipping_dates();
}

/**
 * Update DB Version.
 */
function wc_od_update_141_db_version() {
	WC_OD_Install::update_db_version( '1.4.1' );
}
