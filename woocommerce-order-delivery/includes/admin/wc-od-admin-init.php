<?php
/**
 * Admin Init
 *
 * @author      WooThemes
 * @package     WC_OD
 * @since       1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Admin init.
 *
 * @since 1.0.0
 */
function wc_od_admin_init() {
	if ( defined( 'DOING_AJAX' ) ) {
		include_once( 'wc-od-admin-ajax-functions.php' );
	}

	// The WooCommerce settings page.
	if ( WC_OD_Utils::is_woocommerce_settings_page() ) {
		include_once( 'class-wc-od-admin-settings.php' );
	}
}
add_action( 'admin_init', 'wc_od_admin_init' );

/**
 * Looks at the current screen and loads the correct list table handler.
 *
 * Based on the method WC_Admin_Post_Types::setup_screen().
 *
 * @since 1.4.0
 */
function wc_od_admin_setup_screen() {
	$screen_id = false;

	if ( function_exists( 'get_current_screen' ) ) {
		$screen    = get_current_screen();
		$screen_id = isset( $screen, $screen->id ) ? $screen->id : '';
	}

	if ( ! empty( $_REQUEST['screen'] ) ) {
		$screen_id = wc_clean( wp_unslash( $_REQUEST['screen'] ) );
	}

	switch ( $screen_id ) {
		case 'edit-shop_order':
			include_once 'list-table/class-wc-od-admin-list-table-orders.php';
			new WC_OD_Admin_List_Table_Orders();
			break;
	}

	// Ensure the table handler is only loaded once. Prevents multiple loads if a plugin calls check_ajax_referer many times.
	remove_action( 'current_screen', 'wc_od_admin_setup_screen', 20 );
	remove_action( 'check_ajax_referer', 'wc_od_admin_setup_screen', 20 );
}
add_action( 'current_screen', 'wc_od_admin_setup_screen', 20 );
add_action( 'check_ajax_referer', 'wc_od_admin_setup_screen', 20 );


/** Shop Orders functions *****************************************************/


/**
 * Updates the columns in the shop order list.
 *
 * Added 'shipping_date' column in the version 1.4.0.
 *
 * @since 1.0.0
 *
 * @param array $columns The shop order columns.
 * @return array The modified shop order columns.
 */
function wc_od_admin_shop_order_columns( $columns ) {
	$index = array_search( 'order_date', array_keys( $columns ) );
	$modified_columns = array_slice( $columns, 0, $index );
	$modified_columns['shipping_date'] = __( 'Shipping Date', 'woocommerce-order-delivery' );
	$modified_columns['delivery_date'] = __( 'Delivery Date', 'woocommerce-order-delivery' );
	$modified_columns = array_merge( $modified_columns, array_slice( $columns, $index ) );

	return $modified_columns;
}
add_filter( 'manage_edit-shop_order_columns', 'wc_od_admin_shop_order_columns', 20 );

/**
 * Updates the sortable columns in the shop order list.
 *
 * Added 'shipping_date' column in the version 1.4.0.
 *
 * @since 1.0.0
 *
 * @param array $columns The sortable columns list.
 * @return array The filtered sortable columns list.
 */
function wc_od_admin_shop_order_sort_columns( $columns ) {
	$columns['shipping_date'] = 'shipping_date';
	$columns['delivery_date'] = 'delivery_date';

	return $columns;
}
add_filter( "manage_edit-shop_order_sortable_columns", 'wc_od_admin_shop_order_sort_columns' );

/**
 * Prints the content for the custom orders columns.
 *
 * @since 1.0.0
 *
 * @global WP_Post $post The current post.
 *
 * @param string $column_id The column ID.
 */
function wc_od_admin_shop_order_posts_column( $column_id ) {
	global $post;

	if ( in_array( $column_id, array( 'shipping_date', 'delivery_date' ) ) ) {
		$date = wc_od_get_order_meta( $post->ID, "_{$column_id}" );

		if ( $date ) {
			printf(
				'<time datetime="%1$s" title="%2$s">%3$s</time>',
				esc_attr( wc_od_localize_date( $date, 'c' ) ),
				esc_html( wc_od_localize_date( $date, get_option( 'date_format' ) ) ),
				esc_html( wc_od_localize_date( $date, wc_od_get_date_format( 'admin' ) ) )
			);
		} else {
			echo '<span class="na">–</span>';
		}
	}
}
add_action( 'manage_shop_order_posts_custom_column', 'wc_od_admin_shop_order_posts_column', 20 );

/**
 * Adds the query vars for order by our custom columns.
 *
 * @since 1.0.0
 *
 * @global string $typenow The current post type.
 *
 * @param array $vars The query vars.
 * @return array The filtered query vars.
 */
function wc_od_admin_shop_order_orderby( $vars ) {
	global $typenow;

	if ( 'shop_order' !== $typenow ) {
		return $vars;
	}

	// Sorting
	if ( isset( $vars['orderby'] ) ) {
		if ( in_array( $vars['orderby'], array( 'shipping_date', 'delivery_date' ) ) ) {
			$vars = array_merge( $vars, array(
				'meta_key' => "_{$vars['orderby']}",
				'orderby'  => 'meta_value_num',
			) );
		}
	}

	return $vars;
}
add_filter( 'request', 'wc_od_admin_shop_order_orderby' );

/**
 * Filters the order by query for cast the meta_value as date.
 *
 * @since 1.0.0
 *
 * @global wpdb $wpdb The WordPress Database Access Abstraction Object.
 *
 * @param string $orderby The orderby query.
 * @param array $query    The query parameters.
 * @return string The filtered orderby query.
 */
function wc_od_admin_posts_orderby_date( $orderby, $query ) {
    global $wpdb;

	if ( 'shop_order' === $query->get( 'post_type' ) && in_array( $query->get( 'meta_key' ), array( '_shipping_date', '_delivery_date' ) ) ) {
		$orderby = "CAST( $wpdb->postmeta.meta_value AS DATE ) " . $query->get( 'order' );
	}

	return $orderby;
}
add_filter( 'posts_orderby', 'wc_od_admin_posts_orderby_date', 10, 2 );


/** Edit Order functions ******************************************************/


/**
 * Adds the date fields to the 'Order Details' metabox.
 *
 * @since 1.0.0
 *
 * @param WC_Order $order The order.
 */
function wc_od_admin_order_data_after_order_details( $order ) {
	/**
	 * Filter the fields to add in the order details section.
	 *
	 * @since 1.4.0
	 *
	 * @param array    $fields An array with the fields data.
	 * @param WC_Order $order  The order instance.
	 */
	$dates = apply_filters( 'wc_od_admin_order_details_fields', array(
		'shipping_date' => array(
			'label' => __( 'Shipping date:', 'woocommerce-order-delivery' ),
			'class' => array( 'form-field' ),
		),
		'delivery_date' => array(
			'label' => __( 'Delivery date:', 'woocommerce-order-delivery' ),
			'class' => array( 'form-field', 'last' ),
		),
	), $order );

	foreach ( $dates as $key => $data ) {
		$value = wc_od_localize_date( wc_od_get_order_meta( $order, "_{$key}" ), 'Y-m-d' );
		$class = ( ( isset( $data['class'] ) && is_array( $data['class'] ) ) ? join( ' ', $data['class'] ) : '' );

		/**
		 * Filter the label of the date field in the order details.
		 *
		 * The dynamic portion of the hook name, `$key`, refers to the field name. Since 1.4.0.
		 *
		 * @since 1.1.0
		 * @deprecated 1.4.0 Use the wc_od_admin_order_details_fields hook instead.
		 *
		 * @param string   $label The field label.
		 * @param WC_Order $order The order instance.
		 */
		$label = apply_filters( "wc_od_admin_{$key}_field_label", $data['label'], $order );
		?>
		<p class="<?php echo esc_attr( $class ); ?>">
			<label for="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $label ) ?></label>
			<input id="<?php echo esc_attr( $key ); ?>"
			       class="date-picker-field"
			       type="text"
			       name="_<?php echo esc_attr( $key ); ?>"
			       value="<?php echo esc_attr( $value ); ?>"
			       maxlength="10"
			       pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" />
		</p>
		<?php
	}
}
add_action( 'woocommerce_admin_order_data_after_order_details', 'wc_od_admin_order_data_after_order_details' );

/**
 * Saves the date fields on the edit-order page.
 *
 * The dates are saved before the address data (Priority 40).
 * You should use the $_POST variable to get the updated information.
 *
 * The change of the order status and the email notification is sent with priority 40. To attach the
 * dates correctly to the emails, we have to use a lower priority.
 *
 * @since 1.0.0
 *
 * @param int $order_id The order ID.
 */
function wc_od_admin_process_shop_order_meta( $order_id ) {
	// Process the delivery date field.
	$posted_delivery_date  = ( isset( $_POST['_delivery_date'] ) ? (string) wc_od_localize_date( sanitize_text_field( $_POST[ '_delivery_date' ] ), 'Y-m-d' ) : '' );
	$delivery_date_changed = ( $posted_delivery_date !== (string) wc_od_get_order_meta( $order_id, '_delivery_date' ) );

	if ( $posted_delivery_date ) {
		wc_od_update_order_meta( $order_id, '_delivery_date', $posted_delivery_date, true );
	} else {
		wc_od_delete_order_meta( $order_id, '_delivery_date', true );
	}

	// Process the shipping date field.
	$posted_shipping_date  = ( isset( $_POST['_shipping_date'] ) ? (string) wc_od_localize_date( sanitize_text_field( $_POST[ '_shipping_date' ] ), 'Y-m-d' ) : '' );
	$shipping_date_changed = ( $posted_shipping_date !== (string) wc_od_get_order_meta( $order_id, '_shipping_date' ) );

	// Shipping date not changed manually by the merchant.
	if ( ! $shipping_date_changed && $delivery_date_changed ) {
		if ( $posted_delivery_date ) {
			// This info is updated in the WC_Meta_Box_Order_Data::save() method with priority 40.
			$shipping_country = ( isset( $_POST['_shipping_country'] ) ? wc_clean( $_POST['_shipping_country'] ) : wc_od_get_order_prop( $order_id, 'shipping_country' ) );
			$shipping_state   = ( isset( $_POST['_shipping_state'] ) ? wc_clean( $_POST['_shipping_state'] ) : wc_od_get_order_prop( $order_id, 'shipping_state' ) );

			// Re-calculate the shipping date.
			$posted_shipping_date = wc_od_get_last_shipping_date( array(
				'delivery_date' => $posted_delivery_date,
				'disabled_delivery_days_args' => array(
					'type'    => 'delivery',
					'country' => $shipping_country,
					'state'   => $shipping_state,
				)
			), 'edit-order' );

			if ( $posted_shipping_date ) {
				$posted_shipping_date = wc_od_localize_date( $posted_shipping_date, 'Y-m-d' );

				WC_Admin_Notices::add_custom_notice( 'shipping_date_updated', __( 'The shipping date have been updated according to the new delivery date.', 'woocommerce-order-delivery' ) );
			} else {
				WC_Admin_Notices::add_custom_notice( 'shipping_date_not_found', __( "We couldn't find a shipping date according to the delivery date.", 'woocommerce-order-delivery' ) );
			}
		} else {
			// No delivery date. Remove also the shipping date.
			$posted_shipping_date = '';
		}
	}

	if ( $posted_shipping_date ) {
		wc_od_update_order_meta( $order_id, '_shipping_date', $posted_shipping_date, true );
	} else {
		wc_od_delete_order_meta( $order_id, '_shipping_date', true );
	}
}
add_action( 'woocommerce_process_shop_order_meta', 'wc_od_admin_process_shop_order_meta', 35 );
