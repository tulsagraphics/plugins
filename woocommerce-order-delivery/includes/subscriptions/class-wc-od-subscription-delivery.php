<?php
/**
 * Class to manage the delivery preferences of a subscription.
 *
 * @author     WooThemes
 * @package    WC_OD
 * @since      1.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WC_OD_Subscription_Delivery' ) ) {

	class WC_OD_Subscription_Delivery {

		/**
		 * Constructor.
		 *
		 * @since 1.3.0
		 */
		public function __construct() {
			add_action( 'wc_od_install_add_endpoints', array( $this, 'add_endpoints' ) );
			add_filter( 'query_vars', array( $this, 'query_vars' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

			// View Subscription hooks.
			add_filter( 'wcs_view_subscription_actions', array( $this, 'view_subscription_actions' ), 10, 2 );
			add_action( 'woocommerce_after_template_part', array( $this, 'insert_delivery_content' ) );

			// Edit Delivery hooks.
			add_filter( 'the_title', array( $this, 'edit_delivery_title' ) );
			add_filter( 'wc_get_template', array( $this, 'edit_delivery_template' ), 10, 4 );
			add_action( 'woocommerce_account_edit-delivery_endpoint', array( $this, 'edit_delivery_content' ) );
			add_action( 'template_redirect', array( $this, 'save_delivery' ) );
			add_filter( 'woocommerce_form_field_wc_od_subscription_delivery_days', 'wc_od_subscription_delivery_days_field', 10, 3 );

			add_filter( 'wc_od_validate_subscription_field_delivery_date', array( $this, 'validate_delivery_date' ), 10, 3 );
			add_filter( 'wc_od_sanitize_subscription_field_delivery_date', array( $this, 'sanitize_delivery_date' ) );
			add_filter( 'wc_od_sanitize_subscription_field_delivery_days', array( $this, 'sanitize_delivery_days' ) );
			add_action( 'wc_od_updated_subscription_fields', array( $this, 'updated_subscription_delivery' ), 10, 3 );
		}

		/**
		 * Registers the custom endpoints.
		 *
		 * @since 1.3.0
		 */
		public function add_endpoints() {
			add_rewrite_endpoint( 'edit-delivery', EP_ROOT | EP_PAGES );
		}

		/**
		 * Add custom query vars.
		 *
		 * @since 1.3.0
		 *
		 * @param array $vars The query vars.
		 * @return array An array with the query vars.
		 */
		public function query_vars( $vars ) {
			$vars[] = 'edit-delivery';

			return $vars;
		}

		/**
		 * Enqueue scripts.
		 *
		 * @since 1.3.0
		 */
		public function enqueue_scripts() {
			if ( wc_od_is_edit_delivery_endpoint() ) {
				wc_od_enqueue_datepicker( 'subscription' );
				wp_enqueue_script( 'wc-od-subscription', WC_OD_URL . 'assets/js/wc-od-subscription.js', array( 'jquery', 'wc-od-datepicker' ), WC_OD_VERSION, true );
				wp_localize_script( 'wc-od-subscription', 'wc_od_subscription_l10n', $this->get_calendar_settings( wc_od_get_current_subscription_id() ) );
			}
		}

		/**
		 * Gets the calendar settings.
		 *
		 * @since 1.3.0
		 *
		 * @param mixed $the_subscription Post object or post ID of the subscription. Use the current subscription if empty.
		 * @return array An array with the calendar settings.
		 */
		public function get_calendar_settings( $the_subscription = null ) {
			$subscription = wcs_get_subscription( ( $the_subscription ? $the_subscription : wc_od_get_current_subscription_id() ) );

			$seconds_in_a_day = 86400;
			$date_format      = wc_od_get_date_format( 'php' );
			$args             = wc_od_get_subscription_delivery_date_args( $subscription );

			return wc_od_get_calendar_settings( array(
				'startDate'          => wc_od_localize_date( $args['start_date'], $date_format ),
				'endDate'            => wc_od_localize_date( ( wc_od_get_timestamp( $args['end_date'] ) - $seconds_in_a_day ), $date_format ), // Inclusive.
				'daysOfWeekDisabled' => array_keys( wc_od_get_days_by( $args['delivery_days'], 'enabled', '0' ) ),
				'datesDisabled'      => wc_od_get_disabled_days( $args['disabled_days_args'], 'subscription' ),
			), 'subscription' );
		}

		/**
		 * Filter the actions in the view-subscription page.
		 *
		 * @since 1.3.0
		 *
		 * @param array           $actions      The subscription actions.
		 * @param WC_Subscription $subscription The subscription object.
		 * @return array An array with the subscription actions.
		 */
		public function view_subscription_actions( $actions, $subscription ) {
			if ( wc_od_subscription_has_delivery_preferences( $subscription ) ) {
				$actions['change_delivery'] = array(
					'url'  => wc_od_edit_delivery_endpoint( wc_od_get_order_prop( $subscription, 'id' ) ),
					'name' => __( 'Change Delivery', 'woocommerce-order-delivery' ),
				);
			}

			return $actions;
		}

		/**
		 * Inserts the delivery content at the end of the view-subscription page.
		 *
		 * @since 1.3.0
		 *
		 * @param string $template_name The template name.
		 */
		public function insert_delivery_content( $template_name ) {
			if ( 'myaccount/view-subscription.php' === $template_name ||
				( 'myaccount/my-account.php' === $template_name && version_compare( WC()->version, '2.6', '<' ) &&
					wcs_is_view_subscription_page()
				)
			) {
				$this->view_subscription_delivery_content();
			}
		}

		/**
		 * Prints the delivery content.
		 *
		 * @since 1.3.0
		 */
		public function view_subscription_delivery_content() {
			$subscription_id = intval( wc_od_get_current_subscription_id() );
			$subscription = wcs_get_subscription( $subscription_id );

			if ( ! $subscription || ! wc_od_user_has_subscription_delivery_caps( $subscription ) ||
				! wc_od_subscription_needs_delivery_details( $subscription ) ) {
				return;
			}

			$delivery_date = false;
			$args = array(
				'subscription' => $subscription,
			);

			if ( wc_od_subscription_needs_delivery_date( $subscription ) ) {
				$delivery_date = wc_od_get_order_meta( $subscription, '_delivery_date' );
				if ( $delivery_date ) {
					$args['delivery_date'] = wc_od_localize_date( $delivery_date );
				}
			}

			if ( ! $delivery_date ) {
				$args = array_merge( $args, array(
					'shipping_date'  => wc_od_localize_date( wc_od_get_subscription_first_shipping_date( $subscription_id ) ),
					'delivery_range' => WC_OD()->settings()->get_setting( 'delivery_range' ),
				) );
			}

			wc_od_order_delivery_details( $args );
		}

		/**
		 * Change the edit-delivery endpoint title.
		 *
		 * @since 1.3.0
		 *
		 * @param string $title The title.
		 * @return string The endpoint title.
		 */
		public function edit_delivery_title( $title ) {
			if ( ! is_admin() && is_main_query() && in_the_loop() && wc_od_is_edit_delivery_endpoint() ) {
				$title = __( 'Delivery preferences', 'woocommerce-order-delivery' );

				remove_filter( 'the_title', array( $this, 'edit_delivery_title' ) );
			}

			return $title;
		}

		/**
		 * Set the template location for the edit-delivery page.
		 *
		 * Backward compatibility with WC 2.5.
		 *
		 * @since 1.3.0
		 *
		 * @param string $located       The template location.
		 * @param string $template_name The template name.
		 * @param array  $args          The template arguments.
		 * @param string $template_path The template path.
		 * @return string The template location.
		 */
		public function edit_delivery_template( $located, $template_name, $args, $template_path ) {
			if ( 'myaccount/my-account.php' == $template_name && wc_od_is_edit_delivery_endpoint() && version_compare( WC()->version, '2.6', '<' ) ) {
				$located = wc_locate_template( 'myaccount/edit-delivery.php', $template_path, WC_OD_PATH . 'templates/' );
			}

			return $located;
		}

		/**
		 * Prints the content for the edit-delivery page.
		 *
		 * @since 1.3.0
		 */
		public function edit_delivery_content() {
			// TODO: Move the template arguments here when the minimum WC version is 2.6+.
			wc_od_get_template( 'myaccount/edit-delivery.php' );
		}

		/**
		 * Save the subscription delivery preferences.
		 *
		 * @since 1.3.0
		 */
		public function save_delivery() {
			if ( 'POST' !== strtoupper( $_SERVER[ 'REQUEST_METHOD' ] ) || ! isset( $_POST['subscription_id'] ) ||
				! isset( $_POST[ 'action' ] ) || 'edit_delivery' !== $_POST[ 'action' ] ||
				! isset( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'wc_od_edit_delivery' )
			) {
				return;
			}

			$subscription = wcs_get_subscription( intval( $_POST['subscription_id'] ) );
			if ( ! $subscription || ! wc_od_user_has_subscription_delivery_caps( $subscription ) ||
				! wc_od_subscription_has_delivery_preferences( $subscription ) ) {
				return;
			}

			$fields = wc_od_get_subscription_delivery_fields( $subscription );
			$values = array();
			$valid  = true;

			foreach ( $fields as $key => $field ) {
				$value = null;

				// Validate required.
				if ( $field['required'] && ( ! isset( $_POST[ $key ] ) || '' === $_POST[ $key ] ) ) {
					wc_add_notice( sprintf( __( '<strong>%s</strong> is a required field.', 'woocommerce-order-delivery' ), $field['label'] ), 'error' );
					$valid = false;
				} else {
					$value = wc_clean( $_POST[ $key ] );

					/**
					 * Validate the subscription delivery field.
					 *
					 * NOTE: Return Null to abort the save process.
					 *
					 * @since 1.3.0
					 *
					 * @param mixed           $value        The field value.
					 * @param array           $field        The field parameters.
					 * @param WC_Subscription $subscription The subscription instance.
					 */
					$value = apply_filters( "wc_od_validate_subscription_field_{$key}", $value, $field, $subscription );

					if ( is_null( $value ) ) {
						$valid = false;
					}
				}

				$values[ $key ] = $value;
			}

			if ( ! $valid ) {
				$values = null;
			}

			/**
			 * Global validation for the subscription delivery fields.
			 *
			 * NOTE: Return Null to abort the save process.
			 *
			 * @since 1.3.0
			 *
			 * @param array|null      $values       An array with the fields values. Null if at least one single field validation failed.
			 * @param array           $fields       The fields parameters.
			 * @param WC_Subscription $subscription The subscription instance.
			 */
			$values = apply_filters( 'wc_od_validate_subscription_fields', $values, $fields, $subscription );

			if ( ! is_null( $values ) ) {
				// Sanitize and save the fields.
				foreach ( $fields as $key => $field ) {
					/**
					 * Sanitize the subscription delivery field.
					 *
					 * @since 1.3.0
					 *
					 * @param mixed           $value        The field value.
					 * @param array           $field        The field parameters.
					 * @param WC_Subscription $subscription The subscription instance.
					 */
					$values[ $key ] = apply_filters( "wc_od_sanitize_subscription_field_{$key}", $values[ $key ], $field, $subscription );

					wc_od_update_order_meta( $subscription, "_{$key}", $values[ $key ], true );
				}

				$previous_values = wp_list_pluck( $fields, 'value' );

				/**
				 * Fires immediately after updating the delivery preferences of a subscription.
				 *
				 * @since 1.3.0
				 *
				 * @param array           $values       The fields values.
				 * @param array           $previous     The previous fields values.
				 * @param WC_Subscription $subscription The subscription instance.
				 */
				do_action( 'wc_od_updated_subscription_fields', $values, $previous_values, $subscription );

				wc_add_notice( __( 'Delivery preferences changed successfully.', 'woocommerce-order-delivery' ) );

				wp_safe_redirect( $subscription->get_view_order_url() );
				exit;
			}
		}

		/**
		 * Validates the delivery_date field.
		 *
		 * @since 1.3.0
		 *
		 * @param mixed           $value        The field value.
		 * @param array           $field        The field arguments.
		 * @param WC_Subscription $subscription The subscription instance.
		 * @return mixed|null The field value. Null on failure.
		 */
		public function validate_delivery_date( $value, $field, $subscription ) {
			if ( is_null( $value ) || ! wc_od_validate_subscription_delivery_date( $subscription, $value ) ) {
				$value = null;

				wc_add_notice( __( '<strong>Delivery Date</strong> is not a valid date.', 'woocommerce-order-delivery' ), 'error' );
			}

			return $value;
		}

		/**
		 * Sanitizes the subscription delivery_date field.
		 *
		 * @since 1.3.0
		 *
		 * @param mixed $value The field value.
		 * @return string The sanitized field value.
		 */
		public function sanitize_delivery_date( $value ) {
			// Stores the date in the ISO 8601 format.
			return (string) wc_od_localize_date( sanitize_text_field( $value ), 'Y-m-d' );
		}

		/**
		 * Sanitizes the subscription delivery_days field.
		 *
		 * @since 1.3.0
		 *
		 * @param mixed $value The field value.
		 * @return array The sanitized field value.
		 */
		public function sanitize_delivery_days( $value ) {
			$clean_value = array();
			$delivery_days = WC_OD()->settings()->get_setting( 'delivery_days' );

			foreach ( $delivery_days as $index => $day ) {
				$preferred_day = ( isset( $value[ $index ] ) ? $value[ $index ] : $day );
				$enabled = ( (bool) $day['enabled'] && isset( $value[ $index ] ) && isset( $value[ $index ]['enabled'] ) );
				$preferred_day['enabled'] = ( $enabled ? '1' : '0' );

				$clean_value[ $index ] = $preferred_day;
			}

			return $clean_value;
		}

		/**
		 * Process the updated delivery fields of a subscription.
		 *
		 * @since 1.3.0
		 *
		 * @param array           $values       The fields values.
		 * @param array           $previous     The previous fields values.
		 * @param WC_Subscription $subscription The subscription instance.
		 */
		public function updated_subscription_delivery( $values, $previous, $subscription ) {
			if ( $values['delivery_date'] !== $previous['delivery_date'] ) {
				// Adds an internal note to the subscription to notify to the merchant.
				wc_od_add_order_note( $subscription, sprintf(
					__( 'The customer changed the delivery date for the next order to: %s', 'woocommerce-order-delivery' ),
					'<strong>' . wc_od_localize_date( $values['delivery_date'] ) . '</strong>'
				) );
			}
		}
	}
}

return new WC_OD_Subscription_Delivery();
