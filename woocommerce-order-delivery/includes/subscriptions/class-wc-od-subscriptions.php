<?php
/**
 * Class to add compatibility with the WooCommerce Subscriptions extension.
 *
 * @author     WooThemes
 * @package    WC_OD
 * @since      1.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( version_compare( get_option( 'woocommerce_subscriptions_active_version' ), '2.2', '<' ) ) {
	add_action( 'admin_notices', 'wc_od_subscriptions_requirements_notice' );
	return;
}

/**
 * Displays an admin notice when the minimum requirements are not satisfied for the Subscriptions extension.
 *
 * @since 1.4.1
 */
function wc_od_subscriptions_requirements_notice() {
	if ( current_user_can( 'activate_plugins' ) ) :
		$message = sprintf( __( '<strong>WooCommerce Order Delivery</strong> requires WooCommerce Subscriptions %s or higher.', 'woocommerce-order-delivery' ), '2.2' );

		printf( '<div class="error"><p>%s</p></div>', wp_kses_post( $message ) );
	endif;
}

if ( ! class_exists( 'WC_OD_Subscriptions' ) ) {

	class WC_OD_Subscriptions {

		/**
		 * Constructor.
		 *
		 * @since 1.3.0
		 */
		public function __construct() {
			$this->includes();

			// Priority 5. Before send the emails.
			add_action( 'woocommerce_order_status_pending_to_processing', array( $this, 'validate_order_delivery_date' ), 5 );
			add_action( 'woocommerce_order_status_pending_to_processing', array( $this, 'add_order_shipping_date' ), 7 );

			add_action( 'woocommerce_checkout_subscription_created', 'wc_od_update_subscription_delivery_date' );
			add_action( 'woocommerce_subscription_renewal_payment_complete', 'wc_od_update_subscription_delivery_date' );
			add_action( 'wc_od_subscription_delivery_date_not_found', array( $this, 'delivery_date_not_found' ) );

			add_filter( 'wcs_renewal_order_meta', array( $this, 'copy_order_meta' ), 10, 3 );
			add_filter( 'wcs_resubscribe_order_meta', array( $this, 'copy_order_meta' ), 10, 3 );

			add_filter( 'wc_od_max_delivery_days', array( $this, 'checkout_max_delivery_days' ) );
		}

		/**
		 * Includes the necessary files.
		 *
		 * @since 1.3.0
		 */
		public function includes() {
			include_once( 'wc-od-subscriptions-functions.php' );
			include_once( 'class-wc-od-subscriptions-emails.php' );
			include_once( 'class-wc-od-subscriptions-settings.php' );
			include_once( 'class-wc-od-subscription-delivery.php' );

			if ( is_admin() ) {
				include_once( 'class-wc-od-subscription-admin.php' );
			}
		}

		/**
		 * Validates the delivery date of a renewal order after complete the payment.
		 *
		 * @since 1.3.0
		 *
		 * @param int $order_id The order Id.
		 */
		public function validate_order_delivery_date( $order_id ) {
			$delivery_date = wc_od_get_order_meta( $order_id, '_delivery_date' );

			// Delivery date not found during the subscription renewal or removed manually by the merchant.
			if ( ! $delivery_date ) {
				return;
			}

			// No renewals in the order.
			if ( ! wcs_order_contains_renewal( $order_id ) ) {
				return;
			}

			// The 'next_payment' date is not up to date at this point, so we cannot use the 'end_date' parameter here.
			$args = array(
				'order_id'           => $order_id,
				'disabled_days_args' => array(
					'type'         => 'delivery',
					'country'      => wc_od_get_order_prop( $order_id, 'shipping_country' ),
					'state'        => wc_od_get_order_prop( $order_id, 'shipping_state' ),
					'order_id'     => $order_id,
				)
			);

			// Get the first delivery date since payment.
			$first_delivery_date = wc_od_get_first_delivery_date( $args, 'renewal-order' );

			// No delivery date available.
			if ( ! $first_delivery_date ) {
				wc_od_delete_order_meta( $order_id, '_delivery_date', true );
				return;
			}

			// The minimum date for delivery.
			$args['start_date'] = date( 'Y-m-d', $first_delivery_date );

			// If the current date is not valid, change it for the first delivery date.
			if ( ! wc_od_validate_delivery_date( $delivery_date, $args, 'renewal-order' ) ) {
				wc_od_update_order_meta( $order_id, '_delivery_date', $args['start_date'], true );
			}
		}

		/**
		 * Adds the shipping date to the renewal order after the delivery date validation.
		 *
		 * @since 1.4.1
		 *
		 * @param int $order_id The order Id.
		 */
		public function add_order_shipping_date( $order_id ) {
			// No renewals in the order.
			if ( ! wcs_order_contains_renewal( $order_id ) ) {
				return;
			}

			$delivery_date = wc_od_get_order_meta( $order_id, '_delivery_date' );

			if ( $delivery_date ) {
				$shipping_timestamp = wc_od_get_last_shipping_date( array(
					'delivery_date' => $delivery_date,
					'disabled_delivery_days_args' => array(
						'type'    => 'delivery',
						'country' => wc_od_get_order_prop( $order_id, 'shipping_country' ),
						'state'   => wc_od_get_order_prop( $order_id, 'shipping_state' ),
					)
				), 'renewal-order' );

				if ( $shipping_timestamp ) {
					// Stores the date in the ISO 8601 format.
					$shipping_date = wc_od_localize_date( $shipping_timestamp, 'Y-m-d' );
					wc_od_update_order_meta( $order_id, '_shipping_date', $shipping_date, true );
				}
			}
		}

		/**
		 * Adds a note to the subscription when a delivery date for the next order is not found.
		 *
		 * @since 1.3.0
		 *
		 * @param WC_Subscription $subscription The subscription instance.
		 */
		public function delivery_date_not_found( $subscription ) {
			wc_od_add_order_note( $subscription, __( 'Delivery date not found for the next order.', 'woocommerce-order-delivery' ) );
		}

		/**
		 * Filter the metadata that will be copied from a subscription to an order.
		 *
		 * @since 1.3.0
		 *
		 * @param array           $meta         The metadata to copy to the order.
		 * @param WC_Order        $order        The order instance.
		 * @param WC_Subscription $subscription The subscription instance.
		 * @return array An array with the order metadata.
		 */
		public function copy_order_meta( $meta, $order, $subscription ) {
			$field_ids = array_keys( (array) wc_od_get_subscription_delivery_fields( $subscription ) );

			if ( empty( $field_ids ) ) {
				return $meta;
			}

			/**
			 * Filter the subscription delivery fields that will be copied as metadata to the order.
			 *
			 * @since 1.3.0
			 *
			 * @param array           $fields       An array with the field keys.
			 * @param WC_Subscription $subscription The subscription instance.
			 */
			$copy_meta = apply_filters( 'wc_od_copy_order_meta', array( 'delivery_date' ), $subscription );

			$exclude_meta = array();

			// Select the meta keys to exclude from the copy.
			foreach ( $field_ids as $field_id ) {
				if ( ! in_array( $field_id, $copy_meta ) ) {
					$exclude_meta[] = "_{$field_id}"; // The delivery fields are stored with an underscore at the beginning.
				}
			}

			// Exclude the meta keys from the copy.
			foreach ( $meta as $index => $meta_item ) {
				if ( in_array( $meta_item['meta_key'], $exclude_meta ) ) {
					unset( $meta[ $index ] );
				}
			}

			return $meta;
		}

		/**
		 * Restricts the maximum delivery days value to the minimum subscription period.
		 *
		 * @since 1.3.0
		 *
		 * @param int $max_delivery_days The max delivery days value.
		 * @return int The maximum delivery days.
		 */
		public function checkout_max_delivery_days( $max_delivery_days ) {
			if ( 'yes' !== WC_OD()->settings()->get_setting( 'subscriptions_limit_to_billing_interval' ) ) {
				return $max_delivery_days;
			}

			$period = wc_od_get_min_subscription_period_in_cart();

			if ( $period ) {
				$seconds_in_a_day = 86400;
				$time             = time();
				$diff             = ( strtotime( "+ {$period['interval']} {$period['period']}", $time ) - $time );
				$days             = abs( ( $diff / $seconds_in_a_day ) );

				if ( $days < $max_delivery_days ) {
					$max_delivery_days = $days;
				}
			}

			return $max_delivery_days;
		}

		/**
		 * Registers the emails of a subscription that will include the delivery information.
		 *
		 * @since 1.3.0
		 * @deprecated 1.4.1
		 *
		 * @param array $email_ids The email IDs.
		 * @return array An array with the email IDs.
		 */
		public function register_subscription_emails( $email_ids ) {
			_deprecated_function( __METHOD__, '1.4.1', 'Moved to WC_OD_Subscriptions_Emails->emails_with_delivery_details()' );

			return $email_ids;
		}

		/**
		 * Additional delivery information for the subscription emails.
		 *
		 * @since 1.3.0
		 * @deprecated 1.4.1
		 *
		 * @param array $args The arguments.
		 */
		public function email_after_delivery_details( $args ) {
			_deprecated_function( __METHOD__, '1.4.1', 'Moved to WC_OD_Subscriptions_Emails->delivery_details()' );
		}

	}
}

return new WC_OD_Subscriptions();
