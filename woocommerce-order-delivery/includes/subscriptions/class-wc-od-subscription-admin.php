<?php
/**
 * WC_OD Subscription Admin.
 *
 * @author     WooThemes
 * @package    WC_OD
 * @since      1.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WC_OD_Subscription_Admin' ) ) {

	class WC_OD_Subscription_Admin {

		/**
		 * Constructor.
		 *
		 * @since 1.3.0
		 */
		public function __construct() {
			add_action( 'woocommerce_subscription_date_updated', array( $this, 'subscription_date_updated' ), 10, 2 );
			add_action( 'woocommerce_subscription_date_deleted', array( $this, 'subscription_date_deleted' ), 10, 2 );

			add_filter( 'wc_od_admin_order_details_fields', array( $this, 'subscription_details_fields' ), 10, 2 );

			add_action( 'woocommerce_admin_order_data_after_order_details', array( $this, 'subscription_delivery_preferences' ), 20 );
			add_action( 'wc_od_admin_subscription_delivery_preferences', 'wc_od_admin_subscription_delivery_preferences' );
		}

		/**
		 * Updates the subscription delivery date when the next payment date is modified manually by the merchant.
		 *
		 * @since 1.3.0
		 *
		 * @param WC_Subscription $subscription The subscription instance.
		 * @param string          $date_type    The date type.
		 */
		public function subscription_date_updated( $subscription, $date_type ) {
			if ( 'next_payment' === $date_type && ! empty( $_POST ) && isset( $_POST['woocommerce_meta_nonce'] ) &&
				wp_verify_nonce( $_POST['woocommerce_meta_nonce'], 'woocommerce_save_data' ) ) {
				$delivery_date = wc_od_get_order_meta( $subscription, '_delivery_date' );
				$posted_delivery_date = ( isset( $_POST['_delivery_date'] ) ? sanitize_text_field( $_POST['_delivery_date'] ) : '' );

				// No delivery date or modified manually by the merchant.
				if ( ! $posted_delivery_date || ( $delivery_date !== $posted_delivery_date ) ) {
					return;
				}

				// It's a valid date.
				if ( wc_od_validate_subscription_delivery_date( $subscription, $posted_delivery_date ) ) {
					return;
				}

				// Disable default order meta save to avoid overwrite the value.
				remove_action( 'woocommerce_process_shop_order_meta', 'wc_od_admin_process_shop_order_meta', 35 );

				// Update and store the subscription delivery date.
				wc_od_update_subscription_delivery_date( $subscription );

				// $subscription doesn't have the updated delivery date, so we pass the ID to fetch the subscription object again.
				$subscription_id   = wc_od_get_order_prop( $subscription, 'id' );
				$new_delivery_date = wc_od_get_order_meta( $subscription_id, '_delivery_date' );

				// Adds an internal note to the subscription to notify to the merchant.
				if ( $new_delivery_date && $posted_delivery_date !== $new_delivery_date ) {
					wc_od_add_order_note( $subscription, sprintf(
						__( 'Due to a change of the date for the next payment, we have updated the delivery date of the next order to %s.', 'woocommerce-order-delivery' ),
						'<strong>' . wc_od_localize_date( $new_delivery_date ) . '</strong>'
					), true );
				}
			}
		}

		/**
		 * Deletes the subscription delivery date when the next payment date is deleted manually by the merchant.
		 *
		 * @since 1.3.0
		 *
		 * @param WC_Subscription $subscription The subscription instance.
		 * @param string          $date_type    The date type.
		 */
		public function subscription_date_deleted( $subscription, $date_type ) {
			if ( 'next_payment' === $date_type && ! empty( $_POST ) && isset( $_POST['woocommerce_meta_nonce'] ) &&
				wp_verify_nonce( $_POST['woocommerce_meta_nonce'], 'woocommerce_save_data' ) ) {

				// Disable default order meta save to avoid overwrite the value.
				remove_action( 'woocommerce_process_shop_order_meta', 'wc_od_admin_process_shop_order_meta', 35 );

				// Delete the next delivery date.
				wc_od_delete_order_meta( $subscription, '_delivery_date', true );
			}
		}

		/**
		 * Filter the fields to display in the subscription details section.
		 *
		 * @since 1.4.0
		 *
		 * @param array    $fields An array with the fields data.
		 * @param WC_Order $order  The order instance.
		 * @return array
		 */
		public function subscription_details_fields( $fields, $order ) {
			if ( wcs_is_subscription( $order ) ) {
				unset( $fields['shipping_date'] );
				$fields['delivery_date']['label'] = __( 'Next order delivery date:', 'woocommerce-order-delivery' );
				$fields['delivery_date']['class'] = array_diff( $fields['delivery_date']['class'], array( 'last' ) );
				$fields['delivery_date']['class'][] = 'form-field-wide';
			}

			return $fields;
		}

		/**
		 * Customizes the delivery date field label in the subscription details.
		 *
		 * @since 1.3.0
		 * @deprecated 1.4.0 Use the 'subscription_details_fields' method instead.
		 *
		 * @param string $label The field label.
		 * @return string The field label.
		 */
		public function delivery_date_field_label( $label ) {
			_deprecated_function( __METHOD__, '1.4.0' );

			return $label;
		}

		/**
		 * Adds the delivery preferences in the admin subscription details.
		 *
		 * @since 1.3.0
		 *
		 * @param WC_Order $order The order instance.
		 */
		public function subscription_delivery_preferences( $order ) {
			$subscription = wcs_get_subscription( $order );

			if ( $subscription && wc_od_subscription_has_delivery_preferences( $subscription ) ) {
				/**
				 * Allows to include the delivery preferences in the admin subscription details.
				 *
				 * @since 1.3.0
				 *
				 * @param WC_Subscription $subscription The subscription instance.
				 */
				do_action( 'wc_od_admin_subscription_delivery_preferences', $subscription );
			}
		}
	}
}

return new WC_OD_Subscription_Admin();
