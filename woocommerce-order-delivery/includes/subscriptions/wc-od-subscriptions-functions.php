<?php
/**
 * Useful functions for the integration with the WooCommerce Subscriptions extension.
 *
 * @author      WooThemes
 * @package     WC_OD
 * @since       1.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Gets the edit-delivery endpoint for the specified subscription.
 *
 * @since 1.3.0
 *
 * @param int $subscription_id The subscription Id.
 * @return string The edit-delivery endpoint for the subscription.
 */
function wc_od_edit_delivery_endpoint( $subscription_id ) {
	return wc_get_endpoint_url( 'edit-delivery', $subscription_id, wc_get_page_permalink( 'myaccount' ) );
}

/**
 * Gets if the current page is the edit-delivery endpoint or not.
 *
 * @since 1.3.0
 *
 * @global WP_Query $wp_query The WP_Query instance.
 *
 * @return bool True if the current page is the edit-delivery endpoint. False otherwise.
 */
function wc_od_is_edit_delivery_endpoint() {
	global $wp_query;

	return ( is_account_page() && isset( $wp_query->query_vars['edit-delivery'] ) );
}

/**
 * Gets the current subscription Id.
 *
 * @since 1.3.0
 *
 * @global WP_Query $wp_query The WP_Query instance.
 *
 * @return int|false The subscription Id. False on failure.
 */
function wc_od_get_current_subscription_id() {
	global $wp_query;

	$subscription_id = false;

	if ( wc_od_is_edit_delivery_endpoint() ) {
		$subscription_id = (int) $wp_query->query_vars['edit-delivery'];
	} elseif ( wcs_is_view_subscription_page() ) {
		$subscription_id = (int) $wp_query->query_vars['view-subscription'];
	}

	return $subscription_id;
}

/**
 * Gets if the user has the capabilities to work with the subscription delivery or not.
 *
 * @since 1.3.0
 *
 * @param mixed   $the_subscription Post object or post ID of the subscription.
 * @param WP_User $user             Optional. The user to check. Current by default.
 * @return bool True if the user has the capabilities to work with the subscription delivery. False otherwise.
 */
function wc_od_user_has_subscription_delivery_caps( $the_subscription, $user = null ) {
	$subscription = wcs_get_subscription( $the_subscription );

	if ( ! $subscription ) {
		return false;
	}

	if ( ! $user ) {
		$user = wp_get_current_user();
	}

	$has_caps = user_can( $user, 'view_order', wc_od_get_order_prop( $subscription, 'id' ) );

	/**
	 * Filter if the user has the capabilities to work with the subscription delivery.
	 *
	 * @since 1.3.0
	 *
	 * @param bool            $has_caps     True if the user has subscription delivery caps. False otherwise.
	 * @param WC_Subscription $subscription The subscription instance.
	 * @param WP_User         $user         Optional. The user to check.
	 */
	return apply_filters( 'wc_od_user_has_subscription_delivery_caps', $has_caps, $subscription, $user );
}

/**
 * Gets if the subscription needs the delivery details or not.
 *
 * @since 1.3.0
 *
 * @param mixed $the_subscription Post object or post ID of the subscription.
 * @return bool True if the subscription needs the delivery details. False otherwise.
 */
function wc_od_subscription_needs_delivery_details( $the_subscription ) {
	$subscription = wcs_get_subscription( $the_subscription );

	if ( ! $subscription ) {
		return false;
	}

	// On-hold subscriptions still return the _schedule_next_payment date, even if it has expired.
	// The delivery details are disabled until the customer renew the subscription manually.
	$next_payment = $subscription->get_time( 'next_payment' );

	$needs_details = (
		$subscription->needs_shipping_address() &&
		$subscription->has_status( array( 'active', 'on-hold', 'pending' ) ) &&
		$next_payment && $next_payment >= wc_od_get_local_date( true, 'Y-m-d H:i:s' )
	);

	/**
	 * Filter if the subscription needs the delivery details.
	 *
	 * @since 1.3.0
	 *
	 * @param bool            $needs_details True if the subscription needs the delivery details. False otherwise.
	 * @param WC_Subscription $subscription  The subscription instance.
	 */
	return apply_filters( 'wc_od_subscription_needs_delivery_details', $needs_details, $subscription );
}

/**
 * Gets if the subscription needs a delivery date or not.
 *
 * @since 1.3.0
 *
 * @param mixed $the_subscription Post object or post ID of the subscription.
 * @return bool True if the subscription needs a delivery date. False otherwise.
 */
function wc_od_subscription_needs_delivery_date( $the_subscription ) {
	$subscription = wcs_get_subscription( $the_subscription );

	if ( ! $subscription ) {
		return false;
	}

	$needs_date = (
		wc_od_subscription_needs_delivery_details( $subscription ) &&
		'calendar' === WC_OD()->settings()->get_setting( 'checkout_delivery_option' )
	);

	/**
	 * Filter if the subscription needs a delivery date.
	 *
	 * @since 1.3.0
	 *
	 * @param bool            $needs_date   True if the subscription needs a delivery date. False otherwise.
	 * @param WC_Subscription $subscription The subscription instance.
	 */
	return apply_filters( 'wc_od_subscription_needs_delivery_date', $needs_date, $subscription );
}

/**
 * Gets if the subscription has delivery preferences or not.
 *
 * @since 1.3.0
 *
 * @param mixed $the_subscription Post object or post ID of the subscription.
 * @return bool True if the subscription has delivery preferences. False otherwise.
 */
function wc_od_subscription_has_delivery_preferences( $the_subscription ) {
	$subscription = wcs_get_subscription( $the_subscription );

	if ( ! $subscription ) {
		return false;
	}

	$has_preferences = (
		wc_od_subscription_needs_delivery_date( $subscription ) &&
		'day' !== wc_od_get_order_prop( $subscription, 'billing_period' ) &&
		wc_od_get_order_meta( $subscription, '_delivery_date' ) // Disabled if there is no delivery date.
	);

	/**
	 * Filter if the subscription has delivery preferences.
	 *
	 * @since 1.3.0
	 *
	 * @param bool            $has_preferences True if the subscription has delivery preferences. False otherwise.
	 * @param WC_Subscription $subscription    The subscription instance.
	 */
	return apply_filters( 'wc_od_subscription_has_delivery_preferences', $has_preferences, $subscription );
}

/**
 * Gets the delivery days for a subscription.
 *
 * @since 1.3.0
 *
 * @param mixed $the_subscription Post object or post ID of the subscription.
 * @return array An array with the subscription delivery days.
 */
function wc_od_get_subscription_delivery_days( $the_subscription ) {
	$default_delivery_days = WC_OD()->settings()->get_setting( 'delivery_days' );
	$subscription          = wcs_get_subscription( $the_subscription );
	$delivery_days         = wc_od_get_order_meta( $subscription, '_delivery_days' );

	if ( ! $delivery_days ) {
		$delivery_days = $default_delivery_days; // Use the defaults.
	} else {
		// Merge the defaults with the subscription preferred.
		foreach ( $default_delivery_days as $index => $day ) {
			$enabled = ( (bool) $day['enabled'] && ( ! isset( $delivery_days[ $index ] ) || (bool) $delivery_days[ $index ]['enabled'] ) );
			$delivery_days[ $index ]['enabled'] = ( $enabled ? '1' : '0' );
		}
	}

	/**
	 * Filter the subscription delivery days.
	 *
	 * @since 1.3.0
	 *
	 * @param array           $delivery_days The delivery days.
	 * @param WC_Subscription $subscription  The subscription instance.
	 */
	return apply_filters( 'wc_od_get_subscription_delivery_days', $delivery_days, $subscription );
}

/**
 * Gets the arguments used to calculate the subscription delivery date.
 *
 * @since 1.3.0
 *
 * @param mixed $the_subscription Post object or post ID of the subscription.
 * @return array|false An array with the arguments. False on failure.
 */
function wc_od_get_subscription_delivery_date_args( $the_subscription ) {
	$subscription = wcs_get_subscription( $the_subscription );

	if ( ! $subscription ) {
		return false;
	}

	$seconds_in_a_day = 86400;
	$start_timestamp  = wc_od_get_subscription_first_delivery_date( $subscription );
	$billing_interval = wc_od_get_order_prop( $subscription, 'billing_interval' );
	$billing_period   = wc_od_get_order_prop( $subscription, 'billing_period' );
	$end_timestamp    = ( strtotime( "+ {$billing_interval} {$billing_period}", $subscription->get_time( 'next_payment' ) ) + $seconds_in_a_day ); // Non-inclusive.

	/**
	 * Filter the arguments used to calculate the subscription delivery date.
	 *
	 * @since 1.3.0
	 *
	 * @param array           $args         The arguments
	 * @param WC_Subscription $subscription The subscription instance.
	 */
	return apply_filters( 'wc_od_subscription_delivery_date_args', array(
		'subscription'  => $subscription, // Useful for developers.
		'start_date'    => $start_timestamp,
		'end_date'      => $end_timestamp,
		'delivery_days' => WC_OD()->settings()->get_setting( 'delivery_days' ),
		'disabled_days_args' => array(
			'subscription' => $subscription, // Useful for developers.
			'type'         => 'delivery',
			'start'        => date( 'Y-m-d', $start_timestamp ),
			'end'          => date( 'Y-m-d', $end_timestamp ),
			'country'      => wc_od_get_order_prop( $subscription, 'shipping_country' ),
			'state'        => wc_od_get_order_prop( $subscription, 'shipping_state' ),
		),
	), $subscription );
}

/**
 * Gets if the specified delivery date is valid for the subscription or not.
 *
 * @since 1.3.0
 *
 * @param mixed      $the_subscription Post object or post ID of the subscription.
 * @param string|int $date             The delivery date string or timestamp.
 * @return bool True if the delivery date is a valid date. False otherwise.
 */
function wc_od_validate_subscription_delivery_date( $the_subscription, $date ) {
	$subscription = wcs_get_subscription( $the_subscription );

	if ( ! $subscription ) {
		return false;
	}

	// Fetch the delivery date arguments.
	$args = wc_od_get_subscription_delivery_date_args( $subscription );

	return wc_od_validate_delivery_date( $date, $args, 'subscription' );
}

/**
 * Gets the first date to ship the order of a subscription.
 *
 * @since 1.3.0
 *
 * @param mixed $the_subscription Post object or post ID of the subscription.
 * @return false|int A timestamp representing the first shipping date. False on failure.
 */
function wc_od_get_subscription_first_shipping_date( $the_subscription ) {
	$subscription = wcs_get_subscription( $the_subscription );

	if ( ! $subscription ) {
		return false;
	}

	// Strip the time and add a day of margin to complete the payment.
	$start_date = date( 'Y-m-d', strtotime( '+ 1 day', $subscription->get_time( 'next_payment' ) ) );

	$first_shipping_date = wc_od_get_first_shipping_date( array(
		'subscription' => $subscription, // Useful for developers.
		'start_date'   => $start_date,
	), 'subscription' );

	return $first_shipping_date;
}

/**
 * Gets the first date to deliver the order of a subscription.
 *
 * @since 1.3.0
 *
 * @param mixed $the_subscription Post object or post ID of the subscription.
 * @return false|int A timestamp representing the delivery date. False on failure.
 */
function wc_od_get_subscription_first_delivery_date( $the_subscription ) {
	$first_shipping_date = wc_od_get_subscription_first_shipping_date( $the_subscription );

	if ( ! $first_shipping_date ) {
		return false;
	}

	$delivery_date = wc_od_get_first_delivery_date( array(
		'subscription'  => wcs_get_subscription( $the_subscription ), // Useful for developers.
		'shipping_date' => $first_shipping_date,
	), 'subscription' );

	return $delivery_date;
}

/**
 * Updates the delivery date for new and renewed subscriptions.
 *
 * @since 1.3.0
 *
 * @param mixed $the_subscription Post object or post ID of the subscription.
 */
function wc_od_update_subscription_delivery_date( $the_subscription ) {
	$subscription = wcs_get_subscription( $the_subscription );

	if ( ! $subscription ) {
		return;
	}

	$delivery_date = false;

	// Assign a delivery date automatically.
	if ( wc_od_subscription_needs_delivery_date( $subscription ) ) {
		$delivery_timestamp = wc_od_get_subscription_first_delivery_date( $subscription );

		if ( $delivery_timestamp ) {
			// Try to adapt the date to the customer preferences.
			if ( wc_od_subscription_has_delivery_preferences( $subscription ) ) {
				$delivery_days = wc_od_get_subscription_delivery_days( $subscription );
				$wday = date( 'w', $delivery_timestamp );

				// The first delivery date is not an enabled day by the customer. Try with the next.
				if ( ! $delivery_days[ $wday ]['enabled'] ) {
					$seconds_in_a_day  = 86400;
					$billing_interval  = wc_od_get_order_prop( $subscription, 'billing_interval' );
					$billing_period    = wc_od_get_order_prop( $subscription, 'billing_period' );
					$end_timestamp     = strtotime( "+ {$billing_interval} {$billing_period}", $subscription->get_time( 'next_payment' ) );

					$next_delivery_date = wc_od_get_next_delivery_date( array(
						'delivery_days'      => $delivery_days,
						'delivery_date'      => $delivery_timestamp,
						'end_date'           => date( 'Y-m-d', $end_timestamp + $seconds_in_a_day ),
						'disabled_days_args' => array(
							'type'    => 'delivery',
							'country' => wc_od_get_order_prop( $subscription, 'shipping_country' ),
							'state'   => wc_od_get_order_prop( $subscription, 'shipping_state' ),
						),
					), 'subscription' );

					if ( $next_delivery_date ) {
						$delivery_timestamp = $next_delivery_date;
					}
				}
			}

			/**
			 * Filter the delivery date of a subscription.
			 *
			 * @since 1.3.0
			 *
			 * @param int             $delivery_timestamp A timestamp representing the subscription delivery date.
			 * @param WC_Subscription $subscription       The subscription instance.
			 */
			$delivery_timestamp = apply_filters( 'wc_od_update_subscription_delivery_date', $delivery_timestamp, $subscription );

			if ( $delivery_timestamp ) {
				$delivery_date = wc_od_localize_date( $delivery_timestamp, 'Y-m-d' );

				if ( wc_od_update_order_meta( $subscription, '_delivery_date', $delivery_date, true ) ) {
					/**
					 * Fires immediately after updating the delivery date of a subscription.
					 *
					 * @since 1.3.0
					 *
					 * @param int             $delivery_timestamp A timestamp representing the subscription delivery date.
					 * @param WC_Subscription $subscription       The subscription instance.
					 */
					do_action( 'wc_od_subscription_delivery_date_updated', $delivery_timestamp, $subscription );
				}
			} else {
				/**
				 * Fires if there is no delivery date for the subscription.
				 *
				 * @since 1.3.0
				 *
				 * @param WC_Subscription $subscription The subscription instance.
				 */
				do_action( 'wc_od_subscription_delivery_date_not_found', $subscription );
			}
		}
	}

	if ( ! $delivery_date ) {
		// Delete if exists the _delivery_date meta cloned from the previous order.
		wc_od_delete_order_meta( $subscription, '_delivery_date', true );
	}
}

/**
 * Gets the fields for the delivery form of a subscription.
 *
 * @since 1.3.0
 *
 * @param mixed $the_subscription Post object or post ID of the subscription.
 * @return array|false An array with the delivery fields. False on failure.
 */
function wc_od_get_subscription_delivery_fields( $the_subscription ) {
	$subscription = wcs_get_subscription( $the_subscription );

	if ( ! $subscription || ! wc_od_subscription_has_delivery_preferences( $subscription ) ) {
		return false;
	}

	$fields = array();

	$fields['delivery_date'] = wc_od_get_delivery_date_field_args( array(
		'label'       => __( 'Next order delivery date', 'woocommerce-order-delivery' ),
		'description' => __( 'This will be the delivery date of the next order.', 'woocommerce-order-delivery' ),
		'value'       => wc_od_get_order_meta( $subscription, '_delivery_date' ),
		'return'      => false,
	), 'subscription' );

	$fields['delivery_days'] = array(
		'type'     => 'wc_od_subscription_delivery_days',
		'label'    => __( 'Preferred delivery days', 'woocommerce-order-delivery' ),
		'description' => __( 'If possible, we will try to adapt the delivery date of the future orders to your preferences.', 'woocommerce-order-delivery' ),
		'value'    => wc_od_get_subscription_delivery_days( $subscription ),
		'required' => true,
	);

	/**
	 * Filter the delivery fields of the subscription.
	 *
	 * @since 1.3.0
	 *
	 * @param array           $fields       The form fields.
	 * @param WC_Subscription $subscription The subscription instance.
	 */
	return apply_filters( 'wc_od_get_subscription_delivery_fields', $fields, $subscription );
}

/**
 * Gets the HTML content for the subscription delivery_days field.
 *
 * @since 1.3.0
 *
 * @param string $field The field HTML content.
 * @param string $key   The field key.
 * @param array  $args  The field arguments.
 * @return string The field content.
 */
function wc_od_subscription_delivery_days_field( $field, $key, $args ) {
	$week_days     = wc_od_get_week_days();
	$delivery_days = WC_OD()->settings()->get_setting( 'delivery_days' );
	$value         = ( isset( $args['value'] ) ? (array) $args['value'] : array() );
	$required      = '';

	if ( $args['required'] ) {
		$args['class'][] = 'validate-required';
		$required = ' <abbr class="required" title="' . esc_attr__( 'required', 'woocommerce-order-delivery' ) . '">*</abbr>';
	}

	ob_start();
	?>
	<p class="form-row <?php echo esc_attr( implode( ' ', $args['class'] ) ); ?>" id="<?php echo esc_attr( $key ) ; ?>">
		<label><?php echo esc_html( $args['label'] ) . $required; ?></label>
		<?php
			foreach ( $delivery_days as $index => $data ) :
				$enabled = (bool) $delivery_days[ $index ]['enabled'];
				$checked = ( $enabled && ( ! isset( $value[ $index ] ) || (bool) $value[ $index ]['enabled'] ) );

				printf( '<label class="delivery-day" for="%1$s"><input id="%1$s" type="checkbox" name="%2$s" %3$s %4$s /> %5$s</label><br>',
					esc_attr( "{$key}_{$index}" ),
					esc_attr( $key . "[{$index}][enabled]" ),
					checked( $checked, true, false ),
					disabled( $enabled, false, false ),
					wp_kses_post( $week_days[ $index ] )
				);
			endforeach;

			if ( $args['description'] ) :
				printf( '<span class="description">%s</span>', esc_html( $args['description'] ) );
			endif;
		?>
	</p>

	<style type="text/css">
		#delivery_days .delivery-day { display: inline-block; font-weight: normal; }
	</style>
	<?php
	$field = ob_get_clean();

	return $field;
}

/**
 * Gets the minimum subscription period in the cart.

 * @since 1.3.0
 *
 * @return array|false An array with the subscription period. False otherwise.
 */
function wc_od_get_min_subscription_period_in_cart() {
	$items = WC()->cart->get_cart();

	$min_period     = false;
	$min_timestamp  = false;
	$timestamp_base = time();

	foreach ( $items as $item ) {
		$product = $item['data'];

		if ( WC_Subscriptions_Product::is_subscription( $product ) && $product->needs_shipping() && ! WC_Subscriptions_Product::needs_one_time_shipping( $product ) ) {
			$period   = WC_Subscriptions_Product::get_period( $product );
			$interval = WC_Subscriptions_Product::get_interval( $product );

			$timestamp = strtotime( "+ {$interval} {$period}", $timestamp_base );
			if ( $timestamp && ( ! $min_timestamp || $min_timestamp > $timestamp ) ) {
				$min_timestamp = $timestamp;
				$min_period    = array(
					'period'   => $period,
					'interval' => $interval,
				);
			}
		}
	}

	return $min_period;
}

/**
 * Prints the delivery preferences in the admin subscription details.
 *
 * @since 1.3.0
 *
 * @global WP_Locale $wp_locale The WP_Locale instance.
 *
 * @param WC_Subscription $subscription The subscription instance.
 */
function wc_od_admin_subscription_delivery_preferences( $subscription ) {
	global $wp_locale;

	$fields = wc_od_get_subscription_delivery_fields( $subscription );

	if ( isset( $fields['delivery_days'] ) ) :
		$value = wc_od_get_days_by( $fields['delivery_days']['value'], 'enabled', '1' );
		$days = array();

		foreach ( $value as $index => $date ) :
			$days[] = '<span>' . $wp_locale->get_weekday_abbrev( $wp_locale->get_weekday( $index ) ) . '</span>';
		endforeach;

		printf( '<p class="form-field form-field-wide"><strong>%1$s</strong><br> %2$s</p>',
			__( 'Preferred delivery days:', 'woocommerce-order-delivery' ),
			join( ' ', $days )
		);
	endif;
}
