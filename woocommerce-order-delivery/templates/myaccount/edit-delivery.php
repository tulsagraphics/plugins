<?php
/**
 * My Account edit-delivery
 *
 * @author     WooThemes
 * @package    WC_OD
 * @since      1.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$subscription_id = wc_od_get_current_subscription_id();

if ( ! $subscription_id || ! wc_od_user_has_subscription_delivery_caps( $subscription_id ) ) {
	$notice = sprintf( wp_kses(
		__( 'Invalid Subscription. <a href="%s" class="wc-forward">My Account</a>', 'woocommerce-order-delivery' ),
		array(  'a' => array( 'href' => array(), 'class' => array() ) )
	), esc_url( wc_get_page_permalink( 'myaccount' ) ) );

	wc_print_notice( $notice, 'error' );
	return;
}

$subscription = wcs_get_subscription( $subscription_id );
$fields       = wc_od_get_subscription_delivery_fields( $subscription );
?>

<p><?php printf( __( 'Here you can change the delivery preferences for the subscription: %s', 'woocommerce-order-delivery' ),
	sprintf( '<a href="%1$s">#%2$s</a>', $subscription->get_view_order_url(), $subscription_id )
); ?></p>

<?php if ( empty( $fields ) ) : ?>

	<p><?php _e( 'There is no preferences for this subscription.', 'woocommerce-order-delivery' ); ?></p>

<?php else : ?>

	<form method="post">

		<?php foreach ( $fields as $key => $field ) : ?>

			<?php woocommerce_form_field( $key, $field, ! empty( $_POST[ $key ] ) ? wc_clean( $_POST[ $key ] ) : $field['value'] ); ?>

		<?php endforeach; ?>

		<p>
			<?php wp_nonce_field( 'wc_od_edit_delivery' ); ?>
			<input type="hidden" name="action" value="edit_delivery" />
			<input type="hidden" name="subscription_id" id="subscription_id" value="<?php echo esc_attr( $subscription_id ); ?>" />
			<input type="submit" class="button" name="save_delivery" value="<?php esc_attr_e( 'Save Preferences', 'woocommerce-order-delivery' ); ?>" />
		</p>
	</form>

<?php endif; ?>
