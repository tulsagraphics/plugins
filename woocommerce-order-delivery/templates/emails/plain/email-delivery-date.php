<?php
/**
 * Email delivery details (plain text)
 *
 * @author     WooThemes
 * @package    WC_OD
 * @since      1.4.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

echo "\n" . esc_html( strtoupper( $title ) ) . "\n\n";

do_action( 'wc_od_email_before_delivery_details', $args );

echo sprintf( __( 'We will try our best to deliver your order on %s.', 'woocommerce-order-delivery' ), $delivery_date ) . "\n";

do_action( 'wc_od_email_after_delivery_details', $args );
