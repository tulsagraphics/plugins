/**
 * WC_OD_Subscription scripts
 *
 * @autor   WooThemes
 * @package WC_OD
 * @since   1.3.0
 */

/**
 * Subscription function.
 *
 * @param {jQuery} $       The jQuery instance.
 * @param {Object} options The options.
 */
;(function( $, options ) {

	'use strict';

	var WC_OD_Subscription = function( options ) {
		this.options = options;

		this.init();
	};

	WC_OD_Subscription.prototype = {

		init: function() {
			this.$deliveryDate = $( '#delivery_date' ).wc_od_datepicker( this.options );
		}
	};

	$(function() {
		new WC_OD_Subscription( options );
	});
})( jQuery, wc_od_subscription_l10n );