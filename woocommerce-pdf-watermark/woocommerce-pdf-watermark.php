<?php
/*
 * Plugin Name: WooCommerce PDF Watermark
 * Version: 1.1.6
 * Plugin URI: http://www.woocommerce.com/products/woocommerce-pdf-watermark/
 * Description: Apply text or image-based watermarks to your WooCommerce PDF downloads.
 * Author: WooCommerce
 * Author URI: http://www.woocommerce.com/
 * Developer: Gerhard Potgieter
 * Developer URI: http://gerhardpotgieter.com/
 * Text Domain: woocommerce-pdf-watermark
 * Domain Path: /languages
 * Requires at least: 4.0
 * Tested up to: 4.1
 * WC tested up to: 3.4
 * WC requires at least: 2.6
 * Woo: 754721:20525aa5a4ac479d9dbe0d78a8dce5f4
 *
 * Copyright: 2015-2017 WooCommerce.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package WooCommerce
 * @author Gerhard Potgieter
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Required functions.
 */
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

/**
 * Plugin updates.
 */
woothemes_queue_update( plugin_basename( __FILE__ ), '20525aa5a4ac479d9dbe0d78a8dce5f4', '754721' );

/**
 * Get since instance of WC_PDF_Watermark class
 * @return object WC_PDF_Watermark
 */
function woocommerce_pdf_watermark() {
	require_once( 'includes/class-wc-pdf-watermark.php' );

	if ( ! defined( 'WC_PDF_WATERMARK_VERSION' ) ) {
	        define( 'WC_PDF_WATERMARK_VERSION', '1.1.6' );
	}

	return WC_PDF_Watermark::instance( __FILE__, WC_PDF_WATERMARK_VERSION );
}

if ( is_woocommerce_active() ) {
	woocommerce_pdf_watermark();
}
