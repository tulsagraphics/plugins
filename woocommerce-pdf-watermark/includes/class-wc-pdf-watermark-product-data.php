<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WC_PDF_Watermark_Product_Data' ) ) {

	class WC_PDF_Watermark_Product_Data {

		/**
		 * Constructor
		 * @return void
		 */
		public function __construct() {
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'add_product_data_tab' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'add_product_data_tab_target' ) );
			add_action( 'woocommerce_process_product_meta_simple', array( $this, 'save_product_data' ) );
			add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'variation_product_data' ), 10, 3 );
			add_action( 'woocommerce_save_product_variation', array( $this, 'save_variation_data' ), 10, 2 );
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
		} // End __construct()

		/**
		 * Add a PDF Stamper tab to simple downloadable products
		 * @param array
		 */
		public function add_product_data_tab( $tabs ) {
			if ( isset( $tabs['pdf_watermark'] ) ) {
				return $tabs;
			}

			$tabs['pdf_watermark'] = array(
				'label'  => __( 'PDF Watermark', 'woocommerce-pdf-watermark' ),
				'target' => 'pdf_watermark_data',
				'class'  => array( 'show_if_downloadable' ),
			);

			return $tabs;
		} // End add_product_data_tab()

		/**
		 * Output simple product fields
		 * @return void
		 */
		public function add_product_data_tab_target() {
		?>
			<div id="pdf_watermark_data" class="panel woocommerce_options_panel">
			<?php
				woocommerce_wp_checkbox( array( 'id' => '_pdf_watermark_disable', 'label' => __( 'Disable', 'woocommerce-pdf-watermark' ), 'description' => __( 'Disable PDF downloads from this product to be watermarked.', 'woocommerce-pdf-watermark' ) ) );
				woocommerce_wp_checkbox( array( 'id' => '_pdf_watermark_override', 'label' => __( 'Override', 'woocommerce-pdf-watermark' ), 'description' => __( 'Override the global PDF watermark settings with custom ones.', 'woocommerce-pdf-watermark' ), 'wrapper_class' => 'hide_if_disabled' ) );
				woocommerce_wp_select( array( 'id' => '_pdf_watermark_type', 'label' => __( 'Watermark Type', 'woocommerce-pdf-watermark' ), 'desc_tip' => 'true', 'description' => __( 'The type of watermark you would like to apply to the PDF documents, text-based or image.', 'woocommerce-pdf-watermark' ),
					'options' => array(
						'image'            => __( 'Image', 'woocommerce-pdf-watermark' ),
						'text' => __( 'Text', 'woocommerce-pdf-watermark' ),
					), 'wrapper_class' => 'hide_if_disabled show_if_override' ) );
				woocommerce_wp_text_input( array( 'id' => '_pdf_watermark_image', 'label' =>  __( 'Watermark Image', 'woocommerce-pdf-watermark' ), 'desc_tip' => 'true', 'description' => __( 'URL to the image you want to use for the watermark.', 'woocommerce-pdf-watermark' ), 'wrapper_class' => 'hide_if_disabled show_if_image' ) );
				woocommerce_wp_textarea_input(  array( 'id' => '_pdf_watermark_text', 'label' => __( 'Watermark Text', 'woocommerce-pdf-watermark' ), 'desc_tip' => 'true', 'description' => __( 'Text you want to use for the watermark. You can use {first_name}, {last_name}, {email}, {order_number}, {order_date}, {site_name}, {site_url} tags to insert customer specific data in the watermark.', 'woocommerce-pdf-watermark' ), 'wrapper_class' => 'hide_if_disabled show_if_text' ) );
			?>
			</div>
		<?php
		} // add_product_data_tab_target()

		/**
		 * Save simple product data
		 * @param  int $post_id
		 * @return void
		 */
		public function save_product_data( $post_id ) {
			if ( isset( $_POST['_pdf_watermark_disable'] ) ) {
				update_post_meta( $post_id, '_pdf_watermark_disable', 'yes' );
			} else {
				update_post_meta( $post_id, '_pdf_watermark_disable', 'no' );
			}
			if ( isset( $_POST['_pdf_watermark_override'] ) ) {
				update_post_meta( $post_id, '_pdf_watermark_override', 'yes' );
			} else {
				update_post_meta( $post_id, '_pdf_watermark_override', 'no' );
			}

			if ( isset( $_POST['_pdf_watermark_type'] ) ) {
				update_post_meta( $post_id, '_pdf_watermark_type', wc_clean( $_POST['_pdf_watermark_type'] ) );
			}
			if ( isset( $_POST['_pdf_watermark_image'] ) ) {
				update_post_meta( $post_id, '_pdf_watermark_image', wc_clean( $_POST['_pdf_watermark_image'] ) );
			}
			if ( isset( $_POST['_pdf_watermark_text'] ) ) {
				update_post_meta( $post_id, '_pdf_watermark_text', wp_kses_post( trim( $_POST['_pdf_watermark_text'] ) ) );
			}
		} // End save_product_data()

		/**
		 * Output variation fields
		 * @param  int $loop
		 * @param  array $variation_data
		 * @param  int $variation
		 * @return void
		 */
		public function variation_product_data( $loop, $variation_data, $variation ) {
			$variation_meta          = get_post_meta( $variation->ID );
			$_pdf_watermark_disable  = isset( $variation_meta['_pdf_watermark_disable'][0] ) ? $variation_meta['_pdf_watermark_disable'][0] : 'no';
			$_pdf_watermark_override = isset( $variation_meta['_pdf_watermark_override'][0] ) ? $variation_meta['_pdf_watermark_override'][0] : 'no';
			$_pdf_watermark_type     = isset( $variation_meta['_pdf_watermark_type'][0] ) ? $variation_meta['_pdf_watermark_type'][0] : '';
			$_pdf_watermark_image    = isset( $variation_meta['_pdf_watermark_image'][0] ) ? $variation_meta['_pdf_watermark_image'][0] : '';
			$_pdf_watermark_text     = isset( $variation_meta['_pdf_watermark_text'][0] ) ? $variation_meta['_pdf_watermark_text'][0] : '';
		?>
			<div class="show_if_variation_downloadable" style="display: none;">
				<p class="form-row form-row-first options">
					<label>
						<input type="checkbox" class="checkbox variable_pdf_watermark_disable" name="variable_pdf_watermark_disable[<?php echo $loop; ?>]" <?php checked( 'yes', isset( $_pdf_watermark_disable ) ? $_pdf_watermark_disable : '', true ); ?>/>
						<?php _e( 'Disable PDF Watermark:', 'woocommerce-pdf-watermark' ); ?> <a class="tips" data-tip="<?php _e( 'Disable PDF downloads from this variation to be watermarked.', 'woocommerce-pdf-watermark' ); ?>" href="#">[?]</a>
					</label>
				</p>
				<p class="form-row form-row-last options hide_if_pdf_disabled">
					<label>
						<input type="checkbox" class="checkbox variable_pdf_watermark_override" name="variable_pdf_watermark_override[<?php echo $loop; ?>]" <?php checked( 'yes', isset( $_pdf_watermark_override ) ? $_pdf_watermark_override : '', true ); ?>/>
						<?php _e( 'Override PDF Watermark:', 'woocommerce-pdf-watermark' ); ?> <a class="tips" data-tip="<?php _e( 'Enter the number of days before a download link expires, or leave blank.', 'woocommerce-pdf-watermark' ); ?>" href="#">[?]</a>
					</label>
				</p>
				<p class="form-row form-row-first hide_if_pdf_disabled show_if_pdf_override">
					<label><?php _e( 'Watermark Type:', 'woocommerce-pdf-watermark' ); ?> <a class="tips" data-tip="<?php _e( 'The type of watermark you would like to apply to the PDF documents, text-based or image.', 'woocommerce-pdf-watermark' ); ?>" href="#">[?]</a></label>
					<select class="select variable_pdf_watermark_type" name="variable_pdf_watermark_type[<?php echo $loop; ?>]">
						<option value="image" <?php selected( 'image', $_pdf_watermark_type, true ); ?>><?php _e( 'Image', 'woocommerce-pdf-watermark' ); ?></option>
						<option value="text" <?php selected( 'text', $_pdf_watermark_type, true ); ?>><?php _e( 'Text', 'woocommerce-pdf-watermark' ); ?></option>
					</select>
				</p>
				<p class="form-row form-row-last show_if_pdf_image">
					<label><?php _e( 'Watermark Image:', 'woocommerce-pdf-watermark' ); ?> <a class="tips" data-tip="<?php _e( 'URL to the image you want to use for the watermark.', 'woocommerce-pdf-watermark' ); ?>" href="#">[?]</a></label>
					<input type="text" name="variable_pdf_watermark_image[<?php echo $loop; ?>]" value="<?php if ( isset( $_pdf_watermark_image ) ) echo esc_url( $_pdf_watermark_image ); ?>"/>
				</p>
				<p class="form-row form-row-last show_if_pdf_text">
					<label><?php _e( 'Watermark Text:', 'woocommerce-pdf-watermark' ); ?> <a class="tips" data-tip="<?php _e( 'Text you want to use for the watermark. You can use {first_name}, {last_name}, {email}, {order_number}, {order_date}, {site_name}, {site_url} tags to insert customer specific data in the watermark.', 'woocommerce-pdf-watermark' ); ?>" href="#">[?]</a></label>
					<textarea class="short" name="variable_pdf_watermark_text[<?php echo $loop; ?>]" rows="3" style="width:100%"><?php if ( isset( $_pdf_watermark_text ) ) echo esc_attr( $_pdf_watermark_text ); ?></textarea>
				</p>
			</div>
		<?php
		} // End variation_product_data()

		/**
		 * Handle saving of the each variation's data
		 * @param  int $variation_id
		 * @param  int $loop
		 * @return void
		 */
		public function save_variation_data( $variation_id, $loop ) {
			$_pdf_watermark_disable  = isset( $_POST['variable_pdf_watermark_disable'] ) ? $_POST['variable_pdf_watermark_disable'] : array();
			$_pdf_watermark_override = isset( $_POST['variable_pdf_watermark_override'] ) ? $_POST['variable_pdf_watermark_override'] : array();
			$_pdf_watermark_type     = isset( $_POST['variable_pdf_watermark_type'] ) ? $_POST['variable_pdf_watermark_type'] : array();
			$_pdf_watermark_image    = isset( $_POST['variable_pdf_watermark_image'] ) ? $_POST['variable_pdf_watermark_image'] : array();
			$_pdf_watermark_text     = isset( $_POST['variable_pdf_watermark_text'] ) ? $_POST['variable_pdf_watermark_text'] : array();

			if ( isset( $_pdf_watermark_disable[ $loop ] ) ) {
				update_post_meta( $variation_id, '_pdf_watermark_disable', 'yes' );
			} else {
				update_post_meta( $variation_id, '_pdf_watermark_disable', 'no' );
			}

			if ( isset( $_pdf_watermark_override[ $loop ] ) ) {
				update_post_meta( $variation_id, '_pdf_watermark_override', 'yes' );
			} else {
				update_post_meta( $variation_id, '_pdf_watermark_override', 'no' );
			}

			if ( isset( $_pdf_watermark_type[ $loop ] ) ) {
				update_post_meta( $variation_id, '_pdf_watermark_type', $_pdf_watermark_type[ $loop ] );
			} else {
				update_post_meta( $variation_id, '_pdf_watermark_type', '' );
			}

			if ( isset( $_pdf_watermark_image[ $loop ] ) ) {
				update_post_meta( $variation_id, '_pdf_watermark_image', $_pdf_watermark_image[ $loop ] );
			} else {
				update_post_meta( $variation_id, '_pdf_watermark_image', '' );
			}

			if ( isset( $_pdf_watermark_text[ $loop ] ) ) {
				update_post_meta( $variation_id, '_pdf_watermark_text', $_pdf_watermark_text[ $loop ] );
			} else {
				update_post_meta( $variation_id, '_pdf_watermark_text', '' );
			}
		} // End save_variation_data()

		/**
		 * Enqueue admin scripts
		 * @return void
		 */
		public function admin_scripts() {
			$screen = get_current_screen();

			if ( in_array( $screen->id, array( 'product', 'edit-product' ) ) ) {
				wp_enqueue_script( 'wc-pdf-admin-product-meta-boxes', woocommerce_pdf_watermark()->plugin_url() . 'assets/js/admin/meta-boxes-product.js', array( 'jquery' ), woocommerce_pdf_watermark()->version );
				wp_enqueue_script( 'wc-pdf-admin-variation-meta-boxes', woocommerce_pdf_watermark()->plugin_url() . 'assets/js/admin/meta-boxes-product-variation.js', array( 'jquery' ), woocommerce_pdf_watermark()->version );
			}
		} // End admin_scripts()

	}
}
new WC_PDF_Watermark_Product_Data();