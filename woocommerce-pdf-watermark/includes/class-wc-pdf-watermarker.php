<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WC_PDF_Watermarker' ) ) {

	class WC_PDF_Watermarker {

		private $pdf;

		/**
		 * Constructor
		 * @return void
		 */
		public function __construct() {
			// Include the PDF libs
			$this->includes();

			$this->pdf = new FPDI_Protection( 'P', 'pt' );

		} // __construct()

		/**
		 * Include PDF libraries
		 * @return void
		 */
		public function includes() {
			// Include FPDF
			if ( ! class_exists( 'FPDF' ) ) {
				require_once 'lib/fpdf/fpdf.php';
			}
			// Include FPDI
			if ( ! class_exists( 'FPDI' ) ) {
				require_once 'lib/fpdi/fpdi.php';
			}
			// Include FPDI Protection
			if ( ! class_exists( 'FPDI_Protection' ) ) {
				require_once 'lib/fpdi/fpdi_protection.php';
			}
		} // End includes()

		/**
		 * Test file for open-ability
		 * @return int number of pages in document, false otherwise
		 */
		public function tryOpen( $file ) {

			try {
				$pagecount = $this->pdf->setSourceFile( $file );
			} catch ( Exception $e ) {
				return false;
			}

			return $pagecount;
		}

		/**
		 * Apply the watermark to the file
		 * @return void
		 */
		public function watermark( $order_id, $product_id, $file, $new_file, $preview = false ) {
			// Set up the PDF file
			$pagecount = $this->tryOpen( $file );
			if ( false === $pagecount ) {
				wp_die( __( 'Unable to serve the file at this time.  The file does not support watermarking.', 'woocommerce-pdf-watermark' ) );
			}

			$this->pdf->SetAutoPageBreak( 0 );
			$this->pdf->SetMargins( 0, 0 );

			// Get WC PDF Watermark Settings
			$type              = get_option( 'woocommerce_pdf_watermark_type' );
			$x_pos             = get_option( 'woocommerce_pdf_watermark_font_horizontal' );
			$y_pos             = get_option( 'woocommerce_pdf_watermark_font_vertical' );
			$opacity           = get_option( 'woocommerce_pdf_watermark_opacity', '1' );
			$override          = get_post_meta( $product_id, '_pdf_watermark_override', true );
			$horizontal_offset = apply_filters( 'woocommerce_pdf_watermark_horizontal_offset', intval( get_option( 'woocommerce_pdf_watermark_horizontal_offset', 0 ) ), $order_id, $product_id );
			$vertical_offset   = apply_filters( 'woocommerce_pdf_watermark_vertical_offset', intval( get_option( 'woocommerce_pdf_watermark_vertical_offset', 0 ) ), $order_id, $product_id );
			$display_pages     = apply_filters( 'woocommerce_pdf_watermark_page_display', get_option( 'woocommerce_pdf_watermark_page_display', 'all' ), $order_id, $product_id );

			if ( 'yes' == $override ) {
				$type = get_post_meta( $product_id, '_pdf_watermark_type', true );
			}

			if ( $type && 'text' == $type ) {

				// Get settings
				$text        = get_option( 'woocommerce_pdf_watermark_text' );
				if ( 'yes' == $override ) {
					$text = get_post_meta( $product_id, '_pdf_watermark_text', true );
				}
				$color       = $this->hex2rgb( apply_filters( 'woocommerce_pdf_watermark_font_colour', get_option( 'woocommerce_pdf_watermark_font_colour', '#000' ), $order_id, $product_id ) );
				$font        = apply_filters( 'woocommerce_pdf_watermark_font', get_option( 'woocommerce_pdf_watermark_font', 'times' ), $order_id, $product_id );
				$size        = apply_filters( 'woocommerce_pdf_watermark_font_size', get_option( 'woocommerce_pdf_watermark_font_size', '8' ), $order_id, $product_id );
				$line_height = is_numeric( $size ) ? $size : 8;
				$bold        = apply_filters( 'woocommerce_pdf_watermark_font_style_bold', get_option( 'woocommerce_pdf_watermark_font_style_bold' ), $order_id, $product_id );
				$italics     = apply_filters( 'woocommerce_pdf_watermark_font_style_italics', get_option( 'woocommerce_pdf_watermark_font_style_italics' ), $order_id, $product_id );
				$underline   = apply_filters( 'woocommerce_pdf_watermark_font_style_underline', get_option( 'woocommerce_pdf_watermark_font_style_underline' ), $order_id, $product_id );

				// Build style var
				$style = '';
				if ( $bold && 'yes' == $bold ) {
					$style .= 'B';
				}
				if ( $italics && 'yes' == $italics ) {
					$style .= 'I';
				}
				if ( $underline && 'yes' == $underline ) {
					$style .= 'U';
				}

				// Assign font
				$this->pdf->SetFont( $font, $style, $size );

				$text = $this->parse_template_tags( $order_id, $product_id, $text );
				if ( function_exists( 'iconv' ) ) {
					$text = iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', $text );
				} else {
					$text = html_entity_decode( utf8_decode( $text ) );
				}

				// Get number of lines of text, can use a new line to go to a new line
				$lines = 1;
				if ( stripos( $text, "\n" ) !== FALSE ) {
					$lines = explode( "\n", $text );
					$text = explode( "\n", $text );
					$lines = count( $lines );
					$longest_text = 0;
					foreach ( $text as $line ) {
						if ( $this->pdf->GetStringWidth( $line ) > $this->pdf->GetStringWidth( $longest_text ) ) {
							$longest_text = $this->pdf->GetStringWidth( $line );
						}
					}
				} else {
					$longest_text = $this->pdf->GetStringWidth( $text );
				}

				// Loop through pages to add the watermark
				for ( $i = 1; $i <= $pagecount; $i++ ) {
					$tplidx = $this->pdf->importPage( $i );
					$specs = $this->pdf->getTemplateSize( $tplidx );
					$orientation = ( $specs['h'] > $specs['w'] ? 'P' : 'L' );

					$this->pdf->addPage( $orientation, array( $specs['w'], $specs['h'] ) );
					$this->pdf->useTemplate( $tplidx );

					// Check if we must skip this page based on the display on page setting
					if ( 'first' == $display_pages && 1 !== $i ) {
						continue;
					} else if ( 'last' == $display_pages && $i !== $pagecount ) {
						continue;
					} else if ( 'alternate' == $display_pages ) {
						if ( ( $i % 2 ) == 0 ) {
							continue;
						}
					}

					// Horizontal Alignment for Cell function
					$x = 0;
					if ( 'right' == $x_pos ) {
						$x = $specs['w'];
					} elseif( 'center' == $x_pos ) {
						$x = ( $specs['w'] / 2 );
					} elseif( 'left' == $x_pos ) {
						$x = 0;
					}

					// Vertical Alignment for setY function
					$y = 0;
					if ( 'bottom' == $y_pos ) {
						$y = $specs['h'] - ( ( $line_height * $lines ) + 7 );
					} elseif( 'middle' == $y_pos ) {
						$y = ( $specs['h'] / 2 ) - ( $line_height / 2 );
					} elseif( 'top' == $y_pos ) {
						$y = $line_height / 2;
					}

					// Vertical offset
					$y += $vertical_offset;

					$this->pdf->setY( $y );
					$this->pdf->setAlpha( $opacity );
					$this->pdf->SetTextColor( $color[0], $color[1], $color[2] );

					// Put the text watermark down with Cell
					if ( is_array( $text ) ) {
						foreach ( $text as $line ) {
							if ( 'right' == $x_pos ) {
								$_x = $x - ( $this->pdf->GetStringWidth( $line ) + 7 );
							} elseif( 'center' == $x_pos ) {
								$_x = $x - ( $this->pdf->GetStringWidth( $line ) / 2 );
							} else {
								$_x = $x;
							}

							// Horizontal Offset
							$_x += $horizontal_offset;

							$this->pdf->SetXY( $_x, $y );
							$this->pdf->Write( $line_height, $line );
							$y += $line_height;
							//$this->pdf->Cell( 0, 0, $line, 0, 0, $x );
							//$this->pdf->Ln( $line_height );
						}
					} else {
						if ( 'right' == $x_pos ) {
							$_x = $x - ( $this->pdf->GetStringWidth( $text ) + 7 );
						} elseif( 'center' == $x_pos ) {
							$_x = $x - ( $this->pdf->GetStringWidth( $text ) / 2 );
						} else {
							$_x = $x;
						}

						// Horizontal Offset
						$_x += $horizontal_offset;

						$this->pdf->SetXY( $_x, $y );
						$this->pdf->Write( $line_height, $text );
						//$this->pdf->Cell( 0, 0, $text, 0, 0, $x );
					}
					$this->pdf->setAlpha( 1 );

				} // End forloop

			} elseif ( $type && 'image' == $type ) {
				$image      = get_option( 'woocommerce_pdf_watermark_image' );
				if ( 'yes' == $override ) {
					$image = get_post_meta( $product_id, '_pdf_watermark_image', true );
				}
				$image      = str_replace( WP_CONTENT_URL, WP_CONTENT_DIR, $image );
				$image_info = getimagesize( $image );
				$width      = $image_info[0];
				$height     = $image_info[1];

				for ( $i = 1; $i <= $pagecount; $i++ ) {
					$tplidx = $this->pdf->importPage( $i );
					$specs = $this->pdf->getTemplateSize( $tplidx );
					$orientation = ( $specs['h'] > $specs['w'] ? 'P' : 'L' );

					$this->pdf->addPage( $orientation, array( $specs['w'], $specs['h'] ) );
					$this->pdf->useTemplate( $tplidx );

					// Check if we must skip this page based on the display on page setting
					if ( 'first' == $display_pages && 1 !== $i ) {
						continue;
					} else if ( 'last' == $display_pages && $i !== $pagecount ) {
						continue;
					} else if ( 'alternate' == $display_pages ) {
						if ( ( $i % 2 ) == 0 ) {
							continue;
						}
					}

					// Horizontal alignment
					$x = 0;
					if ( 'right' == $x_pos ) {
						$x = $specs['w'] - ( $width * 20 / 72 );
					} elseif ( 'center' == $x_pos ) {
						$x = ( $specs['w'] / 2 ) - ( $width * 20 / 72 );
					} elseif ( 'left' == $x_pos ) {
						$x = '0';
					}
					// Horizontal Offset
					$x += $horizontal_offset;

					// Vertical alignment
					$y = 0;
					if ( 'bottom' == $y_pos ) {
						$y = $specs['h'] - ( $height );
					} elseif ( 'middle' == $y_pos ) {
						$y = ( $specs['h'] / 2 ) - ( $height );
					} elseif ( 'top' == $y_pos ) {
						$y = '0';
					}
					// Vertical offset
					$y += $vertical_offset;

					$this->pdf->SetAlpha( $opacity );
					$this->pdf->Image( $image, $x, $y );
					$this->pdf->SetAlpha( 1 );
				} // End forloop
			} // End else for image type

			// Apply protection settings
			$password_protect      = apply_filters( 'woocommerce_pdf_watermark_password_protects', get_option( 'woocommerce_pdf_watermark_password_protects', 'no' ), $order_id, $product_id );
			$do_not_allow_copy     = apply_filters( 'woocommerce_pdf_watermark_copy_protection', get_option( 'woocommerce_pdf_watermark_copy_protection', 'no' ), $order_id, $product_id );
			$do_not_allow_print    = apply_filters( 'woocommerce_pdf_watermark_print_protection', get_option( 'woocommerce_pdf_watermark_print_protection', 'no' ), $order_id, $product_id );
			$do_not_allow_modify   = apply_filters( 'woocommerce_pdf_watermark_modification_protection', get_option( 'woocommerce_pdf_watermark_modification_protection', 'no' ), $order_id, $product_id );
			$do_not_allow_annotate = apply_filters( 'woocommerce_pdf_watermark_annotate_protection', get_option( 'woocommerce_pdf_watermark_annotate_protection', 'no' ), $order_id, $product_id );
			$protection_array = array();

			if ( 'no' == $do_not_allow_copy ) {
				$protection_array[] = 'copy';
			}
			if ( 'no' == $do_not_allow_print ) {
				$protection_array[] = 'print';
			}
			if ( 'no' == $do_not_allow_modify ) {
				$protection_array[] = 'modify';
			}
			if ( 'no' == $do_not_allow_annotate ) {
				$protection_array[] = 'annot-forms';
			}
			$user_pass = '';
			if ( 'yes' == $password_protect ) {
				if ( ! $preview ) {
					$order = wc_get_order( $order_id );
					$user_pass = version_compare( WC_VERSION, '3.0', '<' ) ? $order->billing_email : $order->get_billing_email();
				} else {
					$current_user = wp_get_current_user();
					$user_pass = $current_user->user_email;
				}
			}

			$this->pdf->SetProtection( $protection_array, apply_filters( 'woocommerce_pdf_watermark_file_password', $user_pass, $order_id, $product_id ) );

			if ( $preview ) {
				$this->pdf->Output();
			} else {
				$this->pdf->Output( $new_file, 'F' );
			}
		}

		/**
		 * Convert HEX color code to RGB
		 * @param  string $color HEX color code
		 * @return array
		 */
		public function hex2rgb( $color ) {
			if ( $color[0] == '#')
				$color = substr( $color, 1 );

			if ( strlen( $color ) == 6 ) {
				list( $r, $g, $b ) = array(
					$color[0] . $color[1],
					$color[2] . $color[3],
					$color[4] . $color[5]
				);
			} elseif ( strlen( $color ) == 3 ) {
				list( $r, $g, $b ) = array(
					$color[0] . $color[0],
					$color[1] . $color[1],
					$color[2] . $color[2]
				);
			} else {
				return false;
			}

			$r = hexdec( $r );
			$g = hexdec( $g );
			$b = hexdec( $b );

			return array( $r, $g, $b );
		} // End hex2rgb()

		/**
		 * Parse text for template tags and populate it
		 * @param  int $order_id
		 * @param  string $text
		 * @return string
		 */
		public function parse_template_tags( $order_id, $product_id, $text ) {
			if ( false === strpos( $text, '{' ) ) {
				return $text;
			}

			if ( is_null( $order_id ) || is_null( $product_id ) ) {
				return $text;
			}

			$order = wc_get_order( $order_id );
			$product = wc_get_product( $product_id );

			$first_name = version_compare( WC_VERSION, '3.0', '<' ) ? $order->billing_first_name : $order->get_billing_first_name();
			$last_name = version_compare( WC_VERSION, '3.0', '<' ) ? $order->billing_last_name : $order->get_billing_last_name();
			$email = version_compare( WC_VERSION, '3.0', '<' ) ? $order->billing_email : $order->get_billing_email();
			$billing_company = version_compare( WC_VERSION, '3.0', '<' ) ? $order->billing_company : $order->get_billing_company();
			$order_number = $order->get_order_number();
			$order_date = version_compare( WC_VERSION, '3.0', '<' ) ? $order->order_date : ( $order->get_date_created() ? $order->get_date_created()->date( 'Y-m-d H:i:s' ) : '' );
			$site_name = get_bloginfo( 'name' );
			$site_url = home_url();

			$unparsed_text = $parsed_text = $text;

			$parsed_text = str_replace( '{first_name}', $first_name, $parsed_text );
			$parsed_text = str_replace( '{last_name}', $last_name, $parsed_text );
			$parsed_text = str_replace( '{email}', $email, $parsed_text );
			$parsed_text = str_replace( '{billing_company}', $billing_company, $parsed_text );
			$parsed_text = str_replace( '{order_number}', $order_number, $parsed_text );
			$parsed_text = str_replace( '{order_date}', $order_date, $parsed_text );
			$parsed_text = str_replace( '{site_name}', $site_name, $parsed_text );
			$parsed_text = str_replace( '{site_url}', $site_url, $parsed_text );

			return apply_filters( 'woocommerce_pdf_watermark_parse_template_tags', $parsed_text, $unparsed_text, $order, $product );
		} // End parse_template_tags()
	}
}
