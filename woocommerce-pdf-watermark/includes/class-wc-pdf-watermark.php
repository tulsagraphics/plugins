<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WC_PDF_Watermark' ) ) {

	class WC_PDF_Watermark {

		/**
		 * Single instance of the class
		 * @var object
		 */
		private static $_instance = null;

		/**
		 * Plugin version
		 * @var string
		 */
		public $version;

		/**
		 * Main plugin file
		 * @var string
		 */
		public $file;

		/**
		 * Stamped file locations
		 * @var array
		 */
		public $stamped_file = [];

		/**
		 * Constructor
		 * @return void
		 */
		public function __construct( $file, $version ) {
			$this->file = $file;
			$this->version = $version;

			// Admin specific functionality
			if ( is_admin() ) {
				require_once 'class-wc-pdf-watermark-product-data.php';
				add_filter( 'woocommerce_get_settings_pages', array( $this, 'load_settings_class' ), 10, 1 );
				add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
			}

			add_action( 'init', array( $this, 'init' ) );
			// Hook into download function to watermark files, hook in later to accomodate other plugins like Amazon S3 and becuase we use a custom download method
			add_filter( 'woocommerce_product_file_download_path', array( $this, 'watermark_pdf_file' ), 50, 3 );
			add_filter( 'woocommerce_pdf_watermark_file', array( $this, 'watermark_file' ), 50, 3 );
			// Hook into shutdown to see if we should unlink the stamped file, download method calls exit which calls shutdown hook in WP.
			add_action( 'shutdown', array( $this, 'cleanup_files' ) );
			// Check if we should render a preview file
			add_action( 'admin_init', array( $this, 'maybe_render_preview' ) );
			// Assess downloadable PDFs for compatibility
			add_action( 'admin_notices', array( $this, 'file_compatibility_check' ) );
		} // End __construct()

		/**
		 * Get single instance of class
		 * @return object WC_PDF_Stamper
		 */
		public static function instance ( $file, $version ) {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self( $file, $version );
			}
			return self::$_instance;
		} // End instance()

		/**
		 * Cloning is forbidden.
		 */
		public function __clone () {
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '1.0.0' );
		} // End __clone()

		/**
		 * Unserializing instances of this class is forbidden.
		 */
		public function __wakeup () {
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '1.0.0' );
		} // End __wakeup()

		/**
		 * Include and load the settings class
		 * @return object
		 */
		public function load_settings_class() {
			$settings[] = include 'class-wc-settings-pdf-watermark.php';
			return $settings;
		} // End load_settings_class()

		/**
		 * Load the textdomain, includes, etc
		 * @return void
		 */
		public function init() {
			load_plugin_textdomain( 'woocommerce-pdf-watermark', false, dirname( plugin_basename( $this->file ) ) . '/languages/' );
			include_once( dirname( __FILE__ ) . '/class-wc-pdf-watermark-privacy.php' );
		} // End init()

		/**
		 * Simple check for pdf extension
		 * @param  string $file_path
		 * @return bool
		 */
		private static function has_pdf_extension( $file_path ) {

			$file_info = pathinfo( $file_path );
			$file_ext = $file_info['extension'];
			$file_ext = strtolower( substr( $file_ext, 0, 3 ) );
			return ( 'pdf' === $file_ext );

		}


		/**
		 * Simple check for local files
		 * @param  string $file_path
		 * @return bool
		 */
		private static function is_local_file( $file_path ) {
			$upload_dir = wp_upload_dir();
			return ( false !== stripos( $file_path, $upload_dir['basedir'] ) );

		}


		/**
		 * Get a temporary file name (e.g. for bringing a remote file locally for processing)
		 * @param  string $original_file
		 * @return string
		 */
		private static function get_temporary_file_name( $original_file ) {

			$file_name = basename( $original_file );

			if ( apply_filters( 'wc_pdf_watermark_use_uploads_dir', false ) ) {

				$file_name += '.' . $_GET['order'];

			}

			// Remove any query from the remote file name to avoid exposing query parameters and
			// a generally ugly file name to the user (e.g. for S3 served files)
			$file_name = preg_replace( '/[?].*$/', '', $file_name );

			// In the unlikely event that file_name is now empty, use a generic filename
			if ( empty( $file_name ) ) {
				$file_name = 'untitled.pdf';
			}

			return $file_name;

		}


		/**
		 * Get a temporary file folder (e.g. for bringing a remote file locally for processing)
		 * @return string
		 */
		private static function get_temporary_file_folder() {

			if ( apply_filters( 'wc_pdf_watermark_use_uploads_dir', false ) ) {
				$wp_upload_dir = wp_upload_dir();
				$new_file_path = $wp_upload_dir['basedir'] . '/wc-pdf-watermark/';

				// Setup dir in wp-content/uploads to store watermarked files
				if ( false === get_transient( 'wc_pdf_watermark_files' ) ) {
					wp_mkdir_p( $new_file_path );

					// Protect the directory from browsing
					if ( ! file_exists( $new_file_path . 'index.php' ) ) {
						@file_put_contents( $new_file_path . 'index.php', '<?php' . PHP_EOL . '// Silence is golden.' );
					}

					// Top level .htaccess
					$rules = "Options -Indexes";
					if ( file_exists( $new_file_path . '.htaccess' ) ) {
						$contents = @file_get_contents( $new_file_path . '.htaccess' );

						if ( $contents !== $rules || ! $contents ) {
							@file_put_contents( $new_file_path . '.htaccess', $rules );
						}
					}
					set_transient( 'wc_pdf_watermark_files', true, 3600 * 24 );
				}
			} else {
				$new_file_path = trailingslashit( sys_get_temp_dir() );
			}

			return $new_file_path;

		}


		/**
		 * Fetch a remote file to a local path and filename
		 * @param  string $original_file
		 * @param  string $local_path_and_file
		 * @return void
		 */
		private static function fetch_remote_file( $original_file, $local_path_and_file ) {

			if ( ! file_exists( $local_path_and_file ) ) {
				$response = wp_remote_get( $original_file );
				$remote_file = wp_remote_retrieve_body( $response );
				@file_put_contents( $local_path_and_file, $remote_file );
			}

		}


		/**
		 * Check if we have a PDF file and watermark it based on settings
		 * @param  string $file_path
		 * @param  object $product
		 * @param  int $download_id
		 * @return string
		 */
		public function watermark_pdf_file( $file_path, $product, $download_id ) {

			// Make sure this does not fire on the download permissions meta boxes
			if ( function_exists( 'get_current_screen' ) ) {
				$screen = get_current_screen();
				if ( ! is_null( $screen ) && in_array( $screen->id, [ 'shop_order', 'shop_subscription' ] ) ) {
					return;
				}
			}

			if ( ! isset( $_GET['download_file'] ) ) {
				return;
			}

			// Check if we should skip watermark based on product specific settings
			// Check variation level first
			if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
				if ( isset( $product->variation_id ) && 'yes' == get_post_meta( $product->variation_id , '_pdf_watermark_disable', true ) ) {
					return $file_path;
				}
			} else {
				if ( $product->is_type( 'variation' ) && 'yes' == get_post_meta( $product->get_id() , '_pdf_watermark_disable', true ) ) {
					return $file_path;
				}
			}

			// Check single product
			if ( 'yes' == get_post_meta( $product->get_id() , '_pdf_watermark_disable', true ) ) {
				return $file_path;
			}

			// Setup a new file to process
			$original_file = str_replace( WP_CONTENT_URL, WP_CONTENT_DIR, $file_path );

			// Make sure we have a pdf file, if not return original file
			if ( ! self::has_pdf_extension( $original_file ) ) {
				return $file_path;
			}

			// If we made it till here it means the file should be watermarked.

			// Get the product ID to do variation & single level overrides.
			if ( version_compare( WC_VERSION, '3.0', '<' ) && isset( $product->variation_id ) ) {
				$product_id = $product->variation_id;
			} else {
				$product_id = $product->get_id();
			}

			// Get the order id to look up order info
			$download_data = $this->get_download_data( absint( $_GET['download_file'] ), wc_clean( $_GET['order'] ), wc_clean( isset( $_GET['key'] ) ? preg_replace( '/\s+/', ' ', $_GET['key'] ) : '' ) );
			$order_id = $download_data->order_id;

			$new_file = $this->watermark_file( $original_file, $product_id, $order_id );

			return $new_file;
		} // End stamp_pdf_file()

		/**
		 * Actual watermark of a PDF file
		 * @param  string $original_file Original file path
		 * @param  int $order_id
		 * @param  int $product_id
		 * @return string New file path
		 */
		public function watermark_file( $original_file, $product_id, $order_id  ) {
			if ( ! class_exists( 'WC_PDF_Watermarker' ) ) {
				require_once 'class-wc-pdf-watermarker.php';
			}

			$pdf = new WC_PDF_Watermarker();

			// Add fallback to use a custom dir in wp-content/uploads should host not have tmp folder
			$new_file_path = self::get_temporary_file_folder();
			$file_name = self::get_temporary_file_name( $original_file );

			// Make provision for local and remote hosted files
			if ( self::is_local_file( $original_file ) ) {
				// We have a local file
				$pdf->watermark( $order_id, $product_id, $original_file, $new_file_path . $file_name );
			} else {
				// We have a remote file
				$remote_name = $file_name . '.remote';
				self::fetch_remote_file( $original_file, $new_file_path . $remote_name );
				$pdf->watermark( $order_id, $product_id, $new_file_path . $remote_name, $new_file_path . $file_name );
				// Delete downloaded file
				unlink( $new_file_path . $remote_name );
			}

			$new_file = $new_file_path . $file_name;

			// Set the stamped file so we can delete it via shutdown hook.
			$this->stamped_file[] = $new_file;

			return $new_file;
		}

		/**
		 * Fetch the download data for a specific download
		 * @param  int $product_id
		 * @param  string $order_key
		 * @param  int $download_id
		 * @return object
		 */
		public function get_download_data( $product_id, $order_key, $download_id ) {
			global $wpdb;

			$query = "SELECT * FROM " . $wpdb->prefix . "woocommerce_downloadable_product_permissions ";
			$query .= "WHERE order_key = %s ";
			$query .= "AND product_id = %s ";
			$query .= "AND download_id = %s ";

			return $wpdb->get_row( $wpdb->prepare( $query, array( $order_key, $product_id, $download_id ) ) );
		} // End get_download_data()

		/**
		 * Check if there is a stamped file and delete it.
		 * @return void
		 */
		public function cleanup_files() {
			if ( ! empty( $this->stamped_file ) ) {
				array_map( 'unlink', $this->stamped_file );
				$this->stamped_file = [];
			}
		} // End cleanup_files()

		/**
		 * Get the plugin url
		 * @return string
		 */
		public function plugin_url() {
			return plugin_dir_url( $this->file );
		} // End plugin_url()

		/**
		 * Check if we must render a preview pdf
		 * @return void
		 */
		public function maybe_render_preview() {
			// Check if we must generate a preview pdf
			if ( isset( $_GET['pdf-watermark-preview'] ) && 1 == $_GET['pdf-watermark-preview'] && isset( $_GET['pdf-watermark-preview-nonce'] ) ) {
				if ( 1 == wp_verify_nonce( $_GET['pdf-watermark-preview-nonce'], 'preview-pdf' ) ) {
					if ( ! class_exists( 'WC_PDF_Watermarker' ) ) {
						require_once 'class-wc-pdf-watermarker.php';
					}
					$pdf = new WC_PDF_Watermarker();
					$test_pdf_file = plugin_dir_path( trailingslashit( woocommerce_pdf_watermark()->file ) ) . 'assets/pdf/pdf-sample.pdf';
					$pdf->watermark( null, null, $test_pdf_file, null, true );
					die();
				}
			}
		} // End maybe_render_preview()

		/**
		 * Output settings screen scripts
		 * @return void
		 */
		public function admin_scripts() {
			$screen = get_current_screen();
			$wc_screen_id = sanitize_title( __( 'WooCommerce', 'woocommerce' ) );
			if ( in_array( $screen->id, array( $wc_screen_id . '_page_wc-settings' ) ) ) {
				wp_enqueue_script( 'wc-pdf-admin-settings', woocommerce_pdf_watermark()->plugin_url() . 'assets/js/admin/settings.js', array( 'jquery' ), woocommerce_pdf_watermark()->version );
			}
		} // End admin_scripts()


		public function file_compatibility_check() {
			global $post;

			$screen = get_current_screen();
			if ( 'product' !== $screen->id ) {
				return;
			}

			$product_id = $post->ID;

			$product = wc_get_product( $product_id );

			if ( ! $product ) {
				return;
			}

			$files_to_evaluate = array();

			// Build a list of files to test, eliminating duplicates

			if ( $product->is_type( 'variable' ) ) {

				$available_variations = $product->get_available_variations();

				foreach ( $product->get_children() as $child_id ) {

					if ( 'yes' == get_post_meta( $child_id, '_pdf_watermark_disable', true ) ) {
						continue;
					}

					$variation = wc_get_product( $child_id );

					$files = is_callable( array( $variation, 'get_downloads' ) ) ? $variation->get_downloads() : $variation->get_files();

					foreach ( (array) $files as $file ) {

						if ( self::has_pdf_extension( $file['file'] ) ) {

							$files_to_evaluate[] = $file;

						}

					}

				}

			} else {

				// Check single product

				if ( 'yes' == get_post_meta( $product_id , '_pdf_watermark_disable', true ) ) {
					return;
				}

				$files = is_callable( array( $product, 'get_downloads' ) ) ? $product->get_downloads() : $product->get_files();

				foreach ( (array) $files as $file ) {

					if ( self::has_pdf_extension( $file['file'] ) ) {

						$files_to_evaluate[] = $file;

					}

				}

			}

			// If we have any files to evaluate, evaluate them now

			if ( 0 === count( $files_to_evaluate ) ) {
				return;
			}

			// Generate a hash of the file array - if it has not changed since the last
			// successful check, don't bother running the compatibility check again
			// since this is kinda expensive, especially for remote files
			$checksum = hash( 'md5', json_encode( $files_to_evaluate ) );
			$last_good_checksum = get_post_meta( $product_id, '_pdf_watermark_compatibility_checksum', true );

			if ( $checksum === $last_good_checksum ) {
				return;
			}

			$incompatible_files = array();

			// evaluate each attached file for compatibility with the watermarker

			if ( ! class_exists( 'WC_PDF_Watermarker' ) ) {
				require_once 'class-wc-pdf-watermarker.php';
			}

			$pdf = new WC_PDF_Watermarker();

			foreach ( (array) $files_to_evaluate as $file_to_evaluate ) {

				// Attempt to coerce to a local path
				$original_file = str_replace( WP_CONTENT_URL, WP_CONTENT_DIR, $file_to_evaluate['file'] );

				if ( self::is_local_file( $original_file ) ) {

					if ( false === $pdf->tryOpen( $original_file ) ) {
						$incompatible_files[] = $file_to_evaluate;
					}

				} else {

					$new_file_path = self::get_temporary_file_folder();
					$file_name = self::get_temporary_file_name( $original_file );
					$remote_name = $file_name . '.remote';
					self::fetch_remote_file( $original_file, $new_file_path . $remote_name );

					if ( false === $pdf->tryOpen( $new_file_path . $remote_name ) ) {
						$incompatible_files[] = $file_to_evaluate;
					}

					unlink( $new_file_path . $remote_name );

				}

			}

			if ( 0 === count( $incompatible_files ) ) {
				// update the checksum to avoid running this again until the files change
				$checksum = hash( 'md5', json_encode( $files_to_evaluate ) );
				update_post_meta( $product_id, '_pdf_watermark_compatibility_checksum', $checksum );
				return;
			}

			echo "<div class='error'>";

			echo "<p>";
			esc_html_e( "The following Downloadable Files are not compatible with PDF Watermarker.  You should disable watermarking on this product or choose different files.", "woocommerce-pdf-watermark" );
			echo "</p>";

			echo "<ol>";

			foreach ( (array) $incompatible_files as $incompatible_file ) {

				echo "<li>";
				echo esc_html( $incompatible_file['name'] . ' - ' . $incompatible_file['file'] );
				echo "</li>";

			}

			echo "</ol>";

			echo "</div>";

		}

	}

}




