<?php
/**
 * PDF Watermark Settings
 *
 * @author 		Gerhard Potgieter
 * @category 	Admin
 * @package 	WooCommerce/Admin
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WC_Settings_PDF_Watermark' ) ) :

/**
 * WC_Settings_PDF_Watermark
 */
class WC_Settings_PDF_Watermark extends WC_Settings_Page {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->id    = 'pdf_watermark';
		$this->label = __( 'PDF Watermark', 'woocommerce-pdf-watermark' );

		add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_page' ), 20 );
		add_action( 'woocommerce_settings_' . $this->id, array( $this, 'output' ) );
		add_action( 'woocommerce_settings_save_' . $this->id, array( $this, 'save' ) );
		add_action( 'woocommerce_admin_field_button', array( $this, 'button_field' ), 10, 1 );
	}

	/**
	 * Get settings array
	 *
	 * @return array
	 */
	public function get_settings() {
		return apply_filters( 'woocommerce_pdf_watermark_settings', array(
			array( 'title' => __( 'General', 'woocommerce-pdf-watermark' ), 'type' => 'title', 'id' => 'general_options' ),
			array(
				'id' => 'woocommerce_pdf_watermark_type',
				'title' => __( 'Watermark Type', 'woocommerce-pdf-watermark' ),
				'desc' => __( 'The type of watermark you would like to apply to PDF documents, text-based or image.', 'woocommerce-pdf-watermark' ),
				'type' => 'select',
				'options' => array(
					'text' => __( 'Text', 'woocommerce-pdf-watermark' ),
					'image' => __( 'Image', 'woocommerce-pdf-watermark' ),
				),
			),
			array(
				'id' => 'woocommerce_pdf_watermark_image',
				'title' => __( 'Watermark Image', 'woocommerce-pdf-watermark' ),
				'desc' => sprintf( __( 'The image to apply as a watermark to PDF documents. Upload your image using the <a href="%s">media uploader</a>.', 'woocommerce-pdf-watermark' ), admin_url( 'media-new.php' ) ),
				'type' => 'text',
				'css' => 'min-width:300px;',
				'class' => 'hide_if_text',
			),
			array(
				'id' => 'woocommerce_pdf_watermark_text',
				'title' => __( 'Watermark Text', 'woocommerce-pdf-watermark' ),
				'desc' => __( 'The text to use as a watermark on PDF documents. You can use {first_name}, {last_name}, {email}, {order_number}, {order_date}, {site_name}, {site_url} tags to insert customer specific data in the watermark.', 'woocommerce-pdf-watermark' ),
				'type' => 'textarea',
				'css' => 'width:100%;',
				'class' => 'hide_if_image',
			),
			array( 'type' => 'sectionend', 'id' => 'general_options'),

			array( 'title' => __( 'Styling / Layout', 'woocommerce-pdf-watermark' ), 'type' => 'title', 'id' => 'styling_options' ),
			array(
				'id' => 'woocommerce_pdf_watermark_font',
				'title' => __( 'Font', 'woocommerce-pdf-watermark' ),
				'default' => 'times',
				'type' => 'select',
				'options' => array(
					'times' => __( 'Times', 'woocommerce-pdf-watermark' ),
					'helvetica' => __( 'Helvetica', 'woocommerce-pdf-watermark' ),
					'arial' => __( 'Arial', 'woocommerce-pdf-watermark' ),
					'courier' => __( 'Courier', 'woocommerce-pdf-watermark' ),
					'symbol' => __( 'Symbol', 'woocommerce-pdf-watermark' ),
					'zapfdingbats' => __( 'ZapfDingbats', 'woocommerce-pdf-watermark' ),
				),
				'class' => 'hide_if_image',
			),
			array(
				'id' => 'woocommerce_pdf_watermark_font_size',
				'title' => __( 'Font Size', 'woocommerce-pdf-watermark' ),
				'default' => '8',
				'type' => 'text',
				'class' => 'hide_if_image',
				'css' => 'max-width:30px;'
			),
			array(
				'id' => 'woocommerce_pdf_watermark_font_style_bold',
				'title' => __( 'Font Style', 'woocommerce-pdf-watermark' ),
				'type' => 'checkbox',
				'checkboxgroup' => 'start',
				'desc' => __( 'Bold', 'woocommerce-pdf-watermark' ),
				'class' => 'hide_if_image',
			),
			array(
				'id' => 'woocommerce_pdf_watermark_font_style_italics',
				'title' => __( 'Font Style', 'woocommerce-pdf-watermark' ),
				'type' => 'checkbox',
				'checkboxgroup' => '',
				'desc' => __( 'Italics', 'woocommerce-pdf-watermark' ),
				'class' => 'hide_if_image',
			),
			array(
				'id' => 'woocommerce_pdf_watermark_font_style_underline',
				'title' => __( 'Font Style', 'woocommerce-pdf-watermark' ),
				'type' => 'checkbox',
				'checkboxgroup' => 'end',
				'desc' => __( 'Underline', 'woocommerce-pdf-watermark' ),
				'class' => 'hide_if_image',
			),
			array(
				'id' => 'woocommerce_pdf_watermark_font_colour',
				'title' => __( 'Font Colour', 'woocommerce-pdf-watermark' ),
				'type' => 'color',
				'default' => '#000',
				'class' => 'hide_if_image ',
			),
			array(
				'id' => 'woocommerce_pdf_watermark_opacity',
				'title' => __( 'Watermark Opacity', 'woocommerce-pdf-watermark' ),
				'type' => 'select',
				'desc' => __( '0 = fully transparent, 1 = fully opaque.', 'woocommerce-pdf-watermark' ),
				'default' => '1',
				'options' => array(
					'0' => __( '0', 'woocommerce-pdf-watermark' ),
					'0.1' => __( '0.1', 'woocommerce-pdf-watermark' ),
					'0.2' => __( '0.2', 'woocommerce-pdf-watermark' ),
					'0.3' => __( '0.3', 'woocommerce-pdf-watermark' ),
					'0.4' => __( '0.4', 'woocommerce-pdf-watermark' ),
					'0.5' => __( '0.5', 'woocommerce-pdf-watermark' ),
					'0.6' => __( '0.6', 'woocommerce-pdf-watermark' ),
					'0.7' => __( '0.7', 'woocommerce-pdf-watermark' ),
					'0.8' => __( '0.8', 'woocommerce-pdf-watermark' ),
					'0.9' => __( '0.9', 'woocommerce-pdf-watermark' ),
					'1' => __( '1', 'woocommerce-pdf-watermark' ),
				)
			),
			array(
				'id' => 'woocommerce_pdf_watermark_font_horizontal',
				'title' => __( 'Horizontal Position', 'woocommerce-pdf-watermark' ),
				'type' => 'select',
				'options' => array(
					'left' => __( 'Left', 'woocommerce-pdf-watermark' ),
					'right' => __( 'Right', 'woocommerce-pdf-watermark' ),
					'center' => __( 'Center', 'woocommerce-pdf-watermark' ),
				),
			),
			array(
				'id' => 'woocommerce_pdf_watermark_font_vertical',
				'title' => __( 'Vertical Position', 'woocommerce-pdf-watermark' ),
				'type' => 'select',
				'options' => array(
					'top' => __( 'Top', 'woocommerce-pdf-watermark' ),
					'middle' => __( 'Middle', 'woocommerce-pdf-watermark' ),
					'bottom' => __( 'Bottom', 'woocommerce-pdf-watermark' ),
				),
			),
			array(
				'id' => 'woocommerce_pdf_watermark_horizontal_offset',
				'title' => __( 'Horizontal Offset', 'woocommerce-pdf-watermark' ),
				'desc' => __( 'A positive/negative offset in pt to horizontally allign the watermark.', 'woocommerce-pdf-watermark' ),
				'type' => 'text',
				'default' => '0',
				'css' => 'max-width:50px;'
			),
			array(
				'id' => 'woocommerce_pdf_watermark_vertical_offset',
				'title' => __( 'Vertical Offset', 'woocommerce-pdf-watermark' ),
				'desc' => __( 'A positive/negative offset in pt to vertically allign the watermark.', 'woocommerce-pdf-watermark' ),
				'type' => 'text',
				'default' => '0',
				'css' => 'max-width:50px;'
			),
			array(
				'id' => 'woocommerce_pdf_watermark_page_display',
				'title' => __( 'Display on pages', 'woocommerce-pdf-watermark' ),
				'type' => 'select',
				'options' => array(
					'all' => __( 'All', 'woocommerce-pdf-watermark' ),
					'first' => __( 'First', 'woocommerce-pdf-watermark' ),
					'last' => __( 'Last', 'woocommerce-pdf-watermark' ),
					'alternate' => __( 'Alternate pages', 'woocommerce-pdf-watermark' ),
				),
				'default' => 'all',
			),
			array(
				'id' => 'woocommerce_pdf_watermark_preview',
				'title' => __( 'Preview', 'woocommerce-pdf-watermark' ),
				'text' => __( 'Preview PDF Watermark Settings', 'woocommerce-pdf-watermark' ),
				'type' => 'button',
				'href' => wp_nonce_url( add_query_arg( array( 'pdf-watermark-preview' => 1 ) ), 'preview-pdf', 'pdf-watermark-preview-nonce' )
			),
			array( 'type' => 'sectionend', 'id' => 'styling_options'),
			array( 'title' => __( 'Advanced', 'woocommerce-pdf-watermark' ), 'type' => 'title', 'id' => 'advanced_options' ),
			array(
				'id' => 'woocommerce_pdf_watermark_password_protects',
				'title' => __( 'Enable Password Protection', 'woocommerce-pdf-watermark' ),
				'type' => 'checkbox',
				'desc' => __( 'Require a password to open the PDF document. The customer\'s email address will be used as the password, for preview mode use your account email address.', 'woocommerce-pdf-watermark' ),
			),
			array(
				'id' => 'woocommerce_pdf_watermark_copy_protection',
				'title' => __( 'Enable Copy Protection', 'woocommerce-pdf-watermark' ),
				'type' => 'checkbox',
				'desc' => __( 'Do not allow copying or otherwise extracting text and graphics from the PDF document.', 'woocommerce-pdf-watermark' ),
			),
			array(
				'id' => 'woocommerce_pdf_watermark_print_protection',
				'title' => __( 'Enable Print Protection', 'woocommerce-pdf-watermark' ),
				'type' => 'checkbox',
				'desc' => __( 'Do not allow printing of the PDF document.', 'woocommerce-pdf-watermark' ),
			),
			array(
				'id' => 'woocommerce_pdf_watermark_modification_protection',
				'title' => __( 'Enable Modification Protection', 'woocommerce-pdf-watermark' ),
				'type' => 'checkbox',
				'desc' => __( 'Do not allow modifying of the PDF document, other than annotating if that is not disabled.', 'woocommerce-pdf-watermark' ),
			),
			array(
				'id' => 'woocommerce_pdf_watermark_annotate_protection',
				'title' => __( 'Enable Annotate Protection', 'woocommerce-pdf-watermark' ),
				'type' => 'checkbox',
				'desc' => __( 'Do not allow adding or modifying of text annotations, filling in of interactive form fields, and, if modify protection is enabled, creating or modifying of interactive form fields (including signature fields).', 'woocommerce-pdf-watermark' ),
			),
			array( 'type' => 'sectionend', 'id' => 'advanced_options'),

		) ); // End get_settings()
	}

	/**
	 * Add button field type to WooCommerce
	 * @param  array $params
	 * @return void
	 */
	public function button_field( $params ) {
		?>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<label for="<?php echo esc_attr( $params['id'] ); ?>"><?php echo esc_html( $params['title'] ); ?></label>
			</th>
			<td class="forminp forminp-<?php echo sanitize_title( $params['type'] ) ?>">
				<a
					id="<?php echo esc_attr( $params['id'] ); ?>"
					href="<?php echo esc_url( $params['href'] ); ?>"
					class="button"
					target="_blank"
				/><?php echo sanitize_text_field( $params['text'] ); ?></a><p style="margin-top:0"><?php echo wp_kses_post( $params['desc'] ); ?></p>
			</td>
		</tr>
		<?php
	} // End button_field()

}

endif;

return new WC_Settings_PDF_Watermark();
