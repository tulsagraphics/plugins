jQuery( function( $ ){
	$( '#variable_product_options' ).on( 'change', 'input.variable_pdf_watermark_disable', function () {
		$( this ).closest( '.woocommerce_variation' ).find( '.hide_if_pdf_disabled' ).show();
		$( this ).closest( '.woocommerce_variation' ).find( '.show_if_pdf_override' ).hide();
		$( this ).closest( '.woocommerce_variation' ).find( '.show_if_pdf_text, .show_if_pdf_image' ).hide();

		if ( $( this ).is( ':checked' ) ) {
			$( this ).closest( '.woocommerce_variation' ).find( '.hide_if_pdf_disabled' ).hide();
			$( this ).closest( '.woocommerce_variation' ).find( 'input.variable_pdf_watermark_override' ).change();
		}
	});

	$( '#variable_product_options' ).on( 'change', 'input.variable_pdf_watermark_override', function() {
		$( this ).closest( '.woocommerce_variation' ).find( '.show_if_pdf_override' ).hide();
		$( this ).closest( '.woocommerce_variation' ).find( '.show_if_pdf_text, .show_if_pdf_image' ).hide();

		if ( $( this ).is( ':checked' ) ) {
			$( this ).closest( '.woocommerce_variation' ).find( '.show_if_pdf_override' ).show();
			$( this ).closest( '.woocommerce_variation' ).find( 'select.variable_pdf_watermark_type' ).change();
		}
	});

	$( '#variable_product_options' ).on( 'change', 'select.variable_pdf_watermark_type', function() {
		$( this ).closest( '.woocommerce_variation' ).find( '.show_if_pdf_text, .show_if_pdf_image' ).hide();
		$( this ).closest( '.woocommerce_variation' ).find( '.show_if_pdf_' + $( this ).val() ).show();
	});

	$( 'input.variable_pdf_watermark_override, input.variable_pdf_watermark_disable' ).change();
});