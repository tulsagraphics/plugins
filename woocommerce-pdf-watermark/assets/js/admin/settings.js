jQuery( function( $ ){

	$('select#woocommerce_pdf_watermark_type').change( function(){
		$( '.hide_if_image, .hide_if_text' ).closest( 'tr' ).show();
		if ( $( this ).val() == 'image' ) {
			$( '.hide_if_image' ).closest( 'tr' ).hide();
			$( '#woocommerce_pdf_watermark_font_style_bold' ).closest( 'tr' ).hide();
		} else {
			$( '.hide_if_text' ).closest( 'tr' ).hide();
			$( '#woocommerce_pdf_watermark_font_style_bold' ).closest( 'tr' ).show();
		}
	}).change();

});