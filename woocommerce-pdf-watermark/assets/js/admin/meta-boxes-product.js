jQuery( function( $ ){

	$('select#_pdf_watermark_type').change( function(){
		show_hide_pdf_product_fields();
	}).change();

	$('input#_pdf_watermark_disable, input#_pdf_watermark_override').change( function(){
		show_hide_pdf_product_fields();
	});

	function show_hide_pdf_product_fields() {
		var is_disabled  = $('input#_pdf_watermark_disable:checked').size();
		var is_override  = $('input#_pdf_watermark_override:checked').size();
		var pdf_type     = $('select#_pdf_watermark_type').val();

		// Hide/Show all with rules
		var hide_classes = '.hide_if_disabled';
		var show_classes = '.show_if_override, .show_if_text, .show_if_image';

		$( hide_classes ).show();
		$( show_classes ).hide();

		// Show rules
		if ( is_override ) {
			$('.show_if_override').show();
			$('.show_if_' + pdf_type).show();
		}

		// Hide rules
		if ( is_disabled ) {
			$('.hide_if_disabled').hide();
		}
	}

});