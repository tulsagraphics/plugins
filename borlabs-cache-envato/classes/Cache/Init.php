<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache;

use Borlabs\Factory;

class Init
{

    private static $instance;

    public $runtimeStart;
    public $runtimeEnd;

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    /* Init plugin */
    protected function __construct()
    {
    }

    public function initBackend()
    {
        // Init all actions and filters which are relevant for the backend
        Factory::get('Cache\Backend\Backend');
    }

    public function initFrontend()
    {
        $this->runtimeStart = microtime(1);

        // Init all actions and filters which are relevant for the frontend
        Factory::get('Cache\Frontend\Frontend');
    }

    public function initUpdateHooks()
    {
        /* Overwrite API URL when request infos about Borlabs Cache */
        add_action('plugins_api', [Factory::get('Cache\Update'), 'handlePluginAPI'], 9002, 3);

        /* Register Hook for checking for updates */
        add_filter('pre_set_site_transient_update_plugins', [Factory::get('Cache\Update'), 'handleTransientUpdatePlugins']);
    }

    public function pluginActivated()
    {
        Factory::get('Cache\Install')->installPlugin();

        // If cache system was active before, check if htaccess modification was true and add settings again
        if (Factory::get('Cache\Config')->get('browserCacheModifyHtaccess')) {
            Factory::get('Cache\Backend\AdvancedSettings')->modifyHtaccess(1);
        }

        if (Factory::get('Cache\Config')->get('browserSecurityContentSecurityPolicyHeader') && Factory::get('Cache\Config')->get('browserSecurityContentSecurityPolicy')) {
            Factory::get('Cache\Backend\AdvancedSettings')->modifyHtaccessSecurity(1);
        }

        // Remove activated message to display message again
        delete_option('BorlabsCacheActivatedMessage');
        delete_option('BorlabsCacheSystemChangedMessage');
    }

    public function pluginDeactivated()
    {
        // Remove htaccess settings
        Factory::get('Cache\Backend\AdvancedSettings')->modifyHtaccess(0);
        Factory::get('Cache\Backend\AdvancedSettings')->modifyHtaccessSecurity(0);
    }

    public function getTotalRuntime()
    {
        return Factory::get('Cache\Tools')->floatRound(microtime(1) - $this->runtimeStart, 8);
    }
}
?>
