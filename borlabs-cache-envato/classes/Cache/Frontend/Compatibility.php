<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache\Frontend;

use Borlabs\Factory;

class Compatibility {

    private static $instance;

    public static function getInstance () {

        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function __construct () {}

    /**
     * shouldCachingBeSkipped function.
     *
     * @access public
     * @return void
     */
    public function shouldCachingBeSkipped ()
    {
        $skipCaching = false;

        // AMP check when fragment caching is active
        if (Factory::get('Cache\Config')->get('fragmentCaching')) {
            // Check if url is an AMP url
            if (preg_match('/^(.*)\/amp(\/)?$/', Factory::get('Cache\Frontend\Resolver')->getURLInfo('path'))) {
                $skipCaching = true;
            }
        }

        return $skipCaching;
    }
}