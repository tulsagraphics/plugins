<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache\Frontend;

use Borlabs\Factory;

class CDNStackPath
{

    private static $instance;

    private $cdnURL;

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    protected function __construct()
    {
        $this->cdnURL = (Factory::get('Cache\Frontend\Resolver')->isHTTPS() ? 'https://' : 'http://').Factory::get('Cache\Config')->get('cdnURL');
    }

    /**
     * processHTML function.
     *
     * @access public
     * @param mixed &$sourceCode
     * @return void
     */
    public function processHTML(&$sourceCode)
    {
        Factory::get('Cache\Frontend\CDN')->modifySrc($sourceCode, [Factory::get('Cache\Frontend\CDNStackPath'), 'modifySrcCallback']);
        Factory::get('Cache\Frontend\CDN')->modifySrcset($sourceCode, [Factory::get('Cache\Frontend\CDNStackPath'), 'modifySrcsetCallback']);
        Factory::get('Cache\Frontend\CDN')->modifyLink($sourceCode, [Factory::get('Cache\Frontend\CDNStackPath'), 'modifyLinkCallback']);
    }

    /**
     * modifySrcCallback function.
     *
     * @access public
     * @param mixed $matches
     * @return void
     */
    public function modifySrcCallback($matches)
    {
        $tag = $matches[0];

        if (!empty($matches[4])) {
            $orgSrc = $matches[3];

            $tag = str_replace(
                $orgSrc,
                str_replace(
                    $matches[4],
                    $this->modifyURL($matches[4]),
                    $matches[3]
                ),
                $matches[0]
            );
        }

        return $tag;
    }

    /**
     * modifySrcsetCallback function.
     *
     * @access public
     * @param mixed $matches
     * @return void
     */
    public function modifySrcsetCallback($matches)
    {
        $tag = $matches[0];

        if (!empty($matches[3])) {
            $orgSrcset = $matches[2];

            // Split srcsets
            $sources = explode(',', $matches[3]);

            foreach ($sources as $key => $source) {
                $source = trim($source);
                $sources[$key] = $this->modifyURL(substr($source, 0, strpos($source, ' '))).substr($source, strpos($source, ' '));
            }

            $tag = str_replace(
                $orgSrcset,
                str_replace(
                    $matches[3],
                    implode(',', $sources),
                    $matches[2]
                ),
                $matches[0]
            );
        }

        return $tag;
    }

    /**
     * modifyLinkCallback function.
     *
     * @access public
     * @param mixed $matches
     * @return void
     */
    public function modifyLinkCallback($matches)
    {
        // no rel OR rel is stylesheet or icon -> replace href
        $tag = $matches[0];

        if (!empty($matches[3])) {
            $rel = strpos($matches[0], 'rel=');

            if ($rel === false || preg_match('/\<link([^>]+?)(rel=["|\']?(stylesheet|([^"\']*?)icon)["|\']?)([^>]*?)\>/', $matches[0])) {
                $orgHref = $matches[2];

                $tag = str_replace(
                    $orgHref,
                    str_replace(
                        $matches[3],
                        $this->modifyURL($matches[3]),
                        $matches[2]
                    ),
                    $matches[0]
                );
            }
        }

        return $tag;
    }

    /**
     * modifyURL function.
     *
     * @access public
     * @param mixed $orgURL
     * @return void
     */
    public function modifyURL($orgURL)
    {
        $urlInfo = parse_url($orgURL);

        if (!empty($urlInfo['host'])) {
            if ($urlInfo['host'] == Factory::get('Cache\Frontend\Resolver')->getRequestedDomain()) {
                return $this->cdnURL.'/'.(!empty($urlInfo['path']) ? ltrim($urlInfo['path'], '/') : '').(!empty($urlInfo['query']) ? '?'.$urlInfo['query'] : '');
            } else {
                return $orgURL;
            }
        } elseif (strpos($orgURL, 'data:') !== false) {
            return $orgURL;
        } else {
            return $this->cdnURL.'/'.ltrim($orgURL);
        }
    }
}
