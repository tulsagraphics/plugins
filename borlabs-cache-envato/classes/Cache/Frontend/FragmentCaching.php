<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache\Frontend;

use Borlabs\Factory;

class FragmentCaching {

    private static $instance;

    protected $fragmentCachingMaskPhrase;

    public static function getInstance () {

        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function __construct () {
        $this->fragmentCachingMaskPhrase = Factory::get('Cache\Config')->get('fragmentCachingMaskPhrase');
    }

    /**
     * parseContentForFragments function.
     *
     * @access public
     * @param mixed &$html
     * @return void
     */
    public function parseContentForFragments (&$html) {

        $html = str_replace([
                                '<!--[borlabs cache start: '.$this->fragmentCachingMaskPhrase.']-->',
                                '<!--[borlabs cache end: '.$this->fragmentCachingMaskPhrase.']-->',
                                '{'.$this->fragmentCachingMaskPhrase.'}',
                                '{/'.$this->fragmentCachingMaskPhrase.'}',
                            ], [
                                '<?php ',
                                ' ?>',
                                '<?php ',
                                ' ?>',
                            ], $html);
    }

    /**
     * parseContentAndExecuteFragments function.
     *
     * @access public
     * @param mixed &$html
     * @return void
     */
    public function parseContentAndExecuteFragments (&$html) {
        $html = preg_replace_callback('/\<\!--\[borlabs cache start: '.$this->fragmentCachingMaskPhrase.'\]\-\-\>(.*)\<\!--\[borlabs cache end: '.$this->fragmentCachingMaskPhrase.'\]--\>/Us', [$this, 'executeCode'], $html);
    }

    /**
     * executeCode function.
     *
     * @access public
     * @param mixed $code
     * @return void
     */
    public function executeCode ($code) {

        $parsedResult = null;

        if (!empty($code[1])) {

            try {

                $code[1] = str_replace(
                    [
                        '{'.$this->fragmentCachingMaskPhrase.'}',
                        '{/'.$this->fragmentCachingMaskPhrase.'}',
                    ],
                    [
                        '<?php',
                        '?>'
                    ],
                    $code[1]
                );

                ob_start();
                eval($code[1]);
                $parsedResult = ob_get_contents();
                ob_end_clean();

            } catch (\Exception $e) {

                error_log($e->getMessage());
            }
        }

        return $parsedResult;
    }
}
?>