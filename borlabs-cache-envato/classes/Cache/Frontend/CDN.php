<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache\Frontend;

use Borlabs\Factory;

class CDN {

    private static $instance;

    private $cdnProvider;

    public static function getInstance () {

        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __clone () {}

    private function __wakeup () {}

    protected function __construct () {
        $this->cdnProvider = Factory::get('Cache\Config')->get('cdnProvider');
    }

    /**
     * processHTML function.
     *
     * @access public
     * @param mixed &$sourceCode
     * @return void
     */
    public function processHTML (&$sourceCode) {
        Factory::get('Cache\Frontend\\'.$this->cdnProvider)->processHTML($sourceCode);
    }

    /**
     * modifySrc function. Modifies src attribute of various media-tags
     *
     * @access public
     * @param mixed &$sourceCode
     * @param mixed $callback
     * @return void
     */
    public function modifySrc (&$sourceCode, $callback) {
        $sourceCode = preg_replace_callback('/\<(audio|embed|img|input|script|source|track|video)([^>]+?)(src=["|\']?([^"\']+)["|\']?)([^>]*?)\>/', $callback, $sourceCode);
    }

    /**
     * modifySrcset function. Modifies srcset attribute in img-tags
     *
     * @access public
     * @param mixed &$sourceCode
     * @param mixed $callback
     * @return void
     */
    public function modifySrcset (&$sourceCode, $callback) {
        $sourceCode = preg_replace_callback('/\<img([^>]+?)(srcset=["|\']?([^"\']+)["|\']?)([^>]*?)\>/', $callback, $sourceCode);
    }

    /**
     * modifyLink function. Modifies link-tags if they link to a stylesheet
     *
     * @access public
     * @param mixed &$sourceCode
     * @param mixed $callback
     * @return void
     */
    public function modifyLink (&$sourceCode, $callback) {
        // no rel OR rel is stylesheet or icon -> replace href
        $sourceCode = preg_replace_callback('/\<link([^>]+?)(href=["|\']?([^"\']+)["|\']?)([^>]*?)\>/', $callback, $sourceCode);
    }
}
?>