<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache\Frontend;

use Borlabs\Factory;

class Scripts
{
    private static $instance;

    private $homePath                   = null;
    private $blogHosts                  = [];
    private $alternativeBlogHost        = null;
    private $blogPath                   = null;
    private $alternativeblogPath        = null;

    private $deferActive                = false;
    private $detectedScripts            = [];
    private $detectedExternalScripts    = [];
    private $sourceCodeScripts          = [];

    private $htmlScript                     = '';
    private $registeredLocalizeScriptData   = [];

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function __construct()
    {
        global $wp_scripts;

        // Support for multisites, get_home_path() is not available for some reasons
        $this->homePath = ABSPATH;

        $urlInfo = parse_url($wp_scripts->base_url);

        Factory::get('Cache\Log')->addLog(__METHOD__, 'Base URL: '.$wp_scripts->base_url);

        $this->blogHosts[] = $urlInfo['host'];
        $this->blogPath = !empty($urlInfo['path']) ? $urlInfo['path'] : '';
        $this->deferActive = Factory::get('Cache\Config')->get('scriptsDefer');

        $alternativeUrlInfo = parse_url(WP_CONTENT_URL);

        if ($alternativeUrlInfo['host'] != $urlInfo['host']) {
            $this->alternativeBlogHost = $alternativeUrlInfo['host'];
            $this->blogHosts[] = $alternativeUrlInfo['host'];
            $this->alternativeblogPath = !empty($alternativeUrlInfo['path']) ? $alternativeUrlInfo['path'] : '';

            // DNS Prefetch
            Factory::get('Cache\Frontend\Prefetch')->addHost($alternativeUrlInfo['host']);
        }
    }

    /**
     * optimize function.
     *
     * @access public
     * @param mixed &$html
     * @return void
     */
    public function optimize(&$html)
    {
        // Find scripts first, because they can contain <style> tags
        $html = preg_replace_callback('/<script.*<\/script>/Us', [$this, 'collectScripts'], $html);

        // Process scripts
        $this->processDetectedScripts();

        // Create JavaScript file and set htmlStyle with the <script>-tag
        $this->createScriptFile();
    }

    /**
     * collectScripts function.
     *
     * @access public
     * @param mixed $tag
     * @return void
     */
    public function collectScripts($tag)
    {
        // Detect if script is of type javascript
        $scriptType = [];
        preg_match('/\<script([^\>]*)type=("|\')([^"\']*)("|\')/Us', $tag[0], $scriptType);

        // Only <script>-tags without type attribute or with type attribute text/javascript are JavaScript
        if (empty($scriptType) || !empty($scriptType) && strtolower($scriptType[3]) == 'text/javascript') {
            // Exclude external scripts
            $scriptLocation = [];

            if (preg_match('/<script(.*?)src=("|\')([^"\']*)("|\')/', $tag[0], $scriptLocation)) {
                $urlInfo = parse_url($scriptLocation[3]);

                // Only internal scripts will be merged
                if ((!empty($urlInfo['host']) && in_array($urlInfo['host'], $this->blogHosts)) || empty($urlInfo['host'])) {
                    $this->detectedScripts[] = $scriptLocation[3];

                    $tag[0] = '';
                } else {
                    if ($this->deferActive) {
                        // Check if defer-attribute is not present yet
                        $scriptDefer = [];
                        preg_match('/<((script)[^>]*)>(.*)\<\/(script)>/Us', $tag[0], $scriptDefer);

                        if (strpos($scriptDefer[1], ' defer') === false) {
                            $this->detectedExternalScripts[] = '<'.$scriptDefer[1].' defer></script>';
                        } else {
                            $this->detectedExternalScripts[] = $tag[0];
                        }
                    } else {
                        $this->detectedExternalScripts[] = $tag[0];
                    }

                    $tag[0] = '';

                    // DNS Prefetch
                    Factory::get('Cache\Frontend\Prefetch')->addHost($urlInfo['host']);
                }
            } else {
                $this->detectedScripts[] = $tag[0];

                $tag[0] = '';
            }
        }

        return $tag[0];
    }

    /**
     * processDetectedScripts function.
     *
     * @access public
     * @return void
     */
    public function processDetectedScripts()
    {
        if (!empty($this->detectedScripts)) {
            foreach ($this->detectedScripts as $script) {
                // Inline scripts
                if (strpos($script, '<script') !== false) {
                    $this->sourceCodeScripts[] = $this->fixMissingSemicolon(trim(preg_replace('/<script([^>]*?)>(.*)<\/script>/s', '$2', $script)));
                } else {
                    // External script
                    $this->sourceCodeScripts[] = $this->fixMissingSemicolon($this->loadScriptSourceCode($script));
                }
            }
        }
    }

    /**
     * loadScriptSourceCode function.
     *
     * @access public
     * @param mixed $url
     * @return void
     */
    public function loadScriptSourceCode($url)
    {
        Factory::get('Cache\Log')->addLog(__METHOD__, 'Requested URL: '.$url);

        $source = '';

        $originalHomePath = $this->homePath;
        $originalBlogPath = $this->blogPath;

        $urlInfo = parse_url($url);
        $urlInfo['path'] = urldecode($urlInfo['path']);

        // If protocol is missing detect current protocol
        if (empty($urlInfo['scheme']) && substr($url, 0, 2) == '//') {
            $isHttps = Factory::get('Cache\Tools')->isHttps();

            $url = ($isHttps ? 'https' : 'http').':'.$url;
        }

        // Detect which host is used
        if (strpos($url, $this->alternativeBlogHost) !== false) {
            $this->homePath = WP_CONTENT_DIR.'/';
            $this->blogPath = $this->alternativeblogPath;
        }

        if (!empty($this->blogPath)) {
            $urlInfo['path'] = strpos($urlInfo['path'], $this->blogPath) === 0 ? substr($urlInfo['path'], strlen($this->blogPath)) : $urlInfo['path'];
        }

        Factory::get('Cache\Log')->addLog(__METHOD__, 'JS path corrected: '.$urlInfo['path']);

        $pathInfo = pathinfo($urlInfo['path']);

        if (!empty($pathInfo['extension']) && $pathInfo['extension'] == 'js') {
            // Try to find the file on the filesystem
            if (!empty($urlInfo['path']) && $urlInfo['path'] !== '/') {
                // We don't check if the file exists, because this costs too much time.
                // In most cases the file will exists, but when it fails, we have a fallback

                $localPath = $this->homePath.ltrim($urlInfo['path'], '/');

                Factory::get('Cache\Log')->addLog(__METHOD__, 'Local path for given URL: '.$localPath);

                $source = file_get_contents($localPath);
            } else {
                Factory::get('Cache\Log')->addLog(__METHOD__, 'Looks like a dynamic script');
            }
        }

        if (empty($source)) {
            $url = html_entity_decode(trim($url));

            Factory::get('Cache\Log')->addLog(__METHOD__, 'Load script by using the URL: '.$url);

            // We need to define a user_agent or file_get_contents will replace & into &amp;
            // Yes - http is correct even when it's a https connection...
            $options = [
                'http' => [
                    'method' => 'GET',
                    'user_agent' => (!empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'Borlabs-Cache/'.BORLABS_CACHE_VERSION),
                ]
            ];

            $context = stream_context_create($options);

            $source = file_get_contents($url, false, $context);
        }

        $this->homePath = $originalHomePath;
        $this->blogPath = $originalBlogPath;

        return $source;
    }

    /**
     * handleDeferOnInlineScript function.
     *
     * @access public
     * @param mixed &$html
     * @return void
     */
    public function handleDeferOnInlineScript(&$html)
    {
        $html = preg_replace_callback('/<((script)[^>]*)>(.*)\<\/(script)>/Us', [$this, 'makeInlineScriptDeferable'], $html);
    }

    /**
     * makeInlineScriptDeferable function.
     *
     * @access public
     * @param mixed $scriptTag
     * @return void
     */
    public function makeInlineScriptDeferable($scriptTag)
    {
        // Check if type attribute is present and of text/javascript
        $typeMatch = [];
        preg_match('/type=("|\')([^"\']{1,})("|\')/', $scriptTag[0], $typeMatch);

        if (!empty($typeMatch[2]) && $typeMatch[2] == 'text/javascript' || empty($typeMatch)) {
            if (strpos($scriptTag[0], ' defer') === false && strpos($scriptTag[0], 'src') === false) {
                $scriptTag = '<'.$scriptTag[1].' defer>window.addEventListener(\'DOMContentLoaded\', function() { '."\n".$scriptTag[3]."\n".' });</script>';
            } else {
                $scriptTag = $scriptTag[0];
            }
        } else {
            $scriptTag = $scriptTag[0];
        }

        return $scriptTag;
    }

    /**
     * createScriptFile function.
     *
     * @access public
     * @return void
     */
    public function createScriptFile()
    {
        global $wp_scripts;

        $source = implode("\n", $this->sourceCodeScripts);

        if (Factory::get('Cache\Config')->get('scriptsMinify')) {
            if (Factory::get('Cache\Debug')->isDebugEnabled() == false) {
                Factory::get('Cache\Frontend\Minify')->minifyJS($source);
            }
        }

        $extension = 'js';

        if (Factory::get('Cache\Config')->get('scriptsGZIPOutput')) {
            $extension = 'php';

            $source = $this->addGzipCode($source);
        }

        $suffix = Factory::get('Cache\Frontend\Resolver')->isHTTPS() ? '_https' : '';

        $filename = Factory::get('Cache\Tools')->getHash($this->sourceCodeScripts).$suffix.'.'.$extension;

        Factory::get('Cache\Frontend\Cache')->createCachefile($filename, $source, 'js');

        $deferAttribute = '';

        if ($this->deferActive) {
            $deferAttribute = ' defer';
        }

        $this->htmlScript = '<script type="text/javascript" src="'.content_url().'/cache/borlabs_cache/'.Factory::get('Cache\Frontend\Resolver')->getCurrentBlogId().'/js/'.$filename.'"'.$deferAttribute.'></script>';

        if (!empty($this->detectedExternalScripts)) {
            if (Factory::get('Cache\Config')->get('scriptsExternalScriptsPosition') == 'before') {
                $this->htmlScript = implode('', $this->detectedExternalScripts).$this->htmlScript;
            } else {
                $this->htmlScript = $this->htmlScript.implode('', $this->detectedExternalScripts);
            }
        }

        // Free resources
        unset($this->detectedScripts);
        unset($this->detectedExternalScripts);
        unset($this->sourceCodeScripts);
    }

    /**
     * fixMissingSemicolon function.
     *
     * @access public
     * @param mixed $code
     * @return void
     */
    public function fixMissingSemicolon($source)
    {
        if (Factory::get('Cache\Config')->get('scriptsFixSemicolon')) {
            $source = rtrim($source);

            // Works for () and (jQuery)
            if (substr($source, -1) === ')') {
                $source .= ';';
            }
        }

        return $source;
    }

    /**
     * addGzipCode function.
     *
     * @access public
     * @param mixed $jsCode
     * @return void
     */
    public function addGzipCode($jsCode)
    {
        $topPHPCode = "<?php\n";
        $topPHPCode .= "include_once '".Factory::get('Cache\Frontend\Header')->classLocation()."'; \n";
        $topPHPCode .= Factory::get('Cache\Frontend\Header')->getContentTypeHeaderCode('application/javascript', true)."\n";
        $topPHPCode .= "\Borlabs\Cache\Frontend\Header::getInstance()->setConfig(unserialize(base64_decode('".base64_encode(serialize(Factory::get('Cache\Frontend\Header')->getHeaderRelatedConfigs()))."')));\n";
        $topPHPCode .= '\Borlabs\Cache\Frontend\Header::getInstance()->getHeader(__FILE__);'."\n";
        $topPHPCode .= "ini_set('zlib.output_compression_level', ".Factory::get('Cache\Config')->get('scriptsGzipCompressionLevel').");\n";
        $topPHPCode .= "if (ini_get('zlib.output_compression')) {\n";
        $topPHPCode .=  "ob_start();\n";
        $topPHPCode .= "} else {\n";
        $topPHPCode .=  "ob_start('ob_gzhandler');\n";
        $topPHPCode .= "}\n";
        $topPHPCode .= "?>\n";

        $bottomPHPCode = "\n<?php ob_end_flush(); ?>";

        return $topPHPCode.$jsCode.$bottomPHPCode;
    }

    /**
     * getHTMLScript function.
     *
     * @access public
     * @return void
     */
    public function getHTMLScript()
    {
        return $this->htmlScript;
    }

    /**
     * registerLocalizeScriptData function.
     *
     * @access public
     * @param mixed $handle
     * @param mixed $name
     * @param mixed $data
     * @return void
     */
    public function registerLocalizeScriptData($handle, $name, $data)
    {
        if (!empty($handle) && !empty($name) && !empty($data) && is_string($name)) {
            $this->registeredLocalizeScriptData[$handle] = [
                'name'=>$name,
                'data'=>$data,
            ];
        }
    }

    /**
     * getLocalizedScriptData function.
     *
     * @access public
     * @return void
     */
    public function getLocalizedScriptData()
    {
        return $this->registeredLocalizeScriptData;
    }

    /**
     * getLocalizedScriptDataCode function.
     *
     * @access public
     * @return void
     */
    public function getLocalizedScriptDataCode()
    {
        $phpCode = '';

        if (!empty($this->registeredLocalizeScriptData)) {
            $phpCode .= '<?php $borlabsLocalizedScriptData = json_decode("'.addslashes(json_encode($this->registeredLocalizeScriptData)).'", true); echo \Borlabs\Cache\Frontend\Scripts::processLocalizedScriptData($borlabsLocalizedScriptData); ?>';
        }

        return $phpCode;
    }

    /**
     * processLocalizedScriptData function.
     *
     * @access public
     * @static
     * @param mixed $scriptData
     * @return void
     */
    public static function processLocalizedScriptData($scriptData)
    {
        $localizedScript = '';

        if (!empty($scriptData) && is_array($scriptData)) {

            $localizedScript .= "<script type=\"text/javascript\">\n/* <![CDATA[ */\n";

            // Anonymous function for detecting nonces
            $detectNonceFunction = function (&$value, $key) {

                if (is_string($value) && strpos($value, 'wp_create_nonce__') !== false) {
                    $value = str_replace('wp_create_nonce__', '', $value);
                    $value = wp_create_nonce($value);
                }
            };

            foreach ($scriptData as $handle => $data) {

                array_walk_recursive($data['data'], $detectNonceFunction);

                $localizedScript .= "var ".$data['name']." = ".json_encode($data['data']).";\n";
            }

            $localizedScript .= "/* ]]> */</script>";
        }

        return $localizedScript;
    }
}
