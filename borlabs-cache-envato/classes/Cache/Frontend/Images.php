<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache\Frontend;

use Borlabs\Factory;

class Images
{
    private static $instance;

    private $config;

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public function __construct()
    {
    }

    public function registerScript()
    {
        if (Factory::get('Cache\Config')->get('imagesLazyLoad')) {
            wp_enqueue_script('borlabs-lazy-load', BORLABS_CACHE_PLUGIN_URL.'vendor/jquery-lazyload/lazyload.min.js', ['jquery'], null, true);
        }
    }

    public function optimize(&$html)
    {
        // If JS merging is disabled we have to mask all script tags
        if (Factory::get('Cache\Config')->get('scriptsMerge') == false) {
            $html = preg_replace_callback('/<script.*<\/script>/Us', [Factory::get('Cache\Frontend\Optimizer'), 'maskTags'], $html);
        }

        $html = preg_replace_callback('/<img.*>/Us', [$this, 'modifyImageTag'], $html);

        // Parse script tags back
        if (Factory::get('Cache\Config')->get('scriptsMerge') == false) {
            Factory::get('Cache\Frontend\Optimizer')->reInsertPreservedTags($html, true);
        }
    }

    public function modifyImageTag($tag)
    {
        // Check if class attribute is present
        if (!preg_match('/class=("|\')/', $tag[0])) {
            // Add class attribute
            $tag[0] = str_replace('<img', '<img class="lazyload"', $tag[0]);
        } else {
            // Modify class attribute
            $tag[0] = preg_replace('/class=("|\')([^"\']*)("|\')/', 'class=$1$2 lazyload$3', $tag[0]);
        }

        // Replace image src
        $tag[0] = preg_replace('/src=("|\')([^"\']*)("|\')/', 'src=$1data:image/gif;base64,R0lGODdhAQABAIAAANk7awAAACH5BAEAAAEALAAAAAABAAEAAAICTAEAOw==$3 data-src=$1$2$3', $tag[0]);

        // Modify srcset
        $tag[0] = preg_replace('/srcset=("|\')([^"\']*)("|\')/', 'data-srcset=$1$2$3', $tag[0]);

        return $tag[0];
    }
}
