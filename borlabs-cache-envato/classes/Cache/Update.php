<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache;

use Borlabs\Factory;

class Update
{

    private static $instance;

    private $currentBlogId = '';

    public static function getInstance ()
    {

        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public function __construct()
    {
    }

    /**
     * handlePluginAPI function.
     *
     * @access public
     * @param mixed $result Default is false
     * @param string $action Type of information
     * @param object $args Plugin API arguments
     * @return void
     */
    public function handlePluginAPI($result, $action, $args)
    {
        if (!empty($action) && $action == 'plugin_information' && !empty($args->slug)) {
            if ($args->slug == dirname(BORLABS_CACHE_SLUG)) {
                // Return alternative API URL
                $result = Factory::get('Cache\API')->getPluginInformation();
            }
        }

        return $result;
    }

    /**
     * handleTransientUpdatePlugins function.
     *
     * @access public
     * @param mixed $transient
     * @return void
     */
    public function handleTransientUpdatePlugins($transient)
    {
        // If info is already available
        if (isset($transient->response[BORLABS_CACHE_SLUG])) {
            return $transient;
        }

        // Check for updates
        $updateInformation = Factory::get('Cache\API')->getLatestVersion();

        if (!empty($updateInformation)) {
            if (version_compare(BORLABS_CACHE_VERSION, $updateInformation->new_version, '<')) {
                $transient->response[BORLABS_CACHE_SLUG] = $updateInformation;
            }
        }
        return $transient;
    }

    /**
     * upgradeComplete function.
     *
     * @access public
     * @param mixed $upgraderObject
     * @param mixed $options
     * @return void
     */
    public function upgradeComplete($upgraderObject, $options)
    {
        if ($options['action'] == 'update' && $options['type'] == 'plugin' && !empty($options['plugins'])) {
            // Check if Borlabs Cache was updated
            if (in_array(BORLABS_CACHE_SLUG, $options['plugins'])) {
                $this->processUpgrade();
            }
        }
    }

    /**
     * processUpgrade function.
     *
     * @access public
     * @return void
     */
    public function processUpgrade()
    {
        global $wpdb;

        $lastVersion = get_option('BorlabsCacheVersion', false);

        if (is_multisite()) {
            $allBlogs = $wpdb->get_results('
                SELECT
                    `blog_id`
                FROM
                    `'.$wpdb->base_prefix.'blogs`
            ');
        }

        $versionUpgrades = Factory::get('Cache\Upgrade')->getVersionUpgrades();

        if (!empty($lastVersion)) {
            foreach ($versionUpgrades as $upgradeFunction => $version) {
                if (version_compare($lastVersion, $version, '<')) {
                    if (method_exists(Factory::get('Cache\Upgrade'), $upgradeFunction)) {
                        // Call upgrade function
                        call_user_func([Factory::get('Cache\Upgrade'), $upgradeFunction]);

                        // Upgrade multisites
                        if (is_multisite() && !empty($allBlogs)) {
                            $originalBlogId = get_current_blog_id();

                            foreach ($allBlogs as $blogData) {
                                if ($blogData->blog_id != 1) {
                                    switch_to_blog($blogData->blog_id);

                                    $this->currentBlogId = $blogData->blog_id;

                                    call_user_func([Factory::get('Cache\Upgrade'), $upgradeFunction]);

                                    switch_to_blog($originalBlogId);
                                }
                            }

                            // Just in case we use this value at some later point
                            $this->currentBlogId = $originalBlogId;
                        }
                    }
                }
            }
        }
    }
}
