<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache\Backend;

use Borlabs\Factory;

class Support {

    private static $instance;

    private $imagePath;

    public static function getInstance () {

        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __clone () {}

    private function __wakeup () {}

    protected function __construct () {}

    public function display() {

        $this->imagePath = plugins_url('images', realpath(__DIR__.'/../../'));

        if (!empty($_POST['formSend']) && check_admin_referer('borlabs_cache_support')) {

            $error = false;

            if (empty($_POST['supportTopic']) || empty($_POST['supportName']) || empty($_POST['supportMail']) || empty($_POST['supportMessage'])) {
                $error = true;
            }

            if ($error) {

                Factory::get('Cache\Backend\Backend')->addMessage(_x('Please fill out every field.', 'Status message', 'borlabs-cache'), 'error');

            } else {

                // Get user's selected language
                $user = wp_get_current_user();
                $locale = get_user_meta($user->ID, 'locale', true);

                if (empty($locale)) {
                    $locale = get_locale();
                }

                $additionalData = [
                    'licenseKey'=>get_option('BorlabsCacheLicenseKey'),
                    'locale'=>$locale,
                    'borlabsCacheVersion'=>BORLABS_CACHE_VERSION,
                    'wordPressVersion'=>get_bloginfo('version'),
                    'allowUrlFopen'=>ini_get('allow_url_fopen'),
                    'memoryLimit'=>ini_get('memory_limit'),
                ];

                $responseSupport = Factory::get('Cache\API')->sendSupportRequest(
                    $_POST['supportMail'],
                    $_POST['supportName'],
                    $_POST['supportTopic'],
                    $_POST['supportMessage'],
                    $additionalData
                );

                if (!empty($responseSupport->successMessage)) {
                    Factory::get('Cache\Backend\Backend')->addMessage($responseSupport->successMessage, 'success');

                    unset($_POST['supportName']);
                    unset($_POST['supportMail']);
                    unset($_POST['supportMessage']);
                } else {
                    Factory::get('Cache\Backend\Backend')->addMessage(_x("We're sorry, but the support ticket couldn't be created. Please try again later or visit <a href='https://borlabs.io/' target='_blank'>borlabs.io</a>.", 'Status message', 'borlabs-cache'), 'error');
                }
            }
        }

        $user = wp_get_current_user();

        $optionSupportTopicGeneralQuestion  = !empty($_POST['supportTopic']) && $_POST['supportTopic'] == 'general-question' ? ' selected' : '';
        $optionSupportTopicFeatureRequest   = !empty($_POST['supportTopic']) && $_POST['supportTopic'] == 'feature-request' ? ' selected' : '';
        $optionSupportTopicTechnicalIssue   = !empty($_POST['supportTopic']) && $_POST['supportTopic'] == 'technical-issue' ? ' selected' : '';

        $inputSupportName = esc_html(!empty($_POST['supportName']) ? stripslashes($_POST['supportName']) : $user->data->display_name);
        $inputSupportMail = esc_html(!empty($_POST['supportMail']) ? stripslashes($_POST['supportMail']) : $user->data->user_email);

        $textareaSupportMessage = esc_textarea(!empty($_POST['supportMessage']) ? stripslashes($_POST['supportMessage']) : '');

        include Factory::get('Cache\Backend\Backend')->templatePath.'/support.html';
    }
}
?>