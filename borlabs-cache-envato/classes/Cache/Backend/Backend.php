<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache\Backend;

use Borlabs\Factory;

class Backend
{
    private static $instance;

    public $templatePath;

    private $messages = [];

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public function __construct()
    {
        /* Load textdomain */
        add_action('plugins_loaded', [$this, 'loadTextdomain']);

        /* Show message if plugin was activated/deactivated or theme was switched */
        add_action('activated_plugin', [$this, 'handleSystemChanged']);
        add_action('deactivated_plugin', [$this, 'handleSystemChanged']);
        add_action('switch_theme', [$this, 'handleSystemChanged']);

        /* Show activated successfully message and inform user to activate caching */
        add_action('admin_notices', [$this, 'successfullyActivatedMessage']);

        /* Show message when a plugin or theme was changed */
        add_action('admin_notices', [$this, 'systemChangedMessage']);

        /* Register handler for AJAX requests like dismissed notices */
        add_action('wp_ajax_borlabs_cache_handler', [$this, 'handleAjaxRequest']);

        /* Add menu */
        add_action('admin_menu', [$this, 'addMenu']);

        /* Add toolbar menu */
        add_action('wp_before_admin_bar_render', [$this, 'addToolbarMenu']);

        /* Load JavaScript & CSS */
        add_action('admin_enqueue_scripts', [$this, 'registerAdminScripts']);

        /* Meta Box */
        if (Factory::get('Cache\Config')->get('cacheShowMetaBox')) {
            add_action('add_meta_boxes', [Factory::get('Cache\Backend\MetaBox'), 'registerMetaBox']);
            add_action('save_post', [Factory::get('Cache\Backend\MetaBox'), 'saveMetaBoxData'], 10, 3);
        }

        $this->templatePath = realpath(dirname(__FILE__).'/../../../templates');

        if (!file_exists($this->templatePath.'/.htaccess')) {
            file_put_contents($this->templatePath.'/.htaccess', "<Files *.html>\nOrder Deny,Allow\nDeny from all\n</Files>");
        }

        // Check file permissions
        $htaccessPath = ABSPATH.'.htaccess';

        if (defined('BORLABS_CACHE_HTACCESS_PATH')) {
            $htaccessPath = BORLABS_CACHE_HTACCESS_PATH.'.htaccess';
        }

        if (!is_writable($htaccessPath) && Factory::get('Cache\Config')->get('miscellaneousNginx') == false) {
            $this->addMessage(_x('The file <strong>.htaccess</strong> is not writable. Please set the right permissions. See <a href="https://borlabs.io/folder-permissions/" rel="noreferrer" target="_blank">FAQ</a>.', 'Status message', 'borlabs-cache'), 'error');
        }

        // Check if cache folder exists
        if (!file_exists(WP_CONTENT_DIR.'/cache/')) {
            if (!is_writable(WP_CONTENT_DIR)) {
                $this->addMessage(sprintf(_x('The folder <strong>/%s/</strong> is not writable. Please set the right permissions. See <a href="https://borlabs.io/folder-permissions/" rel="noreferrer" target="_blank">FAQ</a>.', 'Status message', 'borlabs-cache'), basename(WP_CONTENT_DIR)), 'error');
            } else {
                mkdir(WP_CONTENT_DIR.'/cache/');
            }
        }

        if (file_exists(WP_CONTENT_DIR.'/cache/') && !is_writable(WP_CONTENT_DIR.'/cache/')) {
            $this->addMessage(sprintf(_x('The folder <strong>/%s/cache/</strong> is not writable. Please set the right permissions. See <a href="https://borlabs.io/folder-permissions/" rel="noreferrer" target="_blank">FAQ</a>.', 'Status message', 'borlabs-cache'), basename(WP_CONTENT_DIR)), 'error');
        }

        // Chech if .htaccess is placed in cache folder
        if (file_exists(WP_CONTENT_DIR.'/cache/.htaccess')) {
            $this->addMessage(sprintf(_x('The folder <strong>/%s/cache/</strong> contains a <strong>.htaccess</strong> file which can prevent that visitors can load your websites CSS and JavaScript. See <a href="https://borlabs.io/blocking-htaccess/" rel="noreferrer" target="_blank">FAQ</a>.', 'Status message', 'borlabs-cache'), basename(WP_CONTENT_DIR)), 'error');
        }

        // Chech if memory_limit is sufficient
        $memoryLimit = ini_get('memory_limit');

        if ($memoryLimit !== '-1') {
            $memoryLimitParts = [];
            preg_match('/^(\d+)(.)$/', $memoryLimit, $memoryLimitParts);

            if ($memoryLimitParts[2] == 'K') {
                $memoryLimitParts[1] = $memoryLimitParts[1] / 1024;
            }

            if ($memoryLimitParts[1] < 128) {
                $this->addMessage(sprintf(_x('Your PHP <strong>memory_limit</strong> is too low. Please increase your current limit of %s to at least 128M. If you have a lot of plugins installed, you should increase your limit to 256M or more.', 'Status message', 'borlabs-cache'), $memoryLimitParts[1].$memoryLimitParts[2]), 'error');
            }
        }

        if (ini_get('allow_url_fopen') != true) {
            $this->addMessage(_x('Your PHP setting <strong>allow_url_fopen</strong> is <strong>Off</strong>. Please switch it <strong>On</strong> otherwise, Borlabs Cache can not merge dynamic CSS.', 'Status message', 'borlabs-cache'), 'error');
        }
    }

    /**
     * handleAjaxRequest function.
     *
     * @access public
     * @return void
     */
    public function handleAjaxRequest()
    {
        if (!empty($_POST['type'])) {

            $requestType = $_POST['type'];

            if (check_ajax_referer('borlabs-cache-dismissed-notice', false, false)) {

                if ($requestType == 'borlabsCacheSystemChangedMessage') {
                    update_option('BorlabsCacheSystemChangedMessage', false, 'yes');
                } elseif ($requestType == 'borlabsCacheSystemActivatedMessage') {
                    update_option('BorlabsCacheActivatedMessage', true, 'yes');
                }

            } elseif (check_ajax_referer('borlabs-cache-maintenance-refresh-cache', false, false)) {

                if ($requestType == 'borlabsCacheRefreshCache') {
                    Factory::get('Cache\Frontend\Garbage')->clearCache();

                    echo json_encode([
                        'message'=>Factory::get('Cache\Backend\Dashboard')->getCacheMaintenanceMessage('refresh'),
                    ]);
                }

            } elseif (check_ajax_referer('borlabs-cache-maintenance-clear-styles-pre-cache', false, false)) {

                if ($requestType == 'borlabsCacheClearStylesPreCache') {
                    Factory::get('Cache\Frontend\Garbage')->clearStylesPreCacheFiles();

                    echo json_encode([
                        'message'=>Factory::get('Cache\Backend\Dashboard')->getCacheMaintenanceMessage('clearStylesPreCache'),
                    ]);
                }

            } elseif (check_ajax_referer('borlabs-cache-indexing', false, false)) {
                if ($requestType == 'borlabsCacheIndexing') {
                    echo json_encode(Factory::get('Cache\Backend\CacheIndexing')->handleIndexingProcess($_POST));
                }
            }
        }

        wp_die();
    }

    /**
     * loadTextdomain function.
     *
     * @access public
     * @return void
     */
    public function loadTextdomain()
    {
        load_plugin_textdomain('borlabs-cache', false, dirname(BORLABS_CACHE_SLUG).'/languages/');
    }

    /**
     * addMenu function.
     *
     * @access public
     * @return void
     */
    public function addMenu()
    {
        /* Main menu */
        add_menu_page(
            _x('Borlabs Cache', 'Site title', 'borlabs-cache'),
            _x('Borlabs Cache', 'Menu entry', 'borlabs-cache'),
            'administrator', /* lowest administrator level */
            'borlabs-cache',
            [Factory::get('Cache\Backend\View'), 'display__Dashboard'],
            Factory::get('Cache\Backend\Icons')->getAdminSVGIcon(),
            null /* menu position */
        );

        /* Dashboard */
        add_submenu_page(
            'borlabs-cache',
            _x('Dashboard Settings', 'Site title', 'borlabs-cache'),
            _x('Dashboard', 'Menu entry', 'borlabs-cache'),
            'administrator',
            'borlabs-cache',
            [Factory::get('Cache\Backend\View'), 'display__Dashboard']
        );

        /* Advanced Settings */
        add_submenu_page(
            'borlabs-cache',
            _x('Advanced Caching Settings', 'Site title', 'borlabs-cache'),
            _x('Advanced Settings', 'Menu entry', 'borlabs-cache'),
            'administrator',
            'borlabs-cache-advanced-settings',
            [Factory::get('Cache\Backend\View'), 'display__AdvancedSettings']
        );

        /* CDN */
        add_submenu_page(
            'borlabs-cache',
            _x('CDN', 'Site title', 'borlabs-cache'),
            _x('CDN', 'Menu entry', 'borlabs-cache'),
            'administrator',
            'borlabs-cache-cdn',
            [Factory::get('Cache\Backend\View'), 'display__CDNSettings']
        );

        /* Cache Indexing */
        add_submenu_page(
            'borlabs-cache',
            _x('Cache Indexing', 'Site title', 'borlabs-cache'),
            _x('Cache Indexing', 'Menu entry', 'borlabs-cache'),
            'administrator',
            'borlabs-cache-indexing',
            [Factory::get('Cache\Backend\View'), 'display__CacheIndexing']
        );

        /* View Cache */
        add_submenu_page(
            'borlabs-cache',
            _x('View Cache', 'Site title', 'borlabs-cache'),
            _x('View Cache', 'Menu entry', 'borlabs-cache'),
            'administrator',
            'borlabs-cache-view-cache',
            [Factory::get('Cache\Backend\View'), 'display__ViewCache']
        );

        /* Optimize Database */
        add_submenu_page(
            'borlabs-cache',
            _x('Optimize Database', 'Site title', 'borlabs-cache'),
            _x('Optimize Database', 'Menu entry', 'borlabs-cache'),
            'administrator',
            'borlabs-cache-optimize-database',
            [Factory::get('Cache\Backend\View'), 'display__OptimizeDatabase']
        );

        /* Fragments */
        add_submenu_page(
            'borlabs-cache',
            _x('Fragments', 'Site title', 'borlabs-cache'),
            _x('Fragments', 'Menu entry', 'borlabs-cache'),
            'administrator',
            'borlabs-cache-fragments',
            [Factory::get('Cache\Backend\View'), 'display__Fragments']
        );

        /* Import Export */
        add_submenu_page(
            'borlabs-cache',
            _x('Import / Export Settings', 'Site title', 'borlabs-cache'),
            _x('Import &amp; Export', 'Menu entry', 'borlabs-cache'),
            'administrator',
            'borlabs-cache-import-export',
            [Factory::get('Cache\Backend\View'), 'display__ImportExport']
        );

        /* Support */
        add_submenu_page(
            'borlabs-cache',
            _x('Support', 'Site title', 'borlabs-cache'),
            _x('Support', 'Menu entry', 'borlabs-cache'),
            'administrator',
            'borlabs-cache-support',
            [Factory::get('Cache\Backend\View'), 'display__Support']
        );

        /* License */
        add_submenu_page(
            'borlabs-cache',
            _x('License', 'Site title', 'borlabs-cache'),
            _x('License', 'Menu entry', 'borlabs-cache'),
            'administrator',
            'borlabs-cache-license',
            [Factory::get('Cache\Backend\View'), 'display__License']
        );

        /* About */
        add_submenu_page(
            'borlabs-cache',
            _x('About Borlabs Cache', 'Site title', 'borlabs-cache'),
            _x('About', 'Menu entry', 'borlabs-cache'),
            'administrator',
            'borlabs-cache-about',
            [Factory::get('Cache\Backend\View'), 'display__About']
        );
    }

    /**
     * addToolbarMenu function.
     *
     * @access public
     * @return void
     */
    public function addToolbarMenu()
    {
        global $wp_admin_bar;

        if (Factory::get('Cache\Config')->get('miscellaneousDisableBorlabsToolbarMenuItem') == false) {
            $user = wp_get_current_user();

            if (!empty($user->allcaps['publish_posts'])) {

                /* Main Toolbar Menu */
                $wp_admin_bar->add_node([
                    'id'=>'borlabs-cache',
                    'title'=>_x('Borlabs Cache', 'Menu entry', 'borlabs-cache'),
                ]);

                /* Refresh Cache Menu Item */
                $message = _x('Do you really want to refresh the complete cache?', 'Message', 'borlabs-cache');
                $nonce = wp_create_nonce('borlabs-cache-maintenance-refresh-cache');
                $type = 'borlabsCacheRefreshCache';

                $wp_admin_bar->add_node([
                    'id'=>'borlabs-cache-refresh',
                    'title'=>_x('Refresh Cache', 'Menu entry', 'borlabs-cache'),
                    'parent'=>'borlabs-cache',
                    'href'=>'#',
                    'meta'=>[
                        'class'=>'borlabs-cache-toolbar-cache-maintenance',
                        'html'=>'<span data-borlabs-cache-confirm-msg="'.$message.'" data-borlabs-cache-nonce="'.$nonce.'" data-borlabs-cache-maintenance-type="'.$type.'"></span>',
                    ],
                ]);

                /* Clear CSS-Pre-Cache Files Menu Item */
                $message = _x('Do you really want to clear the CSS pre-cache files?', 'Message', 'borlabs-cache');
                $nonce = wp_create_nonce('borlabs-cache-maintenance-clear-styles-pre-cache');
                $type = 'borlabsCacheClearStylesPreCache';

                $wp_admin_bar->add_node([
                    'id'=>'borlabs-cache-clear-styles-pre-cache',
                    'title'=>_x('Clear CSS pre-cache files', 'Menu entry', 'borlabs-cache'),
                    'parent'=>'borlabs-cache',
                    'href'=>'#',
                    'meta'=>[
                        'class'=>'borlabs-cache-toolbar-cache-maintenance',
                        'html'=>'<span data-borlabs-cache-confirm-msg="'.$message.'" data-borlabs-cache-nonce="'.$nonce.'" data-borlabs-cache-maintenance-type="'.$type.'"></span>',
                    ],
                ]);
            }
        }
    }

    /**
     * registerAdminScripts function.
     *
     * @access public
     * @return void
     */
    public function registerAdminScripts()
    {
        wp_enqueue_script('borlabs_cache_admin_js', plugins_url('javascript/borlabs-cache-admin.min.js', realpath(__DIR__.'/../../')));

        wp_enqueue_style(
            'borlabs_cache_admin_css',
            plugins_url('css/'.sanitize_html_class(get_user_option('admin_color'), 'fresh').'.css', realpath(__DIR__.'/../../')),
            [],
            '1.2'
        );
    }

    /**
     * handleSystemChanged function.
     *
     * @access public
     * @return void
     */
    public function handleSystemChanged()
    {
        if (Factory::get('Cache\Config')->get('miscellaneousDisableBorlabsRefreshCacheNotice') == false) {
            // Only display system changed message when the system was not just installed and activated
            $activatedMessage = get_option('BorlabsCacheActivatedMessage', false);

            if ($activatedMessage == true) {
                update_option('BorlabsCacheSystemChangedMessage', true, 'yes');
            }
        }
    }

    /**
     * getMessages function.
     *
     * @access public
     * @return void
     */
    public function getMessages()
    {
        if (Factory::get('Cache\Config')->getConfigChangedStatus() == true) {
            $this->messages[] = '<p class="notice">'._x('You have changed the configuration, but these settings are not active yet.<br>If you are done with your configuration, click the button below to apply your changes.', 'Status message', 'borlabs-cache').'</p>';
            $this->messages[] = '<form method="post"><p class="align-center"><label><input type="checkbox" name="cacheMaintenanceRefresh" value="1"> '._x('Mark all pages to refresh their cache', 'Status message', 'borlabs-cache').'</label></p><p class="action-buttons align-center"><input class="button-primary" type="submit" name="applyChanges" value="'.esc_attr_x('Apply changes', 'Button title', 'borlabs-cache').'"> <input class="button-secondary" type="submit" name="resetInactiveConfig" value="'.esc_attr_x('Undo all changes', 'Button title', 'borlabs-cache').'"></p></form>';
        }

        return implode("\n", $this->messages);
    }

    /**
     * addMessage function.
     *
     * @access public
     * @param mixed $message
     * @param mixed $type (critical, offer, error, info, notice, success)
     * @return void
     */
    public function addMessage($message, $type)
    {
        $this->messages[] = '<p class="'.\esc_attr($type).'">'.$message.'</p>';
    }

    /**
     * successfullyActivatedMessage function.
     *
     * @access public
     * @return void
     */
    public function successfullyActivatedMessage()
    {
        if (get_option('BorlabsCacheActivatedMessage', false) == false) {
        ?>
        <div class="notice notice-info is-dismissible" data-borlabs-cache-notice="borlabsCacheSystemActivatedMessage" data-borlabs-cache-nonce="<?php echo wp_create_nonce('borlabs-cache-dismissed-notice'); ?>">
            <p><?php _ex('Borlabs Cache was successfully activated. Please <a href="admin.php?page=borlabs-cache">click here</a> and activate caching.', 'Status message', 'borlabs-cache'); ?></p>
        </div>
        <?php
        }
    }

    /**
     * systemChangedMessage function.
     *
     * @access public
     * @return void
     */
    public function systemChangedMessage()
    {
        if (get_option('BorlabsCacheSystemChangedMessage', false) == true) {
        ?>
        <div class="notice notice-info is-dismissible" data-borlabs-cache-notice="borlabsCacheSystemChangedMessage" data-borlabs-cache-nonce="<?php echo wp_create_nonce('borlabs-cache-dismissed-notice'); ?>">
            <p><?php _ex('The status of a plugin or a theme was changed. Please refresh the cache of Borlabs Cache to ensure it operates as intended. <a href="admin.php?page=borlabs-cache">Click here</a> and scroll down to Cache Maintenance to refresh the cache.', 'Status message', 'borlabs-cache'); ?></p>
        </div>
        <?php
        }
    }
}
?>