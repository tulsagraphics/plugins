<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache\Backend;

use Borlabs\Factory;

class License
{
    private static $instance;

    private $imagePath;

    private $licenseData;

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    protected function __construct()
    {
    }

    public function display()
    {
        $this->imagePath = plugins_url('images', realpath(__DIR__.'/../../'));

        // Remove license
        if (!empty($_POST['formSend']) && !empty($_POST['removeLicense']) && check_admin_referer('borlabs_cache_remove_license')) {
            $this->removeLicense();

            Factory::get('Cache\Backend\Backend')->addMessage(_x('License removed successfully.', 'Status message', 'borlabs-cache'), 'success');
        }

        // Get personal license
        if (!empty($_POST['formSend']) && !empty($_POST['getPersonalLicense']) && check_admin_referer('borlabs_cache_get_personal_license')) {

            $responsePersonalLicense = Factory::get('Cache\API')->getPersonalLicense($_POST['licenseMail']);

            if (!empty($responsePersonalLicense->successMessage)) {
                Factory::get('Cache\Backend\Backend')->addMessage($responsePersonalLicense->successMessage, 'success');
            } else {
                Factory::get('Cache\Backend\Backend')->addMessage($responsePersonalLicense->errorMessage, 'error');
            }
        }

        // Get trial license
        if (!empty($_POST['formSend']) && !empty($_POST['getTrialLicense']) && check_admin_referer('borlabs_cache_get_trial_license')) {

            $responseTrialLicense = Factory::get('Cache\API')->getTrialLicense();

            if (!empty($responseTrialLicense->successMessage)) {
                Factory::get('Cache\Backend\Backend')->addMessage($responseTrialLicense->successMessage, 'success');
            } else {
                Factory::get('Cache\Backend\Backend')->addMessage($responseTrialLicense->errorMessage, 'error');
            }
        }

        // Register license
        if (!empty($_POST['formSend']) && !empty($_POST['registerLicense']) && check_admin_referer('borlabs_cache_manage_license')) {

            $responseRegisterLicense = Factory::get('Cache\API')->registerLicense($_POST['licenseKey'], $_POST['licenseMail']);

            if (!empty($responseRegisterLicense->successMessage)) {
                Factory::get('Cache\Backend\Backend')->addMessage($responseRegisterLicense->successMessage, 'success');
            } else {
                Factory::get('Cache\Backend\Backend')->addMessage($responseRegisterLicense->errorMessage, 'error');
            }
        }

        // Unlink website
        if (!empty($_POST['formSend']) && !empty($_POST['unlinkWebsite']) && check_admin_referer('borlabs_cache_unlink_routine')) {

            // If website was selected
            if (!empty($_POST['hash']) && !empty($_POST['licenseKey'])) {

                $responseUnlinkWebsite = Factory::get('Cache\API')->unlinkWebsite($_POST['licenseKey'], $_POST['hash']);

                if (!empty($responseUnlinkWebsite->successMessage)) {
                    Factory::get('Cache\Backend\Backend')->addMessage($responseUnlinkWebsite->successMessage, 'success');
                } else {
                    Factory::get('Cache\Backend\Backend')->addMessage($responseUnlinkWebsite->errorMessage, 'error');
                }
            }
        }

        $licenseKey             = sanitize_text_field(get_option('BorlabsCacheLicenseKey'));
        $licenseStatus          = $this->isValidLicense() ? 'valid' : 'expired';
        $licenseStatusMessage   = $this->getLicenseStatusMessage($licenseStatus);

        $licenseData            = $this->getLicenseData();
        $licenseRecoveryMail    = !empty($licenseData->recoveryMail) ? sanitize_text_field($licenseData->recoveryMail) : '';
        $licenseTitle           = !empty($licenseData->licenseType) ? $this->getLicenseTitle($licenseData->licenseType) : '';
        $licenseValidUntil      = !empty($licenseData->validUntil) ? Factory::get('Cache\Tools')->formatTimestamp(strtotime($licenseData->validUntil), 'Y-m-d') : '';
        $licenseMaxSites        = !empty($licenseData->maxSites) ? intval($licenseData->maxSites) : '';
        $licenseMaxPPD          = !empty($licenseData->maxPPD) ? intval($licenseData->maxPPD) : '';

        $licenseUnlinkData = get_option('BorlabsCacheUnlinkData');

        include Factory::get('Cache\Backend\Backend')->templatePath.'/license.html';
    }

    /**
     * getLicenseStatusMessage function.
     *
     * @access public
     * @param mixed $status
     * @return void
     */
    public function getLicenseStatusMessage($status)
    {
        if ($status == 'valid') {
            $message = _x('Your license is valid.', 'License status message', 'borlabs-cache');
        } elseif ($status == 'expired') {
            $message = _x('Your license has expired.', 'License status message', 'borlabs-cache');
        }

        return $message;
    }

    /**
     * getLicenseData function.
     *
     * @access public
     * @return void
     */
    public function getLicenseData()
    {
        if (empty($this->licenseData)) {
            // Such license system, much secure, wow.
            // Just kidding, you want all the trouble with updates, just to save some bucks?
            // Please support an independent developer and buy a license, thank you :)
            $licenseData = get_option('BorlabsCacheLicenseData');

            if (!empty($licenseData)) {
                $this->licenseData = unserialize(base64_decode($licenseData));
            } else {
                $this->licenseData = (object) ['noLicense'=>true];
            }
        }

        return $this->licenseData;
    }

    /**
     * getFeatureNotAvailableMessage function.
     *
     * @access public
     * @param bool $compact (default: false)
     * @return void
     */
    public function getFeatureNotAvailableMessage($compact = false)
    {
        $html = '<div class="messages">';
        $html .= '<p class="offer'.(!empty($compact) ? ' small' : '').'">';
        $html .= _x('This feature is only available with a Commercial License. <a href="https://borlabs.io/pricing/?utm_source=BorlabsCache&utm_medium=LicenseMessage&utm_campaign=Analysis" target="_blank">Click here</a> for more information.', 'License info', 'borlabs-cache');
        $html .= '</p></div>';

        return $html;
    }

    /**
     * getFeatureRequireLicenseMessage function.
     *
     * @access public
     * @return void
     */
    public function getFeatureRequireLicenseMessage()
    {
        $html = '<div class="messages">';
        $html .= '<p class="notice">';
        $html .= _x('The Cache Preloader requires a license key. Please go to <a href=\"admin.php?page=borlabs-cache-license\">License</a> and enter your license key or receive a free Personal License.', 'License info', 'borlabs-cache');
        $html .= '</p></div>';

        return $html;
    }

    /**
     * removeLicense function.
     *
     * @access public
     * @return void
     */
    public function removeLicense()
    {
        delete_option('BorlabsCacheLicenseData');
        delete_option('BorlabsCacheLicenseKey');
        delete_option('BorlabsCacheUnlinkData');

        // Set property to null
        $this->licenseData = null;

        // getLicenseData is now able to set the correct information for licenseData
        $this->getLicenseData();
    }

    /**
     * saveLicenseData function.
     *
     * @access public
     * @param mixed $licenseData
     * @return void
     */
    public function saveLicenseData($licenseData)
    {
        if (!empty($licenseData->licenseKey)) {

            update_option('BorlabsCacheLicenseData', base64_encode(serialize($licenseData)), 'no');
            update_option('BorlabsCacheLicenseKey', $licenseData->licenseKey, 'no');

            $this->licenseData = $licenseData;
        }
    }

    /**
     * isValidLicense function.
     *
     * @access public
     * @return void
     */
    public function isValidLicense()
    {
        // Such license system, much secure, wow.
        // Just kidding, you want all the trouble with updates, just to save some bucks?
        // Please support an independent developer and buy a license, thank you :)
        $isValid = false;

        if (!empty($this->getLicenseData()->validUntil)) {
            if ($this->getLicenseData()->validUntil >= date('Y-m-d')) {
                $isValid = true;
            }
        }

        return $isValid;
    }

    /**
     * isPremiumLicense function.
     *
     * @access public
     * @return void
     */
    public function isPremiumLicense()
    {
        // Such license system, much secure, wow.
        // Just kidding, you want all the trouble with updates, just to save some bucks?
        // Please support an independent developer and buy a license, thank you :)
        $isPremium = false;

        if (!empty($this->getLicenseData()->licenseType)) {
            if ($this->getLicenseData()->licenseType !== 'personal') {

                $isPremium = true;

                // Check if license is trial license and valid date exceeded
                if ($this->getLicenseData()->licenseType === 'trial') {
                    if ($this->isValidLicense() === false) {
                        $isPremium = false;
                    }
                } elseif ($this->getLicenseData()->licenseType !== 'envato') {
                    if ($this->isValidLicense() === false) {
                        $isPremium = false;
                    }
                }
            }
        } elseif (defined('BORLABS_CACHE_SLUG') && BORLABS_CACHE_SLUG == 'borlabs-cache-envato/borlabs-cache-envato.php') {
            $isPremium = true;
        }

        return $isPremium;
    }

    /**
     * getLicenseTitle function.
     *
     * @access public
     * @param mixed $licenseType
     * @return void
     */
    public function getLicenseTitle($licenseType)
    {
        $licenseType = strtolower($licenseType);

        switch ($licenseType) {
            case 'legacy':
                $licenseType = _x('Legacy', 'License Title', 'borlabs-cache');
                break;
            case 'envato':
                $licenseType = _x('Envato Edition', 'License Title', 'borlabs-cache');
                break;
            case 'commercial':
                $licenseType = _x('Commercial', 'License Title', 'borlabs-cache');
                break;
            case 'commercial_lifetime':
                $licenseType = _x('Commercial - Lifetime', 'License Title', 'borlabs-cache');
                break;
            case 'commercial_3s':
                $licenseType = _x('Commercial', 'License Title', 'borlabs-cache');
                break;
            case 'commercial_lifetime_3s':
                $licenseType = _x('Commercial - Lifetime', 'License Title', 'borlabs-cache');
                break;
            case 'agency_s':
                $licenseType = _x('Agency S', 'License Title', 'borlabs-cache');
                break;
            case 'agency_m':
                $licenseType = _x('Agency M', 'License Title', 'borlabs-cache');
                break;
            case 'agency_l':
                $licenseType = _x('Agency L', 'License Title', 'borlabs-cache');
                break;
            case 'trial':
                $licenseType = _x('Trial', 'License Title', 'borlabs-cache');
                break;
            case 'personal':
            default:
                $licenseType = _x('Personal', 'License Title', 'borlabs-cache');
        }

        return $licenseType;
    }
}
