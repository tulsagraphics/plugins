<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache\Backend;

use Borlabs\Factory;

class MetaBox
{
    private static $instance;

    public static function getInstance ()
    {

        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    protected function __construct()
    {
    }

    /**
     * registerMetaBox function.
     *
     * @access public
     * @return void
     */
    public function registerMetaBox()
    {
        $user = wp_get_current_user();

        //! TODO: Add support for custom roles
        if (!empty($user->allcaps['publish_posts'])) {
            add_meta_box(
                'borlabs-cache-meta-box',
                _x('Borlabs Cache', 'Meta box title', 'borlabs-cache'),
                [$this, 'displayMetaBox'],
                null,
                'side',
                'default',
                null
            );
        }
    }

    /**
     * displayMetaBox function.
     *
     * @access public
     * @param mixed $postData
     * @return void
     */
    public function displayMetaBox($postData)
    {
        global $wpdb;

        $lastUpdated = _x('Cache will be refreshed or created as soon as possible.', 'Post cache status', 'borlabs-cache');

        // Detect if page is homepage
        if (get_option('show_on_front') == 'page') {
            $homepageId = get_option('page_on_front');

            if (!empty($homepageId) && $homepageId == $postData->ID) {
                $cachedPageData = $this->getCachedData();

                // If multiple, find cached data with / url
                if (!empty($cachedPageData)) {
                    foreach ($cachedPageData as $key => $pageData) {
                        if (!empty($pageData->url) && $pageData->url == '/') {
                            // Update entry with index 0
                            $cachedPageData[0] = $pageData;
                        }
                    }
                }
            }
        }

        // If page is not homepage
        if (empty($cachedPageData)) {
            $cachedPageData = $this->getCachedData($postData->ID);
        }

        $lastUpdated = _x('Cache will be refreshed or created as soon as possible.', 'Post cache status', 'borlabs-cache');

        if (!empty($cachedPageData[0]->last_updated) && $cachedPageData[0]->last_updated != '0000-00-00 00:00:00') {
            $lastUpdated =  Factory::get('Cache\Tools')->formatTimestamp(strtotime($cachedPageData[0]->last_updated));
        }

        include Factory::get('Cache\Backend\Backend')->templatePath.'/meta-box.html';
    }

    public function saveMetaBoxData($postId, $post, $update)
    {
        global $wpdb;

        $user = wp_get_current_user();

        // Detect if page is homepage
        if (get_option('show_on_front') == 'page') {
            $homepageId = get_option('page_on_front');
        }

        // Check user rights again
        if (!empty($user->allcaps['publish_posts']) && !empty($_POST['borlabs-cache']['formSend'])) {
            $wpdb->query('
                UPDATE
                    `'.$wpdb->prefix.'borlabs_cache_pages`
                SET
                    `dont_cache`='.(isset($_POST['borlabs-cache']['dont_cache']) ? 1 : 0).'
                WHERE
                    '.(!empty($homepageId) && $homepageId == $postId ? '`is_home`=1' : '`post_id`='.intval($postId)).'
            ');
        }

        // Refresh cache if user forces to refresh it. The publish state will be ignored,
        // because sometimes a published page goes back to private or draft and we want to remove the cached version
        // Only perform these processes when the meta box was available to ensure, it is a valid page and not something like a price table (go_pricing)
        if (!empty($_POST['borlabs-cache']['metaBox'])) {
            $refreshCache = 0;

            if (!empty($_POST['borlabs-cache']['refresh_cache'])) {
                $refreshCache = 1;
            }

            // Only modify cache if page is published
            if (!empty($post->post_status) && $post->post_status == 'publish') {
                // Check if published pages always refresh cache
                if (Factory::get('Cache\Config')->get('cacheRefreshCacheAfterPublish')) {
                    $refreshCache = 1;
                }
            }

            $postPermalink = get_permalink($postId);

            // Check if page was indexed as 404 and remove it
            if ($refreshCache || (!empty($post->post_status) && $post->post_status == 'publish')) {
                if (!empty($postPermalink)) {
                    $urlParts = parse_url($postPermalink);

                    if (!empty($urlParts['path'])) {
                        Factory::get('Cache\Frontend\Garbage')->remove404FromCache($urlParts['path']);
                    }
                }
            }

            if ($refreshCache) {
                if (!empty($homepageId) && $homepageId == $postId) {
                    Factory::get('Cache\Frontend\Garbage')->refreshCache(0, 0, 1);
                } else {
                    Factory::get('Cache\Frontend\Garbage')->refreshCache($postId, 0, 0);
                }

                // Check if homepage should be refreshed
                if (Factory::get('Cache\Config')->get('cacheRefreshHomeCacheAfterPublish')) {
                    Factory::get('Cache\Frontend\Garbage')->refreshCache(0, 0, 1);
                }

                // Check if archives should be refreshed
                if (Factory::get('Cache\Config')->get('cacheRefreshArchiveCacheAfterPublish')) {
                    Factory::get('Cache\Frontend\Garbage')->refreshCache(0, $post->post_type, 0);
                }

                // Check if feeds should be refreshed
                if (Factory::get('Cache\Config')->get('cacheRefreshFeedCacheAfterPublish')) {
                    Factory::get('Cache\Frontend\Garbage')->refreshCache(0, 0, 0, 1);
                }
            } else {
                if (!empty($post->post_status) && $post->post_status == 'publish') {
                    // Check if homepage should be refreshed
                    if (Factory::get('Cache\Config')->get('cacheRefreshHomeCacheAfterPublish')) {
                        Factory::get('Cache\Frontend\Garbage')->refreshCache(0, 0, 1);
                    }

                    // Check if archives should be refreshed
                    if (Factory::get('Cache\Config')->get('cacheRefreshArchiveCacheAfterPublish')) {
                        Factory::get('Cache\Frontend\Garbage')->refreshCache(0, $post->post_type, 0);
                    }

                    // Check if feeds should be refreshed
                    if (Factory::get('Cache\Config')->get('cacheRefreshFeedCacheAfterPublish')) {
                        Factory::get('Cache\Frontend\Garbage')->refreshCache(0, 0, 0, 1);
                    }
                }
            }

            // Instant preload
            if ($refreshCache && !empty($post->post_status) && $post->post_status == 'publish') {
                Factory::get('Cache\Frontend\InstantPreloader')->preload($postPermalink);
            }
        }
    }

    /**
     * getCachedData function.
     *
     * @access public
     * @param int $postId (default: 0 == home)
     * @return void
     */
    public function getCachedData($postId = 0)
    {
        global $wpdb;

        $postId = intval($postId);

        // page_on_front - static page page ^_^
        // page_for_posts - static post page
        // show_on_front - page: page_on_front or if this is 0 then page_for_posts
        // show_on_front - posts

        $siteUrl = get_site_url();
        $https = parse_url($siteUrl)['scheme'] == 'https' ? 1 : 0;

        $cachedPost = $wpdb->get_results('
            SELECT
                `url`,
                `post_id`,
                `dont_cache`,
                `last_updated`
            FROM
                `'.$wpdb->prefix.'borlabs_cache_pages`
            WHERE
                '.(!empty($postId) ? '`post_id`="'.$postId.'"' : '`is_home`=1').'
                AND
                `https`='.$https.'
        ');

        return $cachedPost;
    }
}
