<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache;

use Borlabs\Factory;

class Uninstall {

    private static $instance;

    public static function getInstance () {

        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __clone () {}

    private function __wakeup () {}

    public function __construct () {}

    public function uninstallPlugin () {

        global $wpdb;

        if (is_multisite()) {

            $allBlogs = $wpdb->get_results('
                SELECT
                    `blog_id`
                FROM
                    `'.$wpdb->prefix.'blogs`
            ');

            if (!empty($allBlogs)) {

                $originalBlogId = get_current_blog_id();

                foreach ($allBlogs as $blogData) {

                    $tableName = $wpdb->prefix.$blogData->blog_id.'_borlabs_cache_pages';

                    $wpdb->query("DROP TABLE IF EXISTS `".$tableName."`");

                    switch_to_blog($blogData->blog_id);

                    delete_option('BorlabsCacheActivatedMessage');
                    delete_option('BorlabsCacheActiveCacheTasks');
                    delete_option('BorlabsCacheConfigActive');
                    delete_option('BorlabsCacheConfigChanged');
                    delete_option('BorlabsCacheConfigCustom');
                    delete_option('BorlabsCacheConfigInactive');
                    delete_option('BorlabsCacheConfigPreset');
                    delete_option('BorlabsCacheLicenseData');
                    delete_option('BorlabsCacheLicenseKey');
                    delete_option('BorlabsCacheLicensePurchaseCode');
                    delete_option('BorlabsCacheOptimizedDatabaseSavedBytes');
                    delete_option('BorlabsCachePreloadedStats');
                    delete_option('BorlabsCachePreloadedLastRequest');
                    delete_option('BorlabsCacheSystemChangedMessage');
                    delete_option('BorlabsCacheUpdateAvailableVersion');
                    delete_option('BorlabsCacheUpdateLastCheck');
                    delete_option('BorlabsCacheUnlinkData');
                    delete_option('BorlabsCacheVersion');
                    delete_option('BorlabsCacheXMLSitemapIndexFiles');
                    delete_option('BorlabsCacheXMLSitemapURLs');

                    /* Deprecated */
                    delete_option('BorlabsCacheInstallMessage');
                }

                switch_to_blog($originalBlogId);
            }

        } else {
            delete_option('BorlabsCacheActivatedMessage');
            delete_option('BorlabsCacheActiveCacheTasks');
            delete_option('BorlabsCacheConfigActive');
            delete_option('BorlabsCacheConfigChanged');
            delete_option('BorlabsCacheConfigCustom');
            delete_option('BorlabsCacheConfigInactive');
            delete_option('BorlabsCacheConfigPreset');
            delete_option('BorlabsCacheLicenseData');
            delete_option('BorlabsCacheLicenseKey');
            delete_option('BorlabsCacheLicensePurchaseCode');
            delete_option('BorlabsCacheOptimizedDatabaseSavedBytes');
            delete_option('BorlabsCachePreloadedStats');
            delete_option('BorlabsCachePreloadedLastRequest');
            delete_option('BorlabsCacheSystemChangedMessage');
            delete_option('BorlabsCacheUpdateAvailableVersion');
            delete_option('BorlabsCacheUpdateLastCheck');
            delete_option('BorlabsCacheUnlinkData');
            delete_option('BorlabsCacheVersion');
            delete_option('BorlabsCacheXMLSitemapIndexFiles');
            delete_option('BorlabsCacheXMLSitemapURLs');

            /* Deprecated */
            delete_option('BorlabsCacheInstallMessage');
        }

        $tableName = $wpdb->prefix.'borlabs_cache_pages';

        $wpdb->query("DROP TABLE IF EXISTS `".$tableName."`");

        // Delete cache folder
        $cacheFolder = realpath(__DIR__.'/../../../../').'/cache/borlabs_cache';

        if (file_exists($cacheFolder)) {
            Factory::get('Cache\Frontend\Garbage')->deleteFilesInDirectory($cacheFolder, true, true, true);

            rmdir($cacheFolder);
        }
    }
}
?>