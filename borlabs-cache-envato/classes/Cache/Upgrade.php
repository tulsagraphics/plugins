<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache;

use Borlabs\Factory;

class Upgrade
{

    private static $instance;

    private $versionUpgrades = [
        'upgradeVersion_1_1_5_2' => '1.1.5.2',
        'upgradeVersion_1_1_6' => '1.1.6',
        'upgradeVersion_1_1_7' => '1.1.7',
        'upgradeVersion_1_1_7_2' => '1.1.7.2',
        'upgradeVersion_1_1_7_3' => '1.1.7.3',
        'upgradeVersion_1_1_8' => '1.1.8',
        'upgradeVersion_1_1_8_1' => '1.1.8.1',
        'upgradeVersion_1_1_9' => '1.1.9',
        'upgradeVersion_1_1_9_1' => '1.1.9.1',
        'upgradeVersion_1_2' => '1.2',
        'upgradeVersion_1_2_1' => '1.2.1',
        'upgradeVersion_1_3' => '1.3',
    ];

    private $currentBlogId = '';

    public static function getInstance ()
    {

        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public function __construct()
    {
    }

    /**
     * getVersionUpgrades function.
     *
     * @access public
     * @return void
     */
    public function getVersionUpgrades()
    {
        return $this->versionUpgrades;
    }

    /**
     * upgradeVersion_1_1_5_2 function.
     *
     * @access public
     * @return void
     */
    public function upgradeVersion_1_1_5_2()
    {
        $scheduledEvent = wp_next_scheduled('borlabs_cache_update_plugin'); // Returns the timestamp

        if (!empty($scheduledEvent)) {
            wp_unschedule_event($scheduledEvent, 'borlabs_cache_update_plugin', []);
        }

        update_option('BorlabsCacheVersion', '1.1.5.2', 'no');
    }

    public function upgradeVersion_1_1_6()
    {
        update_option('BorlabsCacheVersion', '1.1.6', 'no');
    }

    public function upgradeVersion_1_1_7()
    {
        global $wpdb;

        $tableName = $wpdb->prefix.'borlabs_cache_pages';

        // Update structure
        if (!Factory::get('Cache\Install')->checkIfColumnExists($tableName, 'https')) {
            $wpdb->query('
                ALTER TABLE
                    `'.$tableName.'`
                ADD
                    `https` INT(1) unsigned DEFAULT 0
                    AFTER `hash`
            ');
        }

        // Check primary key
        $primaryKeyResult = $wpdb->get_results('
            SHOW INDEXES FROM
                `'.$tableName.'`
            WHERE `Key_name`="PRIMARY"
        ');

        // Check if only one PRIMARY key exists
        if (count($primaryKeyResult) == 1) {
            // Remove old PRIMARY key and add new PRIMARY key
            $wpdb->query('
                ALTER TABLE
                    `'.$tableName.'`
                DROP PRIMARY KEY,
                ADD PRIMARY KEY(`hash`, `https`)
            ');
        }

        // Remove old options
        delete_option('BorlabsCacheJavaScriptHandles');
        delete_option('BorlabsCacheCSSHandles');

        // Mark cache files as outdated
        Factory::get('Cache\Frontend\Garbage')->clearCache();

        update_option('BorlabsCacheVersion', '1.1.7', 'no');
    }

    public function upgradeVersion_1_1_7_2()
    {
        // Mark cache files as outdated
        Factory::get('Cache\Frontend\Garbage')->clearCache();

        update_option('BorlabsCacheVersion', '1.1.7.2', 'no');
    }

    public function upgradeVersion_1_1_7_3()
    {
        // Mark cache files as outdated
        Factory::get('Cache\Frontend\Garbage')->clearCache();

        update_option('BorlabsCacheVersion', '1.1.7.3', 'no');
    }

    public function upgradeVersion_1_1_8()
    {
        // Mark cache files as outdated
        Factory::get('Cache\Frontend\Garbage')->clearCache();

        update_option('BorlabsCacheVersion', '1.1.8', 'no');
        update_option('BorlabsCacheUpdateAvailableVersion', '1.1.8', 'no');
    }

    public function upgradeVersion_1_1_8_1()
    {
        update_option('BorlabsCacheVersion', '1.1.8.1', 'no');
        update_option('BorlabsCacheUpdateAvailableVersion', '1.1.8.1', 'no');
    }

    public function upgradeVersion_1_1_9()
    {
        // Get configs
        $activeConfig = Factory::get('Cache\Config')->getConfig('active');
        $inactiveConfig = Factory::get('Cache\Config')->getConfig('inactive');

        // Set new setting to false
        $activeConfig['miscellaneousNginx'] = false;
        $inactiveConfig['miscellaneousNginx'] = false;

        // Save config
        update_option('BorlabsCacheConfigActive', $activeConfig, 'no');
        update_option('BorlabsCacheConfigInactive', $inactiveConfig, 'no');

        update_option('BorlabsCacheVersion', '1.1.9', 'no');
        update_option('BorlabsCacheUpdateAvailableVersion', '1.1.9', 'no');
    }

    public function upgradeVersion_1_2()
    {
        global $wpdb;

        $tableName = $wpdb->prefix.'borlabs_cache_pages';

        // Replaced with BorlabsCacheUnlinkData
        delete_option('BorlabsCacheUnlinkSites');

        // Removed
        delete_option('BorlabsCacheFreeLicenseOffered');
        delete_option('BorlabsCacheLicenseStatus');
        delete_option('BorlabsCachePreloadedToday');
        delete_option('BorlabsCachePreloadedTodayStamp');
        delete_option('BorlabsCacheExternalMessage');

        // Update configs
        // Get configs
        $activeConfig = get_option('BorlabsCacheConfigActive');
        $inactiveConfig = get_option('BorlabsCacheConfigInactive');

        // Set new settings
        $activeConfig['preloaderActivated']     = true;
        $activeConfig['maxSimultaneousTasks']   = 5;
        $activeConfig['scriptsFixSemicolon']    = true;
        $activeConfig['cacheDontCachePagesContainPath'][] = '(/page/[0-9]{2,})';
        $activeConfig['cacheDontCachePagesOfTaxonomy'] = [];
        $activeConfig['miscellaneousDisableBorlabsToolbarMenuItem']    = false;
        $activeConfig['cacheLifetime']['404']   = 604800;

        $inactiveConfig['preloaderActivated']   = true;
        $inactiveConfig['maxSimultaneousTasks'] = 5;
        $inactiveConfig['scriptsFixSemicolon']  = true;
        $inactiveConfig['cacheDontCachePagesContainPath'][] = '(/page/[0-9]{2,})';
        $inactiveConfig['cacheDontCachePagesOfTaxonomy'] = [];
        $inactiveConfig['miscellaneousDisableBorlabsToolbarMenuItem']  = false;
        $inactiveConfig['cacheLifetime']['404'] = 604800;

        // Adjust the cache lifetimes when they weren't changed
        if ($activeConfig['cacheLifetime']['home'] == 86400) {
            $activeConfig['cacheLifetime']['home'] = 604800;
        }

        if ($inactiveConfig['cacheLifetime']['home'] == 86400) {
            $inactiveConfig['cacheLifetime']['home'] = 604800;
        }

        // Achives
        if ($activeConfig['cacheLifetime']['archives']['-'] == 86400) {
            $activeConfig['cacheLifetime']['archives']['-'] = 604800;
        }

        if ($inactiveConfig['cacheLifetime']['archives']['-'] == 86400) {
            $inactiveConfig['cacheLifetime']['archives']['-'] = 604800;
        }

        // PostType default
        if ($activeConfig['cacheLifetime']['postType']['-'] == 86400) {
            $activeConfig['cacheLifetime']['postType']['-'] = 604800;
        }

        if ($inactiveConfig['cacheLifetime']['postType']['-'] == 86400) {
            $inactiveConfig['cacheLifetime']['postType']['-'] = 604800;
        }

        // Feed
        if ($activeConfig['cacheLifetime']['feed'] == 86400) {
            $activeConfig['cacheLifetime']['feed'] = 604800;
        }

        if ($inactiveConfig['cacheLifetime']['feed'] == 86400) {
            $inactiveConfig['cacheLifetime']['feed'] = 604800;
        }

        // Don't cache all taxonomies by default
        $allTaxonomies = Factory::get('Cache\Backend\AdvancedSettings')->getTaxonomies();

        if (!empty($allTaxonomies)) {
            foreach ($allTaxonomies as $taxonomyData) {
                $activeConfig['cacheDontCachePagesOfTaxonomy'][]  = $taxonomyData->name;
                $inactiveConfig['cacheDontCachePagesOfTaxonomy'][]  = $taxonomyData->name;

                // Delete from cache index
                $wpdb->query('
                    DELETE FROM
                        `'.$tableName.'`
                    WHERE
                        `taxonomy`="'.$wpdb->_escape($taxonomyData->name).'"
                ');
            }
        }

        // Save config
        update_option('BorlabsCacheConfigActive', $activeConfig, 'no');
        update_option('BorlabsCacheConfigInactive', $inactiveConfig, 'no');

        // Upgrade table structure
        if (!Factory::get('Cache\Install')->checkIfColumnExists($tableName, 'prefix')) {
            $wpdb->query('
                ALTER TABLE
                    `'.$tableName.'`
                ADD
                    `prefix` VARCHAR(16) DEFAULT ""
                    AFTER `https`
            ');
        }

        // Save purchaseCode into licenseKey option
        if (defined('BORLABS_CACHE_SLUG') && BORLABS_CACHE_SLUG == 'borlabs-cache-envato/borlabs-cache-envato.php') {

            $purchaseCode = get_option('BorlabsCacheLicensePurchaseCode');

            update_option('BorlabsCacheLicenseKey', $purchaseCode, 'no');

            // Delete option
            delete_option('BorlabsCacheLicensePurchaseCode');
        }

        // Re-activate license
        $licenseKey = get_option('BorlabsCacheLicenseKey');

        if (!empty($licenseKey)) {

            // Remove license data from blog
            Factory::get('Cache\Backend\License')->removeLicense();

            // Retrieve new license data
            $responseRegisterLicense = $this->licenseUpdate($licenseKey);
        }

        // Mark cache files as outdated
        Factory::get('Cache\Frontend\Garbage')->clearCache();

        update_option('BorlabsCacheVersion', '1.2', 'no');
        update_option('BorlabsCacheUpdateAvailableVersion', '1.2', 'no');
    }

    private function licenseUpdate($licenseKey)
    {
        $url = get_site_url();
        $urlWordPress = get_home_url();

        $data = [
            'licenseKey'=>$licenseKey,
            'email'=>'',
            'url'=>$url,
            'urlWordPress'=>($url != $urlWordPress ? $urlWordPress : ''),
            'cron'=>Factory::get('Cache\Config')->get('cacheCronService'),
            'cronInterval'=>Factory::get('Cache\Config')->get('cacheCronInterval'),
            'currentTime'=>Factory::get('Cache\Tools')->getDBTime(),
            'version'=>BORLABS_CACHE_VERSION,
        ];

        // Register license key
        $args = [
            'timeout'=>45,
            'body'=>$data,
        ];

        // Make request
        $response = wp_remote_post(
            'https://api.cache.borlabs.io/v3/register',
            $args
        );

        $error = false;

        if (!empty($response) && $response['response']['code'] == 200 && is_array($response) && !empty($response['body'])) {

            $responseBody = json_decode($response['body']);

            if (empty($responseBody->error)) {
                if (!empty($responseBody->licenseKey)) {
                    // Save license data
                    Factory::get('Cache\Backend\License')->saveLicenseData($responseBody);
                } else {
                    $error = true;
                }
            } else {
                $error = true;
            }
        } else {
            $error = true;
        }

        if ($error) {
            // Something went wrong, save license key back to option
            update_option('BorlabsCacheLicenseKey', $licenseKey, 'no');
        }
    }

    public function upgradeVersion_1_2_1()
    {
        update_option('BorlabsCacheVersion', '1.2.1', 'no');
        update_option('BorlabsCacheUpdateAvailableVersion', '1.2.1', 'no');
    }

    public function upgradeVersion_1_3()
    {
        update_option('BorlabsCacheVersion', '1.3', 'no');
        update_option('BorlabsCacheUpdateAvailableVersion', '1.3', 'no');
    }
}
