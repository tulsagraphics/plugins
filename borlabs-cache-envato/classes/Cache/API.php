<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache;

use Borlabs\Factory;

class API
{

    private static $instance;

    private $apiURL = 'https://api.cache.borlabs.io/v3';
    private $updateURL = 'https://update.borlabs.io/v1';
    private $response = [];

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public function __construct()
    {
    }

    /**
     * addVars function.
     *
     * @access public
     * @param mixed $vars
     * @return void
     */
    public function addVars($vars)
    {
        $vars[] = '__borlabsCacheCall';

        return $vars;
    }

    /**
     * detectRequests function.
     *
     * @access public
     * @return void
     */
    public function detectRequests()
    {
        global $wp;

        if (!empty($wp->query_vars['__borlabsCacheCall'])) {

            $data = json_decode(file_get_contents("php://input"));

            $this->handleRequest($wp->query_vars['__borlabsCacheCall'], $data);

            exit;
        }
    }

    /**
     * handleRequest function.
     *
     * @access public
     * @param mixed $call
     * @param mixed $token
     * @param mixed $data
     * @return void
     */
    public function handleRequest($call, $data)
    {
        // Check if request is authorized
        if ($this->isAuthorized($data)) {

            if ($call == 'getPreloadData') {

                $this->getPreloadData($data);

            } elseif ($call == 'updateLicense') {

                $this->updateLicense($data);

            } elseif ($call == 'performMaintenance') {

                $this->performMaintenance();
            }
        }
    }

    /**
     * isAuthorized function.
     *
     * @access public
     * @param mixed $data
     * @return void
     */
    public function isAuthorized($data)
    {
        $isAuthorized = false;

        // Function getallheaders doesn't exist on FPM...
        $allHeaders = [];

        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $allHeaders[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }

        $hash = '';

        if (!empty($allHeaders['X-Borlabs-Cache-Auth'])) {
            $hash = $allHeaders['X-Borlabs-Cache-Auth'];
        }

        if (Factory::get('Cache\HMAC')->isValid($data, Factory::get('Cache\Backend\License')->getLicenseData()->salt, $hash)) {
            $isAuthorized = true;
        }

        return $isAuthorized;
    }

    /**
     * getPreloadData function.
     *
     * @access public
     * @param mixed $data
     * @return void
     */
    public function getPreloadData($data)
    {
        global $wpdb;

        // Save preload stats from Borlabs Cache Preloader
        update_option('BorlabsCachePreloadedStats', $data->preloadStats, 'no');

        // Save timestamp of this request
        update_option('BorlabsCachePreloadedLastRequest', ['timestamp'=>time()], 'no');

        // Detect if blog is configured for http or https.
        // We only preload the configured scheme not the duplicate content
        $url = get_site_url();
        $https = parse_url($url)['scheme'] == 'https' ? 1 : 0;

        // Get info about total cached pages and their runtime so Borlabs Cache can calculate how much pages should be run in one preload cycle
        $cacheInfoTotalPages = $wpdb->get_results('
            SELECT
                COUNT(*) as `total_pages_cached`
            FROM
                `'.$wpdb->prefix.'borlabs_cache_pages`
            WHERE
                `dont_cache`=0
                AND
                `is_404`=0
                AND
                `https`='.$https.'
        ');

        $cacheInfoAvgRuntimes = $wpdb->get_results('
            SELECT
                AVG(`runtime_without_cache`) as `average_runtime_without_cache`,
                AVG(`runtime_with_cache`) as `average_runtime_with_cache`
            FROM
                `'.$wpdb->prefix.'borlabs_cache_pages`
            WHERE
                `dont_cache`=0
                AND
                `is_404`=0
                AND
                `https`='.$https.'
                AND
                `runtime_with_cache`>0
        ');

        // Get next 100 pages which should be preloaded
        $preloadPages = $wpdb->get_results('
            SELECT
                `domain`,
                `hash`,
                `https`,
                `prefix`,
                `url`,
                IF(`next_update`="0000-00-00 00:00:00", NOW(), `next_update`) as "next_update",
                `is_archive`
            FROM
                `'.$wpdb->prefix.'borlabs_cache_pages`
            WHERE
                `dont_cache`=0
                AND
                `is_404`=0
                AND
                `https`='.$https.'
            ORDER BY
                `next_update` ASC
            LIMIT 0,250
        ');

        echo json_encode([
            'cacheInformation'=>[
                'totalPagesCached'=>$cacheInfoTotalPages[0]->total_pages_cached,
                'avgRuntimeWithoutCache'=>$cacheInfoAvgRuntimes[0]->average_runtime_without_cache,
                'avgRuntimeWithCache'=>$cacheInfoAvgRuntimes[0]->average_runtime_with_cache,
            ],
            'settings'=>[
                'cacheActivated'=>Factory::get('Cache\Config')->get('cacheActivated'),
                'preloaderActivated'=>Factory::get('Cache\Config')->get('preloaderActivated'),
                'maxSimultaneousTasks'=>Factory::get('Cache\Config')->get('maxSimultaneousTasks'),
                'cron'=>Factory::get('Cache\Config')->get('cacheCronService'),
                'cronInterval'=>Factory::get('Cache\Config')->get('cacheCronInterval'),
            ],
            'system'=>[
                'currentTime'=>Factory::get('Cache\Tools')->getDBTime(),
                'premium'=>Factory::get('Cache\Backend\License')->isPremiumLicense(),
                'slug'=>BORLABS_CACHE_SLUG,
                'valid'=>Factory::get('Cache\Backend\License')->isValidLicense(),
                'version'=>BORLABS_CACHE_VERSION,
            ],
            'preload'=>$preloadPages,
        ]);
    }

    /**
     * updateLicense function.
     *
     * @access public
     * @param mixed $data
     * @return void
     */
    public function updateLicense($data)
    {
        if (!empty($data->licenseKey)) {
            Factory::get('Cache\Backend\License')->saveLicenseData($data);
        }

        echo json_encode([
            'success'=>true,
        ]);
    }

    /**
     * performMaintenance function.
     *
     * @access public
     * @return void
     */
    public function performMaintenance()
    {
        // Clear page cache
        Factory::get('Cache\Frontend\Garbage')->clearPageCache();

        // Remove old 404 entries from index
        Factory::get('Cache\Frontend\Garbage')->remove404Entries();

        echo json_encode([
            'success'=>true,
        ]);
    }

    /**
     * sendSupportRequest function.
     *
     * @access public
     * @param mixed $mail
     * @param mixed $name
     * @param mixed $topic
     * @param mixed $message
     * @param mixed $additionalData
     * @return void
     */
    public function sendSupportRequest($mail, $name, $topic, $message, $additionalData)
    {
        $data = [
            'mail'=>$mail,
            'name'=>$name,
            'topic'=>$topic,
            'message'=>$message,
            'additionalData'=>$additionalData,
        ];

        // Register site and receive a trial license
        $response = $this->restPostRequest('/support', $data);

        if (!empty($response->success)) {

            return (object) [
                'success'=>true,
                'successMessage'=>_x('Support ticket submitted.', 'API - Success Message', 'borlabs-cache'),
            ];
        } else {
            return $response;
        }
    }

    /**
     * checkForUpdate function.
     *
     * @access public
     * @return void
     */
    public function checkForUpdate()
    {
        $response = $this->getLatestVersion();

        if (!empty($response->new_version)) {

            if (version_compare(BORLABS_CACHE_VERSION, $response->new_version, '<')) {
                // Remove update transient
                set_site_transient('update_plugins', null);

                // Run WordPress update checker
                do_action('wp_update_plugins');
            }

            return $response->new_version;
        }
    }

    /**
     * getPersonalLicense function.
     *
     * @access public
     * @param string $email (default: '')
     * @return void
     */
    public function getPersonalLicense($email = '')
    {
        $url = get_site_url();
        $urlWordPress = get_home_url();

        $data = [
            'licenseKey'=>'',
            'email'=>$email,
            'url'=>$url,
            'urlWordPress'=>($url != $urlWordPress ? $urlWordPress : ''),
            'cron'=>Factory::get('Cache\Config')->get('cacheCronService'),
            'cronInterval'=>Factory::get('Cache\Config')->get('cacheCronInterval'),
            'currentTime'=>Factory::get('Cache\Tools')->getDBTime(),
            'version'=>BORLABS_CACHE_VERSION,
        ];

        // Register site and receive a trial license
        $response = $this->restPostRequest('/register', $data);

        if (!empty($response->licenseKey)) {
            // Save license data
            Factory::get('Cache\Backend\License')->saveLicenseData($response);

            return (object) [
                'success'=>true,
                'successMessage'=>_x('Personal License received successfully.', 'API - Success Message', 'borlabs-cache'),
            ];
        } else {
            return $response;
        }
    }

    /**
     * getTrialLicense function.
     *
     * @access public
     * @return void
     */
    public function getTrialLicense()
    {
        if (!empty(Factory::get('Cache\Backend\License')->getLicenseData()->salt)) {

            $data = [
                'licenseKey'=>Factory::get('Cache\Backend\License')->getLicenseData()->licenseKey,
                'url'=>get_site_url(),
            ];

            // Try to switch personal license to trial license
            $response = $this->restPostRequest('/register/trial', $data, Factory::get('Cache\Backend\License')->getLicenseData()->salt);

            if (!empty($response->licenseKey)) {
                // Save license data
                Factory::get('Cache\Backend\License')->saveLicenseData($response);

                return (object) [
                    'success'=>true,
                    'successMessage'=>_x('Trial License received successfully.', 'API - Success Message', 'borlabs-cache'),
                ];
            } else {
                return $response;
            }
        }
    }

    /**
     * registerLicense function.
     *
     * @access public
     * @param mixed $licenseKey
     * @param string $email (default: '')
     * @return void
     */
    public function registerLicense($licenseKey, $email = '')
    {
        $url = get_site_url();
        $urlWordPress = get_home_url();

        $data = [
            'licenseKey'=>$licenseKey,
            'email'=>$email,
            'url'=>$url,
            'urlWordPress'=>($url != $urlWordPress ? $urlWordPress : ''),
            'cron'=>Factory::get('Cache\Config')->get('cacheCronService'),
            'cronInterval'=>Factory::get('Cache\Config')->get('cacheCronInterval'),
            'currentTime'=>Factory::get('Cache\Tools')->getDBTime(),
            'version'=>BORLABS_CACHE_VERSION,
        ];

        // Register site and receive a trial license
        $response = $this->restPostRequest('/register', $data);

        if (!empty($response->licenseKey)) {

            // Save license data
            Factory::get('Cache\Backend\License')->saveLicenseData($response);

            return (object) [
                'success'=>true,
                'successMessage'=>_x('License registered successfully.', 'API - Success Message', 'borlabs-cache'),
            ];
        } elseif (!empty($response->unlink)) {
            // License is already used and the unlink routine was triggered
            update_option('BorlabsCacheUnlinkData', (object) ['licenseKey'=>$licenseKey, 'unlink'=>$response->unlink], 'no');

            return $response;
        } else {
            return $response;
        }
    }

    /**
     * unlinkWebsite function.
     *
     * @access public
     * @param mixed $licenseKey
     * @param mixed $unlinkSiteHash
     * @return void
     */
    public function unlinkWebsite($licenseKey, $unlinkSiteHash)
    {
        $data = [
            'licenseKey'=>$licenseKey,
            'hash'=>$unlinkSiteHash,
        ];

        // Unlink website
        $response = $this->restPostRequest('/register/unlink', $data);

        if (!empty($response->unlink)) {

            delete_option('BorlabsCacheUnlinkData');

            return (object) [
                'success'=>true,
                'successMessage'=>_x('Selected website unlinked successfully. You can now re-enter your license key to register this website.', 'API - Success Message', 'borlabs-cache'),
            ];
        } else {
            return $response;
        }
    }

    /**
     * restPostRequest function.
     *
     * @access private
     * @param mixed $route
     * @param mixed $data
     * @param mixed $salt (default: null)
     * @return void
     */
    private function restPostRequest($route, $data, $salt = null)
    {
        $args = [
            'timeout'=>45,
            'body'=>$data,
        ];

        // Add authentification header
        if (!empty($salt)) {
            $args['headers'] = [
                'X-Borlabs-Cache-Auth'=>Factory::get('Cache\HMAC')->hash($data, $salt),
            ];
        }

        // Make post request
        $response = wp_remote_post(
            $this->apiURL.$route,
            $args
        );

        if (!empty($response) && is_array($response) && $response['response']['code'] == 200 && !empty($response['body'])) {

            $responseBody = json_decode($response['body']);

            if (empty($responseBody->error)) {
                return $responseBody;
            } else {
                // Borlabs Cache API messages
                $responseBody->errorMessage = $this->translateErrorCode($responseBody->errorCode, $responseBody->message);

                return $responseBody;
            }
        } else {
            if (!empty($response['response']['message'])) {
                // Server message
                return (object) [
                    'errorMessage'=>$response['response']['code'].' '.$response['response']['message'],
                ];
            } else {
                // WP_Error messages
                return (object) [
                    'errorMessage'=>implode('<br>', $response->get_error_messages())
                ];
            }
        }
    }

    /**
     * translateErrorCode function.
     *
     * @access private
     * @param mixed $errorCode
     * @return void
     */
    private function translateErrorCode($errorCode, $message = '')
    {
        $errorMessage = '';

        if ($errorCode == 'accessError') {

            $errorMessage = _x('The request was blocked. Please try again later.', 'API - Error Message', 'borlabs-cache');

        } elseif ($errorCode == 'hadTrialBefore') {

            $errorMessage = _x('Your website already had a Trial License and can not get a Trial Version a second time.', 'API - Error Message', 'borlabs-cache');

        } elseif ($errorCode == 'switchToTrial') {

            $errorMessage = sprintf(_x('An error occured while switching the license to a Trial License. Please contact the support. %s', 'API - Error Message', 'borlabs-cache'), $message);

        } elseif ($errorCode == 'trialNotPossible') {

            $errorMessage = _x('A Trial License is only available for Personal Licenses.', 'API - Error Message', 'borlabs-cache');

        } elseif ($errorCode == 'unlinkRoutine') {

            $errorMessage = _x('Your license key is already being used by another website.', 'API - Error Message', 'borlabs-cache');

        } elseif ($errorCode == 'unlinkRoutineForTrialLicense') {

            $errorMessage = _x('A Trial License key can not be moved to another website.', 'API - Error Message', 'borlabs-cache');

        } elseif ($errorCode == 'validateHash') {

            $errorMessage = sprintf(_x('The request to the API could not be validated. %s', 'API - Error Message', 'borlabs-cache'), $message);

        } elseif ($errorCode == 'invalidLicenseKey') {

            $errorMessage = _x('Your license key is not valid.', 'API - Error Message', 'borlabs-cache');

        } else {
            // errorCode == error
            $errorMessage = sprintf(_x('An error occurred. Please contact the support. %s', 'API - Error Message', 'borlabs-cache'), $message);
        }

        return $errorMessage;
    }

    /**
     * getPluginInformation function.
     *
     * @access public
     * @return void
     */
    public function getPluginInformation()
    {
        $response = wp_remote_post(
            $this->updateURL.'/plugin-information/'.(defined('BORLABS_CACHE_DEV_BUILD') && BORLABS_CACHE_DEV_BUILD == true ? 'dev-' : '').dirname(BORLABS_CACHE_SLUG),
            [
                'timeout'   =>45,
                'body'      =>[
                    'version'=>BORLABS_CACHE_VERSION,
                    'product'=>(defined('BORLABS_CACHE_DEV_BUILD') && BORLABS_CACHE_DEV_BUILD == true ? 'dev-' : '').dirname(BORLABS_CACHE_SLUG),
                ]
            ]
        );

        if (!empty($response) && is_array($response) && !empty($response['body'])) {
            $body = json_decode($response['body']);

            if (!empty($body->success) && !empty($body->pluginInformation)) {
                return unserialize($body->pluginInformation);
            }
        }
    }

    /**
     * getLatestVersion function.
     *
     * @access public
     * @return void
     */
    public function getLatestVersion()
    {
        $response = wp_remote_post(
            $this->updateURL.'/latest-version/'.(defined('BORLABS_CACHE_DEV_BUILD') && BORLABS_CACHE_DEV_BUILD == true ? 'dev-' : '').dirname(BORLABS_CACHE_SLUG),
            [
                'timeout'   =>45,
                'body'      =>[
                    'version'=>BORLABS_CACHE_VERSION,
                    'product'=>(defined('BORLABS_CACHE_DEV_BUILD') && BORLABS_CACHE_DEV_BUILD == true ? 'dev-' : '').dirname(BORLABS_CACHE_SLUG),
                ]
            ]
        );

        if (!empty($response) && is_array($response) && !empty($response['body'])) {
            $body = json_decode($response['body']);

            if (!empty($body->success) && !empty($body->updateInformation)) {
                return unserialize($body->updateInformation);
            }
        }
    }
}
