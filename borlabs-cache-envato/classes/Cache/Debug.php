<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs\Cache;

use Borlabs\Factory;

class Debug
{
    private static $instance;

    private $debugEnabled = false;

    public static function getInstance ()
    {

        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function __construct()
    {
        if (defined('BORLABS_CACHE_DEBUG') && BORLABS_CACHE_DEBUG == true) {
            $this->debugEnabled = true;
        }
    }

    /**
     * isDebugEnabled function.
     *
     * @access public
     * @return void
     */
    public function isDebugEnabled()
    {
        return $this->debugEnabled;
    }

    /**
     * isDebugLogWritable function.
     *
     * @access public
     * @return void
     */
    public function isDebugLogWritable()
    {
        if (defined('BORLABS_CACHE_DEBUG_WRITE_TO_FILE')) {
            if (!file_exists(BORLABS_CACHE_DEBUG_WRITE_TO_FILE)) {
                touch(BORLABS_CACHE_DEBUG_WRITE_TO_FILE);
            }

            if (is_writable(BORLABS_CACHE_DEBUG_WRITE_TO_FILE)) {
                return true;
            }
        }

        return false;
    }

    /**
     * getDebugLogFile function.
     *
     * @access public
     * @return void
     */
    public function getDebugLogFile()
    {
        if (defined('BORLABS_CACHE_DEBUG_WRITE_TO_FILE')) {
            return BORLABS_CACHE_DEBUG_WRITE_TO_FILE;
        }

        return false;
    }
}
