<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A. Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

namespace Borlabs;

class Factory
{

    private function __construct()
    {
    }

    public static function get($className)
    {
        $className = 'Borlabs\\' . $className;

        if (class_exists($className)) {
            if (self::isSingleton($className)) {
                return $className::getInstance();
            } else {
                return new $className();
            }
        } else {
            return false;
        }
    }

    public static function isSingleton($className)
    {
        $reflection = new \ReflectionClass($className);

        if ($reflection->hasMethod('getInstance')) {
            return true;
        }

        return false;
    }
}
