<?php
/*
Plugin Name: Borlabs Cache - Envato Edition
Plugin URI: https://borlabs.io/
Description: Borlabs Cache is a caching and optimization plugin for WordPress. It reduces server load and saves bandwidth through different types of browser caching, merging of CSS and JavaScript files, minification and compression. Your content is delivered to your visitors in milliseconds.
Author: Benjamin A. Bornschein, Borlabs
Author URI: https://borlabs.io
Version: 1.3
Text Domain: borlabs-cache
Domain Path: /languages
*/

define('BORLABS_CACHE_VERSION', '1.3');
define('BORLABS_CACHE_SLUG', plugin_basename(__FILE__));
define('BORLABS_CACHE_PLUGIN_URL', plugin_dir_url(__FILE__));

// Prevent direct access
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('Borlabs\Cache\Init')) {

    if (version_compare(phpversion(), '5.6', '>=')) {

        include_once plugin_dir_path(__FILE__).'classes/Autoloader.php';

        $Autoloader = new \Borlabs\Autoloader();
	    $Autoloader->register();
    	$Autoloader->addNamespace('Borlabs', realpath(plugin_dir_path(__FILE__).'/classes'));

        register_activation_hook(__FILE__, array(\Borlabs\Factory::get('Cache\Init'), 'pluginActivated'));
        register_deactivation_hook(__FILE__, array(\Borlabs\Factory::get('Cache\Init'), 'pluginDeactivated'));

        /* Init plugin */
        if (is_admin()) {
            /* Backend */
            \Borlabs\Factory::get('Cache\Init')->initBackend();
        } else {
            /* Frontend */
            \Borlabs\Factory::get('Cache\Init')->initFrontend();
        }

        /* Update */
        \Borlabs\Factory::get('Cache\Init')->initUpdateHooks();

        /* Call after upgrade process is complete */
        add_action('upgrader_process_complete', array(\Borlabs\Factory::get('Cache\Update'), 'upgradeComplete'), 10, 2);

        /* Third Party Developer Helper Class Shortcut Function - fun fact: in german this would be a single noun! */
        if (!function_exists('BorlabsCacheHelper')) {
            function BorlabsCacheHelper()
            {
                return \Borlabs\Factory::get('Cache\ThirdPartyHelper');
            }
        }

    } else {
        //! Fallback for very old php version
        add_action('admin_notices', function () {
        ?>
        <div class="notice notice-error">
            <p><?php _ex('Your PHP version is <a href="http://php.net/supported-versions.php" rel="noreferrer" target="_blank">outdated</a> and not supported by Borlabs Cache. Please disable Borlabs Cache, upgrade to PHP 5.6 or higher, and enable Borlabs Cache again. It is necessary to follow these steps in order.', 'Status message', 'borlabs-cache'); ?></p>
        </div>
        <?php
        });
    }
} else {
    add_action('admin_notices', function () {
        ?>
        <div class="notice notice-error">
            <p><?php
                if (defined('BORLABS_CACHE_SLUG') && BORLABS_CACHE_SLUG == 'borlabs-cache-envato/borlabs-cache-envato.php') {
                    _ex('<strong>Borlabs Cache</strong> is installed and active. It is not possible to run <strong>Borlabs Cache</strong> and <strong>Borlabs Cache - Envato Edition</strong> at the same time.<br>Please disable one of the two to remove this message.', 'Status message', 'borlabs-cache');
                } else {
                    _ex('<strong>Borlabs Cache - Envato Edition</strong> is installed and active. It is not possible to run <strong>Borlabs Cache - Envato Edition</strong> and <strong>Borlabs Cache</strong> at the same time.<br>Please disable one of the two to remove this message.', 'Status message', 'borlabs-cache');
                }
            ?></p>
        </div>
        <?php
    });
}
?>