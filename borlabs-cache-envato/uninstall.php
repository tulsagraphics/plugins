<?php
/*
 * ----------------------------------------------------------------------
 *
 *                          Borlabs Cache
 *                      developed by Borlabs
 *
 * ----------------------------------------------------------------------
 *
 * Copyright 2017-2018 Borlabs - Benjamin A. Bornschein. All rights reserved.
 * This file may not be redistributed in whole or significant part.
 * Content of this file is protected by international copyright laws.
 *
 * ----------------- Borlabs Cache IS NOT FREE SOFTWARE -----------------
 *
 * @copyright Borlabs - Benjamin A Bornschein, https://borlabs.io
 * @author Benjamin A. Bornschein, Borlabs ben@borlabs.io
 *
 */

if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}

if (version_compare(phpversion(), '5.6', '>=')) {

    include_once plugin_dir_path(__FILE__).'classes/Autoloader.php';

    $Autoloader = new \Borlabs\Autoloader();
	$Autoloader->register();
	$Autoloader->addNamespace('Borlabs', realpath(plugin_dir_path(__FILE__).'/classes'));

    \Borlabs\Factory::get('Cache\Uninstall')->uninstallPlugin();
}
?>