<?php
/**
 * SCREETS © 2018
 *
 * SCREETS, d.o.o. Sarajevo. All rights reserved.
 * This  is  commercial  software,  only  users  who have purchased a valid
 * license  and  accept  to the terms of the  License Agreement can install
 * and use this program.
 *
 * @package LiveChatX
 *
 */

if ( ! defined( 'ABSPATH' ) ) { exit; }

class LiveChatX_Options extends LiveChatX_Abstract  {

    function __construct() {

        $opts = LiveChatX()->opts();

        // Sanitize fields
        $this->addFilter( LCX_SLUG . '_options_validate', 'sanitize' );

        // Before field
        $this->addAction( LCX_SLUG . '_after_field', 'after_field', 1, 20 );

    }

    public static function plugin_page() {
        global $wpdb;
       ?>
        <div class="wrap">
            <img src="<?php echo LCX_URL; ?>/assets/img/logo-200x.png" alt="" class="lcx-icon">
            <h1><?php echo sprintf( __( '%s Options', 'lcx' ), LCX_NAME ); ?></h1>

            <p class="lcx-desc description"><strong class="lcx-edition"><?php echo LCX_EDITION; ?> Edition</strong> <span class="lcx-highlight"><?php echo LCX_VERSION; ?></span> &nbsp;-&nbsp; <a href="<?php echo LCX_CHANGELOG_URL; ?>" class="action-button" target="_blank">Changelog &raquo;</a></p>

            <?php
            // Output your settings form
            LiveChatX()->opts()->options();
            ?>
        </div>
        <?php
        
    }

     public function after_field( $field ) {
        /*if( $field['id'] == 'general_license_api' ) {
            $dogrula = $this->dogrula( $field['value'] );

            if( !empty( $dogrula->error ) ):?>
                <div class="lcx-ntf lcx-error clearfix">
                    <?php echo $dogrula->error; ?>
                </div>
                <script>
                    document.getElementById('general_license_api').classList.add('lcx-error');
                     <?php if( $dogrula->code == 666 ): ?>
                        jQuery(function() {jQuery(".nav-tab-wrapper a:not(:first-child)").remove();});
                    <?php endif; ?>
                </script>
            <?php
            endif;
        }*/
    }

    /**
     * Sanitize fields (Same as "sanitize_callback" - http://codex.wordpress.org/Function_Reference/register_setting).
     *
     * @link http://codex.wordpress.org/Function_Reference/register_setting
     */
    public static function sanitize( $input ) {

        // 
        // Update user capabilities
        // 
        if ( ! function_exists( 'get_editable_roles' ) ) {
            require_once ABSPATH . 'wp-admin/includes/user.php';
        }
        $roles = get_editable_roles();
        $lcx_caps = fn_lcx_get_capabilities();

        foreach( $roles as $role_name => $role_info ) {
            $role = get_role( $role_name );

            // First, remove all the plugin capabilities
            foreach( $lcx_caps as $capid => $name ) {
                $role->remove_cap( $capid );
            }

            // Add chosen capabilities
            if( !empty( $_POST['op_caps'][$role_name] ) ) {
                foreach( $_POST['op_caps'][$role_name] as $capid ) {
                    $role->add_cap( $capid );
                }
            }

            // In any case, keep admins to manage plugin options
            if( $role_name == 'administrator' ) {
                $role->add_cap( 'lcx_admin' );
            }
        }

        // 
        // Compile application CSS file
        //
        $compile_var = array(
            'primary' => $input['design_colors_primary'],
            'secondary' => $input['design_colors_secondary'],
            'highlightColor' => $input['design_colors_highlight'],
            'radius' => $input['design_ui_radius'] . 'px',
            'radiusBig' => $input['design_ui_radius_big'] . 'px',
            'popupW' => $input['design_ui_popup_width'] . 'px',
            'starterType' => $input['design_ui_starter_type'],
            'starterW' => $input['design_ui_starter_default_w'] . 'px',
            'starterW_mobile' => $input['design_ui_starter_mobile_w'] . 'px',
            'starterH' => $input['design_ui_starter_default_h'] . 'px',
            'offset' => $input['design_ui_offset'] . 'px',
            'position' => $input['design_ui_position']
        );
        fn_lcx_compile_app_css( $compile_var );

        return $input;

    }

    /**
     * Validate fields.
     */
    protected function validate() {

    }
}