<?php
/**
 * SCREETS © 2018
 *
 * SCREETS, d.o.o. Sarajevo. All rights reserved.
 * This  is  commercial  software,  only  users  who have purchased a valid
 * license  and  accept  to the terms of the  License Agreement can install
 * and use this program.
 *
 * @version 1.0
 *
 * @package LiveChatX
 * @author Gilbert Pellegrom, James Kemp
 * @link https://github.com/gilbitron/WordPress-Settings-Framework
 * @version 1.6.3
 * @license MIT
 *
 */

if ( ! defined( 'ABSPATH' ) ) { exit; }

/**
 * LCX_Options_FW class
 */
class LCX_Options_FW {

    /**
     * @access private
     * @var array
     */
    private $settings_wrapper;

    /**
     * @access private
     * @var array
     */
    private $settings;

    /**
     * @access private
     * @var array
     */
    private $tabs;

    /**
     * @access private
     * @var string
     */
    private $prefix;

    /**
     * @access private
     * @var array
     */
    private $settings_page = array();

    /**
     * @access private
     * @var str
     */
    private $options_path;

    /**
     * @access private
     * @var str
     */
    private $options_url;

    /**
     * @access protected
     * @var array
     */
    protected $setting_defaults = array(
        'id'           => 'default_field',
        'title'        => 'Default Field',
        'desc'        => '',
        'std'          => '',
        'type'         => 'text',
        'placeholder' => '',
        'choices'     => array(),
        'prefix'     => '',
        'suffix'     => '',
        'class'       => '',
        'atts'       => array(),
        'subfields'   => array()
    );

    /**
     * Constructor.
     *
     * @param string path to settings file
     * @param string "prefix"
     */
    public function __construct( $opts_file, $prefix ) {

        // Get options
        require $opts_file;

        $this->prefix = $prefix;
        $this->options_path = plugin_dir_path( __FILE__ );
        $this->options_url = plugin_dir_url( __FILE__ );

        $this->construct_options();

        if( is_admin() ) {

            global $pagenow;

            add_action( 'admin_init', array( $this, 'admin_init') );
            add_action( $this->prefix . '_do_options_sections', array( $this, 'do_tabless_options_sections'), 10 );

            if( $this->has_tabs() ) {

                add_action( $this->prefix . '_before_options', array( $this, 'tab_links' ) );
                remove_action( $this->prefix . '_do_options_sections', array( $this, 'do_tabless_options_sections'), 10 );
                add_action( $this->prefix . '_do_options_sections',    array( $this, 'do_tabbed_options_sections'), 10 );

            }

        }

    }

    /**
     * Construct Settings.
     *
     * @return array
     */
    public function construct_options() {

        $this->settings_wrapper = apply_filters( $this->prefix . '_register_options', array() );

        if( !is_array($this->settings_wrapper) ){
            return new WP_Error( 'broke', LCX_SNAME . ': Options must be an array' );
        }

        // If "sections" is set, this settings group probably has tabs
        if( isset( $this->settings_wrapper['sections'] ) ) {

            $this->tabs = (isset( $this->settings_wrapper['tabs'] )) ? $this->settings_wrapper['tabs'] : array();
            $this->settings = $this->settings_wrapper['sections'];

        // If not, it's probably just an array of settings
        } else {

            $this->settings = $this->settings_wrapper;

        }

        $this->settings_page['slug'] = sprintf( '%s-settings', str_replace('_', '-', $this->prefix ) );

    }

    /**
     * Get the option group for this instance
     *
     * @return string the "prefix"
     */
    public function get_prefix() {

        return $this->prefix;

    }

    /**
     * Registers the internal WordPress settings
     */
    public function admin_init() {

        register_setting( $this->prefix, $this->prefix .'_options', array( $this, 'settings_validate') );
        $this->process_options();

    }

    /**
     * Add Settings Page
     *
     * @param array $args
     */

    public function add_options_page( $args ) {

        $defaults = array(
            'parent_slug' => false,
            'page_slug'   => "",
            'page_title'  => "",
            'menu_title'  => "",
            'capability'  => 'manage_options'
        );

        $args = wp_parse_args( $args, $defaults );

        $this->settings_page['title'] = $args['page_title'];

        if( $args['parent_slug'] ) {

            add_submenu_page(
                $args['parent_slug'],
                $this->settings_page['title'],
                $args['menu_title'],
                $args['capability'],
                $this->settings_page['slug'],
                array( $this, 'settings_page_content' )
            );

        } else {

            add_menu_page(
                $this->settings_page['title'],
                $args['menu_title'],
                $args['capability'],
                $this->settings_page['slug'],
                array( $this, 'settings_page_content' )
            );

        }

    }

    /**
     * Settings Page Content
     */

    public function settings_page_content() {
        if ( !current_user_can( 'lcx_admin' ) ) {
            wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
        }
        ?>
        <div class="wrap">
            <div id="icon-options-general" class="icon32"></div>
            <h2><?php echo $this->settings_page['title']; ?></h2>
            <?php
            // Output your settings form
            $this->settings();
            ?>
        </div>
        <?php

    }

    /**
     * Displays any errors from the WordPress settings API
     */
    public function admin_notices() {

        settings_errors();

    }

    /**
     * Adds a filter for settings validation
     *
     * @param array the un-validated settings
     * @return array the validated settings
     */
    public function settings_validate( $input ) {

        return apply_filters( $this->prefix .'_options_validate', $input );

    }

    /**
     * Displays the "section_description" if specified in $this->settings
     *
     * @param array callback args from add_settings_section()
     */
    public function section_intro( $args ) {

        if(!empty($this->settings)){

            foreach($this->settings as $section){

                if($section['section_id'] == $args['id']){

                    if(isset($section['section_description']) && $section['section_description']) echo '<p class="lcx-section-description">'. $section['section_description'] .'</p>';
                    break;

                }

            }

        }

    }

    /**
     * Processes $this->settings and adds the sections and fields via the WordPress settings API
     */
    private function process_options() {

        if( !empty($this->settings) ){

            usort($this->settings, array( $this, 'sort_array'));

            foreach( $this->settings as $section ){

                if( isset($section['section_id']) && $section['section_id'] && isset($section['section_title']) ){

                    $page_name = ( $this->has_tabs() ) ? sprintf( '%s_%s', $this->prefix, $section['tab_id'] ) : $this->prefix;

                    add_settings_section( $section['section_id'], $section['section_title'], array( $this, 'section_intro'), $page_name );

                    if( isset($section['fields']) && is_array($section['fields']) && !empty($section['fields']) ){

                        foreach( $section['fields'] as $field ){

                            if( isset($field['id']) && $field['id'] && isset($field['title']) ){

                                $title = !empty( $field['subtitle'] ) ? sprintf('%s <span class="lcx-subtitle">%s</span>', $field['title'], $field['subtitle']) : $field['title'];

                                add_settings_field( $field['id'], $title, array( $this, 'generate_setting'), $page_name, $section['section_id'], array('section' => $section, 'field' => $field) );

                            }
                        }

                    }

                }

            }

        }

    }

    /**
     * Usort callback. Sorts $this->settings by "section_order"
     *
     * @param mixed section order a
     * @param mixed section order b
     * @return int order
     */
    public function sort_array( $a, $b ) {

        if( !isset($a['section_order']) )
            return;

        return $a['section_order'] > $b['section_order'];

    }

    /**
     * Generates the HTML output of the settings fields
     *
     * @param array callback args from add_settings_field()
     */
    public function generate_setting( $args ) {

        $section = $args['section'];
        $this->setting_defaults = apply_filters( $this->prefix . '_defaults_', $this->setting_defaults );

        $args = wp_parse_args( $args['field'], $this->setting_defaults );

        $options = get_option( $this->prefix .'_options' );

        $args['id'] = $this->has_tabs() ? sprintf( '%s_%s_%s', $section['tab_id'], $section['section_id'], $args['id'] ) : sprintf( '%s_%s', $section['section_id'], $args['id'] );
        $args['value'] = isset( $options[$args['id']] ) ? $options[$args['id']] : ( isset( $args['std'] ) ? $args['std'] : '' );
        $args['name'] = $this->generate_field_name( $args['id'] );

        do_action( $this->prefix . '_before_field', $args );
        do_action( $this->prefix . '_before_field_' . $args['id'] );

        $this->do_field_method( $args );

        do_action( $this->prefix . '_after_field', $args );
        do_action( $this->prefix . '_after_field_' . $args['id'] );

    }

    /**
     * Do field method, if it exists.
     *
     * @param str $type
     */
    public function do_field_method( $args ) {

        $generate_field_method = sprintf('generate_%s_field', $args['type']);

        if( method_exists($this, $generate_field_method) )
            $this->$generate_field_method( $args );

    }

    public function generate_prefix( $value ) {
        if( !empty( $value ) ) {
            echo "<span class='is-prefix'>$value</span>";
        }
    }

    public function generate_suffix( $value ) {
        if( !empty( $value ) ) {
            echo "<span class='is-suffix'>$value</span>";
        }
    }

    /**
     * Generate: Text field.
     *
     * @param arr $args
     */
    public function generate_text_field( $args ) {

        $args['value'] = esc_attr( stripslashes( $args['value'] ) );

        $this->generate_prefix( $args['prefix'] );

        echo '<input type="text" name="'. $args['name'] .'" id="'. $args['id'] .'" value="'. $args['value'] .'" placeholder="'. $args['placeholder'] .'" class="regular-text '. $args['class'] .'" ' . $this->get_attrs( $args['atts'] ) . ' />';

        $this->generate_suffix( $args['suffix'] );

        $this->generate_description( $args['desc'] );

    }

    /**
     * Generate: Number field
     *
     * @param arr $args
     */
    public function generate_number_field( $args ) {

        $args['value'] = esc_attr( stripslashes( $args['value'] ) );

        $this->generate_prefix( $args['prefix'] );

        $step = $min = $max = '';

        if( isset( $args['step'] ) ) {
            $step = ' step="' . $args['step'] . '"';
        }

        if( isset( $args['min'] ) ) {
            $min = ' min="' . $args['min'] . '"';
        }

        if( isset( $args['max'] ) ) {
            $max = ' max="' . $args['max'] . '"';
        }

        echo '<input type="number" name="'. $args['name'] .'" id="'. $args['id'] .'" value="'. $args['value'] .'" placeholder="'. $args['placeholder'] .'" class="regular-text '. $args['class'] .'" ' . $this->get_attrs( $args['atts'] ) . $step . $min . $max . ' />';

        $this->generate_suffix( $args['suffix'] );

        $this->generate_description( $args['desc'] );

    }

    /**
     * Generate: Time field
     *
     * @param arr $args
     */
    public function generate_time_field( $args ) {

        $args['value'] = esc_attr( stripslashes( $args['value'] ) );

        $this->generate_prefix( $args['prefix'] );

        echo '<input name="'. $args['name'] .'" id="'. $args['id'] .'" value="'. $args['value'] .'" class="timepicker regular-text '. $args['class'] .'" data-timepicker="'.htmlentities( json_encode( $args['timepicker'] ) ).'" />';

        $this->generate_suffix( $args['suffix'] );

        $this->generate_description( $args['desc'] );

    }

    /**
     * Generate: Date field
     *
     * @param arr $args
     */
    public function generate_date_field( $args ) {

        $args['value'] = esc_attr( stripslashes( $args['value'] ) );

        $this->generate_prefix( $args['prefix'] );

        echo '<input name="'. $args['name'] .'" id="'. $args['id'] .'" value="'. $args['value'] .'" class="datepicker regular-text '. $args['class'] .'" data-datepicker="'.htmlentities( json_encode( $args['datepicker'] ) ).'" />';

        $this->generate_suffix( $args['suffix'] );

        $this->generate_description( $args['desc'] );

    }

    /**
     * Generate: Group field
     *
     * Generates a table of subfields, and a javascript template for create new repeatable rows.
     *
     * @param arr $args
     */
    public function generate_group_field( $args ) {

        $row_count = count( $args['value'] );

        echo '<table class="widefat lcx-group" cellspacing="0">';

            echo "<tbody>";

                for ($row = 0; $row < $row_count; $row++) {

                    echo $this->generate_group_row_template( $args, false, $row );

                }

            echo "</tbody>";

        echo "</table>";

        printf('<script type="text/html" id="%s_template">%s</script>', $args['id'], $this->generate_group_row_template( $args, true ));

        $this->generate_description( $args['desc'] );

    }

    /**
     * Generate group row template
     *
     * @param arr $args Field arguments
     * @param bool $blank Blank values
     * @param int $row Iterator
     * @return str|bool
     */
    public function generate_group_row_template( $args, $blank = false, $row = 0 ) {

        $row_template = false;

        if( $args['subfields'] ) {

            $row_class = $row%2 == 0 ? "alternate" : "";

            $row_template .= sprintf('<tr class="lcx-group__row %s">', $row_class);

                $row_template .= sprintf('<td class="lcx-group__row-index"><span>%d</span></td>', $row);

                $row_template .= '<td class="lcx-group__row-fields">';

                    foreach( $args['subfields'] as $subfield ) {

                        $subfield = wp_parse_args( $subfield, $this->setting_defaults );

                        $subfield['value'] = ( $blank ) ? "" : isset( $args['value'][$row][$subfield['id']] ) ? $args['value'][$row][$subfield['id']] : "";
                        $subfield['name'] = sprintf('%s[%d][%s]', $args['name'], $row, $subfield['id']);
                        $subfield['id'] = sprintf('%s_%d_%s', $args['id'], $row, $subfield['id']);

                        $row_template .= '<div class="lcx-group__field-wrapper">';

                            $row_template .= sprintf('<label for="%s" class="lcx-group__field-label">%s</label>', $subfield['id'], $subfield['title']);

                            ob_start();
                            $this->do_field_method( $subfield );
                            $row_template .= ob_get_clean();

                        $row_template .= '</div>';

                    }

                $row_template .= "</td>";

                $row_template .= '<td class="lcx-group__row-actions">';

                    $row_template .= sprintf('<a href="javascript: void(0);" class="lcx-group__row-add" data-template="%s_template"><span class="dashicons dashicons-plus-alt"></span></a>', $args['id']);
                    $row_template .= '<a href="javascript: void(0);" class="lcx-group__row-remove"><span class="dashicons dashicons-trash"></span></a>';

                $row_template .= "</td>";

            $row_template .= '</tr>';

        }

        return $row_template;

    }

    /**
     * Generate: Select field
     *
     * @param arr $args
     */
    public function generate_select_field( $args ) {

        $args['value'] = esc_html( esc_attr( $args['value'] ) );

        echo '<select name="'. $args['name'] .'" id="'. $args['id'] .'" class="'. $args['class'] .'" ' . $this->get_attrs( $args['atts'] ) . '>';

            foreach($args['choices'] as $value => $text ){

                $selected = $value == $args['value'] ? 'selected="selected"' : '';

                echo sprintf('<option value="%s" %s>%s</option>', $value, $selected, $text);

            }

        echo '</select>';

        $this->generate_description( $args['desc'] );

    }

    /**
     * Generate: Password field
     *
     * @param arr $args
     */
    public function generate_password_field( $args ) {

        $args['value'] = esc_attr( stripslashes( $args['value'] ) );

        echo '<input type="password" name="'. $args['name'] .'" id="'. $args['id'] .'" value="'. $args['value'] .'" placeholder="'. $args['placeholder'] .'" class="regular-text '. $args['class'] .'" ' . $this->get_attrs( $args['atts'] ) . ' />';

        $this->generate_description( $args['desc'] );

    }

    /**
     * Generate: Textarea field
     *
     * @param arr $args
     */
    public function generate_textarea_field( $args ) {

        $args['value'] = esc_html( esc_attr( $args['value'] ) );

        echo '<textarea name="'. $args['name'] .'" id="'. $args['id'] .'" placeholder="'. $args['placeholder'] .'" rows="5" cols="60" class="'. $args['class'] .'" ' . $this->get_attrs( $args['atts'] ) . '>'. $args['value'] .'</textarea>';

        $this->generate_description( $args['desc'] );

    }

    /**
     * Generate: Radio field
     *
     * @param arr $args
     */
    public function generate_radio_field( $args ) {

        $this->generate_description( $args['desc'] );

        $args['value'] = esc_html( esc_attr( $args['value'] ) );

        foreach( $args['choices'] as $value => $text ){

            $field_id = sprintf('%s_%s', $args['id'], $value);
            $checked = $value == $args['value'] ? 'checked="checked"' : '';

            echo sprintf( '<label><input type="radio" name="%s" id="%s" value="%s" class="%s" %s> %s</label><br />', $args['name'], $args['id'],  $value, $args['class'], $checked, $text );

        }

    }

    /**
     * Generate: Pages field.
     *
     * @param arr $args
     */
    public function generate_pages_field( $args ) {

        $pages = get_pages();

        $args['value'] = esc_html( esc_attr( $args['value'] ) );

        echo '<select name="'. $args['name'] .'" id="'. $args['id'] .'" class="'. $args['class'] .'" ' . $this->get_attrs( $args['atts'] ) . '>';

            if( !empty( $args['std'] ) ) {
                echo '<option value="0">' . $args['std'] . '</option>';
            }

            if( !empty( $pages ) ) {
                foreach( $pages as $page ) {

                    $selected = $page->ID == $args['value'] ? 'selected="selected"' : '';

                    echo sprintf( '<option value="%s" %s>%s</option>', $page->ID, $selected, $page->post_title );

                }
            }

        echo '</select>';

        $this->generate_description( $args['desc'] );

    }

    /**
     * Generate: Multiple pages field.
     *
     * @param arr $args
     */
    public function generate_multi_pages_field( $args ) {

        $pages = get_pages();

        $args['choices'] = array();

        if( !empty( $pages ) ) {
            foreach( $pages as $page ) {
                $args['choices'][ $page->ID] = $page->post_title;
            }
        }

        $this->generate_checkboxes_field( $args );

        // echo '<select name="'. $args['name'] .'" id="'. $args['id'] .'" class="'. $args['class'] .'" ' . $this->get_attrs( $args['atts'] ) . '>';

        /*if( !empty( $args['std'] ) ) {
            echo '<input type="checkbox" value="0">' . $args['std'] . '</option>';
        }*/

    }

    

    /**
     * Generate: Checkbox field
     *
     * @param arr $args
     */
    public function generate_checkbox_field( $args ) {

        $args['value'] = esc_attr( stripslashes( $args['value'] ) );
        $checked = $args['value'] ? 'checked="checked"' : '';

        echo '<input type="hidden" name="'. $args['name'] .'" value="0" />';
        echo '<label><input type="checkbox" name="'. $args['name'] .'" id="'. $args['id'] .'" value="1" class="'. $args['class'] .'" '.$checked.' ' . $this->get_attrs( $args['atts'] ) . '> '. $args['desc'] .'</label>';

    }

    /**
     * Generate: Checkboxes field.
     *
     * @param arr $args
     */
    public function generate_checkboxes_field( $args ) {


        $this->generate_description( $args['desc'] );

        echo '<input type="hidden" name="'. $args['name'] .'" value="0" />';

        foreach($args['choices'] as $value => $text) {

            $checked = is_array( $args['value'] ) && in_array($value, $args['value'])  ? 'checked="checked"' : '';
            $readonly = in_array($value, @$args['req'])  ? ' onclick="return false;"' : '';
            $field_id = sprintf('%s_%s', $args['id'], $value);
            $style = '';

            if( !empty( $readonly ) ) {
                $style = 'opacity: .5;';
            }

            echo sprintf('<label style="%s"><input type="checkbox" name="%s[]" id="%s" value="%s" class="%s" %s> %s</label><br />', $style, $args['name'],  $field_id, $value, $args['class'], $checked . $readonly, $text);

        }
    }

    /**
     * Generate: Color field.
     *
     * @param arr $args
     */
    public function generate_color_field( $args ) {

        $color_picker_id = sprintf('%s_cp', $args['id']);
        $args['value'] = esc_attr( stripslashes( $args['value'] ) );

        echo '<div style="position:relative;">';

            echo sprintf('<input type="text" name="%s" id="%s" value="%s" class="%s lcx-color" ' . $this->get_attrs( $args['atts'] ) . '>', $args['name'], $args['id'], $args['value'], $args['class']);

            echo sprintf('<div id="%s" style="position:absolute;top:0;left:190px;background:#fff;z-index:9999;"></div>', $color_picker_id);

            $this->generate_description( $args['desc'] );

        echo '</div>';

    }

    /**
     * Generate: File field
     *
     * @param arr $args
     */
    public function generate_file_field( $args ) {

        $args['value'] = esc_attr( $args['value'] );
        $button_id = sprintf('%s_button', $args['id']);

        echo sprintf('<input type="text" name="%s" id="%s" value="%s" class="regular-text %s"> ', $args['name'], $args['id'], $args['value'], $args['class']);

        echo sprintf('<input type="button" class="button lcx-browse" id="%s" value="' . __( 'Select', 'lcx' ) . '" data-id="%s" />', $button_id, $args['id']);

    }

    /**
     * Generate: Editor field
     *
     * @param arr $args
     */
    public function generate_editor_field( $args ) {

        wp_editor( $args['value'], $args['id'], array( 'textarea_name' => $args['name'] ) );

        $this->generate_description( $args['desc'] );

    }

    /**
     * Generate: Custom field
     *
     * @param arr $args
     */
    public function generate_custom_field( $args ) {

        echo $args['std'];

    }

    /**
     * Generate: Snippet field
     *
     * @param arr $args
     */
    public function generate_snippet_field( $args ) {

        include_once LCX_PATH . '/core/functions/widget.php';

        echo '<strong>WordPress site:</strong>';
        echo '<pre>' . htmlspecialchars( fn_lcx_get_snippet( false ) ) . '</pre>';

        echo '<hr><strong>Outside of your WordPress:</strong>';
        echo '<pre>' . htmlspecialchars( fn_lcx_get_snippet( true ) ) . '</pre>';

    }

    /**
     * Generate: Multi Inputs field
     *
     * @param arr $args
     */
    public function generate_multiinputs_field( $args ) {

        $field_titles = array_keys( $args['std'] );
        $values = array_values( $args['value'] );

        echo '<div class="lcx-multifields">';

            $i = 0; while($i < count($values)):

                $field_id = sprintf('%s_%s', $args['id'], $i);
                $value = esc_attr( stripslashes( $values[$i] ) );

                echo '<div class="lcx-multifields__field">';
                    echo '<input type="text" name="'. $args['name'] .'[]" id="'. $field_id .'" value="'. $value .'" class="regular-text '. $args['class'] .'" placeholder="'. $args['placeholder'] .'" />';
                    echo '<br><span>'.$field_titles[$i].'</span>';
                echo '</div>';

            $i++; endwhile;

        echo '</div>';

        $this->generate_description( $args['desc'] );

    }

    /**
     * Generate: Autocomplete field.
     *
     * @param arr $args
     */
    public function generate_autocomplete_field( $args ) {

        $args['value'] = esc_attr( stripslashes( $args['value'] ) );
        $choices = implode( '\', \'', $args['choices'] );

        echo '<input type="text" name="'. $args['name'] .'" id="'. $args['id'] .'" value="'. $args['value'] .'" placeholder="'. $args['placeholder'] .'" class="regular-text '. $args['class'] .'" ' . $this->get_attrs( $args['atts'] ) . ' />';

        echo <<< EOT
            <script>
                jQuery(document).ready( function($) {
                    var choices = [ '{$choices}' ];

                    function split( val ) {
                      return val.split( /,\s*/ );
                    }
                    function extractLast( term ) {
                      return split( term ).pop();
                    }
                 
                    $( "#{$args['id']}" )
                      // don't navigate away from the field on tab when selecting an item
                      .on( "keydown", function( event ) {
                        if ( event.keyCode === $.ui.keyCode.TAB &&
                            $( this ).autocomplete( "instance" ).menu.active ) {
                          event.preventDefault();
                        }
                      })
                      .autocomplete({
                        minLength: 0,
                        source: function( request, response ) {
                          // delegate back to autocomplete, but extract the last term
                          response( $.ui.autocomplete.filter(
                            choices, extractLast( request.term ) ) );
                        },
                        focus: function() {
                          // prevent value inserted on focus
                          return false;
                        },
                        select: function( event, ui ) {
                          var terms = split( this.value );
                          // remove the current input
                          terms.pop();
                          // add the selected item
                          terms.push( ui.item.value );
                          // add placeholder to get the comma-and-space at the end
                          terms.push( "" );
                          this.value = terms.join( ", " );
                          return false;
                        }
                      });
                });
            </script>
EOT;

        $this->generate_description( $args['desc'] );

    }


    /**
     * Generate: Field ID
     *
     * @param mixed $id
     */
    public function generate_field_name( $id ) {

        return sprintf('%s_options[%s]', $this->prefix, $id);

    }

    /**
     * Generate: Description
     *
     * @param mixed $description
     */
    public function generate_description( $description ) {

        if( $description && $description !== "" ) echo '<p class="description">'. $description .'</p>';

    }

    /**
     * Get attributes.
     *
     * @param array $atts
     */
    public function get_attrs( $atts ) {
        $output = '';

        if( !empty( $atts ) ) {
            foreach( $atts as $k => $v ) {
                $output .= "$k = \"$v\" ";
            }
        }

        return $output;
    }

    /**
     * Output the settings form
     */
    public function settings() {

        do_action( $this->prefix . '_before_options' );
        ?>
        <form action="options.php" method="post">
            <?php do_action( $this->prefix . '_before_options_fields' ); ?>
            <?php settings_fields( $this->prefix ); ?>

            <?php do_action( $this->prefix . '_do_options_sections' ); ?>

            <p class="submit"><input type="submit" class="button-primary" value="<?php _e( 'Save Changes' ); ?>" /></p>
        </form>
        <?php
        do_action( $this->prefix . '_after_options'  );

    }

    /**
     * Helper: Get Settings.
     *
     * @return arr
     */
    public function get_options() {

        $saved_options = get_option($this->prefix.'_options');

        $settings = array();

        foreach($this->settings as $section){
            foreach($section['fields'] as $field){

                if( !empty( $field['std'] ) && is_array( $field['std'] ) ) {
                    $field['std'] = array_values( $field['std'] );
                }

                $setting_key = $this->has_tabs() ? sprintf('%s_%s_%s', $section['tab_id'], $section['section_id'], $field['id']) : sprintf('%s_%s', $section['section_id'], $field['id']);

                if( isset( $saved_options[$setting_key] ) ) {
                    $settings[ $setting_key ] = $saved_options[$setting_key];
                } else {
                    $settings[ $setting_key ] = (isset($field['std'])) ? $field['std'] : false;
                }
            }
        }

        return $settings;

    }

    /**
     * Tabless Settings sections.
     */

    public function do_tabless_options_sections() {
        ?>
        <div class="lcx-section lcx-tabless">
            <?php do_settings_sections( $this->prefix ); ?>
        </div>
        <?php
    }

    /**
     * Tabbed Settings sections
     */

    public function do_tabbed_options_sections() {

        $i = 0;
        foreach ( $this->tabs as $tab_data ) {
            ?>
            <div id="tab-<?php echo $tab_data['id']; ?>" class="lcx-section lcx-tab lcx-tab--<?php echo $tab_data['id']; ?> <?php if($i == 0) echo 'lcx-tab--active'; ?>">
                <div class="postbox">
                    <?php do_settings_sections( sprintf( '%s_%s', $this->prefix, $tab_data['id'] ) ); ?>
                </div>
            </div>
            <?php
            $i++;
        }

    }

    /**
     * Output the tab links
     */
    public function tab_links() {

        do_action( $this->prefix . '_before_tab_links' );
        ?>
        <h2 class="nav-tab-wrapper">
            <?php
            $i = 0;
            foreach ( $this->tabs as $tab_data ) {
                $active = $i == 0 ? 'nav-tab-active' : '';
                ?>
                <a class="nav-tab lcx-tab-link <?php echo $active; ?>" href="#tab-<?php echo $tab_data['id']; ?>"><?php echo $tab_data['title']; ?></a>
                <?php
            $i++;
            }
            ?>
        </h2>
        <?php
        do_action( $this->prefix . '_after_tab_links' );

    }

    /**
     * Check if this settings instance has tabs
     */
    public function has_tabs() {

        if( !empty( $this->tabs ) )
            return true;

        return false;

    }

}