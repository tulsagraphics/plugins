<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <base target="_parent">

    <style>
        <?php
        // CSS files
        if( !empty( $cssFiles ) ) {
            foreach( $cssFiles as $path ) {
                echo file_get_contents( $path );
            }
        }
        ?>
    </style>

</head>
<body class="lcx">
    
    <div class="lcx-widget">
        <?php
            /**
             * Action for before widget body content.
             */
            do_action( 'lcx_skin_before_widget_body' );
        ?>