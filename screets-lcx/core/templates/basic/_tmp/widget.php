<?php if ( ! defined( 'ABSPATH' ) ) { exit; }

// Get the header
include 'widget-header.php'; 

// Get SVG icons
include 'icons.php'; 

// Get starter
include 'starter.php';

// Get popup pages
include 'popups.php';

// Get the footer
include 'widget-footer.php';

?>