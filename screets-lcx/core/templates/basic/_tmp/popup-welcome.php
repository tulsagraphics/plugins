<?php
if ( ! defined( 'ABSPATH' ) ) { exit; } ?>

<div class="lcx-content">
	<?php 
	/**
	 * Before welcome page actions.
	 */
	do_action( 'lcx_skin_before_welcome_page' );
	?>
	
	<div id="lcx-welcome-page-header" class="lcx-page-header">
		
		<?php 
		/**
		 * Before welcome header actions.
		 */
		do_action( 'lcx_skin_before_welcome_header' );
		?>
		

		<div class="lcx-response-info">

			<?php 
			/**
			 * Before welcome response info actions.
			 */
			do_action( 'lcx_skin_before_welcome_response_info' );
			?>
			
			<div class="lcx-response-welcome lcx__ lcx-active" data-type="welcome">
				<!-- Featured Operators -->
				<?php if( !empty( $featuredOPs ) ): ?>
					<div class="lcx-ops">
						<?php foreach( $featuredOPs as $op ): ?>
							<div class="lcx-op"><img src="<?php echo $op['avatar']; ?>" alt=""></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
				
				<!-- Response time info -->
				<?php if( !empty( $responseTime['online'] ) ): ?>
					<div class="lcx-response-time">
						<?php echo $responseTime['online']; ?>.
					</div>
				<?php endif; ?>

				<!-- Welcome message -->
				<div class="lcx-desc lcx-welcome-msg">
					<?php echo wpautop( $msgs['popup_welcome_msg'] ); ?>
				</div>

			</div>

			<?php 
			/**
			 * After welcome response info actions.
			 */
			do_action( 'lcx_skin_after_welcome_response_info' );
			?>

		</div>

		<?php
		/**
		 * After welcome header actions.
		 */
		do_action( 'lcx_skin_after_welcome_header' );
		?>

	</div>
	
	<div id="lcx-welcome-reply-wrap" class="lcx-page-content lcx-welcome-reply-wrap lcx-reply-wrap">
		<?php 
		/**
		 * Before welcome reply box actions.
		 */
		do_action( 'lcx_skin_before_welcome_reply' );
		?>

		<textarea name="msg" id="lcx-welcome-reply" class="lcx-reply lcx-welcome-reply" placeholder="<?php echo $msgs['forms_reply']; ?>"></textarea>

		<?php
		/**
		 * After welcome reply box actions.
		 */
		do_action( 'lcx_skin_after_welcome_reply' );
		?>
	</div>

	
	<div id="lcx-welcome-send-btn" class="lcx-page-content lcx-welcome-send-btn-wrap lcx-send-btn-wrap">
		<?php 
		/**
		 * Before welcome reply box actions.
		 */
		do_action( 'lcx_skin_before_welcome_reply' );
		?>

		<a href="#" id="lcx-welcome-send-btn" class="lcx-send-btn lcx-welcome-send-btn lcx-btn lcx-primary"><?php echo $msgs['forms_send_msg']; ?></a>

		<?php
		/**
		 * After welcome reply box actions.
		 */
		do_action( 'lcx_skin_after_welcome_reply' );
		?>
	</div>


	
	<?php 
	/**
	 * After welcome page actions.
	 */
	do_action( 'lcx_skin_after_welcome_page' );
	?>
	
</div>

