<?php if ( ! defined( 'ABSPATH' ) ) { exit; } ?>

<div class="lcx-field lcx-field-<?php echo $type; ?> lcx-field-<?php echo $name; ?>" data-form-id="<?php echo $form_id; ?>">
	
	<?php include 'field-label.php'; ?>
	
	<div class="lcx-section lcx-group">
			<div class="lcx-col lcx-span_2_of_4">
				<span class="lcx-sublabel">Your name</span>
				<input type="text" name="<?php echo $name; ?>[name]" placeholder="Your name">
			</div>
			<div class="lcx-col lcx-span_2_of_4">
				<span class="lcx-sublabel">Email address</span>
				<input type="email" name="<?php echo $name; ?>[email]" placeholder="Email address">
			</div>
	</div>

	<?php include 'field-footnote.php'; ?>
	
</div>