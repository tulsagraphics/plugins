<?php if ( ! defined( 'ABSPATH' ) ) { exit; } ?>

<div class="lcx-field lcx-field-<?php echo $type; ?> lcx-field-<?php echo $name; ?>" data-form-id="<?php echo $form_id; ?>">
	
	<?php include 'field-label.php'; ?>
	
	<input type="email" name="<?php echo $name; ?>" placeholder="<?php echo ( !empty( $placeholder ) ) ? $placeholder : $label; ?>">

	<?php include 'field-footnote.php'; ?>

</div>