<?php if ( ! defined( 'ABSPATH' ) ) { exit; } ?>

<div class="lcx-field lcx-field-<?php echo $type; ?> lcx-field-<?php echo $name; ?>" data-form-id="<?php echo $form_id; ?>">
	
	<?php include 'field-label.php'; ?>
	
	<textarea name="<?php echo $name; ?>" placeholder="<?php echo ( !empty( $placeholder ) ) ? $placeholder : $label; ?>"></textarea>

	<?php include 'field-footnote.php'; ?>

</div>