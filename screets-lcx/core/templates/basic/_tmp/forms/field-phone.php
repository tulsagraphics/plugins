<?php if ( ! defined( 'ABSPATH' ) ) { exit; } ?>

<div class="lcx-field lcx-field-<?php echo $type; ?> lcx-field-<?php echo $name; ?>" data-form-id="<?php echo $form_id; ?>">
	
	<?php include 'field-label.php'; ?>
	

    <div class="lcx-section lcx-group">
            <div class="lcx-col lcx-span_2_of_4">
               <div class="lcx-select">
                    <select name="<?php echo $name; ?>[country]">
                       <?php echo fn_lcx_get_countries_list(); ?>
                   </select>
               </div>
            </div>
            <div class="lcx-col lcx-span_2_of_4">
                <input type="text" name="<?php echo $name; ?>[tel]" placeholder="<?php echo ( !empty( $placeholder ) ) ? $placeholder : $label; ?>">
            </div>
    </div>

	<?php include 'field-footnote.php'; ?>

</div>