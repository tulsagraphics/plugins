<?php if ( ! defined( 'ABSPATH' ) ) { exit; } ?>

<?php
// Get support categories
$support_cats = fn_lcx_get_support_cats( $form_id );

if( !empty( $support_cats ) ): ?>

<div class="lcx-field lcx-field-<?php echo $type; ?> lcx-field-<?php echo $name; ?>" data-form-id="<?php echo $form_id; ?>">
	
	<?php include 'field-label.php'; ?>
	
	<div class="lcx-select">
        <select name="<?php echo $name; ?>">
            <?php foreach( $support_cats as $cat ): ?>
                <option value="<?php echo $cat['ID']; ?>"><?php echo $cat['name']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>

	<?php include 'field-footnote.php'; ?>

</div>

<?php endif; ?>