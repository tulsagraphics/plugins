<?php if ( ! defined( 'ABSPATH' ) ) { exit; } ?>

	<?php if( !empty( $label ) ): ?>
		<div class="lcx-label">
			<?php echo $label; ?>

			<?php if( !empty( $desc ) ): ?>
				<span><?php echo $desc; ?></span>
			<?php endif; ?>
		</div>
	<?php endif; ?>