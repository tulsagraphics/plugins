<?php
if ( ! defined( 'ABSPATH' ) ) { exit; } ?>
	
	<?php 
	/**
	 * After page actions.
	 */
	do_action( "lcx_skin_after_popup_page_{$page_name}" );
	?>

</div>
