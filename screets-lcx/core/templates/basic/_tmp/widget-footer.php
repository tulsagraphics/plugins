	
		<?php
		/**
		 * Action for after widget body content.
		 */
		do_action( 'lcx_skin_after_widget_body' );
		?>
		
	</div>
	
	<?php
	// Scripts
	if( !empty( $jsFiles ) ) {
		foreach( $jsFiles as $path ) {
			echo '<script src="' . esc_url( $path ) . "\"></script>\n\t";
		}
	}
	?>

</body>
</html>