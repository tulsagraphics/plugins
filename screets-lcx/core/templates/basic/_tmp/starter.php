<?php
if ( ! defined( 'ABSPATH' ) ) { exit; } ?>

<!-- Starter -->
<a id="lcx-starter" class="lcx-starter __lcx">
	
	<span class="lcx-starter-open">
		
		<?php 
		/**
		 * Actions before starter.
		 */
		do_action( 'lcx_skin_before_starter' );
		?>

		<span id="lcx-starter-prefix" class="lcx-prefix lcx-starter-prefix">
			<?php echo apply_filters( 'lcx_skin_starter_prefix', '<svg class="lcx-ico lcx-ico-chat"><use xlink:href="#lcx-ico-chat"></use></svg>' ); ?>
		</span>
		
		<span id="lcx-starter-title" class="lcx-title lcx-starter-title">
			<?php echo apply_filters( 'lcx_skin_starter_title', $msgs['btn_title'] ); ?>
		</span>
		
		<span id="lcx-starter-suffix" class="lcx-suffix lcx-starter-suffix">
			<?php echo apply_filters( 'lcx_skin_starter_suffix', '' ); ?>
		</span>

		<?php 
		/**
		 * Action after closer.
		 */
		do_action( 'lcx_skin_after_closer' );
		?>

	</span>

	<span class="lcx-starter-close">
		<?php 
		/**
		 * Actions before closer.
		 */
		do_action( 'lcx_skin_before_closer' );
		?>

		<span id="lcx-closer-prefix" class="lcx-prefix lcx-closer-prefix">
			<?php echo apply_filters( 'lcx_skin_closer_prefix', '<svg class="lcx-ico lcx-ico-close"><use xlink:href="#lcx-ico-close"></use></svg>' ); ?>
		</span>

		<span id="lcx-closer-title" class="lcx-title lcx-closer-title">
			<?php echo apply_filters( 'lcx_skin_closer_icon', '' ); ?>
		</span>

		<span id="lcx-closer-suffix" class="lcx-suffix lcx-closer-suffix">
			<?php echo apply_filters( 'lcx_skin_closer_suffix', '' ); ?>
		</span>

		<?php 
		/**
		 * Action after closer.
		 */
		do_action( 'lcx_skin_after_closer' );
		?>
	</span>
</a>
