<?php
if ( ! defined( 'ABSPATH' ) ) { exit; } ?>

<div class="lcx-content">
	<?php 
	/**
	 * Before online page actions.
	 */
	do_action( 'lcx_skin_before_online_page' );
	?>
	
	<div id="lcx-online-page-header" class="lcx-page-header lcx-calc">
		
		<?php 
		/**
		 * Before online header actions.
		 */
		do_action( 'lcx_skin_before_online_header' );
		?>

		<div class="lcx-response-info">
			<?php 
			/**
			 * Before online response info actions.
			 */
			do_action( 'lcx_skin_before_online_response_info' );
			?>
			
			<div class="lcx-response-online lcx__ lcx-active" data-type="online">
				<!-- Featured Operators -->
				<?php if( !empty( $featuredOPs ) ): ?>
					<div class="lcx-ops">
						<?php foreach( $featuredOPs as $op ): ?>
							<div class="lcx-op"><img src="<?php echo $op['avatar']; ?>" alt=""></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
				
				<!-- Response time info -->
				<?php if( !empty( $responseTime['online'] ) ): ?>
					<div class="lcx-response-time">
						<?php echo $responseTime['online']; ?>.
					</div>
				<?php endif; ?>

			</div>

			<?php 
			/**
			 * After online response info actions.
			 */
			do_action( 'lcx_skin_after_online_response_info' );
			?>

		</div>
			

		<?php 
		/**
		 * After online header actions.
		 */
		do_action( 'lcx_skin_after_online_header' );
		?>

	</div>

	<div id="lcx-online-page-content" class="lcx-cnv lcx-page-content lcx-calc">
		<div class="lcx-wrap">
			<?php 
			/**
			 * Before conversation actions.
			 */
			do_action( 'lcx_skin_before_cnv', 'online' );
			?>
			<div id="lcx-msgs" class="lcx-msgs">
				<!-- <li class="lcx-section lcx-col">
					<div class="lcx-msg">
						<div class="lcx-author">screets</div>
						<div class="lcx-meta"><div class="lcx-time">12:30</div></div>
						<div class="lcx-content">Lorem ipsum dolor sit amet, mea ut fabulas democritum, ad vero dolor congue vis. Ad dicit quaeque concludaturque ius, no cum <a href="">blandit moderatius</a>, no augue primis erroribus ius. Ut eum error latine delicatissimi. Ius an nostrud alterum. Cu tation fabulas has. <br><br> eu cum periculis deseruisse instructior, sea cu novum tritani sanctus. <a href="">https://www.google.ba/search?q=code&rlz=1C5CHFA_enBA728BA728&oq=code+&aqs=chrome..69i57j69i60l3j69i65l2.869j0j4&sourceid=chrome&ie=UTF-8</a> Ut mei error aperiri invidunt, mel no diam expetendis but this isit.</div>
					</div>
				</li>
				<li class="lcx-section lcx-col">
					<div class="lcx-msg lcx-me">
						<div class="lcx-author">Jamie Lannister</div>
						<div class="lcx-meta"><div class="lcx-time">12:29</div></div>
						<div class="lcx-content">No augue primis erroribus ius. Ut eum error latine delicatissimi. Ius an nostrud alterum. Cu tation fabulas has. <br><br> eu cum periculis deseruisse instructior, sea cu novum tritani sanctus. <a href="">https://www.google.ba/search?q=code&rlz=1C5CHFA_enBA728BA728&oq=code+&aqs=chrome..69i57j69i60l3j69i65l2.869j0j4&sourceid=chrome&ie=UTF-8</a> Ut mei error aperiri invidunt, mel no diam expetendis but this isit.</div>
					</div>
				</li>

				<li class="lcx-section lcx-col">
					<div class="lcx-msg">
						<div class="lcx-author">screets</div>
						<div class="lcx-meta"><div class="lcx-time">12:30</div></div>
						<div class="lcx-content">haha thanks :D</div>
					</div>
				</li>

				<li class="lcx-section lcx-col">
					<div class="lcx-msg lcx-loader"><div class="lcx-content">Typing...</div></div>
				</li> -->
			</div>
				
			<?php 
			/**
			 * After conversation actions.
			 */
			do_action( 'lcx_skin_after_cnv', 'online' );
			?>

			<div id="lcx-ntf-online" class="lcx-ntf lcx-ntf-online lcx-info __lcx">
				<?php 
				/**
				 * Before online notification actions.
				 */
				do_action( 'lcx_skin_before_online_ntf' );
				?>

				<div class="lcx-content"></div>

				<?php 
				/**
				 * After online notification actions.
				 */
				do_action( 'lcx_skin_after_online_ntf' );
				?>
			</div>
		</div>
	</div>
	
	<div id="lcx-online-reply-wrap" class="lcx-reply-wrap lcx-online-reply-wrap lcx-page-xtra lcx-calc">
		<?php 
		/**
		 * Before online reply box actions.
		 */
		do_action( 'lcx_skin_before_online_reply' );
		?>

		<textarea name="msg" id="lcx-online-reply" class="lcx-reply lcx-online-reply" placeholder="<?php echo $msgs['forms_reply']; ?>"></textarea>


		<?php
		/**
		 * After online reply box actions.
		 */
		do_action( 'lcx_skin_after_online_reply' );
		?>
	</div>
	
	<?php 
	/**
	 * After online page actions.
	 */
	do_action( 'lcx_skin_after_online_page' );
	?>
	
</div>

