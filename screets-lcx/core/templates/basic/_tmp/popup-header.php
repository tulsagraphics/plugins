<?php
if ( ! defined( 'ABSPATH' ) ) { exit; } ?>

<!-- <?php echo ucfirst( $page_name ); ?> popup -->
<div id="lcx-page-<?php echo $page_name; ?>" class="lcx-page lcx-page-<?php echo $page_name; ?> __lcx">
	<?php 
	/**
	 * Before page actions.
	 */
	do_action( "lcx_skin_before_popup_page_{$page_name}" );
	?>

	<div class="lcx-popup-header">
		<div class="lcx-prefix">
			<a href="#" id="lcx-go-menu" class="lcx-go-menu lcx-action" data-type="goPage" data-val="menu">
				<svg class="lcx-ico lcx-ico-menu"><use xlink:href="#lcx-ico-menu"></use></svg>
			</a>
		</div>
		<div class="lcx-title lcx-closable"><?php echo $msgs['popup_header_popup']; ?></div>
		<div class="lcx-suffix lcx-closable">
			<svg class="lcx-ico lcx-ico-menu"><use xlink:href="#lcx-ico-down"></use></svg>
		</div>
	</div>
