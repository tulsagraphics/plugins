<?php
if ( ! defined( 'ABSPATH' ) ) { exit; } ?>


<div class="lcx-content">
	<?php 
	/**
	 * Before menu page actions.
	 */
	do_action( 'lcx_skin_before_menu_page' );
	?>
	
	<a href="" class="lcx-action lcx-btn" data-type="goPage" data-val="welcome">welcome</a>
	<a href="" class="lcx-action lcx-btn" data-type="goPage" data-val="online">online</a>
	<a href="" class="lcx-action lcx-btn" data-type="goPage" data-val="offline">offline</a>
	
	<?php 
	/**
	 * After menu page actions.
	 */
	do_action( 'lcx_skin_after_menu_page' );
	?>		
</div>
