<?php
if ( ! defined( 'ABSPATH' ) ) { exit; } ?>


<div id="page-offline" class="page page-offline __lcx">
	<?php 
	/**
	 * Before offline page actions.
	 */
	do_action( 'lcx_skin_before_offline_page' );
	?>

	<div id="offline-header" class="header">
		<?php 
		/**
		 * Before header actions.
		 */
		do_action( 'lcx_skin_before_offline_header' );
		?>
	OFFLINE HEADER
		<?php 
		/**
		 * After header actions.
		 */
		do_action( 'lcx_skin_after_offline_header' );
		?>
	</div>
	
	<?php 
	//
	// Show up contact forms if there is more than one
	//
	$total_forms = count( $offline_forms );
	
	if( $total_forms > 1 ): ?>
	<div class="field">
		<div class="label">How can we help?</div>

		<ul id="offline-how" class="offline-how tabs">
			<?php foreach( $offline_forms as $formid => $form ): ?>
				<li><a href="#form-<?php echo $formid; ?>" data-tab-id="offline-how"><?php echo $form['name']; ?></a></li>
			<?php endforeach; ?>
	   </ul>
	</div>
	<?php endif; ?>

	<!-- Form fields -->
	<?php if( !empty( $offline_forms ) ): foreach( $offline_forms as $formid => $form ): ?>
		<form id="form-<?php echo $formid; ?>" class="form form-<?php echo $formid; ?> __lcx">
			<input type="hidden" name="form-id" value="<?php echo $formid; ?>">
			<?php
			// Render custom fields
			if( !empty( $form['fields'] ) ) {
				$i = 0;
				foreach( $form['fields'] as $field ) {

					// Print form field
					echo fn_lcx_get_template( 'forms/field-'.$field['type'], $field );

					$i++;

				}
			// Use default offline form fields
			} else {  ?>
				default form
			<?php }//if?>

			<div id="is-ntf-offline" class="__lcx ntf"></div>

			<div class="send-button">
				<button type="submit" class="button is-primary">Send message</button>
			</div>

		</form>
 	<?php endforeach; endif; ?>
	
	<div class="ntf __lcx"></div>

	<?php 
	/**
	 * After offline page actions.
	 */
	do_action( 'lcx_skin_after_offline_page' );
	?>
</div>
