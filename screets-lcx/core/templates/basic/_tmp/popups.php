<?php
/**
 * SCREETS © 2018
 *
 * SCREETS, d.o.o. Sarajevo. All rights reserved.
 * This  is  commercial  software,  only  users  who have purchased a valid
 * license  and  accept  to the terms of the  License Agreement can install
 * and use this program.
 *
 * @package LiveChatX
 * @author Screets
 *
 */

if ( ! defined( 'ABSPATH' ) ) { exit; } 

/**
 * Filter popup pages.
 */
$pages = apply_filters( 'lcx_skin_popup_pages', array(
	'menu' => LiveChatX()->template_path() . '/popup-menu.php',
	'welcome' => LiveChatX()->template_path() . '/popup-welcome.php',
	'online' => LiveChatX()->template_path() . '/popup-online.php'
));

$header_filepath = apply_filters( 'lcx_skin_popup_header_filepath', 'popup-header.php' );
$footer_filepath = apply_filters( 'lcx_skin_popup_footer_filepath', 'popup-footer.php' );

foreach( $pages as $page_name => $filename ) {

	// Get header
	include $header_filepath;

	// Get the content
    if( file_exists( $filename ) ) {
	   include $filename;
    }

	// Get footer
	include $footer_filepath;
    
}
?>