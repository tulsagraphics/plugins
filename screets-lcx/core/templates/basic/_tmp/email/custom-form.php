<?php if( !empty( $fields['message'] ) ): ?>
	<!-- Question / Issue -->
	<tr style="line-height:0px">
		<td width="100%" style="font-size:0px" align="center" height="30"></td>
	</tr>

	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0">
				<tbody>
					<tr>
						<td width="36"></td>
						<td style="color: #444; line-height: 1.4em;">
							<?php echo wpautop( esc_html( $fields['message']['val'] ) ); ?>
						</td>
						<td width="36"></td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
<?php endif; ?>

<tr style="line-height:0px">
	<td width="100%" style="font-size:0px" align="center" height="30"></td>
</tr>

<tr>
	<td>
		<table cellpadding="0" cellspacing="0" style="line-height:25px" border="0" align="center">
			<tbody>
				<tr>
					<td width="36"></td>
					<td width="600" align="left" style="color:#444444;border-collapse:collapse;font-family:proxima_nova,'Roboto', sans-serif;max-width:600px" valign="top">
						
						<?php 
						foreach( $fields as $type => $val ) { 
							if( $type != 'message' and $type != 'form_data') {
								echo fn_lcx_get_template( 'email/field-' . $type, $val );
							}
						}
						?>
						
						<?php if( !empty( $fields['form_data'] ) ): foreach( $fields['form_data'] as $type => $val ): ?>
							<?php echo fn_lcx_get_template( 'email/field-' . $type, $val ); ?>
						<?php endforeach; endif; ?>

					</td>
					<td width="36"></td>
				</tr>
				<tr>
					<td colspan="3" height="36"></td>
				</tr>
				
			</tbody>
		</table>
		
	</td>
</tr>

<tr style="line-height:0px">
	<td width="100%" style="font-size:0px" align="center" height="1"></td>
</tr>