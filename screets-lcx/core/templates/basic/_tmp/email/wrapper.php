<?php if ( ! defined( 'ABSPATH' ) ) exit; ?>
<!doctype html>
<html>
<head>
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><?php echo $site_name; ?></title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet">
	<style>
	/* -------------------------------------
			GLOBAL
	------------------------------------- */
	* {
		font-family: proxima_nova, 'Roboto', sans-serif;
		font-size: 100%;
		line-height: 1.4em;
		margin: 0;
		padding: 0;
	}
	img {
		max-width: 600px;
		width: auto;
	}
	body {
		-webkit-font-smoothing: antialiased;
		height: 100%;
		-webkit-text-size-adjust: none;
		width: 100% !important;
		background-color: #efefef;
		margin: 0;
		padding: 0;
	}
	
	/* -------------------------------------
		TYPOGRAPHY
	------------------------------------- */
	h1, 
	h2, 
	h3 {
		color: #444444;
		font-family: proxima_nova, 'Roboto', sans-serif;
		font-weight: 200;
		line-height: 1.2em;
		margin: 0;
	}

	h1 {
		font-size: 36px;
		margin-bottom: 15px;
	}
	h2 {
		font-size: 21px;
		font-weight: 700;
	}
	h3 {
		font-size: 18px;
	}

	strong { font-weight: 700; }

	p, 
	ul, 
	ol {
		color: #444444;
		font-size: 11pt;
		line-height: 21px;
		padding-bottom: 22px;
		font-weight: normal;
	}

	ul li, 
	ol li {
		margin-left: 5px;
		list-style-position: inside;
	}

	table td {
		color: #444444;
		font-size: 11pt;
		line-height: 21px;
		font-weight: normal;
	}

	.row .label {
		display: inline-block;
		font-weight: 700;
		padding-right: 10px;
		min-width: 30%;
		vertical-align: top;
	}

	.row .content {
		display: inline-block;
		vertical-align: top;
	}

	@media (max-width: 460px ) {
		p, 
		ul, 
		ol,
		table td {
			font-size: 16pt;
		}

		h2 {
			font-size: 24pt;
		}

		table.wrapper {
			border-radius: 0 !important;
		}
	}
	</style>
</head>
<body bgcolor="#FFFFFF">
<center>

<table cellpadding="8" cellspacing="0" style="padding:0;width:100%!important;margin:0;background:#efefef;background-color:#efefef;" border="0">
	<tbody>
	<tr>
		<td valign="top">
			<table class="wrapper" cellpadding="0" cellspacing="0" style="max-width:600px;margin:<?php echo $padding; ?>px auto 0 auto;border-radius:<?php echo $radius_big; ?>px;background:#ffffff;background-color:#ffffff;box-shadow: 0px 30px 24px 0px rgba(0,0,0,0.05);" border="0" align="center">
				<tbody>
					<tr>
						<td colspan="3" height="6"></td>
					</tr>

					<?php
					// Site logo
					if( !empty( $site_logo ) ): ?>
					<tr style="line-height:0px">
						<td width="100%" style="font-size:0px" align="center" height="1">
							<img width="<?php echo $logo_width; ?>px" style="width:<?php echo $logo_width; ?>px;margin-top:<?php echo $padding; ?>px;" alt="" src="<?php echo $site_logo; ?>">
						</td>
					</tr>
					<?php endif; ?>
					
					<!-- Email body -->
					<?php echo $msg; ?>


					<?php
					// Signature
					if( !empty( $signature ) ): ?>

					<tr style="line-height:0px">
						<td width="100%" style="font-size:0px" align="center" height="1"></td>
					</tr>

					<tr>
						<td>
							<table cellpadding="0" cellspacing="0" align="center" border="0" style="width:100%">
								<tbody>									
									<tr>
										<td colspan="3" height="30"></td>
									</tr>
									<tr>
										<td width="36"></td>
										<td width="600" align="left" class="signature" valign="top">	
											<?php echo $signature; ?>					
										</td>
										<td width="36"></td>
									</tr>
									<tr>
										<td colspan="3" height="36"></td>
									</tr>

									
								</tbody>
							</table>
						</td>
					</tr>
					<?php endif; ?>

					<?php
					// Social links
					if( !empty( $social_links ) ): ?>

					<tr style="line-height:0px">
						<td width="100%" style="font-size:0px" align="center" height="1"></td>
					</tr>
					
					<tr>
						<td>
							<table cellpadding="0" cellspacing="0" align="center" border="0" style="width:100%">
								<tbody>
									<tr>
										<td align="center">
											
											<table cellpadding="0" cellspacing="0" align="center" border="0" style="width:100%;">
											<tbody>
												<tr>
													<td align="center">
													<?php foreach( $social_links as $name => $link ): ?>
															<a href="<?php echo $link; ?>"><img src="<?php echo "{$plugin_url}/assets/img/icons/{$name}-ico.png"; ?>" alt="<?php echo ucfirst( $name ); ?>" style="width:20px; height:20px; margin:0 5px;"></a>
													<?php endforeach; ?>
													</td>
												</tr>
											</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>

					<tr style="line-height:0px">
						<td width="100%" style="font-size:0px;height:30px;" align="center" height="30"></td>
					</tr>
					<?php endif; ?>
				</tbody>
			</table>

			<?php 
			// Footer
			if( !empty( $footer ) ): ?>
			<table cellpadding="0" style="width:600px; margin:0 auto;" cellspacing="0" align="center" border="0">
				<tbody>
					<tr>
						<td height="10"></td>
					</tr>
					<tr>
						<td style="padding:0;border-collapse:collapse">
							<table width="98%" style="width:98%;" cellpadding="0" cellspacing="0" align="center" border="0">
								<tbody>
									<tr style="color: #aaaaaa;font-size: 11px;font-family:proxima_nova,'Roboto', sans-serif;">
										<td align="right" style="color: #aaaaaa;font-size: 13px;font-family:proxima_nova,'Roboto', sans-serif;text-align:right;">
											<?php echo $footer; ?>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			<?php endif; ?>
		</td>
	</tr>
	<tr style="line-height:0px">
		<td width="100%" style="font-size:0px;height:30px;" align="center" height="30"></td>
	</tr>
	</tbody>
</table>
</center>
</body>
</html>