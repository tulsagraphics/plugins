<?php 
$term = get_term_by( 'id', $val, 'lcx_support_category' );
if( !empty( $val ) ): ?>
    <div class="row">
        <div class="label"><?php echo $label; ?>:</div>
        <div class="content">
            <?php echo $term->name; ?>
        </div>
    </div>
<?php endif; ?>