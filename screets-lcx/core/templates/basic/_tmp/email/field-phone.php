<?php if( !empty( $val['tel'] ) ): ?>
    <div class="row">
    	<div class="label"><?php echo $label; ?>:</div>
    	<div class="content">
    		<?php echo fn_lcx_get_phone_code( $val['country'] ) . ' ' . $val['tel']; ?>
    	</div>
    </div>
<?php endif; ?>