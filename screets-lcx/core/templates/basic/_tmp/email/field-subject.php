<?php if( !empty( $val ) ): ?>
    <div class="row">
            <div class="label"><?php echo $label; ?>:</div>
            <div class="content">
                <?php echo $val; ?>
            </div>
    </div>
<?php endif; ?>