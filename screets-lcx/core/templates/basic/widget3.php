<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <base target="_parent">
    <style>
    <?php
    // CSS files
    if( !empty( $css ) ) {
        foreach( $css as $path ) {
            echo file_get_contents( $path );
        }
    }
    ?>
    </style>
</head>
<body id="lcx" class="lcx">
    
    <?php
    /**
     * Action for before widget body content.
     */
    do_action( 'lcx_tpl_before_widget_body' );
    ?>


    <?php
    /**
     * In-app messages.
     */
    ?>
    <div id="lcx-inAppMsg" class="lcx-inAppMsg lcx--hidden">
        <a href="#" class="lcx-inAppMsgWrap lcx-action" data-action="openPopup" data-name="online" rel="nofollow">
            <span class="lcx-avatar">
                <img src="" alt="" class="lcx-avatar-img">
            </span>
    
            <span class="lcx-msg-wrap">
                <span class="lcx-msg">
                    <span class="lcx-author"></span>
                    <span class="lcx-msg-content"></span>
                </span>
                <span class="lcx-reply" data-placeholder="<?php echo $msg['forms_reply']; ?>"></span>
            </span>
        </a>
        <a class="lcx-close lcx-action" data-action="closeInAppMsg" rel="nofollow">
            <?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/close.svg' ); ?>
        </a>
    </div>
    
    
    <?php
    /**
     * Starter.
     */
    ?>
    <div id="lcx-starter" class="lcx-starter">
        <?php
        /**
         * Action for before content of starter.
         */
        do_action( 'lcx_tpl_before_starter' );
        ?>
    
        <span class="lcx-starter-default">
    
            <span class="lcx-starter-px">
                <?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/starter-open.svg' ); ?>
            </span>
    
            <span class="lcx-starter-body">
                <?php echo $msg['btn_title']; ?>
            </span>
    
            <span class="lcx-starter-sx"></span>
        </span>
    
        <span class="lcx-starter-minimized lcx--hidden">
            <span class="lcx-starter-px"><?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/starter-close.svg' ); ?></span>
        </span>
    
        <?php
        /**
         * Action for after content of starter.
         */
        do_action( 'lcx_tpl_after_starter' );
        ?>
    </div>
    
    
    <div id="lcx-popups-wrap" class="lcx-popups-wrap">
        
        <?php
        /**
         * Notifications popup.
         */
        ?>
        <div id="lcx-ntfs" class="lcx-ntfs"></div>
    
        <?php
        /**
         * Online popup.
         */
        ?>
        <div id="lcx-popup-online" class="lcx-popup lcx-popup-online lcx--hidden">
            
            <a href="#" class="lcx-btn-close lcx-action" data-action="closePopup" rel="nofollow"><?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/close.svg' ); ?></a>
            
            <!-- Loader -->
            <div class="lcx--loader">
                <div class="lcx-popup-wrap">
                    <div class="lcx-popup-body lcx-col--vCenter"><?php echo $msg['ntf_conn']; ?></div>
                </div>
            </div>
            
            <div class="lcx-popup-content">
                
                <div class="lcx-popup-wrap">
                    
                    <!-- Header -->
                    <div id="lcx-popup-online-header" class="lcx-popup-header lcx--hasOffset">
                           
                        <div class="__lcx-tpl-header lcx--hidden">
                            
                            <!-- Pre-chat header -->
                            <div class="lcx-prechat-header">

                                <div class="_lcx-list-onlineOps"></div>
    
                                <div class="lcx-popup-subtitle">

                                    <div class="lcx-popup-siteName lcx--hidden"><?php echo $site['info_name']; ?></div>

                                    <!-- Response time. (ONLINE) -->
                                    <?php 
                                    $responseTime = fn_lcx_get_response_time( 'online', $msg['popup_replies_in'] );
                                    if( !empty( $responseTime ) ): ?>
                                        <span class="lcx-online-greeting lcx--hidden">
                                            <?php echo $responseTime; ?>
                                        </span>
                                    <?php endif; ?>
                                    
                                    <!-- Case No. -->
                                    <?php if( isset( $msg['popup_case_no'] ) ): ?>
                                        <span class="lcx-case-no">
                                            <?php echo $msg['popup_case_no']; ?>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                                            
                            <!-- In chat header (one of OPs taken this chat)-->
                            <div class="lcx-inchat-header">
    
                                <div class="lcx-row lcx-opInfo">
                                    <div class="lcx-opPhotoURL lcx-col lcx-col--autoSize">
                                        {opPhoto}
                                    </div>
                                    <div class="lcx-opInfoWrap lcx-col lcx-col--autoSize">
                                        <span class="lcx-opName">{opName}</span>
                                        <div class="lcx-opStatus">
                                            <div class="lcx-isAway lcx--hidden">
                                                <?php echo $msg['ops_status_away']; ?>
                                            </div>
                                            <div class="lcx-isOnline lcx--hidden">
                                                <?php echo $msg['ops_status_online']; ?>
                                            </div>

                                            <!-- Case No. -->
                                            <?php if( isset( $msg['popup_case_no'] ) ): ?>
                                                <span class="lcx-case-no">
                                                    <?php echo $msg['popup_case_no']; ?>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <!-- Closed header -->
                            <div class="lcx-close-header">
                                <div class="lcx-popup-subtitle">
                                    <!-- Case No. -->
                                    <?php if( isset( $msg['popup_case_no'] ) ): ?>
                                        <span class="lcx-case-no">
                                            <?php echo $msg['popup_case_no']; ?>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="lcx-row">
                            <div class="lcx-col lcx-col--autoSize">
                                <a href="#" class="lcx-action lcx-btn-go-chats lcx-go-back" rel="nofollow" data-action="openPopup" data-name="chats"><?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/go-back.svg' ); ?></a>
                            </div>
                            <div class="lcx-col">
                                <div class="__lcx-dynamic-header"></div>
                            </div>
                        </div>

                        <!-- Offline message / Response time. (OFFLINE) -->
                        <?php 
                        $responseTime = fn_lcx_get_response_time( 'offline', $msg['popup_replies_in'] );?>
                       

                        <span class="lcx-offline-greeting lcx--hidden">

                            <?php echo $msg['ops_offline_msg']; ?>
                            
                            <?php if( !empty( $responseTime ) )  echo $responseTime; ?>
                        </span>
                    
                    </div>
                    
                    <!-- Body -->
                    <div id="lcx-popup-online-body" class="lcx-popup-body lcx-msgs-container lcx-col--vBottom">
    
                        <!-- Messages -->
                        <ul id="lcx-msgs" class="lcx-msgs"></ul>

                        <div class="lcx--clearfix"></div>

                        <!-- Collector -->
                        <?php if( !empty( $chats['offline_collectorFields'] ) ): 
                            $collectors = $chats['offline_collectorFields'];
                        ?>
                            <div id="__lcx-tpl-collector" class="__lcx-tpl-collector lcx--hidden">
                                <div class="lcx-form lcx-form-collector">
                                    <div class="lcx-steps"></div>

                                    <?php 
                                        foreach( $collectors as $i => $colName ): 
                                            $type = ( $colName == 'email' ) ? 'email' : 'text'; ?>

                                        <div class="lcx-field lcx-field-<?php echo $colName; ?> lcx-step-<?php echo $i+1; ?> lcx--hidden">
                                            <div class="lcx-label"><?php echo $i+1; ?>. <?php echo $msg['forms_' . $colName ]; ?></div>
                                            <input type="<?php echo $type; ?>" name="<?php echo $colName; ?>" id="lcx-f-collector-<?php echo $colName; ?>" class="lcx-input-text lcx-field-item lcx-f-collector-<?php echo $colName; ?>" placeholder="<?php echo $msg['forms_' . $colName ]; ?>">


                                            <div class="lcx-valid"><?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/ok.svg' ); ?></div>
                                        </div>

                                    <?php endforeach; ?>   

                                </div>
                            </div>
                        <?php endif; ?>
    
                        <!-- Ask contact information form -->
                        <div id="__lcx-tpl-ask-contact" class="__lcx-tpl-ask-contact lcx--hidden">
                            <div class="lcx-form lcx-form-askContact">
    
                                <div class="lcx-msg-wrap">
                                    <?php if( !empty( $siteLogo ) ): ?>
                                        <span class="lcx-avatar"><img src="<?php echo $siteLogo; ?>" alt=""></span>
                                    <?php endif; ?>
    
                                    <div class="lcx-content">
                                        <span class="lcx-msg"><?php echo $msg['popup_ask_contact']; ?></span></div>
                                </div>
    
                                <div class="lcx-row lcx-row--gutters">
                                    <div class="lcx-col">
                                        <input type="email" class="lcx-input-text lcx-field-email" name="email" placeholder="<?php echo $msg['forms_email' ]; ?>">
                                    </div>
                                    <div class="lcx-col lcx-col--autoSize">
                                        <a href="#" class="lcx-save-btn lcx-btn lcx-btn-lg lcx-btn--narrow lcx-btn-sec" rel="nofollow"><?php echo $msg['forms_save_btn']; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Footer -->
                    <div id="lcx-popup-online-footer" class="lcx-popup-footer">
    
                        <!-- Reply box -->
                        <div id="lcx-reply-wrap" class="lcx-reply-wrap">
                            <div id="lcx-reply" class="lcx-reply" data-placeholder="<?php echo $msg['forms_reply']; ?>" contenteditable="true"></div>
                        </div>
        
                        <div class="__lcx-tpl-footer lcx--hidden">
                            <!-- Footer for ended chats -->
                            <div class="lcx-close-footer">
                                
                                <!-- <div class="lcx-popup-action-links">
                                    <ul>
                                        <li>
                                            <a href="" class="lcx-link lcx-link-sm">Email transcript</a>
                                        </li>
                                    </ul>
                                </div> -->
                                <span class="lcx-status-msg"><?php echo $msg['popup_chatStatusMsgs_close']; ?></span>
                            </div>
                        </div>
    
                        <div class="__lcx-dynamic-footer"></div>
                    </div>
    
                </div>
                
            </div>
        </div>
    
        <?php
        /**
         * Conversation / chats popup.
         */
        ?>
        <div id="lcx-popup-chats" class="lcx-popup lcx-popup-chats lcx--hidden">
            
            <a href="#" class="lcx-btn-close lcx-action" data-action="closePopup" rel="nofollow"><?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/close.svg' ); ?></a>
    
            <!-- Loader -->
            <div class="lcx--loader">
                <div class="lcx-popup-wrap">
                    <div class="lcx-popup-body lcx-col--vCenter"><?php echo $msg['ntf_conn']; ?></div>
                </div>
            </div>
            
            <div class="lcx-popup-content">
                
                <div class="lcx-popup-wrap">
                    
                    <!-- Header -->
                    <div id="lcx-popup-chats-header" class="lcx-popup-header lcx--hasOffset lcx--hasOffset">
                        <div class="lcx-popup-title">
                            <?php echo $msg['others_cnv' ]; ?>
                        </div>
                    
                    </div>
                    
                    <!-- Body -->
                    <div id="lcx-popup-chats-body" class="lcx-popup-body lcx-chats-container">
                        <ul id="lcx-chats" class="lcx-chats"></ul>
                    </div>
                    
                    <!-- Footer -->
                    <div id="lcx-popup-chats-footer" class="lcx-popup-footer">
                        <a href="" class="lcx-btn lcx-btn-sec lcx-action" data-action="newCnv" rel="nofollow"><?php echo $msg['others_new_cnv' ]; ?></a>
                    </div>
    
                </div>
                
            </div>
        </div>
    
    </div>
</body>
</html>