<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<base target="_parent">
	<style>
	<?php
	// CSS files
	if( !empty( $css ) ) {
		foreach( $css as $path ) {
			echo file_get_contents( $path );
		}
	}
	?>
	</style>
</head>
<body class="lcx">
	
	<?php
	/**
	 * Action for before widget body content.
	 */
	do_action( 'lcx_skin_before_widget_body' );
	?>

	<?php
	/**
	 * Starter button.
	 */
	?>
	<a href="javascript:void(0);" id="lcx-starter" class="lcx-starter lcx-has-icon __lcx">
		<span class="lcx-full">
			<span class="lcx-prefix"><?php echo file_get_contents( LCX_PATH. '/assets/icons/ui/chatboxes.svg' ); ?></span>
			<span class="lcx-title"><?php echo $msg['btn_title']; ?></span>
		</span>
		<span class="lcx-minimized __lcx">
			<span class="lcx-prefix"><?php echo file_get_contents( LCX_PATH. '/assets/icons/ui/close-round.svg' ); ?></span>
		</span>
	</a>

	<?php
	/**
	 * In-app message template.
	 */
	?>
	<div id="lcx-app-msg" class="lcx-app-msg __lcx">
		<div class="lcx-content">
			hey
		</div>

		<div class="lcx-arrow-icon">
			<?php echo file_get_contents( LCX_PATH . '/assets/icons/ui/arrow-down-b.svg' ); ?>
		</div>

		<a href="#" class="lcx-clear">
			<?php echo file_get_contents( LCX_PATH . '/assets/icons/ui/close-circled.svg' ); ?>
		</a>
	</div>

	<?php
	/**
	 * Disconnect page (connecting).
	 */
	?>
	<div id="lcx-window-disconnect" class="lcx-window lcx-window-disconnect __lcx">
		
		<div class="lcx-window-content lcx-wrap">
			<!-- Notifications -->
			<div id="lcx-disconnect-ntfs" class="lcx-ntf lcx-disconnect-ntfs __lcx"></div>
		</div>
	</div>

	<?php
	/**
	 * Pre-chat.
	 */
	?>
	<div id="lcx-window-prechat" class="lcx-window lcx-window-prechat __lcx">
		<div class="lcx-flex-wrap">
			<div class="lcx-window-header lcx-wrap lcx-flex">
				<div class="lcx-hero">
					<div class="lcx-subtitle"><?php echo $msg['popup_header_popup']; ?></div>
					<h3 class="lcx-title"><?php echo $msg['popup_prechat_greeting']; ?></h3>
				</div>
			</div>
			<div class="lcx-window-content lcx-wrap lcx-flex lcx-flex-grow lcx-flex-fill">
				<div class="lcx-flex-fill-content lcx-flex-grow">
					<form id="lcx-form-prechat" action="index.php" class="lcx-form lcx-form-prechat" data-bound-group="prechat">
						<div class="lcx-field-wrap">
							<div id="lcx-prechat-ask-question" class="lcx-editable lcx-ask-question lcx-field" placeholder="<?php echo $msg['forms_ask_question']; ?>" data-name="msg" contenteditable="true"></div>
						</div>
					</form>
				</div>
			</div>
			<div class="lcx-window-footer lcx-flex">

				<!-- Notifications -->
				<div id="lcx-prechat-ntfs" class="lcx-ntf lcx-prechat-ntfs __lcx"></div>
				
				<!-- Submit form and tools -->
				<div class="lcx-table">
					<div class="lcx-cell">
						<button id="lcx-window-prechat-submit" class="lcx-btn lcx-is-secondary lcx-action" data-type="submitForm" data-bind-to="lcx-form-prechat"><?php echo $msg['forms_send_msg']; ?></button>
					</div>
					<div class="lcx-cell">
						<!-- Pre-chat footer tools -->
						<ul class="lcx-tools" style="display:none">
							<li>
								<a href="#" target="_blank" class="lcx-link"><?php echo file_get_contents( LCX_PATH. '/assets/icons/ui/happy-outline.svg' ); ?></a>
							</li>
							<li>
								<a href="#" target="_blank" class="lcx-link"><?php echo file_get_contents( LCX_PATH. '/assets/icons/ui/android-attach.svg' ); ?></a>
							</li>
						</ul>
					</div>
				</div>
				<a href="http://screets.io" target="_blank" class="screets-ico">screets.io</a>
			</div>
		</div>
	</div>
	
	<?php
	/**
	 * Online.
	 */
	?>
	<div id="lcx-window-online" class="lcx-window lcx-window-online __lcx">
		<div class="lcx-flex-wrap">
			<div class="lcx-window-header lcx-wrap lcx-flex">
				<div class="lcx-hero">
					<div class="lcx-subtitle lcx-is-small"><?php echo $msg['popup_header_popup']; ?></div>
					<h3 class="lcx-title lcx-is-small"><?php echo $msg['popup_online_greeting']; ?></h3>
					<div class="lcx-desc">
						<span class="lcx-case-no">
							<?php
							// Case no.
							if( isset( $msg['popup_case_no'] ) )
								echo $msg['popup_case_no']; ?>
						</span>
					</div>
				</div>
			</div>

			<div class="lcx-window-content lcx-flex lcx-flex-grow lcx-flex-fill">
				<div class="lcx-flex-fill-content lcx-flex-grow">
					<div class="lcx-cnv">
						<ul class="lcx-msgs"></ul>
					</div>
				</div>
			</div>
			<div class="lcx-window-footer lcx-flex">
				<!-- Notifications -->
				<div id="lcx-online-ntfs" class="lcx-ntf lcx-online-ntfs __lcx"></div>				
				<!-- Reply box -->
				<div id="lcx-reply-online" class="lcx-editable lcx-reply lcx-reply-bottom lcx-reply-online lcx-autosize" placeholder="<?php echo $msg['forms_reply']; ?>" contenteditable="true"></div>
			</div>
		</div>
	</div>

	<?php
	/**
	* Offline.
	*/
	?>
	<div id="lcx-window-offline" class="lcx-window lcx-window-offline __lcx">
		<div class="lcx-flex-wrap">
			<div class="lcx-window-header lcx-wrap lcx-flex">
				<div class="lcx-hero">
					<div class="lcx-subtitle lcx-is-small"><?php echo $msg['popup_header_popup']; ?></div>
					<h3 class="lcx-title lcx-is-small"><?php echo $msg['popup_offline_greeting']; ?></h3>
					<div class="lcx-desc">
						<?php
						// Online response time.
						if( isset( $msg['popup_replies_in_online'] ) )
							echo $msg['popup_replies_in_online']; ?>
						
						<span class="lcx-case-no">
							<?php
							// Case no.
							if( isset( $msg['popup_case_no'] ) )
								echo $msg['popup_case_no']; ?>
						</span>
					</div>
				</div>
			</div>

			<div class="lcx-window-content lcx-flex lcx-flex-grow lcx-flex-fill">
				<div class="lcx-flex-fill-content lcx-flex-grow">
					<form action="index.php" id="lcx-form-offline" class="lcx-form lcx-form-offline __lcx" data-bound-group="offline">

						<div class="lcx-field-wrap">
							<div class="lcx-label">Name:</div>
							<input type="text" name="name" value="" class="lcx-field lcx-field-name" placeholder="Name">
						</div>
						<div class="lcx-field-wrap">
							<div class="lcx-label">Email:</div>
							<input type="email" name="email" value="" class="lcx-field lcx-field-email" placeholder="Email">
						</div>
						<div class="lcx-field-wrap">
							<div class="lcx-label">Message:</div>
							<textarea name="msg" value="" class="lcx-field lcx-field-msg" placeholder="Message"></textarea>
						</div>

						<button id="lcx-window-prechat-submit" class="lcx-btn lcx-is-secondary lcx-action" data-type="submitForm" data-bind-to="lcx-form-prechat"><?php echo $msg['forms_send_msg']; ?></button>
					</form>

					<div class="lcx-cnv">
						<ul class="lcx-msgs"></ul>
					</div>
				</div>
			</div>
			<div class="lcx-window-footer lcx-flex">
				<!-- Notifications -->
				<div id="lcx-offline-ntfs" class="lcx-ntf lcx-offline-ntfs __lcx"></div>

				<!-- Footnote -->
				<div class="lcx-footnote"><?php echo $msg['popup_offline_footnote']; ?></div>
				
				<!-- Reply box -->
				<div id="lcx-reply-offline" class="lcx-editable lcx-reply lcx-reply-bottom lcx-reply-offline lcx-autosize" placeholder="<?php echo $msg['forms_reply']; ?>" contenteditable="true"></div>
			</div>
		</div>
	</div>

	<?php
	/**
	 * Include files.
	 */
	include 'shortcodes.php';
	?>
	

	<?php
	/**
	 * Action for after widget body content.
	 */
	do_action( 'lcx_skin_after_widget_body' );
	?>

	<?php
	// Scripts
	if( !empty( $js ) ) {
		foreach( $js as $path ) {
			echo '<script src="' . esc_url( $path ) . "\"></script>\n\t";
		}
	}
	?>
</body>
</html>