
<?php
/**
 * Action for before widget body content.
 */
do_action( 'lcx_tpl_before_widget_body' );
?>


<div id="lcx" class="lcx">

    <?php
    /**
     * In-app messages.
     */
    ?>
    <div id="lcx-inAppMsg" class="lcx-inAppMsg lcx--hidden">
        <a href="#" class="lcx-inAppMsgWrap lcx-action" data-action="openPopup" data-name="online" rel="nofollow">
            <span class="lcx-avatar">
                <img src="" alt="" class="lcx-avatar-img">
            </span>

            <span class="lcx-msg-wrap">
                <span class="lcx-msg">
                    <span class="lcx-author"></span>
                    <span class="lcx-msg-content"></span>
                </span>
                <span class="lcx-reply" data-placeholder="<?php echo $msg['forms_reply']; ?>"></span>
            </span>
        </a>
        <a class="lcx-close lcx-action" data-action="closeInAppMsg" rel="nofollow">
            <?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/close.svg' ); ?>
        </a>
    </div>


    <?php
    /**
     * Starter.
     */
    ?>
    <div id="lcx-starter" class="lcx-starter">
        <?php
        /**
         * Action for prepend content of starter.
         */
        do_action( 'lcx_tpl_prepend_starter' );
        ?>

        <span class="lcx-starter-default">

            <span class="lcx-starter-px">
                <?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/starter-open.svg' ); ?>
            </span>

            <span class="lcx-starter-body">
                <?php echo $msg['btn_title']; ?>
            </span>

            <span class="lcx-starter-sx"></span>
        </span>

        <span class="lcx-starter-minimized lcx--hidden">
            <span class="lcx-starter-px"><?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/starter-close.svg' ); ?></span>
        </span>

        <?php
        /**
         * Action for append content of starter.
         */
        do_action( 'lcx_tpl_append_starter' );
        ?>
    </div>


    <div id="lcx-popups-wrap" class="lcx-popups-wrap">
        
        <?php
        /**
         * Notifications popup.
         */
        ?>
        <div id="lcx-ntfs" class="lcx-ntfs"></div>

        <?php
        /**
         * Pre-chat popup.
         */
        ?>
        <div id="lcx-popup-prechat" class="lcx-popup lcx-popup-prechat lcx--hidden <?php if( !empty( $siteLogo ) ) echo 'lcx-has-avatar'; ?>">

            <a href="#" class="lcx-btn-close lcx-action" data-action="closePopup" rel="nofollow"><?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/close.svg' ); ?></a>
            
            <div class="lcx-popup-wrap">
                
                <div class="lcx-avatar">
                    <?php
                    if( !empty( $siteLogo ) ): ?>
                        <img src="<?php echo $siteLogo; ?>" alt="">
                    <?php endif; ?>

                    <a href="#" class="lcx-action lcx-go-back" data-action="openPopup" data-name="chats" rel="nofollow"><?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/go-back.svg' ); ?></a>
                </div>

                <div id="lcx-popup-prechat-body" class="lcx-popup-prechat-body lcx-popup-body">

                    <div id="lcx-prechat-welcome-msg" class="lcx-prechat-welcome-msg lcx-popup--offset">
                        <div class="lcx--loader"><?php echo $msg['ntf_conn']; ?></div>
                    </div>
                                        
                    <!-- Online welcome -->
                    <div class="lcx-online-welcome lcx-popup--offset">
                        <div class="lcx-popup-subtitle lcx-response-time">
                            <?php 
                            $prechatResponseTime = fn_lcx_get_response_time( 'online', $msg['popup_replies_in'] );
                            if( !empty( $prechatResponseTime ) ): ?>
                                <span class="lcx-offline-greeting">
                                    <?php echo $prechatResponseTime; ?>
                                </span>
                            <?php endif; ?>
                        </div>
                        
                        <div class="_lcx-list-onlineOps"></div>
                    </div>

                    <!-- Offline welcome -->
                    <div class="lcx-offline-welcome lcx-popup--offset">
                        <div class="lcx-popup-subtitle lcx-response-time">
                            <?php 
                            $prechatResponseTime = fn_lcx_get_response_time( 'offline', $msg['popup_replies_in'] );
                            if( !empty( $prechatResponseTime ) ): ?>
                                <span class="lcx-offline-greeting">
                                    <?php echo $prechatResponseTime; ?>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="lcx-popup-footer">
                    <form id="lcx-form-prechat" action="index.php" class="lcx-form lcx-form-prechat">

                        <div class="lcx-row">
                            <div class="lcx-col">
                                
                                <!-- Reply box -->
                                <div class="lcx-reply-wrap">
                                    <div id="lcx-field-prechat-msg" class="lcx-field-prechat-msg lcx-reply" data-placeholder="<?php echo $msg['forms_reply']; ?>" contenteditable="true"></div>
                                </div>
                            </div>
                            <div class="lcx-col lcx-col--autoSize lcx-col--center lcx--hideDesktop">
                                <!-- Submit -->
                                <div class="lcx-submit-field">
                                    <a href="" id="lcx-btn-start-chat" class="lcx-btn lcx-btn-sm lcx-btn-sec" rel="nofollow"><?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/send.svg' ); ?></a>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <?php
        /**
         * Online popup.
         */
        ?>
        <div id="lcx-popup-online" class="lcx-popup lcx-popup-online lcx--hidden">
            
            <a href="#" class="lcx-btn-close lcx-action" data-action="closePopup" rel="nofollow"><?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/close.svg' ); ?></a>
            
            <!-- Loader -->
            <div class="lcx--loader">
                <div class="lcx-popup-wrap">
                    <div class="lcx-popup-body lcx-col--vCenter"><?php echo $msg['ntf_conn']; ?></div>
                </div>
            </div>
            
            <div class="lcx-popup-content">
                
                <div class="lcx-popup-wrap">
                    
                    <!-- Header -->
                    <div id="lcx-popup-online-header" class="lcx-popup-header lcx--hasOffset">
                        <div class="lcx-popup-header-links">
                            <a href="#" class="lcx-link lcx-action lcx-link-sm lcx-btn-go-chats" rel="nofollow" data-action="openPopup" data-name="chats">&lsaquo; <?php echo $msg['others_cnv' ]; ?></a>
                        </div>

                        <div class="__lcx-tpl-header lcx--hidden">
                            
                            <!-- Pending header -->
                            <div class="lcx-pending-header">

                                <div class="lcx-popup-title">
                                    <?php 
                                    $offlineGreeting = fn_lcx_get_response_time( 'online', $msg['popup_pending_greeting'] );
                                    if( !empty( $offlineGreeting ) ): ?>
                                        <span class="lcx-offline-greeting">
                                            <?php echo $offlineGreeting; ?>
                                        </span>
                                    <?php endif; ?>
                                </div>

                                <div class="lcx-popup-subtitle">
                                    <!-- Case No. -->
                                    <?php if( isset( $msg['popup_case_no'] ) ): ?>
                                        <span class="lcx-case-no">
                                            <?php echo $msg['popup_case_no']; ?>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                                            
                            <!-- In chat header (one of OPs taken this chat)-->
                            <div class="lcx-inchat-header">

                                <div class="lcx-popup-title">
                                    <?php 
                                    $offlineGreeting = fn_lcx_get_response_time( 'online', $msg['popup_inchat_greeting'] );
                                    if( !empty( $offlineGreeting ) ): ?>
                                        <span class="lcx-offline-greeting">
                                            <?php echo $offlineGreeting; ?>
                                        </span>
                                    <?php endif; ?>
                                </div>

                                <div class="lcx-popup-subtitle">
                                    <!-- Case No. -->
                                    <?php if( isset( $msg['popup_case_no'] ) ): ?>
                                        <span class="lcx-case-no">
                                            <?php echo $msg['popup_case_no']; ?>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <!-- Closed header -->
                            <div class="lcx-close-header">
                                <div class="lcx-popup-subtitle">
                                    <!-- Case No. -->
                                    <?php if( isset( $msg['popup_case_no'] ) ): ?>
                                        <span class="lcx-case-no">
                                            <?php echo $msg['popup_case_no']; ?>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="__lcx-dynamic-header"></div>
                    
                    </div>
                    
                    <!-- Body -->
                    <div id="lcx-popup-online-body" class="lcx-popup-body lcx-msgs-container lcx-col--vBottom">

                        <!-- Messages -->
                        <ul id="lcx-msgs" class="lcx-msgs"></ul>

                        <!-- Ask contact information form -->
                        <div id="__lcx-tpl-ask-contact" class="__lcx-tpl-ask-contact lcx--hidden">
                            <div class="lcx-form lcx-form-askContact">

                                <div class="lcx-msg-wrap">
                                    <?php if( !empty( $siteLogo ) ): ?>
                                        <span class="lcx-avatar"><img src="<?php echo $siteLogo; ?>" alt=""></span>
                                    <?php endif; ?>

                                    <div class="lcx-content">
                                        <span class="lcx-msg"><?php echo $msg['popup_ask_contact']; ?></span></div>
                                </div>

                                <div class="lcx-row lcx-row--gutters">
                                    <div class="lcx-col">
                                        <input type="email" class="lcx-input-text lcx-field-email" name="email" placeholder="<?php echo $msg['forms_email' ]; ?>">
                                    </div>
                                    <div class="lcx-col lcx-col--autoSize">
                                        <a href="#" class="lcx-save-btn lcx-btn lcx-btn-lg lcx-btn--narrow lcx-btn-sec" rel="nofollow"><?php echo $msg['forms_save_btn']; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Footer -->
                    <div id="lcx-popup-online-footer" class="lcx-popup-footer">

                        <!-- Reply box -->
                        <div id="lcx-reply-wrap" class="lcx-reply-wrap">
                            <div id="lcx-reply" class="lcx-reply" data-placeholder="<?php echo $msg['forms_reply']; ?>" contenteditable="true"></div>
                        </div>
        
                        <div class="__lcx-tpl-footer lcx--hidden">
                            <!-- Footer for ended chats -->
                            <div class="lcx-close-footer">
                                
                                <!-- <div class="lcx-popup-action-links">
                                    <ul>
                                        <li>
                                            <a href="" class="lcx-link lcx-link-sm">Email transcript</a>
                                        </li>
                                    </ul>
                                </div> -->
                                <span class="lcx-status-msg"><?php echo $msg['popup_chatStatusMsgs_close']; ?></span>
                            </div>
                        </div>

                        <div class="__lcx-dynamic-footer"></div>
                    </div>

                </div>
                
            </div>
        </div>

        <?php
        /**
         * Conversation / chats popup.
         */
        ?>
        <div id="lcx-popup-chats" class="lcx-popup lcx-popup-chats lcx--hidden">
            
            <a href="#" class="lcx-btn-close lcx-action" data-action="closePopup" rel="nofollow"><?php echo file_get_contents( LCX_PATH . '/assets/icons/chatbox/close.svg' ); ?></a>

            <!-- Loader -->
            <div class="lcx--loader">
                <div class="lcx-popup-wrap">
                    <div class="lcx-popup-body lcx-col--vCenter"><?php echo $msg['ntf_conn']; ?></div>
                </div>
            </div>
            
            <div class="lcx-popup-content">
                
                <div class="lcx-popup-wrap">
                    
                    <!-- Header -->
                    <div id="lcx-popup-chats-header" class="lcx-popup-header lcx--hasOffset">
                        <div class="lcx-popup-title">
                            <?php echo $msg['others_cnv' ]; ?>
                        </div>
                    
                    </div>
                    
                    <!-- Body -->
                    <div id="lcx-popup-chats-body" class="lcx-popup-body lcx-chats-container">
                        <ul id="lcx-chats" class="lcx-chats"></ul>
                    </div>
                    
                    <!-- Footer -->
                    <div id="lcx-popup-chats-footer" class="lcx-popup-footer">
                        <a href="" class="lcx-btn lcx-btn-primary lcx-btn-sm lcx-action" data-action="openPopup" data-name="prechat" rel="nofollow"><?php echo $msg['others_new_cnv' ]; ?></a>
                    </div>

                </div>
                
            </div>
        </div>

    </div>

        
</div>

<script>
    'use strict';

    var lcxOpts = <?php echo json_encode( $appOpts ) ?>;
    var lcxStr = <?php echo json_encode( $strings ) ?>;
    var lcx_events = new wolfEvents();

    var __lcxOpts = {
        db: {},
        ajax: {},
        user: {},
        autoinit: true,
        authMethod: 'anonymous',
        initPopup: 'prechat',
        ntfDuration: 5000, // ms.
        platform: 'frontend',
        dateFormat: 'd/m/Y H:i',
        hourFormat: 'H:i',
        askContactInfo: true,

        // Company data
        companyName: '',
        companyURL: '',
        companyLogo: '',
        anonymousImage: '',
        systemImage: '',
    };

    for( const k in lcxOpts ) {
        __lcxOpts[k] = lcxOpts[k]; 
    }

    var lcx_db = new NBirdDB( __lcxOpts );

    <?php
    /**
     * Actions before app script.
     */
    do_action( 'lcx_before_app_script' );
    ?>

    var lcx_frontend = new nightBird( __lcxOpts, lcxStr );

    <?php
    /**
     * Actions after app script.
     */
    do_action( 'lcx_after_app_script' );
    ?>
</script>