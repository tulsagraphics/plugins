# Copyright (C) 2015 YIThemes
# This file is distributed under the same license as the YITH Plugin Starter package.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: YITH WooCommerce Badge Management\n"
"Report-Msgid-Bugs-To: Your Inspiration Themes <plugins@yithemes.com>\n"
"POT-Creation-Date: 2018-05-03 16:26+0200\n"
"PO-Revision-Date: 2016-04-20 11:09+0100\n"
"Last-Translator: \n"
"Language-Team: Your Inspiration Themes <info@yithemes.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.13\n"
"X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;"
"_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;"
"esc_html_x:1,2c\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: ..\n"
"X-Textdomain-Support: yes\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: plugin-fw\n"

#: badge-styles/advanced-badge-styles.php:628
#: badge-styles/advanced-badge-styles.php:897
msgid "DISCOUNT"
msgstr ""

#: class.yith-wcbm-admin-premium.php:81 functions.yith-wcbm-premium.php:510
#: functions.yith-wcbm.php:143
msgid "Preview"
msgstr ""

#: class.yith-wcbm-admin-premium.php:125 class.yith-wcbm-admin.php:440
msgid "none"
msgstr ""

#: class.yith-wcbm-admin-premium.php:133
msgid "Schedule"
msgstr ""

#: class.yith-wcbm-admin-premium.php:137
msgid "Starting Date"
msgstr ""

#: class.yith-wcbm-admin-premium.php:144
msgid "Ending Date"
msgstr ""

#: class.yith-wcbm-admin-premium.php:156 plugin-options/category-options.php:48
msgid "Category Badges"
msgstr ""

#: class.yith-wcbm-admin-premium.php:157
#: plugin-options/shipping-class-options.php:45
msgid "Shipping Class Badges"
msgstr ""

#: class.yith-wcbm-admin-premium.php:385
msgctxt "Admin:title of column in products table"
msgid "Badge"
msgstr ""

#: class.yith-wcbm-admin-premium.php:436
msgid "Select Badge"
msgstr ""

#: class.yith-wcbm-admin-premium.php:439
msgid "— No Change —"
msgstr ""

#: class.yith-wcbm-admin-premium.php:440 plugin-options/advanced-options.php:10
#: plugin-options/auctions-options.php:9 plugin-options/category-options.php:8
#: plugin-options/dynamic-pricing-options.php:9
#: plugin-options/shipping-class-options.php:8
msgid "None"
msgstr ""

#: class.yith-wcbm-admin.php:129
msgid "No badge to duplicate has been supplied!"
msgstr ""

#: class.yith-wcbm-admin.php:139
#, php-format
msgid "Error while duplicating badge: badge #%s not found"
msgstr ""

#: class.yith-wcbm-admin.php:144
msgid "Copy"
msgstr ""

#: class.yith-wcbm-admin.php:185
msgid "Make a duplicate from this badge"
msgstr ""

#: class.yith-wcbm-admin.php:186
msgid "Duplicate"
msgstr ""

#: class.yith-wcbm-admin.php:201
msgid "Your Inspiration Themes"
msgstr ""

#: class.yith-wcbm-admin.php:205 class.yith-wcbm-admin.php:261
msgid "Plugin Documentation"
msgstr ""

#: class.yith-wcbm-admin.php:209
msgid "Plugin Site"
msgstr ""

#: class.yith-wcbm-admin.php:213
msgid "Live Demo"
msgstr ""

#: class.yith-wcbm-admin.php:235 class.yith-wcbm-admin.php:283
msgid "Settings"
msgstr ""

#: class.yith-wcbm-admin.php:237 class.yith-wcbm-admin.php:284
msgid "Premium Version"
msgstr ""

#: class.yith-wcbm-admin.php:292 class.yith-wcbm-admin.php:293
msgid "Badge Management"
msgstr ""

#: class.yith-wcbm-admin.php:341
msgid "Slider"
msgstr ""

#: class.yith-wcbm-admin.php:342 functions.yith-wcbm-premium.php:385
msgid "Number"
msgstr ""

#: class.yith-wcbm-admin.php:374
msgid "Badge Options"
msgstr ""

#: class.yith-wcbm-admin.php:421
msgid "Product Badge"
msgstr ""

#: class.yith-wcbm-post-types.php:39
msgid "Badges"
msgstr ""

#: class.yith-wcbm-post-types.php:40
msgid "Badge"
msgstr ""

#: class.yith-wcbm-post-types.php:41
msgid "Add Badge"
msgstr ""

#: class.yith-wcbm-post-types.php:42
msgid "Add new Badge"
msgstr ""

#: class.yith-wcbm-post-types.php:43
msgid "Edit Badge"
msgstr ""

#: class.yith-wcbm-post-types.php:44
msgid "View Badge"
msgstr ""

#: class.yith-wcbm-post-types.php:45
msgid "Badge not found"
msgstr ""

#: class.yith-wcbm-post-types.php:46
msgid "Badge not found in trash"
msgstr ""

#: functions.yith-wcbm-premium.php:29 functions.yith-wcbm.php:29
msgid "Text Badge"
msgstr ""

#: functions.yith-wcbm-premium.php:32
msgid "CSS Badge"
msgstr ""

#: functions.yith-wcbm-premium.php:35 functions.yith-wcbm.php:30
msgid "Image Badge"
msgstr ""

#: functions.yith-wcbm-premium.php:38
msgid "Advanced Badge"
msgstr ""

#: functions.yith-wcbm-premium.php:55 functions.yith-wcbm.php:40
msgid "Text Options"
msgstr ""

#: functions.yith-wcbm-premium.php:59 functions.yith-wcbm-premium.php:220
#: functions.yith-wcbm.php:44
msgid "Text"
msgstr ""

#: functions.yith-wcbm-premium.php:67 functions.yith-wcbm-premium.php:237
#: functions.yith-wcbm-premium.php:309 functions.yith-wcbm.php:52
msgid "Text Color"
msgstr ""

#: functions.yith-wcbm-premium.php:76
msgid "Font Size (pixel)"
msgstr ""

#: functions.yith-wcbm-premium.php:84
msgid "Line Height (pixel)"
msgstr ""

#: functions.yith-wcbm-premium.php:89
msgid "[set -1 to set it equal to height of the badge]"
msgstr ""

#: functions.yith-wcbm-premium.php:97 functions.yith-wcbm.php:63
msgid "Style Options"
msgstr ""

#: functions.yith-wcbm-premium.php:101 functions.yith-wcbm.php:67
msgid "Background Color"
msgstr ""

#: functions.yith-wcbm-premium.php:110 functions.yith-wcbm.php:76
msgid "Size (pixel)"
msgstr ""

#: functions.yith-wcbm-premium.php:124 functions.yith-wcbm.php:90
msgid "Width"
msgstr ""

#: functions.yith-wcbm-premium.php:127 functions.yith-wcbm.php:93
msgid "Height"
msgstr ""

#: functions.yith-wcbm-premium.php:135
msgid "Border Radius (pixel)"
msgstr ""

#: functions.yith-wcbm-premium.php:166
msgid "Padding (pixel)"
msgstr ""

#: functions.yith-wcbm-premium.php:201
msgid "Select the CSS Badge"
msgstr ""

#: functions.yith-wcbm-premium.php:216
msgid "CSS Options"
msgstr ""

#: functions.yith-wcbm-premium.php:228 functions.yith-wcbm-premium.php:300
msgid "Badge Color"
msgstr ""

#: functions.yith-wcbm-premium.php:251 functions.yith-wcbm.php:105
msgid "Select the Image Badge"
msgstr ""

#: functions.yith-wcbm-premium.php:270 functions.yith-wcbm-premium.php:542
msgid "Upload"
msgstr ""

#: functions.yith-wcbm-premium.php:281
msgid "Select the Advanced Badge"
msgstr ""

#: functions.yith-wcbm-premium.php:296
msgid "Advanced Options"
msgstr ""

#: functions.yith-wcbm-premium.php:318
msgid "Display"
msgstr ""

#: functions.yith-wcbm-premium.php:323
msgid "Percentage"
msgstr ""

#: functions.yith-wcbm-premium.php:327
msgid "Amount"
msgstr ""

#: functions.yith-wcbm-premium.php:337
msgid "Opacity and position"
msgstr ""

#: functions.yith-wcbm-premium.php:341
msgid "Opacity"
msgstr ""

#: functions.yith-wcbm-premium.php:350
msgid "[0:transparent | 100:opaque]"
msgstr ""

#: functions.yith-wcbm-premium.php:355
msgid "Rotation"
msgstr ""

#: functions.yith-wcbm-premium.php:389
msgid "X"
msgstr ""

#: functions.yith-wcbm-premium.php:390
msgid "Y"
msgstr ""

#: functions.yith-wcbm-premium.php:391
msgid "Z"
msgstr ""

#: functions.yith-wcbm-premium.php:392
msgid "SET MODE"
msgstr ""

#: functions.yith-wcbm-premium.php:400
msgid "Flip Text"
msgstr ""

#: functions.yith-wcbm-premium.php:407
msgid "Horizontally"
msgstr ""

#: functions.yith-wcbm-premium.php:411
msgid "Vertically"
msgstr ""

#: functions.yith-wcbm-premium.php:420
msgid "Anchor Point"
msgstr ""

#: functions.yith-wcbm-premium.php:425 functions.yith-wcbm.php:131
msgid "top-left"
msgstr ""

#: functions.yith-wcbm-premium.php:428 functions.yith-wcbm.php:132
msgid "top-right"
msgstr ""

#: functions.yith-wcbm-premium.php:431 functions.yith-wcbm.php:133
msgid "bottom-left"
msgstr ""

#: functions.yith-wcbm-premium.php:434 functions.yith-wcbm.php:134
msgid "bottom-right"
msgstr ""

#: functions.yith-wcbm-premium.php:438
msgid "[for Drag and Drop positioning]"
msgstr ""

#: functions.yith-wcbm-premium.php:443
msgid "Position (pixel or percentual)"
msgstr ""

#: functions.yith-wcbm-premium.php:462 functions.yith-wcbm-premium.php:494
msgid "Top"
msgstr ""

#: functions.yith-wcbm-premium.php:463 functions.yith-wcbm-premium.php:495
msgid "Bottom"
msgstr ""

#: functions.yith-wcbm-premium.php:464 functions.yith-wcbm-premium.php:496
msgid "Left"
msgstr ""

#: functions.yith-wcbm-premium.php:465 functions.yith-wcbm-premium.php:497
msgid "Right"
msgstr ""

#: functions.yith-wcbm-premium.php:472
msgid "Center Positioning"
msgstr ""

#: functions.yith-wcbm-premium.php:498
msgid "Center"
msgstr ""

#: functions.yith-wcbm-premium.php:512
msgid "Use Drag and Drop for positioning"
msgstr ""

#: functions.yith-wcbm-premium.php:541
msgid "Upload Custom Image"
msgstr ""

#: functions.yith-wcbm.php:123 functions.yith-wcbm.php:127
msgid "Position"
msgstr ""

#: includes/compatibility/class.yith-wcbm-auctions-compatibility.php:92
msgid "Auctions"
msgstr ""

#: includes/compatibility/class.yith-wcbm-dynamic-pricing-compatibility.php:104
msgid "Dynamic Pricing"
msgstr ""

#: init.php:52
msgid ""
"YITH WooCommerce Badge Management is enabled but not effective. It requires "
"Woocommerce in order to work."
msgstr ""

#: plugin-options/advanced-options.php:35 plugin-options/settings-options.php:8
msgid "General Options"
msgstr ""

#: plugin-options/advanced-options.php:43 plugin-options/settings-options.php:16
msgid "Hide \"On sale\" badge"
msgstr ""

#: plugin-options/advanced-options.php:45 plugin-options/settings-options.php:18
msgid "Select to hide the default Woocommerce \"On sale\" badge."
msgstr ""

#: plugin-options/advanced-options.php:51 plugin-options/settings-options.php:24
msgid "Hide in sidebars"
msgstr ""

#: plugin-options/advanced-options.php:53 plugin-options/settings-options.php:26
msgid "Select to hide the badges in sidebars and widgets."
msgstr ""

#: plugin-options/advanced-options.php:59 plugin-options/settings-options.php:32
msgid "Product Badge overrides default on sale badge"
msgstr ""

#: plugin-options/advanced-options.php:61 plugin-options/settings-options.php:34
msgid ""
"Select if you want to hide WooCommerce default \"On Sale\" badge when the product "
"has another badge"
msgstr ""

#: plugin-options/advanced-options.php:67
msgid "Enable Shop Manager to edit these options"
msgstr ""

#: plugin-options/advanced-options.php:75
msgid "Force Positioning"
msgstr ""

#: plugin-options/advanced-options.php:77
msgid "Force the badge positioning through javascript"
msgstr ""

#: plugin-options/advanced-options.php:80
msgid "No"
msgstr ""

#: plugin-options/advanced-options.php:81 plugin-options/advanced-options.php:178
msgid "Single Product"
msgstr ""

#: plugin-options/advanced-options.php:82
msgid "Shop Page"
msgstr ""

#: plugin-options/advanced-options.php:83
msgid "Everywhere"
msgstr ""

#: plugin-options/advanced-options.php:88
msgid "Force Positioning Timeout"
msgstr ""

#: plugin-options/advanced-options.php:90
msgid "Number of milliseconds before force positioning"
msgstr ""

#: plugin-options/advanced-options.php:100
msgid "Automatic Badges"
msgstr ""

#: plugin-options/advanced-options.php:106
msgid "Recent products Badge"
msgstr ""

#: plugin-options/advanced-options.php:108
msgid "Select the badge you want to apply to all recent products."
msgstr ""

#: plugin-options/advanced-options.php:116
msgid "Recent products - newer than"
msgstr ""

#: plugin-options/advanced-options.php:118
msgid "Show the badge for products that are newer than X days."
msgstr ""

#: plugin-options/advanced-options.php:125
msgid "On sale Badge"
msgstr ""

#: plugin-options/advanced-options.php:127
msgid "Select the Badge for products on sale."
msgstr ""

#: plugin-options/advanced-options.php:135
msgid "Featured Badge"
msgstr ""

#: plugin-options/advanced-options.php:137
msgid "Select the badge for featured products."
msgstr ""

#: plugin-options/advanced-options.php:145
msgid "Out of stock Badge"
msgstr ""

#: plugin-options/advanced-options.php:147
msgid "Select the Badge for products out of stock."
msgstr ""

#: plugin-options/advanced-options.php:155
msgid "Low stock Badge"
msgstr ""

#: plugin-options/advanced-options.php:157
msgid "Select the Badge for products with low stock."
msgstr ""

#: plugin-options/advanced-options.php:165
msgid "Low stock quantity"
msgstr ""

#: plugin-options/advanced-options.php:167
msgid "Select the low stock quantity."
msgstr ""

#: plugin-options/advanced-options.php:185
msgid "Hide on Single Product"
msgstr ""

#: plugin-options/advanced-options.php:187
msgid "Select to hide badges on Single Product Page."
msgstr ""

#: plugin-options/advanced-options.php:194
msgid "Show advanced badges in variable products"
msgstr ""

#: plugin-options/advanced-options.php:200
msgid "only if the discount percentage/amount is the same for all variations"
msgstr ""

#: plugin-options/advanced-options.php:201
msgid "Show minimum discount percentage/amount"
msgstr ""

#: plugin-options/advanced-options.php:202
msgid "Show maximum discount percentage/amount"
msgstr ""

#: plugin-options/auctions-options.php:25
msgid "Not Started"
msgstr ""

#: plugin-options/auctions-options.php:26
msgid "Started"
msgstr ""

#: plugin-options/auctions-options.php:27
msgid "Finished"
msgstr ""

#: plugin-options/auctions-options.php:32
msgid "Auction Badges"
msgstr ""

#: plugin-options/auctions-options.php:34
msgid "Select the badges for auction statuses"
msgstr ""

#: plugin-options/auctions-options.php:42
#, php-format
msgid "Select the badge for all auctions marked as \"%s\""
msgstr ""

#: plugin-options/category-options.php:63
#, php-format
msgid "Select the Badge for all products of category %s"
msgstr ""

#: plugin-options/dynamic-pricing-options.php:31
msgid "Dynamic Pricing Settings"
msgstr ""

#: plugin-options/dynamic-pricing-options.php:32
#, php-format
msgid ""
"Select the Badge for Dynamic Pricing Rules. Please Note: you should create rules "
"in %s before"
msgstr ""

#: plugin-options/dynamic-pricing-options.php:36
msgid "Dynamic Pricing Badges"
msgstr ""

#: plugin-options/dynamic-pricing-options.php:50
#, php-format
msgid "Select the Badge for all products of Dynamic Pricing Rule %s"
msgstr ""

#: plugin-options/shipping-class-options.php:59
#, php-format
msgid "Select the Badge for all products of shipping class \"%s\""
msgstr ""

#: templates/advanced_sale_badges.php:117
msgctxt "Text in badge: preserve lenght"
msgid "On Sale"
msgstr ""

#: templates/advanced_sale_badges.php:118
msgctxt "Text in badge: preserve lenght"
msgid "Off"
msgstr ""

#: templates/advanced_sale_badges.php:120
msgctxt "Text in badge: preserve lenght"
msgid "Save"
msgstr ""

#: templates/advanced_sale_badges.php:121
#, php-format
msgctxt "Text in badge(preserve lenght): Save $9"
msgid "%1$s %2$s"
msgstr ""

#: templates/advanced_sale_badges.php:122
msgctxt "Text in badge: preserve lenght"
msgid "Sale!"
msgstr ""

#: templates/advanced_sale_badges.php:123
#, php-format
msgctxt "Text in badge(preserve lenght): 9 $"
msgid "%1$s%2$s"
msgstr ""
