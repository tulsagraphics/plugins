=== YITH WooCommerce Name Your Price ===

Contributors: yithemes
Tags: WooCommerce, ecommerce, name your price, WooCommerce name your price, pay a coffe, shop, yith, yit, yithemes
Requires at least: 3.5.1
Tested up to: 4.9.6
Stable tag: 1.0.22
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Give your users the freedom to choose how much they want to pay with YITH WooCommerce Name Your Price.

== Description ==

What's better than let your customers decide how much they would like to spend? Think about it: whether they are purchasing a song or a book, the emotional attachment to that particular product could encourage them to spend more than the regular price you would expect.
But think about the exact opposite: maybe offering something completely free to catch you users' attention could be also a powerful marketing action, so that they will decide freely to pay you up for something you would also give for free. YITH WooCommerce Name Your Price will unchain you and your customers from fixed prices for your products: feel free to set it on the product you want and see how much your customers would like to spend for it!


== Changelog ==
= 1.0.22 =

* New: Support to WooCommerce 3.4.2
* New: Support to WordPress 4.9.6
* Tweak: Show price range for variable products
* Update: Language files
* Update: Plugin Framework

= 1.0.21 =
* New: Support to YITH Frontend Manager for WooCommerce Premium v1.4.2
* New: Support to WooCommerce 3.3.3
* Update: Plugin Framework

= 1.0.20 =
* New: Support to WooCommerce 3.3.1
* New: Support to WordPress 4.9.4
* Update: Plugin Framework
* Fix: Integration with MultiVendor

= 1.0.19 =
* New: Support to WooCommerce 3.3.0 RC2
* New: Support to WordPress 4.9.2
* Update: Plugin Framework

= 1.0.18 =
* New: Support to WooCommerce 3.2.6
* Update: Plugin Framework 3.0.5
* Fix: Wrong price shown on the single product page
* Fix: Formatting nameyourprice string on shop page

= 1.0.17 =
* New: Support to WordPress 4.9.1
* Update: Plugin Framework 3.0.1
* Tweak: Rounded-up price displaying on frontend

= 1.0.16 =
* New: Support to WooCommerce 3.2.5
* Update: Plugin Framework
* Fix: Support to Aelia Currency Switcher for WooCommerce ( version 4.5.4.171109 )

= 1.0.15 =
* New: Support to WooCommerce 3.2.0
* New: Dutch language file
* Tweak: Show minimum price allowed or maximum price allowed in notices
* Update: Plugin Framework

= 1.0.14 =
* New: Support to WooCommerce 2.7.0-RC1
* Update: Plugin Framework

= 1.0.13 =

* New: Spanish language file
* Update: Plugin Framework

= 1.0.12 =
* New: Support to WordPress 4.7
* New: Support to WooCommerce 2.6.11
* Update: Plugin Framework

= 1.0.11 =

* Fix: Double price in mini-cart with YITH The Polygon Theme
* Fix: Allowed multiple add-to-cart if product is sold individually
* Fix: Other minor bug
* Update: Plugin Framework

= 1.0.10 =

* New: Compatibility with Aelia Currency Switcher for WooCommerce
* Update: Plugin Framework

= 1.0.9 =

* New: Compatibility with WooCommerce 2.6 RC1
* New: Italian Language file
* Fix: JS Error in products where "nameyourprice" checkbox is left unchecked
* Update: Plugin Framework

= 1.0.8
* Update: Plugin Framework
* Fix: Empty regular price in product variation

= 1.0.7 =

* Update: Plugin Framework
* Fix : Show price range for variable product

= 1.0.6 =

* Fix: Disabled ajax add to cart for "name your price" product
* Update: Language File
* Update: Plugin Framework

= 1.0.5 =

* New: Compatibility with YITH WooCommerce New to Cart Popup Premium

= 1.0.4 =

* New: Compatibility with WooCommerce Multilingual (multi-currency support)
* Update: Plugin Framework
* Fix: Minor bug

= 1.0.3 =

* Fix: icon sale visible when product is "name your price"
* Fix : strikethrough line missing when product is on sale

= 1.0.2 =

* Fix: Invalid price when the Decimal Separator is a comma
* Update : Plugin Framework

= 1.0.1 =

* Initial release

== Upgrade Notice ==

Last Stable Tag 1.0.15
