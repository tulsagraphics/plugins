<?php
if ( !defined( 'ABSPATH' ) )
    exit;


if ( !function_exists( 'ywcnp_json_search_product_categories' ) ) {
    /** search product category by term
     * @author YITHEMES
     * @since 1.0.0
     * @param string $x
     * @param array $taxonomy_types
     */
    function ywcnp_json_search_product_categories( $x = '', $taxonomy_types = array( 'product_cat' ) )
    {


            global $wpdb;
            $term = (string) urldecode( stripslashes( strip_tags( $_GET[ 'term' ] ) ) );
            $term = "%" . $term . "%";

            $query_cat = $wpdb->prepare( "SELECT {$wpdb->terms}.term_id,{$wpdb->terms}.name, {$wpdb->terms}.slug
                                   FROM {$wpdb->terms} INNER JOIN {$wpdb->term_taxonomy} ON {$wpdb->terms}.term_id = {$wpdb->term_taxonomy}.term_id
                                   WHERE {$wpdb->term_taxonomy}.taxonomy IN (%s) AND {$wpdb->terms}.slug LIKE %s", implode( ",", $taxonomy_types ), $term );

            $product_categories = $wpdb->get_results( $query_cat );

            $to_json = array();

            foreach ( $product_categories as $product_category ) {

                $to_json[ $product_category->term_id ] = "#" . $product_category->term_id . "-" . $product_category->name;
            }

            wp_send_json( $to_json );

    }
}
add_action( 'wp_ajax_yith_json_search_product_categories', 'ywcnp_json_search_product_categories', 10 );


add_filter( 'ywcnp_product_types', 'ywcnp_premium_product_type_allowed' );

if ( !function_exists( 'ywcnp_premium_product_type_allowed' ) ) {

    /** add premium product type allowed
     * @author YITHEMES
     * @since 1.0.0
     * @param $types
     * @return array
     */
    function  ywcnp_premium_product_type_allowed( $types )
    {

        $new_type = array( 'variable','variation', 'grouped' );

        return array_merge( $types, $new_type );
    }

}

add_filter( 'ywcnp_add_error_message', 'ywcnp_add_premium_message' );

if ( !function_exists( 'ywcnp_add_premium_message' ) ) {
    /** add premium message error
     * @author YITHEMES
     * @since 1.0.0
     * @param $messages
     * @return mixed
     */
    function ywcnp_add_premium_message( $messages )
    {

        $messages[ 'min_error' ] = get_option( 'ywcnp_min_price_error_label' );
        $messages[ 'max_error' ] = get_option( 'ywcnp_max_price_error_label' );

        return $messages;
    }
}

if ( !function_exists( 'ywcnp_get_suggest_price' ) ) {
    /** get suggested price
     * @author YITHEMES
     * @since 1.0.0
     * @param $product_id
     * @return float
     */
    function ywcnp_get_suggest_price( $product_id )
    {
        global $woocommerce_wpml, $sitepress;
        if ( isset( $sitepress ) ) {
            $product_id = apply_filters( 'translate_object_id', $product_id, get_post_type( $product_id ), false, $sitepress->get_default_language() );
        }
        
        $product = wc_get_product( $product_id );

        $sugg_price = yit_get_prop( $product, '_ywcnp_simple_suggest_price' );

        if ( isset( $woocommerce_wpml ) ) {

            $sugg_price = apply_filters( 'wcml_raw_price_amount', $sugg_price );
        }

        return apply_filters( 'ywcnp_get_suggest_price',$sugg_price,  $product );
    }
}

if ( !function_exists( 'ywcnp_get_min_price' ) ) {
    /**
     * get min price
     * @author YITHEMES
     * @since 1.0.0
     * @param int $product_id
     * @return float
     */
    function ywcnp_get_min_price( $product_id )
    {
        global $woocommerce_wpml, $sitepress;
     

        if ( isset( $sitepress ) ) {
            $product_id = apply_filters( 'translate_object_id', $product_id, get_post_type( $product_id ), false, $sitepress->get_default_language() );
        }

        $product = wc_get_product( $product_id );
        $min_price = yit_get_prop( $product, '_ywcnp_simple_min_price' );

      
        if ( isset( $woocommerce_wpml ) ) {

            $min_price = apply_filters( 'wcml_raw_price_amount', $min_price );
        }


        return apply_filters('ywcnp_get_min_price', $min_price , $product );
    }
}

if ( !function_exists( 'ywcnp_get_max_price' ) ) {
    /**get max price
     * @author YITHEMES
     * @since 1.0.0
     * @param int $product_id
     * @return float
     */
    function ywcnp_get_max_price( $product_id )
    {
        global $woocommerce_wpml, $sitepress;

       
        if ( isset( $sitepress ) ) {
            $product_id = apply_filters( 'translate_object_id', $product_id, get_post_type( $product_id ), false, $sitepress->get_default_language() );
           
        }
        
        $product = wc_get_product( $product_id );
        $max_price = yit_get_prop( $product, '_ywcnp_simple_max_price' );
        if ( isset( $woocommerce_wpml ) ) {

            $max_price = apply_filters( 'wcml_raw_price_amount', $max_price );
        }

        return  apply_filters('ywcnp_get_max_price', $max_price,  $product );
    }
}

if ( !function_exists( 'ywcnp_get_suggest_price_html' ) ) {
    /**
     * @author YITHEMES
     * @since 1.0.0
     * @param $product_id
     * @return string suggest price html
     */
    function ywcnp_get_suggest_price_html( $product_id )
    {
        $product = wc_get_product( $product_id );
        $sugg_price =  wc_format_decimal( ywcnp_get_suggest_price( $product_id ) ) ;
        $min_price = wc_format_decimal( ywcnp_get_min_price( $product_id ) );
        $price_html = '';
        $price = '';
        if( !empty( $sugg_price ) ){

            $price =  wc_price( $sugg_price ) ;
            $price_html = get_option( 'ywcnp_suggest_price_label' );

        }elseif( !empty( $min_price ) ){

            $price = wc_price( $min_price );
            $price_html = get_option( 'ywcnp_min_price_label' );
        }else{
            $price = '';
            $price_html = apply_filters( 'ywcnp_from_free_label' , __('From Free!', 'yith-woocommerce-name-your-price') );
        }


        if ( ywcnp_product_has_subscription( $product ) ) {
            $price = ywcnp_get_price_subscription( $product, $price );
        }

        $price = sprintf( '<span class="ywcnp_suggest_label">%s %s</span>', $price_html , $price );
        return $price;
    }
}

if ( !function_exists( 'ywcnp_get_min_price_html' ) ) {
    /**
     * @author YITHEMES
     * @since 1.0.0
     * @param $product_id
     * @return string min price html
     */
    function ywcnp_get_min_price_html( $product_id )
    {
        $product = wc_get_product( $product_id );
        $min_price =  wc_format_decimal( ywcnp_get_min_price( $product_id ) );

        if ( empty( $min_price ) )
            return '<span class="amount"></span>';

        if ( ywcnp_product_has_subscription( $product) ) {
            
            $min_price = ywcnp_get_price_subscription( $product, wc_price( $min_price ) );
        }
        else {
            $min_price = wc_price( $min_price );
        }
        $price = sprintf( '<span>%s%s</span>', get_option( 'ywcnp_min_price_label' ), $min_price );

        return $price;

    }
}

if ( !function_exists( 'ywcnp_get_max_price_html' ) ) {
    /**
     * @author YITHEMES
     * @since 1.0.0
     * @param $product_id
     * @return string max price html
     */
    function ywcnp_get_max_price_html( $product_id )
    {
        $product = wc_get_product( $product_id );
        $max_price =  wc_format_decimal( ywcnp_get_max_price( $product_id ) );

        if ( empty( $max_price ) )
            return '<span class="amount"></span>';

        if ( ywcnp_product_has_subscription( $product ) ) {
           
            $max_price = ywcnp_get_price_subscription( $product, wc_price( $max_price ) );
        }
        else {
            $max_price = wc_price( $max_price );
        }
        $price = sprintf( '<span>%s%s</span>', get_option( 'ywcnp_max_price_label' ), $max_price );

        return $price;
    }
}

if ( !function_exists( 'ywcnp_product_has_subscription' ) ) {
    /**
     * check if product has subscription
     * @author YITHEMES
     * @since 1.0.0
     * @param WC_Product $product
     * @return bool
     */
    function ywcnp_product_has_subscription( $product )
    {
        return ( defined( 'YITH_YWSBS_PREMIUM' ) && class_exists( 'YITH_WC_Subscription' ) && 'yes' == yit_get_prop( $product, '_ywsbs_subscription' ) );
    }
}


if ( !function_exists( 'ywcnp_product_has_rule' ) ) {
    /**
     * check if a category has a "name your price" rule
     * @author YITHEMES
     * @sincr 1.0.0
     * @param $product_id
     * @return string
     */
    function ywcnp_product_has_rule( $product_id )
    {

        $last_category_rule_id = '';
        $categories = wp_get_object_terms( $product_id, 'product_cat', array( 'fields' => 'ids' ) );
        $categories_ids = ywcnp_get_category_ids_rule_vendor();
        foreach ( $categories as $category_id ) {

            $has_rule = !ywcnp_is_multivendor_active() ? ywcnp_get_woocommerce_term_meta( $category_id, '_ywcnp_enable_rule', true ) == 'yes' : in_array( $category_id, $categories_ids );

            if ( 'yes' == $has_rule )
                $last_category_rule_id = $category_id;
        }

        return $last_category_rule_id;
    }
}

if ( !function_exists( 'ywcnp_get_price_subscription' ) ) {
    /**
     * get price in subscription format
     *
     * @author YITHEMES
     * @since 1.0.0
     * @param $product
     * @param $price
     * @return string
     */
    function ywcnp_get_price_subscription( $product, $price )
    {
       
        $price =YITH_WC_Subscription()->change_price_html( $price, $product );

       // $price_is_per = get_post_meta( $product_id, '_ywsbs_price_is_per', true );
        //$price_time_option = get_post_meta( $product_id, '_ywsbs_price_time_option', true );

       // $price .= ' / ' . $price_is_per . ' ' . $price_time_option;

        return $price;
    }
}

if ( !function_exists( 'ywcnp_is_multivendor_active' ) ) {
    /**
     * check if YITH WooCommerce Multivendor Premium is active
     * @author YITHEMES
     * @since 1.0.0
     * @return bool
     */
    function ywcnp_is_multivendor_active()
    {

        return defined( 'YITH_WPV_PREMIUM' ) && YITH_WPV_PREMIUM;
    }
}

if ( !function_exists( 'ywcnp_is_multivendor_name_your_price_enabled' ) ) {
    /**
     * check if Name Your Price is enabled for Vendors
     * @author YITHEMES
     * @since 1.0.0
     * @return bool
     */
    function ywcnp_is_multivendor_name_your_price_enabled()
    {
        $option = get_option( 'yith_wpv_vendors_option_name_your_price_management', 'no' );

        return $option === 'yes';
    }
}

if ( !function_exists( 'ywcnp_get_category_rule_vendor' ) ) {
    /**
     * @author YITHEMES
     * @since 1.0.0
     * @param $vendor_id
     * @param $category_id
     * @return bool | array
     */
    function ywcnp_get_category_rule_vendor( $category_id, $field )
    {

        if ( ywcnp_is_multivendor_active() ) {
            $vendor = yith_get_vendor( 'current', 'user' );
            $vendor_id = $vendor->id;

            $rules = get_user_meta( $vendor_id, '_ywcnp_vendor_cat_rules', true );

            $single_rule = false;

            if ( $rules ) {
                foreach ( $rules as $key => $rule ) {
                    if ( $rule[ 'category' ] == $category_id ) {
                        $single_rule = $rules[ $key ];
                        continue;
                    }
                }
            }

            if ( $single_rule ) {

                return $single_rule[ $field ];
            }

            return false;
        } else
            return false;

    }
}

if ( !function_exists( 'ywcnp_get_category_ids_rule_vendor' ) ) {
    /**
     * return all category ids set in rules vendor
     * @author YITHEMES
     * @since 1.0.0
     * @return array
     */
    function ywcnp_get_category_ids_rule_vendor()
    {

        if ( ywcnp_is_multivendor_active() ) {
            $vendor = yith_get_vendor( 'current', 'user' );
            $vendor_id = $vendor->id;

            $category_ids = array();
            $rules = get_user_meta( $vendor_id, '_ywcnp_vendor_cat_rules', true );

            if ( $rules ) {
                foreach ( $rules as $key => $rule ) {
                    if ( $rule[ 'enabled' ] == 'yes' )
                        $category_ids[] = $rule[ 'category' ];
                }
            }

            return $category_ids;
        }
        return array();
    }

}

if ( !function_exists( 'ywcnp_save_product_meta_global_rule' ) ) {
    /** save product meta for global category rules
     * @author YITHEMES
     * @since 1.0.0
     * @param array $args
     */
    function ywcnp_save_product_meta_global_rule( $args = array() )
    {

        $default = array(
            'product_id' => '',
            'sugg_price' => '',
            'min_price' => '',
            'max_price' => '',
            'is_override' => 'no'
        );


        $default = wp_parse_args( $args, $default );
       
        extract( $default );

        $product = wc_get_product( $product_id );
        $product_meta = false;


        if ( $product->is_type('simple')) {

            $is_override = yit_get_prop( $product, '_ywcnp_simple_is_override' );
            $is_override = $is_override ? $is_override : 'no';

            if ( 'no' == $is_override ) {
                $product_meta = array(
                    '_ywcnp_enabled_product' => 'yes',
                    '_is_nameyourprice' => true,
                    '_ywcnp_simple_suggest_price' => $sugg_price,
                    '_ywcnp_simple_min_price' => $min_price,
                    '_ywcnp_simple_max_price' => $max_price,
                    '_ywcnp_simple_is_override' => 'no'
                );
            }

        } elseif ( $product->is_type('variation') ) {

            $is_override = yit_get_prop( $product, '_ywcnp_variation_is_override' );
            $is_override = $is_override ? $is_override : 'no';

            if ( 'no' == $is_override ) {
                $product_meta = array(
                    '_ywcnp_enabled_variation' => 'yes',
                    '_is_nameyourprice' => true,
                    '_ywcnp_simple_suggest_price' => $sugg_price,
                    '_ywcnp_simple_min_price' => $min_price,
                    '_ywcnp_simple_max_price' => $max_price,
                    '_ywcnp_variation_is_override' => 'no',
                );

                $parent_id = yit_get_base_product_id( $product );
                $parent_product = wc_get_product( $parent_id );
              
                yit_save_prop( $parent_product, '_variation_has_nameyourprice', true );
            }
        }

        if ( $product_meta && !$product->is_type('variable') ) {
           
            yit_save_prop( $product, $product_meta );
        }


    }
}

if ( !function_exists( 'ywcnp_remove_product_meta_global_rule' ) ) {
    /**
     * remove product meta for global category rule
     * @author YITHEMES
     * @since 1.0.0
     * @param $product_id
     */
    function ywcnp_remove_product_meta_global_rule( $product_id )
    {

        $product = wc_get_product( $product_id );
        $product_meta = false;

        if ( $product->is_type('simple' ) ) {

            $is_override = yit_get_prop( $product, '_ywcnp_simple_is_override' );
            $is_override = $is_override ? $is_override : 'no';

            if ( 'no' == $is_override ) {
                $product_meta = array(
                    '_ywcnp_enabled_product' ,
                    '_is_nameyourprice' ,
                    '_ywcnp_simple_is_override' 
                );
            }
        } elseif ( $product->is_type('variation') ) {

            $is_override = yit_get_prop( $product, '_ywcnp_variation_is_override', true );
            $is_override = $is_override ? $is_override : 'no';

            if ( 'no' == $is_override ) {
                $product_meta = array(
                    '_ywcnp_enabled_variation' ,
                    '_is_nameyourprice' ,
                    '_ywcnp_variation_is_override' ,
                );

                $parent_id = yit_get_base_product_id( $product );
                $parent_product = wc_get_product( $parent_id );
                yit_delete_prop( $parent_product, '_variation_has_nameyourprice' );
            }

        }

        if ( $product_meta ) {
            foreach ( $product_meta as $meta ) {
                yit_delete_prop( $product, $meta );
            }
        }
    }
}

if ( !function_exists( 'ywcnp_get_product_id_by_category' ) ) {
    /**
     * get all product_id
     * @author YITHEMES
     * @since 1.0.0
     * @param $category_ids
     * @return array
     */
    function ywcnp_get_product_id_by_category( $category_ids )
    {

        $product_ids = array();
        if ( ywcnp_is_multivendor_active() && !( current_user_can( 'edit_users' ) ) ) {

            $vendor = yith_get_vendor( 'current', 'user' );

            if ( $vendor->is_valid() && $vendor->has_limited_access() ) {

                $products = $vendor->get_products();
                if ( is_array( $products ) && sizeof( $products ) > 0 ) {

                    foreach ( $products as $product_id ) {


                        $product = wc_get_product( $product_id );

                        $product_categories = wc_get_product_terms( $product_id, 'product_cat', array( 'fields' => 'ids' ) );


                        if ( !in_array( $category_ids, $product_categories ) )
                            continue;

                        if ( $product->is_type( 'variable' ) ) {

                            if ( $product->has_child() ) {

                                $childs = $product->get_children();
                                foreach ( $childs as $child ) {
                                    $product_ids[] = $child;

                                }
                            }
                        } else
                            $product_ids[] = $product_id;
                    }

                }

            }
        } else {
            $product_ids = array();

            $args = array(
                'post_type' => 'product',
                'post_status' => 'publish',
                'post_parent' => 0,
                'posts_per_page' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'id',
                        'terms' => explode( ',', $category_ids ),
                    ),
                )
            );

            wp_reset_query();

            $query = new WP_Query( $args );

            if ( $query->have_posts() ) {

                while ( $query->have_posts() ) {

                    $query->the_post();

                    $product_id = $query->post->ID;
                    $product = wc_get_product( $product_id );

                    if ( $product->is_type( 'variable' ) ) {

                        if ( $product->has_child() ) {

                            $childs = $product->get_children();
                            foreach ( $childs as $child ) {
                                $product_ids[] = $child;

                            }
                        }
                    } else {
                        $product_ids[] = $product_id;
                    }
                }

            }

            wp_reset_postdata();
        }
        return $product_ids;
    }
}


if( !function_exists('ywcnp_get_woocommerce_term_meta')){
    /**
     * @author YITHEMES
     * @since 1.0.10
     * @param $term_id
     * @param $key
     * @param $single
     * @return term_meta
     */
    function ywcnp_get_woocommerce_term_meta( $term_id, $key, $single = true ){
        $is_wc_lower_2_6 = version_compare( WC()->version, '2.6', '<' );


        return ! $is_wc_lower_2_6 ? get_term_meta( $term_id, $key, $single ) : get_metadata( 'woocommerce_term', $term_id, $key, $single );
    }
}

if( !function_exists('ywcnp_update_woocommerce_term_meta')){
    /**
     * @author YITHEMES 
     * @since 1.0.10
     * @param $term_id
     * @param $meta_key
     * @param $meta_value
     * @param string $prev_value
     * @return bool|int|WP_Error
     */
    function ywcnp_update_woocommerce_term_meta( $term_id, $meta_key, $meta_value, $prev_value = '' ){
        $is_wc_lower_2_6 = version_compare( WC()->version, '2.6', '<' );
        return ! $is_wc_lower_2_6 ? update_term_meta( $term_id, $meta_key, $meta_value, $prev_value ) : update_metadata( 'woocommerce_term', $term_id, $meta_key, $meta_value, $prev_value );

    }
}

