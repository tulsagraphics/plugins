<?php
if ( !defined( 'ABSPATH' ) )
    exit;


if ( !class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}


if ( !class_exists( 'YITH_Category_Rule_Vendor_Table' ) ) {
    /**
     * Class YITH_Category_Rule_Vendor_Table
     *
     * list vendor category rules
     *
     * array(
     *
     *  array( 'category' => '',
     *         'sugg_price' => '',
     *         'min_price'  => '',
     *         'max_price' => '',
     *         'enabled'   => 'no'
     *      ),
     *  .......
     * )
     *
     */
    class YITH_Category_Rule_Vendor_Table extends WP_List_Table
    {
        /**
         * @var int, current vendor id
         */
        public $vendor_id;

        /**
         * __construct function
         */
        public function __construct()
        {

            global $status, $page;
            parent::__construct( array(
                'singular' => _x( 'rule', 'yith-woocommerce-name-your-price' ),     //singular name of the listed records
                'plural' => _x( 'rules', 'yith-woocommerce-name-your-price' ),    //plural name of the listed records
                'ajax' => false        //does this table support ajax?
            ) );
        }

        /**
         * @author YITHEMES
         * @since 1.0.0
         * @param object $item
         * @param string $column_name
         * @return mixed
         */
        public function column_default( $item, $column_name )
        {

            switch ( $column_name ) {

                case  'sugg_price':
                case  'min_price' :
                case  'max_price':
                    return empty( $item[ $column_name ] ) ? '<span>' . __( 'No Price', 'yith-woocommece-name-your-price' ) . '</span>' : wc_price( $item[ $column_name ] );
                    break;
                case 'enabled':
                    $is_enabled = $item[ $column_name ];
                    if ( $is_enabled == 'yes' ) {
                        $class = 'show';
                        $tip = __( 'Enabled', 'yith-woocommece-name-your-price' );
                    } else {
                        $class = 'hide';
                        $tip = __( 'Disabled', 'yith-woocommece-name-your-price' );
                    }

                    return sprintf( '<mark class="%s tips" data-tip="%s">%s</mark>', $class, $tip, $tip );
                    break;
                default:
                    return print_r( $item, true );
            }
        }

        /** get column category
         * @author YITHEMES
         * @since 1.0.0
         * @param $item
         * @return string
         */
        public function column_category( $item )
        {

            $edit_query_args = array(
                'page' => $_GET[ 'page' ],
                'action' => 'edit',
                'id' => $item[ 'category' ]
            );
            $delete_query_args = array(
                'page' => $_GET[ 'page' ],
                'action' => 'delete',
                'id' => $item[ 'category' ]
            );
            $edit_url = esc_url( add_query_arg( $edit_query_args, admin_url( 'admin.php' ) ) );
            $delete_url = esc_url( add_query_arg( $delete_query_args, admin_url( 'admin.php' ) ) );
            $actions = array(
                'edit' => '<a href="' . $edit_url . '">' . __( 'Edit rule', 'yith-woocommece-name-your-price' ) . '</a>',
                'delete' => '<a href="' . $delete_url . '">' . __( 'Remove rule from list', 'yith-woocommece-name-your-price' ) . '</a>',
            );

            $category_id = $item[ 'category' ];
            $category_name = get_term_by( 'term_id', $category_id, 'product_cat' );

            return sprintf( '<strong><a class="tips" href="%s" data-tip="%s">#%d %s </a></strong> %s',
                $edit_url, __( 'Edit rule', 'yith-woocommece-name-your-price' ), $category_id, $category_name->name, $this->row_actions( $actions ) );
        }

        /**
         * get column checkbox
         * @author YITHEMES
         * @since 1.0.0
         * @param object $item
         * @return string
         */
        public function column_cb( $item )
        {
            return sprintf(
                '<input type="checkbox" name="id[]" value="%1$s" />',
                /*$2%s*/
                $item[ 'category' ]                //The value of the checkbox should be the record's id
            );
        }

        /**
         * get columns
         * @author YITHEMES
         * @since 1.0.0
         * @return array
         */
        public function get_columns()
        {
            $columns = array(
                'cb' => '<input type="checkbox" />', //Render a checkbox instead of text
                'category' => __( 'Category', 'yith-woocommece-name-your-price' ),
                'sugg_price' => __( 'Suggested Price', 'yith-woocommece-name-your-price' ),
                'min_price' => __( 'Minimum Price', 'yith-woocommece-name-your-price' ),
                'max_price' => __( 'Maximum Price', 'yith-woocommece-name-your-price' ),
                'enabled' => __( 'Enabled', 'yith-woocommece-name-your-price' )
            );
            return $columns;
        }

        /** get sortable columns
         * @author YITHEMES
         * @since 1.0.0
         * @return array
         */
        public function get_sortable_columns()
        {
            $sortable_columns = array(
                'category' => array( 'category', false ),     //true means it's already sorted
            );
            return $sortable_columns;
        }

        /** get bulck actions
         * @author YITHEMES
         * @since 1.0.0
         * @return array
         */
        function get_bulk_actions()
        {
            $actions = array(
                'delete' => 'Delete'
            );
            return $actions;
        }


        /**
         * add delete bulk action
         * @author YITHEMES
         * @since 1.0.0
         */
        public function process_bulk_action()
        {

            //Detect when a bulk action is being triggered...
            if ( 'delete' === $this->current_action() ) {

                if ( isset( $_GET[ 'id' ] ) ) {

                    $items = get_user_meta( $this->vendor_id, '_ywcnp_vendor_cat_rules', true );
                    $items = $items ? $items : array();

                    $ids = is_array( $_GET[ 'id' ] ) ? $_GET[ 'id' ] : array( $_GET[ 'id' ] );

                    foreach ( $ids as $id ) {
                        foreach ( $items as $key => $item ) {

                            if ( $item[ 'category' ] == $id ) {
                                unset( $items[ $key ] );
                                continue;
                            }
                        }
                    }

                    update_user_meta( $this->vendor_id, '_ywcnp_vendor_cat_rules', $items );
                }
            }

        }

        /**
         * prepare items
         * @author YITHEMES
         * @since 1.0.0
         */
        public function prepare_items()
        {

            $per_page = 10;

            $columns = $this->get_columns();
            $hidden = array();
            $sortable = $this->get_sortable_columns();
            $this->_column_headers = array( $columns, $hidden, $sortable );
            $this->process_bulk_action();

            $items = get_user_meta( $this->vendor_id, '_ywcnp_vendor_cat_rules', true );
            $items = $items ? $items : array();

            function ywcnp_usort_reorder( $a, $b )
            {
                $orderby = ( !empty( $_REQUEST[ 'orderby' ] ) ) ? $_REQUEST[ 'orderby' ] : 'category'; //If no sort, default to category name
                $order = ( !empty( $_REQUEST[ 'order' ] ) ) ? $_REQUEST[ 'order' ] : 'asc'; //If no order, default to asc
                $result = strcmp( $a[ $orderby ], $b[ $orderby ] ); //Determine sort order
                return ( $order === 'asc' ) ? $result : -$result; //Send final sort direction to usort
            }

            usort( $items, 'ywcnp_usort_reorder' );

            $current_page = $this->get_pagenum();
            $total_items = count( $items );

            $items = array_slice( $items, ( ( $current_page - 1 ) * $per_page ), $per_page );

            /**
             * REQUIRED. Now we can add our *sorted* data to the items property, where
             * it can be used by the rest of the class.
             */
            $this->items = $items;


            /**
             * REQUIRED. We also have to register our pagination options & calculations.
             */
            $this->set_pagination_args( array(
                'total_items' => $total_items,                  //WE have to calculate the total number of items
                'per_page' => $per_page,                     //WE have to determine how many items to show on a page
                'total_pages' => ceil( $total_items / $per_page )   //WE have to calculate the total number of pages
            ) );
        }
    }
}