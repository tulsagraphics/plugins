<?php
if ( !defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

if ( !class_exists( 'YITH_WC_Name_Your_Price_Multivendor_Compatibility' ) ) {
    /**
     * Multivendor Compatibility Class
     *
     * @class   YITH_WC_Name_Your_Price_Multivendor_Compatibility
     * @package YITHEMES
     * @since   1.0.2
     * @author  YITHEMES
     *
     */
    class YITH_WC_Name_Your_Price_Multivendor_Compatibility
    {

        /**
         * Single instance of the class
         *
         * @var \YITH_WC_Name_Your_Price_Multivendor_Compatibility
         * @since 1.0.2
         */
        protected static $instance;


        /**
         * Returns single instance of the class
         *
         * @return \YITH_WC_Surveys_Multivendor_Compatibility
         * @since 1.0.2
         */
        public static function get_instance()
        {
            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        /**
         * Constructor
         *
         * @access public
         * @since  1.0.2
         */
        public function __construct()
        {


            if ( !YITH_WC_Name_Your_Price_Compatibility::has_multivendor_plugin() )
                return;

            $this->vendor = yith_get_vendor( 'current', 'user' );

            if ( $this->vendor->is_valid() && $this->vendor->has_limited_access() && ywcnp_is_multivendor_name_your_price_enabled() ) {

                add_action( 'admin_menu', array( $this, 'add_name_your_price_tab_for_vendor' ) );
                require_once( YWCNP_TEMPLATE_PATH . 'admin/vendor/ywcnp-category-vendor-rules.php' );
                add_action( 'yith_wc_name_your_price_category_vendor_rules', array( YWCNP_Category_Vendor_Rule(), 'output' ) );
                add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts'));
            }

        }

        public function enqueue_scripts(){
	        wp_enqueue_script( 'selectWoo' );
        	wp_enqueue_script('wc-enhanced-select');
	        wp_enqueue_style( 'woocommerce_admin_styles' );
        }
        /**
         * @author YITHEMES
         * @since 1.0.2
         */
        public function add_name_your_price_tab_for_vendor()
        {

            $tabs[ 'category-rules-vendor' ] = __( 'Active Rules', 'yith-woocommerce-name-your-price' );

            $args = array(
                'create_menu_page' => false,
                'parent_slug' => '',
                'page_title' => __( 'Name Your Price', 'yith-woocommerce-name-your-price' ),
                'menu_title' => __( 'Name Your Price', 'yith-woocommerce-name-your-price' ),
                'capability' => 'manage_vendor_store',
                'parent' => 'vendor_' . $this->vendor->id,
                'parent_page' => '',
                'page' => 'yith_vendor_nyp_settings',
                'admin-tabs' => $tabs,
                'options-path' => YWCNP_DIR . 'plugin-options/vendor',
                'icon_url' => 'dashicons-admin-plugins',
                'position' => 40
            );

            /* === Fixed: not updated theme/old plugin framework  === */
            if ( !class_exists( 'YIT_Plugin_Panel' ) ) {
                require_once( YWCNP_DIR . 'plugin-fw/lib/yit-plugin-panel.php' );
            }

            $this->_vendor_panel = new YIT_Plugin_Panel( $args );
        }

    }
}

/**
 * Unique access to instance of YITH_WC_Name_Your_Price_Multivendor_Compatibility class
 *
 * @return YITH_WC_Name_Your_Price_Multivendor_Compatibility
 * @since 1.0.0
 */
function YITH_Name_Your_Price_Multivendor_Compatibility()
{
    return YITH_WC_Name_Your_Price_Multivendor_Compatibility::get_instance();
}

YITH_Name_Your_Price_Multivendor_Compatibility();