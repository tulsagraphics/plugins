<?php
if ( !defined( 'ABSPATH' ) )
    exit;

if ( !class_exists( 'YITH_WC_Name_Your_Price_Premium_Admin' ) ) {
    /**
     * implement premium admin features
     * Class YITH_WC_Name_Your_Price_Admin
     */
    class YITH_WC_Name_Your_Price_Premium_Admin extends YITH_WC_Name_Your_Price_Admin
    {

        /**
         * @var YITH_WC_Name_Your_Price_Premium_Admin , single instance
         */
        protected static $instance;

        /**
         * __construct function
         * @author YITHEMES
         * @since 1.0.0
         */
        public function __construct()
        {

            parent::__construct();

            remove_action( 'woocommerce_product_options_pricing', array( $this, 'add_option_general_product_data' ) );
            //add premium script
            add_action( 'admin_enqueue_scripts', array( $this, 'include_premium_scripts' ) );
            //add premium style
            add_action( 'admin_enqueue_scripts', array( $this, 'include_premium_styles' ) );

            // print category rule table
            add_action( 'yith_wc_name_your_price_category_rules', array( YWCNP_Category_Rule(), 'output' ) );

            //add name your price tab in single product
            add_action( 'woocommerce_product_options_general_product_data', array( $this, 'yith_name_your_price_tab_content' ) );

            //save product meta for simple product
            add_filter( 'ywcnp_add_premium_single_meta', array( $this, 'save_product_meta' ), 10, 2 );


            //manage product variation with nameyourprice
            add_action( 'woocommerce_variation_options', array( $this, 'add_name_your_price_variation_option' ), 20, 3 );
            add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'add_name_your_price_variable_attributes' ), 13, 3 );
            add_action( 'woocommerce_save_product_variation', array( $this, 'save_product_variation_meta' ), 10, 2 );

            // Enable or disable name your price option in product
            add_filter( 'ywcnp_product_name_your_price_option_enabled', array( $this, 'filter_name_your_price_option_enabled' ), 10, 2 );

        }

        /**
         * Enable or disable name your price option in product
         *
         * @param bool $enabled
         * @param int $product_id the id of the product
         *
         * @return bool
         *
         * @author YITHEMES
         * @since 1.0.0
         */
        public function filter_name_your_price_option_enabled( $enabled, $product_id )
        {
            if ( ywcnp_is_multivendor_active() ) {
                $vendor = yith_get_vendor( 'current', 'user' );
                if ( $vendor->is_valid() && $vendor->has_limited_access() ) {
                    $product = wc_get_product( $product_id );
                    if ( !ywcnp_is_multivendor_name_your_price_enabled() && !ywcnp_product_is_name_your_price( $product ) ) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }

            return $enabled;
        }

        /**
         * include premium scripts
         * @author YITHEMES
         * @since 1.0.0
         */
        public function include_premium_scripts()
        {

            $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

            wp_enqueue_script( 'ywcnp_premium_script', YWCNP_ASSETS_URL . 'js/ywcnp_premium_admin' . $suffix . '.js', array( 'jquery' ), YWCNP_VERSION, true );
        }

        /**
         * include premium styles
         * @author YITHEMES
         * @since 1.0.0
         */
        public function include_premium_styles()
        {

            wp_enqueue_style( 'ywcnp_admin_style', YWCNP_ASSETS_URL . 'css/ywcnp_admin_style.css', array(), YWCNP_VERSION );
        }


        /** add name your price tab
         *
         * @param $product_data_tabs
         * @return array
         */
        public function yith_name_your_price_tab( $product_data_tabs )
        {

            $product_data_tabs [ 'nameyourprice' ] = array(
                'label' => __( 'Name Your Price', '' ),
                'target' => 'ywcnp_nameyourprice_data',
                'class' => array( 'show_if_simple', 'show_if_grouped', 'show_if_nameyourprice' )
            );

            return $product_data_tabs;
        }

        /**
         * print custom tab in product data
         * @author YITHEMES
         * @since 1.0.0
         */
        public function yith_name_your_price_tab_content()
        {

            $vendor_dir = '';

            if ( ywcnp_is_multivendor_active() ) {

                $vendor = yith_get_vendor( 'current', 'user' );

                if ( $vendor->is_valid() && $vendor->has_limited_access() )
                    $vendor_dir = 'vendor/';

            }

            ob_start();

            wc_get_template( 'metaboxes/' . $vendor_dir . 'name_your_price_tab.php', array(), '', YWCNP_TEMPLATE_PATH );
            $template = ob_get_contents();

            ob_end_clean();

            echo $template;

        }

        /**
         * add premium meta data
         * @author YITHEMES
         * @since 1.0.0
         * @param $product_meta
         * @return mixed
         */
        public function save_product_meta( $product_meta, $product_id )
        {

            $product_meta[ '_ywcnp_simple_suggest_price' ] = isset( $_REQUEST[ 'ywcnp_simple_suggest_price' ] ) ? $_REQUEST[ 'ywcnp_simple_suggest_price' ] : '';
            $product_meta[ '_ywcnp_simple_min_price' ] = isset( $_REQUEST[ 'ywcnp_simple_min_price' ] ) ? $_REQUEST[ 'ywcnp_simple_min_price' ] : '';
            $product_meta[ '_ywcnp_simple_max_price' ] = isset( $_REQUEST[ 'ywcnp_simple_max_price' ] ) ? $_REQUEST[ 'ywcnp_simple_max_price' ] : '';
            $product_meta[ '_ywcnp_simple_is_override' ] = isset( $_REQUEST[ 'ywcnp_simple_is_override' ] ) ? $_REQUEST[ 'ywcnp_simple_is_override' ] : 'no';

            $product = wc_get_product( $product_id );

            if ( $product->is_type( 'variable' ) ) {
                $is_name_your_price = false;
                $children = $product->get_children();
                foreach ( $children as $child_id ) {
                    $child = wc_get_product( $child_id );
                    if( ywcnp_product_is_name_your_price( $child ) ) {
                        $is_name_your_price = true;
                        break;
                    }
                    
                }
              
                yit_save_prop( $product, '_variation_has_nameyourprice' ,$is_name_your_price );
               

            }

            return $product_meta;
        }

        /**
         * @author YITHEMES
         * @since 1.0.0
         * @param $loop
         * @param $variation_data
         * @param WC_Product $variation
         */
        public function add_name_your_price_variation_option( $loop, $variation_data, $variation )
        {
            $product_id = yit_get_product_id( $variation );
            $enabled = apply_filters( 'ywcnp_product_name_your_price_option_enabled', true, $product_id );
            if ( !$enabled )
                return;

            $args = array( 'loop' => $loop, 'variation_data' => $variation_data, 'variation' => $variation );
            $args[ 'args' ] = $args;

            ob_start();
            wc_get_template( 'metaboxes/name_your_price_variation_option.php', $args, '', YWCNP_TEMPLATE_PATH );
            $template = ob_get_contents();
            ob_end_clean();

            echo $template;
        }

        /**
         * add metabox in edit product variable
         * @author YITHEMES
         * @since 1.0.0
         * @param $loop
         * @param $variation_data
         * @param $variation
         */
        public function add_name_your_price_variable_attributes( $loop, $variation_data, $variation )
        {

            $vendor_dir = '';

            if ( ywcnp_is_multivendor_active() ) {

                $vendor = yith_get_vendor( 'current', 'user' );

                if ( $vendor->is_valid() && $vendor->has_limited_access() )
                    $vendor_dir = 'vendor/';

            }

            $args = array( 'loop' => $loop, 'variation_data' => $variation_data, 'variation' => $variation );
            $args[ 'args' ] = $args;

            ob_start();
            wc_get_template( 'metaboxes/' . $vendor_dir . 'name_your_price_tab_variation.php', $args, '', YWCNP_TEMPLATE_PATH );
            $template = ob_get_contents();
            ob_end_clean();

            echo $template;
        }

        /**
         * save product variation meta
         * @YITHEMES
         * @since 1.0.0
         * @param $variation_id
         * @param $loop
         */
        public function save_product_variation_meta( $variation_id, $loop )
        {
            $variation_meta = array(
                '_ywcnp_enabled_variation' => isset( $_REQUEST[ 'variable_is_nameyourprice' ][ $loop ] ) ? 'yes' : 'no',
                '_ywcnp_simple_suggest_price' => isset( $_REQUEST[ 'ywcnp_variation_suggest_price' ][ $loop ] ) ? $_REQUEST[ 'ywcnp_variation_suggest_price' ][ $loop ] : '',
                '_ywcnp_simple_min_price' => isset( $_REQUEST[ 'ywcnp_variation_min_price' ][ $loop ] ) ? $_REQUEST[ 'ywcnp_variation_min_price' ][ $loop ] : '',
                '_ywcnp_simple_max_price' => isset( $_REQUEST[ 'ywcnp_variation_max_price' ][ $loop ] ) ? $_REQUEST[ 'ywcnp_variation_max_price' ][ $loop ] : '',
                '_ywcnp_variation_is_override' => isset( $_REQUEST[ 'ywcnp_variation_is_override' ][ $loop ] ) ? $_REQUEST[ 'ywcnp_variation_is_override' ][ $loop ] : 'no',
            );


            $variation = wc_get_product( $variation_id );
            
            yit_save_prop( $variation, $variation_meta );

            $is_nameyour_price = $variation_meta[ '_ywcnp_enabled_variation' ] == 'yes';
            
            yit_save_prop( $variation, '_is_nameyourprice', $is_nameyour_price );

        }

        /**
         * return single instance
         * @author YITHEMES
         * @since 1.0.0
         * @return YITH_WC_Name_Your_Price_Premium_Admin
         */
        public static function get_instance()
        {
            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }

            return self::$instance;
        }

    }
}
