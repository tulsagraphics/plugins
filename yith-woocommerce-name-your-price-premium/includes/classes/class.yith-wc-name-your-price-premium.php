<?php
if ( !defined( 'ABSPATH' ) )
    exit;


if ( !class_exists( 'YITH_WooCommerce_Name_Your_Price_Premium' ) ) {
    /**
     * implement premium features
     * @author YITHEMES
     * @since  1.0.0
     * Class YITH_WooCommerce_Name_Your_Price_Premium
     */
    class YITH_WooCommerce_Name_Your_Price_Premium extends YITH_WooCommerce_Name_Your_Price
    {

        /**
         * @var YITH_WooCommerce_Name_Your_Price_Premium, single instance
         */
        protected static $instance;

        /**
         *
         * __construct function
         */
        public function __construct()
        {

            parent::__construct();

            //manage plugin activation license
            add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
            add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );

            // add premium tabs
            add_filter( 'ywcnp_add_premium_tab', array( $this, 'add_premium_tab' ) );

            //add ajax action for load admin template
            add_action( 'wp_ajax_add_new_rule_admin', array( $this, 'add_new_rule_admin' ) );
            add_action( 'wp_ajax_nopriv_add_new_rule_admin', array( $this, 'add_new_rule_admin' ) );

            //print price for simple product
            add_filter( 'ywcnp_get_product_price_html', array( $this, 'get_nameyourprice_html' ), 20, 2 );
            add_filter( 'ywsbs_add_cart_item_data', array( $this, 'ywcnp_add_cart_item_data_subscription' ), 20, 3 );

            add_action( 'init', array( $this, 'init_multivendor_integration' ), 20 );

            //Set product variation as purchasable
            add_filter( 'woocommerce_variation_is_visible', array( $this, 'ywcnp_variation_is_visible' ), 10, 4 );
            
          if( version_compare( WC()->version, '2.7.0','>=' )  ){

              add_action( 'woocommerce_variable_product_sync_data', array( $this, 'variable_product_sync_data' ), 15  );
          }else {
              add_action( 'woocommerce_variable_product_sync', array( $this, 'variable_product_sync' ), 15, 2 );
          }
            add_filter( 'woocommerce_get_variation_price', array( $this, 'get_variation_price' ), 10, 4 );
            add_filter( 'woocommerce_get_variation_regular_price', array( $this, 'get_variation_price' ), 10, 4 );
        }


        /**
         * @author YITHEMES
         * @since  1.0.0
         *
         * @param string $price
         * @param WC_Product $product
         *
         * @return string
         */
        public function get_variation_price( $price, $product, $min_or_max, $display )
        {

            if ( ywcnp_product_is_name_your_price( $product) ) {
                $price = yit_get_prop( $product, '_' . $min_or_max . '_variation_price' );

                return $price;
            }

            return $price;
        }

        /**
         * @author YITHEMES
         * @since  1.0.0
         *
         * @param bool $visible
         * @param int $variation_id
         * @param int $parent_id
         * @param WC_Product_Variation $variation
         *
         * @return bool
         */
        public function ywcnp_variation_is_visible( $visible, $variation_id, $parent_id, $variation )
        {

            if ( ywcnp_product_is_name_your_price( $variation ) ) {
                return true;
            }

            return $visible;
        }


        /**
         * return single instance
         * @author YITHEMES
         * @since  1.0.0
         * @return YITH_WooCommerce_Name_Your_Price_Premium
         */
        public static function get_instance()
        {
            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        /** Register plugins for activation tab
         * @return void
         * @since    1.0.0
         * @author   Andrea Grillo <andrea.grillo@YITHEMES.com>
         */
        public function register_plugin_for_activation()
        {
            if ( !class_exists( 'YIT_Plugin_Licence' ) ) {
                require_once YWCNP_DIR . 'plugin-fw/licence/lib/yit-licence.php';
                require_once YWCNP_DIR . 'plugin-fw/licence/lib/yit-plugin-licence.php';
            }
            YIT_Plugin_Licence()->register( YWCNP_INIT, YWCNP_SECRET_KEY, YWCNP_SLUG );
        }

        /**
         * Register plugins for update tab
         *
         * @return void
         * @since    1.0.0
         * @author   Andrea Grillo <andrea.grillo@yithemes.com>
         */
        public function register_plugin_for_updates()
        {
            if ( !class_exists( 'YIT_Upgrade' ) ) {
                require_once( YWCNP_DIR . 'plugin-fw/lib/yit-upgrade.php' );
            }
            YIT_Upgrade()->register( YWCNP_SLUG, YWCNP_INIT );
        }

        /** add premium tabs in plugin options
         * @author YITHEMES
         * @since  1.0.0
         *
         * @param $tabs
         *
         * @return mixed
         */
        public function  add_premium_tab( $tabs )
        {

            $tabs[ 'category-rules' ] = __( 'Active Rules', 'yith-woocommerce-name-your-price' );


            return $tabs;
        }


        /**
         * return empty rule in admin
         * @author YITHEMES
         * @since  1.0.0
         */
        public function add_new_rule_admin()
        {

            if ( isset( $_REQUEST[ 'ywcnp_add_new_rule' ] ) && isset( $_REQUEST[ 'ywcnp_id' ] ) ) {

                $current_rule = $_REQUEST[ 'ywcnp_add_new_rule' ];
                $option_id = $_REQUEST[ 'ywcnp_id' ];
                $params = array(
                    'option_id' => $option_id,
                    'current_rule' => $current_rule
                );

                $params[ 'params' ] = $params;
                ob_start();

                wc_get_template( 'nameyourprice-single-rule.php', $params, '', YWCNP_TEMPLATE_PATH );
                $template = ob_get_contents();

                ob_end_clean();
                wp_send_json( array( 'result' => $template ) );


            }
        }

        /**
         * @param $new_cart
         * @param $new_cart_item_key
         * @param $old_cart_item
         *
         * @return mixed
         */
        public function ywcnp_add_cart_item_data_subscription( $new_cart, $new_cart_item_key, $old_cart_item )
        {

            if ( isset( $old_cart_item[ 'ywcnp_amount' ] ) ) {

                $new_cart_item = $new_cart->cart_contents[ $new_cart_item_key ];
                $product =  $new_cart_item[ 'data' ];
                $meta_args = array( 
                        'price' => $old_cart_item[ 'ywcnp_amount' ],
                         'subscription_price' => $old_cart_item[ 'ywcnp_amount' ]
                );
                
                yit_set_prop( $product, $meta_args );
             
            }

            return $new_cart;
        }

        /**@author YITHEMES
         * @since 1.0.0
         *
         * @param $price
         * @param WC_Product $product
         *
         * @return string
         */
        public function get_nameyourprice_html( $price, $product )
        {

            if ( ywcnp_product_is_name_your_price( $product ) ) {


	            if ( $product->is_type( 'simple' ) ) {
		            $price = ywcnp_get_suggest_price_html( $product );
	            }
             elseif( $product->is_type('variable' ) ){

	             /**
	              * @var WC_Product_Variable $product
	              */
                	$variation_ids = $product->get_visible_children();

                	$prices = array();

                	foreach( $variation_ids as $variation_id ){

                		$sugg_price = ywcnp_get_suggest_price( $variation_id );
                		if( !empty( $sugg_price ) ) {
			                $prices[] = $sugg_price;
		                }else{

                			$variation_product = wc_get_product( $variation_id );
                			$prices[] = $variation_product->get_price();
		                }
	                }



	                $min_price = min( $prices );
                	$max_price = max( $prices );
                //	$min_price = yit_get_prop( $product, '_min_variation_price', true );
                  //  $max_price = yit_get_prop( $product, '_max_variation_price', true );
                    $price = $min_price !== $max_price ? sprintf( _x( '%1$s&ndash;%2$s', 'Price range: from-to', 'woocommerce' ), wc_price( $min_price ), wc_price( $max_price ) ) : wc_price( $min_price );
                    $price = apply_filters('ywcnp_get_variation_name_price_html', $price, $min_price,$max_price, $product );
                }

            }

            return $price;
        }


        /**
         * @param WC_Product_Variable $product
         */
        public function variable_product_sync_data( $product ){

            $product_id = yit_get_product_id( $product );
            $children = $product->get_visible_children();

            $this->variable_product_sync( $product_id, $children );
        }
        
        /**
         * @author YITHEMES
         * @since 1.0.0
         * @param $product_id
         * @param $children
         */
        public function variable_product_sync( $product_id, $children )
        {

            $product = wc_get_product( $product_id );
            if ( $children ) {

                $min_price = null;
                $max_price = null;
                $min_price_id = null;
                $max_price_id = null;

                // Main active prices
                $min_price = null;
                $max_price = null;
                $min_price_id = null;
                $max_price_id = null;

                // Regular prices
                $min_regular_price = null;
                $max_regular_price = null;
                $min_regular_price_id = null;
                $max_regular_price_id = null;

                // Sale prices
                $min_sale_price = null;
                $max_sale_price = null;
                $min_sale_price_id = null;
                $max_sale_price_id = null;

                foreach ( array( 'price', 'regular_price' ) as $price_type ) {

                    foreach ( $children as $child_id ) {
                        
                        $product_child = wc_get_product( $child_id );

                        if ( ywcnp_product_is_name_your_price( $product_child ) ) {

                            // Skip hidden variations
                            if ( 'yes' === get_option( 'woocommerce_hide_out_of_stock_items' ) ) {
                                $stock = yit_get_prop( $child_id, '_stock', true );
                                if ( $stock !== "" && $stock <= get_option( 'woocommerce_notify_no_stock_amount' ) ) {
                                    continue;
                                }
                            }

                            // get the minimum price for this variation
                            $child_min_price = ywcnp_get_min_price( $child_id );
                            $child_max_price = ywcnp_get_max_price( $child_id );

                            // if there is no set minimum, technically the min is 0
                            $child_min_price = !empty( $child_min_price ) ? $child_min_price : 0;


                            $current_min = $child_min_price;
                            $current_max = empty( $child_max_price ) ? $child_min_price : $child_max_price;


                            // Find min price
                            if ( is_null( ${"min_{$price_type}"} ) || $current_min < ${"min_{$price_type}"} ) {
                                ${"min_{$price_type}"} = $current_min;
                                ${"min_{$price_type}_id"} = $child_id;
                            }

                            // Find max price
                            if ( is_null( ${"max_{$price_type}"} ) || $current_max > ${"max_{$price_type}"} ) {
                                ${"max_{$price_type}"} = $current_max;
                                ${"max_{$price_type}_id"} = $child_id;
                            }

                        } else {

                            $child_price = yit_get_prop( $product_child, '_' . $price_type );

                            // Skip non-priced variations
                            if ( $child_price === '' ) {
                                continue;
                            }

                            // Skip hidden variations
                            if ( 'yes' === get_option( 'woocommerce_hide_out_of_stock_items' ) ) {
                                $stock = yit_get_prop( $product_child, '_stock' );
                                if ( $stock !== "" && $stock <= get_option( 'woocommerce_notify_no_stock_amount' ) ) {
                                    continue;
                                }
                            }

                            // Find min price
                            if ( is_null( ${"min_{$price_type}"} ) || $child_price < ${"min_{$price_type}"} ) {
                                ${"min_{$price_type}"} = $child_price;
                                ${"min_{$price_type}_id"} = $child_id;
                            }

                            // Find max price
                            if ( $child_price > ${"max_{$price_type}"} ) {
                                ${"max_{$price_type}"} = $child_price;
                                ${"max_{$price_type}_id"} = $child_id;
                            }

                        }

                    }
                    
                    

                    // Store prices
                    yit_save_prop( $product, '_min_variation_' . $price_type, ${"min_{$price_type}"} );
                    yit_save_prop( $product, '_max_variation_' . $price_type, ${"max_{$price_type}"} );

                    // Store ids
                    yit_save_prop( $product, '_min_' . $price_type . '_variation_id', ${"min_{$price_type}_id"} );
                    yit_save_prop( $product, '_max_' . $price_type . '_variation_id', ${"max_{$price_type}_id"} );
                }

                
                // The VARIABLE PRODUCT price should equal the min price of any type
                yit_save_prop( $product, '_price', $min_price );

                wc_delete_product_transients( $product_id );

            }

        }

        /**
         * init multivendor integration
         * @author YITHEMES
         * @since  1.0.0
         */
        public function init_multivendor_integration()
        {


            YITH_Name_Your_Price_Compatibility();

        }


    }
}