<?php
if( !defined( 'ABSPATH' ) )
    exit;

$settings = array(

    'category-rules'  =>  array(

        'category_rules' => array(
        'type'   => 'custom_tab',
        'action' => 'yith_wc_name_your_price_category_rules'
        )

    )
);


return apply_filters( 'ywcnp_category_rules', $settings );