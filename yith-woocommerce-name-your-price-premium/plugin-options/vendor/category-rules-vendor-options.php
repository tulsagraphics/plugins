<?php
if( !defined( 'ABSPATH' ) )
    exit;

$settings = array(

    'category-rules-vendor'  =>  array(

        'category_rules_vendor' => array(
        'type'   => 'custom_tab',
        'action' => 'yith_wc_name_your_price_category_vendor_rules',
         'hide_sidebar' => true
        )

    )
);


return apply_filters( 'ywcnp_category_vendor_rules', $settings );