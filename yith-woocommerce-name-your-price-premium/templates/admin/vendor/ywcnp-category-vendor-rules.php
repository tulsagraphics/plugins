<?php
if ( !defined( 'ABSPATH' ) )
    exit;


if ( !class_exists( 'YWCNP_Category_Vendor_Rules' ) ) {

    class YWCNP_Category_Vendor_Rules
    {

        /**
         * Single instance of the class
         *
         * @var \YWCNP_Category_Rules
         * @since 1.0.0
         */
        protected static $instance;

        /**
         * @var YITH_Vendor
         */
        protected $vendor;

        /**
         * Returns single instance of the class
         *
         * @return \YWCNP_Category_Rules
         * @since 1.0.0
         */
        public static function get_instance()
        {

            if ( is_null( self::$instance ) ) {

                self::$instance = new self( $_REQUEST );

            }

            return self::$instance;
        }

        /**
         * Constructor
         *
         * @since   1.0.0
         * @return  mixed
         * @author  YITHEMES
         */
        public function __construct()
        {

            add_filter( 'set-screen-option', array( $this, 'set_options' ), 10, 3 );
            add_action( 'current_screen', array( $this, 'add_options' ) );
            $this->vendor = yith_get_vendor( 'current', 'user' );

        }


        /**
         * print table
         * @author YITHEMES
         * @since 1.0.0
         *
         */
        public function output()
        {

            $table = new YITH_Category_Rule_Vendor_Table();


            $table->vendor_id = $this->vendor->id;

            $list_query_args = array(
                'page' => $_GET[ 'page' ],
            );

            $message = '';
            $notice = '';
            $data_selected = '';
            $value = '';
            $list_url = esc_url( add_query_arg( $list_query_args, admin_url( 'admin.php' ) ) );

            if ( !empty( $_POST[ 'nonce' ] ) && wp_verify_nonce( $_POST[ 'nonce' ], basename( __FILE__ ) ) ) {

                $item_valid = $this->validate_fields( $_POST );

                if ( $item_valid !== true ) {

                    $notice = $item_valid;

                } else {

                    $category_ids = !is_array( $_POST[ 'category_ids' ] ) ? explode( ',', $_POST[ 'category_ids' ] ) : $_POST[ 'category_ids' ] ;
                    $suggest_price = isset( $_POST[ '_ywcnp_suggest_price' ] ) ? $_POST[ '_ywcnp_suggest_price' ] : '';
                    $minimum_price = isset( $_POST[ '_ywcnp_min_price' ] ) ? $_POST[ '_ywcnp_min_price' ] : '';
                    $maximum_price = isset( $_POST[ '_ywcnp_max_price' ] ) ? $_POST[ '_ywcnp_max_price' ] : '';
                    $is_enabled = isset( $_POST[ '_ywcnp_enable_rule' ] ) ? 'yes' : 'no';
                    $has_rule = 'yes';

                    $vendor_rules = get_user_meta( $this->vendor->id, '_ywcnp_vendor_cat_rules', true );

                    $vendor_rules = $vendor_rules ? $vendor_rules : array();

                    if ( !empty( $_POST[ 'insert' ] ) ) {

                        foreach ( $category_ids as $category_id ) {

                            $vendor_rules[] = $this->add_rule_for_vendor( $category_id, $suggest_price, $minimum_price, $maximum_price, $is_enabled );

                            $product_ids = ywcnp_get_product_id_by_category( $category_id );

                            foreach ( $product_ids as $product_id ) {
                                $default = array(
                                    'product_id' => $product_id,
                                    'sugg_price' => $suggest_price,
                                    'min_price' => $minimum_price,
                                    'max_price' => $maximum_price,

                                );

                                if ( 'yes' == $is_enabled )
                                    ywcnp_save_product_meta_global_rule( $default );
                                else
                                    ywcnp_remove_product_meta_global_rule( $product_id );

                            }

                        }

                        update_user_meta( $this->vendor->id, '_ywcnp_vendor_cat_rules', $vendor_rules );
                        update_user_meta( $this->vendor->id, '_ywcnp_vendor_has_rules', $has_rule );

                        $message = sprintf( _n( '%s rule added successfully', '%s rules added successfully', count( $category_ids ), 'yith-woocommece-name-your-price' ), count( $category_ids ) );

                    } elseif ( !empty( $_POST[ 'edit' ] ) ) {

                        foreach ( $category_ids as $category_id ) {

                            $this->update_vendor_rule( $category_id, $suggest_price, $minimum_price, $maximum_price, $is_enabled );

                            $product_ids = ywcnp_get_product_id_by_category( $category_id );

                            foreach ( $product_ids as $product_id ) {
                                $default = array(
                                    'product_id' => $product_id,
                                    'sugg_price' => $suggest_price,
                                    'min_price' => $minimum_price,
                                    'max_price' => $maximum_price,

                                );

                                if ( 'yes' == $is_enabled )
                                    ywcnp_save_product_meta_global_rule( $default );
                                else
                                    ywcnp_remove_product_meta_global_rule( $product_id );
                            }
                        }

                        $message = __( 'Rule updated successfully', 'yith-woocommece-name-your-price' );

                    }

                }

            }
            $table->prepare_items();


            $item = array(
                'ID' => '',
                'suggest_price' => '',
                'min_price' => '',
                'max_price' => '',
                'enabled' => ''

            );

            if ( 'delete' === $table->current_action() ) {
                $message = sprintf( _n( '%s rule removed successfully', '%s rules removed successfully', count( $_GET[ 'id' ] ), 'yith-woocommece-name-your-price' ), count( $_GET[ 'id' ] ) );
            }

            if ( isset( $_GET[ 'id' ] ) && !empty( $_GET[ 'action' ] ) && ( 'edit' == $_GET[ 'action' ] ) ) {

                $single_rule = $this->get_vendor_rule_by( 'category', $_GET[ 'id' ] );

                $s_price = $single_rule ? $single_rule[ 'sugg_price' ] : '';
                $min_price = $single_rule ? $single_rule[ 'min_price' ] : '';
                $max_price = $single_rule ? $single_rule[ 'max_price' ] : '';
                $is_enabled = $single_rule ? $single_rule[ 'enabled' ] : '';
                $item = array(
                    'ID' => $_GET[ 'id' ],
                    'suggest_price' => $s_price,
                    'min_price' => $min_price,
                    'max_price' => $max_price,
                    'enabled' => $is_enabled

                );

                $category = get_term( $_GET[ 'id' ], 'product_cat' );
                $data_selected = wp_kses_post( $category->name );
                $value = $_GET[ 'id' ];
            }
            ?>
            <div class="wrap">
                <div class="icon32 icon32-posts-post" id="icon-edit"><br/></div>
                <h2><?php _e( 'Category rule list', 'yith-woocommece-name-your-price' );

                    if ( empty( $_GET[ 'action' ] ) || ( 'insert' !== $_GET[ 'action' ] && 'edit' !== $_GET[ 'action' ] ) ) : ?>
                        <?php $query_args = array(
                            'page' => $_GET[ 'page' ],
                            'action' => 'insert'
                        );
                        $add_form_url = esc_url( add_query_arg( $query_args, admin_url( 'admin.php' ) ) );
                        ?>
                        <a class="add-new-h2"
                           href="<?php echo $add_form_url; ?>"><?php _e( 'Add Rule', 'yith-woocommece-name-your-price' ); ?></a>
                    <?php endif; ?>
                </h2>
                <?php if ( !empty( $notice ) ) : ?>
                    <div id="notice" class="error below-h2"><p><?php echo $notice; ?></p></div>
                <?php endif;

                if ( !empty( $message ) ) : ?>
                    <div id="message" class="updated below-h2"><p><?php echo $message; ?></p></div>
                <?php endif;

                if ( !empty( $_GET[ 'action' ] ) && ( 'insert' == $_GET[ 'action' ] || 'edit' == $_GET[ 'action' ] ) ) : ?>

                    <form id="form" method="POST">
                        <input type="hidden" name="nonce"
                               value="<?php echo wp_create_nonce( basename( __FILE__ ) ); ?>"/>
                        <table class="form-table">
                            <tbody>
                            <tr valign="top" class="titledesc">
                                <th scope="row">
                                    <label
                                        for="product"><?php _e( 'Select category', 'yith-woocommerce-name-your-price' ); ?></label>
                                </th>
                                <td class="forminp yith-choosen">

                                    <?php if ( 'edit' == $_GET[ 'action' ] ) : ?>
                                        <input id="category_id" name="category_ids" type="hidden"
                                               value="<?php echo esc_attr( $item[ 'ID' ] ); ?>"/>
                                    <?php endif; ?>
                                    <?php

                                    $args = array(
	                                    'id' => 'category_ids',
	                                    'class' => 'wc-product-search',
	                                    'name' => 'category_ids',
	                                    'data-multiple' => ( 'edit' == $_GET['action'] ) ? false : true,
	                                    'data-placeholder' => __( 'Select Category', 'yith-woocommerce-name-your-price' ),
	                                    'data-selected' => array( $value=>$data_selected ),
	                                    'data-action'=>'yith_json_search_product_categories',
	                                    'value' =>  $value

                                    );

                                    yit_add_select2_fields( $args );
                                    ?>

                                    <span
                                        class="description"><?php _e( 'Select the product categories to which you want to apply the rule', 'yith-woocommerce-name-your-price' ); ?></span>

                                </td>
                            </tr>
                            <tr valign="top" class="titledesc">
                                <th scope="row">
                                    <label><?php _e( 'Rule behavior', 'yith-woocommerce-name-your-price' ); ?></label>
                                </th>
                                <td class="forminp forminp-checkbox">
                                    <input type="checkbox"
                                           name="_ywcnp_enable_rule" <?php checked( 'yes', $item[ 'enabled' ] ); ?> />
                                    <span
                                        class="description"><?php _e( 'If selected, the rule will be applied to the products of the selected categories. If not, the rule will be just created but not applied.', 'yith-woocommerce-name-your-price' ); ?></span>
                                </td>
                            </tr>
                            <tr valign="top" class="titledesc">
                                <th scope="row">
                                    <label><?php _e( 'Suggested Price', 'yith-woocommerce-name-your-price' ); ?></label>
                                </th>
                                <td class="forminp forminp-text">
                                    <input type="text" class="ywcnp_suggest_price wc_input_price"
                                           name="_ywcnp_suggest_price"
                                           value="<?php echo esc_attr( $item[ 'suggest_price' ] ); ?>"/>
                                    <span
                                        class="description"><?php _e( 'Select the suggested price for your product, leave blank not to suggest a price', 'yith-woocommerce-name-your-price' ); ?></span>
                                </td>
                            </tr>
                            <tr valign="top" class="titledesc">
                                <th scope="row">
                                    <label><?php _e( 'Minimum Price', 'yith-woocommerce-name-your-price' ); ?></label>
                                </th>
                                <td class="forminp forminp-text">
                                    <input type="text" class="ywcnp_min_price wc_input_price" name="_ywcnp_min_price"
                                           value="<?php echo esc_attr( $item[ 'min_price' ] ); ?>"/>
                                    <span
                                        class="description"><?php _e( 'Set the minimum price for your product, leave blank not to set a minimum price', 'yith-woocommerce-name-your-price' ); ?></span>
                                </td>
                            </tr>
                            <tr valign="top" class="titledesc">
                                <th scope="row">
                                    <label><?php _e( 'Maximum Price', 'yith-woocommerce-name-your-price' ); ?></label>
                                </th>
                                <td class="forminp forminp-text">
                                    <input type="text" class="ywcnp_max_price wc_input_price" name="_ywcnp_max_price"
                                           value="<?php echo esc_attr( $item[ 'max_price' ] ); ?>"/>
                                    <span
                                        class="description"><?php _e( 'Set the maximum price for your product, leave blank not to set a maximum price', 'yith-woocommerce-name-your-price' ); ?></span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <input
                            id="<?php echo $_GET[ 'action' ] ?>"
                            name="<?php echo $_GET[ 'action' ] ?>"
                            type="submit"
                            value="<?php echo( ( 'insert' == $_GET[ 'action' ] ) ? __( 'Add category rule', 'yith-woocommerce-name-your-price' ) : __( 'Update category rule', 'yith-woocommerce-name-your-price' ) ); ?>"
                            class="button-primary"
                        />
                        <a class="button-secondary"
                           href="<?php echo $list_url; ?>"><?php _e( 'Return to rule list', 'yith-woocommerce-name-your-price' ); ?></a>
                    </form>
                <?php else : ?>
                    <form id="custom-table" method="GET" action="<?php echo $list_url; ?>">
                        <input type="hidden" name="page" value="<?php echo $_GET[ 'page' ]; ?>"/>
                        <?php $table->display(); ?>
                    </form>
                <?php endif; ?>
            </div>
            <?php


        }


        /**
         * Validate input fields
         *
         * @since   1.0.0
         * @author  Alberto Ruggiero
         *
         * @param   $item array POST data array
         *
         * @return  bool|string
         */
        private function validate_fields( $item )
        {

            $messages = array();

            if ( empty( $item[ 'category_ids' ] ) ) {
                $messages[] = __( 'Select at least one category', 'yith-woocommerce-name-your-price' );
            }

            if ( empty( $messages ) ) {
                return true;
            }

            return implode( '<br />', $messages );

        }

        /**
         * Add screen options for list table template
         *
         * @since   1.0.0
         * @return  void
         * @author  Alberto Ruggiero
         */
        public function add_options()
        {


            if ( ( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] == 'yith_vendor_settings' ) && ( isset( $_GET[ 'tab' ] ) && $_GET[ 'tab' ] == 'category-rules' ) && ( !isset( $_GET[ 'action' ] ) || ( $_GET[ 'action' ] != 'edit' && $_GET[ 'action' ] != 'insert' ) ) ) {

                $option = 'per_page';

                $args = array(
                    'label' => __( 'Categories', 'yith-woocommerce-name-your-price' ),
                    'default' => 10,
                    'option' => 'items_per_page'
                );

                add_screen_option( $option, $args );

            }

        }

        /**
         * Set screen options for list table template
         *
         * @since   1.0.0
         *
         * @param   $status
         * @param   $option
         * @param   $value
         *
         * @return  mixed
         * @author  Alberto Ruggiero
         */
        public function set_options( $status, $option, $value )
        {

            return ( 'items_per_page' == $option ) ? $value : $status;

        }

        /**
         * @param $category_id
         * @param $sugg_price
         * @param $min_price
         * @param $max_price
         * @return array
         */
        public function add_rule_for_vendor( $category_id, $sugg_price, $min_price, $max_price, $is_enable )
        {

            return array(
                'category' => $category_id,
                'sugg_price' => $sugg_price,
                'min_price' => $min_price,
                'max_price' => $max_price,
                'enabled' => $is_enable

            );
        }

        /**
         * @param string $by
         * @param $value
         * @return bool
         */
        public function get_vendor_rule_by( $by = 'category', $value )
        {

            $vendor_rules = get_user_meta( $this->vendor->id, '_ywcnp_vendor_cat_rules', true );

            $vendor_rules = $vendor_rules ? $vendor_rules : array();

            foreach ( $vendor_rules as $key => $rule ) {
                if ( $rule[ $by ] == $value ) {
                    return $vendor_rules[ $key ];
                }
            }
            return false;
        }


        /**
         * @param $category
         * @param $sugg_price
         * @param $min_price
         * @param $max_price
         * @param $is_anable
         * @return bool|int
         */
        public function update_vendor_rule( $category, $sugg_price, $min_price, $max_price, $is_enable )
        {

            $item = array(
                'category' => $category,
                'sugg_price' => $sugg_price,
                'min_price' => $min_price,
                'max_price' => $max_price,
                'enabled' => $is_enable
            );

            $vendor_rules = get_user_meta( $this->vendor->id, '_ywcnp_vendor_cat_rules', true );
            $vendor_rules = $vendor_rules ? $vendor_rules : array();

            foreach ( $vendor_rules as $key => $rule ) {
                if ( $rule[ 'category' ] == $category ) {
                    $vendor_rules[ $key ] = $item;
                    continue;
                }
            }

            return update_user_meta( $this->vendor->id, '_ywcnp_vendor_cat_rules', $vendor_rules );
        }

    }
}
/**
 * return single instance
 * @author YITHEMES
 * @since 1.0.0
 * @return YWCNP_Category_Rules
 */
function YWCNP_Category_Vendor_Rule()
{

    return YWCNP_Category_Vendor_Rules::get_instance();
}

