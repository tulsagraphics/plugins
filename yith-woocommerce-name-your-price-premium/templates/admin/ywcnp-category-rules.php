<?php
if ( !defined( 'ABSPATH' ) )
    exit;

if ( !class_exists( 'YWCNP_Category_Rules' ) ) {

    class YWCNP_Category_Rules
    {

        /**
         * Single instance of the class
         *
         * @var \YWCNP_Category_Rules
         * @since 1.0.0
         */
        protected static $instance;


        /**
         * Returns single instance of the class
         *
         * @return \YWCNP_Category_Rules
         * @since 1.0.0
         */
        public static function get_instance()
        {

            if ( is_null( self::$instance ) ) {

                self::$instance = new self( $_REQUEST );

            }

            return self::$instance;
        }

        /**
         * Constructor
         *
         * @since   1.0.0
         * @return  mixed
         * @author  YITHEMES
         */
        public function __construct()
        {

            add_filter( 'set-screen-option', array( $this, 'set_options' ), 10, 3 );
            add_action( 'current_screen', array( $this, 'add_options' ) );
        }


        /**
         * print table
         * @author YITHEMES
         * @since 1.0.0
         *
         */
        public function output()
        {

            global $wpdb;

            $table = new YITH_Custom_Table( array(
                'singular' => __( 'category', 'yith-woocommece-name-your-price' ),
                'plural' => __( 'categories', 'yith-woocommece-name-your-price' )
            ) );

            global $wpdb;
            $termmeta_term_id = 'term_id';

            if( version_compare( WC()->version, '2.6', '<') ){
                $termmeta_table 	= $wpdb->woocommerce_termmeta;
                $termmeta_term_id = 'woocommerce_' . $termmeta_term_id;
            }
            else{
                $termmeta_table 	= $wpdb->termmeta;
            }

                $table->options = array(
                    'select_table' => $wpdb->prefix . 'terms a INNER JOIN ' . $wpdb->prefix . 'term_taxonomy b ON a.term_id = b.term_id INNER JOIN ' . $termmeta_table. ' c ON c.'.$termmeta_term_id.' = a.term_id',
                    'select_columns' => array(
                        'a.term_id AS ID',
                        'a.name',
                        'MAX(CASE WHEN c.meta_key = "_ywcnp_category_has_rule" THEN c.meta_value ELSE NULL END) AS category_has_rule',

                    ),
                    'select_where' => 'b.taxonomy = "product_cat" AND ( c.meta_key = "_ywcnp_category_has_rule" ) AND c.meta_value = "yes"',
                    'select_group' => 'a.term_id',
                    'select_order' => 'a.name',
                    'select_order_dir' => 'ASC',
                    'per_page_option' => 'items_per_page',
                    'count_table' => '( SELECT COUNT(*) FROM ' . $wpdb->prefix . 'terms a INNER JOIN ' . $wpdb->prefix . 'term_taxonomy b ON a.term_id = b.term_id INNER JOIN ' . $termmeta_table . ' c ON c.'.$termmeta_term_id.' = a.term_id WHERE b.taxonomy = "product_cat" AND ( c.meta_key = "_ywcnp_category_has_rule" ) AND c.meta_value = "yes" GROUP BY a.term_id ) AS count_table',
                    'count_where' => '',
                    'key_column' => 'ID',
                    'view_columns' => array(
                        'cb' => '<input type="checkbox" />',
                        'category' => __( 'Category', 'yith-woocommece-name-your-price' ),
                        'suggest' => __( 'Suggested Price', 'yith-woocommece-name-your-price' ),
                        'min_price' => __( 'Minimum Price', 'yith-woocommece-name-your-price' ),
                        'max_price' => __( 'Maximum Price', 'yith-woocommece-name-your-price' ),
                        'enabled_rule' => __( 'Enabled', 'yith-woocommece-name-your-price' )
                    ),
                    'hidden_columns' => array(),
                    'sortable_columns' => array(
                        'category' => array( 'post_title', true )
                    ),
                    'custom_columns' => array(
                        'column_category' => function ( $item, $me ) {

                            $edit_query_args = array(
                                'page' => $_GET['page'],
                                'tab' => $_GET['tab'],
                                'action' => 'edit',
                                'id' => $item['ID']
                            );
                            $edit_url = esc_url( add_query_arg( $edit_query_args, admin_url( 'admin.php' ) ) );

                            $delete_query_args = array(
                                'page' => $_GET['page'],
                                'tab' => $_GET['tab'],
                                'action' => 'delete',
                                'id' => $item['ID']
                            );
                            $delete_url = esc_url( add_query_arg( $delete_query_args, admin_url( 'admin.php' ) ) );

                            $actions = array(
                                'edit' => '<a href="' . $edit_url . '">' . __( 'Edit rule', 'yith-woocommece-name-your-price' ) . '</a>',
                                'delete' => '<a href="' . $delete_url . '">' . __( 'Remove rule from list', 'yith-woocommece-name-your-price' ) . '</a>',
                            );

                            return sprintf( '<strong><a class="tips" href="%s" data-tip="%s">#%d %s </a></strong> %s', $edit_url, __( 'Edit rule', 'yith-woocommece-name-your-price' ), $item['ID'], $item['name'], $me->row_actions( $actions ) );
                        },
                        'column_suggest' => function ( $item, $me ) {

                            $price = ywcnp_get_woocommerce_term_meta( $item['ID'], '_ywcnp_suggest_price', true );

                            return empty( $price ) ? '<span>' . __( 'No Price', 'yith-woocommece-name-your-price' ) . '</span>' : wc_price( $price );
                        },
                        'column_min_price' => function ( $item, $me ) {

                            $price = ywcnp_get_woocommerce_term_meta( $item['ID'], '_ywcnp_min_price', true );


                            return empty( $price ) ? '<span>' . __( 'No Price', 'yith-woocommece-name-your-price' ) . '</span>' : wc_price( $price );

                        },
                        'column_max_price' => function ( $item, $me ) {

                            $price = ywcnp_get_woocommerce_term_meta( $item['ID'], '_ywcnp_max_price', true );

                            return empty( $price ) ? '<span>' . __( 'No Price', 'yith-woocommece-name-your-price' ) . '</span>' : wc_price( $price );

                        },
                        'column_enabled_rule' => function ( $item, $me ) {

                            $is_enabled = ywcnp_get_woocommerce_term_meta( $item['ID'], '_ywcnp_enable_rule', true );
                            if( $is_enabled == 'yes' ) {
                                $class = 'show';
                                $tip = __( 'Enabled', 'yith-woocommece-name-your-price' );
                            }
                            else {
                                $class = 'hide';
                                $tip = __( 'Disabled', 'yith-woocommece-name-your-price' );
                            }

                            return sprintf( '<mark class="%s tips" data-tip="%s">%s</mark>', $class, $tip, $tip );

                        }
                    ),
                    'bulk_actions' => array(
                        'actions' => array(
                            'delete' => __( 'Remove rule from list', 'yith-woocommece-name-your-price' )
                        ),
                        'functions' => array(
                            'function_delete' => function () {
                                global $wpdb;

                                $ids = isset( $_GET['id'] ) ? $_GET['id'] : array();
                               $ids = is_array( $ids )  ? $ids : explode(',', $ids );

                                if( is_array( $ids ) ) {
                                    foreach ( $ids as $id ) {

                                        $product_ids = ywcnp_get_product_id_by_category( $id );

                                        foreach ( $product_ids as $product_id ) {
                                            ywcnp_remove_product_meta_global_rule( $product_id );

                                        }
                                    }

                                    $ids = implode( ',', $ids );
                                }


                                $termmeta_term_id = 'term_id';

                                if( version_compare( WC()->version, '2.6', '<') ){
                                    $termmeta_table 	= $wpdb->woocommerce_termmeta;
                                    $termmeta_term_id = 'woocommerce_' . $termmeta_term_id;
                                }
                                else{
                                    $termmeta_table 	= $wpdb->termmeta;
                                }

                                if( !empty( $ids ) ) {

                                    $wpdb->query( "UPDATE {$termmeta_table}
                                           SET meta_value='no'
                                           WHERE ( meta_key = '_ywcnp_category_has_rule' ) AND {$termmeta_term_id} IN ( $ids )"
                                    );

                                }


                            }
                        )
                    ),
                );


            $list_query_args = array(
                'page' => $_GET[ 'page' ],
                'tab' => $_GET[ 'tab' ],
            );

            $message = '';
            $notice = '';
            $data_selected = '';
            $value = '';
            $list_url = esc_url( add_query_arg( $list_query_args, admin_url( 'admin.php' ) ) );

            if ( !empty( $_POST[ 'nonce' ] ) && wp_verify_nonce( $_POST[ 'nonce' ], basename( __FILE__ ) ) ) {

                $item_valid = $this->validate_fields( $_POST );

                if ( $item_valid !== true ) {

                    $notice = $item_valid;

                } else {


                    $category_ids =  $_POST[ 'category_ids' ] ;
                    if( !is_array( $category_ids ) ){
                        $category_ids = explode(',',$category_ids );
                    }
                    
                    $suggest_price = isset( $_POST[ '_ywcnp_suggest_price' ] ) ? $_POST[ '_ywcnp_suggest_price' ] : '';
                    $minimum_price = isset( $_POST[ '_ywcnp_min_price' ] ) ? $_POST[ '_ywcnp_min_price' ] : '';
                    $maximum_price = isset( $_POST[ '_ywcnp_max_price' ] ) ? $_POST[ '_ywcnp_max_price' ] : '';
                    $is_enabled = isset( $_POST[ '_ywcnp_enable_rule' ] ) ? 'yes' : 'no';
                    $has_rule = 'yes';

                    
                    foreach ( $category_ids as $category_id ) {

                        ywcnp_update_woocommerce_term_meta( $category_id, '_ywcnp_suggest_price', $suggest_price );
                        ywcnp_update_woocommerce_term_meta( $category_id, '_ywcnp_min_price', $minimum_price );
                        ywcnp_update_woocommerce_term_meta( $category_id, '_ywcnp_max_price', $maximum_price );
                        ywcnp_update_woocommerce_term_meta( $category_id, '_ywcnp_category_has_rule', $has_rule );
                        ywcnp_update_woocommerce_term_meta( $category_id, '_ywcnp_enable_rule', $is_enabled );

                        $product_ids = ywcnp_get_product_id_by_category( $category_id );
                        
                        foreach ( $product_ids as $product_id ) {
                            $default = array(
                                'product_id' => $product_id,
                                'sugg_price' => $suggest_price,
                                'min_price' => $minimum_price,
                                'max_price' => $maximum_price,
                            );

                            
                            if ( 'yes' == $is_enabled )
                                ywcnp_save_product_meta_global_rule( $default );
                            else
                                ywcnp_remove_product_meta_global_rule( $product_id );

                        }
                    }

                    if ( !empty( $_POST[ 'insert' ] ) ) {

                        $message = sprintf( _n( '%s rule added successfully', '%s rules added successfully', count( $category_ids ), 'yith-woocommece-name-your-price' ), count( $category_ids ) );

                    } elseif ( !empty( $_POST[ 'edit' ] ) ) {

                        $message = __( 'Rule updated successfully', 'yith-woocommece-name-your-price' );

                    }

                }

            }
            $table->prepare_items();


            $item = array(
                'ID' => '',
                'suggest_price' => '',
                'min_price' => '',
                'max_price' => '',
                'enabled' => 'no'

            );

            if ( 'delete' === $table->current_action() ) {
                $message = sprintf( _n( '%s category removed successfully', '%s categories removed successfully', count( $_GET[ 'id' ] ), 'yith-woocommece-name-your-price' ), count( $_GET[ 'id' ] ) );
            }

            if ( isset( $_GET[ 'id' ] ) && !empty( $_GET[ 'action' ] ) && ( 'edit' == $_GET[ 'action' ] ) ) {

                $item = array(
                    'ID' => $_GET[ 'id' ],
                    'suggest_price' => ywcnp_get_woocommerce_term_meta( $_GET[ 'id' ], '_ywcnp_suggest_price', true ),
                    'min_price' => ywcnp_get_woocommerce_term_meta( $_GET[ 'id' ], '_ywcnp_min_price', true ),
                    'max_price' => ywcnp_get_woocommerce_term_meta( $_GET[ 'id' ], '_ywcnp_max_price', true ),
                    'enabled' => ywcnp_get_woocommerce_term_meta( $_GET[ 'id' ], '_ywcnp_enable_rule', true )
                );

                $category = get_term( $_GET[ 'id' ], 'product_cat' );
                $data_selected = wp_kses_post( $category->name );
                $value = $_GET[ 'id' ];
            }
            ?>
            <div class="wrap">
                <div class="icon32 icon32-posts-post" id="icon-edit"><br/></div>
                <h2><?php _e( 'Category rule list', 'yith-woocommece-name-your-price' );

                    if ( empty( $_GET[ 'action' ] ) || ( 'insert' !== $_GET[ 'action' ] && 'edit' !== $_GET[ 'action' ] ) ) : ?>
                        <?php $query_args = array(
                            'page' => $_GET[ 'page' ],
                            'tab' => $_GET[ 'tab' ],
                            'action' => 'insert'
                        );
                        $add_form_url = esc_url( add_query_arg( $query_args, admin_url( 'admin.php' ) ) );
                        ?>
                        <a class="add-new-h2"
                           href="<?php echo $add_form_url; ?>"><?php _e( 'Add rule', 'yith-woocommece-name-your-price' ); ?></a>
                    <?php endif; ?>
                </h2>
                <?php if ( !empty( $notice ) ) : ?>
                    <div id="notice" class="error below-h2"><p><?php echo $notice; ?></p></div>
                <?php endif;

                if ( !empty( $message ) ) : ?>
                    <div id="message" class="updated below-h2"><p><?php echo $message; ?></p></div>
                <?php endif;

                if ( !empty( $_GET[ 'action' ] ) && ( 'insert' == $_GET[ 'action' ] || 'edit' == $_GET[ 'action' ] ) ) : ?>

                    <form id="form" method="POST">
                        <input type="hidden" name="nonce"
                               value="<?php echo wp_create_nonce( basename( __FILE__ ) ); ?>"/>
                        <table class="form-table">
                            <tbody>
                            <tr valign="top" class="titledesc">
                                <th scope="row">
                                    <label
                                        for="product"><?php _e( 'Select category', 'yith-woocommerce-name-your-price' ); ?></label>
                                </th>
                                <td class="forminp yith-choosen">

                                    <?php if ( 'edit' == $_GET[ 'action' ] ) : ?>
                                        <input id="category_id" name="category_ids" type="hidden"
                                               value="<?php echo esc_attr( $item[ 'ID' ] ); ?>"/>
                                    <?php endif; ?>

                                    <?php



                                    $args = array(
	                                    'id' => 'category_ids',
	                                    'class' => 'wc-product-search',
	                                    'name' => 'category_ids',
	                                    'data-multiple' => ( 'edit' == $_GET['action'] ) ? false : true,
	                                    'data-placeholder' => __( 'Select Category', 'yith-woocommerce-name-your-price' ),
	                                    'data-selected' => array( $value=>$data_selected ),
	                                    'data-action'=>'yith_json_search_product_categories',
	                                    'value' =>  $value

                                    );

                                    yit_add_select2_fields( $args );
                                    ?>

                                    <span
                                        class="description"><?php _e( 'Select the product categories to which you want to apply the rule', 'yith-woocommerce-name-your-price' ); ?></span>

                                </td>
                            </tr>
                            <tr valign="top" class="titledesc">
                                <th scope="row">
                                    <label><?php _e( 'Rule behavior', 'yith-woocommerce-name-your-price' ); ?></label>
                                </th>
                                <td class="forminp forminp-checkbox">
                                    <input type="checkbox"
                                           name="_ywcnp_enable_rule" <?php checked( 'yes', $item[ 'enabled' ] ); ?> />
                                    <span
                                        class="description"><?php _e( 'If selected, the rule will be applied to the products of the selected categories. If not, the rule will be just created but not applied.', 'yith-woocommerce-name-your-price' ); ?></span>
                                </td>
                            </tr>
                            <tr valign="top" class="titledesc">
                                <th scope="row">
                                    <label><?php _e( 'Suggested Price', 'yith-woocommerce-name-your-price' ); ?></label>
                                </th>
                                <td class="forminp forminp-text">
                                    <input type="text" class="ywcnp_suggest_price wc_input_price"
                                           name="_ywcnp_suggest_price"
                                           value="<?php echo esc_attr( $item[ 'suggest_price' ] ); ?>"/>
                                    <span
                                        class="description"><?php _e( 'Select the suggested price for your product, leave blank not to suggest a price', 'yith-woocommerce-name-your-price' ); ?></span>
                                </td>
                            </tr>
                            <tr valign="top" class="titledesc">
                                <th scope="row">
                                    <label><?php _e( 'Minimum Price', 'yith-woocommerce-name-your-price' ); ?></label>
                                </th>
                                <td class="forminp forminp-text">
                                    <input type="text" class="ywcnp_min_price wc_input_price" name="_ywcnp_min_price"
                                           value="<?php echo esc_attr( $item[ 'min_price' ] ); ?>"/>
                                    <span
                                        class="description"><?php _e( 'Set the minimum price for your product, leave blank not to set a minimum price', 'yith-woocommerce-name-your-price' ); ?></span>
                                </td>
                            </tr>
                            <tr valign="top" class="titledesc">
                                <th scope="row">
                                    <label><?php _e( 'Maximum Price', 'yith-woocommerce-name-your-price' ); ?></label>
                                </th>
                                <td class="forminp forminp-text">
                                    <input type="text" class="ywcnp_max_price wc_input_price" name="_ywcnp_max_price"
                                           value="<?php echo esc_attr( $item[ 'max_price' ] ); ?>"/>
                                    <span
                                        class="description"><?php _e( 'Set the maximum price for your product, leave blank not to set a maximum price', 'yith-woocommerce-name-your-price' ); ?></span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <input
                            id="<?php echo $_GET[ 'action' ] ?>"
                            name="<?php echo $_GET[ 'action' ] ?>"
                            type="submit"
                            value="<?php echo( ( 'insert' == $_GET[ 'action' ] ) ? __( 'Add category rule', 'yith-woocommerce-name-your-price' ) : __( 'Update category rule', 'yith-woocommerce-name-your-price' ) ); ?>"
                            class="button-primary"
                        />
                        <a class="button-secondary"
                           href="<?php echo $list_url; ?>"><?php _e( 'Return to rule list', 'yith-woocommerce-name-your-price' ); ?></a>
                    </form>
                <?php else : ?>
                    <form id="custom-table" method="GET" action="<?php echo $list_url; ?>">
                        <input type="hidden" name="page" value="<?php echo $_GET[ 'page' ]; ?>"/>
                        <input type="hidden" name="tab" value="<?php echo $_GET[ 'tab' ]; ?>"/>
                        <?php $table->display(); ?>
                    </form>
                <?php endif; ?>
            </div>
            <?php


        }


        /**
         * Validate input fields
         *
         * @since   1.0.0
         * @author  Alberto Ruggiero
         *
         * @param   $item array POST data array
         *
         * @return  bool|string
         */
        private function validate_fields( $item )
        {

            $messages = array();

            if ( empty( $item[ 'category_ids' ] ) ) {
                $messages[] = __( 'Select at least one category', 'yith-woocommerce-name-your-price' );
            }

            if ( empty( $messages ) ) {
                return true;
            }

            return implode( '<br />', $messages );

        }

        /**
         * Add screen options for list table template
         *
         * @since   1.0.0
         * @return  void
         * @author  Alberto Ruggiero
         */
        public function add_options()
        {

            if ( 'yith-plugins_page_yith_wcnp_panel' == get_current_screen()->id && ( isset( $_GET[ 'tab' ] ) && $_GET[ 'tab' ] == 'category-rules' ) && ( !isset( $_GET[ 'action' ] ) || ( $_GET[ 'action' ] != 'edit' && $_GET[ 'action' ] != 'insert' ) ) ) {

                $option = 'per_page';

                $args = array(
                    'label' => __( 'Categories', 'yith-woocommerce-name-your-price' ),
                    'default' => 10,
                    'option' => 'items_per_page'
                );

                add_screen_option( $option, $args );

            }

        }

        /**
         * Set screen options for list table template
         *
         * @since   1.0.0
         *
         * @param   $status
         * @param   $option
         * @param   $value
         *
         * @return  mixed
         * @author  Alberto Ruggiero
         */
        public function set_options( $status, $option, $value )
        {

            return ( 'items_per_page' == $option ) ? $value : $status;

        }

    }
}
/**
 * return single instance
 * @author YITHEMES
 * @since 1.0.0
 * @return YWCNP_Category_Rules
 */
function YWCNP_Category_Rule()
{

    return YWCNP_Category_Rules::get_instance();
}

