<?php
if ( !defined( 'ABSPATH' ) )
    exit;

$variation_id = $variation->ID;
$variation_product = wc_get_product( $variation_id );
// Multivendor Compatibility
/*if (!ywcnp_is_multivendor_name_your_price_enabled() && !ywcnp_product_is_name_your_price($variation_id))
    return;*/

$is_nameyourprice = yit_get_prop( $variation_product, '_ywcnp_enabled_variation' );

?>
    <label><input type="checkbox" class="checkbox variable_is_nameyourprice"
                  name="variable_is_nameyourprice[<?php echo $loop; ?>]" <?php checked( isset( $is_nameyourprice ) ? $is_nameyourprice : '', 'yes' ); ?> /> <?php _e( 'Name Your Price', 'woocommerce' ); ?>
        <a class="tips"
           data-tip="<?php esc_attr_e( 'Enable this option to enable "Name Your Price" for this variation', 'yith-woocommerce-name-your-price' ); ?>"
           href="#">[?]</a></label>
<?php
