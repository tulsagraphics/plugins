<?php
if ( !defined( 'ABSPATH' ) )
    exit;

$variation_id = $variation->ID;
$variation_product = wc_get_product( $variation_id );
$product_id = yit_get_base_product_id( $variation_product );

$is_nameyourprice = yit_get_prop( $variation_product, '_ywcnp_enabled_variation' );
$last_category_rule_id = '';
// Multivendor Compatibility
if ( !ywcnp_is_multivendor_name_your_price_enabled() && !ywcnp_product_is_name_your_price( $variation_product ) )
    return;

$is_override = yit_get_prop( $variation, '_ywcnp_variation_is_override' );
$is_override = empty( $is_override ) ? 'no' : $is_override;

if ( 'no' == $is_override ) {

    $last_category_rule_id = ywcnp_product_has_rule( $product_id );

}

$suggest_price_label = sprintf( '%s %s ', __( 'Suggested Price', 'yith-woocommerce-name-your-price' ), '( ' . get_woocommerce_currency_symbol() . ' )' );
$min_price_label = sprintf( '%s %s ', __( 'Minimum Price', 'yith-woocommerce-name-your-price' ), '( ' . get_woocommerce_currency_symbol() . ' )' );
$max_price_label = sprintf( '%s %s ', __( 'MaximumPrice', 'yith-woocommerce-name-your-price' ), '( ' . get_woocommerce_currency_symbol() . ' )' );

$sugg_price = empty( $last_category_rule_id ) ? get_post_meta( $variation_id, '_ywcnp_simple_suggest_price', true ) : ywcnp_get_category_rule_vendor( $last_category_rule_id, 'sugg_price' );
$min_price = empty( $last_category_rule_id ) ? get_post_meta( $variation_id, '_ywcnp_simple_min_price', true ) : ywcnp_get_category_rule_vendor( $last_category_rule_id, 'min_price' );
$max_price = empty( $last_category_rule_id ) ? get_post_meta( $variation_id, '_ywcnp_simple_max_price', true ) : ywcnp_get_category_rule_vendor( $last_category_rule_id, 'max_price' );

$visibility_button_override = ( $is_override == 'no' && !empty( $last_category_rule_id ) ) ? 'display:block;' : 'display:none;';
$disable_input_field = ( $is_override == 'no' && !empty( $last_category_rule_id ) ) ? 'readonly' : '';

?>
<div class="show_if_variation_nameyourprice"
     style="display: <?php echo 'yes' == $is_nameyourprice ? 'block' : 'none'; ?>;">
    <p class="form-row form-row-full">
        <label><?php echo $suggest_price_label; ?></label>
        <input type="text" class="short wc_input_price" name="ywcnp_variation_suggest_price[<?php echo $loop; ?>]"
               value="<?php echo $sugg_price; ?>" <?php echo $disable_input_field; ?> />
    </p>

    <p class="form-row form-row-full">
        <label><?php echo $min_price_label; ?></label>
        <input type="text" class="short wc_input_price" name="ywcnp_variation_min_price[<?php echo $loop; ?>]"
               value="<?php echo $min_price; ?>" <?php echo $disable_input_field; ?> />
    </p>

    <p class="form-row form-row-full">
        <label><?php echo $max_price_label; ?></label>
        <input type="text" class="short wc_input_price" name="ywcnp_variation_max_price[<?php echo $loop; ?>]"
               value="<?php echo $max_price; ?>" <?php echo $disable_input_field; ?> />
    </p>

    <div class="show_if_variation_nameyourprice">
        <p class="form-row form-row-full ywcnp_container_override " style="<?php echo $visibility_button_override; ?>">
            <button type="button"
                    class="button ywcnp_btn_override"><?php _e( 'Overwrite this rule', 'yith-woocommerce-name-your-price' ); ?></button>
            <span
                class="description"><?php _e( 'These fields have been disabled because you have set a general rule for this category', 'yith-woocommerce-name-your-price' ); ?></span>
        </p>
    </div>
    <input type="hidden" name="ywcnp_variation_is_override[<?php echo $loop; ?>]" value="<?php echo $is_override; ?>">
</div>


