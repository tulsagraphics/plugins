<?php



$is_nameyourprice = yit_get_prop( $product, '_is_nameyourprice' );
$product_id =yit_get_product_id( $product );

if ( $is_nameyourprice ):

    $sugg_price = ywcnp_get_suggest_price( $product_id );
    $min_price = ywcnp_get_min_price( $product_id );
    $max_price = ywcnp_get_max_price( $product_id );

    $suggest_label = is_numeric( $sugg_price ) ? ywcnp_get_suggest_price_html( $product_id ) : '';
    $min_price_label = is_numeric( $min_price ) ? ywcnp_get_min_price_html( $product_id) : '';
    $max_price_label = is_numeric( $max_price ) ? ywcnp_get_max_price_html( $product_id ) : '';
    
    ?>

    <td class="grouped_name_your_price">
        <input type="text" class="ywcnp_amount" name="ywcnp_amount[<?php echo $product_id; ?>]"
               value="<?php echo esc_attr( $sugg_price ); ?>" data-suggest_price="<?php echo esc_attr( $sugg_price );?>"/>

       <!-- <div class="groupedprice"><?php echo $suggest_label; ?></div>-->
        <div class="groupedprice"><?php echo $min_price_label; ?></div>
        <div class="groupedprice"><?php echo $max_price_label; ?></div>
        <?php
        if ( $availability = $product->get_availability() ) {
            $availability_html = empty( $availability[ 'availability' ] ) ? '' : '<p class="stock ' . esc_attr( $availability[ 'class' ] ) . '">' . esc_html( $availability[ 'availability' ] ) . '</p>';
            echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability[ 'availability' ], $product );
        }
        ?>
    </td>
    <?php
endif;
