<?php
/**
 * Plugin Name: YITH WooCommerce Name Your Price Premium
 * Plugin URI: //yithemes.com/themes/plugins/yith-woocommerce-name-your-price/
 * Description: YITH WooCommerce Name Your Price Premium allows you to choose the purchase price of a product
 * Version: 1.0.22
 * Author: YITHEMES
 * Author URI: //yithemes.com/
 * Text Domain: yith-woocommerce-name-your-price
 * Domain Path: /languages/
 * WC requires at least: 3.0.0
 * WC tested up to: 3.4.2
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Name Your Price Premium
 * @version 1.0.22
 */

/*
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
*/
if ( !defined( 'ABSPATH' ) ) {
    exit;
}
if ( !function_exists( 'is_plugin_active' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}

function yith_wc_name_your_price_premium_install_woocommerce_admin_notice()
{
    ?>
    <div class="error">
        <p><?php _e( 'YITH WooCommerce Name Your Price Premium is enabled but not effective. It requires WooCommerce in order to work.', 'yith-woocommerce-name-your-price' ); ?></p>
    </div>
    <?php
}

if ( !function_exists( 'yit_deactive_free_version' ) ) {
    require_once 'plugin-fw/yit-deactive-plugin.php';
}
yit_deactive_free_version( 'YWCNP_FREE_INIT', plugin_basename( __FILE__ ) );


if ( !function_exists( 'yith_plugin_registration_hook' ) ) {
    require_once 'plugin-fw/yit-plugin-registration-hook.php';
}
register_activation_hook( __FILE__, 'yith_plugin_registration_hook' );


if ( !defined( 'YWCNP_VERSION' ) ) {
    define( 'YWCNP_VERSION', '1.0.22' );
}

if ( !defined( 'YWCNP_PREMIUM' ) ) {
    define( 'YWCNP_PREMIUM', '1' );
}

if ( !defined( 'YWCNP_INIT' ) ) {
    define( 'YWCNP_INIT', plugin_basename( __FILE__ ) );
}

if ( !defined( 'YWCNP_FILE' ) ) {
    define( 'YWCNP_FILE', __FILE__ );
}

if ( !defined( 'YWCNP_DIR' ) ) {
    define( 'YWCNP_DIR', plugin_dir_path( __FILE__ ) );
}

if ( !defined( 'YWCNP_URL' ) ) {
    define( 'YWCNP_URL', plugins_url( '/', __FILE__ ) );
}

if ( !defined( 'YWCNP_ASSETS_URL' ) ) {
    define( 'YWCNP_ASSETS_URL', YWCNP_URL . 'assets/' );
}

if ( !defined( 'YWCNP_ASSETS_PATH' ) ) {
    define( 'YWCNP_ASSETS_PATH', YWCNP_DIR . 'assets/' );
}

if ( !defined( 'YWCNP_TEMPLATE_PATH' ) ) {
    define( 'YWCNP_TEMPLATE_PATH', YWCNP_DIR . 'templates/' );
}

if ( !defined( 'YWCNP_INC' ) ) {
    define( 'YWCNP_INC', YWCNP_DIR . 'includes/' );
}
if ( !defined( 'YWCNP_SLUG' ) ) {
    define( 'YWCNP_SLUG', 'yith-woocommerce-name-your-price' );
}

if ( !defined( 'YWCNP_SECRET_KEY' ) ) {

    define( 'YWCNP_SECRET_KEY', 'iq2EtRHgWfS5LIE7sFDh' );
}

/* Plugin Framework Version Check */
if ( !function_exists( 'yit_maybe_plugin_fw_loader' ) && file_exists( YWCNP_DIR . 'plugin-fw/init.php' ) ) {
    require_once( YWCNP_DIR . 'plugin-fw/init.php' );
}
yit_maybe_plugin_fw_loader( YWCNP_DIR );


if ( !function_exists( 'yith_name_your_price_premium_init' ) ) {
    /**
     * Unique access to instance of YITH_Name_Your_Price class
     *
     * @return YITH_WooCommerce_Name_Your_Price
     * @since 1.0.0
     */
    function yith_name_your_price_premium_init()
    {

        load_plugin_textdomain( 'yith-woocommerce-name-your-price', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

        // Load required classes and functions

        require_once( YWCNP_INC . 'functions.yith-name-your-price.php' );
        require_once( YWCNP_INC . 'functions.yith-name-your-price-premium.php' );
        require_once( YWCNP_INC . 'class.yith-custom-table.php' );
        require_once( YWCNP_TEMPLATE_PATH . 'admin/ywcnp-category-rules.php' );
        require_once( YWCNP_INC . 'classes/class.yith-wc-name-your-price-admin.php' );
        require_once( YWCNP_INC . 'classes/compatibility/class.yith-wc-name-your-price-compatibility.php' );
        require_once( YWCNP_INC . 'classes/class.yith-wc-name-your-price-premium-admin.php' );
        require_once( YWCNP_INC . 'classes/class.yith-wc-name-your-price-frontend.php' );
        require_once( YWCNP_INC . 'classes/class.yith-wc-name-your-price-premium-frontend.php' );
        require_once( YWCNP_INC . 'classes/class.yith-wc-name-your-price.php' );
        require_once( YWCNP_INC . 'classes/class.yith-wc-name-your-price-premium.php' );

        if (  !empty( $GLOBALS['woocommerce-aelia-currencyswitcher'] )  ) {
            require_once( YWCNP_INC . 'classes/compatibility/class.yith-wc-aeliacs-module.php' );
        }

        global $YWC_Name_Your_Price;
        $YWC_Name_Your_Price = YITH_WooCommerce_Name_Your_Price_Premium::get_instance();

    }
}

add_action( 'ywcnp_premium_init', 'yith_name_your_price_premium_init' );

if ( !function_exists( 'yith_name_your_price_premium_install' ) ) {

    function yith_name_your_price_premium_install()
    {

        if ( !function_exists( 'WC' ) ) {
            add_action( 'admin_notices', 'yith_wc_name_your_price_premium_install_woocommerce_admin_notice' );
        } else
            do_action( 'ywcnp_premium_init' );
    }
}

add_action( 'plugins_loaded', 'yith_name_your_price_premium_install', 11 );