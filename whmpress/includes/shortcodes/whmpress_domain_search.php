<?php

$last_synced = get_option("sync_time");
if ($last_synced == "") {
    echo "<div style='color: red;' '>WHMCS is not yet synced</div>";
    echo "<div><a href='wp-admin/admin.php?page=whmp-sync'>Click here to Sync</a></div>";
}


/**
 * Created by PhpStorm.
 * User: fokado
 * Date: 12/30/2017
 * Time: 3:51 PM
 */

/**
 * Generates HTML form for search domains
 *
 * List of parameters
 * show_tlds = provide comma seperated tlds e.g. .com,.net,.org or leave it blank for all tlds
 * show_tlds_wildcard = provide tld search as wildcard, e.g. pk for all .pk domains or co for all com and .co domains
 * show_combo = Yes or No for display or hide combo box of tlds
 * placeholder = HTML placeholder for input search box
 * text_class = HTML class for wrapper of input search box
 * combo_class = HTML class for wrapper of combo box
 * button_class = HTML class for wrapper of submit button of form
 * action = Specify url where form will submit
 * button_text = Button text for submit button
 * html_class = HTML class for wrapper of form
 * html_id = HTML id for wrapper of form
 */

$WHMPress = new WHMpress();

$params = shortcode_atts( [
    'html_template'      => '',
    'image'              => '',
    'show_tlds'          => '',
    'show_tlds_wildcard' => '',
    'show_combo'         => whmpress_get_option( 'ds_show_combo' ), //'no',
    'placeholder'        => whmpress_get_option( 'ds_placeholder' ),
    'text_class'         => 'search_div',
    'combo_class'        => 'select_div',
    'button_class'       => 'submit_div',
    'action'             => $WHMPress->get_whmcs_url( "domainchecker" ),
    'button_text'        => whmpress_get_option( 'ds_button_text' ), //'Search',
    'html_class'         => 'whmpress whmpress_domain_search',
    'html_id'            => '',
    'token'              => '',
], $atts );
extract( $params );

# Getting WordPress DB object
global $wpdb;

# Generating and setting combo box if it will display
if ( strtolower( $show_combo ) == "yes" ):
    $Q = "SELECT `extension` FROM `" . whmp_get_domain_pricing_table_name() . "` WHERE 1";
    if ( $show_tlds <> "" ) {
        $show_tlds = explode( ",", $show_tlds );
        $Q .= " AND (`extension`='" . implode( "' OR `extension`='", $show_tlds ) . "')";
    } elseif ( $show_tlds_wildcard <> "" ) {
        $Q .= " AND `extension` LIKE '%{$show_tlds_wildcard}%'";
    }
    $Q .= " ORDER BY `extension`";
    $rows = $wpdb->get_results( $Q, ARRAY_A );
    if ( $show_tlds <> "" ) {
        $tmps = $rows;
        $rows = array();
        foreach ($show_tlds as $show_tld) {
            foreach ($tmps as $tmp) {
                if ($tmp['extension'] == $show_tld) {
                    $rows[] = $tmp;
                }
            }
        }
    }
endif;


# Generating output string
$ACTION = empty( $action ) ? "" : "action='$action'";
$str    = "<form method=\"get\" id=\"searchDomainForm\" $ACTION>";
$params = parse_url( $action );
if ( ! isset( $params["query"] ) ) {
    $params["query"] = "";
}
if ( $params["query"] <> "" ) {
    parse_str( $params["query"], $params );
    foreach ( $params as $key => $val ) {
        $str .= "<input type=\"hidden\" value=\"{$val}\" name=\"{$key}\">";
    }
}
if ($token != ""){
    $str .= "<input type=\"hidden\" name=\"token\" value=\"{$token}\">";
}
$str .= "<!--input type=\"hidden\" name=\"token\" value=\"24372f4f06ca835d9101d60a258c30a4c93b3bf7\">
    <input type=\"hidden\" name=\"direct\" value=\"true\"-->";
$str .= "<div class=\"{$text_class} search_div\">";
$val = isset( $_GET["search_domain"] ) ? $_GET["search_domain"] : "";
$str .= "<input required='required' title='" . __( "Please fill out this field", "whmpress" ) . "' type=\"search\" name=\"sld\" id=\"search_domain\" placeholder=\"" . __( $placeholder, "whmpress" ) . "\" value=\"{$val}\" />\n";
$str .= "</div>";

$WHMPress      = new WHMPress;
$html_template = $WHMPress->check_template_file( $html_template, "whmpress_domain_search" );

$smarty_array                   = [];
$smarty_array["params"]         = $params;
$smarty_array["params_encoded"] = whmpress_json_encode( $params );
$smarty_array["class"]          = $combo_class;
$smarty_array["rows"]           = [];
if ( strtolower( $show_combo ) == "yes" ) {
    $smarty_array["rows"] = $rows;
    $Combo                = "<div class=\"{$combo_class} select_div\">";

    $Combo .= "<select name='tld'>";
    foreach ( $rows as $row ) {
        $Combo .= "<option>" . $row["extension"] . "</option>\n";
    }
    $Combo .= "</select>";
    $Combo .= "</div>";
    if ( ! is_file( $html_template ) ) {
        $str .= $Combo;
    }
} else {
    $str .= "<input type='hidden' name='ext' value='' />\n";
    $Combo = "";
}
$str .= "<div class=\"{$button_class} submit_div\">";
$str .= "<input type=\"submit\" value=\"{$button_text}\">";
$str .= "</div>";
$str .= "</form>";


if ( is_file( $html_template ) ) {
    $vars = [
        "search_text_box" => "<input required='required' type=\"search\" name=\"domain\" id=\"search_domain\" placeholder=\"" . __( $placeholder, "whmpress" ) . "\" value=\"{$val}\" />",
        "search_combo"    => $Combo,
        "search_button"   => "<input type=\"submit\" value=\"{$button_text}\">",
        "action_url"      => $action,
        "combo_data"      => $smarty_array,
    ];

    # Getting custom fields and adding in output
    $TemplateArray = $WHMPress->get_template_array( "whmpress_domain_search" );
    foreach ( $TemplateArray as $custom_field ) {
        $vars[ $custom_field ] = isset( $atts[ $custom_field ] ) ? $atts[ $custom_field ] : "";
    }

    $OutputString = whmp_smarty_template( $html_template, $vars );

    return $OutputString;
} else {

    # Returning output string including wrapper div
    $ID    = ! empty( $html_id ) ? "id='$html_id'" : "";
    $CLASS = ! empty( $html_class ) ? "class='$html_class'" : "";

    return "<div $CLASS $ID>" . $str . "</div>";
}
