<?php
/**
 * Handles the product variable meta HTML
 *
 * The html markup for the product variable
 * 
 * @package WooCommerce - PDF Vouchers
 * @since 1.1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

global $woo_vou_voucher;

$prefix					= WOO_VOU_META_PREFIX;

$variation_id 			= isset($variation->ID) ? $variation->ID : '';

if( empty( $variation_id) ) 
	$variation_id = isset( $variation['id'] ) ? $variation['id'] : '';

$woo_vou_variable_codes = get_post_meta( $variation_id, $prefix . 'codes', true ); // Getting voucher code

$woo_vou_variable_vendor_address = get_post_meta( $variation_id, $prefix . 'vendor_address', true ); // Getting vendor Address

$voucher_data			= $woo_vou_voucher->woo_vou_get_vouchers(); // Getting All Voucher Templates
$woo_vou_pdf_template	= get_post_meta( $variation_id, $prefix . 'pdf_template', true ); // Getting Selected Voucher Template

$variable_voucher_delivery	= array(
							'default'	=> __( 'Default', 'woovoucher' ), 
							'email' 	=> __( 'Email', 'woovoucher' ), 
							'offline' 	=> __( 'Offline', 'woovoucher' )
						); // Set voucher delivery options
$woo_vou_voucher_delivery	= get_post_meta( $variation_id, $prefix . 'voucher_delivery', true ); // Getting Selected Voucher Delivery
?>

<div class="show_if_variation_downloadable" style="display:none;">
	<p class="form-field variable_pdf_template_field form-row form-row-first">
		<label for="woo-vou-pdf-variable-pdf-template-<?php echo $loop; ?>" class="woo-vou-pdf-template-variation-label"><?php _e('PDF Template', 'woovoucher'); ?></label>
		<span data-tip="<?php _e( 'Select a PDF template.', 'woovoucher' ); ?>" class="woocommerce-help-tip"></span>
		<select style="width:100%;" class="chosen_select" name="<?php echo $prefix; ?>variable_pdf_template[<?php echo $loop; ?>]" id="woo-vou-pdf-variable-pdf-template-<?php echo $loop; ?>">
			<option value=""><?php _e('Please Select', 'woovoucher'); ?></option>
				<?php foreach ( $voucher_data as $voucher ) { ?>
					<option value="<?php echo $voucher['ID']; ?>" <?php if( $woo_vou_pdf_template == $voucher['ID'] ) echo "selected=selected"; ?>><?php echo $voucher['post_title']; ?></option>
				<?php } ?>
		</select>
	</p>

	<p class="form-field variable_voucher_delivery_field form-row form-row-last">
		<label for="woo-vou-pdf-variable-voucher-delivery-<?php echo $loop; ?>" class="woo-vou-voucher-delivery-variation-label"><?php _e('Voucher Delivery', 'woovoucher'); ?></label>
		<span data-tip="<?php _e( 'Choose how your customer receives the PDF Voucher.', 'woovoucher' ); ?>" class="woocommerce-help-tip"></span>
		<select style="width:100%;" class="chosen_select" name="<?php echo $prefix; ?>variable_voucher_delivery[<?php echo $loop; ?>]" id="woo-vou-pdf-variable-voucher-delivery-<?php echo $loop; ?>">
			<?php foreach ( $variable_voucher_delivery as $voucher_delivery_key => $voucher_delivery_value ) { ?>
				<option value="<?php echo $voucher_delivery_key; ?>" <?php if( $woo_vou_voucher_delivery == $voucher_delivery_key ) echo "selected=selected"; ?>><?php echo $voucher_delivery_value; ?></option>
			<?php } ?>
		</select>
	</p>
	
	<p class="form-field variable_codes_field form-row form-row-full">
		<label for="woo-vou-variable-codes-<?php echo $loop; ?>"><?php _e('Voucher Codes', 'woovoucher'); ?></label>
		<span data-tip="<?php _e( 'If you have a list of Voucher Codes you can copy and paste them in to this option. Make sure, that they are comma separated.', 'woovoucher' ); ?>" class="woocommerce-help-tip"></span>
		<textarea style="width:100%" rows="2" placeholder="" id="woo-vou-variable-codes-<?php echo $loop; ?>" name="<?php echo $prefix; ?>variable_codes[<?php echo $loop; ?>]" class="short"><?php echo $woo_vou_variable_codes; ?></textarea>
	</p>
        
    <p class="form-field variable_vendor_address_field form-row form-row-full">
		<label for="woo-vou-variable-vendor-address-<?php echo $loop; ?>"><?php _e('Vendor Address', 'woovoucher'); ?></label>
		<span data-tip="<?php _e( 'Here you can enter the complete Vendor\'s address. This will be displayed on the PDF document sent to the customers so that they know where to redeem this Voucher. Limited HTML is allowed.', 'woovoucher' ); ?>" class="woocommerce-help-tip"></span>
		<textarea style="width:100%" rows="2" placeholder="" id="woo-vou-variable-vendor-address-<?php echo $loop; ?>" name="<?php echo $prefix; ?>variable_vendor_address[<?php echo $loop; ?>]" class="short"><?php echo $woo_vou_variable_vendor_address; ?></textarea>
	</p>
</div>