<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Woo_Wou_Settings' ) ) :

/**
 * Setting page Class
 * 
 * Handles Settings page functionality of plugin
 * 
 * @package WooCommerce - PDF Vouchers
 * @since 2.9.8
 */
class Woo_Wou_Settings extends WC_Settings_Page {

	/**
	 * Constructor
	 * 
	 * Handles to add hooks for adding settings
	 * 
	 * @package WooCommerce - PDF Vouchers
	 * @since 2.9.8
	 */
	public function __construct() {

		global $woo_vou_model, $woo_vou_render, $woo_vou_voucher; // Declare global variables

		$this->id    	= 'woo-vou-settings'; // Get id
		$this->label 	= __( 'PDF Vouchers', 'woovoucher' ); // Get tab label
		$this->model 	= $woo_vou_model; // Declare variable $this->model
		$this->render 	= $woo_vou_render; // Declare variable $this->render
		$this->voucher	= $woo_vou_voucher; // Declare variable $this->voucher

		// Add filter for adding tab
		add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_page' ), 20 );

		// Add action to show output
		add_action( 'woocommerce_settings_' . $this->id, array( $this, 'woo_vou_output' ) );

		// Add action for saving data
		add_action( 'woocommerce_settings_save_' . $this->id, array( $this, 'woo_vou_save' ) );

		// Add action for adding sections
		add_action( 'woocommerce_sections_' . $this->id, array( $this, 'output_sections' ) );

		// Add action to add custom field for setting page
		add_action( 'woocommerce_admin_field_vou_filename', array( $this->render, 'woo_vou_render_filename_callback' ) );
		add_action( 'woocommerce_admin_field_vou_upload', array( $this->render, 'woo_vou_render_upload_callback' ) );
		add_action( 'woocommerce_admin_field_vou_preview_upload', array( $this->render, 'woo_vou_render_preview_upload_callback' ) );
		add_action( 'woocommerce_admin_field_vou_textarea', array( $this->render, 'woocommerce_admin_field_vou_textarea' ) );
		add_action( 'woocommerce_admin_field_vou_datetime_picker', array( $this->render, 'woocommerce_admin_field_vou_datetime_picker' ) );
		add_action( 'woocommerce_admin_field_woo_vou_button', array( $this->render, 'woo_vou_button_callback' ) );
	}

	/**
	/**
	 * Handles to add sections for Settings tab
	 * 
	 * @package WooCommerce - PDF Vouchers
	 * @since 2.9.8
	 */
	public function get_sections() {

		// Create array
		$sections = array(
			''          			=> __( 'General Settings', 'woovoucher' ),
			'vou_voucher_settings'  => __( 'Voucher Settings', 'woovoucher' ),
			'vou_misc_settings' 	=> __( 'Misc Settings', 'woovoucher' )
		);

		return apply_filters( 'woo_vou_setting_sections', $sections );
	}

	/**
	 * Handles to output data
	 * 
	 * @package WooCommerce - PDF Vouchers
	 * @since 2.9.8
	 */
	public function woo_vou_output() {

		// Get global variable
		global $current_section;

		// Get settings for current section
		$settings = $this->get_settings( $current_section );

		WC_Admin_Settings::output_fields( $settings );
	}

	/**
	 * Handles to save data
	 * 
	 * @package WooCommerce - PDF Vouchers
	 * @since 2.9.8
	 */
	public function woo_vou_save() {
		global $current_section;
		
		if( isset( $_POST['vou_gift_notification_time'] ) && $_POST['vou_gift_notification_time'] !== '' ) {
			
			// first clear the schedule	
			wp_clear_scheduled_hook( 'woo_vou_send_gift_notification' );
			
			// if not scheduled cron
			if ( ! wp_next_scheduled( 'woo_vou_send_gift_notification' ) ) {
				
				$utc_timestamp = time();
				$local_time = current_time( 'timestamp' ); // to get current local time
				
				// Schedule CRON events starting at user defined hour and periodically thereafter
				// mktime( hours, minute, second, month, day, year )
				$schedule_time 	= mktime( $_POST['vou_gift_notification_time'], 0, 0, date('m', $local_time), date('d', $local_time), date('Y', $local_time) );
				
				// get difference 
				$diff 		= ( $schedule_time - $local_time );
				$utc_timestamp 	= $utc_timestamp + $diff;
				
				wp_schedule_event( $utc_timestamp, 'daily', 'woo_vou_send_gift_notification' );	
			}			
		}

		// If pdf delete time is not empty
		if( !empty( $_POST['vou_pdf_delete_time'] ) ){

			// first clear the schedule	
			wp_clear_scheduled_hook( 'woo_vou_flush_upload_dir_cron' );

			// if not scheduled cron
			if ( ! wp_next_scheduled( 'woo_vou_flush_upload_dir_cron' ) ) {

				// schedule event
				wp_schedule_event( time(), $_POST['vou_pdf_delete_time'], 'woo_vou_flush_upload_dir_cron' );
			}
		}

		// Assign our partial products change to option name in woocommerce so woocommerce saves user changes from partial products
		$_POST['vou_partial_redeem_product_ids'] = !empty($_POST['woo_vou_selected_products']) ? $_POST['woo_vou_selected_products'] : '';

		$settings = $this->get_settings( $current_section );
		WC_Admin_Settings::save_fields( $settings );
	}

	/**
	 * Handles to get setting
	 * 
	 * @package WooCommerce - PDF Vouchers
	 * @since 2.9.8
	 */
	public function get_settings( $current_section = '' ) {

		$voucher_options	= array(); // Declare variable for voucher options
		$voucher_data		= $this->voucher->woo_vou_get_vouchers(); // Get Voucher data

		// Loop on voucher data
		foreach ( $voucher_data as $voucher ) {

			// Check voucher id is not empty
			if( isset( $voucher['ID'] ) && !empty( $voucher['ID'] ) ) {
				
				$voucher_options[$voucher['ID']] = $voucher['post_title']; // Get voucher id
				$multiple_voucher_options[$voucher['ID']] = $voucher['post_title']; // Get voucher post title
			}
		}

		// Usability options
		$usability_options = array(
			'0'	=> __('One time only', 'woovoucher'),
			'1'	=> __('Unlimited', 'woovoucher')
		);

		// Voucher price options
		$voucher_price_options	= array(
			''	=> __('Sale Price', 'woovoucher'),
			'1'	=> __('Regular Price', 'woovoucher'),
			'2'	=> __('Custom Voucher Price', 'woovoucher')
		);

		// Voucher delivery options
		$voucher_delivery_options	= array(
			'email' 	=> __('Email', 'woovoucher'),
			'offline'	=> __('Offline', 'woovoucher')
		);

		// Recipient Form options
		$recipient_form_options = array(
			'1' => __( 'Above add to cart button ', 'woovoucher' ),
			'2' => __( 'Below add to cart button', 'woovoucher' )
		);
										
		// Gift notification schedule time options		
		$all_schedule_time_options = array(
			'0'	=> __( '12 AM', 'woovoucher' ),
			'1'	=> __( '1 AM', 'woovoucher' ),
			'2'	=> __( '2 AM', 'woovoucher' ),
			'3'	=> __( '3 AM', 'woovoucher' ),
			'4'	=> __( '4 AM', 'woovoucher' ),
			'5'	=> __( '5 AM', 'woovoucher' ),
			'6'	=> __( '6 AM', 'woovoucher' ),
			'7'	=> __( '7 AM', 'woovoucher' ),
			'8'	=> __( '8 AM', 'woovoucher' ),
			'9'	=> __( '9 AM', 'woovoucher' ),
			'10'=> __( '10 AM', 'woovoucher' ),
			'11'=> __( '11 AM', 'woovoucher' ),
			'12'=> __( '12 PM', 'woovoucher' ),
			'13'=> __( '1 PM', 'woovoucher' ),
			'14'=> __( '2 PM', 'woovoucher' ),
			'15'=> __( '3 PM', 'woovoucher' ),
			'16'=> __( '4 PM', 'woovoucher' ),
			'17'=> __( '5 PM', 'woovoucher' ),
			'18'=> __( '6 PM', 'woovoucher' ),
			'19'=> __( '7 PM', 'woovoucher' ),
			'20'=> __( '8 PM', 'woovoucher' ),
			'21'=> __( '9 PM', 'woovoucher' ),
			'22'=> __( '10 PM', 'woovoucher' ),
			'23'=> __( '11 PM', 'woovoucher' ),
		);

		// Gift notification schedule time options		
		$pdf_delete_cron_schedules = array(
			'hourly' 		=> __( 'Hourly', 'woovoucher' ),
			'daily'			=> __( 'Daily', 'woovoucher' ),
			'twicedaily'	=> __( 'Twice Daily', 'woovoucher' )
		);

		// Declare option for expiry type options
		$expiry_type_options = array(
										'specific_date' 	=> __('Specific Time', 'woovoucher'),
										'based_on_purchase' => __('Based on Purchase', 'woovoucher'),
									);

		// Declare variable for based on purchase options
		$based_on_purchase_opt  = array(
										'7' 		=> '7 Days',
										'15' 		=> '15 Days',
										'30' 		=> '1 Month (30 Days)',
										'90' 		=> '3 Months (90 Days)',
										'180' 		=> '6 Months (180 Days)',
										'365' 		=> '1 Year (365 Days)',
										'cust'		=> 'Custom',
									);

		// Filter to change date picker format
		$woo_vou_vou_start_end_date_format = apply_filters( 'woo_vou_vou_start_end_date_format', 'dd-mm-yy' );

		// If voucher settings are selected
		if ( 'vou_voucher_settings' == $current_section ) {

			$settings = apply_filters( 'woo_vou_voucher_settings', array(

				array( 
					'name'	=>	__( 'Voucher Settings', 'woovoucher' ),
					'type'	=>	'title',
					'desc'	=>	'',
					'id'	=>	'vou_voucher_settings'
				),
				array(
					'id'		=> 'vou_site_logo',
					'name'		=> __( 'Site Logo', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Here you can upload a logo of your site. This logo will be displayed on the voucher as the Site Logo', 'woovoucher' ).'</p>',
					'type'		=> 'vou_upload',
					'size'		=> 'regular'
				),
				array(
					'id'		=> 'vou_pdf_template',
					'name'		=> __( 'PDF Template', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Select a PDF template.', 'woovoucher' ).'</p>',
					'type'		=> 'select',
					'class'		=> 'wc-enhanced-select',
					'options'	=> $voucher_options
				),
				array(
					'id'		=> 'vou_pdf_usability',
					'name'		=> __( 'Usability', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Choose how you wanted to use vouchers codes.<br />If you set usability "<b>One time only</b>" then it will automatically set product quantity equal to a number of voucher codes entered and it will automatically decrease quantity by 1 when it gets purchased. If you set usability "<b>Unlimited</b>" then the plugin will automatically generate unique voucher codes when the product purchased. ', 'woovoucher' ).'</p>',
					'type'		=> 'select',
					'class'		=> 'wc-enhanced-select',
					'options'	=> $usability_options
				),
				array(
					'id'		=> 'multiple_pdf',
					'name'		=> __( 'Multiple Voucher', 'woovoucher' ),
					'desc'		=> __( 'Enable 1 voucher per PDF', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">'.__( 'Check this box if you want to generate 1 PDF for 1 voucher code instead of creating 1 combined PDF for all vouchers.', 'woovoucher' ).'</p>'
				),
				array(
					'id'		=> 'revoke_voucher_download_link_access',
					'name'		=> __( 'Remove Used / Expired Voucher Download Link', 'woovoucher' ),
					'desc'		=> __( 'Remove Used / Expired Voucher Download Link', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">' . sprintf(__( 'Check this box if you want to remove voucher download link, when voucher is %1$sused%2$s or %1$sexpired%2$s.', 'woovoucher' ), '<strong>', '</strong>' ) . '</p>'
				),
				array(
					'id'		=> 'vou_change_expiry_date',
					'name'		=> __( 'Allow Changing Voucher Expiry Date', 'woovoucher' ),
					'desc'		=> __( 'Allow Changing Voucher Expiry Date', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">'.__( 'Check this box if you want to allow admin/vendors to change voucher expiry date when voucher is <b>purchased</b> or <b>unused</b> from voucher code page.', 'woovoucher' ).'</p>'
				),
				array(
					'id'		=> 'vou_change_template',
					'name'		=> __( 'Allow Change Voucher Template', 'woovoucher' ),
					'desc'		=> __( 'Allow Change Voucher Template', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">'.__( 'Check this box if you want to allow admin/vendors to change voucher template when voucher is <b>purchased</b>, <b>used</b> or <b>unused</b> from voucher code details page.', 'woovoucher' ).'</p>'
				),
				array(
					'id'		=> 'vou_attach_processing_mail',
					'name'		=> __( 'Voucher as Attachment', 'woovoucher' ),
					'desc'		=> __( 'Send voucher PDF as attachment in <b>processing</b> / <b>completed</b> order email.', 'woovoucher' ),
					'type'		=> 'checkbox',
					'checkboxgroup'   => 'start',
				),
				array(
					'id'		=> 'vou_attach_gift_mail',
					'name'		=> '',
					'desc'		=> __( 'Send voucher PDF as attachment in <b>gift notification</b> email.', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">'.__( 'Here you can set where you want to send PDF voucher as an attachment in mail.', 'woovoucher' ).'</p>',
					'checkboxgroup'   => 'end',
				),

				array(
					'id'		=> 'vou_download_processing_mail',
					'name'		=> __( 'Show / Hide Voucher Download Link', 'woovoucher' ),
					'desc'		=> __( 'Allow Voucher to download from <b>processing</b> / <b>completed</b> order mail and <b>order thank you page</b>.', 'woovoucher' ),
					'type'		=> 'checkbox',
					'checkboxgroup'   => 'start',
				),
				array(
					'id'		=> 'vou_download_gift_mail',
					'name'		=> '',
					'desc'		=> __( 'Allow voucher to download from <b>gift notification</b> mail.', 'woovoucher' ),
					'type'		=> 'checkbox',
					'checkboxgroup'   => '',
				),
				array(
					'id'		=> 'vou_download_dashboard',
					'name'		=> '',
					'desc'		=> __( 'Allow voucher to download from <b>dashboard -> Downloads</b> page.', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">'.__( 'Allow customer can download the voucher from which location.', 'woovoucher' ).'</p>',
					'checkboxgroup'   => 'end',
				),
				array(
					'id'		=> 'vou_allow_redeem_expired_voucher',
					'name'		=> __( 'Allow Redemption for Expired Vouchers', 'woovoucher' ),
					'desc'		=> __( 'Allow Redemption for Expired Vouchers', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">'.__( 'Check this box if you want to allow redeem voucher codes after expiration date.', 'woovoucher' ).'</p>'
				),
				array(
					'id'		=> 'vou_allow_unlimited_redeem_vou_code',
					'name'		=> __( 'Allow Unlimited Redemption of Vouchers', 'woovoucher' ),
					'desc'		=> __( 'Allow Unlimited Redemption of Vouchers', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">'.__( 'Check this box if you want to allow users to redeem same voucher code unlimited times.', 'woovoucher' ).'</p>'
				),
				array(
					'id'		=> 'vou_enable_logged_user_check_voucher_code',
					'title'		=> __( 'Access for Check Voucher Code Page', 'woovoucher' ),
					'desc'		=> __( 'Check this box to allow <b>logged in users</b> to access check voucher code page.', 'woovoucher' ),
					'type'		=> 'checkbox',
					'checkboxgroup'   => 'start',
				),
				array(
					'id'		=> 'vou_enable_guest_user_check_voucher_code',
					'title'		=> '',
					'desc'		=> __( 'Check this box to allow <b>guest users</b> to access check voucher code page.', 'woovoucher' ),
					'desc_tip'	=> '<p class="description">'.__( 'By default <b>admin</b> and <b>vendors</b> have access to check voucher code page.', 'woovoucher' ).'</p>',
					'type'		=> 'checkbox',
					'checkboxgroup'   => 'end',
				),
				array(
					'id'		=> 'vou_enable_logged_user_redeem_vou_code',
					'name'		=> __( 'Allow redemption of own purchased vouchers for logged in users', 'woovoucher' ),
					'desc'		=> __( 'Allow redemption of own purchased vouchers for <b>logged in users</b>', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">'.__( 'Check this box to allow logged in users to <b>redeem own purchased vouchers.</b>', 'woovoucher' ).'</p>'
				),
				array(
					'id'		=> 'vou_enable_permission_vou_download_recipient_user',
					'name'		=> __( 'Allow Direct Access from Downloads Page', 'woovoucher' ),
					'desc'		=> __( 'Allow Direct Access from Downloads Page', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">'.__( 'Check this box to allow recipient users to download PDF VOuchers from downloads page.', 'woovoucher' ).'</p>'
				),
				// Add "Enable Voucher Code For Regular Price" to Voucher Setting
				array(
					'id'		=> 'vou_voucher_price_options',
					'name'		=> __( 'Default Voucher Value', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'This option determines default voucher value.<br /><b>Sale Price</b> - Voucher price would be same as product price.<br /><b>Regular Price</b> - Voucher price would be regular price when product sold at either regular price or sale price.<br /><b>Custom Voucher Price</b> - This will add new field of custom voucher price below sale price in product edit page. Voucher price would be same as custom voucher price when product sold at either regular price or sale price.', 'woovoucher' ).'</p>',
					'type'		=> 'select',
					'class'		=> 'wc-enhanced-select',
					'options'	=> $voucher_price_options
				),
				// Add "Enable Voucher Delivery" to Voucher Settings
				array(
					'id'		=> 'vou_voucher_delivery_options',
					'name'		=> __( 'Voucher Delivery', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Choose how your customer receives the "PDF Voucher"<br /><b>Email</b> - Customer receives "PDF Voucher" through email.<br /><b>Offline</b> - You will have to send voucher through physical mode, via post or on-shop.', 'woovoucher' ).'</p>',
					'type'		=> 'select',
					'default'	=> 'email',
					'class'		=> 'wc-enhanced-select',
					'options'	=> $voucher_delivery_options
				),
				array(
					'id'		=> 'vou_exp_type',
					'name'		=> __( 'Expiration Date Type', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'This option determines default voucher expiration type.<br /><b>Specific Time</b> - Voucher will be valid between Start date and Expiry date.<br /><b>Based on Purchase</b> - Voucher will be valid for x days after purchase.', 'woovoucher' ).'</p>',
					'type'		=> 'select',
					'default'	=> 'specific_date',
					'class'		=> 'vou_exp_type wc-enhanced-select',
					//'desc_tip'	=> true,
					'options'	=> $expiry_type_options
				),
				array(
					'id'		=> 'vou_start_date',
					'name'		=> __( 'Start Date', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Enter a start date here if you want to make the voucher codes valid for a specific time only.', 'woovoucher' ).'</p>',
					'type'		=> 'vou_datetime_picker',
					'default'	=> '',
					'class'		=> 'vou_start_date',
					'format'	=> $woo_vou_vou_start_end_date_format
					//'desc_tip'	=> true,
				),
				array(
					'id'		=> 'vou_exp_date',
					'name'		=> __( 'Expiration Date', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'If you want to make the voucher codes valid for a specific time only, you can enter a expiration date here. Leave it empty if you want to make voucher code never expire.', 'woovoucher' ).'</p>',
					'type'		=> 'vou_datetime_picker',
					'default'	=> '',
					'class'		=> 'vou_start_date',
					'format'	=> $woo_vou_vou_start_end_date_format
					//'desc_tip'	=> true,
				),
				array(
					'id'		=> 'vou_days_diff',
					'name'		=> __( 'Expiration Days', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Choose expiration days. This will be counted from day of purchase of voucher code.', 'woovoucher' ).'</p>',
					'type'		=> 'select',
					'class'		=> 'wc-enhanced-select',
					'options'	=> $based_on_purchase_opt
				),
				array(
					'title'    => __( 'Number of Days', 'woovoucher' ),
					'desc'     => __( 'This sets the number of days after which voucher code will expire. It will be counted from day of purchase', 'woovoucher' ),
					'id'       => 'vou_custom_days',
					'css'      => 'width:50px;',
					'default'  => '',
					'type'     => 'number'
				),
				// End Voucher Option section
				array( 
					'type' 		=> 'sectionend',
					'id' 		=> 'vou_voucher_settings'
				),
				array( 
					'name'	=>	__( 'Email Settings', 'woovoucher' ),
					'type'	=>	'title',
					'desc'	=>	'',
					'id'	=>	'vou_email_settings'
				),
				array(
					'id'		=> 'vou_allow_bcc_to_admin',
					'name'		=> __( 'Send Emails to Admin', 'woovoucher' ),
					'desc'		=> __( 'Send Emails to Admin', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">'.__( 'Check this box if you want to send customer order email and gift notification email to admin.', 'woovoucher' ).'</p>'
				),
				array( 
					'type' 		=> 'sectionend',
					'id' 		=> 'vou_email_settings'
				),
			));
		} elseif ( 'vou_misc_settings' == $current_section ) { // If misc settings is selected

			$settings = apply_filters( 'woo_vou_misc_settings', array(

				// Start Misc Settings section
				array( 
					'name'	=>	__( 'Misc Settings', 'woovoucher' ),
					'type'	=>	'title',
					'desc'	=>	'',
					'id'	=>	'vou_misc_settings'
				),
				array(
					'title'         => __( 'Delete Options', 'woovoucher' ),
					'id'            => 'vou_delete_options',
					'default'       => 'no',
					'desc_tip'      => __( 'Check this box If you don\'t want to use the Pdf Voucher Plugin on your site anymore. This makes sure, that all the settings and tables are being deleted from the database when you deactivate the plugin.', 'woovoucher' ),
					'type'          => 'checkbox'
				),
				array(
					'id'		=> 'woo_vou_gen_sys_log',
					'name'		=> __( 'System Report', 'woovoucher' ),
					'type'		=> 'woo_vou_button',
					'class'		=> 'button',
					'btn_title' => __( 'Generate System Report', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Please generate system report file and provide us in your ticket while contacting support.','woovoucher' ).'</p>'
				),
				// End Voucher Option section
				array( 
					'type' 		=> 'sectionend',
					'id' 		=> 'vou_misc_settings'
				),

				array( 
					'name'	=>	__( 'PDF Settings', 'woovoucher' ),
					'type'	=>	'title',
					'desc'	=>	'',
					'id'	=>	'vou_pdf_settings'
				),
				array(
					'id'		=> 'vou_char_support',
					'name'		=> __( 'Characters not Displaying Correctly?', 'woovoucher' ),
					'desc'		=> __( 'Enable characters support', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">'.__( 'Check this box to enable the characters support. Only do this if you have characters which is not displaying correctly (e.g. Greek characters).', 'woovoucher' ).'</p>'
				),
				array(
					'id'		=> 'vou_enable_preview_in_browser',
					'name'		=> __( 'Preview Voucher Template without downloading', 'woovoucher' ),
					'desc'		=> __( 'Preview Voucher Template without downloading', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">' . __( 'Check this box if you want to allow admin to preview voucher template in a browser instead of downloading the pdf from the backend.', 'woovoucher' ) . '</p>'
				),
				array(
					'id'		=> 'vou_enable_pdf_password_protected',
					'name'		=> __( 'Enable PDF Password Protection', 'woovoucher' ),
					'desc'		=> __( 'Enable PDF Password Protection', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">' . __( 'Check this box if you want to enable PDF voucher with password protection.', 'woovoucher' ) . '</p>'
				),
				array(
					'id'		=> 'vou_pdf_password_pattern',
					'name'		=> __( 'Password Pattern for PDF Voucher', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Enter the password that you want to use while generating PDF voucher. Available tags are:','woovoucher' ).'<br /><code>{order_id}</code> '.__('- displays the order id.', 'woovoucher' ).'<br /><code>{order_date}</code> '.__('- displays the date of order completion, like: ', 'woovoucher' ) . date( 'Y-m-d' ) . '<br /><code>{first_name}</code> '.__('- displays buyer\'s first name.', 'woovoucher' ) . '<br /><code>{last_name}</code> '.__('- displays buyer\'s last name.', 'woovoucher' ) . '<br /><code>{buyer_email}</code> '.__('- displays buyer\'s email.', 'woovoucher' ).'</p>',
					'type'		=> 'vou_filename',
					'options'	=> ''
				),
				array(
					'id'		=> 'vou_enable_relative_path',
					'name'		=> __( 'Enable Relative Path', 'woovoucher' ),
					'desc'		=> __( 'Enable Relative Path', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">' . __( 'Check this box if you want to use a relative path instead of absolute path for loading images in voucher pdf.', 'woovoucher' ) . '</p>'
				),
				array( 
					'type' 		=> 'sectionend',
					'id' 		=> 'vou_pdf_settings'
				),
				
				array( 
					'name'	=>	__( 'Vendors Settings', 'woovoucher' ),
					'type'	=>	'title',
					'desc'	=>	'',
					'id'	=>	'vou_vendors_settings'
				),
				array(
					'id'		=> 'vou_enable_vendor_access_all_voucodes',
					'name'		=> __( 'Enable Vendors to Access/Redeem all Voucher Codes', 'woovoucher' ),
					'desc'		=> __( 'Enable Vendors to Access/Redeem all Voucher Codes', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">' . __( 'Check this box if you want to allow vendors to access all voucher codes. By default, they will only be able to access voucher codes assigned to them.', 'woovoucher' ) . '</p>'
				),
				array(
					'id'		=> 'vou_disable_vendor_access_voucher_template',
					'name'		=> __( 'Disable Vendors to Access Voucher Template Page', 'woovoucher' ),
					'desc'		=> __( 'Disable Vendors to Access Voucher Template Page', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">' . __( 'Check this box if you want to disallow vendors to access voucher template page.', 'woovoucher' ) . '</p>'
				),
				array(
					'id'		=> 'vou_allow_vendor_access_voucher_settings',
					'name'		=> __( 'Allow Vendors to Access Voucher Setting Area', 'woovoucher' ),
					'desc'		=> __( 'Allow Vendors to Access Voucher Setting Area', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">' . __( 'Check this box if you want to allow vendors to access voucher setting area at user profile.', 'woovoucher' ) . '</p>'
				),
				array( 
					'type' 		=> 'sectionend',
					'id' 		=> 'vou_vendors_settings'
				),

				array( 
					'name'	=>	__( 'Other Settings', 'woovoucher' ),
					'type'	=>	'title',
					'desc'	=>	'',
					'id'	=>	'vou_other_settings'
				),
				// Add "Disable Variation's Auto Downloadable" to Misc Setting
				array(
					'id'		=> 'vou_disable_variations_auto_downloadable',
					'name'		=> __( 'Disable Auto Check Downloadable', 'woovoucher' ),
					'desc'		=> __( 'Disable Auto Check Downloadable', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">' . __( 'Check this box if you don\'t want to tick all variation as downloadable when we "Enable Voucher Codes" at the product level.', 'woovoucher' ) . '</p>'
				),
				// Add "Auto Enable Voucher" to Voucher Setting
				array(
					'id'		=> 'vou_enable_voucher',
					'name'		=> __( 'Auto Enable Voucher', 'woovoucher' ),
					'desc'		=> __( 'Auto Enable Voucher', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">' . __( 'Check this box to automatically "Enable Voucher Codes" when product is set as <strong>Downloadable.</strong>', 'woovoucher' ) . '</p>'
				),
				array(
					'id'		=> 'vou_download_text',
					'name'		=> __( 'Voucher Download Text', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Enter the Voucher Download link text that you want to use while generating PDF.','woovoucher' ).'</p>',
					'type'		=> 'vou_filename',
					'options'	=> ''
				),
				array(
					'id'		=> 'vou_code_postfix',
					'name'		=> __( 'Voucher Code Postfix', 'woovoucher' ),
					'type'		=> 'number',
					'desc'		=> '<p class="description">'.__( 'Enter a numeric value which will automatically get succeeded at the end of voucher code when usability is set as unlimited.', 'woovoucher' ).'</p>'
				),
				array(
					'id'		=> 'vou_gift_notification_time',
					'name'		=> __( 'Select Time for Gift Notification Email', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'It will send gift notification email at selected time.', 'woovoucher' ).'</p>',
					'type'		=> 'select',
					'class'		=> 'wc-enhanced-select',
					'options'	=> $all_schedule_time_options
				),
				array(
					'id'		=> 'vou_pdf_delete_time',
					'name'		=> __( 'Clear Voucher\'s PDF  ', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( "Select time range at which the voucher's PDF file will get deleted. i.e. Hourly, Daily, twice daily", 'woovoucher' ).'</p>',
					'type'		=> 'select',
					'class'		=> 'wc-enhanced-select',
					'options'	=> $pdf_delete_cron_schedules,
					'default'	=> 'twicedaily'
				),
				array( 
					'type' 		=> 'sectionend',
					'id' 		=> 'vou_other_settings'
				),
			));

			// If class of WC Vendor Pro exists
			if(class_exists('WCV_Vendors')){

				array_push($settings,array( 
												'name'	=>	__( 'WC Vendor Settings', 'woovoucher' ),
												'type'	=>	'title',
												'desc'	=>	'',
												'id'	=>	'vou_wc_vendor_settings'
											),
										array(
												'id'		=> 'vou_enable_auto_integrate_wc_vendor',
												'name'		=> __( 'Auto Integrate vendor with PDF Voucher', 'woovoucher' ),
												'desc'		=> __( 'Auto Integrate vendor with PDF Voucher', 'woovoucher' ),
												'type'		=> 'checkbox',
												'desc_tip'	=> '<p class="description">' . __( 'Check this box if you want to integrate WC Vendors with PDF Vouchers.', 'woovoucher' ) . '</p>',
												'default'	=> 'yes'
											),
										array(
												'id'		=> 'vou_enable_vendor_acess_pdf_vou_frontend',
												'name'		=> __( 'Enable Access to PDF Vouchers tab', 'woovoucher' ),
												'desc'		=> __( 'Enable Access to PDF Vouchers tab', 'woovoucher' ),
												'type'		=> 'checkbox',
												'desc_tip'	=> '<p class="description">' . __( 'Check this box if you want to allow vendors to access PDF Vouchers on add/edit product page on frontend and backend.', 'woovoucher' ) . '</p>',
												'default'	=> 'yes'
											),
										array( 
												'type' 		=> 'sectionend',
												'id' 		=> 'vou_wc_vendor_settings'
											));
			}

			// If class of WC Market place exists
			if(class_exists('WCMp')){

				array_push($settings,array( 
												'name'	=>	__( 'WC Marketplace Settings', 'woovoucher' ),
												'type'	=>	'title',
												'desc'	=>	'',
												'id'	=>	'vou_wcmp_settings'
											),
										array(
												'id'		=> 'vou_enable_auto_integrate_wcmp_vendor',
												'name'		=> __( 'Auto Integrate vendor with PDF Voucher', 'woovoucher' ),
												'desc'		=> __( 'Auto Integrate vendor with PDF Voucher', 'woovoucher' ),
												'type'		=> 'checkbox',
												'desc_tip'	=> '<p class="description">' . __( 'Check this box if you want to integrate WCMp Vendors with PDF Vouchers.', 'woovoucher' ) . '</p>',
												'default'	=> 'yes'
											),
										array(
												'id'		=> 'vou_enable_wcmp_vendor_acess_pdf_vou_meta',
												'name'		=> __( 'Enable Access to PDF Vouchers tab', 'woovoucher' ),
												'desc'		=> __( 'Enable Access to PDF Vouchers tab', 'woovoucher' ),
												'type'		=> 'checkbox',
												'desc_tip'	=> '<p class="description">' . __( 'Check this box if you want to allow vendors to access PDF Vouchers on add/edit product page on frontend and backend.', 'woovoucher' ) . '</p>',
												'default'	=> 'yes'
											),
										array( 
												'type' 		=> 'sectionend',
												'id' 		=> 'vou_wcmp_settings'
											));
			}

			if( class_exists( 'WeDevs_Dokan' ) ) {// add doken seller as a vendor

				array_push($settings,array( 
												'name'	=>	__( 'Dokan Multi Vendor Settings', 'woovoucher' ),
												'type'	=>	'title',
												'desc'	=>	'',
												'id'	=>	'vou_dokan_settings'
											),
										array(
												'id'		=> 'vou_enable_auto_integrate_dokan_vendor',
												'name'		=> __( 'Auto Integrate Dokan Multi Vendor vendor\'s access', 'woovoucher' ),
												'desc'		=> __( 'Auto Integrate Dokan Multi Vendor vendor\'s access', 'woovoucher' ),
												'type'		=> 'checkbox',
												'desc_tip'	=> '<p class="description">' . __( 'Check this box if you want to integrate Dokan Multi Vendor vendors with PDF Vouchers vendors so that you have same access as pdf vouchers vendor\'s have.', 'woovoucher' ) . '</p>',
												'default'	=> 'yes'
											),
										array( 
												'type' 		=> 'sectionend',
												'id' 		=> 'vou_dokan_settings'
											)
						);
			}

		} else { // Else go for general settings
			$settings = apply_filters( 'woocommerce_products_general_settings', array(

				array(
					'title' => __( 'General Settings', 'woovoucher' ),
					'type' 	=> 'title',
					'id' 	=> 'vou_general_settings'
				),
				// Add Partial Redeem settings
				array(
					'id'		=> 'vou_enable_partial_redeem',
					'name'		=> __( 'Enable Partial Redemption', 'woovoucher' ),
					'desc'		=> sprintf(__( 'Enable Partial Redemption %1sChoose individual products%2s ( %3s 0 %4s ) Selected', 'woovoucher' ), '<a class="woo-vou-select-part-redeem-product">', '</a>', '<span class="woo-vou-part-redeem-product-count">', '</span>'),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">' . __( 'Check "Enable Partial Redemption" if you want to enable partial redemption for all products. However, If you want to enable partial redemption for particular products then click on "Choose individual products" and select the products for which you want to allow partial redemption.', 'woovoucher' ) . '</p>'
				),
				array(
					'id'		=> 'vou_partial_redeem_product_ids',
					'type'		=> 'text',
				),
				// Add coupon code settings
				array(
					'id'		=> 'vou_enable_coupon_code',
					'name'		=> __( 'Auto Enable Coupon Code Generation', 'woovoucher' ),
					'desc'		=> __( 'Auto Enable Coupon Code Generation', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">' . __( 'Check this box if you want to allow coupon code generation when a voucher code gets generated. This will allow you to use voucher codes on online store.', 'woovoucher' ) . '</p>'
				),
				array(
					'id'		=> 'vou_enable_voucher_preview',
					'name'		=> __( 'Enable Voucher Preview', 'woovoucher' ),
					'desc'		=> __( 'Enable Voucher Preview', 'woovoucher' ),
					'type'		=> 'checkbox',
					'desc_tip'	=> '<p class="description">' . __( 'Check this box if you want to allow users to preview the voucher on product detail page before placing the order.', 'woovoucher' ) . '</p>'
				),
				array(
					'id'		=> 'vou_preview_image',
					'name'		=> __( 'Preview Watermark Image', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Select the image that you would like to apply as watermark to the generated preview PDF on product page.', 'woovoucher' ).'</p>',
					'type'		=> 'vou_preview_upload',
					'size'		=> 'regular'
				),
				array(
					'type' 	=> 'sectionend',
					'id' 	=> 'vou_general_settings'
				),
				array(
						'title' => __( 'File Download Settings', 'woovoucher' ),
						'type' 	=> 'title',
						'id' 	=> 'vou_file_download_settings'
					),
				array(
					'id'		=> 'vou_pdf_name',
					'name'		=> __( 'Export PDF File Name', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Enter the PDF file name that you want to use while generating a PDF of purchased voucher codes. Available tag is:','woovoucher' ).'<br /><code>{current_date}</code> - '.__('displays the current date.', 'woovoucher' ).'</p>',
					'type'		=> 'vou_filename',
					'options'	=> '.pdf'
				),
				array(
					'id'		=> 'vou_pdf_title',
					'name'		=> __( 'Export PDF Title', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Enter the PDF file title that you want to use while generating a PDF of voucher codes.','woovoucher' ).'</p>',
					'type'		=> 'vou_filename',
					'options'	=> ''
				),
				array(
					'id'		=> 'vou_pdf_author',
					'name'		=> __( 'Export PDF Author Name', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Enter the PDF file author that you want to use while generating a PDF of voucher codes.','woovoucher' ).'</p>',
					'type'		=> 'vou_filename',
					'options'	=> ''
				),
				array(
					'id'		=> 'vou_pdf_creator',
					'name'		=> __( 'Export PDF Creator Name', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Enter the PDF file creator that you want to use while generating a PDF of voucher codes.','woovoucher' ).'</p>',
					'type'		=> 'vou_filename',
					'options'	=> ''
				),
				array(
					'id'		=> 'vou_csv_name',
					'name'		=> __( 'Export CSV File Name', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Enter the CSV file name that you want to use while generating a CSV of purchased voucher codes. Available tag is:','woovoucher' ).'<br /><code>{current_date}</code> - '.__('displays the current date.', 'woovoucher' ).'</p>',
					'type'		=> 'vou_filename',
					'options'	=> '.csv'
				),
				array(
					'id'		=> 'order_pdf_name',
					'name'		=> __( 'Download PDF File Name', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Enter the PDF file name that you want to use while users download a PDF of voucher codes on frontend. The available tags are','woovoucher' ).'<br /><code>{current_date}</code> - '.__('displays the current date.', 'woovoucher' ).'<br /><code>{product_title}</code> - '.__('displays the product title.', 'woovoucher' ).'</p>',
					'type'		=> 'vou_filename',
					'options'	=> '.pdf'
				), 
				array(
					'id'		=> 'attach_pdf_name',
					'name'		=> __( 'Attachment PDF File Name', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'Enter the PDF file name that you want to use while users download a PDF of voucher codes from email attachment. The available tags are','woovoucher' ).'<br /><code>{current_date}</code> - '.__('displays the current date.', 'woovoucher' ).'<br /><code>{product_title}</code> - '.__('displays the product title.', 'woovoucher' ).'</p>',
					'type'		=> 'vou_filename',
					'options'	=> '{unique_string}.pdf'
				),
				array(
					'type' 	=> 'sectionend',
					'id' 	=> 'vou_file_download_settings'
				),
				array(
						'title' => __( 'Display Settings', 'woovoucher' ),
						'type' 	=> 'title',
						'id' 	=> 'vou_display_settings'
					),
				// Add "Enable Voucher Code For Regular Price" to Misc Setting
				array(
					'id'		=> 'vou_recipient_form_position',
					'name'		=> __( 'Recipient Form Position', 'woovoucher' ),
					'desc'		=> '<p class="description">'.__( 'It controls the position of the Recipient Form.', 'woovoucher' ).'</p>',
					'type'		=> 'select',
					'class'		=> 'wc-enhanced-select',
					'options'	=> $recipient_form_options
				),
				array(
					'name'   => __('Custom CSS', 'woovoucher'),
					'class'  => '',
					'css'   => 'width:100%;min-height:100px',
					'desc'   => __('Here you can enter your custom CSS for the  PDF vouchers. The CSS will be automatically added to the header, when you save it.', 'woovoucher'),
					'id'   => 'vou_custom_css',
					'type'   => 'vou_textarea',
					'default' => ''
				),
				array(
					'type' 	=> 'sectionend',
					'id' 	=> 'vou_display_settings'
				),
			));
		}

		return apply_filters( 'woocommerce_get_settings_' . $this->id, $settings, $current_section );
	}
}

endif;

return new Woo_Wou_Settings();
