<?php
/**
 * Colors & Background - Dynamic CSS
 *
 * @package Astra Addon
 */

add_filter( 'astra_dynamic_css', 'astra_ext_sticky_header_colors_dynamic_css' );

/**
 * Dynamic CSS
 *
 * @param  string $dynamic_css          Astra Dynamic CSS.
 * @param  string $dynamic_css_filtered Astra Dynamic CSS Filters.
 * @return string
 */
function astra_ext_sticky_header_colors_dynamic_css( $dynamic_css, $dynamic_css_filtered = '' ) {

	$main_stick                = astra_get_option( 'header-main-stick' );
	$header_color_site_title   = astra_get_option( 'header-color-site-title', '#222' );
	$text_color                = astra_get_option( 'text-color' );
	$link_color                = astra_get_option( 'link-color' );
	$header_color_site_tagline = astra_get_option( 'header-color-site-tagline', $text_color );
	$header_color_h_site_title = astra_get_option( 'header-color-h-site-title' );

	$desktop_primary_menu_color = astar( astra_get_option( 'primary-menu-color-responsive' ), 'desktop', $text_color );
	$tablet_primary_menu_color  = astar( astra_get_option( 'primary-menu-color-responsive' ), 'tablet' );
	$mobile_primary_menu_color  = astar( astra_get_option( 'primary-menu-color-responsive' ), 'mobile' );

	$desktop_primary_menu_h_color = astar( astra_get_option( 'primary-menu-h-color-responsive' ), 'desktop', $link_color );
	$tablet_primary_menu_h_color  = astar( astra_get_option( 'primary-menu-h-color-responsive' ), 'tablet' );
	$mobile_primary_menu_h_color  = astar( astra_get_option( 'primary-menu-h-color-responsive' ), 'mobile' );

	$desktop_primary_menu_a_color = astar( astra_get_option( 'primary-menu-a-color-responsive' ), 'desktop', $link_color );
	$tablet_primary_menu_a_color  = astar( astra_get_option( 'primary-menu-a-color-responsive' ), 'tablet' );
	$mobile_primary_menu_a_color  = astar( astra_get_option( 'primary-menu-a-color-responsive' ), 'mobile' );

	$primary_menu_h_bg_color = astra_get_option( 'primary-menu-h-bg-color-responsive' );
	$primary_menu_a_bg_color = astra_get_option( 'primary-menu-a-bg-color-responsive' );

	$header_break_point = astra_header_break_point(); // Header Break Point.

	$parse_css = '';
	/**
	 * If Primary Menu is Sticky.
	 */
	if ( $main_stick ) {
		// Common Colors without responsive colors option.
		$sticky_css_output = array(
			'#ast-fixed-header .main-header-bar .site-title a, #ast-fixed-header .main-header-bar .site-title a:focus, #ast-fixed-header .main-header-bar .site-title a:hover, #ast-fixed-header .main-header-bar .site-title a:visited, .main-header-bar.ast-sticky-active .site-title a, .main-header-bar.ast-sticky-active .site-title a:focus, .main-header-bar.ast-sticky-active .site-title a:hover, .main-header-bar.ast-sticky-active .site-title a:visited' => array(
				'color' => esc_attr( $header_color_site_title ),
			),
			'#ast-fixed-header .main-header-bar .site-title a:hover, .main-header-bar.ast-sticky-active .site-title a:hover' => array(
				'color' => esc_attr( $header_color_h_site_title ),
			),
			'#ast-fixed-header .main-header-bar .site-description, .main-header-bar.ast-sticky-active .site-description' => array(
				'color' => esc_attr( $header_color_site_tagline ),
			),
		);

		/**
		 * Responsive Colors
		 */
		// Desktop.
		$desktop_colors = array(
			'#ast-fixed-header .main-header-menu, #ast-fixed-header .main-header-menu > li > a, #ast-fixed-header  .ast-masthead-custom-menu-items .slide-search .search-submit, #ast-fixed-header .ast-masthead-custom-menu-items, #ast-fixed-header .ast-masthead-custom-menu-items a, .main-header-bar.ast-sticky-active, .main-header-bar.ast-sticky-active .main-header-menu > li > a, .main-header-bar.ast-sticky-active  .ast-masthead-custom-menu-items .slide-search .search-submit, .main-header-bar.ast-sticky-active .ast-masthead-custom-menu-items, .main-header-bar.ast-sticky-active .ast-masthead-custom-menu-items a' => array(
				'color' => esc_attr( $desktop_primary_menu_color ),
			),

			/**
			 * Primary Menu
			 */
			'#ast-fixed-header .main-header-menu li.current-menu-item > a, #ast-fixed-header .main-header-menu li.current-menu-ancestor > a, #ast-fixed-header .main-header-menu li.current_page_item > a, .main-header-bar.ast-sticky-active .main-header-menu li.current-menu-item > a, .main-header-bar.ast-sticky-active .main-header-menu li.current-menu-ancestor > a, .main-header-bar.ast-sticky-active .main-header-menu li.current_page_item > a' => array(
				'color'            => esc_attr( $desktop_primary_menu_a_color ),
				'background-color' => esc_attr( $primary_menu_a_bg_color['desktop'] ),
			),
			'#ast-fixed-header .main-header-menu a:hover, #ast-fixed-header .main-header-menu li:hover > a, #ast-fixed-header .main-header-menu li.focus > a, .main-header-bar.ast-sticky-active .main-header-menu li:hover > a, .main-header-bar.ast-sticky-active .main-header-menu li.focus > a' => array(
				'background-color' => esc_attr( $primary_menu_h_bg_color['desktop'] ),
				'color'            => esc_attr( $desktop_primary_menu_h_color ),
			),
			'#ast-fixed-header .main-header-menu .ast-masthead-custom-menu-items a:hover, #ast-fixed-header .main-header-menu li:hover > .ast-menu-toggle, #ast-fixed-header .main-header-menu li.focus > .ast-menu-toggle,.main-header-bar.ast-sticky-active .main-header-menu .ast-masthead-custom-menu-items a:hover,.main-header-bar.ast-sticky-active .main-header-menu li:hover > .ast-menu-toggle,.main-header-bar.ast-sticky-active .main-header-menu li.focus > .ast-menu-toggle' => array(
				'color' => esc_attr( $desktop_primary_menu_h_color ),
			),

			'#ast-fixed-header .ast-masthead-custom-menu-items .ast-inline-search form, .main-header-bar.ast-sticky-active .ast-masthead-custom-menu-items .ast-inline-search form' => array(
				'border-color' => esc_attr( $desktop_primary_menu_color ),
			),
		);

		// Tablet.
		$tablet_colors = array(
			'#ast-fixed-header .main-header-menu, #ast-fixed-header .main-header-menu > li > a, #ast-fixed-header  .ast-masthead-custom-menu-items .slide-search .search-submit, #ast-fixed-header .ast-masthead-custom-menu-items, #ast-fixed-header .ast-masthead-custom-menu-items a, .main-header-bar.ast-sticky-active, .main-header-bar.ast-sticky-active .main-header-menu > li > a, .main-header-bar.ast-sticky-active  .ast-masthead-custom-menu-items .slide-search .search-submit, .main-header-bar.ast-sticky-active .ast-masthead-custom-menu-items, .main-header-bar.ast-sticky-active .ast-masthead-custom-menu-items a' => array(
				'color' => esc_attr( $tablet_primary_menu_color ),
			),

			/**
			 * Primary Menu
			 */
			'#ast-fixed-header .main-header-menu li.current-menu-item > a, #ast-fixed-header .main-header-menu li.current-menu-ancestor > a, #ast-fixed-header .main-header-menu li.current_page_item > a, .main-header-bar.ast-sticky-active .main-header-menu li.current-menu-item > a, .main-header-bar.ast-sticky-active .main-header-menu li.current-menu-ancestor > a, .main-header-bar.ast-sticky-active .main-header-menu li.current_page_item > a' => array(
				'color'            => esc_attr( $tablet_primary_menu_a_color ),
				'background-color' => esc_attr( $primary_menu_a_bg_color['tablet'] ),
			),
			'#ast-fixed-header .main-header-menu a:hover, #ast-fixed-header .main-header-menu li:hover > a, #ast-fixed-header .main-header-menu li.focus > a, .main-header-bar.ast-sticky-active .main-header-menu li:hover > a, .main-header-bar.ast-sticky-active .main-header-menu li.focus > a' => array(
				'background-color' => esc_attr( $primary_menu_h_bg_color['tablet'] ),
				'color'            => esc_attr( $tablet_primary_menu_h_color ),
			),
			'#ast-fixed-header .main-header-menu .ast-masthead-custom-menu-items a:hover, #ast-fixed-header .main-header-menu li:hover > .ast-menu-toggle, #ast-fixed-header .main-header-menu li.focus > .ast-menu-toggle,.main-header-bar.ast-sticky-active .main-header-menu .ast-masthead-custom-menu-items a:hover,.main-header-bar.ast-sticky-active .main-header-menu li:hover > .ast-menu-toggle,.main-header-bar.ast-sticky-active .main-header-menu li.focus > .ast-menu-toggle' => array(
				'color' => esc_attr( $tablet_primary_menu_h_color ),
			),

			'#ast-fixed-header .ast-masthead-custom-menu-items .ast-inline-search form, .main-header-bar.ast-sticky-active .ast-masthead-custom-menu-items .ast-inline-search form' => array(
				'border-color' => esc_attr( $tablet_primary_menu_color ),
			),
		);

		// Mobile.
		$mobile_colors = array(
			'#ast-fixed-header .main-header-menu, #ast-fixed-header .main-header-menu > li > a, #ast-fixed-header  .ast-masthead-custom-menu-items .slide-search .search-submit, #ast-fixed-header .ast-masthead-custom-menu-items, #ast-fixed-header .ast-masthead-custom-menu-items a, .main-header-bar.ast-sticky-active, .main-header-bar.ast-sticky-active .main-header-menu > li > a, .main-header-bar.ast-sticky-active  .ast-masthead-custom-menu-items .slide-search .search-submit, .main-header-bar.ast-sticky-active .ast-masthead-custom-menu-items, .main-header-bar.ast-sticky-active .ast-masthead-custom-menu-items a' => array(
				'color' => esc_attr( $mobile_primary_menu_color ),
			),

			/**
			 * Primary Menu
			 */
			'#ast-fixed-header .main-header-menu li.current-menu-item > a, #ast-fixed-header .main-header-menu li.current-menu-ancestor > a, #ast-fixed-header .main-header-menu li.current_page_item > a, .main-header-bar.ast-sticky-active .main-header-menu li.current-menu-item > a, .main-header-bar.ast-sticky-active .main-header-menu li.current-menu-ancestor > a, .main-header-bar.ast-sticky-active .main-header-menu li.current_page_item > a' => array(
				'color'            => esc_attr( $mobile_primary_menu_a_color ),
				'background-color' => esc_attr( $primary_menu_a_bg_color['mobile'] ),
			),
			'#ast-fixed-header .main-header-menu a:hover, #ast-fixed-header .main-header-menu li:hover > a, #ast-fixed-header .main-header-menu li.focus > a, .main-header-bar.ast-sticky-active .main-header-menu li:hover > a, .main-header-bar.ast-sticky-active .main-header-menu li.focus > a' => array(
				'background-color' => esc_attr( $primary_menu_h_bg_color['mobile'] ),
				'color'            => esc_attr( $mobile_primary_menu_h_color ),
			),
			'#ast-fixed-header .main-header-menu .ast-masthead-custom-menu-items a:hover, #ast-fixed-header .main-header-menu li:hover > .ast-menu-toggle, #ast-fixed-header .main-header-menu li.focus > .ast-menu-toggle,.main-header-bar.ast-sticky-active .main-header-menu .ast-masthead-custom-menu-items a:hover,.main-header-bar.ast-sticky-active .main-header-menu li:hover > .ast-menu-toggle,.main-header-bar.ast-sticky-active .main-header-menu li.focus > .ast-menu-toggle' => array(
				'color' => esc_attr( $mobile_primary_menu_h_color ),
			),

			'#ast-fixed-header .ast-masthead-custom-menu-items .ast-inline-search form, .main-header-bar.ast-sticky-active .ast-masthead-custom-menu-items .ast-inline-search form' => array(
				'border-color' => esc_attr( $mobile_primary_menu_color ),
			),
		);

		/* Parse CSS from array() */
		$parse_css .= astra_parse_css( $sticky_css_output );
		$parse_css .= astra_parse_css( $desktop_colors );
		$parse_css .= astra_parse_css( $tablet_colors, '', '768' );
		$parse_css .= astra_parse_css( $mobile_colors, '', '544' );
	}

	return $dynamic_css . $parse_css;
}
