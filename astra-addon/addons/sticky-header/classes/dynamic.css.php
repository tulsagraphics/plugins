<?php
/**
 * Transparent Header - Dynamic CSS
 *
 * @package Astra Addon
 */

add_filter( 'astra_dynamic_css', 'astra_ext_sticky_header_dynamic_css' );

/**
 * Dynamic CSS
 *
 * @param  string $dynamic_css          Astra Dynamic CSS.
 * @param  string $dynamic_css_filtered Astra Dynamic CSS Filters.
 * @return string
 */
function astra_ext_sticky_header_dynamic_css( $dynamic_css, $dynamic_css_filtered = '' ) {

	/**
	 * Set colors
	 *
	 * If colors extension is_active then get color from it.
	 * Else set theme default colors.
	 */
	$stick_header            = astra_get_option_meta( 'stick-header-meta' );
	$stick_header_main_meta  = astra_get_option_meta( 'header-main-stick-meta' );
	$stick_header_above_meta = astra_get_option_meta( 'header-above-stick-meta' );
	$stick_header_below_meta = astra_get_option_meta( 'header-below-stick-meta' );

	$stick_header_main  = astra_get_option( 'header-main-stick' );
	$stick_header_above = astra_get_option( 'header-above-stick' );
	$stick_header_below = astra_get_option( 'header-below-stick' );

	$sticky_header_logo_width = astra_get_option( 'sticky-header-logo-width' );
	// Old Log Width Option that we are no loginer using it our theme.
	$header_logo_width            = astra_get_option( 'ast-header-logo-width' );
	$header_responsive_logo_width = astra_get_option( 'ast-header-responsive-logo-width' );

	$sticky_header_bg_opc = astra_get_option( 'sticky-header-bg-opc' );

	$site_layout = astra_get_option( 'site-layout' );

	$header_color_site_title = '#222';
	$text_color              = astra_get_option( 'text-color' );
	$link_color              = astra_get_option( 'link-color' );

	$sticky_header_inherit = astra_get_option( 'different-sticky-logo' );
	$sticky_header_logo    = astra_get_option( 'sticky-header-logo' );

	// Header Break Point.
	$header_break_point = astra_header_break_point();

	$primary_nav = astra_get_option( 'disable-primary-nav' );
	// Background Colors.
	$main_header_bg_obj    = astra_get_option( 'header-bg-obj-responsive' );
	$primary_menu_bg_image = astra_get_option( 'primary-menu-bg-obj-responsive' );

	$below_header_bg_obj = astra_get_option( 'below-header-bg-obj-responsive' );
	$above_header_bg_obj = astra_get_option( 'above-header-bg-obj-responsive' );

	if ( ! $stick_header_main && ! $stick_header_above && ! $stick_header_below && ( 'disabled' !== $stick_header && empty( $stick_header ) && ( empty( $stick_header_above_meta ) || empty( $stick_header_below_meta ) || empty( $stick_header_main_meta ) ) ) ) {
		return $dynamic_css;
	}

	$parse_css = '';

	if ( '0' === $sticky_header_inherit && '' != $sticky_header_logo ) {
		$css_output = array(
			'.ast-sticky-active .site-logo-img .custom-logo' => array(
				'display' => 'none',
			),
		);
		$parse_css .= astra_parse_css( $css_output );
	}

	// Desktop Sticky Header Logo width.
	$css_output = array(
		'#masthead .site-logo-img .sticky-custom-logo .astra-logo-svg, .site-logo-img .sticky-custom-logo .astra-logo-svg, .ast-sticky-main-shrink .ast-sticky-shrunk .site-logo-img .astra-logo-svg' => array(
			'width' => astra_get_css_value( $sticky_header_logo_width['desktop'], 'px' ),
		),
		'.site-logo-img .sticky-custom-logo img' => array(
			'max-width' => astra_get_css_value( $sticky_header_logo_width['desktop'], 'px' ),
		),
	);
	$parse_css .= astra_parse_css( $css_output );

	// Tablet Sticky Header Logo width.
	$tablet_css_output = array(
		'#masthead .site-logo-img .sticky-custom-logo .astra-logo-svg, .site-logo-img .sticky-custom-logo .astra-logo-svg, .ast-sticky-main-shrink .ast-sticky-shrunk .site-logo-img .astra-logo-svg' => array(
			'width' => astra_get_css_value( $sticky_header_logo_width['tablet'], 'px' ),
		),
		'.site-logo-img .sticky-custom-logo img' => array(
			'max-width' => astra_get_css_value( $sticky_header_logo_width['tablet'], 'px' ),
		),
	);
	$parse_css        .= astra_parse_css( $tablet_css_output, '', '768' );

	// Mobile Sticky Header Logo width.
	$mobile_css_output = array(
		'#masthead .site-logo-img .sticky-custom-logo .astra-logo-svg, .site-logo-img .sticky-custom-logo .astra-logo-svg, .ast-sticky-main-shrink .ast-sticky-shrunk .site-logo-img .astra-logo-svg' => array(
			'width' => astra_get_css_value( $sticky_header_logo_width['mobile'], 'px' ),
		),
		'.site-logo-img .sticky-custom-logo img' => array(
			'max-width' => astra_get_css_value( $sticky_header_logo_width['mobile'], 'px' ),
		),
	);
	$parse_css        .= astra_parse_css( $mobile_css_output, '', '543' );

	// Theme Main Logo width option for responsive devices.
	if ( is_array( $header_responsive_logo_width ) ) {
		/* Responsive main logo width */
		$responsive_logo_output = array(
			'#masthead .site-logo-img .astra-logo-svg, .ast-header-break-point #ast-fixed-header .site-logo-img .custom-logo-link img ' => array(
				'max-width' => astra_get_css_value( $header_responsive_logo_width['desktop'], 'px' ),
			),
		);
		$parse_css             .= astra_parse_css( $responsive_logo_output );

		$responsive_logo_output_tablet = array(
			'#masthead .site-logo-img .astra-logo-svg, .ast-header-break-point #ast-fixed-header .site-logo-img .custom-logo-link img ' => array(
				'max-width' => astra_get_css_value( $header_responsive_logo_width['tablet'], 'px' ),
			),
		);
		$parse_css                    .= astra_parse_css( $responsive_logo_output_tablet, '', '768' );

		$responsive_logo_output_mobile = array(
			'#masthead .site-logo-img .astra-logo-svg, .ast-header-break-point #ast-fixed-header .site-logo-img .custom-logo-link img ' => array(
				'max-width' => astra_get_css_value( $header_responsive_logo_width['mobile'], 'px' ),
			),
		);
		$parse_css                    .= astra_parse_css( $responsive_logo_output_mobile, '', '543' );
	} else {
		/* Old main logo width */
		$logo_output = array(
			'#masthead .site-logo-img .astra-logo-svg' => array(
				'width' => astra_get_css_value( $header_logo_width, 'px' ),
			),
		);
			/* Parse CSS from array() */
		$parse_css .= astra_parse_css( $logo_output );
	}

	// Compatible with header full width.
	$header_break_point = astra_header_break_point();
	$astra_header_width = astra_get_option( 'header-main-layout-width' );

	/* Width for Header */
	if ( 'content' != $astra_header_width ) {
		$general_global_responsive = array(
			'#ast-fixed-header .ast-container' => array(
				'max-width'     => '100%',
				'padding-left'  => '35px',
				'padding-right' => '35px',
			),
		);

		/* Parse CSS from array()*/
		$parse_css .= astra_parse_css( $general_global_responsive, $header_break_point );
	}

	if ( ! Astra_Ext_Extension::is_active( 'colors-and-background' ) ) {
		$css_output = array(
			'#ast-fixed-header .main-header-bar .site-title a, #ast-fixed-header .main-header-bar .site-title a:focus, #ast-fixed-header .main-header-bar .site-title a:hover, #ast-fixed-header .main-header-bar .site-title a:visited, .main-header-bar.ast-sticky-active .site-title a, .main-header-bar.ast-sticky-active .site-title a:focus, .main-header-bar.ast-sticky-active .site-title a:hover, .main-header-bar.ast-sticky-active .site-title a:visited' => array(
				'color' => esc_attr( $header_color_site_title ),
			),
			'#ast-fixed-header .main-header-bar .site-description, .main-header-bar.ast-sticky-active .site-description' => array(
				'color' => esc_attr( $text_color ),
			),

			'#ast-fixed-header .main-header-menu > li.current-menu-item > a, #ast-fixed-header .main-header-menu >li.current-menu-ancestor > a, #ast-fixed-header .main-header-menu > li.current_page_item > a, .main-header-bar.ast-sticky-active .main-header-menu > li.current-menu-item > a, .main-header-bar.ast-sticky-active .main-header-menu >li.current-menu-ancestor > a, .main-header-bar.ast-sticky-active .main-header-menu > li.current_page_item > a' => array(
				'color' => esc_attr( $link_color ),
			),

			'#ast-fixed-header .main-header-menu, #ast-fixed-header .main-header-menu > li > a, #ast-fixed-header  .ast-masthead-custom-menu-items .slide-search .search-submit, #ast-fixed-header .ast-masthead-custom-menu-items, #ast-fixed-header .ast-masthead-custom-menu-items a, .main-header-bar.ast-sticky-active, .main-header-bar.ast-sticky-active .main-header-menu > li > a, .main-header-bar.ast-sticky-active  .ast-masthead-custom-menu-items .slide-search .search-submit, .main-header-bar.ast-sticky-active .ast-masthead-custom-menu-items, .main-header-bar.ast-sticky-active .ast-masthead-custom-menu-items a' => array(
				'color' => esc_attr( $text_color ),
			),
			'#ast-fixed-header .main-header-menu a:hover, #ast-fixed-header .main-header-menu li:hover > a, #ast-fixed-header .main-header-menu li.focus > a, .main-header-bar.ast-sticky-active .main-header-menu li:hover > a, .main-header-bar.ast-sticky-active .main-header-menu li.focus > a' => array(
				'color' => esc_attr( $link_color ),
			),
			'#ast-fixed-header .main-header-menu .ast-masthead-custom-menu-items a:hover, #ast-fixed-header .main-header-menu li:hover > .ast-menu-toggle, #ast-fixed-header .main-header-menu li.focus > .ast-menu-toggle,.main-header-bar.ast-sticky-active .main-header-menu li:hover > .ast-menu-toggle,.main-header-bar.ast-sticky-active .main-header-menu li.focus > .ast-menu-toggle' => array(
				'color' => esc_attr( $link_color ),
			),
		);

		/* Parse CSS from array() */
		$parse_css .= astra_parse_css( $css_output );
	}

	/**
	 * Sticky Header Colors with reponsive option.
	 * [1]. Primary Menu Colors
	 * [2]. Above Header Colors
	 * [3]. Below Header Colors
	 */

	// Responsive Colors options.
	// Desktop Colors options.
	$desktop_colors = array(
		'primary-menu' => ( Astra_Ext_Extension::is_active( 'colors-and-background' ) ) ? $primary_menu_bg_image['desktop']['background-color'] : '',
	);

	// Tablet Colors options.
	$tablet_colors = array(
		'primary-menu' => ( Astra_Ext_Extension::is_active( 'colors-and-background' ) ) ? $primary_menu_bg_image['tablet']['background-color'] : '',
	);

	// Mobile Colors options.
	$mobile_colors = array(
		'primary-menu' => ( Astra_Ext_Extension::is_active( 'colors-and-background' ) ) ? $primary_menu_bg_image['mobile']['background-color'] : '',
	);

	if ( Astra_Ext_Extension::is_active( 'colors-and-background' ) ) {
		$desktop_colors['header-main'] = isset( $main_header_bg_obj['desktop']['background-color'] ) ? $main_header_bg_obj['desktop']['background-color'] : '#ffffff';
		$tablet_colors['header-main']  = isset( $main_header_bg_obj['tablet']['background-color'] ) ? $main_header_bg_obj['tablet']['background-color'] : '';
		$mobile_colors['header-main']  = isset( $main_header_bg_obj['mobile']['background-color'] ) ? $main_header_bg_obj['mobile']['background-color'] : '';
	} else {
		$desktop_colors['header-main'] = '#ffffff';
	}

	if ( ( Astra_Ext_Extension::is_active( 'header-sections' ) ) ) {
		$desktop_colors['header-supp'] = isset( $below_header_bg_obj['desktop']['background-color'] ) ? $below_header_bg_obj['desktop']['background-color'] : '#414042';
		$tablet_colors['header-supp']  = isset( $below_header_bg_obj['tablet']['background-color'] ) ? $below_header_bg_obj['tablet']['background-color'] : '';
		$mobile_colors['header-supp']  = isset( $below_header_bg_obj['mobile']['background-color'] ) ? $below_header_bg_obj['mobile']['background-color'] : '';
	} else {
		$desktop_colors['header-supp'] = '#414042';
	}

	if ( ( Astra_Ext_Extension::is_active( 'header-sections' ) ) ) {
		$desktop_colors['header-top'] = isset( $above_header_bg_obj['desktop']['background-color'] ) ? $above_header_bg_obj['desktop']['background-color'] : '';
		$tablet_colors['header-top']  = isset( $above_header_bg_obj['tablet']['background-color'] ) ? $above_header_bg_obj['tablet']['background-color'] : '';
		$mobile_colors['header-top']  = isset( $above_header_bg_obj['mobile']['background-color'] ) ? $above_header_bg_obj['mobile']['background-color'] : '';
	} else {
		$desktop_colors['header-top'] = '#ffffff';
	}

	$desktop_colors['header-main']  = ( isset( $desktop_colors['header-main'] ) && '' != $desktop_colors['header-main'] ) ? $desktop_colors['header-main'] : '#ffffff';
	$desktop_colors['primary-menu'] = ( isset( $desktop_colors['primary-menu'] ) && '' != $desktop_colors['primary-menu'] ) ? $desktop_colors['primary-menu'] : '';
	$desktop_colors['header-top']   = ( isset( $desktop_colors['header-top'] ) && '' != $desktop_colors['header-top'] ) ? $desktop_colors['header-top'] : '#ffffff';
	$desktop_colors['header-supp']  = ( isset( $desktop_colors['header-supp'] ) && '' != $desktop_colors['header-supp'] ) ? $desktop_colors['header-supp'] : '#414042';

	$tablet_colors['header-main']  = ( isset( $tablet_colors['header-main'] ) && '' != $tablet_colors['header-main'] ) ? $tablet_colors['header-main'] : '';
	$tablet_colors['primary-menu'] = ( isset( $tablet_colors['primary-menu'] ) && '' != $tablet_colors['primary-menu'] ) ? $tablet_colors['primary-menu'] : '';
	$tablet_colors['header-top']   = ( isset( $tablet_colors['header-top'] ) && '' != $tablet_colors['header-top'] ) ? $tablet_colors['header-top'] : '';
	$tablet_colors['header-supp']  = ( isset( $tablet_colors['header-supp'] ) && '' != $tablet_colors['header-supp'] ) ? $tablet_colors['header-supp'] : '';

	$mobile_colors['header-main']  = ( isset( $mobile_colors['header-main'] ) && '' != $mobile_colors['header-main'] ) ? $mobile_colors['header-main'] : '';
	$mobile_colors['primary-menu'] = ( isset( $mobile_colors['primary-menu'] ) && '' != $mobile_colors['primary-menu'] ) ? $mobile_colors['primary-menu'] : '';
	$mobile_colors['header-top']   = ( isset( $mobile_colors['header-top'] ) && '' != $mobile_colors['header-top'] ) ? $mobile_colors['header-top'] : '';
	$mobile_colors['header-supp']  = ( isset( $mobile_colors['header-supp'] ) && '' != $mobile_colors['header-supp'] ) ? $mobile_colors['header-supp'] : '';

	/**
	 * Set RGBA color if transparent header is active
	 */
	$desktop_colors['header-main'] = ( ! empty( $desktop_colors['header-main'] ) ) ? astra_hex2rgba( astra_rgba2hex( $desktop_colors['header-main'] ), $sticky_header_bg_opc ) : '';

	$tablet_colors['header-main'] = ( ! empty( $tablet_colors['header-main'] ) ) ? astra_hex2rgba( astra_rgba2hex( $tablet_colors['header-main'] ), $sticky_header_bg_opc ) : '';

	$mobile_colors['header-main'] = ( ! empty( $mobile_colors['header-main'] ) ) ? astra_hex2rgba( astra_rgba2hex( $mobile_colors['header-main'] ), $sticky_header_bg_opc ) : '';

	if ( '' !== $desktop_colors['primary-menu'] ) {
		$desktop_colors['primary-menu'] = ( ! empty( $desktop_colors['primary-menu'] ) ) ? astra_hex2rgba( astra_rgba2hex( $desktop_colors['primary-menu'] ), $sticky_header_bg_opc ) : '';
	}

	if ( '' !== $tablet_colors['primary-menu'] ) {
		$tablet_colors['primary-menu'] = ( ! empty( $tablet_colors['primary-menu'] ) ) ? astra_hex2rgba( astra_rgba2hex( $tablet_colors['primary-menu'] ), $sticky_header_bg_opc ) : '';
	}

	if ( '' !== $mobile_colors['primary-menu'] ) {
		$mobile_colors['primary-menu'] = ( ! empty( $mobile_colors['primary-menu'] ) ) ? astra_hex2rgba( astra_rgba2hex( $mobile_colors['primary-menu'] ), $sticky_header_bg_opc ) : '';
	}

	$desktop_colors['header-top'] = ( ! empty( $desktop_colors['header-top'] ) ) ? astra_hex2rgba( astra_rgba2hex( $desktop_colors['header-top'] ), $sticky_header_bg_opc ) : '';
	$tablet_colors['header-top']  = ( ! empty( $tablet_colors['header-top'] ) ) ? astra_hex2rgba( astra_rgba2hex( $tablet_colors['header-top'] ), $sticky_header_bg_opc ) : '';
	$mobile_colors['header-top']  = ( ! empty( $mobile_colors['header-top'] ) ) ? astra_hex2rgba( astra_rgba2hex( $mobile_colors['header-top'] ), $sticky_header_bg_opc ) : '';

	$desktop_colors['header-supp'] = ( ! empty( $desktop_colors['header-supp'] ) ) ? astra_hex2rgba( astra_rgba2hex( $desktop_colors['header-supp'] ), $sticky_header_bg_opc ) : '';
	$tablet_colors['header-supp']  = ( ! empty( $tablet_colors['header-supp'] ) ) ? astra_hex2rgba( astra_rgba2hex( $tablet_colors['header-supp'] ), $sticky_header_bg_opc ) : '';
	$mobile_colors['header-supp']  = ( ! empty( $mobile_colors['header-supp'] ) ) ? astra_hex2rgba( astra_rgba2hex( $mobile_colors['header-supp'] ), $sticky_header_bg_opc ) : '';

	/**
	 * Generate Dynamic CSS
	 */

	$css = '';

		$desktop_css_output = array(
			'.ast-transparent-header #ast-fixed-header .main-header-bar, #ast-fixed-header .main-header-bar, .ast-transparent-header .main-header-bar.ast-sticky-active, .main-header-bar.ast-sticky-active, #ast-fixed-header .ast-masthead-custom-menu-items .ast-inline-search .search-field, #ast-fixed-header .ast-masthead-custom-menu-items .ast-inline-search .search-field:focus'                      => array(
				'background-color' => esc_attr( $desktop_colors['header-main'] ),
			),
			'#ast-fixed-header .main-header-bar .main-header-menu, .main-header-bar.ast-sticky-active .main-header-menu, .ast-header-break-point #ast-fixed-header .main-header-menu'                      => array(
				'background-color' => esc_attr( $desktop_colors['primary-menu'] ),
			),
		);

		$tablet_css_output = array(
			'.ast-transparent-header #ast-fixed-header .main-header-bar, #ast-fixed-header .main-header-bar, .ast-transparent-header .main-header-bar.ast-sticky-active, .main-header-bar.ast-sticky-active, #ast-fixed-header .ast-masthead-custom-menu-items .ast-inline-search .search-field, #ast-fixed-header .ast-masthead-custom-menu-items .ast-inline-search .search-field:focus'                      => array(
				'background-color' => esc_attr( $tablet_colors['header-main'] ),
			),
			'#ast-fixed-header .main-header-bar .main-header-menu, .main-header-bar.ast-sticky-active .main-header-menu, .ast-header-break-point #ast-fixed-header .main-header-menu'                      => array(
				'background-color' => esc_attr( $tablet_colors['primary-menu'] ),
			),
		);

		$mobile_css_output = array(
			'.ast-transparent-header #ast-fixed-header .main-header-bar, #ast-fixed-header .main-header-bar, .ast-transparent-header .main-header-bar.ast-sticky-active, .main-header-bar.ast-sticky-active, #ast-fixed-header .ast-masthead-custom-menu-items .ast-inline-search .search-field, #ast-fixed-header .ast-masthead-custom-menu-items .ast-inline-search .search-field:focus'                      => array(
				'background-color' => esc_attr( $mobile_colors['header-main'] ),
			),
			'#ast-fixed-header .main-header-bar .main-header-menu, .main-header-bar.ast-sticky-active .main-header-menu, .ast-header-break-point #ast-fixed-header .main-header-menu'                      => array(
				'background-color' => esc_attr( $mobile_colors['primary-menu'] ),
			),
		);

		/* Parse CSS from array() */
		$css .= astra_parse_css( $desktop_css_output );
		$css .= astra_parse_css( $tablet_css_output, '', '768' );
		$css .= astra_parse_css( $mobile_css_output, '', '544' );

	if ( Astra_Ext_Extension::is_active( 'header-sections' ) ) {
		$main_stick  = astra_get_option( 'header-main-stick' );
		$below_stick = astra_get_option( 'header-below-stick' );
		if ( 1 == $main_stick && 1 == $below_stick ) {

			$desktop_header_sections_css_output = array(
				'.ast-stick-primary-below-wrapper.ast-sticky-active .main-header-bar, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-masthead-custom-menu-items .ast-inline-search .search-field, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-masthead-custom-menu-items .ast-inline-search .search-field:focus'                      => array(
					'background-color' => esc_attr( $desktop_colors['header-main'] ),
				),
				'.ast-stick-primary-below-wrapper.ast-sticky-active .main-header-bar .main-header-menu, .main-header-bar.ast-sticky-active .main-header-menu, .ast-header-break-point .ast-stick-primary-below-wrapper.ast-sticky-active .main-header-menu, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-masthead-custom-menu-items '                      => array(
					'background-color' => esc_attr( $desktop_colors['primary-menu'] ),
				),
				'.ast-stick-primary-below-wrapper.ast-sticky-active .ast-below-header, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-below-header .ast-search-menu-icon .search-field'                      => array(
					'background-color' => esc_attr( $desktop_colors['header-supp'] ),
				),
			);

			$tablet_header_sections_css_output = array(
				'.ast-stick-primary-below-wrapper.ast-sticky-active .main-header-bar, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-masthead-custom-menu-items .ast-inline-search .search-field, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-masthead-custom-menu-items .ast-inline-search .search-field:focus'                      => array(
					'background-color' => esc_attr( $tablet_colors['header-main'] ),
				),
				'.ast-stick-primary-below-wrapper.ast-sticky-active .main-header-bar .main-header-menu, .main-header-bar.ast-sticky-active .main-header-menu, .ast-header-break-point .ast-stick-primary-below-wrapper.ast-sticky-active .main-header-menu, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-masthead-custom-menu-items '                      => array(
					'background-color' => esc_attr( $tablet_colors['primary-menu'] ),
				),
				'.ast-stick-primary-below-wrapper.ast-sticky-active .ast-below-header, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-below-header .ast-search-menu-icon .search-field'                      => array(
					'background-color' => esc_attr( $tablet_colors['header-supp'] ),
				),
			);

			$mobile_header_sections_css_output = array(
				'.ast-stick-primary-below-wrapper.ast-sticky-active .main-header-bar, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-masthead-custom-menu-items .ast-inline-search .search-field, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-masthead-custom-menu-items .ast-inline-search .search-field:focus'                      => array(
					'background-color' => esc_attr( $mobile_colors['header-main'] ),
				),
				'.ast-stick-primary-below-wrapper.ast-sticky-active .main-header-bar .main-header-menu, .main-header-bar.ast-sticky-active .main-header-menu, .ast-header-break-point .ast-stick-primary-below-wrapper.ast-sticky-active .main-header-menu, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-masthead-custom-menu-items '                      => array(
					'background-color' => esc_attr( $mobile_colors['primary-menu'] ),
				),
				'.ast-stick-primary-below-wrapper.ast-sticky-active .ast-below-header, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-below-header .ast-search-menu-icon .search-field'                      => array(
					'background-color' => esc_attr( $mobile_colors['header-supp'] ),
				),
			);
			/* Parse CSS from array() */
			$css .= astra_parse_css( $desktop_header_sections_css_output );
			$css .= astra_parse_css( $tablet_header_sections_css_output, '', '768' );
			$css .= astra_parse_css( $mobile_header_sections_css_output, '', '544' );
		}

		if ( '' != $desktop_colors['header-top'] ) {
			$desktop_above_css_output = array(
				'.ast-sticky-active .ast-above-header, .ast-above-header.ast-sticky-active, .ast-sticky-active .ast-above-header .ast-search-menu-icon .search-field, .ast-above-header.ast-sticky-active .ast-search-menu-icon .search-field'                      => array(
					'background-color' => esc_attr( $desktop_colors['header-top'] ),
					'visibility'       => esc_attr( 'visible' ),
				),
			);
			$css                     .= astra_parse_css( $desktop_above_css_output );
		}
		if ( '' != $desktop_colors['header-supp'] ) {
			$desktop_below_css_output = array(
				'.ast-sticky-active .ast-below-header, .ast-below-header.ast-sticky-active, .ast-below-header-wrap .ast-sticky-active .ast-search-menu-icon .search-field'                      => array(
					'background-color' => esc_attr( $desktop_colors['header-supp'] ),
					'visibility'       => esc_attr( 'visible' ),
				),
			);
			$css                     .= astra_parse_css( $desktop_below_css_output );
		}

		if ( '' != $tablet_colors['header-top'] ) {
			$tablet_above_css_output = array(
				'.ast-stick-primary-below-wrapper.ast-sticky-active .main-header-bar, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-masthead-custom-menu-items .ast-inline-search .search-field, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-masthead-custom-menu-items .ast-inline-search .search-field:focus'                      => array(
					'background-color' => esc_attr( $tablet_colors['header-top'] ),
					'visibility'       => esc_attr( 'visible' ),
				),
			);
			$css                    .= astra_parse_css( $tablet_above_css_output, '', '768' );
		}
		if ( '' != $tablet_colors['header-supp'] ) {
			$tablet_below_css_output = array(
				'.ast-sticky-active .ast-below-header, .ast-below-header.ast-sticky-active, .ast-below-header-wrap .ast-sticky-active .ast-search-menu-icon .search-field'                      => array(
					'background-color' => esc_attr( $tablet_colors['header-supp'] ),
					'visibility'       => esc_attr( 'visible' ),
				),
			);
			$css                    .= astra_parse_css( $tablet_below_css_output, '', '768' );
		}

		if ( '' != $mobile_colors['header-top'] ) {
			$mobile_above_css_output = array(
				'.ast-stick-primary-below-wrapper.ast-sticky-active .main-header-bar, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-masthead-custom-menu-items .ast-inline-search .search-field, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-masthead-custom-menu-items .ast-inline-search .search-field:focus'                      => array(
					'background-color' => esc_attr( $mobile_colors['header-top'] ),
					'visibility'       => esc_attr( 'visible' ),
				),
			);
			$css                    .= astra_parse_css( $mobile_above_css_output, '', '544' );
		}
		if ( '' != $mobile_colors['header-supp'] ) {
			$mobile_below_css_output = array(
				'.ast-sticky-active .ast-below-header, .ast-below-header.ast-sticky-active, .ast-below-header-wrap .ast-sticky-active .ast-search-menu-icon .search-field'                      => array(
					'background-color' => esc_attr( $mobile_colors['header-supp'] ),
					'visibility'       => esc_attr( 'visible' ),
				),
			);
			$css                    .= astra_parse_css( $mobile_below_css_output, '', '544' );
		}
	}

	$page_width = '100%';
	if ( Astra_Ext_Extension::is_active( 'site-layouts' ) ) {
		if ( 'ast-box-layout' == $site_layout ) {
			$page_width = astra_get_option( 'site-layout-box-width' ) . 'px';
		}

		if ( 'ast-padded-layout' == $site_layout ) {

			$padded_layout_padding = astra_get_option( 'site-layout-padded-pad' );

			/**
			 * Padded layout Desktop Spacing
			 */
			$padded_layout_spacing = array(
				'#ast-fixed-header' => array(
					'top'    => astra_responsive_spacing( $padded_layout_padding, 'top', 'desktop' ),
					'left'   => astra_responsive_spacing( $padded_layout_padding, 'left', 'desktop' ),
					'margin' => esc_attr( 0 ),
				),
			);
			/**
			 * Padded layout Tablet Spacing
			 */
			$tablet_padded_layout_spacing = array(
				'#ast-fixed-header' => array(
					'top'    => astra_responsive_spacing( $padded_layout_padding, 'top', 'tablet' ),
					'left'   => astra_responsive_spacing( $padded_layout_padding, 'left', 'tablet' ),
					'margin' => esc_attr( 0 ),
				),
			);

			/**
			 * Padded layout Mobile Spacing
			 */
			$mobile_padded_layout_spacing = array(
				'#ast-fixed-header' => array(
					'top'    => astra_responsive_spacing( $padded_layout_padding, 'top', 'mobile' ),
					'left'   => astra_responsive_spacing( $padded_layout_padding, 'left', 'mobile' ),
					'margin' => esc_attr( 0 ),
				),
			);

			$parse_css .= astra_parse_css( $padded_layout_spacing );
			$parse_css .= astra_parse_css( $tablet_padded_layout_spacing, '', '768' );
			$parse_css .= astra_parse_css( $mobile_padded_layout_spacing, '', '544' );
		}
	}

	$css       .= '.ast-above-header > div, .main-header-bar > div, .ast-below-header > div {';
	$css       .= '-webkit-transition: all 0.2s linear;';
	$css       .= 'transition: all 0.2s linear;';
	$css       .= '}';
	$css       .= '.ast-above-header, .main-header-bar, .ast-below-header {';
	$css       .= 'max-width:' . esc_attr( $page_width ) . ';';
	$css       .= '}';
	$parse_css .= $css;

	return $dynamic_css .= $parse_css;

}
