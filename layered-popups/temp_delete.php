<?php
	function front_init() {
		global $wpdb, $post, $current_user, $ulp, $porto_settings, $w2dc_instance;

		$post_id = 0;
		$posts_page_id = get_option('page_for_posts');
		if (is_home() && !empty($posts_page_id)) {
			$post_id = $posts_page_id;
		} else if (function_exists('is_product') && is_product()) {
			if (!empty($post)) $post_id = $post->ID;
			else $post_id = 0;
		} else if (function_exists('is_shop') && is_shop() && (function_exists('woocommerce_get_page_id') || function_exists('wc_get_page_id'))) {
			if (function_exists('wc_get_page_id')) $post_id = wc_get_page_id('shop');
			else $post_id = woocommerce_get_page_id('shop');
		} else if (is_singular()) {
			if (!empty($post)) $post_id = $post->ID;
			else $post_id = 0;
		} else if (defined('porto_version') && is_post_type_archive('portfolio')) {
			$post_id = $porto_settings['portfolio-archive-page'];
		} else if (defined('porto_version') && is_post_type_archive('event')) {
			$post_id = $porto_settings['event-archive-page'];
		} else if (defined('porto_version') && is_post_type_archive('member')) {
			$post_id = $porto_settings['member-archive-page'];
		} else if (defined('porto_version') && is_post_type_archive('faq')) {
			$post_id = $porto_settings['faq-archive-page'];
		}
		if (class_exists("w2dc_plugin") && is_object($w2dc_instance)) {
			if (property_exists($w2dc_instance, "frontend_controllers") && array_key_exists('webdirectory-listing', $w2dc_instance->frontend_controllers) && is_array($w2dc_instance->frontend_controllers['webdirectory-listing'])) {
				$w2dc_controller = $w2dc_instance->frontend_controllers['webdirectory-listing'][0];
				if (is_object($w2dc_controller) && $w2dc_controller->is_single && property_exists($w2dc_controller, "listing")) {
					$post_id = $w2dc_controller->listing->post->ID;
				}
			}
		}
		
		$event_popups = array();
		$javascript_vars = array();
		$common_roles = array();
		if ($current_user) $common_roles = array_intersect($current_user->roles, $ulp->options['disable_roles']);
		
		if ($ulp->ext_options['advanced_targeting'] == 'on') {
			if ($post_id == 0 && (is_tax() || is_tag() || is_category())) {
				$queried_object = get_queried_object();
				if (is_a($queried_object, 'WP_Term')) {
					if (property_exists($queried_object, 'slug') && property_exists($queried_object, 'taxonomy')) {
						unset($post_id);
						$post_id = array('term' => $queried_object->slug, 'taxonomy' => $queried_object->taxonomy);
					}
				}
			}
			include_once(dirname(__FILE__).'/core-targeting.php');
			$targeting = new ulp_class_targeting();
			$targets = $targeting->front_init($post_id);
			foreach ($ulp->events as $key => $value) {
				if (array_key_exists($key.'_popup', $ulp->options)) {
					$javascript_vars[$key.'_popup'] = "";
					$javascript_vars[$key.'_mode'] = 'none';
					$javascript_vars[$key.'_period'] = 5;
				}
			}
			$javascript_vars['onload_delay'] = 0;
			$javascript_vars['onload_close_delay'] = 0;
			$javascript_vars['onscroll_offset'] = 600;
			$javascript_vars['onidle_delay'] = 30;
			
			foreach ($targets as $event => $target) {
				$popup = $target['popup'];
				if ($target['popup_mobile'] != 'same' && (!empty($target['popup']) || !empty($target['popup_mobile']))) $popup .= '*'.$target['popup_mobile'];
				if (!empty($target['popup'])) $event_popups[] = $target['popup'];
				if (!empty($target['popup_mobile']) && $target['popup_mobile'] != 'same') $event_popups[] = $target['popup_mobile'];
				$javascript_vars[$event.'_popup'] = $popup;
				if (!empty($common_roles)) $javascript_vars[$event.'_mode'] = 'none';
				else $javascript_vars[$event.'_mode'] = $target['options']['mode'];
				$javascript_vars[$event.'_period'] = intval($target['options']['mode_period']);
				if (in_array($event, array('onload', 'onidle'))) {
					$javascript_vars[$event.'_delay'] = intval($target['options']['delay']);
				}
				if ($event == 'onload') {
					$javascript_vars['onload_close_delay'] = intval($target['options']['close_delay']);
				}
				if ($event == 'onscroll') {
					$javascript_vars['onscroll_offset'] = intval($target['options']['offset']).(strpos($target['options']['offset'], '%') !== false ? '%' : '');
				}
			}
		} else {
			foreach ($ulp->events as $key => $value) {
				if (array_key_exists($key.'_popup', $ulp->options)) {
					$ulp->options[$key.'_popup'] = $ulp->wpml_parse_popup_id($ulp->options[$key.'_popup']);
					$ulp->options[$key.'_popup_mobile'] = $ulp->wpml_parse_popup_id($ulp->options[$key.'_popup_mobile'], 'same');
				}
			}
			if ($post_id > 0) {
				$meta = $ulp->get_meta($post_id);
				foreach ($ulp->events as $key => $value) {
					if (array_key_exists($key.'_popup', $ulp->options)) {
						$popup = ($meta[$key.'_popup'] == 'default' ? $ulp->options[$key.'_popup'] : $meta[$key.'_popup']);
						$popup_mobile = ($meta[$key.'_popup_mobile'] == 'default' ? $ulp->options[$key.'_popup_mobile'] : $meta[$key.'_popup_mobile']);
						if (!empty($popup)) $event_popups[] = $popup;
						if (!empty($popup_mobile) && $popup_mobile != 'same') $event_popups[] = $popup_mobile;
						if ($popup_mobile != 'same' && (!empty($popup) || !empty($popup_mobile))) $popup .= '*'.$popup_mobile;
						$javascript_vars[$key.'_popup'] = $popup;
						if (!empty($common_roles)) $javascript_vars[$key.'_mode'] = 'none';
						else $javascript_vars[$key.'_mode'] = $meta[$key.'_mode'] == 'default' ? $ulp->options[$key.'_mode'] : $meta[$key.'_mode'];
						$javascript_vars[$key.'_period'] = $meta[$key.'_mode'] == 'default' ? intval($ulp->options[$key.'_period']) : intval($meta[$key.'_period']);
					}
				}
				$javascript_vars['onload_delay'] = $meta['onload_popup'] == 'default' ? intval($ulp->options['onload_delay']) : intval($meta['onload_delay']);
				$javascript_vars['onload_close_delay'] = $meta['onload_popup'] == 'default' ? intval($ulp->options['onload_close_delay']) : intval($meta['onload_close_delay']);
				$javascript_vars['onscroll_offset'] = $meta['onscroll_popup'] == 'default' ? intval($ulp->options['onscroll_offset']).(strpos($ulp->options['onscroll_offset'], '%') !== false ? '%' : '') : intval($meta['onscroll_offset']).(strpos($meta['onscroll_offset'], '%') !== false ? '%' : '');
				$javascript_vars['onidle_delay'] = $meta['onidle_popup'] == 'default' ? intval($ulp->options['onidle_delay']) : intval($meta['onidle_delay']);
			} else {
				foreach ($ulp->events as $key => $value) {
					if (array_key_exists($key.'_popup', $ulp->options)) {
						$popup = $ulp->options[$key.'_popup'];
						if ($ulp->options[$key.'_popup_mobile'] != 'same' && (!empty($ulp->options[$key.'_popup']) || !empty($ulp->options[$key.'_popup_mobile']))) $popup .= '*'.$ulp->options[$key.'_popup_mobile'];
						if (!empty($ulp->options[$key.'_popup'])) $event_popups[] = $ulp->options[$key.'_popup'];
						if (!empty($ulp->options[$key.'_popup_mobile']) && $ulp->options[$key.'_popup_mobile'] != 'same') $event_popups[] = $ulp->options[$key.'_popup_mobile'];
						$javascript_vars[$key.'_popup'] = $popup;
						if (!empty($common_roles)) $javascript_vars[$key.'_mode'] = 'none';
						else $javascript_vars[$key.'_mode'] = $ulp->options[$key.'_mode'];
						$javascript_vars[$key.'_period'] = intval($ulp->options[$key.'_period']);
					}
				}
				$javascript_vars['onload_delay'] = intval($ulp->options['onload_delay']);
				$javascript_vars['onload_close_delay'] = intval($ulp->options['onload_close_delay']);
				$javascript_vars['onscroll_offset'] = intval($ulp->options['onscroll_offset']).(strpos($ulp->options['onscroll_offset'], '%') !== false ? '%' : '');
				$javascript_vars['onidle_delay'] = intval($ulp->options['onidle_delay']);
			}
		}
		$filtered = array();
		$all = false;
		$str_id = '';
		if (isset($_GET['ulp'])) {
			$str_id = preg_replace('/[^a-zA-Z0-9-]/', '', $_GET['ulp']);
			if (substr($str_id, 0, 3) == 'ab-') {
				$sql = "SELECT t1.*, t2.str_id FROM ".$wpdb->prefix."ulp_campaign_items t1 JOIN ".$wpdb->prefix."ulp_popups t2 ON t2.id = t1.popup_id JOIN ".$wpdb->prefix."ulp_campaigns t3 ON t3.id = t1.campaign_id WHERE t2.deleted = '0' AND t3.deleted = '0' AND t3.str_id = '".$str_id."' AND t1.deleted = '0'";
				$rows = $wpdb->get_results($sql, ARRAY_A);
				if (sizeof($rows) > 0) {
					foreach ($rows as $row) {
						$filtered[] = $row['str_id'];
					}
				} else $filtered[] = 'none';
			} else $filtered[] = $str_id;
			$all = true;
		} else if ($ulp->options['preload_event_popups'] == 'on' && $ulp->options['no_preload'] == 'on') {
			$filtered = $event_popups;
			if (empty($filtered)) $filtered[] = 'none';
		}
		
		if ($ulp->options['no_preload'] != 'on' || $ulp->options['preload_event_popups'] == 'on') {
			$data = $this->get_popups($filtered, true, $all);
			$ulp->front_header .= $data['header'];
			$ulp->front_footer .= $data['footer'];
		}
		
		$ulp->front_header .= '
		<script>
			var ulp_custom_handlers = {};
			var ulp_cookie_value = "'.$ulp->options['cookie_value'].'";
			var ulp_recaptcha_enable = "'.$ulp->options['recaptcha_enable'].'";';
		$ulp->front_header .= $ulp->options['recaptcha_enable'] != 'on' ? '' : '
			var ulp_recaptcha_public_key = "'.esc_html($ulp->options['recaptcha_public_key']).'";';
		foreach ($javascript_vars as $key => $value) {
			$ulp->front_header .= '
			var ulp_'.$key.' = "'.esc_html($value).'";';
		}
		$ulp->front_header .= '
		</script>';
		if ($javascript_vars['onabd_mode'] != 'none') {
			$ulp->front_header .= '<script src="'.$ulp->plugins_url.'/js/ads.js?ver='.ULP_VERSION.'"></script>';
		}
		
		$ulp->front_footer .= '
		<script>
			var ulp_ajax_url = "'.admin_url('admin-ajax.php').'";
			var ulp_css3_enable = "'.$ulp->options['css3_enable'].'";
			var ulp_ga_tracking = "'.$ulp->options['ga_tracking'].'";
			var ulp_km_tracking = "'.$ulp->options['km_tracking'].'";
			var ulp_onexit_limits = "'.$ulp->options['onexit_limits'].'";
			var ulp_no_preload = "'.$ulp->options['no_preload'].'";
			var ulp_campaigns = {';
		$campaigns = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."ulp_campaigns WHERE deleted = '0' AND blocked = '0' ORDER BY created DESC", ARRAY_A);
		foreach ($campaigns as $campaign) {
			$popups = $wpdb->get_results("SELECT t1.*, t2.str_id FROM ".$wpdb->prefix."ulp_campaign_items t1 JOIN ".$wpdb->prefix."ulp_popups t2 ON t2.id = t1.popup_id WHERE t1.campaign_id = '".$campaign['id']."' AND t1.deleted = '0' AND t2.deleted = '0' AND t2.blocked = '0' ORDER BY t1.created DESC", ARRAY_A);
			$campaign_popups = array();
			foreach($popups as $popup) {
				$campaign_popups[] = $popup['str_id'];
			}
			$ulp->front_footer .= '"'.$campaign['str_id'].'":["'.implode('","', $campaign_popups).'"],';
		}
		$ulp->front_footer .= '"none":[""]};';
		$popups = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."ulp_popups WHERE deleted = '0' AND (blocked = '0'".(empty($str_id) ? '' : " OR str_id = '".esc_sql($str_id)."'").")", ARRAY_A);
		$ulp->front_footer .= '
			var ulp_overlays = {';
		foreach ($popups as $popup) {
			$popup_options = unserialize($popup['options']);
			if (is_array($popup_options)) $popup_options = array_merge($ulp->default_popup_options, $popup_options);
			else $popup_options = $ulp->default_popup_options;
			if ($ulp->options['spinkit_enable'] != 'on') $popup_options['ajax_spinner'] = 'classic';
			$ulp->front_footer .= '"'.$popup['str_id'].'":["'.($popup_options['disable_overlay'] == 'on' ? '' : (!empty($popup_options['overlay_color']) ? $popup_options['overlay_color'] : 'transparent')).'", "'.$popup_options['overlay_opacity'].'", "'.$popup_options['enable_close'].'", "'.$popup_options['position'].'", "'.$popup_options['overlay_animation'].'", "'.$popup_options['ajax_spinner'].'", "'.$popup_options['ajax_spinner_color'].'"],';
		}
		$ulp->front_footer .= '"none":["", "", "", "", ""]};';

		if (!empty($str_id)) {
			$ulp->front_footer .= '
			if (typeof ulp_inline_open == "function" && typeof ulp_open == "function") { 
				ulp_prepare_ids(); ulp_inline_open(false); ulp_open("'.$str_id.'"); jQuery(document).ready(function() {ulp_ready();});
			} else {
				jQuery(document).ready(function(){ulp_prepare_ids(); ulp_inline_open(false); ulp_ready(); ulp_open("'.$str_id.'");});
			}';
		} else {
			$ulp->front_footer .= '
			if (typeof ulp_inline_open == "function" && typeof ulp_init == "function") { 
				ulp_prepare_ids(); ulp_inline_open(false); ulp_init(); jQuery(document).ready(function() {ulp_ready();});
			} else {
				jQuery(document).ready(function(){ulp_prepare_ids(); ulp_inline_open(false); ulp_init(); ulp_ready();});
			}';
		}
		$ulp->front_footer .= '
		</script>';
		if ($ulp->ext_options['late_init'] == 'on') {
			$this->front_enqueue_scripts();
		} else {
			add_action('wp_enqueue_scripts', array(&$this, 'front_enqueue_scripts'), 99);
		}
		add_action('wp_head', array(&$this, 'front_header'), 15);
		add_action('wp_footer', array(&$this, 'front_footer'), 999);
	}
?>