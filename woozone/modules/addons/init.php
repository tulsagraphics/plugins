<?php
/*
* Define class WooZoneAddons
* Make sure you skip down to the end of this file, as there are a few
* lines of code that are very important.
*/
!defined('ABSPATH') and exit;
	  
if (class_exists('WooZoneAddons') != true) {
	class WooZoneAddons
	{
		/*
		* Some required plugin information
		*/
		const VERSION = '1.0';

		/*
		* Store some helpers config
		*/
		public $the_plugin = null;

		private $module_folder = '';
		private $module = '';

		static protected $_instance;


		/*
		* Required __construct() function that initalizes the AA-Team Framework
		*/
		public function __construct()
		{
			global $WooZone;

			$this->the_plugin = $WooZone;
			$this->module_folder = $this->the_plugin->cfg['paths']['plugin_dir_url'] . 'modules/addons/';
			$this->module = $this->the_plugin->cfg['modules']['addons'];

			if ( $this->the_plugin->is_admin ) {
				add_action('admin_menu', array( $this, 'adminMenu' ));
			}
		}

		/**
	    * Hooks
	    */
	    static public function adminMenu()
	    {
	       self::getInstance()
	    		->_registerAdminPages();
	    }

	    /**
	    * Register plug-in module admin pages and menus
	    */
		protected function _registerAdminPages()
    	{
    		add_submenu_page(
    			$this->the_plugin->alias,
    			$this->the_plugin->alias . " " . __('Addons & Themes', $this->the_plugin->localizationName),
	            __('Addons & Themes', $this->the_plugin->localizationName),
	            'manage_options',
	            $this->the_plugin->alias . "_addons",
	            array($this, 'printBaseInterface')
	        );

			return $this;
		}

		public function printBaseInterface()
		{
			global $wpdb;
?>

	<div class="woozone-title_line" style="margin-bottom:10px">
		<div class="aat-icon"></div>
		<div class="aat-musthave"></div>
	</div>


		<div id="<?php echo WooZone()->alias?>" class="WooZone-addons">
			
			<div class="<?php echo WooZone()->alias?>-content"> 
				
				<?php
				// show the top menu
				WooZoneAdminMenu::getInstance()->make_active('addons|addons')->show_menu();
				?>
				
				<!-- Content -->
				<section class="WooZone-main">
					
					<?php 
					echo WooZone()->print_section_header(
						$this->module['addons']['menu']['title'],
						$this->module['addons']['description'],
						$this->module['addons']['help']['url']
					);

					require_once( $this->the_plugin->cfg['paths']['freamwork_dir_path'] . 'settings-template.class.php');

					// Initalize the your aaInterfaceTemplates
					$aaInterfaceTemplates = new aaInterfaceTemplates($this->the_plugin->cfg);

					// then build the html, and return it as string
					echo $aaInterfaceTemplates->build_page( $this->options(), $this->the_plugin->alias, $this->module);
					?>
				</section>
			</div>
		</div>

<?php
		}

		private function options()
		{
			return array(
				$this->module['db_alias'] => array(
					
					/* define the form_sizes  box */
					'addons' => array(
						'title' => 'Amazon settings',
						'icon' => '{plugin_folder_uri}images/amazon.png',
						'size' => 'grid_4', // grid_1|grid_2|grid_3|grid_4
						'header' => false, // true|false
						'toggler' => false, // true|false
						'buttons' => true, // true|false
						'style' => 'panel', // panel|panel-widget
						
						// create the box elements array
						'elements' => array(

							'headeline' => array(
								'type' => 'html',
								'html' => $this->the_box(),
							),
						)
					)
				)
			);
		}

		/**
		* Singleton pattern
		*
		* @return WooZoneAddons Singleton instance
		*/
		static public function getInstance()
		{
			if (!self::$_instance) {
				self::$_instance = new self;
			}

			return self::$_instance;
		}


		//======================================
		//== @alexandra
		public function the_box() {

			// current theme
			$theme_name = wp_get_theme();
			$theme_name_ = $theme_name->get( 'Name' );

			$html = array();
			ob_start();
		?>
		<div class="aat-widget-wrapper">
		<!-- Additional plugin -->
			<div class="aat-add-widget">
				<div class="aat-add-title-wrap">
					<div class="aat-add-title">Additional Variation Images Plugin for WooCommerce</div>
					<div class="aat-add-title-button" style="display:block"></div>
				</div>

				<div class="aat-add-widget-inner additional">
					<div class="aat-add-content">
						<div class="aat-add-strong-content">Optimize your WooCommerce product variation image gallery</div>
						<div> and boost your sales today!  </div>	
						<div class="aat-add-content-space"></div>

					</div>
					<div class="aat-add-content">
						<div class="aat-add-strong-content"> Showcase Product Variations by Importing any number of Additional Images for each Variation </div>
						<div> by using WZone and the Additional Variation Images Plugin! </div>		
						<div class="aat-add-content-space"></div>
					</div>
					<div class="aat-add-version-info">
						<div class="aat-add-strong-content aat-add-version-info">Available Version</div>
						1.0					
					</div>
					<div class="aat-add-bottom-wrapper">
						<a href="https://codecanyon.net/item/additional-variation-images-plugin-for-woocommerce/22035959?ref=AA-Team" target="_new" class="aat-add-button">Get Now</a>
						<?php
						if ( $this->the_plugin->is_plugin_avi_active() ) {
							echo '<span>installed</span>';
						}
						?>
					</div>
				</div>		
			</div>

			<!-- Kingdom Theme -->
			<div class="aat-add-widget">
				<div class="aat-add-title-wrap">
					<div class="aat-add-title">Kingdom - WooCommerce Amazon Affiliates Theme</div>
					<div class="aat-add-title-button" style="display:block"></div>
				</div>

				<div class="aat-add-widget-inner kingdom">
					<div class="aat-add-content">
						<div class="aat-add-strong-content">Kingdom is 100% compatible with WZone </div>
						<div> and it comes with some great features that will help you create a store featuring Amazon products in no time! </div>				
					</div>
					<div class="aat-add-content-space"></div>
					<div class="aat-add-version-info">
						<div class="aat-add-strong-content aat-add-version-info">Available Version</div>
						3.5.1			
					</div>
					<div class="aat-add-bottom-wrapper">
						<a href="https://themeforest.net/item/kingdom-woocommerce-amazon-affiliates-theme/15163199?ref=AA-Team" target="_new" class="aat-add-button">Get Now</a>
						<?php
						if ( in_array($theme_name_, array(
							//'Kingdom - Woocommerce Amazon Affiliates Theme',
							'Kingdom',
							'Kingdom Child Theme',
						)) ) {
							echo '<span>installed</span>';
						}
						?>
					</div>
				</div>		
			</div>

			<!-- BravoStore -->
			<div class="aat-add-widget">
				<div class="aat-add-title-wrap">
					<div class="aat-add-title">Bravo Store - WZone Affiliates Theme for WordPress</div>
					<div class="aat-add-title-button" style="display:block"></div>
				</div>

				<div class="aat-add-widget-inner bravostore ">
					<div class="aat-add-content">
						<div class="aat-add-strong-content">BravoStore is a Woocommerce Amazon Affiliates Theme. </div>
						<div> This is the second theme that’s 100% compatible with WZone! </div>				
					</div>
					<div class="aat-add-content-space"></div>
					<div class="aat-add-version-info">
						<div class="aat-add-strong-content aat-add-version-info">Available Version</div>
						1.2					
					</div>
					<div class="aat-add-bottom-wrapper">
						<a href="https://themeforest.net/item/bravostore-wzone-affiliates-theme-for-wordpress/20701838?ref=AA-Team" target="_new" class="aat-add-button">Get Now</a>
						<?php
						if ( in_array($theme_name_, array(
							'BravoStore',
							'BravoStore Child Theme',
						)) ) {
							echo '<span>installed</span>';
						}
						?>
					</div>
				</div>		
			</div>

			<!-- Themarket -->
			<div class="aat-add-widget">
				<div class="aat-add-title-wrap">
					<div class="aat-add-title">The Market - WooZone Affiliates Theme</div>
					<div class="aat-add-title-button" style="display:block"></div>
				</div>

				<div class="aat-add-widget-inner themarket">
					<div class="aat-add-content">
						<div class="aat-add-strong-content">The Market has a minimalist design</div>
						<div>Modern, one page & full width. Also Responsive!</div>				
					</div>
					<div class="aat-add-content-space"></div>
					<div class="aat-add-version-info">
						<div class="aat-add-strong-content aat-add-version-info">Available Version</div>
						2.0					
					</div>
					<div class="aat-add-bottom-wrapper">
						<a href="https://codecanyon.net/item/the-market-woozone-affiliates-theme/13469852?ref=AA-Team" target="_new" class="aat-add-button">Get Now</a>
						<?php
						if ( in_array($theme_name_, array(
							'TheMarket',
							'The Market Child Theme',
						)) ) {
							echo '<span>installed</span>';
						}
						?>
					</div>
				</div>		
			</div>
		</div>
		<?php
			$html[] = ob_get_clean();
			return implode( PHP_EOL, $html );
		}

	}
}
 
// Initialize the WooZoneAddons class
$WooZoneAddons = WooZoneAddons::getInstance();
