<?php
/*
* Define class WooZone_ImagesFix
* Make sure you skip down to the end of this file, as there are a few
* lines of code that are very important.
*/
!defined('ABSPATH') and exit;
if (class_exists('WooZone_ImagesFix') != true) {
	class WooZone_ImagesFix
	{
		const VERSION = '1.0';
		
		public $the_plugin = null;
		public $is_admin = null;

		public $amz_settings = array();

		static protected $_instance;

		public $alias;
		public $localizationName;

		public $page;

		public $is_remote_images;

		public $duplicate_images = array();


		/*
		 * Required __construct() function that initalizes the AA-Team Framework
		 */
		public function __construct( $parent )
		{
			$this->the_plugin = $parent;
			$this->is_admin = $this->the_plugin->is_admin;
			
			$this->amz_settings = $this->the_plugin->amz_settings;

			$this->alias = $this->the_plugin->alias;
			$this->localizationName = $this->the_plugin->localizationName;

			$this->is_remote_images = $this->the_plugin->is_remote_images;


			// Fixed Images Issue (from amazon CDN) with https
			/*
			$cond1 = ! isset($_REQUEST['wp_customize']);
			$cond2 = ! isset($_REQUEST['action']) || ( ! in_array($_REQUEST['action'], array('upload-plugin', 'upload-theme')) );
			if ( $cond1 && $cond2 ) {
				if ( ! $this->is_plugin_active('w3totalcache') ) {

					//add_action( 'after_setup_theme', array( $this, 'buffer_start' ), 10 );
					//add_action( 'shutdown', array( $this, 'buffer_end' ), 10, 1 );
					add_action( 'plugins_loaded', array( $this, 'buffer_end_pre' ), 0 ); // hooks tried: plugins_loaded | wp_loaded
				}
				else {
					//add_filter( 'w3tc_can_cache', array($this, 'w3tc_can_cache'), 10, 3 );
					add_filter( 'w3tc_process_content', array($this, 'w3tc_process_content'), 999, 1 );
				}

				// 2017-08-17 fixes for amazon images
				add_filter( 'wp_prepare_attachment_for_js', array($this, 'wp_prepare_attachment_for_js'), 10, 3 );
				add_filter( 'woocommerce_available_variation', array($this, 'woocommerce_available_variation'), 10, 3 );
			}
			*/


			if ( $this->is_admin ) {
				// FIX: woocommerce product list image srcset wrong url
				//add_filter( 'max_srcset_image_width', create_function( '', 'return 1;' ) );
				add_filter( 'max_srcset_image_width', function () {
					return 1;
				});
			}

			add_filter( 'woocommerce_cart_item_thumbnail', array($this, 'filter_woocommerce_cart_item_thumbnail'), 10, 3 );

			//:: REMOTE AMAZON IMAGES
			if ( $this->is_remote_images ) {

				// deactivated on 2017-08-16
				add_filter( "wp_get_attachment_url", array($this, '_attachment_url'), 0, 2);
				add_filter( "wp_calculate_image_srcset", array($this, '_calculate_image_srcset'), 0, 5);

				//add_filter( "woocommerce_single_product_image_thumbnail_html", array($this, 'woocommerce_image_replace_src_revert'));
				//add_filter( "wp_get_attachment_image_src", array($this, 'woocommerce_image_replace_src_revert'));
				//add_filter( "wp_get_attachment_thumb_url", array($this, '_attachment_url'), 0, 2);
				//add_filter( "wp_get_attachment_metadata", array($this, '_attachment_metadata'), 0, 2);
				//add_filter( "image_get_intermediate_size", array($this, '_intermediate_size'), 0, 3);
				
				/*$meta_type = array('post', 'product');
				foreach ($meta_type as $meta_t) {
					get_{$meta_type}_metadata filter from wp-includes/meta.php
					add_filter( "get_{$meta_t}_metadata", array($this, '_hook_woc_metadata'), 0, 4);
				}*/
			}

			// generate wordpress & woocommerce image sizes for amazon remote images
			add_filter( 'get_post_metadata', array($this, 'gpm_on_wp_attachment_metadata'), 999, 4 );

			// gallery full size (for amazon images)
			// woo filters: woocommerce_gallery_full_size | woocommerce_product_thumbnails_large_size
			add_filter( 'wp_get_attachment_image_src', array($this, 'wp_get_attachment_image_src'), 999, 4 );
		}

		/**
		 * Singleton pattern
		 *
		 * @return Singleton instance
		 */
		static public function getInstance( $parent )
		{
			if (!self::$_instance) {
				self::$_instance = new self($parent);
			}
			
			return self::$_instance;
		}

		public function amazon_url_to_ssl( $url='' ) {
			if (empty($url)) return $url;
			if ( ! $this->the_plugin->is_ssl() ) return $url;

			// http://ecx.images-amazon TO https://images-na.ssl-images-amazon
			$newurl = preg_replace(
				'/^http\:\/\/ec(.){0,1}\.images\-amazon/imu',
				'https://images-na.ssl-images-amazon',
				$url
			);
			return !empty($newurl) ? $newurl : $url;
		}

		public function woocommerce_image_replace_src( $html='' ) {
			//return str_replace( "http", "http__", $html);
			return $html;
		}
		public function woocommerce_image_replace_src_revert( $html='' ) {
			//return str_replace( "http__", "http", $html);
			return $html;
		}

		public function _attachment_url( $url='', $post_id=0 ) 
		{
			$orig_url = $url;

			if( in_array( $orig_url, array_keys($this->duplicate_images) ) ){
				if( isset($this->duplicate_images[$orig_url]) ){
					return $this->duplicate_images[$orig_url];
				}
			}

			// mandatory - must be amazon product
			$post = get_post($post_id);

			if ( isset($post->post_parent) && $post->post_parent
				&& $this->the_plugin->verify_product_is_amazon_valid($post->post_parent) === 0
			) {
				return $url;
			}

			// mandatory rule - must have amazon url
			$rules = array();
			$rules[0] = strpos( $url, $this->the_plugin->get_amazon_images_path() );
			$rules = $rules[0];

			if ( $rules ) {
				$uploads = wp_get_upload_dir();
				$url = str_replace( $uploads['baseurl'] . '/', '', $url );
	
				if( $this->the_plugin->is_ssl() == true ) {
					$uploads['baseurl'] = str_replace( 'http://', 'https://', $uploads['baseurl']);  
					$url = str_replace( $uploads['baseurl'] . '/', '', $url );
				}
			}
			$url = $this->amazon_url_to_ssl( $url );
			if ( ! is_admin() ) {
				$url = $this->woocommerce_image_replace_src( $url );
			}


			$this->duplicate_images[$orig_url] = $url;
			//var_dump( "<pre>", $this->duplicate_images  , "</pre>" ); 
			return $url;
		}

		public function _calculate_image_srcset( $sources=array(), $size_array=array(), $image_src='', $image_meta=array(), $attachment_id=0 ) {

			if ( empty($sources) ) return $sources;

			// mandatory - must be amazon product
			$post = get_post($attachment_id);
			
			if ( isset($post->post_parent) && $post->post_parent
				&& $this->the_plugin->verify_product_is_amazon_valid($post->post_parent) === 0
			) {
				return $sources;
			}

			$uploads = wp_get_upload_dir();
			foreach ( $sources as &$source ) {
				// mandatory rule - must have amazon url
				$rules = array();
				$rules[0] = strpos( $source['url'], $this->the_plugin->get_amazon_images_path() );
				$rules = $rules[0];

				if ( $rules ) {
					$source['url'] = str_replace( $uploads['baseurl'] . '/', '', $source['url'] );
				}
				$source['url'] = $this->amazon_url_to_ssl( $source['url'] );
				if ( ! is_admin() ) {
					$source['url'] = $this->woocommerce_image_replace_src( $source['url'] );
				}
			}

			//var_dump('<pre>',$sources,'</pre>');  
			return $sources;
		}




		public function filter_woocommerce_cart_item_thumbnail( $product_get_image, $cart_item, $cart_item_key ) {
			$product_get_image = $this->_parse_page_fix_amazon( $product_get_image );
			return $product_get_image;
		}

		public function wp_prepare_attachment_for_js($response, $attachment, $meta) {
			$theid = isset($response['id']) ? (int) $response['id'] : 0;
			if ( ! $theid ) return $response;
			
			if ( isset($response['url']) ) {
				$response['url'] = $this->_attachment_url( $response['url'], $theid );
			}
			if ( isset($response['sizes']) ) {
				foreach ($response['sizes'] as $key => $val) {
					if ( isset($val['url']) ) {
						$response['sizes']["$key"]['url'] = $this->_attachment_url( $val['url'], $theid );
					}
				}
			}
			return $response;
		}

		public function woocommerce_available_variation( $data, $that, $variation ) {
			//var_dump('<pre>', $data, $that, $variation, '</pre>'); echo __FILE__ . ":" . __LINE__;die . PHP_EOL;
			
			$image = array();
			if ( isset($data['image']) && ! empty($data['image']) ) {
				$image = $data['image'];
			}
			else {
				return $data;
			}
			
			foreach ($image as $key => $prop) {
				if ( ! in_array($key, array('url', 'src', 'srcset', 'full_src', 'thumb_src')) ) {
					continue 1;
				}
				
				//$image["$key"] = $this->_parse_page_fix_amazon( $prop );

				$theid = isset($data['variation_id']) ? (int) $data['variation_id'] : 0;
				if ( ! $theid ) return $data;

				$image["$key"] = $this->_attachment_url( $prop, $theid );
			}

			$data['image'] = $image;
			return $data;
		}


		/**
		 * w3 total cache related
		 */
		public function w3tc_can_cache($original_can_cache, $that, $buffer) {
			return true;
		}
		
		public function w3tc_process_content( $buffer ) {
			$buffer = $this->buffer_end( array('page' => $buffer, 'show' => false) );
			return $buffer;
		}


		public function buffer_end_pre()
		{
			ob_end_clean();
		}

		public function buffer_end( $pms=array() )
		{
			$pms = array_filter( (array) $pms );
			$pms = array_replace_recursive(array(
				'show'		=> true,
				'page'		=> '',
			), $pms);
			extract($pms);

			$page = isset($page) && ! empty($page) ? $page : $this->page;

			$page = $this->_parse_page_fix_amazon( $page );

			$cacheImagesDebug = $this->debug_cache_images();
			if ( ! empty($cacheImagesDebug) && isset($_REQUEST['aateam']) && (bool) $_REQUEST['aateam'] ) {
				$page .= $cacheImagesDebug;
			}

			$this->page = $page;
			if ( $show ) {
				echo $page;
			}
			return $page;
		}

		public function buffer_start()
		{
			ob_start( array($this, 'buffer_callback') );
		}

		public function buffer_callback( $buffer ) 
		{
			$this->page = $buffer;
		}


		public function _parse_page_fix_amazon( $page ) {
			$upload = wp_upload_dir();
			$upload_base = $upload['baseurl'];
			$upload_base_ = str_replace( array("http://", "https://"), '', $upload_base );
			$upload_base_non_ssl = 'http://' . $upload_base_;
			$upload_base_is_ssl = 'https://' . $upload_base_;
			$upload_base__ = array( $upload_base_is_ssl . '/', $upload_base_non_ssl . '/', '//' . $upload_base_ . '/' );
			//var_dump('<pre>',$upload_base__,'</pre>'); echo __FILE__ . ":" . __LINE__;die . PHP_EOL;
			
			// fix for wordpress.com hosted sites and jetpack
			// https://jetpack.com/support/photon/
			if ( class_exists( 'Jetpack' )
				&& method_exists( 'Jetpack', 'get_active_modules' )
				&& in_array( 'photon', Jetpack::get_active_modules() )
			) {
				$upload_base__[] = 'https://i0.wp.com/' . $upload_base_ . '/';
				$upload_base__[] = 'https://i1.wp.com/' . $upload_base_ . '/';
				$upload_base__[] = 'https://i2.wp.com/' . $upload_base_ . '/';
				$upload_base__[] = 'https://i3.wp.com/' . $upload_base_ . '/';
			}

			//:: PARSE PAGE

			//:: all images
			$nb_images = preg_match_all( '/<img[^>]+>/i', $page, $images );
			$images = isset($images[0]) && ! empty($images[0]) ? (array) $images[0] : array();

			// debug!
			//var_dump('<pre>', $nb_images, $result, '</pre>'); //echo __FILE__ . ":" . __LINE__;die . PHP_EOL;

			// has images?
			if ( ! empty($images) ) {
				foreach ( $images as $page_img ) {

					// is amazon image? images-amazon.
					if ( strpos( $page_img, $this->the_plugin->get_amazon_images_path() ) === false ) {
						continue 1;
					}

					$new_img_html = $page_img;
					$new_img_html = str_replace( $upload_base__, '', $new_img_html );

					// check if is ssl image hosted
					$amz_ = ( strpos( $page_img, 'ssl-images' ) !== false ? 'https://' : 'http://' );

					$new_img_html = str_replace( 'src="//', 'src="' . $amz_, $new_img_html );
					$new_img_html = str_replace( 'srcset="//', 'srcset="' . $amz_, $new_img_html );
					$new_img_html = str_replace( 'data-large_image="//', 'data-large_image="' . $amz_, $new_img_html );
					$new_img_html = str_replace( ', //', ', ' . $amz_, $new_img_html );
	
					$page = str_replace( $page_img, $new_img_html, $page );
				} // end foreach
			} // end has images?

			// debug!
			/*
			$nb_images = preg_match_all('/<img[^>]+>/i', $page, $result);
			var_dump('<pre>', $nb_images, $result, '</pre>'); //echo __FILE__ . ":" . __LINE__;die . PHP_EOL;
			echo __FILE__ . ":" . __LINE__;die . PHP_EOL;
			*/
			
			//:: others
			$nb_others = preg_match_all(
				'/=(?:"|\')[^"\']*' . preg_quote( $this->the_plugin->get_amazon_images_path() ) . '[^"\']*(?:"|\')/i',
				$page,
				$others
			);
			$others = isset($others[0]) && ! empty($others[0]) ? (array) $others[0] : array();

			// debug!
			//var_dump('<pre>', $nb_images, $result, '</pre>'); //echo __FILE__ . ":" . __LINE__;die . PHP_EOL;

			// has others?
			if ( ! empty($others) ) {
				foreach ( $others as $page_img ) {

					$new_img_html = $page_img;
					$new_img_html = str_replace( $upload_base__, '', $new_img_html );
					$page = str_replace( $page_img, $new_img_html, $page );
				} // end foreach
			} // end has images?

			//:: END PARSE PAGE

			//var_dump( "<pre>", $page , "</pre>" ) . PHP_EOL .  __FILE__ . ":" . __LINE__; die;  
			//die( var_dump( "<pre>", $page  , "<pre>" ) . PHP_EOL .  __FILE__ . ":" . __LINE__  );
			
			return $page;
		}


		public function debug_cache_images() {
			if ( $this->the_plugin->is_debug_mode_allowed() ) {
				$html = array();
				$html[] = '<div style="background-color: #3498db; color: #fff; position: fixed; bottom: 25px; right: 25px; max-width: 200px; font-size: 10px;">';
				$html[] = 		'<table style="border-spacing: 2px; margin: 0px; border: 0px;">';
				$html[] = 			'<thead style="line-height: 10px;">';
				$html[] =				'<tr>';
				if ( isset($_SESSION['WooZoneCachedContor']) ) {
					foreach ($_SESSION['WooZoneCachedContor'] as $key => $val) {
						$html[] = 				'<th>' . str_replace('_', ' ', $key) . '</th>';
					}
				}
				$html[] =				'</tr>';
				$html[] = 			'</thead>';
				$html[] = 			'<tbody style="line-height: 10px;">';
				$html[] = 				'<tr>';
				if ( isset($_SESSION['WooZoneCachedContor']) ) {
					foreach ($_SESSION['WooZoneCachedContor'] as $key => $val) {
						$html[] = 				'<td>' . $val . '</td>';
					}
				}
				$html[] = 				'</tr>';
				$html[] = 			'</tbody>';
				$html[] = 		'</table>';
				$html[] = '</div>';

				$html = implode(PHP_EOL, $html);
				return $html;
			}
			return false;
		}


		public function _hook_woc_metadata($metadata, $object_id, $meta_key, $single) {
			//var_dump('<pre>',$metadata, $object_id, $meta_key, $single,'</pre>');
			$metadata_orig = $metadata;
			
			$parsing = array(
				//'_product_image_gallery',
				//'_thumbnail_id',
				'_wp_attached_file',
				'_wp_attachment_metadata'
			);
			if ( !isset($meta_key) || !in_array($meta_key, $parsing) ) return $metadata;
			
			// must be amazon product
			// ... to do

			// loop through keys
			switch ( $meta_key ) {
				case '_wp_attached_file':
					$metadata = $this->_get_meta_key( $meta_key, $object_id );
					if ( empty($metadata) ) return $metadata;

					if ( strpos( $metadata->meta_value, $this->the_plugin->get_amazon_images_path() ) ) {
						return $metadata->meta_value;
					}

					$metadata = $this->_get_amz_asset( (int) $metadata->post_id );
					if ( empty($metadata) ) return $metadata;
						
					$metadata = $metadata->asset;
					break;
					
				case '_wp_attachment_metadata':
					$metadata = $this->_get_meta_key( $meta_key, $object_id );
					if ( empty($metadata) ) return $metadata;

					$meta_value = maybe_unserialize( $metadata->meta_value );
					if ( empty($meta_value) || !is_array($meta_value) ) {
						return $metadata_orig;
					}
	
					$metadata_ = array_replace_recursive(array(
						'width'         => 0,
						'height'        => 0,
						'file'          => '',
						'sizes'         => array(),
						'image_meta'    => array(),
					), $meta_value);
					
					if ( !empty($metadata_['file'])
						&& strpos( $metadata_['file'], $this->the_plugin->get_amazon_images_path() ) ) {
						return array($metadata_);
					}

					$metadata = $this->_get_amz_asset( (int) $metadata->post_id );
					if ( empty($metadata) ) return $metadata;
					
					$metadata_['file'] = $metadata->asset;
					
					$image_sizes = get_intermediate_image_sizes();
					foreach ( $image_sizes as $_size ) {

						$url = $metadata->asset;
						if ( in_array($_size, array('thumbnail', 'shop_thumbnail')) ) {
							$url = $metadata->thumb;
						}
						$url = basename($url);

						if ( isset($metadata_['sizes'], $metadata_['sizes']["$_size"]) ) {
							$metadata_['sizes']["$_size"]['file'] = $url;
						}
					}
					$metadata = array($metadata_);
					break;
			}

			//var_dump('<pre>',$object_id, $meta_key, $metadata,'</pre>');  
			return $metadata;
		}

		public function _get_meta_key( $meta_key, $post_id=0 ) {
			if ( empty($post_id) ) return false;
	 
			global $wpdb;
			
			$q = "select pm.post_id, pm.meta_value from $wpdb->postmeta as pm where 1=1 and pm.post_id = %s and pm.meta_key = %s order by pm.meta_id desc limit 1;";
			$q = $wpdb->prepare( $q, $post_id, $meta_key );
			$res = $wpdb->get_row( $q );
			if ( empty($res) ) return null;
			return $res;
		}
		
		public function _get_amz_asset( $media_id=0 ) {
			if ( empty($media_id) ) return false;
	 
			global $wpdb;
			$table = $wpdb->prefix . 'amz_assets';
	 
			$q = "select a.asset, a.thumb from $table as a where 1=1 and a.media_id = %s order by a.id asc limit 1;";
			$q = $wpdb->prepare( $q, $media_id );
			$res = $wpdb->get_row( $q );
			if ( empty($res) ) return null;
			return $res;
		}

		public function _intermediate_size( $data=array(), $post_id=0, $size='' ) {
		}

		public function _attachment_metadata( $data='', $post_id=0 ) {
			return $data;

			$rules = array();
			$rules[0] = !empty($data) && is_array($data);
			$rules[1] = $rules[0] && isset($data['width'], $data['width'], $data['file'], $data['image_meta']);
			$rules[2] = $rules[0] && isset($data['sizes'])
				&& !empty($data['sizes']) && is_array($data['sizes']);
			$rules = $rules[0] && $rules[1] && $rules[2];

			if ( $rules ) {
			} 
			return $data;
		}



		// used on wordpress hook 'get_post_metadata'
		public function gpm_on_wp_attachment_metadata( $null, $object_id, $meta_key, $single ) {
			if ( ! isset($meta_key) ) {
				return $null;
			}
			if ( ! $object_id ) {
				return $null;
			}

			if ( '_wp_attachment_metadata' == $meta_key ) {

				remove_filter( 'get_post_metadata', array( $this, 'gpm_on_wp_attachment_metadata' ), 999 );
				$current_meta = get_post_meta( $object_id, $meta_key, true );
				add_filter( 'get_post_metadata', array( $this, 'gpm_on_wp_attachment_metadata' ), 999, 4 );
				//var_dump('<pre>', $current_meta , '</pre>'); echo __FILE__ . ":" . __LINE__;die . PHP_EOL;

				if ( empty($current_meta) || ! is_array($current_meta) ) {
					return $null;
				}

				$file = isset($current_meta['file']) ? $current_meta['file'] : '';
				$sizes = isset($current_meta['sizes']) ? $current_meta['sizes'] : array();

				// file is mandatory!
				//if ( empty($file) || empty($sizes) || ! is_array($sizes) ) {
				if ( empty($file) ) {
					return $null;
				}

				// is amazon remote image?
				if ( false === strpos( $file, $this->the_plugin->get_amazon_images_path() ) ) {
					return $null;
				}

				//$wp_sizes = $this->get_image_sizes_allowed();
				$wp_sizes = $this->the_plugin->u->get_image_sizes();
				//var_dump('<pre>', $wp_sizes , '</pre>'); echo __FILE__ . ":" . __LINE__;die . PHP_EOL;

				//:: build image sizes
				$sizes = $this->build_amazon_image_sizes( array(
					'wp_sizes' 		=> $wp_sizes,
					'image_sizes' 	=> $sizes,
					'image_path' 	=> $file,
				));
				//var_dump('<pre>', $sizes, '</pre>'); echo __FILE__ . ":" . __LINE__;die . PHP_EOL;

				//:: high resolution image
				// woo filters: woocommerce_gallery_full_size | woocommerce_product_thumbnails_large_size
				$amzhires_url = isset($current_meta['_wzone'], $current_meta['_wzone']['amzhires_url']) ?
					$current_meta['_wzone']['amzhires_url'] : '';
				$amzhires_size = isset($current_meta['_wzone'], $current_meta['_wzone']['amzhires_size']) ?
					$current_meta['_wzone']['amzhires_size'] : array();

				if ( ! empty($amzhires_url) && ! empty($amzhires_size) ) {
					$sizes['full'] = array_merge(
						isset($sizes['full']) ? $sizes['full'] : array(),
						$amzhires_size
					);
					//$sizes['full']['file'] = $amzhires_url;
				}

				$current_meta['sizes'] = $sizes;
				//var_dump('<pre>', $current_meta , '</pre>'); echo __FILE__ . ":" . __LINE__;die . PHP_EOL;

				// always return an array with your return value => no need to handle $single
				return array( $current_meta );
			}
			return $null;
		}

		// used also in /aa-framework/amz.helper.class.php
		public function build_amazon_image_sizes( $pms=array() ) {

			$pms = array_replace_recursive( array(
				'wp_sizes' 		=> array(),
				'image_sizes' 	=> array(),
				'image_path' 	=> '',
			), $pms );
			extract( $pms );

			$sizes_new = array();

			$wp_filetype = wp_check_filetype( basename( $image_path ), null );
			$image_ext = isset($wp_filetype['ext']) ? $wp_filetype['ext'] : 'jpg';

			$image_name = preg_replace( '/\.[^.]+$/', '', basename( $image_path ) );
			//var_dump('<pre>', $image_name, $image_ext, '</pre>'); echo __FILE__ . ":" . __LINE__;die . PHP_EOL;

			foreach ( $wp_sizes as $sizeid => $sizeinfo ) {

				$imgsize = isset($image_sizes["$sizeid"]) ? $image_sizes["$sizeid"] : array();
				$mime_type = isset($imgsize['mime-type']) ? $imgsize['mime-type'] : 'image/jpeg';

				$w = isset($sizeinfo['width']) ? $sizeinfo['width'] : 0;
				$h = isset($sizeinfo['height']) ? $sizeinfo['height'] : 0;

				$amzsize = $w ? $w : $h;
				if ( empty($amzsize) ) {
					continue 1;
				}

				$amzname = $image_name . "._SS{$amzsize}_";
				//$amzname = str_replace( $image_name, $amzname, $file );
				$amzname = $amzname . '.' . $image_ext;

				$sizes_new["$sizeid"] = array(
					'file' 		=> $amzname,
					'width' 	=> $amzsize,
					'height' 	=> $amzsize,
					'mime-type' => $mime_type,
				);
			}
			//var_dump('<pre>', $sizes_new, '</pre>'); echo __FILE__ . ":" . __LINE__;die . PHP_EOL;
			return $sizes_new;
		}

		public function wp_get_attachment_image_src( $image, $attachment_id, $size, $icon ) {

			if ( 'full' != $size ) {
				return $image;
			}
			//var_dump('<pre>jimmydbg',$image, $attachment_id, $size, $icon ,'</pre>');

			remove_filter( 'get_post_metadata', array( $this, 'gpm_on_wp_attachment_metadata' ), 999 );
			$current_meta = get_post_meta( $attachment_id, '_wp_attachment_metadata', true );
			add_filter( 'get_post_metadata', array( $this, 'gpm_on_wp_attachment_metadata' ), 999, 4 );
			//var_dump('<pre>', $current_meta , '</pre>'); echo __FILE__ . ":" . __LINE__;die . PHP_EOL;

			if ( empty($current_meta) || ! is_array($current_meta) ) {
				return $image;
			}

			$file = isset($current_meta['file']) ? $current_meta['file'] : '';

			// file is mandatory!
			if ( empty($file) ) {
				return $image;
			}

			// is amazon remote image?
			if ( false === strpos( $file, $this->the_plugin->get_amazon_images_path() ) ) {
				return $image;
			}

			//:: high resolution image
			$amzhires_url = isset($current_meta['_wzone'], $current_meta['_wzone']['amzhires_url']) ?
				$current_meta['_wzone']['amzhires_url'] : '';
			$amzhires_size = isset($current_meta['_wzone'], $current_meta['_wzone']['amzhires_size']) ?
				$current_meta['_wzone']['amzhires_size'] : array();

			if ( ! empty($amzhires_url) && ! empty($amzhires_size) ) {

				if ( is_array($image) && isset($image[0]) ) {
					$image[0] = $amzhires_url; // . '#jimmydbg';
				}
			}
			return $image;
		}
	}
}