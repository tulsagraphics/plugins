<?php
/**
 * Body Classes
 *
 * @package Page Builder Framework Premium Addon
 */
 
// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function wpbf_premium_body_classes( $classes ) {

	$push_menu = get_theme_mod( 'menu_off_canvas_push' );
	$menu_position = get_theme_mod( 'menu_position' );

	if( $push_menu == true && $menu_position == 'menu-off-canvas' ) {
		$classes[] = 'wpbf-push-menu-right';
	} elseif ( $push_menu == true && $menu_position == 'menu-off-canvas-left' ) {
		$classes[] = 'wpbf-push-menu-left';
	}

	if( wpbf_has_responsive_breakpoints() ) {

		$classes[] = 'wpbf-responsive-breakpoints';

		$classes[] = 'wpbf-medium-breakpoint-' . wpbf_breakpoint_medium();
		$classes[] = 'wpbf-desktop-breakpoint-' . wpbf_breakpoint_desktop();

	}

	return $classes;

}
add_filter( 'body_class', 'wpbf_premium_body_classes' );