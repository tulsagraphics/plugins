<?php

namespace WPBF;

class HookSystem {
	public function __construct() {
		add_action( 'init', [$this, 'register_cpt'] );
		add_action( 'admin_menu', [$this, 'menu_item'], 100 );
		add_action( 'admin_head', [$this, 'fix_current_item'] );
		add_filter( 'manage_wpbf_hooks_posts_columns', [$this, 'register_columns'] );
		add_action( 'manage_wpbf_hooks_posts_custom_column', array( $this, 'add_columns' ), 10, 2 );

		add_action( 'add_meta_boxes', [$this, 'meta_box'] );

		add_filter( 'post_updated_messages', [$this, 'cpt_messages'] );

		add_action( 'save_post', [$this, 'save_meta_box_data'] );

		add_action( 'wp', [$this, 'do_published_hooks'] );
		add_action( 'wp', [$this, 'frontend_show_hooks'] );
		add_action( 'admin_bar_menu', [$this, 'display_hooks'], 999 );

		add_action( 'template_redirect', [ $this, 'cpt_redirect' ] );
	}

	public function do_published_hooks() {
		$args = array(
			'post_type'     => 'wpbf_hooks',
			'no_found_rows' => true,
			'post_status'   => 'publish',
			'numberposts'   => 100,
			'fields'        => 'ids',
			'order'         => 'ASC',
		);

		$posts = get_posts( $args );

		foreach ( $posts as $post_id ) {

			if( is_singular( 'wpbf_hooks' ) ) return; // stopping from outputting the hooks on the actual hooks posts.

			$action   = get_post_meta( $post_id, '_wpbf_hook_action', true );
			$priority = get_post_meta( $post_id, '_wpbf_hook_priority', true );

			if ( ! empty( $action ) ) {

				if ( empty( $priority ) ) $priority = 10;

				add_action( $action, function () use ( $post_id ) {
					if(class_exists( '\Elementor\Plugin' ) && \Elementor\Plugin::$instance->db->is_built_with_elementor( $post_id ) ) {
						echo do_shortcode( sprintf( '[elementor-template id="%s"]', $post_id ) );
					}
					elseif (class_exists( '\FLBuilderModel' ) && \FLBuilderModel::is_builder_enabled( $post_id ) ) {
						echo do_shortcode( sprintf( '[fl_builder_insert_layout id="%s"]', $post_id ) );
					}
					else {
						echo do_shortcode( get_post_field( 'post_content', $post_id ) );
					}
				}, absint( $priority ) );
			}
		}
	}

	/**
	 * Make sure our admin menu item is highlighted.
	 *
	 */
	public function fix_current_item() {
		global $parent_file, $submenu_file, $post_type;

		if ( 'wpbf_hooks' === $post_type ) {
			$parent_file  = 'themes.php';
			$submenu_file = 'edit.php?post_type=wpbf_hooks';
		}
	}

	public function register_cpt() {
		$labels = array(
			'name'                  => _x( 'Parts', 'Post Type General Name', 'wpbfpremium' ),
			'singular_name'         => _x( 'Part', 'Post Type Singular Name', 'wpbfpremium' ),
			'menu_name'             => __( 'Parts', 'wpbfpremium' ),
			'all_items'             => __( 'All Parts', 'wpbfpremium' ),
			'add_new_item'          => __( 'Add New Part', 'wpbfpremium' ),
			'new_item'              => __( 'New Part', 'wpbfpremium' ),
			'edit_item'             => __( 'Edit Part', 'wpbfpremium' ),
			'update_item'           => __( 'Update Part', 'wpbfpremium' ),
			'search_items'          => __( 'Search Part', 'wpbfpremium' ),
		);

		$args = array(
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => false,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
		);

		register_post_type( 'wpbf_hooks', $args );

	}

	/**
	 * Register custom post type columns.
	 *
	 * @param array $columns Existing CPT columns.
	 *
	 * @return array All our CPT columns.
	 */
	public function register_columns( $columns ) {
		$columns['wpbf_hook_action'] = esc_html__( 'Location', 'wpbfpremium' );

		$new_columns = array();

		foreach ( $columns as $key => $value ) {
			if ( 'date' === $key ) {
				$new_columns['wpbf_hook_action'] = esc_html__( 'Location', 'wpbfpremium' );
			}

			$new_columns[$key] = $value;
		}

		return $new_columns;
	}

	/**
	 * Add content to our custom post type columns.
	 *
	 * @param string $column The name of the column.
	 * @param int $post_id The ID of the post row.
	 */
	public function add_columns( $column, $post_id ) {
		if ($column == 'wpbf_hook_action') {
			echo get_post_meta( $post_id, '_wpbf_hook_action', true );
		}
	}

	/**
	 * CPT updates messages.
	 *
	 * @param array $messages Existing post update messages.
	 *
	 * @return array Amended wpbf_hooks CPT notices
	 */
	public function cpt_messages( $messages ) {
		$post = get_post();

		$messages['wpbf_hooks'] = array(
			0  => '', // Unused. Messages start at index 1.
			1  => __( 'Part updated.', 'wpbfpremium' ),
			2  => __( 'Custom field updated.' ), // do not touch
			3  => __( 'Custom field deleted.' ), // do not touch
			4  => __( 'Part updated.', 'wpbfpremium' ),
			5  => isset($_GET['revision']) ? sprintf( __( 'Part restored to revision from %s', 'wpbfpremium' ), wp_post_revision_title( ( int )$_GET['revision'], false ) ) : false,
			6  => __( 'Part published.', 'wpbfpremium' ),
			7  => __( 'Part saved.', 'wpbfpremium' ),
			8  => __( 'Part submitted.', 'wpbfpremium' ),
			9  => sprintf(
				__( 'Part scheduled for: <strong>%1$s</strong>.', 'wpbfpremium' ),
				date_i18n( __( 'M j, Y @ G:i', 'wpbfpremium' ), strtotime( $post->post_date ) )
			),
			10 => __( 'Part draft updated.', 'wpbfpremium' )
		);

		return $messages;
	}

	public function meta_box()
	{
		add_meta_box(
			'wpbf_hook_settings',
			__( 'Part Settings', 'wpbfpremium' ),
			[$this, 'meta_box_callback'],
			'wpbf_hooks'
		);

		add_meta_box( 'wpbf_hook_sidebar_settings', esc_html__( 'Locations', 'page-builder-framework' ), [$this, 'meta_box_sidebar_callback'], 'wpbf_hooks', 'side', 'default' );

	}

	public function hook_list()
	{
		return [
			__( 'General', 'wpbfpremium' )    => [
				'wpbf_body_open'
			],
			__( 'Pre Header', 'wpbfpremium' ) => [
				'wpbf_before_pre_header',
				'wpbf_pre_header_open',
				'wpbf_pre_header_close',
				'wpbf_pre_header_left_open',
				'wpbf_pre_header_left_close',
				'wpbf_pre_header_right_open',
				'wpbf_pre_header_right_close',
				'wpbf_after_pre_header',
			],
			__( 'Header', 'wpbfpremium' )     => [
				'wpbf_before_header',
				'wpbf_header_open',
				'wpbf_header_close',
				'wpbf_after_header'
			],
			__( 'Navigation', 'wpbfpremium' ) => [
				'wpbf_before_main_menu',
				'wpbf_main_menu_open',
				'wpbf_main_menu_close',
				'wpbf_after_main_menu',
				'wpbf_before_off_canvas_menu',
				'wpbf_after_off_canvas_menu',
				'wpbf_before_full_screen_menu',
				'wpbf_after_full_screen_menu',
			],
			__( 'Sidebar', 'wpbfpremium' )    => [
				'wpbf_before_sidebar',
				'wpbf_sidebar_open',
				'wpbf_after_sidebar',
				'wpbf_sidebar_close'
			],
			__( 'Footer', 'wpbfpremium' )     => [
				'wpbf_before_footer',
				'wpbf_footer_open',
				'wpbf_footer_close',
				'wpbf_after_footer'
			],
			__( 'Posts', 'wpbfpremium' )      => [
				'wpbf_before_article',
				'wpbf_before_article_meta',
				'wpbf_after_article_meta',
				'wpbf_before_comments',
				'wpbf_after_comments',
				'wpbf_after_article'
			]
		];
	}

	public function frontend_show_hooks() {
		if ( ! isset( $_GET['wpbf_hooks'] ) ) return;

		$actions = array_reduce( $this->hook_list(), function ( $carry, $item ) {
			$carry = array_merge( $carry, $item );

			return $carry;
		}, [] );

		foreach ( $actions as $action ) {
			add_action( $action, function () use ( $action ) {
				echo '<div style="display: inline-block; font-family: Helvetica, Arial, sans-serif; padding: 8px; margin: 5px; line-height: 1; border-radius: 4px; font-size: 13px; font-weight: 700; color: #000; background: #f9e880;">'. $action .'</div>';
			});
		}
	}

	public function display_hooks( $wp_admin_bar ) {

		if( !current_user_can( 'edit_theme_options' ) || is_admin() ) return;

		global $wp;
		$current_url = home_url( add_query_arg( array(), $wp->request ) );

		if ( !isset( $_GET['wpbf_hooks'] ) ) {

			$args = array(
				'id' => 'wpbf-hooks',
				'title' => __( 'Display Theme Hooks', 'wpbfpremium' ),
				'href' => trailingslashit( $current_url ) . '?wpbf_hooks',
				'meta' => array(
					'target' => '_self',
					'class' => 'wpbf-hooks-inactive',
					'title' => __( 'Display Theme Hooks', 'wpbfpremium' )
				)
			);

		} else {

			$args = array(
				'id' => 'wpbf-hooks',
				'title' => __( 'Hide Theme Hooks', 'wpbfpremium' ),
				'href' => trailingslashit( $current_url ),
				'meta' => array(
					'target' => '_self',
					'class' => 'wpbf-hooks-active',
					'title' => __( 'Hide Theme Hooks', 'wpbfpremium' )
				)
			);

		}

		$wp_admin_bar->add_node( $args );

	}

	public function meta_box_callback( $post ) {
		wp_nonce_field( 'wpbf_hook_nonce', 'wpbf_hook_nonce' );

		$action   = get_post_meta( $post->ID, '_wpbf_hook_action', true );
		$priority = get_post_meta( $post->ID, '_wpbf_hook_priority', true );
		?>
		<table class="form-table">
			<tbody>
				<tr>
					<th>
						<label><?php esc_attr_e( 'Location', 'wpbfpremium' ); ?></label>
					</th>
					<td>
						<select name="wpbf_hook_action">
							<?php foreach ( $this->hook_list() as $optgroup => $hooks ) : ?>
								<optgroup label="<?php echo $optgroup; ?>">
									<?php foreach ( $hooks as $hook ) : ?>
										<option value="<?php echo $hook; ?>" <?php selected( $hook, $action ); ?>><?php echo $hook; ?></option>
									<?php endforeach; ?>
								</optgroup>
							<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<th>
						<label><?php esc_attr_e( 'Priority', 'wpbfpremium' ); ?></label>
					</th>
					<td>
						<input type="text" placeholder="10" name="wpbf_hook_priority" value="<?php echo $priority; ?>">
					</td>
				</tr>
			</tbody>
		</table>

		<?php
	}

	public function meta_box_sidebar_callback() { ?>
			<a style="margin-top: 1em" target="_blank" href="<?= home_url( '?wpbf_hooks' ) ?>" class="button button-primary button-large">
				<?php _e( 'Display Locations', 'wpbfpremium' ); ?>
			</a>
		<?php
	}

	public function save_meta_box_data( $post_id )
	{
		if ( ! isset( $_POST['wpbf_hook_nonce'] ) ) {
			return;
		}

		if ( ! wp_verify_nonce( $_POST['wpbf_hook_nonce'], 'wpbf_hook_nonce' ) ) {
			return;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		if ( ! isset( $_POST['wpbf_hook_action'] ) ) {
			return;
		}

		$action   = sanitize_text_field( $_POST['wpbf_hook_action'] );
		$priority = sanitize_text_field( $_POST['wpbf_hook_priority'] );

		update_post_meta( $post_id, '_wpbf_hook_action', $action );
		update_post_meta( $post_id, '_wpbf_hook_priority', $priority );
	}

	/**
	 * Create our admin menu item.
	 *
	 * @since 1.7
	 */
	public function menu_item() {
		add_submenu_page(
			'themes.php',
			esc_html__( 'Parts', 'wpbfpremium' ),
			esc_html__( 'Parts', 'wpbfpremium' ),
			'manage_options',
			'edit.php?post_type=wpbf_hooks'
		);
	}

	public static function get_instance() {
		static $instance = null;

		if ( is_null( $instance ) ) {
			$instance = new self();
		}

		return $instance;
	}

	public function cpt_redirect() {
		if ( is_singular( 'wpbf_hooks' ) && ! current_user_can( 'edit_posts' ) ) {
			wp_redirect( site_url(), 301 );
			die;
		}
	}

}

HookSystem::get_instance();