<?php
/**
 * WooCommerce Integration
 *
 * @package Page Builder Framework Premium Addon
 * @subpackage Integration/WooCommerce
 */

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// WooCommerce Customizer Settings
require_once( WPBF_PREMIUM_DIR . 'inc/integration/woocommerce/wpbf-kirki-woocommerce.php' );

// WooCommerce Functions
require_once( WPBF_PREMIUM_DIR . 'inc/integration/woocommerce/woocommerce-functions.php' );

// WooCommerce Quickview
require_once( WPBF_PREMIUM_DIR . 'inc/integration/woocommerce/woocommerce-quick-view.php' );

// WooCommerce Customizer Styles
require_once( WPBF_PREMIUM_DIR . 'inc/integration/woocommerce/woocommerce-styles.php' );

// Styles & Scripts
add_action( 'wp_enqueue_scripts', 'wpbf_premium_woocommerce_scripts', 11 );

function wpbf_premium_woocommerce_scripts() {

	wp_enqueue_style( 'wpbf-premium-woocommerce', WPBF_PREMIUM_URI . 'css/wpbf-premium-woocommerce.css', '', WPBF_PREMIUM_VERSION );

	wp_enqueue_script( 'wpbf-premium-woocommerce', WPBF_PREMIUM_URI . 'js/wpbf-premium-woocommerce.js', array( 'jquery' ), WPBF_PREMIUM_VERSION, true );

	wp_localize_script( 'wpbf-premium-woocommerce', 'wpbf_woo_quick_view', array(
		'ajax_url'        => admin_url( 'admin-ajax.php' ),
		'cart_error_text' => __( 'Please select %variationName%', 'wpbf' )
	) );

}