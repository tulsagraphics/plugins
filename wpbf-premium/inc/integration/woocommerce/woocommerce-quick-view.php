<?php

namespace WPBF\WooCommerce\Quickview;

use WC_Product;
use WC_Product_Data_Store_CPT;

class QuickView {

	public function __construct() {

		add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'add_button' ), 11 );

		add_action( 'wp_ajax_wpbf_woo_quick_view', array( $this, 'ajax_handler_view' ) );
		add_action( 'wp_ajax_nopriv_wpbf_woo_quick_view', array( $this, 'ajax_handler_view' ) );

		add_action( 'wp_ajax_wpbf_woo_quick_view_add_to_cart', array( $this, 'add_to_cart' ) );
		add_action( 'wp_ajax_nopriv_wpbf_woo_quick_view_add_to_cart', array( $this, 'add_to_cart' ) );
	}

	public function add_to_cart() {

		try {
			$product_id   = absint( $_POST['product_id'] );
			$is_variation = sanitize_text_field( $_POST['is_variation'] );
			$quantity     = absint( $_POST['quantity'] );

			if ( $is_variation === 'true' ) {
				$variations   = $_POST['variations'];
				$variation_id = $this->find_matching_product_variation_id( $product_id, $variations );

				WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variations );
			} else {

				WC()->cart->add_to_cart( $product_id, $quantity );
			}
		} catch ( \Exception $e ) {
			// do nothing if there is an uncaught exception.
		}
		exit;
	}

	/*
	* Find matching product variation
	*
	* @param int $product_id
	* @param array $attributes
	* @return int Matching variation ID or 0.
	*/
	function find_matching_product_variation_id( $product_id, $attributes ) {

		return ( new \WC_Product_Data_Store_CPT() )->find_matching_product_variation(
			new \WC_Product( $product_id ),
			$attributes
		);
	}

	public function ajax_handler_view() {

		if ( ! isset( $_POST['product_id'] ) ) {
			exit;
		}

		$product_id = absint( $_POST['product_id'] );

		// wp_query for the product.
		wp( 'p=' . $product_id . '&post_type=product' );

		while ( have_posts() ) : the_post(); ?>

            <div class="wpbf-woo-quick-view-modal-content">
                <div id="product-<?php the_ID(); ?>" <?php post_class( 'product' ); ?>>
					<?php $this->feature_image(); ?>
                    <div class="summary entry-summary">
                        <div class="summary-content">
							<?php $this->quickview_content(); ?>
                        </div>
                    </div>
                </div>
            </div>

		<?php endwhile;

		exit;
	}

	public function feature_image() {

		/** @var \WP_Post $post */
		global $post;

		if ( has_post_thumbnail() ) {
			$props = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
			$image = get_the_post_thumbnail(
				$post->ID, 'shop_single', array(
					'title' => $props['title'],
					'alt'   => $props['alt'],
				)
			);
			echo
			sprintf(
				'<div class="%s">%s</div>',
				'images',
				$image
			);
		} else {
			echo sprintf( '<div class="images"><img src="%s" alt="%s" /></div>', wc_placeholder_img_src(), __( 'Placeholder', 'wpbfpremium' ) );
		}
	}

	public function quickview_content() {
		// Title
		woocommerce_template_single_title();

		// Rating
		woocommerce_template_single_rating();

		// Price
		woocommerce_template_single_price();

		// Excerpt
		woocommerce_template_single_excerpt();

		// Quantity & Add to cart button
		woocommerce_template_single_add_to_cart();

		// Meta
		woocommerce_template_single_meta();
	}

	public function add_button() {

		if ( get_theme_mod( 'woocommerce_loop_quick_view' ) == 'disabled' ) return;

		/** @var \WC_Product $product */
		global $product;

		echo '<a href="#" id="product_id_' . $product->get_id() . '" class="wpbf-woo-quick-view" data-product_id="' . $product->get_id() . '">' . esc_attr( apply_filters( 'wpbf_woo_quick_view_label', __( 'Quick View', 'wpbfpremium' ) ) ) . '</a>';
	}

	public static function get_instance() {
		static $instance = null;

		if ( is_null( $instance ) ) {
			$instance = new self();
		}

		return $instance;
	}
}


QuickView::get_instance();