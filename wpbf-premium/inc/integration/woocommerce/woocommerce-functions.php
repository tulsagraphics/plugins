<?php
/**
 * WooCommerce Functions
 *
 * @package Page Builder Framework Premium Addon
 * @subpackage Integration/WooCommerce
 */

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Loop Image Flip

// add wpbf-woo-has-gallery post class
add_filter( 'post_class', 'wpbf_woo_loop_image_flip_post_class' );
function wpbf_woo_loop_image_flip_post_class( $classes ) {

	if( get_theme_mod( 'woocommerce_loop_image_flip' ) == 'disabled' ) return;

	global $product;
	if( 'product' == get_post_type() ) {

 		$attachment_ids = $product->get_gallery_image_ids();

		if ( $attachment_ids ) {
			$classes[] = 'wpbf-woo-has-gallery';
		}

	}

	return $classes;

}

// construct image flip
add_action( 'woocommerce_before_shop_loop_item_title', 'wpbf_woo_loop_image_flip_construct', 11 );
function wpbf_woo_loop_image_flip_construct() {

	if( get_theme_mod( 'woocommerce_loop_image_flip' ) == 'disabled' ) return;

	global $product, $woocommerce;

	$attachment_ids = $product->get_gallery_image_ids();

	if ( $attachment_ids ) {

		$attachment_ids = array_values( $attachment_ids );
		$secondary_image_id = $attachment_ids['0'];

		$secondary_image_alt = get_post_meta( $secondary_image_id, '_wp_attachment_image_alt', true );
		$secondary_image_title = get_the_title( $secondary_image_id );

		echo wp_get_attachment_image( $secondary_image_id, 'shop_catalog', '',
			array(
				'class' => 'attachment-woocommerce_thumbnail wp-post-image wp-post-image-secondary',
				'alt' => $secondary_image_alt,
				'title' => $secondary_image_title
			)
		);

	}

}

// Menu Item

// CSS Classes
function wpbf_woo_menu_item_class_children( $css_classes ) {

	if ( WC()->cart->get_cart() && get_theme_mod( 'woocommerce_menu_item_dropdown' ) !== 'hide' ) $css_classes .= ' menu-item-has-children';
	return $css_classes;

}
add_filter( 'wpbf_woo_menu_item_classes', 'wpbf_woo_menu_item_class_children' );

function wpbf_woo_menu_item_premium() {

	// vars
	$label = apply_filters( 'wpbf_woo_menu_item_label', __( 'Cart', 'wpbfpremium' ) );
	$cart_total = WC()->cart->get_cart_total();
	$separator = apply_filters( 'wpbf_woo_menu_item_separator', __( '-', 'wpbfpremium' ) );

	// construct
	$menu_item = '';

	if( get_theme_mod( 'woocommerce_menu_item_label' ) !== 'hide' ) $menu_item .= '<span class="wpbf-woo-menu-item-label">'. esc_html( $label ) .'</span>';
	if( get_theme_mod( 'woocommerce_menu_item_amount' ) !== 'hide' ) $menu_item .= '<span class="wpbf-woo-menu-item-total">' . wp_kses_data( $cart_total ) . '</span>';
	if( get_theme_mod( 'woocommerce_menu_item_amount' ) !== 'hide' ) $menu_item .= '<span class="wpbf-woo-menu-item-separator">'. esc_html( $separator ) .'</span>';

	return $menu_item;

}
add_filter( 'wpbf_woo_before_menu_item', 'wpbf_woo_menu_item_premium' );

function wpbf_woo_do_menu_item_dropdown() {

	// vars
	$cart_items = WC()->cart->get_cart();
	$checkout_url = wc_get_checkout_url();
	$cart_url = wc_get_cart_url();

	// construct
	$menu_item = '';

	if( $cart_items && get_theme_mod( 'woocommerce_menu_item_dropdown' ) !== 'hide' ) {

		$menu_item .= '<ul class="wpbf-woo-sub-menu">';
		$menu_item .= '<li>';

		$menu_item .= '<div class="wpbf-woo-sub-menu-table-wrap">';

			$menu_item .= '<table class="wpbf-table">';

			$menu_item .= '<thead>';

				$menu_item .= '<tr>';

				$menu_item .= '<th>'. __( 'Product/s', 'wpbfpremium' ) .'</th>';
				$menu_item .= '<th>'. __( 'Quantity', 'wpbfpremium' ) .'</th>';

				$menu_item .= '</tr>';

			$menu_item .= '</thead>';

				$menu_item .= '<tbody>';

				foreach( $cart_items as $cart_item => $values ) { 

					// vars
					$product = wc_get_product( $values['data']->get_id() ); 
					$item_name = $product->get_title();
					$quantity = $values['quantity'];
					$price = $product->get_price();
					$image = $product->get_image();
					$link = $product->get_permalink();

					$menu_item .= '<tr>';

						$menu_item .= '<td>';
						$menu_item .= '<a href="'. esc_url( $link ) .'">';
						$menu_item .= $image;
						$menu_item .= $item_name;
						$menu_item .= '</a>';
						$menu_item .= '</td>';

						$menu_item .= '<td>';
						$menu_item .= $quantity;
						$menu_item .= '</td>';

					$menu_item .= '</tr>';

				}

				$menu_item .= '</tbody>';

			$menu_item .= '</table>';

		$menu_item .= '</div>';

		$menu_item .= '<div class="wpbf-woo-sub-menu-summary-wrap">';

			$menu_item .= '<div>'. __( 'Subtotal', 'wpbfpremium' ) .'</div>';
			$menu_item .= '<div>'. WC()->cart->get_cart_subtotal() .'</div>';

		$menu_item .= '</div>';

		if( get_theme_mod( 'woocommerce_menu_item_dropdown_cart_button' ) !== 'hide' || get_theme_mod( 'woocommerce_menu_item_dropdown_checkout_button' ) !== 'hide' ) {

			$menu_item .= '<div class="wpbf-woo-sub-menu-button-wrap">';
				if( get_theme_mod( 'woocommerce_menu_item_dropdown_cart_button' ) !== 'hide' ) $menu_item .= '<a href="'. esc_url( $cart_url ) .'" class="wpbf-button">'. __( 'Cart', 'wpbfpremium' ) .'</a>';
				if( get_theme_mod( 'woocommerce_menu_item_dropdown_checkout_button' ) !== 'hide' ) $menu_item .= '<a href="'. esc_url( $checkout_url ) .'" class="wpbf-button wpbf-button-primary">'. __( 'Checkout', 'wpbfpremium' ) .'</a>';
			$menu_item .= '</div>';

		}

		$menu_item .= '</li>';
		$menu_item .= '</ul>';

	}

	return $menu_item;

}
add_filter( 'wpbf_woo_menu_item_dropdown', 'wpbf_woo_do_menu_item_dropdown' );