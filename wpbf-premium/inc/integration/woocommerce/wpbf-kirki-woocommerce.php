<?php
/**
 * kirki WooCommerce
 *
 * @package Page Builder Framework Premium Addon
 * @subpackage Integration/WooCommerce
 */

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'wpbf_kirki_premium', 'wpbf_kirki_premium_woocommerce', 20 );

function wpbf_kirki_premium_woocommerce() {

	/* Fields – Menu Item */

	// Menu Item Icon
	Kirki::add_field( 'wpbf', array(
		'type'				=>			'select',
		'settings'			=>			'woocommerce_menu_item_icon',
		'label'				=>			esc_attr__( 'Cart Icon', 'wpbfpremium' ),
		'section'			=>			'wpbf_woocommerce_menu_item_options',
		'default'			=>			'cart',
		'priority'			=>			0,
		'multiple'			=>			1,
		'choices'			=>			array(
			'cart'			=>			esc_attr__( 'Cart', 'wpbfpremium' ),
			'basket'		=>			esc_attr__( 'Basket', 'wpbfpremium' ),
			'bag'			=>			esc_attr__( 'Bag', 'wpbfpremium' ),
		),
	) );

	Kirki::add_field( 'wpbf', array(
		'type'				=>			'custom',
		'settings'			=>			'separator-56462',
		'section'			=>			'wpbf_woocommerce_menu_item_options',
		'default'			=>			'<hr style="border-top: 1px solid #ccc; border-bottom: 1px solid #f8f8f8">',
		'priority'			=>			0,
	) );

	// Menu Item Text
	Kirki::add_field( 'wpbf', array(
		'type'				=>			'select',
		'settings'			=>			'woocommerce_menu_item_label',
		'label'				=>			esc_attr__( '"Cart" Text', 'wpbfpremium' ),
		'section'			=>			'wpbf_woocommerce_menu_item_options',
		'default'			=>			'show',
		'priority'			=>			7,
		'multiple'			=>			1,
		'choices'			=>			array(
			'show'			=>			esc_attr__( 'Show', 'wpbfpremium' ),
			'hide'			=>			esc_attr__( 'Hide', 'wpbfpremium' ),
		)
	) );

	// Menu Item Amount
	Kirki::add_field( 'wpbf', array(
		'type'				=>			'select',
		'settings'			=>			'woocommerce_menu_item_amount',
		'label'				=>			esc_attr__( 'Amount', 'wpbfpremium' ),
		'section'			=>			'wpbf_woocommerce_menu_item_options',
		'default'			=>			'show',
		'priority'			=>			7,
		'multiple'			=>			1,
		'choices'			=>			array(
			'show'			=>			esc_attr__( 'Show', 'wpbfpremium' ),
			'hide'			=>			esc_attr__( 'Hide', 'wpbfpremium' ),
		)
	) );

	Kirki::add_field( 'wpbf', array(
		'type'				=>			'custom',
		'settings'			=>			'separator-11214',
		'section'			=>			'wpbf_woocommerce_menu_item_options',
		'default'			=>			'<hr style="border-top: 1px solid #ccc; border-bottom: 1px solid #f8f8f8">',
		'priority'			=>			8,
	) );

	// Menu Item Dropdown
	Kirki::add_field( 'wpbf', array(
		'type'				=>			'select',
		'settings'			=>			'woocommerce_menu_item_dropdown',
		'label'				=>			esc_attr__( 'Dropdown', 'wpbfpremium' ),
		'section'			=>			'wpbf_woocommerce_menu_item_options',
		'default'			=>			'show',
		'priority'			=>			8,
		'multiple'			=>			1,
		'choices'			=>			array(
			'show'			=>			esc_attr__( 'Show', 'wpbfpremium' ),
			'hide'			=>			esc_attr__( 'Hide', 'wpbfpremium' ),
		)
	) );

	// Cart Button
	Kirki::add_field( 'wpbf', array(
		'type'				=>			'select',
		'settings'			=>			'woocommerce_menu_item_dropdown_cart_button',
		'label'				=>			esc_attr__( 'Cart Button', 'wpbfpremium' ),
		'section'			=>			'wpbf_woocommerce_menu_item_options',
		'default'			=>			'show',
		'priority'			=>			8,
		'multiple'			=>			1,
		'choices'			=>			array(
			'show'			=>			esc_attr__( 'Show', 'wpbfpremium' ),
			'hide'			=>			esc_attr__( 'Hide', 'wpbfpremium' ),
		),
		'active_callback'	=>			array(
			array(
			'setting'		=>			'woocommerce_menu_item_dropdown',
			'operator'		=>			'!=',
			'value'			=>			'hide',
			),
		)
	) );

	// Checkout Button
	Kirki::add_field( 'wpbf', array(
		'type'				=>			'select',
		'settings'			=>			'woocommerce_menu_item_dropdown_checkout_button',
		'label'				=>			esc_attr__( 'Checkout Button', 'wpbfpremium' ),
		'section'			=>			'wpbf_woocommerce_menu_item_options',
		'default'			=>			'show',
		'priority'			=>			8,
		'multiple'			=>			1,
		'choices'			=>			array(
			'show'			=>			esc_attr__( 'Show', 'wpbfpremium' ),
			'hide'			=>			esc_attr__( 'Hide', 'wpbfpremium' ),
		),
		'active_callback'	=>			array(
			array(
			'setting'		=>			'woocommerce_menu_item_dropdown',
			'operator'		=>			'!=',
			'value'			=>			'hide',
			),
		)
	) );

	/* Fields – Shop & Archive Pages (Loop) */

	// Image Flip
	Kirki::add_field( 'wpbf', array(
		'type'				=>			'select',
		'settings'			=>			'woocommerce_loop_image_flip',
		'label'				=>			esc_attr__( 'Image Flip', 'wpbfpremium' ),
		'description'		=>			esc_attr__( 'Displays the first image of your product gallery (if available) when hovering over the product thumbnail.', 'wpbfpremium' ), 
		'section'			=>			'woocommerce_product_catalog',
		'default'			=>			'show',
		'priority'			=>			20,
		'choices'			=>			array(
			'enabled'		=>			esc_attr__( 'Enabled', 'wpbfpremium' ),
			'disabled'		=>			esc_attr__( 'Disabled', 'wpbfpremium' ),
		),
	) );

	// Separator
	Kirki::add_field( 'wpbf', array(
		'type'				=>			'custom',
		'settings'			=>			'separator-83905',
		'section'			=>			'woocommerce_product_catalog',
		'default'			=>			'<hr style="border-top: 1px solid #ccc; border-bottom: 1px solid #f8f8f8">',
		'priority'			=>			20,
	) );

	// Quick View
	Kirki::add_field( 'wpbf', array(
		'type'				=>			'select',
		'settings'			=>			'woocommerce_loop_quick_view',
		'label'				=>			esc_attr__( 'Quick View', 'wpbfpremium' ),
		'section'			=>			'woocommerce_product_catalog',
		'default'			=>			'show',
		'priority'			=>			20,
		'choices'			=>			array(
			'enabled'		=>			esc_attr__( 'Enabled', 'wpbfpremium' ),
			'disabled'		=>			esc_attr__( 'Disabled', 'wpbfpremium' ),
		),
	) );

	// Quick View Font Size
	Kirki::add_field( 'wpbf', array(
		'type'				=>			'dimension',
		'label'				=>			esc_attr__( 'Font Size', 'wpbfpremium' ),
		'settings'			=>			'woocommerce_loop_quick_view_font_size',
		'section'			=>			'woocommerce_product_catalog',
		'transport'			=>			'postMessage',
		'priority'			=>			20,
		'default'			=>			'14px',
		'active_callback'	=>			array(
			array(
			'setting'		=>			'woocommerce_loop_quick_view',
			'operator'		=>			'!=',
			'value'			=>			'disabled',
			),
		)
	) );

	// Quick View Font Color
	Kirki::add_field( 'wpbf', array(
		'type'				=>			'color',
		'settings'			=>			'woocommerce_loop_quick_view_font_color',
		'label'				=>			esc_attr__( 'Font Color', 'wpbfpremium' ),
		'section'			=>			'woocommerce_product_catalog',
		'transport'			=>			'postMessage',
		'default'			=>			'#ffffff',
		'priority'			=>			20,
		'choices'			=>			array(
			'alpha'			=>			true,
		),
		'active_callback'	=>			array(
			array(
			'setting'		=>			'woocommerce_loop_quick_view',
			'operator'		=>			'!=',
			'value'			=>			'disabled',
			),
		)
	) );

	// Quick View Background Color
	Kirki::add_field( 'wpbf', array(
		'type'				=>			'color',
		'settings'			=>			'woocommerce_loop_quick_view_background_color',
		'label'				=>			esc_attr__( 'Background Color', 'wpbfpremium' ),
		'section'			=>			'woocommerce_product_catalog',
		'transport'			=>			'postMessage',
		'default'			=>			'rgba(0,0,0,.7)',
		'priority'			=>			20,
		'choices'			=>			array(
			'alpha'			=>			true,
		),
		'active_callback'	=>			array(
			array(
			'setting'		=>			'woocommerce_loop_quick_view',
			'operator'		=>			'!=',
			'value'			=>			'disabled',
			),
		)
	) );

	// Quick View Overlay Color
	Kirki::add_field( 'wpbf', array(
		'type'				=>			'color',
		'settings'			=>			'woocommerce_loop_quick_view_overlay_color',
		'label'				=>			esc_attr__( 'Overlay Background Color', 'wpbfpremium' ),
		'section'			=>			'woocommerce_product_catalog',
		'transport'			=>			'postMessage',
		'default'			=>			'rgba(0,0,0,.8)',
		'priority'			=>			20,
		'choices'			=>			array(
			'alpha'			=>			true,
		),
		'active_callback'	=>			array(
			array(
			'setting'		=>			'woocommerce_loop_quick_view',
			'operator'		=>			'!=',
			'value'			=>			'disabled',
			),
		)
	) );

}