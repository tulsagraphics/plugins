<?php
/**
 * Dynamic WooCommerce CSS
 *
 * Holds Customizer WooCommerce CSS styles
 *
 * @package Page Builder Framework Premium Addon
 * @subpackage Integration/WooCommerce
 */

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'wpbf_after_customizer_css', 'wpbf_premium_do_woocommerce_customizer_css', 20 );
function wpbf_premium_do_woocommerce_customizer_css() { ?>

	<?php if( get_theme_mod( 'woocommerce_loop_quick_view_overlay_color' ) ) { ?>

		.wpbf-woo-quick-view-modal {
			background: <?php echo esc_attr( get_theme_mod( 'woocommerce_loop_quick_view_overlay_color' ) ) ?>;
		}

	<?php } ?>

	<?php if( get_theme_mod( 'woocommerce_loop_quick_view_font_size' ) || get_theme_mod( 'woocommerce_loop_quick_view_font_color' ) || get_theme_mod( 'woocommerce_loop_quick_view_background_color' )  ) { ?>

	.wpbf-woo-quick-view {

		<?php if( get_theme_mod( 'woocommerce_loop_quick_view_font_size' ) ) { ?>
		font-size: <?php echo esc_attr( get_theme_mod( 'woocommerce_loop_quick_view_font_size' ) ) ?>;
		<?php } ?>

		<?php if( get_theme_mod( 'woocommerce_loop_quick_view_font_color' ) ) { ?>
		color: <?php echo esc_attr( get_theme_mod( 'woocommerce_loop_quick_view_font_color' ) ) ?>;
		<?php } ?>

		<?php if( get_theme_mod( 'woocommerce_loop_quick_view_background_color' ) ) { ?>
		background-color: <?php echo esc_attr( get_theme_mod( 'woocommerce_loop_quick_view_background_color' ) ) ?>;
		<?php } ?>

	}

	<?php if( get_theme_mod( 'woocommerce_loop_quick_view_font_color' ) ) { ?>
	.wpbf-woo-quick-view:hover {
		color: <?php echo esc_attr( get_theme_mod( 'woocommerce_loop_quick_view_font_color' ) ) ?>;
	}
	<?php } ?>

	<?php } ?>

<?php }