<?php

namespace ForGravity;

use GFAddOn;
use GFAPI;
use GFCommon;
use GFFeedAddOn;
use GF_Fields;
use GFForms;
use GFFormsModel;
use GFFormSettings;

GFForms::include_feed_addon_framework();

/**
 * Easy Passthrough for Gravity Forms.
 *
 * @since     1.0
 * @author    ForGravity
 * @copyright Copyright (c) 2017, Travis Lopes
 */
class Easy_Passthrough extends GFFeedAddOn {

	/**
	 * Contains an instance of this class, if available.
	 *
	 * @since  1.0
	 * @access private
	 * @var    Easy_Passthrough $_instance If available, contains an instance of this class.
	 */
	private static $_instance = null;

	/**
	 * Defines the version of Easy Passthrough for Gravity Forms.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_version Contains the version, defined from easypassthrough.php
	 */
	protected $_version = FG_EASYPASSTHROUGH_VERSION;

	/**
	 * Defines the minimum Gravity Forms version required.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_min_gravityforms_version The minimum version required.
	 */
	protected $_min_gravityforms_version = '2.1';

	/**
	 * Defines the plugin slug.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_slug The slug used for this plugin.
	 */
	protected $_slug = 'forgravity-easypassthrough';

	/**
	 * Defines the main plugin file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_path The path to the main plugin file, relative to the plugins folder.
	 */
	protected $_path = 'forgravity-easypassthrough/easypassthrough.php';

	/**
	 * Defines the full path to this class file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_full_path The full path.
	 */
	protected $_full_path = __FILE__;

	/**
	 * Defines the URL where this Add-On can be found.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string The URL of the Add-On.
	 */
	protected $_url = 'http://forgravity.com/plugins/easy-passthrough/';

	/**
	 * Defines the title of this Add-On.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_title The title of the Add-On.
	 */
	protected $_title = 'Easy Passthrough for Gravity Forms';

	/**
	 * Defines the short title of the Add-On.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_short_title The short title.
	 */
	protected $_short_title = 'Easy Passthrough';

	/**
	 * Defines if feed ordering is supported.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    bool $_supports_feed_ordering Is feed ordering supported?
	 */
	protected $_supports_feed_ordering = true;

	/**
	 * Defines the capability needed to access the Add-On settings page.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_capabilities_settings_page The capability needed to access the Add-On settings page.
	 */
	protected $_capabilities_settings_page = 'forgravity_easypassthrough';

	/**
	 * Defines the capability needed to access the Add-On form settings page.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_capabilities_form_settings The capability needed to access the Add-On form settings page.
	 */
	protected $_capabilities_form_settings = 'forgravity_easypassthrough';

	/**
	 * Defines the capability needed to uninstall the Add-On.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_capabilities_uninstall The capability needed to uninstall the Add-On.
	 */
	protected $_capabilities_uninstall = 'forgravity_easypassthrough_uninstall';

	/**
	 * Defines the capabilities needed for Easy Passthrough.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    array $_capabilities The capabilities needed for the Add-On.
	 */
	protected $_capabilities = array( 'forgravity_easypassthrough', 'forgravity_easypassthrough_uninstall' );

	/**
	 * Stores the field values used during passthrough.
	 *
	 * @since  1.1.5
	 * @access protected
	 * @var    array $field_values Field values used during passthrough.
	 */
	protected $field_values = array();

	/**
	 * Stores the entry IDs (and their form IDs) used during passthrough.
	 *
	 * @since  1.0.5
	 * @access protected
	 * @var    array $passed_through_entries Entry IDs (and their form IDs) used during passthrough.
	 */
	protected $passed_through_entries = array();

	/**
	 * Get instance of this class.
	 *
	 * @since  1.0
	 * @access public
	 * @static
	 *
	 * @return Easy_Passthrough $_instance
	 */
	public static function get_instance() {

		if ( null === self::$_instance ) {
			self::$_instance = new self;
		}

		return self::$_instance;

	}

	/**
	 * Register needed hooks.
	 *
	 * @since  1.1
	 * @access public
	 */
	public function pre_init() {

		parent::pre_init();

		$this->load_entry_token();

	}

	/**
	 * Register needed hooks.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function init() {

		parent::init();

		remove_filter( 'gform_entry_post_save', array( $this, 'maybe_process_feed' ) );

		add_action( 'gform_after_submission', array( $this, 'store_entry_id' ), 10, 2 );
		add_filter( 'gform_pre_render', array( $this, 'populate_fields' ) );

		add_filter( 'gform_admin_pre_render', array( $this, 'add_merge_tags' ) );
		add_action( 'gform_pre_replace_merge_tags', array( $this, 'replace_merge_tags' ), 6, 3 );

		add_filter( 'gform_' . $this->_slug . '_field_value', array( $this, 'override_field_value' ), 10, 4 );

		add_filter( 'auto_update_plugin', array( $this, 'maybe_auto_update' ), 10, 2 );

	}

	/**
	 * Register needed hooks.
	 *
	 * @since  1.2.1
	 * @access public
	 */
	public function init_admin() {

		parent::init_admin();

		// Members 2.0+ integration.
		if ( function_exists( 'members_register_cap_group' ) ) {
			remove_filter( 'members_get_capabilities', array( $this, 'members_get_capabilities' ) );
			add_action( 'members_register_cap_groups', array( $this, 'members_register_cap_group' ) );
			add_action( 'members_register_caps', array( $this, 'members_register_caps' ) );
		}

	}

	/**
	 * Register needed frontend hooks.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function init_frontend() {

		parent::init_frontend();

		/**
		 * Prevent session manager from initializing on page load.
		 *
		 * @since 1.1.8
		 *
		 * @param bool $bypass_session_init Bypass initializing session manager.
		 */
		$bypass_session_init = apply_filters( 'fg_easypassthrough_bypass_session_init', false );

		if ( ! $bypass_session_init ) {
			$this->session_manager();
		}

	}

	/**
	 * Enqueue needed scripts.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @return array
	 */
	public function scripts() {

		$scripts = array(
			array(
				'handle'    => $this->_slug . '_vendor_cookie',
				'src'       => $this->get_base_url() . '/js/vendor/js.cookie.js',
				'version'   => $this->_version,
				'deps'      => array( 'jquery' ),
				'in_footer' => false,
				'enqueue'   => array( '__return_true' ),
			),
		);

		return array_merge( parent::scripts(), $scripts );

	}

	/**
	 * Enqueue needed stylesheets.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   GFAddOn::get_base_url()
	 * @uses   GFAddOn::get_slug()
	 * @uses   GFAddOn::get_version()
	 *
	 * @return array
	 */
	public function styles() {

		$styles = array(
			array(
				'handle'  => $this->get_slug() . '_feed_settings',
				'src'     => $this->get_base_url() . '/css/feed_settings.css',
				'version' => $this->get_version(),
				'enqueue' => array(
					array(
						'admin_page' => array( 'form_settings' ),
						'tab'        => $this->_slug,
					),
				),
			),
			array(
				'handle'  => 'forgravity_dashicons',
				'src'     => $this->get_base_url() . '/css/dashicons.css',
				'version' => $this->get_version(),
				'enqueue' => array(
					array( 'query' => 'page=roles&action=edit' ),
				),
			),
		);

		return array_merge( parent::styles(), $styles );

	}





	// # FEED SETTINGS -------------------------------------------------------------------------------------------------

	/**
	 * Setup fields for feed settings.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   Easy_Passthrough::get_field_map()
	 * @uses   Easy_Passthrough::get_forms_as_choices()
	 * @uses   Easy_Passthrough::get_meta_map()
	 *
	 * @return array
	 */
	public function feed_settings_fields() {

		return array(
			array(
				'fields' => array(
					array(
						'name'     => 'sourceForm',
						'label'    => esc_html__( 'Source Form', 'forgravity_easypassthrough' ),
						'type'     => 'select',
						'required' => true,
						'onchange' => "jQuery( this ).parents( 'form' ).submit()",
						'choices'  => $this->get_forms_as_choices(),
						'tooltip'  => sprintf(
							'<h6>%s</h6>%s',
							esc_html__( 'Source Form', 'forgravity_easypassthrough' ),
							esc_html__( 'Select which form you want to populate this form form.', 'forgravity_easypassthrough' )
						),
					),
					array(
						'name'       => 'fieldMap',
						'label'      => esc_html__( 'Map Fields', 'forgravity_easypassthrough' ),
						'type'       => 'field_map',
						'field_map'  => $this->get_field_map(),
						'dependency' => 'sourceForm',
						'tooltip'    => sprintf(
							'<h6>%s</h6>%s',
							esc_html__( 'Map Fields', 'forgravity_easypassthrough' ),
							esc_html__( 'Select which fields on this form should be populated from fields on the source form.', 'forgravity_easypassthrough' )
						),
					),
					array(
						'name'           => 'metaMap',
						'label'          => esc_html__( 'Map Meta', 'forgravity_easypassthrough' ),
						'type'           => 'dynamic_field_map',
						'field_map'      => $this->get_meta_map(),
						'disable_custom' => true,
						'dependency'     => 'sourceForm',
						'tooltip'        => sprintf(
							'<h6>%s</h6>%s',
							esc_html__( 'Map Fields', 'forgravity_easypassthrough' ),
							esc_html__( 'Select which fields on this form should be populated from entry meta data on the source form.', 'forgravity_easypassthrough' )
						),
					),
					array(
						'name'       => 'options',
						'label'      => esc_html__( 'Options', 'forgravity_easypassthrough' ),
						'type'       => 'checkbox',
						'dependency' => 'sourceForm',
						'choices'    => array(
							array(
								'name'    => 'userPassthrough',
								'label'   => esc_html__( "Use logged in user's last submitted entry", 'forgravity_easypassthrough' ),
								'tooltip' => esc_html__( 'If user is logged in and they have submitted an entry to the source form, submitted entry will be used instead of entry in session.', 'forgravity_easypassthrough' ),
							),
						),
					),
					array(
						'name'           => 'passthroughCondition',
						'type'           => 'feed_condition',
						'label'          => esc_html__( 'Conditional Logic', 'forgravity_easypassthrough' ),
						'dependency'     => 'sourceForm',
						'checkbox_label' => esc_html__( 'Enable', 'forgravity_easypassthrough' ),
						'instructions'   => esc_html__( 'Passthrough form entry if', 'forgravity_easypassthrough' ),
					),
					array(
						'type'     => 'save',
						'messages' => array(
							'error'   => esc_html__( 'There was an error while saving the Easy Passthrough settings. Please review the errors below and try again.', 'forgravity_easypassthrough' ),
							'success' => esc_html__( 'Entry Passthrough settings updated.', 'forgravity_easypassthrough' ),
						),
					),
				),
			),
		);

	}

	/**
	 * Define the title for the feed settings page.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @return string
	 */
	public function feed_settings_title() {

		return esc_html__( 'Easy Passthrough Settings', 'forgravity_easypassthrough' );

	}

	/**
	 * Prepare forms for settings field.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   GFAddOn::get_current_form()
	 * @uses   GFAPI::get_forms()
	 *
	 * @return array
	 */
	public function get_forms_as_choices() {

		// Initialize choices array.
		$choices = array(
			array(
				'label' => esc_html__( 'Select a Form', 'forgravity_easypassthrough' ),
				'value' => '',
			),
		);

		// Get current form.
		$current_form = $this->get_current_form();

		// Get all forms.
		$forms = GFAPI::get_forms();

		// Loop through forms.
		foreach ( $forms as $form ) {

			/**
			 * Allow form to be populated from itself.
			 *
			 * @since 1.0.2
			 *
			 * @param bool $allow_same_form Allow form to be populated from itself.
			 */
			$allow_same_form = apply_filters( 'fg_easypassthrough_populate_same_form', true );

			// If form is the current form, skip it.
			if ( $form['id'] === $current_form['id'] && ! $allow_same_form ) {
				continue;
			}

			// Add form as choice.
			$choices[] = array(
				'label' => esc_html( $form['title'] ),
				'value' => esc_attr( $form['id'] ),
			);

		}

		return $choices;

	}

	/**
	 * Prepare field map for settings field.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   GFAddOn::get_setting()
	 * @uses   GFAPI::get_form()
	 * @uses   GFCommon::get_label()
	 * @uses   GF_Field::get_entry_inputs()
	 *
	 * @return array
	 */
	public function get_field_map() {

		// Initialize field map.
		$field_map = array();

		// Get source form ID.
		$source_form = $this->get_setting( 'sourceForm' );

		// If source form is not set, return field map.
		if ( ! $source_form ) {
			return $field_map;
		}

		// Get source form.
		$source_form = GFAPI::get_form( $source_form );

		// Loop through source form fields.
		foreach ( $source_form['fields'] as $field ) {

			// Get input type.
			$input_type = $field->get_input_type();

			// Get field inputs.
			$inputs = $field->get_entry_inputs();

			// If field has inputs, add each input to field map.
			if ( $inputs ) {

				// Loop through inputs.
				foreach ( $inputs as $input ) {

					// Replace period in input ID.
					$name = str_replace( '.', '_', $input['id'] );

					// Add input to field map.
					$field_map[] = array(
						'name'  => esc_attr( $name ),
						'label' => GFCommon::get_label( $field, $input['id'] ),
					);

				}

			} else if ( 'list' === $input_type && $field->enableColumns ) {

				// Define initial column index.
				$column_index = 0;

				// Loop through columns.
				foreach ( $field->choices as $column ) {

					// Add column to field map.
					$field_map[] = array(
						'name'  => esc_attr( $field->id . '_' . $column_index ),
						'label' => GFCommon::get_label( $field ) . ' (' . esc_html( rgar( $column, 'text' ) ) . ')',
					);

					// Increase column index.
					$column_index++;

				}

			} else {

				// Add field to field map.
				$field_map[] = array(
					'name'  => esc_attr( $field['id'] ),
					'label' => esc_html( $field['label'] ),
				);

			}

		}

		return $field_map;

	}

	/**
	 * Prepare meta map for settings field.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   GFAddOn::get_setting()
	 * @uses   GFFormsModel::get_entry_meta()
	 *
	 * @return array
	 */
	public function get_meta_map() {

		// Get source form ID.
		$source_form = $this->get_setting( 'sourceForm' );

		// If source form is not set, return meta map.
		if ( ! $source_form ) {
			return array();
		}

		// Initialize meta map.
		$meta_map = array(
			array(
				'value' => '',
				'label' => esc_html__( 'Select a Meta Key', 'forgravity_easypassthrough' ),
			),
			array(
				'value' => 'id',
				'label' => esc_html__( 'Entry ID', 'forgravity_easypassthrough' ),
			),
			array(
				'value' => 'date_created',
				'label' => esc_html__( 'Entry Date', 'forgravity_easypassthrough' ),
			),
			array(
				'value' => 'ip',
				'label' => esc_html__( 'User IP', 'forgravity_easypassthrough' ),
			),
			array(
				'value' => 'source_url',
				'label' => esc_html__( 'Source Url', 'forgravity_easypassthrough' ),
			),
			array(
				'value' => 'form_title',
				'label' => esc_html__( 'Form Title', 'forgravity_easypassthrough' ),
			),
			array(
				'label'   => esc_html__( 'Payment Meta', 'forgravity_easypassthrough' ),
				'choices' => array(
					array(
						'value' => 'payment_status',
						'label' => esc_html__( 'Payment Status', 'forgravity_easypassthrough' ),
					),
					array(
						'value' => 'transaction_id',
						'label' => esc_html__( 'Transaction Id', 'forgravity_easypassthrough' ),
					),
					array(
						'value' => 'payment_date',
						'label' => esc_html__( 'Payment Date', 'forgravity_easypassthrough' ),
					),
					array(
						'value' => 'payment_amount',
						'label' => esc_html__( 'Payment Amount', 'forgravity_easypassthrough' ),
					),
					array(
						'value' => 'payment_gateway',
						'label' => esc_html__( 'Payment Amount', 'forgravity_easypassthrough' ),
					),
				),
			),
		);

		// Get entry meta fields for form.
		$form_meta = GFFormsModel::get_entry_meta( $source_form );

		// Add entry meta to meta map.
		foreach ( $form_meta as $meta_key => $meta ) {
			$meta_map[] = array(
				'value' => $meta_key,
				'label' => rgars( $form_meta, "{$meta_key}/label" ),
			);
		}

		return $meta_map;

	}

	/**
	 * Heading row for field map table.
	 *
	 * @since  2.2
	 * @access public
	 *
	 * @uses   GFAddOn::field_map_title()
	 *
	 * @return string
	 */
	public function field_map_table_header() {

		return '<thead>
					<tr>
						<th>' . esc_html__( 'Source Form Field', 'forgravity_easypassthrough' ) . '</th>
						<th>' . esc_html__( 'Target Form Field', 'forgravity_easypassthrough' ) . '</th>
					</tr>
				</thead>';

	}

	/**
	 * Renders the form settings page.
	 * Forked to set Javascript form variable to source form.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function form_settings_page() {

		GFFormSettings::page_header( $this->_title );
		?>
        <div class="gform_panel gform_panel_form_settings" id="form_settings">

			<?php
			$form = $this->get_current_form();

			$form_id = $form['id'];
			$form    = gf_apply_filters( array( 'gform_admin_pre_render', $form_id ), $form );

			if ( $this->method_is_overridden( 'form_settings' ) ) {

				//enables plugins to override settings page by implementing a form_settings() function
				$this->form_settings( $form );
			} else {

				//saves form settings if save button was pressed
				$this->maybe_save_form_settings( $form );

				//reads current form settings
				$settings = $this->get_form_settings( $form );
				$this->set_settings( $settings );

				//reading addon fields
				$sections = $this->form_settings_fields( $form );

				GFCommon::display_admin_message();

				$page_title = $this->form_settings_page_title();
				if ( empty( $page_title ) ) {
					$page_title = rgar( $sections[0], 'title' );

					//using first section title as page title, so disable section title
					$sections[0]['title'] = false;
				}
				$icon = $this->form_settings_icon();
				if ( empty( $icon ) ) {
					$icon = '<i class="fa fa-cogs"></i>';
				}

				?>
                <h3><span><?php echo $icon ?><?php echo $page_title ?></span></h3>
				<?php

				//rendering settings based on fields and current settings
				$this->render_settings( $sections );
			}
			?>

            <script type="text/javascript">
				var form = <?php echo json_encode( GFAPI::get_form( $this->get_setting( 'sourceForm' ) ) ) ?>;
            </script>
        </div>
		<?php
		GFFormSettings::page_footer();
	}





	// # FEED LIST -----------------------------------------------------------------------------------------------------

	/**
	 * Define the title for the feed list page.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   GFAddOn::get_short_title()
	 * @uses   GFFeedAddOn::can_create_feed()
	 *
	 * @return string
	 */
	public function feed_list_title() {

		// If feed creation is disabled, display title without Add New button.
		if ( ! $this->can_create_feed() ) {
			return sprintf(
				esc_html__( '%s Configurations', 'forgravity_easypassthrough' ),
				$this->get_short_title()
			);
		}

		// Prepare add new feed URL.
		$url = add_query_arg( array( 'fid' => '0' ) );
		$url = esc_url( $url );

		// Display feed list title with Add New button.
		return sprintf(
			'%s <a class="add-new-h2" href="%s">%s</a>',
			sprintf(
				esc_html__( '%s Configurations', 'forgravity_easypassthrough' ),
				$this->get_short_title()
			),
			$url,
			esc_html__( 'Add New', 'gravityforms' )
		);

	}

	/**
	 * Setup columns for feed list table.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @return array
	 */
	public function feed_list_columns() {

		return array(
			'sourceForm' => esc_html__( 'Source Form', 'forgravity_easypassthrough' ),
		);

	}

	/**
	 * Prepare Source Form column value for feed list table.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $feed Current feed.
	 *
	 * @uses   GFAPI::get_form()
	 *
	 * @return string
	 */
	public function get_column_value_sourceForm( $feed ) {

		// Get form.
		$form = GFAPI::get_form( $feed['meta']['sourceForm'] );

		// If form was not found, return.
		if ( ! $form ) {
			return $feed['meta']['sourceForm'];
		}

		return esc_html( $form['title'] );

	}





	// # PAGE LOAD -----------------------------------------------------------------------------------------------------

	/**
	 * Load entry ID into session if token is set.
	 *
	 * @since  1.1
	 * @access public
	 *
	 * @uses   Easy_Passthrough::get_entry_for_token()
	 * @uses   Easy_Passthrough::store_entry_id()
	 * @uses   GFAPI::get_form()
	 */
	public function load_entry_token() {

		// If token is not set, return.
		if ( ! rgget( 'ep_token' ) ) {
			return;
		}

		// Get token.
		$token = sanitize_text_field( rgget( 'ep_token' ) );

		// Get entry for token.
		$entry = $this->get_entry_for_token( $token );

		// If no entry was found, return.
		if ( ! $entry ) {
			return;
		}

		// Get form.
		$form = GFAPI::get_form( $entry['form_id'] );

		// Store entry ID.
		$this->store_entry_id( $entry, $form );

	}





	// # FORM SUBMISSION -----------------------------------------------------------------------------------------------

	/**
	 * Store entry ID to session.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $entry The entry that was just created.
	 * @param array $form  The current form.
	 */
	public function store_entry_id( $entry, $form ) {

		// Get session manager.
		$session = $this->session_manager();

		// Store entry ID to session.
		$session[ $this->_slug . '_' . $form['id'] ] = $entry['id'];

	}





	// # FORM RENDER ---------------------------------------------------------------------------------------------------

	/**
	 * Populate fields via Easy Passthrough.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $form The current Form object.
	 *
	 * @uses   GFAPI::get_form()
	 * @uses   GF_Easy_Passthrough::get_field_values()
	 *
	 * @return array
	 */
	public function populate_fields( $form ) {

		// If no form ID is set, return.
		if ( ! rgar( $form, 'id' ) ) {
			return $form;
		}

		// If GravityView is in edit context, return.
		if ( function_exists( 'gravityview_get_context' ) && 'edit' === gravityview_get_context() ) {
			return $form;
		}

		// Get field values.
		$field_values = $this->get_field_values( $form['id'] );

		// If no field values were found, return.
		if ( ! $field_values ) {

			/**
			 * Modify form object after Easy Passthrough has been applied.
			 *
			 * @since 1.0.5
			 *
			 * @param array $form                   The current form object.
			 * @param array $field_values           The prepared field values.
			 * @param array $passed_through_entries The entry IDs used for passthrough and their form IDs.
			 */
			$form = gf_apply_filters( array(
				'fg_easypassthrough_form',
				$form['id'],
			), $form, $this->field_values, $this->passed_through_entries );

			return $form;

		}

		// Loop through form fields.
		foreach ( $form['fields'] as &$field ) {

			// Get field input type.
			$input_type = $field->get_input_type();

			// Get field inputs.
			$inputs = $field->get_entry_inputs();

			// If field has inputs, populate them separately.
			if ( $inputs ) {

				// Handle checkbox field.
				if ( 'checkbox' === $input_type ) {

					// Loop through inputs.
					foreach ( $field->choices as $i => &$choice ) {

						// Get choice input ID.
						$input_id = $field->inputs[ $i ]['id'];

						// Get input value.
						$input_value = rgar( $field_values, $input_id );

						// Get product name.
						if ( strpos( $input_value, '|' ) !== false ) {
							$input_value = implode( '|', explode( '|', $input_value, -1 ) );
						}

						// If input value matches choice value, select checkbox.
						if ( $input_value === $choice['value'] ) {
							$choice['isSelected'] = true;
						}

					}

				} else {

					// Loop through inputs.
					foreach ( $field->inputs as &$input ) {

						// Get input ID.
						$input_id = strval( $input['id'] );

						// Get input value.
						$input_value = rgar( $field_values, $input_id );

						// If input value is set, change input default value.
						if ( ! rgblank( $input_value ) ) {
							$input['defaultValue'] = $input_value;
						}

					}

				}

			} else if ( 'list' === $input_type ) {

				// Get field value.
				$field_value = rgar( $field_values, $field->id );

				// Add field value based on column state.
				if ( $field->enableColumns ) {

					// Initialize field value array.
					$field_value = array();

					// Loop through field columns.
					foreach ( $field->choices as $index => $column ) {

						// Get column value.
						$column_value = rgar( $field_values, $field->id . '.' . $index );

						// If column value was not found, set to array.
						if ( empty( $column_value ) ) {
							$column_value = array( array( 'text' => '' ) );
						}

						// Loop through column value.
						foreach ( $column_value as $row_index => $row ) {

							// Get row value.
							$row_value = end( $row );

							// If row was not found, set to array.
							if ( ! isset( $field_value[ $row_index ] ) ) {
								$field_value[ $row_index ] = array();
							}

							$field_value[ $row_index ] = array_merge( $field_value[ $row_index ], array( $column['text'] => $row_value ) );

						}

					}

				} else {

					// If field value is not an array, convert to array.
					if ( ! is_array( $field_value ) ) {
						$field_value = array( $field_value );
					}

					// Get array values.
					$field_value = array_values( $field_value );

				}

				// Set field value.
				$field->defaultValue = serialize( $field_value );

			} else if ( 'date' === $field->type ) {

				// Get field value.
				$field_value = rgar( $field_values, $field->id );

				// If field value is empty, skip.
				if ( rgblank( $field_value ) ) {
					continue;
				}

				// If this is a datepicker field, change field default value.
				if ( 'datepicker' === $field->dateType ) {
					$field->defaultValue = $field_value;
					continue;
				}

				// Convert string to time.
				$timestamp = strtotime( $field_value );

				// If string could not be converted, exit.
				if ( ! $timestamp ) {
					continue;
				}

				// Populate month, day and year.
				$field->inputs[0]['defaultValue'] = 'datedropdown' == $field->dateType ? date( 'n', $timestamp ) : date( 'm', $timestamp );
				$field->inputs[1]['defaultValue'] = 'datedropdown' == $field->dateType ? date( 'j', $timestamp ) : date( 'd', $timestamp );
				$field->inputs[2]['defaultValue'] = date( 'Y', $timestamp );

			} else if ( 'time' === $field->type ) {

				// Get field value.
				$field_value = rgar( $field_values, $field->id );

				// If field value is empty, skip.
				if ( rgblank( $field_value ) ) {
					continue;
				}

				// Convert string to time.
				$timestamp = strtotime( $field_value );

				// If string could not be converted, exit.
				if ( ! $timestamp ) {
					continue;
				}

				// Populate field based on time format.
				if ( '12' == $field->timeFormat ) {

					$field->inputs[0]['defaultValue'] = date( 'h', $timestamp );
					$field->inputs[1]['defaultValue'] = date( 'i', $timestamp );
					$field->inputs[2]['defaultValue'] = date( 'a', $timestamp );

				} else {

					$field->inputs[0]['defaultValue'] = date( 'H', $timestamp );
					$field->inputs[1]['defaultValue'] = date( 'i', $timestamp );

				}

			} else {

				// Get field value.
				$field_value = rgar( $field_values, $field->id );

				// If field value is set, change field default value.
				if ( ! rgblank( $field_value ) ) {
					$field->defaultValue = $field_value;
				}

			}

		}

		/**
		 * Modify form object after Easy Passthrough has been applied.
		 *
		 * @since 1.0.5
		 *
		 * @param array $form                   The current form object.
		 * @param array $field_values           The prepared field values.
		 * @param array $passed_through_entries The entry IDs used for passthrough and their form IDs.
		 */
		$form = gf_apply_filters( array(
			'fg_easypassthrough_form',
			$form['id'],
		), $form, $this->field_values, $this->passed_through_entries );

		return $form;

	}

	/**
	 * Get field values for entry based on field dynamic parameter name.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param int $form_id Current form ID.
	 *
	 * @uses   GFAddOn::get_field_map_fields()
	 *
	 * @return array|bool
	 */
	public function get_field_values( $form_id = null ) {

		// If no form ID is set, return an empty array.
		if ( rgblank( $form_id ) ) {
			return array();
		}

		// Get session manager.
		$session = $this->session_manager();

		// If field value have already been prepared, return.
		if ( isset( $this->field_values[ $form_id ] ) ) {
			return $this->field_values[ $form_id ];
		}

		// Get target form.
		$target_form = GFAPI::get_form( $form_id );

		// Get Easy Passthrough feeds for form.
		$feeds = $this->get_feeds( $form_id );

		// If no results were found, return false.
		if ( empty( $feeds ) ) {

			// Set field values to false.
			$this->field_values[ $form_id ] = false;

			return $this->field_values[ $form_id ];

		}

		// Log that feeds were found.
		$this->log_debug( __METHOD__ . '(): Easy Passthrough configurations found for form #' . $form_id . '. Beginning preparation of field values.' );

		// Initialize field values array.
		$this->field_values[ $form_id ] = array();

		// Initialize passed through entries array.
		$this->passed_through_entries = array();

		// Loop through feeds.
		foreach ( $feeds as $feed ) {

			// Get source form ID.
			$source_form = $feed['meta']['sourceForm'];

			// Get source form.
			$source_form = GFAPI::get_form( $source_form );

			// Get source entry ID.
			if ( rgars( $feed, 'meta/userPassthrough' ) && is_user_logged_in() ) {

				// Log that we are searching for user entry.
				$this->log_debug( __METHOD__ . '(): Looking for last entry submitted to form #' . $source_form . ' by user; fallback to session entry.' );

				// Get last submitted entry for form.
				$last_submitted_entry = GFAPI::get_entries( $source_form['id'], array(
					'field_filters' => array(
						array(
							'key'   => 'created_by',
							'value' => get_current_user_id(),
						),
					),
					'status'        => 'active',
				), array( 'key' => 'date_created', 'direction' => 'DESC' ), array( 'page_size' => 1 ) );

				// If an entry was found, use it.
				$source_entry_id = ( $last_submitted_entry && count( $last_submitted_entry ) == 1 ) ? $last_submitted_entry[0]['id'] : $session[ $this->get_slug() . '_' . $source_form['id'] ];

			} else {

				// Use entry ID from session.
				$source_entry_id = $session[ $this->get_slug() . '_' . $source_form['id'] ];

			}

			// If no entry exists for source form, skip.
			if ( rgblank( $source_entry_id ) ) {
				$this->log_debug( __METHOD__ . '(): No entry was found for source form #' . $form_id . ' in this session. Skipping.' );
				continue;
			}

			// Get source entry.
			$source_entry = GFAPI::get_entry( $source_entry_id );

			// If feed condition is not met, skip feed.
			if ( ! $this->is_feed_condition_met( $feed, $source_form, $source_entry ) ) {
				$this->log_debug( __METHOD__ . '(): Feed condition was not met for feed #' . $feed['id'] . '. Skipping.' );
				continue;
			}

			// Add entry to array.
			$this->passed_through_entries[] = array(
				'form_id'  => $source_form['id'],
				'entry_id' => $session[ $this->_slug . '_' . $source_form['id'] ],
			);

			// Get field map.
			$mapping = $this->get_field_map_fields( $feed, 'fieldMap' );

			// Loop through field mapping.
			foreach ( $mapping as $key => $value ) {

				// If key does not need to be converted, skip.
				if ( false === strpos( $key, '_' ) ) {
					continue;
				}

				// Convert key.
				$new_key = str_replace( '_', '.', $key );

				// Add to mapping.
				$mapping[ $new_key ] = $value;

				// Remove original mapping.
				unset( $mapping[ $key ] );

			}

			// Add meta map to mapping.
			$mapping += $this->get_dynamic_field_map_fields( $feed, 'metaMap' );
			$mapping = array_filter( $mapping );

			// Loop through mapping.
			foreach ( $mapping as $source_field_id => $target_field_id ) {

				// Get source field.
				$source_field      = GFFormsModel::get_field( $source_form, $source_field_id );
				$source_field_type = GFFormsModel::get_input_type( $source_field );

				// Get target field.
				$target_field = GFFormsModel::get_field( $target_form, $target_field_id );

				// Get list field value.
				if ( 'list' === $source_field_type && $source_field->enableColumns ) {

					// Get field value.
					$field_value = $this->get_field_value( $source_form, $source_entry, $source_field->id );

					// If field value is empty, skip it.
					if ( empty( $field_value ) ) {
						continue;
					}

					// Unserialize field value.
					$field_value = maybe_unserialize( $field_value );

					// Get column index.
					$column_index = explode( '.', $source_field_id );
					$column_index = end( $column_index );

					// Get column label.
					$column_label = $source_field->choices[ $column_index ]['text'];

					// Initialize column values array.
					$column_values = array();

					// Loop through field value and get column values.
					foreach ( $field_value as $row ) {

						// Add to column values.
						$column_values[] = array( $column_label => rgar( $row, $column_label ) );

					}

					// Initialize field value array.
					if ( ! isset( $this->field_values[ $target_field_id ] ) ) {
						$this->field_values[ $form_id ][ $target_field_id ] = array();
					}

					// Add field value to array.
					$this->field_values[ $form_id ][ $target_field_id ] = array_merge( $this->field_values[ $form_id ][ $target_field_id ], $column_values );

				} else {

					// Get field value.
					if ( 'signature' === $source_field_type || ( GFCommon::is_product_field( $source_field->type ) && GFCommon::is_product_field( $target_field->type ) ) ) {
						$field_value = rgar( $source_entry, $source_field_id );
					} else {
						$field_value = $this->get_field_value( $source_form, $source_entry, $source_field_id );
					}

					// Unserialize field value.
					$field_value = maybe_unserialize( $field_value );

					// Add field value to array.
					$this->field_values[ $form_id ][ $target_field_id ] = $field_value;

				}

			}

		}

		// Remove empty keys.
		unset( $this->field_values[ $form_id ][ null ] );

		// Log prepared field values.
		$this->log_debug( __METHOD__ . '(): Prepared field values for form #' . $form_id . ': ' . print_r( $this->field_values[ $form_id ], true ) );

		/**
		 * Modify generated field values.
		 *
		 * @since 1.0.5
		 *
		 * @param array $field_values The prepared field values.
		 * @param int   $form_id      The current form ID being prepared for Easy Passthrough.
		 */
		$this->field_values[ $form_id ] = gf_apply_filters( array(
			'fg_easypassthrough_field_values',
			$form_id,
		), $this->field_values[ $form_id ], $form_id );

		return $this->field_values[ $form_id ];

	}





	// # MERGE TAGS ----------------------------------------------------------------------------------------------------

	/**
	 * Add Easy Passthrough merge tags.
	 *
	 * @since  1.1
	 * @access public
	 *
	 * @param array $form The form object.
	 *
	 * @uses   Easy_Passthrough::has_easy_passthrough_feeds()
	 *
	 * @return array
	 */
	public function add_merge_tags( $form ) {

		// If this form does not have any Easy Passthrough feeds, return.
		if ( ! $this->has_easy_passthrough_feeds( $form['id'] ) ) {
			return $form;
		}

		// If the header has already been output, add merge tags script in the footer.
		if ( ! did_action( 'admin_head' ) ) {
			add_action( 'admin_footer', array( $this, 'add_merge_tags_footer' ) );

			return $form;
		}

		?>

        <script type="text/javascript">

			( function ( $ ) {

				if ( window.gform ) {

					gform.addFilter( 'gform_merge_tags', function ( mergeTags ) {

						mergeTags[ 'fg_easypassthrough' ] = {
							label: '<?php _e( 'Easy Passthrough', 'forgravity_fillablepdfs' ); ?>',
							tags:  [
								{
									tag:   '{Easy Passthrough Token}',
									label: '<?php _e( 'Easy Passthrough Token', 'forgravity_fillablepdfs' ); ?>'
								}
							]
						};

						return mergeTags;

					} );

				}

			} )( jQuery );

        </script>

		<?php
		return $form;

	}

	/**
	 * Add Easy Passthrough merge tags in admin footer.
	 *
	 * @since  1.1
	 * @access public
	 *
	 * @uses   Easy_Passthrough::add_merge_tags()
	 * @uses   GFAddOn::get_current_form()
	 */
	public function add_merge_tags_footer() {

		// Get current form.
		$form = $this->get_current_form();

		// If form was found, include merge tags script.
		if ( $form ) {
			$this->add_merge_tags( $form );
		}

	}

	/**
	 * Replace Easy Passthrough merge tags.
	 *
	 * @since  1.1
	 * @access public
	 *
	 * @param string $text  The current text in which merge tags are being replaced.
	 * @param array  $form  The current form.
	 * @param array  $entry The current entry.
	 *
	 * @return string
	 */
	public function replace_merge_tags( $text, $form, $entry ) {

		// If text does not contain any merge tags, return.
		if ( false === strpos( $text, '{' ) ) {
			return $text;
		}

		// Search for merge tags in text.
		preg_match_all( '/({Easy Passthrough Token})/mi', $text, $matches, PREG_SET_ORDER );

		// Loop through matches.
		foreach ( $matches as $match ) {

			// Get parts.
			$merge_tag = $match[0];

			// If this is not the Easy Passthrough merge tag, skip it.
			if ( strpos( strtolower( $merge_tag ), '{easy passthrough token' ) !== 0 ) {
				continue;
			}

			// Get token for entry.
			$token = $this->get_entry_token( $entry );

			// Replace merge tag.
			$text = str_replace( $merge_tag, $token, $text );

		}

		return $text;

	}





	// # HELPER METHODS ------------------------------------------------------------------------------------------------

	/**
	 * Get Easy Passthrough token for entry.
	 *
	 * @since  1.1.7
	 * @access public
	 *
	 * @param array|int $entry Entry object or ID.
	 *
	 * @return string|bool
	 */
	public function get_entry_token( $entry ) {

		// Get entry ID.
		$entry_id = is_numeric( $entry ) ? $entry : rgar( $entry, 'id' );

		// If entry ID is not provided, return.
		if ( ! $entry_id ) {
			return false;
		}

		// Get existing token for entry.
		$token = gform_get_meta( $entry_id, 'fg_easypassthrough_token' );

		// If token exists, return it.
		if ( $token ) {
			return $token;
		}

		// Generate token.
		$token = md5( uniqid() . time() . $entry['id'] );

		// Save token.
		gform_update_meta( $entry_id, 'fg_easypassthrough_token', $token );

		return $token;

	}

	/**
	 * Get entry using Easy Passthrough token.
	 *
	 * @since  1.1
	 * @access public
	 *
	 * @param string $token Easy Passthrough token.
	 *
	 * @uses   GFAPI::get_entry()
	 * @uses   GFFormsModel::get_lead_meta_table_name()
	 * @uses   wpdb::get_var()
	 * @uses   wpdb::prepare()
	 *
	 * @return array|null
	 */
	public function get_entry_for_token( $token ) {

		global $wpdb;

		// Get entry ID based on Gravity Forms database version.
		if ( version_compare( self::get_gravityforms_db_version(), '2.3-dev-1', '<' ) ) {

			// Get entry meta table name.
			$table_name = GFFormsModel::get_lead_meta_table_name();

			// Get entry ID.
			$entry_id = $wpdb->get_var(
				$wpdb->prepare(
					"SELECT lead_id FROM {$table_name} WHERE `meta_key` = '%s' AND `meta_value` = '%s'",
					'fg_easypassthrough_token',
					$token
				)
			);

		} else {

			// Get entry meta table name.
			$table_name = GFFormsModel::get_entry_meta_table_name();

			// Get entry ID.
			$entry_id = $wpdb->get_var(
				$wpdb->prepare(
					"SELECT entry_id FROM {$table_name} WHERE `meta_key` = '%s' AND `meta_value` = '%s'",
					'fg_easypassthrough_token',
					$token
				)
			);

		}

		// If entry ID was not found, return.
		if ( ! $entry_id ) {
			return null;
		}

		return GFAPI::get_entry( $entry_id );

	}

	/**
	 * Get field map choices for specific form.
	 * (Forked to remove meta fields.)
	 *
	 * @since  1.1.2
	 * @access public
	 *
	 * @uses   GFCommon::get_label()
	 * @uses   GFFormsModel::get_form_meta()
	 * @uses   GF_Field::get_entry_inputs()
	 * @uses   GF_Field::get_form_editor_field_title()
	 * @uses   GF_Field::get_input_type()
	 *
	 * @param int          $form_id             Form ID to display fields for.
	 * @param array|string $field_type          Field types to only include as choices. Defaults to null.
	 * @param array|string $exclude_field_types Field types to exclude from choices. Defaults to null.
	 *
	 * @return array
	 */
	public static function get_field_map_choices( $form_id, $field_type = null, $exclude_field_types = null ) {

		$form = GFFormsModel::get_form_meta( $form_id );

		$fields = array();

		// Setup first choice
		if ( rgblank( $field_type ) || ( is_array( $field_type ) && count( $field_type ) > 1 ) ) {

			$first_choice_label = __( 'Select a Field', 'gravityforms' );

		} else {

			$type = is_array( $field_type ) ? $field_type[0] : $field_type;
			$type = ucfirst( GF_Fields::get( $type )->get_form_editor_field_title() );

			$first_choice_label = sprintf( __( 'Select a %s Field', 'gravityforms' ), $type );

		}

		$fields[] = array( 'value' => '', 'label' => $first_choice_label );

		// Populate form fields
		if ( is_array( $form['fields'] ) ) {
			foreach ( $form['fields'] as $field ) {
				$input_type          = $field->get_input_type();
				$inputs              = $field->get_entry_inputs();
				$field_is_valid_type = ( empty( $field_type ) || ( is_array( $field_type ) && in_array( $input_type, $field_type ) ) || ( ! empty( $field_type ) && $input_type == $field_type ) );

				if ( is_null( $exclude_field_types ) ) {
					$exclude_field = false;
				} else if ( is_array( $exclude_field_types ) ) {
					if ( in_array( $input_type, $exclude_field_types ) ) {
						$exclude_field = true;
					} else {
						$exclude_field = false;
					}
				} else {
					//not array, so should be single string
					if ( $input_type == $exclude_field_types ) {
						$exclude_field = true;
					} else {
						$exclude_field = false;
					}
				}

				if ( is_array( $inputs ) && $field_is_valid_type && ! $exclude_field ) {
					//If this is an address field, add full name to the list
					if ( $input_type == 'address' ) {
						$fields[] = array(
							'value' => $field->id,
							'label' => GFCommon::get_label( $field ) . ' (' . esc_html__( 'Full', 'gravityforms' ) . ')',
						);
					}
					//If this is a name field, add full name to the list
					if ( $input_type == 'name' ) {
						$fields[] = array(
							'value' => $field->id,
							'label' => GFCommon::get_label( $field ) . ' (' . esc_html__( 'Full', 'gravityforms' ) . ')',
						);
					}
					//If this is a checkbox field, add to the list
					if ( $input_type == 'checkbox' ) {
						$fields[] = array(
							'value' => $field->id,
							'label' => GFCommon::get_label( $field ) . ' (' . esc_html__( 'Selected', 'gravityforms' ) . ')',
						);
					}

					foreach ( $inputs as $input ) {
						$fields[] = array(
							'value' => $input['id'],
							'label' => GFCommon::get_label( $field, $input['id'] ),
						);
					}
				} else if ( $input_type == 'list' && $field->enableColumns && $field_is_valid_type && ! $exclude_field ) {
					$fields[]  = array(
						'value' => $field->id,
						'label' => GFCommon::get_label( $field ) . ' (' . esc_html__( 'Full', 'gravityforms' ) . ')',
					);
					$col_index = 0;
					foreach ( $field->choices as $column ) {
						$fields[] = array(
							'value' => $field->id . '.' . $col_index,
							'label' => GFCommon::get_label( $field ) . ' (' . esc_html( rgar( $column, 'text' ) ) . ')',
						);
						$col_index++;
					}
				} else if ( ! $field->displayOnly && $field_is_valid_type && ! $exclude_field ) {
					$fields[] = array( 'value' => $field->id, 'label' => GFCommon::get_label( $field ) );
				}
			}
		}

		/**
		 * Filter the choices available in the field map drop down.
		 *
		 * @since 2.0.7.11
		 *
		 * @param array             $fields              The value and label properties for each choice.
		 * @param int               $form_id             The ID of the form currently being configured.
		 * @param null|array        $field_type          Null or the field types to be included in the drop down.
		 * @param null|array|string $exclude_field_types Null or the field type(s) to be excluded from the drop down.
		 */
		$fields = apply_filters( 'gform_addon_field_map_choices', $fields, $form_id, $field_type, $exclude_field_types );

		if ( function_exists( 'get_called_class' ) ) {
			$callable = array( get_called_class(), 'get_instance' );
			if ( is_callable( $callable ) ) {
				$add_on = call_user_func( $callable );
				$slug   = $add_on->get_slug();

				$fields = apply_filters( "gform_{$slug}_field_map_choices", $fields, $form_id, $field_type, $exclude_field_types );
			}
		}

		return $fields;
	}

	/**
	 * Get Gravity Forms database version number.
	 *
	 * @since 1.3
	 * @access public
	 *
	 * @uses GFFOrmsModel::get_database_version()
	 *
	 * @return string
	 */
	public static function get_gravityforms_db_version() {

		if ( method_exists( 'GFFormsModel', 'get_database_version' ) ) {
			$db_version = GFFormsModel::get_database_version();
		} else {
			$db_version = GFForms::$version;
		}

		return $db_version;

	}

	/**
	 * Override value form GFAddOn::get_field_value().
	 *
	 * @since  1.1.2
	 * @access public
	 *
	 * @param string $field_value The current field value.
	 * @param array  $form        The current Form object.
	 * @param array  $entry       The current Entry object.
	 * @param string $field_id    The current field ID.
	 *
	 * @return string
	 */
	public function override_field_value( $field_value, $form, $entry, $field_id ) {

		// Get field.
		$field = GFFormsModel::get_field( $form, $field_id );

		// If this is not a List or Multi Select field, return.
		if ( ! in_array( $field->type, array( 'list', 'multiselect' ) ) ) {
			return $field_value;
		}

		return rgar( $entry, $field_id );

	}

	/**
	 * Check if form has any Easy Passthrough feeds where it is the source form.
	 *
	 * @since  1.1
	 * @access public
	 *
	 * @param int $form_id Form ID.
	 *
	 * @return bool
	 */
	public function has_easy_passthrough_feeds( $form_id ) {

		// Get Easy Passthrough feeds.
		$feeds = $this->get_feeds();

		// If no Easy Passthrough feeds are configured, return.
		if ( empty( $feeds ) ) {
			return false;
		}

		// Loop through feeds.
		foreach ( $feeds as $feed ) {

			// If form ID is the source form, return.
			if ( intval( $feed['meta']['sourceForm'] ) == $form_id ) {
				return true;
			}

		}

		return false;

	}

	/**
	 * Get an instance of WP Session Manager.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   \ForGravity\WP_Session::get_instance()
	 *
	 * @return \ForGravity\WP_Session
	 */
	public function session_manager() {

		// let users change the session cookie name
		if ( ! defined( 'FG_SESSION_COOKIE' ) ) {
			define( 'FG_SESSION_COOKIE', 'forgravity_session' );
		}

		if ( ! class_exists( '\Recursive_ArrayAccess' ) ) {
			include 'includes/wp-session-manager/class-recursive-arrayaccess.php';
		}

		// Include utilities class
		if ( ! class_exists( '\ForGravity\WP_Session_Utils' ) ) {
			include 'includes/wp-session-manager/class-wp-session-utils.php';
		}

		// Only include the functionality if it's not pre-defined.
		if ( ! class_exists( '\ForGravity\WP_Session' ) ) {
			include 'includes/wp-session-manager/class-wp-session.php';
			include 'includes/wp-session-manager/wp-session.php';
		}

		return \ForGravity\WP_Session::get_instance();

	}





	// # PLUGIN SETTINGS -----------------------------------------------------------------------------------------------

	/**
	 * Prepare plugin settings fields.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   Easy_Passthrough::license_feedback()
	 * @uses   Easy_Passthrough::license_key_description()
	 * @uses   GFAddOn::get_setting()
	 *
	 * @return array
	 */
	public function plugin_settings_fields() {

		$settings = array(
			array(
				'fields' => array(
					array(
						'name'                => 'license_key',
						'label'               => esc_html__( 'License Key', 'forgravity_easypassthrough' ),
						'type'                => 'text',
						'input_type'          => $this->license_feedback( $this->get_setting( 'license_key' ) ) ? 'password' : 'text',
						'class'               => 'medium',
						'default_value'       => '',
						'error_message'       => esc_html__( 'Invalid License', 'forgravity_easypassthrough' ),
						'description'         => $this->license_key_description(),
						'feedback_callback'   => array( $this, 'license_feedback' ),
						'validation_callback' => array( $this, 'license_validation' ),
					),
					array(
						'name'          => 'background_updates',
						'label'         => esc_html__( 'Background Updates', 'forgravity_easypassthrough' ),
						'type'          => 'radio',
						'horizontal'    => true,
						'default_value' => true,
						'tooltip'       => esc_html__( 'Set this to ON to allow Easy Passthrough to download and install bug fixes and security updates automatically in the background. Requires a valid license key.', 'forgravity_easypassthrough' ),
						'choices'       => array(
							array(
								'label' => esc_html__( 'On', 'forgravity_easypassthrough' ),
								'value' => true,
							),
							array(
								'label' => esc_html__( 'Off', 'forgravity_easypassthrough' ),
								'value' => false,
							),
						),
					),
				),
			),
		);

		if ( defined( 'FG_EASYPASSTHROUGH_LICENSE_KEY' ) || ( is_multisite() && ! is_main_site() ) ) {
			$settings[0]['fields'][0]['disabled'] = true;
		}

		return $settings;

	}

	/**
	 * Prepare description for License Key plugin settings field.
	 *
	 * @since  1.2.3
	 * @access public
	 *
	 * @uses   Easy_Passthrough::check_license()
	 * @uses   GFAddOn::get_setting()
	 *
	 * @return string
	 */
	public function license_key_description() {

		// Get license key.
		$license_key = $this->get_license_key();

		// If no license key is entered, display warning.
		if ( rgblank( $license_key ) ) {
			return esc_html__( 'The license key is used for access to automatic upgrades and support.', 'forgravity_easypassthrough' );
		}

		// Get license data.
		$license_data = $this->check_license( $license_key );

		// If no expiration date is provided, return.
		if ( ! rgobj( $license_data, 'expires' ) ) {
			return '';
		}

		if ( 'lifetime' === $license_data->expires ) {

			return sprintf(
				'<em>%s</em>',
				esc_html__( 'Your license is valid forever.', 'forgravity_easypassthrough' )
			);

		} else {

			return sprintf(
				'<em>%s</em>',
				sprintf(
					esc_html__( 'Your license is valid through %s.', 'forgravity_easypassthrough' ),
					date( 'Y-m-d', strtotime( $license_data->expires ) )
				)
			);

		}

	}

	/**
	 * Get license validity for plugin settings field.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param string $value Plugin setting value.
	 * @param array  $field Plugin setting field.
	 *
	 * @uses   Easy_Passthrough::check_license()
	 * @uses   Easy_Passthrough::get_license_key()
	 *
	 * @return null|bool
	 */
	public function license_feedback( $value = '', $field = array() ) {

		// If no license key is provided, check the setting.
		if ( empty( $value ) ) {
			$value = $this->get_license_key();
		}

		// If no license key is provided, return.
		if ( empty( $value ) ) {
			return null;
		}

		// Get license data.
		$license_data = $this->check_license( $value );

		// If no license data was returned or license is invalid, return false.
		if ( empty( $license_data ) || 'invalid' === $license_data->license ) {
			return false;
		} else if ( 'valid' === $license_data->license ) {
			return true;
		}

		return false;

	}

	/**
	 * Activate license on plugin settings save.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array  $field         Plugin setting field.
	 * @param string $field_setting Plugin setting value.
	 *
	 * @uses   GFAddOn::get_plugin_setting()
	 * @uses   GFAddOn::log_debug()
	 * @uses   Easy_Passthrough::activate_license()
	 * @uses   Easy_Passthrough::process_license_request()
	 */
	public function license_validation( $field, $field_setting ) {

		// Get old license.
		$old_license = $this->get_plugin_setting( 'license_key' );

		// If an old license key exists and a new license is being saved, deactivate old license.
		if ( $old_license && $field_setting != $old_license ) {

			// Deactivate license.
			$deactivate_license = $this->process_license_request( 'deactivate_license', $old_license );

			// Log response.
			$this->log_debug( __METHOD__ . '(): Deactivate license: ' . print_r( $deactivate_license, true ) );

		}

		// If field setting is empty, return.
		if ( empty( $field_setting ) ) {
			return;
		}

		// Activate license.
		$this->activate_license( $field_setting );

	}





	// # LICENSE METHODS -----------------------------------------------------------------------------------------------

	/**
	 * Activate a license key.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param string $license_key The license key.
	 *
	 * @uses   Easy_Passthrough::process_license_request()
	 *
	 * @return array
	 */
	public function activate_license( $license_key ) {

		// Activate license.
		$license = $this->process_license_request( 'activate_license', $license_key );

		// Clear update plugins transient.
		set_site_transient( 'update_plugins', null );

		// Delete plugin version info cache.
		$cache_key = md5( 'edd_plugin_' . sanitize_key( $this->_path ) . '_version_info' );
		delete_transient( $cache_key );

		return json_decode( wp_remote_retrieve_body( $license ) );

	}

	/**
	 * Check the status of a license key.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param string $license_key The license key.
	 *
	 * @uses   Easy_Passthrough::get_license_key()
	 *
	 * @return object
	 */
	public function check_license( $license_key = '' ) {

		// If license key is empty, get the plugin setting.
		if ( empty( $license_key ) ) {
			$license_key = $this->get_license_key();
		}

		// Perform a license check request.
		$license = $this->process_license_request( 'check_license', $license_key );

		return json_decode( wp_remote_retrieve_body( $license ) );

	}

	/**
	 * Get license key.
	 *
	 * @since  1.3
	 * @access public
	 *
	 * @uses   GFAddOn::get_plugin_setting()
	 *
	 * @return string
	 */
	public function get_license_key() {

		return defined( 'FG_EASYPASSTHROUGH_LICENSE_KEY' ) ? FG_EASYPASSTHROUGH_LICENSE_KEY : $this->get_plugin_setting( 'license_key' );

	}

	/**
	 * Process a request to the ForGravity store.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param string $action  The action to process.
	 * @param string $license The license key.
	 * @param string $item_id The EDD item ID.
	 *
	 * @return array|\WP_Error
	 */
	public function process_license_request( $action, $license, $item_id = FG_EASYPASSTHROUGH_EDD_ITEM_ID ) {

		// Prepare the request arguments.
		$args = array(
			'method'    => 'POST',
			'timeout'   => 10,
			'sslverify' => false,
			'body'      => array(
				'edd_action' => $action,
				'license'    => trim( $license ),
				'item_id'    => urlencode( $item_id ),
				'url'        => home_url(),
			),
		);

		return wp_remote_request( FG_EDD_STORE_URL, $args );

	}





	// # BACKGROUND UPDATES --------------------------------------------------------------------------------------------

	/**
	 * Display activate license message on Plugins list page.
	 *
	 * @since 1.1.4
	 * @acces public
	 *
	 * @uses  GFAddOn::display_plugin_message()
	 * @uses  GFAddOn::get_plugin_setting()
	 * @uses  Easy_Passthrough::check_license()
	 */
	public function plugin_row() {

		parent::plugin_row();

		// Get license key.
		$license_key = $this->get_plugin_setting( 'license_key' );

		// If no license key is installed, display message.
		if ( rgblank( $license_key ) ) {

			// Prepare message.
			$message = sprintf(
				esc_html__( '%sRegister your copy%s of Easy Passthrough to receive access to automatic upgrades and support. Need a license key? %sPurchase one now.%s', 'forgravity_easypassthrough' ),
				'<a href="' . admin_url( 'admin.php?page=gf_settings&subview=' . $this->_slug ) . '">',
				'</a>',
				'<a href="' . esc_url( $this->_url ) . '" target="_blank">',
				'</a>'
			);

			// Add activate license message.
			self::display_plugin_message( $message );

			return;

		}

		// Get license data.
		$license_data = $this->check_license( $license_key );

		// If license key is invalid, display message.
		if ( empty( $license_data ) || 'valid' !== $license_data->license ) {

			// Prepare message.
			$message = sprintf(
				esc_html__( 'Your license is invalid or expired. %sEnter a valid license key%s or %spurchase a new one.%s', 'forgravity_easypassthrough' ),
				'<a href="' . admin_url( 'admin.php?page=gf_settings&subview=' . $this->_slug ) . '">',
				'</a>',
				'<a href="' . esc_url( $this->_url ) . '" target="_blank">',
				'</a>'
			);

			// Add invalid license message.
			self::display_plugin_message( $message );

			return;

		}

	}

	/**
	 * Determines if automatic updating should be processed.
	 *
	 * @since  Unknown
	 * @access 1.0
	 *
	 * @param bool   $update Whether or not to update.
	 * @param object $item   The update offer object.
	 *
	 * @uses   GFAddOn::log_debug()
	 * @uses   Easy_Passthrough::is_auto_update_disabled()
	 *
	 * @return bool
	 */
	public function maybe_auto_update( $update, $item ) {

		// If this is not the Entry Automation Add-On, exit.
		if ( ! isset( $item->slug ) || 'easypassthrough' !== $item->slug ) {
			return $update;
		}

		// Log that we are starting auto update.
		$this->log_debug( __METHOD__ . '(): Starting auto-update for Easy Passthrough.' );

		// Check if automatic updates are disabled.
		$auto_update_disabled = $this->is_auto_update_disabled();

		// Log automatic update disabled state.
		$this->log_debug( __METHOD__ . '(): Automatic update disabled: ' . var_export( $auto_update_disabled, true ) );

		// If automatic updates are disabled or if the installed version is the newest version or earlier, exit.
		if ( $auto_update_disabled || version_compare( $this->_version, $item->new_version, '=>' ) ) {
			$this->log_debug( __METHOD__ . '(): Aborting update.' );

			return false;
		}

		$current_major = implode( '.', array_slice( preg_split( '/[.-]/', $this->_version ), 0, 1 ) );
		$new_major     = implode( '.', array_slice( preg_split( '/[.-]/', $item->new_version ), 0, 1 ) );

		$current_branch = implode( '.', array_slice( preg_split( '/[.-]/', $this->_version ), 0, 2 ) );
		$new_branch     = implode( '.', array_slice( preg_split( '/[.-]/', $item->new_version ), 0, 2 ) );

		if ( $current_major == $new_major && $current_branch == $new_branch ) {
			$this->log_debug( __METHOD__ . '(): OK to update.' );

			return true;
		}

		$this->log_debug( __METHOD__ . '(): Skipping - not current branch.' );

		return $update;

	}

	/**
	 * Determine if automatic updates are disabled.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   GFAddOn::get_plugin_setting()
	 * @uses   GFAddOn::log_debug()
	 *
	 * @return bool
	 */
	public function is_auto_update_disabled() {

		// WordPress background updates are disabled if you do not want file changes.
		if ( defined( 'DISALLOW_FILE_MODS' ) && DISALLOW_FILE_MODS ) {
			return true;
		}

		// Do not run auto update during install.
		if ( defined( 'WP_INSTALLING' ) ) {
			return true;
		}

		// Get automatic updater disabled state.
		$wp_updates_disabled = defined( 'AUTOMATIC_UPDATER_DISABLED' ) && AUTOMATIC_UPDATER_DISABLED;
		$wp_updates_disabled = apply_filters( 'automatic_updater_disabled', $wp_updates_disabled );

		// If WordPress automatic updates are disabled, return.
		if ( $wp_updates_disabled ) {
			$this->log_debug( __METHOD__ . '(): WordPress background updates are disabled.' );

			return true;
		}

		// Get background updates plugin setting.
		$enabled = $this->get_plugin_setting( 'background_updates' );

		// Log setting.
		$this->log_debug( __METHOD__ . '(): Background updates setting: ' . var_export( $enabled, true ) );

		return $enabled;

	}





	// # MEMBERS INTEGRATION -------------------------------------------------------------------------------------------

	/**
	 * Register the ForGravity capabilities group with the Members plugin.
	 *
	 * @since  1.2.1
	 * @access public
	 */
	public function members_register_cap_group() {

		members_register_cap_group(
			'forgravity',
			array(
				'label' => esc_html( 'ForGravity' ),
				'icon'  => 'dashicons-forgravity',
				'caps'  => array(),
			)
		);

	}

	/**
	 * Register the capabilities and their human readable labels wit the Members plugin.
	 *
	 * @since  1.2.1
	 * @access public
	 */
	public function members_register_caps() {

		// Define capabilities for Easy Passthrough.
		$caps = array(
			'forgravity_easypassthrough'           => esc_html__( 'Manage Settings', 'forgravity_easypassthrough' ),
			'forgravity_easypassthrough_uninstall' => esc_html__( 'Uninstall', 'forgravity_easypassthrough' ),
		);

		// Register capabilities.
		foreach ( $caps as $cap => $label ) {
			members_register_cap(
				$cap,
				array(
					'label' => sprintf( '%s: %s', $this->get_short_title(), $label ),
					'group' => 'forgravity',
				)
			);
		}

	}

}
