<?php
/**
 * Plugin Name: Easy Passthrough for Gravity Forms
 * Plugin URI: http://forgravity.com/plugins/easy-passthrough/
 * Description: Easily transfer entry values from one Gravity Forms form to another
 * Version: 1.3.2
 * Author: ForGravity
 * Author URI: http://forgravity.com
 * Text Domain: forgravity_easypassthrough
 * Domain Path: /languages
 **/

if ( ! defined( 'FG_EDD_STORE_URL' ) ) {
	define( 'FG_EDD_STORE_URL', 'https://forgravity.com' );
}

define( 'FG_EASYPASSTHROUGH_VERSION', '1.3.2' );
define( 'FG_EASYPASSTHROUGH_EDD_ITEM_ID', 114 );

// Initialize plugin updater.
add_action( 'init', array( 'EasyPassthrough_Bootstrap', 'updater' ), 0 );

// If Gravity Forms is loaded, bootstrap the Easy Passthrough Add-On.
add_action( 'gform_loaded', array( 'EasyPassthrough_Bootstrap', 'load' ), 5 );

/**
 * Class EasyPassthrough_Bootstrap
 *
 * Handles the loading of the Easy Passthrough Add-On and registers with the Add-On framework.
 */
class EasyPassthrough_Bootstrap {

	/**
	 * If the Feed Add-On Framework exists, Easy Passthrough Add-On is loaded.
	 *
	 * @access public
	 * @static
	 */
	public static function load() {

		if ( ! method_exists( 'GFForms', 'include_feed_addon_framework' ) ) {
			return;
		}

		if ( ! class_exists( '\ForGravity\EasyPassthrough\EDD_SL_Plugin_Updater' ) ) {
			require_once( 'includes/EDD_SL_Plugin_Updater.php' );
		}

		require_once( 'class-easypassthrough.php' );

		GFAddOn::register( '\ForGravity\Easy_Passthrough' );

	}

	/**
	 * Initialize plugin updater.
	 *
	 * @access public
	 * @static
	 */
	public static function updater() {

		// Get Easy Passthrough instance.
		$easy_passthrough = fg_easypassthrough();

		// If Easy Passthrough could not be retrieved, exit.
		if ( ! $easy_passthrough ) {
			return;
		}

		// Get plugin settings.
		$settings = $easy_passthrough->get_plugin_settings();

		// Get license key.
		if ( defined( 'FG_EASYPASSTHROUGH_LICENSE_KEY' ) ) {
			$license_key = FG_EASYPASSTHROUGH_LICENSE_KEY;
		} else {
			$license_key = trim( rgar( $settings, 'license_key' ) );
		}

		new ForGravity\EasyPassthrough\EDD_SL_Plugin_Updater(
			FG_EDD_STORE_URL,
			__FILE__,
			array(
				'version' => FG_EASYPASSTHROUGH_VERSION,
				'license' => $license_key,
				'item_id' => FG_EASYPASSTHROUGH_EDD_ITEM_ID,
				'author'  => 'ForGravity',
			)
		);

	}

}

/**
 * Returns an instance of the Easy_Passthrough class
 *
 * @see    Easy_Passthrough::get_instance()
 *
 * @return \ForGravity\Easy_Passthrough
 */
function fg_easypassthrough() {
	if ( class_exists( '\ForGravity\Easy_Passthrough' ) ) {
		return \ForGravity\Easy_Passthrough::get_instance();
	}
}
