<?php
/*
Plugin Name: MainWP Rocket Extension
Plugin URI: https://mainwp.com
Description: MainWP Rocket Extension combines the power of your MainWP Dashboard with the popular WP Rocket Plugin. It allows you to mange WP Rocket settings and quickly Clear and Preload cache on your child sites.
Version: 1.3
Author: MainWP
Author URI: https://mainwp.com
Documentation URI: https://mainwp.com/help/category/mainwp-extensions/rocket/
Icon URI:
*/

if ( ! defined( 'ABSPATH' ) ) { die( 'No direct access allowed' ); }

if ( ! defined( 'MAINWP_ROCKET_PLUGIN_FILE' ) ) {
	define( 'MAINWP_ROCKET_PLUGIN_FILE', __FILE__ );
}

define( 'MAINWP_ROCKET_URL', plugins_url( '', __FILE__ ) );

class MainWP_Rocket_Extension
{
	public $plugin_slug;

	public $wprocket_sites = null;

	const REQUIRES_MAINWP_VERSION = '2.0.22';
	const REQUIRES_MAINWP_CHILD_VERSION = '2.0.22';

	public function __construct() {

		$this->plugin_slug = plugin_basename( __FILE__ );

		add_filter( 'plugin_row_meta', array( &$this, 'plugin_row_meta' ), 10, 2 );		
		add_action( 'init', array( &$this, 'init' ) );
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_action( 'in_admin_header', array( &$this, 'in_admin_head' ) ); // Adds Help Tab in admin header
		add_filter( 'mainwp-getsubpages-sites', array( &$this, 'managesites_subpage' ), 10, 1 );
		add_action( 'admin_notices', array( &$this, 'error_notice' ) );

		include_once 'wp-rocket.php';
		MainWP_Rocket_DB::get_instance()->install();
	}

	public function plugin_row_meta( $plugin_meta, $plugin_file ) {

		if ( $this->plugin_slug != $plugin_file ) { return $plugin_meta; }

		$slug = basename($plugin_file, ".php");
		$api_data = get_option( $slug. '_APIManAdder');		
		if (!is_array($api_data) || !isset($api_data['activated_key']) || $api_data['activated_key'] != 'Activated' || !isset($api_data['api_key']) || empty($api_data['api_key']) ) {
			return $plugin_meta;
		}
		
		$plugin_meta[] = '<a href="?do=checkUpgrade" title="Check for updates.">Check for updates now</a>';
		return $plugin_meta;
	}

	function managesites_subpage( $subPage ) {
		$subPage[] = array(
		'title' => __( 'WP Rocket','mainwp-rocket-extension' ),
						   'slug' => 'WPRocket',
						   'sitetab' => true,
						   'menu_hidden' => true,
						   'callback' => array( 'MainWP_Rocket', 'render' ),
						   );
		return $subPage;
	}

	public function init() {

	}

	public function admin_init() {

		wp_enqueue_style( 'mainwp-rocket-extension', MAINWP_ROCKET_URL . '/css/mainwp-rocket.css' );
		wp_enqueue_script( 'mainwp-rocket-extension', MAINWP_ROCKET_URL . '/js/mainwp-rocket.js', array( 'jquery' ), MAINWP_WP_ROCKET_VERSION );

		wp_localize_script(
			'mainwp-rocket-extension',
			'mainwp_rocket_loc',
			array(
						'nonce'                    => wp_create_nonce( 'mainwp_rocket_nonce' ),
				)
		);
		
		MainWP_Rocket::get_instance()->admin_init();
		MainWP_Rocket_Plugin::get_instance()->admin_init();
	}

	function in_admin_head() {
		if ( isset( $_GET['page'] ) && $_GET['page'] == 'Extensions-Mainwp-Rocket-Extension' ) {
			self::addHelpTabs(); // If page is the Extension then call this 'addHelpTabs' function
		}
	}
	/**
	 * This function add help tabs in header.
	 * @return void
	 */
	public static function addHelpTabs() {
		$screen = get_current_screen(); //This function returns an object that includes the screen's ID, base, post type, and taxonomy, among other data points.
		$i      = 1;
		$screen->add_help_tab( array(
			'id'      => 'mainwp_rocket_helptabs_' . $i ++,
			'title'   => __( 'First Steps with Extensions', 'mainwp-rocket-extension' ),
			'content' => self::getHelpContent( 1 ),
		) );
		$screen->add_help_tab( array(
			'id'      => 'mainwp_rocket_helptabs_' . $i ++,
			'title'   => __( 'MainWP Rocket Extension', 'mainwp-rocket-extension' ),
			'content' => self::getHelpContent( 2 ),
		) );
	}
	/**
	 * Get help tab content.
	 *
	 * @param int $tabId
	 *
	 * @return string|bool
	 */
	public static function getHelpContent( $tabId ) {
		ob_start();
		if ( 1 == $tabId ) {
			?>
			<h3><?php echo __( 'First Steps with Extensions', 'mainwp-rocket-extension' ); ?></h3>
			<p><?php echo __( 'If you are having issues with getting started with the MainWP extensions, please review following help documents', 'mainwp-rocket-extension' ); ?></p>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'What are the MainWP Extensions', 'mainwp-rocket-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/order-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Order Extension(s)', 'mainwp-rocket-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/my-downloads-and-api-keys/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'My Downloads and API Keys', 'mainwp-rocket-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/install-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Install Extension(s)', 'mainwp-rocket-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/activate-extensions-api/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Activate Extension(s) API', 'mainwp-rocket-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/updating-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Updating Extension(s)', 'mainwp-rocket-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/remove-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Remove Extension(s)', 'mainwp-rocket-extension' ); ?></a><br/><br/>
			<a href="https://mainwp.com/help/category/mainwp-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Help Documenation for all MainWP Extensions', 'mainwp-rocket-extension' ); ?></a>
		<?php } else if ( 2 == $tabId ) { ?>
			<h3><?php echo __( 'MainWP Rocket Extension', 'mainwp-rocket-extension' ); ?></h3>
			<a href="https://mainwp.com/help/docs/rocket-extension/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Rocket Extension', 'mainwp-rocket-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/rocket-extension/install-and-set-the-mainwp-rocket-extension/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Install and Set the MainWP Rocket Extension', 'mainwp-rocket-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/rocket-extension/wp-rocket-dashboard/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'WP Rocket Dashboard', 'mainwp-rocket-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/rocket-extension/manage-wp-rocket-settings/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Manage WP Rocket Settings', 'mainwp-rocket-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/rocket-extension/clearpreload-wp-rocket-cache/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Clear/Preload WP Rocket Cache', 'mainwp-rocket-extension' ); ?></a><br/>
		<?php }
		$output = ob_get_clean();
		return $output;
	}

	function error_notice() {

		if ( self::is_mainwp_pages() && version_compare( self::REQUIRES_MAINWP_VERSION, get_option( 'mainwp_plugin_version' ), '>' ) ) {
			echo '<div class="error"><p>' . sprintf( __( '<strong>Warning</strong>: MainWP Rocket Extension requires MainWP Dashboard plugin version %s to be installed on your dashboard site. Please update MainWP Dashboard plugin!', 'mainwp-rocket-extension' ), self::REQUIRES_MAINWP_VERSION ) . '</p></div>';
		}
	}

	public static function is_mainwp_pages() {
		$current_screen = get_current_screen();
		if ( $current_screen->parent_base == 'mainwp_tab' ) {
			return true; }
		return false;
	}
}


function mainwp_rocket_extension_autoload( $class_name ) {

	$allowedLoadingTypes = array( 'base' );
	$class_name = str_replace( '_', '-', strtolower( $class_name ) );
	foreach ( $allowedLoadingTypes as $allowedLoadingType ) {
		$class_file = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . str_replace( basename( __FILE__ ), '', plugin_basename( __FILE__ ) ) . $allowedLoadingType . DIRECTORY_SEPARATOR . $class_name . '.class.php';
		if ( file_exists( $class_file ) ) {
			require_once( $class_file );
		}
	}
}


	spl_autoload_register( 'mainwp_rocket_extension_autoload' );


register_activation_hook( __FILE__, 'mainwp_rocket_extension_activate' );
register_deactivation_hook( __FILE__, 'mainwp_rocket_extension_deactivate' );

function mainwp_rocket_extension_activate() {

	update_option( 'mainwp_rocket_extension_activated', 'yes' );
	$extensionActivator = new MainWP_Rocket_Extension_Activator();
	$extensionActivator->activate();	
}

function mainwp_rocket_extension_deactivate() {

	$extensionActivator = new MainWP_Rocket_Extension_Activator();
	$extensionActivator->deactivate();
}

class MainWP_Rocket_Extension_Activator
{
	protected $mainwpMainActivated = false;
	protected $childEnabled = false;
	protected $childKey = false;
	protected $childFile;
	protected $plugin_handle = 'mainwp-rocket-extension';
	protected $product_id = 'MainWP Rocket Extension';
	protected $software_version = '1.3';

	public function __construct() {

		$this->childFile = __FILE__;
		add_filter( 'mainwp-getextensions', array( &$this, 'get_this_extension' ) );
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', false );

		if ( $this->mainwpMainActivated !== false ) {
			$this->activate_this_plugin();
		} else {
			add_action( 'mainwp-activated', array( &$this, 'activate_this_plugin' ) );
		}
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_action( 'admin_notices', array( &$this, 'mainwp_error_notice' ) );
	}

	function admin_init() {
		if ( get_option( 'mainwp_rocket_extension_activated' ) == 'yes' ) {
			delete_option( 'mainwp_rocket_extension_activated' );
			wp_redirect( admin_url( 'admin.php?page=Extensions' ) );
			return;
		}
	}

	function get_this_extension( $pArray ) {

		$pArray[] = array( 'plugin' => __FILE__, 'api' => $this->plugin_handle, 'mainwp' => true, 'callback' => array( &$this, 'settings' ), 'apiManager' => true );
		return $pArray;
	}

	function settings() {
		do_action( 'mainwp-pageheader-extensions', __FILE__ );
		MainWP_Rocket::render();
		do_action( 'mainwp-pagefooter-extensions', __FILE__ );
	}

	function activate_this_plugin() {
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', $this->mainwpMainActivated );
		$this->childEnabled = apply_filters( 'mainwp-extension-enabled-check', __FILE__ );
		$this->childKey = $this->childEnabled['key'];
		if ( function_exists( 'mainwp_current_user_can' )&& ! mainwp_current_user_can( 'extension', 'mainwp-rocket-extension' ) ) {
			return; 			
		}
		new MainWP_Rocket_Extension();
	}

	public function get_child_key() {

		return $this->childKey;
	}

	public function get_child_file() {

		return $this->childFile;
	}

	function mainwp_error_notice() {

		global $current_screen;
		if ( $current_screen->parent_base == 'plugins' && $this->mainwpMainActivated == false ) {
			echo '<div class="error"><p>MainWP Rocket Extension ' . __( 'requires <a href="http://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> to be activated in order to work. Please install and activate <a href="http://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> first.' ) . '</p></div>';
		}
	}

	public function update_option( $option_name, $option_value ) {

		$success = add_option( $option_name, $option_value, '', 'no' );

		if ( ! $success ) {
			$success = update_option( $option_name, $option_value );
		}

		 return $success;
	}

	public function activate() {
		$options = array(
		'product_id' => $this->product_id,
							'activated_key' => 'Deactivated',
							'instance_id' => apply_filters( 'mainwp-extensions-apigeneratepassword', 12, false ),
							'software_version' => $this->software_version,
						);
		$this->update_option( $this->plugin_handle . '_APIManAdder', $options );
	}

	public function deactivate() {
		$this->update_option( $this->plugin_handle . '_APIManAdder', '' );
	}
}

global $mainWPRocketExtensionActivator;
$mainWPRocketExtensionActivator = new MainWP_Rocket_Extension_Activator();
