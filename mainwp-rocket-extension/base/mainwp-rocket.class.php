<?php
class MainWP_Rocket
{
	public static $instance = null;
	private $rocket_settings = array();
	public $template_path = null;
	
	public static function get_instance() {

		if ( null === self::$instance ) { self::$instance = new self(); }
		return self::$instance;
	}

	public function __construct() {
		$this->template_path = MAINWP_WP_ROCKET_PATH . 'views/settings';
	}

	public function admin_init() {
		add_action( 'wp_ajax_mainwp_rocket_site_override_settings', array( $this, 'ajax_override_settings' ) );
		add_action( 'wp_ajax_mainwp_rocket_reload_optimize_info', array( $this, 'ajax_reload_optimize_info' ) );
		add_action( 'wp_ajax_mainwp_rocket_purge_cloudflare', array( $this, 'ajax_purge_cloudflare' ) );
        add_action( 'wp_ajax_mainwp_rocket_purge_opcache', array( $this, 'ajax_purge_opcache' ) );
		add_action( 'wp_ajax_mainwp_rocket_purge_cache_all', array( $this, 'ajax_purge_cache_all' ) );
		add_action( 'wp_ajax_mainwp_rocket_preload_cache', array( $this, 'ajax_preload_cache' ) );
		add_action( 'wp_ajax_mainwp_rocket_save_opts_to_child_site', array( $this, 'ajax_save_opts_to_child_site' ) );
		add_action( 'wp_ajax_mainwp_rocket_optimize_database_on_child_site', array( $this, 'ajax_optimize_database_on_child_site' ) );
		add_action( 'wp_ajax_mainwp_rocket_site_load_existing_settings', array( $this, 'ajax_load_existing_settings' ) );
		add_action( 'wp_ajax_mainwp_rocket_rightnow_load_sites', array( $this, 'ajax_general_load_sites' ) );
		add_action( 'mainwp_delete_site', array( &$this, 'delete_site_data' ), 10, 1 );
		add_action( 'mainwp_rightnow_widget_bottom', array( &$this, 'hook_right_now_widget_bottom' ), 10, 2 );
		add_filter( 'mainwp_managesites_bulk_actions', array( &$this, 'managesites_bulk_actions' ), 10, 1 );
		add_action( 'mainwp-site-synced', array( &$this, 'site_synced' ), 10, 2 );
		add_filter( 'mainwp-sync-others-data', array( $this, 'sync_others_data' ), 10, 2 );
	}

	public function site_synced( $pWebsite, $information = array() ) {
		if ( $pWebsite && $pWebsite->plugins != '' ) {
			$plugins = json_decode( $pWebsite->plugins, 1 );
			$is_active = 0;
			if ( is_array( $plugins ) && count( $plugins ) != 0 ) {
				foreach ( $plugins as $plugin ) {
					if ( 'wp-rocket/wp-rocket.php' == $plugin['slug'] ) {
						if ( $plugin['active'] ) {
							$is_active = 1; }
						break;
					}
				}
			}
			$update = array(
				'site_id' => $pWebsite->id,
				'is_active' => $is_active,
			);
			MainWP_Rocket_DB::get_instance()->update_wprocket( $update );
		}
		if ( is_array( $information ) && isset( $information['syncWPRocketData'] ) ) {
			$data = $information['syncWPRocketData'];
			if ( is_array( $data ) ) {
				$update = array( 'site_id' => $pWebsite->id );
				$wprocket_data = MainWP_Rocket_DB::get_instance()->get_wprocket_by( 'site_id' , $pWebsite->id, 'others' );
				$others = unserialize( base64_decode( $wprocket_data->others ) );
				if ( ! is_array( $others ) ) {
					$others = array(); }
				if ( isset( $data['rocket_boxes'] ) ) {
					$others['rocket_boxes'] = $data['rocket_boxes'];
				} else if ( isset( $others['rocket_boxes'] ) ) {
					unset( $others['rocket_boxes'] );
				}
				$update['others'] = base64_encode( serialize( $others ) );
				MainWP_Rocket_DB::get_instance()->update_wprocket( $update );
			}
			unset( $information['syncWPRocketData'] );
		}
	}

	public function sync_others_data( $data, $pWebsite = null ) {
		if ( ! is_array( $data ) ) {
			$data = array(); }
		$data['syncWPRocketData'] = 'yes';
		return $data;
	}

	public function get_options() {
		if ( empty( $this->rocket_settings ) ) {
			if ( self::is_manage_sites_page() ) {
				$site_id = self::get_manage_site_id();
				$wprocket = MainWP_Rocket_DB::get_instance()->get_wprocket_by( 'site_id', $site_id );
				if ( ! empty( $wprocket ) ) {
					$this->rocket_settings = unserialize( base64_decode( $wprocket->settings ) );
				} else {
					$this->rocket_settings = mainwp_get_rocket_default_options();
					$update = array(
						'site_id' => $site_id,
						'settings' => base64_encode( serialize( $this->rocket_settings ) ),
					);
					MainWP_Rocket_DB::get_instance()->update_wprocket( $update );
				}
			} else {
				$this->rocket_settings = get_option( MAINWP_ROCKET_GENERAL_SETTINGS );
			}

			if ( ! is_array( $this->rocket_settings ) ) {
				$this->rocket_settings = mainwp_get_rocket_default_options(); }
		}
		return $this->rocket_settings;
	}

	public function delete_site_data( $website ) {
		if ( $website ) {
			MainWP_Rocket_DB::get_instance()->delete_wprocket( 'site_id', $website->id );
		}
	}

	public static function render() {
		$website = null;
		if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
			global $mainWPRocketExtensionActivator;
			$option = array(
			'plugin_upgrades' => true,
							'plugins' => true,
			);
			$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), array( $_GET['id'] ), array(), $option );

			if ( is_array( $dbwebsites ) && ! empty( $dbwebsites ) ) {
				$website = current( $dbwebsites );
			}
		}

		if ( self::is_manage_sites_page() ) {
			$error = '';
			if ( empty( $website ) || empty( $website->id ) ) {
				$error = __( 'Error: Site not found.', 'mainwp' );
			} else {
				$activated = false;
				if ( $website && $website->plugins != '' ) {
					$plugins = json_decode( $website->plugins, 1 );
					if ( is_array( $plugins ) && count( $plugins ) > 0 ) {
						foreach ( $plugins as $plugin ) {
							if ( ('wp-rocket/wp-rocket.php' == $plugin['slug']) ) {
								if ( $plugin['active'] ) {
									$activated = true; }
								break;
							}
						}
					}
				}
				if ( ! $activated ) {
					$error = __( 'WP Rocket plugin is not installed or activated on the site.', 'mainwp-rocket-extension' );
				}
			}

			if ( ! empty( $error ) ) {
				do_action( 'mainwp-pageheader-sites', 'WPRocket' );
				echo '<div class="mainwp_info-box-red">' . $error . '</div>';
				do_action( 'mainwp-pagefooter-sites', 'WPRocket' );
				return;
			}
		}

		self::render_tabs( $website );
	}

	public static function render_tabs( $pWebsite = null ) {
		if ( isset( $_GET['action'] ) && 'mwpWPRocketOpenSite' == $_GET['action'] ) {
			self::open_site();
			return;
		}
		$message = '';
		if ( isset( $_GET['message'] ) ) {
			switch ( $_GET['message'] ) {
				case 1:
					$message = __( 'Settings saved', 'mainwp-rocket-extension' );
					break;
				case 2:
					$message = __( 'Settings imported and saved', 'mainwp-rocket-extension' );
					break;
			}
		}

		global $mainWPRocketExtensionActivator;
		$dbwebsites_wprocket = array();

		if ( ! self::is_manage_sites_page() ) {
			$websites = apply_filters( 'mainwp-getsites', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), null );
			$sites_ids = array();
			if ( is_array( $websites ) ) {
				foreach ( $websites as $site ) {
					$sites_ids[] = $site['id'];
				}
				unset( $websites );
			}
			$option = array(
			'plugin_upgrades' => true,
							'version' => true,
							'plugins' => true,
			);

			$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), $sites_ids, array(), $option );
			//print_r($dbwebsites);
			$selected_group = 0;

			if ( isset( $_POST['mainwp_rocket_plugin_groups_select'] ) ) {
				$selected_group = intval( $_POST['mainwp_rocket_plugin_groups_select'] );
			}

			$pluginDataSites = array();
			if ( count( $sites_ids ) > 0 ) {
				$pluginDataSites = MainWP_Rocket_DB::get_instance()->get_wprockets_data( $sites_ids );
			}

			$dbwebsites_wprocket = MainWP_Rocket_Plugin::get_instance()->get_websites_with_the_plugin( $dbwebsites, $selected_group, $pluginDataSites );

			unset( $dbwebsites );
			unset( $pluginDataSites );
		}

		$style_tab_dashboard = $style_submit = $style_tab_rocket_dashboard_opts = $style_tab_basic_cache = $style_tab_optimization = $style_tab_basic_media = $style_tab_adv_opts = $style_tab_database = $style_tab_preload = $style_tab_cloudflare = $style_tab_cdn = $style_tab_varnish = $style_tab_tools = ' style="display: none" ';

		$perform_action = $action = '';
		if ( isset( $_GET['_perform_action'] ) && ! empty( $_GET['_perform_action'] ) ) {
			//add_settings_error( 'general', 'settings_updated', __( 'Settings saved.', 'mainwp-rocket-extension' ), 'updated' );
			$perform_action = $_GET['_perform_action'];
			$security_error = false;
			if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], $perform_action ) ) {
				_e( 'Security Error', 'mainwp-backupwordpress-extension' );
				return;
			}
			$style_tab_rocket_dashboard_opts = '';
			$action = str_replace( 'mainwp_rocket_', '', $perform_action );
		}

		if ( self::is_manage_sites_page() ) {
			do_action( 'mainwp-pageheader-sites', 'WPRocket' );
			$is_manage_site = true;
		} else {
			$is_manage_site = false;
		}

		if ( empty( $action ) ) {
            
			if ( isset( $_GET['tab'] ) ) {
				if ( 'dashboard' == $_GET['tab'] ) {
					$style_tab_rocket_dashboard_opts = '';
				} else if ( 'cache' == $_GET['tab'] ) {
					$style_tab_basic_cache = '';
				} else if ( 'optimization' == $_GET['tab'] ) {
					$style_tab_optimization = '';
				} 
				else if ( 'media' == $_GET['tab'] ) {
					$style_tab_basic_media = '';
				}				
				else if ( 'adv' == $_GET['tab'] ) {
					$style_tab_adv_opts = '';
				} else if ( 'database' == $_GET['tab'] ) {
					$style_tab_database = '';
				}  else if ( 'preload' == $_GET['tab'] ) {
					$style_tab_preload = '';
				} else if ( 'cdn' == $_GET['tab'] ) {
					$style_tab_cdn = '';
				}  else if ( 'varnish' == $_GET['tab'] ) {
					$style_tab_varnish = '';
				} else if ( 'tool' == $_GET['tab'] ) {
					$style_tab_tools = '';
				}
			} else {
				if ( $is_manage_site ) {
					$style_tab_rocket_dashboard_opts = '';
				} else {
					$style_tab_dashboard = '';
				}
			}
		}

		$current_site_id = ! empty( $pWebsite ) ? $pWebsite->id : 0;
		$is_override = false;

		if ( $is_manage_site ) {
			$wprocket = MainWP_Rocket_DB::get_instance()->get_wprocket_by( 'site_id' , $current_site_id, 'override' );
			if ( $wprocket ) {
				$is_override = $wprocket->override;
			}
		} else {

		}

		if ( $is_manage_site ) {
			$dashboard_link = '';
		} else {
			$dashboard_link = '<a tab="dashboard" href="#" class="mainwp_action left ' . (empty( $style_tab_dashboard ) ? 'mainwp_action_down selected' : '') . '">' .  __( 'Dashboard', 'mainwp' ) . '</a>';
		}
		$cloudFlace = mainwp_get_rocket_option( 'do_cloudflare' );
		$page = isset( $_GET['page'] ) ? $_GET['page'] : '';

		?> 
			<div class="mainwp-subnav-tabs">
				<div class="rocket_tabs_lnk"><?php
					echo $dashboard_link;
					?><a style="padding: 15px 25px;" class="mainwp_action mid<?php if ( empty( $style_tab_rocket_dashboard_opts ) ) { ?> mainwp_action_down selected<?php } ?>" href="#" tab="rocket-dashboard"><?php _e( 'WP Rocket Dashboard', 'mainwp-rocket-extension' ); ?></a><?php                            
					?><a style="padding: 15px 25px;" class="mainwp_action mid<?php if ( empty( $style_tab_basic_cache ) ) { ?> mainwp_action_down selected<?php } ?>" href="#" tab="cache"><?php _e( 'Cache', 'mainwp-rocket-extension' ); ?></a><?php                            
					?><a style="padding: 15px 25px;" class="mainwp_action mid<?php if ( empty( $style_tab_optimization ) ) { ?> mainwp_action_down selected<?php } ?>" href="#" tab="optimization"><?php _e( 'File Optimization', 'mainwp-rocket-extension' ); ?></a><?php
					?><a style="padding: 15px 25px;" class="mainwp_action mid<?php if ( empty( $style_tab_basic_media ) ) { ?> mainwp_action_down selected<?php } ?>" href="#" tab="media"><?php _e( 'Media', 'mainwp-rocket-extension' ); ?></a><?php                            
					?><a style="padding: 15px 25px;" class="mainwp_action mid<?php if ( empty( $style_tab_preload ) ) { ?> mainwp_action_down selected<?php } ?>" href="#tab_preload" tab="preload"><?php _e( 'Preload', 'mainwp-rocket-extension' ); ?></a><?php                    
					if ( $cloudFlace ) {
                    ?><a style="padding: 15px 25px;" class="mainwp_action mid<?php if ( empty( $style_tab_cloudflare ) ) { ?> mainwp_action_down selected<?php } ?>" href="#" tab="cloudflare" ><?php _e( 'CloudFlare', 'mainwp-rocket-extension' ); ?></a><?php                    					
					}
					?><a style="padding: 15px 25px;" class="mainwp_action mid<?php if ( empty( $style_tab_adv_opts ) ) { ?> mainwp_action_down selected<?php } ?>" href="#" tab="adv"><?php _e( 'Advanced Rules', 'mainwp-rocket-extension' ); ?></a><?php                    
					?><a style="padding: 15px 25px;" class="mainwp_action mid<?php if ( empty( $style_tab_database ) ) { ?> mainwp_action_down selected<?php } ?>" href="#" tab="database"><?php _e( 'Database', 'mainwp-rocket-extension' ); ?></a><?php					
					?><a style="padding: 15px 25px;" class="mainwp_action mid<?php if ( empty( $style_tab_cdn ) ) { ?> mainwp_action_down selected<?php } ?>" href="#" tab="cdn"><?php _e( 'CDN', 'mainwp-rocket-extension' ); ?></a><?php
					?><a style="padding: 15px 25px;" class="mainwp_action mid<?php if ( empty( $style_tab_varnish  ) ) { ?> mainwp_action_down selected<?php } ?>" href="#" tab="varnish"><?php _e( 'Varnish', 'mainwp-rocket-extension' ); ?></a><?php
					?><a style="padding: 15px 25px;" class="mainwp_action right<?php if ( empty( $style_tab_tools ) ) { ?> mainwp_action_down selected<?php } ?>" href="#" tab="tools"><?php _e( 'Tools', 'mainwp-rocket-extension' ); ?></a><?php
					?>
					<div style="clear: both;"></div>
                </div>
			</div>
           <div class="wrap" id="mainwp-wpr-Modal">
            <div class="clearfix"></div>           
            <div class="inside">                     
                <div class="mainwp_info-box" <?php echo ( ! empty( $message ) ? '' : 'style="display: none"'); ?>><i class="fa fa-check-circle"></i> <?php echo ( ! empty( $message ) ? $message : ''); ?></div>
                <div id="mainwp_rocket_settings">                                                             
                    <div class="clear"> 
                        <br />
                        <div id="mwp_rocket_info" class="mainwp_info-box-yellow" style="display:none;"></div>
                       
                        <?php if ( ! $is_manage_site ) { ?>
                        <div id="mwp_rocket_tab_dashboard" class="mwp_rocket_tabs" <?php echo $style_tab_dashboard; ?>>                           
                            <?php MainWP_Rocket_Plugin::gen_select_sites( $dbwebsites_wprocket, $selected_group ); ?>  
                                <input type="button" class="mainwp-upgrade-button button-primary button" value="<?php _e( 'Sync Data' ); ?>" id="dashboard_refresh" style="background-image: none!important; float:right; padding-left: .6em !important;">                           
                            <?php MainWP_Rocket_Plugin::gen_dashboard_tab( $dbwebsites_wprocket ); ?>                            
                        </div>   
                        <?php }  ?>
                        
                        <form action="options.php" id="rocket_options" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="mainwp_wprocket_options_page" value="1"> 
                            <input type="hidden" name="mainwp_rocket_current_site_id" value="<?php echo $current_site_id; ?>"> 
                                <div id="poststuff">                               
                                    <?php

									if ( ! $is_manage_site && ( ! empty( $action )) ) {
										?><div id="mwp_rocket_tab_perform_action"><?php
										MainWP_Rocket::ajax_general_load_sites( $action );
										?></div><?php
									} else {

										if ( $is_manage_site ) {
											self::site_settings_box();
										}

										$modules = array(
											//'api-key',
											'dashboard',
											'basic',
                                            'optimization',
											'media',
											'advanced',
											'database',
											'preload',
											'cloudflare',
											'cdn',
											'varnish',
											'tools',
										);

										foreach ( $modules as $module ) {
												require( MAINWP_WP_ROCKET_ADMIN_UI_MODULES_PATH . $module . '.php' );
										}

										settings_fields( 'mainwp_wp_rocket' );

										?>
										<div id="mwp_rocket_tab_rocket-dashboard" class="mwp_rocket_tabs postbox" <?php echo $style_tab_rocket_dashboard_opts; ?>>                                                               
                                            <h3 class="hndle"><span><i class="fa fa-cog"></i> <?php _e( 'WP Rocket Dashboard', 'mainwp-rocket-extension' ); ?></span></h3>
                                            <div class="inside">
                                                <?php do_settings_sections( 'dashboard' ); ?>
                                            </div>
                                        </div>
                                        <div id="mwp_rocket_tab_cache" class="mwp_rocket_tabs postbox" <?php echo $style_tab_basic_cache; ?>>                                                               
                                            <h3 class="hndle"><span><i class="fa fa-cog"></i> <?php _e( 'Basic', 'mainwp-rocket-extension' ); ?></span></h3>
                                            <div class="inside">
                                                <?php do_settings_sections( 'basic' ); ?>
                                            </div>
                                        </div> 
                                        <div id="mwp_rocket_tab_optimization" class="mwp_rocket_tabs postbox" <?php echo $style_tab_optimization; ?>>                                                               
                                            <h3 class="hndle"><span><i class="fa fa-cog"></i> <?php _e( 'File Optimization', 'mainwp-rocket-extension' ); ?></span></h3>
                                            <div class="inside">
                                                <?php do_settings_sections( 'rocket_optimization' ); ?>
                                            </div>
                                        </div> 
										<div id="mwp_rocket_tab_media" class="mwp_rocket_tabs postbox" <?php echo $style_tab_basic_media; ?>>                                                               
                                            <h3 class="hndle"><span><i class="fa fa-cog"></i> <?php _e( 'Media', 'mainwp-rocket-extension' ); ?></span></h3>
                                            <div class="inside">
                                                <?php do_settings_sections( 'media' ); ?>
                                            </div>
                                        </div> 
                                        <div id="mwp_rocket_tab_adv" class="mwp_rocket_tabs postbox" <?php echo $style_tab_adv_opts; ?>>                           
                                            <h3 class="hndle"><span><i class="fa fa-cog"></i> <?php _e( 'Advanced Rules', 'mainwp-rocket-extension' ); ?></span></h3>
                                            <div class="inside">
                                                <?php do_settings_sections( 'advanced' ); ?>
                                            </div>
                                        </div>
										<div class="mwp_rocket_tabs" id="mwp_rocket_tab_database" <?php echo $style_tab_database; ?>>											
											<?php if ($is_manage_site) { ?>
												<div style="float: right">
														<span id="mwp_reload_optimize_info_status" >
															<i style="display: none;" class="fa fa-spinner fa-pulse"></i>
																<span class="status hidden"></span>
														</span>&nbsp;
														<input type="button" class="mainwp-upgrade-button button-primary button" 
															value="<?php _e( 'Reload optimize info' ); ?>" id="mwp_rocket_reload_optimize_info" site-id="<?php echo $current_site_id; ?>" style="background-image: none!important; float:right;">
												</div><br/><br/>
											<?php } ?>									
											<div class="postbox">											
												<h3 class="hndle"><span><i class="fa fa-cog"></i> <?php _e( 'Database Optimization', 'mainwp-rocket-extension' ); ?></span></h3>
												<div class="inside">
												<?php do_settings_sections( 'rocket_database' ); ?>
												</div>
											</div>
										</div>
										<div class="mwp_rocket_tabs postbox" id="mwp_rocket_tab_preload" <?php echo $style_tab_preload; ?>>											
											<h3 class="hndle"><span><i class="fa fa-cog"></i> <?php _e( 'Preload options', 'mainwp-rocket-extension' ); ?></span></h3>
                                            <div class="inside">
											<?php do_settings_sections( 'rocket_preload' ); ?>
											</div>
										</div>									
                                        <?php if ( $cloudFlace ) { ?>
                                        <div id="mwp_rocket_tab_cloudflare" class="mwp_rocket_tabs postbox" <?php echo $style_tab_cloudflare; ?>>                           
                                            <h3 class="hndle"><span><i class="fa fa-cog"></i> <?php _e( 'CloudFlare', 'mainwp-rocket-extension' ); ?></span></h3>
                                            <div class="inside">
                                                <?php do_settings_sections( 'cloudflare' ); ?>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div id="mwp_rocket_tab_cdn" class="mwp_rocket_tabs postbox" <?php echo $style_tab_cdn; ?>>
                                            <h3 class="hndle"><span><i class="fa fa-cog"></i> <?php _e( 'Content Delivery Network options', 'mainwp-rocket-extension' ); ?></span></h3>
                                            <div class="inside">
                                                <?php do_settings_sections( 'cdn' ); ?>
                                            </div>
                                        </div>

										<div id="mwp_rocket_tab_varnish" class="mwp_rocket_tabs postbox" <?php echo $style_tab_varnish; ?>>
                                            <h3 class="hndle"><span><i class="fa fa-cog"></i> <?php _e( 'Varnish Caching options', 'mainwp-rocket-extension' ); ?></span></h3>
                                            <div class="inside">																								
												<?php do_settings_sections( 'rocket_varnish' ); ?>												
                                            </div>
                                        </div>
									
                                        <div id="mwp_rocket_tab_tools" class="mwp_rocket_tabs postbox" <?php echo $style_tab_tools; ?>>                           
                                            <h3 class="hndle"><span><i class="fa fa-cog"></i> <?php _e( 'Tools', 'mainwp-rocket-extension' ); ?></span></h3>
                                            <div class="inside">
                                                <?php do_settings_sections( 'tools' ); ?>
                                            </div>
                                        </div>
                                        <?php if ( ! empty( $style_tab_dashboard ) ) { $style_submit = ''; } ?>
                                        <span class="mwp_rocket_opts_submit" <?php echo $style_submit; ?>><?php submit_button(); ?></span>
                                        <?php
									}
									?>  
                                </div>
                        </form>    
                    </div>
                <div class="clear"></div>
                </div>
            </div>
        </div>  
    <?php

	if ( $is_manage_site ) {
		if ( ! empty( $action ) ) {
			?>
                 <script type="text/javascript">
					mainwp_rocket_individual_perform_action(<?php echo $current_site_id; ?>, '<?php echo $action; ?>');
                </script>
                <?php
		}
		do_action( 'mainwp-pagefooter-sites', 'WPRocket' );
	}

	}


	public static function site_settings_box() {
		$site_id = isset( $_GET['id'] ) ? $_GET['id'] : 0;
		$override = 0;
		if ( $site_id ) {
			$site_wprocket = MainWP_Rocket_DB::get_instance()->get_wprocket_by( 'site_id' , $site_id, 'override' );
			if ( $site_wprocket ) {
				$override = $site_wprocket->override;
			}
		}
		?>  
            <div class="postbox" id="wprocket_site_settings">
                <h3 class="mainwp_box_title"><span><i class="fa fa-cog"></i> <?php _e( 'WP Rocket Site Settings', 'mainwp-rocket-extension' ); ?></span></h3>
                <div class="inside">
                    <input type="hidden" name="mwp_rocket_settings_site_id" value="<?php echo $site_id; ?>">
                    <table class="form-table">  
                        <tr valign="top">                            
                            <td colspan="2" style="padding:0">                                
                                <div class="mainwp_info-box" id="mwp_rocket_options_message" style="display: none"></div>
                            </td>
                        </tr>
                        <tr valign="top">                            
                            <th scope="row" class="settinglabel">                                
                                <?php _e( 'Override General Settings','mainwp-rocket-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Set to YES if you want to overwrite general WP Rocket settings.','mainwp-rocket-extension' ) ); ?>
                            </th>
                            <td class="settingfield">
                                <div class="mainwp-checkbox">
                                      <input type="checkbox" id="mainwp_rocket_override_general_settings" name="mainwp_rocket_override_general_settings"  <?php echo (0 == $override ? '' : 'checked="checked"'); ?> value="1"/>
                                      <label for="mainwp_rocket_override_general_settings"></label>
                                </div>&nbsp; 
                                <span id="mwp_rocket_perform_individual_status" >
                                <i style="display: none;" class="fa fa-spinner fa-pulse"></i>
                                <span class="status hidden"></span>
                                </span>
                            </td>
                        </tr>                         
                    </table>
                <input class="button-primary" id="mwp_rocket_settings_save_btn" type="button" value="<?php echo __( 'Save', 'mainwp-rocket-extension' ); ?>" />
              </div>
            </div>  
        <?php
			global $current_user;
		if ( get_option( 'mainwp_rocket_perform_individual_settings_update' ) == 1 ) {
			delete_option( 'mainwp_rocket_perform_individual_settings_update' );
			?>
            <script type="text/javascript">
				mainwp_rocket_individual_save_settings(<?php echo $site_id; ?>);
                </script>
                <?php
		}
	}

	public static function open_site() {

		$id = $_GET['websiteid'];
		global $mainWPRocketExtensionActivator;
		$websites = apply_filters( 'mainwp-getdbsites', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), array( $id ) );
		$website = null;
		if ( $websites && is_array( $websites ) ) {
			$website = current( $websites );
		}

		$open_location = '';
		if ( isset( $_GET['open_location'] ) ) { $open_location = $_GET['open_location']; }
		?>
        <div id="mainwp_background-box">   
            <?php
			if ( function_exists( 'mainwp_current_user_can' ) && ! mainwp_current_user_can( 'dashboard', 'access_wpadmin_on_child_sites' ) ) {
				mainwp_do_not_have_permissions( 'WP-Admin on child sites' );
			} else {
			?>
			   <div style="font-size: 30px; text-align: center; margin-top: 5em;"><?php _e( 'You will be redirected to your website immediately.','mainwp-rocket-extension' ); ?></div>                            
			   <form method="POST" action="<?php echo MainWP_Rocket_Utility::get_data_authed( $website, 'index.php' , 'where', $open_location ); ?>" id="redirectForm">
               </form>
            <?php } ?>
        </div>
        <?php
	}

	public static function is_manage_sites_page( $tabs = array() ) {
		if ( isset( $_POST['individual'] ) && ! empty( $_POST['individual'] ) && isset( $_POST['wprocketRequestSiteID'] ) && ! empty( $_POST['wprocketRequestSiteID'] ) ) {
			return true;
		} else if ( isset( $_GET['page'] ) && ('ManageSitesWPRocket' == $_GET['page']) ) {
			return true;
		} else if ( isset( $_REQUEST['mainwp_rocket_current_site_id'] ) && ! empty( $_REQUEST['mainwp_rocket_current_site_id'] ) ) {
			return true;
		}
		return false;
	}

	public static function get_manage_site_id() {
		$site_id = 0;
		if ( self::is_manage_sites_page() ) {
			// ajax
			if ( isset( $_POST['individual'] ) && ! empty( $_POST['individual'] ) && isset( $_REQUEST['wprocketRequestSiteID'] ) && ! empty( $_REQUEST['wprocketRequestSiteID'] ) ) {
				$site_id = $_REQUEST['wprocketRequestSiteID']; } else if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
				$site_id = $_GET['id']; } else if ( isset( $_REQUEST['mainwp_rocket_current_site_id'] ) && ! empty( $_REQUEST['mainwp_rocket_current_site_id'] ) ) {
					$site_id = $_REQUEST['mainwp_rocket_current_site_id']; }
		}
		return $site_id;
	}


	public static function ajax_general_load_sites( $action = null ) {
		global $mainWPRocketExtensionActivator;

		if ( isset( $_POST['rightnow_action'] ) ) {
			$action = $_POST['rightnow_action'];
			$sites_ids = explode( ',', $_POST['wprocketRequestSiteID'] );
		} else {
			$websites = apply_filters( 'mainwp-getsites', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), null );
			$sites_ids = array();
			if ( is_array( $websites ) ) {
				foreach ( $websites as $website ) {
					$sites_ids[] = $website['id'];
				}
				unset( $websites );
			}
		}

		$option = array(
		'plugin_upgrades' => true,
							'plugins' => true,
		);
		$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), $sites_ids, array(), $option );
		$dbwebsites_wprocket = MainWP_Rocket_Plugin::get_instance()->get_websites_with_the_plugin( $dbwebsites );
		unset( $dbwebsites );

		$title = '';
		if ( 'purge_cloudflare' == $action ) {
			$title = __( 'Clearing CloudFlare cache on child sites ...', 'mainwp-rocket-extension' );
		} else if ( 'purge_opcache' == $action ) {
			$title = __( 'Clearing OPcache cache on child sites ...', 'mainwp-rocket-extension' );
		} else if ( 'purge_cache_all' == $action ) {
			$title = __( 'Clearing All cache on child sites ...', 'mainwp-rocket-extension' );
		} else if ( 'preload_cache' == $action ) {
			$title = __( 'Preloading cache on child sites ...', 'mainwp-rocket-extension' );
		} else if ( 'save_opts_child_sites' == $action ) {
			$title = __( 'Saving settings to child sites ...', 'mainwp-rocket-extension' );
		} else if ( 'optimize_database' == $action ) {
			$title = __( 'Optimize Database on child sites ...', 'mainwp-rocket-extension' );
		}

		if ( ! empty( $title ) && ( ! isset( $_POST['hide_title'] ) || empty( $_POST['hide_title'] )) ) {
			echo '<h1>' . $title . '</h1><br />';
		}

		$have_active = false;
		if ( is_array( $dbwebsites_wprocket ) &&  count( $dbwebsites_wprocket ) > 0 ) {
			foreach ( $dbwebsites_wprocket as $website ) {
				if ( ! isset( $website['wprocket_active'] ) || empty( $website['wprocket_active'] ) ) {
					continue; }
				$have_active = true;
				echo '<div><strong>' . stripslashes( $website['name'] ) .'</strong>: ';
				echo '<span class="siteItemProcess" site-id="' . $website['id'] . '" status="queue"><span class="status">Queue ...</span> <i style="display: none;" class="fa fa-spinner fa-pulse"></i></span>';
				echo '</div>';
			}
		}

		if ( ! $have_active ) {
			echo '<div class="mainwp_info-box-yellow">' . __( 'No websites were found with the WP Rocket plugin installed.', 'mainwp-rocket-extension' ) . '</div>';
		} else {
			if ( ! isset( $_POST['rightnow_action'] ) ) {
				?>
                <script type="text/javascript">
                    jQuery( document ).ready( function ($) {   
                        rocket_bulkTotalThreads = jQuery('.siteItemProcess[status=queue]').length;
                        if (rocket_bulkTotalThreads > 0) {        
							<?php if ( ! empty( $action ) ) { ?>
                                mainwp_rocket_perform_action_start_next('<?php echo $action; ?>');
							<?php } ?>
                        }                                       
                    });
                </script>
                <?php
			}
		}
		echo '<br/>';
		if ( isset( $_POST['_wprocketNonce'] ) ) {
			die();
		}
	}

	public static function get_open_location_link( $site_id, $loc ) {
		$loc = base64_encode( $loc );
		return 'admin.php?page=Extensions-Mainwp-Rocket-Extension&action=mwpWPRocketOpenSite&websiteid=' . $site_id  . '&open_location=' . $loc;
	}

	function ajax_check_data() {
		if ( ! isset( $_REQUEST['_wprocketNonce'] ) || ! wp_verify_nonce( $_REQUEST['_wprocketNonce'], 'mainwp_rocket_nonce' ) ) {
			die( json_encode( array( 'error' => 'Security Error.' ) ) ); }

		if ( empty( $_POST['wprocketRequestSiteID'] ) ) {
			die( json_encode( array( 'error' => 'Empty site id.' ) ) ); }
	}

	function ajax_check_individual_setting( $pSiteId ) {
		$site_wprocket = MainWP_Rocket_DB::get_instance()->get_wprocket_by( 'site_id' , $pSiteId, 'override, is_active' );
//		if ( empty( $site_wprocket ) || $site_wprocket->is_active == 0 ) {
//			die( json_encode( array( 'message' => __( 'WP Rocket plugin is not installed or activated on the site.', 'mainwp-rocket-extension' ) ) ) );
//
//		}
		if ( isset( $_POST['individual'] ) ) {
			$individual = ! empty( $_POST['individual'] ) ? true : false;
			if ( $individual ) {
				if ( $site_wprocket && ! $site_wprocket->override ) {
					die( json_encode( array( 'error' => 'Action could not be performed: Override General Settings needs to be set to Yes.' ) ) );
				}
			} else {
				if ( $site_wprocket && $site_wprocket->override ) {
					die( json_encode( array( 'error' => __( 'Not perform action - Individual site settings are in use.', 'mainwp-rocket-extension' ) ) ) );
				}
			}
		}
	}


	function ajax_override_settings() {
		$this->ajax_check_data();
		$websiteId = $_POST['wprocketRequestSiteID'];
		global $mainWPRocketExtensionActivator;
		$website = apply_filters( 'mainwp-getsites', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), $websiteId );
		if ( $website && is_array( $website ) ) {
			$website = current( $website );
		}

		if ( ! $website ) {
			die( json_encode( array( 'error' => 'Error site data.' ) ) ); }

		$update = array(
			'site_id' => $website['id'],
			'override' => $_POST['override'],
		);

		MainWP_Rocket_DB::get_instance()->update_wprocket( $update );
		die( json_encode( array( 'result' => 'SUCCESS' ) ) );
	}
	
	function ajax_reload_optimize_info() {
		$this->ajax_check_data();
		$websiteId = $_POST['wprocketRequestSiteID'];
		global $mainWPRocketExtensionActivator;		
		$post_data = array( 'mwp_action' => 'get_optimize_info' );

		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), $websiteId, 'wp_rocket', $post_data );
		if (is_array($information) && isset($information['optimize_info'])) {
			update_option('mainwp_rocket_optimize_database_info', $information['optimize_info']);
		}
		die( json_encode( $information ) );
	}
	
	function ajax_purge_cloudflare() {
		$this->ajax_check_data();
		$websiteId = $_POST['wprocketRequestSiteID'];
		$this->ajax_check_individual_setting( $websiteId );

		global $mainWPRocketExtensionActivator;
		$post_data = array( 'mwp_action' => 'purge_cloudflare' );

		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), $websiteId, 'wp_rocket', $post_data );
		die( json_encode( $information ) );
	}

    function ajax_purge_opcache() {
		$this->ajax_check_data();
		$websiteId = $_POST['wprocketRequestSiteID'];
		$this->ajax_check_individual_setting( $websiteId );

		global $mainWPRocketExtensionActivator;
		$post_data = array( 'mwp_action' => 'purge_opcache' );

		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), $websiteId, 'wp_rocket', $post_data );
		die( json_encode( $information ) );
	}    
    
	function ajax_purge_cache_all() {
		$this->ajax_check_data();
		$websiteId = $_POST['wprocketRequestSiteID'];
		$this->ajax_check_individual_setting( $websiteId );

		global $mainWPRocketExtensionActivator;
		$post_data = array( 'mwp_action' => 'purge_all' );
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), $websiteId, 'wp_rocket', $post_data );
		if ( isset( $_POST['where'] ) && ('dashboard_tab' == $_POST['where']) ) {
			if ( is_array( $information ) && isset( $information['result'] ) && ('SUCCESS' == $information['result']) ) {
				$update = array( 'site_id' => $websiteId );
				$wprocket_data = MainWP_Rocket_DB::get_instance()->get_wprocket_by( 'site_id' , $websiteId, 'others' );
				$others = unserialize( base64_decode( $wprocket_data->others ) );
				if ( ! is_array( $others ) ) {
					$others = array(); }
				if ( isset( $data['rocket_boxes'] ) ) {
					if ( ! is_array( $data['rocket_boxes'] ) ) {
						$data['rocket_boxes'] = array(); }
					$others['rocket_boxes'][] = 'rocket_warning_plugin_modification';
				} else {
					$others['rocket_boxes'] = array( 'rocket_warning_plugin_modification' );
				}
				$update['others'] = base64_encode( serialize( $others ) );
				MainWP_Rocket_DB::get_instance()->update_wprocket( $update );

			}
		}
		die( json_encode( $information ) );
	}

	function ajax_preload_cache() {
		$this->ajax_check_data();
		$websiteId = $_POST['wprocketRequestSiteID'];
		$this->ajax_check_individual_setting( $websiteId );

		global $mainWPRocketExtensionActivator;
		$post_data = array( 'mwp_action' => 'preload_cache' );

		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), $websiteId, 'wp_rocket', $post_data );
		die( json_encode( $information ) );
	}

	private function check_override_settings( $override ) {
		if ( 1 == $override ) {
			die( json_encode( array( 'error' => __( 'Not Updated - Individual site settings are in use.', 'mainwp-rocket-extension' ) ) ) );
		}
	}

	function ajax_save_opts_to_child_site() {
		$this->ajax_check_data();
		$websiteId = $_POST['wprocketRequestSiteID'];
		//$this->ajax_check_individual_setting($websiteId);
		$settings = array();
		$settings_site = MainWP_Rocket_DB::get_instance()->get_wprocket_by( 'site_id' , $websiteId );
		$individual_update = isset( $_POST['individual'] ) && ! empty( $_POST['individual'] ) ? true : false;
		$general = false;
		if ( $individual_update ) {
			if ( $settings_site ) {
				if ( $settings_site->override ) {
					$settings = unserialize( base64_decode( $settings_site->settings ) );
				} else {
					die( json_encode( array( 'error' => 'Update Failed: Override General Settings need to be set to Yes.' ) ) );
				}
			}
		} else {
			if ( $settings_site ) {
				$this->check_override_settings( $settings_site->override );
			}
			$settings = get_option( MAINWP_ROCKET_GENERAL_SETTINGS );
			$general = true;
		}

		if ( ! is_array( $settings ) || empty( $settings ) ) {
			die( json_encode( array( 'error' => $general ? __( 'Error: Invalid general settings.', 'mainwp-rocket-extension' ) : __( 'Error: Invalid individual settings.', 'mainwp-rocket-extension' ) ) ) );
		}

		$send_fields = array();
		$defaults = mainwp_get_rocket_default_options();
		foreach ( $settings as $field => $value ) {
			if ( isset( $defaults[ $field ] ) ) {
					$send_fields[ $field ] = $value; 				
			}
		}
		
		global $mainWPRocketExtensionActivator;
		$website = apply_filters( 'mainwp-getsites', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), $websiteId );
		$website = current( $website );
		$send_fields = $this->sanitize_fields( $send_fields, $website['url'], $general );
		
		if (isset( $send_fields['sitemaps'] )) {
			$url      = $website['url'];			
			$search  = array( '%url%' );
			$replace = array( $url );
			$send_fields['sitemaps'] = str_replace( $search, $replace, $send_fields['sitemaps'] );
		}
		
		$post_data = array(
						'mwp_action' => 'save_settings',
						'settings' => base64_encode( serialize( $send_fields ) ),
					);
		
		$do_db_optimization = get_option('mainwp_do_rocket_database_optimization', '');
		if ( 'yes' == $do_db_optimization ) {			
			$post_data['do_database_optimization'] = 1;
			delete_option('mainwp_do_rocket_database_optimization');
		}

		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), $websiteId, 'wp_rocket', $post_data );
		die( json_encode( $information ) );
	}
	
	function ajax_optimize_database_on_child_site() {
		$this->ajax_check_data();
		$websiteId = $_POST['wprocketRequestSiteID'];
		$this->ajax_check_individual_setting( $websiteId );

		global $mainWPRocketExtensionActivator;
		$post_data = array( 'mwp_action' => 'optimize_database' );

		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), $websiteId, 'wp_rocket', $post_data );
		die( json_encode( $information ) );
	}
	
	function ajax_load_existing_settings() {
		$this->ajax_check_data();
		$websiteId = $_POST['wprocketRequestSiteID'];
		global $mainWPRocketExtensionActivator;
		$post_data = array( 'mwp_action' => 'load_existing_settings' );
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), $websiteId, 'wp_rocket', $post_data );
		if ( is_array( $information ) && isset( $information['result'] ) && 'SUCCESS' == $information['result'] ) {
			if ( isset( $information['options'] ) && is_array( $information['options'] ) ) {
				$options = $information['options'];
				$save_fields = mainwp_get_rocket_default_options();
				$save_options = array();
				foreach ( $options as $field => $value ) {
					if ( isset( $save_fields[ $field ] ) ) {
						$save_options[ $field ] = $value; }
				}
				$update = array(
					'site_id' => $websiteId,
					'settings' => base64_encode( serialize( $save_options ) ),
				);
				MainWP_Rocket_DB::get_instance()->update_wprocket( $update );
				unset( $information['options'] );
			}
		}
		die( json_encode( $information ) );
	}


	function sanitize_fields( $input, $site_url, $isGeneral ) {
			$values = array();
		foreach ( $input['minify_js_in_footer'] as $value ) {
			$pos = strpos( $value, 'http' );
			if ( 0 !== $pos ) {
				$values[] = $site_url . ltrim( $value, '/' );
			} else {
				$values[] = $value;
			}
		}
			$input['minify_js_in_footer'] = $values;

			$values = array();
		foreach ( $input['deferred_js_files'] as $value ) {
			$pos = strpos( $value, 'http' );
			if ( 0 !== $pos ) {
				$values[] = $site_url . ltrim( $value, '/' );
			} else {
				$values[] = $value;
			}
		}
			$input['deferred_js_files'] = $values;

		if ( $isGeneral && 1 == $input['cloudflare_domain'] ) {
			$input['cloudflare_domain'] = $site_url;
		}
		
		return $input;
	}

	function hook_right_now_widget_bottom( $site_ids, $globalView ) {
		if ( empty( $site_ids ) ) {
			return; }
		if ( $globalView ) {
			$site_ids = implode( ',', $site_ids );
		} else {
			$siteId = current( $site_ids );
		}
		?>

        <div class="mainwp-postbox-actions-bottom">
        	<a class="button-primary" onclick="return <?php echo ($globalView ? "mainwp_rocket_rightnow_loadsites('". $site_ids . "', 'purge_cache_all');" : 'mainwp_rocket_rightnow_clearcache_individual('. $siteId . ');'); ?>" href="#"><?php _e( 'Clear WP Rocket Cache', 'mainwp-rocket-extension' ); ?></a>&nbsp;
            <a class="button" onclick="return <?php echo ($globalView ? "mainwp_rocket_rightnow_loadsites('". $site_ids . "', 'preload_cache');" : 'mainwp_rocket_rightnow_preloadcache_individual('. $siteId . ');'); ?>" href="#"><?php _e( 'Preload WP Rocket Cache', 'mainwp-rocket-extension' ); ?></a>
            
            <div id="mwp_rocket_rightnow_working_status">
            	<span class="status hidden"></span>&nbsp;<i style="display: none;" class="fa fa-spinner fa-pulse"></i>
            </div>
            <div id="rightnow-rocket-box" title="<?php _e( 'Clear WP Rocket Cache', 'mainwp-rocket-extension' ); ?>" style="display: none; text-align: center">
	            <div style="height: 240px; overflow: auto; margin-top: 10px; margin-bottom: 10px; text-align: left" id="rightnow-rocket-content">
	                <div id="mwp_rocket_rightnow_load_sites"></div>
	            </div>            
	            <input id="rightnow-rocket-close" type="button" name="Close" value="<?php _e( 'Close','mainwp-rocket-extension' ); ?>" class="button" />
	        </div>
        </div>
                
        <?php
	}

	function managesites_bulk_actions( $actions ) {
		$actions['clear_wprocket_cache'] = __( 'Clear WP Rocket Cache', 'mainwp-rocket-extension' );
		$actions['preload_wprocket_cache'] = __( 'Preload WP Rocket Cache', 'mainwp-rocket-extension' );
		return $actions;
	}
	
	private function get_template_path( $path ) {
		return $this->template_path . '/' . $path . '.php';
	}
	
	public function checkbox( $args ) {
		echo $this->generate( 'fields/checkbox', $args );
	}
	
	public function generate( $template, $data = array() ) {
		$template_path = $this->get_template_path( $template );
		
		if ( !file_exists( $template_path ) ) {
			return;
		}

		ob_start();

		include $template_path;

		return trim( ob_get_clean() );
	}		
}

