<?php
class MainWP_Rocket_Plugin
{
	private $option_handle = 'mainwp_rocket_plugin_option';
	private $option = array();

	private static $order = '';
	private static $orderby = '';

	//Singleton
	private static $instance = null;
	static function get_instance() {

		if ( null == MainWP_Rocket_Plugin::$instance ) {
			MainWP_Rocket_Plugin::$instance = new MainWP_Rocket_Plugin();
		}
		return MainWP_Rocket_Plugin::$instance;
	}

	public function __construct() {
		$this->option = get_option( $this->option_handle );
	}

	public function admin_init() {
		add_action( 'wp_ajax_mainwp_wprocket_upgrade_noti_dismiss', array( $this, 'ajax_dismiss_upgrade_notice' ) );
		add_action( 'wp_ajax_mainwp_wprocket_version_noti_dismiss', array( $this, 'ajax_dismiss_version_notice' ) );
		add_action( 'wp_ajax_mainwp_rocket_active_plugin', array( $this, 'ajax_active_plugin' ) );
		add_action( 'wp_ajax_mainwp_rocket_upgrade_plugin', array( $this, 'ajax_upgrade_plugin' ) );
		add_action( 'wp_ajax_mainwp_wprocket_showhide_plugin', array( $this, 'ajax_showhide_plugin' ) );
	}

	public function get_option( $key = null, $default = '' ) {
		if ( isset( $this->option[ $key ] ) ) {
			return $this->option[ $key ]; }
		return $default;
	}

	public function set_option( $key, $value ) {
		$this->option[ $key ] = $value;
		return update_option( $this->option_handle, $this->option );
	}

	public static function gen_dashboard_tab( $websites ) {

		$orderby = 'name';
		$_order = 'desc';
		if ( isset( $_GET['wprocket_orderby'] ) && ! empty( $_GET['wprocket_orderby'] ) ) {
			$orderby = $_GET['wprocket_orderby'];
		}
		if ( isset( $_GET['wprocket_order'] ) && ! empty( $_GET['wprocket_order'] ) ) {
			$_order = $_GET['wprocket_order'];
		}

		$name_order = $version_order = $status_order = $last_scan_order = $time_order = $url_order = $hidden_order = '';

		if ( isset( $_GET['wprocket_orderby'] ) ) {
			if ( 'name' == $_GET['wprocket_orderby'] ) {
				$name_order = ('desc' == $_order) ? 'asc' : 'desc';
			} else if ( 'version' == $_GET['wprocket_orderby'] ) {
				$version_order = ('desc' == $_order) ? 'asc' : 'desc';
			} else if ( 'url' == $_GET['wprocket_orderby'] ) {
				$url_order = ('desc' == $_order) ? 'asc' : 'desc';
			} else if ( 'hidden' == $_GET['wprocket_orderby'] ) {
				$hidden_order = ('desc' == $_order) ? 'asc' : 'desc';
			}
		}

		self::$order = $_order;
		self::$orderby = $orderby;
		usort( $websites, array( 'MainWP_Rocket_Plugin', 'wprocket_data_sort' ) );

	?>
       <table id="mainwp-table-plugins" class="wp-list-table widefat plugins" cellspacing="0">
         <thead>
         <tr>
           <th class="check-column">
               <input type="checkbox"  id="cb-select-all-1" >
           </th>
           <th scope="col" class="manage-column sortable <?php echo $name_order; ?>">
               <a href="?page=Extensions-Mainwp-Rocket-Extension&wprocket_orderby=name&wprocket_order=<?php echo (empty( $name_order ) ? 'asc' : $name_order); ?>"><span><?php _e( 'Site','mainwp-rocket-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
           <th scope="col" class="manage-column sortable <?php echo $url_order; ?>">
               <a href="?page=Extensions-Mainwp-Rocket-Extension&wprocket_orderby=url&wprocket_order=<?php echo (empty( $url_order ) ? 'asc' : $url_order); ?>"><span><?php _e( 'URL','mainwp-rocket-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>  
           <th scope="col" class="manage-column sortable <?php echo $version_order; ?>">
               <a href="?page=Extensions-Mainwp-Rocket-Extension&wprocket_orderby=version&wprocket_order=<?php echo (empty( $version_order ) ? 'asc' : $version_order); ?>"><span><?php _e( 'Plugin Version','mainwp-rocket-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
           <th scope="col" class="manage-column sortable <?php echo $hidden_order; ?>">
               <a href="?page=Extensions-Mainwp-Rocket-Extension&wprocket_orderby=hidden&wprocket_order=<?php echo (empty( $hidden_order ) ? 'asc' : $hidden_order); ?>"><span><?php _e( 'Plugin Hidden','mainwp-rocket-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
           <th scope="col" class="manage-column sortable" >
               <span><?php _e( 'Settings','mainwp-rocket-extension' ); ?></span>
           </th>                    
         </tr>
         </thead>
         <tfoot>
         <tr>
           <th class="check-column">
               <input type="checkbox"  id="cb-select-all-2" >
           </th>
           <th scope="col" class="manage-column sortable <?php echo $name_order; ?>">
               <a href="?page=Extensions-Mainwp-Rocket-Extension&wprocket_orderby=name&wprocket_order=<?php echo (empty( $name_order ) ? 'asc' : $name_order); ?>"><span><?php _e( 'Site','mainwp-rocket-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
           <th scope="col" class="manage-column sortable <?php echo $url_order; ?>">
               <a href="?page=Extensions-Mainwp-Rocket-Extension&wprocket_orderby=url&wprocket_order=<?php echo (empty( $url_order ) ? 'asc' : $url_order); ?>"><span><?php _e( 'URL','mainwp-rocket-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>          
           <th scope="col" class="manage-column sortable <?php echo $version_order; ?>">
               <a href="?page=Extensions-Mainwp-Rocket-Extension&wprocket_orderby=version&wprocket_order=<?php echo (empty( $version_order ) ? 'asc' : $version_order); ?>"><span><?php _e( 'Plugin Version','mainwp-rocket-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>     
            <th scope="col" class="manage-column sortable <?php echo $hidden_order; ?>">
               <a href="?page=Extensions-Mainwp-Rocket-Extension&wprocket_orderby=hidden&wprocket_order=<?php echo (empty( $hidden_order ) ? 'asc' : $hidden_order); ?>"><span><?php _e( 'Plugin Hidden','mainwp-rocket-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
            <th scope="col" class="manage-column sortable">
               <span><?php _e( 'Settings','mainwp-rocket-extension' ); ?></span>
           </th>                  
         </tr>
         </tfoot>
           <tbody id="the-mwp-rocket-list" class="list:sites">
            <?php
			if ( is_array( $websites ) && count( $websites ) > 0 ) {
				self::get_plugin_dashboard_table_row( $websites );
			} else {
				_e( '<tr><td colspan="8">No websites were found with the WP Rocket plugin installed.</td></tr>' );
			}
			?>
           </tbody>
     </table>
	<?php
	}

	public static function get_plugin_dashboard_table_row( $websites ) {
		$dismiss = array();
		if ( session_id() == '' ) { session_start(); }
		if ( isset( $_SESSION['mainwp_wprocket_dismiss_upgrade_plugin_notis'] ) ) {
			$dismiss = $_SESSION['mainwp_wprocket_dismiss_upgrade_plugin_notis'];
		}

		$dismiss_version = array();
		if ( isset( $_SESSION['mainwp_wprocket_dismiss_version_plugin_notis'] ) ) {
			$dismiss_version = $_SESSION['mainwp_wprocket_dismiss_version_plugin_notis'];
		}

		if ( ! is_array( $dismiss ) ) {
			$dismiss = array(); }

		if ( ! is_array( $dismiss_version ) ) {
			$dismiss_version = array(); }

		$plugin_name = 'WP Rocket';
		foreach ( $websites as $website ) {
			$location = 'options-general.php?page=wprocket';
			$website_id = $website['id'];

			$version_warn = $rocket_warn = false;
			if ( ! empty( $website['version'] ) && version_compare( MainWP_Rocket_Extension::REQUIRES_MAINWP_CHILD_VERSION, $website['version'], '>' ) ) {
				$version_warn = true;
			}

			if ( isset( $website['rocket_boxes'] ) && is_array( $website['rocket_boxes'] ) ) {
				if ( ! in_array( 'rocket_warning_plugin_modification', $website['rocket_boxes'] ) ) {
					$rocket_warn = true;
				}
			}

			$hide_version = false;
			if ( isset( $dismiss_version[ $website_id ] ) ) {
				$hide_version = true;
			}

			$cls_active = (isset( $website['wprocket_active'] ) && ! empty( $website['wprocket_active'] )) ? 'active' : 'inactive';
			$cls_update = (isset( $website['wprocket_upgrade'] )) ? 'update' : '';
			$cls_update = ('inactive' == $cls_active) ? 'update' : $cls_update;
			$cls_warn = (( ! $hide_version && $version_warn) || $rocket_warn) ? 'warn' : '';

			$showhide_action = (1 == $website['hide_plugin']) ? 'show' : 'hide';
			$showhide_link = '<a href="#" class="mwp_rocket_showhide_plugin" showhide="' . $showhide_action . '"><i class="fa fa-eye-slash"></i> '. ('show' === $showhide_action ? 'Show ' . $plugin_name : 'Hide ' . $plugin_name ) . '</a>';

			?>
			<tr class="<?php echo $cls_active . ' ' . $cls_update . ' ' . $cls_warn; ?>" website-id="<?php echo $website_id; ?>" plugin-name="<?php echo $plugin_name; ?>">
               <th class="check-column">
                   <input type="checkbox"  name="checked[]">
               </th>
               <td>
                   <a href="admin.php?page=managesites&dashboard=<?php echo $website_id; ?>"><?php echo stripslashes( $website['name'] ); ?></a><br/>
                   <div class="row-actions"><span class="dashboard"><a href="admin.php?page=managesites&dashboard=<?php echo $website_id; ?>"><i class="fa fa-tachometer"></i> <?php _e( 'Dashboard', 'mainwp-rocket-extension' );?></a></span> |  <span class="edit"><a href="admin.php?page=managesites&id=<?php echo $website_id; ?>"><i class="fa fa-pencil-square-o"></i> <?php _e( 'Edit' );?></a> | <?php echo $showhide_link; ?></span></div>                    
                   <div class="its-action-working"><span class="status" style="display:none;"></span><span class="loading" style="display:none;"><i class="fa fa-spinner fa-pulse"></i> <?php _e( 'Please wait...' ); ?></span></div>
               </td>               
               <td>
                   <a href="<?php echo $website['url']; ?>" target="_blank"><?php echo $website['url']; ?></a><br/>
                   <div class="row-actions"><span class="edit"><a target="_blank" href="admin.php?page=SiteOpen&newWindow=yes&websiteid=<?php echo $website_id; ?>"><i class="fa fa-external-link"></i> <?php _e( 'Open WP-Admin', 'mainwp-rocket-extension' );?></a></span> | <span class="edit"><a href="admin.php?page=SiteOpen&newWindow=yes&websiteid=<?php echo $website_id; ?>&location=<?php echo base64_encode( $location ); ?>" target="_blank"><i class="fa fa-cog"></i> <?php _e( 'Open WP Rocket', 'mainwp-rocket-extension' );?></a></span></div>                    
               </td>
               <td>
				<?php
				if ( isset( $website['wprocket_plugin_version'] ) ) {
					echo $website['wprocket_plugin_version'];
				} else {
					echo '&nbsp;'; }
				?>
				</td>     
				<td>
                   <span class="wprocket_hidden_title"><?php
						echo (1 == $website['hide_plugin']) ? __( 'Yes' ) : __( 'No' );
					?>
				 </span>
				</td>
				<td>
					<span><?php echo ($website['isOverride'] ? __( 'Individual', 'mainwp' ) : __( 'General' ));?></span>                                    
				</td>                        
			</tr>        
				<?php

				if ( ($version_warn && ! $hide_version) || $rocket_warn ) {
					?>
					<tr class="plugin-update-tr warn">
                            <td colspan="7">
					<?php
					if ( $version_warn && ! $hide_version ) {
						?>                                               
						<div class="extra-message" website-id="<?php echo $website_id; ?>">
                        <span style="float:right"><a href="#" class="wprocket_plugin_version_noti_dismiss"><?php _e( 'Dismiss' ); ?></a></span>
                        <span><?php echo sprintf( __( '<strong>Warning</strong>: MainWP Rocket Extension requires Child Plugin %s to be installed on your child site. Please update your child plugin!', 'mainwp-rocket-extension' ), MainWP_Rocket_Extension::REQUIRES_MAINWP_CHILD_VERSION ); ?></span>
					   </div>                                            
						<?php
					}
					if ( $rocket_warn ) {
						?>                       
						<div class="mainwp_info-box" style="margin:0">                                    
                        <?php _e( 'One or more extensions have been enabled or disabled, do not forget to clear the cache if necessary.', 'mainwp-rocket-extension' ); ?> <a class="wp-core-ui button" href="#" onclick="mainwp_rocket_dashboard_tab_purge_all(<?php echo $website['id']; ?>, this); return false;" ><?php _e( 'Clear cache', 'mainwp-rocket-extension' ); ?></a>
					   </div>                       
						<?php
					}
				}

				$active_link = $update_link = '';
				$version = '';
				$plugin_slug = $website['plugin_slug'];
				if ( isset( $website['wprocket_active'] ) && empty( $website['wprocket_active'] ) ) {
					$active_link = '<a href="#" class="mwp_rocket_active_plugin" >Activate ' . $plugin_name . ' plugin</a>'; }

				if ( isset( $website['wprocket_upgrade'] ) ) {
					if ( isset( $website['wprocket_upgrade']['new_version'] ) ) {
						$version = $website['wprocket_upgrade']['new_version']; }
					$update_link = '<a href="#" class="mwp_rocket_upgrade_plugin" >Update ' . $plugin_name . ' plugin</a>';
					if ( isset( $website['wprocket_upgrade']['plugin'] ) ) {
						$plugin_slug = $website['wprocket_upgrade']['plugin']; }
				}

				$hide_update = false;
				if ( isset( $dismiss[ $website_id ] ) ) {
					$hide_update = true;
				}

				if ( ! empty( $active_link ) || ! empty( $update_link ) ) {
					$location = 'plugins.php';
					$link_row = $active_link .  ' | ' . $update_link;
					$link_row = rtrim( $link_row, ' | ' );
					$link_row = ltrim( $link_row, ' | ' );
					?>
					<tr class="plugin-update-tr" <?php echo ($hide_update ? 'style="display: none"' : ''); ?>>
                    <td colspan="7" class="plugin-update">
                        <div class="ext-upgrade-noti update-message" plugin-slug="<?php echo $plugin_slug; ?>" website-id="<?php echo $website_id; ?>" version="<?php echo $version; ?>">
                        <?php if ( ! $hide_update ) { ?>
                        <span style="float:right"><a href="#" class="wprocket_plugin_upgrade_noti_dismiss"><?php _e( 'Dismiss' ); ?></a></span>                    
                        <?php } ?>
                        <?php echo $link_row; ?>
                        <span class="mwp-rocket-row-working"><span class="status"></span>&nbsp;<i class="fa fa-spinner fa-pulse" style="display: none"></i></span>
                        </div>
                    </td>
				   </tr>
					<?php
				}
		}

	}

	public static function wprocket_data_sort( $a, $b ) {
		if ( 'version' == self::$orderby ) {
			$a = $a['wprocket_plugin_version'];
			$b = $b['wprocket_plugin_version'];
			$cmp = version_compare( $a, $b );
		} else if ( 'url' == self::$orderby ) {
			$a = $a['url'];
			$b = $b['url'];
			$cmp = strcmp( $a, $b );
		} else if ( 'hidden' == self::$orderby ) {
			$a = $a['hide_plugin'];
			$b = $b['hide_plugin'];
			$cmp = $a - $b;
		} else {
			$a = $a['name'];
			$b = $b['name'];
			$cmp = strcmp( $a, $b );
		}
		if ( 0 == $cmp ) {
			return 0; }

		if ( 'desc' == self::$order ) {
			return ($cmp > 0) ? -1 : 1; } else {
			return ($cmp > 0) ? 1 : -1; }
	}

	public function get_websites_with_the_plugin( $websites, $selected_group = 0, $plugin_data_sites = array() ) {
		$websites_wprocket = array();
		$pluginHide = $this->get_option( 'hide_the_plugin' );
		if ( ! is_array( $pluginHide ) ) {
			$pluginHide = array(); }

		if ( is_array( $websites ) && count( $websites ) ) {
			if ( empty( $selected_group ) ) {
				foreach ( $websites as $website ) {
					if ( $website && $website->plugins != '' ) {
						$plugins = json_decode( $website->plugins, 1 );
						if ( is_array( $plugins ) && count( $plugins ) != 0 ) {
							foreach ( $plugins as $plugin ) {
								if ( ('wp-rocket/wp-rocket.php' == $plugin['slug']) ) {
									$site = MainWP_Rocket_Utility::map_site( $website, array( 'id', 'name', 'url' ) );
									if ( $plugin['active'] ) {
										$site['wprocket_active'] = 1; } else {
										$site['wprocket_active'] = 0; }
										// get upgrade info
										$site['wprocket_plugin_version'] = $plugin['version'];
										$site['plugin_slug'] = $plugin['slug'];
										$plugin_upgrades = json_decode( $website->plugin_upgrades, 1 );
										if ( is_array( $plugin_upgrades ) && count( $plugin_upgrades ) > 0 ) {
											if ( isset( $plugin_upgrades['wp-rocket/wp-rocket.php'] ) ) {
												$upgrade = $plugin_upgrades['wp-rocket/wp-rocket.php'];
												if ( isset( $upgrade['update'] ) ) {
													$site['wprocket_upgrade'] = $upgrade['update'];
												}
											}
										}

										$site['hide_plugin'] = 0;
										if ( isset( $pluginHide[ $website->id ] ) && $pluginHide[ $website->id ] ) {
											$site['hide_plugin'] = 1;
										}

										$plugintDS = isset( $plugin_data_sites[ $site['id'] ] ) ? $plugin_data_sites[ $site['id'] ] : array();
										if ( ! is_array( $plugintDS ) ) {
											$plugintDS = array(); }

										if ( ! isset ( $plugintDS['is_active'] ) || $site['wprocket_active'] != $plugintDS['is_active'] ) {
											//update active status
											$update = array(
											'site_id' => $website->id,
											'is_active' => $site['wprocket_active'],
											);
											MainWP_Rocket_DB::get_instance()->update_wprocket( $update );
										}

										$site['isOverride'] = isset( $plugintDS['override'] ) ? $plugintDS['override'] : 0;

										$others_data = isset( $plugintDS['others'] ) ? $plugintDS['others'] : array();
										if ( is_array( $others_data ) ) {
											if ( isset( $others_data['rocket_boxes'] ) ) {
												$site['rocket_boxes'] = (array) $others_data['rocket_boxes'];
											}
										}
										$site['version'] = property_exists($website, 'version') ? $website->version : '';
										$websites_wprocket[] = $site;
										break;
								}
							}
						}
					}
				}
			} else {
				global $mainWPRocketExtensionActivator;

				$group_websites = apply_filters( 'mainwp-getdbsites', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), array(), array( $selected_group ) );
				$sites = array();
				foreach ( $group_websites as $site ) {
					$sites[] = $site->id;
				}
				foreach ( $websites as $website ) {
					if ( $website && $website->plugins != '' && in_array( $website->id, $sites ) ) {
						$plugins = json_decode( $website->plugins, 1 );
						if ( is_array( $plugins ) && count( $plugins ) != 0 ) {
							foreach ( $plugins as $plugin ) {
								if ( ('wp-rocket/wp-rocket.php' == $plugin['slug']) ) {
									$site = MainWP_Rocket_Utility::map_site( $website, array( 'id', 'name', 'url' ) );
									if ( $plugin['active'] ) {
										$site['wprocket_active'] = 1; } else {
										$site['wprocket_active'] = 0; }

										$site['wprocket_plugin_version'] = $plugin['version'];
										// get upgrade info
										$plugin_upgrades = json_decode( $website->plugin_upgrades, 1 );
										if ( is_array( $plugin_upgrades ) && count( $plugin_upgrades ) > 0 ) {
											if ( isset( $plugin_upgrades['wp-rocket/wp-rocket.php'] ) ) {
												$upgrade = $plugin_upgrades['wp-rocket/wp-rocket.php'];
												if ( isset( $upgrade['update'] ) ) {
													$site['wprocket_upgrade'] = $upgrade['update'];
												}
											}
										}
										$site['hide_plugin'] = 0;
										if ( isset( $pluginHide[ $website->id ] ) && $pluginHide[ $website->id ] ) {
											$site['hide_plugin'] = 1;
										}

										$plugintDS = isset( $plugin_data_sites[ $site['id'] ] ) ? $plugin_data_sites[ $site['id'] ] : array();
										if ( ! is_array( $plugintDS ) ) {
											$plugintDS = array(); }

										if ( $site['wprocket_active'] != $plugintDS['is_active'] ) {
											//update active status
											$update = array(
											'site_id' => $website->id,
											'is_active' => $site['wprocket_active'],
											);
											MainWP_Rocket_DB::get_instance()->update_wprocket( $update );
										}

										$site['isOverride'] = $plugintDS['override'];

										$others_data = isset( $plugintDS['others'] ) ? $plugintDS['others'] : array();
										if ( is_array( $others_data ) ) {
											if ( isset( $others_data['rocket_boxes'] ) ) {
												$site['rocket_boxes'] = (array) $others_data['rocket_boxes'];
											}
										}
										$site['version'] = $website->version;
										$websites_wprocket[] = $site;
										break;
								}
							}
						}
					}
				}
			}
		}

		// if search action
		$search_sites = array();
		if ( isset( $_GET['s'] ) && ! empty( $_GET['s'] ) ) {
			$find = trim( $_GET['s'] );
			foreach ( $websites_wprocket as $website ) {
				if ( stripos( $website['name'], $find ) !== false || stripos( $website['url'], $find ) !== false ) {
					$search_sites[] = $website;
				}
			}
			$websites_wprocket = $search_sites;
		}
		unset( $search_sites );

		return $websites_wprocket;
	}

	public static function gen_select_sites( $websites, $selected_group ) {
		global $mainWPRocketExtensionActivator;
		$groups = apply_filters( 'mainwp-getgroups', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), null );
		$search = (isset( $_GET['s'] ) && ! empty( $_GET['s'] )) ? trim( $_GET['s'] ) : '';
		?> 
                   
        <div class="alignleft actions bulkactions">
            <select id="mwp_rocket_plugin_action">
                <option selected="selected" value="-1"><?php _e( 'Bulk Actions' ); ?></option>
                <option value="activate-selected"><?php _e( 'Active' ); ?></option>
                <option value="update-selected"><?php _e( 'Update' ); ?></option>
				<option value="load-settings"><?php _e( 'Load existing Settings' ); ?></option>
                <option value="hide-selected"><?php _e( 'Hide' ); ?></option>
                <option value="show-selected"><?php _e( 'Show' ); ?></option>
            </select>
            <input type="button" value="<?php _e( 'Apply' ); ?>" class="button action" id="wprocket_plugin_doaction_btn" name="">
        </div>
                   
        <div class="alignleft actions">
            <form action="admin.php" method="GET">
                <input type="hidden" name="page" value="Extensions-Mainwp-Rocket-Extension">
                <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"><?php _e( 'No search results.','mainwp-rocket-extension' ); ?></span>
                <input type="text" class="mainwp_autocomplete ui-autocomplete-input" name="s" autocompletelist="sites" value="<?php echo stripslashes( $search ); ?>" autocomplete="off">
                <datalist id="sites">
                    <?php
					if ( is_array( $websites ) && count( $websites ) > 0 ) {
						foreach ( $websites as $website ) {
							echo '<option>' . $website['name']. '</option>';
						}
					}
					?>                
                </datalist>
                <input type="submit" name="" class="button" value="Search Sites">
            </form>
        </div>    
        <div class="alignleft actions">
            <form method="post" action="admin.php?page=Extensions-Mainwp-Rocket-Extension">
                <select name="mainwp_rocket_plugin_groups_select">
                    <option value="0"><?php _e( 'Select a group' ); ?></option>
                    <?php
					if ( is_array( $groups ) && count( $groups ) > 0 ) {
						foreach ( $groups as $group ) {
							$_select = '';
							if ( $selected_group == $group['id'] ) {
								$_select = 'selected '; }
							echo '<option value="' . $group['id'] . '" ' . $_select . '>' . $group['name'] . '</option>';
						}
					}
					?>
                </select>&nbsp;&nbsp;                     
                <input class="button" type="submit" name="mwp_rocket_plugin_btn_display" value="<?php _e( 'Display', 'mainwp' ); ?>">
            </form>  
        </div>    
        <?php
		return;
	}


	public function ajax_dismiss_upgrade_notice() {
		$website_id = $_POST['wprocketRequestSiteID'];
		if ( $website_id ) {
			@session_start();
			$dismiss = isset( $_SESSION['mainwp_wprocket_dismiss_upgrade_plugin_notis'] ) ? $_SESSION['mainwp_wprocket_dismiss_upgrade_plugin_notis'] : false;
			if ( is_array( $dismiss ) && count( $dismiss ) > 0 ) {
				$dismiss[ $website_id ] = 1;
			} else {
				$dismiss = array();
				$dismiss[ $website_id ] = 1;
			}
			$_SESSION['mainwp_wprocket_dismiss_upgrade_plugin_notis'] = $dismiss;
			die( 'updated' );
		}
		die( 'nochange' );
	}


	public function ajax_dismiss_version_notice() {
		$website_id = $_POST['wprocketRequestSiteID'];
		if ( $website_id ) {
			session_start();
			$dismiss = $_SESSION['mainwp_wprocket_dismiss_version_plugin_notis'];
			if ( is_array( $dismiss ) && count( $dismiss ) > 0 ) {
				$dismiss[ $website_id ] = 1;
			} else {
				$dismiss = array();
				$dismiss[ $website_id ] = 1;
			}
			$_SESSION['mainwp_wprocket_dismiss_version_plugin_notis'] = $dismiss;
			die( 'updated' );
		}
		die( 'nochange' );
	}


	public function ajax_active_plugin() {
		$_POST['websiteId'] = $_POST['wprocketRequestSiteID'];
		do_action( 'mainwp_activePlugin' );
		die();
	}

	public function ajax_upgrade_plugin() {
		$_POST['websiteId'] = $_POST['wprocketRequestSiteID'];
		do_action( 'mainwp_upgradePluginTheme' );
		die();
	}

	public function ajax_showhide_plugin() {
		$siteid = isset( $_POST['wprocketRequestSiteID'] ) ? $_POST['wprocketRequestSiteID'] : null;
		$showhide = isset( $_POST['showhide'] ) ? $_POST['showhide'] : null;
		if ( null !== $siteid && null !== $showhide ) {
			global $mainWPRocketExtensionActivator;
			$post_data = array(
			'mwp_action' => 'set_showhide',
								'showhide' => $showhide,
							);
			$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPRocketExtensionActivator->get_child_file(), $mainWPRocketExtensionActivator->get_child_key(), $siteid, 'wp_rocket', $post_data );

			if ( is_array( $information ) && isset( $information['result'] ) && 'SUCCESS' === $information['result'] ) {
				$hide_plugin = $this->get_option( 'hide_the_plugin' );
				if ( ! is_array( $hide_plugin ) ) {
					$hide_plugin = array(); }
				$hide_plugin[ $siteid ] = ('hide' === $showhide) ? 1 : 0;
				$this->set_option( 'hide_the_plugin', $hide_plugin );
			}
			die( json_encode( $information ) );
		}
		die();
	}
}
