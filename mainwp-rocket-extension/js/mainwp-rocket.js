jQuery( document ).ready(function($) {
	$( '.rocket_tabs_lnk a' ).on('click', function() {
		$( '.rocket_tabs_lnk a' ).removeClass( 'mainwp_action_down' );
		$( this ).addClass( 'mainwp_action_down' );
		$( '.mwp_rocket_tabs' ).hide();
		var tab = $( this ).attr( 'tab' );
		$( '#mwp_rocket_tab_' + tab ).show();

		if (tab == 'dashboard') {
			$( '.mwp_rocket_opts_submit' ).hide();
			$( '#mwp_rocket_tab_perform_action' ).hide();
		} else {
			$( '.mwp_rocket_opts_submit' ).show();
			$( '#mwp_rocket_tab_perform_action' ).show();
		}
		return false;
	});

	$( '#mwp_rocket_settings_save_btn' ).on('click', function() {
		var statusEl = jQuery( '#mwp_rocket_perform_individual_status .status' );
		var loaderEl = jQuery( '#mwp_rocket_perform_individual_status i' );
		statusEl.hide();
		loaderEl.show();
		data = {
			action: 'mainwp_rocket_site_override_settings',
			wprocketRequestSiteID: $( 'input[name=mwp_rocket_settings_site_id]' ).val(),
			override: $( '#mainwp_rocket_override_general_settings' ).is( ":checked" ) ? 1 : 0,
			_wprocketNonce: mainwp_rocket_loc.nonce
		};
		jQuery.post(ajaxurl, data, function(response) {
			loaderEl.hide();
			if (response) {
				if (response.error) {
					statusEl.css( 'color', 'red' );
					statusEl.html( response.error );
				} else if (response.result == 'SUCCESS') {
					statusEl.css( 'color', '#21759B' );
					statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Saved' ) );
					setTimeout(function() {
						statusEl.fadeOut();						
					}, 3000);
				} else {
					statusEl.css( 'color', 'red' );
					statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
				}
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
			statusEl.fadeIn();
		}, 'json');

		return false;
	});
        
        $( '#mwp_rocket_reload_optimize_info' ).on('click', function() {
		var statusEl = jQuery( '#mwp_reload_optimize_info_status .status' );
		var loaderEl = jQuery( '#mwp_reload_optimize_info_status i' );
                var site_id = $( this ).attr('site-id');
		statusEl.hide();
		loaderEl.show();
		data = {
			action: 'mainwp_rocket_reload_optimize_info',
			wprocketRequestSiteID: site_id,			
			_wprocketNonce: mainwp_rocket_loc.nonce
		};
		jQuery.post(ajaxurl, data, function(response) {
			loaderEl.hide();
			if (response) {
				if (response.error) {
					statusEl.css( 'color', 'red' );
					statusEl.html( response.error );
				} else if (response.result == 'SUCCESS') {
					statusEl.css( 'color', '#21759B' );
					statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
					setTimeout(function() {
                                            statusEl.fadeOut();		
                                            location.href = 'admin.php?page=ManageSitesWPRocket&id=' + site_id + '&tab=database'
					}, 1000);
				} else {
					statusEl.css( 'color', 'red' );
					statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
				}
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
			statusEl.fadeIn();
		}, 'json');

		return false;
	});
        
});


jQuery( document ).ready(function($) {

	jQuery( '#mainwp-rocket-tips .mainwp-rocket-show-tut' ).on('click', function() {
		jQuery( '.mainwp-rocket-tut' ).hide();
		var num = jQuery( this ).attr( 'number' );
		jQuery( '.mainwp-rocket-tut[number="' + num + '"]' ).show();
		mainwp_setCookie( 'mwp_rocket_quick_tut_number', jQuery( this ).attr( 'number' ) );
		return false;
	});

	jQuery( '#mainwp-rocket-quick-start-guide' ).on('click', function() {
		if (mainwp_getCookie( 'mwp_rocket_quick_guide' ) == 'on') {
			mainwp_setCookie( 'mwp_rocket_quick_guide', '' );
		} else {
			mainwp_setCookie( 'mwp_rocket_quick_guide', 'on' );
		}
		mwp_rocket_showhide_quick_guide();
		return false;
	});
	jQuery( '#mainwp-rocket-tips-dismiss' ).on('click', function() {
		mainwp_setCookie( 'mwp_rocket_quick_guide', '' );
		mwp_rocket_showhide_quick_guide();
		return false;
	});
	mwp_rocket_showhide_quick_guide();
});

mwp_rocket_showhide_quick_guide = function() {
	var show = mainwp_getCookie( 'mwp_rocket_quick_guide' );
	if (show == 'on') {
		jQuery( '#mainwp-rocket-tips' ).show();
		jQuery( '#mainwp-rocket-quick-start-guide' ).hide();
		mwp_rocket_showhide_quick_tut();
	} else {
		jQuery( '#mainwp-rocket-tips' ).hide();
		jQuery( '#mainwp-rocket-quick-start-guide' ).show();
	}
}

mwp_rocket_showhide_quick_tut = function() {
	var tut = mainwp_getCookie( 'mwp_rocket_quick_tut_number' );
	jQuery( '.mainwp-rocket-tut' ).hide();
	jQuery( '.mainwp-rocket-tut[number="' + tut + '"]' ).show();
}

jQuery( document ).ready(function($) {
	$( '.wprocket_plugin_upgrade_noti_dismiss' ).live('click', function() {
		var parent = $( this ).closest( '.ext-upgrade-noti' );
		parent.hide();
		var data = {
			action: 'mainwp_wprocket_upgrade_noti_dismiss',
			wprocketRequestSiteID: parent.attr( 'website-id' ),
		}
		jQuery.post(ajaxurl, data, function(response) {

		});
		return false;
	});

	$( '.wprocket_plugin_version_noti_dismiss' ).live('click', function() {
		var parent = $( this ).closest( '.extra-message' );
		parent.hide();
		var data = {
			action: 'mainwp_wprocket_version_noti_dismiss',
			wprocketRequestSiteID: parent.attr( 'website-id' )
		}

		jQuery.post(ajaxurl, data, function(response) {

		});
		return false;
	});

	$( '.mwp_rocket_active_plugin' ).on('click', function() {
		mainwp_rocket_plugin_active_start_specific( $( this ), false );
		return false;
	});

	$( '.mwp_rocket_upgrade_plugin' ).on('click', function() {
		mainwp_rocket_plugin_upgrade_start_specific( $( this ), false );
		return false;
	});

	$( '.mwp_rocket_showhide_plugin' ).on('click', function() {
		mainwp_rocket_plugin_showhide_start_specific( $( this ), false );
		return false;
	});

	$( '#wprocket_plugin_doaction_btn' ).on('click', function() {
		var bulk_act = $( '#mwp_rocket_plugin_action' ).val();
		mainwp_rocket_plugin_do_bulk_action( bulk_act );
	});

});


var rocket_bulkMaxThreads = 3;
var rocket_bulkTotalThreads = 0;
var rocket_bulkCurrentThreads = 0;
var rocket_bulkFinishedThreads = 0;

mainwp_rocket_plugin_do_bulk_action = function(act) {
	var selector = '';
	switch (act) {
		case 'activate-selected':
			selector = '#the-mwp-rocket-list tr.plugin-update-tr .mwp_rocket_active_plugin';
			jQuery( selector ).addClass( 'queue' );
			mainwp_rocket_plugin_active_start_next( selector );
			break;
		case 'update-selected':
			selector = '#the-mwp-rocket-list tr.plugin-update-tr .mwp_rocket_upgrade_plugin';
			jQuery( selector ).addClass( 'queue' );
			mainwp_rocket_plugin_upgrade_start_next( selector );
			break;
		case 'hide-selected':
			selector = '#the-mwp-rocket-list tr .mwp_rocket_showhide_plugin[showhide="hide"]';
			jQuery( selector ).addClass( 'queue' );
			mainwp_rocket_plugin_showhide_start_next( selector );
			break;
		case 'show-selected':
			selector = '#the-mwp-rocket-list tr .mwp_rocket_showhide_plugin[showhide="show"]';
			jQuery( selector ).addClass( 'queue' );
			mainwp_rocket_plugin_showhide_start_next( selector );
			break;
		case 'load-settings':
			selector = '#the-mwp-rocket-list tr .its-action-working';
			jQuery( selector ).addClass( 'queue' );
			mainwp_rocket_load_settings_start_next( selector );
			break;
	}
}

mainwp_rocket_load_settings_start_next = function(selector) {
	while ((objProcess = jQuery( selector + '.queue:first' )) && (objProcess.length > 0) && (rocket_bulkCurrentThreads < rocket_bulkMaxThreads)) {
		objProcess.removeClass( 'queue' );
		if (objProcess.closest( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length == 0) {
			continue;
		}
		mainwp_rocket_load_settings_start_specific( objProcess, selector );
	}
}


mainwp_rocket_load_settings_start_specific = function(pObj, selector) {
	var parent = pObj.closest( 'tr' );
	var loader = parent.find( '.its-action-working .loading' );
	var statusEl = parent.find( '.its-action-working .status' );

		rocket_bulkCurrentThreads++;
	var data = {
		action: 'mainwp_rocket_site_load_existing_settings',
		wprocketRequestSiteID: parent.attr( 'website-id' ),
		_wprocketNonce: mainwp_rocket_loc.nonce
	}
	statusEl.hide();
	loader.show();
	jQuery.post(ajaxurl, data, function(response) {
		loader.hide();
		if (response && response['error']) {
			statusEl.css( 'color', 'red' );
			statusEl.html( response['error'] ).show();
		} else if (response && response['result'] == 'SUCCESS') {
			statusEl.css( 'color', '#21759B' );
			statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) ).show();
			statusEl.fadeOut( 3000 );
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + __( "Undefined Error" ) ).show();
		}
				rocket_bulkCurrentThreads--;
				rocket_bulkFinishedThreads++;
				mainwp_rocket_load_settings_start_next( selector );
	}, 'json');
	return false;
}


mainwp_rocket_plugin_showhide_start_next = function(selector) {
	while ((objProcess = jQuery( selector + '.queue:first' )) && (objProcess.length > 0) && (rocket_bulkCurrentThreads < rocket_bulkMaxThreads)) {
		objProcess.removeClass( 'queue' );
		if (objProcess.closest( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length == 0) {
			continue;
		}
		mainwp_rocket_plugin_showhide_start_specific( objProcess, true, selector );
	}
}

mainwp_rocket_plugin_showhide_start_specific = function(pObj, bulk, selector) {
	var parent = pObj.closest( 'tr' );
	var loader = parent.find( '.its-action-working .loading' );
	var statusEl = parent.find( '.its-action-working .status' );
	var showhide = pObj.attr( 'showhide' );
	var pluginName = parent.attr( 'plugin-name' );
	if (bulk) {
		rocket_bulkCurrentThreads++;
	}

	var data = {
		action: 'mainwp_wprocket_showhide_plugin',
		wprocketRequestSiteID: parent.attr( 'website-id' ),
		showhide: showhide
	}
	statusEl.hide();
	loader.show();
	jQuery.post(ajaxurl, data, function(response) {
		loader.hide();
		pObj.removeClass( 'queue' );
		if (response && response['error']) {
			statusEl.css( 'color', 'red' );
			statusEl.html( response['error'] ).show();
		} else if (response && response['result'] == 'SUCCESS') {
			if (showhide == 'show') {
				pObj.text( "Hide " + pluginName + " Plugin" );
				pObj.attr( 'showhide', 'hide' );
				parent.find( '.wprocket_hidden_title' ).html( __( 'No' ) );
			} else {
				pObj.text( "Show " + pluginName + " Plugin" );
				pObj.attr( 'showhide', 'show' );
				parent.find( '.wprocket_hidden_title' ).html( __( 'Yes' ) );
			}

			statusEl.css( 'color', '#21759B' );
			statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) ).show();
			statusEl.fadeOut( 3000 );
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + __( "Undefined Error" ) ).show();
		}

		if (bulk) {
			rocket_bulkCurrentThreads--;
			rocket_bulkFinishedThreads++;
			mainwp_rocket_plugin_showhide_start_next( selector );
		}

	}, 'json');
	return false;
}

mainwp_rocket_plugin_upgrade_start_next = function(selector) {
	while ((objProcess = jQuery( selector + '.queue:first' )) && (objProcess.length > 0) && (objProcess.closest( 'tr' ).prev( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length > 0) && (rocket_bulkCurrentThreads < rocket_bulkMaxThreads)) {
		objProcess.removeClass( 'queue' );
		if (objProcess.closest( 'tr' ).prev( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length == 0) {
			continue;
		}
		mainwp_rocket_plugin_upgrade_start_specific( objProcess, true, selector );
	}
}

mainwp_rocket_plugin_upgrade_start_specific = function(pObj, bulk, selector) {
	var parent = pObj.closest( '.ext-upgrade-noti' );
	var workingRow = parent.find( '.mwp-rocket-row-working' );
	var slug = parent.attr( 'plugin-slug' );
	workingRow.find( '.status' ).html( '' );
	var data = {
		action: 'mainwp_rocket_upgrade_plugin',
		wprocketRequestSiteID: parent.attr( 'website-id' ),
		type: 'plugin',
		'slugs[]': slug
	}

	if (bulk) {
		rocket_bulkCurrentThreads++;
	}

	parent.closest( 'tr' ).show();
	workingRow.find( 'i' ).show();
	jQuery.post(ajaxurl, data, function(response) {
		workingRow.find( 'i' ).hide();
		pObj.removeClass( 'queue' );
		if (response && response['error']) {
			workingRow.find( '.status' ).html( '<font color="red">' + response['error'] + '</font>' );
		} else if (response && response['upgrades'][slug]) {
			pObj.after( 'WP Rocket plugin has been updated' );
			pObj.remove();
		} else {
			workingRow.find( '.status' ).html( '<font color="red">' + '<i class="fa fa-exclamation-circle"></i> ' + __( "Undefined Error" ) + '</font>' );
		}

		if (bulk) {
			rocket_bulkCurrentThreads--;
			rocket_bulkFinishedThreads++;
			mainwp_rocket_plugin_upgrade_start_next( selector );
		}

	}, 'json');
	return false;
}

mainwp_rocket_plugin_active_start_next = function(selector) {
	while ((objProcess = jQuery( selector + '.queue:first' )) && (objProcess.length > 0) && (objProcess.closest( 'tr' ).prev( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length > 0) && (rocket_bulkCurrentThreads < rocket_bulkMaxThreads)) {
		objProcess.removeClass( 'queue' );
		if (objProcess.closest( 'tr' ).prev( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length == 0) {
			continue;
		}
		mainwp_rocket_plugin_active_start_specific( objProcess, true, selector );
	}
}

mainwp_rocket_plugin_active_start_specific = function(pObj, bulk, selector) {
	var parent = pObj.closest( '.ext-upgrade-noti' );
	var workingRow = parent.find( '.mwp-rocket-row-working' );
	var slug = parent.attr( 'plugin-slug' );
	var data = {
		action: 'mainwp_rocket_active_plugin',
		wprocketRequestSiteID: parent.attr( 'website-id' ),
		'plugins[]': [slug]
	}

	if (bulk) {
		rocket_bulkCurrentThreads++;
	}

	workingRow.find( 'i' ).show();
	workingRow.find( '.status' ).html( '' );
	jQuery.post(ajaxurl, data, function(response) {
		workingRow.find( 'i' ).hide();
		pObj.removeClass( 'queue' );
		if (response && response['error']) {
			workingRow.find( '.status' ).html( '<font color="red">' + response['error'] + '</font>' );
		} else if (response && response['result']) {
			pObj.after( 'WP Rocket plugin has been activated' );
			pObj.remove();
		}
		if (bulk) {
			rocket_bulkCurrentThreads--;
			rocket_bulkFinishedThreads++;
			mainwp_rocket_plugin_active_start_next( selector );
		}

	}, 'json');
	return false;
}

mainwp_rocket_individual_perform_action = function(pSiteId, action) {
	if (action == 'save_opts_child_sites') {
            mainwp_rocket_individual_save_opts_to_site( pSiteId );
	} else if (action == 'optimize_database') {
            mainwp_rocket_individual_optimize_database_on_site( pSiteId );
        }
}




mainwp_rocket_individual_save_opts_to_site = function(pSiteId) {
	var statusEl = jQuery( '#mwp_rocket_perform_individual_status .status' );
	var loaderEl = jQuery( '#mwp_rocket_perform_individual_status i' );
	loaderEl.show();
	statusEl.html( __( 'Saving settings to child site ...' ) ).show();
	data = {
		action: 'mainwp_rocket_save_opts_to_child_site',
		wprocketRequestSiteID: pSiteId,
		_wprocketNonce: mainwp_rocket_loc.nonce,
		individual: 1
	};

	jQuery.post(ajaxurl, data, function(response) {
		loaderEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful.' ) );
				jQuery( '#mwp_rocket_options_message' ).html( __( '<strong>Warning</strong>: Since the WP Rocket plugin is making changes in your htaccess file, you should make sure that everything is ok by visiting your site.' ) ).show();
				setTimeout(function() {
					statusEl.fadeOut();
				}, 3000);
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		statusEl.show();
	}, 'json');
}


mainwp_rocket_individual_optimize_database_on_site = function(pSiteId) {
        var statusEl = jQuery( '#mwp_rocket_perform_individual_status .status' );
	var loaderEl = jQuery( '#mwp_rocket_perform_individual_status i' );
	loaderEl.show();
	statusEl.html( __( 'Optimize Database on child site ...' ) ).show();
	data = {
		action: 'mainwp_rocket_optimize_database_on_child_site',
		wprocketRequestSiteID: pSiteId,
		_wprocketNonce: mainwp_rocket_loc.nonce,
		individual: 1
	};

	jQuery.post(ajaxurl, data, function(response) {
		loaderEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful.' ) );				
				setTimeout(function() {
					statusEl.fadeOut();
				}, 3000);
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		statusEl.show();
	}, 'json');
}


mainwp_rocket_individual_purge_cloudflare = function(pSiteId) {
	var statusEl = jQuery( '#mwp_rocket_individual_purge_cloudflare_status .status' );
	var loaderEl = jQuery( '#mwp_rocket_individual_purge_cloudflare_status i' );
	loaderEl.show();
	statusEl.hide();
	data = {
		action: 'mainwp_rocket_purge_cloudflare',
		wprocketRequestSiteID: pSiteId,
		_wprocketNonce: mainwp_rocket_loc.nonce,
		individual: 1
	};

	jQuery.post(ajaxurl, data, function(response) {
		loaderEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
				setTimeout(function() {
					statusEl.fadeOut();
				}, 3000);
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		statusEl.show();
	}, 'json');
}


mainwp_rocket_individual_purge_opcache = function(pSiteId) {
	var statusEl = jQuery( '#mwp_rocket_individual_purge_opcache_status .status' );
	var loaderEl = jQuery( '#mwp_rocket_individual_purge_opcache_status i' );
	loaderEl.show();
	statusEl.hide();
	data = {
		action: 'mainwp_rocket_purge_opcache',
		wprocketRequestSiteID: pSiteId,
		_wprocketNonce: mainwp_rocket_loc.nonce,
		individual: 1
	};

	jQuery.post(ajaxurl, data, function(response) {
		loaderEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
				setTimeout(function() {
					statusEl.fadeOut();
				}, 3000);
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		statusEl.show();
	}, 'json');
}


mainwp_rocket_individual_purge_all = function(pSiteId) {
	var statusEl = jQuery( '#mwp_rocket_individual_purge_all_status .status' );
	var loaderEl = jQuery( '#mwp_rocket_individual_purge_all_status i' );
	loaderEl.show();
	statusEl.hide();
	data = {
		action: 'mainwp_rocket_purge_cache_all',
		wprocketRequestSiteID: pSiteId,
		_wprocketNonce: mainwp_rocket_loc.nonce,
		individual: 1
	};
	jQuery.post(ajaxurl, data, function(response) {
		loaderEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
				setTimeout(function() {
					statusEl.fadeOut();
				}, 3000);
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		statusEl.show();
	}, 'json');
}



mainwp_rocket_dashboard_tab_purge_all = function(pSiteId, pObj) {
	var row = jQuery( 'tr[website-id=' + pSiteId + ']' )
	var statusEl = row.find( '.its-action-working .status' );
	var loaderEl = row.find( '.its-action-working .loading' );

	loaderEl.show();
	statusEl.hide();
	data = {
		action: 'mainwp_rocket_purge_cache_all',
		wprocketRequestSiteID: pSiteId,
		_wprocketNonce: mainwp_rocket_loc.nonce,
		where: 'dashboard_tab'
	};

	jQuery.post(ajaxurl, data, function(response) {
		loaderEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
				setTimeout(function() {
					statusEl.fadeOut();
				}, 3000);
				jQuery( pObj ).closest( 'tr' ).fadeOut( 1000 );
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		statusEl.show();
	}, 'json');
}

mainwp_rocket_individual_preload_cache = function(pSiteId) {
	var statusEl = jQuery( '#mwp_rocket_individual_preload_status .status' );
	var loaderEl = jQuery( '#mwp_rocket_individual_preload_status i' );
	loaderEl.show();
	statusEl.hide();
	data = {
		action: 'mainwp_rocket_preload_cache',
		wprocketRequestSiteID: pSiteId,
		_wprocketNonce: mainwp_rocket_loc.nonce,
		individual: 1
	};
	jQuery.post(ajaxurl, data, function(response) {
		loaderEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
				setTimeout(function() {
					statusEl.fadeOut();
				}, 3000);
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		statusEl.show();
	}, 'json');
}

mainwp_rocket_individual_download_options = function(pSiteId) {
	var statusEl = jQuery( '#mwp_rocket_individual_download_options_status .status' );
	var loaderEl = jQuery( '#mwp_rocket_individual_download_options_status i' );
	loaderEl.show();
	statusEl.hide();
	data = {
		action: 'mainwp_rocket_download_options',
		wprocketRequestSiteID: pSiteId,
		_wprocketNonce: mainwp_rocket_loc.nonce,
		individual: 1
	};
	jQuery.post(ajaxurl, data, function(response) {
		loaderEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				var redirectWindow = window.open( 'admin-post.php?action=mainwp_rocket_export&id=' + pSiteId + '&_wpnonce=' + mainwp_rocket_loc.nonce, '_blank' );
				redirectWindow.location;
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		statusEl.show();
	}, 'json');
}


mainwp_rocket_perform_action_start_next = function(action) {
	while ((objProcess = jQuery( '.siteItemProcess[status=queue]:first' )) && (objProcess.length > 0) && (rocket_bulkCurrentThreads < rocket_bulkMaxThreads)) {
		objProcess.attr( 'status', 'processed' );
		if (action == 'purge_cloudflare') {
			mainwp_rocket_purge_cloudflare_start_specific( objProcess );
		} else if (action == 'purge_cache_all') {
			mainwp_rocket_purge_cache_all_start_specific( objProcess, false );
		} else if (action == 'preload_cache') {
			mainwp_rocket_preload_cache_start_specific( objProcess, false );
		} else if (action == 'save_opts_child_sites') {
			mainwp_rocket_save_opts_child_sites_start_specific( objProcess );
		} else if (action == 'optimize_database') {
			mainwp_rocket_optimize_database_child_sites_start_specific( objProcess );
		} else if (action == 'purge_opcache') {
			mainwp_rocket_purge_opcache_start_specific( objProcess );
		}
	}
	if (rocket_bulkFinishedThreads > 0 && rocket_bulkFinishedThreads == rocket_bulkTotalThreads) {
            var msg = __('Performed action completed');
            if (action != 'optimize_database') 
                msg += __('<br/><strong>Warning:</strong> Since the WP Rocket plugin is making changes in your htaccess file, you should make sure that everything is ok by visiting your site.');                
            jQuery( '#mainwp_rocket_settings #mwp_rocket_tab_perform_action' ).append( '<div class="mainwp_info-box">' + msg + '</div>' + '<p><a class="button-primary" href="admin.php?page=Extensions-Mainwp-Rocket-Extension&tab=dashboard">Return to Options</a></p>' );            
	}
}


mainwp_rocket_rightnow_perform_action_start_next = function(action) {
	while ((objProcess = jQuery( '#mwp_rocket_rightnow_load_sites .siteItemProcess[status=queue]:first' )) && (objProcess.length > 0) && (rocket_bulkCurrentThreads < rocket_bulkMaxThreads)) {
		objProcess.attr( 'status', 'processed' );
		if (action == 'purge_cache_all') {
			mainwp_rocket_purge_cache_all_start_specific( objProcess, true );
		} else if (action == 'preload_cache') {
			mainwp_rocket_preload_cache_start_specific( objProcess, true );
		}
	}
	if (rocket_bulkFinishedThreads > 0 && rocket_bulkFinishedThreads == rocket_bulkTotalThreads) {
		jQuery( '#mwp_rocket_rightnow_load_sites' ).append( '<div class="mainwp_info-box">' + __( "Performed action completed." ) + '</div>' );
	}
}


mainwp_rocket_purge_cloudflare_start_specific = function(objProcess) {
	var loadingEl = objProcess.find( 'i' );
	var statusEl = objProcess.find( '.status' );
	rocket_bulkCurrentThreads++;
	var data = {
		action: 'mainwp_rocket_purge_cloudflare',
		_wprocketNonce: mainwp_rocket_loc.nonce,
		wprocketRequestSiteID: objProcess.attr( 'site-id' ),
		individual: 0
	};
	statusEl.html( '' );
	loadingEl.show();

	jQuery.post(ajaxurl, data, function(response) {
		loadingEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		rocket_bulkCurrentThreads--;
		rocket_bulkFinishedThreads++;
		mainwp_rocket_perform_action_start_next( 'purge_cloudflare' );
	}, 'json');
}

mainwp_rocket_purge_opcache_start_specific = function(objProcess) {
	var loadingEl = objProcess.find( 'i' );
	var statusEl = objProcess.find( '.status' );
	rocket_bulkCurrentThreads++;
	var data = {
		action: 'mainwp_rocket_purge_opcache',
		_wprocketNonce: mainwp_rocket_loc.nonce,
		wprocketRequestSiteID: objProcess.attr( 'site-id' ),
		individual: 0
	};
	statusEl.html( '' );
	loadingEl.show();

	jQuery.post(ajaxurl, data, function(response) {
		loadingEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		rocket_bulkCurrentThreads--;
		rocket_bulkFinishedThreads++;
		mainwp_rocket_perform_action_start_next( 'purge_opcache' );
	}, 'json');
}


mainwp_rocket_purge_cache_all_start_specific = function(objProcess, rightNow) {
	var loadingEl = objProcess.find( 'i' );
	var statusEl = objProcess.find( '.status' );
	rocket_bulkCurrentThreads++;
	var data = {
		action: 'mainwp_rocket_purge_cache_all',
		_wprocketNonce: mainwp_rocket_loc.nonce,
		wprocketRequestSiteID: objProcess.attr( 'site-id' )
	};
	if ( ! rightNow) {
		data['individual'] = 0;
	}
	statusEl.html( '' );
	loadingEl.show();

	jQuery.post(ajaxurl, data, function(response) {
		loadingEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		rocket_bulkCurrentThreads--;
		rocket_bulkFinishedThreads++;
		if ( ! rightNow) {
			mainwp_rocket_perform_action_start_next( 'purge_cache_all' ); } else {
			mainwp_rocket_rightnow_perform_action_start_next( 'purge_cache_all' ); }
	}, 'json');
}

mainwp_rocket_preload_cache_start_specific = function(objProcess, rightNow) {
	var loadingEl = objProcess.find( 'i' );
	var statusEl = objProcess.find( '.status' );
	rocket_bulkCurrentThreads++;
	var data = {
		action: 'mainwp_rocket_preload_cache',
		_wprocketNonce: mainwp_rocket_loc.nonce,
		wprocketRequestSiteID: objProcess.attr( 'site-id' ),
	};
	if ( ! rightNow) {
		data['individual'] = 0;
	}
	statusEl.html( '' );
	loadingEl.show();

	jQuery.post(ajaxurl, data, function(response) {
		loadingEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		rocket_bulkCurrentThreads--;
		rocket_bulkFinishedThreads++;
		if ( ! rightNow) {
			mainwp_rocket_perform_action_start_next( 'purge_cache_all' ); } else {
			mainwp_rocket_rightnow_perform_action_start_next( 'purge_cache_all' ); }

	}, 'json');
}


mainwp_rocket_save_opts_child_sites_start_specific = function(objProcess) {
	var loadingEl = objProcess.find( 'i' );
	var statusEl = objProcess.find( '.status' );
	rocket_bulkCurrentThreads++;
	var data = {
		action: 'mainwp_rocket_save_opts_to_child_site',
		_wprocketNonce: mainwp_rocket_loc.nonce,
		wprocketRequestSiteID: objProcess.attr( 'site-id' ),
		individual: 0
	};
	statusEl.html( '' );
	loadingEl.show();

	jQuery.post(ajaxurl, data, function(response) {
		loadingEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		rocket_bulkCurrentThreads--;
		rocket_bulkFinishedThreads++;
		mainwp_rocket_perform_action_start_next( 'save_opts_child_sites' );
	}, 'json');
}


mainwp_rocket_optimize_database_child_sites_start_specific = function(objProcess) {
	var loadingEl = objProcess.find( 'i' );
	var statusEl = objProcess.find( '.status' );
	rocket_bulkCurrentThreads++;
	var data = {
		action: 'mainwp_rocket_optimize_database_on_child_site',
		_wprocketNonce: mainwp_rocket_loc.nonce,
		wprocketRequestSiteID: objProcess.attr( 'site-id' ),
		individual: 0
	};
	statusEl.html( '' );
	loadingEl.show();

	jQuery.post(ajaxurl, data, function(response) {
		loadingEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		rocket_bulkCurrentThreads--;
		rocket_bulkFinishedThreads++;
		mainwp_rocket_perform_action_start_next( 'optimize_database' );
	}, 'json');
}


mainwp_rocket_rightnow_clearcache_individual = function(siteId) {
	var statusEl = jQuery( '#mwp_rocket_rightnow_working_status .status' );
	var loaderEl = jQuery( '#mwp_rocket_rightnow_working_status i' );
	loaderEl.show();
	statusEl.hide();
	data = {
		action: 'mainwp_rocket_purge_cache_all',
		wprocketRequestSiteID: siteId,
		_wprocketNonce: mainwp_rocket_loc.nonce
	};
	jQuery.post(ajaxurl, data, function(response) {
		loaderEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#ddd' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
				setTimeout(function() {
					statusEl.fadeOut();
				}, 3000);
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		statusEl.show();
	}, 'json');
	return false;
}


mainwp_rocket_rightnow_preloadcache_individual = function(siteId) {
	var statusEl = jQuery( '#mwp_rocket_rightnow_working_status .status' );
	var loaderEl = jQuery( '#mwp_rocket_rightnow_working_status i' );
	loaderEl.show();
	statusEl.hide();
	data = {
		action: 'mainwp_rocket_preload_cache',
		wprocketRequestSiteID: siteId,
		_wprocketNonce: mainwp_rocket_loc.nonce
	};
	jQuery.post(ajaxurl, data, function(response) {
		loaderEl.hide();
		if (response) {
			if (response.error) {
				statusEl.css( 'color', 'red' );
				statusEl.html( response.error );
			} else if (response.message) {
				statusEl.css( 'color', '#ddd' );
				statusEl.html( response.message );
			} else if (response.result == 'SUCCESS') {
				statusEl.css( 'color', '#21759B' );
				statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
				setTimeout(function() {
					statusEl.fadeOut();
				}, 3000);
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
		}
		statusEl.show();
	}, 'json');
	return false;
}

mainwp_rocket_rightnow_loadsites = function(site_ids, action) {
	var statusEl = jQuery( '#mwp_rocket_rightnow_working_status .status' );
	var loaderEl = jQuery( '#mwp_rocket_rightnow_working_status i' );
	jQuery( '#mwp_rocket_rightnow_load_sites' ).html( '' );
	loaderEl.show();
	statusEl.hide();
	data = {
		action: 'mainwp_rocket_rightnow_load_sites',
		wprocketRequestSiteID: site_ids,
		_wprocketNonce: mainwp_rocket_loc.nonce,
		rightnow_action: action,
		hide_title: 1
	};
	jQuery.post(ajaxurl, data, function(response) {
		loaderEl.hide();
		if (response) {
			mainwp_rightnow_rocket_show_box( action );
			jQuery( '#mwp_rocket_rightnow_load_sites' ).html( response ).show();
			rocket_bulkCurrentThreads = 0;
			rocket_bulkFinishedThreads = 0;
			rocket_bulkTotalThreads = jQuery( '#mwp_rocket_rightnow_load_sites .siteItemProcess[status=queue]' ).length;
			if (rocket_bulkTotalThreads > 0) {
				mainwp_rocket_rightnow_perform_action_start_next( action );
			}
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			statusEl.show();
		}
	});
	return false;
}


jQuery( document ).on('click', '#mainwp_managesites_content #doaction', function() {
	var action = jQuery( '#bulk-action-selector-top' ).val();
	if (action == -1) {
            return false; 
        }
        
	if (action == 'clear_wprocket_cache' || action == 'preload_wprocket_cache') {
		if (bulkManageSitesTaskRunning) {
                    return false; 
                }
                
                var msg = 'You are about to clear WP Rocket Cache on the selected sites?';
                if (action == 'preload_wprocket_cache')
                    msg = 'You are about to preload WP Rocket Cache on the selected sites?';  
                
                if (!confirm(msg)) {
                    return false;
                }

		managesites_bulk_init();
		bulkManageSitesTotal = jQuery( '#the-list .check-column INPUT:checkbox:checked[status="queue"]' ).length;
		bulkManageSitesTaskRunning = true;

		if (action == 'clear_wprocket_cache') {
			mainwp_managesites_bulk_clear_rocket_cache_next();
		} else if (action == 'preload_wprocket_cache') {
			mainwp_managesites_bulk_preload_rocket_cache_next();
		}
		return false;
	}

});

mainwp_managesites_bulk_clear_rocket_cache_next = function() {
	while ((checkedBox = jQuery( '#the-list .check-column INPUT:checkbox:checked[status="queue"]:first' )) && (checkedBox.length > 0) && (bulkManageSitesCurrentThreads < bulkManageSitesMaxThreads)) {
		mainwp_managesites_bulk_clear_rocket_cache_specific( checkedBox );
	}
	if ((bulkManageSitesTotal > 0) && (bulkManageSitesFinished == bulkManageSitesTotal)) {
		managesites_bulk_done();
		setHtml( '#mainwp_managesites_add_other_message', __( "Bulk Clear WP Rocket Cache finished." ) );
	}
}

mainwp_managesites_bulk_clear_rocket_cache_specific = function(pCheckedBox) {
	pCheckedBox.attr( 'status', 'running' );
	var rowObj = pCheckedBox.closest( 'tr' );
	bulkManageSitesCurrentThreads++;
	var loadingEl = rowObj.find( '.column-site .bulk_running i' );
	var statusEl = rowObj.find( '.column-site .bulk_running .status' );
	loadingEl.show();
	statusEl.hide();
	var data = mainwp_secure_data({
		action: 'mainwp_rocket_purge_cache_all',
		wprocketRequestSiteID: rowObj.attr( 'siteid' ),
		_wprocketNonce: mainwp_rocket_loc.nonce
	});

	jQuery.ajax({
		type: 'POST',
		url: ajaxurl,
		data: data,
		success: function(response) {
			bulkManageSitesCurrentThreads--;
			bulkManageSitesFinished++;
			loadingEl.hide();

			if (response) {
				if (response.error) {
					statusEl.css( 'color', 'red' );
					statusEl.html( response.error );
				} else if (response.message) {
					statusEl.css( 'color', '#ddd' );
					statusEl.html( response.message );
				} else if (response.result == 'SUCCESS') {
					statusEl.css( 'color', '#21759B' );
					statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
					setTimeout(function() {
						statusEl.fadeOut();
					}, 3000);
				} else {
					statusEl.css( 'color', 'red' );
					statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
				}
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
			statusEl.show();

			mainwp_managesites_bulk_clear_rocket_cache_next();
		},
		dataType: 'json'
	});
	return false;
};


mainwp_managesites_bulk_preload_rocket_cache_next = function() {
	while ((checkedBox = jQuery( '#the-list .check-column INPUT:checkbox:checked[status="queue"]:first' )) && (checkedBox.length > 0) && (bulkManageSitesCurrentThreads < bulkManageSitesMaxThreads)) {
		mainwp_managesites_bulk_preload_rocket_cache_specific( checkedBox );
	}
	if ((bulkManageSitesTotal > 0) && (bulkManageSitesFinished == bulkManageSitesTotal)) {
		managesites_bulk_done();
		setHtml( '#mainwp_managesites_add_other_message', __( "Bulk Clear WP Rocket Cache finished." ) );
	}
}

mainwp_managesites_bulk_preload_rocket_cache_specific = function(pCheckedBox) {
	pCheckedBox.attr( 'status', 'running' );
	var rowObj = pCheckedBox.closest( 'tr' );
	bulkManageSitesCurrentThreads++;
	var loadingEl = rowObj.find( '.column-site .bulk_running i' );
	var statusEl = rowObj.find( '.column-site .bulk_running .status' );
	loadingEl.show();
	statusEl.hide();
	var data = mainwp_secure_data({
		action: 'mainwp_rocket_preload_cache',
		wprocketRequestSiteID: rowObj.attr( 'siteid' ),
		_wprocketNonce: mainwp_rocket_loc.nonce
	});

	jQuery.ajax({
		type: 'POST',
		url: ajaxurl,
		data: data,
		success: function(response) {
			bulkManageSitesCurrentThreads--;
			bulkManageSitesFinished++;
			loadingEl.hide();

			if (response) {
				if (response.error) {
					statusEl.css( 'color', 'red' );
					statusEl.html( response.error );
				} else if (response.message) {
					statusEl.css( 'color', '#ddd' );
					statusEl.html( response.message );
				} else if (response.result == 'SUCCESS') {
					statusEl.css( 'color', '#21759B' );
					statusEl.html( '<i class="fa fa-check-circle"></i> ' + __( 'Successful' ) );
					setTimeout(function() {
						statusEl.fadeOut();
					}, 3000);
				} else {
					statusEl.css( 'color', 'red' );
					statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
				}
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error' );
			}
			statusEl.show();

			mainwp_managesites_bulk_preload_rocket_cache_next();
		},
		dataType: 'json'
	});
	return false;
};

mainwp_rightnow_rocket_show_box = function(action) {
	var rocketBox = jQuery( '#rightnow-rocket-box' );
	var title = (action == 'purge_cache_all' ? __( 'Clearing WP Rocket cache on child sites' ) : __( 'Pre-loading WP Rocket cache on child sites' ));
	rocketBox.attr( 'title', title );
	jQuery( 'div[aria-describedby="rightnow-rocket-box"]' ).find( '.ui-dialog-title' ).html( title );

	rocketBox.dialog({
		resizable: false,
		height: 350,
		width: 500,
		modal: true,
		close: function(event, ui) {
			jQuery( '#rightnow-rocket-box' ).dialog( 'destroy' );
		}
	});

};

jQuery( document ).on('click', '#rightnow-rocket-close', function() {
	jQuery( '#rightnow-rocket-box' ).dialog( 'destroy' );
});
