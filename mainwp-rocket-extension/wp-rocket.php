<?php
/*
Plugin: WP Rocket
Plugin URI: http://www.wp-rocket.me
Description: The best WordPress performance plugin.
Version: 2.6.2
Code Name: Yavin
Author: WP Rocket
Contributors: Jonathan Buttigieg, Julio Potier
Author URI: http://www.wp-rocket.me

Text Domain: rocket
Domain Path: languages

Copyright 2013-2015 WP Rocket
*/

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

// Rocket defines
define( 'MAINWP_WP_ROCKET_VERSION'             , '2.6.4' );
define( 'MAINWP_WP_ROCKET_PRIVATE_KEY'         , '6a0b39ba12013a49abdcace4b5b29be8' );
define( 'MAINWP_WP_ROCKET_SLUG'                , 'mainwp_wp_rocket_settings' );
define( 'MAINWP_ROCKET_GENERAL_SETTINGS'       , 'mainwp_rocket_general_settings' );
define( 'MAINWP_WP_ROCKET_WEB_MAIN'            , 'http://support.wp-rocket.me/' );
define( 'MAINWP_WP_ROCKET_WEB_API'             , MAINWP_WP_ROCKET_WEB_MAIN . 'api/wp-rocket/' );
define( 'MAINWP_WP_ROCKET_WEB_CHECK'           , MAINWP_WP_ROCKET_WEB_MAIN . 'check_update.php' );
define( 'MAINWP_WP_ROCKET_WEB_VALID'           , MAINWP_WP_ROCKET_WEB_MAIN . 'valid_key.php' );
define( 'MAINWP_WP_ROCKET_WEB_INFO'            , MAINWP_WP_ROCKET_WEB_MAIN . 'plugin_information.php' );
define( 'MAINWP_WP_ROCKET_WEB_SUPPORT'         , MAINWP_WP_ROCKET_WEB_MAIN . 'forums/' );
define( 'MAINWP_WP_ROCKET_BOT_URL'             , 'http://bot.wp-rocket.me/launch.php' );
define( 'MAINWP_WP_ROCKET_FILE'                , __FILE__ );
define( 'MAINWP_WP_ROCKET_PATH'                , realpath( plugin_dir_path( MAINWP_WP_ROCKET_FILE ) ) . '/' );
define( 'MAINWP_WP_ROCKET_INC_PATH'            , realpath( MAINWP_WP_ROCKET_PATH . 'inc/' ) . '/' );
define( 'MAINWP_WP_ROCKET_ADMIN_PATH'          , realpath( MAINWP_WP_ROCKET_INC_PATH . 'admin' ) . '/' );
define( 'MAINWP_WP_ROCKET_ADMIN_UI_PATH'       , realpath( MAINWP_WP_ROCKET_ADMIN_PATH . 'ui' ) . '/' );
define( 'MAINWP_WP_ROCKET_ADMIN_UI_MODULES_PATH', realpath( MAINWP_WP_ROCKET_ADMIN_UI_PATH . 'modules' ) . '/' );
define( 'MAINWP_WP_ROCKET_FUNCTIONS_PATH'      , realpath( MAINWP_WP_ROCKET_INC_PATH . 'functions' ) . '/' );

define( 'MAINWP_WP_ROCKET_VENDORS_PATH'      	, realpath( MAINWP_WP_ROCKET_INC_PATH . 'vendors' ) . '/' );
define( 'MAINWP_WP_ROCKET_CONFIG_PATH'         , WP_CONTENT_DIR . '/wp-rocket-config/' );
define( 'MAINWP_WP_ROCKET_CACHE_PATH'          , WP_CONTENT_DIR . '/cache/wp-rocket/' );
define( 'MAINWP_WP_ROCKET_MINIFY_CACHE_PATH'   , WP_CONTENT_DIR . '/cache/min/' );
define( 'MAINWP_WP_ROCKET_URL'                 , plugin_dir_url( MAINWP_WP_ROCKET_FILE ) );
define( 'MAINWP_WP_ROCKET_INC_URL'             , MAINWP_WP_ROCKET_URL . 'inc/' );
define( 'MAINWP_WP_ROCKET_FRONT_URL'           , MAINWP_WP_ROCKET_INC_URL . 'front/' );
define( 'MAINWP_WP_ROCKET_FRONT_JS_URL'        , MAINWP_WP_ROCKET_FRONT_URL . 'js/' );
define( 'MAINWP_WP_ROCKET_LAB_JS_VERSION'      , '2.0.3' );
define( 'MAINWP_WP_ROCKET_LAZYLOAD_JS_VERSION' , '1.0.2' );
define( 'MAINWP_WP_ROCKET_ADMIN_URL'           , MAINWP_WP_ROCKET_INC_URL . 'admin/' );
define( 'MAINWP_WP_ROCKET_ADMIN_UI_URL'        , MAINWP_WP_ROCKET_ADMIN_URL . 'ui/' );
define( 'MAINWP_WP_ROCKET_ADMIN_UI_JS_URL'     , MAINWP_WP_ROCKET_ADMIN_UI_URL . 'js/' );
define( 'MAINWP_WP_ROCKET_ADMIN_UI_CSS_URL'    , MAINWP_WP_ROCKET_ADMIN_UI_URL . 'css/' );
define( 'MAINWP_WP_ROCKET_ADMIN_UI_IMG_URL'    , MAINWP_WP_ROCKET_ADMIN_UI_URL . 'img/' );
define( 'MAINWP_WP_ROCKET_CACHE_URL'           , WP_CONTENT_URL . '/cache/wp-rocket/' );
define( 'MAINWP_WP_ROCKET_MINIFY_CACHE_URL'    , WP_CONTENT_URL . '/cache/min/' );
if ( ! defined( 'CHMOD_MAINWP_WP_ROCKET_CACHE_DIRS' ) ) {
	define( 'CHMOD_MAINWP_WP_ROCKET_CACHE_DIRS', 0755 );
}
if ( ! defined( 'MAINWP_WP_ROCKET_LASTVERSION' ) ) {
	define( 'MAINWP_WP_ROCKET_LASTVERSION', '2.5.12' );
}

require( MAINWP_WP_ROCKET_INC_PATH	. 'compat.php' );

/*
 * Tell WP what to do when plugin is loaded
 *
 * @since 1.0
 */
add_action( 'plugins_loaded', 'mainwp_rocket_init' );
function mainwp_rocket_init() {

	// Load translations
	load_plugin_textdomain( 'rocket', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

	// Nothing to do if autosave
	if ( defined( 'DOING_AUTOSAVE' ) ) {
		return;
	}

	// Necessary to call correctly WP Rocket Bot for cache json
	global $do_rocket_bot_cache_json;
	$do_rocket_bot_cache_json = false;

	// Call defines, classes and functions
	require( MAINWP_WP_ROCKET_FUNCTIONS_PATH . 'options.php' );

	// Call defines,  classes and functions
	require( MAINWP_WP_ROCKET_FUNCTIONS_PATH	. 'admin.php' );
	require( MAINWP_WP_ROCKET_FUNCTIONS_PATH	. 'formatting.php' );
	require( MAINWP_WP_ROCKET_FUNCTIONS_PATH	. 'i18n.php' );
	//    require( dirname( __FILE__ )      . '/licence-data.php' );

	if ( is_admin() ) {
		require( MAINWP_WP_ROCKET_ADMIN_PATH . 'upgrader.php' );
		require( MAINWP_WP_ROCKET_ADMIN_PATH . 'updater.php' );
		require( MAINWP_WP_ROCKET_ADMIN_PATH . 'class-repeater-field.php' );
		require( MAINWP_WP_ROCKET_ADMIN_PATH . 'options.php' );
		require( MAINWP_WP_ROCKET_ADMIN_PATH . 'admin.php' );
		require( MAINWP_WP_ROCKET_ADMIN_UI_PATH . 'enqueue.php' );
		require( MAINWP_WP_ROCKET_ADMIN_UI_PATH . 'meta-boxes.php' );
	}

}
