<?php
defined( 'ABSPATH' ) or	die( 'Cheatin&#8217; uh?' );

add_action( 'admin_post_mainwp_rocket_export', '__mainwp_rocket_do_options_export' );
function __mainwp_rocket_do_options_export()
{        
	if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'mainwp_rocket_export' ) ) {
		return;
	}
        
        $options = null;
        if (isset($_GET['id'])) {
            $options = MainWP_Rocket_DB::get_instance()->get_wprocket_settings_by('site_id', $_GET['id']);           
        } else {
            $options = get_option( MAINWP_ROCKET_GENERAL_SETTINGS ); 
        }
        if (empty($options))
            return;

        $filename = sprintf( 'wp-rocket-settings-%s-%s.txt', date( 'Y-m-d' ), uniqid() );
        $gz = 'gz' . strrev( 'etalfed' );
        $options = $gz//;
        ( serialize( $options ), 1 ); 
        nocache_headers();
        @header( 'Content-Type: text/plain' );
        @header( 'Content-Disposition: attachment; filename="' . $filename . '"' );
        @header( 'Content-Transfer-Encoding: binary' );
        @header( 'Content-Length: ' . strlen( $options ) );
        @header( 'Connection: close' );
        echo $options;
        exit();

}


add_action( 'admin_post_mainwp_rocket_optimize_database', '__mainwp_rocket_optimize_database' );
function __mainwp_rocket_optimize_database() {
    if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'mainwp_rocket_optimize_database' ) ) {
        wp_nonce_ays( '' );
    }
	wp_redirect( add_query_arg( array('_perform_action'=> 'mainwp_rocket_optimize_database', '_wpnonce' => wp_create_nonce('mainwp_rocket_optimize_database')) , remove_query_arg( 's', wp_get_referer()) ) );            
    die();
}
