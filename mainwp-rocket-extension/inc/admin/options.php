<?php
defined( 'ABSPATH' ) or	die( 'Cheatin&#8217; uh?' );

function mainwp_rocket_field( $args )
{
	if( ! is_array( reset( $args ) ) ) {
		$args = array( $args );
	}

	$full = $args;

	foreach ( $full as $args ) {
		if ( isset( $args['display'] ) && !$args['display'] ) {
			continue;
		}
        
        if (!is_array($args))
            $args = array();
        		
		$args['label_for'] 	= isset( $args['label_for'] ) ? $args['label_for'] : '';
		$args['name'] 		= isset( $args['name'] ) ? $args['name'] : $args['label_for'];
		$parent 			= isset( $args['parent'] ) ? 'data-parent="' . sanitize_html_class( $args['parent' ] ). '"' : null;
		$placeholder 		= isset( $args['placeholder'] ) ? 'placeholder="'. $args['placeholder'].'" ' : '';
		$class				= isset( $args['class'] ) ? sanitize_html_class( $args['class'] ) : sanitize_html_class( $args['name'] );
		$class 				.= ( $parent ) ? ' has-parent' : null;
		$label 				= isset( $args['label'] ) ? $args['label'] : '';
		$default			= isset( $args['default'] ) ? $args['default'] : '';
		$readonly			= isset( $args['readonly'] ) && $args['readonly'] ? ' readonly="readonly" disabled="disabled"' : '';
		$cols 				= isset( $args['cols'] ) ? (int) $args['cols'] : 50;
		$rows 				= isset( $args['rows'] ) ? (int) $args['rows'] : 5;
		
		if ( ! empty( $args['container_class'] ) ) {
				$args['container_class'] = implode( ' ', array_map( 'sanitize_html_class', $args['container_class'] ) );
			}
			
		if( ! isset( $args['fieldset'] ) || 'start' == $args['fieldset'] ){
			echo '<fieldset class="fieldname-'.sanitize_html_class( $args['name'] ).' fieldtype-'.sanitize_html_class( $args['type'] ).'">';
		}

		switch( $args['type'] ) {
			case 'number' :
			case 'email' :
			case 'text' :

				$value = esc_attr( mainwp_get_rocket_option( $args['name'] ) );
				if ( $value === false ) {
					$value = $default;
				}

				$number_options = $args['type']=='number' ? ' min="0" class="small-text"' : '';
				$autocomplete = in_array( $args['name'], array( 'consumer_key', 'consumer_email' ) ) ? ' autocomplete="off"' : '';
				$disabled = ( 'consumer_key' == $args['name'] && defined( 'MAINWP_WP_ROCKET_KEY' ) ) || ( 'consumer_email' == $args['name'] && defined( 'MAINWP_WP_ROCKET_EMAIL' ) ) ? ' disabled="disabled"' : $readonly;
				?>

					<legend class="screen-reader-text"><span><?php echo $args['label_screen']; ?></span></legend>
					<label><input<?php echo $autocomplete . $disabled; ?> type="<?php echo $args['type']; ?>"<?php echo $number_options; ?> id="<?php echo $args['label_for']; ?>" name="mainwp_wp_rocket_settings[<?php echo $args['name']; ?>]" value="<?php echo $value; ?>" <?php echo $placeholder; ?><?php echo $readonly; ?>/> <?php echo $label; ?></label>

				<?php
			break;

			case 'textarea' :

				$t_temp = mainwp_get_rocket_option( $args['name'], '' );
                
                if ( is_array( $t_temp ) ) {
					$t_temp = implode( "\n" , $t_temp );
				}

				$value = ! empty( $t_temp ) ? esc_textarea( $t_temp ) : '';

				if ( ! $value ) {
					$value = $default;
				}
                
				?>
				<div class="<?php echo $args['container_class']; ?> <?php echo isset( $args['parent'] ) ? 'child-row-' . $args['parent'] : '';  ?>">
					<?php											
					echo !empty( $label ) ? '<p>' . $label . '</p>' : ''; 
					echo isset( $args['sub_label'] ) ? '<div>' . $args['sub_label'] . '</div>'  : '';						
					?>
					<legend class="screen-reader-text"><span><?php echo $args['label_screen']; ?></span></legend>
					<label><textarea id="<?php echo $args['label_for']; ?>" name="mainwp_wp_rocket_settings[<?php echo $args['name']; ?>]" <?php echo $placeholder; ?> cols="<?php echo $cols; ?>" rows="<?php echo $rows; ?>"<?php echo $readonly; ?>><?php echo $value; ?></textarea>
					</label>
					<?php
						$description = isset( $args['description'] ) ? sprintf( '<p class="description help %1$s" %2$s><strong class="screen-reader-text">%3$s</strong> %4$s</p>', '', $parent, _x( 'Note:', 'screen-reader-text', 'rocket' ), $args['description'] ) : '';
						echo apply_filters( 'rocket_help', $description, $args['name'], 'description' );
					?>
				</div>
				<?php
			break;

			case 'checkbox_new' : 				
				$args['value'] = mainwp_get_rocket_option( $args['name'], 0 );
				MainWP_Rocket::get_instance()->checkbox($args);
			break;	
			case 'checkbox' : 
					if ( isset( $args['label_screen'] ) ) {
					?>
						<legend class="screen-reader-text"><span><?php echo $args['label_screen']; ?></span></legend>
					<?php } ?>
					<label><input type="checkbox" id="<?php echo $args['name']; ?>" class="<?php echo $class; ?>" name="mainwp_wp_rocket_settings[<?php echo $args['name']; ?>]" value="1"<?php echo $readonly; ?> <?php checked( mainwp_get_rocket_option( $args['name'], 0 ), 1 ); ?> <?php echo $parent; ?>/> <?php echo $args['label']; ?>
					</label>

			<?php
			break;
			
			case 'select' : ?>

					<legend class="screen-reader-text"><span><?php echo $args['label_screen']; ?></span></legend>
					<label>	<select id="<?php echo $args['name']; ?>" class="<?php echo $class; ?>" name="mainwp_wp_rocket_settings[<?php echo $args['name']; ?>]"<?php echo $readonly; ?> <?php echo $parent; ?>>
							<?php foreach( $args['options'] as $val => $title) { ?>
								<option value="<?php echo $val; ?>" <?php selected( mainwp_get_rocket_option( $args['name'] ), $val ); ?>><?php echo $title; ?></option>
							<?php } ?>
							</select>
					<?php echo $label; ?>
					</label>

			<?php
			break;
			case 'submit_optimize' : ?>

            <input type="submit" name="mainwp_wp_rocket_settings[submit_optimize]" id="rocket_submit_optimize" class="button button-primary" value="<?php _e( 'Save and optimize', 'mainwp-rocket-extension' ); ?>"> <a href="<?php echo wp_nonce_url( admin_url( 'admin-post.php?action=mainwp_rocket_optimize_database' ), 'mainwp_rocket_optimize_database' ); ?>" class="button button-secondary"><?php _e( 'Optimize', 'mainwp-rocket-extension' ); ?></a>
            <?php break;
			case 'repeater' :

				$fields = new MainWP_WP_Rocket_Repeater_Field( $args );
				$fields->render();

				break;

			case 'mainwp_rocket_defered_module' :

					mainwp_rocket_defered_module();

			break;

			case 'helper_description' :

				$description = isset( $args['description'] ) ? sprintf( '<p class="description help %1$s" %2$s><strong class="screen-reader-text">%3$s</strong> %4$s</p>', $class, $parent, _x( 'Note:', 'screen-reader-text', 'rocket' ), $args['description'] ) : '';
				echo apply_filters( 'rocket_help', $description, $args['name'], 'description' );

			break;
        
            case 'helper_performance' :

				$description = isset( $args['description'] ) ? sprintf( '<p class="description help tip--perf %1$s" %2$s><strong class="screen-reader-text">%3$s</strong> <strong>%4$s</strong></p>', $class, $parent, _x( 'Performance tip:', 'screen-reader-text', 'rocket' ), $args['description'] ) : '';
				echo apply_filters( 'rocket_help', $description, $args['name'], 'description' );

			break;
        
            case 'helper_detection' :

				$description = isset( $args['description'] ) ? sprintf( '<p class="description help tip--detect %1$s" %2$s><strong class="screen-reader-text">%3$s</strong> %4$s</p>', $class, $parent, _x( 'Third-party feature detected:', 'screen-reader-text', 'rocket' ), $args['description'] ) : '';
				echo apply_filters( 'rocket_help', $description, $args['name'], 'description' );

			break;
			
            case 'helper_help' :

				$description = isset( $args['description'] ) ? sprintf( '<p class="description help tip--use %1$s" %2$s>%3$s</p>', $class, $parent, $args['description'] ) : '';
				echo apply_filters( 'rocket_help', $description, $args['name'], 'help' );

			break;
        
			case 'helper_warning' :

				$description = isset( $args['description'] ) ? sprintf( '<p class="description warning file-error %1$s" %2$s><strong class="screen-reader-text">%3$s</strong> %4$s</p>', $class, $parent, _x( 'Warning:', 'screen-reader-text', 'rocket' ), $args['description'] ) : '';
				echo apply_filters( 'rocket_help', $description, $args['name'], 'warning' );

			break;
            case 'helper_panel_description' :

				$description = isset( $args['description'] ) ? sprintf( '<div class="rocket-panel-description"><p class="%1$s" %2$s>%3$s</p></div>', $class, $parent, $args['description'] ) : '';
				echo $description;

			break;
        
			case 'rocket_export_form' :
                                if (isset($_GET['id'])) {
                                    $extra = "&id=" . $_GET['id'];                                                                   
                                } else {
                                    $extra = "";
                                }
                                
                                $download_url = wp_nonce_url( admin_url( 'admin-post.php?action=mainwp_rocket_export' . $extra), 'mainwp_rocket_export' ); 
                                ?>
				<a href="<?php echo $download_url; ?>" id="export" class="button button-secondary rocketicon"><?php _e( 'Download settings', 'rocket' ); ?></a>
				<?php
			break;

			case 'mainwp_rocket_import_upload_form' :

				mainwp_rocket_import_upload_form( 'rocket_importer' );

			break;
			default : 'Type manquant ou incorrect'; // ne pas traduire

		}

		if( ! isset( $args['fieldset'] ) || 'end' == $args['fieldset'] ) {
			echo '</fieldset>';
		}

	}

}

/**
 * Used to display the defered module on settings form
 *
 * @since 1.1.0
 */
function mainwp_rocket_defered_module()
{ ?>
	<fieldset>
	<legend class="screen-reader-text"><span><?php _e( '<b>JS</b> files with Deferred Loading JavaScript', 'mainwp-rocket-extension' ); ?></span></legend>

	<div id="rkt-drop-deferred" class="rkt-module mainwp-rkt-module-drop">

		<?php
		$deferred_js_files = mainwp_get_rocket_option( 'deferred_js_files' );
		$deferred_js_wait = mainwp_get_rocket_option( 'deferred_js_wait' );
                
		if( $deferred_js_files ) {

			foreach( $deferred_js_files as $k=>$_url ) {
                                
				$checked = isset( $deferred_js_wait[$k] ) ? checked( $deferred_js_wait[$k], '1', false ) : ''; ?>

				<p class="rkt-module-drag">
					<span class="dashicons dashicons-sort rkt-module-move hide-if-no-js"></span>

					<input style="width: 32em" type="text" placeholder="" class="deferred_js regular-text" name="mainwp_wp_rocket_settings[deferred_js_files][<?php echo $k; ?>]" value="<?php echo esc_url( $_url ); ?>" />

					<label>
						<input type="checkbox" class="deferred_js" name="mainwp_wp_rocket_settings[deferred_js_wait][<?php echo $k; ?>]" value="1" <?php echo $checked; ?>/> <?php _e( 'Wait until this file is loaded?', 'mainwp-rocket-extension' ); ?>
					</label>

					<span class="dashicons dashicons-no mainwp-rkt-module-remove hide-if-no-js"></span>
				</p>
				<!-- .rkt-module-drag -->

			<?php
			}

		} else {
			// If no files yet, use this template inside #rkt-drop-deferred
			?>

			<p class="rkt-module-drag">
				<span class="dashicons dashicons-sort rkt-module-move hide-if-no-js"></span>

				<input style="width: 32em" type="text" placeholder="" class="deferred_js regular-text" name="mainwp_wp_rocket_settings[deferred_js_files][0]" value="" />

				<label>
					<input type="checkbox" class="deferred_js" name="mainwp_wp_rocket_settings[deferred_js_wait][0]" value="1" /> <?php _e( 'Wait until this file is loaded?', 'mainwp-rocket-extension' ); ?>
				</label>
			</p>
			<!-- .rkt-module-drag -->

		<?php } ?>

	</div>
	<!-- .rkt-drop-deferred -->

	<?php // Clone Template ?>

	<div class="rkt-module-model hide-if-js">

		<p class="rkt-module-drag">
			<span class="dashicons dashicons-sort rkt-module-move hide-if-no-js"></span>

			<input style="width: 32em" type="text" placeholder="" class="deferred_js regular-text" name="mainwp_wp_rocket_settings[deferred_js_files][]" value="" />

			<label>
				<input type="checkbox" class="deferred_js" name="mainwp_wp_rocket_settings[deferred_js_wait][]" value="1" /> <?php _e( 'Wait until this file is loaded?', 'mainwp-rocket-extension' ); ?>
			</label>
			<span class="dashicons dashicons-no mainwp-rkt-module-remove hide-if-no-js"></span>
		</p>
		<!-- .rkt-module-drag -->
	</div>
	<!-- .rkt-model-deferred-->

	<p><a href="javascript:void(0)" class="mainwp-rkt-module-clone hide-if-no-js button-secondary"><?php _e( 'Add URL', 'mainwp-rocket-extension' ); ?></a></p>

<?php
}

/**
 * Used to display the CNAMES module on settings form
 *
 * @since 2.1
 */
function mainwp_rocket_cnames_module()
{ ?>
		<legend class="screen-reader-text"><span><?php _e( 'Replace site\'s hostname with:', 'mainwp-rocket-extension' ); ?></span></legend>

		<div id="rkt-cnames" class="rkt-module">

			<?php

			$cnames = mainwp_get_rocket_option( 'cdn_cnames' );
			$cnames_zone = mainwp_get_rocket_option( 'cdn_zone' );

			if( $cnames ) {

				foreach( $cnames as $k=>$_url ) { ?>

				<p>

					<input style="width: 32em" type="text" placeholder="http://" class="regular-text" name="mainwp_wp_rocket_settings[cdn_cnames][<?php echo $k; ?>]" value="<?php echo esc_attr( $_url ); ?>" />

					<label>
						<?php _e( 'reserved for', 'mainwp-rocket-extension' ); ?>
						<select name="mainwp_wp_rocket_settings[cdn_zone][<?php echo $k; ?>]">
							<option value="all" <?php selected( $cnames_zone[$k], 'all' ); ?>><?php _e( 'All files', 'mainwp-rocket-extension' ); ?></option>
							<option value="images" <?php selected( $cnames_zone[$k], 'images' ); ?>><?php _e( 'Images', 'mainwp-rocket-extension' ); ?></option>
							<option value="css_and_js" <?php selected( $cnames_zone[$k], 'css_and_js' ); ?>>CSS & JavaScript</option>
							<option value="js" <?php selected( $cnames_zone[$k], 'js' ); ?>>JavaScript</option>
							<option value="css" <?php selected( $cnames_zone[$k], 'css' ); ?>>CSS</option>
						</select>
					</label>
					<span class="dashicons dashicons-no mainwp-rkt-module-remove hide-if-no-js"></span>

				</p>

				<?php
				}

			} else {

				// If no files yet, use this template inside #rkt-cnames
				?>

				<p>

					<input style="width: 32em" type="text" placeholder="http://" class="regular-text" name="mainwp_wp_rocket_settings[cdn_cnames][]" value="" />

					<label>
						<?php _e( 'reserved for', 'mainwp-rocket-extension' ); ?>
						<select name="mainwp_wp_rocket_settings[cdn_zone][]">
							<option value="all"><?php _e( 'All files', 'mainwp-rocket-extension' ); ?></option>
							<option value="images"><?php _e( 'Images', 'mainwp-rocket-extension' ); ?></option>
							<option value="css_and_js">CSS & JavaScript</option>
							<option value="js">JavaScript</option>
							<option value="css">CSS</option>
						</select>
					</label>

				</p>

			<?php } ?>

		</div>

		<?php // Clone Template ?>
		<div class="rkt-module-model hide-if-js">

			<p>

				<input style="width: 32em" type="text" placeholder="http://" class="regular-text" name="mainwp_wp_rocket_settings[cdn_cnames][]" value="" />

				<label>
					<?php _e( 'reserved for', 'mainwp-rocket-extension' ); ?>
					<select name="mainwp_wp_rocket_settings[cdn_zone][]">
						<option value="all"><?php _e( 'All files', 'mainwp-rocket-extension' ); ?></option>
						<option value="images"><?php _e( 'Images', 'mainwp-rocket-extension' ); ?></option>
						<option value="css_and_js">CSS & JavaScript</option>
						<option value="js">JavaScript</option>
						<option value="css">CSS</option>
					</select>
				</label>
				<span class="dashicons dashicons-no mainwp-rkt-module-remove hide-if-no-js"></span>

			</p>

		</div>

		<p><a href="javascript:void(0)" class="mainwp-rkt-module-clone hide-if-no-js button-secondary"><?php _e( 'Add CNAME', 'mainwp-rocket-extension' ); ?></a></p>

	</fieldset>

<?php
}

/**
 * Used to display buttons on settings form, tools tab
 *
 * @since 1.1.0
 */
function mainwp_rocket_button( $args )
{
	$button       = $args['button'];
	$desc         = isset( $args['helper_description'] ) ? $args['helper_description'] : null;
	$help         = isset( $args['helper_help'] ) ? $args['helper_help'] : null;
	$warning      = isset( $args['helper_warning'] ) ? $args['helper_warning'] : null;
	$id           = isset( $button['button_id'] ) ? sanitize_html_class( $button['button_id'] ) : null;
        $onclick      = isset( $button['onclick'] ) ?  $button['onclick']  : null;
        $working_status       = isset( $button['status'] ) ?  $button['status']  : null;        
                
	$class        = sanitize_html_class( strip_tags( $button['button_label'] ) );
	$button_style = isset( $button['style'] ) ? 'button-'.sanitize_html_class( $button['style'] ) : 'button-secondary';

	if ( ! empty( $help ) ) {
		$help = '<p class="description help '.$class.'">'.$help['description'].'</p>';
	}
	if ( ! empty( $desc ) ) {
		$desc = '<p class="description desc '.$class.'">'.$desc['description'].'</p>';
	}
	if ( ! empty( $warning ) ) {
		$warning = '<p class="description warning file-error '.$class.'"><b>'.__( 'Warning: ', 'mainwp-rocket-extension' ) . '</b>' . $warning['description'].'</p>';
	}
?>
	<fieldset class="fieldname-<?php echo $class; ?> fieldtype-button">
		<?php
		if ( isset( $button['url'] ) ) {
			echo '<a href="' . esc_url( $button['url'] ) . '" ' . (!empty($onclick) ? 'onclick="' . $onclick . '"' : ''). ' id="' . $id . '" class="' . $button_style . ' rocketicon rocketicon-'. $class . '">' . wp_kses_post( $button['button_label'] ) . '</a>&nbsp;';
		} else {
			echo '<button id="' . $id . '" ' . (!empty($onclick) ? 'onclick="' . $onclick . '"' : ''). ' class="' . $button_style . ' rocketicon rocketicon-'. $class . '">' . wp_kses_post( $button['button_label'] ) . '</button>&nbsp;';
		}
		?>
		<?php echo !empty($working_status) ? $working_status : ""?>
		<?php echo apply_filters( 'rocket_help', $desc, sanitize_key( strip_tags( $button['button_label'] ) ), 'description' ); ?>
		<?php echo apply_filters( 'rocket_help', $help, sanitize_key( strip_tags( $button['button_label'] ) ), 'help' ); ?>
		<?php echo apply_filters( 'rocket_help', $warning, sanitize_key( strip_tags( $button['button_label'] ) ), 'warning' ); ?>
	</fieldset>
<?php
}

/**
 * Used to include a file in any tab
 *
 * @since 2.2
 */
function mainwp_rocket_include( $args )
{
	include_once( dirname( __FILE__ ) . '/' . str_replace( '..', '', $args['file'] ) . '.inc.php' );
}

/**
 * Tell to WordPress to be confident with our setting, we are clean!
 *
 * @since 1.0
 */
add_action( 'admin_init', 'mainwp_rocket_register_setting' );
function mainwp_rocket_register_setting()
{
    register_setting( 'mainwp_wp_rocket', MAINWP_WP_ROCKET_SLUG, 'mainwp_rocket_settings_callback' );
}

/**
 * Used to clean and sanitize the settings fields
 *
 * @since 1.0
 */
function mainwp_rocket_settings_callback( $inputs )
{        
	/*
	 * Option : Minification CSS & JS
	 */
	$inputs['minify_css'] = ! empty( $inputs['minify_css'] ) ? 1 : 0;
	$inputs['minify_js']  = ! empty( $inputs['minify_js'] ) ? 1 : 0;

	/*
	 * Option : Purge delay
	 */
	$inputs['purge_cron_interval'] = isset( $inputs['purge_cron_interval'] ) ? (int)$inputs['purge_cron_interval'] : mainwp_get_rocket_option( 'purge_cron_interval' );
	$inputs['purge_cron_unit'] = isset( $inputs['purge_cron_unit'] ) ? $inputs['purge_cron_unit'] : mainwp_get_rocket_option( 'purge_cron_unit' );

        $inputs['remove_query_strings'] = ! empty( $inputs['remove_query_strings'] ) ? 1 : 0;
        
	/*
	 * Option : Prefetch DNS requests
	 */
	if ( ! empty( $inputs['dns_prefetch'] ) ) {
		if ( ! is_array( $inputs['dns_prefetch'] ) ) {
			$inputs['dns_prefetch'] = explode( "\n", $inputs['dns_prefetch'] );
		}
		$inputs['dns_prefetch'] = array_map( 'trim', $inputs['dns_prefetch'] );
		$inputs['dns_prefetch'] = array_map( 'esc_url', $inputs['dns_prefetch'] );
		$inputs['dns_prefetch'] = (array) array_filter( $inputs['dns_prefetch'] );
		$inputs['dns_prefetch'] = array_unique( $inputs['dns_prefetch'] );
	} else {
		$inputs['dns_prefetch'] = array();
	}
        
        
	/*
	 * Option : Empty the cache of the following pages when updating an article
	 */
	if ( ! empty( $inputs['cache_purge_pages'] ) ) {
		if ( ! is_array( $inputs['cache_purge_pages'] ) ) {
			$inputs['cache_purge_pages'] = explode( "\n", $inputs['cache_purge_pages'] );
		}
		$inputs['cache_purge_pages'] = array_map( 'trim', $inputs['cache_purge_pages'] );
		$inputs['cache_purge_pages'] = array_map( 'esc_url', $inputs['cache_purge_pages'] );
		$inputs['cache_purge_pages'] = array_map( 'mainwp_rocket_clean_exclude_file', $inputs['cache_purge_pages'] );
		$inputs['cache_purge_pages'] = (array) array_filter( $inputs['cache_purge_pages'] );
		$inputs['cache_purge_pages'] = array_unique( $inputs['cache_purge_pages'] );
	} else {
		$inputs['cache_purge_pages'] = array();
	}

	/*
	 * Option : Never cache the following pages
	 */
	if ( ! empty( $inputs['cache_reject_uri'] ) ) {
		if ( ! is_array( $inputs['cache_reject_uri'] ) ) {
			$inputs['cache_reject_uri'] = explode( "\n", $inputs['cache_reject_uri'] );
		}
		$inputs['cache_reject_uri'] = array_map( 'trim', $inputs['cache_reject_uri'] );
		$inputs['cache_reject_uri'] = array_map( 'esc_url', $inputs['cache_reject_uri'] );
		$inputs['cache_reject_uri'] = array_map( 'mainwp_rocket_clean_exclude_file', $inputs['cache_reject_uri'] );
		$inputs['cache_reject_uri'] = (array) array_filter( $inputs['cache_reject_uri'] );
		$inputs['cache_reject_uri'] = array_unique( $inputs['cache_reject_uri'] );
	} else {
		$inputs['cache_reject_uri'] = array();
	}

	/*
	 * Option : Don't cache pages that use the following cookies
	 */
	if ( ! empty( $inputs['cache_reject_cookies'] ) ) {
		if ( ! is_array( $inputs['cache_reject_cookies'] ) ) {
			$inputs['cache_reject_cookies'] = explode( "\n", $inputs['cache_reject_cookies'] );
		}
		$inputs['cache_reject_cookies'] = array_map( 'trim', $inputs['cache_reject_cookies'] );
		$inputs['cache_reject_cookies'] = array_map( 'sanitize_key', $inputs['cache_reject_cookies'] );
		$inputs['cache_reject_cookies'] = (array) array_filter( $inputs['cache_reject_cookies'] );
		$inputs['cache_reject_cookies'] = array_unique( $inputs['cache_reject_cookies'] );
	} else {
		$inputs['cache_reject_cookies'] = array();
	}

	/*
	 * Option : Cache pages that use the following query strings (GET parameters)
	 */
	if ( ! empty( $inputs['cache_query_strings'] ) ) {
		if ( ! is_array( $inputs['cache_query_strings'] ) ) {
			$inputs['cache_query_strings'] = explode( "\n", $inputs['cache_query_strings'] );
		}
		$inputs['cache_query_strings'] = array_map( 'trim', $inputs['cache_query_strings'] );
		$inputs['cache_query_strings'] = array_map( 'sanitize_key', $inputs['cache_query_strings'] );
		$inputs['cache_query_strings'] = (array) array_filter( $inputs['cache_query_strings'] );
		$inputs['cache_query_strings'] = array_unique( $inputs['cache_query_strings'] );
	} else {
		$inputs['cache_query_strings'] = array();
	}

	/*
	 * Option : Never send cache pages for these user agents
	 */
	if ( ! empty( $inputs['cache_reject_ua'] ) ) {
		if ( ! is_array( $inputs['cache_reject_ua'] ) ) {
			$inputs['cache_reject_ua'] = explode( "\n", $inputs['cache_reject_ua'] );
		}
		$inputs['cache_reject_ua'] = array_map( 'trim', $inputs['cache_reject_ua'] );
		$inputs['cache_reject_ua'] = array_map( 'esc_textarea', $inputs['cache_reject_ua'] );
		$inputs['cache_reject_ua'] = (array) array_filter( $inputs['cache_reject_ua'] );
		$inputs['cache_reject_ua'] = array_unique( $inputs['cache_reject_ua'] );
	} else {
		$inputs['cache_reject_ua'] = array();
	}

	/*
	 * Option : CSS files to exclude of the minification
	 */
	if ( ! empty( $inputs['exclude_css'] ) ) {
		if ( ! is_array( $inputs['exclude_css'] ) ) {
			$inputs['exclude_css'] = explode( "\n", $inputs['exclude_css'] );
		}
		$inputs['exclude_css'] = array_map( 'trim', $inputs['exclude_css'] );
		$inputs['exclude_css'] = array_map( 'mainwp_rocket_clean_exclude_file', $inputs['exclude_css'] );
		$inputs['exclude_css'] = array_map( 'mainwp_rocket_sanitize_css', $inputs['exclude_css'] );
		$inputs['exclude_css'] = (array) array_filter( $inputs['exclude_css'] );
		$inputs['exclude_css'] = array_unique( $inputs['exclude_css'] );
	} else {
		$inputs['exclude_css'] = array();
	}

	/*
	 * Option : JS files to exclude of the minification
	 */
	if ( ! empty( $inputs['exclude_js'] ) ) {
		if ( ! is_array( $inputs['exclude_js'] ) ) {
			$inputs['exclude_js'] = explode( "\n", $inputs['exclude_js'] );
		}
		$inputs['exclude_js'] = array_map( 'trim', $inputs['exclude_js'] );
		$inputs['exclude_js'] = array_map( 'mainwp_rocket_clean_exclude_file', $inputs['exclude_js'] );
		$inputs['exclude_js'] = array_map( 'mainpw_rocket_sanitize_js', $inputs['exclude_js'] );
		$inputs['exclude_js'] = (array) array_filter( $inputs['exclude_js'] );
		$inputs['exclude_js'] = array_unique( $inputs['exclude_js'] );
	} else {
		$inputs['exclude_js'] = array();
	}
    
    // Option: Critical CSS
	$inputs['critical_css'] = ! empty( $inputs['critical_css'] ) ? wp_filter_nohtml_kses( $inputs['critical_css'] ) : '';

	/*
	 * Option : JS files with deferred loading
	 */
	if ( ! empty( $inputs['deferred_js_files'] ) ) {
		$inputs['deferred_js_files'] = array_unique( $inputs['deferred_js_files'] );
		$inputs['deferred_js_files'] = array_map( 'mainpw_rocket_sanitize_js', $inputs['deferred_js_files'] );
		$inputs['deferred_js_files'] = array_filter( $inputs['deferred_js_files'] );
	} else {
		$inputs['deferred_js_files'] = array();
	}

	if ( ! $inputs['deferred_js_files'] ) {
		$inputs['deferred_js_wait'] = array();
	} else {
		for ( $i=0; $i<=max( array_keys( $inputs['deferred_js_files'] ) ); $i++ ) {
			if ( ! isset( $inputs['deferred_js_files'][$i] ) ) {
				unset( $inputs['deferred_js_wait'][$i] );
			} else {
				$inputs['deferred_js_wait'][$i] = isset( $inputs['deferred_js_wait'][$i] ) ? '1' : '0';
			}
		}

		$inputs['deferred_js_files'] = array_values( $inputs['deferred_js_files'] );
		ksort( $inputs['deferred_js_wait'] );
		$inputs['deferred_js_wait'] = array_values( $inputs['deferred_js_wait'] );
	}

	/*
	 * Option : JS files of the minification to insert in footer
	 */
	if ( ! empty( $inputs['minify_js_in_footer'] ) ) {
		foreach( $inputs['minify_js_in_footer'] as $k=>$url ) {
			if( in_array( $url, $inputs['deferred_js_files'] ) ) {
				unset( $inputs['minify_js_in_footer'][$k] );
			}
		}

		$inputs['minify_js_in_footer'] = array_filter( array_map( 'mainpw_rocket_sanitize_js', array_unique( $inputs['minify_js_in_footer'] ) ) );
	} else {
		$inputs['minify_js_in_footer'] = array();
	}
	  
	 /*
     * Performs the database optimization when settings are saved with the "save and optimize" submit button"
     */
    if ( ! empty( $_POST ) && isset( $_POST['mainwp_wp_rocket_settings']['submit_optimize'] ) ) {
        //do_rocket_database_optimization();
		update_option('mainwp_do_rocket_database_optimization', 'yes');
    }
	
    /**
     * Database options
     */
    $inputs['database_revisions']          = ! empty( $inputs['database_revisions'] ) ? 1 : 0;
    $inputs['database_auto_drafts']        = ! empty( $inputs['database_auto_drafts'] ) ? 1 : 0;
    $inputs['database_trashed_posts']      = ! empty( $inputs['database_trashed_posts'] ) ? 1 : 0;
    $inputs['database_spam_comments']      = ! empty( $inputs['database_spam_comments'] ) ? 1 : 0;
    $inputs['database_trashed_comments']   = ! empty( $inputs['database_trashed_comments'] ) ? 1 : 0;
    $inputs['database_expired_transients'] = ! empty( $inputs['database_expired_transients'] ) ? 1 : 0;
    $inputs['database_all_transients']     = ! empty( $inputs['database_all_transients'] ) ? 1 : 0;
    $inputs['database_optimize_tables']    = ! empty( $inputs['database_optimize_tables'] ) ? 1 : 0;
    $inputs['schedule_automatic_cleanup']  = ! empty( $inputs['schedule_automatic_cleanup'] ) ? 1 : 0;

    if ( $inputs['schedule_automatic_cleanup'] != 1 && ( 'daily' != $inputs['automatic_cleanup_frequency'] || 'weekly' != $inputs['automatic_cleanup_frequency'] || 'monthly' != $inputs['automatic_cleanup_frequency'] ) ) {
        unset( $inputs['automatic_cleanup_frequency'] );
    }
	
    /**
     * Options: Activate bot preload
     */
    $inputs['manual_preload']    = ! empty( $inputs['manual_preload'] ) ? 1 : 0;
    $inputs['automatic_preload'] = ! empty( $inputs['automatic_preload'] ) ? 1 : 0;

    /*
     * Option: activate sitemap preload
     */
    $inputs['sitemap_preload'] = ! empty( $inputs['sitemap_preload'] ) ? 1 : 0;

    /*
     * Option : XML sitemaps URLs
     */
    if ( ! empty( $inputs['sitemaps'] ) ) {
		if ( ! is_array( $inputs['sitemaps'] ) ) {
			$inputs['sitemaps'] = explode( "\n", $inputs['sitemaps'] );
		}
		$inputs['sitemaps'] = array_map( 'trim', $inputs['sitemaps'] );
		//$inputs['sitemaps'] = array_map( 'rocket_sanitize_xml', $inputs['sitemaps'] );
		$inputs['sitemaps'] = (array) array_filter( $inputs['sitemaps'] );
		$inputs['sitemaps'] = array_unique( $inputs['sitemaps'] );
	} else {
		$inputs['sitemaps'] = array();
	}
	
	/*
	 * Option : CDN
	 */
	$inputs['cdn_cnames'] = isset( $inputs['cdn_cnames'] ) ? array_unique( array_filter( $inputs['cdn_cnames'] ) ) : array();

	if ( ! $inputs['cdn_cnames'] ) {
		$inputs['cdn_zone'] = array();
	} else {
		for ( $i = 0; $i <= max( array_keys( $inputs['cdn_cnames'] ) ); $i++ ) {
			if ( ! isset( $inputs['cdn_cnames'][ $i ] ) ) {
				unset( $inputs['cdn_zone'][ $i ] );
			} else {
				$inputs['cdn_zone'][ $i ] = isset( $inputs['cdn_zone'][ $i ] ) ? $inputs['cdn_zone'][ $i ] : 'all';
			}
		}

		$inputs['cdn_cnames'] 	= array_values( $inputs['cdn_cnames'] );
		ksort( $inputs['cdn_zone'] );
		$inputs['cdn_zone'] 	= array_values( $inputs['cdn_zone'] );
	}

	/*
	 * Option : Files to exclude of the CDN process
	 */
	if ( ! empty( $inputs['cdn_reject_files'] ) ) {
		if ( ! is_array( $inputs['cdn_reject_files'] ) ) {
			$inputs['cdn_reject_files'] = explode( "\n", $inputs['cdn_reject_files'] );
		}
		$inputs['cdn_reject_files'] = array_map( 'trim', $inputs['cdn_reject_files'] );
		$inputs['cdn_reject_files'] = array_map( 'mainwp_rocket_clean_exclude_file', $inputs['cdn_reject_files'] );
		$inputs['cdn_reject_files'] = (array) array_filter( $inputs['cdn_reject_files'] );
		$inputs['cdn_reject_files'] = array_unique( $inputs['cdn_reject_files'] );
	} else {
		$inputs['cdn_reject_files'] = array();
	}
	
	/*
	 * Option: Support
	 */
	$fake_options = array( 
		'support_summary',
		'support_description',
		'support_documentation_validation'
	);
	
	foreach ( $fake_options as $option ) {
		 if( isset( $inputs[$option] ) ) {
			 unset($inputs[$option]);
		 }
	}
	$message = 1; 
	if ( isset( $_FILES['import'] )
		&& preg_match( '/wp-rocket-settings-20\d{2}-\d{2}-\d{2}-[a-f0-9]{13}\.txt/', $_FILES['import']['name'] )
		&& 'text/plain' == $_FILES['import']['type'] ) {
            $file_name 			= $_FILES['import']['name'];
            $_POST_action 		= $_POST['action'];
            $_POST['action'] 	= 'wp_handle_sideload';
            $file 				= wp_handle_sideload( $_FILES['import'], array( 'mimes' => array( 'txt' => 'text/plain' ) ) );
            $_POST['action'] 	= $_POST_action;
            $gz 				= 'gz'.strrev( 'etalfni' );
            $settings 			= @file_get_contents( $file['file'] );
            $settings 			= $gz//;
            ( $settings );
            $settings 			= unserialize( $settings );

            file_put_contents( $file['file'], '' );
            @unlink( $file['file'] );
            if ( is_array( $settings ) ) {
    //			$settings['consumer_key']		= $inputs['consumer_key'];
    //			$settings['consumer_email']		= $inputs['consumer_email'];
    //			$settings['secret_key']			= $inputs['secret_key'];
    //			$settings['secret_cache_key']	= $inputs['secret_cache_key'];
    //			$settings['minify_css_key']		= $inputs['minify_css_key'];
    //			$settings['minify_js_key']		= $inputs['minify_js_key'];
    //			$settings['version']			= $inputs['version'];
                $inputs = $settings;
                //add_settings_error( 'general', 'settings_updated', __( 'Settings imported and saved.', 'mainwp-rocket-extension' ), 'updated' );
                $message = 2;
            }
        }
    
    
        $site_id = 0;     
        if (isset($_POST['mainwp_rocket_current_site_id']) && !empty($_POST['mainwp_rocket_current_site_id'])) {   
            $site_id = $_POST['mainwp_rocket_current_site_id'];                      
        }
    
        if (MainWP_Rocket::is_manage_sites_page()) {          
            if ($site_id) {
                $update = array(
                    'site_id' => $site_id,
                    'settings' => base64_encode(serialize($inputs))
                ); 
                MainWP_Rocket_DB::get_instance()->update_wprocket($update);    
            }
        } else {            
            update_option(MAINWP_ROCKET_GENERAL_SETTINGS, $inputs);
        } 
       
        wp_redirect( add_query_arg( array('_perform_action'=> 'mainwp_rocket_save_opts_child_sites', '_wpnonce' => wp_create_nonce('mainwp_rocket_save_opts_child_sites'), 'message' => $message) , remove_query_arg( 's', wp_get_referer()) ) );        
        
        die();
        
	return $inputs;
}

/**
 * Outputs the form used by the importers to accept the data to be imported
 *
 * @since 2.2
 */
function mainwp_rocket_import_upload_form() {
	/**
	 * Filter the maximum allowed upload size for import files.
	 *
	 * @since (WordPress) 2.3.0
	 *
	 * @see wp_max_upload_size()
	 *
	 * @param int $max_upload_size Allowed upload size. Default 1 MB.
	 */
	$bytes = apply_filters( 'mainwp_import_upload_size_limit', wp_max_upload_size() ); // Filter from WP Core
	$size = size_format( $bytes );
	$upload_dir = wp_upload_dir();
	if ( ! empty( $upload_dir['error'] ) ) {
		?><div class="error"><p><?php _e( 'Before you can upload your import file, you will need to fix the following error:', 'mainwp-rocket-extension' ); ?></p>
		<p><strong><?php echo $upload_dir['error']; ?></strong></p></div><?php
	} else {
		?>
		<p>
		<input type="file" id="upload" name="import" size="25" />
		<br />
		<label for="upload"><?php echo apply_filters( 'rocket_help', __( 'Choose a file from your computer', 'mainwp-rocket-extension' ) . ' (' . sprintf( __( 'maximum size: %s', 'mainwp-rocket-extension' ), $size ) . ')', 'upload', 'help' ); ?></label>
		<input type="hidden" name="max_file_size" value="<?php echo $bytes; ?>" />
		</p>
		<?php submit_button( __( 'Upload file and import settings', 'mainwp-rocket-extension' ), 'button', 'import' );
	}
}
