<?php
defined( 'ABSPATH' ) or die( 'Cheatin\' uh?' );

/**
 * Add the CSS and JS files for WP Rocket options page
 *
 * @since 1.0.0
 */

add_action( 'admin_print_styles-mainwp_page_Extensions-Mainwp-Rocket-Extension', '__mainwp_rocket_add_admin_css_js' );
add_action( 'admin_print_styles-mainwp_page_ManageSitesWPRocket', '__mainwp_rocket_add_admin_css_js' );
function __mainwp_rocket_add_admin_css_js()
{    
	wp_enqueue_script( 'jquery-ui-sortable', null, array( 'jquery', 'jquery-ui-core' ), null, true );
	wp_enqueue_script( 'jquery-ui-draggable', null, array( 'jquery', 'jquery-ui-core' ), null, true );
	wp_enqueue_script( 'jquery-ui-droppable', null, array( 'jquery', 'jquery-ui-core' ), null, true );
	wp_enqueue_script( 'options-wp-rocket', MAINWP_WP_ROCKET_ADMIN_UI_JS_URL . 'options.js', array( 'jquery', 'jquery-ui-core' ), MAINWP_WP_ROCKET_VERSION, true );
	wp_enqueue_script( 'fancybox-wp-rocket', MAINWP_WP_ROCKET_ADMIN_UI_JS_URL . 'vendors/jquery.fancybox.pack.js', array( 'options-wp-rocket' ), MAINWP_WP_ROCKET_VERSION, true );
	wp_enqueue_script( 'sweet-alert-wp-rocket', MAINWP_WP_ROCKET_ADMIN_UI_JS_URL . 'vendors/sweet-alert.min.js', array( 'options-wp-rocket' ), MAINWP_WP_ROCKET_VERSION, true );

	wp_enqueue_style( 'options-wp-rocket', MAINWP_WP_ROCKET_ADMIN_UI_CSS_URL . 'options.css', array(), MAINWP_WP_ROCKET_VERSION );
	wp_enqueue_style( 'fancybox-wp-rocket', MAINWP_WP_ROCKET_ADMIN_UI_CSS_URL . 'fancybox/jquery.fancybox.css', array( 'options-wp-rocket' ), MAINWP_WP_ROCKET_VERSION );

	// Sweet Alert
	$translation_array = array(
		'warningTitle'     => __( 'This could break things!', 'mainwp-rocket-extension' ),
		'requiredTitle'    => __( 'All fields are required!', 'mainwp-rocket-extension' ),
		
		'cloudflareTitle'  => __( 'CloudFlare Settings', 'mainwp-rocket-extension' ),
		'cloudflareText'   => __( 'Click "Save Changes" to activate the Cloudflare tab.', 'mainwp-rocket-extension' ),

		'preloaderTitle' => __( 'Transmitting across the galaxy...', 'mainwp-rocket-extension' ),
		'preloaderImg'	 => MAINWP_WP_ROCKET_ADMIN_UI_IMG_URL . 'preloader.gif',

		'badServerConnectionTitle'             => __( 'Unable to transmit', 'mainwp-rocket-extension' ),
		'badServerConnectionText'              => __( 'It seems that communications with Mission Control are temporarily down....please submit a support ticket while our Rocket Scientists fix the issue.', 'mainwp-rocket-extension' ),
		'badServerConnectionConfirmButtonText' => __( 'Get help from a rocket scientist', 'mainwp-rocket-extension' ),

		'warningSupportTitle' => __( 'Last steps before contacting us', 'mainwp-rocket-extension' ),
		'warningSupportText'  => sprintf( __( 'You have to read the <a href="%s" target="_blank">documentation</a> and to agree to send informations relative to your website to submit a support ticket.', 'mainwp-rocket-extension' ), 'http://docs.wp-rocket.me/?utm_source=wp-rocket&utm_medium=wp-admin&utm_term=doc-support&utm_campaign=plugin' ),

		'successSupportTitle' => __( 'Transmission Received!', 'mainwp-rocket-extension' ),
		'successSupportText'  => __( 'We\'ve received your ticket and will reply back within a few hours!', 'mainwp-rocket-extension' ) . '<br/>' . __( 'We answer every ticket so check your spam if you don\'t hear from us.', 'mainwp-rocket-extension' ),

		'badSupportTitle'      => __( 'Oh dear, someone\'s been naughty...', 'mainwp-rocket-extension' ),
		'badSupportText'       => __( 'Well, well, looks like you\'ve got yourself a "nulled" version! We don\'t provide support to hackers or pirates, so you will need a valid license to proceed.', 'mainwp-rocket-extension' ) . '<br/>' . __( 'Click below to buy a license with a 20% discount automatically applied.', 'mainwp-rocket-extension' ),
		'badConfirmButtonText' => __( 'Buy It Now!', 'mainwp-rocket-extension' ),

		'expiredSupportTitle'      => __( 'Uh-oh, you\'re out of fuel!', 'mainwp-rocket-extension' ),
		'expiredSupportText'       => __( 'To keep your Rocket running with access to support, <strong>you\'ll need to renew your license</strong>.', 'mainwp-rocket-extension' ) . '<br/><br/>' .  __( 'Click below to renew with a <strong>discount of 50%</strong> automatically applied!', 'mainwp-rocket-extension' ),
		'expiredConfirmButtonText' => __( 'I re-synchronize now!', 'mainwp-rocket-extension' ),

		'minifyText' => __( 'If you notice any errors on your website after having activated this setting, just deactivate it again, and your site will be back to normal.', 'mainwp-rocket-extension' ),

		'confirmButtonText' => __( 'Yes, I\'m sure!', 'mainwp-rocket-extension' ),
		'cancelButtonText'  => __( 'Cancel', 'mainwp-rocket-extension' )
	);
	wp_localize_script( 'options-wp-rocket', 'mwp_sawpr', $translation_array );
	wp_enqueue_style( 'sweet-alert-wp-rocket', MAINWP_WP_ROCKET_ADMIN_UI_CSS_URL . 'sweet-alert.css', array( 'options-wp-rocket' ), MAINWP_WP_ROCKET_VERSION );
}

/**
 * Add the CSS and JS files needed by WP Rocket everywhere on admin pages
 *
 * @since 2.1
 */
add_action( 'admin_print_styles', '__mainwp_rocket_add_admin_css_js_everywhere', 11 );
function __mainwp_rocket_add_admin_css_js_everywhere()
{
	wp_enqueue_script( 'mainwp-all-wp-rocket', MAINWP_WP_ROCKET_ADMIN_UI_JS_URL . 'all.js', array( 'jquery' ), MAINWP_WP_ROCKET_VERSION, true );
}

/**
 * Add some CSS to display the dismiss cross
 *
 * @since 1.1.10
 *
 */
add_action( 'admin_print_styles', '__mainwp_rocket_admin_print_styles' );
function __mainwp_rocket_admin_print_styles()
{
	wp_enqueue_style( 'admin-wp-rocket', MAINWP_WP_ROCKET_ADMIN_UI_CSS_URL . 'admin.css', array(), MAINWP_WP_ROCKET_VERSION );
}