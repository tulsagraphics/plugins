<?php 
defined( 'ABSPATH' ) or die( 'Cheatin\' uh?' );

add_settings_section( 'mainwp_rocket_display_main_options', '', '__return_false', 'basic' );

add_settings_field(
	'rocket_basic_options_panel',
	false,
	'mainwp_rocket_field',
	'basic',
	'mainwp_rocket_display_main_options'
);

add_settings_field(
	'rocket_mobile',
	__( 'Mobile cache:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'basic',
	'mainwp_rocket_display_main_options',
	array(
		array(
			'type'		   => 'checkbox',
			'label'		   => __( 'Enable caching for mobile devices.', 'mainwp-rocket-extension' ),
			'label_for'	   => 'cache_mobile',
			'label_screen' => __( 'Mobile cache:', 'mainwp-rocket-extension' ),
		),
        array(
			'type'         => 'helper_performance',
			'name'         => 'mobile_perf_tip',
			'description'  => __( 'Makes your website mobile-friendlier.', 'mainwp-rocket-extension' ),
		),
		array(
			'parent'	   => 'cache_mobile',
			'type'         => 'checkbox',
			'label'        => __( 'Separate cache files for mobile devices.', 'mainwp-rocket-extension' ),
			'name'         => 'do_caching_mobile_files'
		),
        array(
			'parent'       => 'cache_mobile',
			'type'         => 'helper_description',
			'name'         => 'mobile',
			'description'  => __( 'Mobile cache works safest with both options enabled. When in doubt, keep both.', 'rocket' ),
		),		
	)
);


add_settings_field(
	'rocket_logged_user',
	__( 'User cache:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field', 'basic',
	'mainwp_rocket_display_main_options',
    array(
        array(
            'type'         => 'checkbox',
            'label'        => __('Enable caching for logged-in WordPress users.', 'mainwp-rocket-extension' ),
            'label_for'    => 'cache_logged_user',
            'label_screen' =>__( 'User cache:', 'mainwp-rocket-extension' ),
        ),
        array(
            'type'         => 'helper_description',
            'name'         => 'user_cache_desc',
            'description'  =>			
            __( 'User cache is great when you have user-specific or restricted content on your website.', 'mainwp-rocket-extension' ),
        )
    )
);

add_settings_field(
	'rocket_ssl',
	__( 'SSL cache:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'basic',
	'mainwp_rocket_display_main_options',
    array(
        array(
            'type'         => 'checkbox',
            'label'        => __('Enable caching for pages with <code>https://</code>.', 'mainwp-rocket-extension' ),
            'label_for'    => 'cache_ssl',
            'label_screen' => __( 'SSL cache:', 'mainwp-rocket-extension' ),
        ),
        array(
            'type'         => 'helper_description',
            'name'         => 'ssl_cache_desc',
            'description'  =>        
            __( 'SSL cache works best when your entire website runs on HTTPS.', 'mainwp-rocket-extension' ),
        )
    )
);

add_settings_field(
	'rocket_purge',
	__( 'Cache lifespan', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'basic',
	'mainwp_rocket_display_main_options',
	array(
        array(
            'type'         => 'helper_help',
            'name'         => 'purge_tip',
            'description'  => __( ' Specify time after which the global cache is cleared (0 = unlimited ).', 'mainwp-rocket-extension' ),
        ),
		array(
			'type'         => 'number',
			'label_for'    => 'purge_cron_interval',
			'label_screen' => __( 'Clear Cache Lifespan', 'mainwp-rocket-extension' ),
			'fieldset'     => 'start'
		),
		array(
			'type'		   => 'select',
			'label_for'	   => 'purge_cron_unit',
			'label_screen' => __( 'Unit of time', 'mainwp-rocket-extension' ),
			'fieldset'	   => 'end',
			'options' => array(
				'SECOND_IN_SECONDS' => __( 'second(s)', 'mainwp-rocket-extension' ),
				'MINUTE_IN_SECONDS' => __( 'minute(s)', 'mainwp-rocket-extension' ),
				'HOUR_IN_SECONDS'   => __( 'hour(s)', 'mainwp-rocket-extension' ),
				'DAY_IN_SECONDS'    => __( 'day(s)', 'mainwp-rocket-extension' )
			)
		),
		array(
            'type'         => 'helper_description',
            'name'         => 'purge',
            'description'  => 
                __( 'Cache lifespan is the period of time after which all cache files are removed. Enable preloading for the cache to be rebuilt automatically after lifespan expiration.', 'mainwp-rocket-extension' )               
        ),
		array(
            'type'         => 'helper_warning',
            'name'         => 'purge_warning_less',        
            'description'  => __( 'Reduce lifespan to 10 hours or less if you notice issues that seem to appear periodically. <a href="https://joshpress.net/wordpress-nonces-and-wordpress-caching/" target="_blank">Why?</a>', 'mainwp-rocket-extension' )
        )
    )
);

//add_settings_field(
//	'rocket_minify',
//	 __( 'Files optimisation:<br/><span class="description">(Minification & Concatenation)</span>', 'mainwp-rocket-extension' ),
//	'mainwp_rocket_field',
//	'basic',
//	'mainwp_rocket_display_main_options',
//	array(
//		array(
//			'type'         => 'checkbox',
//			'label'        => 'HTML',
//			'name'         => 'minify_html',
//			'label_screen' => __( 'HTML Files minification', 'mainwp-rocket-extension' )
//		),
//		array(
//			'parent'	   => 'minify_html',
//			'type'         => 'checkbox',
//			'label'        => 'Inline CSS',
//			'name'         => 'minify_html_inline_css',
//			'label_screen' => 'Inline CSS minification',
//		),
//		array(
//			'parent'	   => 'minify_html',
//			'type'         => 'checkbox',
//			'label'        => 'Inline JS',
//			'name'         => 'minify_html_inline_js',
//			'label_screen' => 'Inline JS minification'
//		),
//		array(
//			'type'		   => 'checkbox',
//			'label'		   => 'Google Fonts',
//			'name'		   => 'minify_google_fonts',
//			'label_screen' => __( 'Google Fonts minification', 'mainwp-rocket-extension' ),
//		),
//		array(
//			'type'         => 'checkbox',
//			'label'        => 'CSS',
//			'name'         => 'minify_css',
//			'label_screen' => __( 'CSS Files minification', 'mainwp-rocket-extension' )
//		),
//		array(
//			'type'		   => 'checkbox',
//			'label'		   => 'JS',
//			'name'		   => 'minify_js',
//			'label_screen' => __( 'JS Files minification', 'mainwp-rocket-extension' ),
//		),
//		array(
//			'type'			=> 'helper_description',
//			'name'			=> 'minify',
//			'description'  => __( 'Minification removes any spaces and comments present in the CSS and JavaScript files.', 'mainwp-rocket-extension' ) . '<br/>' . __( 'This mechanism reduces the weight of each file and allows a faster reading of browsers and search engines.', 'mainwp-rocket-extension' ) . '<br/>' . __( 'Concatenation combines all CSS and JavaScript files.', 'mainwp-rocket-extension' ) . '<br/>' . __( 'This mechanism reduces the number of HTTP requests and improves the loading time.', 'mainwp-rocket-extension' )
//		),
//		array(
//			'type'			=> 'helper_warning',
//			'name'			=> 'minify_help1',
//			'description'  => __( 'Concatenating files can cause display errors.', 'mainwp-rocket-extension' ),
//		),
//		array(
//			'display'		=> ! mainwp_rocket_is_white_label(),
//			'type'			=> 'helper_warning',
//			'name'			=> 'minify_help2',
//			'description'  => sprintf( __( 'In case of any errors we recommend you to turn off this option or watch the following video: <a href="%1$s" class="fancybox">%1$s</a>.', 'mainwp-rocket-extension' ), ( defined( 'WPLANG' ) && WPLANG == 'fr_FR' ) ? 'http://www.youtube.com/embed/5-Llh0ivyjs' : 'http://www.youtube.com/embed/kymoxCwW03c' )
//		),
//
//	)
//);
