<?php 
defined( 'ABSPATH' ) or die( 'Cheatin\' uh?' );

add_settings_section( 'mainwp_rocket_display_dashboard', '', '__return_false', 'dashboard' );

//if ( ! mainwp_rocket_is_white_label() ) {
	add_settings_field(
		'rocket_do_beta',
		__( 'Beta testing:', 'mainwp-rocket-extension' ),
		'mainwp_rocket_field',
		'dashboard',
		'mainwp_rocket_display_dashboard',
		array(
			array(
				'type'         => 'checkbox',
				'label'        => __( 'I am part of the WP Rocket Beta Testing Program.', 'mainwp-rocket-extension' ),
				'label_for'    => 'do_beta',
				'label_screen' => __( 'Rocket Tester', 'mainwp-rocket-extension' )
			)			
		)
    );
	
	add_settings_field(
		'rocket_analytics_enabled',
		__( 'Rocket Analytics:', 'mainwp-rocket-extension' ),
		'mainwp_rocket_field',
		'dashboard',
		'mainwp_rocket_display_dashboard',
		array(
			array(
				'type'         => 'checkbox',
				'label'        => __( 'I agree to share anonymous data with the development team to help improve WP Rocket.', 'mainwp-rocket-extension' ),
				'label_for'    => 'analytics_enabled',
				'label_screen' => __( 'Rocket Analytics ', 'mainwp-rocket-extension' )
			)			
		)
    );
	
//}

if (MainWP_Rocket::is_manage_sites_page()) {
    $purge_all_url = "#";   
    $purge_all_status = '<span id="mwp_rocket_individual_purge_all_status">
                            <i style="display: none;" class="fa fa-spinner fa-pulse"></i>
                                <span class="status hidden"></span></span>';
    $purge_all_onclick = 'mainwp_rocket_individual_purge_all(' . $_GET['id'] . '); return false;';
} else {
    $purge_all_url = wp_nonce_url( admin_url( "admin.php?page=Extensions-Mainwp-Rocket-Extension&_perform_action=mainwp_rocket_purge_cache_all" ), 'mainwp_rocket_purge_cache_all' );
    $purge_all_status = "";
    $purge_all_onclick = "";
}


add_settings_field(
	'rocket_purge_all',
	__( 'Remove all cached files:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_button',
	'dashboard',
	'mainwp_rocket_display_dashboard',
	array(
		'button'=>array(
			'button_label' => __( 'Clear cache', 'mainwp-rocket-extension' ),
			'url'		   => wp_nonce_url( admin_url( 'admin-post.php?action=mainwp_purge_cache&type=all' ), 'purge_cache_all' ),
                        'url'		   => $purge_all_url,                      
                        'onclick' => $purge_all_onclick,
                        'status' => $purge_all_status
		)
	)
);

if (MainWP_Rocket::is_manage_sites_page()) {
    $preload_url = "#";   
    $preload_status = '<span id="mwp_rocket_individual_preload_status">
                            <i style="display: none;" class="fa fa-spinner fa-pulse"></i>
                                <span class="status hidden"></span></span>';
    $preload_onclick = 'mainwp_rocket_individual_preload_cache(' . $_GET['id'] . '); return false;';
} else {
    $preload_url = wp_nonce_url( admin_url( "admin.php?page=Extensions-Mainwp-Rocket-Extension&_perform_action=mainwp_rocket_preload_cache" ), 'mainwp_rocket_preload_cache' );
    $preload_status = "";
    $preload_onclick = "";
}
add_settings_field(
	'rocket_preload',
	__( 'Start cache preloading:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_button',
	'dashboard',
	'mainwp_rocket_display_dashboard',
	array(
        'button'=>array(
        	'button_label' => __( 'Preload cache', 'mainwp-rocket-extension' ),
        	'url'   => $preload_url,                      
                'onclick' => $preload_onclick,
                'status' => $preload_status
        
        )
	)
);


if (MainWP_Rocket::is_manage_sites_page()) {
    $purge_url = "#";   
    $purge_status = '<span id="mwp_rocket_individual_purge_opcache_status">
                            <i style="display: none;" class="fa fa-spinner fa-pulse"></i>
                                <span class="status hidden"></span></span>';
    $purge_onclick = 'mainwp_rocket_individual_purge_opcache(' . $_GET['id'] . '); return false;';
} else {
    $purge_url = wp_nonce_url( admin_url( "admin.php?page=Extensions-Mainwp-Rocket-Extension&_perform_action=mainwp_rocket_purge_opcache" ), 'mainwp_rocket_purge_opcache' );
    $purge_status = "";
    $purge_onclick = "";
}

add_settings_field(
    'rocket_purge_opcache',
    __( 'Purge OPCache content:', 'mainwp-rocket-extension' ),
    'mainwp_rocket_button',
    'dashboard',
    'mainwp_rocket_display_dashboard',
    array(
        'button' => array(
        'button_label' => __( 'Purge OPcache', 'mainwp-rocket-extension' ),
        'url'		   => $purge_url,                      
        'onclick' => $purge_onclick,
        'status' => $purge_status
    ),
        
       
            
    )
);

