<?php 
defined( 'ABSPATH' ) or die( 'Cheatin\' uh?' );

$is_manage_site = false;
if (MainWP_Rocket::is_manage_sites_page()) {  
	$is_manage_site = true;
	$optimize_info = get_option('mainwp_rocket_optimize_database_info', false);	
	if (is_array($optimize_info)) {
		$total_revisions          = isset($optimize_info['total_revisions']) ? $optimize_info['total_revisions'] : 0;
		$total_auto_draft         = isset($optimize_info['total_auto_draft']) ? $optimize_info['total_auto_draft'] : 0;
		$total_trashed_posts      = isset($optimize_info['total_trashed_posts']) ? $optimize_info['total_trashed_posts'] : 0;
		$total_spam_comments      = isset($optimize_info['total_spam_comments']) ? $optimize_info['total_spam_comments'] : 0;
		$total_trashed_comments   = isset($optimize_info['total_trashed_comments']) ? $optimize_info['total_trashed_comments'] : 0;
		$total_expired_transients = isset($optimize_info['total_expired_transients']) ? $optimize_info['total_expired_transients'] : 0;
		$total_all_transients     = isset($optimize_info['total_all_transients']) ? $optimize_info['total_all_transients'] : 0;
		$total_optimize_tables    = isset($optimize_info['total_optimize_tables']) ? $optimize_info['total_optimize_tables'] : 0;
	}
}



add_settings_section( 'mainwp_rocket_display_database_options', '', '__return_false', 'rocket_database' );

add_settings_field(
	'mwp_rocket_database_options_panel',
	false,
	'mainwp_rocket_field',
	'rocket_database',
	'mainwp_rocket_display_database_options'
);

add_settings_field(
	'mwp_rocket_optimize_posts',
	__( 'Posts cleanup:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'rocket_database',
	'mainwp_rocket_display_database_options',
	array(
		array(
			'type'         => 'checkbox',
			'label'        => __('Revisions', 'mainwp-rocket-extension' ),
			'label_for'    => 'database_revisions',
			'label_screen' => __( 'Cleanup revisions', 'mainwp-rocket-extension' ),
		),
		array(
    		'type'         => 'helper_help',
    		'name'         => 'revisions_desc',
    		'description'  => $is_manage_site ? sprintf( _n( '%d revision in your database.', '%d revisions in your database.', $total_revisions, 'mainwp-rocket-extension' ), $total_revisions ) : ''
		),
		array(
			'type'         => 'checkbox',
			'label'        => __('Auto Drafts', 'mainwp-rocket-extension' ),
			'label_for'    => 'database_auto_drafts',
			'label_screen' => __( 'Cleanup auto drafts', 'mainwp-rocket-extension' ),
		),
		array(
    		'type'         => 'helper_help',
    		'name'         => 'auto_drafts_desc',
    		'description'  => $is_manage_site ? sprintf( _n( '%d draft in your database.', '%d drafts in your database.', $total_auto_draft, 'mainwp-rocket-extension' ), $total_auto_draft ) : ''
		),
		array(
			'type'         => 'checkbox',
			'label'        => __('Trashed posts', 'mainwp-rocket-extension' ),
			'label_for'    => 'database_trashed_posts',
			'label_screen' => __( 'Cleanup trashed posts', 'mainwp-rocket-extension' ),
		),
		array(
    		'type'         => 'helper_help',
    		'name'         => 'trashed_posts_desc',
    		'description'  => $is_manage_site ? sprintf( _n( '%d trashed post in your database.', '%d trashed posts in your database.', $total_trashed_posts, 'mainwp-rocket-extension' ), $total_trashed_posts ) : ''
		),
		array(
    		'type'         => 'helper_help',    		
    		'description'  => 'Post revisions and drafts will be permanently deleted. Do not use this option if you need to retain revisions or drafts.'
		),
	)
);

add_settings_field(
	'mwp_rocket_optimize_comments',
	__( 'Comments cleanup:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'rocket_database',
	'mainwp_rocket_display_database_options',
	array(
		array(
			'type'         => 'checkbox',
			'label'        => __('Spam comments', 'mainwp-rocket-extension' ),
			'label_for'    => 'database_spam_comments',
			'label_screen' => __( 'Cleanup spam comments', 'mainwp-rocket-extension' ),
		),
		array(
    		'type'         => 'helper_help',
    		'name'         => 'spam_comments_desc',
    		'description'  => $is_manage_site ? sprintf( _n( '%d spam comment in your database.', '%d spam comments in your database.', $total_spam_comments, 'mainwp-rocket-extension' ), $total_spam_comments ) : ''
		),
		array(
			'type'         => 'checkbox',
			'label'        => __('Trashed comments', 'mainwp-rocket-extension' ),
			'label_for'    => 'database_trashed_comments',
			'label_screen' => __( 'Cleanup trashed comments', 'mainwp-rocket-extension' ),
		),
		array(
    		'type'         => 'helper_help',
    		'name'         => 'trashed_comments_desc',
    		'description'  => $is_manage_site ? sprintf( _n( '%d trashed comment in your database.', '%d trashed comments in your database.', $total_trashed_comments, 'mainwp-rocket-extension' ), $total_trashed_comments ) : ''
		),
		array(
    		'type'         => 'helper_help',    		
    		'description'  => 'Spam and trashed comments will be permanently deleted.'
		),
		
	)
);

add_settings_field(
	'mwp_rocket_optimize_transients',
	__( 'Transients cleanup:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'rocket_database',
	'mainwp_rocket_display_database_options',
	array(
		array(
			'type'         => 'checkbox',
			'label'        => __('Expired transients', 'mainwp-rocket-extension' ),
			'label_for'    => 'database_expired_transients',
			'label_screen' => __( 'Cleanup expired transients', 'mainwp-rocket-extension' ),
		),
		array(
    		'type'         => 'helper_help',
    		'name'         => 'expired_transients_desc',
    		'description'  => $is_manage_site ? sprintf( _n( '%d expired transient in your database.', '%d expired transients in your database.', $total_expired_transients, 'mainwp-rocket-extension' ), $total_expired_transients ) : ''
		),
		array(
			'type'         => 'checkbox',
			'label'        => __('All transients', 'mainwp-rocket-extension' ),
			'label_for'    => 'database_all_transients',
			'label_screen' => __( 'Cleanup all transients', 'mainwp-rocket-extension' ),
		),
		array(
    		'type'         => 'helper_help',
    		'name'         => 'all_transients_desc',
    		'description'  => $is_manage_site ? sprintf( _n( '%d transient in your database.', '%d transients in your database.', $total_all_transients, 'mainwp-rocket-extension' ), $total_all_transients ) : ''
		),
		array(
    		'type'         => 'helper_help',
    		'name'         => 'all_transients_desc',
    		'description'  => 'Transients are temporary options; they are safe to remove. They will be automatically regenerated as your plugins require them.'
		)
	)
);

add_settings_field(
	'mwp_rocket_optimize_database',
	__( 'Database cleanup:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'rocket_database',
	'mainwp_rocket_display_database_options',
	array(
		array(
			'type'         => 'checkbox',
			'label'        => __('Optimize tables', 'mainwp-rocket-extension' ),
			'label_for'    => 'database_optimize_tables',
			'label_screen' => __( 'Optimize database tables', 'mainwp-rocket-extension' ),
		),
		array(
    		'type'         => 'helper_help',
    		'name'         => 'optimize_tables_desc',
    		'description'  => $is_manage_site ? sprintf( _n( '%d table to optimize in your database.', '%d tables to optimize in your database.', $total_optimize_tables, 'mainwp-rocket-extension' ), $total_optimize_tables ) : ''
		),
		array(
    		'type'         => 'helper_help',
    		'description'  => 'Reduces overhead of database tables'
		),
	)
);

add_settings_field(
	'mwp_rocket_database_cron',
	__( 'Automatic cleanup:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'rocket_database',
	'mainwp_rocket_display_database_options',
	array(
		array(
			'type'         => 'checkbox',
			'label'        => __('Schedule automatic cleanup', 'mainwp-rocket-extension' ),
			'name'         => 'schedule_automatic_cleanup',
			'label_screen' => __( 'Schedule an automatic cleanup of the database', 'mainwp-rocket-extension' ),
		),
		array(
    		'parent'       => 'schedule_automatic_cleanup',
			'type'         => 'select',
			'label'        => __('Frequency', 'mainwp-rocket-extension' ),
			'name'         => 'automatic_cleanup_frequency',
			'label_screen' => __( 'Frequency for the automatic cleanup', 'mainwp-rocket-extension' ),
			'options'      => array(
    			'daily'    => __( 'Daily', 'mainwp-rocket-extension' ),
    			'weekly'   => __( 'Weekly', 'mainwp-rocket-extension' ),
    			'monthly'  => __( 'Monthly', 'mainwp-rocket-extension' )
			)
		),
		array(
    		'type'         => 'helper_help',
    		'description'  => 'Schedule Automatic Cleanup'
		),		
	)
);

add_settings_field(
	'mwp_rocket_run_optimize',
	__( 'Run cleanup:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'rocket_database',
	'mainwp_rocket_display_database_options',
	array(
        array(
            'type' => 'submit_optimize',
        ),
		array(
			'type'         => 'helper_description',
			'name'         => 'submit_optimize_desc',
			'description'  => sprintf(
				/* translators: %1$s and %2$s = button text */
				__( '“%1$s” will save your settings and run a database optimization; “%2$s” below will only save your settings.', 'mainwp-rocket-extension' ),
				__( 'Save and optimize', 'mainwp-rocket-extension' ),
				__( 'Save Changes' )
			),
		),
	)
);