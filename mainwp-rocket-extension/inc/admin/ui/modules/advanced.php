<?php 
defined( 'ABSPATH' ) or die( 'Cheatin\' uh?' );

add_settings_section( 'mainwp_rocket_display_imp_options', '', '__return_false', 'advanced' );

add_settings_field(
	'rocket_advanced_options_panel',
	false,
	'mainwp_rocket_field',
	'advanced',
	'mainwp_rocket_display_imp_options'
);

add_settings_field(
	'mwp_rocket_reject_uri',
	__( 'Never Cache URL(s):', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'advanced',
	'mainwp_rocket_display_imp_options',
	array(
        array(
            'type'         => 'helper_help',
            'name'         => 'reject_uri',
            'description'  => __( 'Specify URLs of pages or posts that should never get cached (one per line)', 'mainwp-rocket-extension' ),
        ),
		array(
			'type'         => 'textarea',
			'label_for'    => 'cache_reject_uri',
			'label_screen' => __( 'Never cache the following pages:', 'mainwp-rocket-extension' ),
		),		
		array(
			'type'         => 'helper_detection',
			'name'         => 'cache_reject_ua',
			'description'  => __( 'The domain part of the URL will be stripped automatically. Use (.*) wildcards to address multiple URLs under a given path. ', 'mainwp-rocket-extension' )
		),
	)
);

add_settings_field(
	'mwp_rocket_reject_cookies',
	__( 'Never Cache Cookies:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'advanced',
	'mainwp_rocket_display_imp_options',
	array(
        array(
			'type'         => 'helper_help',
			'name'         => 'reject_cookies',
			'description'  => __( 'Specify the IDs of cookies that, when set in the visitor’s browser, should prevent a page from getting cached (one per line)', 'rocket' ),
		),
		array(
			'type'         => 'textarea',
			'label_for'    => 'cache_reject_cookies',
			'label_screen' => __( 'Don\'t cache pages that use the following cookies:', 'mainwp-rocket-extension' ),
		)		
	)
);


add_settings_field(
	'mwp_rocket_reject_ua',
	__( 'Never Cache User Agent(s):', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'advanced',
	'mainwp_rocket_display_imp_options',
	array(
        array(
			'type'         => 'helper_help',
			'name'         => 'cache_reject_ua',
			'description'  => __( 'Specify user agent strings that should never see cached pages (one per line)', 'mainwp-rocket-extension' ),
		),
		array(
			'type'         => 'textarea',
			'label_for'    => 'cache_reject_ua',
			'label_screen' => __( 'Never send cache pages for these user agents:', 'mainwp-rocket-extension' ),
		),
		array(
			'type'         => 'helper_description',
			'description'  => sprintf(				
				__( 'Use (.*) wildcards to detect parts of UA strings.', 'mainwp-rocket-extension' )
			),
		)
	)
);

add_settings_field(
	'mwp_rocket_purge_pages',
	__( 'Always Purge URL(s):', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'advanced',
	'mainwp_rocket_display_imp_options',
	array(
        array(
			'type'         => 'helper_help',
			'name'         => 'purge_pages',
			'description'  => __( 'Specify URLs you always want purged from cache whenever you update any post or page (one per line)', 'mainwp-rocket-extension' ),
		),
		array(
			'type'         => 'textarea',
			'label_for'    => 'cache_purge_pages',
			'label_screen' => __( 'Empty the cache of the following pages when updating a post:', 'mainwp-rocket-extension' ),
		),		
		array(
			'type'         => 'helper_description',
			'description'  =>  sprintf(				
				__( 'The domain part of the URL will be stripped automatically.<br>Use (.*) wildcards to address multiple URLs under a given path.', 'rocket' )				
			),
		),
	)
);



add_settings_field(
	'mwp_rocket_query_strings',
	__( 'Cache Query String(s):', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'advanced',
	'mainwp_rocket_display_imp_options',
	array(
        array(
			'type'         => 'helper_help',
			'name'         => 'query_strings',
			'description'  => __( 'Specify query strings for caching (one per line)', 'mainwp-rocket-extension' ),
		),
		array(
			'type'         => 'textarea',
			'label_for'    => 'cache_query_strings',
			'label_screen' => __( 'Cache pages that use the following query strings (GET parameters):', 'mainwp-rocket-extension' ),
		),
		array(
			'type'         => 'helper_description',
			'name'         => 'query_strings',
			'description'  =>				
            __( 'Cache for query strings enables you to force caching for specific GET parameters.', 'mainwp-rocket-extension' ),
		),
	)
);
