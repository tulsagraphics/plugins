<?php
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

add_settings_section( 'mainwp_rocket_display_optimization_options', '', '__return_false', 'rocket_optimization' );

/**
 * Panel caption
 */
add_settings_field(
	'rocket_optimization_options_panel',
	false,
	'mainwp_rocket_field',
	'rocket_optimization',
	'mainwp_rocket_display_optimization_options'
);

/**
 * Minification
 */
$rocket_maybe_disable_minify = array(
	'type'         => 'helper_detection',
	'name'         => 'minify_html_disabled',
);

/* Autoptimize? */
//if ( rocket_maybe_disable_minify_html() || rocket_maybe_disable_minify_css() || rocket_maybe_disable_minify_js() ) {
	$disabled = '';

	//if ( rocket_maybe_disable_minify_html() ) {
		$disabled .= 'HTML, ';
	//}

	//if ( rocket_maybe_disable_minify_css() ) {
		$disabled .= 'CSS, ';
	//}

	//if ( rocket_maybe_disable_minify_js() ) {
		$disabled .= 'JS, ';
	//}

	$disabled = rtrim( $disabled, ', ' );

	$rocket_maybe_disable_minify['description'] = sprintf( __( 'Minification (%s) is currently activated in <strong>Autoptimize</strong>. If you want to use WP Rocket’s minification, disable those options in Autoptimize.', 'mainwp-rocket-extension' ), $disabled );
//}

/* Dynamic warning */
$rocket_minify_fields = array();

/* Minify options */
$rocket_minify_fields[] = array(
	'type'         => 'checkbox',
	'label'        => 'Minify HTML',
	'name'         => 'minify_html',
	'label_screen' => '',
	'readonly'	   => false,
);

$rocket_minify_fields[] = array(
		'type'        => 'helper_description',		
		'description' => __( 'Minifying HTML removes whitespace and comments to reduce the size.', 'mainwp-rocket-extension' ),
);

$rocket_minify_fields[] = array(
	'type'		   => 'checkbox',
	'label'		   => 'Combine Google Fonts files',
	'name'		   => 'minify_google_fonts',
	'label_screen' => ''
);

$rocket_minify_fields[] = array(
	'type'        => 'helper_description',		
	'description' => __( 'Combining Google Fonts will reduce the number of HTTP requests.', 'mainwp-rocket-extension' ),
);

$rocket_minify_fields[] = array(
	'type'		   => 'checkbox',
	'label'		   => 'Remove query strings from static resources',
	'name'		   => 'remove_query_strings',
	'label_screen' => ''
);

$rocket_minify_fields[] = array(
	'type'        => 'helper_description',		
	'description' => __( 'Removes the version query string from static files (e.g. style.css?ver=1.0) and encodes it into the filename instead (e.g. style-1.0.css). Can improve your GTMetrix score.', 'mainwp-rocket-extension' ),
);

add_settings_field(
	'rocket_minify',
	 __( 'Basic Settings:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'rocket_optimization',
	'mainwp_rocket_display_optimization_options',
	$rocket_minify_fields
);

/**
 * Concatenation
 */

/* Dynamic warning */
$rocket_concatenate_fields = array();

$rocket_concatenate_fields[] = array(
			'type'        => 'helper_warning',
			'description' => __( 'This could break things!<br />If you notice any errors on your website after having activated this setting, just deactivate it again, and your site will be back to normal.', 'mainwp-rocket-extension' ),
);

$rocket_concatenate_fields[] = array(
		'name' => 'minify_css',
		'type'              => 'checkbox_new',
		'label'             => __( 'Minify CSS files', 'mainwp-rocket-extension' ),
		'description'       => __( 'Minify CSS removes whitespace and comments to reduce the file size.', 'mainwp-rocket-extension' ),
		'container_class'   => [
			'wpr-field--parent',
		],
		'default'           => 0,
);	

$rocket_concatenate_fields[] = array(
		'name'				=> 'minify_concatenate_css',
		'type'              => 'checkbox_new',	
		'parent'			=> 'minify_css',
		'label'             => __( 'Combine CSS files <em>(Enable Minify CSS files to select)</em>', 'mainwp-rocket-extension' ),
		'description'       => sprintf( __( 'Combine CSS merges all your files into 1, reducing HTTP requests. Not recommended if your site uses HTTP/2.', 'rocket' ) ),
		'container_class'   => [
			mainwp_get_rocket_option( 'minify_css', 0 ) ? '' : 'wpr-isDisabled',
			'wpr-field--parent'
		],
		'default'           => 0,
	);

$rocket_concatenate_fields[] = array(	
		'name'		=> 'exclude_css',
		'type'         => 'textarea',		
		'parent'			=> 'minify_css',
		'label'		=> __( 'Excluded CSS Files', 'mainwp-rocket-extension'),
		'sub_label'             => __( 'Specify URLs of CSS files to be excluded from minification and concatenation.', 'mainwp-rocket-extension' ),
		'placeholder'  => "/wp-content/plugins/some-plugin/(.*).css",
		'description' => sprintf( __( 'The domain part of the URL will be stripped automatically.<br>Use %s wildcards to exclude all CSS files located at a specific path.', 'mainwp-rocket-extension' ), '<code>(.*).css</code>' ),
		'container_class'   => [
			'exclude-css-row', 'child-css-row'
		],	
);

$rocket_concatenate_fields[] = array(
		'name' => 'async_css',
		'type'              => 'checkbox_new',
		'label'             => __( 'Optimize CSS delivery', 'mainwp-rocket-extension' ),
		'description'       => __( 'Optimize CSS delivery eliminates render-blocking CSS on your website for faster perceived load time.', 'mainwp-rocket-extension' ),
		'container_class'   => [
			'wpr-field--parent',
		],
		'default'           => 0	
);
$rocket_concatenate_fields[] = array(
	'type'         => 'textarea',
	'name'    => 'critical_css',
	'parent'  => 'async_css',
	'label_for'    => 'critical_css',
	'label'		=> __( 'Fallback critical CSS:', 'mainwp-rocket-extension' ),
	'label_screen' => __( 'Critical path CSS rules for rendering above-the-fold content', 'mainwp-rocket-extension' ),
	'container_class' => [
		'critical-css-row', 'child-css-row'
	],	
	'description'       => __( 'Provides a fallback if auto-generated critical path CSS is incomplete.', 'mainwp-rocket-extension' ),
);


add_settings_field(
	'rocket_concatenate',
	 __( 'CSS Files:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'rocket_optimization',
	'mainwp_rocket_display_optimization_options',
	$rocket_concatenate_fields
);

/**
 * Async CSS
 */
$rocket_render_blocking = array();

$rocket_render_blocking[] = array(
	'type'        => 'helper_warning',
	'description' => __( 'This could break things!<br />If you notice any errors on your website after having activated this setting, just deactivate it again, and your site will be back to normal.', 'mainwp-rocket-extension' ),
);

$rocket_render_blocking[] = array(
	'type'         => 'checkbox_new',
	'label'        => 'Minify JavaScript files',
	'name'         => 'minify_js',
	'label_screen' => __( 'JS Files minification', 'mainwp-rocket-extension' ),
	'readonly'     => false,
	'description' => __( 'Minify JavaScript removes whitespace and comments to reduce the file size.', 'mainwp-rocket-extension' ),
);

$rocket_render_blocking[] = array(
	'type'         => 'checkbox_new',
	'label'        => __( 'Combine JavaScript files <em>(Enable Minify JavaScript files to select)</em>', 'mainwp-rocket-extension'),	
	'name'         => 'minify_concatenate_js',
	'parent'         => 'minify_js',
	'label_screen' => 'Concatenate JS files',
	'readonly'	   => false,
	'description'		=> __( 'Combine Javascript files combines your site\'s JS info fewer files, reducing HTTP requests. Not recommended if your site uses HTTP/2.', 'mainwp-rocket-extension'),
);
 
$rocket_render_blocking[] = array(	
		'name'		=> 'exclude_js',
		'type'         => 'textarea',				
		'parent'         => 'minify_js',
		'label'			=> __( 'Excluded JavaScript Files:', 'mainwp-rocket-extension' ),
		'sub_label'             => __( 'Specify URLs of JavaScript files to be excluded from minification and concatenation.', 'mainwp-rocket-extension' ),
		'placeholder'  => "/wp-content/themes/some-theme/(.*).js",
		'description' => sprintf( __( 'The domain part of the URL will be stripped automatically. Use (.*).js wildcards to exclude all JS files located at a specific path.', 'mainwp-rocket-extension' ), '<code>(.*).css</code>' ),
		'container_class'   => [
			'exclude-js-row', 'child-css-row'
		],	
);

$rocket_render_blocking[] = array(
	'type'         => 'checkbox_new',
	'label'        => __( 'Load JavaScript deferred', 'mainwp-rocket-extension' ),
	'name'         => 'defer_all_js',
	'label_screen' => __( 'Load JavaScript deferred', 'mainwp-rocket-extension' ),
	'description'  => __( 'Load JavaScript deferred eliminates render-blocking JS on your site and can improve load time.', 'mainwp-rocket-extension' ),
);

$rocket_render_blocking[] = array(
		'name'	=> 'defer_all_js_safe',
		'type'              => 'checkbox_new',
		'parent'         => 'defer_all_js',
		'container_class'   => [
			'defer_all_js_safe-row', 'child-css-row'
		],		
		'label'             => __( 'Safe Mode for jQuery (recommended)', 'rocket' ),
		'description'       => __( 'Safe mode for jQuery for deferred JS ensures support for inline jQuery references from themes and plugins by loading jQuery at the top of the document as a render-blocking script.<br><em>Deactivating may result in broken functionality, test thoroughly!</em>', 'rocket' ),		
		'default'           => 1
);


add_settings_field(
	'rocket_render_blocking',
	 __( 'JavaScript Files:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'rocket_optimization',
	'mainwp_rocket_display_optimization_options',
	$rocket_render_blocking
);


/**
 * Deprecated options panel caption
 */
if ( mainwp_get_rocket_option( 'minify_js_in_footer') || mainwp_get_rocket_option( 'deferred_js' ) ) {
	add_settings_field(
		'rocket_optimization_deprected_options',
		false,
		'mainwp_rocket_field',
		'rocket_optimization',
		'mainwp_rocket_display_optimization_options',
		array(
			array(
				'type'         => 'helper_panel_description',
				'description'  => sprintf(
					'<span class="dashicons dashicons-warning" aria-hidden="true"></span><strong>%1$s</strong>',
					/* translators: line-break recommended, but not mandatory  */
					__( 'The options below will be deprecated in WP Rocket 3.0, in favor of the new options for render-blocking CSS/JS above. If you use those new options, the deprecated one for deferred JS gets ignored already.', 'mainwp-rocket-extension' )
				),
			),
		)
	);
}

/**
 * Legacy: JS to footer
 */
if ( mainwp_get_rocket_option( 'minify_js_in_footer' ) ) {
	add_settings_field(
		'mwp_minify_js_in_footer',
		__( 'Footer JS (deprecated):', 'mainwp-rocket-extension' ),
		'mainwp_rocket_field',
		'rocket_optimization',
		'mainwp_rocket_display_optimization_options',
		array(
			array(
				'type'                     => 'repeater',
				'label_screen'             => __( '<strong>JS</strong> files to be included in the footer during the minification process:', 'mainwp-rocket-extension' ),
				'name'                     => 'minify_js_in_footer',
				'placeholder'              => 'http://',
				'repeater_drag_n_drop'     => true,
				'repeater_label_add_field' => __( 'Add URL', 'mainwp-rocket-extension' ),
			),
			array(
				'type'         => 'helper_help',
				'name'         => 'minify_js_in_footer',
				'description'  => __( 'Empty the field to remove it.', 'mainwp-rocket-extension' ),
				'class'	       => 'hide-if-js',
			),
			array(
				'type'         => 'helper_description',
				'name'         => 'minify_js_in_footer',
				'description'  => __( 'Specify complete URLs like:  <code>http://example.com/path/to/script.js</code>', 'mainwp-rocket-extension' ),
			),
		)
	);
}
