<?php 
defined( 'ABSPATH' ) or die( 'Cheatin\' uh?' );

add_settings_section( 'mainwp_rocket_display_preload_options', '', '__return_false', 'rocket_preload' );

$sitemap_preload_options = array(
    array(
        'type'         => 'checkbox',
	    'label'        => __('Activate sitemap-based cache preloading', 'mainwp-rocket-extension' ),
	    'label_for'    => 'sitemap_preload',
	    'name'         => 'sitemap_preload',
	    'label_screen' => __( 'Activate sitemap preloading', 'mainwp-rocket-extension' ),
	    'default'      => 0,
		'description'  => __( 'Sitemap preloading runs automatically when the cache lifespan expires. You can also launch it manually from the upper toolbar menu, or from Quick Actions on the WP Rocket Dashboard.', 'mainwp-rocket-extension' )
    ),    
	array(
			'type'         => 'textarea',
			'label'        => __( 'Sitemaps for preloading', 'mainwp-rocket-extension' ),
			'sub_label'       => __( 'Specify XML sitemap(s) to be used for preloading', 'mainwp-rocket-extension' ),
			'name'         => 'sitemaps',
			'placeholder'       => 'http://example.com/sitemap.xml',	
			'container_class'   => [
				'sitemaps-row', 'child-css-row'
			],
			'parent'	=> 'sitemap_preload',
			'description' => 'Support token: %url%'			
		)		
);

add_settings_field(
	'mwp_rocket_sitemap_preload_activate',
	 __( 'Sitemap preloading:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'rocket_preload',
	'mainwp_rocket_display_preload_options',
    apply_filters( 'rocket_sitemap_preload_options', $sitemap_preload_options )
);




add_settings_field(
	'mwp_rocket_enable_bot_preload',
	__( 'Preload bot:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'rocket_preload',
	'mainwp_rocket_display_preload_options',
	array(
		array(
			'type'         => 'checkbox',
			'label'        => __('Manual', 'mainwp-rocket-extension' ),
			'label_for'    => 'manual_preload',
			'label_screen' => __( 'Activate manual preload (from admin bar or Tools tab of WP Rocket)', 'mainwp-rocket-extension' ),
			'default'      => 1,
		),
		array(
			'type'         => 'checkbox',
			'label'        => __('Automatic', 'mainwp-rocket-extension' ),
			'label_for'    => 'automatic_preload',
			'label_screen' => __( 'Activate automatic preload after partial cache clearing', 'mainwp-rocket-extension' ),
			'default'      => 1,
		),
		array(
			'type'        => 'helper_description',
			'name'        => 'bot_preload',
			'description' => __( 'Bot-based preloading should only be used on well-performing servers. <br />Once activated, it gets triggered automatically after you add or update content on your website. You can also launch it manually from the upper toolbar menu, or from Quick Actions on the WP Rocket Dashboard.', 'mainwp-rocket-extension' ),
		),
		array(
			'type'        => 'helper_warning',
			'name'        => 'bot_preload_warning',
			'description' => __( 'Deactivate these options in case you notice any overload on your server!', 'mainwp-rocket-extension' ),
		),
	)
);


//add_settings_field(
//	'mwp_rocket_sitemap_preload_interval',
//	 __( 'Sitemap crawl interval:', 'mainwp-rocket-extension' ),
//	'mainwp_rocket_field',
//	'rocket_preload',
//	'mainwp_rocket_display_preload_options',
//	array(
//        array(
//            'type'         => 'select',
//			'label'        => __('waiting time between each URL crawl', 'mainwp-rocket-extension' ),
//			'label_for'    => 'sitemap_preload_url_crawl',
//			'label_screen' => __( 'Sets the intervall between each URL crawl', 'mainwp-rocket-extension' ),
//			/*
//             * Filters the array of options interval for sitemap preload
//             *
//             * @since 2.8
//             *
//             * @param array Array of options interval defined by a $value => $label pair
//             */
//			'options'      => apply_filters( 'rocket_sitemap_preload_interval', array(
//    			'250000'      => '250ms',
//    			'500000'      => '500ms',
//    			'750000'      => '750ms',
//    			'1000000'     => '1s',
//    			'2000000'     => '2s',
//			) )
//        ),
//        array(
//			'type'			=> 'helper_description',
//			'name'			=> 'sitemaps_preload_url_crawl_desc',			
//		),
//		array(
//			'type'			=> 'helper_warning',
//			'name'			=> 'sitemaps_preload_url_crawl_warning',
//			'description'  => __( 'Set a higher value in case you notice any overload on your server!', 'mainwp-rocket-extension' )
//		),
//	)
//);

add_settings_field(
	'mwp_rocket_dns_prefetch',
	__( 'Prefetch DNS requests:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'rocket_preload',
	'mainwp_rocket_display_preload_options',
	array(        
		array(
			'type'         => 'textarea',
			'label_for'    => 'dns_prefetch',
			'label'		=> 'URLs to prefetch',
			'sub_label'  => __( 'Specify external hosts to be prefetched (no <code>http:</code>, one per line)', 'rocket' ),
			'label_screen' => __('Prefetch DNS requests:', 'mainwp-rocket-extension' ),
			'placeholder'  => "//example.com",
			'description'  => __( 'DNS prefetching can make external files load faster, especially on mobile networks.', 'rocket' ),
		)		
	)
);