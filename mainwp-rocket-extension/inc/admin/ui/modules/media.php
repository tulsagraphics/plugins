<?php 
defined( 'ABSPATH' ) or die( 'Cheatin\' uh?' );

add_settings_section( 'mainwp_rocket_display_media_options', '', '__return_false', 'media' );

add_settings_field(
	'rocket_media_options_panel',
	false,
	'mainwp_rocket_field',
	'media',
	'mainwp_rocket_display_media_options'
);

add_settings_field(
	'rocket_lazyload',
	__( 'LazyLoad:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'media',
	'mainwp_rocket_display_media_options',
	array(
		array(
			'type'         => 'checkbox',
			'label'        => __('Enable for images', 'mainwp-rocket-extension' ),
			'label_for'    => 'lazyload',
			'label_screen' => __( 'Lazyload on images', 'mainwp-rocket-extension' ),
		),
		array(
			'type'         => 'checkbox',
			'label'        => __('Enable for iframes and videos', 'mainwp-rocket-extension' ),
			'label_for'    => 'lazyload_iframes',
			'label_screen' => __( 'Lazyload on iframes and videos', 'mainwp-rocket-extension' ),
		), 
		array(
			'type'         => 'checkbox_new',
			'name'         => 'lazyload_youtube',
			'label'        => __('Replace YouTube iframe with preview image', 'mainwp-rocket-extension' ),						
			'description'       => __( 'This can significantly improve your loading time if you have a lot of YouTube videos on a page.', 'rocket' ),
			'default'           => 0,
			'container_class'   => [
				'lazyload_youtube-row', 'child-css-row'
			],
			'parent'    => 'lazyload_iframes',
		),
		array(
			'type'         => 'helper_description',
			'name'         => 'lazyload',
			'description'  =>  __('It can improve actual and perceived loading time as images, iframes, and videos will be loaded only as they enter (or about to enter) the viewport and reduces the number of HTTP requests.', 'mainwp-rocket-extension' )
		),
	)
);


add_settings_field(
	'rocket_emoji',
	__( 'Emoji:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'media',
	'mainwp_rocket_display_media_options',
	array(
		array(
			'type'         => 'checkbox',
			'label'        => __( 'Disable Emoji', 'mainwp-rocket-extension' ),
			'label_for'    => 'emoji',
			'label_screen' => __( 'Emoji cache:', 'mainwp-rocket-extension' ),
		),		
		array(
			'type'         => 'helper_description',
			'name'         => 'emoji_perf_tip',
			'description'  => __( 'Disable Emoji will reduce the number of external HTTP requests.', 'mainwp-rocket-extension' )
		),
	)
);

/**
 * Disable Embeds
 */
add_settings_field(
	'rocket_wordpress_embeds',
	__( 'Embeds:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'media',
	'mainwp_rocket_display_media_options',
	array(
		array(
			'type'         => 'checkbox',
			'label'        => __( 'Disable WordPress Embeds', 'mainwp-rocket-extension' ),
			'label_for'    => 'embeds',
			'label_screen' => __( 'Embeds:', 'mainwp-rocket-extension' ),
		),
		array(
			'type'         => 'helper_description',
			'name'         => 'embeds',
			'description'  => __( 'Prevents others from embedding content from your site, prevents you from embedding content from other (non-whitelisted) sites, and removes JavaScript requests related to WordPress embeds.', 'mainwp-rocket-extension' ),
		),
	)
);
