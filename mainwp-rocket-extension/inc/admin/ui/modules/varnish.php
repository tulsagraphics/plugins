<?php
defined( 'ABSPATH' ) or die( 'Cheatin\' uh?' );

add_settings_section( 'mainwp_rocket_display_main_options', '', '__return_false', 'rocket_varnish' );

add_settings_field(
	'mwp_rocket_varnish_options_panel',
	false,
	'mainwp_rocket_field',
	'rocket_varnish',
	'mainwp_rocket_display_main_options'
);

add_settings_field(
	'mwp_rocket_varnish_auto_purge',
	__( 'Sync Varnish cache:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'rocket_varnish',
	'mainwp_rocket_display_main_options',
	array(
		array(
			'type'         => 'checkbox',
			'label'        => __('Purge Varnish cache automatically', 'mainwp-rocket-extension' ),
			'label_for'    => 'varnish_auto_purge',
			'label_screen' => __( 'Sync Varnish cache with plugin cache', 'mainwp-rocket-extension' ),
		),
		array(
			'type'         => 'helper_description',
			'name'         => 'varnish_auto_purge',
			'description'  => __( 'Varnish cache will be purged each time WP Rocket clears its cache to ensure content is always up to date.', 'mainwp-rocket-extension' ),
		),
	)
);

