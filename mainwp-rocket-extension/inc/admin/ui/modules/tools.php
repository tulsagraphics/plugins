<?php 
defined( 'ABSPATH' ) or die( 'Cheatin\' uh?' );

add_settings_section( 'mainwp_rocket_display_tools', '', '__return_false', 'tools' );

add_settings_field(
        'rocket_export_options',
        __( 'Export settings:', 'mainwp-rocket-extension' ),
        'mainwp_rocket_field',
        'tools',
        'mainwp_rocket_display_tools',
        array( 'type'=>'rocket_export_form', 'name'=>'export' )

);

add_settings_field(
        'rocket_import_options',
        __( 'Import settings:', 'mainwp-rocket-extension' ),
        'mainwp_rocket_field',
        'tools',
        'mainwp_rocket_display_tools',
        array( 'type'=>'mainwp_rocket_import_upload_form' )

);

