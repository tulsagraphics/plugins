<?php 
defined( 'ABSPATH' ) or die( 'Cheatin\' uh?' );

add_settings_section( 'mainwp_rocket_display_cdn_options', '', '__return_false', 'cdn' );
//add_settings_field(
//	'rocket_do_cloudflare',
//	'CloudFlare',
//	'mainwp_rocket_field',
//	'cdn',
//	'mainwp_rocket_display_cdn_options',
//	array(
//		array(
//			'type'         => 'checkbox',
//			'label'        => __( 'Show Cloudflare settings tab ', 'mainwp-rocket-extension' ),
//			'label_for'    => 'do_cloudflare',
//			'label_screen' => 'CloudFlare'
//		),		
//	)
//);

if ( 0 !== absint( mainwp_get_rocket_option('do_cloudflare') ) ) {
	add_settings_field(
		'rocket_cdn_options_panel',
		false,
		'mainwp_rocket_field',
		'cdn',
		'mainwp_rocket_display_cdn_options',
		array(
			array(
				'type'         => 'helper_panel_description',
				'name'         => 'cdn_options_panel_caption',
				'description'  => sprintf(
					'<span class="dashicons dashicons-cloud" aria-hidden="true"></span><strong>%1$s</strong>',
					 __( 'Go to the <strong>Cloudflare tab</strong> to edit your Cloudflare settings. The CDN settings below do NOT apply to Cloudflare.', 'mainwp-rocket-extension' )
				),
			),
		)
	);
}

add_settings_field(
	'rocket_cdn',
	__( 'CDN:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'cdn',
	'mainwp_rocket_display_cdn_options',
	array(
		array(
			'type'         => 'checkbox',
			'label'        => __('Enable Content Delivery Network.', 'mainwp-rocket-extension' ),
			'label_for'    => 'cdn',
			'label_screen' => __( 'CDN:', 'mainwp-rocket-extension' )
		),
		array(
			'type' 		  => 'helper_description',
			'name' 		  => 'cdn',
			'description' =>
			/* translators: line-break recommended, but not mandatory; use URL of localised document if available in your language  */
			__( 'All URLs of static files (CSS, JS, images) will be rewritten to the CNAME(s) entered below.', 'mainwp-rocket-extension' ),
		),
	)
);

add_settings_field(
	'rocket_cdn_cnames',
	__( 'CDN CNAME(S):', 'mainwp-rocket-extension' ),
	'mainwp_rocket_cnames_module',
	'cdn',
	'mainwp_rocket_display_cdn_options'
);

//add_settings_field(
//	'rocket_cdn_on_ssl',
//	'CDN without SSL:',
//	'mainwp_rocket_field',
//	'cdn',
//	'mainwp_rocket_display_cdn_options',
//	array(
//		array(
//			'type'         => 'checkbox',
//			'label'        => __('Disable CDN functionality on HTTPS pages.', 'mainwp-rocket-extension' ),
//			'label_for'    => 'cdn_ssl',
//			'label_screen' => 'CDN without SSL:',
//		),
//        array(
//			'type'         => 'helper_description',
//			'description'  => __( 'In case your CDN account does not fully support SSL, you can disable URL rewriting on HTTPS pages here.', 'rocket' ),
//		),
//	)
//);


add_settings_field(
	'rocket_cdn_reject_files',
	__( 'Exclude files from CDN:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'cdn',
	'mainwp_rocket_display_cdn_options',
	array(        
		array(
			'type'         => 'textarea',
			'label_for'    => 'cdn_reject_files',
			'sub_label'  => __( 'Specify URL(s) of files that should not get served via CDN', 'mainwp-rocket-extension' ),
			'label_screen' => __( 'Rejected files:', 'mainwp-rocket-extension' ),			
			'description'  =>			
			sprintf( __( 'The domain part of the URL will be stripped automatically.<br>Use %s wildcards to exclude all files of a given file type located at a specific path.', 'rocket' ), '<code>(.*)</code>' ),
			'placeholder'       => '/wp-content/plugins/some-plugins/(.*).css',
		)
	)
);