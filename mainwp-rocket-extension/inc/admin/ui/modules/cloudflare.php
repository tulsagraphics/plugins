<?php 
defined( 'ABSPATH' ) or die( 'Cheatin\' uh?' );

add_settings_section( 'mainwp_rocket_display_cloudflare_options', '', '__return_false', 'cloudflare' );

add_settings_field(
	'rocket_cloudflare_api_key',
    _x( 'Global API key:', 'Cloudflare', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'cloudflare',
	'mainwp_rocket_display_cloudflare_options',
	array(
        array(
			'type'        => 'helper_help',
			'description' => __( 'Enter the global API key of your Cloudflare account', 'mainwp-rocket-extension' ),
		),
		array(
			'type'         => 'text',
			'label_for'    => 'cloudflare_api_key',
			'label_screen' => __( 'Global API key', 'mainwp-rocket-extension' ),
		),
		array(
			'type'         => 'helper_description',
			'description'  => sprintf( __( '<a href="%s" target="_blank">Retrieve your API key</a>', 'mainwp-rocket-extension' ), 'https://support.cloudflare.com/hc/en-us/articles/200168246' ),
		)
	)
);

add_settings_field(
	'rocket_cloudflare_email',
    _x( 'Account email:', 'Cloudflare', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'cloudflare',
	'mainwp_rocket_display_cloudflare_options',
	array(
        array(
			'type' 		   => 'helper_help',
			'description'  => __( 'Enter the email address of your Cloudflare account', 'mainwp-rocket-extension' ),
		),
		array(
			'type'         => 'text',
			'label_for'    => 'cloudflare_email',
			'label_screen' => __( 'Cloudflare account email address', 'mainwp-rocket-extension' ),
		)
	)
);

if (self::is_manage_sites_page()) {
    add_settings_field(
            'rocket_cloudflare_domain',
            __( 'Domain', 'mainwp-rocket-extension' ),
            'mainwp_rocket_field',
            'cloudflare',
            'mainwp_rocket_display_cloudflare_options',
            array(
                    array(
                            'type'         => 'text',
                            'label_for'    => 'cloudflare_domain',
                            'label_screen' => __( 'Domain', 'mainwp-rocket-extension' ),
                    )
            )
    );
} else {  
    add_settings_field(
	'rocket_cloudflare_domain',
	__( 'Domain', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'cloudflare',
	'mainwp_rocket_display_cloudflare_options',
	array(
		array(
			'type'		   => 'checkbox',
			'label'		   => __( 'Set child site URL as a domain.', 'mainwp-rocket-extension' ),
			'label_for'	   => 'cloudflare_domain',
			'label_screen' => __( 'Domain', 'mainwp-rocket-extension' ),
		),		
	)
);
    

}

add_settings_field(
	'rocket_cloudflare_devmode',
	__( 'Development Mode', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'cloudflare',
	'mainwp_rocket_display_cloudflare_options',
	array(
		array(
			'type'         => 'select',
			'label_for'    => 'cloudflare_devmode',
			'label_screen' => __( 'Development Mode', 'mainwp-rocket-extension' ),
			'options'	   => array(
				0 => __( 'Off', 'mainwp-rocket-extension' ),
				1 => __( 'On', 'mainwp-rocket-extension' )
			),
		),
		array(
			'type' 		   => 'helper_description',
			'name'         => 'cloudflare_devmode',
			'description'  => sprintf( __( 'Temporarily enter development mode on your website. This setting will automatically get turned off after 3 hours. <a href="%s" target="_blank">Learn more</a>', 'mainwp-rocket-extension' ), 'https://support.cloudflare.com/hc/en-us/articles/200168246' ),
		),
	)
);
add_settings_field(
	'rocket_cloudflare_auto_settings',
	_x( 'Optimal settings:', 'Cloudflare', 'mainwp-rocket-extension' ),
	'mainwp_rocket_field',
	'cloudflare',
	'mainwp_rocket_display_cloudflare_options',
	array(
		array(
			'type'         => 'select',
			'label_for'    => 'cloudflare_auto_settings',
			'label_screen' => __( 'Auto enable the optimal CloudFlare settings (props WP Rocket)', 'mainwp-rocket-extension' ),
			'options'	   => array(
				0 => __( 'No', 'mainwp-rocket-extension' ),
				1 => __( 'Yes', 'mainwp-rocket-extension' )
			),
		),
		array(
			'type' 		   => 'helper_description',
			'name'         => 'cloudflare_auto_settings',
			'description'  => __( 'Automatically enhances your Cloudflare configuration for speed, performance grade, and compatibility.', 'rocket' ),
		)
	)
);

/**
 * CF relative protocol
 */
add_settings_field(
	'rocket_cloudflare_protocol_rewrite',
	_x( 'Relative Protocol:', 'Cloudflare', 'rocket' ),
	'mainwp_rocket_field',
	'cloudflare',
	'mainwp_rocket_display_cloudflare_options',
	array(
		array(
			'type'         => 'select',
			'label_for'    => 'cloudflare_protocol_rewrite',
			'label_screen' => __( 'HTTPS Protocol Rewrite', 'rocket' ),
			'options'	   => array(
				0 => __( 'Off', 'rocket' ),
				1 => __( 'On', 'rocket' ),
			),
		),
		array(
			'type' 		   => 'helper_description',
			'name'         => 'cloudflare_protocol_rewrite',
			'description'  => sprintf(
				__( 'Should only be used with Cloudflare’s <a href="%1$s" target="_blank">Flexible SSL</a> feature.<br>URLs of static files (CSS, JS, images) will be rewritten to use relative protocol (%2$s instead of %3$s or %4$s).', 'rocket' ),
				'https://support.cloudflare.com/hc/en-us/articles/200170416-What-do-the-SSL-options-Off-Flexible-SSL-Full-SSL-Full-SSL-Strict-mean-',
				'<code>//</code>',
				'<code>http://</code>',
				'<code>https://</code>'
			),
		),
	)
);

if (MainWP_Rocket::is_manage_sites_page()) {
    $purge_url = "#";   
    $purge_status = '<span id="mwp_rocket_individual_purge_cloudflare_status">
                            <i style="display: none;" class="fa fa-spinner fa-pulse"></i>
                                <span class="status hidden"></span></span>';
    $purge_onclick = 'mainwp_rocket_individual_purge_cloudflare(' . $_GET['id'] . '); return false;';
} else {
    $purge_url = wp_nonce_url( admin_url( "admin.php?page=Extensions-Mainwp-Rocket-Extension&_perform_action=mainwp_rocket_purge_cloudflare" ), 'mainwp_rocket_purge_cloudflare' );
    $purge_status = "";
    $purge_onclick = "";
}

add_settings_field(
	'mainwp_rocket_purge_cloudflare',
	__( 'Clear Cloudflare cache:', 'mainwp-rocket-extension' ),
	'mainwp_rocket_button',
	'cloudflare',
	'mainwp_rocket_display_cloudflare_options',
	array(
		'button'=>array(
			'button_label' => __( 'Clear Cloudflare cache', 'mainwp-rocket-extension' ),
			'url'		   => $purge_url,                      
            'onclick' => $purge_onclick,
            'status' => $purge_status
		),
		'helper_description'=>array(
			'name'         => 'purge_cloudflare',
			'description'  => sprintf( __( 'Purges cached resources for your website. <a href="%s" target="_blank">Learn more</a>', 'rocket' ), 'https://support.cloudflare.com/hc/en-us/articles/200169246' ),
		),
	)
);