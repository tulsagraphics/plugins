jQuery( document ).ready( function($){
	// Fancybox
	$(".fancybox").fancybox({'type' : 'iframe'});

	// Deferred JS
	function mainwp_rocket_deferred_rename()
	{
		$('#rkt-drop-deferred .rkt-module-drag').each( function(i){
			var $item_t_input = $(this).find( 'input[type=text]' );
			var $item_c_input = $(this).find( 'input[type=checkbox]' );
			$($item_t_input).attr( 'name', 'mainwp_wp_rocket_settings[deferred_js_files]['+i+']' );
			$($item_c_input).attr( 'name', 'mainwp_wp_rocket_settings[deferred_js_wait]['+i+']' );
		});
	}

	// Minify JS in footer
	function mainwp_rocket_minify_js_rename() {
		$('#rkt-drop-minify_js_in_footer .rkt-module-drag').each( function(i){
			var $item_t_input = $(this).find( 'input[type=text]' );
			$($item_t_input).attr( 'name', 'mainwp_wp_rocket_settings[minify_js_in_footer]['+i+']' );
		});
	}

	$('.mainwp-rkt-module-drop').sortable({
		update : function() {
			if ( $(this).attr('id') == 'rkt-drop-deferred' ) {
				mainwp_rocket_deferred_rename();
			}

			if ( $(this).attr('id') == 'rkt-drop-minify_js_in_footer' ) {
				mainwp_rocket_minify_js_rename();
			}
		},
		axis: "y",
		items: ".rkt-module-drag",
		containment: "parent",
		cursor: "move",
		handle: ".rkt-module-move",
		forcePlaceholderSize: true,
		dropOnEmpty: false,
		placeholder: 'sortable-placeholder',
		tolerance: 'pointer',
		revert: true,
	});

	// Remove input
	$('.mainwp-rkt-module-remove').css('cursor','pointer').live('click', function(e){
		e.preventDefault();
		$(this).parent().css('background-color','red' ).slideUp( 'slow' , function(){$(this).remove(); } );
	} );

	// CNAMES
	$('.mainwp-rkt-module-clone').on('click', function(e)
	{
		var moduleID = $(this).parent().siblings('.rkt-module').attr('id');

		e.preventDefault();
		$($('#' + moduleID ).siblings('.rkt-module-model:last')[0].innerHTML).appendTo('#' + moduleID);

		if( moduleID == '' ) {
			mainwp_rocket_deferred_rename();
		}

	});
			
         // Inputs with parent
	$('.has-parent').each( function() {
		var input  = $(this),
                parent = $('#'+$(this).data('parent'));		
		parent.change( function() {
			if( parent.is(':checked') ) {
				input.parents('fieldset').show(200);
			} else {
				input.parents('fieldset').hide(200);
			}
		});

		if( ! parent.is(':checked') ) {
			$(this).parents('fieldset').hide();
		}
	});
        

        $( '#async_css, #minify_css, #minify_js, #defer_all_js, #lazyload_iframes, #sitemap_preload' ).each(function() {	
                obj = $(this);                
                var child = $('.child-row-' + obj.attr('id'));                 
                if ( !obj.is( ':checked' ) ) {
                    child.hide(); 
                } 
                obj.change( function() {
                    child.toggle( 'fast' );
                    if ( !obj.is( ':checked' ) ) {
                        child.find('input[type=checkbox]').prop( 'checked', false ); 
                    }
                });                
        })
        
        
       
	// Sweet Alert for CloudFlare activation
	$( '#do_cloudflare' ).click(function() {
		if ( $(this).is( ':checked' ) ) {
			swal({
				title: mwp_sawpr.cloudflareTitle,
				text: mwp_sawpr.cloudflareText,
				timer: 5000
			});
		}
	});

        
//        var async_css 		 = $( '#async_css' );
//	var critical_css_row = $( '.critical-css-row' );
//
//	if ( ! async_css.is( ':checked' ) ) {
//		critical_css_row.hide();
//	}
//
//	async_css.change( function() {
//		critical_css_row.toggle( 'fast' );
//	});
//
//	var minify_css 		= $( '#minify_css' );
//	var concatenate_css	= $( '.fieldname-minify_concatenate_css' );
//	var exclude_css_row = $( '.exclude-css-row' );
//
//	if ( ! minify_css.is( ':checked' ) ) {
//		concatenate_css.hide();
//		exclude_css_row.hide();
//	}
//
//	minify_css.change( function() {
//		if ( ! minify_css.is( ':checked' ) ) {
//			concatenate_css.find( '#minify_concatenate_css' ).prop( 'checked', false );
//			$( '.fieldname-minify_css_combine_all' ).hide();
//		}
//
//		concatenate_css.toggle( 'fast' );
//		exclude_css_row.toggle( 'fast' );
//	});
//
//	var minify_js	   = $( '#minify_js' );
//	var concatenate_js = $( '.fieldname-minify_concatenate_js' );
//	var exclude_js_row = $( '.exclude-js-row' );
//
//	if ( ! minify_js.is( ':checked' ) ) {
//		concatenate_js.hide();
//		exclude_js_row.hide();
//	}
//
//	minify_js.change( function() {
//		if ( ! minify_js.is( ':checked' ) ) {
//			concatenate_js.find( '#minify_concatenate_js' ).prop( 'checked', false );
//			$( '.fieldname-minify_js_combine_all' ).hide();
//		}
//
//		concatenate_js.toggle( 'fast' );
//		exclude_js_row.toggle( 'fast' );
//	});
//        
//        
//        var defer_all_js	   = $( '#defer_all_js' );
//        var defer_all_js_safe = $( '.defer_all_js_safe-row' );
//        
//        if ( ! defer_all_js.is( ':checked' ) ) {		
//		defer_all_js_safe.hide();
//	}
//
//	defer_all_js.change( function() {
//                if ( ! defer_all_js.is( ':checked' ) ) {
//                    defer_all_js_safe.find( '#defer_all_js_safe' ).prop( 'checked', false );			
//		}                
//		defer_all_js_safe.toggle( 'fast' );
//	});
//        
//        var lazyload_iframes	   = $( '#lazyload_iframes' );
//        var lazyload_youtube = $( '.lazyload_youtube-row' );
//        
//        if ( ! lazyload_iframes.is( ':checked' ) ) {		
//		lazyload_youtube.hide();
//	}
//
//	lazyload_iframes.change( function() {
//                if ( ! lazyload_iframes.is( ':checked' ) ) {
//                    lazyload_youtube.find( '#lazyload_youtube' ).prop( 'checked', false );			
//		}                
//		lazyload_youtube.toggle( 'fast' );
//	});
//        
        
        
	// Support form
	$( '#submit-support-button' ).click( function(e) {
		e.preventDefault();

		var summary 	= $('#support_summary').val().trim(),
			description = $('#support_description').val().trim(),
			validation  = $('#support_documentation_validation'),
			wpnonce		= $('#_wpnonce').val();

		if ( ! validation.is( ':checked' ) ) {
			swal({
				title : mwp_sawpr.warningSupportTitle,
				text  : mwp_sawpr.warningSupportText,
				type  : "warning",
				html  : true
			});
		}
		
		if ( validation.is( ':checked' ) && ( summary == '' || description == '' ) ) {
			swal({
				title : mwp_sawpr.requiredTitle,
				type  : "warning",
			});
		}

		if ( summary != '' && description != '' && validation.is( ':checked' ) ) {

			swal({
				title: mwp_sawpr.preloaderTitle,
				showCancelButton: false,
				showConfirmButton: false,
				imageUrl: mwp_sawpr.preloaderImg,
			});

			$.post(
				ajaxurl,
				{
					action: 'rocket_new_ticket_support',
					summary: summary,
					description: description,
					_wpnonce: wpnonce,
				},
				function(response) {
					response = JSON.parse(response);
					var title, text, type, confirmButtonText, confirmButtonColor;
					if( response.msg == 'BAD_EMAIL' ) {
						title              = mwp_sawpr.badSupportTitle;
						text               = mwp_sawpr.badSupportText;
						confirmButtonText  = mwp_sawpr.badConfirmButtonText;
						confirmButtonColor = "#f7a933";
						type               = "error";
					}

					if( response.msg == 'BAD_LICENCE' ) {
						title = mwp_sawpr.expiredSupportTitle;
						text  = mwp_sawpr.expiredSupportText;
						confirmButtonText  = mwp_sawpr.expiredConfirmButtonText;
						confirmButtonColor = "#f7a933";
						type  = "warning";
					}
					
					if( response.msg == 'BAD_CONNECTION' ) {
						title = mwp_sawpr.badServerConnectionTitle;
						text  = mwp_sawpr.badServerConnectionText;
						confirmButtonText  = mwp_sawpr.badServerConnectionConfirmButtonText;
						confirmButtonColor = "#f7a933";
						type  = "error";
					}
					
					if( response.msg == 'SUCCESS' ) {
						title = mwp_sawpr.successSupportTitle;
						text  = mwp_sawpr.successSupportText;
						type  = "success";

						// Reset the values
						$('#support_summary, #support_description, #support_documentation_validation').val('');
					}

					swal({
						title : title,
						text  : text,
						type  : type,
						confirmButtonText : confirmButtonText,
						confirmButtonColor : confirmButtonColor,
						html  : true
					},
					function() {
						if( response.msg == 'BAD_EMAIL' ) {
							window.open(response.order_url);
						}

						if( response.msg == 'BAD_LICENCE' ) {
							window.open(response.renew_url);
						}
						
						if( response.msg == 'BAD_CONNECTION' ) {
							window.open('http://wp-rocket.me/support/');
						}
					});
				}
			);
		}
	});
	
	$('#support_summary').parents('fieldset').append( '<div id="support_searchbox" class="hidden"><p><strong>These articles should help you resolving your issue (EN):</strong></p><div id="support_searchbox-suggestions"><ul></ul></div></div>' );
	
    // Live Search Cached Results
    last_search_results = new Array();
    
	 //Listen for the event
	$( "#support_summary" ).on( "keyup", function(e) {
		// Set Search String
		var query_value = $(this).val();
		// Set Timeout
		clearTimeout($.data(this, 'timer'));

		if ( query_value.length < 3 ) {
			$("#support_searchbox").fadeOut();
			$(this).parents('fieldset').attr( 'data-loading', "false" );
			return;
		}

		if ( last_search_results[ query_value ] != undefined ) {
			$(this).parents('fieldset').attr( 'data-loading', "false" );
			$("#support_searchbox-suggestions ul").html(last_search_results[ query_value ]);
			$("#support_searchbox").fadeIn();
			return;
		}
		// Do Search
		$(this).parents('fieldset').attr( 'data-loading', "true" );
		$(this).data('timer', setTimeout(search, 200));
	});
    
    // Live Search
    // On Search Submit and Get Results
    function search() {
        var query_value = $('#support_summary').val();
        if( query_value !== '' ) {
            $.ajax({
                type: "POST",
                url: ajaxurl,
                data: {
	                action : 'rocket_helpscout_live_search',
	                query  : query_value
	            },
                success: function(html) {
	                html = JSON.parse(html);
                    if ( html ) {
	                	last_search_results[ query_value ] = html;
	                	$("#support_searchbox-suggestions ul").html(html);
						$("#support_searchbox").fadeIn();
					}
                    $('#support_summary').parents('fieldset').attr( 'data-loading', "false" );
                }
            });
        }
        return false;
    }
} );