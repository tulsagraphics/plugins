<?php 
defined( 'ABSPATH' ) or die( 'Cheatin\' uh?' );

/**
 * Add a link "Purge cache" in the post submit area
 *
 * @since 1.0
 * @todo manage all CPTs
 *
 */
add_action( 'post_submitbox_start', '__mainwp_rocket_post_submitbox_start' );
function __mainwp_rocket_post_submitbox_start()
{
	/** This filter is documented in inc/admin-bar.php */
	if ( current_user_can( apply_filters( 'mainwp_rocket_capacity', 'manage_options' ) ) ) {
		global $post;
		$url = wp_nonce_url( admin_url( 'admin-post.php?action=purge_cache&type=post-' . $post->ID ), 'purge_cache_post-' . $post->ID );
		printf( '<div id="purge-action"><a class="button-secondary" href="%s">%s</a></div>', $url, __( 'Clear cache', 'mainwp-rocket-extension' ) );
	}
}

/**
 * Add "Cache options" metabox
 *
 * @since 2.5
 *
 */
add_action( 'add_meta_boxes', '__mainwp_rocket_cache_options_meta_boxes' );
function __mainwp_rocket_cache_options_meta_boxes() {
	if ( current_user_can( apply_filters( 'mainwp_rocket_capacity', 'manage_options' ) ) ) {
		$cpts = get_post_types( array( 'public' => true ), 'objects' );
		unset( $cpts['attachment'] );
		
		foreach( $cpts as $cpt => $cpt_object ) {
			$label = $cpt_object->labels->singular_name;
			add_meta_box( 'mainwp_rocket_post_exclude', sprintf( __( 'Cache Options', 'mainwp-rocket-extension' ), $label ), '__mainwp_rocket_display_cache_options_meta_boxes', $cpt, 'side', 'core' );
		}
	}
}

/*
 * Displays some checkbox to de/activate some cache options
 *
 * @since 2.5
 */
function __mainwp_rocket_display_cache_options_meta_boxes() {
	/** This filter is documented in inc/admin-bar.php */
	if ( current_user_can( apply_filters( 'mainwp_rocket_capacity', 'manage_options' ) ) ) {
		global $post;
		wp_nonce_field( 'rocket_box_option', '_rocketnonce', false, true );
		?>

		<div class="misc-pub-section">
			<p><?php _e( 'Activate these options on this post:', 'mainwp-rocket-extension' ) ;?></p>
			<?php
			$fields = array(
				'lazyload'  	   => __( 'Images LazyLoad', 'mainwp-rocket-extension' ),
				'lazyload_iframes' => __( 'Iframes & Videos LazyLoad', 'mainwp-rocket-extension' ),
				'minify_html'      => __( 'HTML Minification', 'mainwp-rocket-extension' ),
				'minify_css'       => __( 'CSS Minification', 'mainwp-rocket-extension' ),
				'minify_js'        => __( 'JS Minification', 'mainwp-rocket-extension' ),
				'cdn'              => __( 'CDN', 'mainwp-rocket-extension' ),
			);

			foreach ( $fields as $field => $label ) {
				$disabled = false; //disabled( ! mainwp_get_rocket_option( $field ), true, false );                               
				$title    = $disabled ? ' title="' . sprintf( __( 'Activate first the %s option.', 'mainwp-rocket-extension' ), esc_attr( $label ) ) . '"' : '';
				$class    = $disabled ? ' class="rkt-disabled"' : '';
				$checked   = ! $disabled ? checked( ! get_post_meta( $post->ID, '_rocket_exclude_' . $field, true ), true, false ) : '';
 				?>

				<input name="mainwp_rocket_post_exclude_hidden[<?php echo $field; ?>]" type="hidden" value="on">
				<input name="mainwp_rocket_post_exclude[<?php echo $field; ?>]" id="mainwp_rocket_post_exclude_<?php echo $field; ?>" type="checkbox"<?php echo $title; ?><?php echo $checked; ?><?php echo $disabled; ?>>
				<label for="mainwp_rocket_post_exclude_<?php echo $field; ?>"<?php echo $title; ?><?php echo $class; ?>><?php echo $label; ?></label><br>

				<?php
			}
			?>

			<p class="rkt-note"><?php _e( '<strong>Note:</strong> These options aren\'t applied if you added this post in the "Never cache the following pages" option.', 'mainwp-rocket-extension' ); ?></p>
		</div>

	<?php
	}
}

/*
 * Manage the cache options from the metabox.
 *
 * @since 2.5
 */

add_action( 'mainwp_save_bulkpost', '__mainwp_rocket_save_metabox_options' );
add_action( 'mainwp_save_bulkpage', '__mainwp_rocket_save_metabox_options' );
add_action( 'mainwp_publish_bulkpost', '__mainwp_rocket_save_metabox_options' );
add_action( 'mainwp_publish_bulkpage', '__mainwp_rocket_save_metabox_options' );
function __mainwp_rocket_save_metabox_options($post_id) {               
	if ( current_user_can( apply_filters( 'mainwp_rocket_capacity', 'manage_options' ) ) &&
		isset( $_POST['post_ID'], $_POST['mainwp_rocket_post_exclude_hidden'], $_POST['_rocketnonce'] ) ) {

		check_admin_referer( 'rocket_box_option', '_rocketnonce' );

        $fields = array(
			'lazyload',
			'lazyload_iframes',
			'minify_html',
			'minify_css',
			'minify_js',
			'cdn',
			'async_css',
			'defer_all_js',
		);
        
		foreach ( $fields as $field ) {
			if ( isset( $_POST['mainwp_rocket_post_exclude_hidden'][ $field ] ) && $_POST['mainwp_rocket_post_exclude_hidden'][ $field ] ) {
				if ( isset( $_POST['mainwp_rocket_post_exclude'][ $field ] ) ) {
					delete_post_meta( $_POST['post_ID'], '_rocket_exclude_' . $field );
				} else {
					//if ( mainwp_get_rocket_option( $field ) ) {
						update_post_meta( $_POST['post_ID'], '_rocket_exclude_' . $field, true );
					//}
				}
			}
		}
	}
}