<?php
defined( 'ABSPATH' ) or	die( 'Cheatin&#8217; uh?' );

/*
 * Tell WP what to do when admin is loaded aka upgrader
 *
 * @since 1.0
 */
add_action( 'admin_init', 'mainwp_rocket_upgrader' );
function mainwp_rocket_upgrader()
{
	// Grab some infos
	$actual_version = mainwp_get_rocket_option( 'version' );
	// You can hook the upgrader to trigger any action when WP Rocket is upgraded
	// first install
	if ( ! $actual_version ){
		do_action( 'mainwp_wp_rocket_first_install' );
	}	
}

/* BEGIN UPGRADER'S HOOKS */

/**
 * Keeps this function up to date at each version
 *
 * @since 1.0
 */
add_action( 'mainwp_wp_rocket_first_install', 'mainwp_rocket_first_install' );
function mainwp_rocket_first_install()
{	
        $default = mainwp_get_rocket_default_options();
	// Create Option
	add_option( MAINWP_ROCKET_GENERAL_SETTINGS, $default);
}

/* END UPGRADER'S HOOKS */