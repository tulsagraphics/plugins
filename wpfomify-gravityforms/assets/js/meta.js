;(function($) {

	var templateDesc = $('input[name="ibx_wpfomo_gf_template[]"]').parents('.mbt-field-control-wrapper').find('.description').text() + '<br>';

	$('#ibx_wpfomo_gf_form').on('change', function() {
		var formId = $(this).val();
		
		$.ajax({
			type: 'post',
			url: ajaxurl,
			data: {
				action: 'wpfomo_gforms_get_fields',
				form_id: formId
			},
			success: function( response ) {
				var data = JSON.parse( response );
				if ( data.error ) {
					alert( data.error );
					return;
				}

				var tags_obj = data.data.tags;
				var tags = Object.keys( data.data.tags );
				var tagsStr = '';
				var count = 1;

				tags.forEach(function( tag ) {
					tagsStr += '<span class="ibx-wpfomo-highlight">'+tag+'</span>' + ' : "' + tags_obj[tag] + '"';
					if ( tags.length !== count ) {
						tagsStr += ', ';
					}
					count++;
				});

				$('input[name="ibx_wpfomo_gf_template[]"]').parents('.mbt-field-control-wrapper').find('.description').html(templateDesc + tagsStr).css('font-style', 'normal');
				
				$('input#ibx_wpfomo_gf_template_0, input#ibx_wpfomo_gf_template_2').css('font-size', '12px');
				$('input#ibx_wpfomo_gf_template_1').css('font-weight', 'bold');

				$('#ibx_wpfomo_gf_field_email').html( data.data.options );

				$('#ibx_wpfomo_gf_field_email option[value="'+ibx_wpfomo_settings.gf_field_email+'"]').attr('selected', 'selected');
			}
		});
	});

	$('#ibx_wpfomo_gf_form').trigger('change');

})(jQuery);