<?php
/*
 * Plugin Name: WPfomify - Gravity Forms Add-on
 * Plugin URI: https://wpfomify.com
 * Version: 1.0.0
 * Description: Gravity Forms Add-on for WPfomify - The Social Proof Marketing plugin for WordPress
 * Author: IdeaBox Creations
 * Author URI: https://ideaboxcreations.com
 * Copyright: (c) 2017 IdeaBox Creations
 * License: GNU General Public License v2.0
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: ibx-wpfomo
 */

define( 'IBX_WPFOMO_GFORMS_VER', '1.0.0' );
define( 'IBX_WPFOMO_GFORMS_DIR', plugin_dir_path( __FILE__ ) );
define( 'IBX_WPFOMO_GFORMS_URL', plugins_url( '/', __FILE__ ) );
define( 'IBX_WPFOMO_GFORMS_PATH', plugin_basename( __FILE__ ) );

add_action( 'init', 'ibx_wpfomo_gforms_init' );
function ibx_wpfomo_gforms_init()
{
	if ( class_exists( 'IBX_WPFomo_Addon' ) ) {
		require_once IBX_WPFOMO_GFORMS_DIR . 'classes/class-ibx-wpfomo-gforms.php';
	}
}