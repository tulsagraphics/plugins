<?php

class IBX_WPFomo_GForms_AJAX {
	public function __construct()
	{
		add_action( 'wp_ajax_wpfomo_gforms_get_fields', array( $this, 'get_form_fields' ) );
	}

	public function get_form_fields()
	{
		$response = array(
			'error' => false,
			'data'	=> array(
				'options'	=> '',
				'tags'		=> array()
			),
		);

		if ( ! isset( $_POST['form_id'] ) ) {
			$response['error'] = __('Form ID is not provided.', 'ibx-wpfomo');
		}

		if ( ! $response['error'] ) {

			$fields = IBX_WPFomo_GForms_Helper::get_form_fields( $_POST['form_id'] );

			$response['data']['options'] = '<option value="">' . __('-- Select --', 'ibx-wpfomo') . '</option>';
				
			foreach ( $fields as $id => $label ) {
				$response['data']['options'] .= '<option value="' . $id . '">' . $label . '</option>';
				$response['data']['tags'][ '{{gf_field_' . $id . '}}' ] = $label;
			}
		}

		echo json_encode( $response );
		die();
	}
}
$ibx_wpfomo_gforms_ajax = new IBX_WPFomo_GForms_AJAX;