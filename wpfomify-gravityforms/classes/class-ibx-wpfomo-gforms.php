<?php

class IBX_WPFomo_GForms extends IBX_WPFomo_Addon {
	/**
     * Holds the class object.
     *
     * @since 1.0.0
     * @var object
     */
    public static $instance;

    /**
     * Primary class constructor
     *
     * @since 1.0.0
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'  => 'Gravity Forms',
            'slug'  => 'gravity-forms',
            'dir'   => IBX_WPFOMO_GFORMS_DIR,
            'url'   => IBX_WPFOMO_GFORMS_URL
        ));

        require_once $this->dir . 'classes/class-ibx-wpfomo-gforms-helper.php';
		require_once $this->dir . 'classes/class-ibx-wpfomo-gforms-ajax.php';

		add_action( 'admin_init', array( $this, 'plugin_updater' ), 0 );
		add_action( 'ibx_wpfomo_save_post', array( $this, 'clear_transients' ) );
	}
	
	/**
     * Plugin updater.
     *
     * @since 1.0.0
     */
    public function plugin_updater() {

        if ( ! class_exists( 'IBX_WPFomo_Plugin_Updater' ) ) {
            return;
        }

    	$license_key = trim( get_option( 'ibx_wpfomo_license_key' ) );

    	// setup the updater
    	$updater = new IBX_WPFomo_Plugin_Updater( IBX_WPFOMO_SL_URL, __FILE__, array(
                'version' 	=> IBX_WPFOMO_GFORMS_VER, // current version number
                'license' 	=> $license_key, // license key
                'item_id'   => 52906, // id of this addon
    			'item_name' => 'Gravity Forms Addon', 	// name of this addon
    			'author' 	=> 'IdeaBox Creations',  // author of this addon
    			'beta'		=> false
    		)
    	);
	}

	/**
     * Clear transient on post save.
     *
     * @since 1.0.0
     */
	public function clear_transients( $post_id )
	{
		if ( isset( $_POST['ibx_wpfomo_conversions_source'] ) && $this->slug == $_POST['ibx_wpfomo_conversions_source'] ) {
			delete_transient( 'ibx_wpfomo_gforms_data_' . $post_id );
		}
	}

	/**
     * Enqueue scripts in admin.
     *
     * @since 1.0.0
     */
	public function admin_enqueue_scripts()
	{
		wp_enqueue_script( 'ibx-wpfomo-gforms', $this->url . 'assets/js/meta.js', array('jquery'), IBX_WPFOMO_GFORMS_VER, true );
	}
	
	/**
     * Conversion fields.
     *
     * @since 1.0.0
     * @return array
     */
    public function fields()
    {
        $fields = array(
			'content'	=> array(
				'content_section'	=> array(
					'gf_form'    => array(
						'type'      => 'select',
						'label'     => __('Select form', 'ibx-wpfomo'),
						'default'   => '',
						'options'   => IBX_WPFomo_GForms_Helper::get_forms_list()
					),
					'gf_field_email'	=> array(
						'type'		=> 'select',
						'label'		=> __('Email field', 'ibx-wpfomo'),
						'default'	=> '',
						'options'	=> array()
					),
					'gf_template'  => array(
						'type'          => 'textarea',
						'label'         => __('Notification Template', 'ibx-wpfomo'),
						'rows'          => 3,
						'default'       => array(
							'0'				=> __('Someone submitted', 'ibx-wpfomo'),
							'1'				=> '{{title}}',
							'2'				=> '{{time}}'
						),
						'help'          => __('Variables: {{title}}, {{time}}', 'ibx-wpfomo'),
						'multiplerows'	=> true,
						'sanitize'		=> false
					),
					'gf_entries'    => array(
						'type'          => 'number',
						'label'         => __('Number of Entries', 'ibx-wpfomo'),
						'default'       => 5,
						'help'          => __('Notifications will be displayed from these entries.', 'ibx-wpfomo')
					),
					'gf_entries_time'	=> array(
						'type'				=> 'number',
						'label'				=> __('Display entries from last'),
						'default'			=> 2,
						'description'		=> __('Day(s)', 'ibx-wpfomo')
					),
					'gf_custom_url'    => array(
						'type'      => 'text',
						'label'     => __('Link to', 'ibx-wpfomo'),
						'default'   => '',
					)
				)
			),
		);

        return $fields;
	}

	/**
     * Add conversions.
     *
     * @since 1.0.0
     * @param array $data
     * @param object $settings
     * @return array
     */
    public function add_conversion_data( $data, $settings )
    {
		if ( $this->slug != $settings->conversions_source ) {
            return $data;
		}

		if ( empty( $settings->gf_form ) ) {
			return $data;
		}

		// transient key.
		$transient_key = 'ibx_wpfomo_gforms_data_' . $settings->post_id;

		// get data from transient and unserialize it.
		$cache_data = maybe_unserialize( get_transient( $transient_key ) );

		// return data if exist in transient.
		if ( is_array( $cache_data ) && ! empty( $cache_data ) ) {
			return $cache_data;
		}

		// get time settings for entries to be fetched.
		$entries_days 	= empty( $settings->gf_entries_time ) ? 2 : $settings->gf_entries_time;
		// convert time into seconds.
		$entries_time 	= $entries_days * 24 * 60 * 60;
		// get current time.
		$current_time 	= time();
		// get timezone offset.
		$gmt_offset		= get_option( 'gmt_offset' );
		// set start time.
		$start_time 	= date('Y-m-d', ($current_time - $entries_time) + ($gmt_offset * HOUR_IN_SECONDS * 2));
		// set end time.
		$end_time 		= date('Y-m-d H:i:s', $current_time + ($gmt_offset * HOUR_IN_SECONDS * 2));
		
		// search criteria for entries.
		$search_criteria = array(
			'start_date'	=> $start_time . ' 00:00:00',
			'end_date'		=> $end_time,
			'status'		=> 'active'
		);

		// get entries.
		$entries = IBX_WPFomo_GForms_Helper::get_form_entries( $settings->gf_form, $settings->gf_entries, $search_criteria );

		if ( ! is_array( $entries ) || empty( $entries ) ) {
			return $data;
		}

		// get form object.
		$gf_form = GFAPI::get_form( $settings->gf_form );
		// get for fields.
		$gf_fields = IBX_WPFomo_GForms_Helper::get_form_fields( $settings->gf_form );

		// get email field value from settings.
		$gf_field_email = $settings->gf_field_email;
		
		$name = __('Someone', 'ibx-wpfomo');

		foreach ( $entries as $entry ) {

			$email = '';

			if ( ! empty( $gf_field_email ) && isset( $entry[$gf_field_email] ) ) {
				$email = $entry[$gf_field_email];
			}

			// Data to render notification.
            $fields_data = array(
                'title'     => $gf_form['title'],
                'name'      => '',
                'email'     => $email,
                'time'      => ''
			);

			// Custom variables.
			foreach ( $gf_fields as $id => $label ) {
				$fields_data[ 'gf_field_' . $id ] = isset( $entry[ $id ] ) ? $entry[ $id ] : '';
			}
			
			// Time
			if ( isset( $entry['date_created'] ) && ! empty( $entry['date_created'] ) ) {
                $fields_data['time'] = IBX_WPFomo_Helper::get_timeago_html( $entry['date_created'] );
            }

            // Link
            $fields_data['url'] = esc_url( $settings->gf_custom_url );

            $data['fields'][] = $fields_data;
		}

		$data['template'] = $settings->gf_template;

		// store data in transient for the given time in options.
		$cache_duration = IBX_WPFomo_Admin::get_settings( 'cache_duration' );
		if ( ! $cache_duration || empty( $cache_duration ) ) {
			$cache_duration = 45;
		}
		set_transient( $transient_key, serialize($data), ( $cache_duration / 60 ) * HOUR_IN_SECONDS );

		return $data;
	}
	
	/**
	 * Returns the singleton instance of the class.
	 *
	 * @since 1.0.0
	 * @return object The IBX_WPFomo_GForms object.
	 */
	public static function get_instance()
	{
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof IBX_WPFomo_GForms ) ) {
			self::$instance = new IBX_WPFomo_GForms();
		}

		return self::$instance;
	}
}

$ibx_wpfomo_gforms = IBX_WPFomo_GForms::get_instance();