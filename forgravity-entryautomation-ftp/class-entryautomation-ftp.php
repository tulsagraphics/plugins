<?php

namespace ForGravity\EntryAutomation\Extensions;

use ForGravity\Entry_Automation;
use ForGravity\EntryAutomation\Extension;

use GFAddOn;
use phpseclib\Net\SFTP;

/**
 * Entry Automation FTP Add-On.
 *
 * @since     1.0
 * @author    ForGravity
 * @copyright Copyright (c) 2017, Travis Lopes
 */
class FTP extends Extension {

	/**
	 * Contains an instance of this class, if available.
	 *
	 * @since  1.0
	 * @access private
	 * @var    object $_instance If available, contains an instance of this class.
	 */
	private static $_instance = null;

	/**
	 * Defines the version of Entry Automation FTP Extension.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_version Contains the version, defined from entryautomation-ftp.php
	 */
	protected $_version = FG_ENTRYAUTOMATION_FTP_VERSION;

	/**
	 * Defines the plugin slug.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_slug The slug used for this plugin.
	 */
	protected $_slug = 'forgravity-entryautomation-ftp';

	/**
	 * Defines the main plugin file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_path The path to the main plugin file, relative to the plugins folder.
	 */
	protected $_path = 'forgravity-entryautomation-ftp/entryautomation-ftp.php';

	/**
	 * Defines the full path to this class file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_full_path The full path.
	 */
	protected $_full_path = __FILE__;

	/**
	 * Defines the title of this Extension.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_title The title of the Extension.
	 */
	protected $_title = 'Entry Automation FTP Extension';

	/**
	 * Defines the short title of the Extension.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_short_title The short title.
	 */
	protected $_short_title = 'FTP Extension';

	/**
	 * Get instance of this class.
	 *
	 * @since  1.0
	 * @access public
	 * @static
	 *
	 * @return FTP
	 */
	public static function get_instance() {

		if ( null === self::$_instance ) {
			self::$_instance = new self;
		}

		return self::$_instance;

	}

	/**
	 * Register needed pre-initialization hooks.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function pre_init() {

		parent::pre_init();

		require_once 'includes/autoload.php';

	}

	/**
	 * Register needed hooks.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function init() {

		parent::init();

		add_filter( 'fg_entryautomation_export_settings_fields', array( $this, 'settings_fields' ), 10, 1 );
		add_action( 'wp_ajax_fg_entryautomation_ftp_validate_credentials', array( $this, 'ajax_validate_credentials' ), 10 );
		add_action( 'wp_ajax_fg_entryautomation_ftp_validate_path', array( $this, 'ajax_validate_path' ), 10 );
		add_action( 'fg_entryautomation_after_export', array( $this, 'upload_file' ), 10, 3 );

	}

	/**
	 * Enqueue needed scripts.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   GFAddOn::get_slug()
	 *
	 * @return array
	 */
	public function scripts() {

		$scripts = array(
			array(
				'handle'  => 'forgravity_entryautomation_ftp_feed_settings',
				'src'     =>  $this->get_base_url() . '/js/feed_settings.js',
				'version' => '1.0',
				'enqueue' => array(
					array(
						'admin_page' => array( 'form_settings' ),
						'tab'        => fg_entryautomation()->get_slug(),
					),
				),
				'strings' => array(
					'nonce' => wp_create_nonce( $this->get_slug() ),
				),
			),
		);

		return array_merge( parent::scripts(), $scripts );

	}





	// # ACTION SETTINGS -----------------------------------------------------------------------------------------------

	/**
	 * Adds Export to FTP settings tab.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $fields Entry Automation settings fields.
	 *
	 * @uses   Entry_Automation::is_current_section()
	 * @uses   FTP::validate_credentials()
	 * @uses   FTP::validate_path()
	 *
	 * @return array
	 */
	public function settings_fields( $fields ) {

		// Add FTP section.
		$fields[] = array(
			'id'         => 'section-export-ftp',
			'class'      => 'entryautomation-feed-section ' . fg_entryautomation()->is_current_section( 'section-export-ftp', false, true ),
			'tab'        => array(
				'label' => esc_html__( 'FTP Settings', 'forgravity_entryautomation_ftp' ),
				'icon'  => ' fa-server',
			),
			'dependency' => array( 'field' => 'action', 'values' => array( 'export' ) ),
			'fields'     => array(
				array(
					'name'    => 'ftpEnable',
					'label'   => esc_html__( 'Upload Export File', 'forgravity_entryautomation_ftp' ),
					'type'    => 'checkbox',
					'onclick' => "jQuery( this ).parents( 'form' ).submit()",
					'choices' => array(
						array(
							'name'  => 'ftpEnable',
							'label' => esc_html__( 'Upload export file to FTP server', 'forgravity_entryautomation_ftp' ),
						),
					),
				),
				array(
					'name'          => 'ftpProtocol',
					'label'         => esc_html__( 'Protocol', 'forgravity_entryautomation_ftp' ),
					'type'          => 'radio',
					'required'      => true,
					'horizontal'    => true,
					'default_value' => 'ftp',
					'dependency'    => array( 'field' => 'ftpEnable', 'values' => array( '1' ) ),
					'tooltip'       => sprintf(
						'<h6>%s</h6>%s',
						esc_html__( 'FTP Protocol', 'forgravity_entryautomation_ftp' ),
						esc_html__( 'Select if export file should be uploaded via FTP or SFTP.', 'forgravity_entryautomation_ftp' )
					),
					'choices'       => array(
						array(
							'value' => 'ftp',
							'label' => esc_html__( 'FTP', 'forgravity_entryautomation_ftp' ),
						),
						array(
							'value' => 'sftp',
							'label' => esc_html__( 'SFTP', 'forgravity_entryautomation_ftp' ),
						),
					),
				),
				array(
					'name'              => 'ftpHost',
					'label'             => esc_html__( 'Host', 'forgravity_entryautomation_ftp' ),
					'type'              => 'text',
					'class'             => 'medium',
					'required'          => true,
					'dependency'        => array( 'field' => 'ftpEnable', 'values' => array( '1' ) ),
					'feedback_callback' => array( $this, 'validate_credentials' ),
					'tooltip'           => sprintf(
						'<h6>%s</h6>%s',
						esc_html__( 'FTP Host', 'forgravity_entryautomation_ftp' ),
						esc_html__( 'Enter the host for the (S)FTP server. To specify a port, separate host and port with a colon. (e.g. 127.0.0.1:21)', 'forgravity_entryautomation_ftp' )
					),
				),
				array(
					'name'              => 'ftpUsername',
					'label'             => esc_html__( 'Username', 'forgravity_entryautomation_ftp' ),
					'type'              => 'text',
					'class'             => 'medium',
					'required'          => true,
					'dependency'        => array( 'field' => 'ftpEnable', 'values' => array( '1' ) ),
					'feedback_callback' => array( $this, 'validate_credentials' ),
					'tooltip'           => sprintf(
						'<h6>%s</h6>%s',
						esc_html__( 'FTP Username', 'forgravity_entryautomation_ftp' ),
						esc_html__( 'Enter the username for the (S)FTP server.', 'forgravity_entryautomation_ftp' )
					),
				),
				array(
					'name'              => 'ftpPassword',
					'label'             => esc_html__( 'Password', 'forgravity_entryautomation_ftp' ),
					'type'              => 'text',
					'input_type'        => 'password',
					'class'             => 'medium',
					'required'          => false,
					'dependency'        => array( 'field' => 'ftpEnable', 'values' => array( '1' ) ),
					'feedback_callback' => array( $this, 'validate_credentials' ),
					'tooltip'           => sprintf(
						'<h6>%s</h6>%s',
						esc_html__( 'FTP Password', 'forgravity_entryautomation_ftp' ),
						esc_html__( 'Enter the password for the (S)FTP server.', 'forgravity_entryautomation_ftp' )
					),
				),
				array(
					'name'              => 'ftpPath',
					'label'             => esc_html__( 'Path', 'forgravity_entryautomation_ftp' ),
					'type'              => 'text',
					'class'             => 'medium',
					'required'          => true,
					'dependency'        => array( 'field' => 'ftpEnable', 'values' => array( '1' ) ),
					'default_value'     => '/',
					'feedback_callback' => array( $this, 'validate_path' ),
					'tooltip'           => sprintf(
						'<h6>%s</h6>%s',
						esc_html__( 'FTP Path', 'forgravity_entryautomation_ftp' ),
						esc_html__( 'Enter the path the export file will be uploaded to.', 'forgravity_entryautomation_ftp' )
					),
				),
			),
		);

		return $fields;

	}

	/**
	 * Validate FTP credentials.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   FTP::validate_ftp_credentials()
	 * @uses   FTP::validate_sftp_credentials()
	 * @uses   GFAddOn::get_setting()
	 *
	 * @return bool|null
	 */
	public function validate_credentials() {

		return 'sftp' === fg_entryautomation()->get_setting( 'ftpMode' ) ? $this->validate_sftp_credentials() : $this->validate_ftp_credentials();

	}

	/**
	 * Validate FTP credentials via AJAX
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   FTP::validate_ftp_credentials()
	 * @uses   FTP::validate_sftp_credentials()
	 * @uses   GFAddOn::get_slug()
	 */
	public function ajax_validate_credentials() {

		// Verify nonce.
		if ( ! wp_verify_nonce( rgpost( 'nonce' ), $this->get_slug() ) ) {
			wp_send_json_error();
		}

		// Determine if successful.
		$valid = 'sftp' === rgpost( 'ftpProtocol' ) ? $this->validate_sftp_credentials( true ) : $this->validate_ftp_credentials( true );

		if ( $valid ) {
			wp_send_json_success();
		} else {
			wp_send_json_error();
		}

	}

	/**
	 * Validate FTP path.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   FTP::validate_ftp_path()
	 * @uses   FTP::validate_sftp_path()
	 * @uses   GFAddOn::get_setting()
	 *
	 * @return bool|null
	 */
	public function validate_path() {

		return 'sftp' === fg_entryautomation()->get_setting( 'ftpMode' ) ? $this->validate_sftp_path() : $this->validate_ftp_path();

	}

	/**
	 * Validate FTP path via AJAX
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   FTP::validate_ftp_path()
	 * @uses   FTP::validate_sftp_path()
	 * @uses   GFAddOn::get_slug()
	 */
	public function ajax_validate_path() {

		// Verify nonce.
		if ( ! wp_verify_nonce( rgpost( 'nonce' ), $this->get_slug() ) ) {
			wp_send_json_error();
		}

		// Determine if successful.
		$valid = 'sftp' === rgpost( 'ftpProtocol' ) ? $this->validate_sftp_path( true ) : $this->validate_ftp_path( true );

		if ( $valid ) {
			wp_send_json_success();
		} else {
			wp_send_json_error();
		}

	}





	// # ON EXPORT -----------------------------------------------------------------------------------------------------

	/**
	 * Handle uploading file upon export.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array  $task      The current Task meta.
	 * @param array  $form      The current Form object.
	 * @param string $file_path Path to export file.
	 *
	 * @uses   FTP::upload_via_ftp()
	 * @uses   FTP::upload_via_sftp()
	 */
	public function upload_file( $task, $form, $file_path ) {

		// If task does not have FTP upload enabled, exit.
		if ( ! rgar( $task, 'ftpEnable' ) ) {
			return;
		}

		switch ( rgar( $task, 'ftpProtocol' ) ) {

			case 'ftp':
				$this->upload_via_ftp( $task, $form, $file_path );
				break;

			case 'sftp':
				$this->upload_via_sftp( $task, $form, $file_path );
				break;

		}

	}





	// # FTP METHODS ---------------------------------------------------------------------------------------------------

	/**
	 * Upload export file via FTP.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array  $task      The current Task settings.
	 * @param array  $form      The current Form object.
	 * @param string $file_path Path to export file.
	 *
	 * @uses   FTP::connect_to_ftp()
	 * @uses   GFAddOn::log_error()
	 */
	public function upload_via_ftp( $task, $form, $file_path ) {

		// Get host, port, username, password.
		$credentials = [
			'host'     => rgar( $task, 'ftpHost' ),
			'port'     => 21,
			'username' => rgar( $task, 'ftpUsername' ),
			'password' => rgar( $task, 'ftpPassword' ),
		];

		// If host has port defined, set it.
		if ( rgar( $credentials, 'host' ) && strpos( $credentials['host'], ':' ) !== false ) {
			list( $credentials['host'], $credentials['port'] ) = explode( ':', $credentials['host'] );
		}

		/**
		 * Modify the FTP credentials before connecting.
		 *
		 * @since 1.0
		 *
		 * @param array $credentials FTP credentials: host, port, username, password.
		 * @param array $task        Entry Automation Task meta.
		 * @param array $form        The Form object.
		 */
		$credentials = gf_apply_filters( [
			'fg_entryautomation_ftp_credentials',
			$form['id'],
			$task['task_id'],
		], $credentials, $task, $form );

		// If host or username are empty, return.
		if ( ! rgar( $credentials, 'host' ) || ! rgar( $credentials, 'username' ) ) {
			return;
		}

		// Connect to FTP.
		$ftp_connection = $this->connect_to_ftp( $credentials['host'], $credentials['port'], $credentials['username'], $credentials['password'] );

		// If unable to connect to FTP, return.
		if ( ! $ftp_connection ) {
			$this->log_error( __METHOD__ . '(): Unable to upload file as FTP connection could not be created.' );
			return;
		}

		// Get file name.
		$file_name = basename( $file_path );

		/**
		 * Modify the export file path before uploading.
		 *
		 * @since 1.0
		 *
		 * @param string $remote_file_path Destination file path.
		 * @param string $file_name        Export file name.
		 * @param array  $task             Entry Automation Task meta.
		 * @param array  $form             The Form object.
		 */
		$remote_file_path = gf_apply_filters( [
			'fg_entryautomation_ftp_credentials',
			$form['id'],
			$task['task_id'],
		], trailingslashit( rgar( $task, 'ftpPath' ) ) . $file_name, $file_name, $task, $form );

		// Upload file.
		$upload_response = ftp_nb_put( $ftp_connection, $remote_file_path, $file_path, FTP_ASCII );

		// Continue uploading file.
		while ( $upload_response == FTP_MOREDATA ) {
			$upload_response = ftp_nb_continue( $ftp_connection );
		}

		// If file could not be uploaded, log error.
		if ( $upload_response != FTP_FINISHED ) {
			$this->log_error( __METHOD__ . '(): Unable to upload export file to server "' . $credentials['host'] . ':' . $credentials['port']. '".' );
		}

		// Close connection.
		ftp_close( $ftp_connection );

	}

	/**
	 * Connect to FTP server.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param string $host     FTP host.
	 * @param int    $port     FTP port.
	 * @param string $username FTP username.
	 * @param string $password FTP password.
	 *
	 * @uses   GFAddOn::log_error()
	 *
	 * @return bool|resource
	 */
	public function connect_to_ftp( $host = '', $port = 21, $username, $password ) {

		// Connect to FTP server.
		$connection = ftp_connect( $host, $port );

		// If connection failed, return.
		if ( ! $connection ) {
			$this->log_error( __METHOD__ . '(): Unable to connect to server "' . $host . ':' . $port . '".' );
			return false;
		}

		// Login to FTP server.
		$logged_in = ftp_login( $connection, $username, $password );

		// If login failed, return.
		if ( ! $logged_in ) {
			$this->log_error( __METHOD__ . '(): Unable to connect to server "' . $host . ':' . $port . '" as "' . $username . '".' );
			return false;
		}

		ftp_pasv( $connection, true );

		return $connection;

	}

	/**
	 * Validate FTP credentials.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param bool $via_post Get credentials via $_POST.
	 *
	 * @uses   FTP::connect_to_ftp()
	 * @uses   GFAddOn::get_setting()
	 *
	 * @return bool|null
	 */
	public function validate_ftp_credentials( $via_post = false ) {

		// Get host, port, username, password.
		$host     = $via_post ? rgpost( 'ftpHost' ) : fg_entryautomation()->get_setting( 'ftpHost' );
		$port     = 21;
		$username = $via_post ? rgpost( 'ftpUsername' ) : fg_entryautomation()->get_setting( 'ftpUsername' );
		$password = $via_post ? rgpost( 'ftpPassword' ) : fg_entryautomation()->get_setting( 'ftpPassword' );

		// If host or username are empty, return.
		if ( rgblank( $host ) || rgblank( $username ) ) {
			return null;
		}

		// If host has port defined, set it.
		if ( strpos( $host, ':' ) !== false ) {
			list( $host, $port ) = explode( ':', $host );
		}

		return is_bool( $this->connect_to_ftp( $host, $port, $username, $password ) ) ? false : true;

	}

	/**
	 * Validate FTP path.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param bool $via_post Get credentials via $_POST.
	 *
	 * @uses   FTP::connect_to_ftp()
	 * @uses   GFAddOn::get_setting()
	 *
	 * @return bool|null
	 */
	public function validate_ftp_path( $via_post = false ) {

		// Get host, port, username, password, path.
		$host     = $via_post ? rgpost( 'ftpHost' ) : fg_entryautomation()->get_setting( 'ftpHost' );
		$port     = 21;
		$username = $via_post ? rgpost( 'ftpUsername' ) : fg_entryautomation()->get_setting( 'ftpUsername' );
		$password = $via_post ? rgpost( 'ftpPassword' ) : fg_entryautomation()->get_setting( 'ftpPassword' );
		$path     = $via_post ? rgpost( 'ftpPath' ) : fg_entryautomation()->get_setting( 'ftpPath' );

		// If host or username are empty, return.
		if ( rgblank( $host ) || rgblank( $username ) ) {
			return null;
		}

		// If host has port defined, set it.
		if ( strpos( $host, ':' ) !== false ) {
			list( $host, $port ) = explode( ':', $host );
		}

		// Get STP connection.
		$ftp_connection = $this->connect_to_ftp( $host, $port, $username, $password );

		// If could not connect to FTP, return.
		if ( is_bool( $ftp_connection ) ) {
			return false;
		}

		return ftp_chdir( $ftp_connection, $path );

	}





	// # SFTP METHODS---------------------------------------------------------------------------------------------------

	/**
	 * Upload export file to SFTP server.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array  $task      The current Task meta.
	 * @param array  $form      The current Form object.
	 * @param string $file_path Path to export file.
	 *
	 * @uses   FTP::connect_to_sftp()
	 * @uses   GFAddOn::log_error()
	 * @uses   \phpseclib\Net\SFTP::put()
	 */
	public function upload_via_sftp( $task, $form, $file_path ) {

		// Get host, port, username, password.
		$credentials = [
			'host'     => rgar( $task, 'ftpHost' ),
			'port'     => 21,
			'username' => rgar( $task, 'ftpUsername' ),
			'password' => rgar( $task, 'ftpPassword' ),
		];

		// If host has port defined, set it.
		if ( rgar( $credentials, 'host' ) && strpos( $credentials['host'], ':' ) !== false ) {
			list( $credentials['host'], $credentials['port'] ) = explode( ':', $credentials['host'] );
		}

		/**
		 * Modify the SFTP credentials before connecting.
		 *
		 * @since 1.0
		 *
		 * @param array $credentials SFTP credentials: host, port, username, password.
		 * @param array $task        Entry Automation Task meta.
		 * @param array $form        The Form object.
		 */
		$credentials = gf_apply_filters( [
			'fg_entryautomation_ftp_credentials',
			$form['id'],
			$task['task_id'],
		], $credentials, $task, $form );

		// If host or username are empty, return.
		if ( ! rgar( $credentials, 'host' ) || ! rgar( $credentials, 'username' ) ) {
			return;
		}

		// Connect to SFTP.
		$sftp_connection = $this->connect_to_sftp( $credentials['host'], $credentials['port'], $credentials['username'], $credentials['password'] );

		// If unable to connect to SFTP, return.
		if ( ! $sftp_connection ) {
			$this->log_error( __METHOD__ . '(): Unable to upload file as SFTP connection could not be created.' );
			return;
		}

		// Get file name.
		$file_name = basename( $file_path );

		/**
		 * Modify the export file path before uploading.
		 *
		 * @since 1.0
		 *
		 * @param string $remote_file_path Destination file path.
		 * @param string $file_name        Export file name.
		 * @param array  $task             Entry Automation Task meta.
		 * @param array  $form             The Form object.
		 */
		$remote_file_path = gf_apply_filters( [
			'fg_entryautomation_ftp_credentials',
			$form['id'],
			$task['task_id'],
		], trailingslashit( rgar( $task, 'ftpPath' ) ) . $file_name, $file_name, $task, $form );

		// Upload file.
		$file_uploaded = $sftp_connection->put( $remote_file_path, $file_path, SFTP::SOURCE_LOCAL_FILE );

		// If file could not be uploaded, log error.
		if ( ! $file_uploaded ) {
			$this->log_error( __METHOD__ . '(): Unable to upload export file to server "' . $credentials['host'] . ':' . $credentials['port']. '".' );
		}

		// Close connection.
		unset( $sftp_connection );

	}

	/**
	 * Connect to SFTP server.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param string $host     SFTP host.
	 * @param int    $port     SFTP port.
	 * @param string $username SFTP username.
	 * @param int    $password SFTP password.
	 *
	 * @uses   GFAddOn::log_error()
	 * @uses   \phpseclib\Net\SFTP::login()
	 *
	 * @return bool|SFTP
	 */
	public function connect_to_sftp( $host, $port = 22, $username, $password ) {

		// Initialize SFTP connect.
		$connection = new SFTP( $host, $port );

		// If connection failed, return.
		if ( ! $connection ) {
			$this->log_error( __METHOD__ . '(): Unable to connect to server "' . $host . ':' . $port . '".' );
			return false;
		}

		// Login to SFTP server.
		$logged_in = $connection->login( $username, $password );

		// If login failed, return.
		if ( ! $logged_in ) {
			$this->log_error( __METHOD__ . '(): Unable to connect to server "' . $host . ':' . $port . '" as "' . $username . '".' );
			return false;
		}

		return $connection;

	}

	/**
	 * Validate SFTP credentials.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param bool $via_post Get credentials via $_POST.
	 *
	 * @uses   FTP::connect_to_sftp()
	 * @uses   GFAddOn::get_setting()
	 *
	 * @return bool|null
	 */
	public function validate_sftp_credentials( $via_post = false ) {

		// Get host, port, username, password.
		$host     = $via_post ? rgpost( 'ftpHost' ) : fg_entryautomation()->get_setting( 'ftpHost' );
		$port     = 22;
		$username = $via_post ? rgpost( 'ftpUsername' ) : fg_entryautomation()->get_setting( 'ftpUsername' );
		$password = $via_post ? rgpost( 'ftpPassword' ) : fg_entryautomation()->get_setting( 'ftpPassword' );

		// If host or username are empty, return.
		if ( rgblank( $host ) || rgblank( $username ) ) {
			return null;
		}

		// If host has port defined, set it.
		if ( strpos( $host, ':' ) !== false ) {
			list( $host, $port ) = explode( ':', $host );
		}

		return is_bool( $this->connect_to_sftp( $host, $port, $username, $password ) ) ? false : true;

	}

	/**
	 * Validate SFTP path.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param bool $via_post Get credentials via $_POST.
	 *
	 * @uses   FTP::connect_to_sftp()
	 * @uses   GFAddOn::get_setting()
	 * @uses   \phpseclib\Net\SFTP::chdir()
	 *
	 * @return bool|null
	 */
	public function validate_sftp_path( $via_post = false ) {

		// Get host, port, username, password, path.
		$host     = $via_post ? rgpost( 'ftpHost' ) : fg_entryautomation()->get_setting( 'ftpHost' );
		$port     = 22;
		$username = $via_post ? rgpost( 'ftpUsername' ) : fg_entryautomation()->get_setting( 'ftpUsername' );
		$password = $via_post ? rgpost( 'ftpPassword' ) : fg_entryautomation()->get_setting( 'ftpPassword' );
		$path     = $via_post ? rgpost( 'ftpPath' ) : fg_entryautomation()->get_setting( 'ftpPath' );

		// If host or username are empty, return.
		if ( rgblank( $host ) || rgblank( $username ) ) {
			return null;
		}

		// If host has port defined, set it.
		if ( strpos( $host, ':' ) !== false ) {
			list( $host, $port ) = explode( ':', $host );
		}

		// Get SFTP connection.
		$sftp_connection = $this->connect_to_sftp( $host, $port, $username, $password );

		// If could not connect to SFTP, return.
		if ( is_bool( $sftp_connection ) ) {
			return false;
		}

		return $sftp_connection->chdir( $path );

	}

}
