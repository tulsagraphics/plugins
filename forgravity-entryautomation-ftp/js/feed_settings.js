var FGEntryAutomationFTP = function () {

	var self = this,
		$ = jQuery;

	self.init = function () {

		// Define strings.
		self.strings = forgravity_entryautomation_ftp_feed_settings_strings;

		// Define elements.
		self.$elem = {
			ftpProtocol: $( '#gaddon-setting-row-ftpProtocol' ),
			ftpHost:     $( '#gaddon-setting-row-ftpHost' ),
			ftpUsername: $( '#gaddon-setting-row-ftpUsername' ),
			ftpPassword: $( '#gaddon-setting-row-ftpPassword' ),
			ftpPath:     $( '#gaddon-setting-row-ftpPath' ),
		};

		// Define inputs.
		self.$input = {
			ftpHost:     $( '#ftpHost' ),
			ftpUsername: $( '#ftpUsername' ),
			ftpPassword: $( '#ftpPassword' ),
			ftpPath:     $( '#ftpPath' ),
		}

		// Define icon classes.
		self.iconClasses = {
			loading: 'fa fa-spinner fa-spin',
			success: 'fa icon-check fa-check gf_valid',
			error:   'fa icon-remove fa-times gf_invalid',
		};
		// Initialize icons.
		self.initIcons();

		// Bind validation events.
		self.bindValidationEvents();

	}





	// # VALIDATION ----------------------------------------------------------------------------------------------------

	/**
	 * Bind FTP validation event to input change.
	 *
	 * @since 1.0
	 */
	self.bindValidationEvents = function () {

		// Bind protocol inputs.
		self.$elem.ftpProtocol.find( 'input' ).on( 'click', self.validateCredentials );
		self.$elem.ftpProtocol.find( 'input' ).on( 'click', self.validatePath );

		// Loop through text inputs.
		for ( var input in self.$input ) {

			// Get input element.
			var $input = self.$input[ input ];

			// Bind event.
			if ( 'ftpPath' === input ) {
				$input.on( 'blur', self.validatePath );
			} else {
				$input.on( 'blur', self.validateCredentials );
				$input.on( 'blur', self.validatePath );
			}

		}

	}

	/**
	 * Validate FTP credentials when inputs change.
	 *
	 * @since 1.0
	 */
	self.validateCredentials = function () {

		// Add spinner class.
		for ( var elem in self.$elem ) {

			// Skip protocol element.
			if ( 'ftpProtocol' === elem ) {
				continue;
			}

			// Get element.
			var $elem = self.$elem[ elem ];

			// Add spinner icon.
			$elem.find( 'td i' ).removeClass().addClass( self.iconClasses.loading );

		}

		$.ajax(
			{
				url:      ajaxurl,
				type:     'POST',
				dataType: 'json',
				data:     {
					action:      'fg_entryautomation_ftp_validate_credentials',
					nonce:       self.strings.nonce,
					ftpProtocol: self.$elem.ftpProtocol.find( 'input:checked' ).val(),
					ftpHost:     self.$input.ftpHost.val(),
					ftpUsername: self.$input.ftpUsername.val(),
					ftpPassword: self.$input.ftpPassword.val(),
					ftpPath:     self.$input.ftpPath.val(),
				},
				success:  function ( response ) {

					// Add icon class.
					for ( var elem in self.$elem ) {

						// Skip protocol element.
						if ( 'ftpProtocol' === elem || 'ftpPath' === elem ) {
							continue;
						}

						// Get element.
						var $elem = self.$elem[ elem ];

						// Add icon class.
						$elem.find( 'td i' ).removeClass().addClass( response.success ? self.iconClasses.success : self.iconClasses.error );

					}

				}

			}
		);

	}

	/**
	 * Validate FTP path when inputs change.
	 *
	 * @since 1.0
	 */
	self.validatePath = function () {

		// Add spinner icon.
		self.$elem.ftpPath.find( 'td i' ).removeClass().addClass( self.iconClasses.loading );

		$.ajax(
			{
				url:      ajaxurl,
				type:     'POST',
				dataType: 'json',
				data:     {
					action:      'fg_entryautomation_ftp_validate_path',
					nonce:       self.strings.nonce,
					ftpProtocol: self.$elem.ftpProtocol.find( 'input:checked' ).val(),
					ftpHost:     self.$input.ftpHost.val(),
					ftpUsername: self.$input.ftpUsername.val(),
					ftpPassword: self.$input.ftpPassword.val(),
					ftpPath:     self.$input.ftpPath.val(),
				},
				success:  function ( response ) {

					// Add icon class.
					self.$elem.ftpPath.find( 'td i' ).removeClass().addClass( response.success ? self.iconClasses.success : self.iconClasses.error );

				}

			}
		);

	}





	// # ICONS ---------------------------------------------------------------------------------------------------------

	/**
	 * Initialize icons containers.
	 *
	 * @since 1.0
	 */
	self.initIcons = function () {

		// Loop through elements.
		for ( var elem in self.$elem ) {

			// Skip protocol element.
			if ( 'ftpProtocol' === elem ) {
				continue;
			}

			// Get element.
			var $elem = self.$elem[ elem ];

			// If icon container is found, skip.
			if ( $elem.find( 'td .fa' ).length > 0 ) {
				continue;
			}

			// Add icon container.
			$elem.find( 'td' ).append( ' <i class="fa"></i>' );

		}

	}

	self.init();

}

jQuery( document ).ready( function () {
    new FGEntryAutomationFTP();
} );
