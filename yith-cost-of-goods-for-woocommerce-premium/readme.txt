== YITH Cost of Goods for WooCommerce ==

Contributors: yithemes
Tags: cost of goods, woocommerce, products, themes, yit, e-commerce, shop, plugins
Requires at least: 3.0.0
Tested up to: 3.4
Stable tag: 1.1.1

Licence: GPLv2 or later
Licence URI: http://www.gnu.org/licences/gpl-2.0.html
Documentation: https://docs.yithemes.com/yith-cost-of-goods-for-woocommerce


== Changelog ==

= Version 1.1.1 - Released: Aug 20, 2018 =

* New: New option to let the admin decide the status of the orders to be displayed in the report
* New: Added a basic integration with Aelia Currency Switcher
* New: Added a new option to add a new column with the margin percentage in the report
* New: Added the order total Cost of Goods in the orders totals table
* Update: Italian language
* Update: Spanish language
* Update: Plugin framework 3.0.21
* Update: Updating plugin options
* Update: Updating the plugin data
* Fix: Change the product price caught from the order, now the product price have included the coupons
* Fix: Fixing the refunded items values
* Fix: Fixed an issue with the pagination
* Fix: Fixed a plugin string
* Fix: Fixing non numeric values
* Dev: Fixing a filter name
* Dev: Changing the language file names
* Dev: Added instance in the admin premium class
* Dev: Deleting the plugin name from the text domain


= Version 1.1 - Released: May 24, 2018 =

* New: added a new pagination options in the settings
* New: Added a new option to hide the currency symbol in the reports
* Tweak: Improving the technical language of the reports
* Update: Italian language
* Update: Spanish language
* Update: Dutch language
* Update: Plugin framework 3.0.15
* Fix: Fixing the stock
* Fix: Fixed the name of the variation on the stock report
* Fix: fixing minor issues.
* Fix: adding a round to the values without currency symbol
* Fix: Fixed the item per page select.
* Dev: Hiding the item meta in the orders


= Version 1.0.6 - Released: Mar 20, 2018 =
* Update: Updating .pot
* Fix: Changing some texts strings
* Fix: Fixed a problem with the JS
* Fix: Fixing the stock report with variations.
* Dev: Added a filter in the product column value


= Version 1.0.5 - Released: Feb 5, 2018 =
* New: Add a Export CSV link in the stock report
* New: Add a button to import the cost from WooCommerce Cost of Goods


= Version 1.0.4 - Released: Jan 30, 2018 =
* Fix: Fixing the Quick Edit cost value, now don't disappear when quick edit the product
* Fix: Fixing a problem with the price that the report takes, now takes the order price, with the discount if it have one


= Version 1.0.3 - Released: Jan 30, 2018 =
* New: Support to 3.3.0-rc.2
* Fix: Now the product name is from the order, the deleted products now are showed correctly in the report


= Version 1.0.2 - Released: Jan 28, 2018 =
* Fix: Fixing Ajax problem with the apply cost buttons
* New: Spanish translations
* New: Italian translations


= Version 1.0.1 - Released: Jan 22, 2018 =
* Fix - Fixing Ajax mixed content
* New: Dutch translations


= Version 1.0.0 - Released: Jan 11, 2018 =
* First release


== Suggestions ==
If you have suggestions about how to improve YITH Cost of Goods for WooCommerce Premium, you can [write us](mailto:plugins@yithemes.com "Your Inspiration Themes") so we can bundle them into the next release of the plugin.


== Translators ==
If you have created your own language pack, or have an update for an existing one, you can send [gettext PO and MO file](http://codex.wordpress.org/Translating_WordPress "Translating WordPress")
[use](http://yithemes.com/contact/ "Your Inspiration Themes") so we can bundle it into YITH Cost of Goods for WooCommerce Premium.

 = Available Languages =
 * English
 * Dutch
 * Spanish
 * Italian
