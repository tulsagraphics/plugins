<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

do_action( 'woocommerce_email_header', $email_heading, $email );

$title = wc_get_product($email->object['product_id'])->get_title();
$body  = ! empty( $email->email_body ) ? $email->email_body : '';


$gmt_offset = get_option( 'gmt_offset' );
if ( 0 <= $gmt_offset )
	$offset_name = '+' . $gmt_offset;
else
	$offset_name = (string)$gmt_offset;

$offset_name = str_replace( array( '.25', '.5', '.75' ), array( ':15', ':30', ':45' ), $offset_name );
$offset_name = 'UTC' . $offset_name;

echo '<p>';
$body = str_replace(
	array(
		'{customer_name}',
		'{product_title}',
		'{previous_sale_date}',
		'{new_sale_date}',
		'{offset_name}'
	),
	array(
		ucwords( $customer->display_name ),
		$product_link,
		$email->object['previous_sale_date'],
		$email->object['new_sale_date'],
		$offset_name
	),
	$body
);
echo $body;
echo '</p>';

do_action( 'woocommerce_email_footer' );