<?php

class MainWP_Branding {
	public static $instance = null;
	public static $default_opts = array();
	protected $option_handle = 'mainwp_child_branding_options';
	protected $option;

	public function __construct() {
		$this->option       = get_option( $this->option_handle );
		self::$default_opts = array(
			'textbox_id'    => array(
				'mainwp_branding_plugin_name'           => '',
				'mainwp_branding_plugin_desc'           => '',
				'mainwp_branding_plugin_uri'            => '',
				'mainwp_branding_plugin_author'         => '',
				'mainwp_branding_plugin_author_uri'     => '',
				'mainwp_branding_login_image_link'		=> '',
				'mainwp_branding_login_image_title'		=> '',
				'mainwp_branding_site_generator'        => '',
				'mainwp_branding_site_generator_link'   => '',
				'mainwp_branding_texts_add_value'       => '',
				'mainwp_branding_texts_add_replace'     => '',
				'mainwp_branding_button_contact_label'  => __( 'Contact Support', 'mainwp-branding-extension' ),
				'mainwp_branding_submit_button_title'   => __( 'Submit', 'mainwp-branding-extension' ),
				'mainwp_branding_send_email_message'    => __( 'Your Message was successfully submitted', 'mainwp-branding-extension' ),
				'mainwp_branding_message_return_sender' => __( 'Go back to previous page', 'mainwp-branding-extension' ),
				'mainwp_branding_support_email'         => '',
			),
			'textbox_class' => array(
				'mainwp_branding_texts_value' => '',
				//"mainwp_branding_texts_replace" => ""
			),
			'textareas'     => array(
				'mainwp_branding_admin_css' => '',
				'mainwp_branding_login_css' => '',

			),
			'tinyMCEs'      => array(
				'mainwp_branding_global_footer'    => '',
				'mainwp_branding_dashboard_footer' => '',
				'mainwp_branding_support_message'  => __( 'Welcome to Support', 'mainwp-branding-extension' ),
			),
			'checkboxes'    => array(
				'mainwp_branding_site_disable_wp_branding'        => false,
				'mainwp_branding_site_override'                   => false,
				'mainwp_branding_preserve_branding'               => false,
				'mainwp_branding_hide_child_plugin'               => false,
				'mainwp_branding_disable_change'                  => false,
				'mainwp_branding_disable_switching_theme'		      => false,
				'mainwp_branding_remove_restore_clone'            => false,
				'mainwp_branding_remove_permalink'                => false,
				'mainwp_branding_remove_mainwp_setting'           => false,
				'mainwp_branding_remove_mainwp_server_info'       => false,
				'mainwp_branding_remove_wp_tools'                 => false,
				'mainwp_branding_remove_wp_setting'               => false,
				'mainwp_branding_delete_login_image'              => false,
				'mainwp_branding_delete_favico_image'             => false,
				'mainwp_branding_remove_widget_welcome'           => false,
				'mainwp_branding_remove_widget_glance'            => false,
				'mainwp_branding_remove_widget_activity'          => false,
				'mainwp_branding_remove_widget_quick'             => false,
				'mainwp_branding_remove_widget_news'              => false,
				'mainwp_branding_hide_nag_update'                 => false,
				'mainwp_branding_hide_screen_options'             => false,
				'mainwp_branding_hide_help_box'                   => false,
				'mainwp_branding_hide_metabox_post_excerpt'       => false,
				'mainwp_branding_hide_metabox_post_slug'          => false,
				'mainwp_branding_hide_metabox_post_tags'          => false,
				'mainwp_branding_hide_metabox_post_author'        => false,
				'mainwp_branding_hide_metabox_post_comments'      => false,
				'mainwp_branding_hide_metabox_post_revisions'     => false,
				'mainwp_branding_hide_metabox_post_discussion'    => false,
				'mainwp_branding_hide_metabox_post_categories'    => false,
				'mainwp_branding_hide_metabox_post_custom_fields' => false,
				'mainwp_branding_hide_metabox_post_trackbacks'    => false,
				'mainwp_branding_hide_metabox_page_custom_fields' => false,
				'mainwp_branding_hide_metabox_page_author'        => false,
				'mainwp_branding_hide_metabox_page_discussion'    => false,
				'mainwp_branding_hide_metabox_page_revisions'     => false,
				'mainwp_branding_hide_metabox_page_attributes'    => false,
				'mainwp_branding_hide_metabox_page_slug'          => false,
				'mainwp_branding_show_support_button'             => false,
				'mainwp_branding_button_in_top_admin_bar'         => true,
				'mainwp_branding_button_in_admin_menu'            => true,
			),
		);
	}

	public static function render() {

		$website = null;
		$is_individual = false;
		if ( self::is_managesites_subpage() ) {
			if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
				global $mainWPBrandingExtensionActivator;
				$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPBrandingExtensionActivator->get_child_file(), $mainWPBrandingExtensionActivator->get_child_key(), array( $_GET['id'] ), array());
				if ( is_array( $dbwebsites ) && ! empty( $dbwebsites ) ) {
					$website = current( $dbwebsites );
				}
			}
			if ( empty( $website ) ) {
				do_action( 'mainwp-pageheader-sites', 'Branding' );
				echo '<div class="mainwp-notice mainwp-notice-red">' . __( 'Error: Site not found.', 'mainwp-branding-extension' ) . '</div>';
				do_action( 'mainwp-pagefooter-sites', 'Branding' );
				return;
			}
			$is_individual = true;
			do_action( 'mainwp-pageheader-sites', 'Branding' );
		}

        if (isset($_GET['updated'])) {
            ?>
			<div class="mainwp-notice mainwp-notice-green">
			<?php _e( 'Your settings have been saved.', 'mainwp-branding-extension' ); ?>
            </div>
            <?php
        }

        $general_update = get_option('mainwp_branding_need_to_general_update', false);
        if ($general_update) {
            delete_option('mainwp_branding_need_to_general_update');
            $result = self::prepare_save_settings();
            if ( 'NOCHILDSITE' !== $result ) {
                return; // it's ok, return for saving process.
            } else {
                ?><div class="mainwp-notice mainwp-notice-yellow"><?php _e( 'No child sites have been found.', 'mainwp-branding-extension' ); ?></div><?php
            }

        }

		?>
		<form method="post" enctype="multipart/form-data"
		      id="mainwp-branding-settings-page-form">
			<?php
			self::render_settings($website, $is_individual);
			?>
			<p class="submit">
				<input type="submit"
					   name="submit"
					   id="branding_submit_btn"
					   class="button-primary button button-hero"
				       value="<?php _e( 'Save Settings', 'mainwp-branding-extension' ); ?>"/>
				<input type="button"
					   class="button button-hero mwp_branding_reset_btn"
				       value="<?php _e( 'Reset Settings', 'mainwp-branding-extension' ); ?>"/>

			</p>
            <input type="hidden" name="branding_submit_nonce" value="<?php echo wp_create_nonce('branding_nonce'); ?>" />
		</form>
		<?php
		if ( $is_individual ) {
			do_action( 'mainwp-pagefooter-sites', 'Branding' );
		}
	}

	public static function is_managesites_subpage( $tabs = array() ) {
		if ( isset( $_GET['page'] ) && ( 'ManageSitesBranding' == $_GET['page'] || 'managesites' == $_GET['page'] ) ) {
			return true;
		}
		return false;
	}

	public static function prepare_save_settings() {
		global $mainWPBrandingExtensionActivator;
		$dbwebsites = apply_filters( 'mainwp-getsites', $mainWPBrandingExtensionActivator->get_child_file(), $mainWPBrandingExtensionActivator->get_child_key(), null );
		if ( is_array( $dbwebsites ) && count( $dbwebsites ) > 0 ) {
			echo '<h3>' . __( 'Saving Settings to child sites...', 'mainwp-branding-extension' ) . '</h3>';
			foreach ( $dbwebsites as $website ) {
				echo '<div><strong>' . stripslashes( $website['name'] ) . '</strong>: ';
				echo '<span class="mainwpBrandingSitesItem" siteid="' . $website['id'] . '" status="queue"><span class="status">' . __( 'Queue', 'mainwp-branding-extension' ) . '</span><br />';
				echo '<span class="detail"></span>';
				echo '</div><br />';
			}
			?>
			<div id="mainwp_branding_apply_setting_ajax_message_zone"
			     class="mainwp-notice mainwp-notice-yellow hidden"><?php _e( 'No child sites found.', 'mainwp-branding-extension' ); ?></div>
			<script>
				jQuery(document).ready(function ($) {
					mainwp_branding_start_next();
				})
			</script>
			<?php
			return true;
		} else {
			return 'NOCHILDSITE';
		}
	}

	public function init() {
		add_action( 'wp_ajax_mainwp_branding_performbrandingchildplugin', array( $this, 'ajax_save_settings') );
	}

	public function get_option( $key = null, $default = '' ) {
		if ( isset( $this->option[ $key ] ) ) {
			return $this->option[ $key ];
		}

		return $default;
	}

	public function set_option( $key, $value ) {
		$this->option[ $key ] = $value;

		return update_option( $this->option_handle, $this->option );
	}

	public static function on_load_page() {

        $is_individual = false;
        $websiteid = 0;

        if ( self::is_managesites_subpage() ) {
            $is_individual = true;
            $websiteid = $_GET['id'];
        }

		$pluginName          = $pluginDesc = $pluginAuthor = $pluginAuthorURI = $pluginURI = $supportEmail = $supportMessage = $loginImageLink = $loginImageTitle = '';
		$submitButtonTitle   = $returnMessage = $globalFooter = $dashboardFooter = '';
		$pluginHide          = $disableChanges = $showButton = $override = $removePermalinks = $preserve = 0;
		$site_branding       = null;
		$disableWPBranding   = $hideNag = $hideScreenOpts = $hideHelpBox = 0;
		$showButtonIn        = 1;
		$removeWidgetWelcome = $removeWidgetGlance = $removeWidgetAct = $removeWidgetQuick = $removeWidgetNews = 0;
		$siteGenerator       = $generatorLink = $adminCss = $loginCss = '';
		$textsReplace        = array();
		$hidePostExcerpt     = $hidePostSlug = $hidePostTags = $hidePostAuthor = $hidePostComments = $hidePostRevisions = $hidePostDiscussion = 0;
		$hidePostCategories  = $hidePostFields = $hidePostTrackbacks = 0;
		$hidePageFields      = $hidePageAuthor = $hidePageDiscussion = $hidePageRevisions = $hidePageAttributes = $hidePageSlug = 0;
		$removeRestore       = $removeSetting = $removeServerInfo = $removeWPTools = $removeWPSetting = $imageFavico = 0;

		if ( ! $is_individual ) {
			$pluginName          = self::get_instance()->get_option( 'child_plugin_name' );
			$pluginDesc          = self::get_instance()->get_option( 'child_plugin_desc' );
			$pluginAuthor        = self::get_instance()->get_option( 'child_plugin_author' );
			$pluginAuthorURI     = self::get_instance()->get_option( 'child_plugin_author_uri' );
			$pluginHide          = self::get_instance()->get_option( 'child_plugin_hide' );
			$disableChanges      = self::get_instance()->get_option( 'child_disable_change' );
			$disableSwitchingTheme = self::get_instance()->get_option( 'child_disable_switching_theme' );
			$showButton          = self::get_instance()->get_option( 'child_show_support_button' );
			$showButtonIn        = self::get_instance()->get_option( 'child_show_support_button_in' );
			$supportEmail        = self::get_instance()->get_option( 'child_support_email' );
			$supportMessage      = self::get_instance()->get_option( 'child_support_message' );
			$removeRestore       = self::get_instance()->get_option( 'child_remove_restore' );
			$removeSetting       = self::get_instance()->get_option( 'child_remove_setting' );
			$removeServerInfo    = self::get_instance()->get_option( 'child_remove_server_info' );
			$removeWPTools       = self::get_instance()->get_option( 'child_remove_wp_tools' );
			$removeWPSetting     = self::get_instance()->get_option( 'child_remove_wp_setting' );
			$pluginURI           = self::get_instance()->get_option( 'child_plugin_uri' );
			$buttonLabel         = self::get_instance()->get_option( 'child_button_contact_label' );
			$sendMessage         = self::get_instance()->get_option( 'child_send_email_message' );
			$submitButtonTitle   = self::get_instance()->get_option( 'child_submit_button_title' );
			$returnMessage       = self::get_instance()->get_option( 'child_message_return_sender' );
			$removePermalinks    = self::get_instance()->get_option( 'child_remove_permalink' );
			$globalFooter        = self::get_instance()->get_option( 'child_global_footer' );
			$dashboardFooter     = self::get_instance()->get_option( 'child_dashboard_footer' );
			$removeWidgetWelcome = self::get_instance()->get_option( 'child_remove_widget_welcome' );
			$removeWidgetGlance  = self::get_instance()->get_option( 'child_remove_widget_glance' );
			$removeWidgetAct     = self::get_instance()->get_option( 'child_remove_widget_activity' );
			$removeWidgetQuick   = self::get_instance()->get_option( 'child_remove_widget_quick' );
			$removeWidgetNews    = self::get_instance()->get_option( 'child_remove_widget_news' );
			$loginImageLink    = self::get_instance()->get_option( 'child_login_image_link' );
			$loginImageTitle    = self::get_instance()->get_option( 'child_login_image_title' );			
			$siteGenerator       = self::get_instance()->get_option( 'child_site_generator' );
			$generatorLink       = self::get_instance()->get_option( 'child_generator_link' );
			$adminCss            = self::get_instance()->get_option( 'child_admin_css' );
			$loginCss            = self::get_instance()->get_option( 'child_login_css' );
			$textsReplace        = self::get_instance()->get_option( 'child_texts_replace' );
			$imageLogin          = self::get_instance()->get_option( 'child_login_image' );
			$imageFavico         = self::get_instance()->get_option( 'child_favico_image' );
			$hideNag             = self::get_instance()->get_option( 'child_hide_nag', 0 );
			$hideScreenOpts      = self::get_instance()->get_option( 'child_hide_screen_opts', 0 );
			$hideHelpBox         = self::get_instance()->get_option( 'child_hide_help_box', 0 );

			$hidePostExcerpt  = self::get_instance()->get_option( 'child_hide_metabox_post_excerpt', 0 );
			$hidePostSlug     = self::get_instance()->get_option( 'child_hide_metabox_post_slug', 0 );
			$hidePostTags     = self::get_instance()->get_option( 'child_hide_metabox_post_tags', 0 );
			$hidePostAuthor   = self::get_instance()->get_option( 'child_hide_metabox_post_author', 0 );
			$hidePostComments = self::get_instance()->get_option( 'child_hide_metabox_post_comments', 0 );

			$hidePostRevisions  = self::get_instance()->get_option( 'child_hide_metabox_post_revisions', 0 );
			$hidePostDiscussion = self::get_instance()->get_option( 'child_hide_metabox_post_discussion', 0 );
			$hidePostCategories = self::get_instance()->get_option( 'child_hide_metabox_post_categories', 0 );
			$hidePostFields     = self::get_instance()->get_option( 'child_hide_metabox_post_custom_fields', 0 );
			$hidePostTrackbacks = self::get_instance()->get_option( 'child_hide_metabox_post_trackbacks', 0 );

			$hidePageFields     = self::get_instance()->get_option( 'child_hide_metabox_page_custom_fields', 0 );
			$hidePageAuthor     = self::get_instance()->get_option( 'child_hide_metabox_page_author', 0 );
			$hidePageDiscussion = self::get_instance()->get_option( 'child_hide_metabox_page_discussion', 0 );
			$hidePageRevisions  = self::get_instance()->get_option( 'child_hide_metabox_page_revisions', 0 );
			$hidePageAttributes = self::get_instance()->get_option( 'child_hide_metabox_page_attributes', 0 );
			$hidePageSlug       = self::get_instance()->get_option( 'child_hide_metabox_page_slug', 0 );

			$preserve = self::get_instance()->get_option( 'child_preserve_branding' );
		} else if ( $websiteid ) {
			$site_branding = MainWP_Branding_DB::get_instance()->get_branding_by( 'site_id', $websiteid );
		}

		if ( $is_individual && $site_branding ) {
			$header = unserialize( $site_branding->plugin_header );
			if ( is_array( $header ) ) {
				$pluginName      = $header['plugin_name'];
				$pluginDesc      = $header['plugin_desc'];
				$pluginAuthor    = $header['plugin_author'];
				$pluginAuthorURI = $header['author_uri'];
				$pluginURI       = $header['plugin_uri'];
			}
			$pluginHide       = $site_branding->hide_child_plugin;
			$disableChanges   = $site_branding->disable_theme_plugin_change;
			$showButton       = $site_branding->show_support_button;
			$supportEmail     = $site_branding->support_email;
			$supportMessage   = $site_branding->support_message;
			$removeRestore    = $site_branding->remove_restore;
			$removeSetting    = $site_branding->remove_setting;
			$removeServerInfo = $site_branding->remove_server_info;
			$removeWPTools    = $site_branding->remove_wp_tools;
			$removeWPSetting  = $site_branding->remove_wp_setting;
			$buttonLabel      = $site_branding->button_contact_label;
			$sendMessage      = $site_branding->send_email_message;
			$override         = $site_branding->override;
			$extra_settings   = unserialize( $site_branding->extra_settings );
			if ( is_array( $extra_settings ) ) {
				$submitButtonTitle   = isset( $extra_settings['submit_button_title'] ) ? $extra_settings['submit_button_title'] : '';
				$returnMessage       = isset( $extra_settings['message_return_sender'] ) ? $extra_settings['message_return_sender'] : '';
				$removePermalinks    = isset( $extra_settings['remove_permalink'] ) ? $extra_settings['remove_permalink'] : 0;
				$showButtonIn        = isset( $extra_settings['show_button_in'] ) ? $extra_settings['show_button_in'] : 1;
				$disableWPBranding   = isset( $extra_settings['disable_wp_branding'] ) ? $extra_settings['disable_wp_branding'] : 0;
				$globalFooter        = isset( $extra_settings['global_footer'] ) ? $extra_settings['global_footer'] : '';
				$dashboardFooter     = isset( $extra_settings['dashboard_footer'] ) ? $extra_settings['dashboard_footer'] : '';
				$removeWidgetWelcome = isset( $extra_settings['remove_widget_welcome'] ) ? $extra_settings['remove_widget_welcome'] : 0;
				$removeWidgetGlance  = isset( $extra_settings['remove_widget_glance'] ) ? $extra_settings['remove_widget_glance'] : 0;
				$removeWidgetAct     = isset( $extra_settings['remove_widget_activity'] ) ? $extra_settings['remove_widget_activity'] : 0;
				$removeWidgetQuick   = isset( $extra_settings['remove_widget_quick'] ) ? $extra_settings['remove_widget_quick'] : 0;
				$removeWidgetNews    = isset( $extra_settings['remove_widget_news'] ) ? $extra_settings['remove_widget_news'] : 0;
				$loginImageLink		= isset( $extra_settings['login_image_link'] ) ? $extra_settings['login_image_link'] : '';
				$loginImageTitle	= isset( $extra_settings['login_image_title'] ) ? $extra_settings['login_image_title'] : '';				
				$siteGenerator       = isset( $extra_settings['site_generator'] ) ? $extra_settings['site_generator'] : '';
				$generatorLink       = isset( $extra_settings['generator_link'] ) ? $extra_settings['generator_link'] : '';
				$adminCss            = isset( $extra_settings['admin_css'] ) ? $extra_settings['admin_css'] : '';
				$loginCss            = isset( $extra_settings['login_css'] ) ? $extra_settings['login_css'] : '';
				$textsReplace        = isset( $extra_settings['texts_replace'] ) ? $extra_settings['texts_replace'] : array();
				$imageLogin          = isset( $extra_settings['login_image'] ) ? $extra_settings['login_image'] : '';
				$imageFavico         = isset( $extra_settings['favico_image'] ) ? $extra_settings['favico_image'] : '';
				$hideNag             = isset( $extra_settings['hide_nag'] ) ? $extra_settings['hide_nag'] : 0;
				$hideScreenOpts      = isset( $extra_settings['hide_screen_opts'] ) ? $extra_settings['hide_screen_opts'] : 0;
				$hideHelpBox         = isset( $extra_settings['hide_help_box'] ) ? $extra_settings['hide_help_box'] : 0;

				$hidePostExcerpt  = isset( $extra_settings['hide_metabox_post_excerpt'] ) ? $extra_settings['hide_metabox_post_excerpt'] : 0;
				$hidePostSlug     = isset( $extra_settings['hide_metabox_post_slug'] ) ? $extra_settings['hide_metabox_post_slug'] : 0;
				$hidePostTags     = isset( $extra_settings['hide_metabox_post_tags'] ) ? $extra_settings['hide_metabox_post_tags'] : 0;
				$hidePostAuthor   = isset( $extra_settings['hide_metabox_post_author'] ) ? $extra_settings['hide_metabox_post_author'] : 0;
				$hidePostComments = isset( $extra_settings['hide_metabox_post_comments'] ) ? $extra_settings['hide_metabox_post_comments'] : 0;

				$hidePostRevisions  = isset( $extra_settings['hide_metabox_post_revisions'] ) ? $extra_settings['hide_metabox_post_revisions'] : 0;
				$hidePostDiscussion = isset( $extra_settings['hide_metabox_post_discussion'] ) ? $extra_settings['hide_metabox_post_discussion'] : 0;
				$hidePostCategories = isset( $extra_settings['hide_metabox_post_categories'] ) ? $extra_settings['hide_metabox_post_categories'] : 0;
				$hidePostFields     = isset( $extra_settings['hide_metabox_post_custom_fields'] ) ? $extra_settings['hide_metabox_post_custom_fields'] : 0;
				$hidePostTrackbacks = isset( $extra_settings['hide_metabox_post_trackbacks'] ) ? $extra_settings['hide_metabox_post_trackbacks'] : 0;

				$hidePageFields     = isset( $extra_settings['hide_metabox_page_custom_fields'] ) ? $extra_settings['hide_metabox_page_custom_fields'] : 0;
				$hidePageAuthor     = isset( $extra_settings['hide_metabox_page_author'] ) ? $extra_settings['hide_metabox_page_author'] : 0;
				$hidePageDiscussion = isset( $extra_settings['hide_metabox_page_discussion'] ) ? $extra_settings['hide_metabox_page_discussion'] : 0;
				$hidePageRevisions  = isset( $extra_settings['hide_metabox_page_revisions'] ) ? $extra_settings['hide_metabox_page_revisions'] : 0;
				$hidePageAttributes = isset( $extra_settings['hide_metabox_page_attributes'] ) ? $extra_settings['hide_metabox_page_attributes'] : 0;
				$hidePageSlug       = isset( $extra_settings['hide_metabox_page_slug'] ) ? $extra_settings['hide_metabox_page_slug'] : 0;
				$preserve           = $extra_settings['preserve_branding'];
				$disableSwitchingTheme = isset( $extra_settings['disable_switching_theme'] ) ? $extra_settings['disable_switching_theme'] : 0;
			}
		}

		if ( is_array( $textsReplace ) && count( $textsReplace ) > 0 ) {
			ksort( $textsReplace );
		} else {
			$textsReplace = array();
		}

		if ( empty( $buttonLabel ) ) {
			$buttonLabel = __( 'Contact Support', 'mainwp-branding-extension' );
		}

		if ( empty( $supportMessage ) ) {
			$supportMessage = __( 'Welcome to Support', 'mainwp-branding-extension' );
		}

		if ( empty( $submitButtonTitle ) ) {
			$submitButtonTitle = __( 'Submit', 'mainwp-branding-extension' );
		}

		if ( empty( $sendMessage ) ) {
			$sendMessage = __( 'Your message has been submitted successfully.', 'mainwp-branding-extension' );
		}

		if ( empty( $returnMessage ) ) {
			$returnMessage = __( 'Go back to the previous page', 'mainwp-branding-extension' );
		}

			do_action( 'mainwp_enqueue_meta_boxes_scripts' );
			$i = 1;

            $ext_settings = array(
                'disableWPBranding' => $disableWPBranding,
                'override' => $override,
                'preserve' => $preserve
            );
            add_meta_box(
                'mwp-branding-contentbox-' . $i++,
                '<i class="fa fa-cog"></i> ' . __('Branding extension settings', 'mainwp-branding-extension'),
                array( 'MainWP_Branding', 'renderBrandingExtensionSettings' ),
                array('mainwp_postboxes_branding_global_settings', 'mainwp_postboxes_branding_individual_settings' ),
                'normal',
                'core',
                array( 'isIndividual' => $is_individual, 'settings' => $ext_settings )
            );

            $bran_settings = array(
                'pluginName' => $pluginName,
                'pluginDesc' => $pluginDesc,
                'pluginURI' => $pluginURI,
                'pluginAuthor' => $pluginAuthor,
                'pluginAuthorURI' => $pluginAuthorURI,
                'pluginHide' => $pluginHide,
            );
			add_meta_box(
				'mwp-branding-contentbox-' . $i++,
				'<i class="fa fa-cog"></i> ' . __('Branding options', 'mainwp-branding-extension'),
				array( 'MainWP_Branding', 'renderBrandingOptions' ),
				array( 'mainwp_postboxes_branding_global_settings', 'mainwp_postboxes_branding_individual_settings' ),
				'normal',
				'core',
                array( 'settings' => $bran_settings )
			);

            $rem_settings = array(
                'disableSwitchingTheme' => $disableSwitchingTheme,
                'disableChanges' => $disableChanges,
                'removeSetting' => $removeSetting,
                'removeServerInfo' => $removeServerInfo,
                'removeRestore' => $removeRestore,
                'removeWPTools' => $removeWPTools,
                'removeWPSetting' => $removeWPSetting,
                'removePermalinks' => $removePermalinks,
            );

			add_meta_box(
				'mwp-branding-contentbox-' . $i++,
				'<i class="fa fa-cog"></i> ' . __('Remove & Disable functions', 'mainwp-branding-extension'),
				array( 'MainWP_Branding', 'renderRemoveDisable' ),
				array( 'mainwp_postboxes_branding_global_settings', 'mainwp_postboxes_branding_individual_settings' ),
				'normal',
				'core',
                array( 'settings' => $rem_settings )
			);

            $wpbra_settings = array(
                'imageLogin' => $imageLogin,
                'imageFavico' => $imageFavico,
				'loginImageLink' => $loginImageLink,	
				'loginImageTitle' => $loginImageTitle,					
                'removeWidgetWelcome' => $removeWidgetWelcome,
                'removeWidgetGlance' => $removeWidgetGlance,
                'removeWidgetAct' => $removeWidgetAct,
                'removeWidgetQuick' => $removeWidgetQuick,
                'removeWidgetNews' => $removeWidgetNews,
                'hideNag' => $hideNag,
                'hideScreenOpts' => $hideScreenOpts,
                'hideHelpBox' => $hideHelpBox,
                'hidePostExcerpt' => $hidePostExcerpt,
                'hidePostSlug' => $hidePostSlug,
                'hidePostTags' => $hidePostSlug,
                'hidePostAuthor' => $hidePostAuthor,
                'hidePostComments' => $hidePostComments,
                'hidePostRevisions' => $hidePostRevisions,
                'hidePostDiscussion' => $hidePostDiscussion,
                'hidePostCategories' => $hidePostCategories,
                'hidePostFields' => $hidePostFields,
                'hidePostTrackbacks' => $hidePostTrackbacks,
                'hidePageFields' => $hidePageFields,
                'hidePageAuthor' => $hidePageAuthor,
                'hidePageDiscussion' => $hidePageDiscussion,
                'hidePageRevisions' => $hidePageRevisions,
                'hidePageAttributes' => $hidePageAttributes,
                'hidePageSlug' => $hidePageSlug,
                'globalFooter' => $globalFooter,
                'dashboardFooter' => $dashboardFooter,
                'siteGenerator' => $siteGenerator,
                'generatorLink' => $generatorLink,
                'adminCss' => $adminCss,
                'loginCss' => $loginCss,
                'textsReplace' => $textsReplace
            );

			add_meta_box(
				'mwp-branding-contentbox-' . $i++,
				'<i class="fa fa-cog"></i> ' . __('WordPress branding options', 'mainwp-branding-extension'),
				array( 'MainWP_Branding', 'renderWordPressBranding' ),
				array( 'mainwp_postboxes_branding_global_settings', 'mainwp_postboxes_branding_individual_settings' ),
				'normal',
				'core',
                array( 'settings' => $wpbra_settings )
			);

            $sup_settings = array(
                'showButton' => $showButton,
                'showButtonIn' => $showButtonIn,
                'buttonLabel' => $buttonLabel,
                'supportMessage' => $supportMessage,
                'submitButtonTitle' => $submitButtonTitle,
                'sendMessage' => $sendMessage,
                'returnMessage' => $returnMessage,
                'supportEmail' => $supportEmail
            );

			add_meta_box(
				'mwp-branding-contentbox-' . $i++,
				'<i class="fa fa-cog"></i> ' . __('Support options', 'mainwp-branding-extension'),
				array( 'MainWP_Branding', 'renderSupportOptions' ),
				array( 'mainwp_postboxes_branding_global_settings', 'mainwp_postboxes_branding_individual_settings' ),
				'normal',
				'core',
                array( 'settings' => $sup_settings )
			);
    }

	public static function render_settings( $website = null, $is_individual = false ) {
        ?>
		<script type="text/javascript">
			var mainwpBrandingDefaultOpts = <?php echo json_encode( self::$default_opts ); ?>;
		</script>
		<?php
        if ( $is_individual ) {

            if ( get_option( 'mainwp_branding_need_to_update_site', false ) ) {
                delete_option( 'mainwp_branding_need_to_update_site' );
                ?>
                <script>
                    jQuery(document).ready(function ($) {
                        mainwp_branding_update_specical_site();
                    })
                </script>
                <?php
            }

			?>
			<input type="hidden" name="branding_individual_settings_site_id" id="branding_individual_settings_site_id" value="<?php echo $website->id; ?>"  />
            <?php do_action( 'mainwp_do_meta_boxes', 'mainwp_postboxes_branding_individual_settings' ); ?>
            <?php
        } else {
            do_action( 'mainwp_do_meta_boxes', 'mainwp_postboxes_branding_global_settings');
        }
	}

	static function renderBrandingExtensionSettings($post, $metabox) {
            $is_individual = isset($metabox['args']['isIndividual']) ? $metabox['args']['isIndividual'] : false;

            $settings  = $metabox['args']['settings'];
            $disableWPBranding = $settings['disableWPBranding'];
            $override = $settings['override'];
            $preserve = $settings['preserve'];

            ?>
			<div id="mainwp_branding_edit_site_ajax_message_zone" class="mainwp-notice mainwp-notice-yellow hidden"></div>
			<table class="form-table">
				<tbody>
            <?php if ($is_individual) { ?>
				<tr>
					<th scope="row"><?php _e( 'Disable Wordpress Branding', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Set to YES if you want to disable WordPress branding on your child site.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox"
								   id="mainwp_branding_site_disable_wp_branding"
							       name="mainwp_branding_site_disable_wp_branding"
							       <?php echo( 0 == $disableWPBranding ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_site_disable_wp_branding"></label>
						</div>
					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e( 'Override general settings', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Set to YES if you want to overwrite global branding settings.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox"
								   id="mainwp_branding_site_override"
							       name="mainwp_branding_site_override"
							       <?php echo( 0 == $override ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_site_override"></label>
						</div>
					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e( 'Reset branding options', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Click the Reset Settings button to return branding settings to default. After resetting click the Save Settings button at the bottom of the page.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<input type="button"
							   class="button-primary mwp_branding_reset_btn"
						       value="<?php _e( 'Reset Settings', 'mainwp' ); ?>"/>
					</td>
				</tr>
            <?php } ?>
				<tr>
					<th scope="row"><?php _e( 'Keep branding if child site is disconnected', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Set to YES if you want to preserve branding in case your child site gets disconnected.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox"
								   id="mainwp_branding_preserve_branding"
							       name="mainwp_branding_preserve_branding"
							       <?php echo( 0 == $preserve ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_preserve_branding"></label>
						</div>
						<br/><br/><em><?php  _e( 'If set to "Yes", in case your child site gets disconnected, your custom branding will stay preserved. If set to "No", in case your child site gets disconnected, custom branding will be disabled and the child site will be returned to original state.','mainwp-branding-extension' ); ?></em>
					</td>
				</tr>
				</tbody>
			</table>

		<?php
	}

	static function renderBrandingOptions($post, $metabox) {

        $settings  = $metabox['args']['settings'];
        $pluginName = $settings['pluginName'];
        $pluginDesc = $settings['pluginDesc'];
        $pluginURI = $settings['pluginURI'];
        $pluginAuthor = $settings['pluginAuthor'];
        $pluginAuthorURI = $settings['pluginAuthorURI'];
        $pluginHide = $settings['pluginHide'];

		?>

			<table class="form-table">
				<tbody>
				<tr>
					<th scope="row"><?php _e( 'Plugin name', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Enter a new plugin name for the MainWP Child plugin.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<input type="text"
							   class="regular-text"
							   name="mainwp_branding_plugin_name"
							   id="mainwp_branding_plugin_name"
						       value="<?php echo esc_attr( stripslashes( $pluginName ) ); ?>"/>
					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e( 'Plugin description', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Enter a new plugin description for the MainWP Child plugin.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<input type="text"
							   class="regular-text"
							   name="mainwp_branding_plugin_desc"
							   id="mainwp_branding_plugin_desc"
						       value="<?php echo esc_attr( stripslashes( $pluginDesc ) ); ?>"/>
					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e( 'Plugin URI', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Enter a new plugin URI for the MainWP Child plugin.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<input type="text"
							   class="regular-text"
							   name="mainwp_branding_plugin_uri"
							   id="mainwp_branding_plugin_uri"
						       value="<?php echo esc_attr( stripslashes( $pluginURI ) ); ?>"/>
					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e( 'Plugin author', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Enter a new plugin author for the MainWP Child plugin.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<input type="text"
							   class="regular-text"
							   name="mainwp_branding_plugin_author"
							   id="mainwp_branding_plugin_author"
						       value="<?php echo esc_attr( stripslashes( $pluginAuthor ) ); ?>"/>
					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e( 'Author URL', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Enter a new author URL for the MainWP Child plugin.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<input type="text"
							   class="regular-text"
							   name="mainwp_branding_plugin_author_uri"
						       id="mainwp_branding_plugin_author_uri"
						       value="<?php echo esc_attr( stripslashes( $pluginAuthorURI ) ); ?>"/>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Visually hide the MainWP Child plugin', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Set to YES if you want to visually hide the MainWP Child plugin from the plugins list.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox"
								   id="mainwp_branding_hide_child_plugin"
							       name="mainwp_branding_hide_child_plugin"
							       <?php echo( 0 == $pluginHide ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_hide_child_plugin"></label>
						</div>
					</td>
				</tr>
				</tbody>
			</table>

		<?php
	}

	static function renderRemoveDisable($post, $metabox) {

        $settings  = $metabox['args']['settings'];
        $disableSwitchingTheme = $settings['disableSwitchingTheme'];
        $disableChanges = $settings['disableChanges'];
        $removeSetting = $settings['removeSetting'];
        $removeServerInfo = $settings['removeServerInfo'];
        $removeRestore = $settings['removeRestore'];
        $removeWPTools = $settings['removeWPTools'];
        $removeWPSetting = $settings['removeWPSetting'];
        $removePermalinks = $settings['removePermalinks'];

		?>

			<table class="form-table">
				<tbody>
				<tr>
					<th scope="row">
						<?php _e( 'Disable Theme Switching', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Disable switching theme for your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox" id="mainwp_branding_disable_switching_theme"
							       name="mainwp_branding_disable_switching_theme" <?php echo( 0 == $disableSwitchingTheme ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_disable_switching_theme"></label>
						</div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Disable Theme/Plugin Changes', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Disable all plugin and theme changes for your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox" id="mainwp_branding_disable_change"
							       name="mainwp_branding_disable_change" <?php echo( 0 == $disableChanges ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_disable_change"></label>
						</div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Remove MainWP Settings', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Remove the MainWP Settings menu from your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox" id="mainwp_branding_remove_mainwp_setting"
							       name="mainwp_branding_remove_mainwp_setting" <?php echo( 0 == $removeSetting ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_remove_mainwp_setting"></label>
						</div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Remove MainWP Server Information', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Remove the MainWP Server Information menu from your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox" id="mainwp_branding_remove_mainwp_server_info"
							       name="mainwp_branding_remove_mainwp_server_info" <?php echo( 0 == $removeServerInfo ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_remove_mainwp_server_info"></label>
						</div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Remove MainWP Restore (Clone)', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Remove the MaiNWP Restore (Clone) menu from your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox" id="mainwp_branding_remove_restore_clone"
							       name="mainwp_branding_remove_restore_clone" <?php echo( 0 == $removeRestore ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_remove_restore_clone"></label>
						</div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Remove WP Tools', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Remove the WP Tools menu from your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox" id="mainwp_branding_remove_wp_tools"
							       name="mainwp_branding_remove_wp_tools" <?php echo( 0 == $removeWPTools ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_remove_wp_tools"></label>
						</div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Remove WP Settings', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Remove the WP Settings menu from your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox" id="mainwp_branding_remove_wp_setting"
							       name="mainwp_branding_remove_wp_setting" <?php echo( 0 == $removeWPSetting ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_remove_wp_setting"></label>
						</div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Remove Permalinks Menu', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Remove the Permalinks Menu from your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox" id="mainwp_branding_remove_permalink"
							       name="mainwp_branding_remove_permalink" <?php echo( 0 == $removePermalinks ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_remove_permalink"></label>
						</div>
					</td>
				</tr>
				</tbody>
			</table>

		<?php
	}

	static function renderWordPressBranding($post, $metabox) {

        $settings  = $metabox['args']['settings'];

        $imageLogin = $settings['imageLogin'];
        $imageFavico = $settings['imageFavico'];
		$loginImageLink = $settings['loginImageLink'];
		$loginImageTitle = $settings['loginImageTitle'];		
        $removeWidgetWelcome = $settings['removeWidgetWelcome'];
        $removeWidgetGlance = $settings['removeWidgetGlance'];
        $removeWidgetAct = $settings['removeWidgetAct'];
        $removeWidgetQuick = $settings['removeWidgetQuick'];
        $removeWidgetNews = $settings['removeWidgetNews'];
        $hideNag = $settings['hideNag'];
        $hideScreenOpts = $settings['hideScreenOpts'];
        $hideHelpBox = $settings['hideHelpBox'];
        $hidePostExcerpt = $settings['hidePostExcerpt'];
        $hidePostSlug = $settings['hidePostSlug'];
        $hidePostTags = $settings['hidePostTags'];
        $hidePostAuthor = $settings['hidePostAuthor'];
        $hidePostComments = $settings['hidePostComments'];
        $hidePostRevisions = $settings['hidePostRevisions'];
        $hidePostDiscussion = $settings['hidePostDiscussion'];
        $hidePostCategories = $settings['hidePostCategories'];
        $hidePostFields = $settings['hidePostFields'];
        $hidePostTrackbacks = $settings['hidePostTrackbacks'];
        $hidePageFields = $settings['hidePageFields'];
        $hidePageAuthor = $settings['hidePageAuthor'];
        $hidePageDiscussion = $settings['hidePageDiscussion'];
        $hidePageRevisions = $settings['hidePageRevisions'];
        $hidePageAttributes = $settings['hidePageAttributes'];
        $hidePageSlug = $settings['hidePageSlug'];
        $globalFooter = $settings['globalFooter'];
        $dashboardFooter = $settings['dashboardFooter'];
        $siteGenerator = $settings['siteGenerator'];
        $generatorLink = $settings['generatorLink'];
        $adminCss = $settings['adminCss'];
        $loginCss = $settings['loginCss'];
        $textsReplace = $settings['textsReplace'];



		?>

			<table class="form-table">
				<tbody>
				<tr>
					<th scope="row">
						<?php _e( 'Custom Login Image', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Upload a custom Login Image (logo) for your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<?php
						if ( ! empty( $imageLogin ) ) {
							$imageurl = $imageLogin;
							?>
							<p><img class="brd_login_img" src="<?php echo esc_attr( $imageurl ); ?>"/></p>
							<p>
								<input type="checkbox" class="mainwp-checkbox2" value="1"
								       id="mainwp_branding_delete_login_image"
								       name="mainwp_branding_delete_login_image">
								<label class="mainwp-label2"
								       for="mainwp_branding_delete_login_image"><?php _e( 'Delete Image', 'mainwp' ); ?></label>
							</p><br/>
							<?php
						}
						?>
						<input type="file" name="mainwp_branding_login_image_file"
						       id="mainwp_branding_login_image_file" accept="image/*"/><br/>
						<span
							class="description"><?php _e( "Image must be 500KB maximum. It will be cropped to 310px wide and 70px tall.<br/>For best results  us an image of this site. Allowed formats: jpeg, gif and png.<br /> Note that animated gifs aren't going to be preserved." ) ?></span>

					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Custom Login Image Link', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Custom Login Image Link', 'mainwp-branding-extension' ) ); ?></th>
					<td>						
						<input type="text" name="mainwp_branding_login_image_link"
							   id="mainwp_branding_login_image_link"
							   value="<?php echo esc_attr( stripslashes( $loginImageLink ) ); ?>"/>											
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Custom Login Image Title', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Custom Login Image Title', 'mainwp-branding-extension' ) ); ?></th>
					<td>						
						<input type="text" name="mainwp_branding_login_image_title"
							   id="mainwp_branding_login_image_title"
							   value="<?php echo esc_attr( stripslashes( $loginImageTitle ) ); ?>"/>											
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Custom Favico', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Upload a custom Favico (icon) for your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<?php
						if ( $imageFavico ) {
							$imageurl = $imageFavico;
							?>
							<p><img class="brd_favico_img" src="<?php echo esc_attr( $imageurl ); ?>"/></p>
							<p>
								<input type="checkbox" class="mainwp-checkbox2" value="1"
								       id="mainwp_branding_delete_favico_image"
								       name="mainwp_branding_delete_favico_image">
								<label class="mainwp-label2"
								       for="mainwp_branding_delete_favico_image"><?php _e( 'Delete Image', 'mainwp' ); ?></label>
							</p><br/>
							<?php
						}
						?>
						<input type="file" name="mainwp_branding_favico_file"
						       id="mainwp_branding_favico_file" accept="image/*"/><br/>
						<span
							class="description"><?php _e( "Image must be 500KB maximum. It will be cropped to 16px wide and 16px tall.<br/>For best results  us an image of this site. Allowed formats: jpeg, gif and png.<br /> Note that animated gifs aren't going to be preserved." ) ?></span>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Remove Dashboard Widgets', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Hide Dashboard Widgets from your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<ul class="mainwp_checkboxes mainwp_branding">
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $removeWidgetWelcome ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_remove_widget_welcome"
								       name="mainwp_branding_remove_widget_welcome">
								<label class="mainwp-label2"
								       for="mainwp_branding_remove_widget_welcome"><?php _e( 'Welcome', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $removeWidgetGlance ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_remove_widget_glance"
								       name="mainwp_branding_remove_widget_glance">
								<label class="mainwp-label2"
								       for="mainwp_branding_remove_widget_glance"><?php _e( 'At a Glance', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $removeWidgetAct ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_remove_widget_activity"
								       name="mainwp_branding_remove_widget_activity">
								<label class="mainwp-label2"
								       for="mainwp_branding_remove_widget_activity"><?php _e( 'Activity', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $removeWidgetQuick ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_remove_widget_quick"
								       name="mainwp_branding_remove_widget_quick">
								<label class="mainwp-label2"
								       for="mainwp_branding_remove_widget_quick"><?php _e( 'Quick Draft', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $removeWidgetNews ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_remove_widget_news"
								       name="mainwp_branding_remove_widget_news">
								<label class="mainwp-label2"
								       for="mainwp_branding_remove_widget_news"><?php _e( 'Wordpress News', 'mainwp' ); ?></label>
							</li>
						</ul>
					</td>
				</tr>

				<tr>
					<th scope="row">
						<?php _e( 'Hide Nag Updates', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Hide the Nag Update for out of date versions of WordPress on your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox" id="mainwp_branding_hide_nag_update"
							       name="mainwp_branding_hide_nag_update" <?php echo( 0 == $hideNag ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_hide_nag_update"></label>
						</div>
					</td>
				</tr>

				<tr>
					<th scope="row">
						<?php _e( 'Hide Screen Options', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Hide the Screen Options dropdown on your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox" id="mainwp_branding_hide_screen_options"
							       name="mainwp_branding_hide_screen_options" <?php echo( 0 == $hideScreenOpts ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_hide_screen_options"></label>
						</div>
					</td>
				</tr>

				<tr>
					<th scope="row">
						<?php _e( 'Hide Help Box', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Hide the Help Box dropdown on your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox" id="mainwp_branding_hide_help_box"
							       name="mainwp_branding_hide_help_box" <?php echo( 0 == $hideHelpBox ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_hide_help_box"></label>
						</div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Hide Post Meta Boxes', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Hide meta boxes from the Edit Post panel on your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<ul class="mainwp_checkboxes mainwp_branding">
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePostExcerpt ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_post_excerpt"
								       name="mainwp_branding_hide_metabox_post_excerpt">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_post_excerpt"><?php _e( 'Excerpt', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePostSlug ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_post_slug"
								       name="mainwp_branding_hide_metabox_post_slug">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_post_slug"><?php _e( 'Slug', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePostTags ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_post_tags"
								       name="mainwp_branding_hide_metabox_post_tags">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_post_tags"><?php _e( 'Tags', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePostAuthor ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_post_author"
								       name="mainwp_branding_hide_metabox_post_author">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_post_author"><?php _e( 'Author', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePostComments ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_post_comments"
								       name="mainwp_branding_hide_metabox_post_comments">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_post_comments"><?php _e( 'Comments', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePostRevisions ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_post_revisions"
								       name="mainwp_branding_hide_metabox_post_revisions">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_post_revisions"><?php _e( 'Revisions', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePostDiscussion ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_post_discussion"
								       name="mainwp_branding_hide_metabox_post_discussion">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_post_discussion"><?php _e( 'Discussion', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePostCategories ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_post_categories"
								       name="mainwp_branding_hide_metabox_post_categories">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_post_categories"><?php _e( 'Categories', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePostFields ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_post_custom_fields"
								       name="mainwp_branding_hide_metabox_post_custom_fields">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_post_custom_fields"><?php _e( 'Custom Fields', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePostTrackbacks ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_post_trackbacks"
								       name="mainwp_branding_hide_metabox_post_trackbacks">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_post_trackbacks"><?php _e( 'Send Trackbacks', 'mainwp' ); ?></label>
							</li>
						</ul>
					</td>
				</tr>

				<tr>
					<th scope="row">
						<?php _e( 'Hide Page Meta Boxes', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Hide meta boxes from the Edit Page panel on your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<ul class="mainwp_checkboxes mainwp_branding">
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePageFields ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_page_custom_fields"
								       name="mainwp_branding_hide_metabox_page_custom_fields">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_page_custom_fields"><?php _e( 'Custom Fields', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePageAuthor ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_page_author"
								       name="mainwp_branding_hide_metabox_page_author">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_page_author"><?php _e( 'Author ', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePageDiscussion ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_page_discussion"
								       name="mainwp_branding_hide_metabox_page_discussion">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_page_discussion"><?php _e( 'Discussion', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePageRevisions ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_page_revisions"
								       name="mainwp_branding_hide_metabox_page_revisions">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_page_revisions"><?php _e( 'Revisions', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePageAttributes ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_page_attributes"
								       name="mainwp_branding_hide_metabox_page_attributes">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_page_attributes"><?php _e( 'Page Attributes', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $hidePageSlug ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_hide_metabox_page_slug"
								       name="mainwp_branding_hide_metabox_page_slug">
								<label class="mainwp-label2"
								       for="mainwp_branding_hide_metabox_page_slug"><?php _e( 'Slug', 'mainwp' ); ?></label>
							</li>
						</ul>
					</td>
				</tr>


				<tr>
					<th scope="row">
						<?php _e( 'Global Footer Content', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Set Global Footer Content on your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<?php
						remove_editor_styles(); // stop custom theme styling interfering with the editor
						wp_editor( stripslashes( $globalFooter ), 'mainwp_branding_global_footer', array(
								'textarea_name' => 'mainwp_branding_global_footer',
								'textarea_rows' => 5,
								'teeny'         => true,
								'media_buttons' => false,
							)
						);
						?>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Dashboard Footer Content', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Set Dashboard Footer Content on your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<?php
						remove_editor_styles(); // stop custom theme styling interfering with the editor
						wp_editor( stripslashes( $dashboardFooter ), 'mainwp_branding_dashboard_footer', array(
								'textarea_name' => 'mainwp_branding_dashboard_footer',
								'textarea_rows' => 5,
								'teeny'         => true,
								'media_buttons' => false,
							)
						);
						?>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Site Generator Options', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Set Site Generator Options for your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div style="float: left;">
							<input type="text" name="mainwp_branding_site_generator"
							       id="mainwp_branding_site_generator"
							       value="<?php echo esc_attr( stripslashes( $siteGenerator ) ); ?>"/><br/><?php _e( 'Generator Text' ); ?>
						</div>
						<div style="float: left;">
							<input type="text" name="mainwp_branding_site_generator_link"
							       id="mainwp_branding_site_generator_link"
							       value="<?php echo esc_attr( stripslashes( $generatorLink ) ); ?>"/><br/><?php _e( 'Generator Link' ); ?>
						</div>
						<div style="clear:both;"></div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Custom Admin CSS', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Custom Admin area CSS for your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<textarea class="text" rows="8" cols="48" name="mainwp_branding_admin_css"
						          id="mainwp_branding_admin_css"><?php echo esc_textarea( stripslashes( $adminCss ) ); ?></textarea>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Custom Login CSS', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Custom Login page CSS for your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<textarea class="text" rows="8" cols="48" name="mainwp_branding_login_css"
						          id="mainwp_branding_login_css"><?php echo esc_textarea( stripslashes( $loginCss ) ); ?></textarea>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Text Replace', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Replace text on your child site(s).', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div id="mainwp-branding-text-replace-row-copy" class="hidden">
							<div class="mainwp-branding-text-replace-row">
								<div class="mainwp-branding-table-td-cell2" style="float: left;">
									<input type="text"
										     name="mainwp_branding_texts_value[]"
										     value=""
									       class="mainwp_branding_texts_value"/>
								</div>
								<div class="mainwp-branding-table-td-cell2" style="float: left;">
									<input type="text"
									       name="mainwp_branding_texts_replace[]"
												 value=""
									       class="mainwp_branding_texts_replace"/>
								</div>
								<div class="mainwp-branding-table-td-cell-opt">
									<a href="#" class="restore_text_replace button" title="<?php _e( 'Restore' ); ?>"><?php _e( 'Restore' ); ?></a> <a href="#" class="delete_text_replace button button-primary" title="<?php _e( 'Delete' ); ?>"><?php _e( 'Delete' ); ?></a>
								</div>
								<div style="clear:both;"></div>
							</div>
						</div>

						<?php
						foreach ( $textsReplace as $text => $replace ) {
							?>
							<div class="mainwp-branding-text-replace-row">
								<div class="mainwp-branding-table-td-cell2" style="float: left;">
									<input type="text" name="mainwp_branding_texts_value[]"
									       value="<?php echo esc_attr( stripslashes( $text ) ); ?>"
									       class="mainwp_branding_texts_value"/>
								</div>

								<div class="mainwp-branding-table-td-cell2" style="float: left;">
									<input type="text" name="mainwp_branding_texts_replace[]"
									       value="<?php echo esc_attr( stripslashes( $replace ) ); ?>"
									       class="mainwp_branding_texts_replace"/>
								</div>
								<div class="mainwp-branding-table-td-cell-opt" style="float: left;">
									<a href="#" class="delete_text_replace button"
									   title="<?php _e( 'Delete' ); ?>"><?php _e( 'Delete' ); ?></a>
								</div>
								<div style="clear:both;"></div>
							</div>
							<?php
						}
						?>
						<div class="mainwp-branding-text-replace-row">
							<div class="mainwp-branding-table-td-cell2" style="float: left;">
								<input type="text" id="mainwp_branding_texts_add_value"
								       name="mainwp_branding_texts_add_value" value=""/><br/>
								<?php _e( 'Find This Text' ); ?>
							</div>

							<div class="mainwp-branding-table-td-cell2" style="float: left;">
								<input type="text" id="mainwp_branding_texts_add_replace"
								       name="mainwp_branding_texts_add_replace" value=""/><br/>
								<?php _e( 'Replace With This' ); ?>
							</div>
							<div class="mainwp-branding-table-td-cell-opt" style="float: left;">
								<a href="#" class="add_text_replace button button-primary"
								   title="<?php _e( 'Add new' ); ?>"><?php _e( 'Add New' ); ?></a>
							</div>
							<div style="clear:both;"></div>
						</div>
						<div id="mainwp-branding-texts-replace-ajax-zone"
						     class="mainwp-notice mainwp-notice-yellow hidden"></div>
					</td>
				</tr>
				</tbody>
			</table>

		<?php
	}

	static function renderSupportOptions($post, $metabox) {
        $settings  = $metabox['args']['settings'];

        $showButton = $settings['showButton'];
        $showButtonIn = $settings['showButtonIn'];
        $buttonLabel = $settings['buttonLabel'];
        $supportMessage = $settings['supportMessage'];
        $submitButtonTitle = $settings['submitButtonTitle'];
        $sendMessage = $settings['sendMessage'];
        $returnMessage = $settings['returnMessage'];
        $supportEmail = $settings['supportEmail'];


		?>

			<table class="form-table">
				<tbody>
				<tr>
					<th scope="row">
						<?php _e( 'Show Support Button', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Enable the Support options on child site(s) and show the Contact Support button.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<div class="mainwp-checkbox">
							<input type="checkbox" id="mainwp_branding_show_support_button"
							       name="mainwp_branding_show_support_button" <?php echo( 0 == $showButton ? '' : 'checked="checked"' ); ?>
							       value="1"/>
							<label for="mainwp_branding_show_support_button"></label>
						</div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php _e( 'Show Button In', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Choose where you want to show the  Contact Support button, in the Top Admin Bar and/or Admin Menu.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<ul class="mainwp_checkboxes mainwp_branding">
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="1" <?php echo ( 1 == $showButtonIn || 3 == $showButtonIn ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_button_in_top_admin_bar"
								       name="mainwp_branding_button_in_top_admin_bar">
								<label class="mainwp-label2"
								       for="mainwp_branding_button_in_top_admin_bar"><?php _e( 'Top Admin Bar', 'mainwp' ); ?></label>
							</li>
							<li>
								<input type="checkbox" class="mainwp-checkbox2"
								       value="2" <?php echo ( 2 == $showButtonIn || 3 == $showButtonIn ) ? ' checked="checked" ' : '' ?>
								       id="mainwp_branding_button_in_admin_menu"
								       name="mainwp_branding_button_in_admin_menu">
								<label class="mainwp-label2"
								       for="mainwp_branding_button_in_admin_menu"><?php _e( 'Admin Menu', 'mainwp' ); ?></label>
							</li>
						</ul>
					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e( 'Contact Support Label', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Enter a title for contact support button.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<input type="text" name="mainwp_branding_button_contact_label"
						       id="mainwp_branding_button_contact_label"
						       value="<?php echo esc_attr( stripslashes( $buttonLabel ) ); ?>"/>
					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e( 'Intro Support Message', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Enter header message for the contact support form.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<?php
						remove_editor_styles(); // stop custom theme styling interfering with the editor
						wp_editor( stripslashes( $supportMessage ), 'mainwp_branding_support_message', array(
								'textarea_name' => 'mainwp_branding_support_message',
								'textarea_rows' => 5,
								'teeny'         => true,
								'media_buttons' => false,
							)
						);
						?>
					</td>
				</tr>
				<tr>
					<th scope="row"><?php _e( 'Submit button title', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Enter a title for submit button.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<input type="text" name="mainwp_branding_submit_button_title"
						       id="mainwp_branding_submit_button_title"
						       value="<?php echo esc_attr( stripslashes( $submitButtonTitle ) ); ?>"/>
					</td>
				</tr>
				</tr>
				<tr>
					<th scope="row"><?php _e( 'Successful Submission Message', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Enter the Message that will be displayed after successful form submission.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<input type="text" name="mainwp_branding_send_email_message"
						       id="mainwp_branding_send_email_message"
						       value="<?php echo esc_attr( stripslashes( $sendMessage ) ); ?>"/>
					</td>
				</tr>

				</tr>
				<tr>
					<th scope="row"><?php _e( 'Message to return sender to page they were on', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Enter a message to return sender to page they were on.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<input type="text" name="mainwp_branding_message_return_sender"
						       id="mainwp_branding_message_return_sender"
						       value="<?php echo esc_attr( stripslashes( $returnMessage ) ); ?>"/>
					</td>
				</tr>

				</tr>
				<tr>
					<th scope="row"><?php _e( 'Support Email Address', 'mainwp-branding-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Enter an email address for support emails.', 'mainwp-branding-extension' ) ); ?></th>
					<td>
						<input type="text" name="mainwp_branding_support_email" id="mainwp_branding_support_email"
						       value="<?php echo esc_attr( $supportEmail ); ?>"/> <?php _e( 'Note: Support form WILL NOT show up without an email address.', 'mainwp-branding-extension' ); ?>
					</td>
				</tr>

				</tbody>
			</table>

		<?php
	}

	static function get_instance() {

		if ( null == MainWP_Branding::$instance ) {
			MainWP_Branding::$instance = new MainWP_Branding();
		}

		return MainWP_Branding::$instance;
	}

	public static function handle_settings_post() {
		global $mainWPBrandingExtensionActivator;
		$is_individual = false;
		if ( isset( $_POST['branding_submit_nonce'] ) && wp_verify_nonce($_POST['branding_submit_nonce'], 'branding_nonce' )) {
			if (isset($_POST['branding_individual_settings_site_id']) )	{
				$is_individual = true;
				$websiteId = $_POST['branding_individual_settings_site_id'];

				if ( empty( $websiteId ) ) {
					return false;
				}
			}

			$current_extra_settings = array();
			if ( $is_individual && $websiteId ) {
				$site_branding = MainWP_Branding_DB::get_instance()->get_branding_by( 'site_id', $websiteId );
				if ( is_object( $site_branding ) ) {
					$current_extra_settings = unserialize( $site_branding->extra_settings );
				}
			}
			$output = array();

			$preserve_branding = ( isset( $_POST['mainwp_branding_preserve_branding'] ) && ! empty( $_POST['mainwp_branding_preserve_branding'] ) ) ? $_POST['mainwp_branding_preserve_branding'] : 0;

			$plugin_name = '';
			if ( isset( $_POST['mainwp_branding_plugin_name'] ) && ! empty( $_POST['mainwp_branding_plugin_name'] ) ) {
				$plugin_name = sanitize_text_field( $_POST['mainwp_branding_plugin_name'] );
			}
			$plugin_desc = '';
			if ( isset( $_POST['mainwp_branding_plugin_desc'] ) && ! empty( $_POST['mainwp_branding_plugin_desc'] ) ) {
				$plugin_desc = sanitize_text_field( $_POST['mainwp_branding_plugin_desc'] );
			}
			$plugin_uri = '';
			if ( isset( $_POST['mainwp_branding_plugin_uri'] ) && ! empty( $_POST['mainwp_branding_plugin_uri'] ) ) {
				$plugin_uri = trim( $_POST['mainwp_branding_plugin_uri'] );
				if ( ! preg_match( '/^https?\:\/\/.*$/i', $plugin_uri ) ) {
					$plugin_uri = 'http://' . $plugin_uri;
				}
			}
			$plugin_author = '';
			if ( isset( $_POST['mainwp_branding_plugin_author'] ) && ! empty( $_POST['mainwp_branding_plugin_author'] ) ) {
				$plugin_author = sanitize_text_field( $_POST['mainwp_branding_plugin_author'] );
			}
			$plugin_author_uri = '';
			if ( isset( $_POST['mainwp_branding_plugin_author_uri'] ) && ! empty( $_POST['mainwp_branding_plugin_author_uri'] ) ) {
				$plugin_author_uri = trim( $_POST['mainwp_branding_plugin_author_uri'] );
				if ( ! preg_match( '/^https?\:\/\/.*$/i', $plugin_author_uri ) ) {
					$plugin_author_uri = 'http://' . $plugin_author_uri;
				}
			}
			
			$login_image_link = isset( $_POST['mainwp_branding_login_image_link'] ) ? trim( $_POST['mainwp_branding_login_image_link'] ) : '';
			$login_image_title = isset( $_POST['mainwp_branding_login_image_title'] ) ? trim( $_POST['mainwp_branding_login_image_title'] ) : '';
			
			$plugin_hide = 0;
			if ( isset( $_POST['mainwp_branding_hide_child_plugin'] ) && ! empty( $_POST['mainwp_branding_hide_child_plugin'] ) ) {
				$plugin_hide = $_POST['mainwp_branding_hide_child_plugin'];
			}
			$disable_change = 0;
			if ( isset( $_POST['mainwp_branding_disable_change'] ) && ! empty( $_POST['mainwp_branding_disable_change'] ) ) {
				$disable_change = $_POST['mainwp_branding_disable_change'];
			}

			$disable_switching_theme = ( isset( $_POST['mainwp_branding_disable_switching_theme'] ) && ! empty( $_POST['mainwp_branding_disable_switching_theme'] ) ) ? intval( $_POST['mainwp_branding_disable_switching_theme'] ) : 0;

			$show_button = 0;
			if ( isset( $_POST['mainwp_branding_show_support_button'] ) && ! empty( $_POST['mainwp_branding_show_support_button'] ) ) {
				$show_button = intval( $_POST['mainwp_branding_show_support_button'] );
			}

			$show_button_in = ( isset( $_POST['mainwp_branding_button_in_top_admin_bar'] ) ? intval( $_POST['mainwp_branding_button_in_top_admin_bar'] ) : 0 ) + ( isset( $_POST['mainwp_branding_button_in_admin_menu'] ) ? intval( $_POST['mainwp_branding_button_in_admin_menu'] ) : 0 );

			$button_contact_label = '';
			if ( isset( $_POST['mainwp_branding_button_contact_label'] ) && ! empty( $_POST['mainwp_branding_button_contact_label'] ) ) {
				$button_contact_label = sanitize_text_field( $_POST['mainwp_branding_button_contact_label'] );
			}
			$send_email_message = '';
			if ( isset( $_POST['mainwp_branding_send_email_message'] ) && ! empty( $_POST['mainwp_branding_send_email_message'] ) ) {
				$send_email_message = sanitize_text_field( $_POST['mainwp_branding_send_email_message'] );
			}
			$support_email = '';
			if ( isset( $_POST['mainwp_branding_support_email'] ) && ! empty( $_POST['mainwp_branding_support_email'] ) ) {
				$support_email = trim( $_POST['mainwp_branding_support_email'] );
				if ( ! preg_match( '/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $support_email ) ) {
					$support_email = '';
				}
			}
			$support_message = '';
			if ( isset( $_POST['mainwp_branding_support_message'] ) && ! empty( $_POST['mainwp_branding_support_message'] ) ) {
				$support_message = $_POST['mainwp_branding_support_message'];
			}

			$remove_restore = 0;
			if ( isset( $_POST['mainwp_branding_remove_restore_clone'] ) && ! empty( $_POST['mainwp_branding_remove_restore_clone'] ) ) {
				$remove_restore = intval( $_POST['mainwp_branding_remove_restore_clone'] );
			}
			$remove_setting = 0;
			if ( isset( $_POST['mainwp_branding_remove_mainwp_setting'] ) && ! empty( $_POST['mainwp_branding_remove_mainwp_setting'] ) ) {
				$remove_setting = intval( $_POST['mainwp_branding_remove_mainwp_setting'] );
			}
			$remove_server_info = 0;
			if ( isset( $_POST['mainwp_branding_remove_mainwp_server_info'] ) && ! empty( $_POST['mainwp_branding_remove_mainwp_server_info'] ) ) {
				$remove_server_info = intval( $_POST['mainwp_branding_remove_mainwp_server_info'] );
			}
			$remove_wp_tools = 0;
			if ( isset( $_POST['mainwp_branding_remove_wp_tools'] ) && ! empty( $_POST['mainwp_branding_remove_wp_tools'] ) ) {
				$remove_wp_tools = intval( $_POST['mainwp_branding_remove_wp_tools'] );
			}
			$remove_wp_setting = 0;
			if ( isset( $_POST['mainwp_branding_remove_wp_setting'] ) && ! empty( $_POST['mainwp_branding_remove_wp_setting'] ) ) {
				$remove_wp_setting = intval( $_POST['mainwp_branding_remove_wp_setting'] );
			}
			$remove_permalink = 0;
			if ( isset( $_POST['mainwp_branding_remove_permalink'] ) && ! empty( $_POST['mainwp_branding_remove_permalink'] ) ) {
				$remove_permalink = intval( $_POST['mainwp_branding_remove_permalink'] );
			}

			$message_return_sender = '';
			if ( isset( $_POST['mainwp_branding_message_return_sender'] ) && ! empty( $_POST['mainwp_branding_message_return_sender'] ) ) {
				$message_return_sender = sanitize_text_field( $_POST['mainwp_branding_message_return_sender'] );
			}

			$submit_button_title = '';
			if ( isset( $_POST['mainwp_branding_submit_button_title'] ) && ! empty( $_POST['mainwp_branding_submit_button_title'] ) ) {
				$submit_button_title = sanitize_text_field( $_POST['mainwp_branding_submit_button_title'] );
			}

			$global_footer = '';
			if ( isset( $_POST['mainwp_branding_global_footer'] ) && ! empty( $_POST['mainwp_branding_global_footer'] ) ) {
				$global_footer = $_POST['mainwp_branding_global_footer'];
			}

			$dashboard_footer = '';
			if ( isset( $_POST['mainwp_branding_dashboard_footer'] ) && ! empty( $_POST['mainwp_branding_dashboard_footer'] ) ) {
				$dashboard_footer = $_POST['mainwp_branding_dashboard_footer'];
			}

			$site_generator = isset( $_POST['mainwp_branding_site_generator'] ) ? trim( $_POST['mainwp_branding_site_generator'] ) : '';
			$generator_link = isset( $_POST['mainwp_branding_site_generator_link'] ) ? trim( $_POST['mainwp_branding_site_generator_link'] ) : '';
			if ( ! empty( $generator_link ) && ! preg_match( '/^https?\:\/\/.*$/i', $generator_link ) ) {
				$generator_link = 'http://' . $generator_link;
			}

			$remove_widget_welcome  = isset( $_POST['mainwp_branding_remove_widget_welcome'] ) ? intval( $_POST['mainwp_branding_remove_widget_welcome'] ) : 0;
			$remove_widget_glance   = isset( $_POST['mainwp_branding_remove_widget_glance'] ) ? intval( $_POST['mainwp_branding_remove_widget_glance'] ) : 0;
			$remove_widget_activity = isset( $_POST['mainwp_branding_remove_widget_activity'] ) ? intval( $_POST['mainwp_branding_remove_widget_activity'] ) : 0;
			$remove_widget_quick    = isset( $_POST['mainwp_branding_remove_widget_quick'] ) ? intval( $_POST['mainwp_branding_remove_widget_quick'] ) : 0;
			$remove_widget_news     = isset( $_POST['mainwp_branding_remove_widget_news'] ) ? intval( $_POST['mainwp_branding_remove_widget_news'] ) : 0;

			$admin_css     = isset( $_POST['mainwp_branding_admin_css'] ) ? trim( $_POST['mainwp_branding_admin_css'] ) : '';
			$login_css     = isset( $_POST['mainwp_branding_login_css'] ) ? trim( $_POST['mainwp_branding_login_css'] ) : '';
			$texts_replace = array();
			if ( isset( $_POST['mainwp_branding_texts_value'] ) && is_array( $_POST['mainwp_branding_texts_value'] ) && count( $_POST['mainwp_branding_texts_value'] ) > 0 ) {
				foreach ( $_POST['mainwp_branding_texts_value'] as $i => $value ) {
					$value   = trim( $value );
					$replace = isset( $_POST['mainwp_branding_texts_replace'][ $i ] ) ? trim( $_POST['mainwp_branding_texts_replace'][ $i ] ) : '';
					if ( ! empty( $value ) && ! empty( $replace ) ) {
						$texts_replace[ $value ] = $replace;
					}
				}
			}

			$value   = isset( $_POST['mainwp_branding_texts_add_value'] ) ? trim( $_POST['mainwp_branding_texts_add_value'] ) : '';
			$replace = isset( $_POST['mainwp_branding_texts_add_replace'] ) ? trim( $_POST['mainwp_branding_texts_add_replace'] ) : '';
			if ( ! empty( $value ) && ! empty( $replace ) ) {
				$texts_replace[ $value ] = $replace;
			}

			//            $branding_dir = apply_filters('mainwp_getspecificdir',"branding/images");
			//            if (!file_exists($branding_dir)) {
			//                @mkdir($branding_dir, 0777, true);
			//            }
			//            if (!file_exists($branding_dir . '/index.php'))
			//            {
			//                @touch($branding_dir . '/index.php');
			//            }
			$image_login = 'NOTCHANGE';
			if ( isset( $_POST['mainwp_branding_delete_login_image'] ) && '1' == $_POST['mainwp_branding_delete_login_image'] ) {
				$image_login = '';
			}
			if ( UPLOAD_ERR_OK == $_FILES['mainwp_branding_login_image_file']['error'] ) {
				$output = self::handle_upload_image( $_FILES['mainwp_branding_login_image_file'], 'login', 310, 70 );
				if ( is_array( $output ) && isset( $output['fileurl'] ) && ! empty( $output['fileurl'] ) ) {
					$image_login      = $output['fileurl'];
					$image_login_path = $output['filepath'];
				}
			}

			$image_favico      = 'NOTCHANGE';
			$image_favico_path = '';
			if ( isset( $_POST['mainwp_branding_delete_favico_image'] ) && '1' == $_POST['mainwp_branding_delete_favico_image'] ) {
				$image_favico = '';
			}

			if ( UPLOAD_ERR_OK == $_FILES['mainwp_branding_favico_file']['error'] ) {
				$output = self::handle_upload_image( $_FILES['mainwp_branding_favico_file'], 'favico', 16, 16 );
				if ( is_array( $output ) && isset( $output['fileurl'] ) && ! empty( $output['fileurl'] ) ) {
					$image_favico      = $output['fileurl'];
					$image_favico_path = $output['filepath'];
				}
			}

			$hide_nag_update     = isset( $_POST['mainwp_branding_hide_nag_update'] ) ? intval( $_POST['mainwp_branding_hide_nag_update'] ) : 0;
			$hide_screen_options = isset( $_POST['mainwp_branding_hide_screen_options'] ) ? intval( $_POST['mainwp_branding_hide_screen_options'] ) : 0;
			$hide_help_box       = isset( $_POST['mainwp_branding_hide_help_box'] ) ? intval( $_POST['mainwp_branding_hide_help_box'] ) : 0;

			$hide_metabox_post_excerpt  = isset( $_POST['mainwp_branding_hide_metabox_post_excerpt'] ) ? intval( $_POST['mainwp_branding_hide_metabox_post_excerpt'] ) : 0;
			$hide_metabox_post_slug     = isset( $_POST['mainwp_branding_hide_metabox_post_slug'] ) ? intval( $_POST['mainwp_branding_hide_metabox_post_slug'] ) : 0;
			$hide_metabox_post_tags     = isset( $_POST['mainwp_branding_hide_metabox_post_tags'] ) ? intval( $_POST['mainwp_branding_hide_metabox_post_tags'] ) : 0;
			$hide_metabox_post_author   = isset( $_POST['mainwp_branding_hide_metabox_post_author'] ) ? intval( $_POST['mainwp_branding_hide_metabox_post_author'] ) : 0;
			$hide_metabox_post_comments = isset( $_POST['mainwp_branding_hide_metabox_post_comments'] ) ? intval( $_POST['mainwp_branding_hide_metabox_post_comments'] ) : 0;

			$hide_metabox_post_revisions     = isset( $_POST['mainwp_branding_hide_metabox_post_revisions'] ) ? intval( $_POST['mainwp_branding_hide_metabox_post_revisions'] ) : 0;
			$hide_metabox_post_discussion    = isset( $_POST['mainwp_branding_hide_metabox_post_discussion'] ) ? intval( $_POST['mainwp_branding_hide_metabox_post_discussion'] ) : 0;
			$hide_metabox_post_categories    = isset( $_POST['mainwp_branding_hide_metabox_post_categories'] ) ? intval( $_POST['mainwp_branding_hide_metabox_post_categories'] ) : 0;
			$hide_metabox_post_custom_fields = isset( $_POST['mainwp_branding_hide_metabox_post_custom_fields'] ) ? intval( $_POST['mainwp_branding_hide_metabox_post_custom_fields'] ) : 0;
			$hide_metabox_post_trackbacks    = isset( $_POST['mainwp_branding_hide_metabox_post_trackbacks'] ) ? intval( $_POST['mainwp_branding_hide_metabox_post_trackbacks'] ) : 0;

			$hide_metabox_page_custom_fields = isset( $_POST['mainwp_branding_hide_metabox_page_custom_fields'] ) ? intval( $_POST['mainwp_branding_hide_metabox_page_custom_fields'] ) : 0;
			$hide_metabox_page_author        = isset( $_POST['mainwp_branding_hide_metabox_page_author'] ) ? intval( $_POST['mainwp_branding_hide_metabox_page_author'] ) : 0;
			$hide_metabox_page_discussion    = isset( $_POST['mainwp_branding_hide_metabox_page_discussion'] ) ? intval( $_POST['mainwp_branding_hide_metabox_page_discussion'] ) : 0;
			$hide_metabox_page_revisions     = isset( $_POST['mainwp_branding_hide_metabox_page_revisions'] ) ? intval( $_POST['mainwp_branding_hide_metabox_page_revisions'] ) : 0;
			$hide_metabox_page_attributes    = isset( $_POST['mainwp_branding_hide_metabox_page_attributes'] ) ? intval( $_POST['mainwp_branding_hide_metabox_page_attributes'] ) : 0;
			$hide_metabox_page_slug          = isset( $_POST['mainwp_branding_hide_metabox_page_slug'] ) ? intval( $_POST['mainwp_branding_hide_metabox_page_slug'] ) : 0;

			if ( ! $is_individual ) {
				self::get_instance()->set_option( 'child_plugin_name', $plugin_name );
				self::get_instance()->set_option( 'child_plugin_desc', $plugin_desc );
				self::get_instance()->set_option( 'child_plugin_author', $plugin_author );
				self::get_instance()->set_option( 'child_plugin_author_uri', $plugin_author_uri );
				self::get_instance()->set_option( 'child_plugin_uri', $plugin_uri );
				self::get_instance()->set_option( 'child_plugin_hide', $plugin_hide );
				self::get_instance()->set_option( 'child_disable_change', $disable_change );
				self::get_instance()->set_option( 'child_disable_switching_theme', $disable_switching_theme );
				self::get_instance()->set_option( 'child_show_support_button', $show_button );
				self::get_instance()->set_option( 'child_show_support_button_in', $show_button_in );
				self::get_instance()->set_option( 'child_support_email', $support_email );
				self::get_instance()->set_option( 'child_support_message', $support_message );
				self::get_instance()->set_option( 'child_remove_restore', $remove_restore );
				self::get_instance()->set_option( 'child_remove_setting', $remove_setting );
				self::get_instance()->set_option( 'child_remove_server_info', $remove_server_info );
				self::get_instance()->set_option( 'child_remove_wp_tools', $remove_wp_tools );
				self::get_instance()->set_option( 'child_remove_wp_setting', $remove_wp_setting );
				self::get_instance()->set_option( 'child_remove_permalink', $remove_permalink );
				self::get_instance()->set_option( 'child_button_contact_label', $button_contact_label );
				self::get_instance()->set_option( 'child_send_email_message', $send_email_message );
				self::get_instance()->set_option( 'child_message_return_sender', $message_return_sender );
				self::get_instance()->set_option( 'child_submit_button_title', $submit_button_title );
				self::get_instance()->set_option( 'child_global_footer', $global_footer );
				self::get_instance()->set_option( 'child_dashboard_footer', $dashboard_footer );
				self::get_instance()->set_option( 'child_remove_widget_welcome', $remove_widget_welcome );
				self::get_instance()->set_option( 'child_remove_widget_glance', $remove_widget_glance );
				self::get_instance()->set_option( 'child_remove_widget_activity', $remove_widget_activity );
				self::get_instance()->set_option( 'child_remove_widget_quick', $remove_widget_quick );
				self::get_instance()->set_option( 'child_remove_widget_news', $remove_widget_news );
				self::get_instance()->set_option( 'child_login_image_link', $login_image_link );
				self::get_instance()->set_option( 'child_login_image_title', $login_image_title );				
				self::get_instance()->set_option( 'child_site_generator', $site_generator );
				self::get_instance()->set_option( 'child_generator_link', $generator_link );
				self::get_instance()->set_option( 'child_admin_css', $admin_css );
				self::get_instance()->set_option( 'child_login_css', $login_css );
				self::get_instance()->set_option( 'child_texts_replace', $texts_replace );
				if ( 'NOTCHANGE' !== $image_login ) {
					$old_file = self::get_instance()->get_option( 'child_login_image_path' );
					if ( ( $old_file != $image_login_path ) && ( $old_file != $image_favico_path ) ) {
						@unlink( $old_file );
					}
					self::get_instance()->set_option( 'child_login_image', $image_login );
					self::get_instance()->set_option( 'child_login_image_path', $image_login_path );
				}
				if ( 'NOTCHANGE' !== $image_favico ) {
					$old_file = self::get_instance()->get_option( 'child_favico_image_path' );
					if ( ( $old_file != $image_login_path ) && ( $old_file != $image_favico_path ) ) {
						@unlink( $old_file );
					}
					self::get_instance()->set_option( 'child_favico_image', $image_favico );
					self::get_instance()->set_option( 'child_favico_image_path', $image_favico_path );
				}

				self::get_instance()->set_option( 'child_hide_nag', $hide_nag_update );
				self::get_instance()->set_option( 'child_hide_screen_opts', $hide_screen_options );
				self::get_instance()->set_option( 'child_hide_help_box', $hide_help_box );

				self::get_instance()->set_option( 'child_hide_metabox_post_excerpt', $hide_metabox_post_excerpt );
				self::get_instance()->set_option( 'child_hide_metabox_post_slug', $hide_metabox_post_slug );
				self::get_instance()->set_option( 'child_hide_metabox_post_tags', $hide_metabox_post_tags );
				self::get_instance()->set_option( 'child_hide_metabox_post_author', $hide_metabox_post_author );
				self::get_instance()->set_option( 'child_hide_metabox_post_comments', $hide_metabox_post_comments );

				self::get_instance()->set_option( 'child_hide_metabox_post_revisions', $hide_metabox_post_revisions );
				self::get_instance()->set_option( 'child_hide_metabox_post_discussion', $hide_metabox_post_discussion );
				self::get_instance()->set_option( 'child_hide_metabox_post_categories', $hide_metabox_post_categories );
				self::get_instance()->set_option( 'child_hide_metabox_post_custom_fields', $hide_metabox_post_custom_fields );
				self::get_instance()->set_option( 'child_hide_metabox_post_trackbacks', $hide_metabox_post_trackbacks );

				self::get_instance()->set_option( 'child_hide_metabox_page_custom_fields', $hide_metabox_page_custom_fields );
				self::get_instance()->set_option( 'child_hide_metabox_page_author', $hide_metabox_page_author );
				self::get_instance()->set_option( 'child_hide_metabox_page_discussion', $hide_metabox_page_discussion );
				self::get_instance()->set_option( 'child_hide_metabox_page_revisions', $hide_metabox_page_revisions );
				self::get_instance()->set_option( 'child_hide_metabox_page_attributes', $hide_metabox_page_attributes );
				self::get_instance()->set_option( 'child_hide_metabox_page_slug', $hide_metabox_page_slug );
				self::get_instance()->set_option( 'child_preserve_branding', $preserve_branding );
                update_option( 'mainwp_branding_need_to_general_update', 1 );
			} else if ( $is_individual ) {
				$header   = serialize( array(
					'plugin_name'   => $plugin_name,
					'plugin_desc'   => $plugin_desc,
					'plugin_author' => $plugin_author,
					'author_uri'    => $plugin_author_uri,
					'plugin_uri'    => $plugin_uri,
				) );
				$branding = array(
					//'site_url'                    => '', // legacy
                    'site_id'                    => $websiteId,
					'plugin_header'               => $header,
					'hide_child_plugin'           => $plugin_hide,
					'disable_theme_plugin_change' => $disable_change,
					'show_support_button'         => $show_button,
					'support_email'               => $support_email,
					'support_message'             => $support_message,
					'remove_restore'              => $remove_restore,
					'remove_setting'              => $remove_setting,
					'remove_server_info'          => $remove_server_info,
					'remove_wp_tools'             => $remove_wp_tools,
					'remove_wp_setting'           => $remove_wp_setting,
					'button_contact_label'        => $button_contact_label,
					'send_email_message'          => $send_email_message,
					'override'                    => isset( $_POST['mainwp_branding_site_override'] ) ? intval( $_POST['mainwp_branding_site_override'] ) : 0,
				);

                if ( $site_branding ) {
                    $branding['id'] = $site_branding->id;
                }

				$extra_settings = array(
					'submit_button_title'             => $submit_button_title,
					'message_return_sender'           => $message_return_sender,
					'remove_permalink'                => $remove_permalink,
					'show_button_in'                  => $show_button_in,
					'disable_wp_branding'             => isset( $_POST['mainwp_branding_site_disable_wp_branding'] ) ? intval( $_POST['mainwp_branding_site_disable_wp_branding'] ) : 0,
					'global_footer'                   => $global_footer,
					'dashboard_footer'                => $dashboard_footer,
					'remove_widget_welcome'           => $remove_widget_welcome,
					'remove_widget_glance'            => $remove_widget_glance,
					'remove_widget_activity'          => $remove_widget_activity,
					'remove_widget_quick'             => $remove_widget_quick,
					'remove_widget_news'              => $remove_widget_news,
					'login_image_link'				  => $login_image_link,
					'login_image_title'				  => $login_image_title,
					'site_generator'                  => $site_generator,
					'generator_link'                  => $generator_link,
					'admin_css'                       => $admin_css,
					'login_css'                       => $login_css,
					'texts_replace'                   => $texts_replace,
					'image_favico'                    => $image_favico,
					'hide_nag'                        => $hide_nag_update,
					'hide_screen_opts'                => $hide_screen_options,
					'hide_help_box'                   => $hide_help_box,
					'hide_metabox_post_excerpt'       => $hide_metabox_post_excerpt,
					'hide_metabox_post_slug'          => $hide_metabox_post_slug,
					'hide_metabox_post_tags'          => $hide_metabox_post_tags,
					'hide_metabox_post_author'        => $hide_metabox_post_author,
					'hide_metabox_post_comments'      => $hide_metabox_post_comments,
					'hide_metabox_post_revisions'     => $hide_metabox_post_revisions,
					'hide_metabox_post_discussion'    => $hide_metabox_post_discussion,
					'hide_metabox_post_categories'    => $hide_metabox_post_categories,
					'hide_metabox_post_custom_fields' => $hide_metabox_post_custom_fields,
					'hide_metabox_post_trackbacks'    => $hide_metabox_post_trackbacks,
					'hide_metabox_page_custom_fields' => $hide_metabox_page_custom_fields,
					'hide_metabox_page_author'        => $hide_metabox_page_author,
					'hide_metabox_page_discussion'    => $hide_metabox_page_discussion,
					'hide_metabox_page_revisions'     => $hide_metabox_page_revisions,
					'hide_metabox_page_attributes'    => $hide_metabox_page_attributes,
					'hide_metabox_page_slug'          => $hide_metabox_page_slug,
					'preserve_branding'               => $preserve_branding,
					'disable_switching_theme' => $disable_switching_theme,										
				);

				if ( 'NOTCHANGE' === $image_login ) {
					$extra_settings['login_image']      = isset( $current_extra_settings['login_image'] ) ? $current_extra_settings['login_image'] : '';
					$extra_settings['login_image_path'] = isset( $current_extra_settings['login_image_path'] ) ? $current_extra_settings['login_image_path'] : '';
				} else {
					$extra_settings['login_image']      = $image_login;
					$extra_settings['login_image_path'] = $image_login_path;
					$old_file                           = isset( $current_extra_settings['login_image_path'] ) ? $current_extra_settings['login_image_path'] : '';
					if ( ( $old_file != $image_login_path ) && ( $old_file != $image_favico_path ) ) {
						@unlink( $old_file );
					}
				}

				if ( 'NOTCHANGE' === $image_favico ) {
					$extra_settings['favico_image']      = isset( $current_extra_settings['favico_image'] ) ? $current_extra_settings['favico_image'] : '';
					$extra_settings['favico_image_path'] = isset( $current_extra_settings['favico_image_path'] ) ? $current_extra_settings['favico_image_path'] : '';
				} else {
					$extra_settings['favico_image']      = $image_login;
					$extra_settings['favico_image_path'] = $image_login_path;
					$old_file                            = isset( $current_extra_settings['favico_image_path'] ) ? $current_extra_settings['favico_image_path'] : '';
					if ( ( $old_file != $image_login_path ) && ( $old_file != $image_favico_path ) ) {
						@unlink( $old_file );
					}
				}

				$branding['extra_settings'] = serialize( $extra_settings );

				$result = MainWP_Branding_DB::get_instance()->update_branding( $branding );

				update_option( 'mainwp_branding_need_to_update_site', 1 );
			}

			return $output;
		}

		return false;
	}

	public static function handle_upload_image( $file_input, $what, $max_width, $max_height ) {
		// assign error_message here
		$upload_dir = wp_upload_dir();
		$base_dir   = $upload_dir['basedir'];
		$base_url   = $upload_dir['baseurl'];
		$output     = array();
		$filename   = '';
		$filepath   = '';
		if ( UPLOAD_ERR_OK == $file_input['error'] ) {
			$tmp_file = $file_input['tmp_name'];
			if ( is_uploaded_file( $tmp_file ) ) {
				$file_size      = $file_input['size'];
				$file_type      = $file_input['type'];
				$file_name      = $file_input['name'];
				$file_extension = strtolower( pathinfo( $file_name, PATHINFO_EXTENSION ) );

				if ( ( $file_size > 500 * 1025 ) ) {
					$output['error'][] = ( 'login' === $what ) ? 0 : 3;
				} elseif (
					( 'image/jpeg' != $file_type ) &&
					( 'image/jpg' != $file_type ) &&
					( 'image/gif' != $file_type ) &&
					( 'image/png' != $file_type )
				) {
					$output['error'][] = ( 'login' === $what ) ? 1 : 4;
				} elseif (
					( 'jpeg' != $file_extension ) &&
					( 'jpg' != $file_extension ) &&
					( 'gif' != $file_extension ) &&
					( 'png' != $file_extension )
				) {
					$output['error'][] = ( 'login' === $what ) ? 1 : 4;
				} else {

					$dest_file = $base_dir . '/' . $file_name;
					$dest_file = dirname( $dest_file ) . '/' . wp_unique_filename( dirname( $dest_file ), basename( $dest_file ) );

					if ( move_uploaded_file( $tmp_file, $dest_file ) ) {
						if ( file_exists( $dest_file ) ) {
							list( $width, $height, $type, $attr ) = getimagesize( $dest_file );
						}

						$resize = false;
						if ( $width > $max_width ) {
							$dst_width = $max_width;
							if ( $height > $max_height ) {
								$dst_height = $max_height;
							} else {
								$dst_height = $height;
							}
							$resize = true;
						} else if ( $height > $max_height ) {
							$dst_width  = $width;
							$dst_height = $max_height;
							$resize     = true;
						}

						if ( $resize ) {
							$src          = $dest_file;
							$cropped_file = wp_crop_image( $src, 0, 0, $width, $height, $dst_width, $dst_height, false );
							if ( ! $cropped_file || is_wp_error( $cropped_file ) ) {
								$output['error'][] = ( 'login' === $what ) ? 8 : 9;
							} else {
								@unlink( $dest_file );
								$filename = basename( $cropped_file );
								$filepath = $cropped_file;
							}
						} else {
							$filename = basename( $dest_file );
							$filepath = $dest_file;
						}
					} else {
						$output['error'][] = ( 'login' === $what  ) ? 2 : 5;
					}
				}
			}
		}
		$output['fileurl']  = ! empty( $filename ) ? $base_url . '/' . $filename : '';
		$output['filepath'] = ! empty( $filepath ) ? $filepath : '';

		return $output;
	}

	public function ajax_save_settings() {
		$siteid = $_POST['siteId'];

        if ( empty( $siteid ) ) {
			die( json_encode( array( 'error' => 'Error: site ID empty' ) ) );
		}

        $is_individual = isset($_POST['individual']) && $_POST['individual'] ? true : false;
        $result = MainWP_Branding_DB::get_instance()->get_branding_by( 'site_id', $siteid );
        $save_individual_settings = false;

        if ($is_individual) {
            // error data then stop
            if (empty($result)) {
                die( json_encode( array( 'error' => __('Update failed: Settings empty.', 'mainwp-branding-extension') ) ) );
                return;
            }
            if ($result->override) {
                // if individual saving and overrided then save the individual settings
                $save_individual_settings = true;
            }

        } else if ( $result->override ) {
            // if general saving and overrided then dont update.
            die( json_encode( array( 'result' => 'OVERRIDED' ) ) );
        }

		$information = $this->perform_save_settings($siteid, $save_individual_settings);
		die( json_encode( $information ) );
	}


	function mainwp_apply_plugin_settings($siteid) {
		$information = $this->perform_save_settings($siteid);
		$result = array();
		if (is_array($information)) {
			if ($information['result'] == 'SUCCESS') {
				$result = array('result' => 'success');
			} else if ($information['error']) {
				$result = array('error' => $information['error']);
			} else {
				$result = array('result' => 'failed');
			}
		} else {
			$result = array('result' => 'failed');
		}
		die( json_encode( $result ) );
	}

	public function perform_save_settings($siteid, $save_individual_settings = false) {
		global $mainWPBrandingExtensionActivator;

        $branding = MainWP_Branding_DB::get_instance()->get_branding_by( 'site_id', $siteid );

		$post_data = array( 'action' => 'update_branding' );
		if ( $save_individual_settings && is_object( $branding ) ) {
			$header                                   = unserialize( $branding->plugin_header );
            $extra_settings = unserialize( $branding->extra_settings );

			$settings                                 = array(
				'child_plugin_name'          => $header['plugin_name'],
				'child_plugin_desc'          => $header['plugin_desc'],
				'child_plugin_author'        => $header['plugin_author'],
				'child_plugin_author_uri'    => $header['author_uri'],
				'child_plugin_plugin_uri'    => $header['plugin_uri'],
				'child_plugin_hide'          => $branding->hide_child_plugin,
				'child_disable_change'       => $branding->disable_theme_plugin_change,
				'child_show_support_button'  => $branding->show_support_button,
				'child_support_email'        => $branding->support_email,
				'child_support_message'      => $branding->support_message,
				'child_remove_restore'       => $branding->remove_restore,
				'child_remove_setting'       => $branding->remove_setting,
				'child_remove_server_info'   => $branding->remove_server_info,
				'child_remove_wp_tools'      => $branding->remove_wp_tools,
				'child_remove_wp_setting'    => $branding->remove_wp_setting,
				'child_button_contact_label' => $branding->button_contact_label,
				'child_send_email_message'   => $branding->send_email_message,
			);
			$settings['child_submit_button_title']    = $extra_settings['submit_button_title'];
			$settings['child_message_return_sender']  = $extra_settings['message_return_sender'];
			$settings['child_remove_permalink']       = $extra_settings['remove_permalink'];
			$settings['child_show_support_button_in'] = $extra_settings['show_button_in'];
			$settings['child_global_footer']          = $extra_settings['global_footer'];
			$settings['child_dashboard_footer']       = $extra_settings['dashboard_footer'];
			$settings['child_remove_widget_welcome']  = $extra_settings['remove_widget_welcome'];
			$settings['child_remove_widget_glance']   = $extra_settings['remove_widget_glance'];
			$settings['child_remove_widget_activity'] = $extra_settings['remove_widget_activity'];
			$settings['child_remove_widget_quick']    = $extra_settings['remove_widget_quick'];
			$settings['child_remove_widget_news']     = $extra_settings['remove_widget_news'];
			$settings['child_login_image_link']     = $extra_settings['login_image_link'];			
			$settings['child_login_image_title']     = $extra_settings['login_image_title'];
			$settings['child_site_generator']         = $extra_settings['site_generator'];
			$settings['child_generator_link']         = $extra_settings['generator_link'];
			$settings['child_admin_css']              = $extra_settings['admin_css'];
			$settings['child_login_css']              = $extra_settings['login_css'];
			$settings['child_texts_replace']          = $extra_settings['texts_replace'];
			$settings['child_login_image']            = $extra_settings['login_image'];
			$settings['child_favico_image']           = $extra_settings['favico_image'];
			$settings['child_hide_nag']               = $extra_settings['hide_nag'];
			$settings['child_hide_screen_opts']       = $extra_settings['hide_screen_opts'];
			$settings['child_hide_help_box']          = $extra_settings['hide_help_box'];

			$settings['child_hide_metabox_post_excerpt']  = $extra_settings['hide_metabox_post_excerpt'];
			$settings['child_hide_metabox_post_slug']     = $extra_settings['hide_metabox_post_slug'];
			$settings['child_hide_metabox_post_tags']     = $extra_settings['hide_metabox_post_tags'];
			$settings['child_hide_metabox_post_author']   = $extra_settings['hide_metabox_post_author'];
			$settings['child_hide_metabox_post_comments'] = $extra_settings['hide_metabox_post_comments'];

			$settings['child_hide_metabox_post_revisions']     = $extra_settings['hide_metabox_post_revisions'];
			$settings['child_hide_metabox_post_discussion']    = $extra_settings['hide_metabox_post_discussion'];
			$settings['child_hide_metabox_post_categories']    = $extra_settings['hide_metabox_post_categories'];
			$settings['child_hide_metabox_post_custom_fields'] = $extra_settings['hide_metabox_post_custom_fields'];
			$settings['child_hide_metabox_post_trackbacks']    = $extra_settings['hide_metabox_post_trackbacks'];

			$settings['child_hide_metabox_page_custom_fields'] = $extra_settings['hide_metabox_page_custom_fields'];
			$settings['child_hide_metabox_page_author']        = $extra_settings['hide_metabox_page_author'];
			$settings['child_hide_metabox_page_discussion']    = $extra_settings['hide_metabox_page_discussion'];
			$settings['child_hide_metabox_page_revisions']     = $extra_settings['hide_metabox_page_revisions'];
			$settings['child_hide_metabox_page_attributes']    = $extra_settings['hide_metabox_page_attributes'];
			$settings['child_hide_metabox_page_slug']          = $extra_settings['hide_metabox_page_slug'];
			$settings['child_preserve_branding']               = $extra_settings['preserve_branding'];
			$settings['child_disable_switching_theme']         = $extra_settings['disable_switching_theme'];
			$settings['child_disable_wp_branding']             = $extra_settings['disable_wp_branding'] ? 'Y' : 'N';
			$post_data['specical'] = true;
		} else {
			$settings = $this->option; // using general settings
		}

		if ( isset( $settings['child_login_image'] ) && ! empty( $settings['child_login_image'] ) ) {
			$fix_site_url = !preg_match('#^https?://#i', $settings['child_login_image']) ? get_site_url() : "";
			$settings['child_login_image_url'] = $fix_site_url . $settings['child_login_image'];
		} else {
			$settings['child_login_image_url'] = '';
		}

		if ( isset( $settings['child_favico_image'] ) && ! empty( $settings['child_favico_image'] ) ) {
			$fix_site_url = !preg_match("/^https?:/", $settings['child_favico_image']) ? get_site_url() : "";
			$settings['child_favico_image_url'] = $fix_site_url . $settings['child_favico_image'];
		} else {
			$settings['child_favico_image_url'] = '';
		}

		if ( isset( $settings['child_admin_css'] ) ) {
			$style                       = stripslashes( $settings['child_admin_css'] );
			$style                       = preg_replace( '|^[\s]*<script>|', '', $style );
			$style                       = preg_replace( '|<\/script>[\s]*$|', '', $style );
			$style                       = trim( $style );
			$settings['child_admin_css'] = $style;
		}

		if ( isset( $settings['child_login_css'] ) ) {
			$style                       = stripslashes( $settings['child_login_css'] );
			$style                       = preg_replace( '/^[\s]*<script>/', '', $style );
			$style                       = preg_replace( '/<\/script>[\s]*$/', '', $style );
			$style                       = trim( $style );
			$settings['child_login_css'] = $style;
		}

//		// this is post from site edit page
//		if ( is_object( $branding ) ) {
//			$settings['child_disable_wp_branding'] = $extra_settings['disable_wp_branding'] ? 'Y' : 'N';
//		}

        $settings = apply_filters('mainwp_branding_settings_before_save_to_sites', $settings, $siteid);

		$post_data['settings'] = base64_encode( serialize( $settings ) );

		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPBrandingExtensionActivator->get_child_file(), $mainWPBrandingExtensionActivator->get_child_key(), $siteid, 'branding_child_plugin', $post_data );
		return $information;
	}
}
