var branding_MaxThreads = 3;
var branding_CurrentThreads = 0;
var branding_TotalThreads = 0;
var branding_FinishedThreads = 0;

mainwp_branding_start_next = function () {
	if (branding_TotalThreads == 0) {
		branding_TotalThreads = jQuery( '.mainwpBrandingSitesItem[status="queue"]' ).length; }

	while ((siteToBranding = jQuery( '.mainwpBrandingSitesItem[status="queue"]:first' )) && (siteToBranding.length > 0) && (branding_CurrentThreads < branding_MaxThreads)) {
		mainwp_branding_start_specific( siteToBranding );
	}
};

mainwp_branding_start_specific = function (pSiteToBranding) {
	branding_CurrentThreads++;
	pSiteToBranding.attr( 'status', 'progress' );
	var statusEl = pSiteToBranding.find( '.status' ).html( '<i class="fa fa-spinner fa-pulse"></i> ' + 'Running...' );
	var detailEl = pSiteToBranding.find( '.detail' );

	var data = {
		action: 'mainwp_branding_performbrandingchildplugin',
		siteId: pSiteToBranding.attr( 'siteid' )
	};

	jQuery.post(ajaxurl, data, function (response) {
		pSiteToBranding.attr( 'status', 'done' );
		if (response && response['result'] == 'OVERRIDED') {
			statusEl.html( 'Updates skipped - individual site settings are in use' ).show();
			statusEl.css( 'color', '#a00' );
		} else if (response && response['result'] == 'SUCCESS') {
			statusEl.html( 'Successful' ).show();
			if (response['error'] && response['error']['login_image']) {
				var error = 'Login image:' + response['error']['login_image'];
				detailEl.html( error );
				detailEl.css( 'color', '#a00' );
			}
		} else if (response && response['error']) {
			statusEl.html( response['error'] ).show();
			statusEl.css( 'color', '#a00' );
		} else {
			statusEl.html( __( 'Undefined error' ) ).show();
			statusEl.css( 'color', '#a00' );
		}

		branding_CurrentThreads--;
		branding_FinishedThreads++;
		if (branding_FinishedThreads == branding_TotalThreads && branding_FinishedThreads != 0) {
			jQuery( '#mainwp_branding_apply_setting_ajax_message_zone' ).html( 'Settings have been saved successfully.' ).fadeIn( 100 );
                        jQuery( '#mainwp_branding_apply_setting_ajax_message_zone' ).after('<a href="admin.php?page=Extensions-Mainwp-Branding-Extension" class="button-primary button button-hero" title="Go Back to Settings">Go Back to Settings</a>');
		}
		mainwp_branding_start_next();
	}, 'json');
};


mainwp_branding_update_specical_site = function () {
	var data = {
		action: 'mainwp_branding_performbrandingchildplugin',
		siteId: jQuery('#branding_individual_settings_site_id').val(),
                individual: 1
	};
	statusEl = jQuery( '#mainwp_branding_edit_site_ajax_message_zone' );
        statusEl.html('<i class="fa fa-spinner fa-pulse"></i> Saving to child site...').show();
	jQuery.post(ajaxurl, data, function (response) {
		if (response && response['result'] == 'SUCCESS') {
			statusEl.html( 'Child site has been updated successfully.' ).fadeIn();
		} else if (response && response['error']) {
			statusEl.css( 'color', '#a00' );
			statusEl.html( response['error'] ).fadeIn();
		} else {
			statusEl.css( 'color', '#a00' );
			statusEl.html( __( 'Undefined error: setting could not be saved to your child sites. Please contact <a href="https://mainwp.com/support/">MainWP Support</a>.' ) ).fadeIn();
		}
	}, 'json');
}

var mwp_branding_save_alert = false;

jQuery( document ).ready(function ($) {

	$( '.add_text_replace' ).live('click', function () {
		var errors = [];
		$( '#mainwp_branding_texts_add_value' ).removeClass( 'form-invalid' );
		$( '#mainwp_branding_texts_add_replace' ).removeClass( 'form-invalid' );
		if ($.trim( $( '#mainwp_branding_texts_add_value' ).val() ) == '') {
			errors.push( __( 'The Text field can not be empty! Please enter some value.' ) );
			$( '#mainwp_branding_texts_add_value' ).addClass( 'form-invalid' );
		}

		if ($.trim( $( '#mainwp_branding_texts_add_replace' ).val() ) == '') {
			errors.push( __( 'The Text Replace field can not be empty! Please add some value.' ) );
			$( '#mainwp_branding_texts_add_replace' ).addClass( 'form-invalid' );
		}

		if (errors.length > 0) {
			$( '#mainwp-branding-texts-replace-ajax-zone' ).html( errors.join( '<br/>' ) ).show();
			return false;
		} else {
			$( '#mainwp-branding-texts-replace-ajax-zone' ).html( "" ).hide();
		}

		var parent = $( this ).closest( '.mainwp-branding-text-replace-row' );
		parent.before( $( "#mainwp-branding-text-replace-row-copy" ).html() ).fadeIn();
		var newRow = parent.prev();
		newRow.find( "input[name='mainwp_branding_texts_value[]']" ).val( $( '#mainwp_branding_texts_add_value' ).val() );
		newRow.find( "input[name='mainwp_branding_texts_replace[]']" ).val( $( '#mainwp_branding_texts_add_replace' ).val() );
		$( '#mainwp_branding_texts_add_value' ).val( '' );
		$( '#mainwp_branding_texts_add_replace' ).val( '' );
		return false;
	})

	$( '.delete_text_replace' ).live('click', function () {
		$( this ).closest( '.mainwp-branding-text-replace-row' ).remove();
		return false;
	});
        
        jQuery(document).on('click', '#branding_submit_btn', function () {
            if (!confirm('Are you sure?'))
                return false;            
        });

	$( '.mwp_branding_reset_btn' ).live('click', function () {
		if ( ! confirm( "Are you sure you want to reset the current branding settings?" )) {
			return false; }

		for (var k in mainwpBrandingDefaultOpts.checkboxes) {
			jQuery( '#' + k ).prop( "checked", mainwpBrandingDefaultOpts.checkboxes[k] );
		}
		for (var k in mainwpBrandingDefaultOpts.textareas) {
			jQuery( 'textarea[name="' + k + '"]' ).val( mainwpBrandingDefaultOpts.textareas[k] );
		}
		for (var k in mainwpBrandingDefaultOpts.textbox_id) {
			jQuery( '#' + k ).val( mainwpBrandingDefaultOpts.textbox_id[k] );
		}
		for (var k in mainwpBrandingDefaultOpts.tinyMCEs) {
			var editor = window.parent.tinymce.get( k );
			if (editor != null && typeof(editor) !== "undefined" && editor.isHidden() == false) {
				editor.setContent( mainwpBrandingDefaultOpts.tinyMCEs[k] );
			} else {
				var obj = $( '#' + k );
				obj.val( mainwpBrandingDefaultOpts.tinyMCEs[k] );
			}
		}
		for (var k in mainwpBrandingDefaultOpts.textbox_class) {
			if (/^(?:mainwp_branding_texts_value)$/.test( k )) {
				jQuery( '.' + k ).each(function () {
					jQuery( this ).closest( '.mainwp-branding-text-replace-row' ).each(function () {
						var row = this;
						if (jQuery( this ).closest( "#mainwp-branding-text-replace-row-copy" ).length == 0) {
							jQuery( row ).remove(); }
					});

				})
			}
		}
		mwp_branding_save_alert = true;
		mwp_branding_save_reminder();
		alert( "Click the Save Settings button in the bottom of the page to save changes." );
	})
})

function mwp_branding_save_reminder() {
	setTimeout(function () {
		if (mwp_branding_save_alert) {
			alert( "Click the Save Settings button in the bottom of the page to save changes." );
			mwp_branding_save_reminder();
		}
	}, 1000 * 60 * 10);
}