��    �      �  �   L	      `     a  )   q     �     �     �  
   �     �     �  "     \   <  "   �     �     �     �     �            w   ,     �     �  
   �  
   �     �  H   �     3     J     f     t     �  
   �     �  +   �  A   �  "        =  )   ]  *   �  )   �  "   �     �               0     G     U     f      k  1   �  �   �  �   �  �   �  �   v  �   ^  �     �   �  �   �  !  Z  "  |  �   �  �   �  �   l  �   :     	  s     
   �  
   �     �     �  	   �  3   �     �          "     >     [     w  P   �     �     �  
      F      #   V       z   !   �      �      �   (   �   2   !     >!  7   [!  "   �!  &   �!  !   �!  #   �!  U   #"  -   y"  $   �"  4   �"  %   #     '#  '   D#     l#     �#     �#     �#     �#     �#     �#     �#  
   �#  
    $  
   $  
   $  &   !$  U   H$  *   �$  <   �$  +   %  *   2%     ]%  $   y%  "   �%  "   �%  6   �%     &     ,&  l   A&  y   �&  
   ('  
   3'  ,   >'  =   k'  4   �'  E   �'  A   $(  D   f(  H   �(  �   �(  d   w)  p   �)  X   M*  h   �*  a   +  `   q+  Y   �+  9   ,,  �  f,     !/  3   4/     h/     /     �/     �/     �/     �/  ,   �/  l   0  %   �0     �0     �0  $   �0     �0     	1     1  }   '1     �1     �1     �1     �1     �1  O   2     R2  .   p2     �2     �2  
   �2     �2     �2     �2  :   3  ,   >3  &   k3  (   �3  2   �3  (   �3  +   4     C4     ]4      d4     �4     �4     �4     �4  $   �4  6   �4  �   (5  �   6  �   �6  �   �7  �   �8  �   �9  �   �:  �   ^;    :<    @=    G>    U?  �   d@  �   cA     cB  b   kB     �B     �B     �B     C     C  +   (C     TC     lC     ~C     �C     �C     �C  Y   �C     FD     TD  	   jD  I   tD  +   �D  !   �D  !   E     .E     CE  $   SE  4   xE     �E  &   �E     �E     �E     F     F  @   -F     nF     �F     �F     �F     �F     �F      G     G     (G     5G     FG     \G     jG  	   xG     �G     �G     �G     �G     �G  J   �G  -   H  E   5H  /   {H  6   �H  #   �H     I  #   &I  "   JI  >   mI     �I     �I  z   �I  �   ZJ     �J     �J  /   K  A   1K  /   sK  A   �K  >   �K  F   $L  U   kL  �   �L  s   MM  q   �M  ^   3N  a   �N  k   �N  e   `O  o   �O  D   6P     4   p       w   X   6       2   z            �   U      (   �   h      $   n   0           �       �   �                  /       �   9   u   -   l                  �           V   O   �   x   Z   1       q           b   &          _   k   �   *       �   e       }   �   M              c   �       	   T   �       #           {          D         A   P   I   @      8   a   L   J      R   3   �               �   g      Y   i   <   �   ]   �          v   F                  =   ,   "   �   H   �   G   r           \   +               [      .   
       S             o   ;   m       ~   C      )   7      '   j      |   �       Q   >   t   %       d   N          !       K       f      E                 :      ^   ?   s   B   5       `   W          y    + Add Threshold Allow coupon event management for vendors Allowed placeholders: Amount thresholds An error occurred: %s Birth date Birthday Input Date Format Choose the coupon to send Choose which email format to send. Choose which email template to send. Remember to save options before sending the test email. Click to collapse/expand the table Collapse Coupon Email System Coupon Email System settings Coupon amount: %s%s off Coupon assigned Coupon code:  Coupon management must be enabled to make YITH WooCommerce Coupon Email System work correctly for vendors. %s Enable %s Coupon not valid Coupon settings DD-MM-YYYY DD/MM/YYYY Days to elapse Delete automatically expired coupons (only those created by this plugin) Delete expired coupons Deleting expired coupons... Email content Email subject Email template Email type Enable Mandrill Enable YITH WooCommerce Coupon Email System Enable coupon on a specific number of days from the last purchase Enable coupon on customer birthday Enable coupon on first purchase Enable coupon on specific order threshold Enable coupon on specific product purchase Enable coupon on specific spent threshold Enable coupon on user registration Enable coupon sending Expand Expiration date: %s Following products: %s Free shipping General Settings HTML Happy birthday from {site_title} Happy birthday from {vendor_name} on {site_title} Hi {customer_name},
thanks for making the first purchase on {order_date} on our shop {site_title}!
Because of this, we would like to offer you this coupon as a little gift:

{coupon_description}

See you on our shop,

{site_title}. Hi {customer_name},
thanks for making the first purchase on {order_date} on our shop {site_title}!
Because of this, we would like to offer you this coupon as a little gift:

{coupon_description}

See you on our shop,

{vendor_name}. Hi {customer_name},
thanks for purchasing the following product with the order made on {order_date}: {purchased_product}.
We would like to offer you this coupon as a gift:

{coupon_description}

See you on our shop,

{site_title}. Hi {customer_name},
thanks for purchasing the following product with the order made on {order_date}: {purchased_product}.
We would like to offer you this coupon as a gift:

{coupon_description}

See you on our shop,

{vendor_name}. Hi {customer_name},
thanks for your the registration on {site_title}!
We would like to offer you this coupon as a welcome gift:

{coupon_description}

See you on our shop,

{site_title}. Hi {customer_name},
thanks for your the registration on {site_title}!
We would like to offer you this coupon as a welcome gift:

{coupon_description}

See you on our shop,

{vendor_name}. Hi {customer_name},
we would like to make you our best wishes for a happy birthday!
Please, accept our coupon as a small gift for you:

{coupon_description}

See you on our shop,

{site_title}. Hi {customer_name},
we would like to make you our best wishes for a happy birthday!
Please, accept our coupon as a small gift for you:

{coupon_description}

See you on our shop,

{vendor_name}. Hi {customer_name},
with the order made on {order_date}, you have reached the amount of {spending_threshold} for a total purchase amount of {customer_money_spent}!
Because of this, we would like to offer you this coupon as a gift:

{coupon_description}

See you on our shop,

{site_title}. Hi {customer_name},
with the order made on {order_date}, you have reached the amount of {spending_threshold} for a total purchase amount of {customer_money_spent}!
Because of this, we would like to offer you this coupon as a gift:

{coupon_description}

See you on our shop,

{vendor_name}. Hi {customer_name},
with the order made on {order_date}, you have reached {purchases_threshold} orders!
Because of this, we would like to offer you this coupon as a gift:

{coupon_description}

See you on our shop,

{site_title}. Hi {customer_name},
with the order made on {order_date}, you have reached {purchases_threshold} orders!
Because of this, we would like to offer you this coupon as a gift:

{coupon_description}

See you on our shop,

{vendor_name}. Hi {customer_name},
{days_ago} days have passed since your last order.
We would like to encourage you to purchase something more with this coupon:

{coupon_description}

See you on our shop,

{site_title}. Hi {customer_name},
{days_ago} days have passed since your last order.
We would like to encourage you to purchase something more with this coupon:

{coupon_description}

See you on our shop,

{vendor_name}. How To In order to use some of the features of YITH WooCommerce Coupon Email System you need to create at least one coupon MM-DD-YYYY MM/DD/YYYY Mandrill API Key Mandrill Settings More info On a specific number of days from the last purchase On customer birthday On first purchase On specific order threshold On specific product purchase On specific spent threshold On user registration Operation completed. %d coupon trashed. Operation completed. %d coupons trashed. Order thresholds Placeholder reference Plain text Please enter Mandrill API Key for YITH WooCommerce Coupon Email System Please insert a valid email address Please select at least a product Please specify the number of days Plugin Documentation Premium Version Products of the following categories: %s Products that will cause the sending of the coupon Remove selected threshold(s) Replaced with the amount of money spent by the customer Replaced with the customer's email Replaced with the customer's last name Replaced with the customer's name Replaced with the date of the order Replaced with the description of the given coupon. This placeholder must be included. Replaced with the name of a purchased product Replaced with the name of the vendor Replaced with the number of days since last purchase Replaced with the number of purchases Replaced with the site title Replaced with the spent amount of money Search for a product&hellip; Select a coupon Selected Coupon Send Test Email Sending test email... Settings Target products Target value Template 1 Template 2 Template 3 Test email Test email has been sent successfully! The number of days that have to pass after the last order has been set as "completed" There was an error while sending the email This coupon cannot be used in conjunction with other coupons This coupon will not apply to items on sale Type an email address to send a test email Use Mandrill to send emails Use YITH WooCommerce Email Templates Valid for a maximum purchase of %s Valid for a minimum purchase of %s Valid for a minimum purchase of %s and a maximum of %s Vendors Settings WooCommerce Template YITH WooCommerce Coupon Email System is enabled but not effective. It requires WooCommerce in order to work. YITH WooCommerce Coupon Email System offers an automatic way to send a coupon to your users according to specific events. YYYY-MM-DD YYYY/MM/DD You have received a coupon from {site_title} You have received a coupon from {vendor_name} on {site_title} You have received a welcome coupon from {site_title} You have received a welcome coupon from {vendor_name} on {site_title} You need to select a coupon to send one for a new first purchase. You need to select a coupon to send one for a new user registration. You need to select at least one product to send a coupon once purchased. You need to select at least the amount/percentage of a coupon to send it after a specific number of days following the last order. You need to select at least the amount/percentage of a coupon to send it for the birthday of a user. You need to select at least the amount/percentage of a coupon to send it for the purchase of a specific product. You need to select at least the amount/percentage of a coupon to send it in a test email You need to set a coupon for each threshold to send one when users reach a specific number of purchases. You need to set a coupon for each threshold to send one when users reach a specific spent amount. You need to set a threshold to send a coupon once a user reaches a specific number of purchases. You need to set a threshold to send a coupon once a user reaches a specific spent amount. You need to set at least a threshold to send a test email Project-Id-Version: YITH WooCommerce Coupon Email System Premium v1.1.3
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-08-23 16:01+0200
PO-Revision-Date: 2016-08-23 16:01+0200
Last-Translator: rune <rune@arctic-fritid.no>
Language-Team: 
Language: nb_NO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.8.7.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: plugin-fw
 + Legg til terskel Tillat kupong hendelsesbehandling for leverandører Tillatte plassholdere: Beløpsterskler Det oppstod en feil: %s Fødselsdato Format på fødselsdato Velg kupongen som skal sendes Velg det e-post formatet du ønsker å bruke Velg hvilken e-postmal som skal brukes. Husk å lagre alternativene før du sender en eventuell test e-post. Klikk for å kollapse/utvide tabellen Kollapse Kupong e-post system "Kupong e-post system" innstillinger Rabattkupongens verdi: %s%s Kupong tildelt Rabattkupong:  Bruk av rabattkupong må være aktivert for at "Kupong e-post systemet" skal fungere korrekt for leverandører. %s Aktiver %s Rabattkupongen er ikke gyldig Kuponginnstillinger DD-MM-ÅÅÅÅ DD/MM/ÅÅÅÅ Antall dager Sletter utløpte kuponger automatisk (Kun de som er opprettet av denne plugin). Slett utløpte rabattkuponger Sletting av utløpte rabattkuponger pågår... E-post innhold E-post emne E-post mal E-post format Aktiver Mandrill Aktiver "Kupong e-post system" Aktiver rabattkupong for XX antall dager siden siste kjøp Aktiver rabattkupong for kunders fødselsdag Aktiver rabattkupong for første kjøp Aktiver rabattkupong for XX antall kjøp Aktiver rabattkupong for et spesifikt produktkjøp Aktiver rabattkupong for XX beløp brukt Aktiver rabattkupong for brukerregistrering Aktiver sending av kupong Utvide Rabattkupongen er gyldig til: %s Følgende produkter: %s Fraktfritt levert Generelle innstillinger HTML Gratulere med dagen fra {site_title} Gratulere med dagen fra {vendor_name} hos {site_title} Hei {customer_name}!

Takk, for at du valgte å handle hos oss den {order_date}!
Som en ekstra påskjønnelse ønsker vi å tilby deg denne rabattkupongen:

{coupon_description}

Med vennlig hilsen,
<strong>{site_title}</strong> Hei {customer_name}!

Takk, for at du valgte å handle hos oss den {order_date}!
Som en ekstra påskjønnelse ønsker vi å tilby deg denne rabattkupongen:

{coupon_description}

Med vennlig hilsen,
<strong>{vendor_name}</strong> Hei {customer_name}!

Som takk for ditt kjøp av {purchased_product} den {order_date} ønsker vi å gi deg en hyggelig rabatt på ditt neste kjøp hos oss:

{coupon_description}

Med vennlig hilsen,
<strong>{site_title}</strong> Hei {customer_name}!

Som takk for ditt kjøp av {purchased_product} den {order_date} ønsker vi å gi deg en hyggelig rabatt på ditt neste kjøp hos oss:

{coupon_description}

Med vennlig hilsen,
<strong>{vendor_name}</strong> Hei {customer_name}!

Takk for at du valgte å registrere deg som kunde hos oss!
Vi ønsker å tilby deg denne rabattkupongen som en liten velkomstgave:

{coupon_description}

Med vennlig hilsen,
<strong>{site_title}</strong> Hei {customer_name}!

Takk for at du valgte å registrere deg som kunde hos oss!
Vi ønsker å tilby deg denne rabattkupongen som en liten velkomstgave:

{coupon_description}

Med vennlig hilsen,
<strong>{vendor_name}</strong> Hei {customer_name}!

Vi sender deg våre beste gratulasjoner til din bursdag!
Vennligst, ta imot denne rabattkupongen som en liten gave fra oss:

{coupon_description}

Med vennlig hilsen,
<strong>{site_title}</strong> Hei {customer_name}!

Vi sender deg våre beste gratulasjoner til din bursdag!
Vennligst, ta imot denne rabattkupongen som en liten gave fra oss:

{coupon_description}

Med vennlig hilsen,
<strong>{vendor_name}</strong> Hei {customer_name}!

Med din siste ordre fra {order_date}, har du nå handlet for over {customer_money_spent}!
Vi ønsker derfor å gi deg en hyggelig rabatt på ditt neste kjøp hos oss:

{coupon_description}

Med vennlig hilsen,
<strong>{site_title}</strong> Hei {customer_name}!

Med din siste ordre fra {order_date}, har du nå handlet for over {customer_money_spent}!
Vi ønsker derfor å gi deg en hyggelig rabatt på ditt neste kjøp hos oss:

{coupon_description}

Med vennlig hilsen,
<strong>{vendor_name}</strong> Hei {customer_name}!

Med din siste ordre fra {order_date}, har du nå totalt {purchases_threshold} registrerte ordrer!
Vi ønsker derfor å gi deg en hyggelig rabatt på ditt neste kjøp hos oss:

{coupon_description}

Med vennlig hilsen,
<strong>{site_title}</strong> Hei {customer_name}!

Med din siste ordre fra {order_date}, har du nå totalt {purchases_threshold} registrerte ordrer!
Vi ønsker derfor å gi deg en hyggelig rabatt på ditt neste kjøp hos oss:

{coupon_description}

Med vennlig hilsen,
<strong>{vendor_name}</strong> Hei {customer_name}!

Det er nå over {days_ago} dager siden du sist handlet hos oss. Vi ønsker å se deg tilbake og gir deg derfor en hyggelig rabatt på ditt neste kjøp hos oss:

{coupon_description}

Med vennlig hilsen,
<strong>{site_title}</strong> Hei {customer_name}!

Det er nå over {days_ago} dager siden du sist handlet hos oss. Vi ønsker å se deg tilbake og gir deg derfor en hyggelig rabatt på ditt neste kjøp hos oss:

{coupon_description}

Med vennlig hilsen,
<strong>{vendor_name}</strong> Hvordan For å kunne bruke noen av funksjonene i "Kupong e-post systemet" må du opprette minst én kupong MM-DD-ÅÅÅÅ MM/DD/ÅÅÅÅ Mandrill API-nøkkel Mandrill innstillinger Mer informasjon På et bestemt antall dager fra siste kjøp På kunders fødselsdag På første kjøp På spesifikke ordreterskler På spesifikt produktkjøp På spesifikke beløpsterskler På brukerregistrering Oppdraget fullført. %d kupong ble slettet. Oppdraget fullført. %d kuponger ble slettet. Ordreterskler Plassholder referanse Ren tekst Vennligst skriv inn din Mandrill API-nøkkel for "Kupong e-post systemet" Vennligst skriv inn en gyldig e-postadresse Vennligst, velg minst ett produkt Vennligst spesifiser antall dager Plugin dokumentasjon Premium versjon Produkter i følgende kategorier: %s Produkter som vil utløse utsendelse av rabattkupong Fjern valgte terskel Hvor mye penger som er brukt av kunden Kundens e-postadresse Kundens etternavn Kundens navn Dato for ordre Beskrivelse av rabattkupongen. Denne plassholder må inkluderes. Navn på det kjøpte produktet Navn på leverandøren Antall dager siden siste kjøp Antall kjøp Nettstedets tittel Hvor mye penger som er brukt Søk i produkter&hellip; Velg en kupong Valgt kupong Send testmelding Sender testmelding... Innstillinger Målprodukter Målverdi Mal #1 Mal #2 Mal #3 Test e-post Testmeldingen ble sendt! Antall dager som må passere etter at siste ordren er satt som "Fullført" Det oppstod en feil under sending av e-posten Denne rabattkupongen kan ikke brukes sammen med andre rabattkuponger. Denne rabattkupongen gjelder ikke tilbudsvarer. Skriv inn en e-postadresse for mottak av testmeldingen Bruk Mandrill til å sende e-poster Bruk de innebygde e-post malene Gyldig for et maksimum kjøp på %s Gyldig for et minimum kjøp på %s Gyldig for et minimum kjøp på %s og et maksimum kjøp på %s Leverandør innstillinger WooCommerce standard mal YITH WooCommerce Coupon Email System er aktivert, men ikke trådt i kraft. Dette plugin krever WooCommerce for å fungere. YITH WooCommerce Kupong e-post system sender ut rabattkuponger til dine kunder automatisk og i henhold til forhåndsdefinerte hendelser. ÅÅÅÅ-MM-DD ÅÅÅÅ/MM/DD Du har mottatt en rabattkupong fra {site_title} Du har mottatt en rabattkupong fra {vendor_name} hos {site_title} Du har mottatt en rabattkupong fra {site_title} Du har mottatt en rabattkupong fra {vendor_name} hos {site_title} Du må velge en rabattkupong for utsendelse ved første kjøp. Du må velge en rabattkupong for utsendelse ved ny brukerregistrering. Du må velge minst ett produkt for å sende en rabattkupong når produktet er kjøpt. Du må angi beløp/prosentsats av en rabattkupong for å kunne sende kupongen i henhold til et bestemt antall dager etter den siste ordren. Du må angi beløp/prosentsats av en rabattkupong for å kunne sende kupongen i henhold til en brukers fødselsdag. Du må angi beløp/prosentsats av en rabattkupong for å kunne sende kupongen i henhold til et spesifikt produkt. Du må angi beløp/prosentsats av en rabattkupong for å kunne sende kupongen i en testmelding Du må velge en rabattkupong for hver terskel, sendes når brukerne når et bestemt antall kjøp. Du må velge en rabattkupong for hver terskel, sendes når brukerne når et bestemt beløp brukt til kjøp. Du må angi en terskelverdi for å sende en rabattkupong når en bruker når et bestemt antall kjøp. Du må angi en terskelverdi for å sende en rabattkupong når en bruker når et bestemt beløp brukt til kjøp. Du må angi minst èn terskelverdi for å kunne sende en testmelding 