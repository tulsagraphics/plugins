<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! function_exists( 'yfwp_get_minified' ) ) {

	/**
	 * Get minified file suffix
	 *
	 * @since   1.0.0
	 * @return  string
	 * @author  Alberto Ruggiero
	 */
	function yfwp_get_minified() {

		return defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

	}

}

if ( ! function_exists( 'ywfp_get_option' ) ) {

	/**
	 * Get plugin option
	 *
	 * @since   1.0.0
	 *
	 * @param   $option
	 * @param   $default
	 *
	 * @return  mixed
	 * @author  Alberto Ruggiero
	 */
	function ywfp_get_option( $option, $default = false ) {
		return YITH_FAQ_Settings::get_instance()->get_option( 'faq', $option, $default );
	}

}