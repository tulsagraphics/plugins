=== WooCommerce Product Reviews Pro ===
Author: skyverge
Tags: woocommerce
Requires at least: 4.4
Tested up to: 4.9.7

See http://docs.woothemes.com/document/woocommerce-product-reviews-pro/ for full documentation.

== Installation ==

1. Upload the entire 'wocoommerce-product-reviews-pro' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
