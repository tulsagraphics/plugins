/**
 * Script for settings screen
 */
(function ($) {

	/**
	 * Scanning Method
	 */
	$('select#wpmc_method').on('change', function (ev) {
		var selected = $(this).val();
		if (selected == 'media') { // Method = "Media Library"
			$('input#wpmc_media_library').attr('disabled', true);
		} else { // Method = Other Else
			$('input#wpmc_media_library').attr('disabled', false);
		}
	});

})(jQuery);
