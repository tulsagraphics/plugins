<?php
/**
 * Template options in WC Product Panel
 *
 * @author  Yithemes
 * @package YITH Booking for WooCommerce Premium
 * @version 1.0.0
 */

!defined( 'YITH_WCBK' ) && exit; // Exit if accessed directly


$printer      = YITH_WCBK_Printer();
$months_array = yith_wcbk_get_months_array();
$days_array   = yith_wcbk_get_days_array();
/**
 * @var WC_Product_Booking $product
 */
$product             = wc_get_product( $post_id );
$availability_ranges = yith_wcbk_is_booking_product($product) ? $product->get_availability_ranges() : array();

?>

<div class="options_group">

    <table id="yith-wcbk-booking-availability-table" class="yith-wcbk-booking-settings-table">
        <tr class="yith-wcbk-availability-row yith-wcbk-availability-default-row">
            <td class="yith-wcbk-availability-anchor">
                <?php
                $printer->print_field( array(
                                           'type'  => 'dashicon',
                                           'class' => 'dashicons-menu yith-wcbk-anchor',
                                       ) );
                ?>
            </td>
            <td class="yith-wcbk-availability-range-column">
                <div class="yith-wcbk-availability-range-column-ranges">
                    <div class="yith-wcbk-availability-range-column-ranges-date">

                        <?php
                        $printer->print_field( array(
                                                   'type'  => 'hidden',
                                                   'name'  => '_yith_booking_availability_range[type][]',
                                                   'value' => 'custom',
                                               ) );
                        $printer->print_field( array(
                                                   'type'  => 'text',
                                                   'class' => 'yith-wcbk-date-input-field',
                                                   'title' => __( 'From', 'yith-booking-for-woocommerce' ),
                                                   'name'  => '_yith_booking_availability_range[from][]',

                                               ) );
                        $printer->print_field( array(
                                                   'type'  => 'text',
                                                   'class' => 'yith-wcbk-date-input-field',
                                                   'title' => __( 'To', 'yith-booking-for-woocommerce' ),
                                                   'name'  => '_yith_booking_availability_range[to][]',

                                               ) );
                        ?>

                    </div>
                    <div class="yith-wcbk-availability-range-column-ranges-month">
                        <?php
                        $printer->print_field( array(
                                                   'type'              => 'hidden',
                                                   'name'              => '_yith_booking_availability_range[type][]',
                                                   'value'             => 'month',
                                                   'custom_attributes' => 'disabled="disabled"',

                                               ) );
                        $printer->print_field( array(
                                                   'type'              => 'select',
                                                   'class'             => 'yith-wcbk-month-range-select',
                                                   'title'             => __( 'From', 'yith-booking-for-woocommerce' ),
                                                   'name'              => '_yith_booking_availability_range[from][]',
                                                   'options'           => $months_array,
                                                   'custom_attributes' => 'disabled="disabled"',
                                               ) );
                        $printer->print_field( array(
                                                   'type'              => 'select',
                                                   'class'             => 'yith-wcbk-month-range-select',
                                                   'title'             => __( 'To', 'yith-booking-for-woocommerce' ),
                                                   'name'              => '_yith_booking_availability_range[to][]',
                                                   'options'           => $months_array,
                                                   'custom_attributes' => 'disabled="disabled"',
                                               ) );
                        ?>
                    </div>

                    <?php

                    $printer->print_field( array(
                                               'type'            => 'onoff',
                                               'container_class' => 'yith-wcbk-bookable-onoff',
                                               'enabled_class'   => 'yith-wcbk-bookable-onoff-enabled',
                                               'disabled_class'  => 'yith-wcbk-bookable-onoff-disabled',
                                               'enabled_text'    => __( 'Bookable', 'yith-booking-for-woocommerce' ),
                                               'disabled_text'   => __( 'Non-bookable', 'yith-booking-for-woocommerce' ),
                                               'name'            => '_yith_booking_availability_range[bookable][]',
                                               'value'           => 'yes',
                                           ) );
                    ?>
                </div>
                <div class="yith-wcbk-availability-range-column-days hidden yith-wcbk-hide-times">

                    <div class="yith-wcbk-availability-anchors">
                        <div class="yith-wcbk-availability-time-enabled-anchor bk_show_if_time">
                            <span class="yith-wcbk-availability-time-enabled-anchor__icon dashicons  dashicons-clock"></span>
                            <span class="yith-wcbk-availability-time-enabled-anchor__text"><?php _e( 'Show/Hide Time', 'yith-booking-for-woocommerce' ); ?></span>
                        </div>
                    </div>
                    <?php
                    $printer->print_field( array(
                                               'type'  => 'hidden',
                                               'class' => 'yith-booking-availability-times-enabled',
                                               'name'  => '_yith_booking_availability_range[times_enabled][]',
                                               'value' => 'no',
                                           ) );


                    $printer->print_field( array(
                                               'type'  => 'hidden',
                                               'class' => 'yith-booking-global-availability-days-enabled',
                                               'name'  => '_yith_booking_availability_range[days_enabled][]',
                                               'value' => 'no',
                                           ) );
                    foreach ( $days_array as $day_number => $day_name ) {
                        $day_name = substr( $day_name, 0, 3 );

                        $printer->print_field(
                            array(
                                'type'             => 'section',
                                'section_html_tag' => 'div',
                                'class'            => 'yith-wcbk-availability-range-day-container',
                                'fields'           => array(
                                    array(
                                        'type'            => 'onoff-advanced',
                                        'container_class' => 'yith-wcbk-day-onoff-advanced',
                                        'options'         => array(
                                            'yes'      => array(
                                                'class' => 'yith-wcbk-day-onoff-advanced-yes'
                                            ),
                                            'no'       => array(
                                                'class' => 'yith-wcbk-day-onoff-advanced-no'
                                            ),
                                            'disabled' => array(
                                                'class' => 'yith-wcbk-day-onoff-advanced-disabled'
                                            ),
                                        ),
                                        'default_text'    => $day_name,
                                        'name'            => '_yith_booking_availability_range[days][' . $day_number . '][]',
                                        'value'           => 'yes',
                                    ),
                                    array(
                                        'type' => 'time-select',
                                        'name' => '_yith_booking_availability_range[day_time_from][' . $day_number . '][]',
                                    ),
                                    array(
                                        'type' => 'time-select',
                                        'name' => '_yith_booking_availability_range[day_time_to][' . $day_number . '][]',
                                    )
                                )
                            )
                        );
                    }
                    ?>
                </div>
            </td>
            <td class="yith-wcbk-availability-actions-column">
                <span class="dashicons dashicons-screenoptions yith-wcbk-availability-toggle-visible"></span>
                <?php
                $printer->print_field( array(
                                           'type'  => 'dashicon',
                                           'class' => 'dashicons-no-alt yith-wcbk-delete',
                                       ) );
                ?>
            </td>
        </tr>
        <?php if ( $availability_ranges && is_array( $availability_ranges ) ) : ?>
            <?php foreach ( $availability_ranges as $availability_range ): ?>
                <tr class="yith-wcbk-availability-row ">
                    <td class="yith-wcbk-availability-anchor">
                        <?php
                        $printer->print_field( array(
                                                   'type'  => 'dashicon',
                                                   'class' => 'dashicons-menu yith-wcbk-anchor',
                                               ) );
                        ?>
                    </td>
                    <td class="yith-wcbk-availability-range-column">
                        <div class="yith-wcbk-availability-range-column-ranges">

                            <?php if ( 'custom' === $availability_range->get_type() ) : ?>
                                <div class="yith-wcbk-availability-range-column-ranges-date">

                                    <?php
                                    $printer->print_field( array(
                                                               'type'  => 'hidden',
                                                               'name'  => '_yith_booking_availability_range[type][]',
                                                               'value' => 'custom',
                                                           ) );
                                    $printer->print_field( array(
                                                               'type'  => 'text',
                                                               'class' => 'yith-wcbk-date-input-field yith-wcbk-admin-date-picker',
                                                               'title' => __( 'From', 'yith-booking-for-woocommerce' ),
                                                               'name'  => '_yith_booking_availability_range[from][]',
                                                               'value' => $availability_range->get_from(),

                                                           ) );
                                    $printer->print_field( array(
                                                               'type'  => 'text',
                                                               'class' => 'yith-wcbk-date-input-field yith-wcbk-admin-date-picker',
                                                               'title' => __( 'To', 'yith-booking-for-woocommerce' ),
                                                               'name'  => '_yith_booking_availability_range[to][]',
                                                               'value' => $availability_range->get_to(),
                                                           ) );
                                    ?>
                                </div>
                            <?php elseif ( 'month' === $availability_range->get_type() ) : ?>
                                <div class="yith-wcbk-availability-range-column-ranges-month">
                                    <?php
                                    $printer->print_field( array(
                                                               'type'  => 'hidden',
                                                               'name'  => '_yith_booking_availability_range[type][]',
                                                               'value' => 'month',
                                                           ) );
                                    $printer->print_field( array(
                                                               'type'    => 'select',
                                                               'class'   => 'yith-wcbk-month-range-select',
                                                               'title'   => __( 'From', 'yith-booking-for-woocommerce' ),
                                                               'name'    => '_yith_booking_availability_range[from][]',
                                                               'options' => $months_array,
                                                               'value'   => $availability_range->get_from(),
                                                           ) );
                                    $printer->print_field( array(
                                                               'type'    => 'select',
                                                               'class'   => 'yith-wcbk-month-range-select',
                                                               'title'   => __( 'To', 'yith-booking-for-woocommerce' ),
                                                               'name'    => '_yith_booking_availability_range[to][]',
                                                               'options' => $months_array,
                                                               'value'   => $availability_range->get_to(),
                                                           ) );
                                    ?>
                                </div>
                            <?php endif; ?>

                            <?php

                            $printer->print_field( array(
                                                       'type'            => 'onoff',
                                                       'container_class' => 'yith-wcbk-bookable-onoff',
                                                       'enabled_class'   => 'yith-wcbk-bookable-onoff-enabled',
                                                       'disabled_class'  => 'yith-wcbk-bookable-onoff-disabled',
                                                       'enabled_text'    => __( 'Bookable', 'yith-booking-for-woocommerce' ),
                                                       'disabled_text'   => __( 'Non-bookable', 'yith-booking-for-woocommerce' ),
                                                       'name'            => '_yith_booking_availability_range[bookable][]',
                                                       'value'           => $availability_range->get_bookable(),
                                                   ) );
                            ?>
                        </div>

                        <div class="yith-wcbk-availability-range-column-days <?php echo !$availability_range->has_days_enabled() ? 'hidden' : '' ?> <?php echo !$availability_range->has_times_enabled() ? 'yith-wcbk-hide-times' : '' ?>">

                            <div class="yith-wcbk-availability-anchors">
                                <div class="yith-wcbk-availability-time-enabled-anchor bk_show_if_time">
                                    <span class="yith-wcbk-availability-time-enabled-anchor__icon dashicons  dashicons-clock"></span>
                                    <span class="yith-wcbk-availability-time-enabled-anchor__text"><?php _e( 'Show/Hide Time', 'yith-booking-for-woocommerce' ); ?></span>
                                </div>
                            </div>
                            <?php
                            $printer->print_field( array(
                                                       'type'  => 'hidden',
                                                       'class' => 'yith-booking-availability-times-enabled',
                                                       'name'  => '_yith_booking_availability_range[times_enabled][]',
                                                       'value' => $availability_range->get_times_enabled(),
                                                   ) );


                            $printer->print_field( array(
                                                       'type'  => 'hidden',
                                                       'class' => 'yith-booking-global-availability-days-enabled',
                                                       'name'  => '_yith_booking_availability_range[days_enabled][]',
                                                       'value' => $availability_range->get_days_enabled(),
                                                   ) );

                            foreach ( $days_array as $day_number => $day_name ) {
                                $days                = $availability_range->get_days();
                                $value               = $availability_range->has_days_enabled() && isset( $days[ $day_number ] ) ? $days[ $day_number ] : $availability_range->get_bookable();
                                $day_name            = substr( $day_name, 0, 3 );

                                $day_time_from_array = $availability_range->get_day_time_from();
                                $day_time_to_array   = $availability_range->get_day_time_to();
                                $day_time_from       = $availability_range->has_times_enabled() && isset( $day_time_from_array[ $day_number ] ) ? $day_time_from_array[ $day_number ] : '00:00';
                                $day_time_to         = $availability_range->has_times_enabled() && isset( $day_time_to_array[ $day_number ] ) ? $day_time_to_array[ $day_number ] : '00:00';

                                $printer->print_field(
                                    array(
                                        'type'             => 'section',
                                        'section_html_tag' => 'div',
                                        'class'            => 'yith-wcbk-availability-range-day-container',
                                        'fields'           => array(
                                            array(
                                                'type'            => 'onoff-advanced',
                                                'container_class' => 'yith-wcbk-day-onoff-advanced',
                                                'options'         => array(
                                                    'yes'      => array(
                                                        'class' => 'yith-wcbk-day-onoff-advanced-yes'
                                                    ),
                                                    'no'       => array(
                                                        'class' => 'yith-wcbk-day-onoff-advanced-no'
                                                    ),
                                                    'disabled' => array(
                                                        'class' => 'yith-wcbk-day-onoff-advanced-disabled'
                                                    ),
                                                ),
                                                'default_text'    => $day_name,
                                                'name'            => '_yith_booking_availability_range[days][' . $day_number . '][]',
                                                'value'           => $value,
                                            ),
                                            array(
                                                'type'  => 'time-select',
                                                'name'  => '_yith_booking_availability_range[day_time_from][' . $day_number . '][]',
                                                'value' => $day_time_from,
                                            ),
                                            array(
                                                'type'  => 'time-select',
                                                'name'  => '_yith_booking_availability_range[day_time_to][' . $day_number . '][]',
                                                'value' => $day_time_to,
                                            )
                                        )
                                    )
                                );
                            }
                            ?>
                        </div>

                    </td>
                    <td class="yith-wcbk-availability-actions-column">
                        <span class="dashicons dashicons-screenoptions yith-wcbk-availability-toggle-visible"></span>
                        <?php
                        $printer->print_field( array(
                                                   'type'  => 'dashicon',
                                                   'class' => 'dashicons-no-alt yith-wcbk-delete',
                                               ) );
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <div class="yith-wcbk-bottom-actions yith-wcbk-right">
        <input type="button" id="yith-wcbk-availability-add-date-range" class="button"
               value="<?php _e( 'Add Date Range', 'yith-booking-for-woocommerce' ); ?>">
        <input type="button" id="yith-wcbk-availability-add-month-range" class="button"
               value="<?php _e( 'Add Month Range', 'yith-booking-for-woocommerce' ); ?>">
        <?php
        $global_availability      = __( 'Global Availability', 'yith-booking-for-woocommerce' );
        $global_availability_link = add_query_arg( array(
                                                       'page' => 'yith_wcbk_panel',
                                                       'tab'  => 'availability',
                                                   ), admin_url( 'admin.php' ) );
        $global_availability_html = "<a href='$global_availability_link' target='_blank'>$global_availability</a>";
        $note                     = sprintf( _x( 'Note: setting availability in this booking product will override %s settings', 'Note: setting availability in this booking product will override Global Availability settings', 'yith-booking-for-woocommerce' ), $global_availability_html );
        $note                     = "<span class='yith-wcbk-desc'>$note</span>";
        echo $note;
        ?>
    </div>
</div>