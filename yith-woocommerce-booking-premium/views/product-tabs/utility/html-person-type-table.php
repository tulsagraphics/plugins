<?php
/**
 * Template options in WC Product Panel
 *
 * @author  Yithemes
 * @package YITH Booking for WooCommerce Premium
 * @version 1.0.0
 */

!defined( 'YITH_WCBK' ) && exit; // Exit if accessed directly

$printer = YITH_WCBK_Printer();
?>
<table id="yith-wcbk-booking-person-type-table"
       class="yith-wcbk-booking-settings-table yith-wcbk-booking-settings-table-full-width-fields"
       data-fields-container-id="yith-wcbk-person-type-hidden-utils-fields">
    <tr>
        <th></th>
        <th></th>
        <th><?php _e( 'Person Type', 'yith-booking-for-woocommerce' ) ?></th>
        <th><?php _e( 'Min', 'yith-booking-for-woocommerce' ) ?></th>
        <th><?php _e( 'Max', 'yith-booking-for-woocommerce' ) ?></th>
        <th><?php _e( 'Base price', 'yith-booking-for-woocommerce' ) ?></th>
        <th><?php _e( 'Extra cost per unit', 'yith-booking-for-woocommerce' ) ?></th>
    </tr>

    <?php
    $my_person_types     = get_post_meta( $post_id, '_yith_booking_person_types', true );
    $my_person_types     = !!( $my_person_types ) ? $my_person_types : array();
    $all_person_type_ids = YITH_WCBK()->person_type_helper->get_person_type_ids();


    $my_person_type_ids = array_keys( $my_person_types );

    $person_type_id_intersect = array_intersect( $my_person_type_ids, $all_person_type_ids );
    $person_type_id_diff      = array_diff( $all_person_type_ids, $person_type_id_intersect );

    $all_person_type_ids = array_merge( $person_type_id_intersect, $person_type_id_diff );


    if ( !empty( $all_person_type_ids ) ) {
        foreach ( $all_person_type_ids as $person_type_id ) {
            $default_args = array(
                'enabled'    => 'no',
                'id'         => $person_type_id,
                'min'        => 0,
                'max'        => 0,
                'base_cost'  => '',
                'block_cost' => '',
            );

            $args = $default_args;

            if ( isset( $my_person_types[ $person_type_id ] ) ) {
                $args = wp_parse_args( $my_person_types[ $person_type_id ], $args );
            }

            /**
             * @var string $enabled
             * @var int    $id
             * @var int    $min
             * @var int    $max
             * @var int    $base_cost
             * @var int    $block_cost
             */
            extract( $args );

            ?>

            <tr>
                <td class="yith-wcbk-person-type-anchor">
                    <?php
                    $printer->print_field( array(
                                               'type'  => 'dashicon',
                                               'class' => 'dashicons-menu yith-wcbk-anchor'
                                           ) );
                    ?>
                </td>

                <td class="yith-wcbk-person-type-enabled">
                    <?php
                    $printer->print_field( array(
                                               'type'  => 'onoff',
                                               'id'    => 'yith_booking_person_type-enabled-' . $id,
                                               'name'  => "_yith_booking_person_types[$id][enabled]",
                                               'value' => $enabled === 'yes' ? 'yes' : 'no'
                                           ) );
                    ?>
                </td>


                <td class="yith-wcbk-person-type-name">
                    <?php
                    $printer->print_field( array(
                                               'type'  => 'html',
                                               'value' => '<span>' . get_the_title( $id ) . '</span>',
                                           ) );
                    $printer->print_field( array(
                                               'type'  => 'hidden',
                                               'name'  => "_yith_booking_person_types[$id][id]",
                                               'value' => $id,
                                           ) );
                    ?>
                </td>

                <td class="yith-wcbk-person-type-min">
                    <?php
                    $printer->print_field( array(
                                               'type'              => 'number',
                                               'custom_attributes' => 'step="1" min="0"',
                                               'name'              => "_yith_booking_person_types[$id][min]",
                                               'value'             => $min,
                                           ) );
                    ?>
                </td>

                <td class="yith-wcbk-person-type-max">
                    <?php
                    $printer->print_field( array(
                                               'type'              => 'number',
                                               'custom_attributes' => 'step="1" min="0"',
                                               'name'              => "_yith_booking_person_types[$id][max]",
                                               'value'             => $max,
                                           ) );
                    ?>
                </td>

                <td class="yith-wcbk-person-type-base-cost">
                    <?php
                    $printer->print_field( array(
                                               'type'  => 'text',
                                               'name'  => "_yith_booking_person_types[$id][base_cost]",
                                               'class' => 'wc_input_price',
                                               'value' => wc_format_localized_price( $base_cost ),
                                           ) );
                    ?>
                </td>

                <td class="yith-wcbk-person-type-block-cost">
                    <?php
                    $printer->print_field( array(
                                               'type'  => 'text',
                                               'name'  => "_yith_booking_person_types[$id][block_cost]",
                                               'class' => 'wc_input_price',
                                               'value' => wc_format_localized_price( $block_cost ),
                                           ) );
                    ?>
                </td>
            </tr>


            <?php
        }
    }
    ?>
</table>

<div class="yith-wcbk-bottom-actions yith-wcbk-right">
    <?php
    $new_person_type_text = __( 'Add New Type', 'yith-booking-for-woocommerce' );
    $new_person_type_link = add_query_arg( array(
                                               'post_type' => YITH_WCBK_Post_Types::$person_type,
                                           ), admin_url( 'post-new.php' ) );
    $note                 = "<a href='$new_person_type_link' target='_blank'>$new_person_type_text</a>";
    echo $note;
    ?>
</div>
