<?php
/**
 * Template options in WC Product Panel
 *
 * @author  Yithemes
 * @package YITH Booking for WooCommerce Premium
 * @version 1.0.0
 */

!defined( 'YITH_WCBK' ) && exit; // Exit if accessed directly

$printer      = YITH_WCBK_Printer();
$months_array = yith_wcbk_get_months_array();
$weeks_array  = yith_wcbk_get_weeks_array();
$days_array   = yith_wcbk_get_days_array();

$person_type_ids     = YITH_WCBK()->person_type_helper->get_person_type_ids();
$person_type_options = array();
if ( !!$person_type_ids ) {
    foreach ( $person_type_ids as $person_type_id ) {
        $option_id                         = 'person-type-' . $person_type_id;
        $person_type_options[ $option_id ] = YITH_WCBK()->person_type_helper->get_person_type_title( $person_type_id );
    }
}

$costs_range_types = array(
    'custom' => __( 'Custom date range', 'yith-booking-for-woocommerce' ),
    'month'  => __( 'Range of months', 'yith-booking-for-woocommerce' ),
    'week'   => __( 'Range of weeks', 'yith-booking-for-woocommerce' ),
    'day'    => __( 'Range of days', 'yith-booking-for-woocommerce' ),
    'person' => __( 'People count', 'yith-booking-for-woocommerce' ),
    'block'  => __( 'Unit count', 'yith-booking-for-woocommerce' ),
    /*'group'  => array(
        'title'   => __( 'Time ranges', 'yith-booking-for-woocommerce' ),
        'options' => array(
            'all-week' => __( 'All week', 'yith-booking-for-woocommerce' ),
            'day-1'    => __( 'Monday', 'yith-booking-for-woocommerce' ),
            'day-2'    => __( 'Tuesday', 'yith-booking-for-woocommerce' ),
            'day-3'    => __( 'Wednesday', 'yith-booking-for-woocommerce' ),
            'day-4'    => __( 'Thursday', 'yith-booking-for-woocommerce' ),
            'day-5'    => __( 'Friday', 'yith-booking-for-woocommerce' ),
            'day-6'    => __( 'Saturday', 'yith-booking-for-woocommerce' ),
            'day-7'    => __( 'Sunday', 'yith-booking-for-woocommerce' ),
        )
    )*/
);

if ( !!$person_type_options ) {
    $costs_range_types[ 'group-person' ] = array(
        'title'   => __( 'People', 'yith-booking-for-woocommerce' ),
        'options' => $person_type_options
    );
}

$costs_operators = array(
    'add' => '+',
    'sub' => '-',
    'mul' => '*',
    'div' => '/',
);

?>

<div>
    <table id="yith-wcbk-booking-costs-table"
           class="yith-wcbk-booking-settings-table yith-wcbk-booking-settings-table-full-width-fields"
           data-fields-container-id="yith-wcbk-costs-hidden-utils-fields">
        <tr>
            <th></th>
            <th><?php _e( 'Range Type', 'yith-booking-for-woocommerce' ) ?></th>
            <th><?php _e( 'From', 'yith-booking-for-woocommerce' ) ?></th>
            <th><?php _e( 'To', 'yith-booking-for-woocommerce' ) ?></th>
            <th colspan="2"><?php _e( 'Base price', 'yith-booking-for-woocommerce' ) ?></th>
            <th colspan="2"><?php _e( 'Extra cost per unit', 'yith-booking-for-woocommerce' ) ?></th>
            <th></th>
        </tr>

        <tr class="yith-wcbk-costs-default-row">
            <td class="yith-wcbk-costs-anchor">
                <?php
                $printer->print_field( array(
                                           'type'  => 'dashicon',
                                           'class' => 'dashicons-menu yith-wcbk-anchor'
                                       ) );
                ?>
            </td>


            <td class="yith-wcbk-costs-range-type">
                <?php $printer->print_field( array(
                                                 'type'    => 'select',
                                                 'class'   => 'yith-wcbk-costs-range-type-select',
                                                 'name'    => '_yith_booking_costs_range[type][]',
                                                 'options' => $costs_range_types,
                                             ) ) ?>
            </td>

            <td class="yith-wcbk-costs-from">
                <?php
                $printer->print_field( array(
                                           'type' => 'text',
                                           'name' => '_yith_booking_costs_range[from][]',
                                       ) );
                ?>
            </td>

            <td class="yith-wcbk-costs-to">
                <?php
                $printer->print_field( array(
                                           'type' => 'text',
                                           'name' => '_yith_booking_costs_range[to][]',
                                       ) );
                ?>
            </td>

            <td class="yith-wcbk-costs-base-cost-operator">
                <?php
                $printer->print_field( array(
                                           'type'    => 'select',
                                           'name'    => '_yith_booking_costs_range[base_cost_operator][]',
                                           'options' => $costs_operators
                                       ) );
                ?>
            </td>

            <td class="yith-wcbk-costs-base-cost">
                <?php
                $printer->print_field( array(
                                           'type' => 'text',
                                           'name' => '_yith_booking_costs_range[base_cost][]',
                                       ) );
                ?>
            </td>

            <td class="yith-wcbk-costs-block-cost-operator">
                <?php
                $printer->print_field( array(
                                           'type'    => 'select',
                                           'name'    => '_yith_booking_costs_range[block_cost_operator][]',
                                           'options' => $costs_operators
                                       ) );
                ?>
            </td>

            <td class="yith-wcbk-costs-block-cost">
                <?php
                $printer->print_field( array(
                                           'type' => 'text',
                                           'name' => '_yith_booking_costs_range[block_cost][]',
                                       ) );
                ?>
            </td>

            <td class="yith-wcbk-costs-delete">
                <?php
                $printer->print_field( array(
                                           'type'  => 'dashicon',
                                           'class' => 'dashicons-no-alt yith-wcbk-delete'
                                       ) );
                ?>
            </td>
        </tr>

        <?php
        $costs_range = get_post_meta( $post_id, '_yith_booking_costs_range', true );

        if ( !empty( $costs_range ) ) {
            foreach ( $costs_range as $key => $single_range ) {
                $type                = !empty( $single_range[ 'type' ] ) ? $single_range[ 'type' ] : 'custom';
                $from                = !empty( $single_range[ 'from' ] ) ? $single_range[ 'from' ] : '';
                $to                  = !empty( $single_range[ 'to' ] ) ? $single_range[ 'to' ] : '';
                $base_cost_operator  = !empty( $single_range[ 'base_cost_operator' ] ) ? $single_range[ 'base_cost_operator' ] : 'add';
                $base_cost           = !empty( $single_range[ 'base_cost' ] ) ? wc_format_localized_price( $single_range[ 'base_cost' ] ) : '';
                $block_cost_operator = !empty( $single_range[ 'block_cost_operator' ] ) ? $single_range[ 'block_cost_operator' ] : 'add';
                $block_cost          = !empty( $single_range[ 'block_cost' ] ) ? wc_format_localized_price( $single_range[ 'block_cost' ] ) : '';
                ?>
                <tr>
                    <td class="yith-wcbk-costs-anchor">
                        <?php
                        $printer->print_field( array(
                                                   'type'  => 'dashicon',
                                                   'class' => 'dashicons-menu yith-wcbk-anchor'
                                               ) );
                        ?>
                    </td>


                    <td class="yith-wcbk-costs-range-type">
                        <?php $printer->print_field( array(
                                                         'type'    => 'select',
                                                         'class'   => 'yith-wcbk-costs-range-type-select',
                                                         'name'    => '_yith_booking_costs_range[type][]',
                                                         'options' => $costs_range_types,
                                                         'value'   => $type
                                                     ) ) ?>
                    </td>

                    <?php
                    $args_from_to = array();
                    switch ( $type ) {
                        case 'custom':
                            $args_from_to = array(
                                'type'  => 'text',
                                'class' => 'yith-wcbk-admin-input-range yith-wcbk-admin-date-picker',
                            );
                            break;

                        case 'month':
                            $args_from_to = array(
                                'type'    => 'select',
                                'class'   => 'yith-wcbk-month-range-select',
                                'options' => $months_array
                            );
                            break;
                        case 'week':
                            $args_from_to = array(
                                'type'    => 'select',
                                'class'   => 'yith-wcbk-week-range-select',
                                'options' => $weeks_array
                            );
                            break;
                        case 'day':
                            $args_from_to = array(
                                'type'    => 'select',
                                'class'   => 'yith-wcbk-day-range-select',
                                'options' => $days_array
                            );
                            break;
                        case 'time':
                            $args_from_to = array(
                                'type'              => 'text',
                                'class'             => 'yith-wcbk-admin-input-range yith-wcbk-admin-time-picker',
                                'custom_attributes' => 'pattern="([01]?[0-9]|2[0-3])(:[0-5][0-9])" placeholder="hh:mm"'
                            );
                            break;
                        default:
                            $args_from_to = array(
                                'type'              => 'number',
                                'class'             => 'yith-wcbk-number-range',
                                'custom_attributes' => 'min="0"'
                            );
                            break;

                    }
                    ?>


                    <td class="yith-wcbk-costs-from">
                        <?php
                        $printer->print_field( array_merge( $args_from_to, array(
                            'name'  => '_yith_booking_costs_range[from][]',
                            'value' => $from
                        ) ) );
                        ?>
                    </td>

                    <td class="yith-wcbk-costs-to">
                        <?php
                        $printer->print_field( array_merge( $args_from_to, array(
                            'name'  => '_yith_booking_costs_range[to][]',
                            'value' => $to
                        ) ) );
                        ?>
                    </td>

                    <td class="yith-wcbk-costs-base-cost-operator">
                        <?php
                        $printer->print_field( array(
                                                   'type'    => 'select',
                                                   'name'    => '_yith_booking_costs_range[base_cost_operator][]',
                                                   'options' => $costs_operators,
                                                   'value'   => $base_cost_operator
                                               ) );
                        ?>
                    </td>

                    <td class="yith-wcbk-costs-base-cost">
                        <?php
                        $printer->print_field( array(
                                                   'type'  => 'text',
                                                   'name'  => '_yith_booking_costs_range[base_cost][]',
                                                   'value' => $base_cost
                                               ) );
                        ?>
                    </td>

                    <td class="yith-wcbk-costs-block-cost-operator">
                        <?php
                        $printer->print_field( array(
                                                   'type'    => 'select',
                                                   'name'    => '_yith_booking_costs_range[block_cost_operator][]',
                                                   'options' => $costs_operators,
                                                   'value'   => $block_cost_operator
                                               ) );
                        ?>
                    </td>

                    <td class="yith-wcbk-costs-block-cost">
                        <?php
                        $printer->print_field( array(
                                                   'type'  => 'text',
                                                   'name'  => '_yith_booking_costs_range[block_cost][]',
                                                   'value' => $block_cost
                                               ) );
                        ?>
                    </td>

                    <td class="yith-wcbk-costs-delete">
                        <?php
                        $printer->print_field( array(
                                                   'type'  => 'dashicon',
                                                   'class' => 'dashicons-no-alt yith-wcbk-delete'
                                               ) );
                        ?>
                    </td>
                </tr>

                <?php
            }
        }
        ?>
    </table>

    <div class="yith-wcbk-bottom-actions yith-wcbk-right">
        <input type="button" id="yith-wcbk-costs-add-range" class="button"
               value="<?php _e( 'Add Range', 'yith-booking-for-woocommerce' ); ?>">
    </div>
</div>


<div id="yith-wcbk-costs-hidden-utils-fields" class="yith-wcbk-hidden-utils-fields">
    <?php

    $printer->print_fields( array(
                                array(
                                    'type'  => 'text',
                                    'class' => 'yith-wcbk-admin-input-range',
                                ),
                                array(
                                    'type'              => 'number',
                                    'class'             => 'yith-wcbk-number-range',
                                    'custom_attributes' => 'min="0"'
                                ),
                                array(
                                    'type'    => 'select',
                                    'class'   => 'yith-wcbk-month-range-select',
                                    'options' => $months_array
                                ),
                                array(
                                    'type'    => 'select',
                                    'class'   => 'yith-wcbk-week-range-select',
                                    'options' => $weeks_array
                                ),
                                array(
                                    'type'    => 'select',
                                    'class'   => 'yith-wcbk-day-range-select',
                                    'options' => $days_array
                                ),
                            ) );

    ?>
</div>