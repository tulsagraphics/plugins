<?php
/**
 * @var string $name
 * @var string $url
 */
!defined( 'YITH_WCBK' ) && exit; // Exit if accessed directly
?>
<tr>
    <td>
        <input type="text" name="_yith_booking_external_calendars[name][]" value="<?php echo $name ?>"/>
    </td>
    <td>
        <input type="text" name="_yith_booking_external_calendars[url][]" value="<?php echo $url ?>"/>
    </td>
    <td width="1%"><a href="#" class="delete">Delete</a></td>
</tr>
