<?php
/**
 * Template options in WC Product Panel
 *
 * @author  Yithemes
 * @package YITH Booking for WooCommerce Premium
 * @version 1.0.0
 */


if ( !defined( 'YITH_WCBK' ) ) {
    exit;
} // Exit if accessed directly

?>

    <div class="options_group">

        <?php

        $range_time_array = array(
            'month'  => __( 'Month(s)', 'yith-booking-for-woocommerce' ),
            'day'    => __( 'Day(s)', 'yith-booking-for-woocommerce' ),
            'hour'   => __( 'Hour(s)', 'yith-booking-for-woocommerce' ),
            'minute' => __( 'Minute(s)', 'yith-booking-for-woocommerce' ),
        );

        $available_tab_fields = array(
            array(
                'type'   => 'section',
                'class'  => 'form-field _yith_booking_costs_base_cost_field',
                'fields' => array(
                    array(
                        'type'     => 'text',
                        'title'    => __( 'Base price', 'yith-booking-for-woocommerce' ),
                        'value'    => wc_format_localized_price( get_post_meta( $post_id, '_yith_booking_base_cost', true ) ),
                        'id'       => '_yith_booking_base_cost',
                        'class'    => 'wc_input_price',
                        'help_tip' => __( 'Select the base price for this booking.', 'yith-booking-for-woocommerce' ),
                    ),
                ),
            ),
            array(
                'type'   => 'section',
                'class'  => 'form-field _yith_booking_costs_block_cost_field',
                'fields' => array(
                    array(
                        'type'     => 'text',
                        'title'    => __( 'Extra cost per unit', 'yith-booking-for-woocommerce' ),
                        'value'    => wc_format_localized_price( get_post_meta( $post_id, '_yith_booking_block_cost', true ) ),
                        'id'       => '_yith_booking_block_cost',
                        'class'    => 'wc_input_price',
                        'help_tip' => __( 'Select the extra cost per booking unit.', 'yith-booking-for-woocommerce' ),
                    ),
                ),
            ),

        );

        YITH_WCBK_Printer()->print_fields( $available_tab_fields );
        ?>
    </div>

<?php

include( YITH_WCBK_VIEWS_PATH . 'product-tabs/utility/html-costs-table.php' );
?>