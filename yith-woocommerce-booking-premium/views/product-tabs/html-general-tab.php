<?php
/**
 * Template options in WC Product Panel
 *
 * @author  Yithemes
 * @package YITH Booking for WooCommerce Premium
 * @version 1.0.0
 */


if ( !defined( 'YITH_WCBK' ) ) {
    exit;
} // Exit if accessed directly

$booking_duration      = get_post_meta( $post_id, '_yith_booking_duration', true );
$booking_duration      = !!$booking_duration ? $booking_duration : 1;
$booking_duration_unit = get_post_meta( $post_id, '_yith_booking_duration_unit', true );
$booking_duration_unit = !!$booking_duration_unit ? $booking_duration_unit : 'day';


?>

<div class="options_group show_if_<?php echo $prod_type; ?>">

    <?php

    $range_time_array = array(
        'month'  => __( 'Month(s)', 'yith-booking-for-woocommerce' ),
        'day'    => __( 'Day(s)', 'yith-booking-for-woocommerce' ),
        'hour'   => __( 'Hour(s)', 'yith-booking-for-woocommerce' ),
        'minute' => __( 'Minute(s)', 'yith-booking-for-woocommerce' )
    );

    $general_tab_fields = array(
        array(
            'type'   => 'section',
            'class'  => 'form-field _yith_booking_duration_field yith_booking_multi_fields',
            'fields' => array(
                array(
                    'type'  => 'label',
                    'value' => __( 'Booking Duration', 'yith-booking-for-woocommerce' ),
                    'for'   => '_yith_booking_duration'
                ),
                array(
                    'type'    => 'select',
                    'value'   => get_post_meta( $post_id, '_yith_booking_duration_type', true ),
                    'id'      => '_yith_booking_duration_type',
                    'class'   => 'select short',
                    'options' => array(
                        'fixed'    => __( 'Fixed units of', 'yith-booking-for-woocommerce' ),
                        'customer' => __( 'Customer chooses units of', 'yith-booking-for-woocommerce' ),
                    )
                ),
                array(
                    'type'              => 'number',
                    'value'             => $booking_duration,
                    'id'                => '_yith_booking_duration',
                    'class'             => 'mini',
                    'custom_attributes' => 'step="1" min="1"'
                ),
                array(
                    'type'    => 'select',
                    'value'   => $booking_duration,
                    'id'      => '_yith_booking_duration_minute_select',
                    'name'    => false,
                    'class'   => 'mini',
                    'options' => apply_filters( 'yith_wcbk_duration_minute_select_options', array(
                        '15' => '15',
                        '30' => '30',
                        '45' => '45',
                        '60' => '60',
                        '90' => '90',
                    ) )
                ),
                array(
                    'type'    => 'select',
                    'value'   => $booking_duration_unit,
                    'id'      => '_yith_booking_duration_unit',
                    'class'   => 'select',
                    'options' => $range_time_array
                )
            )
        ),
        array(
            'type'   => 'section',
            'class'  => 'form-field _yith_booking_minimum_duration_field bk_show_if_customer_chooses_blocks',
            'fields' => array(
                array(
                    'type'              => 'number',
                    'title'             => __( 'Minimum Duration', 'yith-booking-for-woocommerce' ),
                    'value'             => get_post_meta( $post_id, '_yith_booking_minimum_duration', true ),
                    'id'                => '_yith_booking_minimum_duration',
                    'custom_attributes' => 'step="1" min="1"',
                    'help_tip'          => __( 'Select the minimum allowed duration the user can input.', 'yith-booking-for-woocommerce' )
                )
            )
        ),
        array(
            'type'   => 'section',
            'class'  => 'form-field _yith_booking_maximum_duration_field bk_show_if_customer_chooses_blocks',
            'fields' => array(
                array(
                    'type'              => 'number',
                    'title'             => __( 'Maximum Duration', 'yith-booking-for-woocommerce' ),
                    'value'             => get_post_meta( $post_id, '_yith_booking_maximum_duration', true ),
                    'id'                => '_yith_booking_maximum_duration',
                    'custom_attributes' => 'step="1" min="0"',
                    'help_tip'          => __( 'Select the maximum allowed duration the user can input. Set zero for unlimited.', 'yith-booking-for-woocommerce' )
                )
            )
        ),
        array(
            'type'   => 'section',
            'class'  => 'form-field _yith_booking_enable_calendar_range_picker bk_show_if_customer_one_day',
            'fields' => array(
                array(
                    'type'     => 'checkbox',
                    'title'    => __( 'Enable calendar range picker', 'yith-booking-for-woocommerce' ),
                    'value'    => get_post_meta( $post_id, '_yith_booking_enable_calendar_range_picker', true ),
                    'id'       => '_yith_booking_enable_calendar_range_picker',
                    'help_tip' => __( 'Lets users select a start and end date on the calendar; duration will be calculated automatically.',
                                      'yith-booking-for-woocommerce' ),
                )
            )
        ),
        array(
            'type'   => 'section',
            'class'  => 'form-field _yith_booking_all_day bk_show_if_day',
            'fields' => array(
                array(
                    'type'     => 'checkbox',
                    'title'    => __( 'All day', 'yith-booking-for-woocommerce' ),
                    'value'    => get_post_meta( $post_id, '_yith_booking_all_day', true ),
                    'id'       => '_yith_booking_all_day',
                    'help_tip' => __( 'Select if the booking is all day. The end date will be included in the duration calculation.', 'yith-booking-for-woocommerce' ),
                )
            )
        ),
        array(
            'type'   => 'section',
            'class'  => 'form-field _yith_booking_request_confirmation_field',
            'fields' => array(
                array(
                    'type'     => 'checkbox',
                    'title'    => __( 'Request Confirmation', 'yith-booking-for-woocommerce' ),
                    'value'    => get_post_meta( $post_id, '_yith_booking_request_confirmation', true ),
                    'id'       => '_yith_booking_request_confirmation',
                    'help_tip' => __( 'Check this box if you want the admin to confirm before accepting a booking.', 'yith-booking-for-woocommerce' ),
                )
            )
        ),
        array(
            'type'   => 'section',
            'class'  => 'form-field _yith_booking_can_be_cancelled_field',
            'fields' => array(
                array(
                    'type'     => 'checkbox',
                    'title'    => __( 'Can be cancelled', 'yith-booking-for-woocommerce' ),
                    'value'    => get_post_meta( $post_id, '_yith_booking_can_be_cancelled', true ),
                    'id'       => '_yith_booking_can_be_cancelled',
                    'help_tip' => __( 'Check this box if you want to allow users to cancel their booking.', 'yith-booking-for-woocommerce' ),
                )
            )
        ),
        array(
            'type'   => 'section',
            'class'  => 'form-field _yith_booking_cancelled_time_field bk_show_if_can_be_cancelled yith_booking_multi_fields',
            'fields' => array(
                array(
                    'type'  => 'label',
                    'value' => __( 'The booking can be cancelled up to', 'yith-booking-for-woocommerce' ),
                ),
                array(
                    'type'              => 'number',
                    'value'             => get_post_meta( $post_id, '_yith_booking_cancelled_duration', true ),
                    'id'                => '_yith_booking_cancelled_duration',
                    'custom_attributes' => 'step="1" min="0"'
                ),
                array(
                    'type'    => 'select',
                    'value'   => get_post_meta( $post_id, '_yith_booking_cancelled_unit', true ),
                    'id'      => '_yith_booking_cancelled_unit',
                    'class'   => 'select',
                    'options' => $range_time_array
                ),
                array(
                    'type'  => 'html',
                    'value' => __( 'before the booking start date', 'yith-booking-for-woocommerce' ),
                )
            )
        ),

        array(
            'type'   => 'section',
            'class'  => 'form-field _yith_booking_location_field',
            'fields' => array(
                array(
                    'type'     => 'text',
                    'title'    => __( 'Location', 'yith-booking-for-woocommerce' ),
                    'value'    => get_post_meta( $post_id, '_yith_booking_location', true ),
                    'id'       => '_yith_booking_location',
                    'class'    => 'yith-wcbk-google-maps-places-autocomplete',
                    'help_tip' => __( 'Enter the location of this booking product. This will facilitate search by location for your customers and will be the location shown in the map.', 'yith-booking-for-woocommerce' ),
                )
            )
        )
    );

    YITH_WCBK_Printer()->print_fields( $general_tab_fields );
    ?>
</div>
