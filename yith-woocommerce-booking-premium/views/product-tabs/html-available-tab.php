<?php
/**
 * Template options in WC Product Panel
 *
 * @author  Yithemes
 * @package YITH Booking for WooCommerce Premium
 * @version 1.0.0
 */


if ( !defined( 'YITH_WCBK' ) ) {
    exit;
} // Exit if accessed directly

$units        = array(
    'month'  => __( 'Months', 'yith-booking-for-woocommerce' ),
    'day'    => __( 'Days', 'yith-booking-for-woocommerce' ),
    'hour'   => __( 'Hours', 'yith-booking-for-woocommerce' ),
    'minute' => __( 'Minutes', 'yith-booking-for-woocommerce' )
);
$current_unit = "";
foreach ( $units as $unit_key => $unit_label ) {
    $current_unit .= "<span class='bk_show_if_unit_is_{$unit_key}'>{$unit_label}</span>";
}

?>

    <div class="options_group">

        <?php

        $days_array = yith_wcbk_get_days_array();

        $range_time_array = array(
            'month' => __( 'Month(s)', 'yith-booking-for-woocommerce' ),
            'day'   => __( 'Day(s)', 'yith-booking-for-woocommerce' ),
            //'hour'   => __( 'Hour(s)', 'yith-booking-for-woocommerce' ),
            //'minute' => __( 'Minute(s)', 'yith-booking-for-woocommerce' )
        );

        $range_time_array_with_year = array(
            'year'  => __( 'Year(s)', 'yith-booking-for-woocommerce' ),
            'month' => __( 'Month(s)', 'yith-booking-for-woocommerce' ),
            'day'   => __( 'Day(s)', 'yith-booking-for-woocommerce' ),
            //'hour'   => __( 'Hour(s)', 'yith-booking-for-woocommerce' ),
            //'minute' => __( 'Minute(s)', 'yith-booking-for-woocommerce' )
        );

        $max_booking_per_block = get_post_meta( $post_id, '_yith_booking_max_per_block', true );
        // set default to 1
        $max_booking_per_block = ( $max_booking_per_block == '' ) ? 1 : $max_booking_per_block;


        $available_tab_fields = array(
            array(
                'type'   => 'section',
                'class'  => 'form-field _yith_booking_available_max_per_block_field',
                'fields' => array(
                    array(
                        'type'              => 'number',
                        'title'             => __( 'Max bookings per unit', 'yith-booking-for-woocommerce' ),
                        'value'             => $max_booking_per_block,
                        'id'                => '_yith_booking_max_per_block',
                        'custom_attributes' => 'step="1" min="0"',
                        'help_tip'          => __( 'Select the maximum number of bookings allowed for each unit. Set zero for unlimited.',
                                                   'yith-booking-for-woocommerce' ),
                    ),
                ),
            ),
            array(
                'type'   => 'section',
                'class'  => 'form-field _yith_booking_available_allow_after_field yith_booking_multi_fields',
                'fields' => array(
                    array(
                        'type'              => 'number',
                        'title'             => __( 'Allow booking no sooner than', 'yith-booking-for-woocommerce' ),
                        'value'             => get_post_meta( $post_id, '_yith_booking_allow_after', true ),
                        'id'                => '_yith_booking_allow_after',
                        'custom_attributes' => 'step="1" min="0"',
                    ),
                    array(
                        'type'    => 'select',
                        'value'   => get_post_meta( $post_id, '_yith_booking_allow_after_unit', true ),
                        'id'      => '_yith_booking_allow_after_unit',
                        'class'   => 'select',
                        'options' => $range_time_array,
                    ),
                ),
            ),
            array(
                'type'   => 'section',
                'class'  => 'form-field _yith_booking_available_allow_until_field yith_booking_multi_fields',
                'fields' => array(
                    array(
                        'type'              => 'number',
                        'title'             => __( 'Allow booking no later than', 'yith-booking-for-woocommerce' ),
                        'value'             => get_post_meta( $post_id, '_yith_booking_allow_until', true ),
                        'id'                => '_yith_booking_allow_until',
                        'custom_attributes' => 'step="1" min="1"',
                    ),
                    array(
                        'type'    => 'select',
                        'value'   => get_post_meta( $post_id, '_yith_booking_allow_until_unit', true ),
                        'id'      => '_yith_booking_allow_until_unit',
                        'class'   => 'select',
                        'options' => $range_time_array_with_year,
                    ),
                ),
            ),
            array(
                'type'   => 'section',
                'class'  => 'form-field _yith_booking_buffer_field',
                'fields' => array(
                    array(
                        'type'              => 'number',
                        'title'             => sprintf( '%s (%s)', __( 'Buffer', 'yith-booking-for-woocommerce' ), $current_unit ),
                        'value'             => get_post_meta( $post_id, '_yith_booking_buffer', true ),
                        'id'                => '_yith_booking_buffer',
                        'custom_attributes' => 'step="1" min="0"',
                        'help_tip'          => __( 'Select the buffer between two bookings. It takes on the same unit that has been set up in the booking product. For example, if you\'ve set up the booking to be in minutes, and you set a buffer of 30, the buffer time between two appointments will be 30 minutes.',
                                                   'yith-booking-for-woocommerce' ),
                    ),
                ),
            ),
            array(
                'type'   => 'section',
                'class'  => 'form-field _yith_booking_available_checkin_field',
                'fields' => array(
                    array(
                        'type'     => 'text',
                        'title'    => __( 'Check-in', 'yith-booking-for-woocommerce' ),
                        'value'    => get_post_meta( $post_id, '_yith_booking_checkin', true ),
                        'id'       => '_yith_booking_checkin',
                        'help_tip' => __( 'Insert check-in time here', 'yith-booking-for-woocommerce' ),
                    ),
                ),
            ),
            array(
                'type'   => 'section',
                'class'  => 'form-field _yith_booking_available_checkout_field',
                'fields' => array(
                    array(
                        'type'     => 'text',
                        'title'    => __( 'Check-out', 'yith-booking-for-woocommerce' ),
                        'value'    => get_post_meta( $post_id, '_yith_booking_checkout', true ),
                        'id'       => '_yith_booking_checkout',
                        'help_tip' => __( 'Insert check-out time here', 'yith-booking-for-woocommerce' ),
                    ),
                ),
            ),
            array(
                'type'   => 'section',
                'class'  => 'form-field _yith_booking_default_start_date',
                'fields' => array(
                    array(
                        'type'     => 'select',
                        'class'    => 'select short',
                        'title'    => __( 'Default start date', 'yith-booking-for-woocommerce' ),
                        'value'    => get_post_meta( $post_id, '_yith_booking_default_start_date', true ),
                        'id'       => '_yith_booking_default_start_date',
                        'options'  => array(
                            ''                => __( 'None', 'yith-booking-for-woocommerce' ),
                            'today'           => __( 'Today', 'yith-booking-for-woocommerce' ),
                            'tomorrow'        => __( 'Tomorrow', 'yith-booking-for-woocommerce' ),
                            'first-available' => __( 'First available', 'yith-booking-for-woocommerce' ),
                            'custom'          => __( 'Custom Date', 'yith-booking-for-woocommerce' ),
                        ),
                        'help_tip' => __( 'Insert the default start date here. Please, note: <b>Today</b> and <b>Tomorrow</b> are dynamic dates, based on when the user accesses the page.', 'yith-booking-for-woocommerce' ),
                    ),
                ),
            ),
            array(
                'type'   => 'section',
                'class'  => 'form-field _yith_booking_default_start_date_custom yith-wcbk-show-conditional',
                'data'   => array(
                    'field-id' => '_yith_booking_default_start_date',
                    'value'    => 'custom',
                ),
                'fields' => array(
                    array(
                        'type'  => 'text',
                        'class' => 'yith-wcbk-date-input-field yith-wcbk-admin-date-picker',
                        'title' => __( 'Custom Default Start Date', 'yith-booking-for-woocommerce' ),
                        'name'  => '_yith_booking_default_start_date_custom',
                        'value' => get_post_meta( $post_id, '_yith_booking_default_start_date_custom', true ),
                    )
                ),
            ),
            array(
                'type'   => 'section',
                'class'  => 'form-field _yith_booking_default_start_time bk_show_if_time',
                'fields' => array(
                    array(
                        'type'     => 'select',
                        'class'    => 'select short',
                        'title'    => __( 'Default start time', 'yith-booking-for-woocommerce' ),
                        'value'    => get_post_meta( $post_id, '_yith_booking_default_start_time', true ),
                        'id'       => '_yith_booking_default_start_time',
                        'options'  => array(
                            ''                => __( 'None', 'yith-booking-for-woocommerce' ),
                            'first-available' => __( 'First available', 'yith-booking-for-woocommerce' ),
                        ),
                        'help_tip' => __( 'Insert the default start time here.', 'yith-booking-for-woocommerce' ),
                    ),
                ),
            ),
            array(
                'type'   => 'section',
                'class'  => 'form-field _yith_booking_allowed_start_days_field',
                'fields' => array(
                    array(
                        'type'     => 'select',
                        'class'    => 'wc-enhanced-select select short',
                        'multiple' => true,
                        'title'    => __( 'Allowed Start Days', 'yith-booking-for-woocommerce' ),
                        'name'     => '_yith_booking_allowed_start_days[]',
                        'options'  => yith_wcbk_get_days_array(),
                        'value'    => get_post_meta( $post_id, '_yith_booking_allowed_start_days', true ),
                    )
                ),
            ),
            array(
                'type'   => 'section',
                'class'  => 'form-field _yith_booking_daily_start_time bk_show_if_time',
                'fields' => array(
                    array(
                        'type'  => 'time-select',
                        'title' => __( 'Daily Start Time', 'yith-booking-for-woocommerce' ),
                        'name'  => '_yith_booking_daily_start_time',
                        'value' => get_post_meta( $post_id, '_yith_booking_daily_start_time', true ),
                    )
                ),
            ),
            array(
                'type'   => 'section',
                'class'  => 'form-field _yith_booking_time_increment_based_on_duration bk_show_if_time',
                'fields' => array(
                    array(
                        'type'     => 'checkbox',
                        'title'    => __( 'Time increment based on duration', 'yith-booking-for-woocommerce' ),
                        'value'    => get_post_meta( $post_id, '_yith_booking_time_increment_based_on_duration', true ),
                        'id'       => '_yith_booking_time_increment_based_on_duration',
                        'help_tip' => __( 'Select to enable Time increment based on duration.', 'yith-booking-for-woocommerce' ),
                    )
                ),
            ),
        );

        YITH_WCBK_Printer()->print_fields( $available_tab_fields );
        ?>
    </div>

<?php
include( YITH_WCBK_VIEWS_PATH . 'product-tabs/utility/html-availability-table.php' );
?>