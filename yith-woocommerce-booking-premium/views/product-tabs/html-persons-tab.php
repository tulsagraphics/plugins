<?php
/**
 * Template options in WC Product Panel
 *
 * @author  Yithemes
 * @package YITH Booking for WooCommerce Premium
 * @version 1.0.0
 */


if ( !defined( 'YITH_WCBK' ) ) {
    exit;
} // Exit if accessed directly

?>

<div class="options_group">

    <?php

    $range_time_array = array(
        'month'  => __( 'Month(s)', 'yith-booking-for-woocommerce' ),
        'day'    => __( 'Day(s)', 'yith-booking-for-woocommerce' ),
        'hour'   => __( 'Hour(s)', 'yith-booking-for-woocommerce' ),
        'minute' => __( 'Minute(s)', 'yith-booking-for-woocommerce' )
    );

    $available_tab_fields = array(
        array(
            'type'   => 'section',
            'class'  => 'form-field _yith_booking_has_persons_field',
            'fields' => array(
                array(
                    'type'     => 'checkbox',
                    'title'    => __( 'Has people', 'yith-booking-for-woocommerce' ),
                    'value'    => get_post_meta( $post_id, '_yith_booking_has_persons', true ),
                    'id'       => '_yith_booking_has_persons',
                    'help_tip' => __( 'Check this box if the booking can be made only by a specific number of people.', 'yith-booking-for-woocommerce' )
                )
            )
        ),
        array(
            'type'             => 'section',
            'class'            => 'bk_show_if_booking_has_persons',
            'section_html_tag' => 'div',
            'fields'           => array(
                array(
                    'type'   => 'section',
                    'class'  => 'form-field _yith_booking_min_persons_field',
                    'fields' => array(
                        array(
                            'type'              => 'number',
                            'title'             => __( 'Min people', 'yith-booking-for-woocommerce' ),
                            'value'             => get_post_meta( $post_id, '_yith_booking_min_persons', true ),
                            'id'                => '_yith_booking_min_persons',
                            'custom_attributes' => 'step="1" min="1"',
                            'help_tip'          => __( 'Select the minimum number of people per booking.', 'yith-booking-for-woocommerce' )
                        )
                    )
                ),
                array(
                    'type'   => 'section',
                    'class'  => 'form-field _yith_booking_max_persons_field',
                    'fields' => array(
                        array(
                            'type'              => 'number',
                            'title'             => __( 'Max people', 'yith-booking-for-woocommerce' ),
                            'value'             => get_post_meta( $post_id, '_yith_booking_max_persons', true ),
                            'id'                => '_yith_booking_max_persons',
                            'custom_attributes' => 'step="1" min="0"',
                            'help_tip'          => __( 'Select the maximum number of people per booking. Set zero for unlimited', 'yith-booking-for-woocommerce' )
                        )
                    )
                ),
                array(
                    'type'   => 'section',
                    'class'  => 'form-field _yith_booking_multiply_costs_by_persons_field',
                    'fields' => array(
                        array(
                            'type'     => 'checkbox',
                            'title'    => __( 'Multiply all costs by number of people', 'yith-booking-for-woocommerce' ),
                            'value'    => get_post_meta( $post_id, '_yith_booking_multiply_costs_by_persons', true ),
                            'id'       => '_yith_booking_multiply_costs_by_persons',
                            'help_tip' => __( 'Check this box if you want to multiply all costs by the number of people selected.', 'yith-booking-for-woocommerce' )
                        )
                    )
                ),
                array(
                    'type'   => 'section',
                    'class'  => 'form-field _yith_booking_count_persons_as_bookings_field',
                    'fields' => array(
                        array(
                            'type'     => 'checkbox',
                            'title'    => __( 'Count people as separated bookings', 'yith-booking-for-woocommerce' ),
                            'value'    => get_post_meta( $post_id, '_yith_booking_count_persons_as_bookings', true ),
                            'id'       => '_yith_booking_count_persons_as_bookings',
                            'help_tip' => __( 'Check this box to consider a different booking for each person until the maximum number of bookings per unit is reached.', 'yith-booking-for-woocommerce' )
                        )
                    )
                ),
                array(
                    'type'   => 'section',
                    'class'  => 'form-field _yith_booking_enable_person_types_field',
                    'fields' => array(
                        array(
                            'type'     => 'checkbox',
                            'title'    => __( 'Enable types for people', 'yith-booking-for-woocommerce' ),
                            'value'    => get_post_meta( $post_id, '_yith_booking_enable_person_types', true ),
                            'id'       => '_yith_booking_enable_person_types',
                            'help_tip' => __( 'Types allow you to offer different booking costs for different types of people (e.g. adults, children etc.)', 'yith-booking-for-woocommerce' )
                        )
                    )
                ),
            )
        ),
    );

    YITH_WCBK_Printer()->print_fields( $available_tab_fields );

    include( YITH_WCBK_VIEWS_PATH . 'product-tabs/utility/html-person-type-table.php' );
    ?>
</div>