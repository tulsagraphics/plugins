<?php
/**
 * Template options in WC Product Panel
 *
 * @author  Yithemes
 * @package YITH Booking for WooCommerce Premium
 *
 * @var string $prod_type
 * @var int    $post_id
 */
!defined( 'YITH_WCBK' ) && exit; // Exit if accessed directly

$product = wc_get_product( $post_id );
?>

<div class="options_group show_if_<?php echo $prod_type; ?>">
    <table class="yith-wcbk-product-settings-table">
        <tr class="_yith_booking_sync_export_future_ics_url">
            <?php
            if ( $product->is_type( YITH_WCBK_Product_Post_Type_Admin::$prod_type ) ) {
                $key = $product->get_external_calendars_key();
            } else {
                $key = yith_wcbk_generate_external_calendars_key();
            }
            $export_future_ics_url = add_query_arg( array( 'yith_wcbk_exporter_action' => 'export_future_ics', 'product_id' => $post_id, 'key' => $key ), trailingslashit( home_url() ) );
            $export_future_ics_id  = 'yith-wcbk-booking-sync-export-future-ics-' . $post_id;
            ?>
            <td class="yith-wcbk-product-settings-table__label">
                <label for="<?php echo $export_future_ics_id ?>"><?php _e( 'Export Future ICS URL', 'yith-booking-for-woocommerce' ) ?></label>
            </td>
            <td class="yith-wcbk-product-settings-table__field">
                <input type="hidden" name="_yith_booking_external_calendars_key" value="<?php echo $key ?>"/>
                <input id="<?php echo $export_future_ics_id ?>" type="text" value="<?php echo $export_future_ics_url ?>"
                       disabled/>
                <span class="dashicons dashicons-admin-page yith-wcbk-copy-to-clipboard"
                      data-selector-to-copy="#<?php echo $export_future_ics_id ?>"
                      title="<?php _e( 'Copy to clipboard', 'yith-booking-for-woocommerce' ) ?>"></span>

                <a href="<?php echo $export_future_ics_url ?>"><span class="dashicons dashicons-download"
                                                                     title="<?php _e( 'Download', 'yith-booking-for-woocommerce' ) ?>"></span></a>
            </td>
        </tr>


        <tr class="_yith_booking_sync_external_calendars">
            <td class="yith-wcbk-product-settings-table__label">
                <label><?php _e( 'Import ICS Calendars', 'yith-booking-for-woocommerce' ) ?></label>
            </td>
            <td class="yith-wcbk-product-settings-table__field">
                <table class="yith-wcbk-product-sync-imported-calendars-table">
                    <thead>
                    <tr>
                        <th><?php _e( 'Name', 'yith-booking-for-woocommerce' ) ?></th>
                        <th><?php _e( 'URL', 'yith-booking-for-woocommerce' ) ?></th>
                        <th width="1%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $calendars = is_callable( array( $product, 'get_external_calendars' ) ) ? $product->get_external_calendars() : array();
                    foreach ( $calendars as $calendar ) {
                        $name = $calendar[ 'name' ];
                        $url  = $calendar[ 'url' ];
                        include( 'utility/html-imported-calendar-row.php' );
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="3">
                            <a href="#" class="button insert" data-row="<?php
                            $name = '';
                            $url  = '';
                            ob_start();
                            include( 'utility/html-imported-calendar-row.php' );
                            echo esc_attr( ob_get_clean() );
                            ?>"><?php _e( 'Add ICS calendar', 'yith-booking-for-woocommerce' ); ?></a>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
        <tr class="_yith_booking_sync_export_future_ics_url">
            <td class="yith-wcbk-product-settings-table__label">
                <label><?php _e( 'Last Sync', 'yith-booking-for-woocommerce' ) ?></label>
            </td>
            <td class="yith-wcbk-product-settings-table__field">
                <?php
                $last_sync = is_callable( array( $product, 'get_external_calendars_last_sync' ) ) ? $product->get_external_calendars_last_sync() : 0;
                echo $last_sync ? date_i18n( 'M d, Y H:i:s', $last_sync ) : '';
                ?>
            </td>
        </tr>
    </table>
</div>
