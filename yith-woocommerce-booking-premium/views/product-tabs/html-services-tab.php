<?php
/**
 * Template options in WC Product Panel
 *
 * @author  Yithemes
 * @package YITH Booking for WooCommerce Premium
 * @version 1.0.0
 */


if ( !defined( 'YITH_WCBK' ) ) {
    exit;
} // Exit if accessed directly

?>

<div class="options_group">
    <p class="form-field">
        <?php
        $service_helper = YITH_WCBK()->service_helper;

        $services         = $service_helper->get_services( array( 'fields' => 'id=>name' ) );
        $product_services = wp_get_object_terms( $post_id, YITH_WCBK_Post_Types::$service_tax, array( 'fields' => 'ids' ) );
        ?>
        <label for="_yith_wcbk_booking_services"><?php _e( 'Services', 'yith-booking-for-woocommerce' ) ?></label>
        <select id='_yith_wcbk_booking_services' name='_yith_booking_services[]' class="multiselect attribute_values wc-enhanced-select" multiple="multiple"
                placeholder="<?php _e( 'select one or more services...', 'yith-booking-for-woocommerce' ) ?>" style="width:50%">
            <?php foreach ( $services as $service_id => $service_name ) : ?>
                <option value="<?php echo $service_id ?>" <?php selected( in_array( $service_id, $product_services ) ) ?>><?php echo $service_name ?></option>
            <?php endforeach; ?>
        </select>
        <input type="button" class="button yith-wcbk-select2-select-all" value="<?php _e( 'Select all', 'yith-booking-for-woocommerce' ); ?>"
               data-select-id="_yith_wcbk_booking_services">
        <input type="button" class="button yith-wcbk-select2-deselect-all" value="<?php _e( 'Deselect all', 'yith-booking-for-woocommerce' ); ?>"
               data-select-id="_yith_wcbk_booking_services">
    </p>
</div>


<div class="yith-wcbk-bottom-actions yith-wcbk-right">
    <?php
    $new_service_text = __( 'Add New Service', 'yith-booking-for-woocommerce' );
    $new_service_link = add_query_arg( array(
                                           'post_type' => YITH_WCBK_Post_Types::$booking,
                                           'taxonomy'  => YITH_WCBK_Post_Types::$service_tax,
                                       ), admin_url( 'edit-tags.php' ) );
    $note             = "<a href='$new_service_link' target='_blank'>$new_service_text</a>";
    echo $note;
    ?>
</div>