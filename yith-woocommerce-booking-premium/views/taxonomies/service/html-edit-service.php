<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

$service_taxonomy_info = YITH_WCBK_Service_Tax_Admin::get_service_taxonomy_info();

/**
 * @var YITH_WCBK_Service $service the booking service
 */


foreach ( $service_taxonomy_info as $id => $args ) {
    $type = $args[ 'type' ] === 'price' ? 'number' : $args[ 'type' ];
    $name = isset( $args[ 'name' ] ) ? $args[ 'name' ] : "yith_booking_service_data[{$id}]";
    if ( isset( $args[ 'person_type_id' ] ) ) {
        $value = $service->get_price_for_person_type( $args[ 'person_type_id' ] );
    } else {
        $value = $service->$id;
    }
    ?>

    <tr class="form-field">
        <th scope="row" valign="top">
            <label for="yith_booking_service_<?php echo $id; ?>"><?php echo $args[ 'title' ] ?></label>
        </th>
        <td>
            <?php switch ( $type ) {
                case 'number':
                    $custom_attributes = $args[ 'type' ] === 'price' ? 'step="0.01" min="0"' : '';
                    if ( isset( $args[ 'min' ] ) ) {
                        $custom_attributes .= " min='" . $args[ 'min' ] . "' ";
                    }
                    ?>
                    <input type="number" class="regular-text" name="<?php echo $name; ?>"
                           id="yith_booking_service_<?php echo $id; ?>" <?php echo $custom_attributes; ?>
                           value="<?php echo $value ?>"/>
                    <p class="description"><?php echo $args[ 'desc' ]; ?></p>
                    <?php
                    break;
                case 'checkbox':
                    $check_method = 'is_' . $id;
                    ?>
                    <input type="checkbox" name="<?php echo $name; ?>" id="yith_booking_service_<?php echo $id; ?>"
                           value="yes" <?php checked( $service->$check_method(), true ); ?> />
                    <?php echo $args[ 'desc' ]; ?>
                    <?php
                    break;
                case 'select':
                    $value = $service->$id;
                    $options = !empty( $args[ 'options' ] ) ? $args[ 'options' ] : array();
                    ?>
                    <select name="<?php echo $name; ?>" id="yith_booking_service_<?php echo $id; ?>" <?php echo $custom_attributes; ?>>
                        <?php foreach ( $options as $option_key => $option_value ) : ?>
                            <option value="<?php echo $option_key ?>" <?php selected( $option_key == $value, true, true ); ?> ><?php echo $option_value ?></option>
                        <?php endforeach; ?>
                    </select>
                    <p class="description"><?php echo $args[ 'desc' ]; ?></p>
                    <?php
                    break;
            } ?>
        </td>
    </tr>


    <?php

}
?>