<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

$fields = array();

$service_taxonomy_info = YITH_WCBK_Service_Tax_Admin::get_service_taxonomy_info();

foreach ( $service_taxonomy_info as $id => $args ) {
    $type              = $args[ 'type' ] === 'price' ? 'number' : $args[ 'type' ];
    $name              = isset( $args[ 'name' ] ) ? $args[ 'name' ] : "yith_booking_service_data[{$id}]";
    $custom_attributes = $args[ 'type' ] === 'price' ? 'step="0.01" min="0"' : '';

    if ( isset( $args[ 'min' ] ) ) {
        $custom_attributes .= " min='" . $args[ 'min' ] . "' ";
    }
    $fields[] = array(
        'type'             => 'section',
        'section_html_tag' => 'div',
        'class'            => 'form-field yith-wcbk-booking-service-form-section yith-wcbk-booking-service-form-section-' . $type,
        'fields'           => array(
            array(
                'type'              => $type,
                'title'             => $args[ 'title' ],
                'value'             => isset( $args[ 'default' ] ) ? $args[ 'default' ] : '',
                'id'                => 'yith_booking_service_' . $id,
                'name'              => $name,
                'custom_attributes' => $custom_attributes,
                'help_tip'          => $args[ 'desc' ],
                'options'           => isset( $args[ 'options' ] ) ? $args[ 'options' ] : array()
            )
        )
    );
}

YITH_WCBK_Printer()->print_fields( $fields );