<?php
!defined( 'YITH_WCBK' ) && exit();

$printer      = YITH_WCBK_Printer();
$months_array = yith_wcbk_get_months_array();
$days_array   = yith_wcbk_get_days_array();

$costs_operators = array(
    'add' => '+',
    'sub' => '-',
    'mul' => '*',
    'div' => '/',
);

$global_costs = YITH_WCBK()->settings->get_global_cost_ranges();
?>

<h2><?php _e( 'Global Costs', 'yith-booking-for-woocommerce' ) ?></h2>
<form method="post">
    <div id="yith-wcbk-settings-tab-wrapper">
        <div class="yith-wcbk-settings-content">
            <table id="yith-wcbk-booking-costs-table" class="yith-wcbk-booking-settings-table">
                <tr class="yith-wcbk-costs-row yith-wcbk-costs-default-row">
                    <td class="yith-wcbk-costs-anchor">
                        <?php
                        $printer->print_field( array(
                                                   'type'  => 'dashicon',
                                                   'class' => 'dashicons-menu yith-wcbk-anchor',
                                               ) );
                        ?>
                    </td>
                    <td class="yith-wcbk-costs-range-column">
                        <div class="yith-wcbk-costs-range-column-ranges">
                            <div class="yith-wcbk-costs-range-column-ranges-date">

                                <?php
                                $printer->print_field( array(
                                                           'type'  => 'hidden',
                                                           'name'  => 'yith_booking_global_cost_ranges[type][]',
                                                           'value' => 'custom',
                                                       ) );
                                $printer->print_field( array(
                                                           'type'  => 'text',
                                                           'class' => 'yith-wcbk-date-input-field',
                                                           'title' => __( 'From', 'yith-booking-for-woocommerce' ),
                                                           'name'  => 'yith_booking_global_cost_ranges[from][]',

                                                       ) );
                                $printer->print_field( array(
                                                           'type'  => 'text',
                                                           'class' => 'yith-wcbk-date-input-field',
                                                           'title' => __( 'To', 'yith-booking-for-woocommerce' ),
                                                           'name'  => 'yith_booking_global_cost_ranges[to][]',

                                                       ) );
                                ?>

                            </div>
                            <div class="yith-wcbk-costs-range-column-ranges-month">
                                <?php
                                $printer->print_field( array(
                                                           'type'              => 'hidden',
                                                           'name'              => 'yith_booking_global_cost_ranges[type][]',
                                                           'value'             => 'month',
                                                           'custom_attributes' => 'disabled="disabled"',

                                                       ) );
                                $printer->print_field( array(
                                                           'type'              => 'select',
                                                           'class'             => 'yith-wcbk-month-range-select',
                                                           'title'             => __( 'From', 'yith-booking-for-woocommerce' ),
                                                           'name'              => 'yith_booking_global_cost_ranges[from][]',
                                                           'options'           => $months_array,
                                                           'custom_attributes' => 'disabled="disabled"',
                                                       ) );
                                $printer->print_field( array(
                                                           'type'              => 'select',
                                                           'class'             => 'yith-wcbk-month-range-select',
                                                           'title'             => __( 'To', 'yith-booking-for-woocommerce' ),
                                                           'name'              => 'yith_booking_global_cost_ranges[to][]',
                                                           'options'           => $months_array,
                                                           'custom_attributes' => 'disabled="disabled"',
                                                       ) );
                                ?>
                            </div>

                            <div class="yith-wcbk-costs-range-column-ranges-day">
                                <?php
                                $printer->print_field( array(
                                                           'type'              => 'hidden',
                                                           'name'              => 'yith_booking_global_cost_ranges[type][]',
                                                           'value'             => 'day',
                                                           'custom_attributes' => 'disabled="disabled"',

                                                       ) );
                                $printer->print_field( array(
                                                           'type'              => 'select',
                                                           'class'             => 'yith-wcbk-day-range-select',
                                                           'title'             => __( 'From', 'yith-booking-for-woocommerce' ),
                                                           'name'              => 'yith_booking_global_cost_ranges[from][]',
                                                           'options'           => $days_array,
                                                           'custom_attributes' => 'disabled="disabled"',
                                                       ) );
                                $printer->print_field( array(
                                                           'type'              => 'select',
                                                           'class'             => 'yith-wcbk-day-range-select',
                                                           'title'             => __( 'To', 'yith-booking-for-woocommerce' ),
                                                           'name'              => 'yith_booking_global_cost_ranges[to][]',
                                                           'options'           => $days_array,
                                                           'custom_attributes' => 'disabled="disabled"',
                                                       ) );
                                ?>
                            </div>
                        </div>
                        <div class="yith-wcbk-costs-range-column-costs">
                            <label><?php _e( 'Base price', 'yith-booking-for-woocommerce' ) ?></label>
                            <?php
                            $printer->print_field( array(
                                                       'type'    => 'select',
                                                       'name'    => 'yith_booking_global_cost_ranges[base_cost_operator][]',
                                                       'options' => $costs_operators,
                                                   ) );
                            ?>

                            <?php
                            $printer->print_field( array(
                                                       'type'              => 'number',
                                                       'name'              => 'yith_booking_global_cost_ranges[base_cost][]',
                                                       'custom_attributes' => 'step="0.01"',
                                                   ) );
                            ?>
                            <label><?php _e( 'Extra cost per unit', 'yith-booking-for-woocommerce' ) ?></label>

                            <?php
                            $printer->print_field( array(
                                                       'type'    => 'select',
                                                       'name'    => 'yith_booking_global_cost_ranges[block_cost_operator][]',
                                                       'options' => $costs_operators,
                                                   ) );
                            ?>

                            <?php
                            $printer->print_field( array(
                                                       'type'              => 'number',
                                                       'name'              => 'yith_booking_global_cost_ranges[block_cost][]',
                                                       'custom_attributes' => 'step="0.01"',

                                                   ) );
                            ?>

                        </div>
                    </td>
                    <td class="yith-wcbk-costs-actions-column">
                        <span class="dashicons dashicons-screenoptions yith-wcbk-costs-toggle-visible"></span>
                        <?php
                        $printer->print_field( array(
                                                   'type'  => 'dashicon',
                                                   'class' => 'dashicons-no-alt yith-wcbk-delete',
                                               ) );
                        ?>
                    </td>
                </tr>
                <?php foreach ( $global_costs as $single_cost ): ?>
                    <tr class="yith-wcbk-costs-row ">
                        <td class="yith-wcbk-costs-anchor">
                            <?php
                            $printer->print_field( array(
                                                       'type'  => 'dashicon',
                                                       'class' => 'dashicons-menu yith-wcbk-anchor',
                                                   ) );
                            ?>
                        </td>
                        <td class="yith-wcbk-costs-range-column">
                            <div class="yith-wcbk-costs-range-column-ranges">

                                <?php if ( $single_cost->type === 'custom' ) : ?>
                                    <div class="yith-wcbk-costs-range-column-ranges-date">

                                        <?php
                                        $printer->print_field( array(
                                                                   'type'  => 'hidden',
                                                                   'name'  => 'yith_booking_global_cost_ranges[type][]',
                                                                   'value' => 'custom',
                                                               ) );
                                        $printer->print_field( array(
                                                                   'type'  => 'text',
                                                                   'class' => 'yith-wcbk-date-input-field yith-wcbk-admin-date-picker',
                                                                   'title' => __( 'From', 'yith-booking-for-woocommerce' ),
                                                                   'name'  => 'yith_booking_global_cost_ranges[from][]',
                                                                   'value' => $single_cost->from,

                                                               ) );
                                        $printer->print_field( array(
                                                                   'type'  => 'text',
                                                                   'class' => 'yith-wcbk-date-input-field yith-wcbk-admin-date-picker',
                                                                   'title' => __( 'To', 'yith-booking-for-woocommerce' ),
                                                                   'name'  => 'yith_booking_global_cost_ranges[to][]',
                                                                   'value' => $single_cost->to,
                                                               ) );
                                        ?>
                                    </div>
                                <?php elseif ( $single_cost->type === 'month' ) : ?>
                                    <div class="yith-wcbk-costs-range-column-ranges-month">
                                        <?php
                                        $printer->print_field( array(
                                                                   'type'  => 'hidden',
                                                                   'name'  => 'yith_booking_global_cost_ranges[type][]',
                                                                   'value' => 'month',
                                                               ) );
                                        $printer->print_field( array(
                                                                   'type'    => 'select',
                                                                   'class'   => 'yith-wcbk-month-range-select',
                                                                   'title'   => __( 'From', 'yith-booking-for-woocommerce' ),
                                                                   'name'    => 'yith_booking_global_cost_ranges[from][]',
                                                                   'options' => $months_array,
                                                                   'value'   => $single_cost->from,
                                                               ) );
                                        $printer->print_field( array(
                                                                   'type'    => 'select',
                                                                   'class'   => 'yith-wcbk-month-range-select',
                                                                   'title'   => __( 'To', 'yith-booking-for-woocommerce' ),
                                                                   'name'    => 'yith_booking_global_cost_ranges[to][]',
                                                                   'options' => $months_array,
                                                                   'value'   => $single_cost->to,
                                                               ) );
                                        ?>
                                    </div>
                                <?php elseif ( $single_cost->type === 'month' ) : ?>

                                    <div class="yith-wcbk-costs-range-column-ranges-day">
                                        <?php
                                        $printer->print_field( array(
                                                                   'type'              => 'hidden',
                                                                   'name'              => 'yith_booking_global_cost_ranges[type][]',
                                                                   'value'             => 'day',
                                                                   'custom_attributes' => 'disabled="disabled"',

                                                               ) );
                                        $printer->print_field( array(
                                                                   'type'              => 'select',
                                                                   'class'             => 'yith-wcbk-day-range-select',
                                                                   'title'             => __( 'From', 'yith-booking-for-woocommerce' ),
                                                                   'name'              => 'yith_booking_global_cost_ranges[from][]',
                                                                   'options'           => $days_array,
                                                                   'custom_attributes' => 'disabled="disabled"',
                                                               ) );
                                        $printer->print_field( array(
                                                                   'type'              => 'select',
                                                                   'class'             => 'yith-wcbk-day-range-select',
                                                                   'title'             => __( 'To', 'yith-booking-for-woocommerce' ),
                                                                   'name'              => 'yith_booking_global_cost_ranges[to][]',
                                                                   'options'           => $days_array,
                                                                   'custom_attributes' => 'disabled="disabled"',
                                                               ) );
                                        ?>

                                    </div>

                                <?php endif; ?>
                            </div>

                            <div class="yith-wcbk-costs-range-column-costs">
                                <label><?php _e( 'Base price', 'yith-booking-for-woocommerce' ) ?></label>
                                <?php
                                $printer->print_field( array(
                                                           'type'    => 'select',
                                                           'name'    => 'yith_booking_global_cost_ranges[base_cost_operator][]',
                                                           'options' => $costs_operators,
                                                           'value'   => $single_cost->base_cost_operator,
                                                       ) );
                                ?>

                                <?php
                                $printer->print_field( array(
                                                           'type'              => 'number',
                                                           'name'              => 'yith_booking_global_cost_ranges[base_cost][]',
                                                           'custom_attributes' => 'step="0.01"',
                                                           'value'             => $single_cost->base_cost,

                                                       ) );
                                ?>
                                <label><?php _e( 'Extra cost per unit', 'yith-booking-for-woocommerce' ) ?></label>

                                <?php
                                $printer->print_field( array(
                                                           'type'    => 'select',
                                                           'name'    => 'yith_booking_global_cost_ranges[block_cost_operator][]',
                                                           'options' => $costs_operators,
                                                           'value'   => $single_cost->block_cost_operator,

                                                       ) );
                                ?>

                                <?php
                                $printer->print_field( array(
                                                           'type'              => 'number',
                                                           'name'              => 'yith_booking_global_cost_ranges[block_cost][]',
                                                           'custom_attributes' => 'step="0.01"',
                                                           'value'             => $single_cost->block_cost,

                                                       ) );
                                ?>

                            </div>

                        </td>
                        <td class="yith-wcbk-costs-actions-column">
                            <span class="dashicons dashicons-screenoptions yith-wcbk-costs-toggle-visible"></span>
                            <?php
                            $printer->print_field( array(
                                                       'type'  => 'dashicon',
                                                       'class' => 'dashicons-no-alt yith-wcbk-delete',
                                                   ) );
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <div class="yith-wcbk-bottom-actions yith-wcbk-right">
            <input type="button" id="yith-wcbk-costs-add-date-range" class="button" value="<?php _e( 'Add Date Range', 'yith-booking-for-woocommerce' ); ?>">
            <input type="button" id="yith-wcbk-costs-add-month-range" class="button" value="<?php _e( 'Add Month Range', 'yith-booking-for-woocommerce' ); ?>">
            <input type="button" id="yith-wcbk-costs-add-day-range" class="button" value="<?php _e( 'Add Day Range', 'yith-booking-for-woocommerce' ); ?>">
        </div>


    </div>
    <div id="yith-wcbk-settings-tab-actions">
        <input type="submit" id="yith-wcbk-settings-tab-actions-save"
               class="button button-primary" value="<?php _e( 'Save', 'yith-booking-for-woocommerce' ) ?>">
    </div>
    <?php
    wp_nonce_field( 'yith_wcbk_settings_fields', 'yith_wcbk_nonce', false );
    ?>
</form>