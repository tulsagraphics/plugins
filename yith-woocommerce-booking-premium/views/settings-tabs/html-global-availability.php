<?php
!defined( 'YITH_WCBK' ) && exit();

$printer             = YITH_WCBK_Printer();
$months_array        = yith_wcbk_get_months_array();
$days_array          = yith_wcbk_get_days_array();
$global_availability = YITH_WCBK()->settings->get_global_availability_ranges();
?>

<h2><?php _e( 'Global Availability', 'yith-booking-for-woocommerce' ) ?></h2>
<form method="post">
    <div id="yith-wcbk-settings-tab-wrapper">
        <div class="yith-wcbk-settings-content">
            <table id="yith-wcbk-booking-availability-table" class="yith-wcbk-booking-settings-table">
                <tr class="yith-wcbk-availability-row yith-wcbk-availability-default-row">
                    <td class="yith-wcbk-availability-anchor">
                        <?php
                        $printer->print_field( array(
                                                   'type'  => 'dashicon',
                                                   'class' => 'dashicons-menu yith-wcbk-anchor'
                                               ) );
                        ?>
                    </td>
                    <td class="yith-wcbk-availability-range-column">
                        <div class="yith-wcbk-availability-range-column-ranges">
                            <div class="yith-wcbk-availability-range-column-ranges-date">

                                <?php
                                $printer->print_field( array(
                                                           'type'  => 'hidden',
                                                           'name'  => 'yith_booking_global_availability_range[type][]',
                                                           'value' => 'custom',
                                                       ) );
                                $printer->print_field( array(
                                                           'type'  => 'text',
                                                           'class' => 'yith-wcbk-date-input-field',
                                                           'title' => __( 'From', 'yith-booking-for-woocommerce' ),
                                                           'name'  => 'yith_booking_global_availability_range[from][]',

                                                       ) );
                                $printer->print_field( array(
                                                           'type'  => 'text',
                                                           'class' => 'yith-wcbk-date-input-field',
                                                           'title' => __( 'To', 'yith-booking-for-woocommerce' ),
                                                           'name'  => 'yith_booking_global_availability_range[to][]',

                                                       ) );
                                ?>

                            </div>
                            <div class="yith-wcbk-availability-range-column-ranges-month">
                                <?php
                                $printer->print_field( array(
                                                           'type'              => 'hidden',
                                                           'name'              => 'yith_booking_global_availability_range[type][]',
                                                           'value'             => 'month',
                                                           'custom_attributes' => 'disabled="disabled"',

                                                       ) );
                                $printer->print_field( array(
                                                           'type'              => 'select',
                                                           'class'             => 'yith-wcbk-month-range-select',
                                                           'title'             => __( 'From', 'yith-booking-for-woocommerce' ),
                                                           'name'              => 'yith_booking_global_availability_range[from][]',
                                                           'options'           => $months_array,
                                                           'custom_attributes' => 'disabled="disabled"',
                                                       ) );
                                $printer->print_field( array(
                                                           'type'              => 'select',
                                                           'class'             => 'yith-wcbk-month-range-select',
                                                           'title'             => __( 'To', 'yith-booking-for-woocommerce' ),
                                                           'name'              => 'yith_booking_global_availability_range[to][]',
                                                           'options'           => $months_array,
                                                           'custom_attributes' => 'disabled="disabled"',
                                                       ) );
                                ?>
                            </div>

                            <?php

                            $printer->print_field( array(
                                                       'type'            => 'onoff',
                                                       'container_class' => 'yith-wcbk-bookable-onoff',
                                                       'enabled_class'   => 'yith-wcbk-bookable-onoff-enabled',
                                                       'disabled_class'  => 'yith-wcbk-bookable-onoff-disabled',
                                                       'enabled_text'    => __( 'Bookable', 'yith-booking-for-woocommerce' ),
                                                       'disabled_text'   => __( 'Non-bookable', 'yith-booking-for-woocommerce' ),
                                                       'name'            => 'yith_booking_global_availability_range[bookable][]',
                                                       'value'           => 'yes',
                                                   ) );
                            ?>
                        </div>
                        <div class="yith-wcbk-availability-range-column-days hidden">
                            <?php
                            $printer->print_field( array(
                                                       'type'  => 'hidden',
                                                       'class' => 'yith-booking-global-availability-days-enabled',
                                                       'name'  => 'yith_booking_global_availability_range[days_enabled][]',
                                                       'value' => 'no',
                                                   ) );
                            foreach ( $days_array as $day_number => $day_name ) {
                                $day_name = substr( $day_name, 0, 3 );
                                $printer->print_field( array(
                                                           'type'            => 'onoff',
                                                           'container_class' => 'yith-wcbk-day-onoff',
                                                           'enabled_class'   => 'yith-wcbk-day-onoff-enabled',
                                                           'disabled_class'  => 'yith-wcbk-day-onoff-disabled',
                                                           'enabled_text'    => $day_name,
                                                           'disabled_text'   => $day_name,
                                                           'name'            => 'yith_booking_global_availability_range[days][' . $day_number . '][]',
                                                           'value'           => 'yes',
                                                       ) );
                            }
                            ?>
                        </div>
                    </td>
                    <td class="yith-wcbk-availability-actions-column">
                        <span class="dashicons dashicons-screenoptions yith-wcbk-availability-toggle-visible"></span>
                        <?php
                        $printer->print_field( array(
                                                   'type'  => 'dashicon',
                                                   'class' => 'dashicons-no-alt yith-wcbk-delete'
                                               ) );
                        ?>
                    </td>
                </tr>
                <?php foreach ( $global_availability as $availability_range ): ?>
                    <tr class="yith-wcbk-availability-row ">
                        <td class="yith-wcbk-availability-anchor">
                            <?php
                            $printer->print_field( array(
                                                       'type'  => 'dashicon',
                                                       'class' => 'dashicons-menu yith-wcbk-anchor'
                                                   ) );
                            ?>
                        </td>
                        <td class="yith-wcbk-availability-range-column">
                            <div class="yith-wcbk-availability-range-column-ranges">

                                <?php if ( 'custom' === $availability_range->get_type() ) : ?>
                                    <div class="yith-wcbk-availability-range-column-ranges-date">

                                        <?php
                                        $printer->print_field( array(
                                                                   'type'  => 'hidden',
                                                                   'name'  => 'yith_booking_global_availability_range[type][]',
                                                                   'value' => 'custom',
                                                               ) );
                                        $printer->print_field( array(
                                                                   'type'  => 'text',
                                                                   'class' => 'yith-wcbk-date-input-field yith-wcbk-admin-date-picker',
                                                                   'title' => __( 'From', 'yith-booking-for-woocommerce' ),
                                                                   'name'  => 'yith_booking_global_availability_range[from][]',
                                                                   'value' => $availability_range->get_from(),

                                                               ) );
                                        $printer->print_field( array(
                                                                   'type'  => 'text',
                                                                   'class' => 'yith-wcbk-date-input-field yith-wcbk-admin-date-picker',
                                                                   'title' => __( 'To', 'yith-booking-for-woocommerce' ),
                                                                   'name'  => 'yith_booking_global_availability_range[to][]',
                                                                   'value' => $availability_range->get_to(),
                                                               ) );
                                        ?>
                                    </div>
                                <?php elseif ( 'month' === $availability_range->get_type() ) : ?>
                                    <div class="yith-wcbk-availability-range-column-ranges-month">
                                        <?php
                                        $printer->print_field( array(
                                                                   'type'  => 'hidden',
                                                                   'name'  => 'yith_booking_global_availability_range[type][]',
                                                                   'value' => 'month',
                                                               ) );
                                        $printer->print_field( array(
                                                                   'type'    => 'select',
                                                                   'class'   => 'yith-wcbk-month-range-select',
                                                                   'title'   => __( 'From', 'yith-booking-for-woocommerce' ),
                                                                   'name'    => 'yith_booking_global_availability_range[from][]',
                                                                   'options' => $months_array,
                                                                   'value'   => $availability_range->get_from(),
                                                               ) );
                                        $printer->print_field( array(
                                                                   'type'    => 'select',
                                                                   'class'   => 'yith-wcbk-month-range-select',
                                                                   'title'   => __( 'To', 'yith-booking-for-woocommerce' ),
                                                                   'name'    => 'yith_booking_global_availability_range[to][]',
                                                                   'options' => $months_array,
                                                                   'value'   => $availability_range->get_to(),
                                                               ) );
                                        ?>
                                    </div>
                                <?php endif; ?>

                                <?php

                                $printer->print_field( array(
                                                           'type'            => 'onoff',
                                                           'container_class' => 'yith-wcbk-bookable-onoff',
                                                           'enabled_class'   => 'yith-wcbk-bookable-onoff-enabled',
                                                           'disabled_class'  => 'yith-wcbk-bookable-onoff-disabled',
                                                           'enabled_text'    => __( 'Bookable', 'yith-booking-for-woocommerce' ),
                                                           'disabled_text'   => __( 'Non-bookable', 'yith-booking-for-woocommerce' ),
                                                           'name'            => 'yith_booking_global_availability_range[bookable][]',
                                                           'value'           => $availability_range->get_bookable(),
                                                       ) );
                                ?>
                            </div>
                            <div class="yith-wcbk-availability-range-column-days <?php echo !$availability_range->has_days_enabled() ? 'hidden' : '' ?>">
                                <?php
                                $printer->print_field( array(
                                                           'type'  => 'hidden',
                                                           'class' => 'yith-booking-global-availability-days-enabled',
                                                           'name'  => 'yith_booking_global_availability_range[days_enabled][]',
                                                           'value' => $availability_range->get_days_enabled(),
                                                       ) );
                                foreach ( $days_array as $day_number => $day_name ) {
                                    $days     = $availability_range->get_days();
                                    $value    = $availability_range->has_days_enabled() && isset( $days[ $day_number ] ) ? $days[ $day_number ] : $availability_range->get_bookable();
                                    $day_name = substr( $day_name, 0, 3 );
                                    $printer->print_field( array(
                                                               'type'            => 'onoff',
                                                               'container_class' => 'yith-wcbk-day-onoff',
                                                               'enabled_class'   => 'yith-wcbk-day-onoff-enabled',
                                                               'disabled_class'  => 'yith-wcbk-day-onoff-disabled',
                                                               'enabled_text'    => $day_name,
                                                               'disabled_text'   => $day_name,
                                                               'name'            => 'yith_booking_global_availability_range[days][' . $day_number . '][]',
                                                               'value'           => $value,
                                                           ) );
                                }
                                ?>
                            </div>
                        </td>
                        <td class="yith-wcbk-availability-actions-column">
                            <span class="dashicons dashicons-screenoptions yith-wcbk-availability-toggle-visible"></span>
                            <?php
                            $printer->print_field( array(
                                                       'type'  => 'dashicon',
                                                       'class' => 'dashicons-no-alt yith-wcbk-delete'
                                                   ) );
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <div class="yith-wcbk-bottom-actions yith-wcbk-right">
            <input type="button" id="yith-wcbk-availability-add-date-range" class="button" value="<?php _e( 'Add Date Range', 'yith-booking-for-woocommerce' ); ?>">
            <input type="button" id="yith-wcbk-availability-add-month-range" class="button" value="<?php _e( 'Add Month Range', 'yith-booking-for-woocommerce' ); ?>">
        </div>


    </div>
    <div id="yith-wcbk-settings-tab-actions">
        <input type="submit" id="yith-wcbk-settings-tab-actions-save"
               class="button button-primary" value="<?php _e( 'Save', 'yith-booking-for-woocommerce' ) ?>">
    </div>
    <?php
    wp_nonce_field( 'yith_wcbk_settings_fields', 'yith_wcbk_nonce', false );
    ?>
</form>
