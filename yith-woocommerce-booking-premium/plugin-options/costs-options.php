<?php
// Exit if accessed directly
!defined( 'YITH_WCBK' ) && exit();

$tab = array(
    'costs' => array(
        'costs-tab' => array(
            'type' => 'custom_tab',
            'action' => 'yith_wcbk_print_global_costs_tab'
        )
    )
);

return apply_filters( 'yith_wcbk_panel_costs_options', $tab );