<?php
// Exit if accessed directly
!defined( 'YITH_WCBK' ) && exit();

$tab = array(
    'availability' => array(
        'availability-tab' => array(
            'type' => 'custom_tab',
            'action' => 'yith_wcbk_print_global_availability_tab'
        )
    )
);

return apply_filters( 'yith_wcbk_panel_availability_options', $tab );