<?php
/**
 * @var string $id
 * @var string $name
 * @var string $value
 * @var array  $options
 * @var string $default_text
 */
$default_text = !empty( $default_text ) ? $default_text : '';

foreach ( $options as $option_key => $option_data ) {
    $default_option_data    = array(
        'class' => 'yith-wcbk-on-off-advanced-' . $value,
        'label' => $default_text,
    );
    $option_data            = wp_parse_args( $option_data, $default_option_data );
    $options[ $option_key ] = $option_data;
}

$name_html = !empty( $id ) ? " name='{$id}'" : '';
$name_html = !empty( $name ) ? " name='{$name}'" : $name_html;
$id_html   = !empty( $id ) ? " id='{$id}'" : '';

$onoff_class       = !empty( $options[ $value ][ 'class' ] ) ? $options[ $value ][ 'class' ] : 'yith-wcbk-on-off-advanced-' . $value;
$class_html        = !empty( $class ) ? " class='yith-wcbk-on-off-advanced $onoff_class {$class}'" : "class='yith-wcbk-on-off-advanced $onoff_class'";
$container_class   = isset( $container_class ) ? $container_class : '';
$custom_attributes = ' ' . $custom_attributes;

$text = isset( $options[ $value ][ 'label' ] ) ? $options[ $value ][ 'label' ] : $default_text;

$data_html         = '';
$data[ 'options' ] = wp_json_encode( $options );
foreach ( $data as $data_key => $data_value ) {
    $data_html .= " data-{$data_key}='{$data_value}'";
}
?>
<span class="yith-wcbk-on-off-advanced-container <?php echo $container_class ?>">
    <input class="yith-wcbk-on-off-advanced-value" type="hidden" <?php echo $name_html; ?> value="<?php echo $value; ?>"/>
    <span <?php echo $id_html . $class_html . $custom_attributes . $data_html; ?> ><?php echo $text ?></span>
</span>