<?php
$enabled = $value === 'yes';
$value   = $enabled ? 'yes' : 'no';

$name_html         = !empty( $id ) ? " name='{$id}'" : '';
$name_html         = !empty( $name ) ? " name='{$name}'" : $name_html;
$id_html           = !empty( $id ) ? " id='{$id}'" : '';
$enabled_class     = isset( $enabled_class ) ? $enabled_class : 'dashicons dashicons-yes';
$disabled_class    = isset( $disabled_class ) ? $disabled_class : 'dashicons dashicons-no';
$onoff_class       = $enabled ? $enabled_class : $disabled_class;
$class_html        = !empty( $class ) ? " class='yith-wcbk-on-off $onoff_class {$class}'" : "class='yith-wcbk-on-off $onoff_class'";
$container_class   = isset( $container_class ) ? $container_class : '';
$custom_attributes = ' ' . $custom_attributes;

$enabled_text  = isset( $enabled_text ) ? $enabled_text : '';
$disabled_text = isset( $disabled_text ) ? $disabled_text : '';

$text = $enabled ? $enabled_text : $disabled_text;

$data_html                = '';
$data[ 'enabled_class' ]  = $enabled_class;
$data[ 'disabled_class' ] = $disabled_class;
$data[ 'enabled_text' ]   = $enabled_text;
$data[ 'disabled_text' ]  = $disabled_text;
foreach ( $data as $data_key => $data_value ) {
    $data_html .= " data-{$data_key}='{$data_value}'";
}
?>
<span class="yith-wcbk-on-off-container <?php echo $container_class ?>">
    <input class="yith-wcbk-on-off-value" type="hidden" <?php echo $name_html; ?> value="<?php echo $value; ?>" />
    <span <?php echo $id_html . $class_html . $custom_attributes . $data_html; ?> ><?php echo $text ?></span>
</span>