<?php
/**
 * Booking Search Form Results List Template
 *
 * Shows list of booking search form results
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/booking/search-form/results/results-list.php.
 *
 * @var WP_Query $products
 * @var array    $booking_request
 * @var int      $current_page
 * @var array    $product_ids
 */

!defined( 'YITH_WCBK' ) && exit;
?>

<?php if ( $products->have_posts() ) : ?>

    <?php while ( $products->have_posts() ) : $products->the_post(); ?>
        <?php
        /**
         * @var WC_Product_Booking $product
         */
        global $product;

        $booking_data = array();

        if ( !empty( $booking_request[ 'from' ] ) && !empty( $booking_request[ 'to' ] ) ) {
            $booking_request[ 'add-to-cart' ]                                           = $product->get_id();
            $booking_data                                                               = YITH_WCBK_Cart::get_booking_data_from_request( $booking_request );
            $booking_data[ YITH_WCBK_Search_Form_Helper::RESULT_KEY_IN_BOOKING_DATA ] = true;
            $booking_data_for_price                                                     = $booking_data;

            if ( $product->has_person_types() && isset( $booking_data_for_price[ 'person_types' ] ) ) {
                $booking_data_for_price[ 'person_types' ] = yith_wcbk_parse_booking_person_types_array( $booking_data_for_price[ 'person_types' ] );
            }
            $the_price = $product->calculate_price( $booking_data_for_price );
            yit_set_prop( $product, 'price', $the_price );
        }


        wc_get_template( 'booking/search-form/results/single.php', compact( 'product', 'booking_data', 'the_price' ), '', YITH_WCBK_TEMPLATE_PATH );
        ?>

    <?php endwhile; // end of the loop. ?>

    <?php wp_reset_query(); ?>

<?php endif; ?>

