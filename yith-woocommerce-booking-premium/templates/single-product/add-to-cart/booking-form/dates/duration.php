<?php
/**
 * Duration Field in booking form
 *
 * @author        Leanza Francesco <leanzafrancesco@gmail.com>
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/booking-form/dates/duration.php
 *
 * @var WC_Product_Booking $product
 */

!defined( 'ABSPATH' ) && exit; // Exit if accessed directly

$default_duration = YITH_WCBK_Search_Form_Helper::get_searched_value_for_field( 'duration' );

$duration        = $product->get_booking_prop( 'duration' );
$duration_unit   = $product->get_duration_unit();
$is_fixed_blocks = $product->is_type_fixed_blocks();
$duration_label  = yith_wcbk_get_product_duration_label( $duration, $duration_unit, $is_fixed_blocks );
$duration_label  = apply_filters( 'yith_wcbk_booking_form_dates_duration_label_html', $duration_label, $product );

$min = $product->get_min_duration();
$max = $product->get_max_duration();

$custom_attributes = "step='1' min='{$min}'";
$custom_attributes .= $max > 0 ? " max='{$max}'" : '';
$custom_attributes .= ' pattern="[0-9]*" inputmode="numeric"';

$id   = 'yith-wcbk-booking-duration-' . $product->get_id();
$type = !$is_fixed_blocks ? 'number' : 'hidden';
?>

<div class="yith-wcbk-form-section yith-wcbk-form-section-duration">
    <label for="<?php echo $id ?>" class='yith-wcbk-booking-form__label'><?php _e( 'Duration', 'yith-booking-for-woocommerce' ) ?></label>
    <?php
    yith_wcbk_print_field( array(
                               'type'              => !$is_fixed_blocks ? 'number' : 'hidden',
                               'id'                => 'yith-wcbk-booking-duration',
                               'name'              => 'duration',
                               'custom_attributes' => $custom_attributes,
                               'value'             => max( $min, $default_duration ),
                               'class'             => 'yith-wcbk-booking-duration yith-wcbk-number-minifield',
                           ) );
    ?>
    <?php echo $duration_label ?>
</div>