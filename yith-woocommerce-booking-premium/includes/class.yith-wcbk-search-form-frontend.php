<?php
!defined( 'YITH_WCBK' ) && exit; // Exit if accessed directly

if ( !class_exists( 'YITH_WCBK_Search_Form_Frontend' ) ) {
    /**
     * Class YITH_WCBK_Search_Form_Frontend
     * handle Booking Forms in frontend
     *
     * @author Leanza Francesco <leanzafrancesco@gmail.com>
     */
    class YITH_WCBK_Search_Form_Frontend {
        /** @var YITH_WCBK_Search_Form_Frontend */
        private static $_instance;

        /**
         * Singleton implementation
         *
         * @return YITH_WCBK_Search_Form_Frontend
         */
        public static function get_instance() {
            return !is_null( self::$_instance ) ? self::$_instance : self::$_instance = new self();
        }

        /**
         * YITH_WCBK_Search_Form_Frontend constructor.
         */
        private function __construct() {
            add_action( 'yith_wcbk_booking_search_form_print_field', array( $this, 'print_field' ), 10, 3 );

            add_action( 'pre_get_posts', array( $this, 'filter_search_results_in_shop' ) );
        }

        /**
         * filter search results in shop
         *
         * @param WP_Query $query
         */
        public function filter_search_results_in_shop( $query ) {
            if ( $query->is_main_query() && isset( $_REQUEST[ 'yith-wcbk-booking-search' ] ) && $_REQUEST[ 'yith-wcbk-booking-search' ] === 'search-bookings' ) {
                $product_ids = YITH_WCBK()->search_form_helper->search_booking_products( $_REQUEST );

                if ( !$product_ids )
                    $product_ids = array( 0 );

                $query->set( 'post__in', $product_ids );
            }
        }

        /**
         * @param string                $field_name
         * @param array                 $field_data
         * @param YITH_WCBK_Search_Form $search_form
         */
        public function print_field( $field_name, $field_data, $search_form ) {
            $template = $field_name;

            if ( !empty( $field_data[ 'type' ] ) ) {
                $template .= '-' . $field_data[ 'type' ];
            }

            $template .= '.php';

            $args = array(
                'field_name'  => $field_name,
                'field_data'  => $field_data,
                'search_form' => $search_form,
            );

            wc_get_template( 'booking/search-form/fields/' . $template, $args, '', YITH_WCBK_TEMPLATE_PATH );
        }
    }
}