<?php
!defined( 'YITH_WCBK' ) && exit; // Exit if accessed directly

if ( !class_exists( 'YITH_WCBK_Product_Post_Type_Admin' ) ) {
    /**
     * Class YITH_WCBK_Product_Post_Type_Admin
     * handle the Booking product post type in Admin
     *
     * @author Leanza Francesco <leanzafrancesco@gmail.com>
     */
    class YITH_WCBK_Product_Post_Type_Admin {
        /** @var YITH_WCBK_Product_Post_Type_Admin */
        private static $_instance;

        /**
         * Booking product type
         *
         * @var string
         * @static
         */
        public static $prod_type = 'booking';

        /** @var array product meta array */
        public $product_meta_array = array();

        /**
         * Singleton implementation
         *
         * @return YITH_WCBK_Product_Post_Type_Admin
         */
        public static function get_instance() {
            return !is_null( self::$_instance ) ? self::$_instance : self::$_instance = new self();
        }

        /**
         * YITH_WCBK_Product_Post_Type_Admin constructor.
         */
        private function __construct() {
            $this->populate_meta_array();

            // Add Booking product to WC product type selector
            add_filter( 'product_type_selector', array( $this, 'product_type_selector' ) );
            add_filter( 'product_type_options', array( $this, 'product_type_options' ) );

            // add tabs for product booking
            add_filter( 'woocommerce_product_data_tabs', array( $this, 'product_booking_tabs' ) );

            // Add options to general product data tab
            add_action( 'woocommerce_product_options_general_product_data', array( $this, 'add_options_to_general_product_data' ) );
            add_action( 'woocommerce_product_data_panels', array( $this, 'add_product_data_panels' ) );

            // save product meta
            add_action( 'woocommerce_process_product_meta_' . self::$prod_type, array( $this, 'save_product_meta' ) );

            // Remove Booking Services Metabox for products
            add_action( 'add_meta_boxes', array( $this, 'manage_meta_boxes' ) );

            // Export action
            add_filter( 'post_row_actions', array( $this, 'customize_booking_product_row_actions' ), 10, 2 );
        }

        /**
         * customize Booking Product Actions
         *
         * @param array   $actions An array of row action links. Defaults are
         *                         'Edit', 'Quick Edit', 'Restore, 'Trash',
         *                         'Delete Permanently', 'Preview', and 'View'.
         * @param WP_Post $post    The post object.
         *
         * @since       2.0.0
         * @author      Leanza Francesco <leanzafrancesco@gmail.com>
         * @return array
         */
        public function customize_booking_product_row_actions( $actions, $post ) {
            if ( yith_wcbk_is_booking_product( $post ) ) {
                /** @var WC_Product_Booking $product */
                $product = wc_get_product( $post );

                $booking_actions = array(
                    'yith_wcbk_export_future_ics' => array(
                        'label' => __( 'Export Future ICS', 'yith-booking-for-woocommerce' ),
                        'url'   => wp_nonce_url( add_query_arg( array( 'yith_wcbk_exporter_action' => 'export_future_ics', 'product_id' => $post->ID, 'key' => $product->get_external_calendars_key() ) ),
                                                 'export', 'yith_wcbk_exporter_nonce' ),
                    ),
                    'yith_wcbk_view_calendar'     => array(
                        'label' => __( 'Booking Calendar', 'yith-booking-for-woocommerce' ),
                        'url'   => $product->get_admin_calendar_url(),
                    ),
                );

                foreach ( $booking_actions as $key => $action ) {
                    $actions[ $key ] = "<a href='{$action['url']}'>{$action['label']}</a>";
                }
            }

            return $actions;
        }

        /**
         * Remove Booking Services Metabox for products
         *
         * @param string $post_type Post type.
         */
        public function manage_meta_boxes( $post_type ) {
            remove_meta_box( YITH_WCBK_Post_Types::$service_tax . 'div', 'product', 'side' );
        }

        /**
         * Add options to general product data tab
         */
        public function add_options_to_general_product_data() {
            global $post;

            $post_type = get_post_type( $post->ID );
            if ( $post_type != 'product' )
                return;

            $args = array(
                'prod_type' => self::$prod_type,
                'post_id'   => $post->ID,
            );
            extract( $args );
            include( YITH_WCBK_VIEWS_PATH . 'product-tabs/html-general-tab.php' );
        }

        /**
         * add data panels to products
         */
        public function add_product_data_panels() {
            global $post;

            $tabs = array(
                'costs'     => 'yith_booking_costs_tab',
                'available' => 'yith_booking_available_tab',
                'persons'   => 'yith_booking_persons_tab',
                'services'  => 'yith_booking_services_tab',
                'sync'      => 'yith_booking_sync_tab',
            );

            $post_id   = $post->ID;
            $prod_type = self::$prod_type;

            foreach ( $tabs as $key => $tab_id ) {
                echo "<div id='{$tab_id}' class='panel woocommerce_options_panel'>";
                include( YITH_WCBK_VIEWS_PATH . 'product-tabs/html-' . $key . '-tab.php' );
                echo '</div>';
            }
        }

        /**
         * get the default meta array
         *
         * @return array
         */
        public static function get_default_meta_array() {
            return array(
                // ------ Tab General --------------------------------------------------
                '_yith_booking_duration_type'                    => '',
                '_yith_booking_duration'                         => 'int',
                '_yith_booking_duration_unit'                    => '',
                '_yith_booking_minimum_duration'                 => 'int',
                '_yith_booking_maximum_duration'                 => 'zero_int',
                '_yith_booking_enable_calendar_range_picker'     => 'checkbox',
                '_yith_booking_all_day'                          => 'checkbox',
                '_yith_booking_request_confirmation'             => 'checkbox',
                '_yith_booking_can_be_cancelled'                 => 'checkbox',
                '_yith_booking_cancelled_duration'               => 'zero_int',
                '_yith_booking_cancelled_unit'                   => '',
                '_yith_booking_location'                         => 'location',
                // ------ Tab Available --------------------------------------------------
                '_yith_booking_max_per_block'                    => 'zero_int',
                '_yith_booking_allow_after'                      => 'zero_int',
                '_yith_booking_allow_after_unit'                 => '',
                '_yith_booking_allow_until'                      => 'int',
                '_yith_booking_allow_until_unit'                 => '',
                '_yith_booking_buffer'                           => 'zero_int',
                '_yith_booking_checkin'                          => '',
                '_yith_booking_checkout'                         => '',
                '_yith_booking_allowed_start_days'               => '',
                '_yith_booking_default_start_date'               => '',
                '_yith_booking_default_start_date_custom'        => '',
                '_yith_booking_default_start_time'               => '',
                '_yith_booking_daily_start_time'                 => '',
                '_yith_booking_time_increment_based_on_duration' => 'checkbox',
                '_yith_booking_availability_range'               => 'availability_from_to',
                '_yith_booking_costs_range'                      => 'costs_from_to',
                // ------ Tab Costs --------------------------------------------------
                '_yith_booking_base_cost'                        => 'price',
                '_yith_booking_block_cost'                       => 'price',
                // ------ Tab Persons --------------------------------------------------
                '_yith_booking_has_persons'                      => 'checkbox',
                '_yith_booking_min_persons'                      => 'int',
                '_yith_booking_max_persons'                      => 'zero_int',
                '_yith_booking_multiply_costs_by_persons'        => 'checkbox',
                '_yith_booking_count_persons_as_bookings'        => 'checkbox',
                '_yith_booking_enable_person_types'              => 'checkbox',
                '_yith_booking_person_types'                     => 'person_types',
                // ------ Tab Services --------------------------------------------------
                '_yith_booking_services'                         => 'services',
                // ------ Tab Sync --------------------------------------------------
                '_yith_booking_external_calendars'               => 'multi_array',
                '_yith_booking_external_calendars_key'           => '',
            );
        }

        /**
         * Populate the meta array
         *
         * @return void
         */
        public function populate_meta_array() {
            $this->product_meta_array = self::get_default_meta_array();
        }

        /**
         * Add tabs for booking products
         *
         * @param $tabs
         *
         * @return array
         */
        public function product_booking_tabs( $tabs ) {
            $new_tabs = array(
                'yith_booking_costs'     => array(
                    'label'  => __( 'Booking Costs', 'yith-booking-for-woocommerce' ),
                    'target' => 'yith_booking_costs_tab',
                    'class'  => array( 'show_if_' . self::$prod_type ),
                ),
                'yith_booking_available' => array(
                    'label'  => __( 'Booking Availability', 'yith-booking-for-woocommerce' ),
                    'target' => 'yith_booking_available_tab',
                    'class'  => array( 'show_if_' . self::$prod_type ),
                ),
                'yith_booking_persons'   => array(
                    'label'  => __( 'Booking People', 'yith-booking-for-woocommerce' ),
                    'target' => 'yith_booking_persons_tab',
                    'class'  => array( 'show_if_' . self::$prod_type ),
                ),
                'yith_booking_services'  => array(
                    'label'  => __( 'Booking Services', 'yith-booking-for-woocommerce' ),
                    'target' => 'yith_booking_services_tab',
                    'class'  => array( 'show_if_' . self::$prod_type ),
                ),
                'yith_booking_sync'      => array(
                    'label'  => __( 'Booking Sync', 'yith-booking-for-woocommerce' ),
                    'target' => 'yith_booking_sync_tab',
                    'class'  => array( 'show_if_' . self::$prod_type ),
                ),
            );

            $tabs = array_merge( $tabs, $new_tabs );

            return $tabs;
        }

        /**
         * add Booking Product type in product type selector
         *
         * @param array $types
         *
         * @return array
         */
        public function product_type_selector( $types ) {
            $types[ self::$prod_type ] = _x( 'Booking Product', 'Admin: type of product', 'yith-booking-for-woocommerce' );

            return $types;
        }

        /**
         * show "virtual" checkbox for Booking products
         *
         * @param array $options
         *
         * @since 2.0.3
         * @return array
         */
        public function product_type_options( $options ) {
            $options[ 'virtual' ][ 'wrapper_class' ] .= ' show_if_' . self::$prod_type;
            return $options;
        }

        /**
         * Save Product Meta
         *
         * @param int $post_id
         *
         */
        public function save_product_meta( $post_id ) {
            // empty cache
            YITH_WCBK_Cache()->delete_product_data( $post_id );

            foreach ( $this->product_meta_array as $key => $type ) {
                $value = '';
                switch ( $type ) {
                    case 'checkbox':
                        $value = isset( $_POST[ $key ] ) ? 'yes' : 'no';
                        break;
                    case 'int':
                        $value = ( isset( $_POST[ $key ] ) && $_POST[ $key ] >= 1 ) ? intval( $_POST[ $key ] ) : 1;
                        break;
                    case 'zero_int':
                        $value = ( isset( $_POST[ $key ] ) && $_POST[ $key ] >= 0 ) ? intval( $_POST[ $key ] ) : 0;
                        break;
                    case 'float':
                        $value = ( isset( $_POST[ $key ] ) && $_POST[ $key ] >= 0 ) ? floatval( $_POST[ $key ] ) : 0;
                        break;
                    case 'price':
                        $value = ( isset( $_POST[ $key ] ) ) ? wc_format_decimal( $_POST[ $key ] ) : 0;
                        break;
                    case 'availability_from_to':
                        $ranges         = isset( $_POST[ $key ] ) ? $_POST[ $key ] : array();
                        $type_array     = isset( $ranges[ 'type' ] ) ? $ranges[ 'type' ] : array();
                        $from_array     = isset( $ranges[ 'from' ] ) ? $ranges[ 'from' ] : array();
                        $to_array       = isset( $ranges[ 'to' ] ) ? $ranges[ 'to' ] : array();
                        $bookable_array = isset( $ranges[ 'bookable' ] ) ? $ranges[ 'bookable' ] : array();
                        $days_enabled   = isset( $ranges[ 'days_enabled' ] ) ? $ranges[ 'days_enabled' ] : array();
                        $days           = isset( $ranges[ 'days' ] ) ? $ranges[ 'days' ] : array();

                        $times_enabled = isset( $ranges[ 'times_enabled' ] ) ? $ranges[ 'times_enabled' ] : array();
                        $day_time_from = isset( $ranges[ 'day_time_from' ] ) ? $ranges[ 'day_time_from' ] : array();
                        $day_time_to   = isset( $ranges[ 'day_time_to' ] ) ? $ranges[ 'day_time_to' ] : array();

                        $availability_size = sizeof( $type_array );
                        $value             = array();
                        for ( $i = 1; $i < $availability_size; $i++ ) {
                            if ( !empty( $type_array[ $i ] ) && !empty( $from_array[ $i ] ) && !empty( $to_array[ $i ] ) && !empty( $bookable_array[ $i ] ) ) {
                                $current_range = array(
                                    'type'          => $type_array[ $i ],
                                    'from'          => $from_array[ $i ],
                                    'to'            => $to_array[ $i ],
                                    'bookable'      => $bookable_array[ $i ],
                                    'days_enabled'  => $days_enabled[ $i ],
                                    'times_enabled' => $times_enabled[ $i ],
                                    'day_time_from' => array(),
                                    'day_time_to'   => array(),
                                    'days'          => array()
                                );

                                if ( $days_enabled[ $i ] === 'yes' ) {
                                    $current_range_days = array();
                                    for ( $j = 1; $j <= 7; $j++ ) {
                                        $current_range_days[ $j ] = isset( $days[ $j ][ $i ] ) ? $days[ $j ][ $i ] : $bookable_array[ $i ];
                                    }

                                    $current_range[ 'days' ] = $current_range_days;

                                    if ( $times_enabled[ $i ] === 'yes' ) {
                                        $current_day_time_from = array();
                                        $current_day_time_to   = array();
                                        for ( $j = 1; $j <= 7; $j++ ) {
                                            $current_day_time_from[ $j ] = isset( $day_time_from[ $j ][ $i ] ) ? $day_time_from[ $j ][ $i ] : '00:00';
                                            $current_day_time_to[ $j ]   = isset( $day_time_to[ $j ][ $i ] ) ? $day_time_to[ $j ][ $i ] : '00:00';
                                        }

                                        $current_range[ 'day_time_from' ] = $current_day_time_from;
                                        $current_range[ 'day_time_to' ]   = $current_day_time_to;
                                    }
                                }

                                $value[] = (object)$current_range;
                            }
                        }
                        break;
                    case 'costs_from_to':
                        $value               = isset( $_POST[ $key ] ) ? $_POST[ $key ] : array();
                        $type_array          = isset( $value[ 'type' ] ) ? $value[ 'type' ] : array();
                        $from_array          = isset( $value[ 'from' ] ) ? $value[ 'from' ] : array();
                        $to_array            = isset( $value[ 'to' ] ) ? $value[ 'to' ] : array();
                        $base_cost_operator  = isset( $value[ 'base_cost_operator' ] ) ? $value[ 'base_cost_operator' ] : array();
                        $base_cost           = isset( $value[ 'base_cost' ] ) ? $value[ 'base_cost' ] : array();
                        $block_cost_operator = isset( $value[ 'block_cost_operator' ] ) ? $value[ 'block_cost_operator' ] : array();
                        $block_cost          = isset( $value[ 'block_cost' ] ) ? $value[ 'block_cost' ] : array();

                        $costs_size = sizeof( $type_array );
                        $value      = array();
                        for ( $i = 1; $i < $costs_size; $i++ ) {
                            if ( isset( $type_array[ $i ] ) && isset( $from_array[ $i ] ) && isset( $to_array[ $i ] ) && isset( $base_cost_operator[ $i ] ) && isset( $block_cost_operator[ $i ] ) ) {
                                $single_base_cost  = !empty( $base_cost[ $i ] ) ? $base_cost[ $i ] : '';
                                $single_block_cost = !empty( $block_cost[ $i ] ) ? $block_cost[ $i ] : '';


                                $value[] = array(
                                    'type'                => $type_array[ $i ],
                                    'from'                => $from_array[ $i ],
                                    'to'                  => $to_array[ $i ],
                                    'base_cost_operator'  => $base_cost_operator[ $i ],
                                    'base_cost'           => yith_wcbk_format_decimals_with_variables( $single_base_cost ),
                                    'block_cost_operator' => $block_cost_operator[ $i ],
                                    'block_cost'          => yith_wcbk_format_decimals_with_variables( $single_block_cost ),
                                );
                            }
                        }
                        break;

                    case 'person_types':
                        $value = isset( $_POST[ $key ] ) ? $_POST[ $key ] : array();
                        foreach ( $value as $_key => $_value ) {
                            if ( isset( $_value[ 'base_cost' ] ) ) {
                                $value[ $_key ][ 'base_cost' ] = wc_format_decimal( $_value[ 'base_cost' ] );
                            }
                            if ( isset( $_value[ 'block_cost' ] ) ) {
                                $value[ $_key ][ 'block_cost' ] = wc_format_decimal( $_value[ 'block_cost' ] );
                            }
                        }
                        break;
                    case 'array':
                        $value = isset( $_POST[ $key ] ) ? $_POST[ $key ] : array();
                        break;
                    case 'services':
                        $values = isset( $_POST[ $key ] ) ? $_POST[ $key ] : array();
                        $values = array_map( 'intval', $values );
                        $values = array_unique( $values );
                        wp_set_object_terms( $post_id, $values, YITH_WCBK_Post_Types::$service_tax );
                        continue;
                        break;

                    case 'multi_array':
                        $post_value = !empty( $_POST[ $key ] ) ? $_POST[ $key ] : array();
                        $size       = !!$post_value ? sizeof( current( $post_value ) ) : 0;

                        $value = array();
                        if ( $size > 0 ) {
                            for ( $i = 0; $i < $size; $i++ ) {
                                $value_to_add = array();
                                foreach ( $post_value as $current_key => $current_value ) {
                                    if ( isset( $current_value[ $i ] ) ) {
                                        $value_to_add[ $current_key ] = $current_value[ $i ];
                                    } else {
                                        $value_to_add = array();
                                        break;
                                    }
                                }
                                if ( $value_to_add ) {
                                    $value[] = $value_to_add;
                                }
                            }
                        }
                        break;
                    default:
                        $value = ( isset( $_POST[ $key ] ) ) ? wc_clean( $_POST[ $key ] ) : '';
                        break;
                }
                if ( $type != 'services' )
                    update_post_meta( $post_id, $key, $value );

                // Save latitude e longitude of booking location
                if ( $type === 'location' && !!$value ) {
                    $location_info = YITH_WCBK()->maps->get_location_by_address( $value );
                    if ( isset( $location_info[ 'lat' ] ) && isset( $location_info[ 'lng' ] ) ) {
                        update_post_meta( $post_id, $key . '_lat', $location_info[ 'lat' ] );
                        update_post_meta( $post_id, $key . '_lng', $location_info[ 'lng' ] );
                    } else {
                        update_post_meta( $post_id, $key . '_lat', '' );
                        update_post_meta( $post_id, $key . '_lng', '' );
                    }
                }
            }

            /** @var WC_Product_Booking $product */
            $product = wc_get_product( $post_id );

            // sync price
            $product->sync_price();

            // delete the last_sync to regenerate externals
            $product->delete_external_calendars_last_sync();

            // Trigger background update data
            YITH_WCBK()->background_processes->schedule_product_data_update( $post_id );
        }

        /**
         * Return true if the product is Booking Product
         *
         * @param bool|int|WP_Post|WC_Product $_product
         *
         * @return bool
         */
        public static function is_booking( $_product = false ) {
            $product_id = false;
            if ( $_product instanceof WC_Product ) {
                return $_product->is_type( self::$prod_type );
            } elseif ( false === $_product ) {
                $_product = $GLOBALS[ 'product' ];
                if ( $_product && $_product instanceof WC_Product ) {
                    return $_product->is_type( self::$prod_type );
                }
                $_product = $GLOBALS[ 'post' ];
                if ( !$_product || !$_product instanceof WP_Post )
                    return false;
                $product_id = $_product->ID;
            } elseif ( is_numeric( $_product ) ) {
                $product_id = absint( $_product );
            } elseif ( $_product instanceof WP_Post ) {
                $product_id = $_product->ID;
            }

            if ( !$product_id )
                return false;

            $terms        = get_the_terms( $product_id, 'product_type' );
            $product_type = !empty( $terms ) && isset( current( $terms )->name ) ? sanitize_title( current( $terms )->name ) : 'simple';

            return self::$prod_type === $product_type;
        }
    }
}
