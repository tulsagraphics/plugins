<?php
!defined( 'YITH_WCBK' ) && exit; // Exit if accessed directly

if ( !class_exists( 'YITH_WCBK_Cart' ) ) {
    /**
     * Class YITH_WCBK_Cart
     * handle add-to-cart processes for Booking products
     *
     * @author Leanza Francesco <leanzafrancesco@gmail.com>
     */
    class YITH_WCBK_Cart {
        /** @var YITH_WCBK_Cart */
        private static $_instance;

        /**
         * Singleton implementation
         *
         * @return YITH_WCBK_Cart
         */
        public static function get_instance() {
            return !is_null( self::$_instance ) ? self::$_instance : self::$_instance = new self();
        }

        /**
         * YITH_WCBK_Cart constructor.
         */
        private function __construct() {
            add_filter( 'woocommerce_add_cart_item_data', array( $this, 'woocommerce_add_cart_item_data' ), 10, 2 );
            add_filter( 'woocommerce_get_cart_item_from_session', array( $this, 'woocommerce_get_cart_item_from_session' ), 99, 3 );
            add_filter( 'woocommerce_add_cart_item', array( $this, 'woocommerce_add_cart_item' ), 10, 2 );

            add_filter( 'woocommerce_get_item_data', array( $this, 'woocommerce_get_item_data' ), 10, 2 );

            add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'prevent_add_to_cart_if_request_confirm' ), 10, 3 );
            add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'add_to_cart_validation' ), 10, 6 );

            /**
             * TODO: remove this commented code and the "remove_non_available_booking" method
             * this code removed non-available booking products on cart loading from session
             * and it caused issues with some payment methods, such us PayPal and others
             * Note: this code was leaved here only for testing purpose (since 2.0.1)
             *
             * add_action( 'woocommerce_cart_loaded_from_session', array( $this, 'remove_non_available_booking' ), 10, 1 );
             */

            add_action( 'woocommerce_before_checkout_process', array( $this, 'check_booking_availability_before_checkout' ) );
        }

        /**
         * get booking data from Request Form
         *
         * @param array $request
         *
         * @return array
         */
        public static function get_booking_data_from_request( $request = array() ) {
            $request     = empty( $request ) ? $_REQUEST : $request;
            $request     = apply_filters( 'yith_wcbk_cart_booking_data_request', $request );
            $date_helper = YITH_WCBK_Date_Helper();

            $booking_fields = array(
                'add-to-cart'              => 0,
                'from'                     => 'now',
                'to'                       => '',
                'duration'                 => 1,
                'persons'                  => 1,
                'person_types'             => array(),
                'booking_services'         => array(),
                'booking_service_quantities' => array()
            );

            $booking_data = array();
            foreach ( $booking_fields as $field => $default ) {
                $booking_data[ $field ] = !empty( $request[ $field ] ) ? $request[ $field ] : $default;
            }

            $product_id = absint( $booking_data[ 'add-to-cart' ] );
            if ( !$product_id && isset( $request[ 'product_id' ] ) ) {
                $product_id = absint( $request[ 'product_id' ] );

            }

            /** @var WC_Product_Booking $product */
            $product = wc_get_product( $product_id );
            if ( !$product || !YITH_WCBK_Product_Post_Type_Admin::is_booking( $product_id ) )
                return array();

            if ( !is_numeric( $booking_data[ 'from' ] ) )
                $booking_data[ 'from' ] = strtotime( $booking_data[ 'from' ] );

            if ( empty( $request[ 'to' ] ) ) {
                $from                       = $booking_data[ 'from' ];
                $duration                   = absint( $booking_data[ 'duration' ] ) * $product->get_booking_prop( 'duration' );
                $booking_data[ 'to' ]       = $date_helper->get_time_sum( $from, $duration, $product->get_duration_unit() );
                $booking_data[ 'duration' ] = $duration;
                if ( $product->is_all_day() ) {
                    $booking_data[ 'to' ] = $date_helper->get_time_sum( $booking_data[ 'to' ], -1, 'day' );
                }
            } else {
                if ( !is_numeric( $booking_data[ 'to' ] ) )
                    $booking_data[ 'to' ] = strtotime( $booking_data[ 'to' ] );

                if ( $product->is_all_day() ) {
                    $booking_data[ 'to' ] = $date_helper->get_time_sum( $booking_data[ 'to' ], 1, 'day' );
                }

                $booking_data[ 'duration' ] = $date_helper->get_time_diff( $booking_data[ 'from' ], $booking_data[ 'to' ], $product->get_duration_unit() );

                if ( $product->is_all_day() ) {
                    $booking_data[ 'to' ] -= 1;
                }
            }

            if ( !empty( $request[ 'person_types' ] ) ) {
                $persons = 0;
                foreach ( $request[ 'person_types' ] as $person_type_id => $number ) {
                    $persons += absint( $number );
                }
                $booking_data[ 'persons' ] = $persons;
            }

            $services          = $product->get_services();
            $selected_services = $booking_data[ 'booking_services' ];
            if ( $services && is_array( $services ) ) {
                $all_services = array();
                foreach ( $services as $service_id ) {
                    $service = yith_get_booking_service( $service_id );
                    if ( !$service->is_valid() ) {
                        continue;
                    }

                    if ( $service->is_optional() && !in_array( $service_id, $selected_services ) )
                        continue;

                    $all_services[] = $service_id;
                }
                $booking_data[ 'booking_services' ] = $all_services;
            }

            unset( $booking_data[ 'add-to-cart' ] );


            return apply_filters( 'yith_wcbk_cart_get_booking_data_from_request', $booking_data, $request );
        }

        /**
         * Bookings that require admin confirmation cannot be added to the cart
         *
         * @param bool $passed_validation
         * @param int  $product_id
         * @param int  $quantity
         *
         * @return bool
         */
        public function prevent_add_to_cart_if_request_confirm( $passed_validation, $product_id, $quantity ) {
            if ( YITH_WCBK_Product_Post_Type_Admin::is_booking( $product_id ) ) {
                /** @var WC_Product_Booking $product */
                $product = wc_get_product( $product_id );
                if ( !$product || ( $product->is_requested_confirmation() && wp_verify_nonce( $_REQUEST[ '_wpnonce' ], $product_id ) === false ) )
                    return false;
            }

            return $passed_validation;
        }

        /**
         * Add to cart validation for Booking Products
         *
         * @param        $passed_validation
         * @param        $product_id
         * @param        $quantity
         *
         * @param string $variation_id
         * @param array  $variations
         * @param array  $cart_item_data
         *
         * @return bool
         */
        public function add_to_cart_validation( $passed_validation, $product_id, $quantity, $variation_id = '', $variations = array(), $cart_item_data = array() ) {
            if ( YITH_WCBK_Product_Post_Type_Admin::is_booking( $product_id ) ) {
                $product_id = apply_filters( 'yith_wcbk_booking_product_id_to_translate', $product_id );

                /**  @var WC_Product_Booking $product */
                $product = wc_get_product( $product_id );

                if ( $product ) {
                    // get the request from cart_item_data; if it's not set, get it by $_REQUEST
                    $request      = !empty( $cart_item_data[ 'yith_booking_request' ] ) ? $cart_item_data[ 'yith_booking_request' ] : false;
                    $booking_data = self::get_booking_data_from_request( $request );

                    $r_persons                 = max( 1, absint( $booking_data[ 'persons' ] ) );
                    $min_persons               = $product->get_booking_prop( 'min_persons' );
                    $max_persons               = $product->get_booking_prop( 'max_persons' );
                    $count_persons_as_bookings = $product->has_count_persons_as_bookings_enabled();

                    if ( !$product->is_available( array(
                                                      'from'    => $booking_data[ 'from' ],
                                                      'to'      => $booking_data[ 'to' ],
                                                      'persons' => $r_persons
                                                  ) )
                    ) {
                        wc_add_notice( sprintf( __( '%s is not available', 'yith-booking-for-woocommerce' ), $product->get_title() ), 'error' );
                        $passed_validation = false;
                    }

                    if ( $r_persons < $min_persons ) {
                        wc_add_notice( sprintf( __( 'Minimum number of people: %s', 'yith-booking-for-woocommerce' ), $min_persons ), 'error' );
                        $passed_validation = false;
                    }

                    if ( $max_persons > 0 && $r_persons > $max_persons ) {
                        wc_add_notice( sprintf( __( 'Maximum number of people: %s', 'yith-booking-for-woocommerce' ), $max_persons ), 'error' );
                        $passed_validation = false;
                    }

                    if ( $passed_validation ) {
                        // check if there are booking products already added to the cart in the same dates
                        $bookings_added_to_cart_in_same_dates = $this->count_added_to_cart_bookings_in_period( array(
                                                                                                                   'product_id'                => $product_id,
                                                                                                                   'from'                      => $booking_data[ 'from' ],
                                                                                                                   'to'                        => $booking_data[ 'to' ],
                                                                                                                   'count_persons_as_bookings' => $count_persons_as_bookings
                                                                                                               ) );
                        $max_booking_per_block                = max( $product->get_booking_prop( 'max_per_block' ), 1 );
                        $booking_weight                       = !!$count_persons_as_bookings ? $r_persons : 1;

                        if ( $bookings_added_to_cart_in_same_dates + $booking_weight > $max_booking_per_block ) {
                            $remained      = $max_booking_per_block - $bookings_added_to_cart_in_same_dates;
                            $remained_text = !!$remained ? sprintf( '(%s remained)', $remained ) : '';

                            wc_add_notice( sprintf( '<a href="%s" class="button wc-forward">%s</a> %s',
                                                    wc_get_cart_url(),
                                                    __( 'View Cart', 'woocommerce' ),
                                                    sprintf( __( 'You cannot add another &quot;%s&quot; to your cart in the dates you selected %s', 'yith-booking-for-woocommerce' ), $product->get_title(), $remained_text ) ),
                                           'error' );
                            $passed_validation = false;
                        }
                    }
                }
            }

            return $passed_validation;
        }

        /**
         * count Bookings added to cart in the same period passed by $args
         *
         * @param array $args
         *
         * @since 1.0.7
         *
         * @return int
         */
        public function count_added_to_cart_bookings_in_period( $args = array() ) {
            $default_args = array(
                'product_id'                => 0,
                'from'                      => '',
                'to'                        => '',
                'count_persons_as_bookings' => false
            );

            $args = wp_parse_args( $args, $default_args );

            $found_bookings = 0;

            if ( !!$args[ 'product_id' ] && !!$args[ 'from' ] && !!$args[ 'to' ] ) {
                $cart_contents = WC()->cart->cart_contents;
                if ( !!$cart_contents ) {
                    foreach ( $cart_contents as $cart_item_key => $cart_item_data ) {
                        if ( isset( $cart_item_data[ 'product_id' ] ) && $cart_item_data[ 'product_id' ] == $args[ 'product_id' ] ) {
                            // booking in cart with the same product_id
                            if ( isset( $cart_item_data[ 'yith_booking_data' ][ 'from' ] ) && isset( $cart_item_data[ 'yith_booking_data' ][ 'to' ] ) ) {
                                if ( $cart_item_data[ 'yith_booking_data' ][ 'from' ] < $args[ 'to' ] && $cart_item_data[ 'yith_booking_data' ][ 'to' ] > $args[ 'from' ] ) {
                                    if ( $args[ 'count_persons_as_bookings' ] && !empty( $cart_item_data[ 'yith_booking_data' ][ 'persons' ] ) ) {
                                        $found_bookings += max( 1, absint( $cart_item_data[ 'yith_booking_data' ][ 'persons' ] ) );
                                    } else {
                                        $found_bookings++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return $found_bookings;

        }

        /**
         * Add Cart item data for booking products
         *
         * @param $cart_item_data
         * @param $product_id
         *
         * @return array
         */
        public function woocommerce_add_cart_item_data( $cart_item_data, $product_id ) {
            $is_booking = YITH_WCBK_Product_Post_Type_Admin::is_booking( $product_id );
            if ( $is_booking && !isset( $cart_item_data[ 'yith_booking_data' ] ) ) {

                // get the request from cart_item_data; if it's not set, get it by $_REQUEST
                $request      = !empty( $cart_item_data[ 'yith_booking_request' ] ) ? $cart_item_data[ 'yith_booking_request' ] : false;
                $booking_data = self::get_booking_data_from_request( $request );

                /**
                 * add the timestamp to allow adding to cart more booking products with the same configuration
                 *
                 * @since 1.0.10
                 */
                $booking_data[ '_added-to-cart-timestamp' ] = strtotime( 'now' );

                $cart_item_data[ 'yith_booking_data' ] = $booking_data;
            }

            return $cart_item_data;
        }

        /**
         * Set correct price for Booking on add-to-cart item
         *
         * @param $cart_item_data
         * @param $cart_item_key
         *
         * @return mixed
         */
        public function woocommerce_add_cart_item( $cart_item_data, $cart_item_key ) {
            $product_id = isset( $cart_item_data[ 'product_id' ] ) ? $cart_item_data[ 'product_id' ] : 0;
            if ( YITH_WCBK_Product_Post_Type_Admin::is_booking( $product_id ) ) {
                /** @var WC_Product_Booking $product */
                $product      = $cart_item_data[ 'data' ];
                $booking_data = $cart_item_data[ 'yith_booking_data' ];

                $price = $this->get_product_price( $product_id, $booking_data );

                $product->set_price( $price );
                $cart_item_data[ 'data' ] = $product;
            }

            return $cart_item_data;
        }

        /**
         * Set correct price for Booking
         *
         * @param $session_data
         * @param $cart_item
         * @param $cart_item_key
         *
         * @return array
         */
        public function woocommerce_get_cart_item_from_session( $session_data, $cart_item, $cart_item_key ) {
            $product_id = isset( $cart_item[ 'product_id' ] ) ? $cart_item[ 'product_id' ] : 0;
            if ( YITH_WCBK_Product_Post_Type_Admin::is_booking( $product_id ) ) {
                /** @var WC_Product_Booking $product */
                $product      = $session_data[ 'data' ];
                $booking_data = $session_data[ 'yith_booking_data' ];

                if ( !$product->is_available( $booking_data ) ) {
                    $session_data[ 'yith_booking_to_remove' ] = true;

                    return $session_data;
                } else {
                    $session_data[ 'yith_booking_to_remove' ] = false;

                    $price = $this->get_product_price( $product_id, $booking_data );

                    $product->set_price( $price );
                    $session_data[ 'data' ] = $product;
                }
            }

            return $session_data;
        }

        /**
         * Remove non-available bookings from cart
         *
         * @since 2.0.0
         *
         * @param WC_Cart $cart
         */
        function remove_non_available_booking( $cart ) {
            $update        = false;
            $cart_contents = $cart->get_cart_contents();

            foreach ( $cart_contents as $key => $values ) {
                if ( !empty( $values[ 'yith_booking_to_remove' ] ) ) {
                    /** @var WC_Product_Booking $product */
                    $product = $values[ 'data' ] ? $values[ 'data' ] : false;
                    $update  = true;

                    if ( $product ) {
                        $notice_text = sprintf( __( '%s has been removed from your cart because it can no longer be booked.', 'yith-booking-for-woocommerce' ), $product->get_title() );
                        wc_add_notice( $notice_text, 'error' );
                    }
                    unset( $cart_contents[ $key ] );
                }
            }

            if ( $update ) {
                $cart->set_cart_contents( $cart_contents );
            }
        }

        /**
         * check the booking availability before checkout
         * and remove no longer available booking products
         *
         * @since 2.0.1
         */
        public function check_booking_availability_before_checkout() {
            $cart = WC()->cart;

            foreach ( $cart->get_cart() as $cart_item_key => $values ) {
                /** @var WC_Product_Booking $product */
                $product      = $values[ 'data' ];
                $booking_data = !empty( $values[ 'yith_booking_data' ] ) ? $values[ 'yith_booking_data' ] : false;

                if ( $product && $product->is_type( YITH_WCBK_Product_Post_Type_Admin::$prod_type ) && $booking_data && !$product->is_available( $booking_data ) ) {
                    $cart->set_quantity( $cart_item_key, 0 );
                    $notice_text = sprintf( __( '%s has been removed from your cart because it can no longer be booked.', 'yith-booking-for-woocommerce' ), $product->get_title() );

                    wc_add_notice( $notice_text, 'error' );

                    WC()->session->set( 'refresh_totals', true );
                }
            }
        }

        /**
         * filter item data
         *
         * @param $item_data
         * @param $cart_item
         *
         * @return array
         */
        public function woocommerce_get_item_data( $item_data, $cart_item ) {
            $product_id = isset( $cart_item[ 'product_id' ] ) ? $cart_item[ 'product_id' ] : 0;
            if ( YITH_WCBK_Product_Post_Type_Admin::is_booking( $product_id ) ) {
                /**  @var WC_Product_Booking $product */
                $product = wc_get_product( $product_id );

                $booking_data = $cart_item[ 'yith_booking_data' ];

                $from          = $booking_data[ 'from' ];
                $to            = $booking_data[ 'to' ];
                $duration      = $booking_data[ 'duration' ];
                $duration_unit = $product->get_duration_unit();
                $duration_unit = yith_wcbk_get_duration_unit_label( $duration_unit, absint( $duration ) );

                $persons = $booking_data[ 'persons' ];

                $has_time    = in_array( $product->get_duration_unit(), array( 'hour', 'minute' ) );
                $date_format = wc_date_format();
                $date_format .= $has_time ? ' ' . wc_time_format() : '';

                $booking_item_data = array(
                    'yith_booking_from'     => array(
                        'key'     => yith_wcbk_get_booking_meta_label( 'from' ),
                        'value'   => $from,
                        'display' => date_i18n( $date_format, $from )
                    ),
                    'yith_booking_to'       => array(
                        'key'     => yith_wcbk_get_booking_meta_label( 'to' ),
                        'value'   => $to,
                        'display' => date_i18n( $date_format, $to )
                    ),
                    'yith_booking_duration' => array(
                        'key'     => yith_wcbk_get_booking_meta_label( 'duration' ),
                        'value'   => $duration,
                        'display' => $duration . ' ' . $duration_unit
                    )
                );

                if ( $product->has_persons() ) {
                    if ( !empty( $booking_data[ 'person_types' ] ) ) {
                        foreach ( $booking_data[ 'person_types' ] as $person_type_id => $person_type_number ) {
                            if ( $person_type_number < 1 )
                                continue;
                            $person_type_name                                                   = YITH_WCBK()->person_type_helper->get_person_type_title( $person_type_id );
                            $booking_item_data[ 'yith_booking_person_type_' . $person_type_id ] = array(
                                'key'     => $person_type_name,
                                'value'   => $person_type_number,
                                'display' => $person_type_number
                            );
                        }
                    } else {
                        $booking_item_data[ 'yith_booking_persons' ] = array(
                            'key'     => yith_wcbk_get_booking_meta_label( 'persons' ),
                            'value'   => $persons,
                            'display' => $persons
                        );
                    }
                }

                $services          = $product->get_services();
                $selected_services = isset( $booking_data[ 'booking_services' ] ) ? $booking_data[ 'booking_services' ] : array();
                $service_quantities  = isset( $booking_data[ 'booking_service_quantities' ] ) ? $booking_data[ 'booking_service_quantities' ] : array();
                if ( $services && is_array( $services ) ) {
                    $my_services = array();
                    foreach ( $services as $service_id ) {
                        $service = yith_get_booking_service( $service_id );
                        if ( !$service->is_valid() ) {
                            continue;
                        }

                        if ( $service->is_optional() && !in_array( $service_id, $selected_services ) )
                            continue;

                        if ( !$service->is_hidden() ) {
                            $quantity      = isset( $service_quantities[ $service_id ] ) ? $service_quantities[ $service_id ] : false;
                            $my_services[] = $service->get_name_with_quantity( $quantity );
                        }
                    }
                    if ( !!$my_services ) {
                        $booking_item_data[ 'yith_booking_services' ] = array(
                            'key'     => __( 'Booking Services', 'yith-booking-for-woocommerce' ),
                            'value'   => $my_services,
                            'display' => implode( ', ', $my_services )
                        );
                    }
                }

                $item_data = array_merge( $item_data, $booking_item_data );
            }

            return $item_data;
        }

        /**
         * get product price depending on booking data
         *
         * @param int   $product_id
         * @param array $booking_data
         *
         * @return bool|float|string
         */
        public function get_product_price( $product_id, $booking_data ) {
            $price = false;
            if ( YITH_WCBK_Product_Post_Type_Admin::is_booking( $product_id ) ) {
                if ( isset( $booking_data[ 'person_types' ] ) ) {
                    $person_types = array();
                    foreach ( $booking_data[ 'person_types' ] as $person_type_id => $person_type_number ) {
                        $person_types[] = array(
                            'id'     => $person_type_id,
                            'number' => $person_type_number
                        );
                    }
                    $booking_data[ 'person_types' ] = $person_types;
                }
                /**
                 * @var WC_Product_Booking $product
                 */
                $product = wc_get_product( $product_id );
                $price   = $product->calculate_price( $booking_data );
            } else {
                $product = wc_get_product( $product_id );
                if ( $product )
                    $price = $product->get_price();
            }

            return $price;
        }

    }
}