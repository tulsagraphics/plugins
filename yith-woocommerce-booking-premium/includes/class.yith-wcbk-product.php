<?php
!defined( 'YITH_WCBK' ) && exit; // Exit if accessed directly

if ( !class_exists( 'WC_Product_Booking' ) ) {
    /**
     * Class WC_Product_Booking
     * the Booking Product
     *
     * @author Leanza Francesco <leanzafrancesco@gmail.com>
     */
    class WC_Product_Booking extends WC_Product {

        private $_enabled_person_types = null;

        private $_booking_props = array();

        /**
         * Constructor gets the post object and sets the ID for the loaded product.
         *
         * @param int|WC_Product|object $product Product ID, post object, or product object
         */
        public function __construct( $product ) {
            $this->product_type = 'booking';
            parent::__construct( $product );
        }

        /**
         * get a booking property
         *
         * @param string $key
         * @param string $default
         *
         * @return mixed
         * @since 1.1.0
         */
        public function get_booking_prop( $key, $default = '' ) {
            if ( !isset( $this->_booking_props[ $key ] ) ) {
                $value = apply_filters( 'yith_wcbk_booking_product_get_booking_prop', null, $key, $this );
                if ( !is_null( $value ) ) {
                    $this->_booking_props[ $key ] = $value;

                    return $value;
                }

                $booking_key                  = '_yith_booking_' . $key;
                $value                        = get_post_meta( $this->get_id(), $booking_key, true );
                $this->_booking_props[ $key ] = '' !== $value ? $value : $default;
            }

            return $this->_booking_props[ $key ];
        }

        /**
         * set a booking property
         *
         * @param      $key
         * @param      $value
         *
         * @param bool $save
         *
         * @since 1.1.0
         */
        public function set_booking_prop( $key, $value, $save = false ) {
            $this->_booking_props[ $key ] = $value;
            if ( $save ) {
                update_post_meta( $this->get_id(), '_yith_booking_' . $key, $value );
            }
        }

        /**
         * save booking properties
         *
         * @since 2.0.0
         */
        public function save_booking_props() {
            foreach ( $this->_booking_props as $key => $value ) {
                update_post_meta( $this->get_id(), '_yith_booking_' . $key, $value );
            }
        }


        /**
         * get the product type
         *
         * @return string
         */
        public function get_type() {
            return 'booking';
        }

        /**
         * Get the add to cart button text
         *
         * @access public
         * @return string
         */
        public function add_to_cart_text() {
            return apply_filters( 'woocommerce_product_add_to_cart_text', yith_wcbk_get_label( 'read-more' ), $this );
        }


        /**
         * get the permalink by adding query args based on passed array
         *
         * @param array $booking_data
         *
         * @since 2.0.0
         * @return string
         */
        public function get_permalink_with_data( $booking_data = array() ) {
            $booking_data_array = array();
            foreach ( $booking_data as $id => $value ) {
                switch ( $id ) {
                    case 'booking_services':
                        if ( is_array( $value ) && !!$value ) {
                            $booking_data_array[ $id ] = implode( ',', $value );
                        } else {
                            $booking_data_array[ $id ] = $value;
                        }
                        break;
                    case 'person_types':
                        if ( is_array( $value ) && !!$value ) {
                            foreach ( $value as $child_id => $child_value ) {
                                $current_id                        = 'person_type_' . absint( $child_id );
                                $booking_data_array[ $current_id ] = $child_value;
                            }
                        }
                        break;
                    default:
                        if ( is_scalar( $value ) )
                            $booking_data_array[ $id ] = $value;
                        break;
                }
            }

            return add_query_arg( $booking_data_array, $this->get_permalink() );
        }

        /**
         * Calculate price for Booking product
         *
         * @param array $args
         *
         * @return float price
         */
        public function calculate_price( $args = array() ) {
            /* NOTE: Duration is the number of booked blocks! */

            $from = isset( $args[ 'from' ] ) ? $args[ 'from' ] : time();
            $from = !is_numeric( $from ) ? strtotime( $from ) : $from;

            $duration      = isset( $args[ 'duration' ] ) ? absint( $args[ 'duration' ] ) : 1;
            $person_number = isset( $args[ 'persons' ] ) ? $args[ 'persons' ] : $this->get_min_persons();
            if ( !empty( $args[ 'person_types' ] ) && is_array( $args[ 'person_types' ] ) && $this->has_persons() ) {
                $person_number = 0;
                foreach ( $args[ 'person_types' ] as $person_type ) {
                    $persons       = absint( $person_type[ 'number' ] );
                    $person_number += $persons;
                }
                // set the total number of persons to allow discount/increment based on total person number
                $args[ 'persons' ] = $person_number;
            }

            $date_helper = YITH_WCBK_Date_Helper();

            if ( isset( $args[ 'to' ] ) ) {
                $to = $args[ 'to' ];
                $to = !is_numeric( $to ) ? strtotime( $to ) : $to;

                if ( $this->is_all_day() ) {
                    $to = $date_helper->get_time_sum( $to, 1, 'day' );
                }

                $duration           = $date_helper->get_time_diff( $from, $to, $this->get_duration_unit() ) / $this->get_booking_prop( 'duration' );
                $args[ 'duration' ] = $duration;
            }

            $person_types = array(
                array(
                    'id'     => 0,
                    'number' => $person_number,
                ),
            );

            if ( !empty( $args[ 'person_types' ] ) && $this->has_persons() && $this->has_multiply_costs_by_persons_enabled() ) {
                $person_types = $args[ 'person_types' ];
            }

            $price = 0;

            $booking_services           = isset( $args[ 'booking_services' ] ) ? $args[ 'booking_services' ] : array();
            $booking_service_quantities = isset( $args[ 'booking_service_quantities' ] ) ? $args[ 'booking_service_quantities' ] : array();


            foreach ( $person_types as $person_type ) {
                // base cost depends only from starting date
                $args[ 'person_type' ] = $person_type;
                $person_type_number    = absint( $person_type[ 'number' ] );

                $base_cost             = $this->calculate_cost( $from, 'base', $args );
                $unit                  = $this->get_duration_unit();
                $single_duration_block = $this->get_booking_prop( 'duration' );

                $block_cost = 0;

                // increase the block cost for every block in base of settings
                for ( $b = 0; $b < $duration; $b++ ) {
                    $referring_date = $date_helper->get_time_sum( $from, $single_duration_block * $b, $unit, true );
                    $block_cost     += $this->calculate_cost( $referring_date, 'block', $args );
                }

                // total price
                $single_price = $base_cost + $block_cost;

                // calculate costs in base of Persons
                if ( $this->has_persons() && $this->has_multiply_costs_by_persons_enabled() ) {
                    $single_price = $single_price * $person_type_number;
                }

                $price += $single_price;
            }

            $service_args  = array(
                'persons'                    => $person_number,
                'person_types'               => !empty( $args[ 'person_types' ] ) ? $args[ 'person_types' ] : $person_types,
                'duration'                   => $duration,
                'booking_services'           => $booking_services,
                'booking_service_quantities' => $booking_service_quantities,
            );
            $service_costs = $this->calculate_service_costs( $service_args );

            $price += $service_costs;

            return $price;
        }


        /**
         * Calculate the total service costs
         *
         * @param array $args
         *
         * @return float
         */
        public function calculate_service_costs( $args = array() ) {
            extract( $args );
            /**
             * @var int   $persons
             * @var array $person_types
             * @var int   $duration
             * @var array $booking_services
             * @var array $booking_service_quantities
             */
            $service_cost = 0;

            if ( $persons > 0 && $this->has_services() ) {
                $services = $this->get_services();
                foreach ( $services as $service_id ) {
                    $service = yith_get_booking_service( $service_id );

                    if ( !$service->is_valid() )
                        continue;

                    if ( $service->is_optional() && !in_array( $service_id, $booking_services ) ) {
                        continue;
                    }

                    $service_cost_total = 0;

                    if ( $service->is_multiply_per_persons() ) {
                        foreach ( $person_types as $person_type ) {
                            $pt_id                      = absint( $person_type[ 'id' ] );
                            $pt_number                  = absint( $person_type[ 'number' ] );
                            $current_service_cost_total = $service->get_price( $pt_id );

                            if ( $current_service_cost_total > 0 ) {
                                if ( $service->is_multiply_per_blocks() ) {
                                    $current_service_cost_total *= $duration;
                                }
                                if ( $service->is_multiply_per_persons() ) {
                                    $current_service_cost_total *= $pt_number;
                                }
                            }

                            $service_cost_total += floatval( $current_service_cost_total );
                        }
                    } else {
                        $service_cost_total = $service->get_price();
                        if ( $service_cost_total > 0 ) {
                            if ( $service->is_multiply_per_blocks() ) {
                                $service_cost_total *= $duration;
                            }
                        }
                    }

                    if ( $service->is_quantity_enabled() ) {
                        $quantity = isset( $booking_service_quantities[ $service->id ] ) ? $booking_service_quantities[ $service->id ] : 0;
                        $quantity = max( $service->get_min_quantity(), $quantity );
                        if ( $max_quantity = $service->get_max_quantity() ) {
                            $quantity = min( $quantity, $service->get_max_quantity() );
                        }

                        $service_cost_total = $service_cost_total * $quantity;
                    }

                    $service_cost_total = apply_filters( 'yith_wcbk_booking_product_single_service_cost_total', $service_cost_total, $service, $args, $this );

                    $service_cost += floatval( $service_cost_total );
                }
            }

            $service_cost = apply_filters( 'yith_wcbk_booking_product_calculate_service_costs', $service_cost, $args, $this );

            return (float)$service_cost;
        }

        /**
         * Return the service cost in base of duration(number of blocks), person type and person number
         *
         * @param int   $duration
         * @param int   $person_type_id
         * @param int   $person_number
         *
         * @param array $optional_services_selected
         *
         * @return int|string
         */
        public function calculate_service_cost( $duration, $person_type_id, $person_number, $optional_services_selected = array() ) {
            $service_cost = 0;
            if ( $person_number > 0 && $this->has_services() ) {
                $services = $this->get_services();
                foreach ( $services as $service_id ) {
                    $service = yith_get_booking_service( $service_id );

                    if ( !$service->is_valid() )
                        continue;

                    if ( $service->is_optional() && !in_array( $service_id, $optional_services_selected ) ) {
                        continue;
                    }

                    $current_service_cost = $service->get_price( $person_type_id );

                    if ( $current_service_cost > 0 ) {
                        if ( $service->is_multiply_per_blocks() ) {
                            $current_service_cost *= $duration;
                        }
                        if ( $service->is_multiply_per_persons() ) {
                            $current_service_cost *= $person_number;
                        }
                    }

                    $service_cost += $current_service_cost;
                }
            }

            return $service_cost;
        }

        /**
         * Calculate costs (block or base) for a single timestamp
         *
         * @param        $timestamp
         * @param string $type
         * @param array  $args {
         *
         * @var array person_type the type of person ['id' => id, ...]
         * }
         *
         * @return float
         */
        public function calculate_cost( $timestamp, $type = 'block', $args = array() ) {
            do_action( 'yith_wcbk_booking_before_calculate_cost', $timestamp, $type, $this );
            $allowed_types = array( 'base', 'block' );
            $type          = in_array( $type, $allowed_types ) ? $type : 'block';
            $cost_variable = $type . '_cost';

            $cost        = (float)$this->get_booking_prop( $cost_variable );
            $cost_ranges = $this->get_costs_ranges();

            $global_cost_ranges = YITH_WCBK()->settings->get_global_cost_ranges();
            $cost_ranges        = array_merge( $cost_ranges, $global_cost_ranges );

            $person_type_id = false;
            $person_number  = 0;

            if ( isset( $args[ 'person_type' ] ) ) {
                $current_person_type = $args[ 'person_type' ];
                $person_types        = $this->get_person_types();
                if ( $current_person_type[ 'id' ] !== 0 && isset( $person_types[ $current_person_type[ 'id' ] ] ) ) {
                    $person_type_id = $current_person_type[ 'id' ];
                    $person_number  = absint( $current_person_type[ 'number' ] );

                    $product_person_type = $person_types[ $current_person_type[ 'id' ] ];
                    if ( $product_person_type[ $type . '_cost' ] !== '' ) {
                        $cost = (float)$product_person_type[ $type . '_cost' ];
                    }
                }
            }

            $date_helper = YITH_WCBK_Date_Helper();

            $persons  = isset( $args[ 'persons' ] ) ? absint( $args[ 'persons' ] ) : $this->get_booking_prop( 'min_persons' );
            $duration = isset( $args[ 'duration' ] ) ? absint( $args[ 'duration' ] ) : 1;

            $variables = array(
                'persons'   => $persons,
                'duration'  => $duration,
                'qty'       => 1,
                'extra_qty' => 1,
            );

            foreach ( $cost_ranges as $cost_range ) {
                $cost_range = (array)$cost_range;
                $date_from  = $timestamp;
                $date_to    = $timestamp;
                $range_type = !empty( $cost_range[ 'type' ] ) ? $cost_range[ 'type' ] : 'custom';
                $range_from = !empty( $cost_range[ 'from' ] ) ? $cost_range[ 'from' ] : '';
                $range_to   = !empty( $cost_range[ 'to' ] ) ? $cost_range[ 'to' ] : '';
                $intersect  = false;

                $is_date_range = !in_array( $range_type, array( 'person', 'block' ) ) && 0 !== strpos( $range_type, 'person-type-' );

                $current_variables = $variables;

                if ( $is_date_range ) {
                    $check = $date_helper->check_date_inclusion_in_range( $range_type, $range_from, $range_to, $date_from, $date_to, $intersect );
                } else {
                    $check = false;

                    if ( $cost_range[ 'type' ] === 'person' && $this->has_persons() ) {
                        $current_variables[ 'qty' ]       = $persons;
                        $current_variables[ 'extra_qty' ] = $persons - $range_from + 1;
                        if ( ( !$range_to || $persons <= $range_to ) && $persons >= $range_from ) {
                            $check = true;
                        }
                    } elseif ( $cost_range[ 'type' ] === 'block' ) {
                        $current_variables[ 'qty' ]       = $duration;
                        $current_variables[ 'extra_qty' ] = $duration - $range_from + 1;
                        if ( ( !$range_to || $duration <= $range_to ) && $duration >= $range_from ) {
                            $check = true;
                        }
                    } elseif ( strpos( $range_type, 'person-type-' ) === 0 && $this->has_persons() && $this->has_person_types() ) {
                        $range_person_type_id = absint( str_replace( 'person-type-', '', $range_type ) );
                        if ( !empty( $args[ 'person_types' ] ) && !$this->has_multiply_costs_by_persons_enabled() ) {
                            foreach ( $args[ 'person_types' ] as $_current_person_type ) {
                                if ( $_current_person_type[ 'id' ] == $range_person_type_id ) {
                                    $_person_number                   = absint( $_current_person_type[ 'number' ] );
                                    $current_variables[ 'qty' ]       = $_person_number;
                                    $current_variables[ 'extra_qty' ] = $_person_number - $range_from + 1;
                                    if ( ( !$range_to || $_person_number <= $range_to ) && $_person_number >= $range_from ) {
                                        $check = true;
                                    }
                                    break;
                                }
                            }
                        } else {
                            if ( $person_type_id && $person_type_id == $range_person_type_id ) {
                                $current_variables[ 'qty' ]       = $person_number;
                                $current_variables[ 'extra_qty' ] = $person_number - $range_from + 1;
                                if ( ( !$range_to || $person_number <= $range_to ) && $person_number >= $range_from ) {
                                    $check = true;
                                }
                            }
                        }
                    }
                }

                if ( !empty( $args[ 'person_types' ] ) ){
                    foreach ( $args[ 'person_types' ] as $_current_person_type ) {
                        $_person_number                                                 = absint( $_current_person_type[ 'number' ] );
                        $current_variables[ 'person_' . $_current_person_type[ 'id' ] ] = $_person_number;

                    }
                }

                $variable_alias = array(
                    'extra_qty' => array( 'qty_diff' )
                );

                foreach ( $variable_alias as $key => $alias_array ) {
                    foreach ( $alias_array as $alias ) {
                        $current_variables[ $alias ] = $current_variables[ $key ];
                    }
                }

                $check = apply_filters( 'yith_wcbk_booking_calculate_cost_check_is_in_range', $check, $cost_range, $timestamp, $type, $this );

                if ( $check ) {
                    $this_cost     = !empty( $cost_range[ $type . '_cost' ] ) ? $cost_range[ $type . '_cost' ] : 0;
                    $this_operator = !empty( $cost_range[ $type . '_cost_operator' ] ) ? $cost_range[ $type . '_cost_operator' ] : 'add';
                    if ( strpos( $this_cost, '*' ) ) {
                        list( $this_cost, $variable ) = explode( '*', $this_cost, 2 );
                        // the $current_variables[ $variable ] is an INTEGER: for this reason it should be > 1
                        if ( isset( $current_variables[ $variable ] ) && $current_variables[ $variable ] > 1 ) {
                            $this_cost *= $current_variables[ $variable ];
                        } elseif ( 'person_' === substr( $variable, 0, 7 ) && !isset( $current_variables[ $variable ] ) ) {
                            $this_cost = 0;
                        }
                    } elseif ( strpos( $this_cost, '/' ) ) {
                        list( $this_cost, $variable ) = explode( '/', $this_cost, 2 );
                        // the $current_variables[ $variable ] is an INTEGER: for this reason it should be > 1
                        if ( !empty( $current_variables[ $variable ] ) && $current_variables[ $variable ] > 1 ) {
                            $this_cost /= $current_variables[ $variable ];
                        } elseif ( 'person_' === substr( $variable, 0, 7 ) && empty( $current_variables[ $variable ] ) ) {
                            $this_cost = 0;
                        }
                    }

                    switch ( $this_operator ) {
                        case 'add':
                            $cost = $cost + $this_cost;
                            break;
                        case 'sub':
                            $cost = $cost - $this_cost;
                            break;
                        case 'mul':
                            $cost = $cost * $this_cost;
                            break;
                        case 'div':
                            if ( $this_cost != 0 )
                                $cost = $cost / $this_cost;
                            break;
                    }
                }
            }

            $cost = apply_filters( 'yith_wcbk_booking_calculate_cost', $cost, $timestamp, $type, $this );

            return (float)$cost;

        }

        /**
         * Get the availability range array
         *
         * @return YITH_WCBK_Availability_Range[]
         */
        public function get_availability_ranges() {
            $availability_ranges = $this->get_booking_prop( 'availability_range' );

            if ( !!$availability_ranges && is_array( $availability_ranges ) ) {
                $availability_ranges = array_map( 'yith_wcbk_availability_range', $availability_ranges );
            }

            return !empty( $availability_ranges ) ? $availability_ranges : array();
        }

        /**
         * Retrieve an array containing the booking data
         *
         * @return array
         */
        public function get_booking_data() {
            $booking_data_vars = apply_filters( 'yith_wcbk_get_booking_data_vars', array(
                'min_persons',
                'max_persons',
                'duration_unit',
                'minimum_duration',
                'maximum_duration',
                'all_day'
            ), $this );

            $booking_data = array();
            foreach ( $booking_data_vars as $var ) {
                $booking_data[ $var ] = $this->get_booking_prop( $var );
            }

            return apply_filters( 'yith_wcbk_get_booking_data', $booking_data, $this );
        }

        /**
         * Get the costs range array
         *
         * @return array
         */
        public function get_costs_ranges() {
            $costs_ranges = $this->get_booking_prop( 'costs_range' );

            return !!$costs_ranges && is_array( $costs_ranges ) ? $costs_ranges : array();
        }

        /**
         * Returns the product's base cost,
         * calculated by (base cost + booking cost) * persons .
         *
         * @return string price
         */
        public function get_base_cost() {
            $base_cost = $this->get_booking_prop( 'base_cost' ) + ( $this->get_booking_prop( 'block_cost' ) * $this->get_min_duration() );

            if ( $this->has_persons() && $this->get_min_persons() > 1 && $this->has_multiply_costs_by_persons_enabled() ) {
                $base_cost = $base_cost * $this->get_min_persons();
            }

            return $base_cost;
        }

        /**
         * get the block duration
         */
        public function get_block_duration_html() {
            $duration = $this->get_booking_prop( 'duration' );

            return $duration . ' ' . yith_wcbk_get_duration_unit_label( $this->get_duration_unit(), $duration );
        }

        /**
         * Get the calculated price html
         *
         * @param array $args
         *
         * @return string
         */
        public function get_calculated_price_html( $args = array() ) {
            $price = yit_get_display_price( $this, $this->calculate_price( $args ) );
            $price = apply_filters( 'yith_wcbk_get_calculated_price_html_price', $price, $args, $this );

            if ( !$price ) {
                $price = apply_filters( 'yith_wcbk_booking_product_free_price_html', __( 'Free!', 'woocommerce' ), $this );
            } else {
                $price = wc_price( $price ) . $this->get_price_suffix();
            }

            return apply_filters( 'woocommerce_get_price_html', $price, $this );
        }

        /**
         * return the default start date
         *
         * @since 1.0.6
         *
         * @return string
         */
        public function get_default_start_date() {
            $date               = '';
            $default_start_date = $this->get_booking_prop( 'default_start_date' );

            if ( in_array( $default_start_date, array( 'today', 'tomorrow' ) ) ) {
                $allow_after = $this->get_booking_prop( 'allow_after' );
                $timestamp   = strtotime( $default_start_date );

                if ( $allow_after ) {
                    $date_helper               = YITH_WCBK_Date_Helper();
                    $allow_after_unit          = $this->get_booking_prop( 'allow_after_unit' );
                    $first_available_timestamp = $date_helper->get_time_sum( strtotime( 'now' ), $allow_after, $allow_after_unit, true );
                    if ( $timestamp < $first_available_timestamp ) {
                        $timestamp = $first_available_timestamp;
                    }
                }

                $date = date( 'Y-m-d', $timestamp );

            } elseif ( 'first-available' === $default_start_date ) {
                $current_day = strtotime( 'now' );
                $allow_after = $this->get_booking_prop( 'allow_after' );
                if ( $allow_after ) {
                    $date_helper      = YITH_WCBK_Date_Helper();
                    $allow_after_unit = $this->get_booking_prop( 'allow_after_unit' );
                    $current_day      = $date_helper->get_time_sum( $current_day, $allow_after, $allow_after_unit, true );
                }
                $date_info           = yith_wcbk_get_booking_form_date_info( $this, array( 'include_default_start_date' => false, 'include_default_end_date' => false ) );
                $last_date           = strtotime( $date_info[ 'next_year' ] . '-' . $date_info[ 'next_month' ] . '-1 +1 month' );
                $not_available_dates = $this->get_not_available_dates( $date_info[ 'current_year' ], $date_info[ 'current_month' ], $date_info[ 'next_year' ], $date_info[ 'next_month' ], 'day' );

                $allowed_start_days = $this->get_allowed_start_days();

                do {
                    $current_date = date( 'Y-m-d', $current_day );
                    if ( !in_array( $current_date, $not_available_dates ) && ( !$allowed_start_days || in_array( date( 'N', $current_day ), $allowed_start_days ) ) ) {
                        $date = $current_date;
                        break;
                    } else {
                        $current_day = strtotime( '+1 day', $current_day );
                    }

                } while ( $current_day < $last_date );

            } elseif ( 'custom' === $default_start_date ) {
                $date = $this->get_booking_prop( 'default_start_date_custom' );
            }

            return apply_filters( 'yith_wcbk_booking_product_get_default_start_date', $date, $this );

        }

        /**
         * return daily start time
         *
         * @since 2.0.0
         *
         * @return string
         */
        public function get_daily_start_time() {
            $time = $this->get_booking_prop( 'daily_start_time' );
            $time = !!$time ? $time : '00:00';

            return apply_filters( 'yith_wcbk_booking_product_get_daily_start_time', $time, $this );

        }

        /**
         * Get the duration unit
         *
         * @return string
         */
        public function get_duration_unit() {
            return $this->get_booking_prop( 'duration_unit' );
        }

        /**
         * return the external calendars array
         *
         * @since 2.0
         *
         * @return array
         */
        public function get_external_calendars() {
            $calendars = $this->get_booking_prop( 'external_calendars' );

            return !!$calendars ? $calendars : array();
        }

        /**
         * return the external calendars sync key
         *
         * @since 2.0
         *
         * @return string
         */
        public function get_external_calendars_key() {
            $key = $this->get_booking_prop( 'external_calendars_key' );
            if ( !$key ) {
                $key = yith_wcbk_generate_external_calendars_key();
                $this->set_booking_prop( 'external_calendars_key', $key, true );
            }

            return $key;
        }

        /**
         * check for the correct external calendars sync key
         *
         * @param $key
         *
         * @since 2.0.0
         * @return bool
         */
        public function check_external_calendars_key( $key ) {
            return $key === $this->get_external_calendars_key();
        }

        /**
         * return the external calendars sync expiration
         *
         * @since 2.0
         *
         * @return int
         */
        public function get_external_calendars_last_sync() {
            return absint( $this->get_booking_prop( 'external_calendars_last_sync' ) );
        }

        /**
         * delete the external calendars sync expiration
         *
         * @since 2.0
         */
        public function delete_external_calendars_last_sync() {
            $this->set_booking_prop( 'external_calendars_last_sync', false, true );
        }

        /**
         * return true if has external calendars
         *
         * @since 2.0.0
         *
         * @return bool
         */
        public function has_external_calendars() {
            return !!$this->get_external_calendars();
        }


        /**
         * return true if externals has already loaded (and not expired) for this product
         *
         * @since 2.0
         *
         * @return bool
         */
        public function has_externals_synchronized() {
            $expiring_time = get_option( 'yith-wcbk-external-calendars-sync-expiration', 6 * HOUR_IN_SECONDS );
            $now           = time();
            $last_loaded   = $this->get_external_calendars_last_sync();

            return !!$last_loaded && ( $now - $last_loaded < $expiring_time );
        }

        /**
         * Load external calendars if not already loaded
         *
         * @since 2.0.0
         */
        public function maybe_load_externals() {
            if ( $this->has_external_calendars() && !$this->has_externals_synchronized() ) {
                $this->get_externals();
            }
        }

        /**
         * return an array of bookings loaded from external calendars
         *
         * @since 2.0
         *
         * @param bool $force_loading
         *
         * @return YITH_WCBK_Booking_External[]
         */
        public function get_externals( $force_loading = false ) {
            $calendars = $this->get_external_calendars();
            $externals = array();
            if ( $calendars ) {
                $load = $force_loading || !$this->has_externals_synchronized();

                if ( $load ) {
                    YITH_WCBK_Booking_Externals()->delete_externals_from_product_id( $this->get_id() );
                    $externals = array();

                    foreach ( $calendars as $calendar ) {
                        $name = htmlspecialchars( $calendar[ 'name' ] );
                        $url  = $calendar[ 'url' ];

                        $timeout  = apply_filters( 'yith_wcbk_booking_product_get_externals_timeout', 15 );
                        $response = wp_remote_get( $url, array( 'timeout' => $timeout ) );

                        if ( !is_wp_error( $response ) && 200 == $response[ 'response' ][ 'code' ] && 'OK' == $response[ 'response' ][ 'message' ] ) {
                            $body = $response[ 'body' ];
                            try {
                                $ics_parser = new YITH_WCBK_ICS_Parser( $body, array(
                                    'product_id'    => $this->get_id(),
                                    'calendar_name' => $name,
                                ) );

                                $externals = array_merge( $externals, $ics_parser->get_events() );

                            } catch ( Exception $e ) {
                                $message = "Error while parsing ICS externals for product #{$this->get_id()}";
                                $message .= " - ";
                                $message .= $e->getMessage();
                                $message .= " - ";
                                $message .= print_r( compact( 'name', 'url', 'body' ), true );

                                yith_wcbk_add_log( $message, YITH_WCBK_Logger_Types::ERROR, YITH_WCBK_Logger_Groups::GENERAL );
                            }
                        } else {
                            $message = "Error while retrieving externals for product #{$this->get_id()}:";
                            $message .= " - ";
                            $message .= print_r( compact( 'name', 'url', 'response' ), true );

                            yith_wcbk_add_log( $message, YITH_WCBK_Logger_Types::ERROR, YITH_WCBK_Logger_Groups::GENERAL );
                        }

                    }

                    // remove completed externals
                    $externals = array_filter( $externals, function ( $external ) {
                        /** @var YITH_WCBK_Booking_External $external */
                        return !$external->is_completed();
                    } );

                    YITH_WCBK_Booking_Externals()->add_externals( $externals, false );
                    $this->set_booking_prop( 'external_calendars_last_sync', time(), true );

                } else {
                    $externals = YITH_WCBK_Booking_Externals()->get_externals_from_product_id( $this->get_id() );
                }
            }

            return $externals;
        }

        /**
         * regenerate product data
         *
         * @param array $data
         */
        public function regenerate_data( $data = array() ) {
            $time_debug_key = __FUNCTION__ . '_' . $this->get_id();
            yith_wcbk_time_debug_start( $time_debug_key );
            if ( !$data ) {
                $data = array( 'externals', 'not-available-dates' );
            }

            if ( in_array( 'externals', $data ) ) {
                $this->maybe_load_externals();
            }

            if ( in_array( 'not-available-dates', $data ) ) {
                $date_info = yith_wcbk_get_booking_form_date_info( $this );
                $this->get_not_available_dates( $date_info[ 'current_year' ], $date_info[ 'current_month' ], $date_info[ 'next_year' ], $date_info[ 'next_month' ], 'day' );
            }

            $seconds = yith_wcbk_time_debug_end( $time_debug_key );
            yith_wcbk_maybe_debug( sprintf( 'Product Data regenerated for product #%s (%s seconds taken)', $this->get_id(), $seconds ) );
        }

        /**
         * Get the location coordinates
         *
         * @return array|bool
         */
        public function get_location_coordinates() {
            $coordinates = false;

            if ( $location = $this->get_booking_prop( 'location' ) ) {
                $lat = $this->get_booking_prop( 'location_lat' );
                $lng = $this->get_booking_prop( 'location_lng' );

                if ( $lat != '' && $lng != '' ) {
                    $coordinates = array(
                        'lat' => $lat,
                        'lng' => $lng,
                    );
                } else {
                    $location_info = YITH_WCBK()->maps->get_location_by_address( $location );
                    if ( $location_info && isset( $location_info[ 'lat' ] ) && isset( $location_info[ 'lng' ] ) ) {
                        $coordinates = $location_info;
                    }

                }
            }

            return $coordinates;
        }

        /**
         * Get the maximum duration
         *
         * @return int
         */
        public function get_max_duration() {
            if ( !$this->is_type_fixed_blocks() ) {
                return max( 0, absint( $this->get_booking_prop( 'maximum_duration' ) ) );
            } else {
                return 1;
            }
        }

        /**
         * Get the maximum duration time object
         *
         * @return object
         */
        public function get_max_duration_time() {
            $duration = $this->get_max_duration() * $this->get_booking_prop( 'duration' );
            $unit     = $this->get_duration_unit();
            $r        = array(
                'duration' => $duration,
                'unit'     => $unit,
            );

            return (object)$r;
        }

        /**
         * Get the minimum duration
         *
         * @return int
         */
        public function get_min_duration() {
            if ( !$this->is_type_fixed_blocks() ) {
                return max( 1, absint( $this->get_booking_prop( 'minimum_duration' ) ) );
            } else {
                return 1;
            }
        }

        /**
         * Get the minimum duration time object
         *
         * @return object
         */
        public function get_min_duration_time() {
            $duration = $this->get_min_duration() * $this->get_booking_prop( 'duration' );
            $unit     = $this->get_duration_unit();
            $r        = array(
                'duration' => $duration,
                'unit'     => $unit,
            );

            return (object)$r;
        }

        /**
         * Get the minimum nomber of persons
         *
         * @return int
         */
        public function get_min_persons() {
            return absint( $this->get_booking_prop( 'min_persons' ) );
        }

        /**
         * Returns the product's active price.
         *
         * @param string $context
         *
         * @return string price
         */
        public function get_price( $context = 'view' ) {
            $price = $this instanceof WC_Data ? parent::get_price( 'edit' ) : $this->price;
            $price = $price ? $price : $this->calculate_price();

            return 'view' === $context ? apply_filters( 'woocommerce_product_get_price', $price, $this ) : $price;
        }

        /**
         * Returns the product's regular price.
         * In case of Booking Product the regular price is ''
         *
         * @param string $context
         *
         * @return string price
         */
        public function get_regular_price( $context = 'view' ) {
            return '';
        }

        /**
         * Checks if a product has the calendar picker enabled
         *
         * @return bool
         */
        public function has_calendar_picker_enabled() {
            return $this->get_booking_prop( 'enable_calendar_range_picker' ) === 'yes' &&
                   $this->get_booking_prop( 'duration' ) == 1 &&
                   $this->get_duration_unit() === 'day' &&
                   !$this->is_type_fixed_blocks();
        }

        /**
         * check if the product is all day
         *
         * @since 2.0.0
         * @return bool
         */
        public function is_all_day() {
            return 'yes' === $this->get_booking_prop( 'all_day' ) &&
                   'day' === $this->get_duration_unit();
        }

        /**
         * Checks if a product has multiply costs by persons enabled.
         *
         * @return bool
         */
        public function has_multiply_costs_by_persons_enabled() {
            return $this->has_persons() && $this->get_booking_prop( 'multiply_costs_by_persons' ) === 'yes';
        }

        /**
         * Checks if a product has count persons as bookings enabled.
         *
         * @return bool
         */
        public function has_count_persons_as_bookings_enabled() {
            return $this->has_persons() && $this->get_booking_prop( 'count_persons_as_bookings' ) === 'yes';
        }

        /**
         * Check if has persons enabled.
         *
         * @return boolean
         */
        public function has_persons() {
            return $this->get_booking_prop( 'has_persons' ) === 'yes';
        }

        /**
         * Check if has person types enabled.
         *
         * @return boolean
         */
        public function has_person_types() {
            $person_type_enabled = $this->get_booking_prop( 'enable_person_types' ) === 'yes';

            return $this->has_persons() && $person_type_enabled && !!$this->get_person_types();
        }

        /**
         * get the enabled person types
         *
         * @return array
         */
        public function get_person_types() {
            if ( is_null( $this->_enabled_person_types ) ) {
                $this->_enabled_person_types = array();
                $person_types                = $this->get_booking_prop( 'person_types' );
                $person_types                = !!$person_types && is_array( $person_types ) ? $person_types : array();
                foreach ( $person_types as $person_type_id => $person_type ) {
                    $person_type[ 'enabled' ] = isset( $person_type[ 'enabled' ] ) && $person_type[ 'enabled' ] === 'yes';
                    if ( $person_type[ 'enabled' ] ) {
                        $this->_enabled_person_types[ $person_type_id ] = $person_type;
                    }
                }
            }

            return $this->_enabled_person_types;
        }

        /**
         * get the services
         *
         * @param array $args arguments passed to wp_get_object_terms
         *
         * @return array
         */
        public function get_services( $args = array() ) {
            if ( !!$this->get_booking_prop( 'services' ) ) {
                return $this->get_booking_prop( 'services' );
            }

            $default_args = array(
                'fields' => 'ids',
            );

            $args               = wp_parse_args( $args, $default_args );
            $booking_product_id = apply_filters( 'yith_wcbk_booking_product_id_to_translate', $this->get_id(), $this );
            $services           = wp_get_object_terms( $booking_product_id, YITH_WCBK_Post_Types::$service_tax, $args );
            $services           = apply_filters( 'yith_wcbk_booking_product_get_services', $services, $this );

            $this->set_booking_prop( 'services', $services );

            return $this->get_booking_prop( 'services' );
        }

        /**
         * check if this booking has services
         *
         * @return bool
         */
        public function has_services() {
            return !!$this->get_services();
        }

        /**
         * get the allows start days
         *
         * @since 2.0.0
         * @return array
         */
        public function get_allowed_start_days() {
            $allowed_start_days = $this->get_booking_prop( 'allowed_start_days' );

            return !!$allowed_start_days && is_array( $allowed_start_days ) ? $allowed_start_days : array();
        }

        /**
         * get the max bookings per unit
         *
         * @return int
         */
        public function get_max_per_block() {
            return $this->get_booking_prop( 'max_per_block', 1 );
        }

        /**
         * get allow after
         *
         * @since 2.0.0
         * @return int
         */
        public function get_allow_after() {
            return absint( $this->get_booking_prop( 'allow_after' ) );
        }

        /**
         * get the buffer
         *
         * @since 2.0.5
         * @return int
         */
        public function get_buffer() {
            return absint( $this->get_booking_prop( 'buffer' ) );
        }

        /**
         * get allow after unit
         *
         * @since 2.0.0
         * @return string
         */
        public function get_allow_after_unit() {
            $unit = $this->get_booking_prop( 'allow_after_unit' );
            if ( !in_array( $unit, array( 'month', 'day', 'hour' ) ) ) {
                $unit = 'day';
            }

            return $unit;
        }

        /**
         * get allow until
         *
         * @since 2.0.0
         * @return int
         */
        public function get_allow_until() {
            return max( 1, absint( $this->get_booking_prop( 'allow_until' ) ) );
        }

        /**
         * get allow until unit
         *
         * @since 2.0.0
         * @return string
         */
        public function get_allow_until_unit() {
            $unit = $this->get_booking_prop( 'allow_until_unit' );
            if ( !in_array( $unit, array( 'year', 'month', 'day' ) ) ) {
                $unit = 'year';
            }

            return $unit;
        }

        /**
         * return true if time increment based on duration is enabled
         *
         * @since 2.0.0
         * @return bool
         */
        public function is_time_increment_based_on_duration() {
            return 'yes' === $this->get_booking_prop( 'time_increment_based_on_duration' );
        }

        /**
         * Check if this booking is available
         *
         * @param array $args {
         *
         * @var int     $from [optional] timestamp from date
         * @var int     $to   [optional] timestamp to date
         * }
         *
         * @return bool
         */
        public function is_available( $args = array() ) {
            do_action( 'yith_wcbk_booking_before_is_available', $args, $this );
            $available         = true;
            $date_helper       = YITH_WCBK_Date_Helper();
            $now               = time();
            $allow_after       = $this->get_allow_after();
            $allow_after_unit  = $this->get_allow_after_unit();
            $min_duration_time = $this->get_min_duration_time();
            $max_duration_time = $this->get_max_duration_time();

            $from                        = isset( $args[ 'from' ] ) ? $args[ 'from' ] : $now;
            $to                          = !empty( $args[ 'to' ] ) ? $args[ 'to' ] : false;
            $exclude_booked              = isset( $args[ 'exclude_booked' ] ) ? $args[ 'exclude_booked' ] : false;
            $exclude_time                = isset( $args[ 'exclude_time' ] ) ? $args[ 'exclude_time' ] : false;
            $check_start_date            = isset( $args[ 'check_start_date' ] ) ? $args[ 'check_start_date' ] : true;
            $check_min_max_duration      = isset( $args[ 'check_min_max_duration' ] ) ? $args[ 'check_min_max_duration' ] : true;
            $check_non_available_in_past = isset( $args[ 'check_non_available_in_past' ] ) ? $args[ 'check_non_available_in_past' ] : true;
            $persons                     = isset( $args[ 'persons' ] ) ? max( 1, absint( $args[ 'persons' ] ) ) : 1;

            // Not available in past for Time booking
            if ( isset( $args[ 'from' ] ) && !$exclude_time && $check_non_available_in_past && $this->has_time() ) {
                if ( $from < current_time( 'timestamp' ) )
                    $available = false;
            }

            // Not available in past (based on 'Allow after' | default 'today midnight')
            $min_date_timestamp = strtotime( "+{$allow_after} {$allow_after_unit}s midnight", $now );
            if ( $check_non_available_in_past && $from < $min_date_timestamp ) {
                $available = false;
            }


            if ( $available && $check_start_date && $allowed_start_days = $this->get_allowed_start_days() ) {
                $from_day  = date( 'N', $from );
                $available = in_array( $from_day, $allowed_start_days );
            }

            if ( !$to ) {
                $_duration = $check_min_max_duration ? $min_duration_time->duration : 1;
                $to        = $date_helper->get_time_sum( $from, $_duration, $min_duration_time->unit );
                if ( $this->is_all_day() ) {
                    $to = $date_helper->get_time_sum( $to, -1, 'day' );
                }
            }

            if ( $this->is_all_day() ) {
                $to = $date_helper->get_time_sum( $to, 1, 'day' );
            }

            if ( $check_min_max_duration && $available ) {
                $min_to = $date_helper->get_time_sum( $from, $min_duration_time->duration, $min_duration_time->unit, true );

                if ( $to < $min_to ) {
                    $available = false;
                }

                if ( $max_duration_time->duration > 0 ) {
                    $max_to = $date_helper->get_time_sum( $from, $max_duration_time->duration, $max_duration_time->unit, true );

                    if ( $this->is_all_day() ) {
                        $max_to = $date_helper->get_time_sum( $max_to, 1, 'day' ) - 1;
                    }

                    if ( $to > $max_to ) {
                        $available = false;
                    }
                }

                if ( $this->get_booking_prop( 'duration' ) > 1 ) {
                    $_duration = $date_helper->get_time_diff( $from, $to, $this->get_duration_unit() );
                    if ( $_duration % $this->get_booking_prop( 'duration' ) !== 0 )
                        $available = false;
                }
            }
            //yith_wcbk_maybe_debug(sprintf('From %s | To %s', date('Y-m-d- H:i:s', $from), date('Y-m-d- H:i:s', $to)));

            if ( $available ) {
                $allow_until      = $this->get_allow_until();
                $allow_until_unit = $this->get_allow_until_unit();
                // Not available in future (based on 'Allow until' | default '+1 year')
                $max_date_timestamp = strtotime( "+{$allow_until} {$allow_until_unit}s midnight", $now );
                if ( $to > $max_date_timestamp ) {
                    $available = false;
                }
            }

            if ( $available ) {
                $is_same_date = strtotime( 'midnight', $from ) === strtotime( 'midnight', $to - 1 );

                // Check if booking is available depending on Global availability settings and Product availability settings
                if ( !$is_same_date && $this->has_time() ) {
                    // check availability for each single day to allow "fluid" availability
                    $tmp_from = $tmp_to = $from;
                    do {
                        if ( !$available )
                            break;

                        $tmp_to    = min( $to, strtotime( 'tomorrow midnight', $tmp_to ) );
                        $available = $this->check_availability( $tmp_from, $tmp_to, $exclude_time );
                        $tmp_from  = $tmp_to;

                    } while ( $tmp_to < $to );

                } else {
                    $available = $this->check_availability( $from, $to, $exclude_time );
                }


                // Check if exist other booked booking (for the same product) in the same dates!
                if ( !$exclude_booked && $available && $this->get_max_per_block() ) {
                    $get_post_args = array();
                    if ( isset( $args[ '_booking_id' ] ) ) {
                        // exclude the booking if the customer is paying for a his/her confirmed booking
                        $product_id_to_exclude = apply_filters( 'yith_wcbk_booking_product_id_to_translate', absint( $args[ '_booking_id' ] ) );
                        $get_post_args         = array(
                            'exclude' => $product_id_to_exclude,
                        );
                    }

                    $product_id                = apply_filters( 'yith_wcbk_booking_product_id_to_translate', $this->get_id() );
                    $include_externals         = $this->has_external_calendars();
                    $unit                      = $this->get_duration_unit();
                    $count_persons_as_bookings = $this->has_count_persons_as_bookings_enabled();

                    if ( $buffer = $this->get_buffer() ) {
                        $from = $date_helper->get_time_sum( $from, -$buffer, $unit );
                        $to   = $date_helper->get_time_sum( $to, $buffer, $unit );
                    }

                    $count_max_booked_bookings_args = compact( 'product_id', 'from', 'to', 'unit', 'include_externals', 'count_persons_as_bookings', 'get_post_args' );
                    $number_of_bookings             = YITH_WCBK_Booking_Helper()->count_max_booked_bookings_per_unit_in_period( $count_max_booked_bookings_args );
                    $max_booking_per_block          = $this->get_max_per_block();

                    $booking_weight = !!$count_persons_as_bookings ? $persons : 1;
                    if ( $number_of_bookings + $booking_weight > $max_booking_per_block ) {
                        $available = false;
                    }
                }
            }

            $available = apply_filters( 'yith_wcbk_booking_is_available', $available, $args, $this );

            return $available;
        }


        public function check_availability( $from, $to, $exclude_time = false ) {
            $date_helper = YITH_WCBK_Date_Helper();
            $available   = true;

            $availability_ranges = YITH_WCBK()->settings->get_global_availability_ranges();
            $availability_ranges = array_merge( $availability_ranges, (array)$this->get_availability_ranges() );
            /** @var YITH_WCBK_Availability_Range[] $availability_ranges */

            $tmp_from = $from;
            $tmp_to   = $to - 1; // subtract one second to fix days and months availability (include the last rule day)

            foreach ( $availability_ranges as $range ) {

                $range_is_bookable = $range->get_bookable() === 'yes';

                $check = $date_helper->check_date_inclusion_in_range( $range->get_type(), $range->get_from(), $range->get_to(), $tmp_from, $tmp_to, !$range_is_bookable );
                $check = apply_filters( 'yith_wcbk_booking_is_available_check_is_in_range', $check, $range, $tmp_from, $tmp_to, $available, $this );

                if ( $check && 'month' !== $this->get_duration_unit() && $range->has_days_enabled() && !!$range->get_days() ) {

                    $range_is_bookable = true;
                    $check             = false;

                    foreach ( $range->get_days() as $day_number => $day_bookable ) {
                        $day_is_bookable = $day_bookable === 'yes';
                        $intersect       = !$day_is_bookable;
                        $day_check       = $date_helper->check_date_inclusion_in_range( 'day', $day_number, $day_number, $tmp_from, $tmp_to, $intersect );

                        if ( $day_check ) {
                            $check = true;

                            if ( 'disabled' === $day_bookable ) {
                                $range_is_bookable = $range_is_bookable && $available;
                                continue;
                            }

                            if ( !$exclude_time && in_array( $this->get_duration_unit(), array( 'hour', 'minute' ) ) ) {
                                $time_from = $range->get_day_time_from( $day_number );
                                $time_to   = $range->get_day_time_to( $day_number );
                                if ( '00:00' === $time_to )
                                    $time_to = '24:00';

                                $time_check = $date_helper->check_date_inclusion_in_range( 'time', $time_from, $time_to, $tmp_from, $tmp_to, $intersect );

                                if ( $time_check )
                                    $range_is_bookable = $range_is_bookable && $day_is_bookable;
                                else
                                    $range_is_bookable = $range_is_bookable && $available;
                            } else {
                                $range_is_bookable = $range_is_bookable && $day_is_bookable;
                            }
                        }
                    }
                }

                if ( $check )
                    $available = $range_is_bookable;
            }

            return $available;

        }

        /**
         * Check if the duration type is "Fixed blocks"
         *
         * @return boolean
         */
        public function is_type_fixed_blocks() {
            return 'fixed' === $this->get_booking_prop( 'duration_type' );
        }

        /**
         * Returns false if the product cannot be bought.
         *
         * @return bool
         */
        public function is_purchasable() {
            $purchasable = true;

            // Products must exist of course
            if ( !$this->exists() ) {
                $purchasable = false;

                // Check the product is published
            } elseif ( ( $this instanceof WC_Data ? $this->get_status() : $this->post->post_status ) !== 'publish' && !current_user_can( 'edit_post', $this->get_id() ) ) {
                $purchasable = false;
            }

            return apply_filters( 'woocommerce_is_purchasable', $purchasable, $this );
        }

        /**
         * Check if Admin has to confirm before purchase booking
         *
         * @return boolean
         */
        public function is_requested_confirmation() {
            return 'yes' === $this->get_booking_prop( 'request_confirmation' );
        }

        /**
         * The booking product is sold individually
         *
         * @return boolean
         */
        public function is_sold_individually() {
            return true;
        }

        /**
         * Checks if a product is virtual (has no shipping).
         *
         * @return bool
         */
        public function is_virtual() {
            return apply_filters( 'yith_wcbk_booking_product_is_virtual', parent::is_virtual(), $this );
        }

        /**
         * create an array of available times
         *
         * @since 2.0.0
         *
         * @param string $from
         * @param int    $duration
         *
         * @return array
         */
        public function create_availability_time_array( $from = '', $duration = 0 ) {
            $function     = __FUNCTION__;
            $cached_key   = compact( 'function', 'from', 'duration' );
            $cached_value = YITH_WCBK_Cache()->get_product_data( $this->get_id(), $cached_key );
            if ( !is_null( $cached_value ) ) {
                $times = $cached_value;
            } else {
                $times = array();
                $unit  = $this->get_duration_unit();
                if ( in_array( $unit, array( 'hour', 'minute' ) ) ) {
                    $date_helper      = YITH_WCBK_Date_Helper();
                    $booking_duration = $this->get_booking_prop( 'duration' );
                    $duration         = !!$duration ? $duration : $this->get_min_duration();
                    $daily_start_time = $this->get_daily_start_time();

                    if ( !$from ) {
                        $from = strtotime( $daily_start_time );
                    } else {
                        if ( !is_numeric( $from ) ) {
                            $from = strtotime( $from );
                        }

                        $daily_start_timestamp = strtotime( $daily_start_time, $from );
                        $from                  = max( $from, $daily_start_timestamp );
                    }

                    $tomorrow     = $date_helper->get_time_sum( $from, 1, 'day', true );
                    $current_time = $from;

                    if ( $this->is_time_increment_based_on_duration() ) {
                        $unit_increment = $booking_duration * $this->get_min_duration();
                    } else {
                        $unit_increment = 'hour' === $unit ? 1 : yith_wcbk_get_minimum_minute_increment();
                    }

                    $unit_increment = apply_filters( 'yith_wcbk_booking_product_create_availability_time_array_unit_increment', $unit_increment, $this, $from, $duration );


                    while ( $current_time < $tomorrow ) {
                        $_duration    = absint( $duration ) * $booking_duration;
                        $_to          = $date_helper->get_time_sum( $current_time, $_duration, $unit );
                        $is_available = $this->is_available( array( 'from' => $current_time, 'to' => $_to ) );
                        if ( $is_available ) {
                            $time_to_add = date( 'H:i', $current_time );
                            $times[]     = $time_to_add;
                        }
                        $current_time = $date_helper->get_time_sum( $current_time, $unit_increment, $unit );
                    }
                }
                YITH_WCBK_Cache()->set_product_data( $this->get_id(), $cached_key, $times );
            }

            return $times;
        }

        /**
         * return true if duration unit is hour or minute
         *
         * @since 2.0.0
         *
         * @return bool
         */
        public function has_time() {
            return in_array( $this->get_duration_unit(), array( 'hour', 'minute' ) );
        }

        /**
         * @param int    $year
         * @param int    $month
         * @param string $return
         * @param string $range day or month
         * @param bool   $exclude_booked
         * @param bool   $check_start_date
         * @param bool   $check_min_max_duration
         *
         * @return array
         */
        public function create_availability_month_calendar( $year = 0, $month = 0, $return = 'all', $range = 'day', $exclude_booked = false, $check_start_date = true, $check_min_max_duration = true ) {
            $disable_day_if_no_time = YITH_WCBK()->settings->get( 'disable-day-if-no-time-available', 'no' ) === 'yes';

            // default for year and month
            $year  = $year == 0 ? date( 'Y', time() ) : $year;
            $month = $month == 0 ? date( 'm', time() ) : $month;

            $month_calendar = array();

            $first_day_of_month      = strtotime( $year . '-' . $month . '-01' );
            $first_day_of_next_month = strtotime( ' + 1 month', $first_day_of_month );

            $current_day = $first_day_of_month;
            while ( $current_day < $first_day_of_next_month ) {
                $number_of_day = date( 'j', $current_day );
                switch ( $range ) {
                    case 'month':
                        $next_day = $first_day_of_next_month;
                        break;
                    case 'day':
                    default:
                        $next_day = strtotime( ' + 1 day', $current_day );
                }

                $is_available = $this->is_available( array( 'from' => $current_day, 'exclude_booked' => $exclude_booked, 'exclude_time' => true, 'check_start_date' => $check_start_date, 'check_min_max_duration' => $check_min_max_duration ) );
                if ( $disable_day_if_no_time && $this->has_time() ) {
                    $check = true;

                    if ( apply_filters( 'yith_wcbk_disable_day_if_no_time_available_check_only_if_bookings_exist', false ) ) {
                        $_count_args = array(
                            'product_id'        => $this->get_id(),
                            'from'              => $current_day,
                            'to'                => $next_day,
                            'include_externals' => true,
                        );
                        $check       = $exclude_booked ? 1 : YITH_WCBK_Booking_Helper()->count_booked_bookings_in_period( $_count_args );
                    }

                    if ( $check ) {
                        $is_available = $is_available && $this->create_availability_time_array( $current_day );
                    }
                }

                switch ( $return ) {
                    case 'bookable':
                        if ( $is_available ) {
                            $month_calendar[ $number_of_day ] = $is_available;
                        }
                        break;
                    case 'not_bookable':
                        if ( !$is_available ) {
                            $month_calendar[ $number_of_day ] = $is_available;
                        }
                        break;
                    default:
                        $month_calendar[ $number_of_day ] = $is_available;

                }
                $current_day = $next_day;
            }

            return $month_calendar;
        }

        /**
         * create availability year calendar
         *
         * @param int    $year
         * @param int    $from_month
         * @param int    $to_month
         * @param string $return
         * @param string $range
         * @param bool   $exclude_booked
         * @param bool   $check_start_date
         * @param bool   $check_min_max_duration
         *
         * @return array
         */
        public function create_availability_year_calendar( $year = 0, $from_month = 1, $to_month = 12, $return = 'all', $range = 'day', $exclude_booked = false, $check_start_date = true, $check_min_max_duration = true ) {
            $year_calendar = array();
            for ( $i = $from_month; $i <= $to_month; $i++ ) {
                $this_month_calendar = $this->create_availability_month_calendar( $year, $i, $return, $range, $exclude_booked, $check_start_date, $check_min_max_duration );
                if ( !empty( $this_month_calendar ) )
                    $year_calendar[ $i ] = $this_month_calendar;
            }

            return $year_calendar;
        }

        /**
         * create availability calendar
         *
         * @param        $from_year
         * @param        $from_month
         * @param        $to_year
         * @param        $to_month
         * @param string $return
         * @param string $range
         * @param bool   $exclude_booked
         * @param bool   $check_start_date
         * @param bool   $check_min_max_duration
         *
         * @return array
         */
        public function create_availability_calendar( $from_year, $from_month, $to_year, $to_month, $return = 'all', $range = 'day', $exclude_booked = false, $check_start_date = true, $check_min_max_duration = true ) {
            $calendar = array();

            for ( $year = $from_year; $year <= $to_year; $year++ ) {
                $first_month        = $year == $from_year ? $from_month : 1;
                $last_month         = $year == $to_year ? ( $to_month - 1 ) : 12; // last month is not included
                $this_year_calendar = $this->create_availability_year_calendar( $year, $first_month, $last_month, $return, $range, $exclude_booked, $check_start_date, $check_min_max_duration );
                if ( !empty( $this_year_calendar ) )
                    $calendar[ $year ] = $this_year_calendar;

            }

            return $calendar;
        }

        /**
         * get non available dates
         *
         * @param        $from_year
         * @param        $from_month
         * @param        $to_year
         * @param        $to_month
         * @param string $range
         * @param bool   $exclude_booked
         * @param bool   $check_start_date
         * @param bool   $check_min_max_duration
         *
         * @return array
         */
        public function get_not_available_dates( $from_year, $from_month, $to_year, $to_month, $range = 'day', $exclude_booked = false, $check_start_date = false, $check_min_max_duration = true ) {
            $args  = compact( 'from_year', 'from_month', 'to_year', 'to_month', 'range', 'exclude_booked', 'check_start_date', 'check_min_max_duration' );
            $dates = apply_filters( 'yith_wcbk_product_get_not_available_dates_before', null, $args, $this );
            if ( !is_null( $dates ) ) {
                return $dates;
            }
            $cached_key   = array_merge( array( 'function' => __FUNCTION__ ), $args );
            $cached_value = YITH_WCBK_Cache()->get_product_data( $this->get_id(), $cached_key );

            if ( !is_null( $cached_value ) ) {
                $dates = $cached_value;
            } else {
                $calendar = $this->create_availability_calendar( $from_year, $from_month, $to_year, $to_month, 'not_bookable', $range, $exclude_booked, $check_start_date, $check_min_max_duration );
                $dates    = array();
                foreach ( $calendar as $year => $months ) {
                    foreach ( $months as $month => $days ) {
                        if ( $month < 10 ) {
                            $month = '0' . $month;
                        }
                        foreach ( $days as $day => $bookable ) {
                            if ( $day < 10 ) {
                                $day = '0' . $day;
                            }
                            $dates[] = $year . '-' . $month . '-' . $day;
                        }
                    }
                }
                YITH_WCBK_Cache()->set_product_data( $this->get_id(), $cached_key, $dates );
            }

            return $dates;
        }

        /**
         * get non available months
         *
         * @param        $from_year
         * @param        $from_month
         * @param        $to_year
         * @param        $to_month
         *
         * @return array
         */
        public function get_not_available_months( $from_year, $from_month, $to_year, $to_month ) {
            $dates           = $this->get_not_available_dates( $from_year, $from_month, $to_year, $to_month, 'month', false, false );
            $number_of_dates = count( $dates );
            if ( $number_of_dates < 1 ) {
                return array();
            }

            $zero_array  = array_fill( 0, $number_of_dates, 0 );
            $seven_array = array_fill( 0, $number_of_dates, 7 );
            $dates       = array_map( 'substr', $dates, $zero_array, $seven_array );

            return $dates;
        }

        /**
         * Get the add to cart button text for the single page.
         *
         * @return string
         */
        public function single_add_to_cart_text() {
            $booking_single_add_to_cart_text = !$this->is_requested_confirmation() ? yith_wcbk_get_label( 'add-to-cart' ) : yith_wcbk_get_label( 'request-confirmation' );

            return apply_filters( 'woocommerce_product_single_add_to_cart_text', $booking_single_add_to_cart_text, $this );
        }

        /**
         * return true if it's possible showing availability of the current product in calendar
         *
         * @since 2.0.3
         *
         * @param string $step
         *
         * @return bool
         */
        public function can_show_availability( $step = '' ) {
            $show = $this->get_max_per_block() > 1;
            if ( $step ) {
                switch ( $step ) {
                    case 'day':
                        $show = $show && 'day' === $this->get_duration_unit();
                        break;
                    case 'h':
                    case 'hour':
                    case 'hours':
                        $show = $show && $this->has_time();
                        break;
                    case 'm':
                    case 'minute':
                    case 'minutes':
                        $show = $show && 'minute' === $this->get_duration_unit();
                        break;
                }
            }
            return $show;
        }


        /**
         * get the admin calendar Url
         *
         * @since 2.0.3
         * @return string
         */
        public function get_admin_calendar_url() {
            $args = array(
                'post_type'  => YITH_WCBK_Post_Types::$booking,
                'page'       => 'yith-wcbk-booking-calendar',
                'product_id' => $this->get_id(),
            );
            $url  = add_query_arg( $args, admin_url( 'edit.php' ) );

            return apply_filters( 'yith_wcbk_product_get_admin_calendar_url', $url, $this );
        }

        /**
         * Sync product price for sorting
         * TODO: move it into Booking Store Data Store (see Variable Product Data Store)
         *
         * @since 2.0.5
         */
        public function sync_price() {
            do_action( 'yith_wcbk_product_sync_price_before', $this );
            delete_post_meta( $this->get_id(), '_price' );
            $price = $this->calculate_price();
            if ( $price ) {
                add_post_meta( $this->get_id(), '_price', $price );
            }
            yith_wcbk_maybe_debug( sprintf( 'Sync Product Price #%s', $this->get_id() ) );
            do_action( 'yith_wcbk_product_sync_price_after', $this );
        }
    }
}
