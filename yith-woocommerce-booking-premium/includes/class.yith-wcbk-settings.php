<?php
!defined( 'YITH_WCBK' ) && exit; // Exit if accessed directly

if ( !class_exists( 'YITH_WCBK_Settings' ) ) {
    /**
     * Class YITH_WCBK_Settings
     *
     * @author Leanza Francesco <leanzafrancesco@gmail.com>
     */
    class YITH_WCBK_Settings {
        /** @var YITH_WCBK_Settings */
        private static $_instance;

        /**
         * Singleton implementation
         *
         * @return YITH_WCBK_Settings
         */
        public static function get_instance() {
            return !is_null( self::$_instance ) ? self::$_instance : self::$_instance = new self();
        }

        /**
         * YITH_WCBK_Settings constructor.
         */
        private function __construct() {
            if ( is_admin() ) {
                $this->save_settings();
            }
        }

        /**
         * Save Settings
         */
        public function save_settings() {
            if ( isset( $_POST[ 'yith-wcbk-cache-check-for-transient-creation' ] ) ) {
                if ( isset( $_POST[ 'yith-wcbk-cache-enabled' ] ) && 'yes' === $_POST[ 'yith-wcbk-cache-enabled' ] ) {
                    delete_transient( 'yith-wcbk-cache-disabled' );
                } else {
                    set_transient( 'yith-wcbk-cache-disabled', '1', DAY_IN_SECONDS );
                }
            }

            if ( empty( $_POST[ 'yith_wcbk_nonce' ] ) || !wp_verify_nonce( $_POST[ 'yith_wcbk_nonce' ], 'yith_wcbk_settings_fields' ) ) {
                return;
            }

            if ( isset( $_POST[ 'yith_booking_global_availability_range' ] ) ) {
                $ranges         = $_POST[ 'yith_booking_global_availability_range' ];
                $type_array     = isset( $ranges[ 'type' ] ) ? $ranges[ 'type' ] : array();
                $from_array     = isset( $ranges[ 'from' ] ) ? $ranges[ 'from' ] : array();
                $to_array       = isset( $ranges[ 'to' ] ) ? $ranges[ 'to' ] : array();
                $bookable_array = isset( $ranges[ 'bookable' ] ) ? $ranges[ 'bookable' ] : array();
                $days_enabled   = isset( $ranges[ 'days_enabled' ] ) ? $ranges[ 'days_enabled' ] : array();
                $days           = isset( $ranges[ 'days' ] ) ? $ranges[ 'days' ] : array();

                $availability_size = sizeof( $type_array );
                $values            = array();
                for ( $i = 1; $i < $availability_size; $i++ ) {
                    if ( !empty( $type_array[ $i ] ) && !empty( $from_array[ $i ] ) && !empty( $to_array[ $i ] ) && !empty( $bookable_array[ $i ] ) ) {
                        $current_range = array(
                            'type'         => $type_array[ $i ],
                            'from'         => $from_array[ $i ],
                            'to'           => $to_array[ $i ],
                            'bookable'     => $bookable_array[ $i ],
                            'days_enabled' => $days_enabled[ $i ],
                            'days'         => array(),
                        );

                        if ( $days_enabled[ $i ] === 'yes' ) {
                            $current_range_days = array();
                            for ( $j = 1; $j <= 7; $j++ ) {
                                $current_range_days[ $j ] = isset( $days[ $j ][ $i ] ) ? $days[ $j ][ $i ] : $bookable_array[ $i ];
                            }

                            $current_range[ 'days' ] = $current_range_days;
                        }

                        $values[] = (object)$current_range;
                    }
                }

                update_option( 'yith_wcbk_booking_global_availability_ranges', $values );
            } elseif ( isset( $_POST[ 'yith_booking_global_cost_ranges' ] ) ) {
                $ranges              = $_POST[ 'yith_booking_global_cost_ranges' ];
                $type_array          = isset( $ranges[ 'type' ] ) ? $ranges[ 'type' ] : array();
                $from_array          = isset( $ranges[ 'from' ] ) ? $ranges[ 'from' ] : array();
                $to_array            = isset( $ranges[ 'to' ] ) ? $ranges[ 'to' ] : array();
                $base_cost_operator  = isset( $ranges[ 'base_cost_operator' ] ) ? $ranges[ 'base_cost_operator' ] : array();
                $base_cost           = isset( $ranges[ 'base_cost' ] ) ? $ranges[ 'base_cost' ] : array();
                $block_cost_operator = isset( $ranges[ 'block_cost_operator' ] ) ? $ranges[ 'block_cost_operator' ] : array();
                $block_cost          = isset( $ranges[ 'block_cost' ] ) ? $ranges[ 'block_cost' ] : array();

                $costs_size = sizeof( $type_array );
                $values     = array();
                for ( $i = 1; $i < $costs_size; $i++ ) {
                    if ( isset( $type_array[ $i ] ) && isset( $from_array[ $i ] ) && isset( $to_array[ $i ] ) && isset( $base_cost_operator[ $i ] ) && isset( $block_cost_operator[ $i ] ) ) {

                        $single_base_cost  = !empty( $base_cost[ $i ] ) ? $base_cost[ $i ] : '';
                        $single_block_cost = !empty( $block_cost[ $i ] ) ? $block_cost[ $i ] : '';

                        $values[] = (object)array(
                            'type'                => $type_array[ $i ],
                            'from'                => $from_array[ $i ],
                            'to'                  => $to_array[ $i ],
                            'base_cost_operator'  => $base_cost_operator[ $i ],
                            'base_cost'           => $single_base_cost,
                            'block_cost_operator' => $block_cost_operator[ $i ],
                            'block_cost'          => $single_block_cost,
                        );
                    }
                }

                update_option( 'yith_wcbk_booking_global_cost_ranges', $values );
            }
        }

        /**
         * retrieves the global availability range array
         *
         * @return YITH_WCBK_Availability_Range[]
         */
        public function get_global_availability_ranges() {
            $ranges = get_option( 'yith_wcbk_booking_global_availability_ranges', array() );

            if ( !!$ranges && is_array( $ranges ) ) {
                $ranges = array_map( 'yith_wcbk_availability_range', $ranges );
            }

            return !!$ranges && is_array( $ranges ) ? $ranges : array();
        }


        /**
         * retrieves the global cost range array
         *
         * @return array the array of ranges as StdClass
         */
        public function get_global_cost_ranges() {
            $ranges = get_option( 'yith_wcbk_booking_global_cost_ranges', array() );

            return !!$ranges && is_array( $ranges ) ? $ranges : array();
        }

        /**
         * get settings related to Booking plugin
         *
         * @param      $key
         * @param bool $default
         *
         * @return mixed
         */
        public function get( $key, $default = false ) {
            return get_option( 'yith-wcbk-' . $key, $default );
        }

        /**
         * return true if showing booking form requires login
         *
         * @return bool
         * @since 1.0.5
         */
        public function show_booking_form_to_logged_users_only() {
            return $this->get( 'show-booking-form-to-logged-users-only', 'no' ) === 'yes';
        }

        /**
         * return true if check min max duration in calendar is enabled
         *
         * @return bool
         * @since 2.0.3
         */
        public function check_min_max_duration_in_calendar() {
            return $this->get( 'check-min-max-duration-in-calendar', 'yes' ) === 'yes';
        }

        /**
         * return true if people selector is enabled
         *
         * @return bool
         */
        public function is_people_selector_enabled() {
            return $this->get( 'people-selector-enabled', 'yes' ) === 'yes';
        }

        /**
         * return true if unique calendar range picker is enabled
         *
         * @return bool
         */
        public function is_unique_calendar_range_picker_enabled() {
            return $this->get( 'unique-calendar-range-picker-enabled', 'yes' ) === 'yes';
        }

        /**
         * return true if show included services is enabled
         *
         * @return bool
         */
        function show_included_services() {
            return $this->get( 'show-included-services', 'yes' ) === 'yes';
        }

        /**
         * return the number of months to show in calendar
         *
         * @return int
         */
        function get_months_loaded_in_calendar() {
            $months = absint( $this->get( 'months-loaded-in-calendar', 12 ) );
            $months = min( 12, max( 1, $months ) );

            return $months;
        }

        /**
         * return true if cache is enabled
         *
         * @since 2.0.5
         * @return bool
         */
        public function is_cache_enabled() {
            return $this->get( 'cache-enabled', 'yes' ) !== 'no' || !get_transient( 'yith-wcbk-cache-disabled' );
        }
    }
}