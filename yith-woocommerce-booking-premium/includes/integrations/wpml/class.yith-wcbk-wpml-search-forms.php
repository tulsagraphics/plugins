<?php
!defined( 'ABSPATH' ) && exit; // Exit if accessed directly

/**
 * Class YITH_WCBK_Wpml_Search_Forms
 *
 * @author Leanza Francesco <leanzafrancesco@gmail.com>
 * @since   1.0.10
 */
class YITH_WCBK_Wpml_Search_Forms {
    /** @var YITH_WCBK_Wpml_Search_Forms */
    private static $_instance;

    /** @var YITH_WCBK_Wpml_Integration */
    public $wpml_integration;

    /**
     * Singleton Implementation
     *
     * @param YITH_WCBK_Wpml_Integration $wpml_integration
     *
     * @return YITH_WCBK_Wpml_Search_Forms
     */
    public static function get_instance( $wpml_integration ) {
        return !is_null( self::$_instance ) ? self::$_instance : self::$_instance = new static( $wpml_integration );
    }

    /**
     * Constructor
     *
     * @access private
     *
     * @param YITH_WCBK_Wpml_Integration $wpml_integration
     */
    private function __construct( $wpml_integration ) {
        $this->wpml_integration = $wpml_integration;

        add_action( 'yith_wcbk_search_booking_products_before_get_results', array( $this->wpml_integration, 'set_current_language_to_default' ) );
        add_action( 'yith_wcbk_search_booking_products_after_get_results', array( $this->wpml_integration, 'restore_current_language' ) );
    }

}