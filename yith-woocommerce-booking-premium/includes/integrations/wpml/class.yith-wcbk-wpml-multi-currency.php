<?php
!defined( 'ABSPATH' ) && exit; // Exit if accessed directly

/**
 * Class YITH_WCBK_Wpml_Multi_Currency
 *
 * @author  Leanza Francesco <leanzafrancesco@gmail.com>
 * @since   2.0.3
 */
class YITH_WCBK_Wpml_Multi_Currency {
    /** @var YITH_WCBK_Wpml_Multi_Currency */
    private static $_instance;

    /** @var YITH_WCBK_Wpml_Integration */
    public $wpml_integration;

    /**
     * Singleton implementation
     *
     * @param YITH_WCBK_Wpml_Integration $wpml_integration
     *
     * @return YITH_WCBK_Wpml_Multi_Currency
     */
    public static function get_instance( $wpml_integration ) {
        return !is_null( self::$_instance ) ? self::$_instance : self::$_instance = new static( $wpml_integration );
    }

    /**
     * Constructor
     *
     * @access private
     *
     * @param YITH_WCBK_Wpml_Integration $wpml_integration
     */
    private function __construct( $wpml_integration ) {
        $this->wpml_integration = $wpml_integration;

        add_filter( 'woocommerce_product_get_price', array( $this, 'multi_currency_price' ), 10, 2 );
        add_action( 'yith_wcbk_booking_form_start', array( $this, 'add_currency_hidden_input_in_booking_form' ), 20 );

        add_action( 'wp_ajax_yith_wcbk_get_booking_data', array( $this, 'set_currency_and_filters' ), 9 );
        add_action( 'wp_ajax_nopriv_yith_wcbk_get_booking_data', array( $this, 'set_currency_and_filters' ), 9 );
    }

    /**
     * return true if the current version of WPML Multi Currency has the right classes and methods
     *
     * @return bool
     */
    public function check_wpml_classes() {
        global $woocommerce_wpml;

        return $woocommerce_wpml && isset( $woocommerce_wpml->multi_currency )
            && isset( $woocommerce_wpml->multi_currency->prices )
            && is_callable( array( $woocommerce_wpml->multi_currency, 'set_client_currency' ) )
            && is_callable( array( $woocommerce_wpml->multi_currency, 'get_client_currency' ) );
    }

    public function set_currency_and_filters() {
        global $woocommerce_wpml;
        if ( !$this->check_wpml_classes() || empty( $_REQUEST[ 'yith_wcbk_wpml_currency' ] ) )
            return;

        // Set the currency
        $currency = $_REQUEST[ 'yith_wcbk_wpml_currency' ];
        $woocommerce_wpml->multi_currency->set_client_currency( $currency );

        // Add filters
        $multi_currency_prices = $woocommerce_wpml->multi_currency->prices;
        if ( is_callable( array( $multi_currency_prices, 'currency_filter' ) ) )
            add_filter( 'woocommerce_currency', array( $multi_currency_prices, 'currency_filter' ) );

        add_filter( 'yith_wcbk_get_calculated_price_html_price', array( $this, 'mutli_currency_price_for_calculated_one' ), 10, 3 );
    }

    /**
     * apply the multi currency price for calculated booking price
     *
     * @param $price
     * @param $args
     * @param $product
     *
     * @return float
     */
    function mutli_currency_price_for_calculated_one( $price, $args, $product ) {
        return $this->multi_currency_price( $price, $product );
    }

    /**
     * change price based on currency
     *
     * @param      $price
     * @param bool $product
     *
     * @return float
     */
    function multi_currency_price( $price, $product = false ) {
        if ( $product && $product->is_type( YITH_WCBK_Product_Post_Type_Admin::$prod_type ) ) {
            $price = apply_filters( 'wcml_raw_price_amount', $price );
        }
        return $price;
    }

    /**
     * add an hidden input field in booking form
     */
    public function add_currency_hidden_input_in_booking_form() {
        global $woocommerce_wpml;
        if ( !$this->check_wpml_classes() )
            return;

        $client_currency = $woocommerce_wpml->multi_currency->get_client_currency();
        echo "<input type='hidden' class='yith-wcbk-booking-form-additional-data' name='yith_wcbk_wpml_currency' value='$client_currency' />";
    }
}