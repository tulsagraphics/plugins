<?php
!defined( 'ABSPATH' ) && exit; // Exit if accessed directly

/**
 * Class YITH_WCBK_Wpml_Booking_Product
 *
 * @author  Leanza Francesco <leanzafrancesco@gmail.com>
 * @since   1.0.3
 */
class YITH_WCBK_Wpml_Booking_Product {
    /** @var YITH_WCBK_Wpml_Booking_Product */
    private static $_instance;

    /** @var YITH_WCBK_Wpml_Integration */
    public $wpml_integration;

    /**
     * Singleton implementation
     *
     * @param YITH_WCBK_Wpml_Integration $wpml_integration
     *
     * @return YITH_WCBK_Wpml_Booking_Product
     */
    public static function get_instance( $wpml_integration ) {
        return !is_null( self::$_instance ) ? self::$_instance : self::$_instance = new static( $wpml_integration );
    }

    /**
     * Constructor
     *
     * @access private
     *
     * @param YITH_WCBK_Wpml_Integration $wpml_integration
     */
    private function __construct( $wpml_integration ) {
        $this->wpml_integration = $wpml_integration;

        // filter the get method of the Booking Product to retrieve the translated meta
        add_filter( 'yith_wcbk_booking_product_get_booking_prop', array( $this, 'booking_product_get' ), 10, 3 );

        // get the parent id of the booking product to associate it to the Booking object
        add_filter( 'yith_wcbk_booking_product_id_to_translate', array( 'YITH_WCBK_Wpml_Integration', 'get_parent_id' ) );
    }


    /**
     * Filter the __get method in Booking Product
     *
     * @param                    $value
     * @param                    $key
     * @param WC_Product_Booking $product
     *
     * @return mixed
     */
    public function booking_product_get( $value, $key, $product ) {
        if ( !in_array( 'yith_booking_' . $key, YITH_WCBK_Wpml_Integration::get_meta_to_copy_from_parent_product() ) ) {
            return $value;
        }

        $parent_id = apply_filters( 'yith_wcbk_booking_product_id_to_translate', $product->get_id() );
        if ( !$parent_id || $parent_id === $product->get_id() )
            return $value;

        /** @var WC_Product_Booking $parent */
        $parent = wc_get_product( $parent_id );
        if ( $parent )
            $value = $parent->get_booking_prop( $key );

        return $value;
    }
}