<?php
!defined( 'YITH_WCBK' ) && exit; // Exit if accessed directly

if ( !class_exists( 'YITH_WCBK_Availability_Range' ) ) {
    /**
     * Class YITH_WCBK_Availability_Range
     *
     * @version 2.0.0
     * @author Leanza Francesco <leanzafrancesco@gmail.com>
     */
    class YITH_WCBK_Availability_Range {
        /** @var array */
        private $data
            = array(
                'type'          => '',
                'from'          => '',
                'to'            => '',
                'bookable'      => 'disabled',
                'days_enabled'  => 'no',
                'days'          => array(),
                'times_enabled' => 'no',
                'day_time_from' => array(),
                'day_time_to'   => array()
            );

        /**
         * YITH_WCBK_Availability_Range constructor.
         *
         * @param array|object $args
         */
        public function __construct( $args = array() ) {
            if ( is_object( $args ) ) {
                $args = get_object_vars( $args );
            }

            $this->data = wp_parse_args( $args, $this->data );
        }

        /**
         * Magic Method __get
         * for backward compatibility
         *
         * @param $key
         *
         * @return mixed|null
         */
        public function __get( $key ) {
            $getter = 'get_' . $key;
            $value  = is_callable( array( $this, $getter ) ) ? $this->$getter : $this->get_prop( $key );
            if ( $value !== null ) {
                $this->$key = $value;
            }

            return $value;
        }

        /**
         * get object properties
         *
         * @param $key
         *
         * @return mixed|null
         */
        private function get_prop( $key ) {
            return array_key_exists( $key, $this->data ) ? $this->data[ $key ] : null;
        }

        /**
         * get the type
         *
         * @return string|null possible values: month, custom
         */
        public function get_type() {
            return $this->get_prop( 'type' );
        }

        /**
         * return the from value
         *
         * @return string|null
         */
        public function get_from() {
            return $this->get_prop( 'from' );
        }

        /**
         * return the to value
         *
         * @return string|null
         */
        public function get_to() {
            return $this->get_prop( 'to' );
        }

        /**
         * return the bookable value
         *
         * @return string|null possible values: yes, no, disabled
         */
        public function get_bookable() {
            return $this->get_prop( 'bookable' );

        }

        /**
         * return days are enabled
         *
         * @return string
         */
        public function get_days_enabled() {
            return $this->get_prop( 'days_enabled' );
        }

        /**
         * return days
         *
         * @return array
         */
        public function get_days() {
            return $this->get_prop( 'days' );
        }

        /**
         * return times enabled
         *
         * @return string
         */

        public function get_times_enabled() {
            return $this->get_prop( 'times_enabled' );
        }

        /**
         * return day time from
         *
         * @param bool|int $day_number
         *
         * @return array|string
         */
        public function get_day_time_from( $day_number = false ) {
            $value = $this->get_prop( 'day_time_from' );
            if ( is_numeric( $day_number ) ) {
                $value = isset( $value[ $day_number ] ) ? $value[ $day_number ] : '00:00';
            }

            return $value;
        }

        /**
         * return day time to
         *
         * @param bool|int $day_number
         *
         * @return array
         */

        public function get_day_time_to( $day_number = false ) {
            $value = $this->get_prop( 'day_time_to' );
            if ( is_numeric( $day_number ) ) {
                $value = isset( $value[ $day_number ] ) ? $value[ $day_number ] : '00:00';
            }

            return $value;
        }

        /**
         * return true if has days enabled
         *
         * @return bool
         */
        public function has_days_enabled() {
            return 'yes' === $this->get_days_enabled();
        }

        /**
         * return true if has days enabled
         *
         * @return bool
         */
        public function has_times_enabled() {
            return 'yes' === $this->get_times_enabled();
        }
    }
}

if ( !function_exists( 'yith_wcbk_availability_range' ) ) {
    function yith_wcbk_availability_range( $args ) {
        return $args instanceof YITH_WCBK_Availability_Range ? $args : new YITH_WCBK_Availability_Range( $args );
    }
}