<?php
/**
 * Functions
 *
 * @author  Yithemes
 * @package YITH Booking for WooCommerce Premium
 * @version 1.0.0
 */

!defined( 'YITH_WCBK' ) && exit;

if ( !function_exists( 'yith_wcbk_get_weeks_array' ) ) {
    function yith_wcbk_get_weeks_array() {
        $weeks_array = array();
        for ( $i = 1; $i < 53; $i++ ) {
            $index                 = $i . '';
            $weeks_array[ $index ] = sprintf( __( 'Week %d', 'yith-booking-for-woocommerce' ), $index );
        }

        return $weeks_array;
    }
}

if ( !function_exists( 'yith_wcbk_get_months_array' ) ) {
    function yith_wcbk_get_months_array( $localized = true ) {
        if ( $localized ) {
            $months_array = array(
                '1'  => __( 'January', 'yith-booking-for-woocommerce' ),
                '2'  => __( 'February', 'yith-booking-for-woocommerce' ),
                '3'  => __( 'March', 'yith-booking-for-woocommerce' ),
                '4'  => __( 'April', 'yith-booking-for-woocommerce' ),
                '5'  => __( 'May', 'yith-booking-for-woocommerce' ),
                '6'  => __( 'June', 'yith-booking-for-woocommerce' ),
                '7'  => __( 'July', 'yith-booking-for-woocommerce' ),
                '8'  => __( 'August', 'yith-booking-for-woocommerce' ),
                '9'  => __( 'September', 'yith-booking-for-woocommerce' ),
                '10' => __( 'October', 'yith-booking-for-woocommerce' ),
                '11' => __( 'November', 'yith-booking-for-woocommerce' ),
                '12' => __( 'December', 'yith-booking-for-woocommerce' ),
            );
        } else {
            $months_array = array(
                '1'  => 'January',
                '2'  => 'February',
                '3'  => 'March',
                '4'  => 'April',
                '5'  => 'May',
                '6'  => 'June',
                '7'  => 'July',
                '8'  => 'August',
                '9'  => 'September',
                '10' => 'October',
                '11' => 'November',
                '12' => 'December',
            );
        }

        return $months_array;
    }
}

if ( !function_exists( 'yith_wcbk_get_days_array' ) ) {
    function yith_wcbk_get_days_array( $localized = true ) {
        if ( $localized ) {
            $days_array = array(
                '1' => __( 'Monday', 'yith-booking-for-woocommerce' ),
                '2' => __( 'Tuesday', 'yith-booking-for-woocommerce' ),
                '3' => __( 'Wednesday', 'yith-booking-for-woocommerce' ),
                '4' => __( 'Thursday', 'yith-booking-for-woocommerce' ),
                '5' => __( 'Friday', 'yith-booking-for-woocommerce' ),
                '6' => __( 'Saturday', 'yith-booking-for-woocommerce' ),
                '7' => __( 'Sunday', 'yith-booking-for-woocommerce' ),
            );
        } else {
            $days_array = array(
                '1' => 'Monday',
                '2' => 'Tuesday',
                '3' => 'Wednesday',
                '4' => 'Thursday',
                '5' => 'Friday',
                '6' => 'Saturday',
                '7' => 'Sunday',
            );
        }

        return $days_array;
    }
}

if ( !function_exists( 'yith_wcbk_get_time_sum' ) ) {
    function yith_wcbk_get_time_sum( $time, $number = 0, $unit = 'day', $midnight = false ) {
        return YITH_WCBK_Date_Helper()->get_time_sum( $time, $number, $unit, $midnight );
    }
}

if ( !function_exists( 'yith_wcbk_get_time_diff' ) ) {
    function yith_wcbk_get_time_diff( $timestamp1, $timestamp2, $unit = '' ) {
        return YITH_WCBK_Date_Helper()->get_time_diff( $timestamp1, $timestamp2, $unit );
    }
}

if ( !function_exists( 'yith_wcbk_get_duration_units' ) ) {
    function yith_wcbk_get_duration_units( $plural_control = 1 ) {
        $duration_units = array(
            'month'  => _n( 'Month', 'Months', $plural_control, 'yith-booking-for-woocommerce' ),
            'day'    => _n( 'Day', 'Days', $plural_control, 'yith-booking-for-woocommerce' ),
            'hour'   => _n( 'Hour', 'Hours', $plural_control, 'yith-booking-for-woocommerce' ),
            'minute' => _n( 'Minute', 'Minutes', $plural_control, 'yith-booking-for-woocommerce' ),
        );

        return apply_filters( 'yith_wcbk_get_duration_units', $duration_units, $plural_control );
    }
}

if ( !function_exists( 'yith_wcbk_get_duration_unit_label' ) ) {
    function yith_wcbk_get_duration_unit_label( $duration_unit, $plural_control = 1 ) {
        $units         = yith_wcbk_get_duration_units( $plural_control );
        $duration_unit = isset( $units[ $duration_unit ] ) ? $units[ $duration_unit ] : '';

        return $duration_unit;
    }
}


if ( !function_exists( 'yith_wcbk_intersect_dates' ) ) {
    function yith_wcbk_intersect_dates( $start1, $end1, $start2, $end2 ) {
        return YITH_WCBK_Date_Helper()->check_intersect_dates( $start1, $end1, $start2, $end2 );
    }
}

if ( !function_exists( 'yith_wcbk_add_one_day' ) ) {
    function yith_wcbk_add_one_day( $time ) {
        return date( 'Y-m-d', strtotime( $time . ' +1 day' ) );
    }
}

if ( !function_exists( 'yith_wcbk_add_some_day' ) ) {
    function yith_wcbk_add_some_day( $time, $days_to_add = 1 ) {
        return date( 'Y-m-d', strtotime( $time . ' +' . $days_to_add . ' day' ) );
    }
}

if ( !function_exists( 'yith_wcbk_get_booking_meta_label' ) ) {
    function yith_wcbk_get_booking_meta_label( $key ) {
        $booking_meta_labels = apply_filters( 'yith_wcbk_booking_meta_labels', array(
            'from'     => yith_wcbk_get_label( 'from' ),
            'to'       => yith_wcbk_get_label( 'to' ),
            'duration' => yith_wcbk_get_label( 'duration' ),
            'persons'  => yith_wcbk_get_label( 'people' ),
        ) );
        $label               = array_key_exists( $key, $booking_meta_labels ) ? $booking_meta_labels[ $key ] : $key;

        return apply_filters( 'yith_wcbk_get_booking_meta_label', $label, $key, $booking_meta_labels );
    }
}

if ( !function_exists( 'yith_wcbk_get_label' ) ) {
    function yith_wcbk_get_label( $key ) {
        return YITH_WCBK()->language->get_label( $key );
    }
}

if ( !function_exists( 'yith_wcbk_get_default_label' ) ) {
    function yith_wcbk_get_default_label( $key ) {
        return YITH_WCBK()->language->get_default_label( $key );
    }
}

if ( !function_exists( 'yith_wcbk_get_booking_statuses' ) ) {

    /**
     * Return the list of status available
     *
     * @return array
     * @since 1.0.0
     */

    function yith_wcbk_get_booking_statuses( $include_accessory_statuses = false ) {
        $statuses = array(
            'unpaid'          => _x( 'Unpaid', 'Booking Status', 'yith-booking-for-woocommerce' ),
            'paid'            => _x( 'Paid', 'Booking Status', 'yith-booking-for-woocommerce' ),
            'completed'       => _x( 'Completed', 'Booking Status', 'yith-booking-for-woocommerce' ),
            'cancelled'       => _x( 'Cancelled', 'Booking Status', 'yith-booking-for-woocommerce' ),
            'pending-confirm' => _x( 'Pending Confirmation', 'Booking Status', 'yith-booking-for-woocommerce' ),
            'confirmed'       => _x( 'Confirmed', 'Booking Status', 'yith-booking-for-woocommerce' ),
            'unconfirmed'     => _x( 'Rejected', 'Booking Status', 'yith-booking-for-woocommerce' ),

        );

        if ( $include_accessory_statuses ) {
            $statuses[ 'cancelled_by_user' ] = _x( 'Cancelled by customer', 'Booking Status', 'yith-booking-for-woocommerce' );
        }

        return apply_filters( 'yith_wcbk_booking_statuses', $statuses );
    }
}

if ( !function_exists( 'yith_wcbk_is_a_booking_status' ) ) {

    /**
     * check if booking status is valid
     *
     * @param string $status
     *
     * @return array
     * @since 1.0.0
     */

    function yith_wcbk_is_a_booking_status( $status ) {
        $booking_statuses = yith_wcbk_get_booking_statuses();

        return isset( $booking_statuses[ $status ] );
    }
}

if ( !function_exists( 'yith_wcbk_get_booking_status_name' ) ) {

    /**
     * Get the booking status name
     *
     * @param string $status
     *
     * @return string
     * @since 1.0.0
     */

    function yith_wcbk_get_booking_status_name( $status ) {
        return strtr( $status, yith_wcbk_get_booking_statuses() );
    }
}

if ( !function_exists( 'yith_get_booking' ) ) {

    /**
     * Get the booking object.
     *
     * @param  mixed $the_booking
     *
     * @uses   WP_Post
     * @uses   YITH_WCBK_Booking
     * @return YITH_WCBK_Booking|bool false on failure
     */
    function yith_get_booking( $the_booking ) {
        $_booking = false;
        if ( false === $the_booking ) {
            $the_booking = isset( $GLOBALS[ 'post' ] ) ? $GLOBALS[ 'post' ] : false;
            if ( $the_booking && isset( $the_booking->ID ) ) {
                $_booking = new YITH_WCBK_Booking( $the_booking->ID );
            }
        } elseif ( is_numeric( $the_booking ) ) {
            $_booking = new YITH_WCBK_Booking( $the_booking );
        } elseif ( $the_booking instanceof YITH_WCBK_Booking ) {
            $_booking = $the_booking;
        } elseif ( !( $the_booking instanceof WP_Post ) ) {
            $_booking = false;
        }

        if ( $_booking instanceof YITH_WCBK_Booking ) {
            if ( !$_booking->is_valid() ) {
                $_booking = false;
            }
        } else {
            $_booking = false;
        }

        return apply_filters( 'yith_wcbk_booking_object', $_booking );
    }
}

if ( !function_exists( 'yith_wcbk_booking_admin_screen_ids' ) ) {

    /**
     * Return booking screen ids
     *
     * @return array
     */
    function yith_wcbk_booking_admin_screen_ids() {
        $booking_post_type = YITH_WCBK_Post_Types::$booking;
        $screen_ids        = array(
            YITH_WCBK_Post_Types::$booking,
            'edit-' . YITH_WCBK_Post_Types::$booking,
            YITH_WCBK_Post_Types::$search_form,
            'edit-' . YITH_WCBK_Post_Types::$search_form,
            'edit-' . YITH_WCBK_Post_Types::$service_tax,
            'product',
            'yith_booking_page_create_booking',
            'yith_booking_page_yith-wcbk-booking-calendar',
        );

        return apply_filters( 'yith_wcbk_booking_admin_screen_ids', $screen_ids );
    }
}

if ( !function_exists( 'yith_wcbk_parse_booking_person_types_array' ) ) {
    function yith_wcbk_parse_booking_person_types_array( $person_types, $reverse = false ) {
        if ( is_array( $person_types ) ) {

            if ( !$reverse ) {
                $new_person_types = array();
                foreach ( $person_types as $person_type_id => $person_type_number ) {
                    $person_type_title  = get_the_title( $person_type_id );
                    $new_person_types[] = array(
                        'id'     => $person_type_id,
                        'title'  => $person_type_title,
                        'number' => $person_type_number,
                    );
                }

                return $new_person_types;
            } else {
                $new_person_types = array();
                foreach ( $person_types as $person_type ) {
                    $new_person_types[ $person_type[ 'id' ] ] = $person_type[ 'number' ];
                }

                return $new_person_types;
            }
        } else {
            return array();
        }
    }
}

if ( !function_exists( 'yith_wcbk_add_product_class' ) ) {

    function yith_wcbk_add_product_class( $classes ) {
        $classes[] = 'product';

        return $classes;
    }
}

if ( !function_exists( 'yith_wcbk_is_in_search_form_result' ) ) {

    function yith_wcbk_is_in_search_form_result() {
        return defined( 'YITH_WCBK_IS_IN_AJAX_SEARCH_FORM_RESULTS' ) && YITH_WCBK_IS_IN_AJAX_SEARCH_FORM_RESULTS;
    }
}

if ( !function_exists( 'yith_wcbk_is_booking_product' ) ) {
    /**
     * Return true if the product is Booking Product
     *
     * @param bool|int|WP_Post|WC_Product $product
     *
     * @return bool
     */
    function yith_wcbk_is_booking_product( $product = false ) {
        return YITH_WCBK_Product_Post_Type_Admin::is_booking( $product );
    }
}

if ( !function_exists( 'yith_wcbk_print_login_form' ) ) {
    /**
     * print the WooCommerce login form
     *
     * @param bool $check_logged_in
     * @param bool $add_woocommerce_container
     *
     * @since 1.0.5
     */
    function yith_wcbk_print_login_form( $check_logged_in = false, $add_woocommerce_container = true ) {
        if ( !$check_logged_in || !is_user_logged_in() ) {
            echo !!$add_woocommerce_container ? '<div class="woocommerce">' : '';
            wc_get_template( 'myaccount/form-login.php' );
            echo !!$add_woocommerce_container ? '</div>' : '';
        }
    }
}

if ( !function_exists( 'yith_wcbk_get_search_form' ) ) {

    /**
     * Get the booking search form object.
     *
     * @param  int $id
     *
     * @uses   YITH_WCBK_Search_Form
     * @return YITH_WCBK_Search_Form|bool
     */
    function yith_wcbk_get_search_form( $id ) {
        $_search_form = false;
        if ( is_numeric( $id ) ) {
            $_search_form = new YITH_WCBK_Search_Form( $id );
        }

        if ( !$_search_form instanceof YITH_WCBK_Search_Form || !$_search_form->is_valid() ) {
            $_search_form = false;
        }

        return apply_filters( 'yith_wcbk_get_search_form', $_search_form );
    }
}

if ( !function_exists( 'yith_wcbk_print_field' ) ) {
    function yith_wcbk_print_field( $args = array(), $echo = true ) {
        if ( !$echo )
            ob_start();

        YITH_WCBK_Printer()->print_field( $args );

        if ( !$echo )
            return ob_get_clean();

        return '';
    }
}

if ( !function_exists( 'yith_wcbk_print_svg' ) ) {
    function yith_wcbk_print_svg( $svg, $echo = true ) {
        return yith_wcbk_print_field( array( 'type' => 'svg', 'svg' => $svg ), $echo );
    }
}

if ( !function_exists( 'yith_wcbk_print_fields' ) ) {
    function yith_wcbk_print_fields( $args = array() ) {
        YITH_WCBK_Printer()->print_fields( $args );
    }
}

if ( !function_exists( 'yith_wcbk_create_complete_time_array' ) ) {
    /**
     * create an array of daily times
     *
     * @since 2.0.0
     */
    function yith_wcbk_create_complete_time_array( $unit, $duration = 1 ) {
        $times = array();
        if ( in_array( $unit, array( 'hour', 'minute' ) ) ) {
            $date_helper  = YITH_WCBK_Date_Helper();
            $from         = strtotime( 'now midnight' );
            $tomorrow     = $date_helper->get_time_sum( $from, 1, 'day', true );
            $current_time = $from;

            while ( $current_time < $tomorrow ) {
                $times[] = date( 'H:i', $current_time );;
                $current_time = $date_helper->get_time_sum( $current_time, $duration, $unit );
            }
        }

        return $times;
    }
}

if ( !function_exists( 'yith_wcbk_create_date_field' ) ) {
    /**
     * create date field with time
     *
     * @param string $unit
     * @param array  $args
     *
     * @since 2.0.0
     *
     * @return string
     */
    function yith_wcbk_create_date_field( $unit, $args = array() ) {
        $value = isset( $args[ 'value' ] ) ? $args[ 'value' ] : '';
        $id    = isset( $args[ 'id' ] ) ? $args[ 'id' ] : '';
        $name  = isset( $args[ 'name' ] ) ? $args[ 'name' ] : '';
        $admin = isset( $args[ 'admin' ] ) ? !!$args[ 'admin' ] : true;

        $datepicker_class = $admin ? 'yith-wcbk-admin-date-picker' : 'yith-wcbk-date-picker';

        if ( !in_array( $unit, array( 'hour', 'minute' ) ) ) {
            $current_value = date_i18n( 'Y-m-d', $value );
            $field         = "<input type='text' class='$datepicker_class' id='$id' name='$name' maxlength='10' value='$current_value' pattern='[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])'/>";
        } else {
            $current_value = date_i18n( 'Y-m-d H:i', $value );
            $date_value    = date_i18n( 'Y-m-d', $value );
            $time_value    = date_i18n( 'H:i', $value );

            $field = "<input type='hidden' class='yith-wcbk-date-time-field' name='$name' data-date='#$id-date' data-time='#$id-time' value='$current_value' />";
            $field .= "<input type='text' class='$datepicker_class' id='$id-date'  maxlength='10' value='$date_value' pattern='[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])'/>";
            $field .= "<span class='yith-wcbk-date-time-field-time'>" . yith_wcbk_print_field( array( 'id' => "$id-time", 'type' => 'time-select', 'value' => $time_value ), false ) . "</span>";
        }

        return $field;
    }
}

if ( !function_exists( 'yith_wcbk_get_person_type_title' ) ) {
    /**
     * @param int $person_type_id
     *
     * @return string
     */
    function yith_wcbk_get_person_type_title( $person_type_id ) {
        return YITH_WCBK()->person_type_helper->get_person_type_title( $person_type_id );
    }
}

if ( !function_exists( 'yith_wcbk_array_add' ) ) {
    /**
     * add key and value after a specific key in array
     *
     * @param array  $array  the array
     * @param string $search the key to search for
     * @param string $key    the key to add
     * @param mixed  $value  the value to add
     * @param bool   $after  the value to add
     */
    function yith_wcbk_array_add( &$array, $search, $key, $value, $after = true ) {
        $position = array_search( $search, array_keys( $array ) );
        if ( $position !== false ) {
            $position = $after ? $position + 1 : $position;
            $first    = array_slice( $array, 0, $position, true );
            $current  = array( $key => $value );
            $last     = array_slice( $array, $position, count( $array ), true );
            $array    = array_merge( $first, $current, $last );
        } else {
            $array = array_merge( $array, array( $key => $value ) );
        }
    }
}

if ( !function_exists( 'yith_wcbk_array_add_after' ) ) {
    /**
     * add key and value after a specific key in array
     *
     * @param array  $array  the array
     * @param string $search the key to search for
     * @param string $key    the key to add
     * @param mixed  $value  the value to add
     */
    function yith_wcbk_array_add_after( &$array, $search, $key, $value ) {
        yith_wcbk_array_add( $array, $search, $key, $value, true );
    }
}

if ( !function_exists( 'yith_wcbk_array_add_before' ) ) {
    /**
     * add key and value after a specific key in array
     *
     * @param array  $array  the array
     * @param string $search the key to search for
     * @param string $key    the key to add
     * @param mixed  $value  the value to add
     */
    function yith_wcbk_array_add_before( &$array, $search, $key, $value ) {
        yith_wcbk_array_add( $array, $search, $key, $value, false );
    }
}


if ( !function_exists( 'yith_wcbk_get_timezone' ) ) {
    /**
     * get the WordPress timezone
     */
    function yith_wcbk_get_timezone( $type = 'default' ) {
        if ( $timezone_string = get_option( 'timezone_string' ) ) {
            $timezone = $timezone_string;
        } else {
            $gmt_offset = get_option( 'gmt_offset' );
            if ( 'human' === $type ) {
                $timezone = sprintf( 'UTC%s%s', $gmt_offset >= 0 ? '+' : '-', absint( $gmt_offset ) );
            } else {
                $timezone = sprintf( '%s0%s00', $gmt_offset >= 0 ? '+' : '-', absint( $gmt_offset ) );
            }
        }

        return apply_filters( 'yith_wcbk_get_timezone', $timezone );
    }
}

if ( !function_exists( 'yith_wcbk_get_active_booking_statuses' ) ) {
    /**
     * return an array of statuses, in which the booking is booked
     */
    function yith_wcbk_get_booked_statuses() {
        $statuses = array(
            'bk-unpaid',
            'bk-paid',
            'bk-completed',
            'bk-confirmed',
        );

        return apply_filters( 'yith_wcbk_get_booked_statuses', $statuses );
    }
}


if ( !function_exists( 'yith_wcbk_array_to_external_booking' ) ) {

    /**
     * Convert an array to external booking object
     *
     * @param  array $args
     *
     * @return YITH_WCBK_Booking_External
     */
    function yith_wcbk_array_to_external_booking( $args ) {
        $_booking = new YITH_WCBK_Booking_External( $args );

        return apply_filters( 'yith_wcbk_array_to_external_booking', $_booking );
    }
}

if ( !function_exists( 'yith_wcbk_is_debug' ) ) {
    /**
     * return true if debug is active
     *
     * @return bool
     */
    function yith_wcbk_is_debug() {
        return defined( 'YITH_WCBK_DEBUG' ) && YITH_WCBK_DEBUG;
    }
}

if ( !function_exists( 'yith_wcbk_get_booking_form_date_info' ) ) {
    /**
     * @param WC_Product_Booking $product
     * @param array              $args
     *
     * @return array
     */
    function yith_wcbk_get_booking_form_date_info( $product, $args = array() ) {
        $default_args       = array(
            'include_default_start_date' => true,
            'include_default_end_date'   => true,
            'start'                      => 'now'
        );
        $args               = wp_parse_args( $args, $default_args );
        $allow_after        = $product->get_booking_prop( 'allow_after' );
        $allow_after_unit   = $product->get_booking_prop( 'allow_after_unit' );
        $allow_until        = $product->get_booking_prop( 'allow_until' );
        $allow_until_unit   = $product->get_booking_prop( 'allow_until_unit' );
        $default_start_date = '';
        $default_end_date   = '';
        $start              = strtotime( $args[ 'start' ] );

        if ( $args[ 'include_default_start_date' ] ) {
            $default_start_date = YITH_WCBK_Search_Form_Helper::get_searched_value_for_field( 'from' );
            $default_start_date = !!$default_start_date ? $default_start_date : $product->get_default_start_date();
        }

        if ( $args[ 'include_default_end_date' ] ) {
            $default_end_date = YITH_WCBK_Search_Form_Helper::get_searched_value_for_field( 'to' );
            $default_end_date = !!$default_end_date ? $default_end_date : '';
        }

        $min_date           = '';
        $min_date_timestamp = $start;
        if ( in_array( $allow_after_unit, array( 'month', 'day', 'hour' ) ) ) {
            $min_date           = '+' . $allow_after . strtoupper( substr( $allow_after_unit, 0, 1 ) );
            $min_date_timestamp = strtotime( '+' . $allow_after . ' ' . $allow_after_unit . 's' );
        }

        $current_year  = date( 'Y', $start );
        $current_month = absint( date( 'm', $start ) );

        /**
         * force $months_to_load to 3 months MAX (if hourly booking) or 1 month MAX (if minutely booking)
         * to prevent loading issue if disable-day-if-no-time-available is active
         *
         * set default months to 12 if duration unit is month
         */
        $months_to_load = YITH_WCBK()->settings->get_months_loaded_in_calendar();
        if ( $product->has_time() && 'yes' === YITH_WCBK()->settings->get( 'disable-day-if-no-time-available', 'no' ) ) {
            $months_to_load = 'hour' === $product->get_duration_unit() ? min( $months_to_load, 3 ) : 1;
        } elseif ( 'month' === $product->get_duration_unit() ) {
            $months_to_load = 12;
        }

        $max_date           = '';
        $max_date_timestamp = strtotime( "+ $months_to_load months", $start );
        $ajax_load_months   = true;
        if ( in_array( $allow_until_unit, array( 'year', 'month', 'day', 'hour' ) ) ) {
            $max_date         = '+' . $allow_until . strtoupper( substr( $allow_until_unit, 0, 1 ) );
            $ajax_load_months = $max_date_timestamp < strtotime( '+' . $allow_until . ' ' . $allow_until_unit . 's' );

            if ( 'month' === $product->get_duration_unit() ) {
                $max_date_timestamp = strtotime( '+' . $allow_until . ' ' . $allow_until_unit . 's' );
            }
        }

        $next_year  = date( 'Y', $max_date_timestamp );
        $next_month = absint( date( 'm', $max_date_timestamp ) );

        return compact( 'current_year', 'current_month', 'next_year', 'next_month', 'min_date', 'min_date_timestamp', 'max_date', 'max_date_timestamp', 'default_start_date', 'default_end_date', 'months_to_load', 'ajax_load_months' );
    }
}
if ( !function_exists( 'yith_wcbk_get_product_duration_label' ) ) {
    /**
     * @param string $duration
     * @param string $duration_unit
     * @param bool   $is_fixed_blocks
     *
     * @return string
     */
    function yith_wcbk_get_product_duration_label( $duration, $duration_unit, $is_fixed_blocks ) {
        if ( $is_fixed_blocks ) {
            $labels = array(
                'month'  => sprintf( _n( '%s month', '%s months', $duration, 'yith-booking-for-woocommerce' ), $duration ),
                'day'    => sprintf( _n( '%s day', '%s days', $duration, 'yith-booking-for-woocommerce' ), $duration ),
                'hour'   => sprintf( _n( '%s hour', '%s hours', $duration, 'yith-booking-for-woocommerce' ), $duration ),
                'minute' => sprintf( _n( '%s minute', '%s minutes', $duration, 'yith-booking-for-woocommerce' ), $duration ),
            );

        } else {
            $labels = array(
                'month'  => sprintf( _n( 'month(s)', '&times; %s months', $duration, 'yith-booking-for-woocommerce' ), $duration ),
                'day'    => sprintf( _n( 'day(s)', '&times; %s days', $duration, 'yith-booking-for-woocommerce' ), $duration ),
                'hour'   => sprintf( _n( 'hour(s)', '&times; %s hours', $duration, 'yith-booking-for-woocommerce' ), $duration ),
                'minute' => sprintf( _n( 'minute(s)', '&times; %s minutes', $duration, 'yith-booking-for-woocommerce' ), $duration ),
            );
        }

        $label = array_key_exists( $duration_unit, $labels ) ? $labels[ $duration_unit ] : '';

        return apply_filters( 'yith_wcbk_get_product_duration_label', $label, $duration, $duration_unit, $is_fixed_blocks );
    }
}

if ( !function_exists( 'yith_wcbk_generate_external_calendars_key' ) ) {
    function yith_wcbk_generate_external_calendars_key() {
        return wp_hash( wp_generate_password() );
    }
}

if ( !function_exists( 'yith_wcbk_print_notice' ) ) {
    function yith_wcbk_print_notice( $notice, $type = 'info', $dismissible = false, $key = '' ) {
        $class = "yith-wcbk-admin-notice notice notice-{$type}";
        $class .= !!$dismissible ? ' is-dismissible' : '';

        if ( !$key ) {
            $key = md5( $notice . '_' . $type );
        }
        $key    = sanitize_key( $key );
        $cookie = 'yith_wcbk_notice_dismiss_' . $key;
        $id     = 'yith-wcbk-notice-' . $key;

        if ( $dismissible && !empty( $_COOKIE[ $cookie ] ) )
            return;

        echo "<div id='{$id}' class='{$class}'><p>{$notice}</p></div>";

        if ( $dismissible ) {
            ?>
            <script>
                jQuery( '#<?php echo $id ?>' ).on( 'click', '.notice-dismiss', function () {
                    document.cookie = "<?php echo $cookie ?>=1";
                } );
            </script>
            <?php
        }
    }
}

if ( !function_exists( 'yith_wcbk_format_decimals_with_variables' ) ) {
    function yith_wcbk_format_decimals_with_variables( $price ) {
        if ( strpos( $price, '*' ) ) {
            list( $_price, $variable ) = explode( '*', $price, 2 );
            $price = wc_format_decimal( $_price ) . '*' . $variable;
        } elseif ( strpos( $price, '/' ) ) {
            list( $_price, $variable ) = explode( '/', $price, 2 );
            $price = wc_format_decimal( $_price ) . '/' . $variable;
        } else {
            $price = wc_format_decimal( $price );
        }
        return $price;
    }
}

if ( !function_exists( 'yith_wcbk_get_product_availability_per_units' ) ) {
    /**
     * @param WC_Product_Booking $product
     * @param int                $from
     * @param int                $to
     *
     * @since 2.0.3
     * @return array
     */
    function yith_wcbk_get_product_availability_per_units( WC_Product_Booking $product, $from, $to ) {
        $_args     = array(
            'product_id'                => $product->get_id(),
            'from'                      => $from,
            'to'                        => $to,
            'include_externals'         => $product->has_external_calendars(),
            'unit'                      => $product->get_duration_unit(),
            'count_persons_as_bookings' => $product->has_count_persons_as_bookings_enabled(),
        );
        $_booked   = YITH_WCBK_Booking_Helper()->count_max_booked_bookings_per_unit_in_period( $_args );
        $_max      = $product->get_max_per_block();
        $_bookable = $_max - $_booked;

        return array(
            'booked'   => $_booked,
            'bookable' => $_bookable,
            'max'      => $_max
        );
    }
}

if ( !function_exists( 'yith_wcbk_get_calendar_product_availability_per_units_html' ) ) {
    /**
     * @param WC_Product_Booking $product
     * @param int                $from
     * @param int                $to
     * @param string             $step
     *
     * @since 2.0.3
     * @return string
     */
    function yith_wcbk_get_calendar_product_availability_per_units_html( WC_Product_Booking $product, $from, $to, $step = '' ) {
        $html = '';
        if ( $product->can_show_availability( $step ) ) {
            $html  .= "<div class='yith-wcbk-booking-calendar-availability'>";
            $_args = array( 'from' => $from, 'exclude_booked' => true, 'check_non_available_in_past' => false );

            if ( $product->is_available( $_args ) ) {
                $_availability = yith_wcbk_get_product_availability_per_units( $product, $from, $to );

                $html .= "<span class='yith-wcbk-booking-calendar-availability__bookable' title='" . __( 'Bookable', 'yith-booking-for-woocommerce' ) . "'>{$_availability['bookable']}</span>";
                $html .= "<span class='yith-wcbk-booking-calendar-availability__booked' title='" . __( 'Booked', 'yith-booking-for-woocommerce' ) . "'>{$_availability['booked']}</span>";
                $html .= "<span class='yith-wcbk-booking-calendar-availability__max' title='" . __( 'Max bookings per unit', 'yith-booking-for-woocommerce' ) . "'>{$_availability['max']}</span>";
            } else {
                $html .= "<span class='yith-wcbk-booking-calendar-availability__non_bookable'>" . __( 'Non Bookable', 'yith-booking-for-woocommerce' ) . "</span>";
            }
            $html .= "</div>";
        }
        return $html;
    }
}


if ( !function_exists( 'yith_wcbk_get_minimum_minute_increment' ) ) {
    /**
     * get the minimum minute increment: default 15
     *
     * @since 2.0.5
     * @return string
     */
    function yith_wcbk_get_minimum_minute_increment() {
        return apply_filters( 'yith_wcbk_get_minimum_minute_increment', 15 );
    }
}