<?php
/**
 * Class BK_Tests_Booking_Product_Form
 *
 * @package YITH Booking for WooCommerce Premium
 */

/**
 * Sample test case.
 */
class BK_Tests_Booking_Product_Form extends BK_Unit_Test_Case_With_Store {
    /**
     * Test custom daily booking
     */
    function test_custom_daily_booking() {
        $ajax          = YITH_WCBK_AJAX::get_instance();
        $ajax->testing = true;

        $product = $this->create_and_store_booking_product();
        $product->set_booking_prop( 'duration_type', 'customer' );
        $product->set_booking_prop( 'duration', 1 );
        $product->set_booking_prop( 'duration_unit', 'day' );
        $product->set_booking_prop( 'availability_range', BK_Helper_Availability_Ranges::create_daily_availability_ranges() );

        $product->save_booking_props();

        $booking_data = $ajax->get_booking_data( array() );
        $this->assertArrayHasKey( 'error', $booking_data );

        $request      = array(
            'product_id' => $product->get_id(),
        );
        $booking_data = $ajax->get_booking_data( $request );
        $this->assertArrayHasKey( 'error', $booking_data );


        $request = array(
            'product_id' => $product->get_id(),
            'from'       => BK_Helper_Date::create_next_year_date( 'Jan 01', 'date' ),
            'to'         => BK_Helper_Date::create_next_year_date( 'Jan 02', 'date' )
        );

        $booking_data = $ajax->get_booking_data( $request );
        $this->assertArrayHasKey( 'is_available', $booking_data );
        $this->assertArrayHasKey( 'price', $booking_data );
        $this->assertArrayHasKey( 'message', $booking_data );
        $this->assertTrue( $booking_data[ 'is_available' ] );

        $request = array(
            'product_id' => $product->get_id(),
            'from'       => BK_Helper_Date::create_next_year_date( 'Jan 01', 'date' ),
            'duration'   => '1'
        );

        $booking_data = $ajax->get_booking_data( $request );
        $this->assertTrue( $booking_data[ 'is_available' ] );
    }

    /**
     * Test custom daily booking all day
     */
    function test_custom_daily_booking_all_day() {
        $ajax          = YITH_WCBK_AJAX::get_instance();
        $ajax->testing = true;

        $product = $this->create_and_store_booking_product();
        $product->set_booking_prop( 'duration_type', 'customer' );
        $product->set_booking_prop( 'duration', 1 );
        $product->set_booking_prop( 'duration_unit', 'day' );
        $product->set_booking_prop( 'all_day', 'yes' );
        $product->set_booking_prop( 'availability_range', BK_Helper_Availability_Ranges::create_daily_availability_ranges() );

        $product->save_booking_props();

        $request = array(
            'product_id' => $product->get_id(),
            'from'       => BK_Helper_Date::create_next_year_date( 'Jan 01', 'date' ),
            'to'         => BK_Helper_Date::create_next_year_date( 'Jan 01', 'date' )
        );

        $booking_data = $ajax->get_booking_data( $request );
        $this->assertTrue( $booking_data[ 'is_available' ] );

        $request = array(
            'product_id' => $product->get_id(),
            'from'       => BK_Helper_Date::create_next_year_date( 'Jan 01', 'date' ),
            'duration'   => '1'
        );

        $booking_data = $ajax->get_booking_data( $request );
        $this->assertTrue( $booking_data[ 'is_available' ] );
    }
}
