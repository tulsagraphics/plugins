<?php
/**
 * Class BK_Tests_Booking_Product
 *
 * @package YITH Booking for WooCommerce Premium
 */

/**
 * Sample test case.
 */
class BK_Tests_Booking_Product extends BK_Unit_Test_Case_With_Store {

    /**
     * Test is booking product.
     */
    function test_is_booking_product() {
        $product = $this->create_and_store_booking_product();
        $this->assertTrue( yith_wcbk_is_booking_product( $product ) );
    }

    /**
     * Test is type fixed blocks.
     */
    function test_is_type_fixed_blocks() {
        $product = $this->create_and_store_booking_product();
        $product->set_booking_prop( 'duration_type', 'customer' );

        $this->assertFalse( $product->is_type_fixed_blocks() );

        $product->set_booking_prop( 'duration_type', 'fixed' );
        $this->assertTrue( $product->is_type_fixed_blocks() );
    }

    /**
     * Test is all day.
     */
    function test_is_all_day() {
        $product = $this->create_and_store_booking_product();
        $product->set_booking_prop( 'duration_unit', 'day' );
        $product->set_booking_prop( 'all_day', 'no' );

        $this->assertFalse( $product->is_all_day() );

        $product->set_booking_prop( 'all_day', 'yes' );
        $this->assertTrue( $product->is_all_day() );

        $product->set_booking_prop( 'duration_unit', 'month' );
        $this->assertFalse( $product->is_all_day() );
    }
}
