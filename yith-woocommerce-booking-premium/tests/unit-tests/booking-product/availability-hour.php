<?php
/**
 * Class BK_Tests_Booking_Product_Availability_Hour
 *
 * @package YITH Booking for WooCommerce Premium
 */

class BK_Tests_Booking_Product_Availability_Hour extends BK_Unit_Test_Case_With_Store {
    /**
     * Test custom hourly booking
     */
    function test_custom_hourly_booking() {
        $product = $this->create_and_store_booking_product();
        $product->set_booking_prop( 'duration_type', 'customer' );
        $product->set_booking_prop( 'duration', 1 );
        $product->set_booking_prop( 'duration_unit', 'hour' );
        $product->set_booking_prop( 'availability_range', BK_Helper_Availability_Ranges::create_hourly_availability_ranges() );


        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 12:00' );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 00:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 24:00' );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = strtotime( 'next monday 08:00', BK_Helper_Date::create_next_year_date( 'Feb 01' ) );
        $to   = strtotime( 'next monday 16:00', BK_Helper_Date::create_next_year_date( 'Feb 01' ) );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = strtotime( 'next saturday 08:00', BK_Helper_Date::create_next_year_date( 'Feb 01' ) );
        $to   = strtotime( 'next saturday 16:00', BK_Helper_Date::create_next_year_date( 'Feb 01' ) );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = strtotime( 'next tuesday 07:00', BK_Helper_Date::create_next_year_date( 'Feb 01' ) );
        $to   = strtotime( 'next tuesday 08:00', BK_Helper_Date::create_next_year_date( 'Feb 01' ) );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = strtotime( 'next friday 08:00', BK_Helper_Date::create_next_year_date( 'Feb 01' ) );
        $to   = strtotime( 'next friday 12:00', BK_Helper_Date::create_next_year_date( 'Feb 01' ) );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = strtotime( 'next friday 15:00', BK_Helper_Date::create_next_year_date( 'Feb 01' ) );
        $to   = strtotime( 'next friday 16:00', BK_Helper_Date::create_next_year_date( 'Feb 01' ) );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = strtotime( 'next monday 16:00', BK_Helper_Date::create_next_year_date( 'Feb 01' ) );
        $to   = strtotime( 'next monday 17:00', BK_Helper_Date::create_next_year_date( 'Feb 01' ) );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );


        $from = strtotime( 'next monday 08:00', BK_Helper_Date::create_next_year_date( 'Apr 01' ) );
        $to   = strtotime( 'next monday 09:00', BK_Helper_Date::create_next_year_date( 'Apr 01' ) );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = strtotime( 'next saturday 08:00', BK_Helper_Date::create_next_year_date( 'Apr 01' ) );
        $to   = strtotime( 'next saturday 09:00', BK_Helper_Date::create_next_year_date( 'Apr 01' ) );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );
    }

    /**
     * Test custom hourly booking multiple hours
     */
    function test_custom_hourly_booking_multiple_hours() {
        $product = $this->create_and_store_booking_product();
        $product->set_booking_prop( 'duration_type', 'customer' );
        $product->set_booking_prop( 'duration', 2 );
        $product->set_booking_prop( 'duration_unit', 'hour' );
        $product->set_booking_prop( 'availability_range', BK_Helper_Availability_Ranges::create_hourly_availability_ranges() );


        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 12:00' );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 13:00' );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 14:00' );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );


        $product = $this->create_and_store_booking_product();
        $product->set_booking_prop( 'duration_type', 'customer' );
        $product->set_booking_prop( 'duration', 3 );
        $product->set_booking_prop( 'duration_unit', 'hour' );
        $product->set_booking_prop( 'availability_range', BK_Helper_Availability_Ranges::create_hourly_availability_ranges() );


        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 13:00' );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 14:00' );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );


        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 16:00' );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );
    }

    /**
     * Test fixed hourly booking
     */
    function test_fixed_hourly_booking() {
        $product = $this->create_and_store_booking_product();
        $product->set_booking_prop( 'duration_type', 'fixed' );
        $product->set_booking_prop( 'duration', 1 );
        $product->set_booking_prop( 'duration_unit', 'hour' );
        $product->set_booking_prop( 'availability_range', BK_Helper_Availability_Ranges::create_hourly_availability_ranges() );


        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 11:00' );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 12:00' );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 15:00' );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );
    }

    /**
     * Test fixed hourly booking multiple hours
     */
    function test_fixed_hourly_booking_multiple_hours() {
        $product = $this->create_and_store_booking_product();
        $product->set_booking_prop( 'duration_type', 'fixed' );
        $product->set_booking_prop( 'duration', 2 );
        $product->set_booking_prop( 'duration_unit', 'hour' );
        $product->set_booking_prop( 'availability_range', BK_Helper_Availability_Ranges::create_hourly_availability_ranges() );


        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 12:00' );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 14:00' );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );


        $product = $this->create_and_store_booking_product();
        $product->set_booking_prop( 'duration_type', 'fixed' );
        $product->set_booking_prop( 'duration', 3 );
        $product->set_booking_prop( 'duration_unit', 'hour' );
        $product->set_booking_prop( 'availability_range', BK_Helper_Availability_Ranges::create_hourly_availability_ranges() );


        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 13:00' );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 14:00' );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 16:00' );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );
    }

    /**
     * Test availability with existing bookings
     */
    function test_with_existing_bookings() {
        $product = $this->create_and_store_booking_product();
        $product->set_booking_prop( 'duration_type', 'customer' );
        $product->set_booking_prop( 'duration', 1 );
        $product->set_booking_prop( 'duration_unit', 'hour' );

        $booking = $this->create_and_store_booking( array(
                                                        'product_id' => $product->get_id(),
                                                        'from'       => BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' ),
                                                        'to'         => BK_Helper_Date::create_next_year_date( 'Jan 01 12:00' ),
                                                    ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 12:00' );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 11:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 04 12:00' );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 05 12:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 05 13:00' );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

    }

    /**
     * Test Max Bookings per unit option with existing bookings
     */
    function test_max_bookings_per_unit_with_existing_bookings() {
        $product = $this->create_and_store_booking_product();
        $product->set_booking_prop( 'duration_type', 'customer' );
        $product->set_booking_prop( 'duration', 1 );
        $product->set_booking_prop( 'duration_unit', 'hour' );
        $product->set_booking_prop( 'max_per_block', 2 );

        $booking_1 = $this->create_and_store_booking( array(
                                                          'product_id' => $product->get_id(),
                                                          'from'       => BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' ),
                                                          'to'         => BK_Helper_Date::create_next_year_date( 'Jan 01 12:00' ),
                                                      ) );

        $booking_2 = $this->create_and_store_booking( array(
                                                          'product_id' => $product->get_id(),
                                                          'from'       => BK_Helper_Date::create_next_year_date( 'Jan 01 11:00' ),
                                                          'to'         => BK_Helper_Date::create_next_year_date( 'Jan 01 13:00' ),
                                                      ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 12:00' );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 11:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 12:00' );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 10:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 11:00' );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = BK_Helper_Date::create_next_year_date( 'Jan 01 12:00' );
        $to   = BK_Helper_Date::create_next_year_date( 'Jan 01 13:00' );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );
    }


    /**
     * Test availability over midnight
     */
    function test_availability_over_midnight() {
        $product = $this->create_and_store_booking_product();
        $product->set_booking_prop( 'duration_type', 'customer' );
        $product->set_booking_prop( 'duration', 1 );
        $product->set_booking_prop( 'duration_unit', 'hour' );

        $availability_ranges = array(
            array(
                'type'         => 'month',
                'from'         => '1',
                'to'           => '12',
                'bookable'     => 'no',
                'days_enabled' => 'no',
                'days'         => array(),
            ),
            array(
                'type'          => 'month',
                'from'          => '1',
                'to'            => '12',
                'bookable'      => 'yes',
                'days_enabled'  => 'yes',
                'times_enabled' => 'yes',
                'days'          => array(
                    1 => 'no',
                    2 => 'yes',
                    3 => 'yes',
                    4 => 'yes',
                    5 => 'yes',
                    6 => 'yes',
                    7 => 'yes',
                ),
                'day_time_from' => array(
                    1 => '02:00',
                    2 => '20:00',
                    3 => '20:00',
                    4 => '20:00',
                    5 => '20:00',
                    6 => '00:00',
                    7 => '20:00',
                ),
                'day_time_to'   => array(
                    1 => '00:00',
                    2 => '00:00',
                    3 => '00:00',
                    4 => '00:00',
                    5 => '00:00',
                    6 => '00:00',
                    7 => '00:00',
                ),
            ),
            array(
                'type'          => 'month',
                'from'          => '1',
                'to'            => '12',
                'bookable'      => 'yes',
                'days_enabled'  => 'yes',
                'times_enabled' => 'yes',
                'days'          => array(
                    1 => 'yes',
                    2 => 'disabled',
                    3 => 'yes',
                    4 => 'yes',
                    5 => 'yes',
                    6 => 'yes',
                    7 => 'yes',
                ),
                'day_time_from' => array(
                    1 => '00:00',
                    2 => '00:00',
                    3 => '00:00',
                    4 => '00:00',
                    5 => '00:00',
                    6 => '00:00',
                    7 => '00:00',
                ),
                'day_time_to'   => array(
                    1 => '02:00',
                    2 => '00:00',
                    3 => '02:00',
                    4 => '02:00',
                    5 => '02:00',
                    6 => '00:00',
                    7 => '02:00',
                ),
            ),

        );

        $availability_ranges = array_map( 'yith_wcbk_availability_range', $availability_ranges );
        $product->set_booking_prop( 'availability_range', $availability_ranges );


        $from = strtotime( 'next monday 08:00', BK_Helper_Date::create_next_year_date( 'Jan 01' ) );
        $to   = strtotime( 'next monday 16:00', BK_Helper_Date::create_next_year_date( 'Jan 01' ) );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = strtotime( 'next monday 20:00', BK_Helper_Date::create_next_year_date( 'Jan 01' ) );
        $to   = strtotime( 'tomorrow midnight ', $from );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = strtotime( 'next sunday 20:00', BK_Helper_Date::create_next_year_date( 'Jan 01' ) );
        $to   = strtotime( 'tomorrow midnight ', $from );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = strtotime( 'next sunday 20:00', BK_Helper_Date::create_next_year_date( 'Jan 01' ) );
        $to   = strtotime( 'tomorrow 02:00 ', $from );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = strtotime( 'next wednesday 20:00', BK_Helper_Date::create_next_year_date( 'Jan 01' ) );
        $to   = strtotime( 'tomorrow 04:00 ', $from );
        $this->assertFalse( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );

        $from = strtotime( 'next friday 20:00', BK_Helper_Date::create_next_year_date( 'Jan 01' ) );
        $to   = strtotime( '+2 days 02:00 ', $from );
        $this->assertTrue( $product->is_available( array( 'from' => $from, 'to' => $to ) ) );
    }
}
