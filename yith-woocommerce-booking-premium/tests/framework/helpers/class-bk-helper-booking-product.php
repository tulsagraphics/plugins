<?php

/**
 * Class BK_Helper_Booking_Product.
 *
 * This helper class should ONLY be used for unit tests!.
 */
class BK_Helper_Booking_Product {
    /**
     * Create booking product.
     *
     * @return WC_Product_Booking
     */
    public static function create_booking_product() {
        // Create the product
        $product = wp_insert_post( array(
                                       'post_title'  => 'Dummy Booking Product',
                                       'post_type'   => 'product',
                                       'post_status' => 'publish',
                                   ) );
        update_post_meta( $product, '_sku', 'DUMMY SKU' );
        update_post_meta( $product, '_manage_stock', 'no' );
        update_post_meta( $product, '_tax_status', 'taxable' );
        wp_set_object_terms( $product, YITH_WCBK_Product_Post_Type_Admin::$prod_type, 'product_type' );

        $product = new WC_Product_Booking( $product );

        // set 'Allow until' by default to 5 years to prevent issue with 'create_next_year_date'
        $product->set_booking_prop( 'allow_until', 5 );
        $product->set_booking_prop( 'allow_until_unit', 'year' );

        return $product;
    }

    /**
     * delete a product
     *
     * @param int|WC_Product $product
     */
    public static function delete_product( $product ) {
        $product = wc_get_product( $product );
        $product && $product->delete( true );
    }
}
