/* global bk, yith_wcbk_dates, yith_wcbk_datepicker_params */
jQuery( document ).ready( function ( $ ) {
    "use strict";

    $.fn.yith_wcbk_datepicker = function () {

        var datepickerFormat  = 'yy-mm-dd',
            block_params      = {
                message        : null,
                overlayCSS     : {
                    background: '#fff',
                    opacity   : 0.7
                },
                ignoreIfBlocked: true
            },
            getDatepicker     = function ( _datepicker ) {
                var _type = _datepicker.data( 'type' ),
                    _from = $( _datepicker.data( 'related-from' ) );

                if ( _type === 'to' && _from.length > 0 ) {
                    _datepicker = _from;
                }

                return _datepicker;
            },
            getDatepickerData = function ( _datepicker ) {
                _datepicker = getDatepicker( _datepicker );

                return {
                    all_day          : _datepicker.data( 'all-day' ),
                    allow_same_date  : _datepicker.data( 'allow-same-date' ) || 'yes',
                    ajax_load_months : _datepicker.data( 'ajax-load-months' ),
                    month_to_load    : _datepicker.data( 'month-to-load' ),
                    year_to_load     : _datepicker.data( 'year-to-load' ),
                    min_duration     : _datepicker.data( 'min-duration' ),
                    notAvailableDates: _datepicker.data( 'not-available-dates' ) || [],
                    product_id       : _datepicker.data( 'product-id' ),
                    static           : _datepicker.data( 'static' ) || 'no'
                };
            };

        return $( this ).each( function () {
            var $current_datepicker     = $( this ),
                type                    = $current_datepicker.data( 'type' ),
                minDate                 = $current_datepicker.data( 'min-date' ),
                maxDate                 = $current_datepicker.data( 'max-date' ),
                to                      = $( $current_datepicker.data( 'related-to' ) ),
                on_select_open          = $( $current_datepicker.data( 'on-select-open' ) ),
                from                    = $( $current_datepicker.data( 'related-from' ) ),
                allowed_days            = $current_datepicker.data( 'allowed-days' ) || [],
                updateNotAvailableDates = function () {
                    var datepickerData = getDatepickerData( $current_datepicker ),
                        datepicker     = getDatepicker( $current_datepicker );

                    $( '#ui-datepicker-div' ).block( block_params );

                    $.ajax( {
                                type   : "POST",
                                data   : {
                                    product_id   : datepickerData.product_id,
                                    month_to_load: datepickerData.month_to_load,
                                    year_to_load : datepickerData.year_to_load,
                                    action       : 'yith_wcbk_get_product_not_available_dates'
                                },
                                url    : bk.ajaxurl,
                                success: function ( response ) {
                                    try {
                                        if ( response.error ) {
                                            console.log( response.error );
                                        } else {
                                            datepicker.data( 'month-to-load', response.month_to_load );
                                            datepicker.data( 'year-to-load', response.year_to_load );
                                            datepicker.data( 'not-available-dates', datepickerData.notAvailableDates.concat( response.not_available_dates ) );

                                            $( '#ui-datepicker-div' ).unblock();
                                            $current_datepicker.datepicker( 'refresh' );
                                        }

                                    } catch ( err ) {
                                        console.log( err.message );
                                    }
                                }
                            } );
                },
                hasMonthsToLoad         = function ( year, month, $_datepicker ) {
                    $current_datepicker = $_datepicker;
                    var datepickerData  = getDatepickerData( $_datepicker );

                    if ( 'yes' === datepickerData.ajax_load_months ) {
                        var selected_month_date = year + '-' + month + '-01',
                            loaded_month_date   = datepickerData.year_to_load + '-' + datepickerData.month_to_load + '-01',
                            interval            = yith_wcbk_dates.date_diff( selected_month_date, loaded_month_date, 'months' );

                        if ( interval < 1 ) {
                            return true;
                        }
                    }
                    return false;
                };

            $( this ).datepicker( {
                                      dateFormat     : datepickerFormat,
                                      minDate        : minDate,
                                      maxDate        : maxDate,
                                      showAnim       : false,
                                      showButtonPanel: true,
                                      closeText      : yith_wcbk_datepicker_params.i18n_clear,
                                      popup          : {
                                          position: "bottom left",
                                          origin  : "top left"
                                      },
                                      beforeShow     : function ( input, instance ) {
                                          $current_datepicker.addClass( 'yith-wcbk-datepicker--opened' );
                                          $( '#ui-datepicker-div' ).addClass( 'yith-wcbk-datepicker' );

                                          var datepickerData = getDatepickerData( $current_datepicker );

                                          if ( 'yes' === datepickerData.static ) {
                                              $( '#ui-datepicker-div' ).addClass( 'yith-wcbk-datepicker--static' ).appendTo( $current_datepicker.parent().parent() );
                                          }

                                          instance.yith_booking_date_selected = false;

                                          setTimeout( function () {
                                              $( '.ui-datepicker-close' ).on( 'click', function ( event ) {
                                                  $current_datepicker.datepicker( "setDate", null ).trigger( 'change' );
                                              } );
                                          }, 10 );
                                      },
                                      beforeShowDay  : function ( date ) {
                                          var allowed        = true,
                                              datepickerData = getDatepickerData( $current_datepicker );

                                          if ( date.getDate() === 15 ) {
                                              if ( hasMonthsToLoad( date.getFullYear(), date.getMonth() + 1, $current_datepicker ) ) {
                                                  updateNotAvailableDates();
                                              }
                                          }

                                          if ( allowed_days.length > 0 ) {
                                              var current_day = date.getDay();
                                              if ( current_day === 0 ) {
                                                  current_day = 7;
                                              }
                                              allowed = allowed_days.indexOf( current_day.toString() ) !== -1;
                                          }

                                          if ( allowed && datepickerData.notAvailableDates.length > 0 ) {
                                              var _min_duration = 1,
                                                  _date, formatted_date;
                                              if ( bk.settings.check_min_max_duration_in_calendar === 'yes' ) {
                                                  _min_duration = 'yes' === datepickerData.all_day ? ( datepickerData.min_duration - 1 ) : datepickerData.min_duration;
                                              }
                                              _date          = 'to' === type ? yith_wcbk_dates.add_days_to_date( date, -_min_duration ) : date;
                                              formatted_date = $.datepicker.formatDate( 'yy-mm-dd', _date );

                                              allowed = datepickerData.notAvailableDates.indexOf( formatted_date ) === -1;
                                          }

                                          return [ allowed ];
                                      },
                                      onClose        : function ( selectedDate, instance ) {
                                          $( '#ui-datepicker-div' ).removeClass( 'yith-wcbk-datepicker' );

                                          var datepickerData = getDatepickerData( $current_datepicker );

                                          if ( 'yes' === datepickerData.static ) {
                                              $( '#ui-datepicker-div' ).removeClass( 'yith-wcbk-datepicker--static' ).appendTo( $( 'body' ) );
                                          }

                                          if ( instance.yith_booking_date_selected && on_select_open.length > 0 && !on_select_open.val() ) {
                                              on_select_open.trigger( 'focus' );
                                          }

                                          $current_datepicker.removeClass( 'yith-wcbk-datepicker--opened' );
                                      },
                                      onSelect       : function ( selectedDate, instance ) {
                                          var datepickerData = getDatepickerData( $current_datepicker );

                                          if ( selectedDate ) {
                                              if ( to.length > 0 ) {
                                                  var minDate = datepickerData.allow_same_date === 'yes' ? selectedDate : yith_wcbk_dates.add_days_to_date( selectedDate, 1 );

                                                  if ( to.val() && yith_wcbk_dates.date_diff( to.val(), minDate ) < 0 ) {
                                                      to.datepicker( "setDate", null );
                                                  }

                                                  to.datepicker( "option", "minDate", minDate );
                                              }

                                              instance.yith_booking_date_selected = true;
                                          }
                                          $( this ).trigger( 'change' );
                                      }
                                  } );

            // open the datepicker when clicking on datepicker icon
            var $datepickerIcon    = $current_datepicker.parent().find( '.yith-wcbk-booking-date-icon' ),
                datepickerIsOpened = false;

            $datepickerIcon
                .on( 'mousedown', function ( e ) {
                    datepickerIsOpened = !!$current_datepicker.datepicker( 'widget' ).is( ':visible' );
                } )
                .on( 'click', function () {
                    if ( !datepickerIsOpened ) {
                        $current_datepicker.trigger( 'focus' );
                    }
                } );
        } );
    };
} );