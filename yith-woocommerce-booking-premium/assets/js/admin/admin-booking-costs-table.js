jQuery( document ).ready( function ( $ ) {
    "use strict";

    var costs_table           = $( '#yith-wcbk-booking-costs-table' ),
        costs_default_row     = costs_table.find( 'tr.yith-wcbk-costs-default-row' ).first(),
        costs_add_date_range  = $( 'input#yith-wcbk-costs-add-date-range' ),
        costs_add_month_range = $( 'input#yith-wcbk-costs-add-month-range' ),
        costs_add_day_range   = $( 'input#yith-wcbk-costs-add-day-range' );

    costs_table.on( 'click', '.yith-wcbk-delete', function ( event ) {
        var delete_btn = $( event.target ),
            target_row = delete_btn.closest( 'tr' );

        target_row.remove();
    } )

    /* ----  S o r t a b l e  ---- */
        .find( 'tbody' ).sortable( {
                                       items               : 'tr',
                                       cursor              : 'move',
                                       handle              : '.yith-wcbk-anchor',
                                       axis                : 'y',
                                       scrollSensitivity   : 40,
                                       forcePlaceholderSize: true,
                                       opacity             : 0.65,
                                       helper              : function ( e, tr ) {
                                           var originals = tr.children(),
                                               helper    = tr.clone();
                                           helper.children().each( function ( index ) {
                                               // Set helper cell sizes to match the original sizes
                                               $( this ).width( originals.eq( index ).width() );
                                           } );
                                           return helper;
                                       }
                                   } );

    costs_add_date_range.on( 'click', function () {
        var added_row = costs_default_row.clone().removeClass( 'yith-wcbk-costs-default-row' );
        added_row.find( '.yith-wcbk-costs-range-column-ranges-month' ).remove();
        added_row.find( '.yith-wcbk-costs-range-column-ranges-day' ).remove();
        added_row.find( 'input' ).prop( 'disabled', false );
        added_row.find( 'select' ).prop( 'disabled', false );
        costs_table.append( added_row );
        added_row.find( '.yith-wcbk-date-input-field' ).addClass( 'yith-wcbk-admin-date-picker' ).yith_wcbk_datepicker();
    } );

    costs_add_month_range.on( 'click', function () {
        var added_row = costs_default_row.clone().removeClass( 'yith-wcbk-costs-default-row' );
        added_row.find( '.yith-wcbk-costs-range-column-ranges-date' ).remove();
        added_row.find( '.yith-wcbk-costs-range-column-ranges-day' ).remove();
        added_row.find( 'input' ).prop( 'disabled', false );
        added_row.find( 'select' ).prop( 'disabled', false );
        costs_table.append( added_row );
    } );

    costs_add_day_range.on( 'click', function () {
        var added_row = costs_default_row.clone().removeClass( 'yith-wcbk-costs-default-row' );
        added_row.find( '.yith-wcbk-costs-range-column-ranges-date' ).remove();
        added_row.find( '.yith-wcbk-costs-range-column-ranges-month' ).remove();
        added_row.find( 'input' ).prop( 'disabled', false );
        added_row.find( 'select' ).prop( 'disabled', false );
        costs_table.append( added_row );
    } );

    $( document ).on( 'click', '.yith-wcbk-costs-toggle-visible', function () {
        var target       = $( this ),
            toggle_class = 'yith-wcbk-costs-range-column-costs',
            parent       = target.closest( 'tr' ),
            to_hide      = parent.find( '.' + toggle_class ).first();

        to_hide.toggle();
    } );
} );