/* global wcbk_admin */
jQuery( document ).ready( function ( $ ) {
    "use strict";

    /** ------------------------------------------------------------------------
     *  PRINTER - OnOff
     * ------------------------------------------------------------------------- */
    $.fn.yith_wcbk_onoff = function () {
        return $( this ).each( function () {
            var onoff          = $( this ),
                hidden_input   = $( this ).find( '.yith-wcbk-on-off-value' ).first(),
                button         = $( this ).find( '.yith-wcbk-on-off' ).first(),
                enabled_class  = button.data( 'enabled_class' ),
                disabled_class = button.data( 'disabled_class' ),
                enabled_text   = button.data( 'enabled_text' ),
                disabled_text  = button.data( 'disabled_text' );

            button.on( 'click', function () {
                var value = hidden_input ? hidden_input.val() : 'no';
                if ( value === 'yes' ) {
                    onoff.trigger( 'disable' );
                } else {
                    onoff.trigger( 'enable' );
                }
            } );

            $( this )
                .on( 'enable', function () {
                    hidden_input.val( 'yes' );
                    button.removeClass( disabled_class );
                    button.addClass( enabled_class );
                    button.html( enabled_text );
                } )

                .on( 'disable', function () {
                    hidden_input.val( 'no' );
                    button.removeClass( enabled_class );
                    button.addClass( disabled_class );
                    button.html( disabled_text );
                } )

        } );
    };

    $( '.yith-wcbk-on-off-container' ).yith_wcbk_onoff();


    /** ------------------------------------------------------------------------
     *  PRINTER - OnOff Advanced
     * ------------------------------------------------------------------------- */
    $.fn.yith_wcbk_onoff_advanced = function () {
        $( this ).each( function () {
            var hidden_input = $( this ).find( '.yith-wcbk-on-off-advanced-value' ).first(),
                button       = $( this ).find( '.yith-wcbk-on-off-advanced' ).first(),
                options      = button.data( 'options' ),
                classes      = [],
                _nextKey     = function ( db, key ) {
                    var keys = Object.keys( db ),
                        i    = keys.indexOf( key );
                    if ( i !== -1 && keys[ i + 1 ] ) {
                        return keys[ i + 1 ];
                    } else {
                        return keys[ 0 ];
                    }
                },
                i;

            // populate the classes array
            for ( i in options ) {
                classes.push( options[ i ].class );
            }

            button.on( 'click', function () {
                var value     = hidden_input.val(),
                    nextValue = _nextKey( options, value );

                button.removeClass( classes.join( ' ' ) );
                hidden_input.val( nextValue );
                button.addClass( options[ nextValue ].class );
                button.html( options[ nextValue ].label );
            } );
        } );
    };

    $( '.yith-wcbk-on-off-advanced-container' ).yith_wcbk_onoff_advanced();


    /** ------------------------------------------------------------------------
     *  Time Select
     * ------------------------------------------------------------------------- */
    $.fn.yith_wcbk_timeselect = function ( options ) {
        var defaults = {
            container: '.yith-wcbk-time-select__container',
            hour     : '.yith-wcbk-time-select-hour',
            minute   : '.yith-wcbk-time-select-minute',
            separator: ':'
        };
        options      = $.extend( defaults, options );

        return $( this ).each( function () {
            var $timeSelect = $( this ),
                $container  = $timeSelect.closest( options.container ),
                $hour       = $container.find( options.hour ).first(),
                $minute     = $container.find( options.minute ).first(),
                update      = function () {
                    $timeSelect.val( $hour.val() + options.separator + $minute.val() ).trigger( 'change' );
                };

            $hour.on( 'change', update );
            $minute.on( 'change', update );
        } );
    };

    $( '.yith-wcbk-time-select' ).yith_wcbk_timeselect();


    /** ------------------------------------------------------------------------
     *  Select2 - Select All | Deselect All
     * ------------------------------------------------------------------------- */
    var select_all_btn   = $( '.yith-wcbk-select2-select-all' ),
        deselect_all_btn = $( '.yith-wcbk-select2-deselect-all' );

    select_all_btn.on( 'click', function () {
        var select_id     = $( this ).data( 'select-id' ),
            target_select = $( '#' + select_id );

        target_select.find( 'option' ).prop( 'selected', true );
        target_select.trigger( 'change' );
    } );

    deselect_all_btn.on( 'click', function () {
        var select_id     = $( this ).data( 'select-id' ),
            target_select = $( '#' + select_id );

        target_select.find( 'option' ).prop( 'selected', false );
        target_select.trigger( 'change' );
    } );


    /** ------------------------------------------------------------------------
     *  Delete Logs Confirmation
     * ------------------------------------------------------------------------- */
    $( '#yith-wcbk-logs' ).on( 'click', 'h2 a.page-title-action', function ( event ) {
        event.stopImmediatePropagation();
        return window.confirm( wcbk_admin.i18n_delete_log_confirmation );
    } );


    /** ------------------------------------------------------------------------
     *  Tip Tip
     * ------------------------------------------------------------------------- */
    $( document ).on( 'yith-wcbk-init-tiptip', function () {
        // Remove any lingering tooltips
        $( '#tiptip_holder' ).removeAttr( 'style' );
        $( '#tiptip_arrow' ).removeAttr( 'style' );
        $( '.tips' ).tipTip( {
                                 'attribute': 'data-tip',
                                 'fadeIn'   : 50,
                                 'fadeOut'  : 50,
                                 'delay'    : 200
                             } );
    } ).trigger( 'yith-wcbk-init-tiptip' );


    /** ------------------------------------------------------------------------
     *  Date Picker
     * ------------------------------------------------------------------------- */
    $( document ).on( 'yith-wcbk-init-datepickers', function () {
        $( '.yith-wcbk-admin-date-picker' ).yith_wcbk_datepicker();
    } ).trigger( 'yith-wcbk-init-datepickers' );


    /** ------------------------------------------------------------------------
     *  Copy on Clipboard
     * ------------------------------------------------------------------------- */
    $( document ).on( 'click', '.yith-wcbk-copy-to-clipboard', function () {
        var target           = $( this ),
            selector_to_copy = target.data( 'selector-to-copy' ),
            obj_to_copy      = $( selector_to_copy );

        if ( obj_to_copy.length > 0 ) {
            var temp  = $( "<input>" ),
                value = obj_to_copy.is( 'input' ) ? obj_to_copy.val() : obj_to_copy.html();
            $( 'body' ).append( temp );

            temp.val( value ).select();
            document.execCommand( "copy" );

            temp.remove();
        }
    } );


    /** ------------------------------------------------------------------------
     *  Settings show conditional: show/hide element based on other element value
     * ------------------------------------------------------------------------- */
    $( '.yith-wcbk-show-conditional' ).hide().each( function () {
        var $show_conditional = $( this ),
            field_id          = $show_conditional.data( 'field-id' ),
            $field            = $( '#' + field_id ),
            value             = $show_conditional.data( 'value' );

        if ( $field.length > 0 ) {
            $field.on( 'change keyup', function () {
                if ( $field.val() === value ) {
                    $show_conditional.show();
                } else {
                    $show_conditional.hide();
                }
            } ).trigger( 'change' );
        }
    } );


    /** ------------------------------------------------------------------------
     *  Move
     * ------------------------------------------------------------------------- */
    $( '.yith-wcbk-move' ).each( function () {
        var $to_move = $( this ),
            after    = $to_move.data( 'after' );

        if ( after.length > 0 ) {
            $to_move.insertAfter( after ).show();
        }
    } );


    /** ------------------------------------------------------------------------
     *  Date Time Fields
     * ------------------------------------------------------------------------- */
    $( '.yith-wcbk-date-time-field' ).each( function () {
        var $dateTime  = $( this ),
            dateAnchor = $( this ).data( 'date' ),
            timeAnchor = $( this ).data( 'time' ),
            $date      = $( dateAnchor ).first(),
            $time      = $( timeAnchor ).first(),
            update     = function () {
                $dateTime.val( $date.val() + ' ' + $time.val() );
            };

        $date.on( 'change', update );
        $time.on( 'change', update );
    } );

    /** ------------------------------------------------------------------------
     *  Logs
     * ------------------------------------------------------------------------- */
    $( document ).on( 'click', '#yith-wcbk-logs-tab-table td.description-column .expand:not(.disabled)', function ( e ) {
        var open        = $( e.target ),
            description_column = open.closest( 'td.description-column' );
        description_column.toggleClass( 'expanded' );
    } );
} );