jQuery( document ).ready( function ( $ ) {
    "use strict";

    /** ------------------------------------------------------------------------
     *  Show/Hide Fields
     * ------------------------------------------------------------------------- */
    var show_if              = {
        customer_chooses_blocks: '.bk_show_if_customer_chooses_blocks',
        can_be_cancelled       : '.bk_show_if_can_be_cancelled',
        customer_one_day       : '.bk_show_if_customer_one_day',
        booking_has_persons    : '.bk_show_if_booking_has_persons',
        day                    : '.bk_show_if_day',
        time                   : '.bk_show_if_time',
        unit_is_month          : '.bk_show_if_unit_is_month',
        unit_is_day            : '.bk_show_if_unit_is_day',
        unit_is_hour           : '.bk_show_if_unit_is_hour',
        unit_is_minute         : '.bk_show_if_unit_is_minute',
    },
        showHideMe           = function ( target, condition ) {
            if ( condition ) {
                $( target ).show();
            } else {
                $( target ).hide();
            }
        },
        minute_select        = $( '#_yith_booking_duration_minute_select' ),
        booking_duration     = $( '#_yith_booking_duration' ),
        product_type_select  = $( 'select#product-type' ),
        initial_product_type = product_type_select.val();

    minute_select.hide().on( 'change', function () {
        booking_duration.val( $( this ).val() );
    } );

    // show TAX if product is Booking
    $( document ).find( '._tax_status_field' ).closest( 'div' ).addClass( 'show_if_' + wcbk_admin.prod_type );

    // on select booking product set Virtual as checked
    product_type_select
        .on( 'change', function () {
            if ( initial_product_type === wcbk_admin.prod_type )
                return;
            if ( wcbk_admin.prod_type === $( this ).val() ) {
                $( '#_virtual' ).attr( 'checked', 'checked' ).trigger( 'change' );
            }
        } )
        // trigger change to product type select to call WC show_and_hide_panels()
        .trigger( 'change' );


    // Booking Min Max Duration
    $( 'select#_yith_booking_duration_type' ).on( 'change', function () {
        var show = $( this ).val() === 'customer';
        showHideMe( show_if.customer_chooses_blocks, show );
    } ).trigger( 'change' );

    // Booking Can be cancelled
    $( 'input#_yith_booking_can_be_cancelled' ).on( 'change', function () {
        var show = $( this ).is( ':checked' );
        showHideMe( show_if.can_be_cancelled, show );
    } ).trigger( 'change' );

    // Show enable calendar picker if customer one day
    $( document ).on( 'check_if_customer_one_day', function () {
        var duration_type = $( 'select#_yith_booking_duration_type' ).val(),
            duration      = $( 'input#_yith_booking_duration' ).val(),
            duration_unit = $( 'select#_yith_booking_duration_unit' ).val(),
            show          = duration_type === 'customer' && duration === '1' && duration_unit === 'day';

        showHideMe( show_if.customer_one_day, show );
    } ).trigger( 'check_if_customer_one_day' );

    $( document ).on( 'change', '#_yith_booking_duration_type, #_yith_booking_duration, #_yith_booking_duration_unit', function () {
        $( document ).trigger( 'check_if_customer_one_day' );
    } );

    // Booking has persons
    $( 'input#_yith_booking_has_persons' ).on( 'change', function () {
        var show = $( this ).is( ':checked' );
        showHideMe( show_if.booking_has_persons, show );
    } ).trigger( 'change' );

    // Booking has times or is day
    $( 'select#_yith_booking_duration_unit' ).on( 'change', function () {
        var value    = $( this ).val(),
            has_time = value === 'hour' || value === 'minute',
            is_day   = value === 'day';
        showHideMe( show_if.time, has_time );
        showHideMe( show_if.day, is_day );

        showHideMe( show_if.unit_is_month, 'month' === value );
        showHideMe( show_if.unit_is_day, 'day' === value );
        showHideMe( show_if.unit_is_hour, 'hour' === value );
        showHideMe( show_if.unit_is_minute, 'minute' === value );

        // Minute Select
        if ( value === 'minute' ) {
            booking_duration.hide();
            minute_select.show().trigger( 'change' );
        } else {
            minute_select.hide();
            booking_duration.show();
        }
    } ).trigger( 'change' );


    /** ------------------------------------------------------------------------
     *  Costs Table
     * ------------------------------------------------------------------------- */
    var costs_table            = $( '#yith-wcbk-booking-costs-table' ),
        costs_default_row      = costs_table.find( 'tr.yith-wcbk-costs-default-row' ).first(),
        costs_add_range        = $( 'input#yith-wcbk-costs-add-range' ),
        costs_fields_container = $( '#' + costs_table.data( 'fields-container-id' ) ),
        costs_input            = costs_fields_container.find( '.yith-wcbk-admin-input-range' ),
        costs_number           = costs_fields_container.find( '.yith-wcbk-number-range' ),
        costs_monthselect      = costs_fields_container.find( '.yith-wcbk-month-range-select' ),
        costs_weekselect       = costs_fields_container.find( '.yith-wcbk-week-range-select' ),
        costs_dayselect        = costs_fields_container.find( '.yith-wcbk-day-range-select' );

    costs_table
        .on( 'change', 'select.yith-wcbk-costs-range-type-select', function ( event ) {
            var select     = $( event.target ),
                row        = select.closest( 'tr' ),
                from       = row.find( 'td.yith-wcbk-costs-from' ),
                to         = row.find( 'td.yith-wcbk-costs-to' ),
                range_type = select.val(),
                from_input, to_input;

            switch ( range_type ) {
                case 'custom':
                    from_input = costs_input.clone().addClass( 'yith-wcbk-admin-date-picker' ).yith_wcbk_datepicker();
                    to_input   = costs_input.clone().addClass( 'yith-wcbk-admin-date-picker' ).yith_wcbk_datepicker();
                    break;
                case 'month':
                    from_input = costs_monthselect.clone();
                    to_input   = costs_monthselect.clone();
                    break;
                case 'week':
                    from_input = costs_weekselect.clone();
                    to_input   = costs_weekselect.clone();
                    break;
                case 'day':
                    from_input = costs_dayselect.clone();
                    to_input   = costs_dayselect.clone();
                    break;
                default:
                    from_input = costs_number.clone();
                    to_input   = costs_number.clone();
                    break;
            }

            from.html( from_input.attr( 'name', '_yith_booking_costs_range[from][]' ) );
            to.html( to_input.attr( 'name', '_yith_booking_costs_range[to][]' ) );

        } )
        /* ----  D e l e t e   R o w  ---- */
        .on( 'click', '.yith-wcbk-delete', function ( event ) {
            var delete_btn = $( event.target ),
                target_row = delete_btn.closest( 'tr' );

            target_row.remove();
        } )

        /* ----  S o r t a b l e  ---- */
        .find( 'tbody' ).sortable( {
                                       items               : 'tr',
                                       cursor              : 'move',
                                       handle              : '.yith-wcbk-anchor',
                                       axis                : 'y',
                                       scrollSensitivity   : 40,
                                       forcePlaceholderSize: true,
                                       opacity             : 0.65,
                                       helper              : function ( e, tr ) {
                                           var originals = tr.children(),
                                               helper    = tr.clone();
                                           helper.children().each( function ( index ) {
                                               // Set helper cell sizes to match the original sizes
                                               $( this ).width( originals.eq( index ).width() );
                                           } );
                                           return helper;
                                       }
                                   } );

    costs_add_range.on( 'click', function () {
        var added_row = costs_default_row.clone().removeClass( 'yith-wcbk-costs-default-row' );
        costs_table.append( added_row );
        added_row.find( 'select.yith-wcbk-costs-range-type-select' ).trigger( 'change' );
    } );


    /** ------------------------------------------------------------------------
     *  Person Types Table
     * ------------------------------------------------------------------------- */
    var person_types_table = $( '#yith-wcbk-booking-person-type-table' );

    /* ----  S o r t a b l e  ---- */
    person_types_table.find( 'tbody' ).sortable( {
                                                     items               : 'tr',
                                                     cursor              : 'move',
                                                     handle              : '.yith-wcbk-anchor',
                                                     axis                : 'y',
                                                     scrollSensitivity   : 40,
                                                     forcePlaceholderSize: true,
                                                     opacity             : 0.65,
                                                     helper              : function ( e, tr ) {
                                                         var originals = tr.children(),
                                                             helper    = tr.clone();
                                                         helper.children().each( function ( index ) {
                                                             // Set helper cell sizes to match the original sizes
                                                             $( this ).width( originals.eq( index ).width() );
                                                         } );
                                                         return helper;
                                                     }
                                                 } );

    $( 'input#_yith_booking_enable_person_types' ).on( 'change', function () {
        if ( $( this ).is( ':checked' ) ) {
            person_types_table.show();
        } else {
            person_types_table.hide();
        }
    } ).trigger( 'change' );


    /** ------------------------------------------------------------------------
     *  Google Maps Auto-complete
     * ------------------------------------------------------------------------- */
    var maps_places_inputs = $( '.yith-wcbk-google-maps-places-autocomplete' );
    maps_places_inputs.each( function () {
        new google.maps.places.Autocomplete( this );
    } );


    /** ------------------------------------------------------------------------
     *  Booking Sync - Imported Calendars Table
     * ------------------------------------------------------------------------- */
    $( '.yith-wcbk-product-sync-imported-calendars-table' ).on( 'click', '.insert', function ( e ) {
        e.preventDefault();
        var button = $( e.target ),
            row    = button.data( 'row' ),
            table  = button.closest( '.yith-wcbk-product-sync-imported-calendars-table' ),
            body   = table.find( 'tbody' );

        body.append( $( row ) );
    } )
                                                           .on( 'click', '.delete', function ( e ) {
                                                               e.preventDefault();
                                                               var button = $( e.target ),
                                                                   row    = button.closest( 'tr' );

                                                               row.remove();
                                                           } );
} );