jQuery( document ).ready( function ( $ ) {
    "use strict";

    var availability_table           = $( '#yith-wcbk-booking-availability-table' ),
        availability_default_row     = availability_table.find( 'tr.yith-wcbk-availability-default-row' ).first(),
        availability_add_date_range  = $( 'input#yith-wcbk-availability-add-date-range' ),
        availability_add_month_range = $( 'input#yith-wcbk-availability-add-month-range' );

    availability_table.on( 'click', '.yith-wcbk-delete', function ( event ) {
        var delete_btn = $( event.target ),
            target_row = delete_btn.closest( 'tr' );

        target_row.remove();
    } )

    /* ----  S o r t a b l e  ---- */
        .find( 'tbody' ).sortable( {
                                       items               : 'tr',
                                       cursor              : 'move',
                                       handle              : '.yith-wcbk-anchor',
                                       axis                : 'y',
                                       scrollSensitivity   : 40,
                                       forcePlaceholderSize: true,
                                       opacity             : 0.65,
                                       helper              : function ( e, tr ) {
                                           var originals = tr.children(),
                                               helper    = tr.clone();
                                           helper.children().each( function ( index ) {
                                               // Set helper cell sizes to match the original sizes
                                               $( this ).width( originals.eq( index ).width() );
                                           } );
                                           return helper;
                                       }
                                   } );

    availability_add_date_range.on( 'click', function () {
        var added_row = availability_default_row.clone().removeClass( 'yith-wcbk-availability-default-row' );
        added_row.find( '.yith-wcbk-availability-range-column-ranges-month' ).remove();
        added_row.find( 'input' ).prop( 'disabled', false );
        added_row.find( 'select' ).prop( 'disabled', false );
        availability_table.append( added_row );
        added_row.find( '.yith-wcbk-on-off-container' ).yith_wcbk_onoff();
        added_row.find( '.yith-wcbk-on-off-advanced-container' ).yith_wcbk_onoff_advanced();
        added_row.find( '.yith-wcbk-date-input-field' ).addClass( 'yith-wcbk-admin-date-picker' ).yith_wcbk_datepicker();
        added_row.find( '.yith-wcbk-time-select' ).yith_wcbk_timeselect();
    } );

    availability_add_month_range.on( 'click', function () {
        var added_row = availability_default_row.clone().removeClass( 'yith-wcbk-availability-default-row' );
        added_row.find( '.yith-wcbk-availability-range-column-ranges-date' ).remove();
        added_row.find( 'input' ).prop( 'disabled', false );
        added_row.find( 'select' ).prop( 'disabled', false );
        availability_table.append( added_row );
        added_row.find( '.yith-wcbk-on-off-container' ).yith_wcbk_onoff();
        added_row.find( '.yith-wcbk-on-off-advanced-container' ).yith_wcbk_onoff_advanced();
        added_row.find( '.yith-wcbk-time-select' ).yith_wcbk_timeselect();
    } );

    $( document ).on( 'click', '.yith-wcbk-availability-toggle-visible', function () {
        var target       = $( this ),
            toggle_class = 'yith-wcbk-availability-range-column-days',
            parent       = target.closest( 'tr' ),
            to_hide      = parent.find( '.' + toggle_class ).first(),
            change       = parent.find( '.yith-wcbk-on-off-value' ).first().val() === 'yes' ? 'enable' : 'disable',
            days_enabled = to_hide.find( '.yith-booking-global-availability-days-enabled' );

        to_hide.toggle();
        to_hide.find( '.yith-wcbk-on-off-container' ).trigger( change );

        if ( days_enabled.val() === 'no' ) {
            days_enabled.val( 'yes' );
        } else {
            days_enabled.val( 'no' );
        }

    } );

    $( document ).on( 'click', '.yith-wcbk-availability-time-enabled-anchor', function () {
        var target       = $( this ),
            container    = target.closest( '.yith-wcbk-availability-range-column-days' ),
            days_enabled = container.find( '.yith-booking-availability-times-enabled' ).first();

        if ( days_enabled.val() === 'no' ) {
            days_enabled.val( 'yes' );
            container.removeClass( 'yith-wcbk-hide-times' );
        } else {
            days_enabled.val( 'no' );
            container.addClass( 'yith-wcbk-hide-times' );
        }

    } );
} );