=== WooCommerce Customer/Order XML Export Suite ===
Author: skyverge
Tags: woocommerce
Requires at least: 4.4
Tested up to: 4.9.7

Easily download customers & orders in XML format and automatically export FTP or HTTP POST on a recurring schedule

See http://docs.woothemes.com/document/woocommerce-customer-order-xml-export-suite/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-customer-order-xml-export-suite' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
