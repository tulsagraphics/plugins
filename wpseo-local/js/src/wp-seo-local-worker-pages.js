/* global analysisWorker */
import LocalStoreLocatorContentAssessment from './assessments/local-store-locator-content-assessment';

class LocalPagesWorker {
	register() {
		analysisWorker.registerMessageHandler( 'initializePages', this.initialize.bind( this ), 'YoastLocalSEO' );
	}

	initialize( settings ) {
		this.storeLocatorContentAssessment = new LocalStoreLocatorContentAssessment( settings );

		analysisWorker.registerAssessment( 'localStorelocatorContent', this.storeLocatorContentAssessment, 'YoastLocalSEO' );
	}
}

const localPagesWorker = new LocalPagesWorker();

localPagesWorker.register();
