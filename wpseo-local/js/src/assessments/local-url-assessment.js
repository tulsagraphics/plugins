import escapeRegExp from "lodash/escapeRegExp";

const { Paper, Researcher, AssessmentResult, Assessment } = yoast.analysis;

export default class LocalUrlAssessment extends Assessment {
	constructor( settings ) {
		super();
		this.settings = settings;
	}

	/**
	 * Runs an assessment for scoring the location in the URL.
	 *
	 * @param {Paper} paper The paper to run this assessment on.
	 * @param {Researcher} researcher The researcher used for the assessment.
	 * @param {Object} i18n The i18n-object used for parsing translations.
	 *
	 * @returns {object} an assessment result with the score and formatted text.
	 */
	getResult( paper, researcher, i18n ) {
		const assessmentResult = new AssessmentResult();
		if( this.settings.location !== '' ) {
			let location = this.settings.location;
			location = location.replace( "'", "" ).replace( /\s/ig, "-" );
			location = escapeRegExp( location );
			const business_city = new RegExp( location, 'ig' );
			const matches = paper.getUrl().match( business_city ) || 0;
			const result = this.score( matches );
			assessmentResult.setScore( result.score );
			assessmentResult.setText( result.text );
		}
		return assessmentResult;
	}

	/**
	 * Scores the url based on the matches of the location.
	 *
	 * @param {Array} matches The matches in the video title.
	 *
	 * @returns {{score: number, text: *}} An object containing the score and text
	 */
	score( matches ) {
		if ( matches.length > 0 ) {
			return{
				score: 9,
				text: this.settings.url_location
			}
		}
		return{
			score: 4,
			text: this.settings.url_no_location
		}
	}
}
