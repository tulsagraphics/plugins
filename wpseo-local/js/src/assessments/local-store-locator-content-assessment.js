const { Paper, Researcher, AssessmentResult, Assessment } = yoast.analysis;

export default class LocalStoreLocatorContentAssessment extends Assessment {
	constructor( settings ) {
		super();
		this.settings = settings;
	}

	/**
	 * Runs an assessment for scoring the presence of a store locator in the content.
	 *
	 * @param {Paper} paper The paper to run this assessment on.
	 * @param {Researcher} researcher The researcher used for the assessment.
	 * @param {Object} i18n The i18n-object used for parsing translations.
	 *
	 * @returns {object} an assessment result with the score and formatted text.
	 */
	getResult( paper, researcher, i18n ) {
		const assessmentResult = new AssessmentResult();

		const storeLocator = new RegExp( '<form action=["\']#wpseo-storelocator-form["\'] method=["\']post["\'] id=["\']wpseo-storelocator-form["\']>((.|[\n|\r|\r\n])*?)<div id=["\']wpseo-storelocator-results["\']>', 'ig' );
		const contains = storeLocator.test( paper.getText() );
		storeLocator.lastIndex = 0;
		const content = paper.getText().replace( storeLocator, '' );
		const result = this.score( content, contains );

		assessmentResult.setScore( result.score );
		assessmentResult.setText( result.text );

		return assessmentResult;
	}

	/**
	 * Scores the url based on the matches of the store locator.
	 *
	 * @param {string} content  The content outside of the store locator.
	 * @param {bool}   contains Whether or not the content contains a store locator.
	 *
	 * @returns {{score: number, text: *}} An object containing the score and text
	 */
	score( content, contains ) {
		if ( contains && content.length <= 200 ) {
			return {
				score: 6,
				text: this.settings.storelocator_content
			}
		}

		return {
			score: 9,
			text: ''
		}
	}
}
