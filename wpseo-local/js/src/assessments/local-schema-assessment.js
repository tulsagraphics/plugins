const { Paper, Researcher, AssessmentResult, Assessment } = yoast.analysis;

export default class LocalSchemaAssessment extends Assessment {
	constructor( settings ) {
		super();
		this.settings = settings;
	}

	/**
	 * Runs an assessment for scoring schema in the paper's text.
	 *
	 * @param {Paper} paper The paper to run this assessment on.
	 * @param {Researcher} researcher The researcher used for the assessment.
	 * @param {Object} i18n The i18n-object used for parsing translations.
	 *
	 * @returns {object} an assessment result with the score and formatted text.
	 */
	getResult( paper, researcher, i18n ) {
		const assessmentResult = new AssessmentResult();
		const schema = new RegExp( 'class=["\']wpseo-location["\'] itemscope', 'ig' );
		const matches = paper.getText().match( schema ) || 0;
		const result = this.score( matches );

		assessmentResult.setScore( result.score );
		assessmentResult.setText( result.text );

		return assessmentResult;
	}

	/**
	 * Scores the content based on the matches of the location schema.
	 *
	 * @param {Array} matches The matches in the video title.
	 *
	 * @returns {{score: number, text: *}} An object containing the score and text
	 */
	score( matches ) {
		if ( matches.length > 0 ) {
			return{
				score: 9,
				text: this.settings.address_schema
			}
		}
		return{
			score: 4,
			text: this.settings.no_address_schema
		}
	}
}
