/* global analysisWorker */
import LocalTitleAssessment from './assessments/local-title-assessment';
import LocalUrlAssessment from './assessments/local-url-assessment';
import LocalSchemaAssessment from './assessments/local-schema-assessment';

class LocalLocationsWorker {
	register() {
		analysisWorker.registerMessageHandler( 'initializeLocations', this.initialize.bind( this ), 'YoastLocalSEO' );
	}

	initialize( settings ) {
		this.titleAssessment = new LocalTitleAssessment( settings );
		this.urlAssessment = new LocalUrlAssessment( settings );
		this.schemaAssessment = new LocalSchemaAssessment( settings );

		analysisWorker.registerAssessment( 'localTitle', this.titleAssessment, 'YoastLocalSEO' );
		analysisWorker.registerAssessment( 'localUrl', this.urlAssessment, 'YoastLocalSEO' );
		analysisWorker.registerAssessment( 'localSchema', this.schemaAssessment, 'YoastLocalSEO' );
	}
}

const localLocationsWorker = new LocalLocationsWorker();

localLocationsWorker.register();
