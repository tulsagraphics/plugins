/* global YoastSEO */
/* global wpseoLocalL10n */

if ( typeof YoastSEO !== 'undefined' && typeof YoastSEO.analysisWorker !== 'undefined' ) {
	new YoastLocalSEOLocationsPlugin();
}
else {
	jQuery( window ).on( 'YoastSEO:ready', () => new YoastLocalSEOLocationsPlugin() );
}

class YoastLocalSEOLocationsPlugin {
	constructor() {
		this.bindEventListener();
		this.loadWorkerScripts();
	}

	bindEventListener() {
		const elem = document.getElementById( 'wpseo_business_city' );
		if( elem !== null){
			elem.addEventListener( 'change', YoastSEO.app.refresh );
		}
	}

	loadWorkerScripts() {
		if ( typeof YoastSEO === 'undefined' || typeof YoastSEO.analysis === "undefined" || typeof YoastSEO.analysis.worker === "undefined" ) {
			return;
		}

		YoastSEO.analysis.worker.loadScript( wpseoLocalL10n.locations_script_url )
			.then( () => YoastSEO.analysis.worker.sendMessage( 'initializeLocations', wpseoLocalL10n, 'YoastLocalSEO' ) );
	}
}
