=== MainWP iThemes Security Extension ===
Plugin Name: MainWP iThemes Security Extension
Plugin URI: https://mainwp.com
Description: MainWP iThemes Security Extension.
Version: 1.8
Author: MainWP
Author URI: https://mainwp.com

== Installation ==

1. Please install plugin "MainWP Dashboard" and active it before install MainWP iThemes Security Extension plugin (get the MainWP Dashboard plugin from url:http://mainwp.com/)
2. Upload the `mainwp-ithemes-security-extension` folder to the `/wp-content/plugins/` directory
3. Activate the MainWP iThemes Security Extension plugin through the 'Plugins' menu in WordPress

== Screenshots ==
1. Enable or Disable extension on the "Extensions" page in the dashboard

== Changelog ==

= 1.8 - 8-10-2018 =
* Fixed: PHP error that caused issues with activating the extension

= 1.7 - 7-27-2018 =
* Fixed: compatibility issue caused by the new iThemes Security plugin version

= 1.6 - 4-19-2018 =
* Added: support for the new iThemes Security options

= 1.5 - 12-5-2017 =
* Fixed: new iThemes Security plugin version compatibility issues
* Added: support for the new iThemes Security plugin options
* Added: option to save all settings to child sites

= 1.4 - 5-24-2017 =
* Fixed: an issue with hidding the iThemes Security plugin on child sites
* Fixed: an issue with a broken layout in the Security Check section
* Added: support for new iThemes Security features
* Added: the Help tab

= 1.3 - 9-2-2016 =
* Fixed: an issue with saving settings to a child site that is being added to a dashboard

= 1.2 - 8-2-2016 =
* Fixed: an issue with displaying the "New! The iThemes Security dashboard just got a new look" message multiple times
* Fixed: an issue with saving Global Settings
* Fixed: an issue with saving the Network Brute Force Protection settings
* Added: new plugin options
* Added: an option to "Use value from individual site settings" for saving "Path to Log Files", "Backup Location", "Exclude Tables" settings

= 1.1 - 6-10-2016 =
* Added: new features introduced in the new iThemes Security plugin version
* Updated: compatibility with the new version of the iThemes Security plugin

= 1.0 - 2-17-2016 =
* Fixed: Fatal error
* Added: Support for WP-CLI
* Added: Support for the new Add Site process
* Added: An auto update warning if the extension is not activated
* Updated: "Check for updates now" link is not vidible if extension is not activated
* Updated: Refactored code to meet WordPress coding standards

= 0.0.4 - 4-22-2015 =
* Fixed: Potential XSS Security Vulnerability issue

= 0.0.3 = 
* Updated: Quick start guide layout

= 0.0.2 =
* Fixed: iThemes Security Pro plugin compatibility issue
* Fixed: A plugin confclit with the iThemes Security plugin installed on a dashbaord site

= 0.0.1 =
* First version

