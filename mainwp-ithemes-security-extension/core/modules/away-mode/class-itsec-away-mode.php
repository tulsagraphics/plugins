<?php

final class MainWP_ITSEC_Away_Mode {

	public function run() {

		//Execute away mode functions on admin init
		add_filter( 'mainwp_itsec_logger_modules', array( $this, 'register_logger' ) );
		add_action( 'mainwp-itsec_admin_init', array( $this, 'run_active_check' ) );
		add_action( 'login_init', array( $this, 'run_active_check' ) );

		//Register Sync
		add_filter( 'mainwp_itsec_sync_modules', array( $this, 'register_sync' ) );

	}
	
	/**
	 * Check if away mode is active
	 *
	 * @since 4.4
	 * @static
	 *
	 * @param bool $get_details Optional, defaults to false. True to receive details rather than a boolean response.
	 *
	 * @return mixed If $get_details is true, an array of status details. Otherwise, true if away and false otherwise.
	 */
	public static function is_active( $get_details = false ) {
		require_once( dirname( __FILE__ ) . '/utilities.php' );
		
		$settings = MainWP_ITSEC_Modules::get_settings( 'away-mode' );
		
		if ( 'daily' === $settings['type'] ) {
			$details = MainWP_ITSEC_Away_Mode_Utilities::is_current_time_active( $settings['start_time'], $settings['end_time'], true );
		} else {
			$details = MainWP_ITSEC_Away_Mode_Utilities::is_current_timestamp_active( $settings['start'], $settings['end'], true );
		}
		
		
		$details['override_type'] = $settings['override_type'];
		$details['override_end'] = $settings['override_end'];
		
		if ( empty( $settings['override_type'] ) || ( MainWP_ITSEC_Core::get_current_time() > $settings['override_end'] ) ) {
			$details['override_active'] = false;
		} else {
			$details['override_active'] = true;
			
			if ( 'activate' === $details['override_type'] ) {
				$details['active'] = true;
			} else {
				$details['active'] = false;
			}
		}
		
		
		if ( ! isset( $details['error'] ) ) {
			$details['error'] = false;
		}
		
		
		
		if ( $get_details ) {
			return $details;
		}
		
		return $details['active'];
	}

	/**
	 * Execute away mode functionality
	 *
	 * @return void
	 */
	public function run_active_check() {


	}

	/**
	 * Register 404 and file change detection for logger
	 *
	 * @param  array $logger_modules array of logger modules
	 *
	 * @return array array of logger modules
	 */
	public function register_logger( $logger_modules ) {

		$logger_modules['away_mode'] = array(
			'type'     => 'away_mode',
			'function' => __( 'Away Mode Triggered', 'l10n-mainwp-ithemes-security-extension' ),
		);

		return $logger_modules;

	}

	/**
	 * Register Lockouts for Sync
	 *
	 * @param  array $sync_modules array of logger modules
	 *
	 * @return array array of logger modules
	 */
	public function register_sync( $sync_modules ) {

		$sync_modules['away_mode'] = array(
			'verbs'      => array(
				'itsec-get-away-mode'      => 'Ithemes_Sync_Verb_ITSEC_Get_Away_Mode',
				'itsec-override-away-mode' => 'Ithemes_Sync_Verb_ITSEC_Override_Away_Mode'
			),
			'everything' => 'itsec-get-away-mode',
			'path'       => dirname( __FILE__ ),
		);

		return $sync_modules;

	}

}
