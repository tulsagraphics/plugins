<?php

if ( ! class_exists( 'MainWP_ITSEC_Away_Mode_Setup' ) ) {

	class MainWP_ITSEC_Away_Mode_Setup {

		public function __construct() {

			add_action( 'mainwp_itsec_modules_do_plugin_activation',   array( $this, 'execute_activate'   )          );
			add_action( 'mainwp_itsec_modules_do_plugin_deactivation', array( $this, 'execute_deactivate' )          );
			add_action( 'mainwp_itsec_modules_do_plugin_uninstall',    array( $this, 'execute_uninstall'  )          );
			add_action( 'mainwp_itsec_modules_do_plugin_upgrade',      array( $this, 'execute_upgrade'    ), null, 2 );

		}

		/**
		 * Execute module activation.
		 *
		 * @since 4.0
		 *
		 * @return void
		 */
		public function execute_activate() {
		}

		/**
		 * Execute module deactivation
		 *
		 * @return void
		 */
		public function execute_deactivate() {

			delete_site_option( 'itsec_away_mode_sync_override' );
			delete_site_transient( 'itsec_away' );
			delete_site_transient( 'itsec_away_mode' );

		}

		/**
		 * Execute module uninstall
		 *
		 * @return void
		 */
		public function execute_uninstall() {

			$this->execute_deactivate();

			delete_site_option( 'itsec_away_mode' );

		}

		/**
		 * Execute module upgrade
		 *
		 * @return void
		 */
		public function execute_upgrade( $mainwp_itsec_old_version ) {

			if ( $mainwp_itsec_old_version < 4041 ) {
				$current_options = null;
				global $mainwp_itsec_globals;
				if (isset($mainwp_itsec_globals['current_itheme_settings']['itsec_away_mode'])) {
					$current_options = $mainwp_itsec_globals['current_itheme_settings']['itsec_away_mode'];
				}				
				$current_override_options = get_site_option( 'mainwp_itsec_away_mode_sync_override' );

				// If there are no current options, go with the new defaults by not saving anything
				if ( is_array( $current_options ) || is_array( $current_override_options ) ) {
					$settings = MainWP_ITSEC_Modules::get_defaults( 'away-mode' );
					$original_settings = $settings;

					if ( is_array( $current_options ) ) {
						$settings['type']       = ( 1 == $current_options['type'] )? 'daily' : 'one-time';
						$settings['start']      = intval( $current_options['start'] - MainWP_ITSEC_Core::get_time_offset() );
						$settings['start_time'] = $current_options['start'] - strtotime( date( 'Y-m-d', $current_options['start'] ) );
						$settings['end']        = intval( $current_options['end'] - MainWP_ITSEC_Core::get_time_offset() );
						$settings['end_time']   = $current_options['end'] - strtotime( date( 'Y-m-d', $current_options['end'] ) );
					}

					if ( is_array( $current_override_options ) ) {
						$settings['override_type'] = $current_override_options['intention'];
						$settings['override_end']  = $current_override_options['expires'];
					}

					MainWP_ITSEC_Modules::set_settings( 'away-mode', $settings );

					if ( isset( $current_options['enabled'] ) && $current_options['enabled'] ) {
						MainWP_ITSEC_Modules::activate( 'away-mode' );
					} else {
						MainWP_ITSEC_Modules::deactivate( 'away-mode' );
					}
					MainWP_IThemes_Security::unset_setting_field(array('itsec_away_mode') , $mainwp_itsec_globals['current_site_id']);
				}
			}

		}

	}

}

new MainWP_ITSEC_Away_Mode_Setup();
