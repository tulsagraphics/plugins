<?php

final class MainWP_ITSEC_Away_Mode_Settings extends MainWP_ITSEC_Settings {
	public function get_id() {
		return 'away-mode';
	}
	
	public function get_defaults() {
		return array(
			'type'          => 'daily',
			'start'         => 1,
			'start_time'    => 100000,
			'end'           => 1,
			'end_time'      => 100000,
			'override_type' => '',
			'override_end'  => 0,
		);
	}
	
	protected function after_save() {
		require_once( dirname( __FILE__ ) . '/utilities.php' );
		
//		MainWP_ITSEC_Away_Mode_Utilities::create_active_file();
	}
}

MainWP_ITSEC_Modules::register_settings( new MainWP_ITSEC_Away_Mode_Settings() );
