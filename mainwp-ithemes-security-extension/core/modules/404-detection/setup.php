<?php

if ( ! class_exists( 'MainWP_ITSEC_Four_Oh_Four_Setup' ) ) {

	class MainWP_ITSEC_Four_Oh_Four_Setup {

		private
			$defaults;

		public function __construct() {

			add_action( 'mainwp_itsec_modules_do_plugin_activation',   array( $this, 'execute_activate'   )          );
			add_action( 'mainwp_itsec_modules_do_plugin_deactivation', array( $this, 'execute_deactivate' )          );
			add_action( 'mainwp_itsec_modules_do_plugin_uninstall',    array( $this, 'execute_uninstall'  )          );
			add_action( 'mainwp_itsec_modules_do_plugin_upgrade',      array( $this, 'execute_upgrade'    ), null, 2 );

		}

		/**
		 * Execute module activation.
		 *
		 * @since 4.0
		 *
		 * @return void
		 */
		public function execute_activate() {
		}

		/**
		 * Execute module deactivation
		 *
		 * @return void
		 */
		public function execute_deactivate() {
		}

		/**
		 * Execute module uninstall
		 *
		 * @return void
		 */
		public function execute_uninstall() {

			$this->execute_deactivate();

			delete_site_option( 'itsec_four_oh_four' );

		}

		/**
		 * Execute module upgrade
		 *
		 * @return void
		 */
		public function execute_upgrade( $mainwp_itsec_old_version ) {
			
			if ( $mainwp_itsec_old_version < 4041 ) {		
				$current_options = null;
				global $mainwp_itsec_globals;
				if (isset($mainwp_itsec_globals['current_itheme_settings']['itsec_four_oh_four'])) {
					$current_options = $mainwp_itsec_globals['current_itheme_settings']['itsec_four_oh_four'];
				}
				
				// If there are no current options, go with the new defaults by not saving anything
				if ( is_array( $current_options ) ) {
					// Make sure the new module is properly activated or deactivated
					if ( $current_options['enabled'] ) {
						MainWP_ITSEC_Modules::activate( '404-detection' );
					} else {
						MainWP_ITSEC_Modules::deactivate( '404-detection' );
					}

					// remove 'enabled' which isn't use in the new module
					unset( $current_options['enabled'] );
					MainWP_ITSEC_Modules::set_settings( '404-detection', $current_options );
					MainWP_IThemes_Security::unset_setting_field(array('itsec_four_oh_four') , $mainwp_itsec_globals['current_site_id']);
				}
			}

		}

	}

}

new MainWP_ITSEC_Four_Oh_Four_Setup();
