<?php

class MainWP_ITSEC_Strong_Passwords {

	function run() {
		add_action( 'mainwp_itsec_register_password_requirements', array( $this, 'register_requirements' ) );
	}

    /**
	 * Register the Strong Passwords requirement.
	 */
	public function register_requirements() {
		MainWP_ITSEC_Lib_Password_Requirements::register( 'strength', array(
			'evaluate_if_not_enabled' => true,
			'defaults'                => array( 'role' => 'administrator' ),
			'settings_config'         => array( $this, 'get_settings_config' ),
		) );
	}

    public function get_settings_config() {
		return array(
			'label'       => esc_html__( 'Strong Passwords', 'better-wp-security' ),
			'description' => esc_html__( 'Force users to use strong passwords as rated by the WordPress password meter.', 'better-wp-security' ),
			'render'      => array( $this, 'render_settings' ),
			'sanitize'    => array( $this, 'sanitize_settings' ),
		);
	}

    public function render_settings( $form ) {

		$href = 'http://codex.wordpress.org/Roles_and_Capabilities';
		$link = '<a href="' . $href . '" target="_blank" rel="noopener noreferrer">' . $href . '</a>';
		?>
		<tr>
			<th scope="row">
				<label for="itsec-password-requirements-requirement_settings-strength-role">
					<?php esc_html_e( 'Minimum Role', 'better-wp-security' ); ?>
				</label>
			</th>
			<td>
				<?php $form->add_canonical_roles( 'role' ); ?>
				<br/>
				<label for="itsec-password-requirements-requirement_settings-strength-role"><?php _e( 'Minimum role at which a user must choose a strong password.', 'better-wp-security' ); ?></label>
				<p class="description"><?php printf( __( 'For more information on WordPress roles and capabilities please see %s.', 'better-wp-security' ), $link ); ?></p>
				<p class="warningtext description"><?php _e( 'Warning: If your site invites public registrations setting the role too low may annoy your members.', 'better-wp-security' ); ?></p>
			</td>
		</tr>
		<?php
	}

    /**
	 * Get a list of the sanitizer rules to apply.
	 *
	 * @param array $settings
	 *
	 * @return array
	 */
	public function sanitize_settings( $settings ) {
		return array(
			array( 'string', 'role', esc_html__( 'Minimum Role for Strong Passwords', 'better-wp-security' ) ),
			array( 'canonical-roles', 'role', esc_html__( 'Minimum Role for Strong Passwords', 'better-wp-security' ) ),
		);
	}

	/**
	 * Enqueue script to check password strength
	 *
	 * @return void
	 */
	public function add_scripts() {

		global $mainwp_itsec_globals;

		$module_path = MainWP_ITSEC_Lib::get_module_path( __FILE__ );

		wp_enqueue_script( 'mainwp-itsec_strong_passwords', $module_path . 'js/script.js', array( 'jquery' ), MainWP_ITSEC_Core::get_plugin_build() );

	}


}
