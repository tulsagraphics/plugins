<?php

if ( ! class_exists( 'MainWP_ITSEC_Strong_Passwords_Setup' ) ) {

	class MainWP_ITSEC_Strong_Passwords_Setup {

		public function __construct() {

			add_action( 'mainwp_itsec_modules_do_plugin_activation',   array( $this, 'execute_activate'   )          );
			add_action( 'mainwp_itsec_modules_do_plugin_deactivation', array( $this, 'execute_deactivate' )          );
			add_action( 'mainwp_itsec_modules_do_plugin_uninstall',    array( $this, 'execute_uninstall'  )          );
			add_action( 'mainwp_itsec_modules_do_plugin_upgrade',      array( $this, 'execute_upgrade'    ), null, 2 );

		}

		/**
		 * Execute module activation.
		 *
		 * @since 4.0
		 *
		 * @return void
		 */
		public function execute_activate() {
		}

		/**
		 * Execute module deactivation
		 *
		 * @return void
		 */
		public function execute_deactivate() {
		}

		/**
		 * Execute module uninstall
		 *
		 * @return void
		 */
		public function execute_uninstall() {

			$this->execute_deactivate();

			delete_site_option( 'itsec_strong_passwords' );

		}

		/**
		 * Execute module upgrade
		 *
		 * @return void
		 */
		public function execute_upgrade( $mainwp_itsec_old_version ) {

//			if ( $mainwp_itsec_old_version < 4041 ) {
//
//				global $mainwp_itsec_globals;
//				$current_options = null;
//				if (isset($mainwp_itsec_globals['current_itheme_settings']['itsec_strong_passwords'])) {
//					$current_options = $mainwp_itsec_globals['current_itheme_settings']['itsec_strong_passwords'];
//				}
//
//				// If there are no current options, go with the new defaults by not saving anything
//				if ( is_array( $current_options ) ) {
//					// Make sure the new module is properly activated or deactivated
//					if ( $current_options['enabled'] ) {
//						MainWP_ITSEC_Modules::activate( 'strong-passwords' );
//					} else {
//						MainWP_ITSEC_Modules::deactivate( 'strong-passwords' );
//					}
//
//					$settings = array( 'role' => $current_options['roll'] );
//
//					MainWP_ITSEC_Modules::set_settings( 'strong-passwords', $settings );
//					MainWP_IThemes_Security::unset_setting_field(array('itsec_strong_passwords') , $mainwp_itsec_globals['current_site_id']);
//				}
//			}

		}

	}

}

new MainWP_ITSEC_Strong_Passwords_Setup();
