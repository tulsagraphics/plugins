<?php

if ( ! class_exists( 'MainWP_ITSEC_IPCheck_Setup' ) ) {

	class MainWP_ITSEC_IPCheck_Setup {

		private
			$defaults;

		public function __construct() {

			add_action( 'mainwp_itsec_modules_do_plugin_activation',   array( $this, 'execute_activate'   )          );
			add_action( 'mainwp_itsec_modules_do_plugin_deactivation', array( $this, 'execute_deactivate' )          );
			add_action( 'mainwp_itsec_modules_do_plugin_uninstall',    array( $this, 'execute_uninstall'  )          );
			add_action( 'mainwp_itsec_modules_do_plugin_upgrade',      array( $this, 'execute_upgrade'    ), null, 2 );

		}

		/**
		 * Execute module activation.
		 *
		 * @since 4.0
		 *
		 * @return void
		 */
		public function execute_activate() {
		}

		/**
		 * Execute module deactivation
		 *
		 * @return void
		 */
		public function execute_deactivate() {

			global $wpdb;

			$wpdb->query( "DELETE FROM `" . $wpdb->base_prefix . "options` WHERE `option_name` LIKE ('%_itsec_ip_cache%')" );

		}

		/**
		 * Execute module uninstall
		 *
		 * @return void
		 */
		public function execute_uninstall() {

			$this->execute_deactivate();

			delete_site_option( 'itsec_ipcheck' );

		}

		/**
		 * Execute module upgrade
		 *
		 * @return void
		 */
		public function execute_upgrade( $mainwp_itsec_old_version ) {
			if ( $mainwp_itsec_old_version < 4041 ) {
				
				global $mainwp_itsec_globals;
				$current_options = null;				
				if (isset($mainwp_itsec_globals['current_itheme_settings']['itsec_ipcheck'])) {
					$current_options = $mainwp_itsec_globals['current_itheme_settings']['itsec_ipcheck'];
				}
				
				// If there are no current options, go with the new defaults by not saving anything
				if ( is_array( $current_options ) ) {
					$settings = MainWP_ITSEC_Modules::get_defaults( 'network-brute-force' );

					if ( isset( $current_options['api_ban'] ) ) {
						$settings['enable_ban'] = $current_options['api_ban'];
					}

					// Make sure the new module is properly activated or deactivated
					if ( $settings['enable_ban'] ) {
						MainWP_ITSEC_Modules::activate( 'network-brute-force' );
					} else {
						MainWP_ITSEC_Modules::deactivate( 'network-brute-force' );
					}

					
					if ( ! empty( $current_options['api_s'] ) ) {
						$settings['api_secret'] = $current_options['api_s'];
					}

					if ( ! empty( $current_options['optin'] ) ) {
						$settings['updates_optin'] = $current_options['optin'];
					}

					MainWP_ITSEC_Modules::set_settings( 'network-brute-force', $settings );
					MainWP_IThemes_Security::unset_setting_field(array('itsec_ipcheck') , $mainwp_itsec_globals['current_site_id']);
				}
			}
		}

	}

}

new MainWP_ITSEC_IPCheck_Setup();
