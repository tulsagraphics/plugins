jQuery( document ).ready( function () {
	var $container = jQuery( '#wpcontent' );
	
	
	$container.on( 'click', '#itsec-backup-reset_backup_location', function( e ) {
		e.preventDefault();
		
		jQuery( '#itsec-backup-location' ).val( mainwp_itsec_backup_local.default_backup_location );
	} );
	
	$container.on( 'change', '#itsec-backup-method', function( e ) {
		var method = jQuery(this).val();
		
		if ( 1 == method ) {
			jQuery( '.itsec-backup-method-file-content' ).hide();
		} else {
			jQuery( '.itsec-backup-method-file-content' ).show();
		}
	} );
	
	jQuery( '#itsec-backup-method' ).trigger( 'change' );
	
	
	jQuery( '#itsec-backup-exclude' ).multiSelect( {
		selectableHeader: '<div class="custom-header">' + mainwp_itsec_backup_local.available_tables_label + '</div>',
		selectionHeader:  '<div class="custom-header">' + mainwp_itsec_backup_local.excluded_tables_label + '</div>',
		keepOrder:        true
	} );
	
	
//	jQuery( '#itsec-backup-create_backup' ).click(function( e ) {
//		e.preventDefault();
//		
//		var originalButtonLabel = jQuery( '#itsec-backup-create_backup' ).attr( 'value' );
//		
//		jQuery( '#itsec-backup-create_backup' )
//			.removeClass( 'button-primary' )
//			.addClass( 'button-secondary' )
//			.attr( 'value', mainwp_itsec_backup_local.creating_backup_text )
//			.prop( 'disabled', true );
//		
//		jQuery( '#itsec_backup_status' ).html( '' );
//		
//		var data = {
//			'method': 'create-backup'
//		};
//		
//		mainwp_itsecSettingsPage.sendModuleAJAXRequest( 'backup', data, function( results ) {
//			jQuery( '#itsec_backup_status' ).html( results.response );
//			
//			jQuery( '#itsec-backup-create_backup' )
//				.removeClass( 'button-secondary' )
//				.addClass( 'button-primary' )
//				.attr( 'value', originalButtonLabel )
//				.prop( 'disabled', false );
//		} );
//	});

} );


jQuery( document ).ready( function ($) {
    $('#mwp_itheme_backups_db_btn').on('click', function(event) {        
        event.preventDefault();
        var caller = this;
        jQuery( caller ).closest('.submit').find('i').show();
        jQuery( caller ).attr('disabled', 'disabled');
        
        if ( mainwp_itsec_page.individualSite && mainwp_itsec_page.individualSite > 0) {                           
            mainwp_itheme_individual_backups(mainwp_itsec_page.ithemeSiteID, caller);
        } else {            
            var data = {
                action:'mainwp_itheme_load_sites',
                what: 'backup_db'
            };            
            jQuery.post(ajaxurl, data, function (response) {
                if (response) {
                    jQuery('#mainwp_itheme_screens_tab').html(response);    
                    ithemes_bulkTotalThreads = jQuery('.siteItemProcess[status=queue]').length;
                    if (ithemes_bulkTotalThreads > 0) 
                        mainwp_itheme_backup_db_start_next();
                } else {
                    jQuery('#mainwp_itheme_screens_tab').html('<div class="mainwp_info-box-red">' + __("Undefined error.") + '</div>');

                }
            })
        }
    });
    
     $('#mwp_itheme_backups_reload_exclude_tables_btn').on('click', function(event) {        
        var statusEl = jQuery('#itsec_reload_exclude_status');  
        statusEl.css('color', '#21759B');
        statusEl.html('<i class="fa fa-spinner fa-pulse"></i> Running ...').show();      
        var data = {
                    action: 'mainwp_itheme_reload_exclude_tables',
                    ithemeSiteID: mainwp_itsec_page.ithemeSiteID,
                    nonce:  mainwp_itsec_page.ajax_nonce
                };
        //call the ajax
        jQuery.post( ajaxurl, data, function ( response ) {                                
                var message = '';
                var error = false;
                if ( response) {
                    if (response.message)
                        message = response.message;

                    if (response.error) {
                        error = true;
                        message = response.error;                    
                    } else if (response.html) {                                      
                        message = __('Successful');                        
                        jQuery( '#backup_multi_select_wrap').html(response.html);
                        jQuery( '#itsec-backup-exclude' ).multiSelect( {
                            selectableHeader: '<div class="custom-header">' + mainwp_itsec_backup_local.available_tables_label + '</div>',
                            selectionHeader:  '<div class="custom-header">' + mainwp_itsec_backup_local.excluded_tables_label + '</div>',
                            keepOrder:        true
                        } );                        
                    } else {
                        error = true;
                        message = __( 'Undefined error' );                    
                    }
                }
                else 
                {
                    error = true;
                    message = __( 'Undefined error' );
                }

                if ( error ) {
                    statusEl.css('color', 'red');
                } else {
                    statusEl.css('color', '#21759B');
                    setTimeout(function ()
                        {
                        statusEl.fadeOut();
                    }, 3000);
                }   
                statusEl.html( message );
                statusEl.fadeIn();                
        }, 'json' );
    });
    
} );

mainwp_itheme_individual_backups = function(pSiteId, pCaller) {    
    var statusEl = jQuery('#itsec_backup_status');  
    statusEl.css('color', '#21759B');
    statusEl.html('<i class="fa fa-spinner fa-pulse"></i> Running ...').show();      
    var data = {
                action: 'mainwp_itheme_backups_database',
                ithemeSiteID: pSiteId,
                individualSite: 1,
                nonce:  mainwp_itsec_page.ajax_nonce
            };
    //call the ajax
    jQuery.post( ajaxurl, data, function ( response ) {
            jQuery( pCaller ).closest('.submit').find('i').hide();
            jQuery( pCaller ).removeAttr('disabled'); 
            var message = '';
            var error = false;
            if ( response) {
                if (response.message)
                    message = response.message;
                    
                if (response.error) {
                    error = true;
                    message = response.error;                    
                } else if (response.result == 'success') {
                    if ( message == '' )                        
                        message = mainwp_itsec_backup_local.success;                    
                } else if (response.result == 'fail') {
                    error = true;
                    if ( message == '' )
                        message = mainwp_itsec_backup_local.fail;                    
                } else {
                    error = true;
                    message = __( 'Undefined error' );                    
                }
            }
            else 
            {
                error = true;
                message = __( 'Undefined error' );
            }
            
            if ( error ) {
                statusEl.css('color', 'red');
            } else {
                statusEl.css('color', '#21759B');
            }   
            statusEl.html( message );
            statusEl.fadeIn();
    }, 'json' );
}

mainwp_itheme_backup_db_start_next = function() {    
    while ((objProcess = jQuery('.siteItemProcess[status=queue]:first')) && (objProcess.length > 0) && (ithemes_bulkCurrentThreads < ithemes_bulkMaxThreads))
    {       
        objProcess.attr('status', 'processed');       
        mainwp_itheme_backup_db_start_specific(objProcess);
    }
    
    if (ithemes_bulkFinishedThreads > 0 && ithemes_bulkFinishedThreads == ithemes_bulkTotalThreads) {
        jQuery('#mainwp_itheme_screens_tab').append('<div class="mainwp_info-box">' + __("Backups finished.") + '</div><p><a class="button-primary" href="' + mainwp_itsec_page.settings_page_url + '">Return to Settings</a></p>');      
    }
    
}

mainwp_itheme_backup_db_start_specific = function(objProcess) {    
    var loadingEl = objProcess.find('i');  
    var statusEl = objProcess.find('.status');  
    statusEl.html('');
    ithemes_bulkCurrentThreads++;
    var data = {
                action: 'mainwp_itheme_backups_database',
                ithemeSiteID: objProcess.attr('site-id'),
                nonce:  mainwp_itsec_page.ajax_nonce
            };
            
    statusEl.html('');
    loadingEl.show();
    //call the ajax
    jQuery.post( ajaxurl, data, function ( response ) {
            loadingEl.hide();            
            var message = '';
            var error = false;
            
            if ( response) {
                if (response.message)
                    message = response.message;
                    
                if (response.error) {
                    error = true;
                    message = response.error;                    
                } else if (response.result == 'success') {
                    if ( message == '' )                        
                        message = mainwp_itsec_backup_local.success;                    
                } else if (response.result == 'fail') {
                    error = true;
                    if ( message == '' )
                        message = mainwp_itsec_backup_local.fail;                    
                } else {
                    error = true;
                    message = __( 'Undefined error' );                    
                }
            }
            else 
            {
                error = true;
                message = __( 'Undefined error' );
            }
            
            if ( error ) {
                statusEl.css('color', 'red');
            } else {
                statusEl.css('color', '#21759B');
            }   
            statusEl.html( message );
            statusEl.fadeIn();
            
            ithemes_bulkCurrentThreads--;
            ithemes_bulkFinishedThreads++;
            mainwp_itheme_backup_db_start_next();
    }, 'json' );
}

