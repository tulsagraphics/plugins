<?php

if ( ! class_exists( 'MainWP_ITSEC_Backup_Setup' ) ) {

	class MainWP_ITSEC_Backup_Setup {

		public function __construct() {

			add_action( 'mainwp_itsec_modules_do_plugin_activation',   array( $this, 'execute_activate'   )          );
			add_action( 'mainwp_itsec_modules_do_plugin_deactivation', array( $this, 'execute_deactivate' )          );
			add_action( 'mainwp_itsec_modules_do_plugin_uninstall',    array( $this, 'execute_uninstall'  )          );
			add_action( 'mainwp_itsec_modules_do_plugin_upgrade',      array( $this, 'execute_upgrade'    ), null, 2 );

		}

		/**
		 * Execute module activation.
		 *
		 * @since 4.0
		 *
		 * @return void
		 */
		public function execute_activate() {
		}

		/**
		 * Execute module deactivation
		 *
		 * @return void
		 */
		public function execute_deactivate() {
		}

		/**
		 * Execute module uninstall
		 *
		 * @return void
		 */
		public function execute_uninstall() {

			$this->execute_deactivate();

			delete_site_option( 'itsec_backup' );

		}

		/**
		 * Execute module upgrade
		 *
		 * @return void
		 */
		public function execute_upgrade( $mainwp_itsec_old_version ) {

		
			if ( $mainwp_itsec_old_version < 4041 ) {
				
				$current_options = null;
				global $mainwp_itsec_globals;
				if (isset($mainwp_itsec_globals['current_itheme_settings']['itsec_backup'])) {
					$current_options = $mainwp_itsec_globals['current_itheme_settings']['itsec_backup'];
				}
				
				// If there are no current options, go with the new defaults by not saving anything
				if ( is_array( $current_options ) ) {
					// Make sure the new module is properly activated or deactivated
					if ( $current_options['enabled'] ) {
						MainWP_ITSEC_Modules::activate( 'backup' );
					} else {
						MainWP_ITSEC_Modules::deactivate( 'backup' );
					}

					if ( isset( $current_options['location'] ) && ! is_dir( $current_options['location'] ) ) {
						unset( $current_options['location'] );
					}

					$options = MainWP_ITSEC_Modules::get_defaults( 'backup' );

					foreach ( $options as $name => $value ) {
						if ( isset( $current_options[$name] ) ) {
							$options[$name] = $current_options[$name];
						}
					}

					MainWP_ITSEC_Modules::set_settings( 'backup', $options );
					MainWP_IThemes_Security::unset_setting_field(array('itsec_backup') , $mainwp_itsec_globals['current_site_id']);
				}
			}

		}

	}

}

new MainWP_ITSEC_Backup_Setup();
