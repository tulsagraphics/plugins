<?php

if ( ! class_exists( 'MainWP_ITSEC_Pro_Setup' ) ) {

	class MainWP_ITSEC_Pro_Setup {

		public function __construct() {

			add_action( 'mainwp_itsec_modules_do_plugin_activation',   array( $this, 'execute_activate'   )          );
			add_action( 'mainwp_itsec_modules_do_plugin_deactivation', array( $this, 'execute_deactivate' )          );
			add_action( 'mainwp_itsec_modules_do_plugin_uninstall',    array( $this, 'execute_uninstall'  )          );
			add_action( 'mainwp_itsec_modules_do_plugin_upgrade',      array( $this, 'execute_upgrade'    ), null, 2 );

		}

		/**
		 * Execute module activation.
		 *
		 * @since 4.0
		 *
		 * @return void
		 */
		public function execute_activate() {
		}

		/**
		 * Execute module deactivation
		 *
		 * @return void
		 */
		public function execute_deactivate() {
		}

		/**
		 * Execute module uninstall
		 *
		 * @return void
		 */
		public function execute_uninstall() {
		}

		/**
		 * Execute module upgrade
		 *
		 * @return void
		 */
		public function execute_upgrade( $mainwp_itsec_old_version ) {
		}

	}

}

new MainWP_ITSEC_Pro_Setup();
