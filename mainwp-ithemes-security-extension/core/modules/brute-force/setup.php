<?php

if ( ! class_exists( 'MainWP_ITSEC_Brute_Force_Setup' ) ) {

	class MainWP_ITSEC_Brute_Force_Setup {

		private
			$defaults;

		public function __construct() {

			add_action( 'mainwp_itsec_modules_do_plugin_activation',   array( $this, 'execute_activate'   )          );
			add_action( 'mainwp_itsec_modules_do_plugin_deactivation', array( $this, 'execute_deactivate' )          );
			add_action( 'mainwp_itsec_modules_do_plugin_uninstall',    array( $this, 'execute_uninstall'  )          );
			add_action( 'mainwp_itsec_modules_do_plugin_upgrade',      array( $this, 'execute_upgrade'    ), null, 2 );

		}

		/**
		 * Execute module activation.
		 *
		 * @since 4.0
		 *
		 * @return void
		 */
		public function execute_activate() {
		}

		/**
		 * Execute module deactivation
		 *
		 * @return void
		 */
		public function execute_deactivate() {
		}

		/**
		 * Execute module uninstall
		 *
		 * @return void
		 */
		public function execute_uninstall() {

			$this->execute_deactivate();

			delete_site_option( 'itsec_brute_force' );

		}

		/**
		 * Execute module upgrade
		 *
		 * @return void
		 */
		public function execute_upgrade( $mainwp_itsec_old_version ) {

			if ( $mainwp_itsec_old_version < 4041 ) {				
				global $mainwp_itsec_globals;
				$current_options = null;				
				if (isset($mainwp_itsec_globals['current_itheme_settings']['itsec_brute_force'])) {
					$current_options = $mainwp_itsec_globals['current_itheme_settings']['itsec_brute_force'];
				}

				// If there are no current options, go with the new defaults by not saving anything
				if ( is_array( $current_options ) ) {
					// Make sure the new module is properly activated or deactivated
					if ( $current_options['enabled'] ) {
						MainWP_ITSEC_Modules::activate( 'brute-force' );
					} else {
						MainWP_ITSEC_Modules::deactivate( 'brute-force' );
					}

					// remove 'enabled' which isn't use in the new module
					unset( $current_options['enabled'] );
					MainWP_ITSEC_Modules::set_settings( 'brute-force', $current_options );
					MainWP_IThemes_Security::unset_setting_field(array('itsec_brute_force') , $mainwp_itsec_globals['current_site_id']);
				}
			}

		}

	}

}

new MainWP_ITSEC_Brute_Force_Setup();
