<?php

if ( ! class_exists( 'MainWP_ITSEC_File_Change_Setup' ) ) {

	class MainWP_ITSEC_File_Change_Setup {

		private
			$defaults;

		public function __construct() {

			add_action( 'mainwp_itsec_modules_do_plugin_activation',   array( $this, 'execute_activate'   )          );
			add_action( 'mainwp_itsec_modules_do_plugin_deactivation', array( $this, 'execute_deactivate' )          );
			add_action( 'mainwp_itsec_modules_do_plugin_uninstall',    array( $this, 'execute_uninstall'  )          );
			add_action( 'mainwp_itsec_modules_do_plugin_upgrade',      array( $this, 'execute_upgrade'    ), null, 2 );

		}

		/**
		 * Execute module activation.
		 *
		 * @since 4.0
		 *
		 * @return void
		 */
		public function execute_activate() {
		}

		/**
		 * Execute module deactivation
		 *
		 * @return void
		 */
		public function execute_deactivate() {

			wp_clear_scheduled_hook( 'itsec_file_check' );

		}

		/**
		 * Execute module uninstall
		 *
		 * @return void
		 */
		public function execute_uninstall() {

			$this->execute_deactivate();

			delete_site_option( 'itsec_file_change' );
			delete_site_option( 'itsec_local_file_list' );
			delete_site_option( 'itsec_local_file_list_0' );
			delete_site_option( 'itsec_local_file_list_1' );
			delete_site_option( 'itsec_local_file_list_2' );
			delete_site_option( 'itsec_local_file_list_3' );
			delete_site_option( 'itsec_local_file_list_4' );
			delete_site_option( 'itsec_local_file_list_5' );
			delete_site_option( 'itsec_local_file_list_6' );
			delete_site_option( 'itsec_file_change_warning' );

		}

		/**
		 * Execute module upgrade
		 *
		 * @return void
		 */
		public function execute_upgrade( $mainwp_itsec_old_version ) {

			if ( $mainwp_itsec_old_version < 4041 ) {				
				global $mainwp_itsec_globals;
				$current_options = null;				
				if (isset($mainwp_itsec_globals['current_itheme_settings']['itsec_file_change'])) {
					$current_options = $mainwp_itsec_globals['current_itheme_settings']['itsec_file_change'];
				}

				// If there are no current options, go with the new defaults by not saving anything
				if ( is_array( $current_options ) ) {
					// Make sure the new module is properly activated or deactivated
					if ( $current_options['enabled'] ) {
						MainWP_ITSEC_Modules::activate( 'file-change' );
					} else {
						MainWP_ITSEC_Modules::deactivate( 'file-change' );
					}

					// remove 'enabled' which isn't use in the new module
					unset( $current_options['enabled'] );

					// This used to be boolean. Attempt to migrate to new string, falling back to default
					if ( ! is_array( $current_options['method'] ) ) {
						$current_options['method'] = ( $current_options['method'] )? 'exclude' : 'include';
					} elseif ( ! in_array( $current_options['method'], array( 'include', 'exclude' ) ) ) {
						$current_options['method'] = 'exclude';
					}

					MainWP_ITSEC_Modules::set_settings( 'file-change', $current_options );
					MainWP_IThemes_Security::unset_setting_field(array('itsec_file_change') , $mainwp_itsec_globals['current_site_id']);
				}
			}

		}

	}

}

new MainWP_ITSEC_File_Change_Setup();
