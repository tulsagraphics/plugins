<?php

if ( ! class_exists( 'MainWP_ITSEC_SSL_Setup' ) ) {

	class MainWP_ITSEC_SSL_Setup {

		public function __construct() {

			add_action( 'mainwp_itsec_modules_do_plugin_activation',   array( $this, 'execute_activate'   )          );
			add_action( 'mainwp_itsec_modules_do_plugin_deactivation', array( $this, 'execute_deactivate' )          );
			add_action( 'mainwp_itsec_modules_do_plugin_uninstall',    array( $this, 'execute_uninstall'  )          );
			add_action( 'mainwp_itsec_modules_do_plugin_upgrade',      array( $this, 'execute_upgrade'    ), null, 2 );

		}

		/**
		 * Execute module activation.
		 *
		 * @since 4.0
		 *
		 * @return void
		 */
		public function execute_activate() {
		}

		/**
		 * Execute module deactivation
		 *
		 * @return void
		 */
		public function execute_deactivate() {}

		/**
		 * Execute module uninstall
		 *
		 * @return void
		 */
		public function execute_uninstall() {

			delete_site_option( 'itsec_ssl' );

			delete_metadata( 'post', null, 'itsec_enable_ssl', null, true );
			delete_metadata( 'post', null, 'bwps_enable_ssl', null, true );

		}

		/**
		 * Execute module upgrade
		 *
		 * @return void
		 */
		public function execute_upgrade( $mainwp_itsec_old_version ) {

			if ( $mainwp_itsec_old_version < 4041 ) {
				
				global $mainwp_itsec_globals;
				$current_options = null;				
				if (isset($mainwp_itsec_globals['current_itheme_settings']['itsec_ssl'])) {
					$current_options = $mainwp_itsec_globals['current_itheme_settings']['itsec_ssl'];
				}
				// If there are no current options, go with the new defaults by not saving anything
				if ( is_array( $current_options ) ) {
					// If anything in this module is being used activate it, otherwise deactivate it
					$activate = false;
					foreach ( $current_options as $on ) {
						if ( $on ) {
							$activate = true;
							break;
						}
					}
					if ( $activate ) {
						MainWP_ITSEC_Modules::activate( 'ssl' );
					} else {
						MainWP_ITSEC_Modules::deactivate( 'ssl' );
					}

					// remove 'enabled' which isn't used in the new module
					unset( $current_options['enabled'] );

					MainWP_ITSEC_Modules::set_settings( 'ssl', $current_options );
					MainWP_IThemes_Security::unset_setting_field(array('itsec_ssl') , $mainwp_itsec_globals['current_site_id']);
				}
			}

		}

	}

}

new MainWP_ITSEC_SSL_Setup();
