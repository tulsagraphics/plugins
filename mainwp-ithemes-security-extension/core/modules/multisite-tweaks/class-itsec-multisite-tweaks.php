<?php

final class MainWP_ITSEC_Multisite_Tweaks {
	function run() {
		if ( defined( 'WP_CLI' ) && WP_CLI ) {
			// Don't risk blocking anything with WP_CLI.
			return;
		}
		
		add_action( 'init', array( $this, 'init' ) );
	}
	
	public function init() {
		if ( MainWP_ITSEC_Core::is_iwp_call() ) {
			return;
		}
		
		if ( current_user_can( 'manage_options' ) ) {
			return;
		}
				
		$settings = MainWP_ITSEC_Modules::get_settings( 'multisite-tweaks' );
		
		
	}
}
