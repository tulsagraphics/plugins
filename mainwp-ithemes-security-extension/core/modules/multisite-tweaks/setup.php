<?php

if ( ! class_exists( 'MainWP_ITSEC_Multisite_Tweaks_Setup' ) ) {

	class MainWP_ITSEC_Multisite_Tweaks_Setup {

		private
			$defaults;

		public function __construct() {

			add_action( 'mainwp_itsec_modules_do_plugin_activation',   array( $this, 'execute_activate'   )          );
			add_action( 'mainwp_itsec_modules_do_plugin_deactivation', array( $this, 'execute_deactivate' )          );
			add_action( 'mainwp_itsec_modules_do_plugin_uninstall',    array( $this, 'execute_uninstall'  )          );
			add_action( 'mainwp_itsec_modules_do_plugin_upgrade',      array( $this, 'execute_upgrade'    ), null, 2 );

		}

		/**
		 * Execute module activation.
		 *
		 * @since 4.0
		 *
		 * @return void
		 */
		public function execute_activate() {
		}

		/**
		 * Execute module deactivation
		 *
		 * @return void
		 */
		public function execute_deactivate() {
		}

		/**
		 * Execute module uninstall
		 *
		 * @return void
		 */
		public function execute_uninstall() {

			$this->execute_deactivate();

		}

		/**
		 * Execute module upgrade
		 *
		 * @since 4.0
		 *
		 * @return void
		 */
		public function execute_upgrade( $mainwp_itsec_old_version ) {

			if ( $mainwp_itsec_old_version < 4041 ) {				
				global $mainwp_itsec_globals;
				$current_options = null;				
				if (isset($mainwp_itsec_globals['current_itheme_settings']['itsec_tweaks'])) {
					$current_options = $mainwp_itsec_globals['current_itheme_settings']['itsec_tweaks'];
				}

				// If there are no current options, go with the new defaults by not saving anything
				if ( is_array( $current_options ) ) {
					$new_module_settings = MainWP_ITSEC_Modules::get_settings( 'multisite-tweaks' );

					// Reduce to only settings in new module
					$current_options = array_intersect_key( $current_options, $new_module_settings );

					// Use new module settings as defaults for any missing settings
					$current_options = array_merge( $new_module_settings, $current_options );

					// If anything in this module is being used activate it, otherwise deactivate it
					$activate = false;
					foreach ( $current_options as $on ) {
						if ( $on ) {
							$activate = true;
							break;
						}
					}
					if ( $activate ) {
						MainWP_ITSEC_Modules::activate( 'multisite-tweaks' );
					} else {
						MainWP_ITSEC_Modules::deactivate( 'multisite-tweaks' );
					}

					MainWP_ITSEC_Modules::set_settings( 'multisite-tweaks', $current_options );
					
				}
			}

		}

	}

}

new MainWP_ITSEC_Multisite_Tweaks_Setup();
