<?php

final class MainWP_ITSEC_Multisite_Tweaks_Settings extends MainWP_ITSEC_Settings {
	public function get_id() {
		return 'multisite-tweaks';
	}
	
	public function get_defaults() {
		return array(
			'theme_updates'  => false,
			'plugin_updates' => false,
			'core_updates'   => false,
		);
	}
}

MainWP_ITSEC_Modules::register_settings( new MainWP_ITSEC_Multisite_Tweaks_Settings() );
