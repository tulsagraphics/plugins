<?php

class MainWP_ITSEC_Multisite_Tweaks_Validator extends MainWP_ITSEC_Validator {
	public function get_id() {
		return 'multisite-tweaks';
	}
	
	protected function preprocess_settings() {
		$this->sanitize_setting( 'bool', 'theme_updates', __( 'Theme Update Notifications', 'l10n-mainwp-ithemes-security-extension' ) );
		$this->sanitize_setting( 'bool', 'plugin_updates', __( 'Plugin Update Notifications', 'l10n-mainwp-ithemes-security-extension' ) );
		$this->sanitize_setting( 'bool', 'core_updates', __( 'Core Update Notifications', 'l10n-mainwp-ithemes-security-extension' ) );
	}
}

MainWP_ITSEC_Modules::register_validator( new MainWP_ITSEC_Multisite_Tweaks_Validator() );
