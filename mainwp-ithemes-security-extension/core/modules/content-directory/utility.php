<?php

final class MainWP_ITSEC_Content_Directory_Utility {
	public static function change_content_directory( $dir_name ) {
		
	}
	
	public static function get_wp_config_define_warning() {
		return __( 'Do not remove. Removing this line could break your site. Added by Security > Settings > Change Content Directory.', 'l10n-mainwp-ithemes-security-extension' );
	}
	
	public static function get_wp_config_define( $name, $value, $include_warning_comment = true ) {
		$name = str_replace( "'", "\\'", $name );
		$value = str_replace( "'", "\\'", $value );
		$line = "define( '$name', '$value' );";
		
		if ( $include_warning_comment ) {
			$line .= ' // ' . self::get_wp_config_define_warning();
		}
		
		return $line;
	}
	
	public static function get_wp_config_modification( $dir, $url, $include_warning_comment = true ) {
		$modification  = self::get_wp_config_define( 'WP_CONTENT_DIR', $dir, $include_warning_comment ) . "\n";
		$modification .= self::get_wp_config_define( 'WP_CONTENT_URL', $url, $include_warning_comment );
		
		return $modification;
	}
	
	public static function get_wp_config_define_expression( $include_warning_comment = true ) {
		$expression = self::get_wp_config_modification( 'WILDCARD', 'WILDCARD', $include_warning_comment );
		$expression = preg_quote( $expression, '|' );
		$expression = str_replace( ' ', '\s*', $expression );
		$expression = str_replace( 'WILDCARD', "[^']+", $expression );
		$expression = "|$expression|";
		
		if ( $include_warning_comment ) {
			$expression = str_replace( "\n", "\s*[\r\n]+\s*", $expression );
		} else {
			$expression = str_replace( "\n", "\s*", $expression );
		}
		
		return $expression;
	}
	
	public static function is_custom_directory() {
//		if ( isset( $GLOBALS['__itsec_content_directory_is_custom_directory'] ) ) {
//			return $GLOBALS['__itsec_content_directory_is_custom_directory'];
//		}
//		
//		if ( ABSPATH . 'wp-content' !== WP_CONTENT_DIR ) {
//			$GLOBALS['__itsec_content_directory_is_custom_directory'] = true;
//		} else if ( get_option( 'siteurl' ) . '/wp-content' !== WP_CONTENT_URL ) {
//			$GLOBALS['__itsec_content_directory_is_custom_directory'] = true;
//		} else {
//			$GLOBALS['__itsec_content_directory_is_custom_directory'] = false;
//		}
//		
//		return $GLOBALS['__itsec_content_directory_is_custom_directory'];
	}
	
	public static function is_modified_by_it_security() {
//		if ( isset( $GLOBALS['__itsec_content_directory_is_modified_by_it_security'] ) ) {
//			return $GLOBALS['__itsec_content_directory_is_modified_by_it_security'];
//		}
//		
//		$GLOBALS['__itsec_content_directory_is_modified_by_it_security'] = false;
//		
//		
//		if ( ! self::is_custom_directory() ) {
//			return false;
//		}
//		
//		
//		require_once( $GLOBALS['itsec_globals']['plugin_dir'] . 'core/lib/class-itsec-lib-config-file.php' );
//		
//		$wp_config_file = MainWP_ITSEC_Lib_Config_File::get_wp_config_file_path();
//		
//		if ( empty( $wp_config_file ) ) {
//			return false;
//		}
//		
//		require_once( $GLOBALS['itsec_globals']['plugin_dir'] . 'core/lib/class-itsec-lib-file.php' );
//		
//		$wp_config = MainWP_ITSEC_Lib_File::read( $wp_config_file );
//		
//		if ( is_wp_error( $wp_config ) ) {
//			return false;
//		}
//		
//		$define_expression = self::get_wp_config_define_expression();
//		
//		if ( ! preg_match( $define_expression, $wp_config ) ) {
//			return false;
//		}
//		
//		require_once( $GLOBALS['itsec_globals']['plugin_dir'] . 'core/lib/class-itsec-lib-utility.php' );
//		
//		$wp_config_without_comments = MainWP_ITSEC_Lib_Utility::strip_php_comments( $wp_config );
//		
//		if ( is_wp_error( $wp_config_without_comments ) ) {
//			return false;
//		}
//		
//		$define_expression_without_comment = self::get_wp_config_define_expression( false );
//		
//		if ( ! preg_match( $define_expression_without_comment, $wp_config_without_comments ) ) {
//			return false;
//		}
//		
//		
//		$GLOBALS['__itsec_content_directory_is_modified_by_it_security'] = true;
//		
//		return true;
	}
}
