<?php

final class MainWP_ITSEC_WordPress_Tweaks_Settings_Page extends MainWP_ITSEC_Module_Settings_Page {
	public function __construct() {
		$this->id = 'wordpress-tweaks';
		$this->title = __( 'WordPress Tweaks', 'l10n-mainwp-ithemes-security-extension' );
		$this->description = __( 'Advanced settings that improve security by changing default WordPress behavior.', 'l10n-mainwp-ithemes-security-extension' );
		$this->type = 'recommended';

		parent::__construct();
	}

	protected function render_description( $form ) {

?>
	<p><?php _e( 'These are advanced settings that may be utilized to further strengthen the security of your WordPress site.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
<?php

	}

	protected function render_settings( $form ) {
		$settings = $form->get_options();


		$xmlrpc_options = array(
			'2' => __( 'Disable XML-RPC (recommended)', 'l10n-mainwp-ithemes-security-extension' ),
			'1' => __( 'Disable Pingbacks', 'l10n-mainwp-ithemes-security-extension' ),
			'0' => __( 'Enable XML-RPC', 'l10n-mainwp-ithemes-security-extension' ),
		);

		$allow_xmlrpc_multiauth_options = array(
			false => __( 'Block (recommended)', 'l10n-mainwp-ithemes-security-extension' ),
			true  => __( 'Allow', 'l10n-mainwp-ithemes-security-extension' ),
		);

        $rest_api_options = array(
			'restrict-access' => esc_html__( 'Restricted Access (recommended)', 'l10n-mainwp-ithemes-security-extension' ),
			'default-access'  => esc_html__( 'Default Access', 'l10n-mainwp-ithemes-security-extension' ),
		);

        $valid_user_login_types = array(
			'both'     => esc_html__( 'Email Address and Username (default)', 'l10n-mainwp-ithemes-security-extension' ),
			'email'    => esc_html__( 'Email Address Only', 'l10n-mainwp-ithemes-security-extension' ),
			'username' => esc_html__( 'Username Only', 'l10n-mainwp-ithemes-security-extension' ),
		);


?>
	<p><?php _e( 'Note: These settings are listed as advanced because they block common forms of attacks but they can also block legitimate plugins and themes that rely on the same techniques. When activating the settings below, we recommend enabling them one by one to test that everything on your site is still working as expected.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
	<p><?php _e( 'Remember, some of these settings might conflict with other plugins or themes, so test your site after enabling each setting.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
	<table class="form-table">
		<tr>
			<th scope="row"><label for="itsec-wordpress-tweaks-wlwmanifest_header"><?php _e( 'Windows Live Writer Header', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_checkbox( 'wlwmanifest_header' ); ?>
				<label for="itsec-wordpress-tweaks-wlwmanifest_header"><?php _e( 'Remove the Windows Live Writer header.', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'This is not needed if you do not use Windows Live Writer or other blogging clients that rely on this file.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-wordpress-tweaks-edituri_header"><?php _e( 'EditURI Header', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_checkbox( 'edituri_header' ); ?>
				<label for="itsec-wordpress-tweaks-edituri_header"><?php _e( 'Remove the RSD (Really Simple Discovery) header.', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'Removes the RSD (Really Simple Discovery) header. If you don\'t integrate your blog with external XML-RPC services such as Flickr then the "RSD" function is pretty much useless to you.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-wordpress-tweaks-comment_spam"><?php _e( 'Comment Spam', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_checkbox( 'comment_spam' ); ?>
				<label for="itsec-wordpress-tweaks-comment_spam"><?php _e( 'Reduce Comment Spam', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'This option will cut down on comment spam by denying comments from bots with no referrer or without a user-agent identified.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-wordpress-tweaks-file_editor"><?php _e( 'File Editor', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_checkbox( 'file_editor' ); ?>
				<label for="itsec-wordpress-tweaks-file_editor"><?php _e( 'Disable File Editor', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'Disables the file editor for plugins and themes requiring users to have access to the file system to modify files. Once activated you will need to manually edit theme and other files using a tool other than WordPress.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-wordpress-tweaks-disable_xmlrpc"><?php _e( 'XML-RPC', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<p><?php printf( __( 'WordPress\' XML-RPC feature allows external services to access and modify content on the site. Common example of services that make use of XML-RPC are <a href="%1$s">the Jetpack plugin</a>, <a href="%2$s">the WordPress mobile app</a>, and <a href="%3$s">pingbacks</a>. If the site does not use a service that requires XML-RPC, select the "Disable XML-RPC" setting as disabling XML-RPC prevents attackers from using the feature to attack the site.', 'l10n-mainwp-ithemes-security-extension' ), esc_url( 'https://jetpack.me/' ), esc_url( 'https://apps.wordpress.org/' ), esc_url( 'https://make.wordpress.org/support/user-manual/building-your-wordpress-community/trackbacks-and-pingbacks/#pingbacks' ) ); ?></p>
				<?php $form->add_select( 'disable_xmlrpc', $xmlrpc_options ); ?>
				<ul>
					<li><?php _e( '<strong>Disable XML-RPC</strong> - XML-RPC is disabled on the site. This setting is highly recommended if Jetpack, the WordPress mobile app, pingbacks, and other services that use XML-RPC are not used.', 'l10n-mainwp-ithemes-security-extension' ); ?></li>
					<li><?php _e( '<strong>Disable Pingbacks</strong> - Only disable pingbacks. Other XML-RPC features will work as normal. Select this setting if you require features such as Jetpack or the WordPress Mobile app.', 'l10n-mainwp-ithemes-security-extension' ); ?></li>
					<li><?php _e( '<strong>Enable XML-RPC</strong> - XML-RPC is fully enabled and will function as normal. Use this setting only if the site must have unrestricted use of XML-RPC.', 'l10n-mainwp-ithemes-security-extension' ); ?></li>
				</ul>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-wordpress-tweaks-allow_xmlrpc_multiauth"><?php _e( 'Multiple Authentication Attempts per XML-RPC Request', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<p><?php _e( 'WordPress\' XML-RPC feature allows hundreds of username and password guesses per request. Use the recommended "Block" setting below to prevent attackers from exploiting this feature.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
				<?php $form->add_select( 'allow_xmlrpc_multiauth', $allow_xmlrpc_multiauth_options ); ?>
				<ul>
					<li><?php _e( '<strong>Block</strong> - Blocks XML-RPC requests that contain multiple login attempts. This setting is highly recommended.', 'l10n-mainwp-ithemes-security-extension' ); ?></li>
					<li><?php _e( '<strong>Allow</strong> - Allows XML-RPC requests that contain multiple login attempts. Only use this setting if a service requires it.', 'l10n-mainwp-ithemes-security-extension' ); ?></li>
				</ul>
			</td>
		</tr>
                <tr>
			<th scope="row"><label for="itsec-wordpress-tweaks-rest_api"><?php esc_html_e( 'REST API', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<p><?php printf( wp_kses( __( 'The <a href="%1$s">WordPress REST API</a> is part of WordPress and provides developers with new ways to manage WordPress. By default, it could give public access to information that you believe is private on your site. For more details, see our post about the WordPress REST API <a href="%1$s">here</a>.', 'l10n-mainwp-ithemes-security-extension' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( 'https://ithemes.com/security/wordpress-rest-api-restrict-access' ) ); ?></p>
				<?php $form->add_select( 'rest_api', $rest_api_options ); ?>
				<ul>
					<li><?php echo wp_kses( __( '<strong>Restricted Access</strong> - Restrict access to most REST API data. This means that most requests will require a logged in user or a user with specific privileges, blocking public requests for potentially-private data. We recommend selecting this option.', 'l10n-mainwp-ithemes-security-extension' ), array( 'strong' => array() ) ); ?></li>
					<li><?php echo wp_kses( __( '<strong>Default Access</strong> - Access to REST API data is left as default. Information including published posts, user details, and media library entries is available for public access.', 'l10n-mainwp-ithemes-security-extension' ), array( 'strong' => array() ) ); ?></li>
				</ul>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-wordpress-tweaks-login_errors"><?php _e( 'Login Error Messages', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_checkbox( 'login_errors' ); ?>
				<label for="itsec-wordpress-tweaks-login_errors"><?php _e( 'Disable login error messages', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'Prevents error messages from being displayed to a user upon a failed login attempt.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-wordpress-tweaks-force_unique_nicename"><?php _e( 'Force Unique Nickname', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_checkbox( 'force_unique_nicename' ); ?>
				<label for="itsec-wordpress-tweaks-force_unique_nicename"><?php _e( 'Force users to choose a unique nickname', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'This forces users to choose a unique nickname when updating their profile or creating a new account which prevents bots and attackers from easily harvesting user\'s login usernames from the code on author pages. Note this does not automatically update existing users as it will affect author feed urls if used.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-wordpress-tweaks-disable_unused_author_pages"><?php _e( 'Disable Extra User Archives', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_checkbox( 'disable_unused_author_pages' ); ?>
				<label for="itsec-wordpress-tweaks-disable_unused_author_pages"><?php _e( 'Disables a user\'s author page if their post count is 0.', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'This makes it harder for bots to determine usernames by disabling post archives for users that don\'t post to your site.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
                <tr>
			<th scope="row"><label for="itsec-wordpress-tweaks-block_tabnapping"><?php esc_html_e( 'Protect Against Tabnapping', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_checkbox( 'block_tabnapping' ); ?>
				<label for="itsec-wordpress-tweaks-block_tabnapping"><?php esc_html_e( 'Alter target="_blank" links to protect against tabnapping', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php printf( wp_kses( __( 'Enabling this feature helps protect visitors to this site (including logged in users) from phishing attacks launched by a linked site. Details on tabnapping via target="_blank" links can be found in <a href="%s">this article</a>.', 'l10n-mainwp-ithemes-security-extension' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( 'https://www.jitbit.com/alexblog/256-targetblank---the-most-underestimated-vulnerability-ever/' ) ); ?></p>
			</td>
		</tr>
        <tr>
			<th scope="row"><label for="itsec-wordpress-tweaks-valid_user_login_type"><?php esc_html_e( 'Login with Email Address or Username', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<p><?php esc_html_e( 'By default, WordPress allows users to log in using either an email address or username. This setting allows you to restrict logins to only accept email addresses or usernames.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
				<?php $form->add_select( 'valid_user_login_type', $valid_user_login_types ); ?>
				<ul>
					<li><?php echo wp_kses( __( '<strong>Email Address and Username (Default)</strong> - Allow users to log in using their user\'s email address or username. This is the default WordPress behavior.', 'l10n-mainwp-ithemes-security-extension' ), array( 'strong' => array() ) ); ?></li>
					<li><?php echo wp_kses( __( '<strong>Email Address Only</strong> - Users can only log in using their user\'s email address. This disables logging in using a username.', 'l10n-mainwp-ithemes-security-extension' ), array( 'strong' => array() ) ); ?></li>
					<li><?php echo wp_kses( __( '<strong>Username Only</strong> - Users can only log in using their user\'s username. This disables logging in using an email address.', 'l10n-mainwp-ithemes-security-extension' ), array( 'strong' => array() ) ); ?></li>
				</ul>
			</td>
		</tr>
        <tr>
			<th scope="row"><label for="itsec-wordpress-tweaks-patch_thumb_file_traversal"><?php esc_html_e( 'Mitigate Attachment File Traversal Attack', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_checkbox( 'patch_thumb_file_traversal' ); ?>
				<label for="itsec-wordpress-tweaks-patch_thumb_file_traversal"><?php esc_html_e( 'Prevent attachment thumbnails from traversing to other files.', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php esc_html_e( 'Disabling this feature is not recommended. This helps mitigate an attack where users with the "author" role or higher could delete any file in your WordPress installation including sensitive files like wp-config.php.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
	</table>
<?php

	}
}

new MainWP_ITSEC_WordPress_Tweaks_Settings_Page();
