<?php

final class MainWP_ITSEC_WordPress_Tweaks {
	private static $instance = false;
	
	private $config_hooks_added = false;
	private $settings;
	private $first_xmlrpc_credentials;
	
	
	private function __construct() {
		$this->init();
	}
	
	public static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new self;
		}
		
		return self::$instance;
	}
	
	public static function activate() {
		$self = self::get_instance();
		
		$self->add_config_hooks();
		MainWP_ITSEC_Response::regenerate_server_config();
		MainWP_ITSEC_Response::regenerate_wp_config();
	}
	
	public static function deactivate() {
		$self = self::get_instance();
		
		$self->remove_config_hooks();
		MainWP_ITSEC_Response::regenerate_server_config();
		MainWP_ITSEC_Response::regenerate_wp_config();
	}
	
	public function add_config_hooks() {
		if ( $this->config_hooks_added ) {
			return;
		}
		
		add_filter( 'mainwp_itsec_filter_apache_server_config_modification', array( $this, 'filter_apache_server_config_modification' ) );
		add_filter( 'mainwp_itsec_filter_nginx_server_config_modification', array( $this, 'filter_nginx_server_config_modification' ) );
		add_filter( 'mainwp_itsec_filter_litespeed_server_config_modification', array( $this, 'filter_litespeed_server_config_modification' ) );
		add_filter( 'mainwp_itsec_filter_wp_config_modification', array( $this, 'filter_wp_config_modification' ) );
		
		$this->config_hooks_added = true;
	}
	
	public function remove_config_hooks() {
		remove_filter( 'mainwp_itsec_filter_apache_server_config_modification', array( $this, 'filter_apache_server_config_modification' ) );
		remove_filter( 'mainwp_itsec_filter_nginx_server_config_modification', array( $this, 'filter_nginx_server_config_modification' ) );
		remove_filter( 'mainwp_itsec_filter_litespeed_server_config_modification', array( $this, 'filter_litespeed_server_config_modification' ) );
		remove_filter( 'mainwp_itsec_filter_wp_config_modification', array( $this, 'filter_wp_config_modification' ) );
		
		$this->config_hooks_added = false;
	}
	
	public function init() {
		$this->add_config_hooks();


		if ( defined( 'WP_CLI' ) && WP_CLI ) {
			// Don't risk blocking anything with WP_CLI.
			return;
		}


		$this->settings = MainWP_ITSEC_Modules::get_settings( 'wordpress-tweaks' );

	}

	
	public function filter_wp_config_modification( $modification ) {
		require_once( dirname( __FILE__ ) . '/config-generators.php' );
		
		return MainWP_ITSEC_WordPress_Tweaks_Config_Generators::filter_wp_config_modification( $modification );
	}
	
	public function filter_apache_server_config_modification( $modification ) {
		require_once( dirname( __FILE__ ) . '/config-generators.php' );
		
		return MainWP_ITSEC_WordPress_Tweaks_Config_Generators::filter_apache_server_config_modification( $modification );
	}
	
	public function filter_nginx_server_config_modification( $modification ) {
		require_once( dirname( __FILE__ ) . '/config-generators.php' );
		
		return MainWP_ITSEC_WordPress_Tweaks_Config_Generators::filter_nginx_server_config_modification( $modification );
	}
	
	public function filter_litespeed_server_config_modification( $modification ) {
		require_once( dirname( __FILE__ ) . '/config-generators.php' );
		
		return MainWP_ITSEC_WordPress_Tweaks_Config_Generators::filter_litespeed_server_config_modification( $modification );
	}
}


MainWP_ITSEC_WordPress_Tweaks::get_instance();
