<?php

final class MainWP_ITSEC_WordPress_Tweaks_Config_Generators {
	public static function filter_wp_config_modification( $modification ) {
		$input = MainWP_ITSEC_Modules::get_settings( 'wordpress-tweaks' );
		
		if ( $input['file_editor'] ) {
			$modification .= "define( 'DISALLOW_FILE_EDIT', true ); // " . __( 'Disable File Editor - Security > Settings > WordPress Tweaks > File Editor', 'l10n-mainwp-ithemes-security-extension' ) . "\n";
		}
		
		return $modification;
	}
	
	public static function filter_litespeed_server_config_modification( $modification ) {
		return self::filter_apache_server_config_modification( $modification, 'litespeed' );
	}
	
	public static function filter_apache_server_config_modification( $modification, $server = 'apache' ) {
		
	}
	
	public static function filter_nginx_server_config_modification( $modification ) {
		$input = MainWP_ITSEC_Modules::get_settings( 'wordpress-tweaks' );
		
	}
	
	protected static function get_valid_referers( $server_type ) {
		$valid_referers = array();
		
		if ( 'apache' === $server_type ) {
			$domain = MainWP_ITSEC_Lib::get_domain( get_site_url() );
			
			if ( '*' == $domain ) {
				$valid_referers[] = $domain;
			} else {
				$valid_referers[] = "*.$domain";
			}
		} else if ( 'nginx' === $server_type ) {
			$valid_referers[] = 'server_names';
		} else {
			return array();
		}
		
		$valid_referers[] = 'jetpack.wordpress.com/jetpack-comment/';
		$valid_referers = apply_filters( 'itsec_filter_valid_comment_referers', $valid_referers, $server_type );
		
		if ( is_string( $valid_referers ) ) {
			$valid_referers = array( $valid_referers );
		} else if ( ! is_array( $valid_referers ) ) {
			$valid_referers = array();
		}
		
		foreach ( $valid_referers as $index => $referer ) {
			$valid_referers[$index] = preg_replace( '|^https?://|', '', $referer );
		}
		
		return $valid_referers;
	}
}
