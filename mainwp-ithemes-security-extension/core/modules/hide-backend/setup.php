<?php

if ( ! class_exists( 'MainWP_ITSEC_Hide_Backend_Setup' ) ) {

	class MainWP_ITSEC_Hide_Backend_Setup {

		private
			$defaults;

		public function __construct() {

			add_action( 'mainwp_itsec_modules_do_plugin_activation',   array( $this, 'execute_activate'   )          );
			add_action( 'mainwp_itsec_modules_do_plugin_deactivation', array( $this, 'execute_deactivate' )          );
			add_action( 'mainwp_itsec_modules_do_plugin_uninstall',    array( $this, 'execute_uninstall'  )          );
			add_action( 'mainwp_itsec_modules_do_plugin_upgrade',      array( $this, 'execute_upgrade'    ), null, 2 );

		}

		/**
		 * Execute module activation.
		 *
		 * @since 4.0
		 *
		 * @return void
		 */
		public function execute_activate() {
		}

		/**
		 * Execute module deactivation
		 *
		 * @return void
		 */
		public function execute_deactivate() {

			delete_site_transient( 'MainWP_ITSEC_SHOW_HIDE_BACKEND_TOOLTIP' );
			delete_site_option( 'itsec_hide_backend_new_slug' );

		}

		/**
		 * Execute module uninstall
		 *
		 * @return void
		 */
		public function execute_uninstall() {

			$this->execute_deactivate();

			delete_site_option( 'itsec_hide_backend' );

		}

		/**
		 * Execute module upgrade
		 *
		 * @return void
		 */
		public function execute_upgrade( $mainwp_itsec_old_version ) {

			if ( $mainwp_itsec_old_version < 4041 ) {				
				global $mainwp_itsec_globals;
				$current_options = null;				
				if (isset($mainwp_itsec_globals['current_itheme_settings']['itsec_hide_backend'])) {
					$current_options = $mainwp_itsec_globals['current_itheme_settings']['itsec_hide_backend'];
				}

				// If there are no current options, go with the new defaults by not saving anything
				if ( is_array( $current_options ) ) {
					// remove 'show-tooltip' which is old and not used in the new module
					unset( $current_options['show-tooltip'] );
					MainWP_ITSEC_Modules::set_settings( 'hide-backend', $current_options );
					MainWP_IThemes_Security::unset_setting_field(array('itsec_hide_backend') , $mainwp_itsec_globals['current_site_id']);
				}
			}

		}

		/**
		 * Flush rewrite rules.
		 *
		 * @since 4.0.6
		 *
		 * @return void
		 */
		public function flush_rewrite_rules() {

			flush_rewrite_rules();
		}

	}

}

new MainWP_ITSEC_Hide_Backend_Setup();
