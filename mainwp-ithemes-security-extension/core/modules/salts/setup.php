<?php

if ( ! class_exists( 'MainWP_ITSEC_Salts_Setup' ) ) {

	class MainWP_ITSEC_Salts_Setup {

		public function __construct() {

			add_action( 'mainwp_itsec_modules_do_plugin_activation',   array( $this, 'execute_activate'   )          );
			add_action( 'mainwp_itsec_modules_do_plugin_deactivation', array( $this, 'execute_deactivate' )          );
			add_action( 'mainwp_itsec_modules_do_plugin_uninstall',    array( $this, 'execute_uninstall'  )          );
			add_action( 'mainwp_itsec_modules_do_plugin_upgrade',      array( $this, 'execute_upgrade'    ), null, 2 );

		}

		/**
		 * Execute module activation.
		 *
		 * @since 4.7.0
		 *
		 * @return void
		 */
		public function execute_activate() {

		}

		/**
		 * Execute module deactivation
		 *
		 * @since 4.7.0
		 *
		 * @return void
		 */
		public function execute_deactivate() {

		}

		/**
		 * Execute module uninstall
		 *
		 * @since 4.7.0
		 *
		 * @return void
		 */
		public function execute_uninstall() {

			$this->execute_deactivate();

			delete_site_option( 'itsec_salts' );

		}

		/**
		 * Execute module upgrade
		 *
		 * @return void
		 */
		public function execute_upgrade( $mainwp_itsec_old_version ) {

			if ( $mainwp_itsec_old_version < 4041 ) {				
				global $mainwp_itsec_globals;
				$last_generated = null;				
				if (isset($mainwp_itsec_globals['current_itheme_settings']['itsec_salts'])) {
					$last_generated = $mainwp_itsec_globals['current_itheme_settings']['itsec_salts'];
				}

				if ( is_int( $last_generated ) && $last_generated >= 0 ) {
					MainWP_ITSEC_Modules::set_setting( 'wordpress-salts', 'last_generated', $last_generated );
					MainWP_IThemes_Security::unset_setting_field(array('itsec_salts') , $mainwp_itsec_globals['current_site_id']);
				}
			}
		}

	}

}

new MainWP_ITSEC_Salts_Setup();
