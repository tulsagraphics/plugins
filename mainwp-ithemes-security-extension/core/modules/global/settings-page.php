<?php

final class MainWP_ITSEC_Global_Settings_Page extends MainWP_ITSEC_Module_Settings_Page {
	private $version = 1;


	public function __construct() {
		$this->id = 'global';
		$this->title = __( 'Global Settings', 'l10n-mainwp-ithemes-security-extension' );
		$this->description = __( 'Configure basic settings that control how iThemes Security functions.', 'l10n-mainwp-ithemes-security-extension' );
		$this->type = 'recommended';

		parent::__construct();

		add_filter( 'admin_body_class', array( $this, 'filter_body_classes' ) );
	}

	public function filter_body_classes( $classes ) {
		if ( MainWP_ITSEC_Modules::get_setting( 'global', 'show_error_codes' ) ) {
			$classes .= ' itsec-show-error-codes';
		}

		if ( MainWP_ITSEC_Modules::get_setting( 'global', 'write_files' ) ) {
			$classes .= ' itsec-write-files-enabled';
		} else {
			$classes .= ' itsec-write-files-disabled';
		}

		$classes = trim( $classes );

		return $classes;
	}

	public function enqueue_scripts_and_styles() {
		global $mainwp_itheme_site_data_values;
		$vars = array(
			'ip'           => MainWP_ITSEC_Lib::get_ip(),
			'log_location' => is_array($mainwp_itheme_site_data_values) && isset($mainwp_itheme_site_data_values['default_log_location']) ? $mainwp_itheme_site_data_values['default_log_location'] : ''
		);

		wp_enqueue_script( 'mainwp-itsec-global-settings-page-script', plugins_url( 'js/settings-page.js', __FILE__ ), array( 'jquery' ), $this->version, true );
		wp_localize_script( 'mainwp-itsec-global-settings-page-script', 'itsec_global_settings_page', $vars );
	}

	public function handle_form_post( $data ) {
		$retval = MainWP_ITSEC_Modules::set_settings( $this->id, $data );

		if ( $retval['saved'] ) {
			if ( $retval['old_settings']['show_error_codes'] !== $retval['new_settings']['show_error_codes'] ) {
				MainWP_ITSEC_Response::add_js_function_call( 'mainwp_itsec_change_show_error_codes', array( (bool) $retval['new_settings']['show_error_codes'] ) );
			}

			if ( $retval['old_settings']['write_files'] !== $retval['new_settings']['write_files'] ) {
				MainWP_ITSEC_Response::add_js_function_call( 'mainwp_itsec_change_write_files', array( (bool) $retval['new_settings']['write_files'] ) );
			}
		}
	}

	protected function render_description( $form ) {

?>
	<p><?php _e( 'The following settings modify the behavior of many of the features offered by iThemes Security.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
<?php

	}

	protected function render_settings( $form ) {
		$validator = MainWP_ITSEC_Modules::get_validator( $this->id );

		$log_types = $validator->get_valid_log_types();

		$show_error_codes_options = array(
			false => __( 'No (default)' ),
			true  => __( 'Yes' ),
		);

		$is_individual = MainWP_IThemes_Security::is_manage_site();

?>
	<table class="form-table itsec-settings-section">
		<tr>
			<th scope="row"><label for="itsec-global-write_files"><?php _e( 'Write to Files', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_checkbox( 'write_files' ); ?>
				<label for="itsec-global-write_files"><?php _e( 'Allow iThemes Security to write to wp-config.php and .htaccess.', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'Whether or not iThemes Security should be allowed to write to wp-config.php and .htaccess automatically. If disabled you will need to manually place configuration options in those files.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-global-lockout_message"><?php _e( 'Host Lockout Message', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_textarea( 'lockout_message', array( 'class' => 'widefat' ) ); ?>
				<p class="description"><?php _e( 'The message to display when a computer (host) has been locked out.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
				<p class="description"><?php _e( 'You can use HTML in your message. Allowed tags include: a, br, em, strong, h1, h2, h3, h4, h5, h6, div.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-global-user_lockout_message"><?php _e( 'User Lockout Message', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_textarea( 'user_lockout_message', array( 'class' => 'widefat' ) ); ?>
				<p class="description"><?php _e( 'The message to display to a user when their account has been locked out.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
				<p class="description"><?php _e( 'You can use HTML in your message. Allowed tags include: a, br, em, strong, h1, h2, h3, h4, h5, h6, div.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-global-community_lockout_message"><?php _e( 'Community Lockout Message', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_textarea( 'community_lockout_message', array( 'class' => 'widefat' ) ); ?>
				<p class="description"><?php _e( 'The message to display to a user when their IP has been flagged as bad by the iThemes network.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
				<p class="description"><?php _e( 'You can use HTML in your message. Allowed tags include: a, br, em, strong, h1, h2, h3, h4, h5, h6, div.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-global-blacklist"><?php _e( 'Blacklist Repeat Offender', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_checkbox( 'blacklist' ); ?>
				<label for="itsec-global-blacklist"><?php _e( 'Enable Blacklist Repeat Offender', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'If this box is checked the IP address of the offending computer will be added to the "Ban Users" blacklist after reaching the number of lockouts listed below.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-global-blacklist_count"><?php _e( 'Blacklist Threshold', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_text( 'blacklist_count', array( 'class' => 'small-text' ) ); ?>
				<label for="itsec-global-blacklist_count"><?php _e( 'Lockouts', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'The number of lockouts per IP before the host is banned permanently from this site.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-global-blacklist_period"><?php _e( 'Blacklist Lookback Period', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_text( 'blacklist_period', array( 'class' => 'small-text' ) ); ?>
				<label for="itsec-global-blacklist_period"><?php _e( 'Days', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'How many days should a lockout be remembered to meet the blacklist count above.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-global-lockout_period"><?php _e( 'Lockout Period', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_text( 'lockout_period', array( 'class' => 'small-text' ) ); ?>
				<label for="itsec-global-lockout_period"><?php _e( 'Minutes', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'The length of time a host or user will be banned from this site after hitting the limit of bad logins.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-global-lockout_white_list"><?php _e( 'Lockout White List', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_textarea( 'lockout_white_list' ); ?>
				<?php
				$server_ip = "";
				if ( isset( $_SERVER['LOCAL_ADDR'] ) ) {
					$server_ip = $_SERVER['LOCAL_ADDR'];
				} elseif ( isset( $_SERVER['SERVER_ADDR'] ) ) {
					$server_ip = $_SERVER['SERVER_ADDR'];
				}
				?>
				<p><?php $form->add_button( 'add-to-whitelist', array( 'value' => __( 'Add my current IP to the White List', 'l10n-mainwp-ithemes-security-extension' ), 'class' => 'button-primary' ) ); ?>&nbsp;&nbsp;<a href="<?php echo PHP_EOL . $server_ip; ?>" class="itsec_add_dashboard_ip_to_whitelist button-primary"><?php echo __( 'Add Dashboard IP to Whitelist', 'l10n-mainwp-ithemes-security-extension' ); ?></a>

				</p>
				<p class="description"><?php _e( 'Use the guidelines below to enter hosts that will not be locked out from your site. This will keep you from locking yourself out of any features if you should trigger a lockout. Please note this does not override away mode and will only prevent a temporary ban. Should a permanent ban be triggered you will still be added to the "Ban Users" list unless the IP address is also white listed in that section.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
				<ul>
					<li>
						<?php _e( 'You may white list users by individual IP address or IP address range using wildcards or CIDR notation.', 'l10n-mainwp-ithemes-security-extension' ); ?>
						<ul>
							<li><?php _e( 'Individual IP addresses must be in IPv4 or IPv6 standard format (###.###.###.### or ####:####:####:####:####:####:####:####).', 'l10n-mainwp-ithemes-security-extension' ); ?></li>
							<li><?php _e( 'CIDR notation is allowed to specify a range of IP addresses (###.###.###.###/## or ####:####:####:####:####:####:####:####/###).', 'l10n-mainwp-ithemes-security-extension' ); ?></li>
							<li><?php _e( 'Wildcards are also supported with some limitations. If using wildcards (*), you must start with the right-most chunk in the IP address. For example ###.###.###.* and ###.###.*.* are permitted but ###.###.*.### is not. Wildcards are only for convenient entering of IP addresses, and will be automatically converted to their appropriate CIDR notation format on save.', 'l10n-mainwp-ithemes-security-extension' ); ?></li>
						</ul>
					</li>
					<li><?php _e( 'Enter only 1 IP address or 1 IP address range per line.', 'l10n-mainwp-ithemes-security-extension' ); ?></li>
				</ul>
				<p><a href="http://ip-lookup.net/domain-lookup.php" target="_blank"><?php _e( 'Lookup IP Address.', 'l10n-mainwp-ithemes-security-extension' ); ?></a></p>
				<p class="description"><strong><?php _e( 'This white list will prevent any IP listed from triggering an automatic lockout. You can still block the IP address manually in the banned users settings.', 'l10n-mainwp-ithemes-security-extension' ); ?></strong></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-global-log_type"><?php _e( 'Log Type', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_select( 'log_type', $log_types ); ?>
				<label for="itsec-global-log_type"><?php _e( 'How should even logs be kept', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'iThemes Security can log events in multiple ways, each with advantages and disadvantages. Database Only puts all events in the database with your posts and other WordPress data. This makes it easy to retrieve and process but can be slower if the database table gets very large. File Only is very fast but the plugin does not process the logs itself as that would take far more resources. For most users or smaller sites Database Only should be fine. If you have a very large site or a log processing software then File Only might be a better option.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-global-log_rotation"><?php _e( 'Days to Keep Database Logs', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_text( 'log_rotation', array( 'class' => 'small-text' ) ); ?>
				<label for="itsec-global-log_rotation"><?php _e( 'Days', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'The number of days database logs should be kept. File logs will be kept indefinitely but will be rotated once the file hits 10MB.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
        <tr>
			<th scope="row"><label for="itsec-global-file_log_rotation"><?php _e( 'Days to Keep File Logs', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_text( 'file_log_rotation', array( 'class' => 'small-text' ) ); ?>
				<label for="itsec-global-log_rotation"><?php _e( 'Days', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'The number of days file logs should be kept. File logs will additionally be rotated once the file hits 10MB. Set to 0 to only use log rotation.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-global-log_location"><?php _e( 'Path to Log Files', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php
				if ($is_individual) {
				?>
				<?php $form->add_text( 'log_location', array( 'class' => 'large-text code' ) ); ?>
				<p><label for="itsec-global-log_location"><?php _e( 'The path on your server where log files should be stored.', 'l10n-mainwp-ithemes-security-extension' ); ?></label></p>
				<p class="description"><?php _e( 'This path must be writable by your website. For added security, it is recommended you do not include it in your website root folder.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
				<p><?php $form->add_button( 'reset-log-location', array( 'value' => __( 'Restore Default Log File Path', 'l10n-mainwp-ithemes-security-extension' ), 'class' => 'button-secondary' ) ); ?></p>
				<?php
				} else {
				$form->add_checkbox( 'use_individual_log_location' );
				?>
				<label for="itsec-global-use_individual_log_location"><?php _e( 'Use value from individual site settings', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'Use "Path to Log Files" value from individual site settings. Uncheck will do not update the value.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
				<?php
				}
				?>
			</td>
		</tr>
		<?php if ( is_dir( WP_PLUGIN_DIR . '/iwp-client' ) ) : ?>
			<tr>
				<th scope="row"><label for="itsec-global-infinitewp_compatibility"><?php _e( 'Add InfiniteWP Compatibility', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
				<td>
					<?php $form->add_checkbox( 'infinitewp_compatibility' ); ?>
					<label for="itsec-global-infinitewp_compatibility"><?php _e( 'Enable InfiniteWP Compatibility', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
					<p class="description"><?php printf( __( 'Turning this feature on will enable compatibility with <a href="%s" target="_blank">InfiniteWP</a>. Do not turn it on unless you use the InfiniteWP service.', 'l10n-mainwp-ithemes-security-extension' ), 'http://infinitewp.com' ); ?></p>
				</td>
			</tr>
		<?php endif; ?>
		<tr>
			<th scope="row"><label for="itsec-global-allow_tracking"><?php _e( 'Allow Data Tracking', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_checkbox( 'allow_tracking' ); ?>
				<label for="itsec-global-allow_tracking"><?php _e( 'Allow iThemes to track plugin usage via anonymous data.', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
			</td>
		</tr>
		<?php if ( 'nginx' === MainWP_ITSEC_Lib::get_server() ) : ?>
			<tr>
				<th scope="row"><label for="itsec-global-nginx_file"><?php _e( 'NGINX Conf File', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
				<td>
					<?php $form->add_text( 'nginx_file', array( 'class' => 'large-text code' ) ); ?>
					<p><label for="itsec-global-nginx_file"><?php _e( 'The path on your server where the nginx config file is located.', 'l10n-mainwp-ithemes-security-extension' ); ?></label></p>
					<p class="description"><?php _e( 'This path must be writable by your website. For added security, it is recommended you do not include it in your website root folder.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
				</td>
			</tr>
		<?php endif; ?>

		<tr>
			<th scope="row"><label for="itsec-global-proxy_override"><?php _e( 'Override Proxy Detection', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_checkbox( 'proxy_override' ); ?>
				<label for="itsec-global-proxy_override"><?php _e( 'Disable Proxy IP Detection', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
				<p class="description"><?php _e( 'If you\'re not using a proxy service such as Varnish, Cloudflare or others turning this on may result in more accurate IP detection.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-global-hide_admin_bar"><?php _e( 'Hide Security Menu in Admin Bar', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_checkbox( 'hide_admin_bar' ); ?>
				<label for="itsec-global-hide_admin_bar"><?php _e( 'Hide security menu in admin bar.', 'l10n-mainwp-ithemes-security-extension' ); ?></label>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="itsec-global-show_error_codes"><?php _e( 'Show Error Codes', 'l10n-mainwp-ithemes-security-extension' ); ?></label></th>
			<td>
				<?php $form->add_select( 'show_error_codes', $show_error_codes_options ); ?>
				<p class="description"><?php _e( 'Each error message in iThemes Security has an associated error code that can help diagnose an issue. Changing this setting to "Yes" causes these codes to display. This setting should be left set to "No" unless iThemes Security support requests that you change it.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</td>
		</tr>
	</table>
<?php

	}
}

new MainWP_ITSEC_Global_Settings_Page();
