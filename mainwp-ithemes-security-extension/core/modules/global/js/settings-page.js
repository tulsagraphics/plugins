function mainwp_itsec_change_show_error_codes( args ) {
	var show = args[0];

	if ( show ) {
		jQuery( 'body' ).addClass( 'itsec-show-error-codes' );
	} else {
		jQuery( 'body' ).removeClass( 'itsec-show-error-codes' );
	}
}

function mainwp_itsec_change_write_files( args ) {
	var enabled = args[0];

	if ( enabled ) {
		jQuery( 'body' ).removeClass( 'itsec-write-files-disabled' ).addClass( 'itsec-write-files-enabled' );
	} else {
		jQuery( 'body' ).removeClass( 'itsec-write-files-enabled' ).addClass( 'itsec-write-files-disabled' );
	}
}

jQuery( document ).ready(function() {
	var $container = jQuery( '#wpcontent' );

	$container.on( 'click', '#itsec-global-add-to-whitelist', function( e ) {
		e.preventDefault();

		var whitelist = jQuery( '#itsec-global-lockout_white_list' ).val();
		whitelist = whitelist.trim();
		whitelist += "\n" + itsec_global_settings_page.ip;
		jQuery( '#itsec-global-lockout_white_list' ).val( whitelist );
	} );

	$container.on( 'click', '#itsec-global-reset-log-location', function( e ) {
		e.preventDefault();

		jQuery( '#itsec-global-log_location' ).val( itsec_global_settings_page.log_location );
	} );


        jQuery('#itsec-module-settings-save-all').on('click', function(event) {
            event.preventDefault();
            if ( mainwp_itsec_page.individualSite && mainwp_itsec_page.individualSite > 0) {
                mainwp_itheme_individual_save_all_settings(mainwp_itsec_page.ithemeSiteID);
            } else {
                var data = {
                    action:'mainwp_itheme_load_sites',
                    what: 'save_all_settings'
                };
                jQuery.post(ajaxurl, data, function (response) {
                    if (response) {
                        jQuery('#mainwp_itheme_screens_tab').html(response);
                        ithemes_bulkTotalThreads = jQuery('.siteItemProcess[status=queue]').length;
                        if (ithemes_bulkTotalThreads > 0)
                            mainwp_itheme_save_all_settings_start_next();
                    } else {
                        jQuery('#mainwp_itheme_screens_tab').html('<div class="mainwp_info-box-red">' + __("Undefined error.") + '</div>');

                    }
                })
            }
        });

        jQuery( '#mwp_itheme_btn_savegeneral' ).live('click', function () {
             if (!confirm('Are you sure?'))
                    return false;

            var statusEl = jQuery('#itsec_save_all_status');
            statusEl.css('color', '#21759B');
            statusEl.html('<i class="fa fa-spinner fa-pulse"></i> Running ...').show();
            var data = {
                        action: 'mainwp_itheme_save_all_settings',
                        ithemeSiteID: mainwp_itsec_page.ithemeSiteID,
                        saveGeneralSettings: 1,
                        nonce:  mainwp_itsec_page.ajax_nonce
                    };

            jQuery.post( ajaxurl, data, function ( response ) {
                    var message = '';
                    var error = false;
                    if ( response) {
                        if (response.error) {
                            error = true;
                            message = response.error;
                        } else if (response.result == 'success') {
                            message= 'Successful';
                        } else {
                            error = true;
                            message = __( 'Undefined error' );
                        }
                    }
                    else
                    {
                        error = true;
                        message = __( 'Undefined error' );
                    }

                    if ( error ) {
                        statusEl.css('color', 'red');
                    } else {
                        statusEl.css('color', '#21759B');
                    }
                    statusEl.html( message );
                    statusEl.fadeIn();
            }, 'json' );
            return false;
	});

});


mainwp_itheme_individual_save_all_settings = function(pSiteId) {
    var statusEl = jQuery('#itsec_save_all_status');
    statusEl.css('color', '#21759B');
    statusEl.html('<i class="fa fa-spinner fa-pulse"></i> Running ...').show();
    var data = {
                action: 'mainwp_itheme_save_all_settings',
                ithemeSiteID: pSiteId,
                individualSite: 1,
                nonce:  mainwp_itsec_page.ajax_nonce
            };
    //call the ajax
    jQuery.post( ajaxurl, data, function ( response ) {
            var message = '';
            var error = false;
            if ( response) {
                if (response.error) {
                    error = true;
                    message = response.error;
                } else if (response.result == 'success') {
                    message= 'Successful';
                } else {
                    error = true;
                    message = __( 'Undefined error' );
                }
            }
            else
            {
                error = true;
                message = __( 'Undefined error' );
            }

            if ( error ) {
                statusEl.css('color', 'red');
            } else {
                statusEl.css('color', '#21759B');
            }
            statusEl.html( message );
            statusEl.fadeIn();
    }, 'json' );
}

mainwp_itheme_save_all_settings_start_next = function() {
    while ((objProcess = jQuery('.siteItemProcess[status=queue]:first')) && (objProcess.length > 0) && (ithemes_bulkCurrentThreads < ithemes_bulkMaxThreads))
    {
        objProcess.attr('status', 'processed');
        mainwp_itheme_save_all_settings_start_specific(objProcess);
    }

    if (ithemes_bulkFinishedThreads > 0 && ithemes_bulkFinishedThreads == ithemes_bulkTotalThreads) {
        jQuery('#mainwp_itheme_screens_tab').append('<div class="mainwp_info-box">' + __("Finished.") + '</div><p><a class="button-primary" href="' + mainwp_itsec_page.settings_page_url + '">Return to Settings</a></p>');
    }

}

mainwp_itheme_save_all_settings_start_specific = function(objProcess) {
    var loadingEl = objProcess.find('i');
    var statusEl = objProcess.find('.status');
    statusEl.html('');
    ithemes_bulkCurrentThreads++;
    var data = {
                action: 'mainwp_itheme_save_all_settings',
                ithemeSiteID: objProcess.attr('site-id'),
                nonce:  mainwp_itsec_page.ajax_nonce
            };

    statusEl.html('');
    loadingEl.show();
    //call the ajax
    jQuery.post( ajaxurl, data, function ( response ) {
            loadingEl.hide();
            var message = '';
            var error = false;

            if ( response) {
                if (response.error) {
                    error = true;
                    message = response.error;
                } else if (response.result == 'success') {
                    message = 'Successful';
                } else {
                    error = true;
                    message = __( 'Undefined error' );
                }
            }
            else
            {
                error = true;
                message = __( 'Undefined error' );
            }

            if ( error ) {
                statusEl.css('color', 'red');
            } else {
                statusEl.css('color', '#21759B');
            }
            statusEl.html( message );
            statusEl.fadeIn();

            ithemes_bulkCurrentThreads--;
            ithemes_bulkFinishedThreads++;
            mainwp_itheme_save_all_settings_start_next();
    }, 'json' );
}



var itsec_log_type_changed = function() {
	var type = jQuery( '#itsec-global-log_type' ).val();

	if ( 'both' === type ) {
		jQuery( '#itsec-global-log_rotation' ).parents( 'tr' ).show();
		jQuery( '#itsec-global-file_log_rotation' ).parents( 'tr' ).show();
		jQuery( '#itsec-global-log_location' ).parents( 'tr' ).show();
	} else if ( 'file' === type ) {
		jQuery( '#itsec-global-log_rotation' ).parents( 'tr' ).hide();
		jQuery( '#itsec-global-file_log_rotation' ).parents( 'tr' ).show();
		jQuery( '#itsec-global-log_location' ).parents( 'tr' ).show();
	} else {
		jQuery( '#itsec-global-log_rotation' ).parents( 'tr' ).show();
		jQuery( '#itsec-global-file_log_rotation' ).parents( 'tr' ).hide();
		jQuery( '#itsec-global-log_location' ).parents( 'tr' ).hide();
	}
};

jQuery( document ).ready(function() {
	var $container = jQuery( '#wpcontent' );

	$container.on( 'click', '#itsec-global-reset-log-location', function( e ) {
		e.preventDefault();

		jQuery( '#itsec-global-log_location' ).val( itsec_global_settings_page.log_location );
	} );

	$container.on( 'change', '#itsec-global-log_type', itsec_log_type_changed );

	itsec_log_type_changed();
});

