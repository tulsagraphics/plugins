<?php


final class MainWP_ITSEC_Logs_Page {
	private $version = 1.2;

	private $self_url = '';
	private $modules = array();
	private $widgets = array();
	private $translations = array();
	private $logger_modules = array();
	private $logger_displays = array();
	
	
	public function __construct() {
		add_action( 'mainwp-itsec-logs-page-register-widget', array( $this, 'register_widget' ) );
		
		add_action( 'mainwp-itsec-page-show', array( $this, 'handle_page_load' ) );
		add_action( 'mainwp-itsec-page-ajax', array( $this, 'handle_ajax_request' ) );
		add_action( 'admin_print_scripts', array( $this, 'add_scripts' ) );
		add_action( 'admin_print_styles', array( $this, 'add_styles' ) );
		
		$this->set_translation_strings();
		
		
		$this->logger_modules = apply_filters( 'mainwp_itsec_logger_modules', array() );
		$this->logger_displays = apply_filters( 'mainwp_itsec_logger_displays', array() );
		
		
		require_once( dirname( __FILE__ ) . '/module-settings.php' );
		require_once( dirname( __FILE__ ) . '/sidebar-widget.php' );
	
		require_once( MainWP_ITSEC_Core::get_core_dir() . '/lib/form.php' );
		
		
		do_action( 'mainwp-itsec-logs-page-init' );
		do_action( 'mainwp-itsec-logs-page-register-widgets' );
	
		
		if ( ! empty( $_POST ) && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
			$this->handle_post();
		}
	}
	
	private function set_translation_strings() {
		$this->translations = array(
			'successful_save'   => __( 'Settings saved successfully for %1$s.', 'l10n-mainwp-ithemes-security-extension' ),
			
			'ajax_invalid'      => new WP_Error( 'itsec-settings-page-invalid-ajax-response', __( 'An "invalid format" error prevented the request from completing as expected. The format of data returned could not be recognized. This could be due to a plugin/theme conflict or a server configuration issue.', 'l10n-mainwp-ithemes-security-extension' ) ),
			
			'ajax_forbidden'    => new WP_Error( 'itsec-settings-page-forbidden-ajax-response: %1$s "%2$s"',  __( 'A "request forbidden" error prevented the request from completing as expected. The server returned a 403 status code, indicating that the server configuration is prohibiting this request. This could be due to a plugin/theme conflict or a server configuration issue. Please try refreshing the page and trying again. If the request continues to fail, you may have to alter plugin settings or server configuration that could account for this AJAX request being blocked.', 'l10n-mainwp-ithemes-security-extension' ) ),
			
			'ajax_not_found'    => new WP_Error( 'itsec-settings-page-not-found-ajax-response: %1$s "%2$s"', __( 'A "not found" error prevented the request from completing as expected. The server returned a 404 status code, indicating that the server was unable to find the requested admin-ajax.php file. This could be due to a plugin/theme conflict, a server configuration issue, or an incomplete WordPress installation. Please try refreshing the page and trying again. If the request continues to fail, you may have to alter plugin settings, alter server configurations, or reinstall WordPress.', 'l10n-mainwp-ithemes-security-extension' ) ),
			
			'ajax_server_error' => new WP_Error( 'itsec-settings-page-server-error-ajax-response: %1$s "%2$s"', __( 'A "internal server" error prevented the request from completing as expected. The server returned a 500 status code, indicating that the server was unable to complete the request due to a fatal PHP error or a server problem. This could be due to a plugin/theme conflict, a server configuration issue, a temporary hosting issue, or invalid custom PHP modifications. Please check your server\'s error logs for details about the source of the error and contact your hosting company for assistance if required.', 'l10n-mainwp-ithemes-security-extension' ) ),
			
			'ajax_unknown'      => new WP_Error( 'itsec-settings-page-ajax-error-unknown: %1$s "%2$s"', __( 'An unknown error prevented the request from completing as expected. This could be due to a plugin/theme conflict or a server configuration issue.', 'l10n-mainwp-ithemes-security-extension' ) ),
			
			'ajax_timeout'      => new WP_Error( 'itsec-settings-page-ajax-error-timeout: %1$s "%2$s"', __( 'A timeout error prevented the request from completing as expected. The site took too long to respond. This could be due to a plugin/theme conflict or a server configuration issue.', 'l10n-mainwp-ithemes-security-extension' ) ),
			
			'ajax_parsererror'  => new WP_Error( 'itsec-settings-page-ajax-error-parsererror: %1$s "%2$s"', __( 'A parser error prevented the request from completing as expected. The site sent a response that jQuery could not process. This could be due to a plugin/theme conflict or a server configuration issue.', 'l10n-mainwp-ithemes-security-extension' ) ),
		);
		
		foreach ( $this->translations as $key => $message ) {
			if ( is_wp_error( $message ) ) {
				$messages = MainWP_ITSEC_Response::get_error_strings( $message );
				$this->translations[$key] = $messages[0];
			}
		}
	}
	
	public function add_scripts() {
		foreach ( $this->modules as $id => $module ) {
			$module->enqueue_scripts_and_styles();
		}
		
		foreach ( $this->widgets as $id => $widget ) {
			$widget->enqueue_scripts_and_styles();
		}
		
		$vars = array(
			'ajax_action'   => 'mainwp_itsec_settings_page',
			'ajax_nonce'    => wp_create_nonce( 'mainwp-itsec-settings-nonce' ),
			'logs_page_url' => MainWP_ITSEC_Core::get_logs_page_url(),
			'settings_page_url' => MainWP_ITSEC_Core::get_settings_page_url(false),	
			'translations'  => $this->translations,
			'nonce' => wp_create_nonce( '_wpnonce' )
		);
		
		wp_enqueue_script( 'mainwp-itsec-settings-page-script', plugins_url( 'js/script.js', __FILE__ ), array( 'jquery-ui-dialog' ), $this->version, true );
		wp_localize_script( 'mainwp-itsec-settings-page-script', 'mainwp_itsec_page', $vars );
	}
	
	public function add_styles() {
		wp_enqueue_style( 'mainwp-itsec-settings-page-style', plugins_url( 'css/style.css', __FILE__ ), array(), $this->version );
		wp_enqueue_style( 'wp-jquery-ui-dialog' );
	}
	
	public function register_widget( $widget ) {
		if ( ! is_object( $widget ) || ! is_a( $widget, 'MainWP_ITSEC_Settings_Page_Sidebar_Widget' ) ) {
			trigger_error( 'An invalid widget was registered.', E_USER_ERROR );
			return;
		}
		
		if ( isset( $this->modules[$widget->id] ) ) {
			trigger_error( "A widget with the id of {$widget->id} is registered. Widget id's must be unique from any other module or widget." );
			return;
		}
		
		if ( isset( $this->widgets[$widget->id] ) ) {
			trigger_error( "A widget with the id of {$widget->id} is already registered. Widget id's must be unique from any other module or widget." );
			return;
		}
		
		
		$this->widgets[$widget->id] = $widget;
	}
	
		
	private function handle_post() {
	
	}
	
	public function handle_page_load( $self_url ) {
		$this->self_url = $self_url;
		
		$this->show_settings_page();
	}
	
	private function get_widget_settings( $id, $form = false, $echo = false ) {
		if ( ! isset( $this->widgets[$id] ) ) {
			$error = new WP_Error( 'itsec-settings-page-get-widget-settings-invalid-id', sprintf( __( 'The requested widget (%s) does not exist. Logs for it cannot be rendered.', 'l10n-mainwp-ithemes-security-extension' ), $id ) );
			
			if ( $echo ) {
				MainWP_ITSEC_Lib::show_error_message( $error );
			} else {
				return $error;
			}
		}
		
		if ( false === $form ) {
			$form = new MainWP_ITSEC_Form();
		}
		
		$widget = $this->widgets[$id];
		
		$form->add_input_group( $id );
		$form->set_defaults( $widget->get_defaults() );
		
		if ( ! $echo ) {
			ob_start();
		}
		
		$widget->render( $form );
		
		$form->remove_all_input_groups();
		
		if ( ! $echo ) {
			return ob_get_clean();
		}
	}
	
	private function show_settings_page() {
		require_once( MainWP_ITSEC_Core::get_core_dir() . '/lib/class-itsec-wp-list-table.php' );
		
		
		if ( isset( $_GET['filter'] ) ) {
			$filter = $_GET['filter'];
		} else {
			$filter = 'all';
		}
		
		
		$form = new MainWP_ITSEC_Form();
		
		
		$filters = array(
			'all' => __( 'All Log Data', 'l10n-mainwp-ithemes-security-extension' ),
		);
		
		foreach ( $this->logger_displays as $log_provider ) {
			$filters[$log_provider['module']] = $log_provider['title'];
		}
		
		
		$form->set_option( 'filter', $filter );
		
?>
	<div class="wrap">
		<div id="itsec-settings-messages-container">
			<?php
				foreach ( MainWP_ITSEC_Response::get_errors() as $error ) {
					MainWP_ITSEC_Lib::show_error_message( $error );
				}
				
				foreach ( MainWP_ITSEC_Response::get_messages() as $message ) {
					MainWP_ITSEC_Lib::show_status_message( $message );
				}
			?>
		</div>		
		<div id="poststuff">
			<div id="post-body" class="metabox-holder columns-2 hide-if-no-js">				
				<div id="postbox-container-2" class="postbox-container">
					<div class="postbox">
						<h3 class="mainwp_box_title"><span><?php _e('Security Log Data' , 'mainwp-ithemes-security-extension'); ?></span></h3>
						<div class="inside">
					<?php	if ( 'file' === MainWP_ITSEC_Modules::get_setting( 'global', 'log_type' ) ) : ?>
								<p><?php _e( 'To view logs within the plugin you must enable database logging in the Global Settings. File logging is not available for access within the plugin itself.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
					<?php	else :
								$this->all_logs_content(); 								
							endif; 
					?>							
						</div>
					</div>
					<div class="itsec-modal-background"></div>		
				</div>			
				<div id="postbox-container-1" class="postbox-container">
				<?php foreach ( $this->widgets as $id => $widget ) : ?>
					<?php $form->start_form( "itsec-sidebar-widget-form-$id" ); ?>
						<?php $form->add_nonce( 'itsec-logs-page' ); ?>
						<?php $form->add_hidden( 'widget-id', $id ); ?>
						<div id="itsec-sidebar-widget-<?php echo $id; ?>" class="postbox itsec-sidebar-widget">
							<h3 class="hndle ui-sortable-handle"><span><?php echo esc_html( $widget->title ); ?></span></h3>
							<div class="inside">
								<?php $this->get_widget_settings( $id, $form, true ); ?>
							</div>
						</div>
					<?php $form->end_form(); ?>
				<?php endforeach; ?>
			</div>
			</div>
			<div class="hide-if-js">
				<p class="itsec-warning-message"><?php _e( 'iThemes Security requires Javascript in order for the settings to be modified. Please enable Javascript to configure the settings.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
			</div>
		</div>
	</div>
<?php
		
	}

	/**
	 * Displays all logs content
	 *
	 * @since 4.3
	 *
	 * @return void
	 */
	public function all_logs_content() {
		global $mainwp_itsec_globals;				
		?>
		<div class="mainwp_info-box-yellow"><?php _e("This information is stored on the Child site since it would cause a strain to your network syncing this information to the Dashboard", "mainwp"); ?></div>		
		<?php		
		if (MainWP_IThemes_Security::is_manage_site()) {                       
			$location = "admin.php?page=itsec-logs";             
			$website_id =  isset($_GET['id']) ? $_GET['id'] : 0;
			if ($website_id)
				echo "View <a  href=\"admin.php?page=SiteOpen&newWindow=yes&websiteid=" . $website_id . "&location=" . base64_encode($location) . "\" target=\"_blank\">Security Log Data</a> on the Child Site.";
		}		
		?>		
		<form method="post" action="">
			<?php wp_nonce_field( 'itsec_clear_logs', 'wp_nonce' ); ?>
			<input type="hidden" name="itsec_clear_logs" value="clear_logs"/>
			<table class="form-table">
				<tr valign="top">
					<th scope="row" class="settinglabel">
						<?php _e( 'Log Summary', 'l10n-mainwp-ithemes-security-extension' ); ?>
					</th>
					<td class="settingfield">

						<p><?php _e( 'Use the button below to purge the log table in your database. Please note this will purge all log entries in the database including 404s.', 'l10n-mainwp-ithemes-security-extension' ); ?></p>
						<p class="submit" style="float:right">
							<span class="mwp_itheme_clear_logs_status" class="hidden"></span><span><i class="fa fa-spinner fa-pulse" style="display:none"></i>&nbsp;
							<a href="#" class="mwp_itheme_clear_logs_btn button-primary"><?php _e( 'Clear Logs', 'l10n-mainwp-ithemes-security-extension' ); ?></a>
						</p>
					</td>
				</tr>
			</table>
		</form>
	<?php

	}
}

new MainWP_ITSEC_Logs_Page();
