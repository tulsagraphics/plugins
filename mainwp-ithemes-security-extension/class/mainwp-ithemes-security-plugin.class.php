<?php
class MainWP_IThemes_Security_Plugin
{
	private $option_handle = 'mainwp_ithemes_plugin_option';
	private $option = array();

	private static $order = '';
	private static $orderby = '';

	//Singleton
	private static $instance = null;
	static function get_instance() {

		if ( null == MainWP_IThemes_Security_Plugin::$instance ) {
			MainWP_IThemes_Security_Plugin::$instance = new MainWP_IThemes_Security_Plugin();
		}
		return MainWP_IThemes_Security_Plugin::$instance;
	}

	public function __construct() {
		$this->option = get_option( $this->option_handle );
	}

	public function admin_init() {
		add_action( 'wp_ajax_mainwp_ithemes_upgrade_noti_dismiss', array( $this, 'ajax_dismiss_notice' ) );
		add_action( 'wp_ajax_mainwp_ithemes_active_plugin', array( $this, 'active_plugin' ) );
		add_action( 'wp_ajax_mainwp_ithemes_upgrade_plugin', array( $this, 'upgrade_plugin' ) );
		add_action( 'wp_ajax_mainwp_ithemes_showhide_plugin', array( $this, 'ajax_showhide_plugin' ) );
	}

	public function get_option( $key = null, $default = '' ) {
		if ( isset( $this->option[ $key ] ) ) {
			return $this->option[ $key ];
        }
		return $default;
	}

	public function set_option( $key, $value ) {
		$this->option[ $key ] = $value;
		return update_option( $this->option_handle, $this->option );
	}

	public static function gen_plugin_dashboard_tab( $websites ) {

		$orderby = 'name';
		$_order = 'desc';
		if ( isset( $_GET['ithemes_orderby'] ) && ! empty( $_GET['ithemes_orderby'] ) ) {
			$orderby = $_GET['ithemes_orderby'];
		}
		if ( isset( $_GET['ithemes_order'] ) && ! empty( $_GET['ithemes_order'] ) ) {
			$_order = $_GET['ithemes_order'];
		}

		$name_order = $version_order = $status_order = $last_scan_order = $time_order = $url_order = $hidden_order = $settings_order = '';

		if ( isset( $_GET['ithemes_orderby'] ) ) {
			if ( 'name' == $_GET['ithemes_orderby'] ) {
				$name_order = ('desc' == $_order) ? 'asc' : 'desc';
			} else if ( 'version' == $_GET['ithemes_orderby'] ) {
				$version_order = ('desc' == $_order) ? 'asc' : 'desc';
			} else if ( 'url' == $_GET['ithemes_orderby'] ) {
				$url_order = ('desc' == $_order) ? 'asc' : 'desc';
			} else if ( 'hidden' == $_GET['ithemes_orderby'] ) {
				$hidden_order = ('desc' == $_order) ? 'asc' : 'desc';
			} else if ( 'settings' == $_GET['ithemes_orderby'] ) {
				$settings_order = ('desc' == $_order) ? 'asc' : 'desc';
			}
		}

		self::$order = $_order;
		self::$orderby = $orderby;
		usort( $websites, array( 'MainWP_IThemes_Security_Plugin', 'ithemes_data_sort' ) );
		global $mainwp_itsec_globals;
	?>
       <table id="mainwp-table-plugins" class="wp-list-table widefat plugins" cellspacing="0" >
         <thead>
         <tr>
           <th class="check-column">
               <input type="checkbox"  id="cb-select-all-1" >
           </th>
           <th scope="col" class="manage-column sortable <?php echo $name_order; ?>">
               <a href="?page=<?php echo $mainwp_itsec_globals['mainwp_itheme_url']; ?>&ithemes_orderby=name&ithemes_order=<?php echo (empty( $name_order ) ? 'asc' : $name_order); ?>"><span><?php _e( 'Site','l10n-mainwp-ithemes-security-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
           <th scope="col" class="manage-column sortable <?php echo $url_order; ?>">
               <a href="?page=<?php echo $mainwp_itsec_globals['mainwp_itheme_url']; ?>&ithemes_orderby=url&ithemes_order=<?php echo (empty( $url_order ) ? 'asc' : $url_order); ?>"><span><?php _e( 'URL','l10n-mainwp-ithemes-security-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
           <th scope="col" class="manage-column sortable <?php echo $version_order; ?>">
               <a href="?page=<?php echo $mainwp_itsec_globals['mainwp_itheme_url']; ?>&ithemes_orderby=version&ithemes_order=<?php echo (empty( $version_order ) ? 'asc' : $version_order); ?>"><span><?php _e( 'Plugin Version','l10n-mainwp-ithemes-security-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
           <th scope="col" class="manage-column sortable <?php echo $hidden_order; ?>">
               <a href="?page=<?php echo $mainwp_itsec_globals['mainwp_itheme_url']; ?>&ithemes_orderby=hidden&ithemes_order=<?php echo (empty( $hidden_order ) ? 'asc' : $hidden_order); ?>"><span><?php _e( 'Plugin Hidden','l10n-mainwp-ithemes-security-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
		   <th scope="col" class="manage-column sortable <?php echo $settings_order; ?>">
               <a href="?page=<?php echo $mainwp_itsec_globals['mainwp_itheme_url']; ?>&ithemes_orderby=settings&ithemes_order=<?php echo (empty( $settings_order ) ? 'asc' : $settings_order); ?>"><span><?php _e( 'Settings in use','l10n-mainwp-ithemes-security-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
         </tr>
         </thead>
         <tfoot>
         <tr>
           <th class="check-column">
               <input type="checkbox"  id="cb-select-all-2" >
           </th>
           <th scope="col" class="manage-column sortable <?php echo $name_order; ?>">
               <a href="?page=<?php echo $mainwp_itsec_globals['mainwp_itheme_url']; ?>&ithemes_orderby=name&ithemes_order=<?php echo (empty( $name_order ) ? 'asc' : $name_order); ?>"><span><?php _e( 'Site','l10n-mainwp-ithemes-security-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
           <th scope="col" class="manage-column sortable <?php echo $url_order; ?>">
               <a href="?page=<?php echo $mainwp_itsec_globals['mainwp_itheme_url']; ?>&ithemes_orderby=url&ithemes_order=<?php echo (empty( $url_order ) ? 'asc' : $url_order); ?>"><span><?php _e( 'URL','l10n-mainwp-ithemes-security-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
           <th scope="col" class="manage-column sortable <?php echo $version_order; ?>">
               <a href="?page=<?php echo $mainwp_itsec_globals['mainwp_itheme_url']; ?>&ithemes_orderby=version&ithemes_order=<?php echo (empty( $version_order ) ? 'asc' : $version_order); ?>"><span><?php _e( 'Plugin Version','l10n-mainwp-ithemes-security-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
            <th scope="col" class="manage-column sortable <?php echo $hidden_order; ?>">
               <a href="?page=<?php echo $mainwp_itsec_globals['mainwp_itheme_url']; ?>&ithemes_orderby=hidden&ithemes_order=<?php echo (empty( $hidden_order ) ? 'asc' : $hidden_order); ?>"><span><?php _e( 'Plugin Hidden','l10n-mainwp-ithemes-security-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
		   <th scope="col" class="manage-column sortable <?php echo $settings_order; ?>">
               <a href="?page=<?php echo $mainwp_itsec_globals['mainwp_itheme_url']; ?>&ithemes_orderby=settings&ithemes_order=<?php echo (empty( $settings_order ) ? 'asc' : $settings_order); ?>"><span><?php _e( 'Settings in use','l10n-mainwp-ithemes-security-extension' ); ?></span><span class="sorting-indicator"></span></a>
           </th>
         </tr>
         </tfoot>
           <tbody id="the-mwp-ithemes-list" class="list:sites">
            <?php
			if ( is_array( $websites ) && count( $websites ) > 0 ) {
				self::get_plugin_dashboard_table_row( $websites );
			} else {
				_e( '<tr><td colspan="9">No websites were found with the iThemes Security plugin installed.</td></tr>' );
			}
			?>
           </tbody>
     </table>
	<?php
	}

	public static function get_plugin_dashboard_table_row( $websites ) {
		$dismiss = array();
		if ( session_id() == '' ) { session_start(); }
		if ( isset( $_SESSION['mainwp_ithemes_dismiss_upgrade_plugin_notis'] ) ) {
			$dismiss = $_SESSION['mainwp_ithemes_dismiss_upgrade_plugin_notis'];
		}

		if ( ! is_array( $dismiss ) ) {
			$dismiss = array(); }

		foreach ( $websites as $website ) {
			$location = 'admin.php?page=itsec';
			$website_id = $website['id'];
			$plugin_name = ! empty( $website['is_pro'] ) ? 'iThemes Security Pro' : 'iThemes Security';
			$cls_active = (isset( $website['ithemes_active'] ) && ! empty( $website['ithemes_active'] )) ? 'active' : 'inactive';
			$cls_update = (isset( $website['ithemes_upgrade'] )) ? 'update' : '';
			$cls_update = ('inactive' == $cls_active) ? 'update' : $cls_update;
			$showhide_action = (1 == $website['hide_ithemes']) ? 'show' : 'hide';
			$showhide_link = '<a href="#" class="mwp_ithemes_showhide_plugin" showhide="' . $showhide_action . '"><i class="fa fa-eye-slash"></i> '. ('show' === $showhide_action ? 'Show ' . $plugin_name : 'Hide ' . $plugin_name ) . '</a>';

			$td_status = '';

			?>
			<tr class="<?php echo $cls_active . ' ' . $cls_update; ?>" website-id="<?php echo $website_id; ?>" plugin-name="<?php echo $plugin_name; ?>">
               <th class="check-column">
                   <input type="checkbox"  name="checked[]">
               </th>
               <td>
                   <a href="admin.php?page=managesites&dashboard=<?php echo $website_id; ?>"><?php echo stripslashes( $website['name'] ); ?></a><br/>
                   <div class="row-actions"><span class="dashboard"><a href="admin.php?page=managesites&dashboard=<?php echo $website_id; ?>"><i class="fa fa-tachometer"></i> <?php _e( 'Overview' );?></a></span> |  <span class="edit"><a href="admin.php?page=managesites&id=<?php echo $website_id; ?>"><i class="fa fa-pencil-square-o"></i> <?php _e( 'Edit' );?></a> | <?php echo $showhide_link; ?></span></div>
                   <div class="its-action-working"><span class="status" style="display:none;"></span><span class="loading" style="display:none;"><i class="fa fa-spinner fa-pulse" ></i> <?php _e( 'Please wait...' ); ?></span></div>
               </td>
               <td>
                   <a href="<?php echo $website['url']; ?>" target="_blank"><?php echo $website['url']; ?></a><br/>
                   <div class="row-actions"><span class="edit"><a target="_blank" href="admin.php?page=SiteOpen&newWindow=yes&websiteid=<?php echo $website_id; ?>"><i class="fa fa-external-link"></i> <?php _e( 'Open WP-Admin' );?></a></span> | <span class="edit"><a href="admin.php?page=SiteOpen&newWindow=yes&websiteid=<?php echo $website_id; ?>&location=<?php echo base64_encode( $location ); ?>" target="_blank"><i class="fa fa-cog"></i> <?php _e( 'Open iThemes Security' );?></a></span></div>
               </td>
               <td>
				<?php
				if ( isset( $website['ithemes_plugin_version'] ) ) {
					echo $website['ithemes_plugin_version']; } else {
					echo '&nbsp;'; }
				?>
				</td>
				<td>
                   <span class="ithemes_hidden_title"><?php
						echo (1 == $website['hide_ithemes']) ? __( 'Yes' ) : __( 'No' );
					?>
				 </span>
				<td>
                   <span ><?php
						echo (1 == $website['individual_in_use']) ? __( 'Individual' ) : __( 'General' );
					?>
				 </span>
				</td>
			</tr>
				<?php

				$active_link = $update_link = '';
				$version = '';
				$plugin_slug = $website['plugin_slug'];
				if ( isset( $website['ithemes_active'] ) && empty( $website['ithemes_active'] ) ) {
					$active_link = '<a href="#" class="mwp_ithemes_active_plugin" >Activate ' . $plugin_name . ' plugin</a>'; }

				if ( isset( $website['ithemes_upgrade'] ) ) {
					if ( isset( $website['ithemes_upgrade']['new_version'] ) ) {
						$version = $website['ithemes_upgrade']['new_version']; }
					$update_link = '<a href="#" class="mwp_ithemes_upgrade_plugin" >Update ' . $plugin_name . ' plugin</a>';
					if ( isset( $website['ithemes_upgrade']['plugin'] ) ) {
						$plugin_slug = $website['ithemes_upgrade']['plugin']; }
				}

				$hide_update = false;
				if ( isset( $dismiss[ $website_id ] ) ) {
					$hide_update = true;
				}

				if ( ! empty( $active_link ) || ! empty( $update_link ) ) {
					$location = 'plugins.php';
					$link_row = $active_link .  ' | ' . $update_link;
					$link_row = rtrim( $link_row, ' | ' );
					$link_row = ltrim( $link_row, ' | ' );
					?>
					<tr class="plugin-update-tr" <?php echo ($hide_update ? 'style="display: none"' : ''); ?>>
                    <td colspan="9" class="plugin-update">
                        <div class="ext-upgrade-noti update-message" plugin-slug="<?php echo $plugin_slug; ?>" website-id="<?php echo $website_id; ?>" version="<?php echo $version; ?>"><p>
                        <?php if ( ! $hide_update ) { ?>
                        <span style="float:right; margin-top:5px"><a href="#" class="ithemes_plugin_upgrade_noti_dismiss"><?php _e( 'Dismiss' ); ?></a></span>
                        <?php } ?>
                        <?php echo $link_row; ?>
                        <span class="mwp-ithemes-row-working"><span class="status"></span><i class="fa fa-spinner fa-pulse" style="display:none"></i></span>
                        </p></div>
                    </td>
				   </tr>
					<?php
				}
		}

	}

	public static function ithemes_data_sort( $a, $b ) {
		if ( 'version' == self::$orderby ) {
			$a = $a['ithemes_plugin_version'];
			$b = $b['ithemes_plugin_version'];
			$cmp = version_compare( $a, $b );
		} else if ( 'url' == self::$orderby ) {
			$a = $a['url'];
			$b = $b['url'];
			$cmp = strcmp( $a, $b );
		} else if ( 'hidden' == self::$orderby ) {
			$a = $a['hide_ithemes'];
			$b = $b['hide_ithemes'];
			$cmp = $a - $b;
		}  else if ( 'settings' == self::$orderby ) {
			$a = $a['individual_in_use'];
			$b = $b['individual_in_use'];
			$cmp = $a - $b;
		} else {
			$a = $a['name'];
			$b = $b['name'];
			$cmp = strcmp( $a, $b );
		}
		if ( 0 == $cmp ) {
			return 0; }

		if ( 'desc' == self::$order ) {
			return ($cmp > 0) ? -1 : 1; } else {
			return ($cmp > 0) ? 1 : -1; }
	}

	public function get_websites_with_the_plugin( $websites, $selected_group = 0 , $activated_only = false ) {
		$websites_itheme = array();

		$ithemeHide = $this->get_option( 'hide_the_plugin' );

		if ( ! is_array( $ithemeHide ) ) {
			$ithemeHide = array();
		}

		$in_use = MainWP_IThemes_Security_DB::get_instance()->get_settings_field_array();

		if ( is_array( $websites ) && count( $websites ) ) {
			if ( empty( $selected_group ) ) {
				foreach ( $websites as $website ) {
					if ( $website && $website->plugins != '' ) {
						$plugins = json_decode( $website->plugins, 1 );
						if ( is_array( $plugins ) && count( $plugins ) != 0 ) {
							foreach ( $plugins as $plugin ) {
								if ( ('better-wp-security/better-wp-security.php' == $plugin['slug']) || ('ithemes-security-pro/ithemes-security-pro.php' == $plugin['slug']) ) {
										$site = MainWP_IThemes_Security_Utility::map_site( $website, array( 'id', 'name', 'url' ) );
										$site['is_pro'] = ('ithemes-security-pro/ithemes-security-pro.php' == $plugin['slug']) ? 1 : 0;
                                                                                if ($activated_only && !$plugin['active'])
                                                                                    continue;
										if ( $plugin['active'] ) {
											$site['ithemes_active'] = 1;
										} else {
											$site['ithemes_active'] = 0;
										}
										// get upgrade info
										$site['ithemes_plugin_version'] = $plugin['version'];
										$site['plugin_slug'] = $plugin['slug'];
										$plugin_upgrades = json_decode( $website->plugin_upgrades, 1 );
										if ( is_array( $plugin_upgrades ) && count( $plugin_upgrades ) > 0 ) {
											if ( isset( $plugin_upgrades['better-wp-security/better-wp-security.php'] ) ) {
												$upgrade = $plugin_upgrades['better-wp-security/better-wp-security.php'];
												if ( isset( $upgrade['update'] ) ) {
													$site['ithemes_upgrade'] = $upgrade['update'];
												}
											} else if ( isset( $plugin_upgrades['ithemes-security-pro/ithemes-security-pro.php'] ) ) {
												$upgrade = $plugin_upgrades['ithemes-security-pro/ithemes-security-pro.php'];
												if ( isset( $upgrade['update'] ) ) {
													$site['ithemes_upgrade'] = $upgrade['update'];
												}
											}
										}

										$site['hide_ithemes'] = 0;
										if ( isset( $ithemeHide[ $website->id ] ) && $ithemeHide[ $website->id ] ) {
											$site['hide_ithemes'] = 1;
										}
										$site['individual_in_use'] = isset( $in_use[$website->id] ) ? $in_use[$website->id]  : 0 ;
										$websites_itheme[] = $site;
										break;
								}
							}
						}
					}
				}
			} else {
				global $mainWPIThemesSecurityExtensionActivator;

				$group_websites = apply_filters( 'mainwp-getdbsites', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), array(), array( $selected_group ) );
				$sites = array();
				foreach ( $group_websites as $site ) {
					$sites[] = $site->id;
				}
				foreach ( $websites as $website ) {
					if ( $website && $website->plugins != '' && in_array( $website->id, $sites ) ) {
						$plugins = json_decode( $website->plugins, 1 );
						if ( is_array( $plugins ) && count( $plugins ) != 0 ) {
							foreach ( $plugins as $plugin ) {
								if ( ('better-wp-security/better-wp-security.php' == $plugin['slug']) || ('ithemes-security-pro/ithemes-security-pro.php' == $plugin['slug']) ) {
                                                                        if ($activated_only && !$plugin['active'])
                                                                            continue;
									$site = MainWP_IThemes_Security_Utility::map_site( $website, array( 'id', 'name', 'url' ) );
									$site['is_pro'] = ('ithemes-security-pro/ithemes-security-pro.php' == $plugin['slug']) ? 1 : 0;
									if ( $plugin['active'] ) {
										$site['ithemes_active'] = 1; } else {
										$site['ithemes_active'] = 0; }
										$site['ithemes_plugin_version'] = $plugin['version'];
										// get upgrade info
										$plugin_upgrades = json_decode( $website->plugin_upgrades, 1 );
										if ( is_array( $plugin_upgrades ) && count( $plugin_upgrades ) > 0 ) {
											if ( isset( $plugin_upgrades['better-wp-security/better-wp-security.php'] ) ) {
												$upgrade = $plugin_upgrades['better-wp-security/better-wp-security.php'];
												if ( isset( $upgrade['update'] ) ) {
													$site['ithemes_upgrade'] = $upgrade['update'];
												}
											} else if ( isset( $plugin_upgrades['ithemes-security-pro/ithemes-security-pro.php'] ) ) {
												$upgrade = $plugin_upgrades['ithemes-security-pro/ithemes-security-pro.php'];
												if ( isset( $upgrade['update'] ) ) {
													$site['ithemes_upgrade'] = $upgrade['update'];
												}
											}
										}
										$site['hide_ithemes'] = 0;
										if ( isset( $ithemeHide[ $website->id ] ) && $ithemeHide[ $website->id ] ) {
											$site['hide_ithemes'] = 1;
										}
										$site['individual_in_use'] = isset( $in_use[$website->id] ) ? $in_use[$website->id]  : 0 ;
										$websites_itheme[] = $site;
										break;
								}
							}
						}
					}
				}
			}
		}

		// if search action
		$search_sites = array();
		if ( isset( $_GET['s'] ) && ! empty( $_GET['s'] ) ) {
			$find = trim( $_GET['s'] );
			foreach ( $websites_itheme as $website ) {
				if ( stripos( $website['name'], $find ) !== false || stripos( $website['url'], $find ) !== false ) {
					$search_sites[] = $website;
				}
			}
			$websites_itheme = $search_sites;
		}
		unset( $search_sites );

		return $websites_itheme;
	}

	public static function gen_select_sites( $websites, $selected_group ) {
		global $mainWPIThemesSecurityExtensionActivator, $mainwp_itsec_globals;
		//$websites = apply_filters('mainwp-getsites', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), null);
		$groups = apply_filters( 'mainwp-getgroups', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), null );
		$search = (isset( $_GET['s'] ) && ! empty( $_GET['s'] )) ? trim( $_GET['s'] ) : '';
		?>

        <div class="alignleft actions bulkactions">
            <select id="mwp_ithemes_plugin_action">
                <option selected="selected" value="-1"><?php _e( 'Bulk Actions' ); ?></option>
                <option value="activate-selected"><?php _e( 'Active' ); ?></option>
                <option value="update-selected"><?php _e( 'Update' ); ?></option>
                <option value="hide-selected"><?php _e( 'Hide' ); ?></option>
                <option value="show-selected"><?php _e( 'Show' ); ?></option>
            </select>
            <input type="button" value="<?php _e( 'Apply' ); ?>" class="button action" id="ithemes_plugin_doaction_btn" name="">
        </div>

        <div class="alignleft actions">
            <form action="" method="GET">
                <input type="hidden" name="page" value="<?php echo $mainwp_itsec_globals['mainwp_itheme_url']; ?>">
                <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"><?php _e( 'No search results.','l10n-mainwp-ithemes-security-extension' ); ?></span>
                <input type="text" class="mainwp_autocomplete ui-autocomplete-input" name="s" autocompletelist="sites" value="<?php echo stripslashes( $search ); ?>" autocomplete="off">
                <datalist id="sites">
                    <?php
					if ( is_array( $websites ) && count( $websites ) > 0 ) {
						foreach ( $websites as $website ) {
							echo '<option>' . stripslashes( $website['name'] ). '</option>';
						}
					}
					?>
                </datalist>
                <input type="submit" name="" class="button" value="Search Sites">
            </form>
        </div>
        <div class="alignleft actions">
            <form method="post" action="admin.php?page=<?php echo $mainwp_itsec_globals['mainwp_itheme_url']; ?>">
                <select name="mainwp_ithemes_plugin_groups_select">
                    <option value="0"><?php _e( 'Select a group' ); ?></option>
                    <?php
					if ( is_array( $groups ) && count( $groups ) > 0 ) {
						foreach ( $groups as $group ) {
							$_select = '';
							if ( $selected_group == $group['id'] ) {
								$_select = 'selected '; }
							echo '<option value="' . $group['id'] . '" ' . $_select . '>' . stripslashes( $group['name'] ) . '</option>';
						}
					}
					?>
                </select>&nbsp;&nbsp;
                <input class="button" type="submit" name="ithemes_plugin_btn_display" id="ithemes_plugin_btn_display" value="<?php _e( 'Display', 'l10n-mainwp-ithemes-security-extension' ); ?>">
            </form>
        </div>
        <?php
		return;
	}


	public function ajax_dismiss_notice() {
		$website_id = $_POST['ithemeSiteID'];
		$version = $_POST['new_version'];
		if ( $website_id ) {
            if ( session_id() == '' ) { session_start(); }

			$dismiss = $_SESSION['mainwp_ithemes_dismiss_upgrade_plugin_notis'];
			if ( is_array( $dismiss ) && count( $dismiss ) > 0 ) {
				$dismiss[ $website_id ] = 1;
			} else {
				$dismiss = array();
				$dismiss[ $website_id ] = 1;
			}
			$_SESSION['mainwp_ithemes_dismiss_upgrade_plugin_notis'] = $dismiss;
			die( 'updated' );
		}
		die( 'nochange' );
	}

	public function active_plugin() {
		$_POST['websiteId'] = $_POST['ithemeSiteID'];
		do_action( 'mainwp_activePlugin' );
		die();
	}

	public function upgrade_plugin() {
		$_POST['websiteId'] = $_POST['ithemeSiteID'];
		do_action( 'mainwp_upgradePluginTheme' );
		die();
	}

	public function ajax_showhide_plugin() {
		$siteid = isset( $_POST['ithemeSiteID'] ) ? $_POST['ithemeSiteID'] : null;
		$showhide = isset( $_POST['showhide'] ) ? $_POST['showhide'] : null;
		if ( null !== $siteid && null !== $showhide ) {
			global $mainWPIThemesSecurityExtensionActivator;
			$post_data = array(
                                'mwp_action' => 'set_showhide',
								'showhide' => $showhide,
							);
			$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );

			if ( is_array( $information ) && isset( $information['result'] ) && 'success' === $information['result'] ) {
				$hide_itheme = $this->get_option( 'hide_the_plugin' );
				if ( ! is_array( $hide_itheme ) ) {
					$hide_itheme = array();
                }
				$hide_itheme[ $siteid ] = ('hide' === $showhide) ? 1 : 0;
				$this->set_option( 'hide_the_plugin', $hide_itheme );
			}
			die( json_encode( $information ) );
		}
		die();
	}
}
