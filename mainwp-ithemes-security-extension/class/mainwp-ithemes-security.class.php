<?php
class MainWP_IThemes_Security
{
	public static $instance = null;
	
	private $translations = array();
	
	public static function get_instance() {

		if ( null === self::$instance ) { self::$instance = new self(); }
		return self::$instance;
	}

	public function __construct() {
		$this->set_translation_strings();
	}

	public function init() {

	}

	public function admin_init() {
		add_action( 'wp_ajax_mainwp_itheme_load_sites', array( $this, 'do_load_sites' ) );
		add_action( 'wp_ajax_mainwp_itheme_whitelist', array( $this, 'ajax_whitelist' ) );
		add_action( 'wp_ajax_mainwp_itheme_backups_database', array( $this, 'ajax_backups_database' ) );
        add_action( 'wp_ajax_mainwp_itheme_save_all_settings', array( $this, 'ajax_save_all_settings' ) );
        add_action( 'wp_ajax_mainwp_itheme_reload_exclude_tables', array( $this, 'ajax_reload_exclude_tables' ) );
		add_action( 'wp_ajax_mainwp_itheme_change_content_dir_name', array( $this, 'ajax_change_content_dir_name' ) );
		add_action( 'wp_ajax_mainwp_itheme_site_override_settings', array( $this, 'ajax_override_settings' ) );
		add_action( 'wp_ajax_mainwp_itheme_malware_scan', array( $this, 'ajax_malware_scan' ) );
		add_action( 'wp_ajax_mainwp_itheme_clear_all_logs', array( $this, 'ajax_clear_all_logs' ) );
		add_action( 'wp_ajax_mainwp_itheme_release_lockouts', array( $this, 'ajax_itheme_release_lockouts' ) );		
		add_filter( 'mainwp-sync-others-data', array( $this, 'sync_others_data' ), 10, 2 );		
        add_action( 'mainwp-site-synced', array( $this, 'synced_site' ), 10, 2 );
		add_action( 'mainwp_delete_site', array( &$this, 'delete_site_data' ), 10, 1 );
	}
	
	private function set_translation_strings() {
		$this->translations = array(
			'individual_settings_in_use'      => __( 'Not Updated - Individual site settings are in use.', 'l10n-mainwp-ithemes-security-extension' ),
			'need_to_override_settings'   => __( 'Update Failed: Override General Settings need to be set to Yes.', 'l10n-mainwp-ithemes-security-extension' )			
		);		
	}	
	
	public function sync_others_data( $data, $pWebsite = null ) {
		if ( ! is_array( $data ) ) {
			$data = array(); }
		$data['ithemeExtActivated'] = 'yes';
		return $data;
	}

    public function synced_site( $pWebsite, $information = array() ) {
		if ( is_array( $information ) && isset( $information['syncIThemeData'] ) ) {
			$data = $information['syncIThemeData'];            
			if ( is_array( $data ) ) {
				if ( isset( $data['users_and_roles'] ) ) {
                    $websiteId = $pWebsite->id;                     
                    MainWP_IThemes_Security_DB::get_instance()->update_site_status_fields_by( 'site_id', $websiteId, array('users_and_roles' => $data['users_and_roles']) );                    
				}
			}
			unset( $information['syncIThemeData'] );
		}
	}
    
	
	public function delete_site_data( $website ) {
		if ( $website ) {
			MainWP_IThemes_Security_DB::get_instance()->delete_setting( 'site_id', $website->id );
		}
	}

	public static function render() {

		$website = null;
		if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
			global $mainWPIThemesSecurityExtensionActivator;
			$option = array(
			'plugin_upgrades' => true,
							'plugins' => true,
			);
			$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), array( $_GET['id'] ), array(), $option );

			if ( is_array( $dbwebsites ) && ! empty( $dbwebsites ) ) {
				$website = current( $dbwebsites );
			}
		}

		if ( self::is_manage_site() ) {
			$error = '';
			if ( empty( $website ) ) {
				$error = __( 'Error: Site not found.', 'l10n-mainwp-ithemes-security-extension' );
			} else {
				$activated = false;
				if ( $website && $website->plugins != '' ) {
					$plugins = json_decode( $website->plugins, 1 );
					if ( is_array( $plugins ) && count( $plugins ) > 0 ) {
						foreach ( $plugins as $plugin ) {
							if ( ('better-wp-security/better-wp-security.php' == $plugin['slug']) || ('ithemes-security-pro/ithemes-security-pro.php' == $plugin['slug']) ) {
								if ( $plugin['active'] ) {
									$activated = true; }
								break;
							}
						}
					}
				}
				if ( ! $activated ) {
					$error = __( 'iThemes Security plugin is not installed or activated on the site.', 'l10n-mainwp-ithemes-security-extension' );
				}
			}

			if ( ! empty( $error ) ) {
				do_action( 'mainwp-pageheader-sites', 'iThemes' );
				echo '<div class="mainwp_info-box-red">' . $error . '</div>';
				do_action( 'mainwp-pagefooter-sites', 'iThemes' );
				return;
			}
		}
		
		self::render_tabs( $website );
	}

	public static function render_tabs( $website = null ) {
		$style_tab_dashboard = $style_tab_settings = $style_tab_advanced = $style_tab_backups = $style_tab_logs = $style_tab_help = ' style="display: none" ';

		if ( isset( $_GET['tab'] ) && ! empty( $_GET['tab'] ) ) {
			if ( 'settings' == $_GET['tab'] ) {
				$style_tab_settings = '';
			} else if ( 'logs' == $_GET['tab'] ) {
				$style_tab_logs = '';
			} else {
				$style_tab_dashboard = '';
			}
		} else {
			$style_tab_dashboard = '';
		}

		global $mainWPIThemesSecurityExtensionActivator, $mainwp_itsec_globals;

		if ( ! self::is_manage_site() ) {
			$websites = apply_filters( 'mainwp-getsites', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), null );
			$sites_ids = array();
			if ( is_array( $websites ) ) {
				foreach ( $websites as $site ) {
					$sites_ids[] = $site['id'];
				}
				unset( $websites );
			}

			$option = array(
						'plugin_upgrades' => true,
						'plugins' => true,
			);

			$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $sites_ids, array(), $option );
			//print_r($dbwebsites);
			$selected_group = 0;

			if ( isset( $_POST['mainwp_ithemes_plugin_groups_select'] ) ) {
				$selected_group = intval( $_POST['mainwp_ithemes_plugin_groups_select'] );
			}

			$dbwebsites_itheme = MainWP_IThemes_Security_Plugin::get_instance()->get_websites_with_the_plugin( $dbwebsites, $selected_group );
			unset( $dbwebsites );
		}

		if ( self::is_manage_site() ) {
			do_action( 'mainwp-pageheader-sites', 'iThemes' );
			$dashboard_link = '';
			if ( ! isset( $_GET['tab'] ) || empty( $_GET['tab'] ) ) {
				$_GET['tab'] = 'settings';
				$style_tab_settings = '';
			}
		} else {
			$dashboard_link = '<a id="mwp_ithemes_dashboard_tab_lnk" href="admin.php?page=' . $mainwp_itsec_globals['mainwp_itheme_url'] .'" class="mainwp_action left ' . (empty( $style_tab_dashboard ) ? 'mainwp_action_down selected' : '') . '">' .  __( 'iThemes Security Dashboard' ) . '</a>';
		}

		?>
            <span id="mainwp_itheme_managesites_site_id" site-id="<?php echo ! empty( $website ) ? $website->id : '' ?>"></span>
            <div class="mainwp-subnav-tabs">
				<div class="itheme_tabs_lnk"><?php echo $dashboard_link; ?><a href="admin.php?page=<?php echo $mainwp_itsec_globals['mainwp_itheme_url']; ?>&tab=settings" id="mwp_ithemes_setting_tab_lnk" class="mainwp_action <?php echo empty($dashboard_link) ? 'left' : 'mid'; ?> <?php  echo (empty( $style_tab_settings ) ? 'mainwp_action_down selected' : ''); ?>"><?php _e( 'Settings' ); ?></a><a href="admin.php?page=<?php echo $mainwp_itsec_globals['mainwp_itheme_url']; ?>&tab=logs" id="mwp_ithemes_log_tab_lnk" class="mainwp_action right <?php  echo (empty( $style_tab_logs ) ? 'mainwp_action_down selected' : ''); ?>"><?php _e( 'Logs' ); ?></a></div>													 						
        		<div style="clear: both"></div>  
        	</div>
            <div class="wrap" id="mainwp-ap-option">
            <div class="inside">                 
                <div id="mainwp_ithemes_settings">
                    <div class="mainwp_error error" id="mwpithemeerror_box"></div>
                    <div style="clear: both">
                        <?php if ( ! self::is_manage_site() ) { ?>
                        <div id="mwp_ithemes_dashboard_tab" <?php echo $style_tab_dashboard; ?>>                                                
                            <?php MainWP_IThemes_Security_Plugin::gen_select_sites( $dbwebsites_itheme, $selected_group ); ?>  
                                <input type="button" class="mainwp-upgrade-button button-primary button" value="<?php _e( 'Sync Data' ); ?>" id="dashboard_refresh" style="background-image: none!important; float:right; padding-left: .6em !important;">
                            <?php MainWP_IThemes_Security_Plugin::gen_plugin_dashboard_tab( $dbwebsites_itheme ); ?>                            
                        </div>   
                        <?php }  ?>
                        <?php if ( isset( $_GET['tab'] ) && ! empty( $_GET['tab'] ) ) {  ?>
                            <div id="mainwp_itheme_screens_tab"> 
                            <?php							
								if ( self::is_manage_site() ) {
									self::site_settings_box();
								}
								$self_url = MainWP_ITSEC_Core::get_settings_page_url();
								do_action( 'mainwp-itsec-page-show', $self_url );							
								?>                                
                            </div>
                        <?php } ?>                       
                    </div>
                <div style="clear: both"></div>  
                </div>
            </div>
        </div>              
    <?php

	if ( self::is_manage_site() ) {
		do_action( 'mainwp-pagefooter-sites', 'iThemes' ); }

	}


	public static function site_settings_box() {
		global $mainwp_itsec_globals;
		$site_id = isset( $_GET['id'] ) ? $_GET['id'] : 0;
		$override = 0;
		if ( $site_id ) {
			$site_itheme = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $site_id );
			if ( $site_itheme ) {
				$override = $site_itheme->override;
			}
		}
		
		?>  
			<div class="poststuff">
				<div class="wrap">				
						<div class="postbox" id="itheme_site_settings">
						<h3 class="mainwp_box_title"><span><?php _e('iTheme Site Settings', 'l10n-mainwp-ithemes-security-extension'); ?></span></h3>
						<div class="inside">
							<input type="hidden" name="mainwp_itheme_settings_site_id" value="<?php echo $site_id; ?>">
							<table class="form-table">              
								<tr valign="top">
									<th scope="row" class="settinglabel">
										<?php _e( 'Override General Settings','l10n-mainwp-ithemes-security-extension' ); ?> <?php do_action( 'mainwp_renderToolTip', __( 'Set to YES if you want to overwrite general iTheme settings.','l10n-mainwp-ithemes-security-extension' ) ); ?>
									</th>
									<td class="settingfield">
										<div class="mainwp-checkbox">
											  <input type="checkbox" id="mainwp_itheme_override_general_settings" name="mainwp_itheme_override_general_settings"  <?php echo (0 == $override ? '' : 'checked="checked"'); ?> value="1"/>
											  <label for="mainwp_itheme_override_general_settings"></label>
										</div>&nbsp;&nbsp;
										<i class="fa fa-spinner fa-pulse" style="display:none"></i>										      
										<span id="mwp_itheme_site_save_settings_status" class="hidden"></span>

									</td>
								</tr>  
							</table>
					  </div>
					</div>  
			</div>
		</div>
        <?php
		
	}

	public static function is_itheme_page( $tabs = array() ) {
		if ( isset( $_GET['page'] ) && ('Extensions-Mainwp-Ithemes-Security-Extension' == $_GET['page'] || 'ManageSitesiThemes' == $_GET['page']) ) {
			if ( 'ManageSitesiThemes' == $_GET['page'] ) {
				if ( ! isset( $_GET['tab'] ) || empty( $_GET['tab'] ) ) {
					$_GET['tab'] = 'settings';
				}
			}
			if ( empty( $tabs ) ) {
				return true;
			} else if ( is_array( $tabs ) && isset( $_GET['tab'] ) && in_array( $_GET['tab'], $tabs ) ) {
				return true;
			} else if ( isset( $_GET['tab'] ) && $_GET['tab'] == $tabs ) {
				return true;
			}
		}
		return false;
	}

	public static function is_manage_site() {
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX !== false && isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ) {
			return true;
		} else if ( isset( $_GET['page'] ) && ('ManageSitesiThemes' == $_GET['page']) ) {
			return true;
		}
		return false;
	}

	public static function get_manage_site_id() {
		$site_id = false;
		if ( self::is_manage_site() ) {
			if ( isset( $_POST['ithemeSiteID'] ) && ! empty( $_POST['ithemeSiteID'] ) ) {
				$site_id = $_POST['ithemeSiteID']; 				
			} else if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
				$site_id = $_GET['id']; 				
			} else if ( isset( $_POST['mainwp_itheme_site_id'] ) && ! empty( $_POST['mainwp_itheme_site_id'] ) ) {
					$site_id = $_POST['mainwp_itheme_site_id']; 					
			}
		}
		return $site_id;
	}

	public static function get_itheme_slug() {
		if ( isset( $_GET['page'] ) && ('ManageSitesiThemes' == $_GET['page'] || 'Extensions-Mainwp-Ithemes-Security-Extension' == $_GET['page']) ) {
			return $_GET['page'];
		}
		return '';
	}

	public static function get_itheme_url() {
		$slug = '';
		if ( isset( $_GET['page'] ) ) {
			if ( 'ManageSitesiThemes' == $_GET['page'] ) {
				$slug = $_GET['page'] . (isset( $_GET['id'] ) ? '&id=' . $_GET['id'] : '');
			} else if ( 'Extensions-Mainwp-Ithemes-Security-Extension' == $_GET['page'] ) {
				$slug = $_GET['page'];
			}
		}
		return $slug;
	}
	
	public static function get_itheme_settings( $site_id = false ) {
		// general settings
		if (empty($site_id))
			$itheme_settings = get_site_option( 'mainwp_itheme_generalSettings' );
		else {
			$itheme_settings = MainWP_IThemes_Security_DB::get_instance()->get_setting_fields_by('site_id', $site_id);							
		}
		if (!is_array($itheme_settings)) {
			$itheme_settings = array();
		}
		return $itheme_settings;
	}
	
	public static function update_general_settings( $settings ) {
		$curgen_settings = MainWP_IThemes_Security::get_itheme_settings();
		if ( ! is_array( $curgen_settings ) ) {
			$curgen_settings = array(); }

		foreach ( $settings as $key => $value ) {
			$curgen_settings[ $key ] = $value;
		}
		return update_site_option( 'mainwp_itheme_generalSettings', $curgen_settings );
	}

	public static function update_itheme_settings( $settings, $site_id = false ) {
		if ( $site_id ) {
			return MainWP_IThemes_Security_DB::get_instance()->update_setting_fields_by( 'site_id', $site_id, $settings );
		} else {
			return self::update_general_settings( $settings );
		}
		return false;
	}
		
	public static function update_build_number( $build, $site_id = false ) {
		if ( $site_id ) {
			return MainWP_IThemes_Security_DB::get_instance()->update_setting_fields_by( 'site_id', $site_id, array('build_number' => $build) );
		} else {
			return self::update_general_settings( array('build_number' => $build)  );
		}
		return false;
	}

	public static function unset_setting_field( $fields, $site_id = false ) {
		$itheme_settings = self::get_itheme_settings($site_id);
		$changed = false;
		foreach ($fields as $field) {
			if (isset($itheme_settings[$field])) {
				unset($itheme_settings[$field]);
				$changed = true;
			}
		}
		if ($changed) {
			self::update_itheme_settings($itheme_settings, $site_id);
		}
			
	}
	
	public function do_load_sites($return = false) {
		
		if ( isset($_POST['what']) ) {
			$what = $_POST['what'] ;
		} else if (isset( $_POST['data']['what'] )) {
			$what =  $_POST['data']['what'] ;
		}
		
		global $mainWPIThemesSecurityExtensionActivator;
		$websites = apply_filters( 'mainwp-getsites', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), null );
		$sites_ids = array();
		if ( is_array( $websites ) ) {
			foreach ( $websites as $website ) {
				$sites_ids[] = $website['id'];
			}
			unset( $websites );
		}
		$option = array(
			'plugin_upgrades' => true,
			'plugins' => true,
		);
		$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $sites_ids, array(), $option );
		$dbwebsites_itheme = MainWP_IThemes_Security_Plugin::get_instance()->get_websites_with_the_plugin( $dbwebsites, false, true ); //actived only
		unset( $dbwebsites );
		
                ob_start();		
		echo '<div class="wrap">';
		if ( ! is_array( $dbwebsites_itheme ) ||  count( $dbwebsites_itheme ) <= 0 ) {
			echo '<div class="mainwp_info-box-yellow">' . __( 'No websites were found with the iThemes Security plugin installed.', 'l10n-mainwp-ithemes-security-extension' ) . '</div>';			
		} else {	
			echo '<div class="poststuff">';				
				echo '<div class="postbox">';				
					echo '<h3 class="mainwp_box_title"><span><i class="fa fa-cog"></i> ';
					if ( 'backup_db' == $what ) {
						echo __( 'Backups database on child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;
					} else if ( 'change_admin_user' == $what ) {
						echo __( 'Changing Admin User on child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;
					} else if ( 'change_content_dir' == $what ) {
						echo __( 'Changing the Content Directory name on child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;
					} else if ( 'database_prefix' == $what ) {
						echo __( 'Changing the Database Prefix on child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;
					} else if ( 'whitelist' == $what ) {
						echo __( 'Temporarily Whitelist my IP on child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;
					} else if ( 'whitelist_release' == $what ) {
						echo __( 'Remove IP from Whitelist on child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;
					} else if ( 'reset_api_key' == $what  ) {
						echo __( 'Reset Brute Force Protection API Key on child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;
					} else if ( 'malware_scan' == $what ) {
						echo __( 'Scan Homepage for Malware on child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;
					} else if ( 'clearlogs' == $what ) {
						echo __( 'Clear Security Logs on child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;
					} else if ( 'save_settings' == $what ) {
						echo __( 'Saving Settings to child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;
					} else if ( 'one_time_check' == $what ) {
						echo __( 'Scan files change on child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;
					} else if ( 'secure-site' == $what ) {
						echo __( 'Secure child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;
					} else if ( 'update_module_status' == $what ) {
						echo __( 'Updating module status to child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;
					} else if ( 'save_all_settings' == $what ) {
						echo __( 'Saving all settings to child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;
					} else if ( 'reset-api-key' == $what ) {
						echo __( 'Reset Api Key on child sites ...', 'l10n-mainwp-ithemes-security-extension' ) ;						
						$defaults = MainWP_ITSEC_Modules::get_defaults( 'network-brute-force' );
						MainWP_ITSEC_Modules::set_settings( 'network-brute-force', $defaults );						
					} else {
						echo '&nbsp;';
					}
					echo '</span></h3>';
					echo '<div class="inside">';					
					foreach ( $dbwebsites_itheme as $website ) {
						echo '<div><strong>' . stripslashes( $website['name'] ) .'</strong>: ';
						echo '<div class="siteItemProcess" site-id="' . $website['id'] . '" status="queue"><span class="status">Queue ...</span> <i class="fa fa-spinner fa-pulse" style="display:none"></i></div>';
						echo '</div><br />';
					}
					echo '</div>';
				echo '</div>';
			echo '</div>';		
		}
		echo '</div>';
		$html = ob_get_clean();
		
		
		if ( $return )
			return $html;		
		else 
			die($html);		
	}

	function ajax_whitelist() {
		$siteid = $_POST['ithemeSiteID'];
		if ( empty( $siteid ) ) {
			die( json_encode( array( 'error' => 'Empty site id.' ) ) ); }

		$itheme_site = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $siteid );
		$individual_update = isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ? true : false;
		if ( ! $individual_update ) {
			if ( $itheme_site ) {
				$this->is_override_settings( $itheme_site->override );
			}
		}

		global $mainWPIThemesSecurityExtensionActivator;

		$ip = MainWP_ITSEC_Lib::get_ip();
		$post_data = array(
		'mwp_action' => 'whitelist',
							'ip' => $ip,
		);

		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );

		if ( is_array( $information ) && ! empty( $information['ip'] ) ) {
			$response = array(
				'ip'  => $information['ip'],
				'exp' => $information['exp'],
			);
			if ( $individual_update ) {
				$result = self::update_itheme_settings( array( 'itsec_temp_whitelist_ip' => $response ), $siteid );
			} else {
				self::update_itheme_settings( array( 'itsec_temp_whitelist_ip' => $response ) );
				if ( $itheme_site->override != 1 ) {
					self::update_itheme_settings( array( 'itsec_temp_whitelist_ip' => $response ), $siteid ); }
			}
		}

		die( json_encode( $information ) );
	}

	function ajax_backups_database() {
		if ( !isset($_POST['nonce']) || !wp_verify_nonce( $_POST['nonce'], 'mainwp-itsec-settings-nonce' ) ) {
			die('invalid request');
		}
		$siteid = $_POST['ithemeSiteID'];
		if ( empty( $siteid ) ) {
			die( json_encode( array( 'error' => 'Empty site id.' ) ) ); }

		$itheme_site = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $siteid );
		$individual_update = isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ? true : false;
		
		if ( $individual_update ) {
			if ( $itheme_site ) {
				if ( $itheme_site->override ) {
					$settings = unserialize( base64_decode( $itheme_site->settings ) );
				} else {
					die( json_encode( array( 'error' => $this->translations['need_to_override_settings'] ) ) ) ;
				}
			}
		} else {
			if ( $itheme_site ) {
				if (1 == $itheme_site->override) 
					die( json_encode( array( 'error' => $this->translations['individual_settings_in_use'] ) ) ) ;
			}
			$settings = MainWP_IThemes_Security::get_itheme_settings();
		}		

		global $mainWPIThemesSecurityExtensionActivator;
		$post_data = array( 'mwp_action' => 'backup_db' );
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );
		die( json_encode( $information ) );
	}

    
    function ajax_save_all_settings() {
		if ( !isset($_POST['nonce']) || !wp_verify_nonce( $_POST['nonce'], 'mainwp-itsec-settings-nonce' ) ) {
			die('Invalid request');
		}
		$siteid = $_POST['ithemeSiteID'];
		
        if ( empty( $siteid ) ) {
			die( json_encode( array( 'error' => 'Empty site id.' ) ) );             
        }
        
        $save_general = (isset($_POST['saveGeneralSettings']) && $_POST['saveGeneralSettings']) ? true : false;        
        $information = $this->save_settings_to_site($siteid, $save_general, '', true);	        
		
        $result = array();
		if (is_array($information)) {
			if (isset($information['result'])) {
				$result = array('result' => 'success');
			} else if ($information['error']) {
				$result = array('error' => $information['error']);				
			}			
		} 			
		die( json_encode( $result ) );	
	}
    
	function ajax_reload_exclude_tables() {
		if ( !isset($_POST['nonce']) || !wp_verify_nonce( $_POST['nonce'], 'mainwp-itsec-settings-nonce' ) ) {
			die('invalid request');
		}
		
		$siteid = $_POST['ithemeSiteID'];
		if ( empty( $siteid ) ) {
			die( json_encode( array( 'error' => 'Empty site id.' ) ) ); }

		global $mainWPIThemesSecurityExtensionActivator;
		$post_data = array( 'mwp_action' => 'reload_backup_exclude' );
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );
		
		if (is_array($information) && isset($information['exclude'])) {			
			$exlude = $information['exclude'];
			MainWP_IThemes_Security_DB::get_instance()->update_setting_module_fields_by( 'site_id', $siteid, 'backup', array('exclude' => $exlude) );			
			MainWP_IThemes_Security_DB::get_instance()->update_site_status_fields_by( 'site_id', $siteid, array('excludable_tables' => $information['excludable_tables']) );
			$html = '<select id="itsec-backup-exclude" name="backup[exclude][]" multiple="multiple" style="position: absolute; left: -9999px;">';
			if (is_array($information['excludable_tables'])) {
				if (!is_array($exlude))
					$exlude = array();
				foreach($information['excludable_tables'] as $short_name => $name) {
					$selected = in_array($short_name, $exlude) ? 'selected="selected"' : '';
					$html .= '<option value="' . $short_name . '" ' . $selected . '>' . $name . '</option>';
				}
			}
			$html .= '</select>';			
			$information['html'] = $html;
			unset($information['exclude']);
		}
		
		die( json_encode( $information ) );
	}

	function do_specical_modules_update( $module , $module_settings = array()) {		
		
		$siteid = $_POST['ithemeSiteID'];		
		$post_data = array();		
		if ( $module == 'admin-user' ) {
			$post_data['mwp_action'] = 'admin_user';
			$post_data['settings'] = $module_settings;			
		} else if ( $module == 'wordpress-salts' ) {
			$post_data['mwp_action'] = 'wordpress_salts';
		} else if ( $module == 'file-permissions' ) {
			$post_data['mwp_action'] = 'file_permissions';
		}
		
		global $mainWPIThemesSecurityExtensionActivator;
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );
		
		return  $information;		
	}
	
	public function update_module_status( $module ) {
		
		$siteid = $_POST['ithemeSiteID'];		
		if ( empty( $siteid ) ) {
			return array( 'error' => 'Empty site id.' ); 			
		}

		$data = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $siteid );
		$individual_update = isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ? true : false;
		
		if ( $individual_update ) {
			if ( $data ) {
				if ( !$data->override ) {					
					return array( 'error' => $this->translations['need_to_override_settings']);					
				}				
			}
			$settings = unserialize( base64_decode( $data->settings ) );
		} else {
			if ( $data ) {
				if ( $data->override )
					return array( 'error' => $this->translations['individual_settings_in_use']);								
			}			
			$settings = MainWP_IThemes_Security::get_itheme_settings(); 
		}
		
		if ( !is_array($settings) )		
			$settings = array();
		
		$field = 'itsec_active_modules';
		$active_modules = isset($settings[$field]) ? $settings[$field] : array();
				
		global $mainWPIThemesSecurityExtensionActivator;
		$post_data = array( 
							'mwp_action' => 'module_status',
							'active_modules' => $active_modules
						);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );
		
		return $information;
	}
	
	function ajax_change_content_dir_name() {
		$siteid = $_POST['ithemeSiteID'];
		if ( empty( $siteid ) ) {
			die( json_encode( array( 'error' => 'Empty site id.' ) ) ); }

		$itheme_site = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $siteid );
		$individual_update = isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ? true : false;
		if ( ! $individual_update ) {
			if ( $itheme_site ) {
				$this->is_override_settings( $itheme_site->override );
			}
		}

		global $mainWPIThemesSecurityExtensionActivator;
		$post_data = array( 'mwp_action' => 'content_dir' );

		if ( isset( $_POST['content_dir_name'] ) && ! empty( $_POST['content_dir_name'] ) ) {
			$post_data['name'] = sanitize_file_name( $_POST['content_dir_name'] ); }

		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );
		die( json_encode( $information ) );
	}

	function do_change_database_prefix() {
		$siteid = $_POST['ithemeSiteID'];
		if ( empty( $siteid ) ) {
			return array( 'error' => 'Empty site id.' );			
		}

		$data = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $siteid );
		$individual_update = isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ? true : false;
		
		if ( $individual_update ) {
			if ( $data ) {
				if ( !$data->override ) {					
					return array( 'error' => $this->translations['need_to_override_settings']);					
				}				
			} 			
		} else {
			if ( $data ) {
				if ( $data->override )
					return array( 'error' => $this->translations['individual_settings_in_use']);								
			}			
		}
		
		global $mainWPIThemesSecurityExtensionActivator;
		$post_data = array( 'mwp_action' => 'database_prefix',
							'change_prefix' => 'yes');

		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );
		
		return $information;
	}
		
	function do_save_settings($module = '') {		
		$siteid = $_POST['ithemeSiteID'];
		
		if ( empty( $siteid ) ) {
			return ( array( 'error' => 'Empty site id.' ) ); 			
		}
		
		return $this->save_settings_to_site($siteid, false, $module);			
	}

	function mainwp_apply_plugin_settings($siteid) {				
		$information = $this->save_settings_to_site($siteid, true);	
		$result = array();
		if (is_array($information)) {
			if (isset($information['result']) && ( 'success' == $information['result'] || 'noupdate' == $information['result'])) {
				$result = array('result' => 'success');
			} else if ($information['error']) {
				$result = array('error' => $information['error']);				
			} else if ($information['message']) {
				$result = array('message' => $information['message']);				
			} else {
				$result = array('result' => 'failed');
			}			
		} else {
			$result = array('result' => 'failed');
		}			
		die( json_encode( $result ) );		
	}
	
	
	public function save_settings_to_site($siteid, $forced_global_setting = false, $module = '', $save_active_modules = false) {		
		
		global $mainwp_itsec_globals;		
		$itheme_site = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $siteid );
		$individual_update = isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ? true : false;
		$general = false;
		
		$settings = array();
		if (!$forced_global_setting && $individual_update ) {
			if ( $itheme_site ) {
				if ( $itheme_site->override ) {
					$settings = unserialize( base64_decode( $itheme_site->settings ) );
				} else {
					return array( 'error' => $this->translations['need_to_override_settings'] ) ;
				}
			}
		} else {
			if ( !$forced_global_setting && $itheme_site ) {
				if (1 == $itheme_site->override) 
					return array( 'error' => $this->translations['individual_settings_in_use'] ) ;
			}
			$settings = MainWP_IThemes_Security::get_itheme_settings();		
			$general = true;
		}
		
		if ($general) {
			$individual_settings = array();
			if ( $itheme_site ) {				
				$individual_settings = unserialize( base64_decode( $itheme_site->settings ) );		
			}			
			if (isset($settings['global']['use_individual_log_location']) && !empty($settings['global']['use_individual_log_location'])) {								
				if (is_array($individual_settings) && isset($individual_settings['global']['log_location']) && !empty($individual_settings['global']['log_location'])) {
					$settings['global']['log_location'] = $individual_settings['global']['log_location'];
				}										
			}			                        
			if (isset($settings['backup']['use_individual_location']) && !empty($settings['backup']['use_individual_location'])) {
				if (is_array($individual_settings) && isset($individual_settings['backup']['location']) && !empty($individual_settings['backup']['location'])) {
					$settings['backup']['location'] = $individual_settings['backup']['location'];
				}				
			}                        
			if (isset($settings['backup']['use_individual_exclude']) && !empty($settings['backup']['use_individual_exclude'])) {
				if (is_array($individual_settings) && isset($individual_settings['backup']['exclude'])) {
					$settings['backup']['location'] = $individual_settings['backup']['exclude'];
				}								
			}
            if (isset($settings['global']['use_individual_log_location'])) unset($settings['global']['use_individual_log_location']);
            if (isset($settings['global']['use_individual_location'])) unset($settings['backup']['use_individual_location']);
            if (isset($settings['global']['use_individual_exclude'])) unset($settings['backup']['use_individual_exclude']);
            //if (isset($settings['notification-center'])) unset($settings['notification-center']);
		}

		$update_settings = array();
		if ( !empty( $module ) ) {
			if ( in_array( $module, array( 'admin-user', 'wordpress-salts', 'file-permissions') ) ) {
				$module_settings = array();
				if (isset($settings[$module])) {
					$module_settings = $settings[$module];
				}				
				return $this->do_specical_modules_update($module, $module_settings);
			} 
			else if ( in_array( $module, $mainwp_itsec_globals['itheme_module_settings'] ) ) {				
				$module_settings = isset( $settings[$module] ) ? $settings[$module] : array();
				$settings = array($module => $module_settings);
			}
		} 
        
        $update_settings = $settings;
		
		if ( ! is_array( $update_settings ) || empty( $update_settings ) ) {
			return array( 'error' => $general ? 'Error: Empty General Settings.' : 'Error: Empty Individual Settings.' );
		}
        
		if (!$save_active_modules && !$forced_global_setting) {
            if (isset($update_settings['itsec_active_modules'])) {
                unset($update_settings['itsec_active_modules']);
            }
        }
		
		global $mainWPIThemesSecurityExtensionActivator;
				
		$post_data = array(
			'mwp_action' => 'save_settings',
			'settings' => base64_encode( serialize( $update_settings ) ),
            'is_individual' => $general ? 0 : 1
		);
				
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );
		if ( is_array( $information ) ) {			
			if ( isset( $information['require_permalinks'] ) && !empty($information['require_permalinks']) ) {				
				MainWP_IThemes_Security_DB::get_instance()->update_setting_module_fields_by( 'site_id', $siteid, 'hide-backend', array('enabled' => 0) );											
			}
			
			if ( isset( $information['nbf_settings'] ) ) {				
				MainWP_IThemes_Security_DB::get_instance()->update_setting_module_fields_by( 'site_id', $siteid, 'network-brute-force', $information['nbf_settings'] );																			
				if ($individual_update) {
					MainWP_ITSEC_Response::reload_module( 'network-brute-force' );
				}
			}		
			
			if ( isset( $information['site_status'] ) ) {
				$siteStatus = $information['site_status'];
				if ( is_array( $siteStatus ) ) {					
//					if ( isset( $siteStatus['log_location'] ) ) {						
//						MainWP_IThemes_Security_DB::get_instance()->update_setting_module_fields_by( 'site_id', $siteid, 'global', array('log_location' => $siteStatus['log_location']) );
//						unset($siteStatus['log_location']);		
//					}
//					if ( isset( $siteStatus['location'] ) ) {						
//						MainWP_IThemes_Security_DB::get_instance()->update_setting_module_fields_by( 'site_id', $siteid, 'backup', array('location' => $siteStatus['location']) );
//						unset($siteStatus['location']);		
//					}		
					$update = array(
						'site_id' => $siteid,
						'site_status' => base64_encode( serialize( $siteStatus ) ),
					);
					MainWP_IThemes_Security_DB::get_instance()->update_setting( $update );
				}
			}	
			unset($information['site_status']);
		}				
		return $information;
	}
	
	function ajax_override_settings() {
		$websiteId = $_POST['ithemeSiteID'];
		if ( empty( $websiteId ) ) {
			die( json_encode( array( 'error' => 'Empty site id.' ) ) ); }

		global $mainWPIThemesSecurityExtensionActivator;

		$website = apply_filters( 'mainwp-getsites', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $websiteId );
		if ( $website && is_array( $website ) ) {
			$website = current( $website );
		}
		if ( ! $website ) {
			return; }

		$update = array(
			'site_id' => $website['id'],
			'override' => $_POST['override'],
		);

		MainWP_IThemes_Security_DB::get_instance()->update_setting( $update );
		die( json_encode( array( 'result' => 'success' ) ) );
	}

//	public function ajax_reset_api_key() {
//		$siteid = $_POST['ithemeSiteID'];
//
//		if ( empty( $siteid ) ) {
//			die( json_encode( array( 'error' => 'Empty site id.' ) ) ); }
//		global $mainwp_itsec_globals;
//
//		$itheme_site = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $siteid );
//		$individual_update = isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ? true : false;
//		if ( ! $individual_update ) {
//			if ( $itheme_site ) {
//				$this->is_override_settings( $itheme_site->override );
//			}
//		}
//		global $mainWPIThemesSecurityExtensionActivator;
//
//		$post_data = array( 'mwp_action' => 'reset_api_key' );
//		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );
//		if ( is_array( $information ) && 'success' == $information['result'] ) {
//			$settings = isset( $mainwp_itsec_globals['all_saved_settings']['itsec_ipcheck'] ) ? $mainwp_itsec_globals['all_saved_settings']['itsec_ipcheck'] : array();
//			$reseted = isset( $settings['api_key'] ) ? true : false;
//			unset( $settings['api_key'] );
//			unset( $settings['api_s'] );
//			unset( $settings['email'] );
//			unset( $settings['reset'] );
//
//			if ( $individual_update ) {
//				self::update_itheme_settings( array( 'itsec_ipcheck' => $settings ), $siteid );
//			} else {
//				if ( $reseted ) { // avoid update many times
//					self::update_itheme_settings( array( 'itsec_ipcheck' => $settings ) );
//				}
//				if ( $itheme_site->override != 1 ) {
//					self::update_itheme_settings( array( 'itsec_ipcheck' => $settings ), $siteid ); }
//			}
//		}
//		die( json_encode( $information ) );
//	}

	public function ajax_malware_scan() {
		$siteid = $_POST['ithemeSiteID'];
		if ( empty( $siteid ) ) {
			die( json_encode( array( 'error' => 'Empty site id.' ) ) ); }

		$itheme_site = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $siteid );
		$individual_update = isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ? true : false;
		
		if ($individual_update ) {
			if ( $itheme_site ) {
				if ( ! $itheme_site->override ) {					
					die( json_encode( array( 'error' => $this->translations['need_to_override_settings'] )));
				}
			}
		} else {
			if ( $itheme_site ) {
				if (1 == $itheme_site->override) 
					die( json_encode( array( 'error' => $this->translations['individual_settings_in_use'] ) ) ) ;
			}			
		}
		
		global $mainWPIThemesSecurityExtensionActivator;
		$post_data = array( 'mwp_action' => 'malware_scan' );
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );

		die( json_encode( $information ) );
	}

	public function ajax_malware_get_scan_results() {
		$siteid = $_POST['ithemeSiteID'];
		if ( empty( $siteid ) ) {
			die( json_encode( array( 'error' => 'Empty site id.' ) ) ); }

		global $mainWPIThemesSecurityExtensionActivator;

		$post_data = array( 'mwp_action' => 'malware_get_scan_results' );
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );

		die( json_encode( $information ) );
	}

	public function ajax_clear_all_logs() {
		$siteid = $_POST['ithemeSiteID'];
		if ( empty( $siteid ) ) {
			die( json_encode( array( 'error' => 'Empty site id.' ) ) ); }

		$itheme_site = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $siteid );
		$individual_update = isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ? true : false;
		if ( ! $individual_update ) {
			if ( $itheme_site ) {
				$this->is_override_settings( $itheme_site->override );
			}
		}

		global $mainWPIThemesSecurityExtensionActivator;

		$post_data = array( 'mwp_action' => 'clear_all_logs' );
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );

		die( json_encode( $information ) );
	}

	public function do_scan_file_change( $with_html = true ) {
		$siteid = $_POST['ithemeSiteID'];
		if ( empty( $siteid ) ) {
			return array( 'error' => 'Empty site id.' ) ;			
		}
		
		$itheme_site = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $siteid );
		$individual_update = isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ? true : false;		
		if ( $individual_update ) {
			if ( $itheme_site ) {
				if ( ! $itheme_site->override ) {					
					return array( 'error' => $this->translations['need_to_override_settings'] ) ;
				}
			}
		} else {
			if ( $itheme_site ) {
				if (1 == $itheme_site->override) {
					return array( 'error' => $this->translations['individual_settings_in_use'] )  ;								
				}			
			}
		}
		
		global $mainWPIThemesSecurityExtensionActivator;

		$post_data = array( 'mwp_action' => 'file_change' );
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );
		
		if (is_array($information) && isset($information['scan_result'])) {
			$result = $information['scan_result'];
			$message = '';	
			
			$location = "admin.php?page=itsec-logs";             
			$log_url = "admin.php?page=SiteOpen&newWindow=yes&websiteid=" . $siteid . "&location=" . base64_encode($location);			
			
			if ( false == $result ) {
				$message = __( 'No changes were detected.', 'l10n-mainwp-ithemes-security-extension' ) ;
				if ( $with_html ) {
					$message = '<div class="updated fade inline"><p><strong>'  . $message . '</strong></p></div>'; 
				}
			} else if ( true == $result ) {
				$message = sprintf ( __( 'Changes were detected. Please check the <a href="%s" target="_blank">logs page</a> for details.', 'l10n-mainwp-ithemes-security-extension' ), $log_url ) ;
				if ( $with_html ) {
					$message = '<div class="error inline"><p><strong>'  . $message . '</strong></p></div>'; 
				}
			} else if ( -1 == $result) {
				$message = sprintf ( __( 'A scan is already in progress. Please check the <a href="%s" target="_blank">logs page</a> at a later time for the results of the scan.', 'l10n-mainwp-ithemes-security-extension' ), $log_url ) ;			
				if ( $with_html ) {
					$message = '<div class="error inline"><p><strong>'  . $message . '</strong></p></div>'; 
				}
			} 
			$information['message'] = $message;
		}
		return $information;
	}

	public function do_security_site() {
		$siteid = $_POST['ithemeSiteID'];
		if ( empty( $siteid ) ) {
			return array( 'error' => 'Empty site id.' ) ;			
		}
		
		$itheme_site = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $siteid );
		$individual_update = isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ? true : false;		
		if ( $individual_update ) {
			if ( $itheme_site ) {
				if ( ! $itheme_site->override ) {					
					return array( 'error' => $this->translations['need_to_override_settings'] ) ;
				}
			}
		} else {
			if ( $itheme_site ) {
				if (1 == $itheme_site->override) {
					return array( 'error' => $this->translations['individual_settings_in_use'] )  ;								
				}			
			}
		}
		
		global $mainWPIThemesSecurityExtensionActivator;

		$post_data = array( 'mwp_action' => 'security_site' );
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );		
		return $information;
	}
	     
	public function do_activate_network_brute_force() {
		
		$siteid = $_POST['ithemeSiteID'];
		if ( empty( $siteid ) ) {
			return array( 'error' => 'Empty site id.' ) ;			
		}
		
		$itheme_site = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $siteid );
		$individual_update = isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ? true : false;		
		if ( $individual_update ) {
			if ( $itheme_site ) {
				if ( ! $itheme_site->override ) {					
					return array( 'error' => $this->translations['need_to_override_settings'] ) ;
				}
			}
		} else {
			if ( $itheme_site ) {
				if (1 == $itheme_site->override) {
					return array( 'error' => $this->translations['individual_settings_in_use'] )  ;								
				}			
			}
		}
		
		global $mainWPIThemesSecurityExtensionActivator;

		$post_data = array( 'mwp_action' => 'activate_network_brute_force',
							'data' => base64_encode( serialize( $_POST['data'] ) )
							);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );		
		
		if (is_array($information) && isset($information['result']) && $information['result'] == 'success') {
			if (isset($information['nbf_settings'])) {				
				MainWP_IThemes_Security_DB::get_instance()->update_setting_module_fields_by( 'site_id', $siteid, 'network-brute-force', $information['nbf_settings'] );			
				MainWP_ITSEC_Modules::activate( 'network-brute-force' );
				if ($individual_update)
					MainWP_ITSEC_Response::add_js_function_call( 'setModuleToActive', 'network-brute-force' );			
				unset($information['nbf_settings']);
			}
			if ($individual_update) {
				MainWP_ITSEC_Response::reload_module( 'network-brute-force' );				
			}
		}
		
		return $information;
	}
	
	
	public function do_reset_api_key() {
		$siteid = $_POST['ithemeSiteID'];
		if ( empty( $siteid ) ) {
			return array( 'error' => 'Empty site id.' ) ;			
		}
		
		$itheme_site = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $siteid );
		$individual_update = isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ? true : false;		
		if ( $individual_update ) {
			if ( $itheme_site ) {
				if ( ! $itheme_site->override ) {					
					return array( 'error' => $this->translations['need_to_override_settings'] ) ;
				}
			}
		} else {
			if ( $itheme_site ) {
				if (1 == $itheme_site->override) {
					return array( 'error' => $this->translations['individual_settings_in_use'] )  ;								
				}			
			}
		}		
		global $mainWPIThemesSecurityExtensionActivator;
		$post_data = array( 'mwp_action' => 'reset_api_key' );
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );		
		
		if (is_array($information) && isset($information['result']) && $information['result'] == 'success') {
			if (isset($information['nbf_settings'])) {
				MainWP_IThemes_Security_DB::get_instance()->update_setting_module_fields_by( 'site_id', $siteid, 'network-brute-force', $information['nbf_settings'] );			
			}
			if ($individual_update) {
				MainWP_ITSEC_Response::reload_module( 'network-brute-force' );
			}
		}
		
		return $information;
	}
		
	public function ajax_itheme_release_lockouts() {
		$siteid = $_POST['ithemeSiteID'];
		if ( empty( $siteid ) ) {
			die( json_encode( array( 'error' => 'Empty site id.' ) ) ); }

		$itheme_site = MainWP_IThemes_Security_DB::get_instance()->get_site_setting_by( 'site_id' , $siteid );
		$individual_update = isset( $_POST['individualSite'] ) && ! empty( $_POST['individualSite'] ) ? true : false;
		if ( ! $individual_update ) {
			if ( $itheme_site ) {
				$this->is_override_settings( $itheme_site->override );
			}
		}

		global $mainWPIThemesSecurityExtensionActivator;

		$post_data = array(
		'mwp_action' => 'release_lockout',
						'lockout_ids' => $_POST['lockout_ids'],
						);
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPIThemesSecurityExtensionActivator->get_child_file(), $mainWPIThemesSecurityExtensionActivator->get_child_key(), $siteid, 'ithemes', $post_data );

		if ( is_array( $information ) && isset( $information['result'] ) && 'success' == $information['result'] ) {
			if ( isset( $information['site_status'] ) ) {
				$siteStatus = $information['site_status'];
				if ( is_array( $siteStatus ) ) {
					$update = array(
						'site_id' => $siteid,
						'site_status' => base64_encode( serialize( $siteStatus ) ),
					);
					MainWP_IThemes_Security_DB::get_instance()->update_setting( $update );
				}
				unset( $information['site_status'] );
			}
		}

		die( json_encode( $information ) );
	}

	private function is_override_settings( $override, $return = false ) {
		$massage = __( 'Not Updated - Individual site settings are in use.', 'l10n-mainwp-ithemes-security-extension' );		
		if ( 1 == $override ) {
			die( json_encode( array( 'message' => $massage ) ) );
		}
	}	
}

