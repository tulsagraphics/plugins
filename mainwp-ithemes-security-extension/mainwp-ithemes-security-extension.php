<?php
/*
Plugin Name: MainWP iThemes Security Extension
Plugin URI: https://mainwp.com
Description: The iThemes Security Extension combines the power of your MainWP Dashboard with the popular iThemes Security Plugin. It allows you to manage iThemes Security plugin settings directly from your dashboard. Requires MainWP Dashboard plugin.
Version: 1.8
Author: MainWP
Author URI: https://mainwp.com
Documentation URI: https://mainwp.com/help/category/mainwp-extensions/ithemes-security/
*/

if ( ! defined( 'MAINWP_ITHEME_PLUGIN_FILE' ) ) {
	define( 'MAINWP_ITHEME_PLUGIN_FILE', __FILE__ );
}

class MainWP_IThemes_Security_Extension
{
	public static $plugin_url;
	public $plugin_slug;
	public $plugin_dir;

	public function __construct() {

		$this->plugin_dir = plugin_dir_path( __FILE__ );
		self::$plugin_url = plugin_dir_url( __FILE__ );
		$this->plugin_slug = plugin_basename( __FILE__ );

		add_filter( 'plugin_row_meta', array( &$this, 'plugin_row_meta' ), 10, 2 );		
		add_action( 'init', array( &$this, 'init' ) );
		add_action( 'in_admin_header', array( &$this, 'in_admin_head' ) ); // Adds Help Tab in admin header
		add_action( 'init', array( &$this, 'localization' ) );
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_filter( 'mainwp_managesites_column_url', array( &$this, 'managesites_column_url' ), 10, 2 );		
		add_filter( 'mainwp-getsubpages-sites', array( &$this, 'managesites_subpage' ), 10, 1 );
		add_filter( 'mainwp-sync-extensions-options', array( &$this, 'mainwp_sync_extensions_options' ), 10, 1 );		
		add_action( 'mainwp_applypluginsettings_mainwp-ithemes-security-extension', array( MainWP_IThemes_Security::get_instance(), 'mainwp_apply_plugin_settings' ) );
		
		require_once( dirname( __FILE__ ) .  '/core/class-itsec-core.php' );
		$itheme_core = MainWP_ITSEC_Core::get_instance();
		$itheme_core->init( __FILE__, __( 'iThemes Security', 'l10n-mainwp-ithemes-security-extension' ) );		
		
	}

	public function plugin_row_meta( $plugin_meta, $plugin_file ) {

		if ( $this->plugin_slug != $plugin_file ) { return $plugin_meta; }
		
		$slug = basename($plugin_file, ".php");
		$api_data = get_option( $slug. '_APIManAdder');		
		if (!is_array($api_data) || !isset($api_data['activated_key']) || $api_data['activated_key'] != 'Activated' || !isset($api_data['api_key']) || empty($api_data['api_key']) ) {
			return $plugin_meta;
		}
		
		$plugin_meta[] = '<a href="?do=checkUpgrade" title="Check for updates.">Check for updates now</a>';
		return $plugin_meta;
	}

	function managesites_subpage( $subPage ) {
		$subPage[] = array(
							'title' => __( 'iThemes Security','l10n-mainwp-ithemes-security-extension' ),
						   'slug' => 'iThemes',
						   'sitetab' => true,
						   'menu_hidden' => true,
						   'callback' => array( 'MainWP_IThemes_Security', 'render' ),
						   'on_load_callback' => array('MainWP_ITSEC_Admin_Page_Loader' , 'load')
						   );
		return $subPage;
	}

	public function managesites_column_url( $actions, $websiteid ) {
		$actions['itheme'] = sprintf( '<a href="admin.php?page=ManageSitesiThemes&id=%1$s">' . __( 'iThemes Security', 'l10n-mainwp-ithemes-security-extension' ) . '</a>', $websiteid );
		return $actions;
	}

	public function init() {

	}

	function in_admin_head() {
		if ( isset( $_GET['page'] ) && $_GET['page'] == 'Extensions-Mainwp-Ithemes-Security-Extension' ) {
			self::addHelpTabs(); // If page is the Extension then call this 'addHelpTabs' function
		}
	}
	/**
	 * This function add help tabs in header.
	 * @return void
	 */
	public static function addHelpTabs() {
		$screen = get_current_screen(); //This function returns an object that includes the screen's ID, base, post type, and taxonomy, among other data points.
		$i      = 1;
		$screen->add_help_tab( array(
			'id'      => 'mainwp_ithemes_helptabs_' . $i ++,
			'title'   => __( 'First Steps with Extensions', 'l10n-mainwp-ithemes-security-extension' ),
			'content' => self::getHelpContent( 1 ),
		) );
		$screen->add_help_tab( array(
			'id'      => 'mainwp_ithemes_helptabs_' . $i ++,
			'title'   => __( 'MainWP iThemes Security Extension', 'l10n-mainwp-ithemes-security-extension' ),
			'content' => self::getHelpContent( 2 ),
		) );
	}
	/**
	 * Get help tab content.
	 *
	 * @param int $tabId
	 *
	 * @return string|bool
	 */
	public static function getHelpContent( $tabId ) {
		ob_start();
		if ( 1 == $tabId ) {
			?>
			<h3><?php echo __( 'First Steps with Extensions', 'mainwp-article-uploader-extension' ); ?></h3>
			<p><?php echo __( 'If you are having issues with getting started with the MainWP extensions, please review following help documents', 'mainwp-article-uploader-extension' ); ?></p>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'What are the MainWP Extensions', 'mainwp-article-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/order-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Order Extension(s)', 'mainwp-article-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/my-downloads-and-api-keys/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'My Downloads and API Keys', 'mainwp-article-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/install-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Install Extension(s)', 'mainwp-article-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/activate-extensions-api/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Activate Extension(s) API', 'mainwp-article-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/updating-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Updating Extension(s)', 'mainwp-article-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/what-are-mainwp-extensions/remove-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Remove Extension(s)', 'mainwp-article-uploader-extension' ); ?></a><br/><br/>
			<a href="https://mainwp.com/help/category/mainwp-extensions/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Help Documenation for all MainWP Extensions', 'mainwp-article-uploader-extension' ); ?></a>
		<?php } else if ( 2 == $tabId ) { ?>
			<h3><?php echo __( 'MainWP iThemes Security Extension', 'mainwp-article-uploader-extension' ); ?></h3>
			<a href="https://mainwp.com/help/docs/ithemes-security-extension/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'iThemes Security Extension', 'mainwp-article-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/ithemes-security-extension/install-and-set-mainwp-ithemes-security-extension/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Install and Set MainWP iThemes Security Extension', 'mainwp-article-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/ithemes-security-extension/manage-ithemes-security-settings/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'Manage iThemes Security Settings', 'mainwp-article-uploader-extension' ); ?></a><br/>
			<a href="https://mainwp.com/help/docs/ithemes-security-extension/ithemes-security-dashboard/" target="_blank"><i class="fa fa-book"></i> <?php echo __( 'iThemes Security Dashboard', 'mainwp-article-uploader-extension' ); ?></a><br/>
		<?php }
		$output = ob_get_clean();
		return $output;
	}

	public function localization() {
		load_plugin_textdomain( 'l10n-mainwp-ithemes-security-extension', false,  dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}
	
	public function admin_init() {
        if (isset($_GET['page']) && ( $_GET['page'] == 'ManageSitesiThemes' || $_GET['page'] == 'Extensions-Mainwp-Ithemes-Security-Extension' )) {
            wp_enqueue_style( 'mainwp-ithemes-extension', self::$plugin_url . 'css/mainwp-ithemes.css' );
            wp_enqueue_script( 'mainwp-ithemes-extension', self::$plugin_url . 'js/mainwp-ithemes.js' );
        }
		MainWP_IThemes_Security::get_instance()->admin_init();
		MainWP_IThemes_Security_Plugin::get_instance()->admin_init();
	}
	
	function mainwp_sync_extensions_options($values = array()) {
		$values['mainwp-ithemes-security-extension'] = array(
				'plugin_slug' => 'better-wp-security/better-wp-security.php',	
				'plugin_name' => 'iThemes Security'
		);
		return $values;
	}	
}


function mainwp_ithemes_security_extension_autoload( $class_name ) {
	$allowedLoadingTypes = array( 'class' );
	$class_name = str_replace( '_', '-', strtolower( $class_name ) );
	foreach ( $allowedLoadingTypes as $allowedLoadingType ) {
		$class_file = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . str_replace( basename( __FILE__ ), '', plugin_basename( __FILE__ ) ) . $allowedLoadingType . DIRECTORY_SEPARATOR . $class_name . '.' . $allowedLoadingType . '.php';
		if ( file_exists( $class_file ) ) {
			require_once( $class_file );
		}
	}
}

if ( function_exists( 'spl_autoload_register' ) ) {
	spl_autoload_register( 'mainwp_ithemes_security_extension_autoload' );
} else {
	function __autoload( $class_name ) {

		mainwp_ithemes_security_extension_autoload( $class_name );
	}
}

register_activation_hook( __FILE__, 'mainwp_ithemes_security_extension_activate' );
register_deactivation_hook( __FILE__, 'mainwp_ithemes_security_extension_deactivate' );

function mainwp_ithemes_security_extension_activate() {

	update_option( 'mainwp_ithemes_security_extension_activated', 'yes' );
	$extensionActivator = new MainWP_IThemes_Security_Extension_Activator();
	$extensionActivator->activate();	
}

function mainwp_ithemes_security_extension_deactivate() {

	$extensionActivator = new MainWP_IThemes_Security_Extension_Activator();
	$extensionActivator->deactivate();
}

class MainWP_IThemes_Security_Extension_Activator
{
	protected $mainwpMainActivated = false;
	protected $childEnabled = false;
	protected $childKey = false;
	protected $childFile;
	protected $plugin_handle = 'mainwp-ithemes-security-extension';
	protected $product_id = 'MainWP Security Extension';
	protected $software_version = '1.8';

	public function __construct() {

		$this->childFile = __FILE__;
		add_filter( 'mainwp-getextensions', array( &$this, 'get_this_extension' ) );
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', false );

		if ( $this->mainwpMainActivated !== false ) {
			$this->activate_this_plugin();
		} else {
			add_action( 'mainwp-activated', array( &$this, 'activate_this_plugin' ) );
		}
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_action( 'admin_notices', array( &$this, 'mainwp_error_notice' ) );
		MainWP_IThemes_Security_DB::get_instance()->install();
	}

	function admin_init() {
		if ( get_option( 'mainwp_ithemes_security_extension_activated' ) == 'yes' ) {
			delete_option( 'mainwp_ithemes_security_extension_activated' );
			wp_redirect( admin_url( 'admin.php?page=Extensions' ) );
			return;
		}
	}

	function get_this_extension( $pArray ) {

		$pArray[] = array( 'plugin' => __FILE__, 'api' => $this->plugin_handle, 'mainwp' => true, 'callback' => array( &$this, 'settings' ), 'apiManager' => true , 'on_load_callback' => array('MainWP_ITSEC_Admin_Page_Loader' , 'load') );
		return $pArray;
	}

	function settings() {
		do_action( 'mainwp-pageheader-extensions', __FILE__ );
		MainWP_IThemes_Security::render();
		do_action( 'mainwp-pagefooter-extensions', __FILE__ );
	}

	function activate_this_plugin() {

		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', $this->mainwpMainActivated );
		$this->childEnabled = apply_filters( 'mainwp-extension-enabled-check', __FILE__ );		
		$this->childKey = $this->childEnabled['key'];

		if ( function_exists( 'mainwp_current_user_can' )&& ! mainwp_current_user_can( 'extension', 'mainwp-ithemes-security-extension' ) ) {
			return; 			
		}
		new MainWP_IThemes_Security_Extension();
	}

	public function get_child_key() {

		return $this->childKey;
	}

	public function get_child_file() {

		return $this->childFile;
	}

	function mainwp_error_notice() {

		global $current_screen;
		if ( $current_screen->parent_base == 'plugins' && $this->mainwpMainActivated == false ) {
			echo '<div class="error"><p>MainWP iThemes Security Extension ' . __( 'requires <a href="http://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> to be activated in order to work. Please install and activate <a href="http://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> first.' ) . '</p></div>';
		}
	}

	public function update_option( $option_name, $option_value ) {

		$success = add_option( $option_name, $option_value, '', 'no' );

		if ( ! $success ) {
			$success = update_option( $option_name, $option_value );
		}

		 return $success;
	}

	public function activate() {
		$options = array(
		'product_id' => $this->product_id,
							'activated_key' => 'Deactivated',
							'instance_id' => apply_filters( 'mainwp-extensions-apigeneratepassword', 12, false ),
							'software_version' => $this->software_version,
						);
		$this->update_option( $this->plugin_handle . '_APIManAdder', $options );
	}

	public function deactivate() {
		$this->update_option( $this->plugin_handle . '_APIManAdder', '' );
	}
}

global $mainWPIThemesSecurityExtensionActivator;
$mainWPIThemesSecurityExtensionActivator = new MainWP_IThemes_Security_Extension_Activator();
