jQuery( document ).ready(function($) {
	$( '#mwp_ithemes_dashboard_tab_lnk' ).on('click', function () {
		showIThemeTab( true );
		return false;
	});

	$( '.itheme_tabs_lnk a' ).on('click', function () {
		if (jQuery( this ).attr( 'id' ) != 'mwp_ithemes_dashboard_tab_lnk') {
			jQuery( '.itheme_tabs_lnk a' ).removeClass( 'mainwp_action_down' );
			jQuery( this ).addClass( 'mainwp_action_down' );
		}
	});

	$( '#mainwp_itheme_override_general_settings' ).on('change', function (){
		var statusEl = jQuery( '#mwp_itheme_site_save_settings_status' );
		var loaderEl = jQuery( '#itheme_site_settings .settingfield i' );
		statusEl.hide();
		loaderEl.show();
		data = {
			action:'mainwp_itheme_site_override_settings',
			ithemeSiteID: $( 'input[name=mainwp_itheme_settings_site_id]' ).val(),
			override: $( '#mainwp_itheme_override_general_settings' ).is( ":checked" ) ? 1 : 0
		};
		jQuery.post(ajaxurl, data, function (response) {
			loaderEl.hide();
			if ( response) {
				if (response.error) {
					statusEl.css( 'color', 'red' );
					statusEl.html( response.error );
				} else if (response.result == 'success') {
					statusEl.css( 'color', '#21759B' );
					statusEl.html( __( 'Saved.' ) );
					setTimeout(function ()
						{
						statusEl.fadeOut();
					}, 3000);
				} else {
					statusEl.css( 'color', 'red' );
					statusEl.html( 'Undefined error' );
				}
			} else {
				statusEl.css( 'color', 'red' );
				statusEl.html( 'Undefined error' );
			}
			statusEl.fadeIn();
		}, 'json' );

		return false;
	});
        
        jQuery( '.itsec_add_dashboard_ip_to_whitelist' ).click( function ( event ) {
		event.preventDefault();
		jQuery( '#itsec-global-lockout_white_list' ).val( jQuery( '#itsec-global-lockout_white_list' ).val() + jQuery( '.itsec_add_dashboard_ip_to_whitelist' ).attr( 'href' ) );

	} );

});

showIThemeTab = function(dashboard) {
	var dashboard_tab_lnk = jQuery( "#mwp_ithemes_dashboard_tab_lnk" );
	if (dashboard) {
		jQuery( '.itheme_tabs_lnk a.selected' ).removeClass( 'mainwp_action_down' );
		dashboard_tab_lnk.addClass( 'mainwp_action_down' );
		jQuery( "#mainwp_itheme_screens_tab" ).hide();
		jQuery( "#mwp_ithemes_dashboard_tab" ).show();
	}
};

jQuery( document ).ready(function($) {

	jQuery( '#mainwp-ithemes-tips .mainwp-show-tut' ).on('click', function(){
		jQuery( '.mainwp-ithemes-tut' ).hide();
		var num = jQuery( this ).attr( 'number' );
		jQuery( '.mainwp-ithemes-tut[number="' + num + '"]' ).show();
		mainwp_setCookie( 'mwp_ithemes_quick_tut_number', jQuery( this ).attr( 'number' ) );
		return false;
	});

	jQuery( '#mainwp-ithemes-quick-start-guide' ).on('click', function () {
		if (mainwp_getCookie( 'mwp_ithemes_quick_guide' ) == 'on') {
			mainwp_setCookie( 'mwp_ithemes_quick_guide', '' ); } else {
			mainwp_setCookie( 'mwp_ithemes_quick_guide', 'on' ); }
			mwp_ithemes_showhide_quick_guide();
			return false;
	});
	jQuery( '#mainwp-ithemes-tips-dismiss' ).on('click', function () {
		mainwp_setCookie( 'mwp_ithemes_quick_guide', '' );
		mwp_ithemes_showhide_quick_guide();
		return false;
	});

	mwp_ithemes_showhide_quick_guide();

});

mwp_ithemes_showhide_quick_guide = function() {
	var show = mainwp_getCookie( 'mwp_ithemes_quick_guide' );
	if (show == 'on') {
		jQuery( '#mainwp-ithemes-tips' ).show();
		jQuery( '#mainwp-ithemes-quick-start-guide' ).hide();
		mwp_ithemes_showhide_quick_tut();
	} else {
		jQuery( '#mainwp-ithemes-tips' ).hide();
		jQuery( '#mainwp-ithemes-quick-start-guide' ).show();
	}
}

mwp_ithemes_showhide_quick_tut = function() {
	var tut = mainwp_getCookie( 'mwp_ithemes_quick_tut_number' );
	jQuery( '.mainwp-ithemes-tut' ).hide();
	jQuery( '.mainwp-ithemes-tut[number="' + tut + '"]' ).show();
}


jQuery( document ).ready(function($) {
	$( '.ithemes_plugin_upgrade_noti_dismiss' ).live('click', function() {
		var parent = $( this ).closest( '.ext-upgrade-noti' );
		parent.hide();
		var data = {
			action: 'mainwp_ithemes_upgrade_noti_dismiss',
			ithemeSiteID: parent.attr( 'website-id' ),
			new_version: parent.attr( 'version' ),
		}
		jQuery.post(ajaxurl, data, function (response) {

		});
		return false;
	});

	$( '.mwp_ithemes_active_plugin' ).on('click', function() {
		mainwp_ithemes_plugin_active_start_specific( $( this ), false );
		return false;
	});

	$( '.mwp_ithemes_upgrade_plugin' ).on('click', function() {
		mainwp_ithemes_plugin_upgrade_start_specific( $( this ), false );
		return false;
	});

	$( '.mwp_ithemes_showhide_plugin' ).on('click', function() {
		mainwp_ithemes_plugin_showhide_start_specific( $( this ), false );
		return false;
	});

	$( '#ithemes_plugin_doaction_btn' ).on('click', function() {
		var bulk_act = $( '#mwp_ithemes_plugin_action' ).val();
		mainwp_ithemes_plugin_do_bulk_action( bulk_act );
	});
});

var ithemes_bulkMaxThreads = 3;
var ithemes_bulkTotalThreads = 0;
var ithemes_bulkCurrentThreads = 0;
var ithemes_bulkFinishedThreads = 0;

mainwp_ithemes_plugin_do_bulk_action = function(act) {
	var selector = '';
	switch (act) {
		case 'activate-selected':
			selector = '#the-mwp-ithemes-list tr.plugin-update-tr .mwp_ithemes_active_plugin';
			jQuery( selector ).addClass( 'queue' );
			mainwp_ithemes_plugin_active_start_next( selector );
			break;
		case 'update-selected':
			selector = '#the-mwp-ithemes-list tr.plugin-update-tr .mwp_ithemes_upgrade_plugin';
			jQuery( selector ).addClass( 'queue' );
			mainwp_ithemes_plugin_upgrade_start_next( selector );
			break;
		case 'hide-selected':
			selector = '#the-mwp-ithemes-list tr .mwp_ithemes_showhide_plugin[showhide="hide"]';
			jQuery( selector ).addClass( 'queue' );
			mainwp_ithemes_plugin_showhide_start_next( selector );
			break;
		case 'show-selected':
			selector = '#the-mwp-ithemes-list tr .mwp_ithemes_showhide_plugin[showhide="show"]';
			jQuery( selector ).addClass( 'queue' );
			mainwp_ithemes_plugin_showhide_start_next( selector );
			break;
	}
}

mainwp_ithemes_plugin_showhide_start_next = function(selector) {
	while ((objProcess = jQuery( selector + '.queue:first' )) && (objProcess.length > 0) && (ithemes_bulkCurrentThreads < ithemes_bulkMaxThreads)) {
		objProcess.removeClass( 'queue' );
		if (objProcess.closest( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length == 0) {
			continue;
		}
		mainwp_ithemes_plugin_showhide_start_specific( objProcess, true, selector );
	}
}

mainwp_ithemes_plugin_showhide_start_specific = function(pObj, bulk, selector) {
	var parent = pObj.closest( 'tr' );
	var loader = parent.find( '.its-action-working .loading' );
	var statusEl = parent.find( '.its-action-working .status' );
	var showhide = pObj.attr( 'showhide' );
	var pluginName = parent.attr( 'plugin-name' );
	if (bulk) {
		ithemes_bulkCurrentThreads++; }

	var data = {
		action: 'mainwp_ithemes_showhide_plugin',
		ithemeSiteID: parent.attr( 'website-id' ),
		showhide: showhide
	}
	statusEl.hide();
	loader.show();
	jQuery.post(ajaxurl, data, function (response) {
		loader.hide();
		pObj.removeClass( 'queue' );
		if (response && response['error']) {
			statusEl.css( 'color', 'red' );
			statusEl.html( response['error'] ).show();
		} else if (response && response['result'] == 'success') {
			if (showhide == 'show') {
				pObj.text( "Hide " + pluginName + " Plugin" );
				pObj.attr( 'showhide', 'hide' );
				parent.find( '.ithemes_hidden_title' ).html( __( 'No' ) );
			} else {
				pObj.text( "Show " + pluginName + " Plugin" );
				pObj.attr( 'showhide', 'show' );
				parent.find( '.ithemes_hidden_title' ).html( __( 'Yes' ) );
			}

			statusEl.css( 'color', '#21759B' );
			statusEl.html( __( 'Successful' ) ).show();
			statusEl.fadeOut( 3000 );
		} else {
			statusEl.css( 'color', 'red' );
			statusEl.html( __( "Undefined error" ) ).show();
		}

		if (bulk) {
			ithemes_bulkCurrentThreads--;
			ithemes_bulkFinishedThreads++;
			mainwp_ithemes_plugin_showhide_start_next( selector );
		}

	},'json');
	return false;
}

mainwp_ithemes_plugin_upgrade_start_next = function(selector) {
	while ((objProcess = jQuery( selector + '.queue:first' )) && (objProcess.length > 0) && (objProcess.closest( 'tr' ).prev( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length > 0) && (ithemes_bulkCurrentThreads < ithemes_bulkMaxThreads)) {
		objProcess.removeClass( 'queue' );
		if (objProcess.closest( 'tr' ).prev( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length == 0) {
			continue;
		}
		mainwp_ithemes_plugin_upgrade_start_specific( objProcess, true, selector );
	}
}

mainwp_ithemes_plugin_upgrade_start_specific = function(pObj, bulk, selector) {
	var parent = pObj.closest( '.ext-upgrade-noti' );
	var workingRow = parent.find( '.mwp-ithemes-row-working' );
        workingRow.find( '.status' ).hide();
	var slug = parent.attr( 'plugin-slug' );
	var data = {
		action: 'mainwp_ithemes_upgrade_plugin',
		ithemeSiteID: parent.attr( 'website-id' ),
		type: 'plugin',
		'slugs[]': [slug]
	}

	if (bulk) {
		ithemes_bulkCurrentThreads++; }

	parent.closest( 'tr' ).show();
	workingRow.find( 'i' ).show();
	jQuery.post(ajaxurl, data, function (response) {
		workingRow.find( 'i' ).hide();
		pObj.removeClass( 'queue' );
		if (response && response['error']) {
			workingRow.find( '.status' ).html( '<font color="red">' + response['error'] + '</font>' );
		} else if (response && response['upgrades'][slug]) {
			pObj.after( 'iThemes Security plugin has been updated' );
			pObj.remove();
		} else {
			workingRow.find( '.status' ).html( '<font color="red">' + __( "Undefined error" ) + '</font>' );
		}

		if (bulk) {
			ithemes_bulkCurrentThreads--;
			ithemes_bulkFinishedThreads++;
			mainwp_ithemes_plugin_upgrade_start_next( selector );
		}

	},'json');
	return false;
}

mainwp_ithemes_plugin_active_start_next = function(selector) {
	while ((objProcess = jQuery( selector + '.queue:first' )) && (objProcess.length > 0) && (objProcess.closest( 'tr' ).prev( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length > 0) && (ithemes_bulkCurrentThreads < ithemes_bulkMaxThreads)) {
		objProcess.removeClass( 'queue' );
		if (objProcess.closest( 'tr' ).prev( 'tr' ).find( '.check-column input[type="checkbox"]:checked' ).length == 0) {
			continue;
		}
		mainwp_ithemes_plugin_active_start_specific( objProcess, true, selector );
	}
}

mainwp_ithemes_plugin_active_start_specific = function(pObj, bulk, selector) {
	var parent = pObj.closest( '.ext-upgrade-noti' );
	var workingRow = parent.find( '.mwp-ithemes-row-working' );
	var slug = parent.attr( 'plugin-slug' );
	var data = {
		action: 'mainwp_ithemes_active_plugin',
		ithemeSiteID: parent.attr( 'website-id' ),
		'plugins[]': [slug]
	}

	if (bulk) {
		ithemes_bulkCurrentThreads++; }

	workingRow.find( 'i' ).show();
	jQuery.post(ajaxurl, data, function (response) {
		workingRow.find( 'i' ).hide();
		pObj.removeClass( 'queue' );
		if (response && response['error']) {
			workingRow.find( '.status' ).html( '<font color="red">' + response['error'] + '</font>' );
		} else if (response && response['result']) {
			pObj.after( 'iThemes Security plugin has been activated' );
			pObj.remove();
		}
		if (bulk) {
			ithemes_bulkCurrentThreads--;
			ithemes_bulkFinishedThreads++;
			mainwp_ithemes_plugin_active_start_next( selector );
		}

	},'json');
	return false;
}


jQuery( document ).ready( function () {
        jQuery( '#mwp_itheme_lockouts_release_btn' ).bind( 'click', function ( event ) {
            event.preventDefault(); 
            var caller = this;
            jQuery( caller ).closest('.submitleft').find('i').show();
            jQuery( caller ).attr('disabled', 'disabled');

            var site_id = jQuery('#mainwp_itheme_managesites_site_id').attr('site-id');
            if ( site_id > 0) {                        
                mainwp_itheme_individual_lockouts_release(site_id, caller);
            }   
    } );
    
}); 

mainwp_itheme_individual_lockouts_release = function(pSiteId, pCaller) {
    var statusEl = jQuery('.mwp_itheme_lockouts_status'); 
    statusEl.hide();
    var ids = jQuery(pCaller).closest('.inside').find('input[type="checkbox"]:checked').map(function(){
                                                                                                        return jQuery( this ).val();
                                                                                                    }).get();
    if (ids.length == 0)
        return; 

    var data = {
                action: 'mainwp_itheme_release_lockouts',
                siteId: pSiteId,   
                lockout_ids: ids,
                individual: true                
            };   
    //call the ajax
    jQuery.post( ajaxurl, data, function ( response ) {
            jQuery( pCaller ).closest('.submitleft').find('i').hide();
            jQuery( pCaller ).removeAttr('disabled'); 
            if ( response) {
                if (response.error) {
                    statusEl.css('color', 'red');
                    statusEl.html( response.error );
                } else if (response.message) {
                    statusEl.css('color', '#21759B');
                    statusEl.html(response.message);
                } else if (response.result == 'success') {
                    statusEl.css('color', '#21759B');
                    statusEl.html('Successful.');
                    setTimeout(function ()
                    {
                        location.href = location.href;        
                    }, 3000);
                } else {
                    statusEl.css('color', 'red');
                    statusEl.html( 'Undefined error.' );
                }
            }
            else 
            {
                statusEl.css('color', 'red');
                statusEl.html( 'Undefined error.' );
            }        
            statusEl.fadeIn();
    }, 'json' );                            
}


mainwp_itheme_individual_malware_scan = function(pSiteId, pCaller) {
    var statusEl = jQuery('#mwp_itheme_malware_scan_status'); 
    statusEl.hide();
    var data = {
                action: 'mainwp_itheme_malware_scan',
                ithemeSiteID: pSiteId,                
                individualSite: true,                
            };   
    //call the ajax
    jQuery.post( ajaxurl, data, function ( response ) {
            jQuery( pCaller ).closest('.submit').find('i').hide();
            jQuery( pCaller ).removeAttr('disabled'); 
            var error = true;
            if ( response) {
                if (response.error) {                   
                    statusEl.html( response.error );
                } else if (response.html) {
                    error = false;
                    jQuery('.itsec-malware-scan-results-wrapper').html(response.html);
                } else {                   
                    statusEl.html( 'Undefined error' );
                }
            }
            else 
            {
                statusEl.css('color', 'red');
                statusEl.html( 'Undefined error' );
            }   
            
            if (error) {
                statusEl.css('color', 'red');
                statusEl.fadeIn();
            }
            
    }, 'json' );
}

mainwp_itheme_malware_scan_start_next = function() {    
    while ((objProcess = jQuery('.siteItemProcess[status=queue]:first')) && (objProcess.length > 0) && (ithemes_bulkCurrentThreads < ithemes_bulkMaxThreads))
    {       
        objProcess.attr('status', 'processed');       
        mainwp_itheme_malware_scan_start_specific(objProcess);
    }

    if (ithemes_bulkFinishedThreads > 0 && ithemes_bulkFinishedThreads == ithemes_bulkTotalThreads) {
        jQuery('#mainwp_itheme_screens_tab').append('<div class="mainwp_info-box">' + __("Scan Homepage for Malware finished.") + '</div><p><a class="button-primary" href="' + mainwp_itsec_page.settings_page_url + '">Return to Settings</a></p>');
        jQuery( '.itsec-malware-scan-results-wrapper' ).on( 'click', '.itsec-malware-scan-toggle-details', mainwp_scan_results_toggleDetails );
    }
}

mainwp_itheme_malware_scan_start_specific = function(objProcess) {    
    var loadingEl = objProcess.find('i');  
    var statusEl = objProcess.find('.status');  
    var site_id = objProcess.attr('site-id');
    ithemes_bulkCurrentThreads++;
    var data = {
                action: 'mainwp_itheme_malware_scan',
                ithemeSiteID: site_id                               
            };

    statusEl.html('');
    loadingEl.show();
    //call the ajax
    jQuery.post( ajaxurl, data, function ( response ) {
            loadingEl.hide();
            var error = true;           
            if ( response) {
                if (response.error) {                
                    statusEl.html( response.error );
                } else if ( response.message ) {                    
                    statusEl.html( response.message );
                } else if ( response.html ) {
                    error = false;
                    statusEl.html( 'Successful' );
                    statusEl.after('<br/><br/><div class="itsec-malware-scan-results-wrapper">' + response.html + '</div>');
                } else {                    
                    statusEl.html( 'Undefined error' );
                }
            }
            else 
            {                
                statusEl.html( 'Undefined error' );
            }
            if (error) {
                statusEl.css('color', 'red');                
            }
            ithemes_bulkCurrentThreads--;
            ithemes_bulkFinishedThreads++;
            mainwp_itheme_malware_scan_start_next();
    }, 'json' );
}

mainwp_scan_results_toggleDetails = function( e ) {        
    e.preventDefault();
    var $container = jQuery(this).parents( '.itsec-malware-scan-results-section' );
    var $details = $container.find( '.itsec-malware-scan-details' );
    if ( $details.is(':visible') ) {
            jQuery(this).html( 'Show Details' );
            $details.hide();
    } else {
            jQuery(this).html( 'Hide Details' );
            $details.show();
    }
}
    
    