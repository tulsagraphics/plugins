<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! function_exists( 'ywpc_days' ) ) {

	/**
	 * How many days remains to $to
	 *
	 * @since   1.0.0
	 *
	 * @param   $to
	 *
	 * @return  integer
	 * @author  Alberto Ruggiero
	 */
	function ywpc_days( $total_seconds ) {

		return floor( $total_seconds / 60 / 60 / 24 );

	}

}

if ( ! function_exists( 'ywpc_hours' ) ) {

	/**
	 * How many hours remains to $to
	 *
	 * @since   1.0.0
	 *
	 * @param   $to
	 *
	 * @return  integer
	 * @author  Alberto Ruggiero
	 */
	function ywpc_hours( $total_seconds ) {

		return floor( $total_seconds / 60 / 60 );

	}

}

if ( ! function_exists( 'ywpc_minutes' ) ) {

	/**
	 * How many minutes remains to $to
	 *
	 * @since   1.0.0
	 *
	 * @param   $to
	 *
	 * @return  integer
	 * @author  Alberto Ruggiero
	 */
	function ywpc_minutes( $total_seconds ) {

		return floor( $total_seconds / 60 );

	}

}

if ( ! function_exists( 'ywpc_seconds' ) ) {

	/**
	 * How many seconds remains to $to
	 *
	 * @since   1.0.0
	 *
	 * @param   $to
	 *
	 * @return  integer
	 * @author  Alberto Ruggiero
	 */
	function ywpc_seconds( $to ) {

		return ywpc_current_timestamp( $to );

	}

}


if ( ! function_exists( 'ywpc_get_countdown' ) ) {

	/**
	 * Return Countdown
	 *
	 * @since   1.0.0
	 *
	 * @param   $end_date
	 *
	 * @return  array
	 * @author  Alberto Ruggiero
	 */
	function ywpc_get_countdown( $end_date ) {

		$total_seconds = ( $end_date - strtotime( current_time( "Y-m-d H:i:s" ) ) );
		$total_days    = ywpc_days( $total_seconds );
		$total_hours   = ywpc_hours( $total_seconds );
		$total_minutes = ywpc_minutes( $total_seconds );

		$days    = $total_days;
		$hours   = $total_hours - ( $total_days * 24 );
		$minutes = $total_minutes - ( $total_hours * 60 );
		$seconds = $total_seconds - ( $total_minutes * 60 );

		return array(
			'gmt' => get_option( 'gmt_offset' ),
			'to'  => $end_date,
			'dd'  => str_pad( $days, 3, '0', STR_PAD_LEFT ),
			'hh'  => str_pad( $hours, 2, '0', STR_PAD_LEFT ),
			'mm'  => str_pad( $minutes, 2, '0', STR_PAD_LEFT ),
			'ss'  => str_pad( $seconds, 2, '0', STR_PAD_LEFT ),
		);

	}

}

if ( ! function_exists( 'ywpc_version_change' ) ) {

	function ywpc_version_change() {

		$ywpc_version = get_option( 'ywpc_version' );

		if ( empty( $ywpc_version ) || version_compare( $ywpc_version, '1.0.8', '<' ) ) {

			$query_args = array(
				'posts_per_page' => - 1,
				'post_status'    => 'publish',
				'post_type'      => 'product',
				'meta_query'     => array(
					'relation' => 'AND',
					array(
						'key'   => '_ywpc_enabled',
						'value' => 'yes',
					),
				)
			);

			$products = new WP_Query( $query_args );

			if ( $products->have_posts() ) {

				while ( $products->have_posts() ) {

					$products->the_post();

					$product = wc_get_product( $products->post->ID );

					if ( ! $product->is_type( 'variable' ) ) {

						$start_date = yit_get_prop( $product, '_sale_price_dates_from' );
						$end_date   = yit_get_prop( $product, '_sale_price_dates_to' );

						if ( $start_date != '' ) {

							yit_save_prop( $product, '_ywpc_sale_price_dates_from', $start_date );
							yit_save_prop( $product, '_ywpc_sale_price_dates_to', $end_date );

						}

					} else {

						$product_variables = $product->get_available_variations();

						if ( count( array_filter( $product_variables ) ) > 0 ) {

							$product_variables = array_filter( $product_variables );

							foreach ( $product_variables as $product_variable ) {

								$variation = wc_get_product( $product_variable['variation_id'] );

								$start_date = yit_get_prop( $variation, '_sale_price_dates_from' );
								$end_date   = yit_get_prop( $variation, '_sale_price_dates_to' );

								if ( $start_date != '' ) {

									yit_save_prop( $product, '_ywpc_sale_price_dates_from', $start_date );
									yit_save_prop( $product, '_ywpc_sale_price_dates_to', $end_date );
								}

							}

						}

					}

				}

				wp_reset_query();

				update_option( 'ywpc_version', YWPC_VERSION );

			}
		}

	}

	add_action( 'init', 'ywpc_version_change' );

}

if ( defined( 'YITH_WCPO_PREMIUM' ) && YITH_WCPO_PREMIUM ) {

	add_action( 'ywpc_countdown_expiration', 'ywpc_preorder_expiration', 10, 2 );

	function ywpc_preorder_expiration( $product, $id ) {

		$auto_for_sale = get_option( 'yith_wcpo_enable_pre_order_purchasable' );

		if ( 'yes' == $auto_for_sale ) {

			$pre_order_product = new YITH_Pre_Order_Product( $id );

			if ( $pre_order_product->get_pre_order_status() == 'yes' ) {

				$pre_order_product->clear_pre_order_product();

				wc_delete_product_transients( $id );

				if ( $product->is_type( 'variable' ) ) {

					$variation_object = wc_get_product( $id );

					$args = array(
						'_ywpc_sale_price_dates_from' => '',
						'_ywpc_sale_price_dates_to'   => '',
						'_ywpo_variation'             => ''
					);

					yit_save_prop( $variation_object, $args );

				} else {

					yit_save_prop( $product, '_ywpc_enabled', 'no' );

				}

			}

		}

	}

}

