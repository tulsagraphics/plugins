<?php

class UptimeUrl extends MvcModel {

	public $primary_key = 'url_id';
	public $just_insertupdate_id;
	var $table = '{prefix}aum_urls';
	var $validate = array(
			'url_friendly_name' => array(
				'rule' => 'not_empty',
				'message' => 'Please enter Monitor Friendly Name'
			),
			'url_address' => array(
				'rule' => 'not_empty',
				'message' => 'Please enter Site URL'
			),
			'not_not_email' => array(
				'rule' => 'email',
				'message' => 'Please enter valid notification email address'
			),
		);
	
	function after_save( $object ) {
		if ( ! empty( $object ) ) {
			$this->just_insertupdate_id = $object->{$this->primary_key};
		} else {
			$this->just_insertupdate_id = null;
		}
	}

	function save_uptime_monitors( $monitor_id, $urls_monitor ) {
		if ( empty( $monitor_id ) ) {
			return false;
		}
		if ( ! is_array( $urls_monitor ) && count( $urls_monitor ) < 1 ) {
			return false;
		}
		$saved_ids = array();
		foreach ( $urls_monitor as $monitor ) {
            
			$current = $this->find(array(
				'conditions' => array(
					//'user_id' => get_current_user_id(),
					'url_uptime_monitor_id' => $monitor->id,
					'monitor_id' => $monitor_id,
				),
			));
            $this->params['data']['UptimeUrl'] = array();
			if ( ! empty( $current ) ) {
				$this->params['data']['UptimeUrl']['url_id'] = $current[0]->url_id;
			}
            
			$this->params['data']['UptimeUrl']['monitor_id'] = $monitor_id;
			$this->params['data']['UptimeUrl']['url_uptime_monitor_id'] = $monitor->id;
			$this->params['data']['UptimeUrl']['url_friendly_name'] = $monitor->friendly_name;
			$this->params['data']['UptimeUrl']['url_address'] = $monitor->url;
			$this->params['data']['UptimeUrl']['url_monitor_type'] = $monitor->type;
			$this->params['data']['UptimeUrl']['url_monitor_subtype'] = $monitor->sub_type;
			$this->params['data']['UptimeUrl']['url_monitor_keywordtype'] = $monitor->keyword_type;
			$this->params['data']['UptimeUrl']['url_monitor_keywordvalue'] = $monitor->keyword_value;
			$this->params['data']['UptimeUrl']['monitor_interval'] = intval($monitor->interval / 60);
            $this->params['data']['UptimeUrl']['http_username'] = $monitor->http_username;
            $this->params['data']['UptimeUrl']['http_password'] = $monitor->http_password;
			$this->params['data']['UptimeUrl']['status'] = $monitor->status;
            if(property_exists($monitor, 'all_time_uptime_ratio')) {
                $this->params['data']['UptimeUrl']['alltimeuptimeratio'] = $monitor->all_time_uptime_ratio;
            }
            //$this->params['data']['UptimeUrl']['customuptimeratio'] = $monitor->custom_uptime_ratio;
			$this->save( $this->params['data'] );
			if ( ! empty( $this->just_insertupdate_id ) ) {
				$saved_ids[] = $this->just_insertupdate_id;
			}
		}
		return $saved_ids;
	}
	
	function save_uptime_monitor_status( $monitor_id, $monitor ) {
		if ( !is_object( $monitor ) || !property_exists($monitor, 'id')) {
			return false;
		}
		
		$current = $this->find(array(
			'conditions' => array(				
				'url_uptime_monitor_id' => $monitor->id,
				'monitor_id' => $monitor_id
			),
		));
		
		if ( ! empty( $current ) ) {            
			$this->params['data']['UptimeUrl']['url_id'] = $current[0]->url_id;
			$this->params['data']['UptimeUrl']['status'] = $monitor->status;
			$this->params['data']['UptimeUrl']['alltimeuptimeratio'] = $monitor->all_time_uptime_ratio;
            $this->params['data']['UptimeUrl']['customuptimeratio'] = $monitor->custom_uptime_ratio;
			$this->save( $this->params['data'] );
			return true;
		}
		
		return false;
	}
	
}
