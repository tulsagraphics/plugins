<?php

class UptimeStats extends MvcModel {

	public $primary_key = 'event_id';
	var $per_page = 10;
	var $table = '{prefix}aum_stats';

	function get_event( $condition = 1 ) {

	}

	function get_last_event( $conditions = array() ) {
		$event = $this->find_one( array( 'order' => 'event_datetime_gmt DESC', 'conditions' => $conditions ) );
		return $event;
	}

	// retrieve events: monitor is on or off (not url status events)
	function get_events( $conditions = '1', $order_by = '', $limit = '') {
        if ($order_by == '') {
            $order_by = 'event_datetime_gmt ASC, monitor_id ASC';
        }             
		global $wpdb;
        
        if (empty($conditions))
            $conditions = '1';
        
		$sql = 'SELECT * FROM ' . $wpdb->prefix . 'aum_stats WHERE ' . $conditions . " ORDER BY $order_by $limit";
		$events = $wpdb->get_results( $sql );
		return $events;
	}
    
	function get_monitor_events( $conditions = '1', $order_by = '', $limit = '') {
        if ($order_by == '') {
            $order_by = 'event_datetime_gmt ASC, monitor_id ASC';
        }             
		global $wpdb;
        
        if (empty($conditions))
            $conditions = '1';
        
		$sql = 'SELECT urls.*, stats.* FROM ' . $wpdb->prefix . 'aum_urls as urls INNER JOIN ' . $wpdb->prefix . 'aum_stats as stats ' .
                ' ON urls.url_uptime_monitor_id = stats.monitor_id ' .
                ' WHERE ' . $conditions . 
                " ORDER BY $order_by $limit";
		$events = $wpdb->get_results( $sql );
        
		return $events;
	}
    
    
}
