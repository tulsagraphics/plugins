<?php
if ( ! empty( $urls ) ) {		
		$event_statuses = UptimeRobot_DB::get_event_statuses();
    ?>
     <table id="mainwp-table" class="wp-list-table aum-monitors-tbl widefat" cellspacing="0">
        <thead>
            <tr>
                <th><?php _e( 'Site / Monitor', 'advanced-uptime-monitor-extension'); ?></th>
                <th><?php _e( 'Status', 'advanced-uptime-monitor-extension'); ?></th>
                <th><?php _e( 'Type', 'advanced-uptime-monitor-extension'); ?></th>
                <th><?php _e( 'Uptime Ratio', 'advanced-uptime-monitor-extension'); ?></th>
            </tr>
        </thead>
        <tbody>
        <?php
		foreach ( $urls as $url ) {
				?>
            <tr>
                <td>
                    <?php echo ( ! empty( $url->url_friendly_name ) ? $url->url_friendly_name . '<br/><a href="' . $url->url_address. '" target="_blank">' . $url->url_address . '</a>' : '<a href="' . $url->url_address . '" target="_blank">' . $url->url_address . '</a>'); ?>
                </td>
                <td>
						<?php
                    if ( isset( $url->monitor_status ) && isset( $url->monitor_alltimeuptimeratio ) ) {
                        echo Advanced_Uptime_Monitor_Extension::get_install()->render_status_bar( $url->monitor_status, $url->monitor_alltimeuptimeratio );
                                    } else {
                        echo 'N / A';
                                    }
                    ?>
                </td>
                <td><?php echo Advanced_Uptime_Monitor_Extension::get_install()->monitor_types[ $url->url_monitor_type ] ?></td>
                <td>
                    <?php 
                    if ( isset( $url->monitor_status ) && isset( $url->monitor_alltimeuptimeratio ) ) {
                        echo Advanced_Uptime_Monitor_Extension::get_install()->render_ratio_bar( $url->monitor_status, $url->monitor_alltimeuptimeratio );
                                } else {
                            echo 'N / A';
                                }
						?>              
                </td>
            </tr>            
					<?php
    	}    
					?>
        </tbody>
        <?php if ( ! isset( $site_id ) ) : ?>
        <tfoot>
        	<tr>
        		<th><?php echo __( 'Total: ', 'advanced-uptime-monitor-extension' ) . $total .  __( ' monitors', 'advanced-uptime-monitor-extension' ); ?></th>
        		<th>&nbsp;</th>
        		<th>&nbsp;</th>
        		<th>
				<?php
			        if ( $total > 50 ) {
			$count_page = ceil( $total / 50 );
			                echo 'Pages:  ';
			for ( $i = 1; $i <= $count_page; $i++ ) {
				if ( $i != $get_page ) {
			                        echo '<a href="admin.php?page=Extensions-Advanced-Uptime-Monitor-Extension&get_page=' . $i . '">' . $i . '</a>  ';
				} else {
			                        echo '<strong>' . $i . '</strong>  ';
				}
			}
		}			
			        ?>
        		</th>
        	</tr>
        </tfoot>
        <?php endif; ?>
    </table>
    <?php if ( ! isset( $site_id ) ) : ?>
	    <div class="mainwp-postbox-actions-bottom" style="margin: 10px -10px -10px -10px;">
	    	<a href="admin.php?page=Extensions-Advanced-Uptime-Monitor-Extension" class="button button-primary"><?php _e( 'See all Monitors ', 'advanced-uptime-monitor-extension' ); ?></a>
	    </div>
	<?php endif; ?>
<?php
} else {
	if ( ! isset( $site_id ) ) {
			?>
		<div class="mainwp-notice mainwp-notice-yellow"><?php _e( 'No monitors have been created yet or set to show in the widget. If you have monitors created but not showing here, please set monitors to show in Overview Widget.', 'advanced-uptime-monitor-extension' ); ?></div>
		<div class="mainwp-postbox-actions-bottom" style="margin: 10px -10px -10px -10px;">
			<a href="admin.php?page=Extensions-Advanced-Uptime-Monitor-Extension" class="button button-primary"><?php _e( 'Add monitors to the widget or Create New Monitors', 'advanced-uptime-monitor-extension' ); ?></a>
			</div>
			<?php
	} else {
			?>
		<div class="mainwp-notice mainwp-notice-yellow"><?php _e( 'Monitor has not been created for the child site.', 'advanced-uptime-monitor-extension' ); ?></div>
		<div class="mainwp-postbox-actions-bottom" style="margin: 10px -24px -24px -24px;">
				<a href="admin.php?page=Extensions-Advanced-Uptime-Monitor-Extension" title="Create Monitor" class="button button-primary"><?php _e( 'Create Monitor', 'advanced-uptime-monitor-extension' ); ?></a>
			</div>
			<?php
	}
}
