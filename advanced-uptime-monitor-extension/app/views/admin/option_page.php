<?php
$this->display_flash();

if ( ! empty( $urls ) ) {
	?>
    <input type="hidden" id="mainwp_aum_extension_display_dashboard_nonce" value="<?php echo wp_create_nonce( 'aum_nonce_display_dashboard' ); ?>"/>
    <div class="monitor_action actions">
        <select name="monitor_action" >
            <option value="-1" selected="selected"><?php _e( 'Bulk Actions', 'advanced-uptime-monitor-extension' ); ?></option>
            <option value="display" ><?php _e( 'Display in Overview Widget', 'advanced-uptime-monitor-extension' ); ?></option>			
            <option value="hidden" ><?php _e( 'Hide in Overview Widget', 'advanced-uptime-monitor-extension' ); ?></option>
            <option value="start" ><?php _e( 'Start', 'advanced-uptime-monitor-extension' ); ?></option>
            <option value="pause" ><?php _e( 'Pause', 'advanced-uptime-monitor-extension' ); ?></option>
            <option value="delete"><?php _e( 'Delete', 'advanced-uptime-monitor-extension' ); ?></option>
        </select>
        <input type="button" href="javascript:void(0)" onclick="urm_apply_check(this, event)" name="doaction" id="doaction" class="button" value="<?php _e( 'Apply', 'advanced-uptime-monitor-extension' ); ?>">
    </div>                            
    <?php
    $event_statuses = UptimeRobot_DB::get_event_statuses();    
	$current_gmt_time = time(); // timezone independent (=UTC).	
    $hour = (int) date( 'H', $current_gmt_time + $log_gmt_offset * 60 * 60 );
    $hours = '';
    if ( 24 == $hour ) {
        for ( $i = 0; $i < $hour; $i++ ) {
            $hours .= '<div>' . $i . '</div>'; }
    } else {
        $begin_hour = $hour + 1;
        for ( $i = $begin_hour; $i < 24; $i++ ) {
            $hours .= '<div>' . $i . '</div>';
        }
        for ( $i = 0; $i <= $hour; $i++ ) {
            $hours .= '<div>' . $i . '</div>';
        }
    }
    
    ?>
     <table id="mainwp-table" class="wp-list-table aum-monitors-tbl widefat" cellspacing="0">
        <thead>
            <tr>
                <th><input type="checkbox" name="checkall" class="url_checkall" id="url_checkall"></th>
                <th><?php _e( 'Site / Monitor', 'advanced-uptime-monitor-extension'); ?></th>
                <th><?php _e( 'Status', 'advanced-uptime-monitor-extension'); ?></th>
                <th><?php _e( 'Type', 'advanced-uptime-monitor-extension'); ?></th>
                <th>
                    <div class="stratch-hours">
                        <?php echo $hours; ?>
                    </div>
                </th>
                <th style="text-align:center;"><?php _e( 'Uptime Ratio', 'advanced-uptime-monitor-extension'); ?></th>
                <th style="text-align:center;"><?php _e( 'Widget', 'advanced-uptime-monitor-extension'); ?></th>
                <th><?php _e( 'Actions', 'advanced-uptime-monitor-extension'); ?></th>
            </tr>
        </thead>
        <tbody>
    <?php
	foreach ( $urls as $url ) {
		?>
        <tr>
            <td><input type="checkbox" name="checkbox_url" class="checkbox_url" id="checkbox_url"></td>
            <td>
                <?php echo ( ! empty( $url->url_friendly_name ) ? $url->url_friendly_name . '<br/><a href="' . $url->url_address. '" target="_blank">' . $url->url_address . '</a>' : '<a href="' . $url->url_address . '" target="_blank">' . $url->url_address . '</a>'); ?>
                    <div id="loading_status" class="aum_mainwp_monitor_actions_loading" ><i class="fa fa-spinner fa-pulse"></i> <?php _e( 'Please wait...', 'advanced-uptime-monitor-extension' ); ?></div>
            </td>
            <td>
                <?php 
                if ( isset( $url->monitor_status ) && isset( $url->monitor_alltimeuptimeratio ) ) {
                    echo Advanced_Uptime_Monitor_Extension::get_install()->render_status_bar( $url->monitor_status, $url->monitor_alltimeuptimeratio );
                } else {
                    echo 'N / A';
                } 
                ?>
            </td>
            <td><?php echo Advanced_Uptime_Monitor_Extension::get_install()->monitor_types[ $url->url_monitor_type ] ?></td>
            <td class="aum_monitors_list">
                <?php
				$first = true;
				$status = '';                
				if ( is_array( $stats ) ) {					
                        $total_events = ( isset( $stats[ $url->url_uptime_monitor_id ] ) && is_array( $stats[ $url->url_uptime_monitor_id ] ) ) ?  count( $stats[ $url->url_uptime_monitor_id ] ) : 0;
					$i = 0;
                        if ( $total_events > 0 ) {                        
                        if ( isset( $stats[ $url->url_uptime_monitor_id ] ) ) {                            
                            foreach ( $stats[ $url->url_uptime_monitor_id ] as $event ) {
                                $i++;
                                if ( ! $first ) { // avoid display the beging status
                                        $class = isset( $event_statuses[ $log_type ] ) ? $event_statuses[ $log_type ] : 'not_checked';
                                    $style = '';
                                    if ( $event->status_bar_length > 0 ) {
                                        $style = 'style = "width: ' . $event->status_bar_length . '%"';
                                    } else {
                                        $style = 'style = "width: 1px"';
                                    }          
                                    $last_event = '';
                                    if ( $i == $total_events ) {
                                        $last_event = ' last_event="' . $class . '"';
                                        $class .= ' last_event';
                                    }
                                    
                                    $status = '<div class="event_fill ' . $class . '" ' . $style . $last_event . ' event-time="' . $event->event_datetime_gmt . '"></div>' . $status;
                                } else {
                                    $first = false;
                                }
                                $log_type = $event->type;
                            }
                        }
                    }
				}

				if ( empty( $status ) ) {
					$status = '<div class="event_fill" style = "width: 2px; margin-right: -2px;"></div>';
				}
				echo $status;
				//echo '<div class="aum_diagram_overlay"></div>';
				?>
            </td> 
            <td align="center">
                <?php 
                if ( isset( $url->monitor_status ) && isset( $url->monitor_alltimeuptimeratio ) ) {
                    echo Advanced_Uptime_Monitor_Extension::get_install()->render_ratio_bar( $url->monitor_status, $url->monitor_alltimeuptimeratio );
                } else {
                        echo 'N / A';
                } 
                ?>
            </td>                       
            <td align="center">
            <?php
                $showhide = 0;
                $iconname = '<i class="fa fa-eye-slash" title="Monitor is hidden in Widget" aria-hidden="true"></i>';
                if ( isset( $url->dashboard ) && ($url->dashboard) ) {
                    $showhide = 1;
                    $iconname = '<i class="fa fa-eye mainwp-green" title="Monitor is displayed in Widget" aria-hidden="true"></i>';
				}                
				?>
                <div class="mainwp-extra-col display cell url_display">
                    <span class="monitor_status url_showhide_link" show-hide="<?php echo $showhide; ?>" onclick="urm_showhide_monitor_in_widget_onclick(jQuery(this))" ><?php echo $iconname; ?></span>
                </div>    
                <?php
			?>   
            </td>
            <td class="url_actions" url_id="<?php echo $url->url_id ?>">
                    <span onclick="urm_status_monitor_button(this, <?php echo $url->url_id ?>,event, '<?php echo wp_create_nonce( 'aum_nonce_url_sp' ); ?>')" title="<?php  echo ($url->monitor_status == 0) ? 'Start' : 'Pause'; ?>" class="aum_action_link <?php  echo ($url->monitor_status == 0) ? 'start' : 'pause'; ?>"><i class="fa  <?php  echo ($url->monitor_status == 0) ? 'fa-play' : 'fa-pause' ?> fa-lg"></i></span>&nbsp;&nbsp;
                    <span onclick="urm_stats_monitor_button(<?php echo $url->url_id; ?>, '<?php echo wp_create_nonce( 'aum_nonce_statistics_table' ); ?>')" title="Statistics" class="aum_action_link stats_link"><i class="fa fa-bar-chart fa-lg"></i></span>&nbsp;&nbsp;
                    <span onclick="urm_edit_monitor_button(this, <?php echo $url->url_id; ?>, '<?php echo wp_create_nonce( 'aum_nonce_update_url' ); ?>')" title="Edit" class="aum_action_link url_edit_link"><i class="fa fa-pencil-square-o fa-lg"></i></span>&nbsp;&nbsp;
                    <span onclick="if (!confirm('Are you sure to delele selected item?')) return; urm_delete_monitor_button(jQuery(this), '<?php echo wp_create_nonce( 'aum_nonce_delete_monitor' ); ?>')" title="Delete" class="aum_action_link url_delete_link"><i class="fa fa-trash-o fa-lg"></i></span>
            </td>
        </tr>            
        <?php
	}    
    ?>        
    </tbody>
        <tfoot>
            <tr>
                <th><input type="checkbox" name="checkall" class="url_checkall" id="url_checkall"></th>
                <th><?php _e( 'Site / Monitor', 'advanced-uptime-monitor-extension'); ?></th>
                <th><?php _e( 'Status', 'advanced-uptime-monitor-extension'); ?></th>
                <th><?php _e( 'Type', 'advanced-uptime-monitor-extension'); ?></th>
                <th>
                    <div class="stratch-hours">
                        <?php echo $hours; ?>
                    </div>
                </th>
                <th style="text-align:center;"><?php _e( 'Uptime Ratio', 'advanced-uptime-monitor-extension'); ?></th>
                <th style="text-align:center;"><?php _e( 'Widget', 'advanced-uptime-monitor-extension'); ?></th>
                <th><?php _e( 'Actions', 'advanced-uptime-monitor-extension'); ?></th>
            </tr>
        </tfoot>
    </table>
    
    <div class="mainwp-postbox-actions-bottom" style="margin: 12px -12px -22px -12px !important;">
        <?php echo __( 'Total: ', 'advanced-uptime-monitor-extension' ) . $total .  __( ' monitors', 'advanced-uptime-monitor-extension' ); ?>
        <div class="mainwp-right">
    <?php
        if ( $total > 50 ) {
            $count_page = ceil( $total / 50 );
                echo 'Pages:  ';
            for ( $i = 1; $i <= $count_page; $i++ ) {
                if ( $i != $get_page ) {
                        echo '<a href="admin.php?page=Extensions-Advanced-Uptime-Monitor-Extension&get_page=' . $i . '">' . $i . '</a>  ';
                } else {
                        echo '<strong>' . $i . '</strong>  ';
                }
            }
        }
        ?>
        </div>
    </div>
    <?php
} else {
	?>
    <div class="mainwp-notice mainwp-notice-yellow">
        <?php _e( 'You have no monitors yet.', 'advanced-uptime-monitor-extension' ); ?>
        <?php _e( 'If you have monitors created but not showing here, please try to resave your Uptime Robot API key.', 'advanced-uptime-monitor-extension' ); ?>
    </div>
    <?php
}
?>    
<script>
    jQuery('input[name=checkall]').click(function () {
        if (jQuery(this).is(':checked'))
        {
            jQuery('input[name=checkbox_url]').each(function () {
                jQuery(this).attr('checked', 'checked');
            })
        }
        else
        {
            jQuery('input[name=checkbox_url]').each(function () {
                jQuery(this).removeAttr('checked');
            })
        }
    })
</script>
