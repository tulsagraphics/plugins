<?php
if ( ! is_array( $this->params['data']['UptimeUrl'] ) ) {  // is it object retrieved for monitor editing?
	$url = $this->params['data']['UptimeUrl'];
	$this->params['data']['UptimeUrl'] = array();
	$this->params['data']['UptimeUrl']['url_friendly_name'] = $url->url_friendly_name;
	$this->params['data']['UptimeUrl']['url_api_key'] = $url->url_api_key;
	$this->params['data']['UptimeUrl']['url_address'] = $url->url_address;
	$this->params['data']['UptimeUrl']['url_not_email'] = $url->url_not_email;
	$this->params['data']['UptimeUrl']['url_monitor_type'] = $url->url_monitor_type;
	$this->params['data']['UptimeUrl']['url_monitor_subtype'] = $url->url_monitor_subtype;
	$this->params['data']['UptimeUrl']['url_monitor_keywordtype'] = $url->url_monitor_keywordtype;
	$this->params['data']['UptimeUrl']['url_monitor_keywordvalue'] = $url->url_monitor_keywordvalue;
	$this->params['data']['UptimeUrl']['monitor_interval'] = $url->monitor_interval;
    $this->params['data']['UptimeUrl']['http_username'] = $url->http_username;
    $this->params['data']['UptimeUrl']['http_password'] = $url->http_password;
	$this->params['data']['UptimeUrl']['url_id'] = $url->url_id;
}

// For test on local host
global $wpdb;
// process monitor that don't exist
global $current_user;
$orderby = 'wp.url';
global $wpdb;
$sql = 'SELECT url_address FROM ' . $wpdb->prefix . 'aum_urls';
$result = $wpdb->get_results( $sql );
$current_sites_addresses = array();
for ( $i = 0; $i < count( $result ); $i++ ) {
	$current_sites_addresses[ $i ] = $result[ $i ]->url_address;
}
global $mainwpAdvancedUptimeMonitorExtensionActivator;
$pWebsites = apply_filters( 'mainwp-getsites', $mainwpAdvancedUptimeMonitorExtensionActivator->get_child_file(), $mainwpAdvancedUptimeMonitorExtensionActivator->get_child_key(), null );
if ( count( $current_sites_addresses ) > 0 ) {
	foreach ( $pWebsites as $website ) {
		$url = rtrim( $website['url'], '/' );
		if ( ! in_array( $url, $current_sites_addresses ) ) {
			$other_site_urls[ $url ] = stripslashes( $website['name'] );
		}
	}
} else {
	foreach ( $pWebsites as $website ) {
		$url = rtrim( $website['url'], '/' );
		$other_site_urls[ $url ] = stripslashes( $website['name'] );
	}
}


if ( count( $other_site_urls ) > 0 ) {
	$_urls = array_keys( $other_site_urls );
	$tring_result = array_shift( $_urls );
} else {
	$tring_result = 'http://';
}
$edit_url_id  = 0;
$list_notification_contact = Advanced_Uptime_Monitor_Extension::get_install()->get_option( 'list_notification_contact' );
?>
<!-- Add New Monitor Form -->

<div id="popup_message_info_box" class="mainwp-notice mainwp-notice-green" style="display: none"></div>
<div id="popup_message_error_box" class="mainwp-notice mainwp-notice-red" style="display: none"></div>
    <?php $this->display_flash(); ?>
    <?php echo $this->form->create( $model->name ); ?>
<input type="hidden" name="title" value="<?php echo $title ?>" />
<table class="form-table">
	<tbody>
    <?php
	if ( isset( $this->params['url_id'] ) ) {
		$edit_url_id = $this->params['url_id'];
		echo $this->form->hidden_input( 'url_id', array( 'value' => $this->params['url_id'] ) );
	} elseif ( isset( $this->params['data']['UptimeUrl']['url_id'] ))
		$edit_url_id = $this->params['data']['UptimeUrl']['url_id'];
		echo $this->form->hidden_input( 'url_id', array( 'value' => $this->params['data']['UptimeUrl']['url_id'] ) );
	if ( isset( $monitor ) ) {
		echo $this->form->hidden_input( 'monitor_id', array( 'value' => $monitor->monitor_id ) );
	} elseif ($this->params['monitor_id'])
		echo $this->form->hidden_input( 'monitor_id', array( 'value' => $this->params['monitor_id'] ) );

    if ( isset( $this->params['api_key'] ) ) {
        echo $this->form->hidden_input( 'add_monitor_api_key', array( 'value' => $this->params['api_key'] ) );
    }
	
	?>
		<?php if ( ! isset( $this->params['url_id'] ) && empty( $this->params['data']['UptimeUrl']['url_id'] ) && count( $other_site_urls ) > 0 ) { ?>
		<tr>
			<th scope="row"><?php _e( 'Load Child Site', 'advanced-uptime-monitor-extension' ); ?></th>
			<td>
					<input type="checkbox" name="checkbox_show_select" class = "checkbox_show_select" id="checkbox_show_select" <?php echo (isset( $this->params['checkbox_show_select'] ) ? 'checked="checked"' : ''); ?>>                
			</td>
		</tr>
		 <?php } ?>
		<tr><th scope="row" colspan="2"><h3 style="margin: 0;"><?php _e( 'Monitor Information', 'advanced-uptime-monitor-extension' ); ?></h3></th></tr>

		<tr class="monitor_type">
			<th scope="row"><?php _e( 'Monitor Type', 'advanced-uptime-monitor-extension' ); ?> <span style="color: #a00;">*</span></th>
			<td>
			<?php	
			$args = array( 'options' => Advanced_Uptime_Monitor_Extension::get_install()->monitor_types );
			if ( isset( $this->params['data']['UptimeUrl'] ) ) {
				$args['value'] = $this->params['data']['UptimeUrl']['url_monitor_type'];
			}
			echo $this->form->select( 'data[UptimeUrl][url_monitor_type]', $args );
			?>
			</td>
		</tr>

		<tr class="monitor_subtype">
			<th scope="row"><?php _e( 'Monitor Subtype', 'advanced-uptime-monitor-extension' ); ?> <span style="color: #a00;">*</span></th>
			<td>
			<?php
			$monitors_subtypes = array(
				'1' => 'HTTP',
				'2' => 'HTTPS',
				'3' => 'FTP',
				'4' => 'SMTP',
				'5' => 'POP3',
				'6' => 'IMAP',
			);

			$args = array( 'options' => $monitors_subtypes );
			if ( isset( $this->params['data']['UptimeUrl'] ) ) {
				$args['value'] = $this->params['data']['UptimeUrl']['url_monitor_subtype'];
			}
			echo $this->form->select( 'data[UptimeUrl][url_monitor_subtype]', $args );
			?>  
			</td>
		</tr>

		<tr class="url_monitor_keywordtype">
			<th scope="row"><?php _e( 'Alert When:', 'advanced-uptime-monitor-extension' ); ?></th>
			<td>
				<input type="radio" name="data[UptimeUrl][url_monitor_keywordtype]" value=1 id="keywordtype1" <?php echo (isset( $this->params['data']['UptimeUrl'] ) && $this->params['data']['UptimeUrl']['url_monitor_keywordtype'] == '1' ? 'checked=checked' : '') ?> /><label for="keywordtype1"><?php _e( 'exists', 'advanced-uptime-monitor-extension' ); ?></label>&nbsp;
				<input type="radio" name="data[UptimeUrl][url_monitor_keywordtype]" value=2 id="keywordtype2" <?php echo (isset( $this->params['data']['UptimeUrl'] ) && $this->params['data']['UptimeUrl']['url_monitor_keywordtype'] == '2' ? 'checked=checked' : '') ?> /><label for="keywordtype2"><?php _e( 'not exists', 'advanced-uptime-monitor-extension' ); ?></label>
			</td>
		</tr>

		<tr class="url_monitor_keywordvalue">
			<th scope="row"><?php _e( 'Keyword:', 'advanced-uptime-monitor-extension' ); ?></th>
			<td>
			<input type="text" name="data[UptimeUrl][url_monitor_keywordvalue]" value="<?php echo (isset( $this->params['data']['UptimeUrl'] ) ? esc_textarea( $this->params['data']['UptimeUrl']['url_monitor_keywordvalue'] ) : '') ?>">
			</td>
		</tr>

	<?php
	$style_select = 'style="display:none"';
    $style_text = 'style="display:table-row"';
	if ( isset( $this->params['checkbox_show_select'] ) && count( $other_site_urls ) >= 1 ) {
		$style_select = 'style="display:block"';
		$style_text = 'style="display:none"';
	} else {
		$friendly_n = '';
		if ( isset( $this->params['data']['UptimeUrl']['url_friendly_name'] ) ) {
			$friendly_n = $this->params['data']['UptimeUrl']['url_friendly_name'];
		}
	}
	?>
		<tr class="monitor_url_friendly_name_text" <?php echo $style_text; ?>>
			<th scope="row"><?php _e( 'Friendly Name', 'advanced-uptime-monitor-extension' ); ?> <span style="color: #a00;">*</span></th>
			<td>
            <input type="text" name="url_friendly_name_textbox" value="<?php echo esc_attr( $friendly_n ); ?>">
			</td>
		</tr>

		<tr class="monitor_url_friendly_name" <?php echo $style_select; ?>>
			<th scope="row"><?php _e( 'Friendly Name', 'advanced-uptime-monitor-extension' ); ?> <span style="color: #a00;">*</span></th>
			<td>
        <?php if ( $url_saved ) { ?>
            <input type="text" name="data[UptimeUrl][url_friendly_name]" value="<?php echo esc_attr( $this->params['data']['UptimeUrl']['url_friendly_name'] ); ?>">
            <?php
} else {
	$args = array( 'options' => $other_site_urls, 'value' => $this->params['data']['UptimeUrl']['url_friendly_name'] );
	echo $this->form->select( 'data[UptimeUrl][url_friendly_name]', $args );
}
		?>
			</td>
		</tr>

		<tr>
			<th scope="row"><?php _e( 'URL (or IP)', 'advanced-uptime-monitor-extension' ); ?> <span style="color: #a00;">*</span></th>
			<td>
			<?php
					if ( isset( $this->params['data']['UptimeUrl']['url_address'] ) ) {
						echo $this->form->input( 'url_address', array( 'label' => '', 'value' => $this->params['data']['UptimeUrl']['url_address'] ) );
					} else {
						echo $this->form->input( 'url_address', array( 'label' => '', 'value' => 'http://' ) );
					}
			?>
			</td>
        </tr>        
	  	<?php
		if ( isset( $this->params['data']['UptimeUrl'] ) && ! empty( $this->params['data']['UptimeUrl']['url_api_key'] ) ) {
				echo $this->form->input( 'url_api_key', array( 'label' => __( 'Monitor API key:', 'advanced-uptime-monitor-extension' ), 'value' => (isset( $this->params['data']['UptimeUrl'] ) ? $this->params['data']['UptimeUrl']['url_api_key'] : '') ) );
		}
		?>
		
        <tr>
			<th scope="row"><?php _e( 'Monitoring Interval', 'advanced-uptime-monitor-extension' ); ?> <span style="color: #a00;">*</span></th>
			<td>
            <?php echo $this->form->input( 'monitor_interval', array( 'label' => '', 'value' => !empty($this->params['data']['UptimeUrl']['monitor_interval'] ) ? $this->params['data']['UptimeUrl']['monitor_interval'] : 5, 'after' => ' <em>minutes</em>' ) ); ?>
			</td>
        </tr>        
		<?php if ( is_array( $list_notification_contact ) && count( $list_notification_contact ) > 0 ) { ?>
    <?php
	$alert_contacts = array();
	if ( ! empty( $this->params['monitor_contacts_notification'] ) ) {
		$alert_contacts = explode( '-', $this->params['monitor_contacts_notification'] );
	} else if ( isset( $this->params['data']['UptimeUrl']['url_not_email'] ) && count( $this->params['data']['UptimeUrl']['url_not_email'] ) > 0 ) {
		$alert_contacts = $this->params['data']['UptimeUrl']['url_not_email'];
	}
        
	$default_contact_id = Advanced_Uptime_Monitor_Extension::get_install()->get_option( 'uptime_default_notification_contact_id' );
		?>

		<tr class="aum_contacts_list">
			<th scope="row"><?php _e( 'Select Alert Contacts to Notify', 'advanced-uptime-monitor-extension' ); ?> <span style="color: #a00;">*</span></th>
			<td>
				<?php 
	if ( count( $alert_contacts ) > 0 ) {
		foreach ( $list_notification_contact as $key => $val ) {
			$checked_flag = '';
			if ( in_array( $key, $alert_contacts ) ) {
				$checked_flag = 'checked="checked"';
			}

            echo '<label class="label2"><input type="checkbox" name="checkbox_contact" value="' . esc_attr( $key ) . '"' . $checked_flag . '> ' . esc_html( $val ) . '</label><br/>';
		}
	} else {

		foreach ( $list_notification_contact as $key => $val ) {
			if ( $default_contact_id == $key ) {
				$checked = empty( $this->params['data']['UptimeUrl']['url_id'] ) ? ' checked="checked"' : '';				
			} else {
				$checked = '';
			}
            echo '<label class="label2"><input type="checkbox" name="checkbox_contact" value="' . esc_attr( $key ) . '" ' . $checked . '> ' . esc_html( $val ) . '</label><br/>';
		}
	}
	?>            
			<div class="mainwp-notice mainwp-notice-yellow"><?php _e( 'New alert contacts can be defined from the "My Settings" page on your Uptime Robot account.', 'advanced-uptime-monitor-extension' ); ?></div>
			</td>
		</tr>
		<tr style="background: #eee;"><th scope="row" colspan="2" style="padding-left: 10px;"><h3 style="margin: 0;"><?php _e( 'HTTP Basic Authentication Settings', 'advanced-uptime-monitor-extension' ); ?></h3></th></tr>
        <tr style="background: #eee;">
			<th scope="row" style="padding-left: 10px;"><?php _e( 'HTTP Username (Optional)', 'advanced-uptime-monitor-extension' ); ?></th>
			<td>
            <?php echo $this->form->input( 'http_username', array( 'label' => '', 'value' => !empty($this->params['data']['UptimeUrl']['http_username'] ) ? $this->params['data']['UptimeUrl']['http_username'] : '') ); ?>
            <em><?php _e( 'Use only if your child site is protected with HTTP Basic Authentication. Not required for Ping and Port monitor types.', 'advanced-uptime-monitor-extension' ); ?></em>
			</td>
        </tr>        
        <tr style="background: #eee;">
			<th scope="row" style="padding-left: 10px;"><?php _e( 'HTTP Password (Optional)', 'advanced-uptime-monitor-extension' ); ?></th>
			<td>
            <?php echo $this->form->input( 'http_password', array( 'label' => '', 'value' => !empty($this->params['data']['UptimeUrl']['http_password'] ) ? $this->params['data']['UptimeUrl']['http_password'] : '' ) ); ?>
            <em><?php _e( 'Use only if your child site is protected with HTTP Basic Authentication. Not required for Ping and Port monitor types.', 'advanced-uptime-monitor-extension' ); ?></em>
			</td>
        </tr>
		<?php } ?>
	</tbody>
    </table>     
<input type="hidden" name="monitor_contacts_notification" value="<?php echo (isset( $this->params['monitor_contacts_notification'] ) ? esc_attr( $this->params['monitor_contacts_notification'] ) : esc_attr( $default_contact_id )); ?>" />
<?php
$timestamp = time();
$submit_text = empty( $this->params['data']['UptimeUrl']['url_id'] ) ? __( 'Create Monitor', 'advanced-uptime-monitor-extension' ) : __( 'Save Monitor', 'advanced-uptime-monitor-extension' );
?>
<input type="hidden" name="action" value="admin_uptime_monitors_url_form">
<input type="hidden" name="wp_nonce" value="<?php echo wp_create_nonce( 'aum_nonce_url_form' ); ?>">
<p>
	<input type="button" value="<?php echo $submit_text; ?>" class="button-primary button-hero button" id="aum_edit_monitor_button_<?php echo $timestamp;?>">
</p>
</form>
<!-- End Add New Monitor Form -->

<script type="text/javascript">
    jQuery(document).ready(function () {        
		jQuery( '#aum_edit_monitor_button_<?php echo $timestamp; ?>' ).live('click', function (event) {	
			event.preventDefault();
			var errors = [];			
			jQuery('div.error').hide();
            
            var contacts_chossen = '';
            jQuery('input:checkbox[name=checkbox_contact]').each(function () {
                if (this.checked)
                {   
                    contacts_chossen += jQuery.trim(jQuery(this).val()) + '-';
                }
            });
            contacts_chossen = contacts_chossen.slice(0, -1);
            jQuery('input[name=monitor_contacts_notification]').val(contacts_chossen);
            
			if (jQuery('input[name=monitor_contacts_notification]').val() == '')
            {           
				errors.push('Please select at least one Alert Contact');            
            }
			if (!jQuery('input[name=checkbox_show_select]').is(':checked') && jQuery('input[name=url_friendly_name_textbox]').val() == '')
            {           
				errors.push('Please enter Monitor Friendly Name');
                
            }
			if (jQuery('#UptimeUrlUrlAddress').val() == '')
            {      
				errors.push('Please enter URL (or IP)');
                
            }
			
			if (errors.length > 0) {
				jQuery('#popup_message_error_box').html(errors.join('<br />')).show();             
				return false;
			}
			
            jQuery('div.monitor_url_friendly_name select option').each(function () {
                var html_name = jQuery(this).html();
                jQuery(this).val(html_name);
            })
			
			jQuery('#popup_message_info_box').html('<div><i class="fa fa-spinner fa-pulse"></i> Working...<div>').show();
			jQuery('#popup_message_error_box').html('').hide();			
			jQuery(this).attr('disabled', 'disabled');		
			var data = jQuery(this).closest('form').serialize();
			jQuery.ajax({
				url: ajaxurl,
				type: 'GET',
				data: data,
				error: function () {					
					jQuery('#aum-popup-box').html('Error - Please, try again later');                
				},
				success: function (response) {					
					jQuery('#aum-popup-box').html(response);                
				},
				timeout: 20000
			});
			return false;
		});

        jQuery('input[name=checkbox_show_select]').change(function () {
            var item = '<?php echo $tring_result; ?>';
            if (jQuery(this).is(':checked')) {
                jQuery('tr.monitor_url_friendly_name_text').hide(); // css('display', 'none');
                jQuery('tr.monitor_url_friendly_name').show(); // css('display', 'block');
                jQuery('tr.monitor_url_friendly_name div select').prop('selectedIndex', 0);

                jQuery('input#UptimeUrlUrlAddress').val(item);
                jQuery('input#UptimeUrlUrlAddress').attr('readonly', 'readonly');
            } else {
                jQuery('tr.monitor_url_friendly_name').hide(); //css('display', 'none');
                jQuery('tr.monitor_url_friendly_name_text').show(); //css('display', 'block');
                jQuery('tr.monitor_url_friendly_name_text input').val('');
                jQuery('input#UptimeUrlUrlAddress').val('http://');
                jQuery('input#UptimeUrlUrlAddress').removeAttr('readonly');
            }
        });

        jQuery('.monitor_type select').change(function () {
            if (jQuery(this).val() == '4')
                jQuery('.monitor_subtype').show();
            else
                jQuery('.monitor_subtype').hide();

            if (jQuery(this).val() == '2') {
                jQuery('.url_monitor_keywordtype').show();
                jQuery('.url_monitor_keywordvalue').show();
            } else {
                jQuery('.url_monitor_keywordtype').hide();
                jQuery('.url_monitor_keywordvalue').hide();
            }
        })

        jQuery('div.monitor_url_address select').attr('disabled', 'disabled');

        jQuery('.monitor_url_friendly_name').change(function ()
        {
            var select = jQuery('.monitor_url_friendly_name select option:selected').val();
            jQuery('input#UptimeUrlUrlAddress').val(select);
        })
        
        jQuery('tr.aum_contacts_list').change(function () {            
//            var contacts_chossen = '';
//            jQuery('input:checkbox[name=checkbox_contact]').each(function () {
//                if (this.checked)
//                {                  
//                    // jQuery.trim(contacts_chossen);  
//                    contacts_chossen += jQuery.trim(jQuery(this).val()) + '-';
//                }
//            });
//            contacts_chossen = contacts_chossen.slice(0, -1);
//            jQuery('input[name=monitor_contacts_notification]').val(contacts_chossen);
        });
<?php
if ( isset( $this->params['data']['UptimeUrl'] ) && $this->params['data']['UptimeUrl']['url_monitor_type'] == '4' ) {
	echo "jQuery('.monitor_subtype').show();";
}

if ( isset( $this->params['data']['UptimeUrl'] ) && $this->params['data']['UptimeUrl']['url_monitor_type'] == '2' ) {
	echo "jQuery('.url_monitor_keywordtype').show();jQuery('.url_monitor_keywordvalue').show();";
}
if ( $url_saved ) {
	?>
            jQuery('.url_form').find('input').attr('disabled', 'disabled');
            jQuery('.url_form').find('select').attr('disabled', 'disabled');
            jQuery('.url_form').find('textarea').attr('disabled', 'disabled');
            setTimeout(function () {
//                parent.location.reload(true);
                location.href = location.href
            }, 3000);
    <?php
}
?>
    });
</script>

