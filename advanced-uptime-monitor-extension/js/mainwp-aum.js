jQuery( document ).ready(function () {
	jQuery( '#infobox-uptime' ).insertBefore( '#mainwp-tabs' );
	jQuery( '#errorbox-uptime' ).insertBefore( '#mainwp-tabs' );
	jQuery( '#aum_monitor_reload' ).live('click', function () {
		jQuery( this ).closest( 'form' ).submit();
	})
        
	jQuery( '#mainwp-ur-settings .handlediv' ).live('click', function () {		
		var pr = jQuery( this ).closest( 'div.postbox' );
		if (pr.hasClass( 'closed' )) {                    
                    mainwp_aum_set_showhide_section(pr, true);
                } else {
                    mainwp_aum_set_showhide_section(pr, false);
                }
	});

	urm_apply_check = function (me, event) {

		var action = jQuery( '#aum_mainwp_widget_uptime_monitor_option_page select[name=monitor_action]' ).val();
		var number_checked = -1;
		number_checked = jQuery( '.aum-monitors-tbl input[name=checkbox_url]:checkbox:checked' ).length;

		if (number_checked < 1) {
			alert( 'Please select at least one monitor!' );
			return;
		} else {
			//jQuery.ajaxSetup({async:false});
			switch (action) {
				case 'display':
				case 'hidden':
                                        jQuery( '.aum-monitors-tbl input[name=checkbox_url]:checked' ).each(function () {
						click_link = jQuery( this ).closest('tr').find( 'span.url_showhide_link' );
						urm_showhide_monitor_in_widget( click_link, action );                                                
					});                                        
					break;
				case 'delete':
					if ( ! confirm( 'Are you sure to delele selected monitors?' )) {
						break; 
                                        }
					jQuery( '.aum-monitors-tbl input[name=checkbox_url]:checked' ).each(function () {
						click_link = jQuery( this ).closest('tr').find( 'span.url_delete_link' );
						urm_delete_monitor_button( click_link );
					});
					break;
				case 'pause':
					jQuery( '.aum-monitors-tbl input[name=checkbox_url]:checked' ).each(function () {
						url_row_obj = jQuery( this ).closest('tr');
						if (url_row_obj.find( '.aum_action_link' ).hasClass( 'pause' )) {
							url_row_obj.find( 'div#loading_status' ).show();
							url_row_obj.find( '.pause' ).click();
						}
						jQuery( this ).removeAttr( 'checked' );
					});
					break;
				case 'start':
					jQuery( '.aum-monitors-tbl input[name=checkbox_url]:checked' ).each(function (event) {
						url_row_obj = jQuery( this ).closest('tr');
						if (url_row_obj.find( '.aum_action_link' ).hasClass( 'start' )) {
							url_row_obj.find( 'div#loading_status' ).show();
							url_row_obj.find( '.start' ).click();
						}
						jQuery( this ).removeAttr( 'checked' );
					});
					break;
				default:
					break;
			}
			jQuery( '.aum-monitors-tbl input[name=checkall]' ).removeAttr( 'checked' );
		}
	}
	
	
	jQuery( 'body' ).mousemove(function (event) {
		if (jQuery( '.aumloading' ).length > 0) {
			jQuery( '.aumloading' ).css( 'left', (event.pageX + 17) + 'px' );
			jQuery( '.aumloading' ).css( 'top', (event.pageY + 27) + 'px' );
		}
	})
})


function aum_popup_box(action, item_id, nonce, title, pPage) {       
        
        jQuery('#aum-popup-box').attr('title', title);
        jQuery('#aum-popup-box').html('<div><i class="fa fa-spinner fa-pulse"></i> Loading...</div>');        

        jQuery('#aum-popup-box').dialog({
            resizable: false,
            height: 550,
            width: 700,
            modal: true,
            close: function(event, ui) {jQuery('#aum-popup-box').dialog('destroy'); /* location.href = location.href; */}
        });
        
        var data = {
            action: 'admin_uptime_monitors_' + action,            
            wp_nonce: nonce
        };
       
        if (action == 'statistics_table' || action == 'update_url' )
            data['url_id'] = item_id;
        else 
            data['monitor_id'] = item_id;
        
        if (action == 'statistics_table') {
            if ( typeof(pPage) != 'undefined'  ) {
                data['stats_page'] = pPage;
            }
        }
            
        jQuery.ajax({
            url: ajaxurl,
            type: "POST",
            data: data,
            error: function () {
                jQuery('#aum-popup-box').html('Undefined Error: Please, try again later');                
            },
            success: function (response) {
                jQuery('#aum-popup-box').html(response);                
            },
            timeout: 20000
        });
}
function show_loading(event) {
	jQuery( 'body' ).append( '<i class="fa fa-spinner fa-pulse aumloading"></i>' );
	jQuery( '.aumloading' ).css( 'left', (event.pageX + 17) + 'px' );
	jQuery( '.aumloading' ).css( 'top', (event.pageY + 27) + 'px' );
}
function hide_loading() {
	jQuery( '.aumloading' ).remove();
}

urm_add_new_monitor_button = function (me, event, monitor_id, nonce) {
        aum_popup_box( 'url_form', monitor_id, nonce, 'New monitor' );
}

urm_delete_monitor_button = function (me, nonce) {
	var url_row_obj = me.closest('tr');
	url_row_obj.find( 'div#loading_status' ).show();
	jQuery.post(ajaxurl, {action: 'admin_uptime_monitors_delete_url', 'url_id': me.parent().attr( 'url_id' ), 'wp_nonce': nonce}, function (response) {
		if (response == 'success') {
			url_row_obj.html( '<td colspan="8">Monitor has been deleted.</td>' );
		} else {
			url_row_obj.find( 'div#loading_status' ).hide(); 
                }
	});
}
//jQuery('.url_delete_link').bind('click',function(){
urm_dashboard_delete_monitor_button = function (me, event, nonce) {
	show_loading( event );
	jQuery.post(ajaxurl, {action: 'admin_uptime_monitors_delete_url', 'url_id': jQuery( me ).parent().attr( 'url_id' ), 'wp_nonce': nonce}, function (response) {
		if (response == 'success') {
			jQuery( me ).closest('tr').remove(); 
                }
		hide_loading( event );
	});
}

//jQuery('.url_edit_link').click(function(event){
urm_edit_monitor_button = function (me, url_id, nonce) {
        aum_popup_box( 'update_url', url_id, nonce, 'Edit Monitor');
}
//jQuery('.status_link').click(function(event){
urm_status_monitor_button = function (me, url_id, event, nonce) {
	var current_status = jQuery( me ).hasClass( 'start' ) ? 'start' : 'pause';
	var status_link_obj = jQuery( me );
	var data = {
		action: 'admin_uptime_monitors_url_' + (jQuery( me ).hasClass( 'start' ) ? 'start' : 'pause'),
		url_id: url_id,
		wp_nonce: nonce
	};
	show_loading( event );
	jQuery( me ).closest( 'tr' ).find( 'div#loading_status' ).show();
	jQuery.post(ajaxurl, data, function (response) {
		jQuery( me ).closest( 'tr' ).find( 'div#loading_status' ).hide();
		hide_loading();
		if (response == 'success') {
                    if (current_status == 'start') {
                        status_link_obj.removeClass( 'start' ).addClass( 'pause' );
                        status_link_obj.find( 'i' ).removeClass( 'fa-play' ).addClass( 'fa-pause' );
                        // to fix
                        var last_stat = status_link_obj.closest('tr').find( '.last_event' ).attr( 'last_event' );
                        if (last_stat == 'paused')
                            last_stat = 'up';
                        
                        status_link_obj.closest('tr').find( '.aum_upm_status' ).removeClass( 'paused' ).addClass( last_stat );
                    } else {
                        status_link_obj.removeClass( 'pause' ).addClass( 'start' );
                        status_link_obj.find( 'i' ).removeClass( 'fa-pause' ).addClass( 'fa-play' );
                        status_link_obj.closest('tr').find( '.aum_upm_status' ).removeClass( 'down' ).removeClass( 'up' ).removeClass( 'seems_down' ).removeClass( 'not_checked' ).addClass( 'paused' );
                    }
                }                
                jQuery( me ).closest('tr').find( 'div#loading_status' ).hide();
	});
}

urm_showhide_monitor_in_widget_onclick = function (me) {
    var show = me.attr('show-hide');
    var action = (show ==1) ? 'hidden' : 'show';
    urm_showhide_monitor_in_widget(me, action);    
}

urm_showhide_monitor_in_widget = function (me, action) {	
        var url_row_obj = me.closest('tr');
	url_row_obj.find( 'div#loading_status' ).show();	
        var show = 1;
        var icon = '<i class="fa fa-eye mainwp-green" title="Monitor is displayed in Widget" aria-hidden="true"></i>';                                                
        if (action == 'hidden') {
            show = 0;
            icon = '<i class="fa fa-eye-slash" title="Monitor is hidden in Widget" aria-hidden="true"></i>';
        }						
        jQuery.post(
            ajaxurl,
        {
            action: 'admin_uptime_monitors_display_dashboard',
            url_id: url_row_obj.find( '.url_actions' ).attr( 'url_id' ),
            dashboard: show,
            wp_nonce: jQuery( "#mainwp_aum_extension_display_dashboard_nonce" ).val()
        },
        function (response, status) {
            me.attr('show-hide', show);
            url_row_obj.find( 'input[name=checkbox_url]' ).removeAttr( 'checked' ); // for bulk actions            
            url_row_obj.find( 'div#loading_status' ).hide();            
            url_row_obj.find( 'span.monitor_status' ).html( icon );
        });
}

urm_stats_monitor_button = function (url_id, nonce, pPage ) {	
    aum_popup_box( 'statistics_table', url_id, nonce, 'Monitor Statistics And Reports', pPage);
}