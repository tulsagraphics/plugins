<?php
class MainWPGA
{
    public $security_nonces;
    private static $redirect_uri;

    public function __construct()
    {
//       require_once MAINWP_GA_PLUGIN_DIR.'/vendor/autoload.php';
        self::$redirect_uri = admin_url("admin.php?page=Extensions-Mainwp-Google-Analytics-Extension&mainwp_ga=1");
    }

    public function init()
    {
        add_action('mainwp_update_site', array(&$this, 'mainwp_update_site'));
        add_action('mainwp_delete_site', array(&$this, 'mainwp_delete_site'));
        add_action('mainwp_ga_delete_site', array(&$this, 'ga_delete_site'));
        add_action('mainwp_extension_sites_edit_tablerow', array(&$this, 'mainwp_extension_sites_edit_tablerow'));
        add_filter('mainwp_ga_get_data', array(&$this, 'get_ga_data'), 10, 4);
        $this->init_ajax();
    }

    public function init_ajax()
    {
        add_action('wp_ajax_mainwp_ga_disconnect', array(&$this, 'mainwp_ga_disconnect')); //ok
        add_action('wp_ajax_mainwp_ga_connect', array(&$this, 'mainwp_ga_connect')); //ok
        add_action('wp_ajax_mainwp_ga_getstats', array(&$this, 'mainwp_ga_getstats')); //ok
    }

    public static function getClassName()
    {
        return __CLASS__;
    }

     public static function autoloadVendor() {
       require_once MAINWP_GA_PLUGIN_DIR.'/vendor/autoload.php';
    }

    //Renders the top of the widget (name + dropdown)
    public static function getName()
    {
        $name = '<i class="fa fa-line-chart"></i> ' . __('Google Analytics','mainwp-google-analytics-extension');
        return $name;
    }

    //Renders the content of the widget
    public static function render_metabox()
    {
        // to fix
        self::handleUpdateGA();

        $gaSites = MainWPGA::getAvailableSites();
        MainWPGAUtility::array_sort( $gaSites, 'name' );

        if (count($gaSites) > 0) {
            $selection = '';
            if (count($gaSites) > 0) {
                $selection .= '<div id="mainwp_ga_metabox_buttons" class="mainwp-row-top">';
                $selection .= '<a href="admin.php?page=Extensions-Mainwp-Google-Analytics-Extension" class="button button-ga-add">' . __('Add GA Account','mainwp-google-analytics-extension') . '</a>&nbsp;&nbsp;&nbsp;';
                $selection .= '<input type="button" name="mainwp_ga_updatenow" id="mainwp_ga_updatenow" class="button-primary button-ga-refresh" value="'. __('Refresh Now','mainwp-google-analytics-extension') .'"/>';
                $selection .= '<span class="button-ga-select"><select name="mainwp_widget_ga_site" id="mainwp_widget_ga_site">';
                foreach ($gaSites as $site)
                {
                    $selection .= '<option value="' . $site['id'] . '">' . $site['name'] . '</option>';
                }
                $selection .= '</select></span>';
                $selection .= '</div><br />';
            }
            echo $selection . '';
            ?>
            <div id="mainwp_widget_ga_content" style="margin-top: 1em;"><p style="text-align:center"><i class="fa fa-spinner fa-pulse fa-5x"></i></p></div>
            <script type="text/javascript">
                google.charts.load('current', {'packages':['corechart']});
                jQuery(document).ready(function() {
                    mainwp_ga_getstats();
                });
            </script>
        <?php
        } else {
            ?>
            <div id="mainwp_widget_ga_content">
                <?php _e('No statistics available.','mainwp-google-analytics-extension'); ?>
            </div>
        <?php
        }
    }



    private static function getStatsInt($profile_id, $gas_id, $startDate, $endDate, $full = false, $graph = false)
    {
        $ga_entry = MainWPGADB::Instance()->getGAEntryBy('id', $gas_id);

        if (empty($ga_entry) || empty($ga_entry->refresh_token))
            return false;

        self::autoloadVendor();

        try
        {
            $client = new Google_Client();
            $client->setApplicationName("Google Analytics Application");
            $client->setClientId($ga_entry->client_id);
            $client->setClientSecret($ga_entry->client_secret);
            $client->setRedirectUri(self::$redirect_uri);
            $client->setAccessType('offline');

            $results = array();

            $access_token = array();
//            if  (isset($_SESSION['MainWP_GA_Access_Token'])) {
//                $access_token = $_SESSION['MainWP_GA_Access_Token'];
//                $client->setAccessToken($access_token);
//            }

            if (empty($access_token) || $client->isAccessTokenExpired()) {
                $access_token = $client->refreshToken($ga_entry->refresh_token);
                //$_SESSION['MainWP_GA_Access_Token'] = $access_token;
            }

            $service = new Google_Service_Analytics($client);

            if (!empty($access_token)) {
                if ($graph) { //Graph stats requested
                    $metrics = array('ga:sessions');
                }
                else
                { //Normal info requested
                    $metrics = array('ga:sessions', 'ga:bounceRate', 'ga:pageviews', 'ga:avgSessionDuration', 'ga:pageviewsPerSession', 'ga:percentNewSessions');
                }
                $metrics = implode(',', $metrics);

                $dimensions = array();
                if ($graph) { //Graph stats requested (link the stats to the visits)
                    $dimensions = array('ga:day' => 'ga:sessions', 'ga:month' => 'ga:sessions', 'ga:year' => 'ga:sessions');
                }
                else
                {
                    if ($full) { //Full stats requested (link the stats to the visits)
                        $dimensions = array('ga:source' => 'ga:sessions', 'ga:keyword' => 'ga:sessions', 'ga:country' => 'ga:sessions', 'ga:pagePath' => 'ga:pageviews');
                    }
                }
                $options = array();
                if (count($dimensions) > 0) {
                    $options['dimensions'] = implode(',', array_keys($dimensions));
                }

                $results = $service->data_ga->get(
                            'ga:' . $profile_id, $startDate, $endDate, $metrics, $options
                           );
            }
        }
        catch (Exception $e)
        {
            $error = $e->getMessage();
            die(__('An error occured: ' , 'mainwp-google-analytics-extension') . $error . '<br />' . __('Please check your settings, try reconnecting to your Google Analytics account in the settings.','mainwp-google-analytics-extension'));
        }

        if (!is_object($results) || !isset($results->columnHeaders)) {
            die(__('An error occured' , 'mainwp-google-analytics-extension'));
        }

        $outputs = array();

        // for backward compatible
        $convert_index_to_name = $metric_indexs = array();
        foreach($results['columnHeaders'] as $index => $column ) {
            $convert_index_to_name[$index] = $column['name'];
            if ($column['columnType'] == 'METRIC') {
                $metric_indexs[] = $index;
            }
        }

        //Metrics:
        $aggregates = array();
        foreach ($results['totalsForAllResults'] as $mt => $val)
        {
            $aggregates[$mt] = $val;
        }
        $outputs['aggregates'] = $aggregates;

        if ($full) {
            //Dimensions:
            $individual = array();

            foreach ($results['rows'] as $row)
            {
                $entry_metrics = array();
                $entry_dimensions = array();
                foreach($row as $idx => $value) {
                    $name = $convert_index_to_name[$idx];
                    if (in_array($idx, $metric_indexs)) {
                        $entry_metrics[$name] = $value;
                    } else {
                        $entry_dimensions[$name] = $value;
                    }
                }

                //Count the visits/pageviews/... (whatever is set in the dimensions array above)
                foreach ($dimensions as $dimension => $metric)
                {
                    if ($entry_dimensions[$dimension] != '(not set)') {
                        if (isset($individual[$dimension][$entry_dimensions[$dimension]])) {
                            $individual[$dimension][$entry_dimensions[$dimension]] += $entry_metrics[$metric];
                        }
                        else
                        {
                            $individual[$dimension][$entry_dimensions[$dimension]] = $entry_metrics[$metric];
                        }
                    }
                }

            }
            $outputs['individual'] = $individual;
        }

        if ($graph) {
            //Graph dimensions
            $daystats = array();
            foreach ($results['rows'] as $row)
            {
                $entry_metrics = array();
                $entry_dimensions = array();
                foreach($row as $idx => $value) {
                    $name = $convert_index_to_name[$idx];
                    if (in_array($idx, $metric_indexs)) {
                        $entry_metrics[$name] = $value;
                    } else {
                        $entry_dimensions[$name] = $value;
                    }
                }

                //Count the visitors per day
                foreach ($dimensions as $dimension => $metric)
                {
                    $daystats[$entry_dimensions['ga:year']][$entry_dimensions['ga:month']][$entry_dimensions['ga:day']] = $entry_metrics[$metric];
                }
            }

            $outputs['daystats'] = $daystats;
        }
        return $outputs;
    }


    function get_ga_data($websiteid, $start_date, $end_date, $graph = false) {
        $website = MainWPGADB::Instance()->getGAGASId($websiteid);
        if ($end_date - $start_date <= 24 * 60 * 60 ) { // to fix one day period issue
            $end_date += 24 * 60 * 60;
        }
        $startDate = date('Y-m-d', $start_date);
        $endDate = date('Y-m-d', $end_date);
        $return = array();

        $property_ids = MainWPGADB::Instance()->getGASettingGlobal('propertyIds');
        if ($property_ids != '')
        {
            $property_ids = json_decode($property_ids, 1);
        }

        if (!is_array($property_ids))
            $property_ids = array();

        if ($website) {
            $id = $website->ga_id;
            $gas_id = $website->gas_id;
            $profile_id = isset($property_ids[$id]) ? $property_ids[$id] : 0;
            if (empty($profile_id))
                return;

            $return['stats_int'] = MainWPGA::getStatsInt($profile_id, $gas_id, $startDate, $endDate, false);

            if ($graph) {
                $valuesGraph = MainWPGA::getStatsInt($profile_id, $gas_id, $startDate, $endDate, false, true);
                $graphData = array();

                //===============================================================
                //enym: modified stepping
                //$step = ($end_date - $start_date) / (10 * 24 * 60 * 60);
                //$step = ($end_date - $start_date) / (31 * 24 * 60 * 60); //31 days is more robust than "1 month" and must match daterange in MainWPCReport.class.php
                //===============================================================

                for ($i = $start_date; $i <= $end_date; $i = $i + 24 * 60 * 60)
                {
                    $currVal = (int)$valuesGraph['daystats'][date('Y', $i)][date('m', $i)][date('d', $i)];
                    $idate = date('M j', $i);
                    $format_date = apply_filters('mainwp_ga_visit_chart_date', $i, $websiteid);
                    if ($i !== $format_date && !empty($format_date)) {
                        $graphData[] = array($idate, $currVal, $format_date);
                    } else {
                        $graphData[] = array($idate, $currVal);
                    }
                }

                $valuesGraph = json_encode($graphData);

                //===============================================================
                //enym: new line to send raw graph data to the client reports extension
                $return['stats_graphdata'] = $graphData;
                //===============================================================
            }
        }
        return $return;
    }

    //Renders the stats on the widget (from db or fetches them via getStatsInt)
    public static function getStats($websiteid)
    {

            $website = MainWPGADB::Instance()->getGAGASId($websiteid);
            $cacheCheck = MainWPGADB::Instance()->getGACache($websiteid);
            $interval = MainWPGADB::Instance()->getGASettingGlobal('update_interval');

            $property_ids = MainWPGADB::Instance()->getGASettingGlobal('propertyIds');
            if ($property_ids != '')
            {
                $property_ids = json_decode($property_ids, 1);
            }

            if (!is_array($property_ids))
                $property_ids = array();

            // to debug
            //$cacheCheck = null;

            if ($cacheCheck == null) { //Update Cache
                $id = $website->ga_id;
                $gas_id = $website->gas_id;
                $profile_id = isset($property_ids[$id]) ? $property_ids[$id] : 0;
                if (empty($profile_id))
                    return;

                $week = 7;
                $month = 30;

                $days = $week;
                if ($interval == 'week') {
                    $days = $week;
                }
                else
                {
                    $days = $month;
                }
                $startDate = date('Y-m-d', strtotime('-' . $days . ' day'));
                $endDate = date('Y-m-d', strtotime('-1 day'));
                $start2Date = date('Y-m-d', strtotime('-' . (2 * $days) . ' day'));
                $end2Date = date('Y-m-d', strtotime('-' . (1 + $days) . ' day'));

                $values = MainWPGA::getStatsInt($profile_id, $gas_id, $startDate, $endDate, true);
                $valuesPrev = MainWPGA::getStatsInt($profile_id, $gas_id, $start2Date, $end2Date);

                $valuesGraph = MainWPGA::getStatsInt($profile_id, $gas_id, $start2Date, $endDate, false, true);

                $end = strtotime('-1 day');
                $start = strtotime('-' . $days . ' day');
                $graphData = array();
                for ($i = $start; $i <= $end; $i = $i + (24 * 60 * 60))
                {
                    $currVal = (int)$valuesGraph['daystats'][date('Y', $i)][date('m', $i)][date('d', $i)];
                    $previousTime = $i - ($days * 24 * 60 * 60);
                    $prevVal = (int)$valuesGraph['daystats'][date('Y', $previousTime)][date('m', $previousTime)][date('d', $previousTime)];
                    $graphData[] = array(date('M j', $i), $currVal, $prevVal);
                }
                $valuesGraph = json_encode($graphData);
                MainWPGADB::Instance()->updateGACache($websiteid, json_encode($values), json_encode($valuesPrev), $valuesGraph);
            }
            else
            {
                $values = json_decode($cacheCheck->statsValues, true);
                $valuesPrev = json_decode($cacheCheck->statsValuesPrev, true);
                $valuesGraph = $cacheCheck->graphValues;
            }
            //Showing stats:

            echo '
        <div id="chart_div"></div>
        <script>

        function drawGraph() {
            var data = new google.visualization.DataTable();
            data.addColumn("string", "category");
            data.addColumn("number", "Visits");
            data.addColumn("number", "Visits");
            data.addRows(' . $valuesGraph . ');

            var chart = new google.visualization.AreaChart(document.getElementById("chart_div"));
            chart.draw(data, {width: "100%",
                height: 190,
                legend:"none",
                pointSize: 5,
                backgroundColor: { fill: "none" },
                chartArea: {top:5, width: "90%", height: "80%"},
                hAxis: {gridlineColor: "red", slantedText: false, maxAlternation: 1' . ($interval == 'month'
                        ? ', showTextEvery: 3' : '') . '},
                format:"#",
                vAxis: {maxValue: 2},
                series: [{color: "#0078CE"},{color: "#87BE2C", areaOpacity: 0}]
                });
        }

          drawGraph();
        </script>';
            ?>
        <div class="mainwp_ga_graph_legend">
            <span><span id="mainwp_ga_graph_legend_blue">&nbsp;</span>this <?php echo $interval; ?></span><span><span
                id="mainwp_ga_graph_legend_green">&nbsp;</span>last <?php echo $interval; ?></span>
        </div>
        <table class="mainwp_ga_table">
            <tr>
                <td><?php MainWPGA::getStats_print_aggregate(__('Visits','mainwp-google-analytics-extension'), $valuesPrev['aggregates']['ga:sessions'], $values['aggregates']['ga:sessions']); ?></td>
                <td><?php MainWPGA::getStats_print_aggregate(__('Bounce Rate','mainwp-google-analytics-extension'), $valuesPrev['aggregates']['ga:bounceRate'], $values['aggregates']['ga:bounceRate'], true, true); ?></td>
            </tr>
            <tr>
                <td><?php MainWPGA::getStats_print_aggregate(__('Pageviews','mainwp-google-analytics-extension'), $valuesPrev['aggregates']['ga:pageviews'], $values['aggregates']['ga:pageviews']); ?></td>
                <td><?php MainWPGA::getStats_print_aggregate(__('Avg. Time on Site','mainwp-google-analytics-extension'), $valuesPrev['aggregates']['ga:avgSessionDuration'], $values['aggregates']['ga:avgSessionDuration'], false, false, true); ?></td>
            </tr>
            <tr>
                <td><?php MainWPGA::getStats_print_aggregate(__('Pages/Visit','mainwp-google-analytics-extension'), $valuesPrev['aggregates']['ga:pageviewsPerSession'], $values['aggregates']['ga:pageviewsPerSession'], true); ?></td>
                <td><?php MainWPGA::getStats_print_aggregate(__('New Visits','mainwp-google-analytics-extension'), $valuesPrev['aggregates']['ga:percentNewSessions'], $values['aggregates']['ga:percentNewSessions'], true, true); ?></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right;"><a href="#" id="mainwp_ga_show_extra"><i class="fa fa-eye-slash"></i> Show <span>more</span></a></td>
            </tr>
        </table>
        <table class="mainwp_ga_table" id="mainwp_ga_extra" style="display: none;">
        <col style="width: 100%">
            <tr>
                <td><?php MainWPGA::getStats_print_individual(__('Referrers','mainwp-google-analytics-extension'), isset($values['individual']['ga:source']) ? $values['individual']['ga:source'] : null); ?></td>
                <td><?php MainWPGA::getStats_print_individual(__('Keywords','mainwp-google-analytics-extension'), isset($values['individual']['ga:keyword']) ? $values['individual']['ga:keyword'] : null); ?></td>
            </tr>
            <tr>
                <td><?php MainWPGA::getStats_print_individual(__('Pages','mainwp-google-analytics-extension'), isset($values['individual']['ga:pagePath']) ? $values['individual']['ga:pagePath'] : null); ?></td>
                <td><?php MainWPGA::getStats_print_individual(__('Country','mainwp-google-analytics-extension'), isset($values['individual']['ga:country']) ? $values['individual']['ga:country'] : null); ?></td>
            </tr>
        </table>
        <?php
        die();
    }

    //Used by getStats
    private static function getStats_print_aggregate($name, $old, $new, $round = false, $perc = false, $showAsTime = false)
    {
        $newVal = $new;
        $oldVal = $old;
        if ($showAsTime) {
            $newVal = MainWPGAUtility::sec2hms($newVal);
            $oldVal = MainWPGAUtility::sec2hms($oldVal);
        }
        else
        {
            if ($round) {
                $newVal = round($newVal, 2);
                $oldVal = round($oldVal, 2);
            }
            if ($perc) {
                $newVal = $newVal . '%';
                $oldVal = $oldVal . '%';
            }
        }
        $difference = MainWPGAUtility::getDifferenceInPerc($old, $new);
        if ($difference < 0) {
            $class = 'mainwp_ga_minus';
        }
        else
        {
            $class = 'mainwp_ga_plus';
            $difference = '+' . $difference;
        }
        ?>
    <?php echo $newVal; ?> <?php echo $name; ?>
    <div class="mainwp_ga_prev">Previous: <?php echo $oldVal; ?> (<span class="<?php echo $class; ?>"><?php echo $difference ?>
        %</span>)
    </div>
    <?php
    }

    //Used by getStats
    private static function getStats_print_individual($name, $items)
    {
        ?>
    <table class="mainwp_ga_table_inner">
    <col style="width: 85% ;">
        <tr>
            <th><?php echo $name; ?></th>
            <th>Visits</th>
        </tr>
        <?php
        if ($items != null) {
            foreach ($items as $key => $value)
            {
                ?>
                <tr>
                    <td><?php echo htmlentities($key); ?></td>
                    <td><?php echo htmlentities($value); ?></td>
                </tr>
                <?php
            }
        }?>
    </table>
    <?php
    }

    public static function getConsumerKey()
    {
        $key = get_option('GA_CONSUMERKEY');
        return ($key == '') ? 'anonymous' : $key;
    }

    public static function getPrivateKey()
    {
        $key = get_option('GA_PRIVKEY');
        return ($key == '') ? null : $key;
    }

    //Renders & handles post for settings in menu
    public static function handleSettingsPost()
    {
        global $current_user;
        if (isset($_POST['submit'])) {
            if (isset($_POST['mainwp_ga_interval']) && $_POST['mainwp_ga_interval'] == 'week') {
                $interval = 'week';
            }
            else
            {
                $interval = 'month';
            }
            if (isset($_POST['mainwp_ga_refreshrate']) && MainWPGAUtility::ctype_digit($_POST['mainwp_ga_refreshrate']))
            {
                $ga_refreshrate = $_POST['mainwp_ga_refreshrate'];
            }
            else
            {
                $ga_refreshrate = 9;
            }
            $ga_autoassign = 0;
            if (isset($_POST['mainwp_ga_automaticAssign']) && $_POST['mainwp_ga_automaticAssign']) {
                $ga_autoassign = 1;
            }

            if (isset($_POST['mainwp_ga_client_id'])) {
                foreach($_POST['mainwp_ga_client_id'] as $index => $cid) {
                    $ga_client_id = sanitize_text_field($cid);
                    $ga_client_secret = sanitize_text_field($_POST['mainwp_ga_client_secret'][$index]);
                    $ga_account_name = sanitize_text_field($_POST['mainwp_ga_account_name'][$index]);
                    if (!empty($ga_client_id)) {
                        $data = array('client_id' => $ga_client_id, 'client_secret' => $ga_client_secret, 'ga_account_name' => $ga_account_name);
                        MainWPGADB::Instance()->updateGAEntry($data);
                    }
                }
            }

            MainWPGADB::Instance()->updateGASettingGlobal('update_interval', $interval);
            MainWPGADB::Instance()->updateGASettingGlobal('refreshrate', $ga_refreshrate);
            MainWPGADB::Instance()->updateGASettingGlobal('auto_assign', $ga_autoassign);

            MainWPGADB::Instance()->removeGACache();

            return true;
        }
        return false;
    }

    public static function handleUpdateGA() {
        if (isset($_GET['GAUpdate']) && ($_GET['GAUpdate'] == 1))
        {
            if (self::updateAvailableSites())
                echo '<div class="mainwp-notice mainwp-notice-green">Google analytics statistics have been updated.</div>';
            else
                echo '<div class="mainwp-notice mainwp-notice-red">Error: Unable to connect to Google analytics.</div>';
        }
    }

    private static function renderAddOtherAccount() {
        ?>
            <tr class="mainwp_ga_account_row_client_id" row-id="0">
                <th scope="row"><?php _e('Client ID:','mainwp-google-analytics-extension');?> <?php do_action('mainwp_renderToolTip', __('','mainwp-google-analytics-extension'));?></th>
                <td>
                    <input type="text" name="mainwp_ga_client_id[]" class="row_client_id"
                           value=""/>
                </td>
            </tr>
             <tr class="mainwp_ga_account_row_client_secret" row-id="0">
                <th scope="row"><?php _e('Client Secret:','mainwp-google-analytics-extension');?> <?php do_action('mainwp_renderToolTip', __('','mainwp-google-analytics-extension'));?></th>
                <td>
                    <input type="text" name="mainwp_ga_client_secret[]" class="row_client_secret"
                           value=""/>
                </td>
            </tr>
            <tr row-id="0">
                <th scope="row">&nbsp;</th>
                <td>
                    <input type="button" name="mainwp_ga_connect" id="mainwp_ga_connect" class="button-primary"
                           value="<?php _e('Connect with GA','mainwp-google-analytics-extension'); ?>"/>
                </td>
             </tr>
            <?php

    }

    public static function renderSettings()
    {
        $verify_result = get_option('mainwp_ga_verify_result_status');
        if ($verify_result !== false && is_array($verify_result)) {
            delete_option('mainwp_ga_verify_result_status');
        }

        $ga_interval = MainWPGADB::Instance()->getGASettingGlobal('update_interval');
        $ga_refreshrate = MainWPGADB::Instance()->getGASettingGlobal('refreshrate');

        if ($ga_refreshrate == '') $ga_refreshrate = 9;
        $ga_entries = MainWPGADB::Instance()->getGAEntries();

        $auto_assign = MainWPGADB::Instance()->getGASettingGlobal('auto_assign');

//        $availableGASites = MainWPGADB::Instance()->getGASettingGlobal('availableSites');
//        if ($availableGASites != '')
//        {
//            $availableGASites = json_decode($availableGASites, 1);
//        }
//        print_r($availableGASites);

        if (is_array($verify_result) && isset($verify_result['error'])) {
            ?><div class="mainwp_info-box-red"><?php echo $verify_result['error']; ?></div><?php
        }

        //self::GoogleAnalyticsQSG();

        ?>
        <div class="postbox">
    <h3 class="mainwp_box_title"><span><i class="fa fa-cog"></i> <?php _e('Google Analytics Integration','mainwp-google-analytics-extension'); ?></span></h3>
    <div class="inside">
        <div class="mainwp_info-box">
            <a href="https://console.developers.google.com/" target="_blank" >
            <?php _e("Follow this link to your Google API Console, and there activate the Analytics API and create a Client ID in the API Access section.", "mainwp-google-analytics-extension"); ?><br/>
            </a>
            <?php printf(__("Select 'Web Application' as the application type. You must add the following as the authorised redirect URI (under \"More Options\") when asked:<br/><br/> <kbd>%s</kbd>", "mainwp-google-analytics-extension"), self::$redirect_uri); ?>
        </div>
    <table class="form-table">
        <tbody>
    <?php
        if (is_array($ga_entries) && count($ga_entries) > 0) {

            $idx = 0;
            foreach($ga_entries as $ga_entry) {
			?>
			  <tr>
					<th scope="row"><?php _e('Account Name:','mainwp-google-analytics-extension');?></th>
					<td>
						<input type="text" name="mainwp_ga_account_name[]"  class="row_account_name"
                           value="<?php echo $ga_entry->ga_account_name; ?>"/>
					</td>
				</tr>
			<?php
                if (!empty($ga_entry->ga_name)) {
                ?>

                    <tr>
                        <th scope="row"><?php _e('Account:','mainwp-google-analytics-extension');?> <?php do_action('mainwp_renderToolTip', __('You can link multiple GA accounts to your Network.','mainwp-google-analytics-extension'));?></th>
                        <td>
                            <span><?php echo $ga_entry->ga_name; ?></span>
                        </td>
                    </tr>
            <?php } ?>

            <tr class="mainwp_ga_account_row_client_id" row-id="<?php echo $idx; ?>">
                <th scope="row"><?php _e('Client ID:','mainwp-google-analytics-extension');?> <?php do_action('mainwp_renderToolTip', __('','mainwp-google-analytics-extension'));?></th>
                <td>
                    <input type="text" name="mainwp_ga_client_id[]"  class="row_client_id"
                           value="<?php echo $ga_entry->client_id; ?>"/>
                </td>
            </tr>
             <tr class="mainwp_ga_account_row_client_secret" row-id="<?php echo $idx; ?>">
                <th scope="row"><?php _e('Client Secret:','mainwp-google-analytics-extension');?> <?php do_action('mainwp_renderToolTip', __('','mainwp-google-analytics-extension'));?></th>
                <td>
                    <input type="text" name="mainwp_ga_client_secret[]"  class="row_client_secret"
                           value="<?php echo $ga_entry->client_secret; ?>"/>
                </td>
            </tr>
            <tr row-id="<?php echo $idx; ?>">
                <th scope="row">&nbsp;</th>
                <td>
                    <div class="mainwp_ga_accounts_settings" client-id="<?php echo $ga_entry->client_id; ?>" client-secret="<?php echo $ga_entry->client_secret; ?>">
                    <?php
                        if (!empty($ga_entry->refresh_token)) {
                            $dis_class = "button-primary";
                            $dis_title = __('Disconnect','mainwp-google-analytics-extension');
                        } else {
                            $dis_class = "button";
                            $dis_title = __('Remove','mainwp-google-analytics-extension');
                        }
                    ?>
                        <input type="button" name="mainwp_ga_disconnect" id="mainwp_ga_disconnect" mainwp_ga_entry="<?php echo $ga_entry->id; ?>"
                                     class="<?php echo $dis_class; ?>" value="<?php echo $dis_title; ?>"/>
                    </div>
                </td>
             </tr>
            <?php
                $idx++;
                }
            } else {
                self::renderAddOtherAccount();
            }
            ?>
            <tr id="mainwp_ga_add_account_another">
                    <th scope="row">&nbsp;</th>
                <td>
                    <a id="mainwp_ga_account_another" href="#"><i class="fa fa-plus"></i> <?php echo __('Add another Account...', 'mainwp-google-analytics-extension'); ?></a>
                </td>
            </tr>
        <tr>
            <th scope="row"><?php _e('Time interval','mainwp-google-analytics-extension'); ?> <?php do_action('mainwp_renderToolTip', __('Show GA stats by week or month.','mainwp-google-analytics-extension')); ?></th>
            <td><span class="mainwp-select-bg">
                <select name="mainwp_ga_interval" id="mainwp_ga_interval">
                    <option value="week" <?php if ($ga_interval == 'week') {
                        ?>selected<?php } ?>><?php _e('Week','mainwp-google-analytics-extension'); ?>
                    </option>
                    <option value="month" <?php if ($ga_interval == 'month') {
                        ?>selected<?php } ?>><?php _e('Month','mainwp-google-analytics-extension'); ?>
                    </option>
                </select><label></label></span>
            </td>
        </tr>
        <tr>
            <th scope="row"><?php _e('Refresh rate','mainwp-google-analytics-extension'); ?> <?php do_action('mainwp_renderToolTip', __('How often should we check GA for new traffic data, additional sites?', '//docs.mainwp.com/how-often-are-my-analytics-updated/','mainwp-google-analytics-extension')); ?></th>
            <td>
                <?php _e('Every','mainwp-google-analytics-extension'); ?> <span class="mainwp-select-bg"><select name="mainwp_ga_refreshrate" id="mainwp_ga_refreshrate">
                    <option value="1" <?php if ($ga_refreshrate == '1') { ?>selected<?php } ?>>1</option>
                    <option value="3" <?php if ($ga_refreshrate == '3') { ?>selected<?php } ?>>3</option>
                    <option value="9" <?php if ($ga_refreshrate == '9') { ?>selected<?php } ?>>9</option>
                    <option value="15" <?php if ($ga_refreshrate == '15') { ?>selected<?php } ?>>15</option>
                    <option value="18" <?php if ($ga_refreshrate == '18') { ?>selected<?php } ?>>18</option>
                    <option value="24" <?php if ($ga_refreshrate == '24') { ?>selected<?php } ?>>24</option>
                </select><label></label></span> <?php _e('hours','mainwp-google-analytics-extension'); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="button" name="mainwp_ga_updatenow" id="mainwp_ga_updatenow" class="button-primary"
                                       value="<?php _e('Refresh Now','mainwp-google-analytics-extension'); ?>"/>
            </td>
        </tr>
         <tr>
            <th scope="row"><?php _e('Automatically Assign Child sites','mainwp-google-analytics-extension'); ?> <?php do_action('mainwp_renderToolTip', __('Set to YES if you want to automatically Assign Child sites?', 'mainwp-google-analytics-extension')); ?></th>
            <td>
               <div class="mainwp-checkbox">
                    <input type="checkbox" id="mainwp_ga_automaticAssign" name="mainwp_ga_automaticAssign" <?php echo ($auto_assign == 0 ? '' : 'checked="checked"'); ?> value="1">
                    <label for="mainwp_ga_automaticAssign"></label>
                </div>
            </td>
        </tr>

        </tbody>
    </table>
    </div>
    </div>
    <?php
    }


    //Gets all the avilable sites (sites with a known GA id)
    private static function getAvailableSites()
    {
        $current_wpid = MainWPGAUtility::get_current_wpid();
        if ($current_wpid)
            $websites = MainWPGADB::Instance()->getWebsitesByUserIdWithGAId($current_wpid);
        else
            $websites = MainWPGADB::Instance()->getWebsitesByUserIdWithGAId();

        $output = array();
        foreach ($websites as $website)
        {
            $output[] = array('id' => $website->id, 'name' => $website->name );
        }
        return $output;
    }

    //This function will fetch your google account & check against the WPs added in our database, if one matches, we update the GAid
    public static function updateAvailableSites()
    {
        //Get available sites from all the GA Entries
        $availableSites = array();
        $availableIds = array();
        $propertyIds = array();
        $gaEntries = MainWPGADB::Instance()->getGAEntries();
        self::autoloadVendor();
        $connect_ok = false;
        foreach ($gaEntries as $ga_entry)
        {
            $client_id = $ga_entry->client_id;
            $client_secret = $ga_entry->client_secret;
            $refresh_token = $ga_entry->refresh_token;
            if (empty($refresh_token))
                continue;
            try
            {
                $client = new Google_Client();
                $client->setApplicationName("Google Analytics Application");
                $client->setClientId($client_id);
                $client->setClientSecret($client_secret);
                $client->setAccessType('offline');
                $client->setRedirectUri(self::$redirect_uri);

                $access_token = array();
//                if  (isset($_SESSION['MainWP_GA_Access_Token'])) {
//                    $access_token = $_SESSION['MainWP_GA_Access_Token'];
//                    $client->setAccessToken($access_token);
//                }

                if (empty($access_token) || $client->isAccessTokenExpired()) {
                    $access_token = $client->refreshToken($refresh_token);
//                    $_SESSION['MainWP_GA_Access_Token'] = $access_token;
                }

                $service = new Google_Service_Analytics($client);
                if (!empty($access_token)) {
                    $profs = $service->management_profiles->listManagementProfiles("~all", "~all");
                    if (is_object($profs)) {
                        $connect_ok = true;
                        foreach ($profs->items as $value)
                        {
                            $valueId = $value['webPropertyId'];
                            if (!isset($availableSites[$valueId]) || stripos($value['name'], 'mainwp') !== false) {
                                $valueUrl = $value['websiteUrl'];
                                $ga_item_id = $value['id'];
                                $propertyIds[$valueId] = $value['id'];
                                $availableSites[$valueId] = array('url' => $valueUrl, 'id' => $valueId, 'gas_id' => $ga_entry->id, 'ga_item_id' => $ga_item_id ,'name' => $value['name']); // to fix double listing
                                $availableIds[] = $ga_item_id . '_' . $valueId . '_' . $ga_entry->id;
                            }
                        }
                    }
                }
            }
            catch (Exception $exception)
            {
                if (is_a($exception, 'Google_Service_Exception')) {
                    $err = $exception->getErrors();
                }
                return false;
            }

        }

        if (!$connect_ok)
            return false;

        MainWPGADB::Instance()->updateGASettingGlobal('availableSites', json_encode($availableSites));
        MainWPGADB::Instance()->updateGASettingGlobal('propertyIds', json_encode($propertyIds));

        global $mainWPGoogleAnalyticsExtensionActivator;
        $dbwebsites = apply_filters('mainwp-getsites', $mainWPGoogleAnalyticsExtensionActivator->getChildFile(), $mainWPGoogleAnalyticsExtensionActivator->getChildKey(), null);
        $auto_assign = MainWPGADB::Instance()->getGASettingGlobal('auto_assign');

        if ($dbwebsites)
        {
            foreach ($dbwebsites as $website)
            {
                $website['ga_id'] = MainWPGADB::Instance()->getGAId($website['id']);
                $website['gas_id'] = MainWPGADB::Instance()->getGASId($website['id']);
                $website['ga_item_id'] = MainWPGADB::Instance()->getGAItemID($website['id']);

                //$url = preg_replace('/http(s?):\/\/(.*?)(\/?)$/', '${2}', $website['url']);
                //$url = preg_replace('/(https?:\/\/(.*?))(\/?)$/', '${1}', $website['url']);
                $url = preg_replace('/^https?:\/\//', '', $website['url']);
                $url = trim($url , '/');
                $url = preg_replace('/^www\./', '', $url);

                if ($auto_assign) {
                    foreach ($availableSites as $availableSite)
                    {

                        $avail_url = preg_replace('/^https?:\/\//', '', $availableSite['url']);
                        $avail_url = trim($avail_url , '/');
                        $avail_url = preg_replace('/^www\./', '', $avail_url);

                        if ($url == $avail_url) {
                            //update this website to that id!
                            if ($website['ga_id'] == '') {
                                $ret = MainWPGADB::Instance()->updateGAId($website['id'], $availableSite['id'], $availableSite['gas_id'], $availableSite['ga_item_id']);
                                if (is_object($ret) && $ret->ga_id && $ret->gas_id) {
                                    $website['ga_id'] = $ret->ga_id; // to fix bug
                                    $website['gas_id'] = $ret->gas_id;
                                    $website['ga_item_id'] = $ret->ga_item_id;
                                }
                            }
                            break;
                        }
                    }
                }

                //Check if the site is still available!
                if (!empty($website['ga_item_id'])) { // to  fix bug
                        if (!in_array($website['ga_item_id'] . '_' .$website['ga_id'] . '_' . $website['gas_id'], $availableIds)) {
                                //remove GA
                                MainWPGADB::Instance()->removeGAId($website['id']);
                        }
                }
            }
        }

        return true;
    }

    public static function admin_footer() {
    ?>
            <script type="text/javascript">
            jQuery(document).ready(function(){
                jQuery('#mainwp_ga_account_another').click(function(e) {
                    e.preventDefault();
                    var ind = jQuery('.mainwp_ga_account_row_client_secret').length;
                    jQuery('#mainwp_ga_add_account_another').before('<tr class="mainwp_ga_account_row_client_id" row-id="' + ind + '"><th scope="row"><?php _e('Client ID:','mainwp-google-analytics-extension');?> <?php do_action('mainwp_renderToolTip', __('','mainwp-google-analytics-extension'));?></th><td><input type="text" name="mainwp_ga_client_id[]"  class="row_client_id" value=""/></td></tr><tr class="mainwp_ga_account_row_client_secret" row-id="' + ind + '"><th scope="row"><?php _e('Client Secret:','mainwp-google-analytics-extension');?> <?php do_action('mainwp_renderToolTip', __('','mainwp-google-analytics-extension'));?></th><td><input type="text" name="mainwp_ga_client_secret[]"  class="row_client_secret" value=""/></td></tr><tr row-id="' + ind + '"><th scope="row">&nbsp;</th><td><input type="button" name="mainwp_ga_connect" id="mainwp_ga_connect" class="button-primary" value="<?php _e('Connect with GA','mainwp-google-analytics-extension'); ?>"/>&nbsp;&nbsp;<a href="#" class="button" onclick="mainwp_ga_remove_account_row(this); return false;"><?php echo __("Remove", "mainwp"); ?></a></td></tr>');
                });
            });
        </script>
    <?php
    }

    //Disconnects the google account
    public static function disconnect($gaId)
    {
        MainWPGADB::Instance()->removeGAIds($gaId);
        //Config:
        MainWPGADB::Instance()->removeGASettings($gaId);
        MainWPGADB::Instance()->updateGASettingGlobal('lastupdate', 0);
//        if  (isset($_SESSION['MainWP_GA_Access_Token'])) {
//            unset($_SESSION['MainWP_GA_Access_Token']);
//        }
    }

    public static function connect()
    {
        $ga_account_name = isset($_POST['name']) ? sanitize_text_field($_POST['name']) : '';
        $ga_client_id = sanitize_text_field($_POST['client_id']);
        $ga_client_secret = sanitize_text_field($_POST['client_secret']);

        if (empty($ga_client_id))
            die(json_encode(array('error' => __('Error: empty client id.', 'mainwp-google-analytics-extension'))));

        $data = array('client_id' => $ga_client_id, 'client_secret' => $ga_client_secret, 'ga_account_name' => $ga_account_name);
        $ga_entry = MainWPGADB::Instance()->updateGAEntry($data);

        if (empty($ga_entry))
            die(json_encode(array('error' => __('Error: Can not create GA entry.', 'mainwp-google-analytics-extension'))));

        self::autoloadVendor();

        try {
            $client = new Google_Client();
            $client->setApplicationName("Google Analytics Application");
            $client->setClientId($ga_client_id);
            $client->setClientSecret($ga_client_secret);
            $client->setAccessType('offline');
            $client->setApprovalPrompt('force');
            $client->setPrompt('consent');
            $client->setRedirectUri(self::$redirect_uri);
            $client->setScopes(array(
                'https://www.googleapis.com/auth/analytics.readonly'
            ));

            $output['url'] = $client->createAuthUrl();

            global $current_user;
            $userId = $current_user->ID;

            update_user_option($userId, 'mainwp_ga_processing_oauth_ga_id', $ga_entry->id);

        }
        catch (Exception $e)
        {
            $output['error'] = $e->getMessage();
        }

        die(json_encode($output));
    }

    public static function verify($gaId, $code)
    {
        global $current_user;

        $ga_entry = MainWPGADB::Instance()->getGAEntryBy('id', $gaId);

        if (empty($ga_entry))
            return array('error' => 'Error: Empty GA entry.');

        $client_id = $ga_entry->client_id;
        $client_secret = $ga_entry->client_secret;
        self::autoloadVendor();
        try
        {
            $client = new Google_Client();
            $client->setApplicationName("Google Analytics Application");
            $client->setClientId($client_id);
            $client->setClientSecret($client_secret);
            $client->setAccessType('offline');
            $client->setRedirectUri(self::$redirect_uri);

            $service = new Google_Service_Analytics($client);
            $client->authenticate($code);
            $authArr = $client->getAccessToken();

            //error_log(print_r($authArr, true));

            $update = array(
                    'client_id' => $client_id,
                    'enabled' => 0,
                    'refresh_token' => '',
                    'ga_name' => ''
                );

            if (!empty($authArr)) {
                MainWPGADB::Instance()->updateGASettingGlobal('lastupdate', 0);
                //if offline access requested and granted, get refresh token
                if (isset($authArr['refresh_token'])){
                    $update['refresh_token'] = $authArr['refresh_token'];
                    $update['enabled'] = 1;
                }

                //$props = $service->management_accountSummaries->listManagementAccountSummaries("~all", "~all");
                $props = $service->management_webproperties->listManagementWebproperties("~all");
                //error_log(print_r($props, true));

                if (is_object($props) && property_exists($props, 'username')) {
                    $update['ga_name'] = $props['username'];
                }
            }

            MainWPGADB::Instance()->updateGAEntry($update);
            return array('connection' => 'success');
        }
        catch (Exception $e)
        {
            return array('error' => $e->getMessage());
        }
    }

    function mainwp_ga_disconnect()
    {
        MainWPGA::disconnect($_POST['gaId']);
    }

    function mainwp_ga_connect()
    {
        MainWPGA::connect();
    }

    function mainwp_ga_getstats()
    {
        $id = null;
        if (isset($_POST['id']))
        {
            $id = $_POST['id'];
            $id = $id;

            MainWPGA::getStats($id);
        }
        die();
    }

    function mainwp_delete_site($website)
    {
        MainWPGADB::Instance()->removeWebsite($website->id);
    }

    function ga_delete_site($websiteId)
    {
        MainWPGADB::Instance()->removeWebsite($websiteId);
    }

    function mainwp_update_site($websiteId)
    {
        //Update GA settings:
        if (isset($_POST['ga_id']))
        {
            if ($_POST['ga_id'] == '0')
            {
                MainWPGADB::Instance()->removeGAId($websiteId);
            }
            else
            {
                $splitted = explode('_', $_POST['ga_id']);
                if (count($splitted) > 1)
                {
					if (count($splitted) == 2) {
						$ga_item_id = 0;
						$ga_id = $splitted[0];
						$gas_id = $splitted[1];
					} else {
						$ga_item_id = $splitted[0];
						$ga_id = $splitted[1];
						$gas_id = $splitted[2];
					}
                    MainWPGADB::Instance()->updateGAId($websiteId, $ga_id, $gas_id, $ga_item_id);
                }
            }
        }
    }

    function mainwp_extension_sites_edit_tablerow($website)
    {
        $gaSites = MainWPGADB::Instance()->getGASettingGlobal('availableSites');
        if ($gaSites != '')
        {
            $gaSites = json_decode($gaSites, 1);
        }
        else
        {
            $gaSites = array();
        }

        MainWPGAUtility::array_sort( $gaSites, 'url' ); // index of array $gaSites changed
        $websiteInfo = MainWPGADB::Instance()->getGAGASId($website->id);

        ?>
    <tr>
        <th scope="row"><?php _e('Site from your Google account','mainwp-google-analytics-extension'); ?></th>
        <td><div class="mainwp-select-bg" id="sites-edit-site-ga">
            <select name="ga_id" id="ga_id">
                <option value="0"><?php _e('No website','mainwp-google-analytics-extension'); ?></option>
                <?php
                foreach ($gaSites as $gaSite)
                {
                    $selected = false;
                    if (!empty($websiteInfo) && isset($websiteInfo->ga_id) && isset($gaSite['id']) && !empty($gaSite['id'])) {
                            $selected = ($websiteInfo->ga_id == $gaSite['id']);
                    }
                    ?>
                    <option value="<?php echo $gaSite['ga_item_id'] . '_' .$gaSite['id'] . '_' . $gaSite['gas_id']; ?>" <?php echo ($selected
                            ? 'selected="selected"'
                            : ''); ?>><?php echo $gaSite['url']; ?></option>
                    <?php

                }
                ?>
            </select><label></label>
            </div>
        </td>
    </tr>
        <?php
    }

    public static function GoogleAnalyticsQSG() {

        $plugin_data =  get_plugin_data( MAINWP_GA_PLUGIN_FILE, false );
        $description = $plugin_data['Description'];
        $extraHeaders = array('DocumentationURI' => 'Documentation URI');
        $file_data = get_file_data(MAINWP_GA_PLUGIN_FILE, $extraHeaders);
        $documentation_url  = $file_data['DocumentationURI'];

        ?>
         <div  class="mainwp_ext_info_box" id="ga-pth-notice-box">
            <div class="mainwp-ext-description"><?php echo $description; ?></div><br/>
            <b><?php echo __("Need Help?", 'mainwp-google-analytics-extension'); ?></b> <?php echo __("Review the Extension", 'mainwp-google-analytics-extension'); ?> <a href="<?php echo $documentation_url; ?>" target="_blank"><i class="fa fa-book"></i> <?php echo __('Documentation','mainwp-google-analytics-extension'); ?></a>.
                    <a href="#" id="mainwp-ga-quick-start-guide"><i class="fa fa-info-circle"></i> <?php _e('Show Quick Start Guide','mainwp-google-analytics-extension'); ?></a></div>
                    <div  class="mainwp_ext_info_box" id="mainwp-ga-tips" style="color: #333!important; text-shadow: none!important;">
                      <span><a href="#" class="mainwp-show-tut" number="1"><i class="fa fa-book"></i> <?php _e('Google Analytics Authentication','mainwp-google-analytics-extension') ?></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="mainwp-show-tut"  number="2"><i class="fa fa-book"></i> <?php _e('Analytics drop down shows "All Web Data"','mainwp-google-analytics-extension') ?></a></span><span><a href="#" id="mainwp-ga-tips-dismiss" style="float: right;"><i class="fa fa-times-circle"></i> <?php _e('Dismiss','mainwp-google-analytics-extension'); ?></a></span>
                      <div class="clear"></div>
                      <div id="mainwp-ga-tuts">
                        <div class="mainwp-ga-tut" number="1">
                            <h3>Google Analytics Authentication</h3>
                            <section class="entry"><p>The recent change&nbsp;on the Google API Authentication, the MainWP Google Analytics Extension needed updated to. The usage of the new <a href="https://developers.google.com/identity/protocols/OAuth2">OAuth 2.0 protocol</a>&nbsp;required significant changes in authentication process.</p><p>After updating the MainWP Google Analytics Extension to version&nbsp;0.1.4, if your previous connection with the Google Analytics service doesn’t work anymore, you need to remove the old account and go through the new Authentication Process.</p><p>Here is how you can create your <strong>Client ID</strong> and <strong>Secret Client</strong>:</p><p>1. Open your MainWP Google Anaylitics Extension settings page;</p><p>2. On the Extension&nbsp;Settings&nbsp;page, follow the provided link to access your Google API Console;</p><p><a title="Google Analytics Authentication" rel="lightbox[5094]" href="//docs.mainwp.com/wp-content/uploads/2015/05/ga-link-to-gac.png" class="cboxElement"><img width="1125" height="606" alt="ga-link-to-gac" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-link-to-gac.png" style="max-width: 100%;"><noscript>&lt;img
class="alignnone size-full wp-image-5095" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-link-to-gac.png" alt="ga-link-to-gac" width="1125" height="606" /&gt;</noscript></a></p><p>3. In the Google API Console, you need to create a new project. To do that, click the <strong>Create New Project </strong>button;</p><p><a title="Google Analytics Authentication" rel="lightbox[5094]" href="//docs.mainwp.com/wp-content/uploads/2015/05/ga-create-project.png" class="cboxElement"><img width="1366" height="683" alt="ga-create-project" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-create-project.png" style="max-width: 100%;"><noscript>&lt;img
class="alignnone size-full wp-image-5096" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-create-project.png" alt="ga-create-project" width="1366" height="683" /&gt;</noscript></a></p><p>&nbsp;</p><p>4. Enter a custom project name an click the <strong>Create</strong> button;</p><p><a title="Google Analytics Authentication" rel="lightbox[5094]" href="//docs.mainwp.com/wp-content/uploads/2015/05/ga-create.png" class="cboxElement"><img width="1366" height="683" alt="ga-create" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-create.png" style="max-width: 100%;"><noscript>&lt;img
class="alignnone size-full wp-image-5097" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-create.png" alt="ga-create" width="1366" height="683" /&gt;</noscript></a></p><p>5. Select your project from the <strong>Dropdown</strong> menu and click the <strong>APIs &amp; Auth</strong> link in the left side menu.</p><p><a title="Google Analytics Authentication" rel="lightbox[5094]" href="//docs.mainwp.com/wp-content/uploads/2015/05/ga-select-project.png" class="cboxElement"><img width="1366" height="683" alt="ga-select-project" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-select-project.png" style="max-width: 100%;"><noscript>&lt;img
class="alignnone size-full wp-image-5098" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-select-project.png" alt="ga-select-project" width="1366" height="683" /&gt;</noscript></a></p><p>6. In the <strong>APIs</strong> menu, locate the<strong> Analytics API</strong> and click it;</p><p><a title="Google Analytics Authentication" rel="lightbox[5094]" href="//docs.mainwp.com/wp-content/uploads/2015/05/ga-apis.png" class="cboxElement"><img width="1366" height="683" alt="ga-apis" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-apis.png" style="max-width: 100%;"><noscript>&lt;img
class="alignnone size-full wp-image-5099" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-apis.png" alt="ga-apis" width="1366" height="683" /&gt;</noscript></a></p><p>7. Click on the Enable API button;</p><p><a title="Google Analytics Authentication" rel="lightbox[5094]" href="//docs.mainwp.com/wp-content/uploads/2015/05/ga-enable-api.png" class="cboxElement"><img width="1366" height="683" alt="ga-enable-api" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-enable-api.png" style="max-width: 100%;"><noscript>&lt;img
class="alignnone size-full wp-image-5100" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-enable-api.png" alt="ga-enable-api" width="1366" height="683" /&gt;</noscript></a></p><p>8. Go to the <strong>Credentials</strong> menu and click the <strong>Create new Client ID</strong> button;</p><p><a title="Google Analytics Authentication" rel="lightbox[5094]" href="//docs.mainwp.com/wp-content/uploads/2015/05/ga-credentials.png" class="cboxElement"><img width="1366" height="683" alt="ga-credentials" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-credentials.png" style="max-width: 100%;"><noscript>&lt;img
class="alignnone size-full wp-image-5101" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-credentials.png" alt="ga-credentials" width="1366" height="683" /&gt;</noscript></a></p><p>9. In the popup form, make sure the <strong>Web Application</strong> is selected and click the <strong>Create consent screen </strong>button<strong>;</strong></p><p><a title="Google Analytics Authentication" rel="lightbox[5094]" href="//docs.mainwp.com/wp-content/uploads/2015/05/ga-web-application.png" class="cboxElement"><img width="1366" height="683" alt="ga-web-application" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-web-application.png" style="max-width: 100%;"><noscript>&lt;img
class="alignnone size-full wp-image-5102" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-web-application.png" alt="ga-web-application" width="1366" height="683" /&gt;</noscript></a></p><p>10. In the Consent Screen form, select your <strong>Email Address</strong>, enter a custom <strong>Product Name</strong> and click the <strong>Save</strong> button;</p><p><a title="Google Analytics Authentication" rel="lightbox[5094]" href="//docs.mainwp.com/wp-content/uploads/2015/05/ga-consent-screen.png" class="cboxElement"><img width="1366" height="683" alt="ga-consent-screen" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-consent-screen.png" style="max-width: 100%;"><noscript>&lt;img
class="alignnone size-full wp-image-5103" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-consent-screen.png" alt="ga-consent-screen" width="1366" height="683" /&gt;</noscript></a></p><p>11. After setting consent screen, you will be prompted back to the <strong>Create Client ID</strong> form, make sure the Web Application is still selected.</p><p>12. Copy the <strong>Redirect URI</strong> from the Extension settings page and enter it to the <strong>Authorized Redirect URIs</strong> box and click the <strong>Create Client ID</strong> button.</p><p><a title="Google Analytics Authentication" rel="lightbox[5094]" href="//docs.mainwp.com/wp-content/uploads/2015/05/ga-create-id.png" class="cboxElement"><img width="1366" height="683" alt="ga-create-id" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-create-id.png" style="max-width: 100%;"><noscript>&lt;img
class="alignnone size-full wp-image-5104" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-create-id.png" alt="ga-create-id" width="1366" height="683" /&gt;</noscript></a></p><div class="woo-sc-box normal">If you are using multiple MainWP dashboards, you can enter multiple Redirect URIs here, you need to copy them from all your dashboard.</div><p>13. Now your Client ID and Secret Client is created.</p><p><a title="Google Analytics Authentication" rel="lightbox[5094]" href="//docs.mainwp.com/wp-content/uploads/2015/05/ga-creds.png" class="cboxElement"><img width="1366" height="683" alt="ga-creds" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-creds.png" style="max-width: 100%;"><noscript>&lt;img
class="alignnone size-full wp-image-5105" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-creds.png" alt="ga-creds" width="1366" height="683" /&gt;</noscript></a></p><p>14. Copy your <strong>Client ID</strong> and <strong>Secret Client</strong> to the extension settings page in correct fields;</p><p>15. Click the <strong>Connect to GA</strong> button;</p><p>16. After clicking the button, Google Analytics will ask you to allow the application to access data.</p><p>17. Click the <strong>Accept&nbsp;</strong>button.</p><p><a title="Google Analytics Authentication" rel="lightbox[5094]" href="//docs.mainwp.com/wp-content/uploads/2015/05/ga-accept.png" class="cboxElement"><img width="1366" height="683" alt="ga-accept" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-accept.png" style="max-width: 100%;"><noscript>&lt;img
class="alignnone size-full wp-image-5106" src="//docs.mainwp.com/wp-content/uploads/2015/05/ga-accept.png" alt="ga-accept" width="1366" height="683" /&gt;</noscript></a></p><p>That is it. After completing this process, your account will be connected to your extension.</p>
</section>
                        </div>
                        <div class="mainwp-ga-tut"  number="2">
                            <h3>Analytics drop down shows "All Web Data"</h3>
                            <p>"All Web Data" is the default profile name when you are setting up a new profile/site for Google Analytics.</p>
                            <p>To change the name:</p>
                            <ol>
                                <li>Login to http://google.com/analytics</li>
                                <li>Click the site you wish to change the name for to open the folder structure until you can click the site to see traffic data</li>
                                <li>Click Admin in the top right</li>
                                <li>Click the site name once again</li>
                                <li>You should now see a link that reads Profile Settings, click it</li>
                                <li>Change the Profile Name to something specific you can associate with your site</li>
                                <li>Click "Apply"</li>
                            </ol>
                            <p>You can now do 1 of 2 things:</p>
                            <ol>
                                <li>You can disconnect the Google Account from the settings menu. THIS WILL REMOVE YOUR CURRENT ANALYTICS SETTINGS, then reconnect to show the new site names.</li>
                                <li>You can wait and the names will be auto updated, and each profile should be associated with the correct site. This process takes place about once a day or every 15 hours. When new sites are added the site data is also imported.</li>
                            </ol>
                        </div>
                      </div>
                    </div>
        <?php
}
}

?>
