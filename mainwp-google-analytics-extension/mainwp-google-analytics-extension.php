<?php
/*
Plugin Name: MainWP Google Analytics Extension
Plugin URI: https://mainwp.com
Description: MainWP Google Analytics Extension is an extension for the MainWP plugin that enables you to monitor detailed statistics about your child sites traffic. It integrates seamlessly with your Google Analytics account.
Version: 1.8
Author: MainWP
Author URI: https://mainwp.com
Documentation URI: https://mainwp.com/help/category/mainwp-extensions/google-analytics/
*/

if (!defined('MAINWP_GA_PLUGIN_FILE')) {
    define('MAINWP_GA_PLUGIN_FILE', __FILE__);
}

if (!defined('MAINWP_GA_PLUGIN_DIR')) {
    define('MAINWP_GA_PLUGIN_DIR', dirname(__FILE__));
}

class MainWPGoogleAnalyticsExtension
{
    public static $instance = null;
    public  $plugin_handle = "mainwp-google-analytics-extension";
    protected $plugin_url;
    protected $mainwpGA;
    private $plugin_slug;
	private $update_version = '1.0';

	static function Instance()
    {
        if (MainWPGoogleAnalyticsExtension::$instance == null) MainWPGoogleAnalyticsExtension::$instance = new MainWPGoogleAnalyticsExtension();
        return MainWPGoogleAnalyticsExtension::$instance;
    }

    public function __construct()
    {
		$this->plugin_url = plugin_dir_url(__FILE__);
        $this->plugin_slug = plugin_basename(__FILE__);

        add_action('init', array(&$this, 'init'));
//        add_action( 'admin_head', array( &$this, 'admin_head' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
        add_action('admin_init', array(&$this, 'admin_init'));
        add_action('mainwp_admin_menu', array(&$this, 'mainwp_admin_menu'));
        add_filter('plugin_row_meta', array(&$this, 'plugin_row_meta'), 10, 2);

        MainWPGADB::Instance()->install();
        $this->mainwpGA = new MainWPGA();
        $this->mainwpGA->init();
	}

    public function mainwp_admin_menu()
    {
        $lastupdate = MainWPGADB::Instance()->getGASettingGlobal('lastupdate');
        $ga_refreshrate = MainWPGADB::Instance()->getGASettingGlobal('refreshrate');
        if (!MainWPGAUtility::ctype_digit($ga_refreshrate) || ($ga_refreshrate == '')) $ga_refreshrate = 9;
        if ($lastupdate == 0 || (time() - $lastupdate) > 60 * 60 * $ga_refreshrate) { //Never updated or longer then $ga_refreshrate hours ago
            MainWPGA::updateAvailableSites();
            MainWPGADB::Instance()->updateGASettingGlobal('lastupdate', time());
        }

        $update_version = get_option('mainwp_ga_update_version', false);
        if (version_compare($this->update_version, $update_version, '<>')) {
                if (empty($update_version)) {
                        MainWPGA::updateAvailableSites();
                        MainWPGADB::Instance()->updateGASettingGlobal('lastupdate', time());
                }
                update_option('mainwp_ga_update_version', $this->update_version);
        }
    }

    public function init()
    {

    }

    public function plugin_row_meta($plugin_meta, $plugin_file)
    {
        if ($this->plugin_slug != $plugin_file) return $plugin_meta;

		$slug = basename($plugin_file, ".php");
		$api_data = get_option( $slug. '_APIManAdder');
		if (!is_array($api_data) || !isset($api_data['activated_key']) || $api_data['activated_key'] != 'Activated' || !isset($api_data['api_key']) || empty($api_data['api_key']) ) {
			return $plugin_meta;
		}

        $plugin_meta[] = '<a href="?do=checkUpgrade" title="Check for updates.">Check for updates now</a>';
        return $plugin_meta;
    }

//    public function admin_head()
//    {
//        echo '<script type="text/javascript" src="' . $this->plugin_url . 'js/jsapi.js' . '"></script>';
//        echo '<script type="text/javascript">
//            google.load("visualization", "1", {packages:["corechart"]});
//        </script>';
//    }

    public function admin_enqueue_scripts()
    {
        if ( isset( $_GET['page'] ) && ( $_GET['page'] == 'mainwp_tab' || ( $_GET['page'] == 'managesites') && isset($_GET['dashboard'])) ) {
            wp_enqueue_script('mainwp-ga-chart', plugins_url( 'js/loader.js', __FILE__ ));
        }
    }

    public function admin_init()
    {
        $verify_result = null;
        if (isset($_GET['page']) && $_GET['page'] == 'Extensions-Mainwp-Google-Analytics-Extension') {
            if (isset($_GET['mainwp_ga']) && $_GET['mainwp_ga'] == 1 && isset($_GET['code']) ) {
                $gaId = get_user_option('mainwp_ga_processing_oauth_ga_id');
                if($gaId) {
                    $verify_result = MainWPGA::verify($gaId, $_GET['code']);
                    update_option('mainwp_ga_verify_result_status', $verify_result);
                    wp_safe_redirect( admin_url('admin.php?page=Extensions-Mainwp-Google-Analytics-Extension'), 303 );
                    exit();
                }
            }
            add_action('admin_footer', array('MainWPGA', 'admin_footer'));
        }

        wp_enqueue_style('mainwp-google-analytics-extension-css', $this->plugin_url . 'css/mainwp-google-analytics.css');
        wp_enqueue_script('mainwp-google-analytics-extension-js', $this->plugin_url . 'js/mainwp-google-analytics.js');
    }
}


function mainwp_google_analytics_extension_autoload($class_name)
{
    $allowedLoadingTypes = array('class');

    foreach ($allowedLoadingTypes as $allowedLoadingType)
    {
        $class_file = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . str_replace(basename(__FILE__), '', plugin_basename(__FILE__)) . $allowedLoadingType . DIRECTORY_SEPARATOR . $class_name . '.' . $allowedLoadingType . '.php';
        if (file_exists($class_file))
        {
            require_once($class_file);
        }
    }
}


spl_autoload_register('mainwp_google_analytics_extension_autoload');


register_activation_hook(__FILE__, 'google_analytics_extension_activate');
register_deactivation_hook(__FILE__, 'google_analytics_extension_deactivate');

function google_analytics_extension_activate()
{
    update_option('mainwp_google_analytics_extension_activated', 'yes');
    $extensionActivator = new MainWPGoogleAnalyticsExtensionActivator();
    $extensionActivator->activate();
}
function google_analytics_extension_deactivate()
{
    $extensionActivator = new MainWPGoogleAnalyticsExtensionActivator();
    $extensionActivator->deactivate();
}

class MainWPGoogleAnalyticsExtensionActivator
{
    protected $mainwpMainActivated = false;
    protected $childEnabled = false;
    protected $childKey = false;
    protected $childFile;
    protected $plugin_handle = "mainwp-google-analytics-extension";
    protected $product_id = "MainWP Google Analytics Extension";
    protected $software_version = "1.8";

    public function __construct()
    {
        $this->childFile = __FILE__;
        add_filter('mainwp-getextensions', array(&$this, 'get_this_extension'));
        $this->mainwpMainActivated = apply_filters('mainwp-activated-check', false);

        if ($this->mainwpMainActivated !== false)
        {
            $this->activate_this_plugin();
        }
        else
        {
            add_action('mainwp-activated', array(&$this, 'activate_this_plugin'));
        }
        add_action('admin_init', array(&$this, 'admin_init'));
        add_action('admin_notices', array(&$this, 'mainwp_error_notice'));
    }

    function admin_init() {
        if (get_option('mainwp_google_analytics_extension_activated') == 'yes')
        {
            delete_option('mainwp_google_analytics_extension_activated');
            wp_redirect(admin_url('admin.php?page=Extensions'));
            return;
        }
    }

    public function getMetaboxes($metaboxes)
    {
        if (!$this->childEnabled) return $metaboxes;

        if (!is_array($metaboxes)) $metaboxes = array();
        $metaboxes[] = array('plugin' => $this->childFile, 'key' => $this->childKey, 'metabox_title' => MainWPGA::getName(), 'callback' => array(MainWPGA::getClassName(), 'render_metabox'));
        return $metaboxes;
    }

    function get_this_extension($pArray)
    {
        $pArray[] = array('plugin' => __FILE__, 'api' => $this->plugin_handle, 'mainwp' => true, 'callback' => array(&$this, 'settings'), 'apiManager' => true);
        return $pArray;
    }

    function settings()
    {
        do_action('mainwp-pageheader-extensions', __FILE__);
        if ($this->childEnabled)
        {
            $updated = MainWPGA::handleSettingsPost();

            ?>
            <?php if ($updated)
            {
                ?>
            <div class="mainwp-notice mainwp-notice-green"><?php _e('Your settings have been saved.','mainwp-google-analytics-extension'); ?></div>
            <?php
            }

            MainWPGA::handleUpdateGA();

            ?>
            <form method="POST" action="" id="mainwp-ga-settings-page-form">
                <?php
                MainWPGA::renderSettings(); //Takes care of post & visual
                ?>
                <p class="submit"><input type="submit" name="submit" id="submit" class="button-primary" value="<?php _e('Save Settings','mainwp-google-analytics-extension'); ?>"/>
                </p>
            </form>
        <?php
        }
        else
        {
                ?><div class="mainwp_info-box-yellow"><strong><?php _e("The Extension has to be enabled to change the settings."); ?></strong></div><?php
        }
        do_action('mainwp-pagefooter-extensions', __FILE__);
    }

    function activate_this_plugin()
    {
        if ( version_compare(phpversion(), '5.5') < 0 ) {
            echo '<div class="error">' . __('MainWP Google Analytics extension requires PHP 5.5 or higher. Activating the extension on older versions of PHP can cause fatal errors. Please contact your host support and have them update PHP for you.', 'mainwp') . '</div>';
            return;
        }

        $this->mainwpMainActivated = apply_filters('mainwp-activated-check', $this->mainwpMainActivated);
        $this->childEnabled = apply_filters('mainwp-extension-enabled-check', __FILE__);
        $this->childKey = $this->childEnabled['key'];
        if (function_exists("mainwp_current_user_can")&& !mainwp_current_user_can("extension", "mainwp-google-analytics-extension"))
            return;
        add_filter('mainwp-getmetaboxes', array(&$this, 'getMetaboxes'));
        new MainWPGoogleAnalyticsExtension();
    }

    function mainwp_error_notice()
    {
        global $current_screen;
        if ($current_screen->parent_base == 'plugins' && $this->mainwpMainActivated == false)
        {
            echo '<div class="error"><p>MainWP Google Analytics Extension ' . __('requires <a href="http://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> to be activated in order to work. Please install and activate <a href="http://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> first.') . '</p></div>';
        }
    }

    public function getChildKey()
    {
        return $this->childKey;
    }

    public function getChildFile()
    {
        return $this->childFile;
    }

    public function update_option($option_name, $option_value)
    {
        $success = add_option($option_name, $option_value, '', 'no');

         if (!$success)
         {
             $success = update_option($option_name, $option_value);
         }

         return $success;
    }

    public function activate() {
        $options = array (  'product_id' => $this->product_id,
                            'activated_key' => 'Deactivated',
                            'instance_id' => apply_filters('mainwp-extensions-apigeneratepassword', 12, false),
                            'software_version' => $this->software_version
                        );
        $this->update_option($this->plugin_handle . "_APIManAdder", $options);
    }

    public function deactivate() {
        $this->update_option($this->plugin_handle . "_APIManAdder", '');
    }
}

global $mainWPGoogleAnalyticsExtensionActivator;
$mainWPGoogleAnalyticsExtensionActivator = new MainWPGoogleAnalyticsExtensionActivator();
