jQuery(document).ready(function () {
    jQuery('#mainwp_ga_connect').live('click', function (event) {
        mainwp_ga_connect(this, event);
    });
    jQuery('#mainwp_ga_disconnect').live('click', function (event) {
        mainwp_ga_disconnect(this, event);
    });
    jQuery('#mainwp_ga_updatenow').live('click', function (event) {       
       location.href=window.location.href + '&GAUpdate=1'
    });
    jQuery('#mainwp_widget_ga_site').live('change', function (event) {
        mainwp_ga_getstats();
    });
    jQuery(document).on('click', '#mainwp_ga_show_extra', function(event) {
        var extraContent = jQuery('#mainwp_ga_extra');
        if (extraContent.is(':visible'))
        {
            jQuery('#mainwp_ga_show_extra span').html(__('more'));
        }
        else
        {
            jQuery('#mainwp_ga_show_extra span').html(__('less'));
        }
        extraContent.toggle();
        return false;
    });
});



mainwp_ga_remove_account_row = function(pObj) {
    var rid = jQuery(pObj).closest('tr').attr('row-id');
    jQuery('.mainwp_ga_account_row_client_id[row-id="' + rid + '"]').fadeOut().remove(); 
    jQuery('.mainwp_ga_account_row_client_secret[row-id="' + rid + '"]').fadeOut().remove();     
    jQuery(pObj).closest('tr').fadeOut().remove();    
}


mainwp_ga_disconnect = function (pObj, event) {
    jQuery(pObj).attr('disabled', 'true'); //Disable
    var gaEntryId = jQuery(pObj).attr('mainwp_ga_entry');    
    var data = {
        action:'mainwp_ga_disconnect',
        gaId: gaEntryId
    };
    
    jQuery.post(ajaxurl, data, function (response) {
        response = jQuery.trim(response);
        jQuery(pObj).removeAttr('disabled'); //Enable
        location.href = 'admin.php?page=Extensions-Mainwp-Google-Analytics-Extension';
    });
};
mainwp_ga_connect = function (pObj, event) {
    jQuery(pObj).attr('disabled', 'true'); //Disable
    var rid = jQuery(pObj).closest('tr').attr('row-id');
    var clientId = jQuery('.mainwp_ga_account_row_client_id[row-id="' + rid + '"]').find('.row_client_id').val();
    var clientSecret = jQuery('.mainwp_ga_account_row_client_secret[row-id="' + rid + '"]').find('.row_client_secret').val();
    var accountName = jQuery('.mainwp_ga_account_row_client_secret[row-id="' + rid + '"]').find('.row_account_name').val();
    var data = {
        action:'mainwp_ga_connect',
        name: accountName,
        client_id: clientId,
        client_secret: clientSecret,
    };
    jQuery.post(ajaxurl, data, function (response) {
        response = jQuery.trim(response);
        var res = jQuery.parseJSON(response);
        if (res['error']) {
            alert(__('An error occured: ') + res['error']);
        } else if (res['url']) {
            location.href = res['url'];
        }
        jQuery(pObj).removeAttr('disabled'); //Enable
    });
};
mainwp_ga_getstats = function () {
    var mainwp_widgetGACurrentSite = jQuery('#mainwp_widget_ga_site').val();
    if (mainwp_widgetGACurrentSite) {
        var data = {
            action:'mainwp_ga_getstats',
            id:mainwp_widgetGACurrentSite
        };
        jQuery.post(ajaxurl, data, function (response) {
            response = jQuery.trim(response);
            jQuery('#mainwp_widget_ga_content').html(response);
        });
    }
    else {
        jQuery('#mainwp_widget_ga_content').html(__('No data available. Connect your sites using the Settings submenu.'));
    }
};

jQuery(document).ready(function($) {       
    jQuery('.mainwp-show-tut').on('click', function(){
        jQuery('.mainwp-ga-tut').hide();   
        var num = jQuery(this).attr('number');
        jQuery('.mainwp-ga-tut[number="' + num + '"]').show();
        ga_setCookie('ga_quick_tut_number', jQuery(this).attr('number'));
        return false;
    }); 
    
    jQuery('#mainwp-ga-quick-start-guide').on('click', function () {
        if(ga_getCookie('ga_quick_guide') == 'on')
            ga_setCookie('ga_quick_guide', '');
        else 
            ga_setCookie('ga_quick_guide', 'on');        
        ga_showhide_quick_guide();
        return false;
    });
    jQuery('#mainwp-ga-tips-dismiss').on('click', function () {    
        ga_setCookie('ga_quick_guide', '');
        ga_showhide_quick_guide();
        return false;
    });
    
    ga_showhide_quick_guide();

    jQuery('#mainwp-ga-dashboard-tips-dismiss').on('click', function () {    
        $(this).closest('.mainwp_info-box-yellow').hide();
        ga_setCookie('ga_dashboard_notice', 'hide', 2);        
        return false;
    });    

});

ga_showhide_quick_guide = function(show, tut) {
    var show = ga_getCookie('ga_quick_guide');
    var tut = ga_getCookie('ga_quick_tut_number');
    
    if (show == 'on') {
        jQuery('#mainwp-ga-tips').show();
        jQuery('#mainwp-ga-quick-start-guide').hide();   
        ga_showhide_quick_tut();        
    } else {
        jQuery('#mainwp-ga-tips').hide();
        jQuery('#mainwp-ga-quick-start-guide').show();    
    }
    
    if ('hide' == ga_getCookie('ga_dashboard_notice')) {
        jQuery('#mainwp-ga-dashboard-tips-dismiss').closest('.mainwp_info-box-yellow').hide();
    }
}

ga_showhide_quick_tut = function() {
    var tut = ga_getCookie('ga_quick_tut_number');
    jQuery('.mainwp-ga-tut').hide();   
    jQuery('.mainwp-ga-tut[number="' + tut + '"]').show();   
}

function ga_setCookie(c_name, value, expiredays)
{
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : ";expires=" + exdate.toUTCString());
}
function ga_getCookie(c_name)
{
    if (document.cookie.length > 0)
    {
        var c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1)
        {
            c_start = c_start + c_name.length + 1;
            var c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1)
                c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}