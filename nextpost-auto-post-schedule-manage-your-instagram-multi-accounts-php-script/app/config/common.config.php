<?php 
// Used for cache control
define("VERSION", "040002"); 

// Application version name
define("APP_VERSION", "4.0.2"); 

// Default timezone
date_default_timezone_set("UTC"); 

// Defaullt multibyte encoding
mb_internal_encoding("UTF-8"); 
