<?php
if ( ! defined( 'ABSPATH' ) ) exit;


	/***
	***	@custom notice
	***/
	add_filter('um_custom_success_message_handler', 'um_woocommerce_custom_notice', 10, 2 );
	function um_woocommerce_custom_notice( $msg, $err_t ) {
		
		if ( $err_t == 'edit-billing' ) {
			$msg = __( 'Your billing address is updated.', 'um-woocommerce' );
		}
		
		if ( $err_t == 'edit-shipping' ) {
			$msg = __( 'Your shipping address is updated.', 'um-woocommerce' );
		}

		return $msg;
	}

	
	/***
	***	@add tab to account page
	***/
	add_filter( 'um_account_page_default_tabs_hook', 'um_woocommerce_account_tabs', 100 );
	function um_woocommerce_account_tabs( $tabs ) {

		if ( um_user( 'woo_account_billing' ) && ! UM()->options()->get( 'woo_hide_billing_tab_from_account' ) ) {
			$tabs[210]['billing']['icon'] = 'um-faicon-credit-card';
			$tabs[210]['billing']['title'] = __( 'Billing Address', 'um-woocommerce' );
			$tabs[210]['billing']['submit_title'] = __( 'Save Address', 'um-woocommerce' );
			$tabs[210]['billing']['custom'] = true;
		}

		if ( um_user( 'woo_account_shipping' ) && ! UM()->options()->get('woo_hide_shipping_tab_from_account') ) {
			$tabs[220]['shipping']['icon'] = 'um-faicon-truck';
			$tabs[220]['shipping']['title'] = __( 'Shipping Address', 'um-woocommerce' );
			$tabs[220]['shipping']['submit_title'] = __( 'Save Address', 'um-woocommerce' );
			$tabs[220]['shipping']['custom'] = true;
		}

		// if ( um_user( 'woo_account_subscription' ) && ! UM()->options()->get('woo_hide_subscription_tab_from_account') ) {
		if ( class_exists( 'WC_Subscriptions' ) ) {
			$tabs[240]['subscription']['icon'] = 'um-faicon-book';
			$tabs[240]['subscription']['title'] = __( 'Subscriptions', 'um-woocommerce' );
			$tabs[240]['subscription']['show_button'] = false;
			$tabs[240]['subscription']['custom'] = true;
		}
		//}

		if ( um_user( 'woo_account_orders' ) ) {
			$tabs[230]['orders']['icon'] = 'um-faicon-shopping-cart';
			$tabs[230]['orders']['title'] = __( 'My Orders', 'um-woocommerce' );
			$tabs[230]['orders']['show_button'] = false;
			$tabs[230]['orders']['custom'] = true;
		}
		
		return $tabs;
	}


	add_filter('um_account_content_hook_subscription', 'um_account_content_hook_subscription');
	function um_account_content_hook_subscription( $output ){
		ob_start();

		do_action( 'woocommerce_add_subscriptions_to_my_account' );

		$output .= ob_get_clean();

		return do_shortcode( $output );

	}	


	/***
	***	@add content to account tab
	***/
	add_filter('um_account_content_hook_orders', 'um_account_content_hook_orders');
	function um_account_content_hook_orders( $output ){
		ob_start(); ?>
		
		<div class="um-woo-form um-woo-orders">
		
			<?php $orders_page = isset( $_REQUEST['orders_page'] ) ? $_REQUEST['orders_page'] : 1;
			$page = $orders_page;
			$orders_per_page = 10;
			$args = apply_filters( "um_woocommerce_account_orders_args", array(
				'posts_per_page'	=> $orders_per_page,
				'paged'				=> $orders_page,
				'meta_key'    		=> '_customer_user',
				'meta_value'  		=> get_current_user_id(),
				'post_type' 		=> wc_get_order_types( 'view-orders' ),
				'post_status' 		=> array_keys( wc_get_order_statuses() ),
				'order'				=> 'ASC'
			) );

			$loop = new WP_Query( $args );

			$total_pages =  ceil( $loop->found_posts / $orders_per_page );
			$pages_to_show = $total_pages ;

			$order = '';
			$url = UM()->account()->tab_link( 'orders' );

			$customer_orders = $loop->posts;

			if ( $total_pages ) { ?>

				<table class="shop_table shop_table_responsive my_account_orders">

					<thead>
						<tr>
						<?php do_action('um_woocommerce_orders_tab_before_table_header_row', $order, $customer_orders ); ?>
							<th class="order-date"><span class="nobr"><?php _e( 'Date', 'woocommerce' ); ?></span></th>
							<th class="order-status"><span class="nobr"><?php _e( 'Status', 'woocommerce' ); ?></span></th>
							<th class="order-total"><span class="nobr"><?php _e( 'Total', 'woocommerce' ); ?></span></th>
							<th class="order-actions">&nbsp;</th>
						<?php do_action('um_woocommerce_orders_tab_after_table_header_row', $order, $customer_orders ); ?>
						</tr>
					</thead>

					<tbody>
						<?php foreach ( $customer_orders as $customer_order ) {
							$order = wc_get_order( $customer_order->ID );
							$order_id = $customer_order->ID; ?>

							<tr class="order" data-order_id="<?php echo $order_id; ?>">
								<?php do_action('um_woocommerce_orders_tab_before_table_row', $order, $customer_orders ); ?>
								<td class="order-date" data-title="<?php _e( 'Date', 'woocommerce' ); ?>">
									<time datetime="<?php echo date( 'Y-m-d', strtotime( $order->get_date_created() ) ); ?>" title=""><?php echo date_i18n( 'j M y - h:i A', strtotime( $order->get_date_created() ) ); ?></time>
								</td>
								<td class="order-status" data-title="<?php _e( 'Status', 'woocommerce' ); ?>" style="text-align:left; white-space:nowrap;">
									<span class="um-woo-status <?php echo $order->get_status(); ?>"><?php echo wc_get_order_status_name( $order->get_status() ); ?></span>
								</td>
								<td class="order-total" data-title="<?php _e( 'Total', 'woocommerce' ); ?>"><?php echo $order->get_formatted_order_total() ?></td>
								<td class="order-detail">
									<?php echo '<a href="' . $url . '#!/' . $order_id . '" class="um-woo-view-order um-tip-n" title="'.__('View order','um-woocommerce').'"><i class="um-icon-eye"></i></a>'; ?>
								</td>
								<?php do_action('um_woocommerce_orders_tab_after_table_row', $order, $customer_orders ); ?>
							</tr><?php
						} ?>
					</tbody>

				</table>

				<div class="um-members-pagidrop uimob340-show uimob500-show">

					<?php _e('Jump to page:','um-woocommerce');

					if ( $pages_to_show ) { ?>
						<select onChange="window.location.href=this.value" class="um-s2" style="width: 100px">
							<?php for( $i = 1; $i<=$pages_to_show; $i++ ) { ?>
							<option value="<?php echo '?orders_page='.$i; ?>" <?php selected($i, $page ); ?>><?php printf(__('%s of %d','um-woocommerce'), $i, $total_pages ); ?></option>
							<?php } ?>
						</select>
					<?php } ?>

				</div>

				<div class="um-members-pagi uimob340-hide uimob500-hide">

					<?php if ( $page != 1 ) { ?>
						<a href="<?php echo '?orders_page=1'; ?>" class="pagi pagi-arrow um-tip-n" title="<?php _e('First Page','um-woocommerce'); ?>"><i class="um-faicon-angle-double-left"></i></a>
					<?php } else { ?>
						<span class="pagi pagi-arrow disabled"><i class="um-faicon-angle-double-left"></i></span>
					<?php }

					if ( $page > 1 ) { ?>
						<a href="<?php echo '?orders_page='.( $page - 1 ); ?>" class="pagi pagi-arrow um-tip-n" title="<?php _e('Previous','um-woocommerce'); ?>"><i class="um-faicon-angle-left"></i></a>
					<?php } else { ?>
						<span class="pagi pagi-arrow disabled"><i class="um-faicon-angle-left"></i></span>
					<?php }

					if ( $pages_to_show ) {
						for( $i = 1; $i<=$pages_to_show; $i++ ) {
							if ( $page == $i ) { ?>
								<span class="pagi current"><?php echo $i; ?></span>
							<?php } else { ?>
								<a href="<?php echo '?orders_page='.$i; ?>" class="pagi"><?php echo $i; ?></a>
							<?php }
						}
					}

					if ( $page != $total_pages ) { ?>
						<a href="<?php echo '?orders_page='.( $page + 1 ); ?>" class="pagi pagi-arrow um-tip-n" title="<?php _e('Next','um-woocommerce'); ?>"><i class="um-faicon-angle-right"></i></a>
					<?php } else { ?>
						<span class="pagi pagi-arrow disabled"><i class="um-faicon-angle-right"></i></span>
					<?php }

					if ( $page != $total_pages ) { ?>
						<a href="<?php echo '?orders_page='.( $total_pages ); ?>" class="pagi pagi-arrow um-tip-n" title="<?php _e('Last Page','um-woocommerce'); ?>"><i class="um-faicon-angle-double-right"></i></a>
					<?php } else { ?>
						<span class="pagi pagi-arrow disabled"><i class="um-faicon-angle-double-right"></i></span>
					<?php } ?>

				</div>

			<?php } else { ?>
				<div class="um-field"><?php _e( 'You don\'t have orders yet', 'um-woocommerce' ); ?></div>
			<?php } ?>
	
		</div>
		
		<?php $output .= ob_get_clean();

		return do_shortcode( $output );
	}


	/***
	***	@add content to account tab
	***/
	add_filter('um_account_content_hook_billing', 'um_account_content_hook_billing');
	function um_account_content_hook_billing( $output ){
		global $wp;
        $wp->query_vars['edit-address'] = 'billing';
	    ob_start();
		?>
		
		<div class="um-woo-form um-woo-billing">
			<?php UM()->WooCommerce_API()->api()->edit_address( 'billing' ); ?>
		</div>
		
		<?php $output .= ob_get_clean();

		return do_shortcode( $output );
	}


	/***
	***	@add content to account tab
	***/
	add_filter('um_account_content_hook_shipping', 'um_account_content_hook_shipping');
	function um_account_content_hook_shipping( $output ){
	    global $wp;
        $wp->query_vars['edit-address'] = 'shipping';
		ob_start(); ?>
		
		<div class="um-woo-form um-woo-shipping">
			<?php UM()->WooCommerce_API()->api()->edit_address('shipping'); ?>
		</div>
		
		<?php $output .= ob_get_clean();

		return do_shortcode( $output );
	}