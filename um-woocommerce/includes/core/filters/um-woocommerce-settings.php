<?php if ( ! defined( 'ABSPATH' ) ) exit;


/**
 * Extend settings
 *
 * @param $settings
 *
 * @return mixed
 */
function um_woocommerce_settings( $settings ) {

	$settings['licenses']['fields'][] = array(
		'id'      		=> 'um_woocommerce_license_key',
		'label'    		=> __( 'Woocommerce License Key', 'um-woocommerce' ),
		'item_name'     => 'WooCommerce',
		'author' 	    => 'Ultimate Member',
		'version' 	    => um_woocommerce_version,
	);

	$key = ! empty( $settings['extensions']['sections'] ) ? 'woocommerce' : '';
	$settings['extensions']['sections'][$key] = array(
		'title'     => __( 'Woocommerce', 'um-woocommerce' ),
		'fields'    => array(
            array(
                'id'       		=> 'woo_remove_roles',
                'type'     		=> 'select',
                'label'    		=> __( 'Remove previous roles when change role on complete or refund payment', 'um-woocommerce' ),
                'tooltip'    	=> __( 'If yes then remove all users roles and add current, else add current role to other roles, which user already has', 'um-woocommerce' ),
                'options' 		=> array( 0 => __( 'No', 'um-woocommerce' ), 1 => __( 'Yes', 'um-woocommerce' ) ),
                'size'          => 'small',
            ),
		    array(
				'id'       		=> 'woo_oncomplete_role',
				'type'     		=> 'select',
				'label'    		=> __( 'Assign this role to users when payment is completed', 'um-woocommerce' ),
				'tooltip' 	    => __( 'Automatically set the user this role when a payment is completed.', 'um-woocommerce' ),
				'options' 		=> array( '' => __( 'None', 'um-woocommerce' ) ) + UM()->roles()->get_roles(),
				'placeholder' 	=> __( 'Community role...', 'um-woocommerce' ),
				'size' => 'small',
			),
			array(
				'id'       		=> 'woo_oncomplete_except_roles',
				'type'     		=> 'select',
				'label'    		=> __( 'Do not update these roles when payment is completed', 'um-woocommerce' ),
				'tooltip' 	=> __( 'Only applicable if you assigned a role when payment is completed.', 'um-woocommerce' ),
				'options' 		=> UM()->roles()->get_roles(),
				'placeholder' 	=> __( 'Community role(s)..', 'um-woocommerce' ),
				'multi'         => true,
				'size' => 'small'
			),
			array(
				'id'       		=> 'woo_onhold_change_roles',
				'type'     		=> 'select',
				'label'    		=> __( 'Upgrade user role when payment is on-hold before complete or processing status', 'um-woocommerce' ),
				'options' 		=> array( 0 => __( 'No', 'um-woocommerce' ), 1 => __( 'Yes', 'um-woocommerce' ) ),
				'size'          => 'small',
				'conditional'	=> array( 'woo_oncomplete_role', '!=', '' )
			),
			array(
				'id'       		=> 'woo_onrefund_role',
				'type'     		=> 'select',
				'label'    		=> __( 'Assign this role to users when payment is refunded', 'um-woocommerce' ),
				'tooltip' 	    => __( 'Automatically set the user this role when a payment is refunded.', 'um-woocommerce' ),
				'options' 		=> array( '' => __( 'None', 'um-woocommerce' ) ) + UM()->roles()->get_roles(),
				'placeholder' 	=> __( 'Community role...', 'um-woocommerce' ),
				'size'          => 'small',
			),
			array(
				'id'       		=> 'woo_hide_billing_tab_from_account',
				'type'     		=> 'checkbox',
				'label'   		=> __( 'Hide billing tab from members in account page','um-woocommerce' ),
				'tooltip' 	=> __( 'Enable this option If you do not want to show the billing tab from members in account page.', 'um-woocommerce' ),
			),
			array(
				'id'       		=> 'woo_hide_shipping_tab_from_account',
				'type'     		=> 'checkbox',
				'label'   		=> __( 'Hide shipping tab from members in account page','um-woocommerce' ),
				'tooltip' 	=> __( 'Enable this option If you do not want to show the shipping tab from members in account page.', 'um-woocommerce' ),
			)
		)
	);

	return $settings;
}
add_filter( 'um_settings_structure', 'um_woocommerce_settings', 10, 1 );