<?php if ( ! defined( 'ABSPATH' ) ) exit;


/**
 * Class UM_WooCommerce_API
 */
class UM_WooCommerce_API {


	/**
	 * @var
	 */
	private static $instance;


	/**
	 * @return UM_WooCommerce_API
	 */
	static public function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}


	/**
	 * UM_WooCommerce_API constructor.
	 */
	function __construct() {
		// Global for backwards compatibility.
		$GLOBALS['um_woocommerce'] = $this;
		add_filter( 'um_call_object_WooCommerce_API', array( &$this, 'get_this' ) );

		$this->enqueue();
		$this->access();
		$this->account();

		if ( UM()->is_request( 'admin' ) ) {
			$this->admin();
		}

		add_action( 'plugins_loaded', array( &$this, 'init' ), 0 );

		add_filter( 'um_settings_default_values', array( &$this, 'default_settings' ), 10, 1 );
	}


	/**
	 * @param $defaults
	 *
	 * @return array
	 */
	function default_settings( $defaults ) {
		$defaults = array_merge( $defaults, $this->setup()->settings_defaults );
		return $defaults;
	}


	/**
	 * @return um_ext\um_woocommerce\core\WooCommerce_Setup()
	 */
	function setup() {
		if ( empty( UM()->classes['um_woocommerce_setup'] ) ) {
			UM()->classes['um_woocommerce_setup'] = new um_ext\um_woocommerce\core\WooCommerce_Setup();
		}
		return UM()->classes['um_woocommerce_setup'];
	}


	/**
	 * @return $this
	 */
	function get_this() {
		return $this;
	}


	/**
	 * @return um_ext\um_woocommerce\core\WooCommerce_Main_API()
	 */
	function api() {
		if ( empty( UM()->classes['um_woocommerce_api'] ) ) {
			UM()->classes['um_woocommerce_api'] = new um_ext\um_woocommerce\core\WooCommerce_Main_API();
		}
		return UM()->classes['um_woocommerce_api'];
	}


	/**
	 * @return um_ext\um_woocommerce\core\WooCommerce_Enqueue()
	 */
	function enqueue() {
		if ( empty( UM()->classes['um_woocommerce_enqueue'] ) ) {
			UM()->classes['um_woocommerce_enqueue'] = new um_ext\um_woocommerce\core\WooCommerce_Enqueue();
		}
		return UM()->classes['um_woocommerce_enqueue'];
	}


	/**
	 * @return um_ext\um_woocommerce\core\WooCommerce_Access()
	 */
	function access() {
		if ( empty( UM()->classes['um_woocommerce_access'] ) ) {
			UM()->classes['um_woocommerce_access'] = new um_ext\um_woocommerce\core\WooCommerce_Access();
		}
		return UM()->classes['um_woocommerce_access'];
	}


	/**
	 * @return um_ext\um_woocommerce\admin\core\Admin()
	 */
	function admin() {
		if ( empty( UM()->classes['um_woocommerce_admin'] ) ) {
			UM()->classes['um_woocommerce_admin'] = new um_ext\um_woocommerce\admin\core\Admin();
		}
		return UM()->classes['um_woocommerce_admin'];
	}


	/**
	 * @return um_ext\um_woocommerce\core\WooCommerce_Account()
	 */
	function account() {
		if ( empty( UM()->classes['um_woocommerce_account'] ) ) {
			UM()->classes['um_woocommerce_account'] = new um_ext\um_woocommerce\core\WooCommerce_Account();
		}
		return UM()->classes['um_woocommerce_account'];
	}


	/**
	 * Init
	 */
	function init() {

		// Actions
		require_once um_woocommerce_path . 'includes/core/actions/um-woocommerce-tabs.php';
		require_once um_woocommerce_path . 'includes/core/actions/um-woocommerce-order.php';
		
		// Filters
		require_once um_woocommerce_path . 'includes/core/filters/um-woocommerce-fields.php';
		require_once um_woocommerce_path . 'includes/core/filters/um-woocommerce-reviews.php';
		require_once um_woocommerce_path . 'includes/core/filters/um-woocommerce-tabs.php';

	}
}

//create class var
add_action( 'plugins_loaded', 'um_init_woocommerce', -10, 1 );
function um_init_woocommerce() {
	if ( function_exists( 'UM' ) ) {
		UM()->set_class( 'WooCommerce_API', true );
	}
}