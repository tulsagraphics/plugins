<?php if ( ! defined( 'ABSPATH' ) ) exit;


/**
 * Create role options
 *
 * @param $roles_metaboxes
 *
 * @return array
 */
function um_woocommerce_add_role_metabox( $roles_metaboxes ) {
	$roles_metaboxes[] = array(
		'id'        => "um-admin-form-woocommerce{" . um_woocommerce_path . "}",
		'title'     => __( 'WooCommerce', 'um-woocommerce' ),
		'callback'  => array( UM()->metabox(), 'load_metabox_role' ),
		'screen'    => 'um_role_meta',
		'context'   => 'normal',
		'priority'  => 'default'
	);

	return $roles_metaboxes;
}
add_filter( 'um_admin_role_metaboxes', 'um_woocommerce_add_role_metabox', 10, 1 );


/**
 * Add handler for save metabox fields content
 *
 * @param $post_id
 * @param $post
 *
 * @return mixed
 */
function um_save_metabox_product_settings( $post_id, $post ) {
	//make this handler only on product form submit
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		return $post_id;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post_id;
	}

	if ( empty( $_REQUEST['_wpnonce'] ) ) {
		return $post_id;
	}

	if ( empty( $post->post_type ) || 'product' != $post->post_type ) {
		return $post_id;
	}

	if ( ! empty( $_POST['_um_woo_product_role'] ) ) {
		update_post_meta( $post_id, '_um_woo_product_role', $_POST['_um_woo_product_role'] );
	} else {
		delete_post_meta( $post_id, '_um_woo_product_role' );
	}

	if ( function_exists( 'wcs_get_subscription' ) ) {
		if ( ! empty( $_POST['_um_woo_product_activated_role'] ) ) {
			update_post_meta( $post_id, '_um_woo_product_activated_role', $_POST['_um_woo_product_activated_role'] );
		} else {
			delete_post_meta( $post_id, '_um_woo_product_activated_role' );
		}

		if ( ! empty( $_POST['_um_woo_product_downgrade_pending_role'] ) ) {
			update_post_meta( $post_id, '_um_woo_product_downgrade_pending_role', $_POST['_um_woo_product_downgrade_pending_role'] );
		} else {
			delete_post_meta( $post_id, '_um_woo_product_downgrade_pending_role' );
		}

		if ( ! empty( $_POST['_um_woo_product_downgrade_onhold_role'] ) ) {
			update_post_meta( $post_id, '_um_woo_product_downgrade_onhold_role', $_POST['_um_woo_product_downgrade_onhold_role'] );
		} else {
			delete_post_meta( $post_id, '_um_woo_product_downgrade_onhold_role' );
		}

		if ( ! empty( $_POST['_um_woo_product_downgrade_expired_role'] ) ) {
			update_post_meta( $post_id, '_um_woo_product_downgrade_expired_role', $_POST['_um_woo_product_downgrade_expired_role'] );
		} else {
			delete_post_meta( $post_id, '_um_woo_product_downgrade_expired_role' );
		}

		if ( ! empty( $_POST['_um_woo_product_downgrade_cancelled_role'] ) ) {
			update_post_meta( $post_id, '_um_woo_product_downgrade_cancelled_role', $_POST['_um_woo_product_downgrade_cancelled_role'] );
		} else {
			delete_post_meta( $post_id, '_um_woo_product_downgrade_cancelled_role' );
		}
	}

	return $post_id;
}
add_action( 'save_post', 'um_save_metabox_product_settings', 10, 2 );