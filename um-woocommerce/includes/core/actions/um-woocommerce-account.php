<?php
if ( ! defined( 'ABSPATH' ) ) exit;


	/***
	***	@hook before woocommerce update address
	***/
	add_action('template_redirect', 'um_woocommerce_pre_update', 1 );
	function um_woocommerce_pre_update() {
		global $wp;

		if ( isset( $_POST['um_account_submit'] ) && get_query_var('um_tab') == 'shipping' ) {
			$wp->query_vars['edit-address'] = 'shipping';
		}

		if ( isset( $_POST['um_account_submit'] ) && get_query_var('um_tab') == 'billing' ) {
			$wp->query_vars['edit-address'] = 'billing';
		}

		if ( wc_has_notice( __( 'Address changed successfully.', 'woocommerce' ) ) ) {
			wc_clear_notices();
			$url = UM()->account()->tab_link( 'billing' );
			exit( wp_redirect( add_query_arg('updated','edit-billing', $url ) ) );
		}

	}

	// update billing email when the user's email address is changed
	add_action( 'um_update_profile_full_name', 'um_sync_update_user_wc_email', 10, 2 );
	function um_sync_update_user_wc_email( $user_id, $changes ) {
		if(isset($changes['user_email'])) {
			update_user_meta( UM()->user()->id, 'billing_email', $changes['user_email']);
		}

		if(isset($changes['first_name'])) {
			update_user_meta( UM()->user()->id, 'billing_first_name', $changes['first_name']);
		}

		if(isset($changes['last_name'])) {
			update_user_meta( UM()->user()->id, 'billing_last_name', $changes['last_name']);
		}
	}

	// update um profile when wc billing is updated
	add_action( 'woocommerce_checkout_update_user_meta', 'um_update_um_profile_from_wc_billing', 10, 2 );
	add_action( 'woocommerce_customer_save_address', 'um_update_um_profile_from_wc_billing', 10, 2 );
	function um_update_um_profile_from_wc_billing($user_id, $data = null) {

		if ( isset( $_POST['um_account_submit'] ) && isset( $_POST[ 'billing_first_name'] ) && isset( $_POST['billing_last_name'] ) && isset( $_POST[ 'billing_email' ] ) ) {
			$changes = array();
			foreach($_POST as $key => $value) {
				if(preg_match('/^billing_/', $key)) {
					$key           = str_replace('billing_', '', $key);

					if (in_array($key, array('first_name', 'last_name', 'user_email'))) {
						$changes[$key] = $value;

						update_user_meta( $user_id, $key, $value );
					}
				}
			}

			// update email
			$args = array('ID' => $user_id, 'user_email' => $_POST['billing_email']);
			wp_update_user($args);

			// hook for name changes
			do_action( 'um_update_profile_full_name', $user_id, $changes );

			UM()->user()->remove_cache( $user_id );
		}
	}

	add_action('um_after_user_account_updated', 'um_call_wc_user_account_update', 99, 2);
	function um_call_wc_user_account_update( $user_id, $changes ) {
		global $wp;

		if( $wp->query_vars['edit-address'] == 'billing' || $wp->query_vars['edit-address'] == 'shipping' ) {
			do_action( 'woocommerce_customer_save_address', $user_id, $wp->query_vars['edit-address'] );
		}

		if ( isset( $_POST['um_account_submit'] ) && get_query_var('um_tab') == 'shipping' ) {
			exit( wp_redirect( add_query_arg('updated','edit-shipping') ) );
		}

		if ( isset( $_POST['um_account_submit'] ) && get_query_var('um_tab') == 'billing' ) {
			exit( wp_redirect( add_query_arg('updated','edit-billing') ) );
		}
	}