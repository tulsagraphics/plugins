<?php
namespace um_ext\um_woocommerce\core;

if ( ! defined( 'ABSPATH' ) ) exit;

class WooCommerce_Enqueue {

	function __construct() {
	
		add_action( 'wp_enqueue_scripts',  array( &$this, 'wp_enqueue_scripts' ), 9999 );

        add_filter( 'um_enqueue_localize_data',  array( &$this, 'localize_data' ), 10, 1 );
	}


    function localize_data( $data ) {

        $data['woocommerce_get_order'] = UM()->get_ajax_route( 'um_ext\um_woocommerce\core\WooCommerce_Main_API', 'ajax_get_order' );
        $data['woocommerce_get_subscription'] = UM()->get_ajax_route( 'um_ext\um_woocommerce\core\WooCommerce_Main_API', 'ajax_get_subscription' );

        return $data;

    }


	function wp_enqueue_scripts(){
		
		if ( !is_user_logged_in() ) return;

		wp_register_style( 'um_woocommerce', um_woocommerce_url . 'assets/css/um-woocommerce.css' );
		wp_enqueue_style( 'um_woocommerce' );
		
		wp_register_script( 'um_woocommerce', um_woocommerce_url . 'assets/js/um-woocommerce.js', '', '', true );
		wp_enqueue_script( 'um_woocommerce' );
		
	}
	
}