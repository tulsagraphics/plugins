msgid ""
msgstr ""
"Project-Id-Version: WP Cookie Allow Plugin\n"
"POT-Creation-Date: 2018-08-30 01:28+0100\n"
"PO-Revision-Date: 2018-08-30 01:28+0100\n"
"Last-Translator: Vincent Weber <vincent@webrtistik.nl>\n"
"Language-Team: WeePie Plugins <vincent@webrtistik.nl>\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"
"X-Poedit-KeywordsList: _e;__;_x\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../upgrade.php:33
#, php-format
msgid ""
"Hi! You have update to version %s. Please <strong>check your 3rd party list "
"items</strong> as the could have been 're-checked' during the update "
"process. Our apologies for the invonvenience, we're working on a solution!"
msgstr ""

#: ../wp-cookie-allow.php:42
msgid ""
"<strong>WeePie Framework Plugin could not be located.</strong><br/><br/"
">Please upload the <strong>\"weepie-framework\"</strong> folder to the "
"<strong>WordPress Plugins folder</strong>."
msgstr ""

#: ../modules/automate/wpca-module-automate.php:751
msgid "YouTube"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:764
msgid "Vimeo"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:769
msgid "Google Maps"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:777
msgid "Instagram"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:782
msgid "Slideshare"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:789
msgid "Soundcloud"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:796
msgid "Twitter"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:803
msgid "Google+"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:815 ../settings/I18n.php:76
msgid "Google Analytics"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:828
msgid "Facebook"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:842
msgid "Facebook Pixel Code"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:853
msgid "LinkedIn"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:860
msgid "Pinterest"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:875
msgid "Disqus"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:883
msgid "Disqus Comment System"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:888
msgid "AddThis"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:896
msgid "Sharethis"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:904
msgid "Vine"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:911
msgid "Google Adsense"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:922
msgid "Google Tag Manager"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:930
msgid "JotForm"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:939
msgid "Leadpages"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:945
msgid "Cloudflare"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:951
msgid "Bandcamp"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:958
msgid "Sharethis platform API"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:965
msgid "AddToAny"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:972
msgid "Hupso Share Buttons"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:979
msgid "Matomo (Piwik)"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:985
msgid "Amazon adsystem"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:995
msgid "Active Campaign"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1007
msgid "WP Stats"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1013
msgid "Twitter ads"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1023
msgid "LinkedIn Insight"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1029
msgid "Cxense"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1037
msgid "Pardot"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1043
msgid "Zopim"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1052
msgid "Sumo"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1062
msgid "OneSignal"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1069
msgid "LikeBtn"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1079
msgid "Tawk.to"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1087
msgid "Media.net "
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1094
msgid "Hotjar"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1103
msgid "HubSpot"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1113
msgid "Tripadvisor"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1125
msgid "StatCounter"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:1149
#, php-format
msgid ""
"Invalid index found in the thirdparties array. Index should be of type "
"'string'. Index found: %d."
msgstr ""

#: ../modules/automate/wpca-module-automate.php:2090
msgid "Cookie Category"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:2214
msgid "Live blocking has been enabled but no third parties has been checked."
msgstr ""

#: ../modules/automate/wpca-module-automate.php:2217
#, php-format
msgid ""
"Automatic cookie blocking has been disabled. %sCookies are not automatically "
"blocked now!%s"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:2514
msgid "Functional"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:2517
msgid "Analytical"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:2520
msgid "Social media"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:2523
msgid "Advertising"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:2526
msgid "Other"
msgstr ""

#: ../modules/automate/wpca-module-automate.php:3011
msgid "Automate 'action' is unknow."
msgstr ""

#: ../modules/automate/templates/wpca-general-automate-table.php:19
#: ../modules/automate-replace/wpca-module-automate-replace.php:219
msgid "3rd party"
msgstr ""

#: ../modules/automate/templates/wpca-general-automate-table.php:62
msgid "Refresh"
msgstr ""

#: ../modules/automate/templates/wpca-general-automate-table.php:72
msgid "Request missing 3rd party"
msgstr ""

#: ../modules/automate/templates/wpca-general-automate.php:8
msgid "Click here"
msgstr ""

#: ../modules/automate/templates/wpca-general-automate.php:8
msgid " to enable/disable your thirdparties manually."
msgstr ""

#: ../modules/automate-replace/templates/wpca-replacement-element.php:12
msgid "Content blocked by WeePie Cookie Allow Plugin"
msgstr ""

#: ../modules/consent-log/wpca-module-consent-log.php:407
msgid "Filtering on column \"%COL%\" is not possible."
msgstr ""

#: ../modules/consent-log/wpca-module-consent-log.php:408
msgid ""
"Filtering on column \"%COL%\" is not possible. Your value can be maximal: "
"%MAX%."
msgstr ""

#: ../modules/consent-log/wpca-module-consent-log.php:409
msgid "No results found."
msgstr ""

#: ../modules/consent-log/wpca-module-consent-log.php:410
msgid "Are you sure you want to delete this log?"
msgstr ""

#: ../modules/consent-log/wpca-module-consent-log.php:526
msgid "Invalid AJAX request context."
msgstr ""

#: ../modules/consent-log/wpca-module-consent-log.php:532
msgid "Required AJAX request data missing: consent."
msgstr ""

#: ../modules/consent-log/wpca-module-consent-log.php:536
msgid "Required AJAX request data missing or invalid: categories."
msgstr ""

#: ../modules/consent-log/wpca-module-consent-log.php:548
msgid "Invalid AJAX request data: "
msgstr ""

#: ../modules/consent-log/wpca-module-consent-log.php:675
#, php-format
msgid ""
"WeePie Cookie Allow: Consent Logging needs MySql version %s or higher. To "
"create a legacy table, add the following code to your functions.php and "
"deactivate/activate the plugin. <code>%s</code>"
msgstr ""

#: ../modules/consent-log/wpca-module-consent-log.php:681
msgid ""
"WeePie Cookie Allow: it seems that the Consent Logging database table hasn't "
"been created. Please deactivate/activate the plugin."
msgstr ""

#: ../modules/consent-log/wpca-module-consent-log.php:686
#, php-format
msgid ""
"WeePie Cookie Allow: Consent Logging could not anonymize IP Addresses. "
"Please <a href=\"%s\" target=\"_blank\">update your WordPress core</a> to at "
"least 4.9.6."
msgstr ""

#: ../modules/consent-log/templates/wpca-log-list-table.php:26
msgid "Delete logged consent for IP address: "
msgstr ""

#: ../modules/consent-log/templates/wpca-log-list-table.php:31
#, php-format
msgid "Nothing logged yet. %sDid you enabled logging%s?"
msgstr ""

#: ../modules/consent-log/templates/wpca-log-list.php:18
msgid "Search for IP addresses here"
msgstr ""

#: ../modules/consent-log/templates/wpca-log-list.php:28
msgid "Filter"
msgstr ""

#: ../modules/consent-log/templates/wpca-log-list.php:34
msgid ""
"Search for logged IP addresses in the database. type the 'full' address or "
"just a start or ending part: for example all addresses starting with 123 "
"type \"123%\". To query all, type \"*\"."
msgstr ""

#: ../modules/core/wpca-module-core.php:520
msgid "-- Select --"
msgstr ""

#: ../modules/core/wpca-module-core.php:536
msgid "No items found for post type \""
msgstr ""

#: ../modules/core/wpca-module-core.php:569
#, php-format
msgid ""
"The WeePie Cookie Allow Plugin is currently disabled. <a href=\"%s\">Enable "
"it on the settings page</a>"
msgstr ""

#: ../modules/geo/wpca-module-geo.php:240
#, php-format
msgid ""
"WeePie Cookie Allow: it seems that the GEO database file (GeoIP.dat or "
"GeoIPv6.dat) could not be located. Please verify the existence of these "
"files at: %s."
msgstr ""

#: ../modules/geo/wpca-module-geo.php:244
#, php-format
msgid ""
"WeePie Cookie Allow: it seems that the GeoIP class does not exist at %s."
msgstr ""

#: ../settings/I18n.php:3
msgid "Cookie policy page"
msgstr ""

#: ../settings/I18n.php:4
msgid "Post type"
msgstr ""

#: ../settings/I18n.php:5
msgid "Select your <strong>Cookie policy page</strong> Post Type."
msgstr ""

#: ../settings/I18n.php:6
msgid "Page"
msgstr ""

#: ../settings/I18n.php:7
msgid ""
"Select your <strong>Cookie policy page</strong> here and use it inside your "
"choosen lay-out."
msgstr ""

#: ../settings/I18n.php:8
msgid "Target"
msgstr ""

#: ../settings/I18n.php:9
msgid "Open the cookie policy page in a new browser tab or in the same."
msgstr ""

#: ../settings/I18n.php:10
msgid "Same browser tab"
msgstr ""

#: ../settings/I18n.php:11
msgid "New browser tab"
msgstr ""

#: ../settings/I18n.php:12
msgid "Cookie policy link text"
msgstr ""

#: ../settings/I18n.php:13
msgid ""
"To display the <strong>Cookie policy link text</strong> in your lay-out (bar "
"or box), use the shortcode <code>[wpca_cookie_policy_link]</code>."
msgstr ""

#: ../settings/I18n.php:14
msgid "Show bar/box?"
msgstr ""

#: ../settings/I18n.php:15
msgid "Show or hide the bar/box on your cookie policy page."
msgstr ""

#: ../settings/I18n.php:16
msgid "Reset consent"
msgstr ""

#: ../settings/I18n.php:17
msgid ""
"Please note that resetting the consent, will not always clear all cookies "
"set in the browser of the website visitor."
msgstr ""

#: ../settings/I18n.php:18
msgid "Button Reset consent text"
msgstr ""

#: ../settings/I18n.php:19
msgid "Button Reset consent text \"Resetting consent\""
msgstr ""

#: ../settings/I18n.php:20
msgid "Content bar/box"
msgstr ""

#: ../settings/I18n.php:21 ../settings/I18n.php:29 ../settings/I18n.php:38
msgid "Text"
msgstr ""

#: ../settings/I18n.php:22
msgid "Button Accept text"
msgstr ""

#: ../settings/I18n.php:23
msgid "Button Accept text \"Accepting\""
msgstr ""

#: ../settings/I18n.php:24
msgid "Button Decline text"
msgstr ""

#: ../settings/I18n.php:25
msgid "Button Decline text \"Declining\""
msgstr ""

#: ../settings/I18n.php:26
msgid "Content Cookie Category settings box"
msgstr ""

#: ../settings/I18n.php:27 ../settings/I18n.php:209
msgid "Please note this is a <strong>b&egrave;ta functionality</strong>."
msgstr ""

#: ../settings/I18n.php:28
msgid ""
"<strong>Cookie categories</strong> let website visitors decide which "
"<strong>type of cookie should be blocked or not</strong> after 'clicking' on "
"the \"<strong>Save cookie settings</strong>\" button or by your choosen "
"\"<strong>Consent method</strong>\". The following categories are allowed: "
"<code>functional</code>, <code>analytical</code>, <code>social-media</code>, "
"<code>advertising</code>, <code>other</code>. Add the box with the "
"shortcode: <code>[wpca_cc_settings_box]</code>"
msgstr ""

#: ../settings/I18n.php:30
msgid "Button default settings text"
msgstr ""

#: ../settings/I18n.php:31
msgid "Button default settings text \"Processing\""
msgstr ""

#: ../settings/I18n.php:32
msgid "Button save text"
msgstr ""

#: ../settings/I18n.php:33
msgid "Button save text \"Saving\""
msgstr ""

#: ../settings/I18n.php:34 ../settings/I18n.php:186
msgid "Placeholder for blocked content"
msgstr ""

#: ../settings/I18n.php:35 ../settings/I18n.php:195 ../settings/I18n.php:210
msgid "Enable"
msgstr ""

#: ../settings/I18n.php:36
msgid "Hide logo"
msgstr ""

#: ../settings/I18n.php:37
msgid "Hide our plugin Cookie logo inside the replacement elemement."
msgstr ""

#: ../settings/I18n.php:39
msgid ""
"To display the <strong>Cookie accept button</strong> in your placeholder, "
"use the shortcode <code>[wpca_btn_accept]</code> with or without a "
"<strong>custom button text</strong> <code>[wpca_btn_accept txt=\"My custom "
"button text\"]</code>."
msgstr ""

#: ../settings/I18n.php:40
msgid "Shortcodes"
msgstr ""

#: ../settings/I18n.php:41
msgid ""
"A short explaination of the WeePie Cookie Allow shortcodes you can use on "
"your website."
msgstr ""

#: ../settings/I18n.php:42
msgid ""
"<table>\n"
"<tbody>\n"
"<tr>\n"
"  <th>Shortcode</th>\n"
"  <th>Description</th>\n"
"  <th>Attributes</th>\n"
"  <th>Example</th>\n"
"</tr>\n"
"<tr><td><code>[wpca_cookie_policy_link]</code></td><td>Shows a hyperlink to "
"you <strong>Cookie policy page</strong>.</td><td>txt (optional)</"
"td><td><code>[wpca_cookie_policy_link txt=\"Our cookie policy page\"]</"
"code></td></tr>\n"
"<tr><td><code>[wpca_btn_accept]</code></td><td>Shows an <strong>Accept "
"button</strong>.</td><td>txt (optional), txt_accepting (optional)</"
"td><td><code>[wpca_btn_accept txt=\"Accept cookies!\" txt_accepting=\"Woehee "
"accepting..\"]</code></td></tr>\n"
"<tr><td><code>[wpca_btn_decline]</code></td><td>Shows a <strong>Decline "
"button</strong>.</td><td>txt (optional), txt_declining (optional)</"
"td><td><code>[wpca_btn_decline txt=\"No, thanks!\" txt_declining=\"Declining "
"now..\"]</code></td></tr>\n"
"<tr><td><code>[wpca_btn_reset_consent]</code></td><td>Shows a <strong>Reset "
"consent button</strong>.</td><td>txt (optional), txt_resetting (optional)</"
"td><td><code>[wpca_btn_reset_consent txt=\"Reset your consent\" "
"txt_resetting=\"Resetting now..\"]</code></td></tr>\n"
"<tr><td><code>[wpca_cookie_allow_code]</code></td><td><strong>Block (3rd "
"party/privacy sensitive) content manually</strong> by wrapping content with "
"this shortcode. Allowed Cookie Categories: <strong>functional</strong>, "
"<strong>analytical</strong>, <strong>social-media</strong>, "
"<strong>advertising</strong>, <strong>other</strong>.</td><td>txt "
"(optional), type (optional), cc (required)</td><td><code>"
"[wpca_cookie_allow_code txt=\"My 3rd party content is blocked\" type="
"\"my-3rd-party\" cc=\"advertising\"]SCRIPT OR IFRAME[/wpca_cookie_allow_code]"
"</code></td></tr>\n"
"<tr><td><code>[wpca_cc_settings_box]</code></td><td>Shows the <strong>Cookie "
"Category settings box</strong>.</td><td>None</td><td><code>"
"[wpca_cc_settings_box]</code></td></tr>\n"
"</tbody>\n"
"<table>"
msgstr ""

#: ../settings/I18n.php:60
msgid "Plugin"
msgstr ""

#: ../settings/I18n.php:61
msgid "Status"
msgstr ""

#: ../settings/I18n.php:62
msgid "Enable / disable the Plugin."
msgstr ""

#: ../settings/I18n.php:63
msgid "Disabled"
msgstr ""

#: ../settings/I18n.php:64
msgid "Enabled"
msgstr ""

#: ../settings/I18n.php:65
msgid "Disable for logged in users"
msgstr ""

#: ../settings/I18n.php:66
msgid "Disable for non-EU visitors"
msgstr ""

#: ../settings/I18n.php:67
msgid ""
"This will disable all plugin logic (no bar/box and no content blocking) for "
"requests from IP addresses that are detected from non EU countries. The GEO "
"IP data is created by MaxMind, available from <a href=\"http://www.maxmind."
"com\" target=\"_blank\">maxmind.com</a>."
msgstr ""

#: ../settings/I18n.php:68
msgid "Cookie expiration time"
msgstr ""

#: ../settings/I18n.php:69
msgid ""
"Set the expiration time (<strong>in days</strong>) for the required cookies "
"of this plugin."
msgstr ""

#: ../settings/I18n.php:70
msgid "Disable browser caching"
msgstr ""

#: ../settings/I18n.php:71
msgid ""
"This will try to send no caching headers to avoid browser HTTP caching. This "
"will use the PHP <code>header()</code> function."
msgstr ""

#: ../settings/I18n.php:72
msgid "Consent type"
msgstr ""

#: ../settings/I18n.php:73
msgid ""
"<strong>Which cookies</strong> should be set before the website visitor "
"<strong>gives consent</strong> (by your choosen consent method)?"
msgstr ""

#: ../settings/I18n.php:74
msgid "Cookies before consent"
msgstr ""

#: ../settings/I18n.php:75
msgid "Necessary and functional"
msgstr ""

#: ../settings/I18n.php:77
msgid "Google Analytics &amp; 3rd party"
msgstr ""

#: ../settings/I18n.php:78
msgid "Consent method?"
msgstr ""

#: ../settings/I18n.php:79
msgid "Accept button"
msgstr ""

#: ../settings/I18n.php:80
msgid "Accept button / Scroll"
msgstr ""

#: ../settings/I18n.php:81
msgid "Accept button / Click"
msgstr ""

#: ../settings/I18n.php:82
msgid "Accept button / Click / Scroll"
msgstr ""

#: ../settings/I18n.php:83
msgid "Click / Scroll"
msgstr ""

#: ../settings/I18n.php:84
msgid "Only Scroll"
msgstr ""

#: ../settings/I18n.php:85
msgid "Only Click"
msgstr ""

#: ../settings/I18n.php:86
msgid "Minimal scrolling (px)"
msgstr ""

#: ../settings/I18n.php:87
msgid ""
"Set the Minimal ammount of px an user must scroll before consent is given."
msgstr ""

#: ../settings/I18n.php:88
msgid "Show a close (X)"
msgstr ""

#: ../settings/I18n.php:89
msgid ""
"Add a closing cross (X) to the (bottom/top) right corner of the bar/box. The "
"(X) will act like an 'Accept button'."
msgstr ""

#: ../settings/I18n.php:90
msgid "Use (X) as dismiss"
msgstr ""

#: ../settings/I18n.php:91
msgid ""
"Using the closing cross (X) as dismiss (decline) will only hide the bar/box, "
"but will not trigger the consent functionality."
msgstr ""

#: ../settings/I18n.php:92
msgid "Show layer"
msgstr ""

#: ../settings/I18n.php:93
msgid ""
"Add a layer on top of your website to force attention to your cookie consent "
"message and disable website usage by the visitor. <strong>Please note</"
"strong> that this will disable the click / scroll consent methods."
msgstr ""

#: ../settings/I18n.php:94
msgid "Show a decline button"
msgstr ""

#: ../settings/I18n.php:95
msgid "Show a 'Decline button' next to the 'Accept button' in the bar or box."
msgstr ""

#: ../settings/I18n.php:96
msgid "Replace Reset by Accept button before consent"
msgstr ""

#: ../settings/I18n.php:97
msgid ""
"If checked, the 'Reset button' is replaced by the 'Accept button' before "
"consent."
msgstr ""

#: ../settings/I18n.php:98
msgid "Reload page after consent"
msgstr ""

#: ../settings/I18n.php:99
msgid ""
"If checked, the website visitors page will be reloaded after consent. This "
"might be useful when blocked content needs a page reload to function "
"properly."
msgstr ""

#: ../settings/I18n.php:100
msgid "Multisite network"
msgstr ""

#: ../settings/I18n.php:101
msgid "Give global consent"
msgstr ""

#: ../settings/I18n.php:102
msgid ""
"Enabling this will set the <strong>cookie path and domain</strong> to the "
"<strong>network main</strong> website. Note that for <strong>domain mapping</"
"strong> installs, this will not work (handled automatically by the plugin)."
msgstr ""

#: ../settings/I18n.php:103
msgid "3rd party &amp; privacy sensitive cookies"
msgstr ""

#: ../settings/I18n.php:104
msgid "Automatic cookie blocking"
msgstr ""

#: ../settings/I18n.php:105
msgid ""
"Automatically block 3rd party or privacy sensitive content (i.e. iFrames, "
"JavaScripts, Tracking image pixels) in your web pages before sending it to "
"the browser of your website visitor."
msgstr ""

#: ../settings/I18n.php:106
msgid "Block all iFrames"
msgstr ""

#: ../settings/I18n.php:107
msgid "Automatically delete all iframes inside the body tag."
msgstr ""

#: ../settings/I18n.php:108
msgid "Keep anonymized Google Analytics"
msgstr ""

#: ../settings/I18n.php:109
msgid ""
"Allow the <strong>anonymized Google Analytics tracking code</strong> before "
"the client has accepted for <strong>Necessary and functional</strong>.<br/"
">Your tracking code should contain somthing like: <code>ga('set', "
"'anonymizeIp', true)</code> or <code>_gaq.push (['_gat._anonymizeIp'])</"
"code>."
msgstr ""

#: ../settings/I18n.php:110
msgid "automate/templates/wpca-general-automate.php"
msgstr ""

#: ../settings/I18n.php:113 ../settings/I18n.php:194 ../settings/I18n.php:208
msgid "General"
msgstr ""

#: ../settings/I18n.php:114
msgid "Type"
msgstr ""

#: ../settings/I18n.php:115 ../settings/I18n.php:146
msgid "Bar"
msgstr ""

#: ../settings/I18n.php:116 ../settings/I18n.php:150
msgid "Box"
msgstr ""

#: ../settings/I18n.php:117
msgid "Layer background color"
msgstr ""

#: ../settings/I18n.php:118
msgid "Layer opacity"
msgstr ""

#: ../settings/I18n.php:119
msgid "Enter a number between 0 and 1, e.g. 0.75."
msgstr ""

#: ../settings/I18n.php:120
msgid "Box Shadow"
msgstr ""

#: ../settings/I18n.php:121
msgid "Add a CSS box shadow to the bar/box."
msgstr ""

#: ../settings/I18n.php:122
msgid "Text align"
msgstr ""

#: ../settings/I18n.php:123 ../settings/I18n.php:166
msgid "Left"
msgstr ""

#: ../settings/I18n.php:124 ../settings/I18n.php:167
msgid "Center"
msgstr ""

#: ../settings/I18n.php:125 ../settings/I18n.php:168
msgid "Right"
msgstr ""

#: ../settings/I18n.php:126 ../settings/I18n.php:174 ../settings/I18n.php:187
msgid "Background color"
msgstr ""

#: ../settings/I18n.php:127
msgid "Close X color"
msgstr ""

#: ../settings/I18n.php:128
msgid "Opacity"
msgstr ""

#: ../settings/I18n.php:129
msgid "Enter a number between 0 and 1, e.g. 0.95."
msgstr ""

#: ../settings/I18n.php:130 ../settings/I18n.php:175
msgid "Text color"
msgstr ""

#: ../settings/I18n.php:131 ../settings/I18n.php:176
msgid "Link color"
msgstr ""

#: ../settings/I18n.php:132 ../settings/I18n.php:177
msgid "Link color mouseover"
msgstr ""

#: ../settings/I18n.php:133
msgid "Button Accept color"
msgstr ""

#: ../settings/I18n.php:134
msgid "Button Accept color mouseover"
msgstr ""

#: ../settings/I18n.php:135
msgid "Button Accept text color"
msgstr ""

#: ../settings/I18n.php:136
msgid "Button Accept text color mouseover"
msgstr ""

#: ../settings/I18n.php:137
msgid "Button Decline color"
msgstr ""

#: ../settings/I18n.php:138
msgid "Button Decline color mouseover"
msgstr ""

#: ../settings/I18n.php:139
msgid "Button Decline text color"
msgstr ""

#: ../settings/I18n.php:140
msgid "Button Decline text color mouseover"
msgstr ""

#: ../settings/I18n.php:141
msgid "Button Reset consent color"
msgstr ""

#: ../settings/I18n.php:142
msgid "Button Reset consent color mouseover"
msgstr ""

#: ../settings/I18n.php:143
msgid "Button Reset consent text color"
msgstr ""

#: ../settings/I18n.php:144
msgid "Button Reset consent text color mouseover"
msgstr ""

#: ../settings/I18n.php:145
msgid "Custom CSS"
msgstr ""

#: ../settings/I18n.php:147 ../settings/I18n.php:151
msgid "Position"
msgstr ""

#: ../settings/I18n.php:148
msgid "Top"
msgstr ""

#: ../settings/I18n.php:149
msgid "Bottom"
msgstr ""

#: ../settings/I18n.php:152
msgid "Top left"
msgstr ""

#: ../settings/I18n.php:153
msgid "Top right"
msgstr ""

#: ../settings/I18n.php:154
msgid "Bottom left"
msgstr ""

#: ../settings/I18n.php:155
msgid "Bottom right"
msgstr ""

#: ../settings/I18n.php:156
msgid "Centered"
msgstr ""

#: ../settings/I18n.php:157
msgid "Width (%)"
msgstr ""

#: ../settings/I18n.php:158
msgid ""
"The width of the box relative to your screen width. Enter a number between 1 "
"and 100%. Please note that using a low width (< 30%) may have impact on the "
"layout."
msgstr ""

#: ../settings/I18n.php:159
msgid "Minimum height (%)"
msgstr ""

#: ../settings/I18n.php:160
msgid ""
"The minimum height of the box relative to your screen height. Enter a number "
"between 1 and 100%."
msgstr ""

#: ../settings/I18n.php:161 ../settings/I18n.php:172
msgid "Border radius (px)"
msgstr ""

#: ../settings/I18n.php:162 ../settings/I18n.php:173
msgid "Enter a number between 0 and 50 pixels."
msgstr ""

#: ../settings/I18n.php:163
msgid "Space (px)"
msgstr ""

#: ../settings/I18n.php:164
msgid ""
"Posistion the box with a distance from your browser window. Enter a number "
"between 0 and 50 pixels."
msgstr ""

#: ../settings/I18n.php:165
msgid "Button Accept/Decline align"
msgstr ""

#: ../settings/I18n.php:169
msgid "Cookie Category settings"
msgstr ""

#: ../settings/I18n.php:170
msgid "Shadow"
msgstr ""

#: ../settings/I18n.php:171
msgid "Add a CSS box shadow to the Cookie Category settings."
msgstr ""

#: ../settings/I18n.php:178
msgid "Button default settings color"
msgstr ""

#: ../settings/I18n.php:179
msgid "Button default settings color mouseover"
msgstr ""

#: ../settings/I18n.php:180
msgid "Button default settings text color"
msgstr ""

#: ../settings/I18n.php:181
msgid "Button default settings text color mouseover"
msgstr ""

#: ../settings/I18n.php:182
msgid "Button save"
msgstr ""

#: ../settings/I18n.php:183
msgid "Button save mouseover"
msgstr ""

#: ../settings/I18n.php:184
msgid "Button save text color"
msgstr ""

#: ../settings/I18n.php:185
msgid "Button save text color mouseover"
msgstr ""

#: ../settings/I18n.php:188
msgid "Minimal placeholder width (px)"
msgstr ""

#: ../settings/I18n.php:189
msgid ""
"When our plugin script tries to inherit the dimensions (WxH) of an element, "
"sometimes it can't find the correct dimensions. You can set the "
"<strong>minimal 'width' in pixels</strong> for the placholder here."
msgstr ""

#: ../settings/I18n.php:190
msgid "Minimal placeholder height (px)"
msgstr ""

#: ../settings/I18n.php:191
msgid ""
"When our plugin script tries to inherit the dimensions (WxH) of an element, "
"sometimes it can't find the correct dimensions. You can set the "
"<strong>minimal 'heigth' in pixels</strong> for the placholder here."
msgstr ""

#: ../settings/I18n.php:196
msgid "Enable / disable consent logging"
msgstr ""

#: ../settings/I18n.php:197
msgid "Consent logs"
msgstr ""

#: ../settings/I18n.php:198
msgid "Find logged consent here."
msgstr ""

#: ../settings/I18n.php:199 ../settings/I18n.php:216
msgid "Limit"
msgstr ""

#: ../settings/I18n.php:200
msgid "The total number of items to fetch from the database"
msgstr ""

#: ../settings/I18n.php:201 ../settings/I18n.php:218
msgid "Order by"
msgstr ""

#: ../settings/I18n.php:202
msgid "How to order the list (\"Newest\" on top or the \"Oldest\" on top)"
msgstr ""

#: ../settings/I18n.php:203 ../settings/I18n.php:220
msgid "Newest"
msgstr ""

#: ../settings/I18n.php:204 ../settings/I18n.php:221
msgid "Oldest"
msgstr ""

#: ../settings/I18n.php:205
msgid "log/templates/wpca-log-list.php"
msgstr ""

#: ../settings/I18n.php:211
msgid "Enable / disable consent logging."
msgstr ""

#: ../settings/I18n.php:212
msgid "Anonymize IP address"
msgstr ""

#: ../settings/I18n.php:213
msgid "Enable / disable anonymizing IP addresses."
msgstr ""

#: ../settings/I18n.php:214
msgid "Overview of logged consents"
msgstr ""

#: ../settings/I18n.php:215
msgid ""
"Find your logged client consents here. The <span class=\"wpie-red-light-bgd"
"\">declined</span> consents are colored red."
msgstr ""

#: ../settings/I18n.php:217
msgid "The total number of items to fetch from the database."
msgstr ""

#: ../settings/I18n.php:219
msgid "How to order the list (\"Newest\" on top or the \"Oldest\" on top)."
msgstr ""

#: ../settings/I18n.php:222
msgid "consent-log/templates/wpca-log-list.php"
msgstr ""
