<?php
/**
 * Please see wp-cookie-allow.php for more details.
 *
 * @author $Vincent Weber <vincent@webrtistik.nl>$
 * 
 * This file contains the upgrade logic
 */

add_action( 'wpca_wpiefw_upgrade_logic', 'wpca_wpiefw_upgrade_logic', 10, 5 );

/**
 * Callback for wpca_wpiefw_upgrade_logic hook
 * 
 * Handles upgrade logic
 * 
 * @param		string $currVersionPlugin
 * @param		string $newVersionPlugin
 * @param		string $wpiefwVersionOld
 * @param		string $versionWpieFw
 * @param		bool $network_wide
 * 
 * @uses		wpca_wpiefw_upgrade_logic_30()
 * 
 * @since 	2.3.3
 */
function wpca_wpiefw_upgrade_logic( $currVersionPlugin, $newVersionPlugin, $wpiefwVersionOld, $versionWpieFw, $networkWide ) 
{
	if( version_compare( $wpiefwVersionOld, '1.4.13', '<' ) ) {
		delete_option( 'wpca_globals' );
	}
	if( version_compare( $newVersionPlugin, '3.2.6', '>=' ) ) {
		$msg = sprintf( __( 'Hi! You have update to version %s. Please <strong>check your 3rd party list items</strong> as the could have been \'re-checked\' during the update process. Our apologies for the invonvenience, we\'re working on a solution!', 'wpca' ),	$newVersionPlugin );
		set_transient( 'wpca_upgrade_msg', $msg, 86400 );
	}	
}

/**
 * Replace values in new settings based on old settings
 * 
 * @param array $old_option
 * @param array $option
 * @param array $replace
 * 
 * @return bool true if array is updated else false
 */
function _wpca_upgrade_sett( $old_option, &$option, $replace = array() ) 
{	
	$updated = false;

	if( false !== $option && is_array( $option ) && false !== $old_option && is_array( $old_option ) )
	{
		foreach ( $replace as $old => $new )
		{
			if( isset( $option[$new] ) && isset( $old_option[$old] ) ) {
				$v = $old_option[$old];
				$option[$new] = $v;
				$updated = true;
			}
		}
	}	
	return $updated;
}