<?php
/*
Main plugin file for the WeePie Cookie Allow Plugin
 
@author $Author: Vincent Weber <vincent@webrtistik.nl> $
@since 1.0
 
Plugin Name: WeePie Cookie Allow
Plugin URI: http://www.weepie-plugins.com/cookie-allow/ 
Description: Easy & Complete Cookie Consent Plugin. Comply to different Cookie Law implementations, e.g. the EU, UK and Dutch cookie law.
Author: WeePie Plugins
Version: 3.2.6
Author URI: http://www.weepie-plugins.com/about-weepie-plugins/
License: GPL v3

WeePie Cookie Allow - A premium WordPress Plugin that makes it possible to 
comply to different Cookie Law implementations, e.g. the EU, UK and Dutch cookie law.
Copyright (C) 2012 - 2018, Vincent Weber webRtistik

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if( !class_exists( 'WeePieFramework' ) ) 
{
	if( file_exists( $file = dirname( __FILE__ ) . '/../weepie-framework/weepie-framework.php' ) ) 
	{		
		require_once $file;
			
	} else {
		
		wp_die( __( '<strong>WeePie Framework Plugin could not be located.</strong><br/><br/>Please upload the <strong>"weepie-framework"</strong> folder to the <strong>WordPress Plugins folder</strong>.', 'weepie' ), 'WeePie Framework error'  );
	}
}

if( !class_exists( 'WpieCookieAllow' ) ) {

	/**
	 * WpieCookieAllow Class
	 * 
	 * This is the main Plugin class for the WeePie Cookie Allow Plugin
	 * 
	 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
	 * @since 1.0
	 */
	class WpieCookieAllow extends WeePieFramework {
		
		
		/**
		 * Flag if WeePie Framework needs to init settings
		 * 
		 * @since 2.3
		 * 
		 * @var bool
		 */
		private $_doSettings = true;

		
		/**
		 * Settings page title
		 * 
		 * @since 2.3
		 * 
		 * @var string
		 */
		const PAGE_TITLE = 'WeePie Cookie Allow';

		
		/**
		 * Settings page menu title
		 * 
		 * @since 2.3
		 * 
		 * @var string
		 */
		const MENU_TITLE = 'Cookie Allow';		
		
		
		/**
		 * Plugin namespace
		 * 
		 * @var string
		 * 
		 * @since 2.0.3
		 */
		const WPIE_NS = 'wpca';				
		
		
		/**
		 * Current version of the WeePie Cookie Allow Plugin
		 *
		 * @var string
		 * 
		 * @since 2.0.4
		 */
		const VERSION = '3.2.6';
		
		
		/**
		 * Constructor
		 * 
		 * @since 1.0
		 */
		public function __construct()	{
			// all code have been moved to self:run()
		}
		
		
		/* 
		 * Start the plugin
		 * 
		 * @uses $did to allow only one call
		 * @see WeePieFramework::start()
		 * 
		 * @since 2.2.7
		 */
		public function run() 
		{
			static $did = false;

			if( $did )
				return;
			
			if( $this->_doSettings ) {
				$this->pageTitle = __( self::PAGE_TITLE, self::WPIE_NS );
				$this->menuTitle = __( self::MENU_TITLE, self::WPIE_NS );
			}
			
			// Start the Plugin
			parent::start( self::WPIE_NS, __FILE__, self::VERSION, $this->_doSettings );
			
			$did = true;
		}
		
		
		/**
		 * Set a WordPress notice
		 *
		 * @access	public
		 *
		 * @param 	string $message
		 * @param 	string $type
		 * @param 	string $setting
		 *
		 * @uses	WpieNotices::add()
		 *
		 * @since
		 */		
		public static function setNotice( $message, $type = 'error', $setting = false )
		{
		 	WpieNotices::add( self::WPIE_NS, $message, $type, $setting );		
		}
		
		
		/**
		 * Get a Plugin Module by its identifier
		 * 
		 * @param string $key the modules unique identifier
		 * 
		 * @uses WpiePluginModuleProcessor::$modules
		 * 
		 * @since 2.0.3
		 * 
		 * @return WpPluginModule Module
		 */
		public static function getModule( $key )
		{
			if( isset( WpiePluginModuleProcessor::$modules[self::WPIE_NS][$key] ) )
				return WpiePluginModuleProcessor::$modules[self::WPIE_NS][$key];
			else
				return false;
		}
	} // end WpieCookieAllow class


	/**
	 * Create WpieCookieAllow instance and start the plugin
	 */
	$wpca = new WpieCookieAllow();
	$wpca->run();
}