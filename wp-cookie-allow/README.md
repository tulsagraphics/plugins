# WeePie Cookie Allow Plugin

The WeePie Cookie Allow Plugin is a simple cookie compliance WordPress Plugin which makes it possible to fully comply with any cookie law with lots of styling options.

== Features ==

*   Automatic blocking of 3rd party cookie scripts, iFrames and other privacy sensitive content (before consent)
*   After consent all cookies will be placed
*   Cookie Category settings for website visitors 
*   Consent logging
*   Responsive design
*   Easy to customize, e.g.:
    - bar or box lay-out
    - colors (background, buttons, text, hovers)
    -   sizes
    - texts 
*   Possibility to show a replacement block for blocked cookie content
*   Set implicit or explicit (cookie wall) permission by adding a layer on top of your website
*   Decline Cookies
*   Reset Cookie consent (recall the bar/box)
*   GDPR cookie proof / Comply with cookie law requirements
*   Fully translatable (pot-file included)
*   WPML compatible
*   WordPress Multisite ready
*   Keep anonymized Google Analytics cookies

== Installation / Configuration ==
https://www.weepie-plugins.com/instruction-guide-weepie-cookie-allow-plugin/#how-to-install-the-plugin

== Update ==
https://www.weepie-plugins.com/instruction-guide-weepie-cookie-allow-plugin/#how-to-update-the-plugin

== Change domain ==
https://www.weepie-plugins.com/instruction-guide-weepie-cookie-allow-plugin/#how-to-use-the-plugin-on-another-website

== Intruction Guide ==
For more details and intructions, see: http://www.weepie-plugins.com/instruction-guide-weepie-cookie-allow-plugin/ 