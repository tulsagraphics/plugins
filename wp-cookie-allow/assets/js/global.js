/*
 * Please see wp-cookie-allow.php for more details.
 * 
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 2.0
 */

var WPCAGLOBAL = (function(c) {
	""; // fix for yuicompressor
	"use strict";
	c.setKookie = function(name, value, days, domain, path, secure) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			expires = "; expires="+date.toGMTString();
		}
		
		var path = ('undefined' !== typeof path) ? "; path="+path : "; path=/";
		var domain = ('undefined' !== typeof domain && '' !== domain) ? "; domain="+domain : "";
		var secure = ('boolean' === typeof secure && secure) ? "; secure" : "";
				
		var cookieStr = name+"="+value+expires+path+domain+secure;
		document.cookie = cookieStr;
	};

	c.readKookie  = function(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	};
	
	c.eraseKookie  = function(name, domain, path) {		
		c.setKookie (name, "", -1, domain, path);
	};
	/* END COOKIE FUNCTIONS */
		
	c.arrDiff = function(a1, a2)
	{
	  var a=[], diff=[];
	  for(var i=0;i<a1.length;i++)
	    a[a1[i]]=true;
	  for(var i=0;i<a2.length;i++)
	    if(a[a2[i]]) delete a[a2[i]];
	    else a[a2[i]]=true;
	  for(var k in a)
	    diff.push(k);
	  return diff;
	};
	
	c.removeQueryVar = function(qvar, qvarVal, uri) 
	{
		var pat = new RegExp(qvar, 'i');	  			
		if( pat.test(uri) ) {	  				
			uri = uri.replace('?'+qvar+'='+qvarVal+'&', '?');
			uri = uri.replace('?'+qvar+'='+qvarVal, '');
			uri = uri.replace('&'+qvar+'='+qvarVal, '');
		}		
		return uri;
	};
	
	
	/**
	 * Log wrapper function
	 * 
	 * @returns void
	 */
	c.log = function() {
		if(null != console && null != console.log) {
			if(0 < arguments.length) {
				[].unshift.call(arguments, 'WeePie log: ');
				console.log.apply(this, arguments);
			}
		}
	};
	
	/**
	 * Alert wrapper function
	 * 
	 * @returns void
	 */
	c.alert = function(msg) {
		alert('WeePie says: '+msg)
	};
	
	/**
	 * Get the width and height of an jQuery DOM object
	 * 
	 * The obj Object will be filled with the width and height values
	 * 
	 * @param {Object.<jQuery>} $el
	 * @param {Object} obj
	 * @param {Boolean} outer flag if outerWidth and outerHeight should be calculated
	 * 
	 * @returns {Boolean} true if valid width and height found
	 */
	c.getWidthHeight = function ($el, obj, outer) {
		var w=0, 
		    h=0;
		try {
			if(!($el instanceof jQuery)) {
				throw new Error('param "$el" needs to be a jQuery Object.');
			}			
			if('object' !== typeof obj) {
				throw new Error('param "obj" needs to be an Object to return width and height.');
			}
			if('boolean' !== typeof outer) {
				outer = false;
			}			
			// try to get the w and h from the attributes 								
			w = $el.attr('width');
			h = $el.attr('height');	
			// if failed, try to get the w h from the css
			if(undefined === w || undefined === h || '' === w || '' === h) {
				w = $el[0].clientWidth;
				h = $el[0].clientHeight;
				if(outer && 0 < w && 0 < h) {
					w = $el.outerWidth();
					h = $el.outerHeight();					
				}										
			}		
		} catch(exc) {
			obj = {};
			this.log(exc.message);
		} finally {
			obj.w = parseInt(w);
			obj.h = parseInt(h);
			return (0 < obj.w && 0 < obj.h);			
		}
	};
	
	return c;
	
})(WPCAGLOBAL || {});  