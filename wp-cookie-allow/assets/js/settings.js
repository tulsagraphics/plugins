/*
 * Please see wp-cookie-allow.php for more details.
 * 
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 3.0.2
 */

var WPCA_SETT = (function($) {
	""; // fix for yuicompressor
	"use strict";
	var settings = {
			
		// tab general
		elCookiesBeforeConsent: {},
		elShowDeclineBtn: {},
		elConsentMethod: {},
		elMinScrollTop: {},
		elShowLayer: {},
		elReplaceResetByAccept: {},
		elShowX: {},
		elUseXAsDismiss: {},
		elGaAnonymized: {},
		elGroupAutomate: {},
		elAutoErase: {},
		elAllIframes: {},
		cookiesBeforeConsent: 0,
		consentMethod: 0,
		showX: false,
		
		// tab style
		elStyleType: {},
		elGroupBar: {},
		elGroupBox: {},
		styleType: '',
		
		// tab content
		elCookiePolicyPostType: {},
		elCookiePolicyPage: {},
		elEnableReplace: {},
		elReplaceHideLogo: {},
		elReplaceTxt: {},
		
		done: false,	

	};
		
	var showHideGroupAutomate = function(cookiesBeforeConsent) {
		if(3 === cookiesBeforeConsent) {
			WPIESETT.hideGroup(settings.elGroupAutomate);
		} else {
			WPIESETT.showGroup(settings.elGroupAutomate);
		}		
	};
	
	var showHideMinScrollTop = function(consentMethod) {
		if(-1 !== [1,3,7].indexOf(consentMethod)) {
			WPIESETT.hideRow(settings.elMinScrollTop);
		} else {
			WPIESETT.showRow(settings.elMinScrollTop);
		}	
	};
	
	var showHideUseXasDismiss = function(cookiesBeforeConsent, showX) {
		if(3 === cookiesBeforeConsent || !showX) {
			WPIESETT.hideRow(settings.elUseXAsDismiss);
		} else {
			WPIESETT.showRow(settings.elUseXAsDismiss);
		}	
	};
	
	var showHideGaAnonymized = function(cookiesBeforeConsent) {
		if(1 !== cookiesBeforeConsent) {
			WPIESETT.hideRow(settings.elGaAnonymized);
		} else {
			WPIESETT.showRow(settings.elGaAnonymized);
		}		
	};

	var showHideShowLayer = function(consentMethod) {
		if(-1 !== [2,3,4,5,6,7].indexOf(consentMethod)) {
			WPIESETT.hideRow(settings.elShowLayer);
		} else {
			WPIESETT.showRow(settings.elShowLayer);
		}	
	};
	
	var showHideReplaceResetByAccept= function(consentMethod) {
		if(-1 !== [5,6,7].indexOf(consentMethod)) {
			WPIESETT.hideRow(settings.elReplaceResetByAccept);
		} else {
			WPIESETT.showRow(settings.elReplaceResetByAccept);
		}	
	};	
	
	var showHideShowDeclineBtn = function(cookiesBeforeConsent) {
		if(3 === cookiesBeforeConsent) {
			WPIESETT.hideRow(settings.elShowDeclineBtn);
		} else {
			WPIESETT.showRow(settings.elShowDeclineBtn);
		}	
	};	
	
	var showHideBlockAllIframes = function(elAutoErase) {
		WPIESETT.showHideRowForCheckbox(elAutoErase, settings.elAllIframes);
	}
	
	var showHideGroupBarBox = function(layout) {
		if('bar' === layout) {
			WPIESETT.showHideGroup(settings.elGroupBar, settings.elGroupBox);
		} else if('box' === layout) {
			WPIESETT.showHideGroup(settings.elGroupBox, settings.elGroupBar);
		}		
	};
	
	var showHideReplace = function(elEnableReplace) {		
		WPIESETT.showHideRowForCheckbox(elEnableReplace, settings.elReplaceHideLogo);
		WPIESETT.showHideRowForCheckbox(elEnableReplace, settings.elReplaceTxt);
		
		settings.elReplaceTxt.css( 'max-width', settings.elReplaceTxt.parent('td').innerWidth()+'px' );	
	};
	
	/**
	 * Init all events
	 * 
	 * @returns void
	 */
	settings.events = function() {		
		var thiz = this,
			cookiesBeforeConsent = this.cookiesBeforeConsent,
			showX = this.showX;
		
		// tab general
		this.elCookiesBeforeConsent.on('change', function(e) {			
			var $this = $(this);
			cookiesBeforeConsent = parseInt($this.val());
			
			showHideGroupAutomate(cookiesBeforeConsent);
			showHideShowDeclineBtn(cookiesBeforeConsent);
			showHideUseXasDismiss(cookiesBeforeConsent, showX);
			showHideGaAnonymized(cookiesBeforeConsent);
		});
		this.elConsentMethod.on('change', function(e) {			
			var $this = $(this),
				consentMethod = parseInt($this.val());
			
			showHideMinScrollTop(consentMethod);
			showHideShowLayer(consentMethod);
			showHideReplaceResetByAccept(consentMethod);
		});
		this.elShowX.on('change', function(e) {
			showX = $(this).isChecked();
			if(3 !== cookiesBeforeConsent) {
				WPIESETT.showHideRowForCheckbox($(this), thiz.elUseXAsDismiss);
			}
		});			
		this.elAutoErase.on('click', function(e) {			
			WPIESETT.showHideRowForCheckbox($(this), thiz.elAllIframes);
		});		
		
		// tab style
		this.elStyleType.on('change', function(e) {			
			var $this = $(this),
				val = $this.val();
			
			showHideGroupBarBox(val);
		});	
		
		// tab content
		this.elCookiePolicyPostType.on('change', function(e) {			
			var postType = $(this).val(),
			    args;
			
			args = WPIESETT.getAjaxArgs( 'wpca-update-list-cookie-policy-items', {
				post_type:postType
				});
			
			$.post(ajaxurl, args, function(r) {				
				try {
					if(null == r) {
						alert(WPIESETT.currNsDatal10n.unknown_error);						 
						return false;
					}					
					
				  	switch(r.state) {
				  		case '-1':
				  			alert(r.out);
				  			break;
				  		case '1':	
				  			thiz.elCookiePolicyPage.html(r.out);	
					    break;		    		
				  	}
				} catch(exc) {
					if(null != console && null != console.log) {
						console.log('WeePie Exception: ', exc.message, exc);
					}					
				}
			}, 'json');	
		});
		
		this.elEnableReplace.on('click', function(e) {	
			showHideReplace($(this));
		});
	};

	settings.init = function() {		
		try {		
			if(this.done) {
				return;
			}			
			if( null == WPIESETT || false === WPIESETT.done ) {
				throw new Error('Could not continue. WPIESETT object is not ready.');
			}			
			
			// tab general
			this.elCookiesBeforeConsent = $('#general-cookies-before-consent');
			this.elShowDeclineBtn = $('#general-show-btn-decline'); 
			this.elConsentMethod = $('#general-consent-method');
			this.elMinScrollTop = $('#general-min-scroll-top');
			this.elShowLayer = $('#general-show-layer');
			this.elReplaceResetByAccept = $('#general-replace-reset-by-accept-button');
			this.elShowX = $('#general-show-x'); 
			this.elUseXAsDismiss = $('#general-use-x-as-dismiss');
			this.elGaAnonymized = $('#general-automate-enable-ga-anonymized');
			this.elGroupAutomate = $('#form-group-automate');
			this.elAutoErase = $('#general-automate-enable-auto-erase');
			this.elAllIframes = $('#general-automate-erase-all-iframes');
			
			// consent settings values
			this.cookiesBeforeConsent = parseInt(this.elCookiesBeforeConsent.val());
			this.consentMethod = parseInt(this.elConsentMethod.val());
			this.showX = this.elShowX.isChecked();
			
			// tab content
			this.elCookiePolicyPostType = $('#content-cookie-policy-post-type');
			this.elCookiePolicyPage = $('#content-cookie-policy-page');
			this.elEnableReplace = $('#content-automate-replace-enable');
			this.elReplaceHideLogo = $('#content-automate-replace-hide-logo');
			this.elReplaceTxt = $('#content-automate-replace-txt');
			
			// tab style
			this.elStyleType = $('#style-type');			
			this.elGroupBar = $('#form-group-bar');
			this.elGroupBox = $('#form-group-box');			
			this.styleType = this.elStyleType.val();
			
			// init all events
			this.events();

			// tab general			
			showHideGroupAutomate(this.cookiesBeforeConsent);
			showHideMinScrollTop(this.consentMethod);
			showHideUseXasDismiss(this.cookiesBeforeConsent, this.showX);
			showHideShowLayer(this.consentMethod);
			showHideReplaceResetByAccept(this.consentMethod);
			showHideShowDeclineBtn(this.cookiesBeforeConsent);
			showHideBlockAllIframes(this.elAutoErase);
			showHideGaAnonymized(this.cookiesBeforeConsent);
			
			// tab style						
			showHideGroupBarBox(this.styleType);

			// tab content
			showHideReplace(this.elEnableReplace);
			
			this.done = true;
		
		} catch (exc) {
			if(null != console && null != console.log) {
				console.log('WeePie Exception: ', exc.message, exc);
			}
		}
	};	
	
	return settings;	
	
})(jQuery || {}, WPCA_SETT || {});

jQuery(function($) {
	// call init method when DOM is ready	
	WPCA_SETT.init();	
});