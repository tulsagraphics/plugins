<?php
/**
 * Please see wp-cookie-allow.php for more details.
 *
 * @author $Vincent Weber <vincent@webrtistik.nl>$
 */
if( !class_exists( 'WpcaCore' ) ) {
	
	/**
	 * WpcaCore Class
	 *
	 * Module to handle the core functionality of the WeePie Cookie Allow Plugin
	 *
	 * @author $Vincent Weber <vincent@webrtistik.nl>$
	 * 
	 * @since 2.0
	 */	
	class WpcaCore extends WpiePluginModule {

		/**
		 * The loading priority for the Module
		 *
		 * @since 2.0
		 *
		 * @var int
		 */
		protected $priority = 5;
		
		/**
		 * Flag if the Plugin is active
		 * 
		 * @since 1.0
		 * 
		 * @var bool
		 */
		public $pluginsStatus = false;

		
		/**
		 * The choosen cookies to allow on 1st visit
		 * 
		 * @since 3.0
		 * 
		 * @var string
		 */
		public $cookiesBeforeConsent = 1;		
		
		
		/**
		 * The choosen accept method
		 *
		 * @since 2.2.6
		 *
		 * @var string
		 */
		public $consentMethod = 1;		
		
		
		/**
		 * Flag if the plugin is disbaled for logged-in clients
		 *
		 * @since 1.0
		 *
		 * @var bool
		 */		
		public $disableLoggedIn = false;
		
	
		/**
		 * Value of the Cookie wpca_consent
		 * 
		 * @since 2.0
		 * 
		 * @var string default '1'
		 */
		public $cookieValueConsent = false;
		
		/**
		 * Current tab index
		 * 
		 * @since 2.3
		 * 
		 * @var string
		 */
		public $currentTabIdx = null;
		
		
		/**
		 * Cookie policy Post Type
		 *
		 * @since 3.1
		 *
		 * @var bool
		 */
		public $cookiePolicyPostType = 'page';
		

		/**
		 * The cookies before consent ID allowing Google Analytics
		 *
		 * @since 3.2
		 *
		 * @var integer
		 */
		public $cookiesBeforeConsentIdGoogleAnalytics = 2;		
		
		
		/**
		 * The cookies before consent ID allowing 3rd parties 
		 * 
		 * @since 3.1.2
		 * 
		 * @var integer
		 */
		public $cookiesBeforeConsentId3rdParties = 3;
		

		/**
		 * The consent method ID's without Accept button 
		 * 
		 * @since 3.1.2
		 * 
		 * @var array
		 */
		public $consentMethodsIdsNoBtn = array( 5, 6, 7 );

		
		/**
		 * The consent method ID's without layer 
		 * 
		 * @since 3.1.2
		 * 
		 * @var array
		 */
		public $consentMethodsIdsNoLayer= array( 2, 3, 4, 5, 6, 7 );
		

		/**
		 * Cookie name for Cookie that will be set if client confirmed Cookie Settings at front-end
		 * 
		 * @since 2.0
		 * 
		 * @since 2.3.3 renamed to wpca_consent
		 * 
		 * @var string
		 */
		const COOKIE_NAME_CONSENT = 'wpca_consent';
		
		
		/**
		 * Cookie name for Cookie that will hold the Cookie Category Settings at front-end
		 *
		 * @since 3.2
		 *
		 * @var string
		 */
		const COOKIE_NAME_CC = 'wpca_cc';		

		
		/**
		 * CSS file name
		 * 
		 * @since 3.1
		 * 
		 * @var string
		 */
		const FILE_NAME_CSS = 'wpca-core';
		
		
		/**
		 * Constructor
		 * 
		 * @since 2.0
		 * 
		 * @acces public
		 */
		public function __construct() {}
		
		
		/* (non-PHPdoc)
		 * @see WpiePluginModule::start()
		 */
		
		public function start() 
		{				
			parent::setPriority( $this->priority );
		}		
		
		
	
		/**
		 * Helper to get one of the Cookie named that are defined as const in this class
		 * 
		 * @acces public
		 * 
		 * @param string $cookie, shortname for the Cookie
		 * 
		 * @since 2.0
		 * 
		 * @return string empty on failure or the name of the wanted Cookie
		 */
		public function getCookieName( $cookie = '' )
		{
			if ( '' === $cookie || !is_string( $cookie ) ) {
				return '';
			}
			
			$name = '';			
			switch( $cookie ) {				
				case 'consent': $name = self::COOKIE_NAME_CONSENT; break;
				case 'cc': $name = self::COOKIE_NAME_CC; break;
			}
			
			return $name;
		}
		
		
		/**
		 * Callback for the wpca_styles_frontend hook
		 *
		 * Enqueue core styles - styles that are needed for both bar and popup
		 *
		 * @access public
		 *
		 * @param WP_Styles $wp_styles
		 * @param array $excludes items to exclude
		 * @param bool $isModeDev
		 *
		 * @uses wp_enqueue_style()
		 *
		 * @since 2.3
		 */
		public function setStyles( $wp_styles, $excludes, $isModeDev )
		{
			if( !in_array( 'wpca-core', $excludes ) ) {
				$ext = ( $isModeDev ) ? '.css' : '.min.css';
				wp_enqueue_style( 'wpca-core', $this->globals->get('moduleUri') . '/'. parent::getIndex() .'/css/' . self::FILE_NAME_CSS . $ext, array(), WpieCookieAllow::VERSION );
			}
		}
			
		
		/**
		 * Callback for the wpca_script_frontend_vars hook
		 *
		 * @access public
		 *
		 * @param array $vars
		 *
		 * @since 2.2
		 *
		 * @return array
		 */
		public function setScriptsVars( $vars ) {
		
			$vars['cookieNameConsent'] 	= $this->getCookieName( 'consent' );
			$vars['cookieNameCc'] 	= $this->getCookieName( 'cc' );
		
			return $vars;
		}
		
		
		/**
		 * Callback for the wpie_wp_query_args_post_type_dropdown hook
		 * 
		 * Set post type for the dropdown with Cookie Policy select items
		 * 
		 * @param array $args
		 * 
		 * @since 3.1
		 * 
		 * @return array
		 */
		public function setCookiePolicyQueryArgs( $args ) 
		{
			if( !$this->cookiePolicyPostType ) {
				return $args;	
			}			
			
			$args['post_type'] = $this->cookiePolicyPostType;
			
			return $args;
		}
		
		
		/**
		 * Determine if front-end logic must be executed
		 * 
		 * @acces public
		 * 
		 * @param bool $do default to true
		 * 
		 * @since 2.0
		 * 
		 * @return bool true or false
		 */
		public function doFrontendLogic( $do = true ) 
		{
			if( is_admin() ) {
				return false;
			}
			
			if( '0' === $this->pluginsStatus ) {
				$do = false;				
			}	elseif( true === $this->disableLoggedIn && is_user_logged_in() ) {
				$do = false;				
			} else {				
				$do = true;
			}
			
			/**
			 * Let others decide to or not to do the frontend logic
			 * 
			 * @param boolean $do
			 * 
			 * @since 3.2.6
			 */
			return apply_filters( 'wpca_do_frontend_logic' , $do );
		}
		
		
		/**
		 * Determine if front-end hooks must be added inside {@link WeePieFramework::init()} 
		 * 
		 * @acces public
		 * 
		 * @uses WpcaCore::doFrontendLogic()
		 * 
		 * @since 1.0
		 * 
		 * @return bool
		 */		
		public function doFrontendHooks( $do )
		{
			return $this->doFrontendLogic( $do );
		}	
		
		
		/**
		 * Determine if 3rd party cookies are allowed before consent
		 * 
		 * @since 3.1
		 * 
		 * @return boolean
		 */
		public function do3rdPartyCookiesBeforeConsent() 
		{
			return ( $this->cookiesBeforeConsentId3rdParties === $this->cookiesBeforeConsent );
		}
		

		/**
		 * Determine if Google Analytics cookies are allowed before consent
		 *
		 * @since 3.2
		 *
		 * @return boolean
		 */
		public function doGoogleAnalyticsCookiesBeforeConsent()
		{
			return ( $this->cookiesBeforeConsentIdGoogleAnalytics === $this->cookiesBeforeConsent );
		}		
		
		
		/**
		 * Determine if the accept button should be force to hide
		 *
		 * @since 3.1.2
		 *
		 * @return boolean
		 */
		public function forceHideBtnAccept()
		{
			return ( in_array( $this->consentMethod, $this->consentMethodsIdsNoBtn ) );
		}		
		
		
		/**
		 * Determine if the layer should be forced to hide
		 *
		 * @since 3.1.2
		 *
		 * @return boolean
		 */
		public function forceHideLayer()
		{
			return ( in_array( $this->consentMethod, $this->consentMethodsIdsNoLayer ) );			
		}		
				

		/**
		 * Callback for the hook wpca_settings_escape_groupfield_style
		 * 
		 * Escape fields when not needed
		 * 
		 * @param boolean $escape
		 * @param array $field
		 * @param array $vars
		 * 
		 * @since 3.0.2
		 * 
		 * @return bool
		 */
		public function escapeSettingsField( $escape, $field, $vars  )
		{
			$showLayer = $this->settingsProcessor->getSetting('general')->get( 'general_show_layer' );
			$showCloseX = $this->settingsProcessor->getSetting( 'general' )->get( 'general_show_x' );
			$showBtnDecline = $this->settingsProcessor->getSetting('general')->get( 'general_show_btn_decline' );
				
			switch( $vars['current_templ'] ) {
				case 'style':
					if( !$showLayer ) {
						$unsets = array( 'style_overlay_color', 'style_overlay_opacity' );
						if( in_array( WpieXmlSettingsHelper::getFieldAttributeName( $field ), $unsets ) ) {
							return true;
						}
					}
					if( !$showCloseX ) {
						$unsets = array( 'style_close_color' );
						if( in_array( WpieXmlSettingsHelper::getFieldAttributeName( $field ), $unsets ) ) {
							return true;
						}
					}
					if( !$showBtnDecline || $this->do3rdPartyCookiesBeforeConsent() ) {
						$unsets = array( 'style_button_decline_color',
								             'style_button_decline_color_hover',
								             'style_button_decline_text_color',
								             'style_button_decline_text_color_hover' );
						if( in_array( WpieXmlSettingsHelper::getFieldAttributeName( $field ), $unsets ) ) {
							return true;
						}
					}
					if( $this->do3rdPartyCookiesBeforeConsent() ) {
						$unsets = array( 'style_button_reset_consent_color',
								             'style_button_reset_consent_color_hover',
								             'style_button_reset_consent_text_color',
								             'style_button_reset_consent_text_color_hover' );
						if( in_array( WpieXmlSettingsHelper::getFieldAttributeName( $field ), $unsets ) ) {
							return true;
						}
					}
					break;
				case 'content':
					if( !$showBtnDecline || $this->do3rdPartyCookiesBeforeConsent() ) {
						$unsets = array( 'content_button_decline_txt',
								             'content_button_decline_txt_declining' );
						if( in_array( WpieXmlSettingsHelper::getFieldAttributeName( $field ), $unsets ) ) {
							return true;
						}
					}
					break;
			}	
			
			return $escape;
		}
		
		
		/**
		 * Callback for the hook wpca_settings_escape_group_style
		 * 
		 * Escape groups when not needed
		 * 
		 * @param boolean $escape
		 * @param array $groupName the name of the group
		 * @param array $vars
		 * 
		 * @since 3.1
		 * 
		 * @return boolean
		 */
		public function escapeSettingsGroup( $escape, $groupName, $vars )
		{					
			switch( $vars['current_templ'] ) {
				case 'style':
					if( 'automate_replace' === $groupName && $this->do3rdPartyCookiesBeforeConsent() ) {
						return true;
					}					
					break;
				case 'content':
					if( 'automate_replace' === $groupName && $this->do3rdPartyCookiesBeforeConsent() ) {
						return true;
					}
					if( 'reset_consent' === $groupName && $this->do3rdPartyCookiesBeforeConsent() ) {
						return true;
					}					
					break;					
			}
		}
		
		
		/**
		 * Callback for the wpca_ajax_json_return hook
		 * 
		 * @param mixed  $return
		 * @param string $action
		 * @param array  $data
		 * 
		 * @since 3.1
		 * 
		 * @return string|mixed $return
		 */
		public function processAjaxRequest( $return, $action = '', $data = array() ) 
		{
			switch ($action) {
				case 'wpca-update-list-cookie-policy-items':					
					$postType = ( isset( $data['post_type'] ) ) ? $data['post_type'] : $this->cookiePolicyPostType;					
					$postTypeObj = new WpiePostType( $postType );					
					
					$args = array(
							'orderby' => 'ID', 
							'order' => 'DESC',
							'post_type' => $postType,
							'post_status' => 'publish'
							);
					
					// query for the posts
					$posts = $postTypeObj->retrieveCustom( $args, false, false );
				
					// flag if posts are available
					if( ! empty( $posts ) ) {
						$return = sprintf( '<option value="-1">%s</option>', __( '-- Select --', 'wpca' ) );
						foreach( $posts as $postId => $post ) {
							$isPdf = ( wp_attachment_is( 'pdf', $postId ) );
							if( 'attachment' === $postType && !$isPdf ) {
								continue;
							}							
							if( 'attachment' === $postType && $isPdf ) {
								$title = basename( get_post_meta( $postId, '_wp_attached_file', true ) );
							} else {
								$title = $post->post_title;
							}			
							$return .= sprintf( '<option value="%d">%s</option>', $postId, $title );
						}						
												
						return $return;
					}	else {
						return sprintf( '<option value="-1">%s</option>', __( 'No items found for post type "'.$postType.'"', 'wpca' ) );
					}					
					break;
			}	
		}
		
		
		/* (non-PHPdoc)
		 * @see iWpiePluginModule::init()
		 * 
		 * @since 2.0
		 */
		public function init() 
		{	
			// set plugin status (enabled/disabled)
			$this->pluginsStatus = $this->settingsProcessor->getSetting('general')->get( 'general_plugin_status' );
			$onSettingsPage 	 = $this->settingsProcessor->settingsPage->onSettingsPage;
			
			// show a upgrade message
			if( !$this->activating && $msg = get_transient( 'wpca_upgrade_msg' ) ) {
				WpieCookieAllow::setNotice( $msg, 'error' );
				if( $onSettingsPage ) {
					delete_transient( 'wpca_upgrade_msg' );
				}
			}			
			
			if( $onSettingsPage ) {
				$this->currentTabIdx = $this->settingsProcessor->settingsPage->currentTabIdx;
				$this->cookiePolicyPostType = $this->settingsProcessor->getSetting('content')->get( 'content_cookie_policy_post_type' );			
			}					
						
			if( '0' === $this->pluginsStatus && !is_network_admin() ) {				
				$uri = $this->settingsProcessor->settingsPage->getTabParam( $this->settingsProcessor->settingsPage->getDefaultTabIdx(), 'uri' );
				$msg = ( $onSettingsPage ) ? 'The WeePie Cookie Allow Plugin is currently disabled' : sprintf( __( 'The WeePie Cookie Allow Plugin is currently disabled. <a href="%s">Enable it on the settings page</a>', 'wpca' ), $uri );
				WpieCookieAllow::setNotice( $msg, 'error' );
			}
			
			// enable/disable plugin for loggedin clients
			$this->disableLoggedIn = $this->settingsProcessor->getSetting('general')->get( 'general_disable_for_logged_in' );
			
			// the choosen cookies that should be allowde before consent (1,2,3)
			$this->cookiesBeforeConsent = (int) $this->settingsProcessor->getSetting('general')->get( 'general_cookies_before_consent' );
			
			add_filter( 'wpca_script_frontend_vars', array( $this, 'setScriptsVars' ) );
			add_filter( 'wpca_do_frontend_hooks', array( $this, 'doFrontendHooks' ) );			
			add_filter( "wpca_settings_escape_groupfield", array( $this, 'escapeSettingsField' ), 10, 3 );
			add_filter( "wpca_settings_escape_group", array( $this, 'escapeSettingsGroup' ), 10, 3 );
			add_filter( 'wpie_wp_query_args_post_type_dropdown', array( $this, 'setCookiePolicyQueryArgs' ) );
			add_filter( 'wpca_ajax_json_return', array( $this, 'processAjaxRequest' ), 10, 3 );
			
			// allow shortcodes to be added in text (custom HTML) widgets
			add_filter( 'widget_text', 'do_shortcode', 1 );
			
			// if PLugin is disabled or disabled for logged-in clients, stop here
			if( true === $this->doFrontendLogic() ) {					 
				define( 'WPCA_ACTIVE', true );				
				
				$d = $this->settingsProcessor->getSetting('general')->get( 'general_expire_time' );
				
				// the choosen consent method (1,2,3,4,5,6,7)
				$this->consentMethod = (int) $this->settingsProcessor->getSetting('general')->get( 'general_consent_method' );
				
				// read the value of cookie wpca_consent into a class member
				$this->cookieValueConsent = WpieCookieHelper::read( $this->getCookieName( 'consent' ) );
				
				add_action( 'wpca_styles_frontend', array( $this, 'setStyles' ), 10, 3 );
			}
		}		
	}
}