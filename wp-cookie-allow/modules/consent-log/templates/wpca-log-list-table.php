<?php
/**
 * Template for the logged consent list table
 *
 * @since 3.2
 */
?>
<?php if( $has_logs ): ?>
<table id="logs" class="group wpie-list-rows-table<?php echo ( !$has_scroll ) ? ' no-scroll':'' ?>" cellspacing="0">
	<thead>
		<tr class="wpie-list-heading wpie-list-row group">
		<?php foreach ( $columns as $column => $name ): ?>
		<th scope="row" class="wpie-list-heading-item wpie-list-heading-item-<?php echo str_replace( '_', '-', $column ) ?>"><?php echo $name ?></th>
		<?php endforeach ?>
		</tr>		
	</thead>
	<tbody>		
		<?php foreach ( $logs as $log ): 
		$declined = ( !$log->consent );
		$css_class = ( $declined ) ? ' declined' : '';
		?>
		<tr valign="top" class="wpie-list-row<?php echo $css_class?>" data-id="<?php echo $log->id ?>">
		<?php foreach ( $columns as $column => $name ): ?>
			<td class="wpie-list-row-field wpie-list-row-field-<?php echo str_replace( '_', '-', $column ) ?>"><?php echo $log->{$column} ?></td>
		<?php endforeach ?>
		<td colspan="<?php echo $columns_count ?>" class="wpie-list-row-field wpie-list-row-field-delete wpie-red-bgd"><span class="wpca-consent-log-delete dashicons dashicons-trash"></span><?php _e( 'Delete logged consent for IP address: ', 'wpca' ); echo $log->ip_address ?></td>
		</tr>
		<?php endforeach ?>					
	</tbody>
</table>
<?php else: ?><?php printf( __( 'Nothing logged yet. %sDid you enabled logging%s?', 'wpca' ), '<strong>', '</strong>' ) ?><?php endif ?>