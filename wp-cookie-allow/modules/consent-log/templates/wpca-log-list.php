<?php
/**
 * Template for the logged consent list wrapper
 * 
 * @since 3.2
 */
?>
<div id="wpca-log-list" class="wpie-list-wrapper">
	<table class="form-table form-fields-wpca">
		<tr valign="top" class="no-border">
			<td colspan="2">
			<?php 
			echo WpieFormHelper::formField(
					'text',
					'',
					'',
					'',
					array( 'id' => 'wpca-consent-log-filter', 'placeholder' => __( 'Search for IP addresses here' ), 'size' => 50 )
					);
			?>
			<?php
			echo WpieWpFormHelper::formField(
					'wpajaxbutton',
					'',
					'',
					'',
					array( 'id' => 'wpca-consent-log-filter-btn' ),
					__( 'Filter', 'wpca' )
					);
			?>
			<span class="descr-i toggler"><i></i></span>
			</td>
		</tr>
		<tr class="description" valign="top"><td colspan="2"><p class="togglethis" data-toggle-status="hide" style="display: none;"><?php _e( 'Search for logged IP addresses in the database. type the \'full\' address or just a start or ending part: for example all addresses starting with 123 type "123%". To query all, type "*".', 'wpca' ); ?></p></td></tr>
	</table>
<?php echo $table ?>
</div>