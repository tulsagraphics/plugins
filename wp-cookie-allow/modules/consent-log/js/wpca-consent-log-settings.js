/*
 * Please see wp-cookie-allow.php for more details.
 * 
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 3.2.3
 */

var WPCACONSENTLOGSETT = (function($) {
	""; // fix for yuicompressor
	"use strict";
	var settings = {			
			// declare public params
			tableParentDiv:	{},	
			table: 			{},
			filterTxtField:	{},
			filterBtn:		{}
		};

	var _idTableParent = 'wpca-log-list',
		_idTable = 'logs',
		_idTxtFieldFilter = 'wpca-consent-log-filter',
		_idBtnFilter = 'wpca-consent-log-filter-btn',
		_classRowDelete = 'wpie-list-row-field-delete',
		_classIconDelete = 'wpca-consent-log-delete',
		_allowedFilterCol = ['ip', 'IP'],
		_defaultCol = 'ip',
		_maxLengthColNeedles = {
			'ip':39 // ip address can has a max length of 39 chars (ipv6)
			};
	
	var _ajaxActionFilter = 'wpca-consent-log-filter',
		_ajaxActionDelete = 'wpca-consent-log-delete';
	
	/**
	 * Wrapper for console.log()
	 * 
	 * TODO: Needs to be replaced by WeePie Framework
	 */
	function log() {
		if(null != console && null != console.log) {
			if(0 < arguments.length) {
				[].unshift.call(arguments, 'WeePie log: ');
				console.log.apply(this, arguments);
			}
		}		
	}
	
	/**
	 * Determine if given column is allowed for filtering
	 * 
	 * @param {string} col
	 * 
	 * @returns {boolean}
	 */
	function isAllowedFilterCol(col) {
		if('string' !== typeof col) {
			return false;
		}
		return (-1 !== _allowedFilterCol.indexOf(col));
	}
	
	/**
	 * Success handler for the filter
	 * 
	 * @param {Object} r
	 */
	function fitlerSuccess(r) {
		try {
		  	switch(r.state) {
		  		case '-1':
		  			alert(r.out);
		  			break;
		  		case '0':
		  			if(r.wperrors) {
		  				if(r.wperrors.length) {
		  					for(var i=0; i<r.wperrors.length; i++) {
		  						log(r.wperrors[i]);		  						
		  					}
		  				}		  					
		  			}
		  			break;		  			
		  		case '1':	
		  			if(0 < r.out.count) {
		  				// add the updated table HTML
		  				$('#'+_idTable).replaceWith(r.out.table);
		  			} else {
		  				alert(wpcaDatal10n.log_filter_no_results);
		  			}
			    break;		    		
		  	}
		} catch(exc) {
			log(exc.message, exc);					
		}		
	}
	
	/**
	 * Success handler for the delete process
	 * 
	 * @param {Object} r
	 */
	function deleteSuccess(r) {
		try {
		  	switch(r.state) {
		  		case '-1':
		  			alert(r.out);
		  			break;
		  		case '0':
		  			if(r.wperrors) {
		  				if(r.wperrors.length) {
		  					for(var i=0; i<r.wperrors.length; i++) {
		  						// TODO consider log to console
		  						alert(r.wperrors[i]);		  						
		  					}
		  				}		  					
		  			}
		  			break;		  			
		  		case '1':	
		  			if(0 < r.out.deleted) {
		  				$('#'+_idTable).find('tr[data-id="'+r.out.deleted+'"]').remove();
		  			}
			    break;		    		
		  	}
		} catch(exc) {
			log(exc.message, exc);					
		}		
	}	
	
	/**
	 * Handler for click event on Refresh button
	 * 
	 * @param {Object} e
	 * 
	 * @returns {void} with invalid data
	 */
	function handlerBtnFilter(e) {
		e.preventDefault();
		var thiz = e.data.thiz,
			btn = $(this),
			col = _defaultCol,
			needle;
		
		// get the filter value
		needle = thiz.filterTxtField.val().trim();
		// basic filter value validation
		if('' === needle) {
			return;	
		} 
		
		// remove white spaces and lower case the needle
		needle = needle
					.trim()
					.toLowerCase();		
		
		if(-1 !== needle.indexOf('|')) {
			var parts = needle.split('|');
			// the filter column
			col = parts[0];
			// the needle witout the column
			needle = parts[1];
			
			// return if column is not allowed and show an alert message
			if(!isAllowedFilterCol(col)) {
				alert(wpcaDatal10n.log_filter_not_possible.replace('%COL%', col));
				return;
			}
		}		
		
		// return if column needle is exceeds the allowed max and show an alert message
		if(_maxLengthColNeedles[col] < needle.length) {
			alert(wpcaDatal10n.log_filter_not_possible_value_max.replace('%COL%', col).replace('%MAX%', _maxLengthColNeedles[col]));
			return;
		}		
		
		// disable the button while refreshing
		btn.prop('disabled', true).setLoadingImg(true);
		
		// setup ajax arguments
		var args = WPIESETT.getAjaxArgs(_ajaxActionFilter, {ndl:needle, col:col});
		
		// do ajax request
		$.get(ajaxurl, args, function(r) {
			fitlerSuccess(r);
		}, 'json')
		// if fails, log the error
		.fail(function(XMLHttpRequest, textStatus, errorThrown) {
			log(XMLHttpRequest.statusText);
		 })
		 // reset filter fields and button
		.always(function() {
			settings.filterTxtField.val('');
		  	// enable the button after refreshing
		  	settings.filterBtn.prop('disabled', false).setLoadingImg(false);
		});		
	}	
	
	/**
	 * Handler for mouseenter/mouseleave event on the table rows
	 * 
	 * @param {Object} e
	 */	
	function handlerClickDelete(e) {
		var icon = $(this),
			tr = icon.parents('tr'),
			id = tr.data('id');
		
		if (confirm(wpcaDatal10n.log_filter_del_confirm)) {				
			
			// setup ajax arguments
			var args = WPIESETT.getAjaxArgs(_ajaxActionDelete, {id:id});
			
			$.post(ajaxurl, args, deleteSuccess, 'json')
			// if fails, log the error
			.fail(function(XMLHttpRequest, textStatus, errorThrown) {
				log(XMLHttpRequest.statusText);
			 })
			 // reset filter fields and button
			.always(function() {

			});			
		} else {
			e.preventDefault();
			return false;				
		}		
	}	
	
	/**
	 * Init all events
	 * 
	 * @returns void
	 */
	settings.events = function() {		
		var thiz = this;
		
		this.tableParentDiv.on('click', '#'+_idBtnFilter, {thiz:this}, handlerBtnFilter);
		this.tableParentDiv.on('click', '.'+_classIconDelete, {thiz:this}, handlerClickDelete);
		
		// trigger btn filter when hitting 'Enter' key on input text or add btn
		this.tableParentDiv.on('keypress', '#'+_idTxtFieldFilter, {thiz:this}, function(e) {			
			if (13 === (e.keyCode || e.which)) {				
				$('#'+_idBtnFilter).trigger('click');				
			};			
		});
	};
	
	/**
	 * Init the automate logic when the DOM is ready
	 */
	settings.init = function() {
		try {
			// init DOM elements
			this.tableParentDiv  = $('#'+_idTableParent);
			this.table           = $('table#'+_idTable);
			this.filterTxtField  = $('#'+_idTxtFieldFilter);
			this.filterBtn       = $('#'+_idBtnFilter);
			
			// init all events
			this.events();
		} catch(exc) {
			log(exc.message, exc);
		}	
	};	
	
	return settings;
	
})(jQuery || {}, WPCACONSENTLOGSETT || {});

jQuery(function($) {
	// call init method when DOM is ready
	WPCACONSENTLOGSETT.init();	
});