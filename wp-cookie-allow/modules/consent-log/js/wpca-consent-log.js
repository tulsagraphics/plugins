/*
 * Please see wp-cookie-allow.php for more details.
 * 
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 3.2
 */

// TODO: error handler for ajax call

var WPCACONSENTLOG = (function($) {
	""; // fix for yuicompressor
	"use strict";
	var logger = {			
		// declare public params		
	};
	
	var _ajaxAction = 'wpca-log-consent';
	
	/**
	 * Success handler for the log
	 * 
	 * @param {Object} r
	 */
	function logSuccess(r) {
		try {
		  	switch(r.state) {
		  		case '-1':
		  			WPCAGLOBAL.log(r.out);
		  			break;
		  		case '0':
		  			if(r.wperrors) {
		  				if(r.wperrors.length) {
		  					for(var i=0; i<r.wperrors.length; i++) {
		  						WPCAGLOBAL.log(r.wperrors[i]);		  						
		  					}
		  				}		  					
		  			}
		  			break;
		  		case '1':
		  			WPCAGLOBAL.log('your consent has been logged succesfully.');	
			    break;		    		
		  	}
		} catch(exc) {
			WPCAGLOBAL.log(exc.message, exc);				
		}		
	}
	
	/**
	 * Perform the ajax log
	 * 
	 * @param {string} type
	 * @param {Object} args
	 */
	function doLog(type, args) {
		try {
			var payload = {}, 
				data = {};
			
			data['context'] = wpcaData.ajaxContext;
			data['type'] = type;
			data['consent'] = ('consent' === type) ? '1' : '0';
			data['categories'] = args.categories || [];			
			
			payload['action'] = 'wpie-action';
			payload[wpcaData.ns+'_action'] = _ajaxAction;
			payload['data'] = data;
			payload['nonce'] = wpcaData.nonce;
			
			$.post(wpcaData.ajaxurl, payload, logSuccess, 'json');
		} catch(exc) {
			WPCAGLOBAL.log(exc.message, exc);
		}
	}	
	
	/**
	 * Handler for wpca.consent:after event
	 * 
	 * @param {Object} e
	 * @param {Object} args
	 * 
	 * @returns {boolean|void}
	 */
	function handlerLogConsent(e, args) {
		var type = 'consent';
		doLog(type, args);
	}
	
	/**
	 * Handler for wpca.decline:after event
	 * 
	 * @param {Object} e
	 * @param {Object} args
	 * 
	 * @returns {boolean|void}
	 */	
	function handlerLogDecline(e, args) {
		var type = 'decline';
		doLog(type, args);
	}
	
	/**
	 * Init all events
	 * 
	 * @returns void
	 */
	logger.events = function() {
		if(wpcaData.needLogging) {
			$(window).on(WPCAFR.getEventName('consent_after'), handlerLogConsent);	
			$(window).on(WPCAFR.getEventName('decline_after'), handlerLogDecline);
		}
	};
	
	/**
	 * Init the automate logic when the DOM is ready
	 */
	logger.init = function() {
		try {
			if(!WPCAFR.done) {
				throw new Error('could not init WPCACONSENTLOG. WPCAFR is not done yet.');
			} else if(this.done) {
				return;
			}
			
			var requiredGlobals = ['ajaxContext', 'needLogging'];
			
			if(null == wpcaData) {
				throw new Error('Missing global param: wpcaData');
			}		

			for(var i=0; i<requiredGlobals.length; i++) {
				if(null == wpcaData[requiredGlobals[i]]) {
					throw new Error('Missing global param: '+requiredGlobals[i]);
				}				
			}			
			
			// init all events
			this.events();
		} catch(exc) {
			WPCAGLOBAL.log(exc.message, exc);
		}	
	};	
	
	return logger;
	
})(jQuery || {}, WPCACONSENTLOG || {});

jQuery(function($) {
	// call init method when DOM is ready
	WPCACONSENTLOG.init();	
});