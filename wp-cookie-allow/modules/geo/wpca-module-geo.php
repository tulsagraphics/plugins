<?php
/**
 * Please see wp-cookie-allow.php for more details.
 *
 * @author $Vincent Weber <vincent@webrtistik.nl>$
 */
if( !class_exists( 'WpcaGeo' ) ) {	
	/**
	 * WpcaGeo Class
	 *
	 * Module to handle the GEO functionality
	 *
	 * @author $Vincent Weber <vincent@webrtistik.nl>$
	 * 
	 * @see https://github.com/maxmind/geoip-api-php
	 * 
	 * @since 3.2.6
	 */	
	class WpcaGeo extends WpiePluginModule 
	{
		/**
		 * Filename of the GeoIP src
		 *
		 * @var string
		 *
		 * @since 3.2.6
		 */
		const FILE_NAME_SRC_GEOIP = 'geoip.inc';		
		
		/**
		 * Filename of the GeoIP ipv4 database
		 * 
		 * @var string
		 * 
		 * @since 3.2.6
		 */
		const FILE_NAME_DB_GEOIP = 'GeoIP.dat';
		
		/**
		 * Filename of the GeoIP ipv6 database
		 * 
		 * @var string
		 * 
		 * @since 3.2.6
		 */		
		const FILE_NAME_DB_GEOIPV6 = 'GeoIPv6.dat';
		
		/**
		 * The EU continent code
		 * 
		 * @var string
		 * 
		 * @since 3.2.6
		 */
		const EU_CONTINENT_CODE = 'EU';

		/**
		 * Module priority
		 * 
		 * Must be lower then WpcaCore (5)
		 * 
		 * @var integer
		 * 
		 * @since 3.2.6
		 */
		protected $priority = 1;
		
		/**
		 * The client IP address
		 *
		 * @var string
		 *
		 * @since 3.2.6
		 */
		private $clientIP = '0.0.0.0';
		
		/**
		 * Path to the src folder
		 *
		 * @var string
		 *
		 * @since 3.2.6
		 */
		private $pathSrc = '';		
		
		/**
		 * Path to the DB file folder
		 * 
		 * @var string
		 * 
		 * @since 3.2.6
		 */
		private $pathDb = '';
		
		/**
		 * Path to the GeoIP PHP src file
		 * 
		 * @var string
		 * 
		 * @since 3.2.6
		 */
		private $pathSrcFile = '';
		
		/**
		 * Path to the DB file
		 * 
		 * @var string
		 * 
		 * @since 3.2.6
		 */
		private $pathDbFile = '';
		
		/**
		 * Flag if GeoIP db is ready
		 * 
		 * @var boolean
		 * 
		 * @since 3.2.6
		 */
		private $hasGeoIPDB = false;
		
		/**
		 * Flag if GeoIP class exist
		 *
		 * @var boolean
		 *
		 * @since 3.2.6
		 */
		private $hasGeoIPClass = false;		
		
		/**
		 * Flag if is a IPv4 address
		 * 
		 * @var string
		 * 
		 * @since 3.2.6
		 */
		private $isIpv4 = false;
		
		/**
		 * Flag if is a IPv6 address
		 * 
		 * @var string
		 * 
		 * @since 3.2.6
		 */
		private $isIpv6 = false;
		
		/**
		 * Flag if GeoIP lohic should be done
		 * 
		 * @var boolean
		 * 
		 * @since 3.2.6
		 */
		private $doGeoIP = false;
		
		/**
		 * The GeoIP instance
		 * 
		 * @var GeoIP
		 * 
		 * @since 3.2.6
		 */
		private $geoip;
		
		/**
		 * Settings value for general_disable_for_non_eu_visitors
		 * 
		 * @var boolean
		 * 
		 * @since 3.2.6
		 */
		private $disableStatus = false;
		
		/**
		 * The country code
		 * 
		 * @var string
		 * 
		 * @since 3.2.6
		 */
		public $countryCode = '';
		
		/**
		 * The country code number
		 * 
		 * @var string
		 * 
		 * @since 3.2.6
		 */
		public $countryCodeNum = 0;

		/**
		 * The country regoin
		 *
		 * @var string
		 *
		 * @since 3.2.6
		 */
		public $countryRegion = '';		
		
		/**
		 * Flag if client IP address is from the EU
		 * 
		 * @var string
		 * 
		 * @since 3.2.6
		 */
		public $isIpAddressFromEU = false;
		
		/**
		 * Constructor
		 * 
		 * @since 3.2.6
		 */
		public function __construct() {}

		/**
		 * {@inheritDoc}
		 * @see WpiePluginModule::start()
		 */
		public function start() 
		{
			$error = '';
			$this->pathSrc     = $this->globals->get( 'modulePath' ) . '/'. parent::getIndex() .'/src';
			$this->pathDb      = $this->globals->get( 'modulePath' ) . '/'. parent::getIndex() .'/db';
			$this->pathSrcFile = $this->pathSrc . '/' . self::FILE_NAME_SRC_GEOIP;
			$this->clientIP    = WpieMiscHelper::getClientIp();
			
			if( false !== filter_var( $this->clientIP, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
				$this->isIpv4 = true;
				$this->pathDbFile = $this->pathDb . '/' . self::FILE_NAME_DB_GEOIP;
			} elseif( false !== filter_var( $this->clientIP, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 ) ) {
				$this->isIpv6 = true;
				$this->pathDbFile = $this->pathDb . '/' . self::FILE_NAME_DB_GEOIPV6;
			}			
				
			if( !( $this->hasGeoIPDB = file_exists( $this->pathDbFile ) ) ) {
				$error = sprintf( __( 'WeePie Cookie Allow: it seems that the GEO database file (GeoIP.dat or GeoIPv6.dat) could not be located. Please verify the existence of these files at: %s.', 'wpca' ), $this->pathDb );
			} else {
				@include( $this->pathSrcFile );
				if( !( $this->hasGeoIPClass = class_exists( 'GeoIP' ) ) ) {
					$error = sprintf( __( 'WeePie Cookie Allow: it seems that the GeoIP class does not exist at %s.', 'wpca' ), $this->pathSrcFile );
				}
			}
			
			$this->doGeoIP = ( $this->hasGeoIPDB && $this->hasGeoIPClass && !WpieMiscHelper::isLocalhost() );
				
			if( '' !== $error && is_admin() ) {
				WpieCookieAllow::setNotice( $error, 'error' );
			}
		}
		
		/**
		 * Determine if IP address is from the EU
		 * 
		 * @since 3.2.6
		 * 
		 * @return boolean
		 */
		public function isIpAddressFromEU() 
		{
			return $this->isIpAddressFromEU;
		}
		
		/**
		 * Determine if need to disable for non EU IP Address
		 *
		 * @since 3.2.6
		 *
		 * @return boolean
		 */
		public function disableForNonEUAddress()
		{
			return ( $this->disableStatus && !$this->isIpAddressFromEU );
		}		
		
		/**
		 * Determine if NOT need to disable for non EU IP Address
		 *
		 * @since 3.2.6
		 *
		 * @return boolean
		 */
		public function disableNOTForNonEUAddress()
		{
			if( !$this->disableStatus ) {
				return true;
			} elseif ( $this->disableStatus && $this->isIpAddressFromEU ) {
				return true;
			} else {
				return false;
			}
		}		
		
		/**
		 * {@inheritDoc}
		 * @see iWpiePluginModule::init()
		 */
		public function init() 
		{	
			// only do this logic for the frontend
			if( is_admin() || !$this->doGeoIP ) {
				return;
			}

			$settingsGeneral = $this
				->settingsProcessor
				->getSetting( 'general' );
				
			// just a check to prevent fatal errors
			if( !$settingsGeneral ) {
				return;
			}
			
			if( false == ( $this->disableStatus = $settingsGeneral->get( 'general_disable_for_non_eu_visitors' ) ) ) {
				return;
			}
				
			$this->fetch();
			
			// apply GeoIP logic
			add_filter( 'wpca_do_frontend_logic', 'wpca_disable_not_for_non_eu_ip_address', 100 );			
		}
		
		/**
		 * Fetch the data
		 * 
		 * @uses geoip_load_shared_mem() if memory usage is enabled
		 * @uses geoip_open()
		 * @uses geoip_country_code_by_addr()
		 * @uses geoip_close()
		 * 
		 * @since 3.2.6 
		 */
		private function fetch() 
		{
			/**
			 * Let others change the GeoIP flags
			 * 
			 * Please note: GEOIP_SHARED_MEMORY requires php >= 4.0.4 compiled with --enable-shmop configure time
			 * 
			 * @var Number $flags
			 * 
			 * @see https://github.com/maxmind/geoip-api-php#memory-caching
			 * @see http://us2.php.net/manual/en/ref.shmop.php
			 */
			$flags = apply_filters( 'wpca_geoip_open_flags', GEOIP_STANDARD );
			
			if( GEOIP_SHARED_MEMORY === $flags ) {
				geoip_load_shared_mem( $this->pathDbFile );
			}
			
			$this->geoip = geoip_open( $this->pathDbFile, $flags );
			$this->countryCode = geoip_country_code_by_addr( $this->geoip, $this->clientIP );
			$this->countryCodeNum = $this->geoip->GEOIP_COUNTRY_CODE_TO_NUMBER[$this->countryCode];
			$this->countryRegion = $this->geoip->GEOIP_CONTINENT_CODES[$this->countryCodeNum];
			$this->isIpAddressFromEU = ( self::EU_CONTINENT_CODE === $this->countryRegion );				
			geoip_close( $this->geoip );
		}
	}
	
	if( !function_exists( 'wpca_is_ip_from_eu' ) )
	{
		/**
		 * Determine if IP address is from the EU
		 *
		 * @uses WpcaGeo::isIpAddressFromEU()
		 *
		 * @since 3.2.6
		 *
		 * @return boolean
		 */
		function wpca_is_ip_from_eu()
		{
			if( false !== ( $module = WpieCookieAllow::getModule( 'geo' ) ) )
			{
				$is = $module->isIpAddressFromEU();
				unset( $module );
	
				return $is;
			}
		}
	}
	
	if( !function_exists( 'wpca_disable_for_non_eu_ip_address' ) )
	{
		/**
		 * Determine if need to disable for non EU IP Address
		 *
		 * @uses WpcaGeo::disableForNonEUAddress()
		 *
		 * @since 3.2.6
		 *
		 * @return boolean
		 */
		function wpca_disable_for_non_eu_ip_address()
		{
			if( false !== ( $module = WpieCookieAllow::getModule( 'geo' ) ) )
			{
				$disable = $module->disableForNonEUAddress();
				unset( $module );
	
				return $disable;
			}
		}
	}
	
	if( !function_exists( 'wpca_disable_not_for_non_eu_ip_address' ) )
	{
		/**
		 * Determine if need to disable for non EU IP Address
		 *
		 * @uses WpcaGeo::disableNOTForNonEUAddress()
		 *
		 * @since 3.2.6
		 *
		 * @return boolean
		 */
		function wpca_disable_not_for_non_eu_ip_address()
		{
			if( false !== ( $module = WpieCookieAllow::getModule( 'geo' ) ) )
			{
				$disable = $module->disableNOTForNonEUAddress();
				unset( $module );
	
				return $disable;
			}
		}
	}	
}