<?php
/**
 * Template for the Cookie Category settings outer box
 * 
 * @since 3.2
 */
?>
<div class="wpca-cc-sett-box<?php echo $css_class ?>" id="wpca-cc-sett-box-<?php echo $counter ?>" data-id="<?php echo $counter ?>"><?php echo $content ?></div>