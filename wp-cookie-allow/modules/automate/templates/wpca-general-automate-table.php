<?php
/**
 * Template for the automate table on tab general
 * 
 * @since 3.1.x
 */
?>
<table id="automate">
	<tbody>
		<tr valign="top">
			<th scope="row">
			<?php echo WpieFormHelper::formField( 
			
					'checkboxcheckall',
					"{$setting}[automate_check_all]",
					$automate_check_all,
					'',
					array( '_parent' => 'automate' )				
					); _e( '3rd party', 'wpca' )?>
			</th>
			<?php if( isset( $extra['th'] ) ): foreach ( $extra['th'] as $th ): ?>
			<th scope="row"><?php echo $th ?></th>
			<?php endforeach; endif; ?>
		</tr>
		<?php foreach ( $automate_data as $key => $data ): ?>		
		<?php if( 0 === strpos( $key, '_' ) ) continue; ?>				
		<tr valign="top" class="<?php echo $key ?>">
		<th>
		<?php echo WpieFormHelper::formField( 
		
				'hidden',
				"{$setting}[automate_data][$key][k]",
				$key				
				); ?>		
		<?php echo WpieFormHelper::formField( 
		
				'checkbox',
				"{$setting}[automate_data][$key][check]",
				$data['check'],
				'',
				array( 'class' => 'automate-check' ),
				'',
				array()			
				
				); ?>
				<?php echo $data['label']; ?>
		</th>
		<?php if( isset( $extra['td'] ) && isset( $extra['td'][$key] ) ): ?>
		<td class="cc"><?php echo $extra['td'][$key] ?></td>
		<?php endif; ?>
		</tr>
		<?php endforeach ?>
	
		<tr valign="top" class="no-border">
			<td colspan="2" class="automate-action"><?php echo WpieFormHelper::formField( 
			
					'button',
					'',
					'',
					'',
					array( 'id' => 'wpca-automate-refresh-btn' ),
					__( 'Refresh', 'wpca' )
					
					) 
			?><?php	echo WpieFormHelper::formField( 
			
					'linkbutton',
					'',
					'',
					'',
					array( 'href' => $automate_request_3rd_mailto, 'style' => 'margin-left:5px' ),
					__( 'Request missing 3rd party', 'wpca' )
											
					) 
			?><span class="ajax-loading ajax-loading-wpie"></span></td>		
		</tr>			
	</tbody>
</table>