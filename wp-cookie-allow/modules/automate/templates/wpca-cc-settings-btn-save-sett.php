<?php
/**
 * Template for showing the cookie category save settings button
 *
 * Please see wp-cookie-allow.php for more details.
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 3.2
 */
?>
<button class="wpca-btn-cc-save-sett wpca-btn" data-txt="<?php echo $txt ?>" data-txt-processing="<?php echo $txt_processing ?>"><?php echo $txt ?></button>