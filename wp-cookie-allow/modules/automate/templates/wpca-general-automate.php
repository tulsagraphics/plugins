<?php
/**
 * Template for the automate table on tab general
 * 
 * @since 3.0
 */
?>
<a class="toggler"><?php _e( 'Click here', 'wpca' ) ?></a><?php _e( ' to enable/disable your thirdparties manually.', 'wpca' ) ?>
<div class="togglethis" data-toggle-status="hide">
<?php echo $table ?>
</div>