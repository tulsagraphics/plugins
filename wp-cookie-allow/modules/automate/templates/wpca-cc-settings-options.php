<?php
/**
 * Template for Cookie Category settings
 * 
 * @since 3.2
 */
?>
<?php if( $has_cookie_categories ): ?>

<div class="wpca-cc-sett-opions"><?php echo WpieFormHelper::formField( 
		'checkboxmulti',
		$cookie_category_attr_name,
		$cookie_category_attr_value,
		'',
		$cookie_category_input_attributes,
		'',
		$cookie_category_inputs	
		); 
?></div>
<?php else: ?>

<?php endif ?>