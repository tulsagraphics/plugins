<?php
/**
 * Please see wp-cookie-allow.php for more details.
 *
 * @author $Vincent Weber <vincent@webrtistik.nl>$
 */
if( !class_exists( 'WpcaAutomateReplace' ) ) {
	
	class WpcaAutomateReplace extends WpiePluginModule {

		/**
		 * Template file name for the replacement html
		 * 
		 * @since 2.3
		 * 
		 * @var string
		 */
		const TEMPL_FILE_NAME_REPLACE_ELEM = 'wpca-replacement-element.php';
		
		
		/**
		 * File name for the replace css
		 *
		 * @since 2.3
		 *
		 * @var string
		 */
		const FILE_NAME_REPLACE_CSS = 'wpca-replacement';
		
		
		/**
		 * File name for the blocked js src attribute
		 *
		 * @since 3.2.3
		 *
		 * @var string
		 */
		const FILE_NAME_BLOCKED_JS = 'blocked.js';
		

		/**
		 * File name for the blocked iFrame src attribute
		 *
		 * @since 3.2.3
		 *
		 * @var string
		 */
		const FILE_NAME_BLOCKED_IFRAME = 'blocked.html';
		
		
		/**
		 * File name for the blocked img src attribute
		 *
		 * @since 3.2.3
		 *
		 * @var string
		 */
		const FILE_NAME_BLOCKED_IMG = 'blocked.jpg';		
		

		/**
		 * The default replace string
		 *
		 * @since 3.1.2
		 *
		 * @var string
		 */
		const REPLACE_STR = 'WeePie Cookie Allow: %s has been blocked for category %s';	
		
		/**
		 * HTML node names in the body that shouldnt have a replacement element 
		 * 
		 * @since 3.2
		 * 
		 * @var array
		 */
		private $nodeNamesBodyNoReplacementElement = array( 'script', 'img' );
		
		
		/**
		 * Constructor
		 *
		 * @acces public
		 *
		 * @since 2.3
		 */
		public function __construct() {}
		
	
		/* (non-PHPdoc)
		 * @see WpiePluginModule::start()
		 */
		public function start()	{}		
		
			
		/**
		 * Get HTML for the replacement block		 
		 * 
		 * @access public
		 * 
		 * @param string $txt
		 * @param string $type the 3rd party
		 * @param boolean $forceAcceptBtn, add the accept button if not present
		 * @param string $cc
		 * 
		 * @uses WpieTemplate
		 * @uses do_shortcode()
		 * 
		 * @since 2.3
		 * 
		 * @return string
		 */
		public function getElement( $txt = '', $type = '', $forceAcceptBtn = false, $cc = ''  )
		{		
			$block = new WpieTemplate( self::TEMPL_FILE_NAME_REPLACE_ELEM, dirname( __FILE__ ) . '/templates', self::TEMPL_FILE_NAME_REPLACE_ELEM );
			$txt = ( '' === $txt ) ? $this->settingsProcessor->getSetting( 'content' )->get( 'content_automate_replace_txt' ) : $txt;
			$bgdColor = $this->settingsProcessor->getSetting( 'style' )->get( 'style_placeholder_bgd_color' );
			$hideLogo = $this->settingsProcessor->getSetting( 'content' )->get( 'content_automate_replace_hide_logo' );
			
			if ( WpieMiscHelper::isValidHexColor( "#$bgdColor") && false === strpos( $bgdColor , '#') ) {
				$bgdColor = "#$bgdColor";
			}
			
			if( $hasShortcode = has_shortcode( $txt, 'wpca_btn_accept' ) ) {
				$txt = do_shortcode( $txt );
			}
			if( $hasShortcode = has_shortcode( $txt, 'wpca_btn_decline' ) ) {
				$txt = do_shortcode( $txt );
			}
		
			if( !$hasShortcode && $forceAcceptBtn ) {
				$btnAcceptTxt = $this->settingsProcessor->getSetting( 'content' )->get( 'content_button_accept_txt' );
				$txt .= sprintf( '[wpca_btn_accept txt="%s"]', $btnAcceptTxt );
				$txt = do_shortcode( $txt );
			}
			
			$block->setVar( 'img_uri', $this->globals->get( 'imgUri' ) ); 
			$block->setVar( 'txt', $txt );
			$block->setVar( 'bgd_color', $bgdColor );
			$block->setVar( 'hide_logo', $hideLogo );
			$block->setVar( 'type', $type );
			$block->setVar( 'cc', $cc );
						
			return $block->render( false, true );		
		}			
	
		
		/**
		 * Callback for the wpca_styles_frontend hook
		 *
		 * Enqueue styles for the replacement blocks
		 *
		 * @access public
		 *
		 * @param WP_Styles	$wp_styles
		 * @param array	$excludes items to exclude
		 * @param bool $isModeDev
		 *
		 * @uses wp_register_style()
		 *
		 * @since 2.3
		 */
		public function setStyles( $wp_styles, $excludes, $isModeDev )
		{
			if( !in_array( 'wpca-replacement', $excludes ) ) {
				$ext = ( $isModeDev ) ? '.css' : '.min.css';
				wp_enqueue_style( 'wpca-replacement', $this->globals->get('moduleUri') . '/'. parent::getIndex() .'/css/'. self::FILE_NAME_REPLACE_CSS . $ext, array(), WpieCookieAllow::VERSION );
			}
		}
		
		/**
		 * Callback for the wpca_script_frontend_vars hook
		 *
		 * @access public
		 *
		 * @param array $vars
		 *
		 * @since 3.2.3
		 *
		 * @return array
		 */
		public function setScriptsVars( $vars )
		{
			$settingsContent = $this->settingsProcessor->getSetting( 'content' );
			$settingsStyle = $this->settingsProcessor->getSetting( 'style' );
			$assetsUri = $this->globals->get('assetsUri') . '/';
			
			$vars['doPlaceholder'] = $settingsContent->get( 'content_automate_replace_enable' );
			$vars['minPlacehoderDim'] = array(
					'w' => (int)$settingsStyle->get( 'style_placeholder_min_w' ),
					'h' => (int)$settingsStyle->get( 'style_placeholder_min_h' )
			);
			$vars['blockedAssetsUri'] = array( 
					'js' => $assetsUri.'js/'.self::FILE_NAME_BLOCKED_JS,
					'img' => $assetsUri.'img/'.self::FILE_NAME_BLOCKED_IMG,
					'iframe' => $assetsUri.self::FILE_NAME_BLOCKED_IFRAME					
			);
			
			unset( $settingsContent );
		
			return $vars;
		}
		
		/**
		 * Mark the matched HTML
		 * 
		 * @param string $type
		 * @param string $cc
		 * @param string $match
		 * @param array $matches
		 * 
		 * @since 3.2.3
		 * 
		 * @return string
		 */
		public function mark( $text = '', $type = '', $context = '', $match = '', $cc = '', $matches = array() )
		{
			if( '' === $type ) {
				$type = __( '3rd party', 'wpca' );
			}
			
			$_match = $match;
			$markHtml = '';
			$hasElStart = ( isset( $matches['EL_START'] ) );
			$nodeName = ( $hasElStart ) ? ltrim( $matches['EL_START'], '<' ) : '';
			
			if( $hasElStart ) {
				$isScript = ( 'script' === $nodeName );
				$isIframe = ( 'iframe' === $nodeName );
				$isImg = ( 'img' === $nodeName );
				$src = false;
				
				// remove type="text/javascript"
				if( $isScript ) {
					if( false !== strpos( $_match, 'type="text/javascript"') ) {
						$_match = str_replace( 'type="text/javascript"', 'type="text/template"', $_match );
					} elseif( false !== strpos( $_match, 'type=\'text/javascript\'') ) {
						$_match = str_replace( 'type=\'text/javascript\'', 'type=\'text/template\'', $_match );
					} else {
						$_match = str_replace( '<script', '<script type="text/template"', $_match );
					}
				}
				
				// get the source attribute
				if( isset( $matches['EL_SRC'] ) ) {
					$src = $matches['EL_SRC'];
				} elseif( $isImg && isset( $matches['ATTR_V'] ) ) {
					$src = $matches['ATTR_V'];
				}
				
				$data = array();
				$data['id'] = 0;
				$data['cc'] = $cc;
				$data['txt'] = $text;
				$data['type'] = $type;
				$data['context'] = $context;
				$data['blocked'] = 1;
				$data['placeholder'] = false;
				$markHtml .= ' data-wpca-marked="1" data-wpca-marked-auto="1"';
				
				if( $src ) {
					$srcBlocked = $this->globals->get('assetsUri') . '/';
					if( $isScript ) {
						$srcBlocked .= 'js/'.self::FILE_NAME_BLOCKED_JS;
					} elseif( $isIframe ) {
						$srcBlocked .= self::FILE_NAME_BLOCKED_IFRAME;
					} elseif( $isImg ) {
						$srcBlocked .= 'img/'.self::FILE_NAME_BLOCKED_IMG;
					} else {
						$srcBlocked = '';
					}
					
					$data['src'] = $src;
					$_match = str_replace( $src, $srcBlocked, $_match );
				}
				
				if( 'body' === $context ) {					
					$data['placeholder'] = true;
					
					/**
					 * Let others filter the node names
					 *
					 * @param array $nodeNamesBodyNoReplacementElement
					 * @param string $type
					 *
					 * @since 3.2
					 */
					$nodeNamesBodyNoReplacementElement = apply_filters( 'wpca_node_names_body_no_replacement_element', $this->nodeNamesBodyNoReplacementElement, $type );
				
					if( count( $nodeNamesBodyNoReplacementElement ) && preg_match( '#^('.join( '|', $nodeNamesBodyNoReplacementElement ).')#', substr( trim( $match ), 1 ) ) ) {						
						$data['placeholder'] = false;
					}
				}
				
				$markHtml .= sprintf( " data-wpca-marked-data='%s'", wp_json_encode( $data ) );
				$markHtml = str_replace( '<'.$nodeName, '<'.$nodeName.$markHtml.' ', $_match );
			}
			
			return  $markHtml . '<!--[wpca_mrkd]-->';	
		}
		
		/**
		 * Callback for wpca_validated_data hook
		 * 
		 * Validate the form fields for the replacement module
		 * 
		 * @access public
		 * 
		 * @param array $data
		 * @param string $page
		 * @param bool	$hasWarnings
		 * 
		 * @since 2.3
		 * 
		 * @param array $data
		 */
		public function validate( $data = array(), $page = '', $hasWarnings = false ) 
		{
			if( $page === $this->settingsProcessor->getSettingName( 'style' ) )
			{
				if( isset( $data['style_placeholder_bgd_color'] ) ) {
					$color = $data['style_placeholder_bgd_color'];
					if ( WpieMiscHelper::isValidHexColor( "#$color") && false === strpos( $color , '#') ) {
						$data['style_placeholder_bgd_color'] = "#$color";
					}
				}
			}
			
			return $data;
		}
		
		
		/**
		 * Callback for the wpca_settings_skip_fields hook
		 *
		 * Skip the "content_automate_replace_enable" setting when satinizing
		 *
		 * @acces public
		 *
		 * @param array	$skip
		 * @param string $setting
		 *
		 * @since 2.3
		 *
		 * @return array the filtered array
		 */
		public function skipSatinizing( $skip, $setting )
		{
			if( $this->settingsProcessor->getSettingName( 'content' ) !== $setting )
				return $skip;
				
			$skip[] = 'content_automate_replace_enable';
				
			return $skip;
		}		

		
		/**
		 * Callback for the wpca_update_settings hook
		 *
		 * @access public
		 *
		 * @param array $updates
		 * @param array $data
		 * @param string $setting the current settings DB name
		 *
		 * @since 2.3
		 *
		 * @return array with updated setting form fields
		 */
		public function updateSetting( $updates, $data, $setting )
		{
			if( $this->settingsProcessor->getSettingName( 'style' ) === $setting ) {
				if( isset( $data['style_placeholder_bgd_color'] ) ) {
					$updates['style_placeholder_bgd_color'] = $data['style_placeholder_bgd_color'];
				}
			} elseif( $this->settingsProcessor->getSettingName( 'content' ) === $setting ) {
				$updates['content_automate_replace_enable'] = ( isset( $data['content_automate_replace_enable'] ) ) ? true : false;
				
				if( isset( $data['content_automate_replace_txt'] ) ) {
					$updates['content_automate_replace_txt'] = $data['content_automate_replace_txt'];
				}
			}
		
			return $updates;
		}
		
		/**
		 * Callback for the wp_footer hook
		 * 
		 * Add the placeholder HTML
		 * 
		 * @uses self::getElement()
		 * 
		 * @since 3.2.3
		 */
		public function addHtml() 
		{
			$placeHolder = $this->getElement( '', '%TYPE%', false, '%CC%' );
			echo "<template id='wpca-placeholer-html'>$placeHolder</template>";
		}
		
		
		/* (non-PHPdoc)
		 * @see iWpiePluginModule::init()
		 * 
		 * @since 2.3
		 */
		public function init() 
		{			
			// return to prevent fatal errors
			// @see https://github.com/webRtistik/wp-cookie-allow/issues/79
			if( false === ( $moduleCore = WpieCookieAllow::getModule( 'core' ) ) ) {
				return;
			}
			
			$pluginEnabled = ( '1' === $moduleCore->pluginsStatus ) ? true : false;
		
			if( is_admin() ) {
				add_filter( 'wpca_validated_data', array( $this, 'validate' ), 10, 3 );
				add_filter( 'wpca_settings_skip_fields', array( $this, 'skipSatinizing' ), 10, 2 );
				add_filter( 'wpca_update_settings', array( $this, 'updateSetting' ), 10, 3 );			
			}	else {				
				if( !defined( 'WPCA_ACTIVE' ) ) {
					return;
				}								

				add_action( 'wpca_styles_frontend', array( $this, 'setStyles' ), 10, 3 );
				add_filter( 'wpca_script_frontend_vars', array( $this, 'setScriptsVars' ) );
				add_filter( 'wpca_replacement_text', array( $this, 'mark' ), 10, 6 );
				add_action( 'wp_footer', array( $this, 'addHtml' ), 999999 );
			}			
		}	
	}
	
	if( !function_exists( 'wpca_do_cookies_block' ) )
	{
		/**
		 * Show a replacement block if cookies are not accepted
		 * 
		 * @uses WpcaAutomateReplace::getElement() to render the block
		 * 
		 * @param string $txt the text to display inside the replacement block
		 * @param string $type
		 * @param boolean $forceAcceptBtn
		 * @param string $cc
		 * 
		 * @return bool
		 */
		function wpca_do_cookies_block( $txt = '', $type = '', $forceAcceptBtn = false, $cc = '' )
		{
			if( false !== ( $module = WpieCookieAllow::getModule( 'automate-replace' ) ) )
			{
				echo $module->getElement( $txt, $type, $forceAcceptBtn, $cc );
				unset( $module );
			}			
		}
	}	
}