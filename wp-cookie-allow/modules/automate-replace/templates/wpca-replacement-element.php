<?php
/**
 * Template for showing a cookie block replacement
 * 
 * Please see wp-cookie-allow.php for more details.
 * 
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 */
?>
<div class="wpca-replacement-elem<?php echo ( '' !== $type ) ? ' wpca-replace-'.$type : ''?><?php echo ( '' !== $cc ) ? ' wpca-replace-'.$cc : '' ?>" style="background-color:<?php echo $bgd_color ?>;">	
	<?php if( !$hide_logo ): ?><img src="<?php echo $img_uri ?>/logo-cookie-allow-replacement-block.png" class="wpca-replacement-logo" /><?php endif ?>
	<p class="wpca-replace-txt"><?php echo ( '' !== $txt ) ? $txt : __( 'Content blocked by WeePie Cookie Allow Plugin' , 'wpca' ) ?></p>
</div>