/*
 * Please see wp-cookie-allow.php for more details.
 * 
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 2.3.3
 */

var WPCAFR = (function($) {
	""; // fix for yuicompressor
	"use strict";	
	var front = {			
		// declare public params
		done: false
	};
	
	// declare private params
	var classShow  = 'wpca-show',
		classHide  = 'wpca-hide',
		classBtn   = 'wpca-btn',
		idLayer    = 'wpca-trans-layer',	
		statusHide = 'hide',
		statusShow = 'show',
		idCloseX   = 'wpca-close',
		cssIdPlaceholderTmpl = 'wpca-placeholer-html',
		cssIdPlaceholderPrefix = 'wpca-replacement-elem-',
		cssClassPlaceholderTxt = 'wpca-replace-txt',
		cssClassPosTop = 'wpca-top',
		cssClassCenteredBox = 'wpca-ct',
		cssClassCcBox = 'wpca-cc-sett-box',
		cssClassCcBoxOptions = 'wpca-cc-sett-opions',
		cssClassCcBoxBtnDefault = 'wpca-btn-cc-default-sett',
		cssClassCcBoxBtnSave = 'wpca-btn-cc-save-sett',		
		cssClassBtnAccept = 'wpca-btn-accept',
		cssClassBtnDecline = 'wpca-btn-decline',
		cssClassBtnResetConsent = 'wpca-btn-reset-consent',		
		cssClassblocked = 'wpca-blocked',
		cssClassBtnHide = 'wpca-btn-hide',
		cssClassBtnHasReset = 'wpca-btn-has-reset',
		elemNameBlock = 'wpca-block',
		eventBaseNamespace = 'wpca',
		scriptTypeBlocked = 'text/template',
		dataKeyTxt = 'txt',
		dataKeyTxtProcessing = 'txt-processing',
		dataKeyMarked = 'wpca-marked',
		dataKeyMarkedAuto = 'wpca-marked-auto',
		dataKeyMarkedData = 'wpca-marked-data',
		dataKeySkipBckp = 'wpca-skip-bckp',
		dataKeyCcCat = 'cat',
		dataKeyCcBoxId = 'id',
		timeoutProcessing = 200, 
		animateDuration = 200,
		layout = 'bar',
		yPos = 'top',	
		posIn = 0,
		posOut = 0,			
		cookieExpire = 30,
		hasWPadminbar = false,
		isCenteredBox = false,
		consentMethod = 0,
		hasClientCc = false,
		hasCloseEl = false,
		useXAsDismiss = false,
		minScrollTop = 200,
		didSetCcDefaultSett = false,
		didCcSaveSett = false,
		selectedCc = [],
		clientCc = [],
		hasChangedCat = false,
		states = {
				barbox:null, 
				accepting: false, 
				declining: false, 
				resetting: false,
				savingcc: false
		},		
		elem = {
			barbox:      {},
			ccBox:       {},
			wpadminbar:  {},		
			layer:	     {},
			privacyBtns: {},
			privacyBtn:	 {},		
			closeEl:	 {}
		};	
	
	/**
	 * Custom events
	 */
	var cEvents = {
		/**
		 * The names of the events
		 */
		names: {
			consent_after: 'wpca.consent:after',
			decline_after: 'wpca.decline:after',
			reset_after: 'wpca.reset:after',
			cc_saved: 'wpca.cc:saved'
		},
		/**
		 * Get the name of an event by index
		 * 
		 * @param {String} idx
		 * 
		 * @returns {String}
		 */
		getName: function(idx) {
			var event = '', i;
			if('string' !== typeof idx) {
				return event;
			}
			for(i in this.names) {
				if(idx === i) {
					event = this.names[i];
					break;
				}
			}
			return event;
		},			
		/**
		 * Trigger an event
		 * 
		 * @returns void
		 */
		trigger: function() {
			var allowedEv = false,
				event = '',
				i;
			if(0 < arguments.length) {
				var idx = arguments[0];
				if('string' !== typeof idx) {
					idx = '';
				}			
				if(1 < arguments.length) {
					[].shift.apply(arguments);
				}						
			}
			event = this.getName(idx);	
			if('' !== event) {
				$(window).triggerHandler(event, arguments);
			}		
		}
	};
	
	var	events = {				
		/**
		 * Unbind all events
		 * 
		 * @returns events
		 */	
		unbind:function() {
			$(window).off(cEvents.names.consent_after, handlerConsentAfter);
			$(window).off(cEvents.names.decline_after, handlerDeclineAfter);		
			$(window).off(cEvents.names.reset_after, handlerResetAfter);
			$('body').off('click', '.'+cssClassBtnAccept, handlerConsentClickAccept);
			$('body').off('click', '.'+cssClassBtnDecline, handlerConsentClickDecline);
			$('body').off('click', '.'+cssClassBtnResetConsent, handlerConsentClickResetConsent);
			elem.close.off('click', handlerClickClose);
			$(window).off( 'click', handlerConsentClickWindow);
			$('.'+cssClassCcBoxOptions+' input', '.'+cssClassCcBox).off('click change', handlerCcClickInput);
			$('.'+cssClassCcBoxBtnDefault, '.'+cssClassCcBox).off('click', handlerCcClickDefaultSett);
			$('.'+cssClassCcBoxBtnSave, '.'+cssClassCcBox).off('click', handlerCcClickSaveSett);
			
			return this;
		},			
		/**
		 * Bind all events
		 * 
		 * @returns events
		 */
		bind: function(unbind) {
			if('undefined' !== typeof unbind && unbind) {
				unbindEvents();
			}
			
			// attach default logic for the wpca.consent:after event
			$(window).on(cEvents.names.consent_after, handlerConsentAfter);
			// attach default logic for the wpca.decline:after event
			$(window).on(cEvents.names.decline_after, handlerDeclineAfter);		
			// attach default logic for the wpca.reset:after event
			$(window).on(cEvents.names.reset_after, handlerResetAfter);
			
			// do not attach click accept button event if consent method is "Click/Scroll", "Only Click" or "Only Scroll"
			if(hasConsentMethodForBtnAccept()) {
				// attach click event to the cookie accept button
				$('body').on('click', '.'+cssClassBtnAccept, handlerConsentClickAccept);
			}
			// attach click event to the cookie decline button
			$('body').on('click', '.'+cssClassBtnDecline, handlerConsentClickDecline);
			// attach click event to the cookie reset button
			$('body').on('click', '.'+cssClassBtnResetConsent, handlerConsentClickResetConsent);
			// take care of the scroll events 
			addScrollEventListener(handlerScrollWindow, {});
			
			// attach click event for the close X
			if(hasCloseEl) {
				elem.close.on('click', handlerClickClose);
			}
			
			if(consentMethod && !front.accepted() && !front.declined()) {

				// 1 accept button
				// 2 accept button / scroll
				// 3 accept button / click
				// 4 accept button / click scroll
				// 5 click scroll
				// 6 only scroll
				// 7 only click
				
				// only click (NO accept button)
				if(7 === consentMethod ) {
					$(window).on( 'click', handlerConsentClickWindow);
				// only scroll (NO accept button)				
				} else if(6 === consentMethod && !front.accepted()) {
					addScrollEventListener(handlerConsentscrollWindow);			
				// take a click or scroll event as an 'Accept' cookie consent				
				} else if(5 === consentMethod && !front.accepted()) {
					$(window).on( 'click', handlerConsentClickWindow);
					addScrollEventListener(handlerConsentscrollWindow);				
				// take a click or scroll event as an 'Accept' cookie consent				
				} else if(4 === consentMethod && !front.accepted()) {			
					$(window).on( 'click', handlerConsentClickWindow);
					addScrollEventListener(handlerConsentscrollWindow);
				// only click (and accept button)
				} else if(3 === consentMethod && !front.accepted()) {			
					$(window).on( 'click', handlerConsentClickWindow);
				// only scroll (and accept button)
				} else if(2 === consentMethod && !front.accepted()) {
					addScrollEventListener(handlerConsentscrollWindow);
				}			
			}

			// cookie category settings
			$('.'+cssClassCcBoxOptions+' input', '.'+cssClassCcBox).on('click change', handlerCcClickInput);
			$('.'+cssClassCcBoxBtnDefault, '.'+cssClassCcBox).on('click', handlerCcClickDefaultSett);
			$('.'+cssClassCcBoxBtnSave, '.'+cssClassCcBox).on('click', handlerCcClickSaveSett);
			
			// take care of the resize event		
			$( window ).resize( function() {			
				// re-init position params/ CSS on resizing 
				initPosParams(states.barbox);
				initStatus(states.barbox);			
			});
			
			return this;
		}
	};
	
	var obzerver = {
		/**
		 * @var MutationObserver
		 */
		MutationObzerverObj:null,
		ready: false,
		obzervering: false,		
		/**
		 * Click event callback for MutationObserver instance
		 * 
		 * @param {Array.<MutationRecord>} mutationsList
		 */			
		handlerMutationObzerver: function(mutationsList) {
			try {
				var i, mutation, $removed, data = {}, skip = false;
				
			    for(i=0; i<mutationsList.length; i++) {
			    	mutation = mutationsList[i];
			        if('childList' === mutation.type) {
			        	// removing
			        	if(mutation.removedNodes.length) {
				        	$removed = $(mutation.removedNodes[0]);
				        	if($removed.isMarked()) {
				        		data = $removed.data(dataKeyMarkedData);
				        		skip = $removed.data(dataKeySkipBckp);
				        		// only backup in the body
				        		if('body' === data.context && true !== skip) {
				        			// mark as backup
				        			data['backup'] = true;
				        			$removed
				        				.setMarkedDataObj(data)	        				
				        				.insertAfter($(mutation.previousSibling))
				        				.hide();	
				        		}
				        	}
			        	}	        	
			        }
			    }
			} catch(exc) {
				WPCAGLOBAL.log(exc.message, exc);
			}		    
		},
		/**
		 * Init the MutationObserver
		 * 
		 * @returns {this}
		 */
		init: function() {
			if(!this.ready) {
				this.MutationObzerverObj = new MutationObserver(this.handlerMutationObzerver);
				this.ready = true;
			}
			return this;
		},
		/**
		 * Start observing
		 * 
		 * @returns {this}
		 */		
		obzerve: function() {
			if(this.ready) {
				this.MutationObzerverObj.observe(window.document.body, {attributes: true, childList: true, subtree: true});
				this.obzervering = true;	
			}
			return this;
		},
		/**
		 * Disconnect the observer
		 * 
		 * @returns {this}
		 */			
		disconnect: function() {
			if(this.obzervering) {
				this.MutationObzerverObj.disconnect();
				this.obzervering = false;
			}
			return this;
		}			
	};

	// extend
	var extend = {			
		/**
		 * Check if current element is a processing element
		 */
		isProcessingBtn: function() {
			return ($(this).hasClass(classBtn) && $(this).is('[data-'+dataKeyTxt+']') && $(this).is('[data-'+dataKeyTxtProcessing+']'))	
		},
		/**
		 * Check if current element is marked for blocking
		 */		
		isMarked: function() {
			return ($(this).is('[data-'+dataKeyMarked+']') && $(this).is('[data-'+dataKeyMarkedData+']'));	
		},
		/**
		 * Get marked data
		 * 
		 * @param {String} k
		 * 
		 * @returns {Mixed} or null on failure
		 */				
		getMarkedData: function(k) {
			if('undefined' === typeof k) {
				return null;
			}			
			if($(this).isMarked()) {				
				var data = $(this).data(dataKeyMarkedData);
				return ('undefined' !== typeof data[k]) ? data[k] : null;
			} else {
				return null;
			}
		},
		/**
		 * Set marked data object
		 * 
		 * @param {String} data
		 * @param {Boolean} force
		 * 
		 * @returns {Object}
		 */				
		setMarkedDataObj: function(data, force) {
			if('undefined' === typeof data || 'object' !== typeof data) {
				return $(this);
			}
			
			return this.each(function() {
				if($(this).isMarked()) {
					var k, _data = $(this).data(dataKeyMarkedData);
					for(k in data) {
						var v = data[k];
						_data[k] = v;
					}
					$(this)
						.data(dataKeyMarkedData, _data)
						.attr('data-'+dataKeyMarkedData, JSON.stringify(_data));
				} else if(true === force) {
					$(this)
						.attr('data-'+dataKeyMarked, '1')
						.attr('data-'+dataKeyMarkedData, JSON.stringify(data))
						.data(dataKeyMarked, '1')
						.data(dataKeyMarkedData, data);
				}
			});
		},		
		/**
		 * Check a checkbox
		 * 
		 * @param {boolean|number} status
		 * 
		 * @returns {Object} the jQuery current element instance for chaining
		 */
		cbCheck: function(status) {
			if('undefined' === typeof status) {
				status = true;
			}
			if('boolean' !== typeof status) {
				if(0 === parseInt(status)) {
					status = false;
				} else if(1 === parseInt(status)) {
					status = true;
				}
			}
			$(this).prop('checked', status);
			
			// allow chaining
			return $(this);
		},
		/**
		 * Check checkboxes in a cc settings box for given categories
		 * 
		 * @param {boolean} check
		 * @param {Object[]} categories
		 * 
		 * @returns {Object|void} the jQuery current element instance for chaining or void
		 */
		cbCheckForCategory: function(check, categories) {
			if('undefined' === typeof check) {
				return $(this);
			} else if('string' !== typeof categories && !$.isArray(categories)) {
				return $(this);
			} else if(!$(this).hasClass(cssClassCcBox)) {
				return $(this);
			}
			
			if(!$.isArray(categories)) {
				categories = [categories];
			}
			
			for(var i=0; i<categories.length; i++) {
				var cat = categories[i],
					ccBox = $(this),			
					cb = ccBox.find('.'+cssClassCcBoxOptions+' input[data-cat="'+cat+'"]');
			
				if(cb.length) {			
					if(check && !cb.is(':checked')) {
						cb.cbCheck();
					} else if(!check && cb.is(':checked')) {
						cb.cbCheck(0);
					}
				}				
			}
		},
		/**
		 * Hide/show a Button
		 * 
		 * @param {boolean} status
		 * 
		 * @returns {Object} the jQuery current element instance for chaining
		 */
		hideButton: function(status) {
			if('undefined' === typeof status) {
				status = false;
			}
			if('boolean' !== typeof status) {
				if(0 === parseInt(status)) {
					status = false;
				} else if(1 === parseInt(status)) {
					status = true;
				}
			}
			if(status) {
				$(this).addClass(cssClassBtnHide);
			} else {
				$(this).removeClass(cssClassBtnHide);
			}
			
			// allow chaining
			return $(this);			
		},
		/**
		 * Determine if current element has a placholder
		 * 
		 * @returns {Boolean}
		 */	
		hasPlaceholder: function(c) {
			return (0<c && 0<$('#'+cssIdPlaceholderPrefix+c).length);			
		}
	}
	
	/**
	 * Set a cookie
	 * 
	 * Wrapper function for WPCAGLOBAL.setKookie()
	 * 
	 * @param {string} name
	 * @param {string} value
	 * @param {number} days
	 * @param {string} domain
	 * @param {string} path
	 * @param {boolean} secure
	 */		
	function setKookie(name, value, days, domain, path, secure) {
		if('undefined' === typeof name || 'undefined' === typeof value) {
			throw new Error('Could not set cookie. Name or value is undefined.');
		}		
		if('undefined' === typeof days) {
			days = cookieExpire;
		}		
		if('undefined' === typeof domain) {
			domain = wpcaData.cookieDomain;
		}
		if('undefined' === typeof path) {
			path = wpcaData.cookiePath;
		}
		if('undefined' === typeof secure) {
			secure = wpcaData.ssl;
		}		
				
		WPCAGLOBAL.setKookie(name, value, days, domain, path, secure);		
	}
	
	/**
	 * Delete a cookie
	 * 
	 * Wrapper function for WPCAGLOBAL.eraseKookie()
	 * 
	 * @param {string} name
	 * @param {string} domain
	 * @param {string} path
	 */		
	function deleteKookie(name, domain, path) {
		if('undefined' === typeof name) {
			throw new Error('Could not delete cookie. Name is undefined.');
		}
		if('undefined' === typeof domain) {
			domain = wpcaData.cookieDomain;
		}
		if('undefined' === typeof path) {
			path = wpcaData.cookiePath;
		}
		//TODO check if secure param is needed 
				
		WPCAGLOBAL.eraseKookie(name, domain, path);
	}
	
	/**
	 * Read a cookie
	 * 
	 * Wrapper function for WPCAGLOBAL.readKookie()
	 * 
	 * @param {string} name
	 * 
	 * @returns {string|null} the cookie value or null
	 */		
	function readKookie(name) {
		return WPCAGLOBAL.readKookie(name)
	}
	
	/**
	 * Determine if having accept buttons
	 * 
	 * @returns {boolean}
	 */
	function hasBtnAccept() {
		return $('.'+cssClassBtnAccept).length;
	}
	
	/**
	 * Determine if having decline buttons
	 * 
	 * @returns {boolean}
	 */
	function hasBtnDecline() {
		return $('.'+cssClassBtnDecline).length;
	}
	
	/**
	 * Determine if having reset consent buttons
	 * 
	 * @returns {boolean}
	 */
	function hasBtnResetConsent() {
		return $('.'+cssClassBtnResetConsent).length;
	}
	
	/**
	 * Determine if the consent method supports accepting by button
	 * 
	 * @returns {boolean}
	 */
	function hasConsentMethodForBtnAccept() {
		return (-1 === [5,6,7].indexOf(consentMethod));	
	}
	
	/**
	 * Helper function to know if 3 === cookiesBeforeConsent
	 * 
	 * @returns bool
	 */
	function do3rdPartyCookiesBeforeConsent() {
		return (null != wpcaData.cookiesBeforeConsent && 3 === wpcaData.cookiesBeforeConsent);
	}
	
	/**
	 * Helper function to determine if given Cookie Category is allowed
	 * 
	 * @param {string} name
	 * 
	 * @returns {boolean}
	 */	
	function isAllowedCookieCategory(name) {
		return (-1 !== wpcaData.allowedCc.indexOf(name));
	}
	
	/**
	 * Helper function to determine if all required Cookie Category are present
	 * 
	 * @param {Object[]} categories
	 * 
	 * @returns bool
	 */		
	function hasRequiredCookieCategory(categories) {
		var hasArr = [];
		
		for(var i=0; i< wpcaData.requiredCc.length; i++) {
			var rcc = wpcaData.requiredCc[i];
			hasArr.push((-1 !== categories.indexOf(rcc)));
		}
		
		var has = (-1 === hasArr.indexOf(false));
				
		return has;
	}
	
	/**
	 * Get selected (checked) cookie category names based on given input elements
	 * 
	 * @param {Object} inputs
	 * 
	 * @throws Error if finds a cookie category that is not allowed
	 * 
	 * @returns {Object[]} the categories
	 */	
	function getSelectedCookieCategories(inputs) {
		var categories = [];
		if(inputs) {		
			inputs.each(function() {
				var cb = $(this), 
					cat = cb.data(dataKeyCcCat);
				
				// check if it is an allowed category				
				if(!isAllowedCookieCategory(cat)) {
					throw new Error(cat+' is not an allowed cookie category.');
				}
				
				// if is checked, add the category
				if(cb.is(':checked')) {
					categories.push(cat);
				}
			});
		}
		
		return categories;
	}
	
	/**
	 * Check only the cookie category checkboxed that are required
	 * 
	 * @returns void
	 */
	function checkCookieCategoriesRequiredOnly() {
		elem.ccBox.each(function() {
			$(this).cbCheckForCategory(false, wpcaData.allowedCc);			
		});			
		elem.ccBox.each(function() {
			$(this).cbCheckForCategory(true, wpcaData.requiredCc);			
		});	
	}
	
	/**
	 * Check all allowed cookie category checkboxed
	 * 
	 * @returns void
	 */
	function checkCookieCategoriesAllowed() {
		elem.ccBox.each(function() {
			$(this).cbCheckForCategory(true, wpcaData.allowedCc);			
		});
	}
	
	/**
	 * Check only client cookie category checkboxes
	 * 
	 * @returns void
	 */
	function checkCookieCategoriesClient() {
		elem.ccBox.each(function() {
			$(this).cbCheckForCategory(false, wpcaData.allowedCc);			
		});			
		elem.ccBox.each(function() {
			$(this).cbCheckForCategory(true, clientCc);			
		});
	}	
	
	/**
	 * Get the option input elements for the current box based on the clicked button
	 * 
	 * @param {Object} btn
	 * 
	 * @returns {Object} 
	 */
	function getInputsForBtn(btn) {				
		return btn.parents('.'+cssClassCcBox).find('.'+cssClassCcBoxOptions+' input');
	}
	
	/**
	 * Get array with client Cookie Categories from the cookie
	 */
	function getClientCookieCategoriesFromCookie() {
		var cookiesStr = readKookie(wpcaData.cookieNameCc);
		if(null != cookiesStr) {
			return cookiesStr.split(',');
		} else {
			return [];
		}
	}
	
	/**
	 * Update the client Cookie and related params
	 * 
	 * @param {Object[]} categories
	 * 
	 * @returns void
	 */	
	function updateClientCookieAndParams(categories) {
		// if categories is NOT an array, use the required cookies array 
		if(!$.isArray(categories)) {
			categories = [];
		}				
		
		hasClientCc = (0 < categories.length);
		
		// get the current categories as an array
		var ccArrCurr = getClientCookieCategoriesFromCookie();
		// get new categories as an comma separated sting
		var	ccStrNew = (hasClientCc) ? categories.join() : '';
		
		// delete the wpca_cc cookie
		deleteKookie(wpcaData.cookieNameCc);
		// set the wpca_cc cookie
		if(hasClientCc) {
			setKookie(wpcaData.cookieNameCc, ccStrNew);
		}
		
		// set params		
		clientCc = categories;
		hasChangedCat = hasChangedCookieCategories(categories, ccArrCurr);		
	}
	
	/**
	 * Compare to arrays and determine if they are different
	 * 
	 * @param {Object[]} arr1
	 * @param {Object[]} arr2
	 * 
	 * @returns {boolean}
	 */
	function hasChangedCookieCategories(arr1, arr2) {		
		var diff = WPCAGLOBAL.arrDiff(arr1, arr2);
		
		return (0 < diff.length);
	}
	
	/**
	 * Determine if AJAX request will be done
	 * 
	 * @param {Object[]|string} namespaces
	 * 
	 * @returns boolean
	 */
	function hasEventWillDoAjaxRequest(namespaces) {
		if('object' !== typeof namespaces && 'string' !== typeof namespaces) {
			return false;
		}
		
		var	events = $(window).data( 'events' ),
			nsEvents;
		
		if('undefined' === typeof events) {
			return false;
		} else if('undefined' === typeof events[eventBaseNamespace]) {
			return false;
		} else if('string' === typeof namespaces) {
			namespaces = [namespaces];
		}		
		
		nsEvents = events[eventBaseNamespace];
		for(var i=0; i<nsEvents.length; i++) {
			var ev = nsEvents[i],
				evNamespace = ev.namespace||null;			
			for(var ii=0; ii<namespaces.length; ii++) {
				var ns = namespaces[ii],
					evMatch = (evNamespace === ns);
				if(evMatch && null != ev.data[willDoAjaxRequest] && ev.data[willDoAjaxRequest]) {
					return true;
				}
			}
		}
		return false;
	}	
	
	/**
	 * Set CSS classes for the element
	 * 
	 * @param {string} state, the state of the element:  hide, show 
	 * 
	 * @returns void 
	 */	
	function setStateClasses(state) {		
		var _state = ( 'undefined' === typeof state || false == state ) ? statusShow : state;
		
		switch(_state) {			
			case statusHide:
				elem.barbox.removeClass(classShow);
				elem.barbox.addClass(classHide);
				break;
			case statusShow:
			default:
				elem.barbox.removeClass(classHide);
				elem.barbox.addClass(classShow);
				break;
		}		
	}	
	
	/**
	 * Animate the current element
	 * 
	 * Tasks:
	 * 		- Animating the element position
	 * 		- Setting CSS classes
	 * 		- Setting the element position in this.setStateClasses 
	 * 		- set cookie to remember the clients choice
	 * 
	 * @returns void
	 */		
	function animate(status) {
		var animSettings = {};
		
		// return if the requested status is aleady active
		if((front.hasLayout() && status === statusShow) || (!front.hasLayout() && status === statusHide)) {
			return;
		} 
		
		if( 'top' === yPos )
			animSettings = ( front.hasLayout() ) ? {'top':'-'+posIn+'px'} : {'top':posOut+'px'};
		if( 'bottom' === yPos )
			animSettings = ( front.hasLayout() ) ? {'bottom': '-'+posIn+'px'} : {'bottom': posOut+'px'};
		
		elem.barbox.animate(animSettings, animateDuration, function() {
			if(!front.hasLayout()) {
				//element == visible
				setStateClasses(statusShow);				
			} else {
				//element == hided				
				setStateClasses(statusHide);				
			}		
		});			
	}	
	
	/**
	 * Reload a script by removing and adding it again
	 * 
	 * @param {Object|String} src
	 * 
	 * @returns {Object}  
	 */
	function reloadScript(src) {
		var $elem, $cln, next;
		
		if('string' === typeof src) {
			$elem = $('script[src$="' + src + '"]');
		} else if ('object' === typeof src) {
			$elem = src;
		} else {
			return false;
		}
		
		// return if already reloaded
		if(true === $elem.data(dataKeySkipBckp)) {
			return $elem;
		}
		// add data indicating not to backup
		$elem
			.data(dataKeySkipBckp, true)
			.attr('data-'+dataKeySkipBckp, true);
		
		// find the next sibling
		next = $elem[0].nextSibling;
		// clone the current element
		$cln = $($elem[0].cloneNode(true));
		// check if next sibling is the <!--[wpca_mrkd]--> comment
		if(8 == next.nodeType && '[wpca_mrkd]' == next.nodeValue) {
			next.parentNode.removeChild(next);
		}
		try{
			// insert the clone after current elememt
			$elem.after($cln);
		} catch(exc) {}

		// then remove the current element
		$elem.remove();
		// insert the comment for aesthetic
		$cln.after(document.createComment('[wpca_mrkd]'));
		
		// return the cloned (reloaded) element
		return $cln;
	}
	
	/**
	 * Init the blocking/unbloking of content on the current page
	 * 
	 * @returns void 
	 */		
	function initBlocking() {
		var elements,
			elementsMarkedMan = $(elemNameBlock).not('[data-'+dataKeyMarkedAuto+']'),
			placeholderHtmlTmpl = (wpcaData.doPlaceholder) ? $('#'+cssIdPlaceholderTmpl).html() : '',
			c,
			$elem, $elemInner,
			txt,
			type,			
			placeholder,
			data,
			cc,
			block,
			blocked,
			backup,
			hasPlaceholder,
			context,
			src,
			hasSrc,
			placeholderHtml,
			isScript, isIframe, isImg,
			dim,
			$placeholder;
		
		// Check for manually wrapped elements with <wpca-block></wpca-block>
		for(c=0; c<elementsMarkedMan.length; c++) {
			$elem = $(elementsMarkedMan[c]);
			$elemInner = $elem.find('[data-'+dataKeyMarkedAuto+']');
			// properties set in a parent <wpca-block>
			txt = $elem.data('txt')  || '';
			type = $elem.data('type') || '';
			placeholder = (false === $elem.data('placeholder'))?false:true;
			// mark inner marked elements and let them inherit the txt, type and placeholder property
			// but skip the marking for the <wpca-block> itself
			if(0 < $elemInner.length) {
				$elemInner.setMarkedDataObj({'txt':txt, 'type':type, 'placeholder':placeholder});
				continue;
			}
			// a manual wrapped element needs at least the cc
			cc = $elem.data('cc') || '';
			if('' !== cc) {
				data = {};
				data['id'] = 0;
				data['cc'] = cc;
				data['txt'] = txt;
				data['type'] = type;
				data['context'] = 'body';
				data['blocked'] = 1;
				data['placeholder'] = placeholder;
				$elem.setMarkedDataObj(data, true);
			}
		}
		
		// find all marked elements
		elements = $('[data-'+dataKeyMarked+'="1"]');
		
		for(c=0; c<elements.length; c++) {
			$elem = $(elements[c]);
			
			if($elem.isMarked()) {
				data = $elem.data(dataKeyMarkedData);
				cc = $elem.getMarkedData('cc');
				block = front.doBlockCookies(cc);
				blocked = $elem.getMarkedData('blocked');
				placeholder = $elem.getMarkedData('placeholder');
				backup = $elem.getMarkedData('backup');
				hasPlaceholder = $elem.hasPlaceholder(c);
				
				// continue if need to block and already blocked
				if(block && blocked) {
					if(placeholder && hasPlaceholder) {
						continue; 
					}
				}
				
				type = $elem.getMarkedData('type');
				context = $elem.getMarkedData('context');
				src = $elem.getMarkedData('src');
				hasSrc = (null !== src);
				placeholderHtml = '';
				isScript = $elem.is('script');
				isIframe = $elem.is('iframe');
				isImg = $elem.is('img');
				dim = {};
				
				// do general logic when not blocking
				if(!block) {
					data.blocked = 0;
					
					// for elements with a src attribute
					if(hasSrc) {
						$elem.attr('src', src);	
					}
					
					// for scripts
					if(isScript) {
						if($elem[0].hasAttribute('type') && scriptTypeBlocked === $elem.attr('type') ) {
							$elem.attr('type', 'text/javascript');
						}
						if(!hasSrc) {
							// reload the inner content							
							$elem = reloadScript($elem);							
						} else {
							$elem = reloadScript(src);	
						}
					}					
				} else {
					// for scripts
					data.blocked = 1;
					// restore the blocked sources to allow multiple accept/reset/accept actions
					if(isScript) {
						$elem.attr('type', scriptTypeBlocked);
						if(hasSrc && $elem.attr('src') !== wpcaData.blockedAssetsUri.js) {
							$elem.attr('src', wpcaData.blockedAssetsUri.js);	
						}
					} else if(isIframe) {
						if(hasSrc && $elem.attr('src') !== wpcaData.blockedAssetsUri.iframe) {
							$elem.attr('src', wpcaData.blockedAssetsUri.iframe);	
						}
					} else if(isImg) {
						if(hasSrc && $elem.attr('src') !== wpcaData.blockedAssetsUri.img) {
							$elem.attr('src', wpcaData.blockedAssetsUri.img);	
						}						
					}
				}
				
				// do body specific logic
				if('body' === context) {
					$placeholder = '';					
					// when blocking and placeholder is needed
					if(block) {
						if(placeholder && wpcaData.doPlaceholder) {
							// add the current counter as elem id
							data.id = c;
							// show placeholer
							placeholderHtml = placeholderHtmlTmpl
								.replace('%TYPE%', type)
								.replace('%CC%', cc);
							
							// construct a jQuery dom object for the placeholder element
							$placeholder = $(placeholderHtml);
							$placeholder.attr('id', cssIdPlaceholderPrefix+c);
							
							// replace custom text if given
							if('' !== (txt = $elem.getMarkedData('txt'))) {
								$placeholder.find('.'+cssClassPlaceholderTxt).html(txt);
							}
							
							// try to find the width and height of element or inner elements
							if(!WPCAGLOBAL.getWidthHeight($elem, dim) && 0 < $elem.children().length) {
								// find elements 
								$elemInner = $elem.find('[width][height]');
								// if no elements found with width height attributes, grab the first child elem
								if(0 === $elemInner.length) {
									$elemInner = $elem.children(':first');
								// else if multiple inner elements found, grab the first of the matched set
								} else if(1 < $elemInner.length) {
									$elemInner = $elemInner.first();
								}
								WPCAGLOBAL.getWidthHeight($elemInner, dim);
							}
							
							// if there is a real width and height,
							// and the dimenisons are equal or greater then the minimum 
							// let the placeholder inherit
							if(wpcaData.minPlacehoderDim.w <= dim.w && wpcaData.minPlacehoderDim.h <= dim.h) {
								$placeholder.css({'width':dim.w,'height':dim.h});
							}						
							
							// if element is marked as backup,
							// remove the previous sibling
							if(backup) {
								$elem
									.show()
									.prev()
										.hide();
							}
							
							$elem.after($placeholder);
						}						
						// update the data and add the placeholder after the element
						$elem.addClass(cssClassblocked);
					// when not blocking
					} else if(!block) {
						if(backup) {
							$elem
								.hide()
								.prev()
									.show();							
						}						
						// remove the placholder
						$('#'+cssIdPlaceholderPrefix+c).remove();
						$elem.removeClass(cssClassblocked);
					}
				}
				
				// update the elements data
				$elem.setMarkedDataObj(data);
			}
		}
	}
	
	/**
	 * Init position params for the element
	 * 
	 * These are used during animating the element
	 * 
	 * @param {string} status
	 * 
	 * @returns void
	 */		
	function initPosParams(status) {
		yPos = ( elem.barbox.hasClass(cssClassPosTop) ) ? 'top' : 'bottom';
		posIn = elem.barbox.innerHeight();
		
		if('box' === layout) {
			// for the box centered, the parent must be positioned relative
			if( isCenteredBox ) {
				yPos = 'top';
				$('body').css('position', 'relative');
			}
			
			var space = parseInt(elem.barbox.data('space'));
			posIn = (0 < space) ? posIn+space : posIn;
		}
		
		if(hasWPadminbar && statusShow === status && ('top' === yPos || isCenteredBox)) {
			if(600 < $(window).width()) {				
				// calculate the absolute value of posIn
				posIn = Math.abs(elem.wpadminbar.innerHeight()-posIn);
				posOut = elem.wpadminbar.innerHeight();
			} else {				
				// fix gab on top of screen
				$('body').css('position', '');
				
				if(window.pageYOffset <= elem.wpadminbar.innerHeight()) {
					posOut = Math.abs(elem.wpadminbar.innerHeight()-window.pageYOffset);
				} else {
					posOut = 0;
				}
			}
		} else {
			posOut = 0;
		}
	}
	
	
	/**
	 * Init the element position CSS
	 * 
	 * This will position the element on top or bottom of the client window
	 * Also invokes the front.setStateClasses() method to show or hide the element
	 * For the centered box layout, thie top bottom process is skipped
	 * 
	 * @param {string} status
	 * 
	 * @returns void 
	 */		
	function initStatus(status) {
		if(null !== status ) {
			var css = {};
			
			setStateClasses(status);
			
			if(statusShow === status) {
				if(isCenteredBox && 640 < $(window).width()) {
					css = {'top':''};
				} else {
					if( 'top' === yPos ) {
						css = {'top':posOut+'px'};
					}
					if( 'bottom' === yPos ) {
						css = {'bottom': posOut+'px'};
					}
				}
			} else {
				if( 'top' === yPos ) {
					css = {'top':'-'+posIn+'px'};
				}
				if( 'bottom' === yPos ) {
					css = {'bottom': '-'+posIn+'px'};
				}
			}
			
			elem.barbox.css(css);
		}
	}
	
	/**
	 * Init the button status (show/hide)
	 */
	function initButtonStatus() {
		// reset buttons
		if(hasBtnResetConsent()) {
			$('.'+cssClassBtnResetConsent).each(function() {
				var btnReset = $(this),
					btnAccept = btnReset.next('.'+cssClassBtnAccept);
				
				btnAccept.addClass(cssClassBtnHasReset);
				
				if(front.needToReplaceResetByAccept()) {
					btnReset.hideButton(1);	
					btnAccept.hideButton(0);
				} else {
					btnReset.hideButton(0);
					btnAccept.hideButton(1);
				}
			});
		}
		
		// accept buttons
		if(hasBtnAccept() && !front.needButtonAccept()) {
			$('.'+cssClassBtnAccept).not('.'+cssClassBtnHasReset).hideButton(1);
		} else {
			$('.'+cssClassBtnAccept).not('.'+cssClassBtnHasReset).hideButton(0);
		}

		// decline buttons
		if(hasBtnDecline() && !front.needButtonDecline()) {
			$('.'+cssClassBtnDecline).hideButton(1);
		} else {
			$('.'+cssClassBtnDecline).hideButton(0);
		}
	}
	
	function _initStates(args) {
		// init blocking/unblocking
		initBlocking();
		
		// init the status of the buttons
		initButtonStatus();	
		
		// setup the params needed later
		initPosParams(args.states.barbox);
		
		if(args.animate) {
			// animate the lay-out
			animate(args.states.barbox);
		} else {			
			// initialize element position and status
			initStatus(args.states.barbox);
		}
		
		// restore the button default text
		if(args.hasBtn && args.btn.isProcessingBtn()) {
			args.btn.html(args.btn.data(dataKeyTxt));
		}
		
		// check client saved cookie categories
		// but only if is not the 1st visit
		if(!front.isFirstVisit()) {
			checkCookieCategoriesClient();
		}
		
		// the layer
		if(front.doLayer()) {
			// if has an overlay, disable scrolling
			if(statusShow === states.barbox) {
				elem.layer.removeClass(classHide);
				$('html').css('overflow', 'hidden');
			} else {
				elem.layer.addClass(classHide);
				$('html').css('overflow', 'initial');
			}
		}
	}
	
	/**
	 * Initialize states of bar/box (layout) and blocking
	 */
	function initStates(args) {
		if('object' !== typeof args) {
			throw new Error('Could not initialize: no valid args found.');
		}		
		if('undefined' === typeof args.states || 'undefined' === typeof args.states.barbox) {
			throw new Error('Could not initialize the bar/box layout: states.barbox argument not found.');
		}
		if('undefined' === typeof args.animate) {
			args.animate = false;
		}		
		args.hasBtn = ('object' === typeof args.btn);
		args.timeout = parseInt(timeoutProcessing);
		
		if(0 < args.timeout) {
			setTimeout(function() {
				_initStates(args);
			}, args.timeout);
		} else {
			_initStates(args);
		}	
	}
	
	/**
	 * Do 'Accept' logic after client triggered an accept event
	 * 
	 * - Change btn txt with processing notice
	 * 
	 * @param {Object} btn
	 * @param {Object[]} categories
	 * 
	 * @returns {boolean|void}
	 */	
	function doAcceptLogic(btn, categories) {
		if(states.accepting) {
			return;
		} else if(front.acceptedAll()) {
			// fire event: wpca.consent:after
			cEvents.trigger('consent_after', {'categories':categories, 'btn':btn});
			return;
		}
		
		// flag accepting is active
		states.accepting = true;
		
		// check if a btn param has been given
		var hasBtn = (false !== btn && 'undefined' !== typeof btn); 

		if(hasBtn && btn.isProcessingBtn()) {		
			btn
				.css({'height':btn.outerHeight()+'px'})
				.html(btn.data(dataKeyTxtProcessing))
				.focus();			
		}
		
		setKookie(wpcaData.cookieNameConsent, '1');
		
		if('undefined' === typeof categories) {
			categories = wpcaData.allowedCc;
		}		
		
		if($.isArray(categories) && categories.length) {
			// uncomment this will set the wpca_cc cookie with the passed cookies
			// update params that reflect the client cookies				
			updateClientCookieAndParams(categories);
		}
		
		// flag accepting is not active
		states.accepting = false;
		// fire event: wpca.consent:after
		cEvents.trigger('consent_after', {'categories':categories, 'btn':btn});
	}
	
	/**
	 * Do 'Decline' logic after client triggered a decline event
	 * 
	 * - Change btn txt with processing notice
	 * 
	 * @param {Object} btn
	 * @param {Object} categories
	 * 
	 * @returns {boolean|void}
	 */	
	function doDeclineLogic(btn, categories) {
		if(states.declining) {
			return;
		} else if(front.declined()) {
			// fire event: wpca.decline:after
			cEvents.trigger('decline_after', {'categories':categories, 'btn':btn});
			return;
		}
		
		// flag declining is active
		states.declining = true;
		
		// check if a btn param has been given
		var hasBtn = (false !== btn && 'undefined' !== typeof btn);
		
		if(hasBtn && btn.isProcessingBtn()) {
			btn
				.css({'height':btn.outerHeight()+'px'})
				.html(btn.data(dataKeyTxtProcessing))
				.focus();
		}
			
		setKookie(wpcaData.cookieNameConsent, '0');
		
		if('undefined' === typeof categories) {
			categories = wpcaData.requiredCc;
		}		
		
		if($.isArray(categories) && categories.length) {
			// uncomment this will set the wpca_cc cookie with the passed cookies
			// update params that reflect the client cookies				
			updateClientCookieAndParams(categories);
		}		

		checkCookieCategoriesRequiredOnly();
		
		// flag declining is not active
		states.declining = false;		
		// fire event: wpca.decline:after
		cEvents.trigger('decline_after', {'categories':categories, 'btn':btn});
	}
	
	/**
	 * Do 'reset consent' logic after client triggered a reset consent event
	 * 
	 * - Change btn txt with processing notice
	 * 
	 * @param {Object} btn
	 * 
	 * @returns {boolean|void} 
	 */	
	function doResetConsentLogic(btn) {
		if(states.resetting) {
			return;
		}

		// flag resetting is active
		states.resetting = true;
		
		// check if a btn param has been given
		var hasBtn = (false !== btn && 'undefined' !== typeof btn); 

		if(hasBtn && btn.isProcessingBtn()) {
			btn
				.css({'height':btn.outerHeight()+'px'})
				.html(btn.data(dataKeyTxtProcessing))
				.focus();				
		}
			
		// delete the consent and cc cookies
		deleteKookie(wpcaData.cookieNameConsent);
		
		// update params that reflect the client cookies
		updateClientCookieAndParams([]);
		
		// check all cc checkboxes
		checkCookieCategoriesAllowed();
		
		// flag resetting is not active
		states.resetting = false;
		// fire event: wpca.reset:after
		cEvents.trigger('reset_after', {'btn':btn});
	}	

	/**
	 * Do Cookie Category 'save settings' logic
	 * 
	 * - Change btn txt with processing notice
	 * 
	 * @param {Object} btn
	 * @param {Object[]} categories
	 * 
	 * @returns {boolean|void} 
	 */	
	function doCcSaveSettLogic(btn, categories) {
		// if already saved and nothing has changed, return false
		if(states.savingcc || !hasChangedCat) {
			return;
		}
		
		// flag saving cc is active
		states.savingcc = true;
		
		// check if a btn param has been given
		var hasBtn = (false !== btn && 'undefined' !== typeof btn); 

		if(hasBtn && btn.isProcessingBtn()) {
			btn
				.css({'height':btn.outerHeight()+'px'})
				.html(btn.data(dataKeyTxtProcessing))
				.focus();				
		}		
		
		if($.isArray(categories) && categories.length) {
			// uncomment this will set the wpca_cc cookie with the passed cookies
			// update params that reflect the client cookies
			updateClientCookieAndParams(categories);
		}
		
		// flag saving cc is not active
		states.savingcc = false;
		// fire event: wpca.cc:saved
		cEvents.trigger('cc_saved', {'categories':categories});
	}
	
	/**
	 * Wrapper for adding scroll events
	 * 
	 * @param {function} callback
	 * @param {Object} params
	 * 
	 * @returns void
	 */			
	function addScrollEventListener(callback, params) {
		
		// use JavaScript native addEventListener because jQuery scroll() 
		// takes a #my-hash also as a scroll event
		if (window.addEventListener) {
			
			// scrollbar 
			window.addEventListener('scroll', function(e) {
				var _e = window.event || e;				
				callback(_e, params);
			}, false);			
			
			// Chrome, IE, opera
			window.addEventListener('mousewheel', function(e) {
				var _e = window.event || e;				
				callback(_e, params);
			}, false);
			
			// Firefox
			window.addEventListener('DOMMouseScroll', function(e) {
				var _e = window.event || e;				
				callback(_e, params);
			}, false);

			window.addEventListener('touchmove', function(e) {
				var _e = window.event || e;				
				callback(_e, params);
			}, false);						
		}
		else {				
			// IE 8 and older
			window.attachEvent('onmousewheel', function(e) {
				var _e = window.event || e;				
				callback(_e, params);
			});
		}		
	}
	
	/**
	 * Click event callback for wpca.consent:after event
	 * 
	 * @param {Object} e
	 */
	function handlerConsentAfter(e, args) {		
		if('undefined' === typeof args.btn) {
			 args.btn = false;
		}		
		
		// force a page reload if needed
		if(front.needPageReloadAfterConsent()) {
			window.location.reload(true);
		}
		
		// check all cc checkboxes if all cookie categories are accepted
		if(front.acceptedAll()) {
			checkCookieCategoriesAllowed();
		}
		
		states.barbox = statusHide;		
		initStates({
			states:states,
			btn:args.btn,
			animate: true
			});
	}
	
	/**
	 * Click event callback for wpca.decline:after event
	 * 
	 * @param {Object} e
	 */
	function handlerDeclineAfter(e, args) {		
		if('undefined' === typeof args.btn) {
			 args.btn = false;
		}		
		
		states.barbox = statusHide;
		
		initStates({
			states:states,
			btn:args.btn,
			animate: true
			});
	}
	
	/**
	 * Click event callback for wpca.reset:after event
	 * 
	 * @param {Object} e
	 */
	function handlerResetAfter(e, args) {
		if('undefined' === typeof args.btn) {
			 args.btn = false;
		}
		
		states.barbox = statusShow;
		initStates({
			states:states,
			btn:args.btn,
			animate: true
			});
		
		// unbind events and bind again
		events
			.unbind()
			.bind();
	}	
	
	/**
	 * Click event callback for accept button
	 * 
	 * @param {Object} e
	 * 
	 * @returns {boolean} false
	 */		
	function handlerConsentClickAccept(e) {
		try {		
			e.preventDefault();
			
			//TODO consider to pass 2th param categories=wpcaData.allowedCc 
			doAcceptLogic($(this));	
	
			return false;
		} catch(exc) {
			WPCAGLOBAL.log(exc.message, exc);
		}
	}
	
	/**
	 * Click event callback for decline button
	 * 
	 * @param {Object} e
	 * 
	 * @returns {boolean} false
	 */		
	function handlerConsentClickDecline(e) {
		try {		
			e.preventDefault();
			
			// TODO consider to pass (1st btn=false) and 2th param categories=[]			
			doDeclineLogic($(this));
	
			return false;
		} catch(exc) {
			WPCAGLOBAL.log(exc.message, exc);
		}		
	}	
	
	/**
	 * Click event callback for reset consent button
	 * 
	 * @param {Object} e
	 * 
	 * @returns {boolean} false
	 */		
	function handlerConsentClickResetConsent(e) {
		try {		
			e.preventDefault();
			
			doResetConsentLogic($(this));
	
			return false;
		} catch(exc) {
			WPCAGLOBAL.log(exc.message, exc);
		}		
	};
	
	/**
	 * Click event callback when clicked in the current window
	 * 
	 * @param {Object} e
	 * 
	 * @returns {boolean} false
	 */		
	function handlerConsentClickWindow(e) {
		try {	
			if(front.doLayer()) {
				return false;
			}
			
			if(window === e.currentTarget && 'A' === e.target.nodeName && 'click' === e.type) {
				// stop here if consent or decline already given,
				// otherwise a clicked link will not work
				if(front.accepted() || front.declined()) {
					return;
				}
				if(!$(e.target).hasClass(wpcaData.cookiePolicyCssClass) ) {				
					e.preventDefault();
	
					//TODO consider to pass (1st btn=false) and 2th param categories=wpcaData.allowedCc
					doAcceptLogic(false);
					
					// redirect to the clicked link target
					window.location = e.target.href;
				}
			}
		} catch(exc) {
			WPCAGLOBAL.log(exc.message, exc);
		}		
	};
	
	
	/**
	 * Scroll event callback
	 * 
	 * @param {Object} e
	 * @param {Object} params
	 * 
	 * @returns {boolean} false
	 */		
	function handlerConsentscrollWindow(e, params) {
		try {
			if(-1 !== window.location.search.indexOf(wpcaData.queryVarBypass+'=1')) {
				return false;
			}
			
			var scrollTop;
			
			// stop here if consent or decline already given
			if(front.accepted() || front.declined()) {
				return false;
			}		
			
			scrollTop  = $(window).scrollTop();
			
			if(minScrollTop > scrollTop) {
				return false;
			}
	
			if(front.doLayer()) {
				return false;
			}
			
			//TODO consider to pass 2nd param categories=wpcaData.allowedCc
			doAcceptLogic(false);
		} catch(exc) {
			WPCAGLOBAL.log(exc.message, exc);
		}		
	};	

	
	/**
	 * Click Close X event callback
	 * 
	 * @param {Object} e
	 * 
	 * @returns {boolean} false
	 */		
	function handlerClickClose(e) {
		try {
			var accepting = !useXAsDismiss;
			
			// TODO consider to pass 2nd param as 
			// categories=wpcaData.allowedCc for doAcceptLogic()
			// categories=[] for doDeclineLogic()
			if(accepting) {
				doAcceptLogic(false);
			} else {			
				doDeclineLogic(false);	
			}
	
			return false;
		} catch(exc) {
			WPCAGLOBAL.log(exc.message, exc);
		}		
	};	

	
	/**
	 * Click event callback for Cookie Category setting inputs
	 * 
	 * @param {Object} e
	 * 
	 * @returns void
	 */
	function handlerCcClickInput(e) {
		try {			
			var cb = $(this),
				elCcBox = cb.parents('.'+cssClassCcBox),
				elCcBoxId = elCcBox.data(dataKeyCcBoxId),
				cssId = cssClassCcBox+'-'+elCcBoxId,
				cat = cb.data(dataKeyCcCat),
				checked = cb.is(':checked');
			
			didSetCcDefaultSett = false;
			
			// sync Cookie Category setting boxes 
			elem.ccBox.not('#'+cssId).each(function() {				
				$(this).cbCheckForCategory(checked, cat);			
			});			
		} catch(exc) {
			WPCAGLOBAL.log(exc.message, exc);
		}			
	};
	
	
	/**
	 * Click event callback for Cookie Categories default settings button
	 * 
	 * @param {Object} e
	 * 
	 * @returns {boolean} false
	 */		
	function handlerCcClickDefaultSett(e) {
		try {		
			e.preventDefault();

			if(didSetCcDefaultSett) {
				return false;
			}			
			
			var btn = $(this),
				inputs = getInputsForBtn(btn);

			inputs.each(function() {
				var cb = $(this);				
				// if is not checked, check
				if(!cb.is(':checked')) {
					cb.cbCheck().trigger('change', []);
				}
			});
			
			didSetCcDefaultSett = true;	
	
			return false;
		} catch(exc) {
			WPCAGLOBAL.log(exc.message, exc);
		}		
	};
	
	
	/**
	 * Click event callback for Cookie Categories save settings button
	 * 
	 * @param {Object} e
	 * 
	 * @returns {boolean|void}
	 */		
	function handlerCcClickSaveSett(e) {
		try {		
			e.preventDefault();

			var btn = $(this),
				// get the checkbox inputs
				inputs = getInputsForBtn(btn),
				// get the selected categories based on the inputs
				categories = getSelectedCookieCategories(inputs);			
			
			hasChangedCat = hasChangedCookieCategories(categories, clientCc);
			
			// return now if nothing changed
			if(!hasChangedCat) {				
				return;
			}			

			// check if required categories are present
			if(!hasRequiredCookieCategory(categories)) {
				categories = wpcaData.requiredCc;
				WPCAGLOBAL.alert('not all required categories found. Required: '+wpcaData.requiredCc.join());
			}

			// do accept logic if needed by hooking into wpca.cc:saved event
			$(window).one(cEvents.names.cc_saved, function(e, args) {
				doAcceptLogic(btn, args.categories);	
			});
			
			// do save settings logic
			doCcSaveSettLogic(btn, categories);			
	
			return false;
		} catch(exc) {
			WPCAGLOBAL.log(exc.message, exc);
		}		
	};
	
	/**
	 * Handler for scrolling the window
	 * 
	 * Takes care of the WP adminbar if present
	 * WP currently makes the adminbar position:absolute from width <= 600
	 * 
	 * @param {Object} e
	 * 
	 * @returns void
	 */	
	function handlerScrollWindow(e) {		
		if(hasWPadminbar && 'top' === yPos && statusShow === states.barbox && 600 >= $(window).width()) {			
			var y = null;
			
			if(window.pageYOffset < elem.wpadminbar.innerHeight()) {
				y = elem.wpadminbar.innerHeight() - window.pageYOffset;		
			}
			else if(elem.wpadminbar.innerHeight() <= window.pageYOffset) {
				y = 0;			
			}
			
			if(null !== y)
				elem.barbox.css('top', y+'px');
		}		
	};
	
	/**
	 * Determine if client has accepted all cookie categories
	 * 
	 * @returns {bool}
	 */
	front.acceptedAll = function() {
		if(this.accepted() && this.hasClientCookieCategoriesAll()) {
			return true;
		} else if (this.accepted() && !hasClientCc) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Determine if user has given consent
	 * 
	 * @returns {bool}
	 */	
	front.accepted = function() {
		return ('1' === readKookie(wpcaData.cookieNameConsent));
	};
	
	/**
	 * Determine if user has declined
	 * 
	 * @returns {bool}
	 */	
	front.declined = function() {
		return ('0' === readKookie(wpcaData.cookieNameConsent));
	};
	
	/**
	 * Determine if user has a 1st visit
	 * 
	 * @returns {bool}
	 */		
	front.isFirstVisit = function() {
		return (!this.accepted() && !this.declined());
	};
	
	/**
	 * Determine if are bypassing consent
	 * 
	 * @returns {bool}
	 */		
	front.isBypassingConsent = function() {
		return wpcaData.bypassingConsent;
	};
	
	/**
	 * Determine if has resetted consent
	 * 
	 * @returns {bool}
	 */		
	front.hasResettedConsent = function() {
		return wpcaData.resettedConsent;
	};	
	
	/**
	 * Determine if needing the accept button
	 * 
	 * Only need the decline button when:
	 * - not accepted yet
	 * 
	 * @returns {bool}
	 */
	front.needButtonAccept = function() {		
		return (!this.acceptedAll() && hasConsentMethodForBtnAccept());
	};
	
	/**
	 * Determine if needing the decline button
	 * 
	 * Only need the decline button when:
	 * - not already declined AND
	 * - not already accepted AND
	 * - 3rd party cookies are not set before consent
	 * 
	 * @returns {bool}
	 */
	front.needButtonDecline = function() {		
		return (!this.declined() && !this.accepted() && !do3rdPartyCookiesBeforeConsent());
	};
	
	/**
	 * Determine if needing the reset button
	 * 
	 * Only need the reset button when:
	 * - already accepted AND
	 * - not already resetted OR
	 * - NOT accepted yet AND admin settings are confoigured to NOT replace the Reset with Accept button
	 * 
	 * @returns {bool}
	 */	
	front.needButtonReset = function() {		
		return (this.accepted() && !this.hasResettedConsent());
	};
	
	/**
	 * Determine if needing to replace the reset button by an Accept button
	 * 
	 * @returns {bool}
	 */
	front.needToReplaceResetByAccept = function() {
		return (!this.accepted() && wpcaData.replaceResetBtn);
	};
	
	/**
	 * Determine if needing a page reload afer consent
	 * 
	 * @returns {bool}
	 */	
	front.needPageReloadAfterConsent = function() {
		return (wpcaData.reloadAfterConsent);
	};

	/**
	 * Determine if needing the consent layout
	 * 
	 * Only need the layout when:
	 * - is NOT bypassing consent OR
	 * - did NOT declined OR
	 * - is a 1st visit OR
	 * - has resetted consent
	 * 
	 * @returns {bool}
	 */
	front.needConsentLayout = function() {
		if( this.isBypassingConsent() ) {
			return false;
		} else if( this.declined() ) {
			return false;
		} else if( this.isFirstVisit() ) {
			return true;
		} else if ( this.hasResettedConsent() ) {
			return true;
		} else {
			return false;
		}	
	};
	
	/**
	 * Determine if needing a layer
	 * 
	 * @returns {bool}
	 */
	front.doLayer = function() {
		if('' !== wpcaData.cookiePolicyPathRel && wpcaData.cookiePolicyPathRel === window.location.pathname) {
			return false;
		} else {
			return wpcaData.doLayer && 1 === elem.layer.length;
		}		
	};
	
	/**
	 * Get the declined cookie categories
	 * 
	 * @returns {Object[]} the categories
	 */	
	front.getClientCookieCategoriesDeclined = function() {		
		return WPCAGLOBAL.arrDiff(wpcaData.allowedCc, clientCc);	
	};
	
	/**
	 * Determine if client has all allowed categories
	 * 
	 * @returns {bool}
	 */		
	front.hasClientCookieCategoriesAll = function() {	
		if(hasClientCc) {
			var declined = this.getClientCookieCategoriesDeclined();
			
			return ( 0 === declined.length );			
		} else {
			return false;
		}		
	};

	/**
	 * Determine if client has given category
	 * 
	 * @returns {bool}
	 */	
	front.hasClientThisCookieCategory = function(cc) {
		return (hasClientCc && -1 !== clientCc.indexOf(cc));
	};
	
	/**
	 * Determine if the bar or box is visible
	 * 
	 * @returns {boolean}
	 */
	front.hasLayout = function() {
		return elem.barbox.hasClass(classShow);
	}
	
	/**
	 * Determine if cookies should be blocked
	 * 
	 * @returns {bool}
	 */		
	front.doBlockCookies = function(cc) {
		var cc = ('undefined' !== typeof cc) ? cc.trim() : '';
		
		var clientHasAll = this.hasClientCookieCategoriesAll(),
			checkForClientCc = false,			
			clientHasCategory,
			// block cookies by default	
			block = true;		
		
		// Does the client has the passed category set
		if( '' !== cc && isAllowedCookieCategory(cc) ) {
			checkForClientCc = true;
			clientHasCategory = this.hasClientThisCookieCategory(cc);
		}
			
		if( checkForClientCc && clientHasCategory ) {
			block = false;
			
		// and don't block cookies if 
		// client has accepted ALL categories
		} else if( this.accepted() && clientHasAll ) {
			$block = false;
			
		// and don't block cookies if
		// client has accepted and hasn't got categories yet
		// This is the case when the user accepts without saving the settings
		} else if( this.accepted() && !hasClientCc ) {
			block = false;
			
		// and don't block cookies if
		// client has NOT accepted and declined yet (1st visit untill client makes a choice)
		// and ALL (3rd party) cookies are allowed before consent
		} else if( this.isFirstVisit() && do3rdPartyCookiesBeforeConsent() ) {
			block = false;				
		}
	
		return block;
	};
	
	/**
	 * Get the name of an event by index
	 * 
	 * @param {String} idx
	 * 
	 * @returns {String}
	 */
	front.getEventName = function(idx) {
		return cEvents.getName(idx);
	};
	
	/**
	 * Init the element logic when the DOM is ready
	 * 
	 * Tasks:
	 * 		- validating required global params
	 * 		- declaring params
	 * 		- callings methods: initStates(), events.bind()
	 * 
	 * @returns void
	 */
	front.init = function() {
		
		try {		
			if(this.done) {
				return;
			}
			
			var requiredGlobals = ['layout',
			                       'cookiePolicyCssClass',
			                       'cookiePolicyPathRel',
			                       'queryVarBypass',
			                       'cookieNameConsent',
			                       'cookieNameCc', 
			                       'cookiesBeforeConsent', 
			                       'consentMethod',
			                       'doLayer',
			                       'hasClose',
			                       'useXAsDismiss',
			                       'minScrollTop',
			                       'resettedConsent',
			                       'bypassingConsent',
			                       'ssl',
			                       'allowedCc',
			                       'requiredCc',
			                       'replaceResetBtn',
			                       'doPlaceholder',
			                       'blockedAssetsUri',
			                       'reloadAfterConsent',
			                       'minPlacehoderDim'];
			
			if(null == wpcaData) {
				throw new Error('Missing global param: wpcaData');
			}		

			for(var i=0; i<requiredGlobals.length; i++) {
				if(null == wpcaData[requiredGlobals[i]]) {
					throw new Error('Missing global param: '+requiredGlobals[i]);
				}				
			}
			
			// bar of box?
			layout = wpcaData.layout;			
			// init DOM elements
			elem.barbox = $('#wpca-'+layout);		
			elem.wpadminbar = $('#wpadminbar');
			elem.privacyBtns = $('.'+wpcaData.cookiePolicyCssClass); 
			elem.privacyBtn = elem.barbox.find('.'+wpcaData.cookiePolicyCssClass);			
			elem.ccBox = $('.'+cssClassCcBox);			
			elem.layer = $('#'+idLayer);
			elem.close = elem.barbox.find('#'+idCloseX);
			// configs passded by wpcaData
			useXAsDismiss = wpcaData.useXAsDismiss;
			consentMethod = parseInt(wpcaData.consentMethod);
			cookieExpire = (wpcaData.cookieExpire) ? wpcaData.cookieExpire : cookieExpire;
			minScrollTop = (0 <= wpcaData.minScrollTop) ? wpcaData.minScrollTop : minScrollTop;
			// client cookies
			clientCc = getClientCookieCategoriesFromCookie();
			// flags
			isCenteredBox = elem.barbox.hasClass(cssClassCenteredBox);
			hasWPadminbar = (null != elem.wpadminbar[0]) ? true : false;
			hasCloseEl = wpcaData.hasClose;
			hasClientCc = (0 < clientCc.length);
			hasCloseEl = (hasCloseEl && 0 < elem.close.length);
						
			// extra check. if client hasn't accepted and declined yet (1st visit), but Cookie Category cookie is present
			// delete the Cookie Category cookie
			if(this.isFirstVisit() && clientCc.length) {
				deleteKookie(wpcaData.cookieNameCc);
			}
			
			// set the status of the element (show/hide) after the cookie has been read
			states.barbox = (!this.needConsentLayout()) ? statusHide : statusShow; 
			
			// extend the jQuery with methods stored in param "extend"
			$.fn.extend(extend);
			
			// init all layout params (bar/box/buttons and blocking logic
			initStates({
				states:states
				});
			
			// bind events
			events.bind();
			
			// start observing
			obzerver
				.init()
				.obzerve();

			this.done = true;
			
		} catch (exc) {
			WPCAGLOBAL.log(exc.message, exc);
		}
	};	
	
	return front;
	
})(jQuery || {}, WPCAFR || {}); 

jQuery(function($) {
	
	if(WPCAFR.done)
		return;
	
	// call init method when DOM is ready
	WPCAFR.init();	
});