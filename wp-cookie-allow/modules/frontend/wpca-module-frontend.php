<?php
/**
 * Please see wp-cookie-allow.php for more details.
 *
 * @author $Vincent Weber <vincent@webrtistik.nl>$
 */
if( !class_exists( 'WpcaFrontend' ) ) {
	
	/**
	 * WpcaFrontend Class
	 *
	 * Module to handle the front-end lay-outs 
	 *
	 * @author $Vincent Weber <vincent@webrtistik.nl>$
	 * 
	 * @since 2.3.3
	 */	
	class WpcaFrontend extends WpiePluginModule {	

		
		/**
		 * The selected lay-out
		 * 
		 * @since 2.3.3
		 * 
		 * @var string
		 */
		public $layout = 'bar';
		
		
		/**
		 * Flag if overlay is realy needed
		 * 
		 * @since 3.1.2
		 *
		 * @var bool
		 */
		public $doLayer = false; 
		
		
		/**
		 * Flag if closing (X) should be used as a 'dismiss'
		 *
		 * @since 3.1.2
		 *
		 * @var bool
		 */
		public $useXasDismiss = false;
		
		
		/**
		 * Flag if a privacy page is present
		 *
		 * @since 3.0
		 *
		 * @var bool
		 */
		public $hasCookiePolicyPage = false;	
		
		
		/**
		 * Flag if privacy page is a PDF
		 *
		 * @since 3.1
		 * 
		 * @var bool
		 */
		public $cookiePolicyIsPdf = false;
		

		/**
		 * Flag if consent has been given
		 *
		 * @since 3.1.2
		 *
		 * @var bool
		 */
		public $accepted = false;		
		
		
		/**
		 * Flag if user declined
		 *
		 * @since 3.1
		 *
		 * @var bool
		 */
		public $declined = false;
		
		
		/**
		 * Flag if is 1st visit
		 *
		 * @since 3.1.2
		 *
		 * @var bool
		 */
		public $firstVisit = false;
		
		
		/**
		 * Flag if user is bypassing consent
		 *
		 * @since 3.1
		 *
		 * @var bool
		 */
		public $bypassingConsent = false;
		
		/**
		 * Flag if user resetted consent
		 *
		 * @since 3.1
		 *
		 * @var bool
		 */
		public $resettedConsent = false;
		
		
		/**
		 * URL to the cookie policy page
		 * 
		 * since 3.0
		 * 
		 * @var string
		 */
		public $cookiePolicyUrl = '';
		
		/**
		 * Relative URL to the cookie policy page
		 *
		 * since 3.2.3
		 *
		 * @var string
		 */		
		public $cookiePolicyUrlRel = '';
		

		/**
		 * Flag if the bar/box should be displayed on the cookie policy page
		 *
		 * @since 3.2
		 *
		 * @var string
		 */
		public $showBarBoxOnCookiePolicyPage = false;
		
		/**
		 * CSS class for the cookie policy page link
		 * 
		 * since 3.0
		 * 
		 * @var string
		 */
		private $_cookiePolicyCssClass = 'wpca-cookie-policy-link';
		
		
		/**
		 * Privacy page Post ID
		 *
		 * @since 3.0
		 *
		 * @var int
		 */
		private $_cookiePolicyPageId = 0;

		
		/**
		 * Flag to show or not show the bar
		 * 
		 * @since 2.3.3
		 * 
		 * @var bool
		 */
		private $_showBar = false;
		
		
		/**
		 * Flag to show or not show the box
		 *
		 * @since 2.3.3
		 *
		 * @var bool
		 */
		private $_showBox = false;
		
		
		/**
		 * Flag to show or not show the box or bar
		 *
		 * @since 3.1
		 *
		 * @var bool
		 */
		private $_showBarBox = false;		
		
		
		/**
		 * The HTML for the layout (bar or box) 
		 * 
		 * @since 2.3.3
		 * 
		 * @var string
		 */
		private $_html = '';
				
		
		/**
		 * Roor URL for the templates folder
		 *
		 * @since 3.0.2
		 *
		 * @var string
		 */
		private $_templateRootUri = '';		
		
		
		/**
		 * Cookie path for settings cookies
		 * 
		 * @since 3.1
		 * 
		 * @var string
		 */
		private $_cookiePath = '';
		
		/**
		 * Cookie domain for settings cookies
		 * 
		 * @since 3.1
		 * 
		 * @var string
		 */
		private $_cookieDomain = '';
		
		/**
		 * Flaf if no cache headers should be added
		 * 
		 * @since 3.2.3
		 * 
		 * @var string
		 */
		private $_noCacheHeaders = false;

		
		/**
		 * Template file name for the bar
		 * 
		 * @since 2.3.3
		 * 
		 * @var string
		 */
		const TEMPL_FILE_NAME_BAR = 'wpca-frontend-bar.php';
		
		
		/**
		 * Template file name for the box
		 *
		 * @since 2.3.3
		 *
		 * @var string
		 */
		const TEMPL_FILE_NAME_BOX = 'wpca-frontend-box.php';
		

		/**
		 * Template file name for the closing X SVG
		 *
		 * @since 3.0.2
		 *
		 * @var string
		 */
		const TEMPL_FILE_NAME_CLOSE_X = 'wpca-frontend-close-x.php';		
		
		
		/**
		 * Template file name for the Accept button
		 *
		 * @since 2.3.3
		 *
		 * @var string
		 */
		const TEMPL_FILE_NAME_BTN_ACCEPT = 'wpca-frontend-btn-accept.php';
		
		
		/**
		 * Template file name for the Decline button
		 *
		 * @since 3.1
		 *
		 * @var string
		 */
		const TEMPL_FILE_NAME_BTN_DECLINE = 'wpca-frontend-btn-decline.php';
		
		
		/**
		 * Template file name for the reset consent button
		 *
		 * @since 3.1
		 *
		 * @var string
		 */
		const TEMPL_FILE_NAME_BTN_RESET_CONSENT = 'wpca-frontend-btn-reset-consent.php';		
				
		
		/**
		 * Template file name for the cookie policy page link
		 *
		 * @since 2.3.3
		 *
		 * @var string
		 */
		const TEMPL_FILE_NAME_COOKIE_POLICY_LINK = 'wpca-frontend-link-cookie-policy.php';
		
		
		/**
		 * File name for the frontend JavaScript
		 * 
		 * @since 2.3.3
		 * 
		 * @var string
		 */
		const FILE_NAME_JS = 'wpca-frontend';
		
		
		/**
		 * File name for the frontend CSS
		 * 
		 * @since 2.3.3
		 * 
		 * @var string
		 */
		const FILE_NAME_CSS = 'wpca-frontend';		
		
		/**
		 * The query var param name for bypassing the cookie consent process
		 * 
		 * @since 2.3.3
		 * 
		 * @var string
		 */
		const QUERY_VAR_BYPASS_COOKIE_CONSENT = 'wpca_bypass_consent';
		
		/**
		 * The query var param name for resetting the cookie consent
		 * 
		 * @since 3.1
		 * 
		 * @var string
		 */
		const QUERY_VAR_RESET_COOKIE_CONSENT = 'wpca_reset_consent';
		
		/**
		 * The query var param name for indicating to use the no cache headers
		 * 
		 * @since 3.2.3
		 * 
		 * @var string
		 */
		const QUERY_VAR_NO_CACHE_HEADERS = 'wpca_no_cache_headers';
		
		
		/**
		 * Constructor
		 * 
		 * @since 3.0.1
		 * 
		 * @acces public
		 */
		public function __construct() {}
		
		
		/* (non-PHPdoc)
		 * @see WpiePluginModule::start()
		 */
		public function start()	
		{
			$this->_templateRootUri = $this->globals->get( 'moduleUri' ) . '/'. parent::getIndex() .'/templates/';
		}
		
		
		/**
		 * Render the Templates
		 *
		 * @access public
		 * 
		 * @uses WpieTemplate
		 * 
		 * @since 2.3.3
		 */
		public function render()
		{
			do_action( 'wpca_before_render_'.$this->layout );
				
			$moduleCore = WpieCookieAllow::getModule( 'core' );
			$showBtnAccept = !$moduleCore->forceHideBtnAccept();
			$showCloseX = $this->settingsProcessor->getSetting( 'general' )->get( 'general_show_x' );
			$showBtnDecline = $this->settingsProcessor->getSetting( 'general' )->get( 'general_show_btn_decline' ); 
			$text = wpautop( $this->settingsProcessor->getSetting( 'content' )->get( 'content_text' ) );			
			$shadow = $this->settingsProcessor->getSetting( 'style' )->get( 'style_box_shadow' );

			// do or not do the layer
			$doLayer = $this->doLayer;
			
			// force hiding the decline button if 3rd party cookies are set before consent
			if( $showBtnDecline && $moduleCore->do3rdPartyCookiesBeforeConsent() ) {
				$showBtnDecline = false;
			} 
			
			// show or hide Accept button
			$showBtns =  ( $showBtnDecline || $showBtnAccept );	
			
			$linkCookiePolicy = '';
			$urlCloseSvg = '';
			
			if( $this->hasCookiePolicyPage ) {
				$linkCookiePolicy = $this->renderLinkCookiePolicy();
			}
			
			// accept all shortcodes
			$text = do_shortcode( $text );
						
			switch ( $this->layout ) 
			{
				case 'box':
					$template = $this->_renderBox();
					$this->_showBox = true;
					break;
				case 'bar':										
				default:
					$template = $this->_renderBar();
					$this->_showBar = true;
					break;
			}
			
			// the bar or box is showing
			$this->_showBarBox = true;
			
			// retreive the css class string
			$cssClass = $template->getVar( 'css_class' );
			
			// add a css class indicating a that a shadow should be added
			if( $shadow ) {
				$cssClass .= ' wpca-shadow';
			}
			
			if( $showCloseX ) {
				$fill = $this->settingsProcessor->getSetting( 'style' )->get( 'style_close_color' );
				if( 0 === strpos( $fill, '#' ) ) {
					$fill = substr( $fill, 1 , strlen( $fill ) );					
					$urlCloseSvg = sprintf( '%s%s?c=%s', $this->_templateRootUri, self::TEMPL_FILE_NAME_CLOSE_X, $fill );
					$cssClass .= ' wpca-close-x';
				}					
			}		
			
			if( $showBtnDecline ) {
				$cssClass .= ' wpca-has-decline';
			}
			
			// update the css class string
			$template->setVar( 'css_class', $cssClass );
			
			// setup Template vars
			$template->setVar( 'do_layer', $doLayer );
			$template->setVar( 'content_text', $text );
			$template->setVar( 'show_btns', $showBtns );
			$template->setVar( 'show_btn_decline', $showBtnDecline );
			$template->setVar( 'show_btn_accept', $showBtnAccept );
			$template->setVar( 'show_close_x', $showCloseX );			
			$template->setVar( 'close_url', $urlCloseSvg );
			// set Template tags(s)
			$template->setTag( 'WPCA_PRIVACY_LINK', $linkCookiePolicy );
			// render the buttons
			$template->setVar( 'btn_accept', $this->renderBtnAccept( '', '', false ) );
			$template->setVar( 'btn_decline', $this->renderBtnDecline( '', '', false ) );
			
			// render
			$this->_html = $template->render( false, true );
		}		
		
		
		/**
		 * Render the privacy button / link
		 *
		 * @access public
		 *
		 * @param	string $txt
		 * @param bool $cache 
		 * @param	bool $echo
		 *
		 * @uses WpieTemplate
		 *
		 * @since 2.3.3
		 *
		 * @return the rendered html if $echo is bool false
		 */
		public function renderLinkCookiePolicy( $txt = '', $cache = true, $echo = false )
		{
			static $cached = array();

			$cacheKey = ( '' === $txt ) ? '_' : md5($txt);						
			if( !empty( $cached ) && isset($cached[$cacheKey] ) ) {
				if( $echo ) {
					echo $cached[$cacheKey];
				} else {
					return $cached[$cacheKey];
				}
			}
			
			if( '' === $txt ) {
				$txt = $this->settingsProcessor->getSetting( 'content' )->get( 'content_cookie_policy_link_text' );
			}
			
			$target = $this->settingsProcessor->getSetting( 'content' )->get( 'content_cookie_policy_page_target' );
												
			$link = new WpieTemplate( self::TEMPL_FILE_NAME_COOKIE_POLICY_LINK, dirname(__FILE__) . '/templates', self::TEMPL_FILE_NAME_COOKIE_POLICY_LINK );
			$link->setVar( 'css_class' , $this->_cookiePolicyCssClass );			
			$link->setVar( 'cookie_policy_link_txt', $txt );
			$link->setVar( 'cookie_policy_page_url', $this->cookiePolicyUrl );
			$link->setVar( 'target', $target );			
			$html = $link->render( false, true );
						
			if( $cache ) {
				$cached[$cacheKey] = $html;
			}
			if( $echo ) {
				echo $html;
			}	else {
				return $html;
			}			
		}		
		
		
		/**
		 * Render the Accept button
		 *
		 * @access 	public
		 * 
		 * @param	string $txt
		 * @param 	string $txtProcessing
		 * @param 	bool $cache 
		 * @param	bool $echo
		 * 
		 * @uses 	WpieTemplate
		 * 
		 * @since 	2.3.3
		 * @since 	2.3.3 added static cache 
		 * 
		 * @return 	the rendered html if $echo is bool false
		 */
		public function renderBtnAccept( $txt = '', $txtProcessing = '', $cache = true, $echo = false ) 
		{		
			static $cached = array();
					
			$cacheKey = ( '' === $txt && '' === $txtProcessing) ? '_' : md5($txt.$txtProcessing);
			if( !empty( $cached ) && isset($cached[$cacheKey] ) ) {
				if( $echo ) {
					echo $cached[$cacheKey];
				} else {
					return $cached[$cacheKey];
				}
			}
			
			if( '' === $txt ) {
				$txt = $this->settingsProcessor->getSetting('content')->get( 'content_button_accept_txt' );
			}			
			if( '' === $txtProcessing ) {
				$txtProcessing = $this->settingsProcessor->getSetting('content')->get( 'content_button_accept_txt_accepting' );
			}			
			
			$btn = new WpieTemplate( self::TEMPL_FILE_NAME_BTN_ACCEPT, dirname(__FILE__) . '/templates', self::TEMPL_FILE_NAME_BTN_ACCEPT );			
			$btn->setVar( 'txt', $txt );
			$btn->setVar( 'txt_processing', $txtProcessing );	
			$html = $btn->render( false, true );
			
			if( $cache ) {
				$cached[$cacheKey] = $html;
			}
			if( $echo ) {
				echo $html;
			}	else {
				return $html;
			}					
		}
		
		
		/**
		 * Render the Decline button
		 *
		 * @access public
		 *
		 * @param	string $txt
		 * @param 	string $txtProcessing
		 * @param 	bool $cache 
		 * @param	bool $echo
		 *
		 * @uses WpieTemplate
		 *
		 * @since 3.1
		 *
		 * @return the rendered html if $echo is bool false
		 */
		public function renderBtnDecline( $txt = '', $txtProcessing = '', $cache = true, $echo = false )
		{
			static $cached = array();

			$cacheKey = ( '' === $txt && '' === $txtProcessing) ? '_' : md5($txt.$txtProcessing);
			if( !empty( $cached ) && isset($cached[$cacheKey] ) ) {
				if( $echo ) {
					echo $cached[$cacheKey];
				} else {
					return $cached[$cacheKey];
				}
			}
				
			if( '' === $txt ) {
				$txt = $this->settingsProcessor->getSetting('content')->get( 'content_button_decline_txt' );
			}				
			if( '' === $txtProcessing ) {
				$txtProcessing = $this->settingsProcessor->getSetting('content')->get( 'content_button_decline_txt_declining' );
			}

			$btn = new WpieTemplate( self::TEMPL_FILE_NAME_BTN_DECLINE, dirname(__FILE__) . '/templates', self::TEMPL_FILE_NAME_BTN_DECLINE );
			$btn->setVar( 'txt', $txt );
			$btn->setVar( 'txt_processing', $txtProcessing );	
			$html = $btn->render( false, true );	

			if( $cache ) {
				$cached[$cacheKey] = $html;
			}
			if( $echo ) {
				echo $html;
			}	else {
				return $html;
			}
		}
		

		/**
		 * Render the Reset consent button
		 *
		 * @access public
		 *
		 * @param	string $txt
		 * @param 	string $txtProcessing
		 * @param 	bool $cache 
		 * @param	bool $echo
		 *
		 * @uses WpieTemplate
		 *
		 * @since 3.1
		 *
		 * @return the rendered html if $echo is bool false
		 */
		public function renderBtnResetConsent( $txt = '', $txtProcessing = '', $cache = true, $echo = false ) 
		{
			static $cached = array();
			
			$cacheKey = ( '' === $txt && '' === $txtProcessing) ? '_' : md5($txt.$txtProcessing);
			if( !empty( $cached ) && isset($cached[$cacheKey] ) ) {
				if( $echo ) {
					echo $cached[$cacheKey];
				} else {
					return $cached[$cacheKey];
				}
			}
			
			if( '' === $txt ) {
				$txt = $this->settingsProcessor->getSetting('content')->get( 'content_button_reset_consent_txt' );
			}
			if( '' === $txtProcessing ) {
				$txtProcessing = $this->settingsProcessor->getSetting('content')->get( 'content_button_reset_consent_txt_resetting' );
			}			
			
			$btn = new WpieTemplate( self::TEMPL_FILE_NAME_BTN_RESET_CONSENT, dirname(__FILE__) . '/templates', self::TEMPL_FILE_NAME_BTN_RESET_CONSENT);
			$btn->setVar( 'txt', $txt );
			$btn->setVar( 'txt_processing', $txtProcessing );
			$html = $btn->render( false, true ).$this->renderBtnAccept();
			
			if( $cache ) {
				$cached[$cacheKey] = $html;
			}
			if( $echo ) {
				echo $html;
			}	else {
				return $html;
			}						
		}
				
		
		/**
		 * Callback for the wpca_print_scripts_frontend hook
		 * 
		 * Print scripts and CSS for the bar and box.
		 * 
		 * @access public
		 * 
		 * @since 2.3.3
		 */
		public function printScripts()
		{
			if( $this->doLayer ) {
				$style_overlay_color                      = $this->settingsProcessor->getSetting('style')->get( 'style_overlay_color' );
				$style_overlay_opacity                    = $this->settingsProcessor->getSetting('style')->get( 'style_overlay_opacity' );
			}
			$style_button_accept_color                    = $this->settingsProcessor->getSetting('style')->get( 'style_button_accept_color' );
			$style_button_accept_text_color               = $this->settingsProcessor->getSetting('style')->get( 'style_button_accept_text_color' );
			$style_button_accept_color_hover              = $this->settingsProcessor->getSetting('style')->get( 'style_button_accept_color_hover' );
			$style_button_accept_text_color_hover         = $this->settingsProcessor->getSetting('style')->get( 'style_button_accept_text_color_hover' );			
			$style_button_decline_color                   = $this->settingsProcessor->getSetting('style')->get( 'style_button_decline_color' );
			$style_button_decline_text_color              = $this->settingsProcessor->getSetting('style')->get( 'style_button_decline_text_color' );
			$style_button_decline_color_hover             = $this->settingsProcessor->getSetting('style')->get( 'style_button_decline_color_hover' );
			$style_button_decline_text_color_hover        = $this->settingsProcessor->getSetting('style')->get( 'style_button_decline_text_color_hover' );
			$style_button_reset_consent_color             = $this->settingsProcessor->getSetting('style')->get( 'style_button_reset_consent_color' );
			$style_button_reset_consent_text_color        = $this->settingsProcessor->getSetting('style')->get( 'style_button_reset_consent_text_color' );
			$style_button_reset_consent_color_hover       = $this->settingsProcessor->getSetting('style')->get( 'style_button_reset_consent_color_hover' );
			$style_button_reset_consent_text_color_hover  = $this->settingsProcessor->getSetting('style')->get( 'style_button_reset_consent_text_color_hover' );			
			$style_css                                    = $this->settingsProcessor->getSetting('style')->get( 'style_css' );
			
			// cookie category settings
			$style_cc_border_radius                        = $this->settingsProcessor->getSetting('style')->get( 'style_cc_border_radius' );
			$style_cc_bgd_color                            = $this->settingsProcessor->getSetting('style')->get( 'style_cc_bgd_color' );
			$style_cc_text_color                           = $this->settingsProcessor->getSetting('style')->get( 'style_cc_text_color' );
			$style_cc_link_color                           = $this->settingsProcessor->getSetting('style')->get( 'style_cc_link_color' );
			$style_cc_link_color_hover                     = $this->settingsProcessor->getSetting('style')->get( 'style_cc_link_color_hover' );
			$style_cc_button_default_sett_color            = $this->settingsProcessor->getSetting('style')->get( 'style_cc_button_default_sett_color' );
			$style_cc_button_default_sett_color_hover      = $this->settingsProcessor->getSetting('style')->get( 'style_cc_button_default_sett_color_hover' );
			$style_cc_button_default_sett_text_color       = $this->settingsProcessor->getSetting('style')->get( 'style_cc_button_default_sett_text_color' );
			$style_cc_button_default_sett_text_color_hover = $this->settingsProcessor->getSetting('style')->get( 'style_cc_button_default_sett_text_color_hover' );
			$style_cc_button_save_color                    = $this->settingsProcessor->getSetting('style')->get( 'style_cc_button_save_color' );
			$style_cc_button_save_color_hover              = $this->settingsProcessor->getSetting('style')->get( 'style_cc_button_save_color_hover' );
			$style_cc_button_save_text_color               = $this->settingsProcessor->getSetting('style')->get( 'style_cc_button_save_text_color' );
			$style_cc_button_save_text_color_hover         = $this->settingsProcessor->getSetting('style')->get( 'style_cc_button_save_text_color_hover' );
			// placeholder
			$style_placeholder_min_w                     = $this->settingsProcessor->getSetting('style')->get( 'style_placeholder_min_w' );
			$style_placeholder_min_h                     = $this->settingsProcessor->getSetting('style')->get( 'style_placeholder_min_h' );
			
			// convert hexadecimal colors to rgb to prevent an opacity in the buttons
			$style_button_accept_color_rgb                 = WpieMiscHelper::hex2rgb( $style_button_accept_color );
			$style_button_accept_color_hover_rgb           = WpieMiscHelper::hex2rgb( $style_button_accept_color_hover );
			$style_button_decline_color_rgb                = WpieMiscHelper::hex2rgb( $style_button_decline_color );
			$style_button_decline_color_hover_rgb          = WpieMiscHelper::hex2rgb( $style_button_decline_color_hover );
			$style_button_reset_consent_color_rgb          = WpieMiscHelper::hex2rgb( $style_button_reset_consent_color );
			$style_button_reset_consent_hover_rgb          = WpieMiscHelper::hex2rgb( $style_button_reset_consent_color_hover );			
			$style_cc_button_default_sett_color_rgb        = WpieMiscHelper::hex2rgb( $style_cc_button_default_sett_color );
			$style_cc_button_default_sett_color_hover_rgb  = WpieMiscHelper::hex2rgb( $style_cc_button_default_sett_color_hover );			
			$style_cc_button_save_color_rgb                = WpieMiscHelper::hex2rgb( $style_cc_button_save_color );
			$style_cc_button_save_color_hover_rgb          = WpieMiscHelper::hex2rgb( $style_cc_button_save_color_hover );
			?>			
<?php echo PHP_EOL ?><!-- WeePie Cookie Allow Plugin - V<?php echo WpieCookieAllow::VERSION ?> - https://www.weepie-plugins.com/cookie-allow/ --><?php echo PHP_EOL ?>
<style type="text/css">
.wpca-btn-accept{background:rgba(<?php echo $style_button_accept_color_rgb['r'] ?>,<?php echo $style_button_accept_color_rgb['g'] ?>,<?php echo $style_button_accept_color_rgb['b'] ?>,1);color:<?php echo $style_button_accept_text_color ?>}
.wpca-btn-accept:hover,.wpca-btn-accept:focus,.wpca-btn-accept:active{background:rgba(<?php echo $style_button_accept_color_hover_rgb['r'] ?>,<?php echo $style_button_accept_color_hover_rgb['g'] ?>,<?php echo $style_button_accept_color_hover_rgb['b'] ?>,1);color:<?php echo $style_button_accept_text_color_hover ?>}
.wpca-btn-decline{background:rgba(<?php echo $style_button_decline_color_rgb['r'] ?>,<?php echo $style_button_decline_color_rgb['g'] ?>,<?php echo $style_button_decline_color_rgb['b'] ?>, 1); color:<?php echo $style_button_decline_text_color ?>}
.wpca-btn-decline:hover,.wpca-btn-decline:focus,.wpca-btn-decline:active{background:rgba(<?php echo $style_button_decline_color_hover_rgb['r'] ?>,<?php echo $style_button_decline_color_hover_rgb['g'] ?>,<?php echo $style_button_decline_color_hover_rgb['b'] ?>,1);color:<?php echo $style_button_decline_text_color_hover ?>}
.wpca-btn-reset-consent{background:rgba(<?php echo $style_button_reset_consent_color_rgb['r'] ?>,<?php echo $style_button_reset_consent_color_rgb['g'] ?>,<?php echo $style_button_reset_consent_color_rgb['b'] ?>,1);color:<?php echo $style_button_reset_consent_text_color ?>}
.wpca-btn-reset-consent:hover,.wpca-btn-reset-consent:focus,.wpca-btn-reset-consent:active{background:rgba(<?php echo $style_button_reset_consent_hover_rgb['r'] ?>,<?php echo $style_button_reset_consent_hover_rgb['g'] ?>,<?php echo $style_button_reset_consent_hover_rgb['b'] ?>,1);color:<?php echo $style_button_reset_consent_text_color_hover ?>}
/* Cookie Category settings */
.wpca-cc-sett-box{background-color:<?php echo $style_cc_bgd_color?>;border-radius:<?php echo $style_cc_border_radius ?>px;padding:15px;color:<?php echo $style_cc_text_color ?>}
.wpca-cc-sett-box *{color:<?php echo $style_cc_text_color ?>}
.wpca-cc-sett-box a{color:<?php echo $style_cc_link_color ?>}
.wpca-cc-sett-box a{color:<?php echo $style_cc_link_color_hover ?>}
.wpca-cc-sett-box .wpca-btn-cc-default-sett{background:rgba(<?php echo $style_cc_button_default_sett_color_rgb['r'] ?>,<?php echo $style_cc_button_default_sett_color_rgb['g'] ?>,<?php echo $style_cc_button_default_sett_color_rgb['b'] ?>,1);color:<?php echo $style_cc_button_default_sett_text_color ?>}
.wpca-cc-sett-box .wpca-btn-cc-default-sett:hover,.wpca-cc-sett-box .wpca-btn-cc-default-sett:focus,.wpca-cc-sett-box .wpca-btn-cc-default-sett:active {background:rgba(<?php echo $style_cc_button_default_sett_color_hover_rgb['r'] ?>,<?php echo $style_cc_button_default_sett_color_hover_rgb['g'] ?>,<?php echo $style_cc_button_default_sett_color_hover_rgb['b'] ?>,1);color:<?php echo $style_cc_button_default_sett_text_color_hover ?>}
.wpca-cc-sett-box .wpca-btn-cc-save-sett{background:rgba(<?php echo $style_cc_button_save_color_rgb['r'] ?>,<?php echo $style_cc_button_save_color_rgb['g'] ?>,<?php echo $style_cc_button_save_color_rgb['b'] ?>,1);color:<?php echo $style_cc_button_save_text_color ?>}
.wpca-cc-sett-box .wpca-btn-cc-save-sett:hover,.wpca-cc-sett-box .wpca-btn-cc-save-sett:focus,.wpca-cc-sett-box .wpca-btn-cc-save-sett:active{background:rgba(<?php echo $style_cc_button_save_color_hover_rgb['r'] ?>,<?php echo $style_cc_button_save_color_hover_rgb['g'] ?>,<?php echo $style_cc_button_save_color_hover_rgb['b'] ?>,1);color:<?php echo $style_cc_button_save_text_color_hover ?>}
<?php
			if( $this->_showBox ) {
				$style_box_width     = $this->settingsProcessor->getSetting('style')->get( 'style_box_width' );
				$style_box_height    = $this->settingsProcessor->getSetting('style')->get( 'style_box_height' );
				$style_border_radius = $this->settingsProcessor->getSetting('style')->get( 'style_box_border_radius' );
				$style_window_space  = $this->settingsProcessor->getSetting('style')->get( 'style_box_window_space' );
				$style_box_btn_align = $this->settingsProcessor->getSetting('style')->get( 'style_box_btn_align' );
				
			}
			if( $this->_showBarBox) {
				$style_color            = $this->settingsProcessor->getSetting('style')->get( 'style_color' );					
				$style_opacity          = $this->settingsProcessor->getSetting('style')->get( 'style_opacity' );
				$style_text_color       = $this->settingsProcessor->getSetting('style')->get( 'style_text_color' );
				$style_link_color       = $this->settingsProcessor->getSetting('style')->get( 'style_link_color' );
				$style_link_color_hover = $this->settingsProcessor->getSetting('style')->get( 'style_link_color_hover' );			
				// convert hexadecimal colors to rgb
				$style_color_rgb        = WpieMiscHelper::hex2rgb( $style_color );
?>
<?php if( $this->_showBox ): ?>
#wpca-<?php echo $this->layout ?>{width:<?php echo $style_box_width ?>%;min-height:<?php echo $style_box_height?>%;border-radius:<?php echo $style_border_radius ?>px;margin:<?php echo $style_window_space ?>px;}
<?php endif ?>
#wpca-<?php echo $this->layout ?>{ background:rgba(<?php echo $style_color_rgb['r'] ?>,<?php echo $style_color_rgb['g'] ?>,<?php echo $style_color_rgb['b'] ?>,<?php echo $style_opacity ?>);filter: alpha(opacity=<?php echo round(100*$style_opacity)?>);}
#wpca-<?php echo $this->layout ?>-content, #wpca-<?php echo $this->layout ?>-content > p{color:<?php echo $style_text_color ?>}
#wpca-<?php echo $this->layout ?> a{color:<?php echo $style_link_color ?>}
#wpca-<?php echo $this->layout ?> a:hover{color:<?php echo $style_link_color_hover ?>}
<?php if( $this->doLayer ): ?>#wpca-trans-layer{background:<?php echo $style_overlay_color ?>;opacity:<?php echo $style_overlay_opacity ?>;filter:alpha(opacity=<?php echo round(100*$style_overlay_opacity) ?>);}<?php endif ?>
.wpca-replacement-elem{min-width:<?php echo $style_placeholder_min_w ?>px;min-height:<?php echo $style_placeholder_min_h ?>px}			
<?php } if( !empty( $style_css ) ): ?> 
/* WeePie Cookie Allow custom styles for the frontend */
<?php echo esc_textarea( $style_css ) . "\n"?>
/* End custom style */
<?php endif; ?>
</style><?php echo PHP_EOL ?><!-- //END WeePie Cookie Allow Plugin --><?php echo PHP_EOL ?>
<?php
		}
		
		
		/**
		 * Callback for the wp_footer hook
		 * 
		 * Add the bar HTML to the current page
		 * 
		 * @access public
		 * 
		 * @since 2.3.3
		 */
		public function addHtml() 
		{
			if( $this->_showBarBox )
			{
				echo $this->_html;
			}
		}
				
		
		/**
		 * Callback for the wpca_scripts_frontend hook
		 *
		 * Enqueue jQuery scripts
		 *
		 * @access public
		 *
		 * @param WP_Scripts $wp_scripts
		 * @param array $excludes items to exclude
		 * @param bool $isScriptDebug
		 *
		 * @uses wp_enqueue_script()
		 *
		 * @since 2.3.3
		 */
		public function setScripts( $wp_scripts, $excludes, $isScriptDebug )
		{
			$ext = ( $isScriptDebug ) ? '.js' : '.min.js';
			wp_enqueue_script( 'jquery' );
			wp_enqueue_script( 'wpca-frontend', $this->globals->get('moduleUri') . '/'. parent::getIndex() .'/js/' . self::FILE_NAME_JS . $ext, array( 'jquery' ), WpieCookieAllow::VERSION, true );
		}
		
		
		/**
		 * Callback for the wpca_styles_frontend hook
		 *
		 * Enqueue styles
		 *
		 * @access public
		 *
		 * @param WP_Styles $wp_styles
		 * @param array $excludes items to exclude
		 * @param bool $isModeDev
		 *
		 * @uses wp_enqueue_style()
		 *
		 * @since 2.3.3
		 */
		public function setStyles( $wp_styles, $excludes, $isModeDev )
		{
			$ext = ( $isModeDev ) ? '.css' : '.min.css';
			wp_enqueue_style( 'wpca-frontend', $this->globals->get('moduleUri') . '/'. parent::getIndex() .'/css/' . self::FILE_NAME_CSS . $ext, array(), WpieCookieAllow::VERSION );
	
			/**
			 * Let other modules add styles
			 *
			 * @since 2.3.3
			 */
			do_action( 'wpca_styles_frontend_'.$this->layout, $wp_styles, $excludes, $isModeDev );
		}
		
		
		/**
		 * Callback for the wpca_script_frontend_vars hook
		 * 
		 * Add the expire time to the global JavaScript vars array 
		 * 
		 * @access public
		 * 
		 * @param array $vars
		 *  
		 * @since 2.3.3 
		 *  
		 * @return array
		 */
		public function setScriptsVars( $vars ) 
		{		
			$moduleCore = WpieCookieAllow::getModule( 'core' );
			$moduleAutomate = WpieCookieAllow::getModule( 'automate' );
			$settingsGeneral = $this->settingsProcessor->getSetting( 'general' );
			
			$vars['layout']               = $this->layout;
			$vars['cookieExpire']         = (int)$settingsGeneral->get( 'general_expire_time' );
			$vars['cookiesBeforeConsent'] = $moduleCore->cookiesBeforeConsent;
			$vars['consentMethod']        = $moduleCore->consentMethod;
			$vars['cookiePolicyCssClass'] = $this->_cookiePolicyCssClass;
			$vars['cookiePolicyPathRel']  = $this->cookiePolicyUrlRel;
			$vars['queryVarBypass']       = self::QUERY_VAR_BYPASS_COOKIE_CONSENT;
			$vars['doLayer']              = $this->doLayer;
			$vars['hasClose']             = $settingsGeneral->get( 'general_show_x' );
			$vars['useXAsDismiss']        = $this->useXasDismiss;			
			$vars['minScrollTop']         = (int)$settingsGeneral->get( 'general_min_scroll_top' );
			$vars['cookiePath']           = $this->_cookiePath;
			$vars['cookieDomain']         = $this->_cookieDomain;
			$vars['resettedConsent']      = $this->resettedConsent;
			$vars['bypassingConsent']     = $this->bypassingConsent;
			$vars['allowedCc']            = $moduleAutomate->getAllowedCookieCategories();
			$vars['requiredCc']           = $moduleAutomate->getRequiredCookieCategories();
			$vars['replaceResetBtn']      = $settingsGeneral->get( 'general_replace_reset_by_accept_button' );
			$vars['reloadAfterConsent']   = $settingsGeneral->get( 'general_reload_after_consent' );
									
			unset( $moduleCore, $moduleAutomate, $settingsGeneral );
						
			return $vars;
		}		

		
		/**
		 * Callback for the wpca_registered_shortcodes hook
		 * 
		 * Register the shortcodes for this module
		 * 
		 * @param array $registered
		 * 
		 * @since 3.0
		 * 
		 * @return array
		 */
		public function registerShortcodes( $registered = array() )
		{
			$shordcodes = array(
					'wpca_btn_privacy'        => 'doShortcodeLinkCookiePolicy', // backword compat
					'wpca_cookie_policy_link' => 'doShortcodeLinkCookiePolicy',					
					'wpca_btn_accept'         => 'doShortcodeBtnAccept',
					'wpca_btn_decline'        => 'doShortcodeBtnDecline',
					'wpca_btn_reset_consent'  => 'doShortcodeBtnResetConsent'
			);
				
			foreach ( $shordcodes as $shordcode => $callback ) {
				$registered[] = $shordcode;
				
				if( !defined( 'WPCA_ACTIVE' ) ) {
					$callback = array( 'WpieMiscHelper', 'doShortcodeNot' );
				} else {
					$callback = array( $this, $callback );
				}
				
				add_filter( 'wpca_shortcode_' . $shordcode, $callback, 10, 3 );
			}
				
			return $registered;
		}
		
		
		/**
		 * Callback for the wpca_shortcode_wpca_btn_privacy hook
		 *
		 * Render the shortcode content
		 *
		 * @access public
		 *
		 * @param array $atts
		 * @param string $content
		 * @param string $tag
		 *
		 * @uses WpcaFrontend::renderbtnCookiePolicy()
		 *
		 * @since 2.3.3
		 *
		 * @return string
		 */
		public function doShortcodeLinkCookiePolicy( $atts = array(), $content = '', $tag = '' )
		{
			if( !$this->hasCookiePolicyPage )
				return '';
						
			$atts = shortcode_atts(
					array(
							'txt' => '',
					), $atts, $tag );
			
			$txt = $atts['txt'];
			
			return $this->renderLinkCookiePolicy( $txt );
		}
		
		
		/**
		 * Callback for the wpca_shortcode_wpca_btn_accept hook
		 *
		 * Render the shortcode content
		 *
		 * @access public
		 *
		 * @param array $atts
		 * @param string $content
		 * @param string $tag
		 *
		 * @uses WpcaFrontend::renderBtnAccept()
		 *
		 * @since 2.3.3
		 * 
		 * @return string
		 */
		public function doShortcodeBtnAccept( $atts = array(), $content = '', $tag = '' ) 
		{
			$atts = shortcode_atts(
					array(
							'txt' => '',
							'txt_accepting' => '',
					), $atts, $tag );
		
			$txt = $atts['txt'];
			$txtAccepting = $atts['txt_accepting'];
			
			return $this->renderBtnAccept( $txt, $txtAccepting );
		}		
		

		/**
		 * Callback for the wpca_shortcode_wpca_btn_decline hook
		 *
		 * Render the shortcode content
		 *
		 * @access public
		 *
		 * @param array $atts
		 * @param string $content
		 * @param string $tag
		 *
		 * @uses WpcaFrontend::renderBtnDecline()
		 *
		 * @since 3.1
		 *
		 * @return string
		 */
		public function doShortcodeBtnDecline( $atts = array(), $content = '', $tag = '' )
		{
			$atts = shortcode_atts(
					array(
							'txt' => '',
							'txt_declining' => '',
					), $atts, $tag );
				
			$txt = $atts['txt'];
			$txtDeclining = $atts['txt_declining'];
	
			return $this->renderBtnDecline( $txt, $txtDeclining );
		}		
		
		
		/**
		 * Callback for the wpca_shortcode_wpca_btn_reset_consent hook
		 *
		 * Render the shortcode content
		 *
		 * @access public
		 *
		 * @param array $atts
		 * @param string $content
		 * @param string $tag
		 *
		 * @uses WpcaFrontend::renderBtnResetConsent()
		 *
		 * @since 3.1
		 *
		 * @return string
		 */		
		public function doShortcodeBtnResetConsent( $atts = array(), $content = '', $tag = '' ) 
		{	
			$atts = shortcode_atts(
					array(
							'txt' => '',
							'txt_resetting' => ''
					), $atts, $tag );
				
			$txt = $atts['txt'];
			$txtResetting = $atts['txt_resetting'];
		
			return $this->renderBtnResetConsent( $txt, $txtResetting );						
		}


		/**
		 * Determine if cookies should be blocked
		 *
		 * @param string $cc
		 *
		 * @since 3.0.2
		 *
		 * @return boolean
		 */
		public function doBlockCookies( $cc = '' )
		{				
			$moduleCore = WpieCookieAllow::getModule( 'core' );
			$moduleAutomate = WpieCookieAllow::getModule( 'automate' );
			
			$clientHasAll = $moduleAutomate->hasClientCookieCategoriesAll();
			$clientHasCategories = $moduleAutomate->hasClientCookieCategories();
			$do3rdPartyCookiesBeforeConsent = $moduleCore->do3rdPartyCookiesBeforeConsent();
			
			// Does the client has the passed category set
			$cc = trim( $cc );
			$checkForClientCc = false;
			if( '' !== $cc && $moduleAutomate->isAllowedCookieCategory( $cc ) ) {
				$checkForClientCc = true;
				$clientHasCategory = $moduleAutomate->hasClientThisCookieCategory( $cc );
			}
	
			// block cookies by default	
			$block = true;
				
			// @TODO: move this if statement up to begin of method
			// don't block cookies if plugin is not active
			if( !defined( 'WPCA_ACTIVE' ) ) {
				$block = false;
				
			// and don't block cookies if a cookie category is passed 
			// and client has this category stored	
			} elseif( $checkForClientCc && $clientHasCategory ) {
				$block = false;
				
			// and don't block cookies if 
			// client has accepted ALL categories
			} elseif( $this->accepted && $clientHasAll ) {
				$block = false;
				
			// and don't block cookies if
			// client has accepted and hasn't got categories yet
			// This is the case when the user accepts without saving the settings
			} elseif( $this->accepted && !$clientHasCategories ) {
				$block = false;
				
			// and don't block cookies if
			// client has NOT accepted and declined yet (1st visit untill client makes a choice)
			// and ALL (3rd party) cookies are allowed before consent
			} elseif( $this->firstVisit && $do3rdPartyCookiesBeforeConsent ) {
				$block = false;				
			}
		
			return $block;
		}		
		
		
		/**
		 * Callback for the wpca_init_frontend_only_hooks hook
		 * 
		 * @since 2.3.3
		 */
		public function initHooksFrontend()
		{		
			if( !defined( 'WPCA_ACTIVE' ) ) {
				add_filter( 'wpca_registered_shortcodes', array( $this, 'registerShortcodes' ) );
				return;
			}

			// return to prevent fatal errors
			// @see https://github.com/webRtistik/wp-cookie-allow/issues/79
			if( false === ( $moduleCore = WpieCookieAllow::getModule( 'core' ) ) ) {
				return;
			}			
			
			// set all the necessary cookie params 
			$this->_setCookieParams();
			
			if( isset( $_REQUEST[self::QUERY_VAR_NO_CACHE_HEADERS] ) ) {
				$this->_noCacheHeaders = true;
			} elseif( $this->settingsProcessor->getSetting( 'general' )->get( 'general_disable_browser_cache' ) ) {
				$this->_noCacheHeaders = true;
			}		
			
			if( $this->_noCacheHeaders ) {
				$this->_setNoCacheHeaders();
			}
			
			// do not show the bar or box on the cookie policy page
			if( isset($_REQUEST[self::QUERY_VAR_BYPASS_COOKIE_CONSENT]) ) {
				$this->bypassingConsent = true;
			}
			
			// check if user initiated a cookie consent reset
			if( isset($_REQUEST[self::QUERY_VAR_RESET_COOKIE_CONSENT]) ) {
				WpieCookieHelper::delete( $moduleCore->getCookieName( 'consent' ), true, $this->_cookiePath, $this->_cookieDomain );
				$this->resettedConsent = true;
			}
			
			$this->layout = $this->settingsProcessor->getSetting( 'style' )->get( 'style_type' );
			
			// did consent (accept) or not
			$this->accepted = ( '1' === $moduleCore->cookieValueConsent );
				
			// declined or not
			$this->declined = ( '0' === $moduleCore->cookieValueConsent );
			
			// if not accepted and declined yet, assume it's a 1st visit
			$this->firstVisit = ( !$this->accepted && !$this->declined );
				
			// is the layer needed
			$this->doLayer = $this->_doLayer();
			
			// use the (X) a dismiss action
			$this->useXasDismiss = $this->_doXasDismiss();			
			
			$this->_doHooks();
								
			// the privacy page
			$this->_cookiePolicyPageId = (int)$this->settingsProcessor->getSetting( 'content' )->get( 'content_cookie_policy_page' );
			$this->hasCookiePolicyPage = ( -1 !== $this->_cookiePolicyPageId );				
			if( $this->hasCookiePolicyPage ) {
				$this->cookiePolicyIsPdf = ( wp_attachment_is( 'pdf', $this->_cookiePolicyPageId ) );	
				if( !$this->cookiePolicyIsPdf ) {
					// show or hide the bar/box on the cookie policy page
					$this->showBarBoxOnCookiePolicyPage = $this->settingsProcessor
						->getSetting( 'content' )
						->get( 'content_show_bar_box_cookie_policy_page' );
					
					// the cookie policy page URL 
					$cookiePolicyUrl = get_page_link( $this->_cookiePolicyPageId );							
					
					if( $this->showBarBoxOnCookiePolicyPage ) {
						$this->cookiePolicyUrl = $cookiePolicyUrl;
						$this->cookiePolicyUrlRel = wp_make_link_relative( $this->cookiePolicyUrl );
					} else {
						$this->cookiePolicyUrl = add_query_arg( array( self::QUERY_VAR_BYPASS_COOKIE_CONSENT => '1' ), $cookiePolicyUrl );							
					}
				} else {
					$this->cookiePolicyUrl = wp_get_attachment_url( $this->_cookiePolicyPageId );
				}
			}
			
			unset( $moduleCore );
		}	

	
		/* (non-PHPdoc)
		 * @see iWpiePluginModule::init()
		 * 
		 * @since 2.3.3
		 */
		public function init() 
		{
			// apply hooks for frontend only
			add_action( 'wpca_init_frontend_only_hooks', array( $this, 'initHooksFrontend') );				
		}
		
		
		/**
		 * Set no caching headers
		 * 
		 * @since 3.2.3 
		 */
		private function _setNoCacheHeaders()
		{
			add_filter( 'wp_headers', function( $headers, $wp ) {	
				$headers['Cache-Control'] = 'no-cache, no-store, must-revalidate';
				$headers['Pragma'] = 'no-cache';
				$headers['Expires'] = '0';
				
				return $headers;			
			}, 999999, 2 );
		} 
		
		
		/**
		 * Set cookie params
		 * 
		 * Set cookie params: path, domain counting also for multisite situations
		 * 
		 * @since 3.1.2
		 * @since 3.2.3 added check for domain mapping
		 */
		private function _setCookieParams()
		{
			$isMs = WpieMultisiteHelper::isMs();
			$isSubdomain = ( $isMs ) ? is_subdomain_install() : false;
			$isDomainMapping = WpieMultisiteHelper::isDomainMapping();
			
			$msGlobalConsent = $this
				->settingsProcessor
				->getSetting( 'general' )
				->get( 'general_ms_enable_global_consent' );
			
			
			/**
			 * Wether to or not to let visitors give global consent on multisite installs
			 *
			 * @param bool $msGlobalConsent
			 *
			 * @since 3.2.3
			 *
			 * @return bool
			 */
			$msGlobalConsent = apply_filters( 'wpca_multisite_give_global_consent', $msGlobalConsent );	
			
			// if domain mapping, force global consent to be false
			if( $isMs && $isDomainMapping ) {
				$this->_cookiePath = WpieMultisiteHelper::getBlogDetail( 'path' );
				$this->_cookieDomain = '';				
			} elseif( $isMs && $msGlobalConsent && $isSubdomain ) {
				$this->_cookiePath = '/';
				$this->_cookieDomain = '.' . WpieMiscHelper::getCleanUri( network_home_url() );
			} elseif( $isMs && $msGlobalConsent && !$isSubdomain ) {
				$this->_cookiePath = WpieMultisiteHelper::getBlogDetail( 'path' );
				$this->_cookieDomain = '';
			} elseif( $isMs && !$msGlobalConsent && $isSubdomain ) {
				// subdomain install
				$this->_cookiePath = WpieMultisiteHelper::getBlogDetail( 'path' );
				$this->_cookieDomain = '';
			} elseif( $isMs && !$msGlobalConsent && !$isSubdomain ) {
				// subfolder install
				$this->_cookiePath = WpieMultisiteHelper::getBlogDetail( 'path' );
				$this->_cookieDomain = '';
			} else {
				// no multisite
				$this->_cookiePath = '/';
				$this->_cookieDomain = '';
			}
		}
		
		
		/**
		 * Determine if the complete bar/box logic or only scripts/styles/shortcodes should be loaded
		 * 
		 * @since 3.1
		 * 
		 * @return boolean
		 */
		private function _doConsentLayout() 
		{
			if( $this->bypassingConsent ) {
				return false;
			} elseif( $this->declined ) {
				return false;
			} elseif( $this->firstVisit ) {
				return true;
			} elseif ( $this->resettedConsent ) {
				return true;
			} else {
				return false;
			}	
		}
		
		
		/**
		 * Determine if the layer is realy needed
		 * 
		 * @uses WpcaCore::forceHideLayer()
		 * 
		 * @since 3.1.2
		 * 
		 * @return boolean
		 */
		private function _doLayer()
		{
			$showLayer = $this->settingsProcessor->getSetting( 'general' )->get( 'general_show_layer' );
			
			return ( $showLayer && !$this->declined && !WpieCookieAllow::getModule( 'core' )->forceHideLayer() );
		}
		
		
		/**
		 * Determine if the closing (X) is realy a dismiss
		 * 
		 * @uses WpcaCore::do3rdPartyCookiesBeforeConsent()
		 * 
		 * @since 3.1.2
		 * 
		 * @return boolean
		 */
		private function _doXasDismiss() 
		{
			// extra check for useXasDismiss, if allowing 3rd party cookies before consent
			// the closing (X) should not work as a dismiss but as an accept
			if( WpieCookieAllow::getModule( 'core' )->do3rdPartyCookiesBeforeConsent() ) {
				return false;
			} else {
				return $this->settingsProcessor->getSetting( 'general' )->get( 'general_use_x_as_dismiss' );
			}
		}
		
		
		/**
		 * do hooks
		 * 
		 * @since 3.1
		 */
		private function _doHooks()
		{
			add_filter( 'wpca_registered_shortcodes', array( $this, 'registerShortcodes' ) );
			add_action( 'wpca_print_scripts_frontend', array( $this, 'printScripts' ) );
			add_action( 'wpca_scripts_frontend', array( $this, 'setScripts' ), 10, 3 );
			add_action( 'wpca_styles_frontend', array( $this, 'setStyles' ), 10, 3 );
			add_filter( 'wpca_script_frontend_vars', array( $this, 'setScriptsVars' ) );

			// hook at priority 2 so that  params are declared
			add_action( 'wp_loaded', array( $this, 'render' ), 2 );
			add_action( 'wp_footer', array( $this, 'addHtml' ), 999999 );
		}
		
		
		/**
		 * Setup the template for layout bar
		 * 
		 * @access	private
		 * 
		 * @uses 	WpieTemplate
		 * 
		 * @since 	2.3.3
		 * 
		 * @return 	WpieTemplate
		 */
		private function _renderBar()
		{
			// create CSS classes based on settings defaults or user input			
			$pos = $this->settingsProcessor->getSetting( 'style' )->get( 'style_bar_pos' );
			$textAlign = $this->settingsProcessor->getSetting( 'style' )->get( 'style_text_align' );
				
			$position = ( '0' == $pos ) ? 'top' : 'bottom';
			$textAlign = ( '0' == $textAlign ) ? 'left' : ( '1' == $textAlign ? 'center' : 'right' );
			$cssClass = "wpca-$position wpca-align-$textAlign wpca-hide";
				
			// create new WpieTemplate instance
			$template = new WpieTemplate( self::TEMPL_FILE_NAME_BAR, dirname(__FILE__) . '/templates', self::TEMPL_FILE_NAME_BAR );
			$template->setVar( 'css_class', $cssClass );
				
			return $template;
		}
		
		
		/**
		 * Setup the tenplate for layout box
		 * 
		 * @access	private
		 * 
		 * @uses 	WpieTemplate
		 * 
		 * @since 	2.3.3
		 * 
		 * @return 	WpieTemplate
		 */
		private function _renderBox()
		{
			// create CSS classes based on settings defaults or user input			
			$position = $this->settingsProcessor->getSetting( 'style' )->get( 'style_box_pos' );
			$textAlign = $this->settingsProcessor->getSetting( 'style' )->get( 'style_text_align' );
			$btnAlign = $this->settingsProcessor->getSetting( 'style' )->get( 'style_box_btn_align' );
			$space = $this->settingsProcessor->getSetting('style')->get( 'style_box_window_space' );
						
			$centered = ( 'ct' === $position ) ? true : false;			
			$posTopBot = ( ( 'tl' === $position || 'tr' === $position ) ) ? 'wpca-top' : (( !$centered ) ? 'wpca-bottom' : '' );
			$textAlign = ( '0' == $textAlign ) ? 'left' : ( '1' == $textAlign ? 'center' : 'right' );
			$btnAlign = ( '0' == $btnAlign ) ? 'left' : ( '1' == $btnAlign ? 'center' : 'right' );
			$cssClass = "wpca-$position $posTopBot wpca-align-$textAlign wpca-btn-align-$btnAlign wpca-hide";
				
			// create new WpieTemplate instance
			$template = new WpieTemplate( self::TEMPL_FILE_NAME_BAR, dirname(__FILE__) . '/templates', self::TEMPL_FILE_NAME_BOX );
			$template->setVar( 'css_class', $cssClass );
			$template->setVar( 'window_space', $space );
				
			return $template;
		}	
	}
	
	if( !function_exists( 'wpca_block_cookies' ) )
	{
		/**
		 * Block cookies if consent has not been given
		 * 
		 * @param string $cc
		 *
		 * @uses WpcaFrontend::doBlockCookies()
		 * 
		 * @since 3.0.2
		 *
		 * @return boolean
		 */
		function wpca_block_cookies( $cc = '' )
		{
			if( false !== ( $module = WpieCookieAllow::getModule( 'frontend' ) ) )
			{
				$block = $module->doBlockCookies( $cc );
				unset( $module );
				
				return $block;
			}
		}
	}	
}