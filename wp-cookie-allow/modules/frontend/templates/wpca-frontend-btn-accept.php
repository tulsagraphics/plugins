<?php
/**
 * Template for showing the Accept button
 *
 * Please see wp-cookie-allow.php for more details.
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 2.2
 */
?>
<button class="wpca-btn-accept wpca-btn wpca-btn-hide" data-txt="<?php echo $txt ?>" data-txt-processing="<?php echo $txt_processing ?>"><?php echo $txt ?></button>