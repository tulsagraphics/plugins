<?php
// only allow these params to be passed
$allowedParams = array( 'c' );
foreach ( $_GET as $k => $v ) {
	if( !in_array( $k , $allowedParams) ) {
		unset($_GET[$k]);
	}
}
$r = $_GET;
$fill = ( isset( $r['c'] ) ) ? $r['c'] : '#D7562B';
$fill = '#'.$fill;
ob_start();
?><svg 
	version="1.1" 
	class=""
	xmlns="http://www.w3.org/2000/svg" 
	xmlns:xlink="http://www.w3.org/1999/xlink" 
	x="0px" 
	y="0px" 
	width="20px" 
	height="20px" 
	viewBox="0 0 20 20" 
	enable-background="new 0 0 20 20"
	xml:space="preserve">
	<polygon fill-rule="evenodd" points="19,3.581 16.42,1 10,7.42 3.581,1 1,3.58 7.42,10 1,16.42 3.581,19 10,12.581 16.42,19 19,16.419 12.581,10" fill="<?php echo $fill ?>"/>
</svg><?php 
$svg = ob_get_contents();
ob_end_clean();
header('Content-Type: image/svg+xml');
echo trim($svg); exit;