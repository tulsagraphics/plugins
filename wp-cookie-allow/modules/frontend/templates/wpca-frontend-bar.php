<?php
/**
 * Template for showing the bar (top, bottom of screen)
 * 
 * Please see wp-cookie-allow.php for more details.
 * 
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 1.0
 * 
 * @since 3.1 show a decline button
 */
?>
<div id="wpca-bar" class="wpca-lay-out <?php echo $css_class ?> group">
	<div id="wpca-bar-content" class="wpca-lay-out-content"><?php echo $content_text ?></div><?php if( $show_btns ): ?><div id="wpca-bar-meta" class="wpca-lay-out-meta"><?php if( $show_btn_accept ): echo $btn_accept; endif ?><?php if( $show_btn_decline ): echo $btn_decline; endif ?></div><?php endif ?>
 	<?php if( $show_close_x ): ?>
  <img id="wpca-close" src="<?php echo $close_url ?>" width="20" height="20" />	 
 	<?php endif ?>		
</div>
<?php if( $do_layer ): ?><div id="wpca-trans-layer" class="wpca-hide"></div><?php endif ?>