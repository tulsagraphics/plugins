<?php
/**
 * Template for showing the privacy button / link
 *
 * Please see wp-cookie-allow.php for more details.
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 2.3.3
 */
?>
<a class="<?php echo $css_class ?>" href="<?php echo $cookie_policy_page_url ?>" target="<?php echo $target ?>"><?php echo $cookie_policy_link_txt ?></a>