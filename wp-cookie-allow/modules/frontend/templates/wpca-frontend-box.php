<?php
/**
 * Template for showing the box (top left, top right, bottom left, bottom right of the screen)
 * 
 * Please see wp-cookie-allow.php for more details.
 * 
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 2.3.3
 * 
 * @since 3.1 show a decline button
 */
?>
<div id="wpca-box" class="wpca-lay-out <?php echo $css_class ?> group" data-space="<?php echo $window_space ?>">
	<div id="wpca-box-inner">
		<div id="wpca-box-content" class="wpca-lay-out-content"><?php echo $content_text ?></div><?php if( $show_btns ): ?><div id="wpca-box-meta" class="wpca-lay-out-meta"><?php if( $show_btn_accept ): echo $btn_accept; endif ?><?php if( $show_btn_decline ): echo $btn_decline; endif ?></div><?php endif ?>
	 	<?php if( $show_close_x ): ?>
	  <img id="wpca-close" src="<?php echo $close_url ?>" width="20" height="20" />	 
	 	<?php endif ?>
	</div>
</div>
<?php if( $do_layer ): ?><div id="wpca-trans-layer" class="wpca-hide"></div><?php endif ?>	