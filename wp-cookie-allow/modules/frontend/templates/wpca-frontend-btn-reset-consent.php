<?php
/**
 * Template for showing the reset consent button
 *
 * Please see wp-cookie-allow.php for more details.
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 3.1
 */
?>
<button class="wpca-btn-reset-consent wpca-btn wpca-btn-hide" data-txt="<?php echo $txt ?>" data-txt-processing="<?php echo $txt_processing ?>"><?php echo $txt ?></button>