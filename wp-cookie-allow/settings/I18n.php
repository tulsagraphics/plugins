<?php
//BEGIN wpca_settings_content
__('Cookie policy page','wpca');
__('Post type','wpca');
__('Select your <strong>Cookie policy page</strong> Post Type.','wpca');
__('Page','wpca');
__('Select your <strong>Cookie policy page</strong> here and use it inside your choosen lay-out.','wpca');
__('Target','wpca');
__('Open the cookie policy page in a new browser tab or in the same.','wpca');
__('Same browser tab','wpca');
__('New browser tab','wpca');
__('Cookie policy link text','wpca');
__('To display the <strong>Cookie policy link text</strong> in your lay-out (bar or box), use the shortcode <code>[wpca_cookie_policy_link]</code>.','wpca');
__('Show bar/box?','wpca');
__('Show or hide the bar/box on your cookie policy page.','wpca');
__('Reset consent','wpca');
__('Please note that resetting the consent, will not always clear all cookies set in the browser of the website visitor.','wpca');
__('Button Reset consent text','wpca');
__('Button Reset consent text "Resetting consent"','wpca');
__('Content bar/box','wpca');
__('Text','wpca');
__('Button Accept text','wpca');
__('Button Accept text "Accepting"','wpca');
__('Button Decline text','wpca');
__('Button Decline text "Declining"','wpca');
__('Content Cookie Category settings box','wpca');
__('Please note this is a <strong>b&egrave;ta functionality</strong>.','wpca');
__('<strong>Cookie categories</strong> let website visitors decide which <strong>type of cookie should be blocked or not</strong> after \'clicking\' on the "<strong>Save cookie settings</strong>" button or by your choosen "<strong>Consent method</strong>". The following categories are allowed: <code>functional</code>, <code>analytical</code>, <code>social-media</code>, <code>advertising</code>, <code>other</code>. Add the box with the shortcode: <code>[wpca_cc_settings_box]</code>','wpca');
__('Text','wpca');
__('Button default settings text','wpca');
__('Button default settings text "Processing"','wpca');
__('Button save text','wpca');
__('Button save text "Saving"','wpca');
__('Placeholder for blocked content','wpca');
__('Enable','wpca');
__('Hide logo','wpca');
__('Hide our plugin Cookie logo inside the replacement elemement.','wpca');
__('Text','wpca');
__('To display the <strong>Cookie accept button</strong> in your placeholder, use the shortcode <code>[wpca_btn_accept]</code> with or without a <strong>custom button text</strong> <code>[wpca_btn_accept txt="My custom button text"]</code>.','wpca');
__('Shortcodes','wpca');
__('A short explaination of the WeePie Cookie Allow shortcodes you can use on your website.','wpca');
__('<table>
<tbody>
<tr>
  <th>Shortcode</th>
  <th>Description</th>
  <th>Attributes</th>
  <th>Example</th>
</tr>
<tr><td><code>[wpca_cookie_policy_link]</code></td><td>Shows a hyperlink to you <strong>Cookie policy page</strong>.</td><td>txt (optional)</td><td><code>[wpca_cookie_policy_link txt="Our cookie policy page"]</code></td></tr>
<tr><td><code>[wpca_btn_accept]</code></td><td>Shows an <strong>Accept button</strong>.</td><td>txt (optional), txt_accepting (optional)</td><td><code>[wpca_btn_accept txt="Accept cookies!" txt_accepting="Woehee accepting.."]</code></td></tr>
<tr><td><code>[wpca_btn_decline]</code></td><td>Shows a <strong>Decline button</strong>.</td><td>txt (optional), txt_declining (optional)</td><td><code>[wpca_btn_decline txt="No, thanks!" txt_declining="Declining now.."]</code></td></tr>
<tr><td><code>[wpca_btn_reset_consent]</code></td><td>Shows a <strong>Reset consent button</strong>.</td><td>txt (optional), txt_resetting (optional)</td><td><code>[wpca_btn_reset_consent txt="Reset your consent" txt_resetting="Resetting now.."]</code></td></tr>
<tr><td><code>[wpca_cookie_allow_code]</code></td><td><strong>Block (3rd party/privacy sensitive) content manually</strong> by wrapping content with this shortcode. Allowed Cookie Categories: <strong>functional</strong>, <strong>analytical</strong>, <strong>social-media</strong>, <strong>advertising</strong>, <strong>other</strong>.</td><td>txt (optional), type (optional), cc (required)</td><td><code>[wpca_cookie_allow_code txt="My 3rd party content is blocked" type="my-3rd-party" cc="advertising"]SCRIPT OR IFRAME[/wpca_cookie_allow_code]</code></td></tr>
<tr><td><code>[wpca_cc_settings_box]</code></td><td>Shows the <strong>Cookie Category settings box</strong>.</td><td>None</td><td><code>[wpca_cc_settings_box]</code></td></tr>
</tbody>
<table>','wpca');
//END wpca_settings_content
//BEGIN wpca_settings_general
__('Plugin','wpca');
__('Status','wpca');
__('Enable / disable the Plugin.','wpca');
__('Disabled','wpca');
__('Enabled','wpca');
__('Disable for logged in users','wpca');
__('Disable for non-EU visitors','wpca');
__('This will disable all plugin logic (no bar/box and no content blocking) for requests from IP addresses that are detected from non EU countries. The GEO IP data is created by MaxMind, available from <a href="http://www.maxmind.com" target="_blank">maxmind.com</a>.','wpca');
__('Cookie expiration time','wpca');
__('Set the expiration time (<strong>in days</strong>) for the required cookies of this plugin.','wpca');
__('Disable browser caching','wpca');
__('This will try to send no caching headers to avoid browser HTTP caching. This will use the PHP <code>header()</code> function.','wpca');
__('Consent type','wpca');
__('<strong>Which cookies</strong> should be set before the website visitor <strong>gives consent</strong> (by your choosen consent method)?','wpca');
__('Cookies before consent','wpca');
__('Necessary and functional','wpca');
__('Google Analytics','wpca');
__('Google Analytics &amp; 3rd party','wpca');
__('Consent method?','wpca');
__('Accept button','wpca');
__('Accept button / Scroll','wpca');
__('Accept button / Click','wpca');
__('Accept button / Click / Scroll','wpca');
__('Click / Scroll','wpca');
__('Only Scroll','wpca');
__('Only Click','wpca');
__('Minimal scrolling (px)','wpca');
__('Set the Minimal ammount of px an user must scroll before consent is given.','wpca');
__('Show a close (X)','wpca');
__('Add a closing cross (X) to the (bottom/top) right corner of the bar/box. The (X) will act like an \'Accept button\'.','wpca');
__('Use (X) as dismiss','wpca');
__('Using the closing cross (X) as dismiss (decline) will only hide the bar/box, but will not trigger the consent functionality.','wpca');
__('Show layer','wpca');
__('Add a layer on top of your website to force attention to your cookie consent message and disable website usage by the visitor. <strong>Please note</strong> that this will disable the click / scroll consent methods.','wpca');
__('Show a decline button','wpca');
__('Show a \'Decline button\' next to the \'Accept button\' in the bar or box.','wpca');
__('Replace Reset by Accept button before consent','wpca');
__('If checked, the \'Reset button\' is replaced by the \'Accept button\' before consent.','wpca');
__('Reload page after consent','wpca');
__('If checked, the website visitors page will be reloaded after consent. This might be useful when blocked content needs a page reload to function properly.','wpca');
__('Multisite network','wpca');
__('Give global consent','wpca');
__('Enabling this will set the <strong>cookie path and domain</strong> to the <strong>network main</strong> website. Note that for <strong>domain mapping</strong> installs, this will not work (handled automatically by the plugin).','wpca');
__('3rd party &amp; privacy sensitive cookies','wpca');
__('Automatic cookie blocking','wpca');
__('Automatically block 3rd party or privacy sensitive content (i.e. iFrames, JavaScripts, Tracking image pixels) in your web pages before sending it to the browser of your website visitor.','wpca');
__('Block all iFrames','wpca');
__('Automatically delete all iframes inside the body tag.','wpca');
__('Keep anonymized Google Analytics','wpca');
__('Allow the <strong>anonymized Google Analytics tracking code</strong> before the client has accepted for <strong>Necessary and functional</strong>.<br/>Your tracking code should contain somthing like: <code>ga(\'set\', \'anonymizeIp\', true)</code> or <code>_gaq.push ([\'_gat._anonymizeIp\'])</code>.','wpca');
__('automate/templates/wpca-general-automate.php','wpca');
//END wpca_settings_general
//BEGIN wpca_settings_style
__('General','wpca');
__('Type','wpca');
__('Bar','wpca');
__('Box','wpca');
__('Layer background color','wpca');
__('Layer opacity','wpca');
__('Enter a number between 0 and 1, e.g. 0.75.','wpca');
__('Box Shadow','wpca');
__('Add a CSS box shadow to the bar/box.','wpca');
__('Text align','wpca');
__('Left','wpca');
__('Center','wpca');
__('Right','wpca');
__('Background color','wpca');
__('Close X color','wpca');
__('Opacity','wpca');
__('Enter a number between 0 and 1, e.g. 0.95.','wpca');
__('Text color','wpca');
__('Link color','wpca');
__('Link color mouseover','wpca');
__('Button Accept color','wpca');
__('Button Accept color mouseover','wpca');
__('Button Accept text color','wpca');
__('Button Accept text color mouseover','wpca');
__('Button Decline color','wpca');
__('Button Decline color mouseover','wpca');
__('Button Decline text color','wpca');
__('Button Decline text color mouseover','wpca');
__('Button Reset consent color','wpca');
__('Button Reset consent color mouseover','wpca');
__('Button Reset consent text color','wpca');
__('Button Reset consent text color mouseover','wpca');
__('Custom CSS','wpca');
__('Bar','wpca');
__('Position','wpca');
__('Top','wpca');
__('Bottom','wpca');
__('Box','wpca');
__('Position','wpca');
__('Top left','wpca');
__('Top right','wpca');
__('Bottom left','wpca');
__('Bottom right','wpca');
__('Centered','wpca');
__('Width (%)','wpca');
__('The width of the box relative to your screen width. Enter a number between 1 and 100%. Please note that using a low width (< 30%) may have impact on the layout.','wpca');
__('Minimum height (%)','wpca');
__('The minimum height of the box relative to your screen height. Enter a number between 1 and 100%.','wpca');
__('Border radius (px)','wpca');
__('Enter a number between 0 and 50 pixels.','wpca');
__('Space (px)','wpca');
__('Posistion the box with a distance from your browser window. Enter a number between 0 and 50 pixels.','wpca');
__('Button Accept/Decline align','wpca');
__('Left','wpca');
__('Center','wpca');
__('Right','wpca');
__('Cookie Category settings','wpca');
__('Shadow','wpca');
__('Add a CSS box shadow to the Cookie Category settings.','wpca');
__('Border radius (px)','wpca');
__('Enter a number between 0 and 50 pixels.','wpca');
__('Background color','wpca');
__('Text color','wpca');
__('Link color','wpca');
__('Link color mouseover','wpca');
__('Button default settings color','wpca');
__('Button default settings color mouseover','wpca');
__('Button default settings text color','wpca');
__('Button default settings text color mouseover','wpca');
__('Button save','wpca');
__('Button save mouseover','wpca');
__('Button save text color','wpca');
__('Button save text color mouseover','wpca');
__('Placeholder for blocked content','wpca');
__('Background color','wpca');
__('Minimal placeholder width (px)','wpca');
__('When our plugin script tries to inherit the dimensions (WxH) of an element, sometimes it can\'t find the correct dimensions. You can set the <strong>minimal \'width\' in pixels</strong> for the placholder here.','wpca');
__('Minimal placeholder height (px)','wpca');
__('When our plugin script tries to inherit the dimensions (WxH) of an element, sometimes it can\'t find the correct dimensions. You can set the <strong>minimal \'heigth\' in pixels</strong> for the placholder here.','wpca');
//END wpca_settings_style
//BEGIN wpca_settings_log
__('General','wpca');
__('Enable','wpca');
__('Enable / disable consent logging','wpca');
__('Consent logs','wpca');
__('Find logged consent here.','wpca');
__('Limit','wpca');
__('The total number of items to fetch from the database','wpca');
__('Order by','wpca');
__('How to order the list ("Newest" on top or the "Oldest" on top)','wpca');
__('Newest','wpca');
__('Oldest','wpca');
__('log/templates/wpca-log-list.php','wpca');
//END wpca_settings_log
//BEGIN wpca_settings_consent_log
__('General','wpca');
__('Please note this is a <strong>b&egrave;ta functionality</strong>.','wpca');
__('Enable','wpca');
__('Enable / disable consent logging.','wpca');
__('Anonymize IP address','wpca');
__('Enable / disable anonymizing IP addresses.','wpca');
__('Overview of logged consents','wpca');
__('Find your logged client consents here. The <span class="wpie-red-light-bgd">declined</span> consents are colored red.','wpca');
__('Limit','wpca');
__('The total number of items to fetch from the database.','wpca');
__('Order by','wpca');
__('How to order the list ("Newest" on top or the "Oldest" on top).','wpca');
__('Newest','wpca');
__('Oldest','wpca');
__('consent-log/templates/wpca-log-list.php','wpca');
//END wpca_settings_consent_log
