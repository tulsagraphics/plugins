<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if(!class_exists('FPD_Parameters')) {

	class FPD_Parameters {

		//deprecated (method not used anymore in last versions of fpd plugins, remove with V3.9)
		public static function convert_parameters_to_string( $parameters, $type = '' ) {

			return FPD_Parameters::to_json($parameters, $type);

		}

		public static function parse_property( $prop_key, $value, $type = 'image' ) {

			$properties = array(
				'left' => 'floatval',
				'top' => 'floatval',
				'originX' => 'strval',
				'originY' => 'strval',
				'z' => 'intval',
				'colors' => function($value) { return is_array($value) ? implode(", ", $value) : ($value == "0" ? '' : $value); },
				'colorLinkGroup' => function($value) { return $value == "0" ? false : $value; },
				'draggable' => 'boolval',
				'rotatable' => 'boolval',
				'resizable' => 'boolval',
				'removable' => 'boolval',
				'zChangeable' => 'boolval',
				'scaleX' => 'floatval',
				'scaleY' => 'floatval',
				'angle' => 'intval',
				'price' => 'floatval',
				'autoCenter' => 'boolval',
				'replace' => 'strval',
				'autoSelect' => 'boolval',
				'topped' => 'boolval',
				'boundingBoxMode' => 'strval',
				'opacity' => 'floatval',
				'minW' => 'strval',
				'minH' => 'floatval',
				'resizeToW' => 'floatval',
				'resizeToH' => 'floatval',
				'maxSize' => 'floatval',
				'minDPI' => 'intval',
				'fill' => function($value) { return $value == "0" ? false : $value; },
				'uploadZone' => 'boolval',
				'filter' => function($value) { return $value == "0" ? false : $value; },
				'replaceInAllViews' => 'boolval',
				'lockUniScaling' => 'boolval',
				'uniScalingUnlockable' => 'boolval',
				'scaleMode' => 'strval',
				'sku' => 'strval',
				'excludeFromExport' => 'boolval',
				'minScaleLimit' => function($value) { return number_format(floatval($value), 4); },
				'advancedEditing' => 'boolval',
				'locked' => 'boolval',
				'showInColorSelection' => 'boolval',
				'designCategories' => function($value) { return is_array($value) ? $value : array(); },
			);

			if( $type === 'text' ) {

				$text_properties = array(
					'fontFamily' => 'strval',
					'fontSize' => 'intval',
					'editable' => 'boolval',
					'lineHeight' => 'floatval',
					'textDecoration' => 'strval',
					'maxLength' => 'intval',
					'fontWeight' => 'strval',
					'fontStyle' => 'strval',
					'textAlign' => 'strval',
					'curvable' => 'boolval',
					'curved' => 'boolval',
					'curveSpacing' => 'intval',
					'curveRadius' => 'intval',
					'curveReverse' => 'boolval',
					'stroke' => 'strval',
					'strokeWidth' => 'intval',
					'maxLines' => 'intval',
					'letterSpacing' => 'floatval',
					'chargeAfterEditing' => 'boolval',
					'minFontSize' => 'intval',
					'maxFontSize' => 'intval',
					'textTransform' => 'strval',
					'widthFontSize' => 'intval',
					'textBox' => 'boolval',
					'width' => 'floatval', //textbox
				);

				$properties = array_merge($properties, $text_properties);

			}

			if( isset($properties[$prop_key]) )
				return call_user_func($properties[$prop_key], $value);
			else
				return null;

		}

		public static function to_json( $parameters, $type = 'image', $encode = true ) {

			if( empty($parameters) ) { return '{}'; }

			$json_data = array();

			foreach($parameters as $key => $value) {

				if( fpd_not_empty($value) ) {

					if( $key == 'designCategories[]' ) {
						$json_data['designCategories'] = self::parse_property('designCategories', $value, $type);
					}
					else if( $type == 'text' && $key == 'width' ) {
						if(isset($parameters['textBox']) && $parameters['textBox'])
							$json_data['width'] = self::parse_property('width', $value, $type);
					}
					else if( $type == 'text' && $key == 'textNumberPlaceholder' ) {

						if($value === 'text')
							$json_data['textPlaceholder'] = true;
						else if($value === 'number') {

							if( isset($parameters['numberPlaceholderMin']) && isset($parameters['numberPlaceholderMax']) ) {
								$json_data['numberPlaceholder'] = array(
									$parameters['numberPlaceholderMin'], $parameters['numberPlaceholderMax']
								);
							}
							else
								$json_data['numberPlaceholder'] = true;

						}

					}
					else { //general

						$parsed_val = self::parse_property($key, $value, $type);
						if( !is_null($parsed_val) )
							$json_data[$key] = $parsed_val;

					}

				}
			}

			//upload zones media sources
			if( isset($parameters['uploadZone'])  ) {

				$json_data['customAdds'] = array();

				if( isset($parameters['adds_uploads']) )
					$json_data['customAdds']['uploads'] = (bool)$parameters['adds_uploads'];

				if( isset($parameters['adds_texts']) )
					$json_data['customAdds']['texts'] = (bool)$parameters['adds_texts'];

				if( isset($parameters['adds_designs']) )
					$json_data['customAdds']['designs'] = (bool)$parameters['adds_designs'];

				if( isset($parameters['adds_facebook']) )
					$json_data['customAdds']['facebook'] = (bool)$parameters['adds_facebook'];

				if( isset($parameters['adds_instagram']) )
					$json_data['customAdds']['instagram'] = (bool)$parameters['adds_instagram'];

			}


			//bounding box
			if( empty($parameters['bounding_box_control']) ) {

				//use custom bounding box
				if(isset($parameters['bounding_box_x']) &&
				   isset($parameters['bounding_box_y']) &&
				   isset($parameters['bounding_box_width']) &&
				   isset($parameters['bounding_box_height'])
				   ) {

					if( fpd_not_empty($parameters['bounding_box_x']) &&
						fpd_not_empty($parameters['bounding_box_y']) &&
						fpd_not_empty($parameters['bounding_box_width']) &&
						fpd_not_empty($parameters['bounding_box_height'])
						) {

						$json_data['boundingBox'] = array(
							'x' => floatval($parameters['bounding_box_x']),
							'y' => floatval($parameters['bounding_box_y']),
							'width' => floatval($parameters['bounding_box_width']),
							'height' => floatval($parameters['bounding_box_height']),
						);

					}
				}

			}
			else if ( isset($parameters['bounding_box_by_other']) && fpd_not_empty(trim($parameters['bounding_box_by_other'])) ) {
				$json_data['boundingBox'] = $parameters['bounding_box_by_other'];
			}

			return $encode ?  json_encode($json_data, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES) : $json_data;

		}

	}

}


?>