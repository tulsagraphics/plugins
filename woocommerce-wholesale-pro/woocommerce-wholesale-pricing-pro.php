<?php
/*
Plugin Name: WooCommerce Wholesale Pro Suite
Plugin URI:  http://ignitewoo.com
Description: Allows you to set price tiers for products and variations based on user roles.
Version: 2.2.24
Author: IgniteWoo.com
Author URI: http://ignitewoo.com
Text Domain: ignitewoo_wholesale_pro
Domain Path: languages/
WC requires at least: 3.0
WC tested up to: 3.1.2
*/

/**
 * TODO:
 
	* Make the quanity requirement rules compat with phone/manual orders

	* Make invoice payment gateway setting so that it's only available to role based users
		Add a checkbox "Enable for all users" - when set public can use the gateway
		otherwise only roles that have it set can use it?

	* add shortcodes from wholesale pricing plus?

	* Support for Bundled Products
	
	* Make it possible to set specific gateways for ALL roles, not just roles created by this plugin.
	
	* Add support for min/max rules that are counted across variations of a product (e.g. Min qty = 3, shoppers puts 1 x Var1, 2 x Var2 = 3 total in the cart, rule passes )

	Maybe make a coupon plugin that allows a coupon to be valid for X products if a customer already purchased Y products
	and use this to sell UPS, Fedex, USPS, Australia Post, and Table Rate Shipping
	
	* Send email when a user is approved - trigger by switching their role from Customer to a tier role, or when auto-setting to a tiered role 
	* Ensure that if auto registration is enabled to show a confirm message on the page telling the user they are approved 
		and put a link on the page to login My Account page 
			

TEST: Make sure shipping method restrictions work on WC with no zones, and WC pre 2.6
TEST: Works with WooCommerce Currency Switcher ???
	https://wordpress.org/plugins/woocommerce-currency-switcher/


DOCUMENTATION :

	Includes an Invoice Payment gateway

	Roles

	Import and export pricing using commonly available CSV import/export tools for WooCommerce

	Global Discounts
		Define global discounts for any or all roles

	Category Discounts
		These override any global discount if set
		Applied to the role price if set otherwise regular price
		The discount set in LAST category in the hierarchy of categories that has a discount becomes the discount used

	Discount Rules - global and product specific

	Min/Max Quantities - global and product specific including variations

	Quick Order Forms for any role
		Shortcode [wsp_quick_order_form id=XXXXX] where XXXXX is the form ID number
		Obtain the shortcode when editing a form, it's located in the sidebar

	Coupons valid only for any defined roles

	Registration form include full billing address and tax /vat fields
		Easy drag and drop form builder
		Stores Tax / VAT ID in orders and shows on the order details screen, exportable to CSV using an order export tool
		Sends complete form data to site admins
		Automatically created a user account
		User login name is automatically created from the user's email address
		Optional automatical approval
		Shortcode for easy insertion into posts or pages
			[wholesale_pro_suite_register]
			Create a page, insert that shortcode, then visit the WPS settings Registration tab and select the page you
			created in the Registration Page settings.

		You MUST include billing email, one password field and a submit button into the form or submittal of the form will fail

		All "billing" fields are automatically saved into the user's account

	Custom "Thank you" message for purchases by users with any of your pricing roles

	Shortcode to hide or show content to users with roles
		Example:
		[wsp_hide_or_show roles="administrator" mode="hide" restricted_message="blah<br/>blah" restricted_style="margin-bottom:1em;color:#cf0000"]

		OR
		[wsp_hide_or_show logged_in="true" mode="hide"]
			Hides content for logged in users, shows content to all others not logged in
			NOTE: This logged_in attr overrides any shortcode attributes for roles and caps

		Get the role values from the Roles tab

 */

class IGN_Wholesale_Pro_Suite {

	var $roles;

	function __construct() {

		$this->plugin_url = plugins_url() . '/' . str_replace( basename( __FILE__ ), '' , plugin_basename( __FILE__ ) );

		$this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );

		$this->settings = get_option( 'woocommerce_ignitewoo_wholesale_pro_suite_settings' );

		add_action( 'init', array( &$this, 'load_plugin_textdomain' ), 0 );

		add_action( 'init', array( &$this, 'init' ), -1 );
		
		add_action( 'wp', array( &$this, 'maybe_hide_price_init' ), 900 );

	}

	function load_plugin_textdomain() {

		$locale = apply_filters( 'plugin_locale', get_locale(), 'ignitewoo_wholesale_pro' );

		load_textdomain( 'ignitewoo_wholesale_pro', WP_LANG_DIR.'/woocommerce/ignitewoo_wholesale_pro-'.$locale.'.mo' );

		$plugin_rel_path = apply_filters( 'ignitewoo_translation_file_rel_path', dirname( plugin_basename( __FILE__ ) ) . '/languages' );

		load_plugin_textdomain( 'ignitewoo_wholesale_pro', false, $plugin_rel_path );
	}

	function create_initial_roles() {
		global $wp_roles, $wpdb;
		
		if ( class_exists( 'WP_Roles' ) )
		    if ( !isset( $wp_roles ) )
			$wp_roles = new WP_Roles();

		if ( is_object( $wp_roles ) ) {

			if ( empty( $wp_roles->roles['pending_wholesaler'] ) ) {
				add_role( 'pending_wholesaler' ,  __( 'Pending Wholesaler', 'ignitewoo_wholesale_pro' ), array( 'read' => true ) );
			}
		
			$already_done = false;

			foreach( $wp_roles->roles as $name => $role ) {
				if ( 'ignite_level_' == substr( $name, 0, 13 ) )
					$already_done = true;
			}

			if ( $already_done )
				return;

			$caps = array(
				'read' => true,
				'edit_posts' => false,
				'delete_posts' => false,

			);

			$uid = uniqid();

			add_role( 'ignite_level_' . $uid, 'Wholesale Buyer', $caps );

			$role = get_role( 'ignite_level_' . $uid );

			if ( is_object( $role ) )
				$role->add_cap( 'buy_wholesale' );
		}
	}

	function incompat() {

		$msg = __( 'WooCommerce Wholesale Pro Suite replaces functionality for the following plugins and therefore those plugins must be disabled before WooCommerce Wholesale Pro Suite can operate', 'ignitewoo_wholesale_pro' ) . ': ' . implode( ', ', $this->incompat );

		?>
		<div class="notice notice-error">
			<p><?php echo $msg ?></p>
		</div>
		<?php
	}

	function incompat_wc_version() {

		$msg = sprintf( __( "WooCommerce Wholesale Pro Suite requires WooCommerce 2.4 or newer to operate. You're using version %s", 'ignitewoo_wholesale_pro' ), WOOCOMMERCE_VERSION );

		?>
		<div class="notice notice-error">
			<p><?php echo $msg ?></p>
		</div>
		<?php
	}

	function all_checks() {

		if ( version_compare( WOOCOMMERCE_VERSION, '2.4', '<=' ) ) {
			add_action( 'admin_notices', array( &$this, 'incompat_wc_version' ) );
			return false;
		}

		$this->incompat = array();

		// Plugins whose functionality is handled by this plugin already, so they are not allowed to be active on the site
		if ( class_exists( 'IGN_Quick_Orders' ) )
			$this->incompat[] = 'WooCommerce Quick Order Forms';

		if ( class_exists( 'woocommerce_tiered_pricing' ) || class_exists( 'IGN_Tiered_Pricing' ) )
			$this->incompat[] = 'Woocommerce Tiered Pricing';

		if ( class_exists( 'IgniteWoo_Tiered_Pricing_Filter' ) )
			$this->incompat[] = 'Woocommerce Tiered Pricing Filters';

		if ( class_exists( 'IgniteWoo_Dynamic_Discounts' ) )
			$this->incompat[] = 'Woocommerce Dynamic Discounts';

		if ( class_exists( 'woocommerce_wholesale_pricing' ) )
			$this->incompat[] = 'Woocommerce Wholesale Pricing';

		if ( class_exists( 'IgniteWoo_Wholesale_Pricing_Plus' ) )
			$this->incompat[] = 'Woocommerce Wholesale Pricing Plus';

		if ( class_exists( 'IgniteWoo_Minimum_Wholesale_Cart_Total' ) )
			$this->incompat[] = 'Woocommerce Minimum Order and Quantities';

		if ( class_exists( 'IgniteWoo_Minimum_Cart_Total_Quantity' ) )
			$this->incompat[] = 'WooCommerce Min Max Totals and Quantities';

		if ( class_exists( 'WooCommerceWholeSalePrices' ) )
			$this->incompat[] = 'WooCommerce Wholesale Prices';

		if ( class_exists( 'WooCommerceWholeSalePricesPremium' ) )
			$this->incompat[] = 'WooCommerce Wholesale Prices Premium';

		if ( !empty( $this->incompat ) ) {
			add_action( 'admin_notices', array( &$this, 'incompat' ) );
			return false;
		} else
			return true;
	}
		
	function init() {

		if ( !$this->all_checks() )
			return;

		//@session_start();

		$this->create_initial_roles();
		
		require_once( 'includes/ign_compat_functions.php' );
		require_once( 'includes/class-wc-wholesale-pro-discounts.php' );
		require_once( 'includes/class-wc-wholesale-pro-tax-exempt-users.php' );
		require_once( 'includes/class-wc-wholesale-emails.php' );
		

		if ( is_admin() ) {
			require_once( 'includes/class-wc-wholesale-pro-admin.php' );
			require_once( 'includes/class-wc-wholesale-pro-admin-settings.php' );
		}

		require_once( 'includes/class-wc-wholesale-pro-coupons.php' );
		require_once( 'includes/class-wc-wholesale-pro-minimum-order.php' );

		new IgniteWoo_Wholesale_Pro_Minimum_Cart_Total_Quantity();

		require_once( 'includes/class-wc-wholesale-pro-hide-content.php' );

		// Do not load if IgniteWoo Quick Order Forms is active on the site
		if ( !class_exists( 'IGN_Quick_Orders' ) )
			require_once( 'includes/class-wc-wholesale-pro-quick-order-form.php' );

 		add_filter( 'option_woocommerce_prices_include_tax', array( &$this, 'include_tax' ), 999, 1 );
		add_filter( 'option_woocommerce_tax_display_shop', array( &$this, 'display_shop' ), 999, 1 );
		add_filter( 'option_woocommerce_tax_display_cart', array( &$this, 'display_cart' ), 999, 1 );

		add_action( 'woocommerce_process_product_meta_simple', array( &$this, 'process_product_meta' ), 1, 1 );
		add_action( 'woocommerce_process_product_meta_bundle', array( &$this, 'process_product_meta' ), 1, 1 );
		add_action( 'woocommerce_process_product_meta_variable', array( &$this, 'process_product_meta_variable' ), 999, 1 );
/**
		// Regular price displays, before variations are selected by a buyer
		add_filter( 'woocommerce_grouped_price_html', array( &$this, 'maybe_return_wholesale_price' ), 1, 2 );
		add_filter( 'woocommerce_variable_price_html', array( &$this, 'maybe_return_wholesale_price' ), 1, 2 );
**/
		// Javscript related

		add_filter( 'woocommerce_variation_sale_price_html', array( &$this, 'maybe_return_variation_price' ), 1, 2 );
		add_filter( 'woocommerce_variation_price_html', array( &$this, 'maybe_return_variation_price' ), 1, 2 );
		add_filter( 'woocommerce_variable_empty_price_html', array( &$this, 'maybe_return_variation_price_empty' ), 999, 2 );

		add_filter( 'woocommerce_product_is_visible', array( &$this, 'variation_is_visible' ), 99999, 2 );

		// WC 3.x
		add_filter( 'woocommerce_variation_is_visible', array( &$this, 'variation_is_visible3x' ), 99999, 2 );
		add_filter( 'woocommerce_hide_invisible_variations', array( &$this, 'hide_invisible_variations' ), 999, 3 );

		add_filter( 'woocommerce_available_variation', array( &$this, 'maybe_adjust_variations' ), 1, 3 );

		add_filter( 'woocommerce_is_purchasable', array( &$this, 'is_purchasable' ), 1, 2 );

		add_filter( 'woocommerce_get_price_html', array( &$this, 'maybe_return_wholesale_price' ), 1, 2 );
		add_filter( 'woocommerce_sale_price_html', array( &$this, 'maybe_return_wholesale_price' ), 1, 2 );
		add_filter( 'wc_price_html', array( &$this, 'maybe_return_wholesale_price' ), 1, 2 );
		add_filter( 'woocommerce_empty_price_html', array( &$this, 'maybe_return_wholesale_price' ), 1, 2 );

		if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) ) {
			add_filter( 'woocommerce_get_product_price', array( &$this, 'maybe_return_price' ), 999, 2 );
			add_filter( 'woocommerce_product_get_price', array( &$this, 'maybe_return_price' ), 999, 2 );
			add_filter( 'woocommerce_product_variation_get_price', array( &$this, 'maybe_return_price' ), 999, 2 );

		} else {
			add_filter( 'woocommerce_get_price', array( &$this, 'maybe_return_price' ), 999, 2 );
		}
		
		add_filter( 'woocommerce_get_variation_price', array( &$this, 'maybe_return_var_price' ), 999, 4 );
		add_filter( 'woocommerce_get_variation_regular_price', array( &$this, 'maybe_return_var_price' ), 999, 4 );

		// ensure sale price is always empty for tier role buyers so that the price display doesn't include
		// a marked out regular retail price
		add_filter( 'woocommerce_get_price_html', array( &$this, 'maybe_get_tier_price_html' ), 999, 2 );

		
		add_filter( 'woocommerce_get_cart_item_from_session', array( &$this, 'get_item_from_session' ), 15, 1 );

		$this->settings = get_option( 'woocommerce_ignitewoo_wholesale_pro_suite_settings' );

		$defaults = array(
				'show_regular_price' => '',
				'show_savings' => '',
				'show_regular_price_label' => __( 'Regularly', 'ignitewoo_wholesale_pro' ),
				'show_savings_label' => __( 'You Save', 'ignitewoo_wholesale_pro' ),
		);

		$this->settings = wp_parse_args( $this->settings, $defaults );

		// Force the cart to recalculate so that the first item add into a the cart widgets results in the correct subtotal
		//add_action( 'wp_ajax_woocommerce_get_refreshed_fragments', array( &$this, 'get_refreshed_fragments' ), -1 );
		//add_action( 'wp_ajax_nopriv_woocommerce_get_refreshed_fragments', array( &$this, 'get_refreshed_fragments' ), -1 );
		//add_action( 'wc_ajax_get_refreshed_fragments', array( &$this, 'get_refreshed_fragments' ), -1 );

		add_shortcode( 'wholesale_pro_suite_register', array( &$this, 'show_register_form' ) );

		add_filter( 'woocommerce_payment_gateways', array( $this, 'register_gateway' ) );

		// this lets the plugin adjust the price so it shows up in the cart on the fly
		// triggers when someone clicks "Add to Cart" from a product page
		add_filter( 'woocommerce_get_cart_item_from_session', array( &$this, 'get_item_from_session' ), -1, 3 );

		add_filter( 'woocommerce_package_rates', array( &$this, 'get_available_shipping_methods' ), 999999, 2 );
		add_filter( 'woocommerce_available_payment_gateways', array( &$this, 'available_gateways' ), 999999 );

		// this helps with mini carts such as the one in the ShelfLife theme
		// gets accurate pricing into the session before theme displays it on the screen
		// helps when "add to cart" does not redirect to cart page immediately
		// Deprecate this?
		// add_action('woocommerce_before_calculate_totals', array( &$this, 'predisplay_calculate_and_set_session'), 9999999, 1 );

		// Backorders
		add_filter( 'woocommerce_product_backorders_allowed', array( &$this, 'maybe_allow_backorders' ), 9999, 2 );
		add_filter( 'woocommerce_product_is_in_stock', array( &$this, 'product_is_in_stock' ), 9999, 1 );
		add_filter( 'woocommerce_product_backorders_require_notification', array( &$this, 'product_backorders_require_notification' ), 9999, 2 );


		add_action( 'woocommerce_variable_product_bulk_edit_actions', array( &$this, 'bulk_edit' ) );
		add_action( 'woocommerce_product_after_variable_attributes', array( &$this, 'add_variable_attributes'), 1, 3 );
		add_action( 'woocommerce_product_options_pricing', array( &$this, 'add_simple_price' ), 1 );

		add_action( 'woocommerce_admin_order_data_after_billing_address', array( &$this, 'display_tax_vat_in_order' ), 1, 1 );
		add_action( 'woocommerce_new_order', array( &$this, 'add_tax_vat_to_order' ), 1, 1 );

		// adjust taxes to zero in cart and checkout
		//add_filter( 'woocommerce_get_cart_tax', array( &$this, 'get_cart_tax' ), 1, 1 );
		//add_filter( 'woocommerce_calculate_totals', array( &$this, 'calculate_totals' ), 999, 1 );
		add_action( 'woocommerce_init', array( &$this, 'maybe_override_tax_exempt' ), 1 );
		add_filter( 'woocommerce_matched_tax_rates', array( &$this, 'filter_matched_rates' ), 999999, 10 );

		add_filter( 'woocommerce_coupon_is_valid', array( &$this, 'disable_coupons' ), 99999, 2 );
		add_filter( 'woocommerce_coupon_error', array( &$this, 'disable_coupons_error_msg' ), 99999, 2 );

		add_action( 'woocommerce_thankyou_order_received_text', array( &$this, 'maybe_insert_thankyou_message' ), 99999, 2 );

		// WC 2.4 and newer:
		add_action( 'woocommerce_ajax_save_product_variations', array( &$this, 'ajax_process_product_meta_variable' ), 5 );

		if ( 'yes' == get_option( 'woocommerce_enable_myaccount_registration' ) )
			add_action( 'woocommerce_register_form_end', array( $this, 'maybe_add_myaccount_button' ) );
		else
			add_action( 'woocommerce_login_form_end', array( $this, 'maybe_add_myaccount_button' ) );

		if ( !empty( $this->settings['ignitewoo_wsp_add_btn_to_myaccount_logged_in'] ) && 'yes' == $this->settings['ignitewoo_wsp_add_btn_to_myaccount_logged_in'] )
			add_action( 'woocommerce_after_my_account', array( $this, 'maybe_add_myaccount_button' ) );

		if ( !empty( $this->settings['ignitewoo_wsp_disallow_login'] ) && 'yes' == $this->settings['ignitewoo_wsp_disallow_login'] ) { 
			add_action( 'wp_login', array( $this, 'maybe_disallow_login' ), -99, 2 );
		}
		
		add_action( 'wp_loaded', array( $this, 'process_registration' ), 20 );

		if ( version_compare( WOOCOMMERCE_VERSION, '3.2', '>=' ) ) 
			add_filter( 'woocommerce_product_query_meta_query', array( $this, 'loop_meta_query' ), 5, 2 );
		else 
			add_filter( 'loop_shop_post_in', array( $this, 'product_filter' ) );
		
		add_filter( 'the_posts', array( $this, 'the_posts' ), 11, 2 );
		
		add_filter( 'woocommerce_product_export_meta_value', array( $this, 'export_dd_pricing_rules' ), 10, 4 );
		add_filter( 'woocommerce_product_importer_formatting_callbacks', array( $this, 'import_dd_pricing_rules' ), 10, 2 );

		if ( 'yes' == $this->settings['display_when_no_retail'] ) {
			add_filter( 'woocommerce_is_purchasable', array( &$this, 'is_purchasable' ), 999999999, 2 );
			add_filter( 'woocommerce_empty_price_html', array( &$this, 'empty_price_html' ), 999999999, 2 );
		}

		if ( is_admin() ) {
			require_once( 'includes/class-wc-wholesale-pro-minimum-order-product-edit.php' );
			require_once( 'includes/class-wc-wholesale-pro-categories.php' );
			return;
		}
	}

	/*
	function get_refreshed_fragments() {
		WC()->cart->calculate_totals();
	}
	*/
	
	public function maybe_hide_price_init() {

		if ( !empty( $this->settings['login_required_for_prices'] ) && 'yes' == $this->settings['login_required_for_prices'] && !is_user_logged_in() ) {

			if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) ) {
				add_filter( 'woocommerce_get_price_html', array(&$this, 'replace_price_html'), 999999, 2 );
				add_filter( 'woocommerce_is_purchasable', array(&$this, 'disable_purchaseable'), 999999, 2 );
				add_filter( 'woocommerce_loop_add_to_cart_link',array(&$this, 'remove_add_to_cart_button'), 999999, 2 );
				remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

			} else {

				add_filter( 'woocommerce_is_purchasable', array(&$this, 'disable_purchaseable'), 999999, 2 );

				add_filter( 'woocommerce_grouped_price_html', array(&$this, 'replace_price_html'), 999999, 2 );
				add_filter( 'woocommerce_variable_price_html', array(&$this, 'replace_price_html'), 999999, 2 );
				add_filter( 'woocommerce_sale_price_html', array(&$this, 'replace_price_html'), 999999, 2 );
				add_filter( 'woocommerce_price_html', array(&$this, 'replace_price_html'), 999999, 2 );
				add_filter( 'woocommerce_empty_price_html', array(&$this, 'replace_price_html'), 999999, 2 );

				add_filter( 'woocommerce_variable_sale_price_html', array(&$this, 'replace_price_html'), 999999, 2 );
				add_filter( 'woocommerce_variable_free_sale_price_html', array(&$this, 'replace_price_html'), 999999, 2 );
				add_filter( 'woocommerce_variable_free_price_html', array(&$this, 'replace_price_html'), 999999, 2 );
				add_filter( 'woocommerce_variable_empty_price_html', array(&$this, 'replace_price_html'), 999999, 2 );

				add_filter( 'woocommerce_free_sale_price_html', array(&$this, 'replace_price_html'), 999999, 2 );
				add_filter( 'woocommerce_free_price_html', array(&$this, 'replace_price_html'), 999999, 2 );

				add_filter( 'woocommerce_loop_add_to_cart_link',array(&$this, 'remove_add_to_cart_button'), 999999, 2 );

				remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

			}
		}
	}

	public function replace_price_html( $price ) {
		if ( function_exists( 'is_product' ) && is_product() )
			$class = 'alt';
		else
			$class = '';
		if ( 'nothing' == $this->settings['price_alt_type'] ) {
			return false;
		} else if ( 'link' == $this->settings['price_alt_type'] && !empty( $this->settings['price_alt_link'] ) && !empty( $this->settings['price_alt_text'] ) ) {
			return '<a href="' . $this->settings['price_alt_link'] . '" class="wsp_link">' . $this->settings['price_alt_text'] . '</a>';
		} else if ( 'button' == $this->settings['price_alt_type'] && !empty( $this->settings['price_alt_link'] ) && !empty( $this->settings['price_alt_text'] ) ) {
			return '<a href="' . $this->settings['price_alt_link'] . '" class="button wsp_button ' . $class . '">' . $this->settings['price_alt_text'] . '</a>';
		}

		return false;
	}

	public function disable_purchaseable( $purchaseable ) {
		return false;
	}

	public function remove_add_to_cart_button( $button, $product ) {
		return false;
	}

	public function register_gateway( $methods ) {
		require_once( dirname( __FILE__ ) . '/includes/class-wc-wholesale-pro-invoice-gateway.php' );
		$methods[] = 'IgniteWoo_Wholesale_Pro_Invoice_Gateway';
		return $methods;
	}

	/*
	function predisplay_calculate_and_set_session( $stuff = '' ) {
		global $woocommerce;

		if ( WC()->cart->cart_contents )
			return;

		foreach( WC()->cart->cart_contents as $key => $item ) {

			$item_data = array();
			$item_data = $item['data'];

			// call our internal function to see if wholesale prices need to be set
			$item_data = $this->add_cart_item( $item );

		}

		// Set session data
		$_SESSION['cart'] = $woocommerce->cart->cart_contents;
		$_SESSION['coupons'] = $woocommerce->cart->applied_coupons;
		$_SESSION['cart_contents_total'] = $woocommerce->cart->cart_contents_total;
		$_SESSION['cart_contents_weight'] = $woocommerce->cart->cart_contents_weight;
		$_SESSION['cart_contents_count'] = $woocommerce->cart->cart_contents_count;
		$_SESSION['cart_contents_tax'] = $woocommerce->cart->cart_contents_tax;
		$_SESSION['total'] = $woocommerce->cart->total;
		$_SESSION['subtotal'] = $woocommerce->cart->subtotal;
		$_SESSION['subtotal_ex_tax'] = $woocommerce->cart->subtotal_ex_tax;
		$_SESSION['tax_total'] = $woocommerce->cart->tax_total;
		$_SESSION['shipping_taxes'] = $woocommerce->cart->shipping_taxes;
		$_SESSION['taxes'] = $woocommerce->cart->taxes;
		$_SESSION['discount_cart'] = $woocommerce->cart->discount_cart;
		$_SESSION['discount_total'] = $woocommerce->cart->discount_total;
		$_SESSION['shipping_total'] = $woocommerce->cart->shipping_total;
		$_SESSION['shipping_tax_total'] = $woocommerce->cart->shipping_tax_total;
		$_SESSION['shipping_label'] = isset( $woocommerce->cart->shipping_label ) ? $woocommerce->cart->shipping_label : '';

	}
	*/

	function format_price( $price = '' ) {

		if ( function_exists( 'wc_price' ) )
			return wc_price( $price );
		else
			return woocommerce_price( $price );
	}

	function maybe_override_tax_exempt() {
		global $current_user, $ignitewoo_remove_tax;
 
		if ( is_admin() && !defined( 'DOING_AJAX' ) )
			return;

		$current_user = $this->get_user();

		if ( empty( $current_user ) )
			return;

		if ( $current_user->has_cap( 'no_tax' ) && isset( WC()->customer ) && method_exists( WC()->customer, 'set_is_vat_exempt' ) )
			WC()->customer->set_is_vat_exempt( true );
		else if ( isset( WC()->customer ) && method_exists( WC()->customer, 'set_is_vat_exempt' ) )
			WC()->customer->set_is_vat_exempt( false );
	}

	function filter_matched_rates( $matched_tax_rates, $country, $state, $postcode, $city, $tax_class ) {
		global $current_user;

		// Don't let WP cache the rates since we need to filter them
		$cache_key = WC_Cache_Helper::get_cache_prefix( 'taxes' ) . 'wc_tax_rates_' . md5( sprintf( '%s+%s+%s+%s+%s', $country, $state, $city, $postcode, $tax_class ) );
		wp_cache_delete( $cache_key, 'taxes' );

		$current_user = $this->get_user();

		if ( empty( $current_user->roles ) )
			return $matched_tax_rates;

		if ( isset( $_POST['post_data'] ) ) {

			$args = wp_parse_args( $_POST['post_data'] );

			if ( !empty( $args ) && !empty( $args['ignitewoo_one_page_checkout_customer_id'] ) )
				$uid = absint( $args['ignitewoo_one_page_checkout_customer_id'] );

		}

		$user_tax_exemptions = '';

		foreach( $current_user->roles as $key ) {
			if ( !empty( $user_tax_exemptions ) )
				continue;
			$user_tax_exemptions = get_option( $key . '_tax_exemptions', '' );
		}

		if ( empty( $user_tax_exemptions ) )
			return $matched_tax_rates;

		$user_tax_exemptions = explode( ',', $user_tax_exemptions );

		foreach( $user_tax_exemptions as $id ) {
			if ( isset( $matched_tax_rates[ $id ] ) )
				unset( $matched_tax_rates[ $id ] );
		}

		return $matched_tax_rates;
	}

	/*
	function calculate_totals() {

		global  $woocommerce, $ignitewoo_remove_tax;

		$current_user = $this->get_user();

		if ( false == $ignitewoo_remove_tax ) return;

		//if ( !$current_user )
			$current_user = $this->get_user();

		if ( $current_user->has_cap( 'no_tax' ) ) {

			foreach ( $woocommerce->cart->cart_contents as &$line_item ) {

				$line_item['line_tax'] = 0;
				$line_item['line_subtotal_tax'] = 0;

			}

			$woocommerce->cart->tax_total = 0;
			$woocommerce->cart->shipping_tax_total = 0;
			$woocommerce->cart->taxes = array();
			$woocommerce->cart->shipping_taxes = array();

		}

	}

	function get_cart_tax( $amount ) {
		global  $woocommerce, $ignitewoo_remove_tax;

		if ( false == $ignitewoo_remove_tax ) return $amount;

		//if ( !$current_user )
			$current_user = $this->get_user();

		if ( $current_user->has_cap( 'no_tax' ) )
			return 0;

		return $amount;

	}
	*/

	function get_user() {
		global $pagenow, $current_user;

		if ( !empty( $pagenow ) )
			return $current_user;

		if ( empty( $_REQUEST['ignitewoo_one_page_checkout_customer_id'] ) && !empty( $_REQUEST['post_data'] ) ) {
			$args = wp_parse_args( $_REQUEST['post_data'] );
			if ( !empty( $args['ignitewoo_one_page_checkout_customer_id'] ) )
				$_REQUEST['ignitewoo_one_page_checkout_customer_id'] = $args['ignitewoo_one_page_checkout_customer_id'];
		}

		if ( isset( $_REQUEST['ignitewoo_one_page_checkout_customer_id'] ) && absint( $_REQUEST['ignitewoo_one_page_checkout_customer_id'] ) > 0 ) {

			$current_user = get_user_by( 'id', absint( $_REQUEST['ignitewoo_one_page_checkout_customer_id'] ) );

		} else if ( defined( 'DOING_AJAX' ) && isset( $_POST['post_data'] ) ) {

			$args = wp_parse_args( $_POST['post_data'] );

			if ( !empty( $args ) && isset( $args['ignitewoo_one_page_checkout_customer_id'] ) )
				$current_user = get_user_by( 'id', absint( $args['ignitewoo_one_page_checkout_customer_id'] ) );
			else {
				if ( function_exists( 'get_current_user' ) )
					$current_user = wp_get_current_user();
				else
					$current_user = get_currentuserinfo();
			}

			if ( empty( $current_user ) ) {
				if ( function_exists( 'get_current_user' ) )
					$current_user = wp_get_current_user();
				else
					$current_user = get_currentuserinfo();
			}

		} else {

			if ( empty( $current_user ) ) {
				if ( function_exists( 'get_current_user' ) )
					$current_user = wp_get_current_user();
				else
					$current_user = get_currentuserinfo();
			}
		}
		return $current_user;
	}

	function get_item_from_session( $item_data = '', $values = false, $key = false  ) {
		global $woocommerce, $ign_opc;

		$current_user = $this->get_user();

		$this->get_roles();

		if ( empty( $this->roles ) )
			return;

		if ( !empty( $ign_opc ) && ( !empty( $_REQUEST['post_data'] ) || !empty( $_POST['opc_checkout_flag'] ) ) ) {

			if ( !empty( $_REQUEST['post_data'] ) )
				$args = wp_parse_args( $_REQUEST['post_data'] );
			else
				$args['opc_checkout_flag'] = $_POST['opc_checkout_flag'];

			if ( !empty( $args['opc_checkout_flag'] ) ) {
				$ign_opc->switch_user_context_to_customer( 'customer' );
				$current_user = $this->get_user();
				//$ign_opc->switch_user_context_to_customer( 'opc_user' );
			}
		}

		$_product = $item_data['data'];
		if ( !is_object( $_product ) )
			$_product = wc_get_product( $item_data['product_id'] );

		foreach( $this->roles as $role => $name ) {

			if ( !$current_user->has_cap( $role ) )
				continue;

			if ( isset( $item_data['variation_id'] ) && 'variable' == $_product->get_type() )
				$level_price = get_post_meta( $item_data['variation_id' ], '_' . $role . '_price', true );
			else if ( isset( $item_data['variation_id'] ) && 'variation' == $_product->get_type() )
				$level_price = get_post_meta( $item_data['variation_id' ], '_' . $role . '_price', true );
			else if ( 'simple' == $_product->get_type() || 'external' == $_product->get_type() )
				$level_price = get_post_meta( $item_data['product_id' ], '_' . $role . '_price', true );

			else // all other product types - possibly incompatible with custom product types added by other plugins
				$level_price = get_post_meta( $item_data['product_id' ], '_' . $role . '_price', true );

			if ( $level_price ) {
				if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) {
					$item_data['data']->set_price( $level_price );
					$item_data['data']->set_regular_price( $level_price );
				} else {
					$item_data['data']->price = $level_price;
					$item_data['data']->regular_price = $level_price;
				}
			}
		}

		return $item_data;
	}

	// Returns unformated price
	function maybe_return_tier_price_for_addons( $price, $pid ) {
		global $ign_opc;

		$current_user = $this->get_user();

		$this->get_roles();

		if ( empty( $this->roles ) )
			return;

		if ( !empty( $ign_opc ) && ( !empty( $_REQUEST['post_data'] ) || !empty( $_POST['opc_checkout_flag'] ) ) ) {

			if ( !empty( $_REQUEST['post_data'] ) )
				$args = wp_parse_args( $_REQUEST['post_data'] );
			else
				$args['opc_checkout_flag'] = $_POST['opc_checkout_flag'];

			if ( !empty( $args['opc_checkout_flag'] ) ) {
				$ign_opc->switch_user_context_to_customer( 'customer' );
				$current_user = $this->get_user();
				//$ign_opc->switch_user_context_to_customer( 'opc_user' );
			}
		}

		foreach( $this->roles as $role => $name ) {

			if ( !$current_user->has_cap( $role ) )
				continue;

			$price = get_post_meta( $pid, '_' . $role . '_price', true );
		}

		if ( empty( $price ) )
			return 0;
		else
			return $price;

	}

	function maybe_return_wholesale_price( $price, $_product ) {
		global $ign_opc;

		//if ( !isset( $current_user->ID ) )
			$current_user = $this->get_user();

		$this->get_roles();

		if ( empty( $this->roles ) )
			return $price;

		if ( !empty( $ign_opc ) && ( !empty( $_REQUEST['post_data'] ) || !empty( $_POST['opc_checkout_flag'] ) ) ) {

			if ( !empty( $_REQUEST['post_data'] ) )
				$args = wp_parse_args( $_REQUEST['post_data'] );
			else
				$args['opc_checkout_flag'] = $_POST['opc_checkout_flag'];

			if ( !empty( $args['opc_checkout_flag'] ) ) {
				$ign_opc->switch_user_context_to_customer( 'customer' );
				$current_user = $this->get_user();
				//$ign_opc->switch_user_context_to_customer( 'opc_user' );
			}
		}

		foreach( $this->roles as $role => $name ) {

			if ( !$current_user->has_cap( $role ) )
				continue;

			$vtype = 'variable';

			if ( $_product->is_type('grouped') ) {

				$min_price = '';
				$max_price = '';

				foreach ( $_product->get_children() as $child_id ) {

					$child_price = get_post_meta( $child_id, '_' . $role . '_price', true );

					if ( !$child_price )
						continue;

					if ( $child_price < $min_price || $min_price == '' ) $min_price = $child_price;

					if ( $child_price > $max_price || $max_price == '' ) $max_price = $child_price;

				}
				
				$price_format = get_woocommerce_price_format();
				$min_price = sprintf( $price_format, '<span class="woocommerce-Price-currencySymbol">' . get_woocommerce_currency_symbol( '' ) . '</span>', $min_price );
					
				$price = '<span class="from">' . __('From:', 'ignitewoo_wholesale_pro') . ' </span>' . $min_price;

			} elseif ( $_product->is_type( $vtype ) ) {

				$id = ign_get_product_id( $_product, false );

				$wprice_min = get_post_meta( $id, 'min_variation_' . $role . '_price', true );

				$wprice_max = get_post_meta( $id, 'max_variation_' . $role . '_price', true );

				if ( $wprice_min !== $wprice_max )
					$price = '<span class="from">' . __( 'From:', 'ignitewoo_wholesale_pro') . $wprice_min . ' </span>';

				if ( !empty( $wprice_min ) && !empty( $wprice_max ) && $wprice_min == $wprice_max )
					return $price;

				else if ( !empty( $wprice_min ) )
					$price = '<span class="from">' . __( 'From:', 'ignitewoo_wholesale_pro') . ' ' . $this->format_price( $wprice_min ) . ' </span>';

				else {

					$wprice_min = get_post_meta( $id, '_min_variation_regular_price', true );

					$wprice_max = get_post_meta( $id, '_max_variation_regular_price', true );
					
					if ( $wprice_min !== $wprice_max ) {
						
						$price_format = get_woocommerce_price_format();
						
						$wprice_min = sprintf( $price_format, '<span class="woocommerce-Price-currencySymbol">' . get_woocommerce_currency_symbol( '' ) . '</span>', $wprice_min );
						
						$price = '<span class="from">' . __( 'From:', 'ignitewoo_wholesale_pro') . $wprice_min . ' </span>';
					}
					if (  !empty( $wprice_min ) && !empty( $wprice_max ) && $wprice_min == $wprice_max )
						return $price;

					else if ( !empty( $wprice_min ) ) { 
					
						$price_format = get_woocommerce_price_format();
						
						$wprice_min = sprintf( $price_format, '<span class="woocommerce-Price-currencySymbol">' . get_woocommerce_currency_symbol( '' ) . '</span>', $wprice_min );
						
						$price = '<span class="from">' . __( 'From:', 'ignitewoo_wholesale_pro') . ' ' . $wprice_min . ' </span>';
					}

				}

			} else {

				$id = ign_get_product_id( $_product, false );

				$wprice_min = get_post_meta( $id, '_' . $role . '_price', true );

				if ( isset( $wprice_min ) && $wprice_min > 0 )
					$price = $this->format_price( $wprice_min );

				elseif ( '' === $wprice_min ) {

					$price = get_post_meta( $id, '_price', true );

					if ( !empty( $price ) )
						$price = $this->format_price( $price );

				} elseif ( 0 == $wprice_min )
					$price = __( 'Free!', 'ignitewoo_wholesale_pro' );

				if ( !empty( $wprice_min ) && 'yes' == $this->settings['show_regular_price'] || 'yes' == $this->settings['show_savings'] ) {

					$rprice = get_post_meta( $id, '_regular_price', true );

					if ( empty( $wprice_min ) )
						continue;

					if ( floatval( $rprice ) > floatval( $wprice_min ) && 'yes' == $this->settings['show_regular_price'] )
						$price .= '<br><span class="normal_price">' . $this->settings['show_regular_price_label'] . ' ' . $this->format_price( $rprice ) . '</span>';

					$savings = ( floatval( $rprice ) - floatval( $wprice_min ) );

					if ( ( $savings < $rprice ) && 'yes' == $this->settings['show_savings'] )
						$price .= '<br><span class="normal_price savings">' . $this->settings['show_savings_label'] . ' ' . $this->format_price( $savings ) . '</span>';

				}
			}

		}

		return $price;
	}

	function is_purchasable( $purchasable, $_product ) {
		global $ign_opc;

		$current_user = $this->get_user();

		if ( !empty( $_REQUEST['post_data'] ) ) {
			$args = wp_parse_args( $_REQUEST['post_data'] );
			if ( !empty( $args['opc_checkout_flag'] ) )
				return true;
		} else if ( !empty( $_REQUEST['ignitewoo_one_page_checkout_customer_id'] ) ) {
			WC()->session->set( 'ign_opc_customer_id', absint( $_REQUEST['ignitewoo_one_page_checkout_customer_id'] ) );
			WC()->session->save_data();
			return true;
		} else {
			$args['opc_checkout_flag'] = false;
		}

		if ( empty( $args['opc_checkout_flag'] ) ) {
			if ( empty( $this->settings['display_when_no_retail'] ) || 'yes' != $this->settings['display_when_no_retail'] )
				return $purchasable;
		}

		$this->get_roles();

		if ( empty( $this->roles ) )
			return $purchasable;

		if ( isset( $_REQUEST['one_page_customer_user'] ) ) {
			return true;
		}

		$temp_user = false;

		if ( !empty( $ign_opc ) && ( !empty( $_REQUEST['post_data'] ) || !empty( $_REQUEST['ignitewoo_one_page_checkout_customer_id'] ) ) ) {

			if ( !empty( $_REQUEST['post_data'] ) ) {
				$args = wp_parse_args( $_REQUEST['post_data'] );
				if ( !empty( $args['ignitewoo_one_page_checkout_customer_id'] ) )
					$_REQUEST['ignitewoo_one_page_checkout_customer_id'] = $args['ignitewoo_one_page_checkout_customer_id'];
			}

			if ( !empty( $_REQUEST['ignitewoo_one_page_checkout_customer_id'] ) ) {
				$temp_user = $current_user;
				$current_user = get_user_by( 'id', absint( $_REQUEST['ignitewoo_one_page_checkout_customer_id'] ) );
				WC()->session->set( 'ign_opc_customer_id', absint( $_REQUEST['ignitewoo_one_page_checkout_customer_id'] ) );
				WC()->session->save_data();
			}

		} else {
			$session = WC()->session;
			if ( isset( $session ) && method_exists( $session, 'get' ) )
				$ign_opc_customer_id = WC()->session->get( 'ign_opc_customer_id' );
			if ( !empty( $ign_opc_customer_id ) )
				$current_user = get_user_by( 'id', $ign_opc_customer_id );
		}

		foreach( $this->roles as $role => $name ) {

			if ( !$current_user->has_cap( $role ) )
				continue;

			$is_variation = $_product->is_type( 'variation' );

			if ( !$is_variation )
				$is_variation = $_product->is_type( 'variable' );

			if ( $is_variation  ) {

				$id = ign_get_product_id( $_product, true );

				// Variable products
				if ( !isset( $id ) )
					return $purchasable;

				$price = get_post_meta( $id, 'min_variation_' . $role . '_price', true );

				if ( !isset( $price ) )
					return $purchasable;

			} else {

				// Simple products

				$id = ign_get_product_id( $_product, false );

				$price = get_post_meta( $id, '_' . $role . '_price', false );

				if ( !empty( $price ) )
					return true;
				else
					return $purchasable;


			}
		}

		if ( !empty( $temp_user ) )
			$current_user = $temp_user;

		return $purchasable;
	}


	function maybe_allow_backorders( $allow, $product_id ) {
		global $ign_opc;

		$current_user = $this->get_user();

		$this->get_roles();

		if ( empty( $this->roles ) )
			return $purchasable;

		if ( !empty( $ign_opc ) && ( !empty( $_REQUEST['post_data'] ) || !empty( $_POST['opc_checkout_flag'] ) ) ) {

			if ( !empty( $_REQUEST['post_data'] ) )
				$args = wp_parse_args( $_REQUEST['post_data'] );
			else
				$args['opc_checkout_flag'] = $_POST['opc_checkout_flag'];

			if ( !empty( $args['opc_checkout_flag'] ) ) {
				$ign_opc->switch_user_context_to_customer( 'customer' );
				$current_user = $this->get_user();
				//$ign_opc->switch_user_context_to_customer( 'opc_user' );
			}
		}

		foreach( $this->roles as $role => $name ) {

			if ( !$current_user->has_cap( $role ) )
				continue;

			if ( $current_user->has_cap( 'backorders' ) )
				return true;
		}

		return $allow;
	}

	function product_is_in_stock( $status ) {
		// No need to send any sort of $allow pr $product_id to maybe_allow_backorders() because it's global based on the role
		if ( $this->maybe_allow_backorders( null, null ) )
			return 'instock';
		else
			return $status;
	}

	function product_backorders_require_notification( $allow, $product ) {
		if ( $this->maybe_allow_backorders( $allow, ign_get_product_id( $product, false ) ) )
			return true;
		else
			return $allow;
	}

	function include_tax( $val = null ) {
		global $wp_roles, $current_user, $user_ID;

		if ( is_admin() && !defined( 'DOING_AJAX' ) )
			return $val;

		if ( !isset( $current_user->ID ) )
			if ( function_exists( 'wp_get_current_user' ) )
				wp_get_current_user();
			else
				get_currentuserinfo();

		$user_role = false;

		if ( isset( $wp_roles->roles ) ) {
			foreach( $wp_roles->roles as $role => $data ) {

				if ( 'ignite_level_' != substr( $role, 0, 13 ) )
					continue;

				$has_role = current_user_can( $role );

				if ( $has_role )
					$user_role = $role;
			}
		}

		if ( empty( $user_role ) )
			return $val;

		return !empty( $this->settings['include_tax'] ) ? $this->settings['include_tax'] : 'excl';

	}


	function display_cart( $val = null ) {
		global $wp_roles, $current_user;

		if ( is_admin() && !defined( 'DOING_AJAX' ) )
			return $val;

		if ( !isset( $current_user->ID ) )
			if ( function_exists( 'wp_get_current_user' ) )
				wp_get_current_user();
			else
				get_currentuserinfo();


		$user_role = false;

		if ( isset( $wp_roles->roles ) ) {
			foreach( $wp_roles->roles as $role => $data ) {

				if ( 'ignite_level_' != substr( $role, 0, 13 ) )
					continue;

				$has_role = current_user_can( $role );

				if ( $has_role )
					$user_role = $role;
			}
		}

		if ( empty( $user_role ) )
			return $val;

		return !empty( $this->settings['display_cart'] ) ? $this->settings['display_cart'] : 'excl';
	}


	function display_shop( $val = null ) {
		global $wp_roles, $current_user;

		if ( is_admin() && !defined( 'DOING_AJAX' ) )
			return $val;

		if ( !isset( $current_user->ID ) )
			if ( function_exists( 'wp_get_current_user' ) )
				wp_get_current_user();
			else
				get_currentuserinfo();

		$user_role = false;

		if ( isset( $wp_roles->roles ) ) {
			foreach( $wp_roles->roles as $role => $data ) {

				if ( 'ignite_level_' != substr( $role, 0, 13 ) )
					continue;

				$has_role = current_user_can( $role );

				if ( $has_role )
					$user_role = $role;
			}
		}

		if ( empty( $user_role ) )
			return $val;

		return !empty( $this->settings['display_shop'] ) ? $this->settings['display_shop'] : 'excl';
	}

	function maybe_get_global_discount_price( $price, $product, $role ) {

		// First check category discounts
		$id = ign_get_product_id( $product, false );

		$terms = get_the_terms( $id, 'product_cat' );

		$discount = false;

		// Last category in the list that has a discount for the role becomes the discount used
		if ( !empty( $terms ) ) {
			foreach( $terms as $term ) {
				$tid = $term->term_id;
				$discounts = get_option( 'product_cat_wsp_discount_' . $tid, false );
				if ( !empty( $discounts[ $role ] ) )
					$discount = round( floatval( $discounts[ $role ] ), 6 ) / 100;
			}
		}

		if ( !empty( $discount ) ) {
			$price = $price - ( $price * $discount );

			return $price;
		}

		// Check for a global discount
		if ( !empty( $this->settings['global_discounts'][ $role ] ) ) {
			// convert to decimal
			$discount = round( floatval( $this->settings['global_discounts'][ $role ] ), 6 ) / 100;

			$price = $price - ( $price * $discount );
		}

		return $price;

	}

	function maybe_get_price_with_tax( $price, $_product ) {
		$qty = 1;

		if ( $_product->is_taxable() ) {

			if ( 'no' == $this->settings['include_tax']  ) {

				if ( ! empty( WC()->customer ) && !WC()->customer->is_vat_exempt() ) {

					$tax_rates  = WC_Tax::get_rates( $_product->get_tax_class() );
					$taxes      = WC_Tax::calc_tax( $price * $qty, $tax_rates, false );

					$tax_amount = WC_Tax::get_tax_total( $taxes );

					$price      = round( $price * $qty + $tax_amount, wc_get_price_decimals() );
				}

			} else {

				$tax_rates      = WC_Tax::get_rates( $_product->get_tax_class() );
				$base_tax_rates = WC_Tax::get_base_tax_rates( $_product->get_tax_class() );

				if ( ! empty( WC()->customer ) && WC()->customer->is_vat_exempt() ) {

					$base_taxes         = WC_Tax::calc_tax( $price * $qty, $base_tax_rates, true );
					$base_tax_amount    = array_sum( $base_taxes );
					$price              = round( $price * $qty - $base_tax_amount, wc_get_price_decimals() );

				/**
				* The woocommerce_adjust_non_base_location_prices filter can stop base taxes being taken off when dealing with out of base locations.
				* e.g. If a product costs 10 including tax, all users will pay 10 regardless of location and taxes.
				* This feature is experimental @since 2.4.7 and may change in the future. Use at your risk.
				*/
				} elseif ( $tax_rates !== $base_tax_rates && apply_filters( 'woocommerce_adjust_non_base_location_prices', true ) ) {

					$base_taxes         = WC_Tax::calc_tax( $price * $qty, $base_tax_rates, true );
					$modded_taxes       = WC_Tax::calc_tax( ( $price * $qty ) - array_sum( $base_taxes ), $tax_rates, false );
					$price              = round( ( $price * $qty ) - array_sum( $base_taxes ) + array_sum( $modded_taxes ), wc_get_price_decimals() );

				} else {

					$price = $price * $qty;

				}

			}
		}

		return $price;
	}

	function maybe_get_price_excluding_tax( $price = '', $product ) {
		$qty = 1;

		if ( $price === '' ) {
			$price = $this->get_price();
		}
		$price = (float) $price;

		if ( $product->is_taxable() && 'yes' === $this->settings['include_tax'] ) {
			// Not necessary, this results in double tax remove when the setting is "I enter prices including tax"
			//$tax_rates  = WC_Tax::get_base_tax_rates( $product->get_tax_class() );
			//$taxes      = WC_Tax::calc_tax( $price * $qty, $tax_rates, true );
			//$price      = WC_Tax::round( ( $price * $qty ) - array_sum( $taxes ) );
		} else {
			$price = $price * $qty;
		}

		return $price;
		// return apply_filters( 'woocommerce_get_price_excluding_tax', $price, $qty, $this );
	}

	function maybe_get_tier_price_html( $price_html, $_product ) {
		global $ign_opc;

		$current_user = $this->get_user();

		$this->get_roles();

		if ( empty( $this->roles ) )
			return $price_html;

		if ( !empty( $ign_opc ) && ( !empty( $_REQUEST['post_data'] ) || !empty( $_POST['opc_checkout_flag'] ) ) ) {

			if ( !empty( $_REQUEST['post_data'] ) )
				$args = wp_parse_args( $_REQUEST['post_data'] );
			else
				$args['opc_checkout_flag'] = $_POST['opc_checkout_flag'];

			if ( !empty( $args['opc_checkout_flag'] ) ) {
				$ign_opc->switch_user_context_to_customer( 'customer' );
				$current_user = $this->get_user();
				//$ign_opc->switch_user_context_to_customer( 'opc_user' );
			}
		}

		foreach( $this->roles as $role => $name ) {

			if ( !$current_user->has_cap( $role ) )
				continue;

			if ( $_product->is_type( 'variable' ) ) {

				$min = $this->maybe_return_var_price( $price = null, $_product, 'min', false );
				$max = $this->maybe_return_var_price( $price = null, $_product, 'max', false );
				
				if ( '' !== $max || '' !== $min ) { 
				
					$min = wc_format_localized_price( $min );
					$max = wc_format_localized_price( $max );
					
					$price_format = get_woocommerce_price_format();
						
					if ( $min == $max ) {

						//$min = sprintf( $price_format, '<span class="woocommerce-Price-currencySymbol">' . get_woocommerce_currency_symbol( '' ) . '</span>', $min );
						
						$price_html = wc_price( $min ); //'<p class="price"><span class="amount">' . $min . '</span></p>';

					} else {

						//$min = sprintf( $price_format, '<span class="woocommerce-Price-currencySymbol">' . get_woocommerce_currency_symbol( '' ) . '</span>', $min );
						//$max = sprintf( $price_format, '<span class="woocommerce-Price-currencySymbol">' . get_woocommerce_currency_symbol( '' ) . '</span>', $max );
						
						$price_html = wc_price( $min ) . '&ndash;' . wc_price( $max );
						//$price_html = '<p class="price"><span class="amount">' . $min . ' </span>&ndash;<span class="amount">' . $max . '</span></p>';
					}
				}
				
				return $price_html;

			} else { // non-variable products

				$product_price = ign_get_product_price( $_product );

				$price = $this->maybe_return_price( $product_price, $_product );

				if ( '' !== $price ) { 
					$price_html = '<span class="woocommerce-Price-amount amount">' . wc_price( $price ) . '</span>';

					if ( floatval( $product_price ) > floatval( $price ) && 'yes' == $this->settings['show_regular_price'] )
						$price_html .= '<br><span class="normal_price">' . $this->settings['show_regular_price_label'] . ' ' . $this->format_price( $product_price ) . '</span>';

					$savings = ( floatval( $product_price ) - floatval( $price ) );

					if ( ( $savings < $product_price ) && 'yes' == $this->settings['show_savings'] )
						$price_html .= '<br><span class="normal_price savings">' . $this->settings['show_savings_label'] . ' ' . $this->format_price( $savings ) . '</span>';
				}
				
				// Not implemented - WooCommerce Currency Converter support
				//if ( !empty( $GLOBALS['WOOCS'] ) )
				//	$price_html = $GLOBALS['WOOCS']->woocommerce_price_html( $price_html, $_product );
				//else
					return $price_html;
				/*
				$pos = strpos( $price_html, '</del>' );

				if ( $pos !== false )
					$price_html = trim( substr( $price_html, $pos, strlen( $price_html ) ) );

				$price_html = str_replace( array( '<ins>', '</ins>'), '', $price_html );

				return $price_html;
				*/
			}

		}

		return $price_html;
	}

	function maybe_return_price( $price = '', $_product ) {
		global $ign_opc;

		// This may be necessary for certain conditions, leave it commented out until we test completely.
		//if ( is_cart() || is_checkout() )
		//	return $price;

		//if ( !isset( $current_user->ID ) )
			$current_user = $this->get_user();

		$this->get_roles();

		if ( empty( $this->roles ) )
			return $price;

		if ( !empty( $ign_opc ) && ( !empty( $_REQUEST['post_data'] ) || !empty( $_POST['opc_checkout_flag'] ) ) ) {

			if ( !empty( $_REQUEST['post_data'] ) )
				$args = wp_parse_args( $_REQUEST['post_data'] );
			else
				$args['opc_checkout_flag'] = $_POST['opc_checkout_flag'];

			if ( !empty( $args['opc_checkout_flag'] ) ) {
				$ign_opc->switch_user_context_to_customer( 'customer' );
				$current_user = $this->get_user();
				//$ign_opc->switch_user_context_to_customer( 'opc_user' );
			}
		}

		foreach( $this->roles as $role => $name ) {

			if ( !$current_user->has_cap( $role ) )
				continue;

			if ( $_product->is_type( 'variation' ) )
				$id = ign_get_product_id( $_product, true );
			else
				$id = ign_get_product_id( $_product );

			$product_price = ign_get_product_price( $_product );

			/*
			if ( isset( $id ) ) {

				$price = get_post_meta( $id, '_' . $role . '_price', true );

				if ( intval( $price ) > 0 && version_compare( WOOCOMMERCE_VERSION, '2.2', '<' ) )
					$_product->product_custom_fields[ '_' . $role . '_price' ] = array( $price );

				if ( isset( $_product->product_custom_fields[ '_' . $role . '_price' ] ) && is_array( $_product->product_custom_fields[ '_' . $role . '_price'] ) && $_product->product_custom_fields[ '_' . $role . '_price'][0] > 0 ) {

					if ( version_compare( WOOCOMMERCE_VERSION, '2.2', '<' ) )
						$price = $_product->product_custom_fields[ '_' . $role . '_price'][0];

				} elseif ( '' === $product_price ) {

					$price = $this->maybe_get_global_discount_price( $price, $_product, $role );

					// $price = '';

				} elseif ( $product_price == 0 ) {

					$price = __( 'Free!', 'ignitewoo_wholesale_pro' );

				} else if ( '' !== $price ) {

					return $price;

				}

				$price = $this->maybe_get_global_discount_price( $price, $_product, $role );

				return $price;

			}
			*/
			$tier_price = get_post_meta( $id, '_' . $role . '_price', true );

			if ( '' === $tier_price || empty( $tier_price ) ) {

				// This helps with the phone/manual order software so that a product is only ever discounted once.
				// We track the discount for each product.
				if ( empty( $this->global_discounted[ $id ] ) ) { 
					$price = $this->maybe_get_global_discount_price( $price, $_product, $role );
					$this->global_discounted[ $id ] = $price;
				} else {
					$price = $this->global_discounted[ $id ];
				}

				return $price;

			} else {

				$tax_display_mode = $this->settings['display_shop'];
				$tier_price = $tax_display_mode == 'incl' ? $this->maybe_get_price_with_tax( $tier_price, $_product ) : $this->maybe_get_price_excluding_tax( $tier_price, $_product );

				//$tier_price = $this->maybe_get_price_with_tax( $tier_price, $_product );

				return $tier_price;
			}

			//$rprice = get_post_meta( $_product->id, '_' . $role . '_price', true );

			//if ( !empty( $rprice ) )
			//	return $rprice;
		}

		return $price;

	}


	function maybe_return_var_price( $price, $_variation, $min_or_max, $display ) {
		global $ign_opc;

		//if ( !isset( $current_user->ID ) )
			$current_user = $this->get_user();

		if ( empty( $this->roles ) )
			return $price;

		if ( !empty( $ign_opc ) && ( !empty( $_REQUEST['post_data'] ) || !empty( $_POST['opc_checkout_flag'] ) ) ) {

			if ( !empty( $_REQUEST['post_data'] ) )
				$args = wp_parse_args( $_REQUEST['post_data'] );
			else
				$args['opc_checkout_flag'] = $_POST['opc_checkout_flag'];

			if ( !empty( $args['opc_checkout_flag'] ) ) {
				$ign_opc->switch_user_context_to_customer( 'customer' );
				$current_user = $this->get_user();
				//$ign_opc->switch_user_context_to_customer( 'opc_user' );
			}
		}

		foreach( $this->roles as $role => $name ) {

			if ( !$current_user->has_cap( $role ) )
				continue;

			$low_price = 999999999;

			$price_high = 0;

			$cid = null;

			foreach ( $_variation->get_children() as $child_id ) {

				$p = floatval( get_post_meta( $child_id, '_' . $role . '_price', true ) );

				if ( empty( $p ) ) {
					$p = floatval( get_post_meta( $child_id, '_price', true ) );
					
					if ( empty( $this->global_discounted[ $child_id ] ) ) { 
						$p = $this->maybe_get_global_discount_price( $p, $_variation, $role );
						$this->global_discounted[ $child_id ] = $p;
					} else { 
						$p = $this->global_discounted[ $child_id ];
					}
				}

				if ( 'max' == $min_or_max && ( $p > (float)$price_high )  ) {

					$price_high = $p;
					$cid = $child_id;
				}

				if ( 'min' == $min_or_max && ( $p < (float)$low_price )  ) {

					$low_price = $p;
					$cid = $child_id;
				}
			}

			$variation_id = $cid;

			if ( $display ) {

				$variation = $_variation->get_child( $variation_id );

				if ( $variation ) {

					$tax_display_mode = get_option( 'woocommerce_tax_display_shop' );

					if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
						$price = $tax_display_mode == 'incl' ? wc_get_price_including_tax( $variation ) : wc_get_price_excluding_tax( $variation );
					else
						$price = $tax_display_mode == 'incl' ? $variation->get_price_including_tax() : $variation->get_price_excluding_tax();

				} else {

					if ( empty( $this->global_discounted[ $variation_id ] ) ) { 
						$price = $this->maybe_get_global_discount_price( $price, $_variation, $role );
						$this->global_discounted[ $variation_id ] = $price;
					} else { 
						$price = $this->global_discounted[ $variation_id ];
					}

				}

			} else {

				$price = get_post_meta( $variation_id, '_' . $role . '_price', true );

				if ( '' === $price ) {
					$price = floatval( get_post_meta( $variation_id, '_price', true ) );
					if ( empty( $this->global_discounted[ $variation_id ] ) ) { 
						$price = $this->maybe_get_global_discount_price( $price, $_variation, $role );
						$this->global_discounted[ $variation_id ] = $price;
					} else { 
						$price = $this->global_discounted[ $variation_id ];
					}
				}
			}

			break;

		}

//var_dump( $min_or_max, $price );
		return $price;

	}
	
	function variation_is_visible3x( $visible, $vid ) {

		if ( 'yes' !== $this->settings['tier_filter'] && 'yes' !== $this->settings['retail_filter']  ) {		
			return $visible;
		}
		
		$current_user = $this->get_user();

		$this->get_roles();

		if ( empty( $this->roles ) )
			return;

		$is_wholesale = false; 
		
		if ( 'yes' == $this->settings['tier_filter'] ) { 
			foreach( $this->roles as $role => $name ) {

				if ( !$current_user->has_cap( $role ) ) {
					continue;
				}
				
				$is_wholesale = true;
				
				$price = get_post_meta( $vid, '_' . $role . '_price', true );

				if ( empty( $price ) )
					return false; 
			}
		}
		
		if ( 'yes' == $this->settings['retail_filter'] && !$is_wholesale ) {
			$prod = wc_get_product( $vid );
			
			$price = ign_get_product_price( $prod );
			
			if ( empty( $price ) )
				return false;
		}

		return $visible;
	}
	
	function hide_invisible_variations( $hide, $id, $variation ) {

		if ( 'yes' !== $this->settings['tier_filter'] && 'yes' !== $this->settings['retail_filter']  ) {		
			return $hide;
		}
	
		$current_user = $this->get_user();

		$this->get_roles();

		if ( empty( $this->roles ) )
			return;
			
		$is_wholesale = false; 
		
		$vid = ign_get_product_id( $variation );
		
		if ( 'yes' == $this->settings['tier_filter'] ) { 
			foreach( $this->roles as $role => $name ) {

				if ( !$current_user->has_cap( $role ) ) {
					continue;
				}
				
				$is_wholesale = true;
				
				$price = get_post_meta( $vid, '_' . $role . '_price', true );

				if ( empty( $price ) )
					return true; 
			}
		}	
		
		if ( 'yes' == $this->settings['retail_filter'] && !$is_wholesale ) {
			$prod = wc_get_product( $vid );
			
			$price = ign_get_product_price( $prod ); 
			if ( empty( $price ) )
				return true;;
		}
		
		return $hide; 
	}

	function maybe_adjust_variations( $variation = '', $obj = '' , $variation_obj  = '') {
		global $ign_opc;
		//if ( !isset( $current_user->ID ) )
			$current_user = $this->get_user();

		$this->get_roles();

		if ( empty( $this->roles ) )
			return;

		if ( !empty( $ign_opc ) && ( !empty( $_REQUEST['post_data'] ) || !empty( $_POST['opc_checkout_flag'] ) ) ) {

			if ( !empty( $_REQUEST['post_data'] ) )
				$args = wp_parse_args( $_REQUEST['post_data'] );
			else
				$args['opc_checkout_flag'] = $_POST['opc_checkout_flag'];

			if ( !empty( $args['opc_checkout_flag'] ) ) {
				$ign_opc->switch_user_context_to_customer( 'customer' );
				$current_user = $this->get_user();
				//$ign_opc->switch_user_context_to_customer( 'opc_user' );
			}
		}

		$product_id = ign_get_product_id( $variation_obj, false );
		$variation_id = ign_get_product_id( $variation_obj, true );

		foreach( $this->roles as $role => $name ) {

			if ( !$current_user->has_cap( $role ) ) {
				continue;
			}

			$id = ign_get_product_id( $variation_obj, true );

			$price = get_post_meta( $id, '_' . $role . '_price', true );

			if ( empty( $price ) ) {
				$variation_price = ign_get_product_price( $variation_obj );
				
				if ( empty( $this->global_discounted[ $id ] ) ) { 
					$variation_price = $this->maybe_get_global_discount_price( $variation_price, $variation_obj, $role );
					$this->global_discounted[ $id ] = $variation_price;
				} else { 
					$variation_price = $this->global_discounted[ $id ];
				}

			}
			

			/*
			$thousands_sep = get_option( 'woocommerce_price_thousand_sep' );
			$decimal_sep = get_option( 'woocommerce_price_decimal_sep' );
			
			// For EU style number formatting, convert it to US first, then further below convert it back using number_format()
			if ( '.' == $thousands_sep ) { 
				$price = str_replace( '.', '', $price );
				$price = str_replace( ',', '.', $price );
			}
			*/
			
			$tax_display_mode = get_option( 'woocommerce_tax_display_shop' );

			if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
				$price = $tax_display_mode == 'incl' ? wc_get_price_including_tax( $variation_obj, array( 'qty' => 1, 'price' => $price ) ) : wc_get_price_excluding_tax( $variation_obj, array( 'qty' => 1, 'price' => $price ) );

			else
				$price = $tax_display_mode == 'incl' ? $variation_obj->get_price_including_tax( 1, $price ) : $variation_obj->get_price_excluding_tax( 1, $price );

			$price = wc_format_localized_price( $price );

			/*
			if ( '.' == $thousands_sep ) { 
				$price = number_format( $price, 2, $decimal_sep, $thousands_sep );
			}
			*/
			
			//$price = number_format( $price, absint( get_option( '$this->format_price_num_decimals' ) ), get_option( '$this->format_price_decimal_sep' ), get_option( '$this->format_price_thousand_sep' ) );

			$price_format = get_woocommerce_price_format();

			$price = sprintf( $price_format, '<span class="woocommerce-Price-currencySymbol">' . get_woocommerce_currency_symbol() . '</span>', $price );

			$variation['price_html'] = '<span class="price">' . $price . '</span>';

			if ( ( 'yes' == $this->settings['show_regular_price'] || 'yes' == $this->settings['show_savings'] ) ) {

				$reg_price = get_post_meta( $variation_id, '_regular_price', true );

				$role_price = get_post_meta( $variation_id, '_' . $role . '_price', true );

				if ( empty( $role_price ) ) { 

					if ( empty( $this->global_discounted[ $variation_id ] ) ) { 
						$role_price = $this->maybe_get_global_discount_price( $variation['display_price'], $variation_obj, $role );
						$this->global_discounted[ $variation_id ] = $role_price;
					} else { 
						$price = $this->global_discounted[ $variation_id ];
					}
									
				}

				if ( ( floatval( $role_price ) < floatval( $reg_price ) ) && 'yes' == $this->settings['show_regular_price'] )
					$variation['price_html']  .= '<br><span class="price normal_price">' . $this->settings['show_regular_price_label'] . ' <span class="amount">' . $this->format_price( $reg_price ) . '</span></span>';

				$savings = ( floatval( $reg_price ) - floatval( $role_price ) );

				if ( $savings < $reg_price && 'yes' == $this->settings['show_savings'] ) { 
					
					$savings = wc_format_localized_price( $savings );
					
					$variation['price_html']  .= '<br><span class="price normal_price savings">' . $this->settings['show_savings_label'] . ' <span class="amount">' . $this->format_price( $savings ) . '</span></span>';
				}

			}

		}

		return $variation;

	}

	// For WooCommerce 2.x flow, to ensure product is visible as long as a role price is set
	function variation_is_visible( $visible, $vid ) {
		global $product;

		if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '<' ) ) {
			if ( !isset( $product->children ) || count( $product->children ) <= 0 )
				return $visible;
		}

		$variation = new IGN_Wholesale_Pro_Dummy_Variation();

		$variation->variation_id = $vid;

		$res = $this->maybe_return_variation_price( 'xxxxx', $variation );

		if ( !isset( $res ) || empty( $res ) || '' == $res )
			$res = false;
		else
			$res = true;

		return $res;
	}

	// Runs during the woocommerce_variable_empty_price_html filter call, used here in this way for debugging purposes
	// This is used for WooCommerce 2.x compatibility
	function maybe_return_variation_price_empty( $price, $_product ) {
		global $product, $ign_opc;

		$current_user = $this->get_user();

		$this->get_roles();

		if ( empty( $this->roles ) )
			return;

		if ( !empty( $ign_opc ) && ( !empty( $_REQUEST['post_data'] ) || !empty( $_POST['opc_checkout_flag'] ) ) ) {

			if ( !empty( $_REQUEST['post_data'] ) )
				$args = wp_parse_args( $_REQUEST['post_data'] );
			else
				$args['opc_checkout_flag'] = $_POST['opc_checkout_flag'];

			if ( !empty( $args['opc_checkout_flag'] ) ) {
				$ign_opc->switch_user_context_to_customer( 'customer' );
				$current_user = $this->get_user();
				//$ign_opc->switch_user_context_to_customer( 'opc_user' );
			}
		}

		// Get parent product ID
		$id = ign_get_product_id( $_product, false );

		foreach( $this->roles as $role => $name ) {

			if ( !$current_user->has_cap( $role  ) )
				continue;

			$min_variation_wholesale_price = get_post_meta( $id, 'min_variation_' . $role . '_price' , true );

			if ( empty( $min_variation_wholesale_price ) ) {
				
				if ( empty( $this->global_discounted[ $id ] ) ) { 
					$min_variation_wholesale_price = $this->maybe_get_global_discount_price( $price, $_product, $role );
					$this->global_discounted[ $id ] = $role_price;
				} else { 
					$min_variation_wholesale_price = $this->global_discounted[ $id ];
				}
				
			}

			$max_variation_wholesale_price = get_post_meta( $id, 'max_variation_' . $role . '_price', true );

			if ( $min_variation_wholesale_price !== $max_variation_wholesale_price )
				$price = '<span class="from">' . __( 'From:', 'ignitewoo_wholesale_pro') . ' ' .  $this->format_price( $min_variation_wholesale_price ) . ' </span>';

			else
				$price = '<span class="from">' . $this->format_price( $min_variation_wholesale_price ) . ' </span>';
		}

		return $price;
	}

	// Handles getting prices for variable products
	// Used by woocommerce_variable_add_to_cart() function to generate Javascript vars that are later
	// automatically injected on the public facing side into a single product page.
	// This price is then displayed when someone selected a variation in a dropdown
	function maybe_return_variation_price( $price, $_product ) {
		global $product, $ign_opc; // parent product object - global

		// Sometimes this hook runs when the price is empty but wholesale price is not,
		// So check for that and handle returning a price for archive page view
		// $attrs = $_product->get_attributes();

		$this->get_roles();

		if ( empty( $this->roles ) )
			return $price;

		$is_variation = $_product->is_type( 'variation' );

		if ( !$is_variation )
			$is_variation = $_product->is_type( 'variable' );


		$id = ign_get_product_id( $_product, true );

		if ( !isset( $id ) && !$is_variation )
			return $price;

		$current_user = $this->get_user();

		if ( !empty( $ign_opc ) && ( !empty( $_REQUEST['post_data'] ) || !empty( $_POST['opc_checkout_flag'] ) ) ) {

			if ( !empty( $_REQUEST['post_data'] ) )
				$args = wp_parse_args( $_REQUEST['post_data'] );
			else
				$args['opc_checkout_flag'] = $_POST['opc_checkout_flag'];

			if ( !empty( $args['opc_checkout_flag'] ) ) {
				$ign_opc->switch_user_context_to_customer( 'customer' );
				$current_user = $this->get_user();
				//$ign_opc->switch_user_context_to_customer( 'opc_user' );
			}
		}

		foreach( $this->roles as $role => $name ) {

			if ( $is_variation && $current_user->has_cap( $role ) ) {

				$price = $this->format_price( get_post_meta( $id, '_' . $role . '_price', true ) );

				if ( empty( $price ) ) { 
					
					if ( empty( $this->global_discounted[ $id ] ) ) { 
						$price = $this->maybe_get_global_discount_price( $price, $_product, $role );
						$this->global_discounted[ $id ] = $role_price;
					} else { 
						$price = $this->global_discounted[ $id ];
					}

				}
				
				return $price;

			}
		}

		$product_price = ign_get_product_price( $_product );

		foreach( $this->roles as $role => $name ) {

			if ( $current_user->has_cap( $role ) )  {

				$wholesale = get_post_meta( $id, '_' . $role . '_price', true );

				if (  ( $wholesale ) > 0 && version_compare( WOOCOMMERCE_VERSION, '2.2', '<' ) )
					$product->product_custom_fields[ '_' . $role . '_price'] = array( $wholesale );

				if ( is_array( $product->product_custom_fields[ '_' . $role . '_price' ] ) && $product->product_custom_fields[ '_' . $role . '_price'][0] > 0 ) {

					if ( version_compare( WOOCOMMERCE_VERSION, '2.2', '<' ) )
						$price = $this->format_price( $product->product_custom_fields[ '_' . $role . '_price'][0] );

				} elseif ( $product_price === '' ) { 
				
					if ( empty( $this->global_discounted[ $id ] ) ) { 
						$price = $this->maybe_get_global_discount_price( $price, $_product, $role );
						$this->global_discounted[ $id ] = $role_price;
					} else { 
						$price = $this->global_discounted[ $id ];
					}
					
				} elseif ( $product_price == 0 )

					$price = __( 'Free!', 'ignitewoo_wholesale_pro' );
			}
		}

		return $price;
	}

	// process simple product meta
	function process_product_meta( $post_id, $post = '' ) {

		$this->get_roles();

		if ( empty( $this->roles ) )
			return;

		foreach( $this->roles as $role => $name ) {

			$price = isset( $_POST[ $role . '_price'] ) ? wc_format_decimal( str_replace( wc_get_price_thousand_separator(), '', $_POST[ $role . '_price'] ) )  : '';
		
			if ( '' !== $_POST[ $role . '_price'] )
				update_post_meta( $post_id, '_' . $role . '_price', $price );
			else
				delete_post_meta( $post_id, '_' . $role . '_price' );

		}
	}

	// process variable product meta
	function process_product_meta_variable( $post_id ) {

		$this->get_roles();

		if ( empty( $this->roles ) )
			return;

		$variable_post_ids = $_POST['variable_post_id'];

		if ( empty( $variable_post_ids ) )
			return;

		foreach( $this->roles as $role => $name ) {

			foreach( $variable_post_ids as $key => $id ) {

				if ( empty( $id ) || absint( $id ) <= 0 )
					continue;

				//if ( '' == $_POST[ $role .  '_price' ][ $key ] )
				//	continue;

				$price = isset( $_POST[ $role .  '_price' ][ $key ] ) ? wc_format_decimal( str_replace( wc_get_price_thousand_separator(), '', $_POST[ $role .  '_price' ][ $key ] ) )  : '';
				
				update_post_meta( $id, '_' . $role . '_price', $price );

			}

		}

		$post_parent = $post_id;

		$children = get_posts( array(
				    'post_parent' 	=> $post_parent,
				    'posts_per_page'=> -1,
				    'post_type' 	=> 'product_variation',
				    'fields' 		=> 'ids'
			    ) );

		if ( $children ) {

			foreach( $this->roles as $role => $name ) {

				$lowest_price = '';

				$highest_price = '';

				foreach ( $children as $child ) {

					$child_price = get_post_meta( $child, '_' . $role . '_price', true );

					if ( is_null( $child_price ) ) continue;

					// Low price
					if ( !is_numeric( $lowest_price ) || $child_price < $lowest_price ) $lowest_price = $child_price;


					// High price
					if ( $child_price > $highest_price )
						$highest_price = $child_price;
				}

				update_post_meta( $post_parent, '_' . $role . '_price', $lowest_price );
				update_post_meta( $post_parent, 'min_variation_' . $role . '_price' , $lowest_price );
				update_post_meta( $post_parent, 'max_variation_' . $role . '_price', $highest_price );
			}
		}
	}

	function ajax_process_product_meta_variable() {
		$product_id = absint( $_POST['product_id'] );
		$this->process_product_meta_variable( $product_id );
	}


	function bulk_edit() {

		$this->get_roles();

		if ( empty( $this->roles ) )
			return;

		foreach( $this->roles as $role => $name ) {

			?>
			<option value="<?php echo $role ?>_price"><?php _e( $name . ' Price', 'ignitewoo_wholesale_pro' ); ?></option>
			<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.2', '>=' ) ) { ?>
			<script>
			jQuery('select#field_to_edit').bind( '<?php echo $role ?>_price', function( event ) {
				var bulk_edit  = jQuery( 'select#field_to_edit' ).val(),
				checkbox,
				answer,
				value;

				value = window.prompt( woocommerce_admin_meta_boxes_variations.i18n_enter_a_value );

				jQuery( ':input[name^="' + bulk_edit + '"]').not('[name*="dates"]').val( value ).change();
			});

			</script>
			<?php }
		}
	}

	function add_variable_attributes( $loop, $variation_data, $variation ) {

		$this->get_roles();

		if ( empty( $this->roles ) )
			return;

		foreach( $this->roles as $role => $name ) {

			if ( empty( $variation_data['variation_post_id'] ) && !empty( $variation->ID ) )
				$id = $variation->ID;
			else
				$id = $variation_data['variation_post_id'];

			$wprice = get_post_meta( $id, '_' . $role . '_price', true );

			if ( false === $wprice )
				$wprice = '';
			else 
				$wprice = wc_format_localized_price( $wprice );

			if ( version_compare( WOOCOMMERCE_VERSION, '2.3', '<=' ) ) {
			?>
			<tr>
				<td>
			<?php } ?>

					<p class="form-row form-row-full">
					<label><?php echo $name; echo ' ('.get_woocommerce_currency_symbol().')'; ?> <a class="tips" data-tip="<?php _e( 'Enter the price for ', 'ignitewoo_wholesale_pro' ); echo $name ?>" href="#">[?]</a></label>
					<input class="<?php echo $role ?>_price wc_input_price" type="text" size="99" name="<?php echo $role ?>_price[<?php echo $loop; ?>]" value="<?php echo $wprice ?>" step="any" min="0" placeholder="<?php _e( 'Set price ( optional )', 'ignitewoo_wholesale_pro' ) ?>"/>
					</p>
			<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.3', '<=' ) ) { ?>
				</td>
			</tr>
			<?php }
		}
	}

	function add_simple_price() {
		global $thepostid;

		$this->get_roles();

		if ( empty( $this->roles ) )
			return;

		foreach( $this->roles as $role => $name ) {

			$wprice = get_post_meta( $thepostid, '_' . $role . '_price', true );
			$wprice = wc_format_localized_price( $wprice );

			woocommerce_wp_text_input( array( 'id' => $role . '_price', 'class' => 'wc_input_price short', 'label' => $name . ' (' . get_woocommerce_currency_symbol() . ')', 'description' => '', 'type' => 'text', 'custom_attributes' => array(
						'step' 	=> 'any',
						'min'	=> '0'
					), 'value' => $wprice ) );

		}
	}


	function get_roles() {
		global $wp_roles;

		if ( !empty( $this->roles ) )
			return;

		if ( class_exists( 'WP_Roles' ) )
		    if ( !isset( $wp_roles ) )
			$wp_roles = new WP_Roles();

		foreach( $wp_roles->roles as $role => $data )
			if ( 'ignite_level_' == substr( $role, 0, 13 ) )
				$this->roles[ $role ] = $data['name'];

	}

	// Filter search results
	public function the_posts( $posts, $query = false ) {

		if ( is_admin() )
			return $posts;

		if ( ( empty( $this->settings['tier_filter'] ) && empty( $this->settings['tier_filter'] ) ) ||
			'yes' != $this->settings['tier_filter'] && 'yes' != $this->settings['retail_filter'] )
			return $posts;

		// Abort if there's no query
		if ( ! $query )
			return $posts;

		$post__in = array_unique( apply_filters( 'loop_shop_post_in', array() ) );

		// Abort if we're not filtering posts
		if ( empty( $post__in ) )
			return $posts;

		// Abort if this query has already been done
		if ( ! empty( $query->wc_query ) )
			return $posts;

		// Abort if this isn't a search query
		if ( empty( $query->query_vars["s"] ) )
			return $posts;

		// Abort if we're not on a post type archive/product taxonomy
		//if 	( ! $query->is_post_type_archive( 'product' ) && ! $query->is_tax( get_object_taxonomies( 'product' //) ) )
		//	return $posts;

		$filtered_posts = array();

		$queried_post_ids = array();

		foreach ( $posts as $post ) {
			if ( in_array( $post->ID, $post__in ) ) {
				$filtered_posts[] = $post;
				$queried_post_ids[] = $post->ID;
			}
		}

		$query->posts = $filtered_posts;

		$query->post_count = count( $filtered_posts );

		// Ensure filters are set
		$this->unfiltered_product_ids = $queried_post_ids;

		$this->filtered_product_ids = $queried_post_ids;

		/*
		$this->layered_nav_product_ids = null;

		if ( sizeof( $this->layered_nav_post__in ) > 0 ) {
			$this->layered_nav_product_ids = array_intersect( $this->unfiltered_product_ids, $this->layered_nav_post__in );
		} else {
			$this->layered_nav_product_ids = $this->unfiltered_product_ids;
		}
		*/

		return $filtered_posts;
	}

	// For WC 3.1 or older, WC 3.2 and newer use the newer loop_meta_query() further below
	// Filter products in the shop - does not affect admins and shop managers
	// Runs via a WooCommerce hook "loop_shop_post_in"
	// This query runs ONCE for each view of the site's "shop" pages (not individual product pages );
	// function product_filter( $matched_products = null ) {
	function product_filter( $meta_query = array() ) {
		global $wpdb, $ignitewoo_wholesale_pricing_plus, $current_user;

		if ( ( empty( $this->settings['tier_filter'] ) && empty( $this->settings['retail_filter'] ) ) ||
			'yes' != $this->settings['tier_filter'] && 'yes' != $this->settings['retail_filter'] )
			return $meta_query;

		if ( empty( $current_user ) ) {
			if ( function_exists( 'wp_get_current_user' ) )
				$current_user = wp_get_current_user();
			else
				$current_user = get_currentuserinfo();
		}

		$tiered_role = false;

		if ( !empty( $current_user->roles ) ) {
			foreach( $current_user->roles as $role ) {
				if ( 'ignite_level_' == substr( $role, 0, 13 ) )
					$tiered_role = $role;
			}
		}

		if ( current_user_can( 'administrator' ) || current_user_can( 'shop_manager' ) ) {

			return $meta_query;

		} else if ( $tiered_role && 'yes' == $this->settings['tier_filter'] ) {

			$sql = "SELECT DISTINCT ID, post_parent, post_type FROM $wpdb->posts
				INNER JOIN $wpdb->postmeta ON ID = post_id
				WHERE post_type IN ( 'product', 'product_variation' )
				AND post_status = 'publish'
				AND ( meta_key = '_{$tiered_role}_price' AND meta_value != '' )
			";

			
		} else if ( !$tiered_role && 'yes' == $this->settings['retail_filter'] ) {

			$sql = "SELECT DISTINCT ID, post_parent, post_type FROM $wpdb->posts
				INNER JOIN $wpdb->postmeta m1 ON ID = m1.post_id
				INNER JOIN $wpdb->postmeta m2 ON ID = m2.post_id
				WHERE post_type IN ( 'product', 'product_variation' )
				AND post_status = 'publish'
				AND (
					( m1.meta_key = '_price' AND m1.meta_value != '' )
					OR
					( m2.meta_key = '_sale_price' AND m2.meta_value != '' ) )
			";
			
			
		} else {

			// return $matched_products;

		}
		
		$matched_products_query = $wpdb->get_results( $sql, OBJECT_K );

		 if ( $matched_products_query ) {

			foreach ( $matched_products_query as $product ) {

				if ( $product->post_type == 'product' )
					$matched_products[] = $product->ID;

				if ( $product->post_parent > 0 && ! in_array( $product->post_parent, $matched_products ) )
					$matched_products[] = $product->post_parent;
			}
	        }

	        return $matched_products;
	}
	
	function loop_meta_query( $meta_query = array(), $wc_query ) {
	
		global $wpdb, $ignitewoo_wholesale_pricing_plus, $current_user;

		if ( ( empty( $this->settings['tier_filter'] ) && empty( $this->settings['retail_filter'] ) ) ||
			'yes' != $this->settings['tier_filter'] && 'yes' != $this->settings['retail_filter'] )
			return $meta_query;

		if ( empty( $current_user ) ) {
			if ( function_exists( 'wp_get_current_user' ) )
				$current_user = wp_get_current_user();
			else
				$current_user = get_currentuserinfo();
		}

		$tiered_role = false;

		if ( !empty( $current_user->roles ) ) {
			foreach( $current_user->roles as $role ) {
				if ( 'ignite_level_' == substr( $role, 0, 13 ) )
					$tiered_role = $role;
			}
		}

		if ( current_user_can( 'administrator' ) || current_user_can( 'shop_manager' ) ) {

			return  $meta_query;

		} else if ( $tiered_role && 'yes' == $this->settings['tier_filter'] ) {

			$meta_query[] =  array(
				array(
					'key'       =>  '_' . $tiered_role . '_price',
					'value'     =>  '',
					'compare'   =>  '!='
				),
			);

			
		} else if ( !$tiered_role && 'yes' == $this->settings['retail_filter'] ) {

			$meta_query[] =  array(
				'relation' 	=> 'OR',
				array(
					'key'       =>  '_price',
					'value'     =>  '',
					'compare'   =>  '!='
				),
				array(
					'key'       =>  '_sale_price',
					'value'     =>  '',
					'compare'   =>  '!='
				),
			);
		
			
		} else {
			return $meta_query; 
		}

		return $meta_query; 
	}

	/*
	function woocom_wholesale_join( $join ) {
		global $wp_query, $wpdb;

		if ( is_single() )
                        return $join;

		if ( 'product' != $wp_query->query_vars['post_type'] && empty( $wp_query->query_vars['product_cat'] ) && empty( $wp_query->query_vars['product_tag'] ) && empty( $wp_query->query_vars['product_brands'] ) && false == $this->doing_shortcode )
			return $join;

		//if ( !current_user_can( 'wholesale_buyer' ) )
		//	return $join;

		$join .= " LEFT JOIN $wpdb->postmeta wwjm ON $wpdb->posts.ID = wwjm.post_id ";

		return $join;
	}


	function posts_where( $where ) {
		global $wpdb, $wp_query, $wp_roles;

		if ( is_single() )
                        return $where;

		$roles = array( 'wholesale_buyer' );

		if ( isset( $wp_roles->roles ) ) {
			foreach( $wp_roles->roles as $role => $data ) {

				if ( 'ignite_level_' != substr( $role, 0, 13 ) )
					continue;

				$roles[] = $role;
			}
		}

		if ( 'product' != $wp_query->query_vars['post_type'] && empty( $wp_query->query_vars['product_cat'] ) && empty( $wp_query->query_vars['product_tag'] ) && empty( $wp_query->query_vars['product_brands'] ) && false == $this->doing_shortcode )
			return $where;

		if ( 'yes' == $this->settings['filter_products'] && $this->has_filterable_role( $roles ) )
			$where .= " AND ( wwjm.meta_key = 'wholesale_price' AND wwjm.meta_value != '' ) ";

		if ( 'yes' == $this->settings['retail_filter_products'] && !$this->has_filterable_role( $roles ) )
			$where .= " AND ( wwjm.meta_key = '_price' AND wwjm.meta_value != '' ) ";

		return $where;
	}
	*/

	// Support for Tiered Pricing because $roles is loaded with any roles created by Tiered Pricing plus wholesale_buyer
	function has_filterable_role() {

		return $this->get_filterable_role();

	}

	function get_filterable_role() {
		global $wp_roles;

		$current_user_can = false;
		$roles = array();

		if ( isset( $wp_roles->roles ) ) {
			foreach( $wp_roles->roles as $role => $data ) {

				if ( 'ignite_level_' !== substr( $role, 0, 13 ) )
					continue;

				$roles[] = $role;
			}
		}

		if ( empty( $roles ) )
			return false;

		foreach( $roles as $r ) {
			if ( current_user_can( $r ) )
				$current_user_can = $r;
		}

		return $current_user_can;
	}

	function empty_price_html( $foobar, $obj ) {

		remove_filter( 'woocommerce_empty_price_html', array( &$this, 'empty_price_html' ), 999999999 );

		$out = $this->maybe_return_wholesale_price( '', $obj );

		add_filter( 'woocommerce_empty_price_html', array( &$this, 'empty_price_html' ), 999999999, 2 );

		return $out;
	}

	function disable_coupons( $valid, $obj ) {

		// are retail coupons disabled for wholesale buyers?
		if ( 'yes' != $this->settings['disable_coupons'] )
			return $valid;

		// does the shopper have a tier role? if so then at this point disallow the coupon
		if ( !$this->has_filterable_role() )
			return $valid;
		else
			return false;

		return $valid;
	}

	function disable_coupons_error_msg( $error, $obj ) {

		if ( 'yes' != $this->settings['disable_coupons'] )
			return $error;

		if ( !$this->has_filterable_role() )
			return $error;

		$error = isset( $this->settings['disable_coupons_error_msg'] ) ? $this->settings['disable_coupons_error_msg'] : __( "You're not allow to use coupons", 'ignitewoo_wholesale_pro' );

		return $error;
	}

	function maybe_insert_thankyou_message( $message = '', $order_id = 0 ) {
		global $wp_roles;

		$role = $this->get_filterable_role();

		if ( empty( $role ) )
			return;

		if ( !empty( $this->settings['thankyou_message'] ) ) {
			echo '<div class="wsp_thankyou_message">' . wpautop( $this->settings['thankyou_message'] ) . '</div>';
		}
	}

	function get_available_shipping_methods( $methods = array(), $package = false ) {
		global $user_ID;

		if ( empty( $methods ) || !is_array( $methods ) )
			return $methods;

		if ( !$role = $this->get_filterable_role() ) {
			return $methods;
		}

		$role_shipping_methods = get_option( $role . '_shipping_methods', array() );

		if ( empty( $role_shipping_methods ) )
			return $methods;
			
		$new_way = false; 
		
		foreach( $role_shipping_methods as $r ) { 
			if ( false !== strpos( $r, ':' ) )
				$new_way = true; 
		}

		foreach( $methods as $k => $r ) { 
			if ( false !== strpos( $k, ':' ) )
				$new_way = true; 
		}
		
		// The new way, as of 2.2.6, uses method_id and instance ID for keys.
		// Only necessary when zones are used. 
		// Ignore all methods passed into this function, re-add only the allowed methods
		if ( $new_way ) { 
		
			$avail_methods = array();

			foreach ( $methods as $id => $rate ) {

				if ( $rate->method_id === 'table_rate' ) {
					$rate_id = implode(':', array(
						$rate->method_id,
						$rate->instance_id
					));
				} else if ( class_exists( $rate, 'get_instance_id' ) && $rate->get_instance_id() ) {
					$rate_id = implode(':', array(
						$rate->get_method_id(),
						$rate->get_instance_id(),
					));
				} else {
					$rate_id = $id;
				}

				if ( in_array( $rate_id, $role_shipping_methods ) )
					$avail_methods[ $id ] = $rate;
			}
			
			return $avail_methods;
			
		} else { 

			foreach ( WC()->shipping->load_shipping_methods( $package ) as $shipping_method ) {

				if ( version_compare( WOOCOMMERCE_VERSION, '2.6', '>=' ) ) {

					$zones = WC_Shipping_Zones::get_zones();
					$zero_zone = new WC_Shipping_Zone(0); 

					if ( !empty( $zero_zone ) ) {
					
						 if ( class_exists( $shipping_method, 'get_instance_id' ) ) 
							$method_instance_id = $shipping_method->get_instance_id();
						else 
							$method_instance_id = false; 

						if ( !in_array( $method_instance_id, $role_shipping_methods ) ) {
							$rate_id = $shipping_method->get_rate_id();
							if ( isset( $methods[ $rate_id ] ) )
								unset( $methods[ $rate_id ] );
						}
					}
					// Zones in use?
					if ( !empty( $zones ) ) {
						if ( class_exists( $shipping_method, 'get_instance_id' ) ) 
							$method_instance_id = $shipping_method->get_instance_id();
						else 
							$method_instance_id = false; 

						if ( !in_array( $method_instance_id, $role_shipping_methods ) ) {
							$rate_id = $shipping_method->get_rate_id();
							if ( isset( $methods[ $rate_id ] ) )
								unset( $methods[ $rate_id ] );
						}

					} else {
					// No zones

						$rate_id = $shipping_method->get_rate_id();

						// Trim off zone ID number, looks like flat_rate:2 or similar
						$temp_rate_id = explode( ':', $rate_id );
						$temp_rate_id = $temp_rate_id[0];

						if ( !in_array( $temp_rate_id, $role_shipping_methods ) ) {

							if ( isset( $methods[ $rate_id ] ) )
								unset( $methods[ $rate_id ] );
						}
					}

				} else {

					foreach( $role_shipping_methods as $id ) {
						if ( isset( $methods[ $id ] ) )
							unset( $methods[ $id ] );
					}

				}
			}

			return $methods;
		}
		
	}

	function available_gateways( $available_gateways = array() ) {
		global $user_ID, $woocommerce;

		if ( empty( $available_gateways ) )
			return $available_gateways;

		// If the user does not have a custom role check if invoice payment gateway setting,
		// if not enabled for all then remove it from the list of available gateways
		if ( !$role = $this->get_filterable_role() ) {
			if ( isset( $available_gateways['ignitewoo_wholesale_pro_invoice_gateway'] ) ) {
				if ( empty( $available_gateways['ignitewoo_wholesale_pro_invoice_gateway']->settings['enabled_all'] ) ||
				( 'yes' !== $available_gateways['ignitewoo_wholesale_pro_invoice_gateway']->settings['enabled_all'] ) )
					unset( $available_gateways['ignitewoo_wholesale_pro_invoice_gateway'] );
			}
			return $available_gateways;
		}
		$role_payment_gateways = get_option( $role . '_payment_gateways', array() );

		if ( empty( $role_payment_gateways ) )
			return $available_gateways;

		foreach( $available_gateways as $gateway => $data ) {
			if ( !in_array( $gateway, $role_payment_gateways ) )
				unset( $available_gateways[ $gateway ] );

		}

		return $available_gateways;
	}

	// Disallow pending wholesale users from logging in?
	function maybe_disallow_login( $user_login, $user = null ) {

		if ( !$user ) {
			$user = get_user_by('login', $user_login);
		}
		if ( !$user ) {
			// not logged in - definitely not disabled
			return;
		}
		
		// Get user meta
		$pending_role = $this->settings['ignitewoo_wsp_pending_role'];

		if ( empty( $pending_role ) || !user_can( $user, $pending_role ) )
			return;
	
		
	
		// Clear cookies to log out the user
		wp_clear_auth_cookie();

		// Add a notice 
		$message = __( 'You cannot login while your account is pending approval.', 'ignitewoo_wholesale_pro' );
		
		throw new Exception( $message );
		
	}
	
	function process_registration() {
		
		if ( isset( $_POST['ign_wsp_form'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'ign_wsp_form' ) ) {
		
			$customer_id = $this->process_reg_fields();

			if ( !empty( $customer_id ) ) {

				// Save billing fields
				foreach( $_POST as $key => $val ) {
				
					if ( 'billing_' == substr( $key, 0, 8 ) ) {
						
						update_user_meta( $customer_id, $key, $val );
						
						if ( 'billing_first_name' == $key )
							update_user_meta( $customer_id, 'first_name', $val );
							
						if ( 'billing_last_name' == $key )
							update_user_meta( $customer_id, 'last_name', $val );
					} else {
					
						// Store extra data other than billing fields in the usermeta table
						update_user_meta( $customer_id, $key, $val );
					
					}
				}

				$this->registration_success = true;

				return;
			}
		}
	}

	function show_register_form( $attrs, $content = '' ) {
		global $current_user;

		if ( empty( $this->settings['reg_form_fields'] ) )
			return $content;

		if ( !empty( $this->registration_success ) ) {

			if ( 'yes' != $this->settings['ignitewoo_wsp_automatic_approval'] || empty( $this->settings['ignitewoo_wsp_automatic_role'] ) ) {
				wc_add_notice( __( 'Your registration request has been sent. Please be patient while it is reviewed.', 'ignitewoo_wholesale_pro' ), 'success' );
			} else {
				$link = get_permalink( wc_get_page_id( 'myaccount' ) );
				wc_add_notice( sprintf( __( 'Your account has been created. <a class="button" href="%s">Please login</a>.', 'ignitewoo_wholesale_pro' ), $link ), 'success' );
			}

			if ( function_exists( 'wc_print_notices' ) )
				wc_print_notices();
			return;
		}

		wp_enqueue_style( 'ign-wsp-form-render', $this->plugin_url . '/assets/css/form-render.css' );
		
		// For the form layout, some themes don't enqueue this on regular pages
		wp_enqueue_style( 'ign-wsp-form-render', WC()->plugin_url( 'assets/css/woocommerce.css' ) );

		ob_start();
		
		if ( function_exists( 'wc_print_notices' ) )
			wc_print_notices();
			
		?>
		<div class="woocommerce">
		<form id="ign_wsp_form"  action="" method="post">
			<input type="hidden" name="ign_wsp_form" value="true" >
			<?php wp_nonce_field( 'ign_wsp_form' ); ?>
			<?php
			for ( $i = 0; $i < count( $this->settings['reg_form_fields']['title'] ); $i++ ) {


				if ('yes' == $this->settings['reg_form_fields']['required'][ $i ] ) {
					$required = 'data-required="required"';
					$required_indicator = '<span class="required">*</span>';
				} else {
					$required = 'data-required=""';
					$required_indicator = '';
				}

				$type = $this->settings['reg_form_fields']['field_type'][ $i ] ;
				$title = $this->settings['reg_form_fields']['title'][ $i ] ;
				$name = $this->settings['reg_form_fields']['field_name'][ $i ] ;
				
				$align = isset( $this->settings['reg_form_fields']['field_align'][ $i ] ) ? $this->settings['reg_form_fields']['field_align'][ $i ] : 'form-row-wide';
				
				$align = 'form-row ' . $align; 
				
				$val = isset( $_POST[ $name ] ) ? $_POST[ $name ] : '';
				
				if ( empty( $val ) ) {
					if ( is_user_logged_in() ) {
						$val = get_user_meta( $current_user->data->ID, $name, true );
					}
				}
			
				switch ( $type )  {
					case 'text' :
						?>
						<p class="<?php echo $name ?>_row <?php echo $align ?>">
							<label><?php echo $title; ?><?php echo $required_indicator ?></label>
							
							<?php
							if ( 'billing_country' == $name ) {
								?>
								<select id="<?php echo $name ?>" class="ign_wsp_field <?php echo $name ?>" name="<?php echo $name ?>" value="<?php echo $val ?>" <?php echo $required ?> >
								<?php
								$countries = WC()->countries->get_countries();
								foreach( $countries as $k => $v ) {
									$selected = '';
									if ( !empty( $val ) && $val == $k )  {
										$selected = 'selected="selected"';
									} else if ( empty( $val ) && !empty( $this->settings['ignitewoo_wsp_register_default_country'] ) ) { 
										if ( $this->settings['ignitewoo_wsp_register_default_country'] == $k )
											$selected = 'selected="selected"';
									} else {
										$selected = '';
									}
									?>
									<option value="<?php echo $k ?>" <?php echo $selected ?>><?php echo $v ?></option>
									<?php
								}
								?>
								</select>

								<?php
							} else if ( 'billing_state' == $name ) {
								?>
								<input id="<?php echo $name ?>" class="ign_wsp_field <?php echo $name ?>" type="text" name="<?php echo $name ?>" value="<?php echo $val ?>" <?php echo $required ?> >
								<?php
							} else {
							?>
								<input id="<?php echo $name ?>" class="ign_wsp_field <?php echo $name ?>" type="text" name="<?php echo $name ?>" value="<?php echo $val ?>" <?php echo $required ?> >

								<?php
							}
							?>
						</p>
						<?php
						break;
					case 'textarea' :
						?>
						<p class="<?php echo $align ?>">
							<label><?php echo $title; ?><?php echo $required_indicator ?></label>
							<textarea id="<?php echo $name ?>" class="ign_wsp_field <?php echo $name ?>" name="<?php echo $name ?>" value="" <?php echo $required ?> ><?php echo $val ?></textarea>
						</p>
						<?php
						break;
					case 'checkbox' :
						?>
						<p class=" <?php echo $align ?>">
							<label><input id="<?php echo $name ?>" class="ign_wsp_field <?php echo $name ?>" type="checkbox" name="<?php echo $name ?>" value="true" <?php echo $required ?> <?php checked( $val, 'true', true ) ?>><?php echo $title; ?><?php echo $required_indicator ?></label>
						</p>
						<?php
						break;
					case 'radio': 
						if ( !empty( $this->settings['reg_form_fields']['field_type_radio_options'][ $name ] ) ) {
							?>
							<p class="ign_wsp_field <?php echo $name ?>_row <?php echo $align ?> radio_btn_wrap">
								<label><?php echo $title; ?><?php echo $required_indicator ?></label>
								<?php 
								foreach( $this->settings['reg_form_fields']['field_type_radio_options'][ $name ] as $val ) { 
									?>
									<span class="radio_span"><input type="radio" class="input-radio" name="<?php echo $name ?>" value="<?php echo esc_attr( $val ) ?>" <?php echo $required ?>/><label><?php echo $val ?></label></span>
									<br/>
									<?php 
								}
								?>
							</p>
							<?php 
						}
						break;
					case 'button' :
						?>
						<p class=" <?php echo $align ?>">
							<input id="<?php echo $name ?>" class="ign_wsp_field <?php echo $name ?>" type="submit" name="<?php echo $name ?>" value="<?php echo $title ?>" <?php echo $required ?> >
						</p>
						<?php
						break;
					case 'password' :
						if ( is_user_logged_in() )
							break;
						?>
						<p class=" <?php echo $align ?>">
							<label><?php echo $title; ?><?php echo $required_indicator ?></label>
							<input id="<?php echo $name ?>" class="ign_wsp_field <?php echo $name ?>" type="password" name="<?php echo $name ?>" value="<?php echo $val ?>" <?php echo $required ?> >
						</p>
						<?php
						break;
					case 'captcha' : 
			
						$privatekey  = $this->settings['ignitewoo_wsp_recaptcha_secret_key']; 
						$publickey = $this->settings['ignitewoo_wsp_recaptcha_site_key'];
						
						if ( !empty( $publickey ) && !empty( $privatekey ) ) {
							?>
							<p class="<?php echo $align ?>">
								<span class="g-recaptcha" data-sitekey="<?php echo esc_attr( $publickey ) ?>"></span>
							</p>
							<?php 
							
							wp_enqueue_script( 'enq-recaptcha', 'https://www.google.com/recaptcha/api.js' );
						}
				
						break;
						
					default: 
						break;
				}
			}
			?>
			<script>
			jQuery( document ).ready( function( $ ) {
				$( '#ign_wsp_form' ).on( 'submit', function( e ) {
					failed_validate = false
					
					$( this ).find( 'input, textarea, select' ).each( function() {
						
						$( this ).removeClass( 'field_required' );
						
						if ( $( this ).is( ':hidden' ) )
							return;

						// Skip radio buttons, we check those independently because they always have values set 
						// so they can't be checked for an empty value 
						if ( 'radio' == $( this ).attr( 'type' ) && 'required' == $( this ).data( 'required' ) )
							return;

						if ( 'required' == $( this ).data( 'required' ) && '' == $( this ).val() ) {

							$( this ).addClass( 'field_required' );
							failed_validate = true;
						}
					});
					
					 
					$( this ).find( '.radio_btn_wrap' ).each( function() {

						$( this ).removeClass( 'field_required' );
						
						var check_set = false;
						var required = false; 
						
						$( this ).find( 'input' ).each( function() {
							// Not required
							if ( 'required' !== $( this ).data( 'required' ) )
								return;

							required = true; 
						
							if ( $( this ).is( ':checked' ) )
								check_set = true; 
						});
						
						if ( required && !check_set ) { 
							$( this ).addClass( 'field_required' );
							failed_validate = true;
						}
					});
					
					if ( failed_validate ) {
						$( 'body, html' ).animate( { scrollTop: $( '#ign_wsp_form' ).offset().top - 100 }, 250 );
						return false;
					}
					
					$( this ).submit();
				});
			})
			</script>

		</form>
		</div>
		<?php

		wp_register_script( 'ign-wsp-country-select', $this->plugin_url . '/assets/js/countries.js', array( 'jquery' ), WC_VERSION, true );

		wp_localize_script( 'ign-wsp-country-select', 'ign_wsp_country_select_params', apply_filters( 'ign_wsp_country_select_params', array(
			'countries' => json_encode( array_merge( WC()->countries->get_allowed_country_states(), WC()->countries->get_shipping_country_states() ) ),
			'i18n_select_state_text' => esc_attr__( 'Select an option&hellip;', 'ignitewoo_wholesale_pro' ),
		) ) );

		wp_enqueue_script( 'ign-wsp-country-select' );

		return ob_get_clean();
	}

	function maybe_add_myaccount_button() {
		global $current_user; 
		
		$this->get_roles();

		if ( empty( $this->roles ) )
			return;
			
		// Don't show for users already registered as a wholesale buyer
		foreach( $this->roles as $role => $name ) {
			if ( $current_user->has_cap( $role ) ) {
				return;
			}			
		}
		
		if ( empty( $this->settings['ignitewoo_wsp_add_btn_to_myaccount'] ) || 'yes' != $this->settings['ignitewoo_wsp_add_btn_to_myaccount'] )
			return;

		$reg_page = !empty( $this->settings['ignitewoo_wsp_register_page'] ) ? $this->settings['ignitewoo_wsp_register_page'] : '';

		if ( empty( $reg_page ) )
			return;

		$btn_label = !empty( $this->settings['ignitewoo_wsp_add_myaccount_btn_label'] ) ? $this->settings['ignitewoo_wsp_add_myaccount_btn_label'] : '';

		if ( empty( $btn_label ) )
			return;

		$template_base = $this->plugin_path;

		$template = 'wholesale-register-button.php';

		if ( file_exists( get_stylesheet_directory() . '/woocommerce/wholesale-pro/' . $template ) )
			$file = get_stylesheet_directory() . '/woocommerce/wholesale-pro/' . $template;
		else
			$file = $template_base . '/templates/' . $template;

		require( $file );

	}

	function process_reg_fields() {
		global $current_user, $ign_wholesale_pro_suite;

		// Find email and password
		foreach( $_POST as $key => $val ) {
			if ( 'password_' == substr( $key, 0, 9 ) )
				$password = $val;
			if ( 'billing_email' == $key )
				$email = $val;
		}
		
		// If the captcha is in the form try to validate it
		if ( isset( $_POST['g-recaptcha-response'] ) && empty( $_POST['g-recaptcha-response'] ) ) { 
			
			wc_add_notice( __( "Please check the box to indicate you're not a robot", 'ignitewoo_wholesale_pro' ), 'error' );
			wp_redirect( $_SERVER['REQUEST_URI'] );
			die;
			
		} else if ( !empty( $_POST['g-recaptcha-response'] ) ) { 
		
			$privatekey  = $this->settings['ignitewoo_wsp_recaptcha_secret_key']; 
			$publickey = $this->settings['ignitewoo_wsp_recaptcha_site_key'];

			if ( !empty( $publickey ) && !empty( $privatekey ) ) {

				$response = wp_safe_remote_get( add_query_arg( array(
					'secret'   => $privatekey,
					'response' => isset( $_POST['g-recaptcha-response'] ) ? $_POST['g-recaptcha-response'] : '',
					'remoteip' => isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']
				), 'https://www.google.com/recaptcha/api/siteverify' ) );

				if ( is_wp_error( $response ) || empty( $response['body'] ) || ! ( $json = json_decode( $response['body'] ) ) || ! $json->success ) {
					wc_add_notice( __( "Please check the box to indicate you're not a robot", 'ignitewoo_wholesale_pro' ), 'error' );
					wp_redirect( $_SERVER['REQUEST_URI'] );
					die;
				}
			}
		}

		try {

			if ( !is_user_logged_in() ) {

				if ( empty( $email ) || ! is_email( $email ) ) {
					throw new Exception( __( 'Please provide a valid email address.', 'ignitewoo_wholesale_pro' ) );
				}

				if ( email_exists( $email ) ) {
					throw new Exception( __( 'An account is already registered with your email address.', 'ignitewoo_wholesale_pro' ) );
				}

				$username = sanitize_user( current( explode( '@', $email ) ), true );

				// Ensure username is unique.
				$append     = 1;
				$o_username = $username;

				while ( username_exists( $username ) ) {
					$username = $o_username . $append;
					$append++;
				}

				// Handle password creation.
				if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) && empty( $password ) ) {
					$password           = wp_generate_password();
					$password_generated = true;
				} elseif ( empty( $password ) ) {
					throw new Exception( __( 'Please enter an account password.', 'ignitewoo_wholesale_pro' ) );
				} else {
					$password_generated = false;
				}

				// Automatic role approval and assignment?

				if ( !empty( $this->settings['ignitewoo_wsp_automatic_role'] ) && 'yes' == $this->settings['ignitewoo_wsp_automatic_approval'] ) {
				
					if ( !empty( $this->settings['ignitewoo_wsp_automatic_role'] ) && get_role( $this->settings['ignitewoo_wsp_automatic_role'] ) )
						$role = $this->settings['ignitewoo_wsp_automatic_role'];
						
				// IF automatic approval is turned on, is there a Pending Registration role assigned? 
				// Maybe not, if the admin has seen the new setting added in v2.2
				} else if ( 'yes' !== $this->settings['ignitewoo_wsp_automatic_approval'] && !empty( $this->settings['ignitewoo_wsp_pending_role'] ) && get_role( $this->settings['ignitewoo_wsp_pending_role'] ) ) {
				
					$role = $this->settings['ignitewoo_wsp_pending_role'];
							
				} else {
					$role = 'customer';
				}

				$new_customer_data = apply_filters( 'woocommerce_new_customer_data', array(
					'user_login' => $username,
					'user_pass'  => $password,
					'user_email' => $email,
					'role'       => $role,
				) );

				$customer_id = wp_insert_user( $new_customer_data );

				if ( is_wp_error( $customer_id ) ) {
					throw new Exception( '<strong>' . __( 'ERROR', 'ignitewoo_wholesale_pro' ) . '</strong>: ' . __( 'Couldn&#8217;t register you&hellip; please contact us if you continue to have problems.', 'ignitewoo_wholesale_pro' ) );
				}

				update_user_meta( $customer_id, 'ignitewoo_wsp_registered_user', true );
				
				do_action( 'woocommerce_created_customer', $customer_id, $new_customer_data, $password_generated );

			} else {


				// User already logged in, update role
				if ( !empty( $this->settings['ignitewoo_wsp_automatic_role'] ) && 'yes' == $this->settings['ignitewoo_wsp_automatic_approval'] ) {
					if ( !empty( $this->settings['ignitewoo_wsp_automatic_role'] ) && get_role( $this->settings['ignitewoo_wsp_automatic_role'] ) ) {
						$role = $this->settings['ignitewoo_wsp_automatic_role'];
						$new_customer_data = apply_filters( 'woocommerce_new_customer_data', array(
							'ID'		 => $current_user->data->ID,
							'role'       => $role,
						) );

						$customer_id = wp_update_user( $new_customer_data );
						
						update_user_meta( $customer_id, 'ignitewoo_wsp_registered_user', true );
					}
				}

				$new_customer_data['user_email'] = $current_user->data->user_email;
				$new_customer_data['user_login'] = $current_user->data->user_login;
			}

			$extra_data = array();

			for ( $i = 0; $i < count( $this->settings['reg_form_fields']['field_name'] ); $i++ ) {
				
				$type = $this->settings['reg_form_fields']['field_name'][ $i ];

				if ( 'password_' == substr( $type, 0, 9 ) )
					continue;
					
				if ( 'checkbox_' == substr( $type, 0, 9 ) && !isset( $_POST[ $type ] ) ) {
					$val = '';
				} else if ( 'checkbox_' == substr( $type, 0, 9 ) && isset( $_POST[ $type ] ) ) {
					$val = __( 'Yes', 'ignitewoo_wholesale_pro' );
				} else { 
					$val =  wc_clean( $_POST[ $type ] );
				}
					
				$extra_data[] = array(
					'title' => $this->settings['reg_form_fields']['title'][ $i ],
					'content' => $val,
				);
			}

			// Maybe notify user if auto-approval is enabled 
			if ( 'yes' == $ign_wholesale_pro_suite->settings['ignitewoo_wsp_automatic_approval'] )
				do_action( 'ign_wsp_do_approval_email', $customer_id );
			
			$this->notify_admins( array( 'id' => $customer_id, 'email' => $new_customer_data['user_email'], 'login' => $new_customer_data['user_login'], 'extra_data' => $extra_data ) );

			return $customer_id;

		} catch ( Exception $e ) {
			wc_add_notice( '<strong>' . __( 'Error', 'ignitewoo_wholesale_pro' ) . ':</strong> ' . $e->getMessage(), 'error' );
		}
	}

	function get_field_title( $key ) { 
		$title = '';
		for ( $i = 0; $i < count( $this->settings['reg_form_fields']['field_name'] ); $i++ ) {
			if ( $key != $this->settings['reg_form_fields']['field_name'][ $i ] )
				continue;
			$title = $this->settings['reg_form_fields']['title'][ $i ];
			break;
		}
		return $title; 
	}
	
	function notify_admins( $args ) {
		if ( !empty( $args ) )
			extract( $args );

		$sitename = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );

		$template_base = $this->plugin_path;

		$template = 'user-registration-email.php';

		if ( file_exists( get_stylesheet_directory() . '/woocommerce/wholesale-pro/' . $template ) )
			$file = get_stylesheet_directory() . '/woocommerce/wholesale-pro/' . $template;
		else
			$file = $template_base . '/templates/' . $template;

		$email_heading = $subject = __( 'New User Registered', 'ignitewoo_wholesale_pro' );

		ob_start();

		require( $file );

		$message = ob_get_clean();
		
		if ( !empty( $this->settings['ignitewoo_wsp_new_registration_to'] ) )
			$to = $this->settings['ignitewoo_wsp_new_registration_to'];
		else 
			$to = get_option( 'admin_email' );
		
		$mailer = WC()->mailer();
		$mailer->send( $to, $subject, $message );
	}

	function display_tax_vat_in_order( $order ) {

		$order_id = is_callable( array( $order, 'get_id' ) ) ? $order->get_id() : $order->id;

		$taxvat = get_post_meta( $order_id, '_billing_tax_vat', true );

		if ( empty( $taxvat ) )
			return;
		?>
		<p class="form-field form-field-wide">
			<label><strong><?php _e( 'Tax / VAT ID:', 'ignitewoo_wholesale_pro' ); ?></strong></label>
			<?php echo $taxvat ?>
		</p>
		<?php
	}

	function add_tax_vat_to_order( $order_id ) {
		$customer = get_post_meta( $order_id, '_customer_user', true );
		if ( empty( $customer ) )
			return;

		$taxvat = get_user_meta( $customer, 'billing_tax_vat', true );
		if ( empty( $taxvat ) )
			return;

		update_post_meta( $order_id, '_billing_tax_vat', $taxvat );
	}
	
	function export_dd_pricing_rules( $meta_value, $meta, $product, $row ) { 
		if ( '_dd_pricing_rules' !== $meta->key  )
			return $meta_value; 

		return maybe_serialize( $meta_value );
	}

	/**
	  * Very sketchy. Callbacks are indexed field number as listed on importer's config page, so there's no set index number for custom fields
	  * Gotta find the index number of meta: name, then set a callback to unserialize the data. 
	  * Otherwise WC runs it through a callback of its choice, which results in serialized data not being imported
	  */
	function import_dd_pricing_rules( $callbacks = array(), $importer = false ) { 
	
		$key = array_search( 'meta:_dd_pricing_rules', $importer->get_mapped_keys() );
		
		if ( empty( $key ) )
			return $callbacks; 

		$callbacks[ $key ] = array( $this, 'handle_import_dd_pricing_rules' );

		return $callbacks;
	}
	
	// Unserializes the string and sends it back
	function handle_import_dd_pricing_rules( $data = '' ) { 
		return maybe_unserialize( $data );
	}
}

global $ign_wholesale_pro_suite;
$ign_wholesale_pro_suite = new IGN_Wholesale_Pro_Suite();

class IGN_Wholesale_Pro_Dummy_Variation {
	function is_type() {
		return true;
	}
}





































































































































if ( ! function_exists( 'ignitewoo_queue_update' ) )
	require_once( dirname( __FILE__ ) . '/ignitewoo_updater/ignitewoo_update_api.php' );

$this_plugin_base = plugin_basename( __FILE__ );

add_action( "after_plugin_row_" . $this_plugin_base, 'ignite_plugin_update_row', 1, 2 );

ignitewoo_queue_update( plugin_basename( __FILE__ ), '315b90269971f935b67668aa43d57359', '32987' );
