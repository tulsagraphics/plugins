<?php
/*
 * Copyright (c) 2013 - IgniteWoo.com - All Rights Reserved
*/


class IgniteWoo_Wholesale_Pro_Coupons {

	var $error = '';

	function __construct() {
		add_action( 'init', array( &$this, 'init' ), 1 );
	}

	function init() {
		add_action( 'woocommerce_coupon_options_usage_restriction', array( &$this, 'restriction' ) );
		add_action( 'woocommerce_process_shop_coupon_meta', array( &$this, 'save_meta' ), 2, 2 );
		// Runs AFTER the disable_coupons in the main plugin file
		// because coupons with assigned roles takes precendence.
		add_filter( 'woocommerce_coupon_is_valid', array( &$this, 'is_valid' ), 9999999, 2 );
		add_filter( 'woocommerce_coupon_error', array( &$this, 'error' ), 9999999, 2 );
		add_filter( 'manage_edit-shop_coupon_columns', array( &$this, 'woocommerce_edit_coupon_columns' ), 9999 );
		add_action( 'manage_shop_coupon_posts_custom_column', array( &$this, 'woocommerce_custom_coupon_columns' ), 3 );
	}
	
	function restriction() {
		global $post, $wp_roles;

		$vals = get_post_meta( $post->ID, 'coupon_roles', true );

		asort( $wp_roles->roles );
		?>
		<p class='form-field wholesale_only_field '>
			<label for='apply_before_tax'><?php _e( 'Valid for these roles', 'ignitewoo_wholesale_pro' )?></label>
			<select name="coupon_roles[]" multiple="multiple" class="select2" style="width: 50%">
			<?php 
			foreach( $wp_roles->roles as $role => $data ) { 
				if ( in_array( $role, $vals ) )
					$selected = 'selected="selected"';
				else 
					$selected = '';
				?>
				<option value="<?php echo $role ?>" <?php echo $selected ?>><?php echo $data['name'] ?></option>
				<?php 
			}
			?>
			</select>
			<span class="help_tip woocommerce-help-tip" data-tip="<?php _e( 'Limit use of this coupon to shoppers with the selected roles', 'ignitewoo_wholesale_pro' ) ?>">
			<script>
			jQuery( '.select2' ).select2();
			</script>
		</p>	
		<?php
	}

	function woocommerce_edit_coupon_columns( $columns ) {
		$columns["valid_for"] = __( "Valid For", 'ignitewoo_wholesale_pro' );
		return $columns;
	}

	function woocommerce_custom_coupon_columns( $column ) {
		global $post, $wp_roles;

		switch ( $column ) {
			case "valid_for" :
					
				$valid_for_roles = get_post_meta( $post->ID, 'coupon_roles', true );

				if ( !$valid_for_roles )
					echo '&ndash;';
				else { 
					$roles = array();
					
					foreach( $valid_for_roles as $role ) {
						$roles[] = $wp_roles->roles[$role]['name'];
					}
					echo implode( ', ', $roles );
				}
				break;
		}
	}
	
	function save_meta( $post_id, $post ) {
		$valid_for_roles = isset( $_POST['coupon_roles'] ) ? $_POST['coupon_roles'] : '';
		update_post_meta( $post_id, 'coupon_roles', $valid_for_roles );
	}

	function is_valid( $valid, $coupon_obj ) {
		global $post, $wp_roles;

		if ( method_exists( $coupon_obj, 'get_id' ) )
			$valid_for_roles = get_post_meta( $coupon_obj->get_id(), 'coupon_roles', true );
		else 
			$valid_for_roles = get_post_meta( $coupon_obj->id, 'coupon_roles', true );

		if ( empty( $valid_for_roles ) )
			return $valid;

		asort( $wp_roles->roles );

		$is_valid = false; 
		
		foreach( $valid_for_roles as $role ) { 
			if ( current_user_can( $role ) ) {
				$is_valid = true; 				
			}
		}

		if ( !$is_valid ) { 
			$this->error = __( 'That is not valid for your account', 'ignitewoo_wholesale_pro' );
			return false;
		}
		
		return $is_valid;
	}

	function error( $error, $coupon_obj ) {
		if  ( isset( $this->error ) && !empty( $this->error ) )
			return $this->error;
		else 
			return $error;
	}
	
}
global $ignitewoo_wholesale_pro_coupons;
$ignitewoo_wholesale_pro_coupons = new IgniteWoo_Wholesale_Pro_Coupons();
