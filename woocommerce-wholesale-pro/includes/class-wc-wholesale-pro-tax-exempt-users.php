<?php
/**
 * Copyright (c) 2013 - 2017 - IgniteWoo.com - All Rights Reserved 
 * 
 */

// The standalone plugin might be installed, so test for that: 
if ( class_exists( 'WC_Tax_Exempt' ) )
	return;
	
class WC_WSP_Tax_Exempt { 

	function __construct() {

		add_action( 'init', array( &$this, 'init' ), 999 );
		
		// Main plugin's woocommerce_init hook is priority 1, so we run after that 
		// to detect user level settings
		add_action( 'woocommerce_init', array( &$this, 'wc_init' ), 999 );
		
		add_action( 'init', array( &$this, 'init' ), 999999 );
		add_action( 'wp', array( &$this, 'init' ), 999999 );
		
		add_filter( 'woocommerce_matched_tax_rates', array( &$this, 'filter_matched_rates' ), 999999, 10 );
	
		add_action( 'admin_head', array( &$this, 'admin_head' ) );
		//add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( &$this, 'plugin_action_links' ) );
		//add_action( 'plugin_row_meta', array( &$this, 'plugin_meta_links' ), 10, 2 );

	}

	function init() { 

		add_action( 'personal_options_update', array( &$this, 'woocommerce_save_customer_meta_fields' ), 5 );
		add_action( 'edit_user_profile_update', array( &$this, 'woocommerce_save_customer_meta_fields' ), 5 );
		add_action( 'show_user_profile', array( &$this, 'woocommerce_customer_meta_fields' ), 5 );
		add_action( 'edit_user_profile', array( &$this, 'woocommerce_customer_meta_fields' ), 5 );
		add_action( 'manage_users_custom_column', array( &$this, 'woocommerce_user_column_values' ), 11, 3 );
		add_filter( 'manage_users_columns',array( &$this, 'woocommerce_user_columns' ), 11, 1 );

		// Deprecated
		// add_filter( 'manage_users_sortable_columns', array( &$this, 'user_sortable_columns' ) );
		
	}
 
	function admin_head() { 
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_style('jquery-style', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
		?>
		<script>
		jQuery( document ).ready( function() { 
			jQuery( '.datepicker' ).datepicker({ dateFormat: 'yy-mm-dd' });
		})
		</script>
		<?php
	}

	function is_user_exempt() { 
		global $current_user, $woocommerce;

		if ( isset( $_POST['post_data'] ) ) { 
		
			$args = wp_parse_args( $_POST['post_data'] );

			if ( !empty( $args ) && !empty( $args['ignitewoo_one_page_checkout_customer_id'] ) )
				$uid = absint( $args['ignitewoo_one_page_checkout_customer_id'] );
				
		}

		if ( isset( $_POST['ignitewoo_one_page_checkout_customer_id'] ) ) {
			$uid = absint( $_POST['ignitewoo_one_page_checkout_customer_id'] );
		}

		if ( !$current_user && !$uid ) {
		
			$current_user = get_currentuserdata();

			if ( empty( $current_user->data->ID ) )
				return;
				
			$uid = $current_user->data->ID; 
		}

		if ( empty( $uid ) && isset( $current_user->data->ID ) ) { 
			$uid = $current_user->data->ID; 
		}
		
		if ( empty( $uid ) )
			return false;
			
		$user_tax_exemptions = get_user_meta( $uid, '_tax_class_exemptions', true );
		
		$te = get_user_meta( $uid, '_tax_exempt', true );
		
		$tex = get_user_meta( $uid, '_tax_id_expires', true );

		if ( 'yes' != $te )
			return false;

		if ( 'yes' == $te && empty( $tex ) ) {
			// If there are exemptions set then tell WC that the user 
			// is not except so that we can filter the matched_tax_rates later via a filter
			if ( !empty( $user_tax_exemptions ) )
				return false;
			else 
				return true;
		}
					
		// Is the expiration date set and in the past? 
		$now = current_time( 'timestamp', false );
		
		$tex = strtotime( $tex );
		
		if ( $now > $tex )
			return false;
			
		if ( !empty( $user_tax_exemptions ) )
			return false;
		else
			return true;
	}
	
	function wc_init() { 

		if ( empty( WC()->customer ) || !method_exists( WC()->customer, 'set_is_vat_exempt' ) )
			return;
		else if ( $this->is_user_exempt() )
			WC()->customer->set_is_vat_exempt( true );
	}

	function filter_matched_rates( $matched_tax_rates, $country, $state, $postcode, $city, $tax_class ) {
		global $current_user;

		// Don't let WP cache the rates since we need to filter them
		$cache_key = WC_Cache_Helper::get_cache_prefix( 'taxes' ) . 'wc_tax_rates_' . md5( sprintf( '%s+%s+%s+%s+%s', $country, $state, $city, $postcode, $tax_class ) );
		wp_cache_delete( $cache_key, 'taxes' );

		if ( isset( $_POST['post_data'] ) ) { 
		
			$args = wp_parse_args( $_POST['post_data'] );

			if ( !empty( $args ) && !empty( $args['ignitewoo_one_page_checkout_customer_id'] ) )
				$uid = absint( $args['ignitewoo_one_page_checkout_customer_id'] );
				
		}

		if ( !$current_user && !$uid ) {
		
			$current_user = get_currentuserdata();

			if ( empty( $current_user->data->ID ) )
				return $matched_tax_rates;
				
			$uid = $current_user->data->ID; 
		}

		if ( empty( $uid ) && isset( $current_user->data->ID ) ) { 
			$uid = $current_user->data->ID; 
		}

		if ( empty( $uid ) )
			return $matched_tax_rates;
			
		$te = get_user_meta( $uid, '_tax_exempt', true );
		
		$tex = get_user_meta( $uid, '_tax_id_expires', true );

		$user_tax_exemptions = get_user_meta( $uid, '_tax_class_exemptions', true );
				
		if ( empty( $user_tax_exemptions ) )
			return $matched_tax_rates;

		if ( 'yes' != $te )
			return $matched_tax_rates;

		if ( 'yes' == $te && empty( $tex ) ) {
			foreach( $user_tax_exemptions as $id ) { 
				if ( isset( $matched_tax_rates[ $id ] ) ) 
					unset( $matched_tax_rates[ $id ] );
			}
			return $matched_tax_rates;
		}

		// Is the expiration date set and in the past? 
		$now = current_time( 'timestamp', false );
		
		$tex = strtotime( $tex );
		
		if ( $now > $tex )
			return $matched_tax_rates;

		foreach( $user_tax_exemptions as $id ) { 
			if ( isset( $matched_tax_rates[ $id ] ) ) 
				unset( $matched_tax_rates[ $id ] );
		}

		return $matched_tax_rates;
	}
	
	function woocommerce_user_columns( $columns ) {

		if ( ! current_user_can( 'manage_woocommerce' ) )
			return $columns;

		$columns['woocommerce_tax_exempt'] = __( 'Tax Exempt', 'ignitewoo_wholesale_pro' );
		$columns['woocommerce_tax_id'] = __( 'Tax ID', 'ignitewoo_wholesale_pro' );
		$columns['woocommerce_tax_id_expires'] = __( 'Tax ID Expires', 'ignitewoo_wholesale_pro' );
		$columns['woocommerce_tax_class_exemptions'] = __( 'Tax Types Exemptions', 'ignitewoo_wholesale_pro' );
		return $columns;
	}


	function woocommerce_user_column_values( $value, $column_name, $user_id ) {
		global $woocommerce, $wpdb;
		
		
		switch ( $column_name ) {
		
			case "woocommerce_tax_exempt" :

				$te = get_user_meta( $user_id, '_tax_exempt', true );
		
				// If there's no tax exempt turned on for the user in their account then 
				// check their user account to see if they have the "no_tax" capability
				// which is set if their role has tax disabled by Wholesale Pro Suite
				if ( empty( $te ) || 'no' == $te ) { 
					$u = get_user_by( 'id', $user_id );
					if ( $u->has_cap( 'no_tax' ) )
						$te = 'yes_role';
				}
		
				if ( 'yes_role' == $te )
					return __( 'Yes &ndash; ( by role )', 'ignitewoo_wholesale_pro' );
				else if ( 'yes' == $te )
					return __( 'Yes', 'ignitewoo_wholesale_pro' );
				else 
					return __( 'No', 'ignitewoo_wholesale_pro' );

				break;
			case "woocommerce_tax_id" :
			
				if ( !$te = get_user_meta( $user_id, 'billing_tax_vat', true ) )
					return '';
				else
					return $te; 
					
				break;
			case "woocommerce_tax_id_expires" :
				
				if ( !$te = get_user_meta( $user_id, '_tax_id_expires', true ) )
					return '';
				else
					return $te; 
				
				break;
			case "woocommerce_tax_class_exemptions" :

				if ( !$te = get_user_meta( $user_id, '_tax_class_exemptions', true ) )
					return '';
				else
					return __( 'Yes', 'ignitewoo_wholesale_pro' ); 
				
				break;
		}
		
		return $value;
	}

	function woocommerce_get_customer_meta_fields() {
		$show_fields = apply_filters('woocommerce_customer_meta_fields', array(
			'tax_exempt' => array(
				'title' => __( 'Tax / Vat Information', 'ignitewoo_wholesale_pro' ),
				'fields' => array(
					'_tax_exempt' => array(
							'label' => __( 'Tax Exempt?', 'ignitewoo_wholesale_pro' ),
							'description' => __( 'Enable', 'ignitewoo_wholesale_pro' ),
							'type' => 'checkbox'
						),
					'billing_tax_vat' => array(
							'label' => __( 'Tax ID', 'ignitewoo_wholesale_pro' ),
							'description' => '',
							'type' => 'text'
						),
					'_tax_id_expires' => array(
							'label' => __( 'Tax ID Expires', 'ignitewoo_wholesale_pro' ),
							'description' => '',
							'type' => 'text'
						),
					'_tax_class_exemptions' => array(
							'label' => __( 'Tax Types Exemptions', 'ignitewoo_wholesale_pro' ),
							'description' => __( 'If you select tax rate exemptions then those rates will not be charged to the shopper', 'ignitewoo_wholesale_pro' ),
							'type' => 'select'
						),
				)
			)
		));
		return $show_fields;
	}

	function woocommerce_customer_meta_fields( $user ) {
		global $wpdb;
		
		if ( ! current_user_can( 'manage_woocommerce' ) )
			return;

		$rate_types = array();
		
		$rates = $wpdb->get_results( "SELECT * FROM `{$wpdb->prefix}woocommerce_tax_rates` order by tax_rate_country" );
		
		if ( !empty( $rates ) ) {
			foreach( $rates as $r ) {
				$label = array();
				if ( !empty( $r->tax_rate_country ) )
				$label[] = $r->tax_rate_country;
				if ( !empty( $r->tax_rate_state ) )
					$label[] = $r->tax_rate_state;
				if ( !empty( $r->tax_rate_name ) )
					$label[] = $r->tax_rate_name;
				if ( !empty( $r->tax_rate ) )
					$label[] = $r->tax_rate;
				$label[] = !empty( $r->tax_rate_class ) ? $r->tax_rate_class : __( 'Standard Rates', 'ignitewoo_wholesale_pro' );
				$label = implode( ' - ', $label );
				$rate_types[ $r->tax_rate_id ] = $label;
			}
		}

		$show_fields = $this->woocommerce_get_customer_meta_fields();

		foreach( $show_fields as $fieldset ) {
			?>
			<h3><?php echo $fieldset['title']; ?></h3>
			<table class="form-table">
				<?php
				foreach( $fieldset['fields'] as $key => $field ) :
					?>
					<tr>
						<th><label for="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $field['label'] ); ?></label></th>
						<td>
						<?php if ( 'text' == $field['type'] ) { ?>
						
							<?php if ( '_tax_id_expires' == $key ) $class = 'datepicker'; else $class = ''; ?>
							
							<input type="text" class="<?php echo $class ?>" name="<?php echo esc_attr( $key ); ?>" id="<?php echo esc_attr( $key ); ?>" value="<?php echo esc_attr( get_user_meta( $user->ID, $key, true ) ); ?>" class="regular-text" /><br/>
							
							<span class="descriptions"><?php echo wp_kses_post( $field['description'] ); ?></span>
							
						<?php } else if ( 'checkbox' == $field['type'] ) { ?>

							<input type="checkbox" name="<?php echo esc_attr( $key ); ?>" id="<?php echo esc_attr( $key ); ?>" value="yes" <?php checked( get_user_meta( $user->ID, $key, true , false ), 'yes', true ) ?>/> 
							
							<span class="description"><?php echo wp_kses_post( $field['description'] ); ?></span>
							
						<?php } else { ?>
						
							<?php 
								$tax_classes = get_user_meta( $user->ID, $key, true );
								if ( empty( $tax_classes ) )
									$tax_classes = array();
							?>
							
							<select class="tax_select2" name="<?php echo esc_attr( $key ); ?>[]" id="<?php echo esc_attr( $key ); ?>" multiple="multiple" style="width:400px"/>
							<?php foreach( $rate_types as $id => $label ) { ?>
								<?php 
								
								if ( in_array( $id, $tax_classes ) ) 
									$selected = 'selected="selected"';
								else 
									$selected = ''; 
								?>
								<option value="<?php echo $id ?>" <?php echo $selected ?>><?php echo $label ?></option>
							<?php } ?>
							</select>
							
							<span class="description"><?php echo wp_kses_post( $field['description'] ); ?></span>
						<?php } ?>
						</td>
					</tr>
					<?php
				endforeach;
				?>
			</table>
			<script>
			jQuery( document ).ready( function($) { 
				$( '.tax_select2' ).select2();
			})
			</script>
			<?php
		}
	}

	function woocommerce_save_customer_meta_fields( $user_id ) {
		if ( ! current_user_can( 'manage_woocommerce' ) )
			return $columns;

		$save_fields = $this->woocommerce_get_customer_meta_fields();

		foreach( $save_fields as $fieldset )
			foreach( $fieldset['fields'] as $key => $field )
				if ( isset( $_POST[ $key ] ) ) {
				var_dump( $key, $_POST[ $key ] );
					update_user_meta( $user_id, $key, woocommerce_clean( $_POST[ $key ] ) );
				} else { 
					update_user_meta( $user_id, $key, '' );
				}
				
					
		if ( empty( $_POST['_tax_exempt'] ) ) 
			update_user_meta( $user_id, '_tax_exempt', 'no' );
	}
	
	
	function user_sortable_columns( $columns ) {
		$columns['woocommerce_tax_exempt'] = 'woocommerce_tax_exempt';
		//$columns['woocommerce_tax_id'] = 'woocommerce_tax_id';
		//$columns['woocommerce_tax_id_expires'] = 'woocommerce_tax_id_expires';
		return $columns;
	}
	

	/*
	function user_column_orderby( $vars ) {
		global $pagenow;

		if ( isset( $vars['orderby'] ) ) 
		switch( $vars['orderby'] ) {
		
			case 'woocommerce_tax_exempt' : 
				$vars = array_merge( $vars, array(
					'meta_key' => '_tax_exempt',
					'orderby' => 'meta_value',
					//'order'     => 'asc'
				) );
				
				break;
			case 'woocommerce_tax_id' : 
				$vars = array_merge( $vars, array(
					'meta_key' => '_tax_id',
					'orderby' => 'meta_value',
					//'order'     => 'asc'
				) );
				
				break;
			case 'woocommerce_tax_id_expires' : 
				$vars = array_merge( $vars, array(
					'meta_key' => '_tax_id_expires',
					'orderby' => 'meta_value',
					//'order'     => 'asc'
				) );
				
				break;
		}

		return $vars;
	}
	
	public static function user_query( $query ) { 

		if( 'woocommerce_tax_exempt' == $query->query_vars['orderby'] ) {//check if church is the column being sorted
			global $wpdb;
		
			$query->query_from .= " LEFT JOIN " . $wpdb->usermeta . " AS ualias ON ID = ualias.user_id ";
			$query->query_where .= " AND ( ualias.meta_key = '_tax_exempt' ) OR ( ualias.meta_key = '_tax_exempt' AND ualias.meta_value = NULL ) ";
			$query->query_orderby = " ORDER BY ualias.meta_value ". ( $query->query_vars["order"] == "ASC" ? "asc " : "desc " );
		}
	
		return $query;
	}
	*/
	
}
new WC_WSP_Tax_Exempt();
	
// add_filter( 'pre_user_query',  'WC_WSP_Tax_Exempt::user_query', 1, 1 );
