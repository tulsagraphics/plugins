<?php
/*
 * Copyright (c) 2016 - IgniteWoo.com - ALL RIGHTS RESERVED
 * Copyright (c) 2012 - Milan Petrovic
 *
 *
 * Example: 
 * [wsp_hide_or_show roles="administrator" mode="hide" restricted_message="blah<br/>blah" restricted_style="margin-bottom:1em;color:#cf0000"] 
 * 
 */

class IGN_Wholesale_Pro_Hide_This {
	
	function __construct() {
		add_shortcode( 'wsp_hide_or_show', array(
			 &$this,
			'shortcode' 
		) );
	}
	
	function shortcode( $atts, $content = null ) {
		$defaults = array(
			'class' => '',
			'style' => '',
			'mode' => 'show',
			'logged_in' => null,
			'roles' => null,
			'caps' => null,
			'restricted_message' => '',
			'restricted_class' => 'access-restricted',
			'restricted_style' => '' 
		);
		
		$atts = shortcode_atts( $defaults, $atts );

		$to_show = true;
		$has_role = false;
		$user_can = false;
		$logged_in = false;
		
		if ( $to_show && isset( $atts[ 'roles' ] ) && !is_null( $atts[ 'roles' ] ) ) {
			$atts[ 'roles' ] = explode( ',', $atts[ 'roles' ] );
			$atts[ 'roles' ] = array_map( 'trim', $atts[ 'roles' ] );
			
			$has_role = $this->is_current_user_roles( $atts[ 'roles' ] );
		}
		
		if ( $to_show && isset( $atts[ 'caps' ] ) && !is_null( $atts[ 'caps' ] ) ) {
			$atts[ 'caps' ] = explode( ',', $atts[ 'caps' ] );
			$atts[ 'caps' ] = array_map( 'trim', $atts[ 'caps' ] );
			
			$user_can = $this->current_user_can( $atts[ 'caps' ] );
		}
		
		// If the checks passed for roles and/or users then $to_show will be "true" 
		// So if the mode is "hide" then set $to_show to false so that no shortcode content is returned
		// unless there's a restrict message set
		if ( $has_role && !empty( $atts['mode'] ) && 'hide' == $atts['mode'] )
			$to_show = false;
		else if ( $has_role && !empty( $atts['mode'] ) && 'show' == $atts['mode'] )
			$to_show = true;
		else if ( !$has_role && !empty( $atts['mode'] ) && 'show' == $atts['mode'] )
			$to_show = false;
		else if ( !$has_role && !empty( $atts['mode'] ) && 'hide' == $atts['mode'] )
			$to_show = false;	

		// The logged_in attr overrides roles and caps
		if ( isset( $atts[ 'logged_in' ] ) && !is_null( $atts[ 'logged_in' ] ) ) {
			
			$logged_in = is_user_logged_in();
			
			if ( $logged_in && !empty( $atts['mode'] ) && 'hide' == $atts['mode'] )
				$to_show = false;
			else if ( $logged_in && !empty( $atts['mode'] ) && 'show' == $atts['mode'] )
				$to_show = true;
			else if ( !$logged_in && !empty( $atts['mode'] ) && 'hide' == $atts['mode'] )
				$to_show = false;
			else if ( !$logged_in && !empty( $atts['mode'] ) && 'show' == $atts['mode'] )
				$to_show = true;
		}
		
		$to_show = apply_filters( 'd4p_access_control', $to_show, $atts, $content );
		
		$class = $atts[ 'class' ];
		$style = $atts[ 'style' ];

		if ( !$to_show ) {
			$content = $atts[ 'restricted_message' ];
			$class   = $atts[ 'restricted_class' ];
			$style   = $atts[ 'restricted_style' ];
		}

		// HTML allowed in the restricted_message
		$content = html_entity_decode( $content );

		return '<div class="' . $atts['restricted_class'] . '" style="' . $atts['restricted_style' ]. '">' . do_shortcode( $content ) . '</div>';
	}
	
	function is_current_user_roles( $roles = array() ) {
		global $current_user;
		$roles = (array) $roles;
		
		if ( is_array( $current_user->roles ) && !empty( $roles ) ) {
			$match = array_intersect( $roles, $current_user->roles );
			
			return !empty( $match );
		} else {
			return false;
		}
	}
	
	function current_user_can( $caps = array() ) {
		$caps = (array) $caps;
		
		foreach ( $caps as $cap ) {
			if ( current_user_can( $cap ) ) {
				return true;
			}
		}
		
		return false;
	}
}

new IGN_Wholesale_Pro_Hide_This();
