<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * OPC Dummy Gateway
 */
class IgniteWoo_Wholesale_Pro_Invoice_Gateway extends WC_Payment_Gateway {

	/**
	* Constructor for the gateway.
	*/
	public function __construct( $instance_id = 0 ) {
		$this->id                 = 'ignitewoo_wholesale_pro_invoice_gateway';
		$this->icon               = apply_filters('woocommerce_cheque_icon', '');
		$this->has_fields         = false;
		$this->method_title       = __( 'Invoice Payment', 'ignitewoo_wholesale_pro' );
		$this->method_description = __( 'Allows shoppers to checkout without the need to enter any payment information. Useful when you want to send your customers an invoice, etc.', 'ignitewoo_wholesale_pro' );

		$this->instance_id = absint( $instance_id );
		
		//if ( $this->instance_id > 0 || ( version_compare( WC_VERSION, '2.6', '>' ) && is_admin() && !defined( 'DOING_AJAX' ) ) ) { 
			$this->supports = array(
				'settings',
				'shipping-zones',
				'instance-settings',
				//'instance-settings-modal',
			);
		//}
	
		// Load the settings.
		$this->init();

		// Actions
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_thankyou_ignitewoo_wholesale_pro_invoice_gateway', array( $this, 'thankyou_page' ) );

		// Customer Emails
		add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );

	}
	
	function init() { 
	
		if ( $this->instance_id > 0 ) { 
			$this->instance_form_fields = $this->get_the_settings();
			$this->init_form_fields();
			$this->init_settings();
		} else { 
			$this->init_form_fields();
			$this->init_settings();
		}
	
	
		// Define user set variables
		$this->title        = $this->get_option( 'title' );
		$this->description  = $this->get_option( 'description' );
		$this->instructions = $this->get_option( 'instructions', $this->description );
		
	}

	function get_instance_form_fields() { 
		return $this->get_the_settings(); 
	}
	
	// Init form fields - modeled on WooCommerce core code
	function init_form_fields() {
		$this->form_fields = $this->get_the_settings();
	}
	
	/**
	* Initialise Gateway Settings Form Fields
	*/
	public function get_the_settings() {

		return array(
				'enabled' => array(
					'title'   => __( 'Enable/Disable', 'ignitewoo_wholesale_pro' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable Gateway', 'ignitewoo_wholesale_pro' ),
					'default' => ''
				),
				'enabled_all' => array(
					'title'   => __( 'Enable for All', 'ignitewoo_wholesale_pro' ),
					'type'    => 'checkbox',
					'label'   => __( 'Allow use by all shoppers', 'ignitewoo_wholesale_pro' ),
					'default' => '',
					'description' => __( 'When enabled all shoppers will see this payment method on your checkout page. When disabled only shoppers that have one of your custom roles can see this gateway at checkout - unless their role is set to not offer this gateway.', 'ignitewoo_wholesale_pro' ),
					'desc_tip' => true,
				),
				'title' => array(
					'title'       => __( 'Title', 'ignitewoo_wholesale_pro' ),
					'type'        => 'text',
					'description' => __( 'This controls the title which the user sees during checkout.', 'ignitewoo_wholesale_pro' ),
					'default'     => __( 'Invoice Payment', 'ignitewoo_wholesale_pro' ),
					'desc_tip'    => true,
				),
				'description' => array(
					'title'       => __( 'Description', 'ignitewoo_wholesale_pro' ),
					'type'        => 'textarea',
					'description' => __( 'Payment method description that the customer will see on your checkout.', 'ignitewoo_wholesale_pro' ),
					'default'     => __( 'Please send your make payment to Store Name, Store Street, Store Town, Store State / County, Store Postcode.', 'ignitewoo_wholesale_pro' ),
					'desc_tip'    => true,
				),
				'instructions' => array(
					'title'       => __( 'Instructions', 'ignitewoo_wholesale_pro' ),
					'type'        => 'textarea',
					'description' => __( 'Instructions that will be added to the thank you page and emails.', 'ignitewoo_wholesale_pro' ),
					'default'     => '',
					'desc_tip'    => true,
				),
			);

	}

	/**
	* Output for the order received page.
	*/
	public function thankyou_page() {
		if ( $this->instructions )
			echo wpautop( wptexturize( $this->instructions ) );
	}

	/**
	* Add content to the WC emails.
	*
	* @access public
	* @param WC_Order $order
	* @param bool $sent_to_admin
	* @param bool $plain_text
	*/
	public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
		
		if ( method_exists( $order, 'get_payment_method' ) )
			$method = $order->get_payment_method();
		else 
			$method = $order->payment_method;
		
		if ( $this->instructions && ! $sent_to_admin && 'ignitewoo_wholesale_pro_invoice_gateway' === $method && $order->has_status( 'on-hold' ) ) {
			echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
		}
	}

	/**
	* Process the payment and return the result
	*
	* @param int $order_id
	* @return array
	*/
	public function process_payment( $order_id ) {

		$order = wc_get_order( $order_id );

		// Mark as on-hold (we're awaiting the cheque)
		$order->update_status( 'on-hold', __( 'Awaiting payment', 'ignitewoo_wholesale_pro' ) );

		// Reduce stock levels
		$order->reduce_order_stock();

		// Remove cart
		WC()->cart->empty_cart();

		// Return thankyou redirect
		return array(
			'result' 	=> 'success',
			'redirect'	=> $this->get_return_url( $order )
		);
	}
}
