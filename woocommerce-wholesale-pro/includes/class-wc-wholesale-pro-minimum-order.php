<?php
/*
 * Copyright � 2014 IgniteWoo.com - All Rights Reserved
*/

class IgniteWoo_Wholesale_Pro_Minimum_Cart_Total_Quantity {

	var $settings;
	var $minimum_order_quantity;
	var $maximum_order_quantity;
	var $minimum_order_value;
	var $maximum_order_value;
	var $excludes = array();

	public function __construct() {

		$this->plugin_url = WP_PLUGIN_URL . '/' . str_replace( basename( __FILE__ ), '' , plugin_basename( __FILE__ ) );
		
		add_action( 'init', array( &$this, 'init' ), 20 );		
	}

	function init() { 
		global $ign_wholesale_pro_suite;

		$this->settings = $ign_wholesale_pro_suite->settings;

		if ( empty( $this->settings['enable_min_max'] ) || 'yes' !== $this->settings['enable_min_max'] )
			return;
			
		add_action( 'wp_head', array( &$this, 'wp_head' ), 10 );

		// Notices
		//add_action( 'woocommerce_checkout_process', array( &$this, 'minimum_order_amount' ) );
		//add_action( 'woocommerce_before_cart_table', array( &$this, 'cart_notice' ), 1 );
		//add_action( 'woocommerce_before_checkout_form', array( &$this, 'cart_notice' ), 20 );
		
		// Product pages
		add_filter( 'woocommerce_quantity_input_max',  array( &$this, 'input_max' ), 10, 2 );
		add_filter( 'woocommerce_quantity_input_min',  array( &$this, 'input_min' ), 10, 2 );
		add_filter( 'woocommerce_quantity_input_step',  array( &$this, 'input_step' ), 10, 2 );
		add_filter( 'woocommerce_available_variation',  array( &$this, 'available_variation' ), 10, 3 );
		
		// Shop and category pages 
		add_filter( 'woocommerce_loop_add_to_cart_args',  array( &$this, 'add_to_cart_args' ), 10, 3 );
		
		
		// Qty input for the cart
		add_filter( 'woocommerce_quantity_input_args',  array( &$this, 'woocommerce_quantity_input_args' ), 10, 2 );

		// Validate
		add_filter( 'woocommerce_add_to_cart_validation', array( &$this, 'add_to_cart' ), 10, 4 );
		
		// Check the cart items
		add_action( 'woocommerce_check_cart_items', array( &$this, 'check_cart_items' ) );
		
	}
	
	function wp_head() { 
		if ( !function_exists( 'is_product' ) || !is_product() )
			return;
		?>
		<script>
		jQuery( document ).ready( function($) {
			setTimeout( function() { 
				$variation_form = $( 'form.variations_form' );
				if ( 'undefined' !== typeof $variation_form && null !== $variation_form && $variation_form.length > 0 ) { 
					$( '.variations_form' ).on( 'found_variation', function( event, variation ) { 
					var $single_variation_wrap = $variation_form.find( '.single_variation_wrap' );
					if ( variation.step !== '' )
						$single_variation_wrap.find( '.quantity input.qty' ).attr( 'step', variation.step );
					else
						$single_variation_wrap.find( '.quantity input.qty' ).attr( 'step', 1 );
					});
				};
			}, 1000 );
		})
		</script>
		<?php
	}
	
	function find_matching_rule() { 
		global $current_user; 
		
		if ( function_exists( 'wp_get_current_user' ) )
			wp_get_current_user();
		else 
			get_currentuserinfo();
		
		if ( empty( $this->settings['rules']['rule_index'] ) )
			return false;
				
		$rule = false;
		
		$user_role = null;
		
		if ( !empty( $current_user ) ) {
			$user_roles = $current_user->roles;
			$user_role = array_shift( $user_roles );
		}

		foreach ( $this->settings['rules']['rule_index'] as $i => $vals ) { 

			// Check for a rule for non-logged-in users
			if ( empty( $user_role ) && empty( $this->settings['rules']['roles'][$i] ) && empty( $this->settings['rules']['users'][$i] ) ) {

				$rule = array( 
					//'roles' => $this->settings['rules']['roles'][$i],
					'cart_total' => $this->settings['rules']['cart_total'][$i],
					'cart_total_max' => $this->settings['rules']['cart_total_max'][$i],
					'cart_qty' => $this->settings['rules']['cart_qty'][$i],
					'product_qty' => $this->settings['rules']['product_qty'][$i],
					'order_qty' => $this->settings['rules']['order_qty'][$i]
				);
				
				break;
			}
			
			// Check for logged in users
			if ( !empty( $user_role ) && !empty( $this->settings['rules']['roles'][$i] ) ) {

				if ( !is_array( $this->settings['rules']['roles'][$i] ) )
					$this->settings['rules']['users'][$i] = explode( ',', $this->settings['rules']['roles'][$i] );
					
				if ( !in_array( $user_role, $this->settings['rules']['roles'][$i] ) )
					continue;
					
				// Matched
				$rule = array( 
					//'roles' => $this->settings['rules']['roles'][$i],
					'cart_total' => $this->settings['rules']['cart_total'][$i],
					'cart_total_max' => $this->settings['rules']['cart_total_max'][$i],
					'cart_qty' => $this->settings['rules']['cart_qty'][$i],
					'product_qty' => $this->settings['rules']['product_qty'][$i],
					'order_qty' => $this->settings['rules']['order_qty'][$i]
				);

				break;
			}

			if ( !empty( $current_user->ID ) && !empty( $this->settings['rules']['users'][$i] ) ) {
			
				if ( !is_array( $this->settings['rules']['users'][$i] ) )
					$this->settings['rules']['users'][$i] = explode( ',', $this->settings['rules']['users'][$i] );

				if ( !in_array( $current_user->ID, $this->settings['rules']['users'][$i] ) )
					continue;
					
				// Matched
				$rule = array( 
					//'roles' => $this->settings['rules']['roles'][$i],
					'cart_total' => $this->settings['rules']['cart_total'][$i],
					'cart_total_max' => $this->settings['rules']['cart_total_max'][$i],
					'cart_qty' => $this->settings['rules']['cart_qty'][$i],
					'product_qty' => $this->settings['rules']['product_qty'][$i],
					'order_qty' => $this->settings['rules']['order_qty'][$i]
				);
				
				break;
			}

		}

		return $rule; 
	
	}
	
	function find_matching_product_rule( $pid, $vid = '' ) { 
		global $current_user; 
		
		if ( function_exists( 'wp_get_current_user' ) )
			wp_get_current_user();
		else 
			get_currentuserinfo();
		
		if ( !empty( $vid ) ) { 
			$rules_on = get_post_meta( $vid, 'min_max_rules', true );

			if ( 'yes' == $rules_on )
				$the_rules = get_post_meta( $vid, 'var_min_max_rules', true );
		}

		// Load product rules if $the_rules is empty 
		if ( empty( $the_rules ) || empty( $the_rules['rule_index'] ) )
			$the_rules = get_post_meta( $pid, 'min_max_rules', true );

		if ( empty( $the_rules ) || empty( $the_rules['rule_index'] ) )
			return false;

		$rule = false;
		
		$user_role = null;
		
		if ( !empty( $current_user ) ) {
			$user_roles = $current_user->roles;
			$user_role = array_shift( $user_roles );
		}

		$i = -1;

		for( $x = 0; $x <= count( $the_rules['rule_index'] ); $x++ ) {

			$i++;

			if ( empty( $the_rules['min'][$i] ) && empty( $the_rules['max'][$i] ) && empty( $the_rules['inc'][$i] ) )
				continue;
					
			// Check for a rule for non-logged-in users
			if ( empty( $user_role ) && empty( $the_rules['roles'][$i] ) && empty( $the_rules['users'][$i] ) ) {

				$rule = array( 
					//'roles' => isset( $the_rules['roles'][$i] ) ? $the_rules['roles'][$i] : '',
					'min' => $the_rules['min'][$i],
					'max' => $the_rules['max'][$i],
					'inc' => $the_rules['inc'][$i]
				);

				break;
			}

			// Check for logged in users
			if ( !empty( $user_role ) && isset( $the_rules['roles'][$i] ) && is_array( $the_rules['roles'][$i] ) ) {

				if ( !in_array( $user_role, $the_rules['roles'][$i] ) )
					continue;
					
				// Matched
				$rule = array( 
					//'roles' => isset( $the_rules['roles'][$i] ) ? $the_rules['roles'][$i] : '',
					'min' => $the_rules['min'][$i],
					'max' => $the_rules['max'][$i],
					'inc' => $the_rules['inc'][$i]
				);

				break;
			}
			
			// Check for logged in users
			if ( !empty( $current_user->ID ) && isset( $the_rules['users'][$i] ) && is_array( $the_rules['users'][$i] ) ) {

				if ( !in_array( $current_user->ID, $the_rules['users'][$i] ) )
					continue;
					
				// Matched
				$rule = array( 
					//'roles' => isset( $the_rules['roles'][$i] ) ? $the_rules['roles'][$i] : '',
					'min' => $the_rules['min'][$i],
					'max' => $the_rules['max'][$i],
					'inc' => $the_rules['inc'][$i]
				);

				break;
			}

			// Check for logged in user but no roles or users set for the rule
			if ( !empty( $user_role ) && empty( $the_rules['roles'][$i] ) && empty( $the_rules['users'][$i] ) ) {

				$rule = array( 
					//'roles' => isset( $the_rules['roles'][$i] ) ? $the_rules['roles'][$i] : '',
					'min' => $the_rules['min'][$i],
					'max' => $the_rules['max'][$i],
					'inc' => $the_rules['inc'][$i]
				);

				break;
			}
		}

		return $rule; 
	}
	
	function input_min( $data, $product ) {
	
		$product_id = ign_get_product_id( $product, false );
		$variation_id = ign_get_product_id( $product, true );
		
		if ( !empty( $variation_id ) )
			$vid = $variation_id;
		else 
			$vid = null;
			
		$rule = $this->find_matching_product_rule( $product_id, $vid );
		
		if ( empty( $rule ) )
			return $data;

		if ( !empty( $rule['min'] ) ) {
			return $rule['min'];
		}
		return $data;
	}

	function input_max( $data, $product ) {

		$product_id = ign_get_product_id( $product, false );
		$variation_id = ign_get_product_id( $product, true );
	
		if ( !empty( $variation_id ) )
			$vid = $variation_id;
		else 
			$vid = null;
			
		$rule = $this->find_matching_product_rule( $product_id, $vid );
 
		if ( empty( $rule ) )
			return $data;

		if ( !empty( $rule['max'] ) ) {
			return $rule['max'];
		}
		
		return $data;
	}

	function add_to_cart_args( $args, $product ) {

		$product_id = ign_get_product_id( $product, false );
		$variation_id = ign_get_product_id( $product, true );
		
		if ( !empty( $variation_id ) )
			$vid = $variation_id;
		else 
			$vid = null;
			
		$rule = $this->find_matching_product_rule( $product_id, $vid );

		if ( empty( $rule ) )
			return $args;

		if ( !empty( $rule['min'] ) ) {
			$args['quantity'] = $rule['min'];
		}
		
		return $args;
		
	}
	
	function woocommerce_quantity_input_args( $args, $product ) { 
	
		$product_id = ign_get_product_id( $product, false );
		$variation_id = ign_get_product_id( $product, true );
		
		if ( !empty( $variation_id ) )
			$vid = $variation_id;
		else 
			$vid = null;
			
		$rule = $this->find_matching_product_rule( $product_id, $vid );
		
		if ( empty( $rule ) )
			return $args;

		if ( !empty( $rule['min'] ) ) {
			$args['min_value'] = $rule['min'];
		}
		
		if ( !empty( $rule['max'] ) ) {
			$args['max_value'] = $rule['max'];
		}
		
		if ( !empty( $rule['inc'] ) ) {
			$args['step'] = $rule['inc'];
		}
		
		return $args;
	
	}
	
	function input_step( $data, $product ) {

		$product_id = ign_get_product_id( $product, false );
		$variation_id = ign_get_product_id( $product, true );
		
		if ( !empty( $variation_id ) )
			$vid = $variation_id;
		else 
			$vid = null;
			
		$rule = $this->find_matching_product_rule( $product_id, $vid );
	
		if ( empty( $rule ) )
			return $data;

		if ( !empty( $rule['inc'] ) ) {
			return $rule['inc'];
		}
		return $data;
	}

	function available_variation( $data, $product, $variation ) {

		$product_id = ign_get_product_id( $product, false );
		$variation_id = ign_get_product_id( $variation, true );
		
		$rule = $this->find_matching_product_rule( $product_id, $variation_id );

		if ( empty( $rule ) )
			return $data; 
			
		if ( !empty( $rule['min'] ) ) {
			$data['min_qty'] = $rule['min'];
		}

		if ( !empty( $rule['max'] ) ) {
			$data['max_qty'] = $rule['max'];
		}
		
		if ( !empty( $rule['inc'] ) ) {
			$data['step'] = $rule['inc'];
		}

		return $data;
	}
	
	/*
	function cart_notice() { 

		$this->maybe_add_cart_messages();

		if ( function_exists( 'wc_print_notices' ) )
			wc_print_notices();
		else 
			woocommerce_show_messages();
	
	}

	function minimum_order_amount() {

		$this->maybe_add_cart_messages();
		
	}
	*/
	
	// Messages regarding cart contents - based on global rules
	function maybe_add_cart_messages( $excludes = '' ) { 
		global $woocommerce, $user_ID, $wp_version; 

		$rule = $this->find_matching_rule();

		if ( empty( $rule ) )
			return;
			
		$added = false;

		if ( ( floatval( $rule['cart_total'] ) > 0 ) && $woocommerce->cart->subtotal < $rule['cart_total'] ) {
					
			if ( function_exists( 'wc_price' ) )
				$price = wc_price( $rule['cart_total'] );
			else 
				$price = wc_price( $rule['cart_total'] );
				
			$msg = str_replace( '{amount}', $price, $this->settings['min_cart_total_error_message'] ) . $excludes;
			wc_add_notice( $msg, 'error' );
			$added = true;
				
		}
		
		if ( ( floatval( $rule['cart_total_max'] ) > 0 ) && $woocommerce->cart->subtotal > $rule['cart_total_max'] ) {

			if ( function_exists( 'wc_price' ) )
				$price = wc_price( $rule['cart_total_max'] );
			else 
				$price = wc_price( $rule['cart_total_max'] );
				
			$msg = str_replace( '{amount}', $price, $this->settings['max_cart_total_error_message'] );
			wc_add_notice( $msg, 'error' );
			$added = true;	
		}
		
		$qty_in_cart = 0;

		foreach( $woocommerce->cart->get_cart() as $item => $data ) 
			$qty_in_cart += $data['quantity'];
		
		if ( $qty_in_cart < $rule['cart_qty'] ) {
					
			$msg = str_replace( '{quantity}', $rule['cart_qty'], $this->settings['min_cart_qty_error_message'] ) . $excludes;
			wc_add_notice( $msg, 'error' );
			$added = true;
		}
		
		$show_msg = false;
		
		$qty = array();
		
		// Do this counting by product ID, or variation ID
		foreach( $woocommerce->cart->get_cart() as $item => $data ) {

			//$product_id = ign_get_product_id( $data, false );
			//$variation_id = ign_get_product_id( $data, true );
			
			if ( isset( $data['variation_id'] ) && !empty( $data['variation_id'] ) )
				@$qty[ $data['variation_id'] ] += $data['quantity'];
			else 
				@$qty[ $data['product_id'] ] += $data['quantity'];
		}

		if ( !empty( $rule['product_qty'] ) )
		foreach( $qty as $count ) {
			if ( $count < $rule['product_qty'] )
				$show_msg = true; 
		}

		if ( $show_msg ) { 

			$msg = str_replace( '{quantity}', $rule['product_qty'], $this->settings['min_product_qty_error_message'] );
			wc_add_notice( $msg, 'error' );
			$added = true;
		}
		
		if ( $added && is_user_logged_in() && !empty( $rule['order_qty'] ) && absint( $rule['order_qty'] ) > 0 ) { 
			$order_count = wc_get_customer_order_count( $user_ID );
			if ( $order_count < absint( $rule['order_qty'] ) ) { 
				$msg = !empty( $this->settings['min_product_qty_error_message'] ) ? $this->settings['min_order_qty_required_message'] : __( 'These limitations are effective until you have at least {quantity} paid orders', 'ignitewoo_wholesale_pro' );
				$msg = str_replace( '{quantity}', absint( $rule['order_qty'] ), $msg );
				wc_add_notice( $msg, 'notice' );
			}
		}
	}
	

	function check_cart_items() {

		$checked_ids = $product_quantities = array();
		
		$total_quantity = $total_cost = 0;
		
		$cart_excludes = false;

		// Count items + variations first
		foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {

			//$product_id = ign_get_product_id( $values, false );
			//$variation_id = ign_get_product_id( $values, true );
			
			if ( ! isset( $product_quantities[ $values['product_id'] ] ) )
				$product_quantities[ $values['product_id'] ] = 0;

			if ( $values['variation_id'] ) {

				if ( ! isset( $product_quantities[ $values['variation_id'] ] ) )
					$product_quantities[ $values['variation_id'] ] = 0;

				$rules_on = get_post_meta( $values['variation_id'], 'min_max_rules', true );

				if ( 'yes' == $rules_on  )
					$product_quantities[ $values['variation_id'] ] += $values['quantity'];
				else
					$product_quantities[ $values['product_id'] ] += $values['quantity'];
			} else {
			
				$product_quantities[ $values['product_id'] ] += $values['quantity'];
			}
		}

		$excludes_in_cart = 0;
		
		// Check cart items
		foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {

			//$product_id = ign_get_product_id( $values, false );
			//$variation_id = ign_get_product_id( $values, true );
			
			if ( $values['variation_id'] ) {
			
				$rules_on = get_post_meta( $values['variation_id'], 'min_max_rules', true );

				if ( 'yes' == $rules_on )
					$checking_id = $values['variation_id'];
				else
					$checking_id = $values['product_id'] ;

			} else {
				$checking_id = $values['product_id'] ;
			}

			if ( in_array( $checking_id, $checked_ids ) )
				continue;

			$checked_ids[] = $checking_id;

			$product = $values['data'];

			$rule = $this->find_matching_product_rule( $values['product_id'] , $values['variation_id'] );
			
			if ( empty( $rule ) )
				$rule = array();
				
			if ( empty( $rule['min'] ) )
				$rule['min'] = '';
				
			if ( empty( $rule['max'] ) )
				$rule['max'] = '';
				
			if ( empty( $rule['inc'] ) )
				$rule['inc'] = '';
				
			$do_not_count = get_post_meta( $checking_id, 'minmax_do_not_count', true );
			$cart_exclude = get_post_meta( $checking_id, 'minmax_cart_exclude', true );

			$this->maybe_add_product_rule_messages( $product, $product_quantities[ $checking_id ], $rule['min'], $rule['max'], $rule['inc'] );

			if ( 'yes' == $do_not_count || 'yes' == $cart_exclude ) {
				$this->excludes[] = $this->get_formatted_name( $product );
				$excludes_in_cart++;
			} else {
				$total_quantity += $product_quantities[ $checking_id ];
				$total_cost += $product->get_price() * $product_quantities[ $checking_id ];
			}

			if ( 'yes' !== $cart_exclude )
				$cart_excludes = true;

		}

		$excludes = '';

		if ( $cart_excludes ) {
			if ( sizeof( $this->excludes ) > 0 ) {
				$excludes = ' (' . __( 'Excludes ', 'ignitewoo_wholesale_pro' ) . implode( ', ', $this->excludes ) . ')';
			}
		}

		if ( $excludes_in_cart != sizeof( WC()->cart->get_cart() ) )
			$this->maybe_add_cart_messages( $excludes );
	}

	// Product related messages - based on per-product rule settings
	function maybe_add_product_rule_messages( $product, $quantity, $minimum_quantity, $maximum_quantity, $group_of_quantity ) {
		
		if ( $product->is_type( 'variation' ) )
			$title = $this->get_formatted_name( $product );
		else 
			$title = $product->get_title();
				
		if ( $minimum_quantity > 0 && ( $quantity < $minimum_quantity ) ) {

			$msg = str_replace( '{product}', $title, $this->settings['min_product_qty_required_message'] );
			$msg = str_replace( '{quantity}', $minimum_quantity, $msg );
			
			wc_add_notice( $msg, 'error' );

		} elseif ( $maximum_quantity > 0 && ( $quantity > $maximum_quantity ) ) {

			$msg = str_replace( '{product}', $title, $this->settings['max_product_qty_allowed_message'] );
			$msg = str_replace( '{quantity}', $maximum_quantity, $msg );
			
			wc_add_notice( $msg, 'error' );

		}

		if ( $group_of_quantity > 0 && ( $quantity % $group_of_quantity ) ) {

			$msg = str_replace( '{product}', $title, $this->settings['inc_product_qty_required_message'] );
			$msg = str_replace( '{quantity}', $group_of_quantity, $msg );
			$msg = str_replace( '{difference}', ($group_of_quantity - ( $quantity % $group_of_quantity ) ), $msg );
			
			wc_add_notice( $msg, 'error' );

		}
	}
	
	public function get_formatted_name( $product ) {

			if ( $product->get_sku() )
				$identifier = $product->get_sku() . ' &ndash;';
			else
				$identifier = ''; //$identifier = '#' . $product->variation_id;

			if ( $product->is_type( 'variation' ) || $product->is_type( 'variable' ) ) { 
			$attributes = $product->get_variation_attributes();
			$extra_data = ' &ndash; ' . implode( ', ', $attributes );
		} else { 
			$extra_data = '';
		}
			return sprintf( __( '%s %s%s', 'ignitewoo_wholesale_pro' ), $identifier, $product->get_title(), $extra_data );
	}

	function add_to_cart( $pass, $product_id, $quantity, $variation_id = 0 ) {

		$rule_for_variaton = false;
		
		$rule = $this->find_matching_product_rule( $product_id, $variation_id );
				
		if ( empty( $rule ) || empty( $rule['max'] ) )
			return $pass;
			
		$maximum_quantity = $rule['max'];

		$total_quantity = $quantity;

		// Count items
		foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
			if ( $values['variation_id'] == $variation_id ) {
				$total_quantity += $values['quantity'];
			} else if ( $values['product_id'] == $product_id ) {
				$total_quantity += $values['quantity'];
			}
		}

		if ( $total_quantity > 0 && $total_quantity > $maximum_quantity ) {

			if ( function_exists( 'wc_get_product' ) ) {
				$_product = wc_get_product( $product_id );
			} else {
				$_product = get_product( $product_id );
			}

			$msg = str_replace( '{product}', $_product->get_title(), $this->settings['max_product_qty_allowed_message'] );
			$msg = str_replace( '{quantity}', $this->settings['inc_product_qty_required_message'], $msg );
			$msg .= sprintf( __( ' ( you currently have %s in your cart )', 'ignitewoo_wholesale_pro' ), ( $total_quantity - $quantity )  );
						
			wc_add_notice( $msg, 'error' );

			$pass = false;
		}

		return $pass;
	}
}

