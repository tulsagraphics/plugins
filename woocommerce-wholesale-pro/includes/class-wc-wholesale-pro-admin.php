<?php 
/*
 * Copyright © 2014 - 2017 - IgniteWoo.com - All Rights Reserved
 */

/*
	This class lets the administrator change the role of users and possibly send them an email notice of the change.
	
	It works by injecting a form at the top of the user editor page.
	
*/
class IgniteWoo_Wholesale_Pro_User_Admin extends WC_Integration {

	function __construct() { 
		
		// Check if our custom form was submitted
		add_action( 'init' , array( $this , 'maybe_update_user' ), 99 );
		
		// Add form to edit user page via admin notice hook
		add_action( 'admin_notices' , array( $this , 'add_to_edit_screen' ), 9999 );
		
		// Not currently implemented, maybe not necessary 
		
		//add_action( 'personal_options_update', array( &$this, 'maybe_update_user' ), -1 );
		//add_action( 'edit_user_profile_update', array( &$this, 'maybe_update_user' ), -1 );
		//add_action( 'admin_init', array( &$this, 'maybe_intercept_bulk_role_changes' ), -1 );
		//add_action( 'restrict_manage_users', array( &$this, 'approve_or_reject_users' ), 9999 );

		//add_action( 'manage_users_extra_tablenav', array( $this, 'maybe_render_form' ) );
		//add_action( 'admin_footer', array( &$this, 'bulk_action_js' ), -1 );
		//add_action( 'load-users.php' , array( $this , 'bulk_actions' ) );
		//add_action( 'admin_notices' , array( $this , 'bulk_action_notices' ) );
	}

	// Print WP_ERROR class messages, if any are returned when updating a user's role
	public function maybe_print_error( $res ) {
	
		if ( !is_wp_error( $res ) ) 
			return;
			
		$codes = $res->get_error_codes();
		
		?>
		<div class="error">
			<?php foreach( $codes as $code ) { ?>
				<p><?php echo $res->get_error_message( $code ) ?></p>
			<?php } ?>
		</div>
		<?php
	}

	public function print_success() {
		?>
		<div class="updated fade">
			<p>
				<?php 
				if ( !empty( $_POST['ign_wsp_notify_users'] ) )
					_e( 'Account updated, email notification sent to the user', 'ignitewoo_wholesale_pro' );
				else 
					_e( 'User account updated', 'ignitewoo_wholesale_pro' );
				?>
			</p>
		</div>
		<?php
	}
	
	public function maybe_update_user() {
		global $wp_roles; 
		
		if ( empty( $_POST['ign_wsp_manage_user'] ) || empty( $_POST['ign_wsp_manage_user_id'] ) || absint( $_POST['ign_wsp_manage_user_id'] ) <= 0 )
			return; 

		if ( !wp_verify_nonce( $_POST['_wpnonce'], 'ign_wsp_manage_user' ) ) {
			?>
			<div class="error">
				<p><?php _e( 'Security error changing user role. Try again', 'ignitewoo_wholesale_pro' ) ?></p>
			</div>
			<?php 
			return;
		}
		
		$this->new_role = $_REQUEST['new_role'];
		
		if ( empty( $this->new_role ) ) {
			?>
			<div class="error">
				<p><?php _e( 'Error: Select a user role first!', 'ignitewoo_wholesale_pro' ) ?></p>
			</div>
			<?php 
			return;
		}
		
		if ( !$wp_roles->is_role( $this->new_role ) ) {
			?>
			<div class="error">
				<p><?php _e( 'Error: The selected role no longer exists. Try again', 'ignitewoo_wholesale_pro' ) ?></p>
			</div>
			<?php 
			return;
		}
		
		$user_id = absint( $_POST['ign_wsp_manage_user_id'] );

		// Update user, maybe display admin notice of error or success, maybe email user if option selected
		$this->update_user( $user_id );
	}
	
	public function add_to_edit_screen() {
		global $wp_roles, $user_ID;
		
		$screen = get_current_screen();

		if ( $screen->id !== 'user-edit' )
			return; 

		?>
		<div class="notice" data-screen-view="edit-screen" style="padding-bottom:10px;border:1px solid #aaa;border-left: 4px solid #0073AA">
			<p style="font-weight:bold">
				<?php _e( 'If this is a wholesale user or applicant you can optionally change their role to another role (thereby removing all other roles they already have), and send them an email notice.', 'ignitewoo_wholesale_pro' ); ?>
				<?php echo sprintf( __( 'See the <a href="%s" target="_blank">documentation</a> for help.', 'ignitewoo_wholesale_pro' ), 'http://ignitewoo.com/documentation/wholesale-pro-suite/' ); ?>
			</p>
                	<form method="post" action="">
				<?php wp_nonce_field( 'ign_wsp_manage_user' ) ?>
				<input type="hidden" name="ign_wsp_manage_user" value="1">
				<input type="hidden" name="ign_wsp_manage_user_id" value="<?php echo $_GET['user_id'] ?>">
				<select name="new_role">
					<option value=""><?php _e( 'Change role to...', 'ignitewoo_wholesale_pro' ) ?></option>
					<?php foreach( $wp_roles->roles as $role => $data ) { ?>
						<option value="<?php echo $role ?>"><?php echo $data['name'] ?></option>
					<?php } ?>
				</select> 
				&nbsp;<?php _e( 'and', 'ignitewoo_wholesale_pro' ) ?>&nbsp;
				<select name="new_role_notice">
					<option value=""><?php _e( 'Do not send a notice', 'ignitewoo_wholesale_pro' ) ?></option>
					<option value="approved"><?php _e( 'Send approval notice', 'ignitewoo_wholesale_pro' ) ?></option>
					<option value="rejected"><?php _e( 'Send denied notice', 'ignitewoo_wholesale_pro' ) ?></option>
				</select>
				<?php /*
				<input type="checkbox" name="ign_wsp_notify_users" value="yes"> <?php _e( 'Notify user of role change', 'ignitewoo_wholesale_pro' ); ?>
				*/ ?>
				<input type="submit" class="button" name="ign_wsp_update_user" value="<?php _e( 'Update User', 'ignitewoo_wholesale_pro' ); ?>" style="margin-left:10px">
                	</form>
                	<script>
                	jQuery( document ).ready( function($) { 
				$( 'select' ).on( 'change', function() { $( this ).blur() } );
                	});
                	</script>
		</div>
		<?php 
		
	}
	function approve_or_reject_users() { 
		?>
		<div style="display:inline-block;padding-top:6px;margin-left:10px">
			<input type="checkbox" name="ign_wsp_notify_users" value="yes"> <?php _e( 'Notify users of role change', 'ignitewoo_wholesale_pro' ); ?>
		</div>
		<?php 
	}
	
	function update_user( $user_id ) {
		//global $ign_wholesale_pro_suite;

		/*
		$send_accepted = $ign_wholesale_pro_suite->settings['ignitewoo_wsp_automatic_notify_accepted'];
		$send_rejected = $ign_wholesale_pro_suite->settings['ignitewoo_wsp_automatic_notify_rejected']; 
		
		if ( 
			empty( $ign_wholesale_pro_suite->settings['ignitewoo_wsp_automatic_notify_accepted'] ) || 
			empty( $ign_wholesale_pro_suite->settings['ignitewoo_wsp_automatic_notify_rejected'] ) || 
			'yes' !== $ign_wholesale_pro_suite->settings['ignitewoo_wsp_automatic_notify_accepted'] ||
			'yes' !== $ign_wholesale_pro_suite->settings['ignitewoo_wsp_automatic_notify_rejected'] 
		) return;
		*/
		
		if ( empty( $this->new_role ) ) 
			return;
		
		$user = get_userdata( $user_id );

		//$user = get_user_by( 'id', $user_id ); 

		if ( empty( $user ) ) 
			return;

		// Remove all roles
		foreach( $user->roles as $key => $role ) { 
			$user->remove_role( $role );
		}

		// New selected role already set for this user? 
		// if ( in_array( $this->new_role, $user->roles )  )
		//	return;

		//foreach( $user->roles as $role ) 
		//	$user->remove_role( $role );
			
		$user->set_role( $this->new_role );

		$res = wp_update_user( $user );
		
		if ( is_wp_error( $res ) ) {
			$this->maybe_print_error( $res ) ;
		} else {
			$this->print_success();
			if ( !empty( $_POST['new_role_notice'] ) ) { 
				$this->send_email_notice_to_user( $_POST['new_role_notice'], $user_id );
			}
		}
		
		return $res; 
	}
	
	public function send_email_notice_to_user( $type, $user_id ) { 
		
		// Trigger the email. This fires hooks inside the associated email classes to
		// get the mail sent
		if ( 'approved' == $type ) { 
			WC()->mailer();
			require_once( dirname(__FILE__) . '/class-wc-wholesale-email-approval.php' );
			do_action( 'ign_wsp_do_approval_email', $user_id );
		} else if ( 'rejected' == $type ) { 
			WC()->mailer();
			require_once( dirname(__FILE__) . '/class-wc-wholesale-email-rejected.php' );
			do_action( 'ign_wsp_do_rejected_email', $user_id );
		}
		
	}
	
	/*
	function maybe_render_form( $which ) { 
		//if ( in_array( $post_type, array( 'ign_voucher', 'ign_voucher_email' ) ) && 'bottom' === $which ) {

		if ( 'top' == $which )
			echo 'ASDFASDFASDF';
	}
	*/
	
	/*
	function maybe_intercept_bulk_role_changes() {
		global $wp_roles; 

return;
		// NEED A SECURITY CHECK HERE FIRST
		
		
		// Check if the bulk role change for has been submitted
		if ( empty( $_REQUEST['new_role'] ) || empty( $_REQUEST['changeit'] ) )
			return;
			
		if ( empty( $_REQUEST['users'] ) )
			return;
			
		$this->new_role = $_REQUEST['new_role'];

		if ( !$wp_roles->is_role( $this->new_role ) )
			return;

		$current_user_id = get_current_user_id();
		
		foreach( $users as $user_id ) { 
			// don't change the logged in user's role! 
			if ( $user_id == $current_user_id )
				continue; 

			$res = $this->update_user( $user_id );
		}
		
		// check each user, maybe change the role and maybe send email 
//var_dump( $_REQUEST ); die;
	
	}
	*/
	
	/*
	public function approve_user( $user_id ) {
		global $ign_wholesale_pro_suite, $wp_roles; 
return true;
		if ( empty( $ign_wholesale_pro_suite->settings['ignitewoo_wsp_automatic_role'] ) || 'yes' !== empty( $ign_wholesale_pro_suite->settings['ignitewoo_wsp_automatic_role'] ) )
			return false;
			
var_dump( $wp_roles ); die;
		// change user role 
		$user = get_userdata( $user_id );
		$user->add_role( $new_user_role );
		
		// maybe send an email notice to the user 
		
		
		return true; 
	}
	
	public function reject_user() { 
		return true; 
	}
	*/
	
	/*
	public function bulk_action_js() {
		global $pagenow, $ign_wholesale_pro_suite;

		if ( $pagenow !== 'users.php' || ( !current_user_can( 'manage_options' ) && !current_user_can( 'manage_woocommerce' ) ) )
			return;
			
		// don't add bulk actions if automatic approval role
		if ( empty( $ign_wholesale_pro_suite->settings['ignitewoo_wsp_automatic_role'] ) || 'yes' == empty( $ign_wholesale_pro_suite->settings['ignitewoo_wsp_automatic_role'] ) )
			return; 
		?>

		<script type="text/javascript">
		jQuery( document ).ready( function($) {

			$( '<option>' ).val( 'ign_wsp_approve' ).text( '<?php _e( 'Approve' , 'ignitewoo_wholesale_pro' ); ?>' ).appendTo( "select[name='action']" );
			
			$( '<option>' ).val( 'ign_wsp_reject' ).text( '<?php _e( 'Reject' , 'ignitewoo_wholesale_pro' ); ?>' ).appendTo( "select[name='action']" );
			
			$( 'input#doaction' ).on( 'click', function(e) { 
				e.preventDefault();
				alert( 'test' );
				return false; 
			})
		});
		</script>
		<?php 
	}

	public function bulk_actions() {
		global $pagenow;

		if ( $pagenow !== 'users.php' || ( !current_user_can( 'manage_options' ) && !current_user_can( 'manage_woocommerce' ) ) )
			return;
			
		$wp_list_table = _get_list_table( 'WP_Users_List_Table' );  

		$action = $wp_list_table->current_action();

		if ( !in_array( $action , array( 'ign_wsp_approve', 'ign_wsp_reject' ) ) ) 
			return;

		check_admin_referer( 'bulk-users' );
		
		if ( isset( $_REQUEST[ 'users' ] ) )
			$user_ids = $_REQUEST[ 'users' ];
		else 
			return;
		
		$sendback = remove_query_arg( array( 'ign_wsp_approve', 'ign_wsp_reject', 'untrashed' , 'deleted' , 'ids' ), wp_get_referer() );
		
		if ( !$sendback )
			$sendback = admin_url( 'users.php' );

		$pagenum = $wp_list_table->get_pagenum();
		
		$sendback = add_query_arg( 'paged', $pagenum, $sendback );

		$current_user_id = get_current_user_id();
		
		switch( $action ) {

			case 'ign_wsp_approve':

				$users_activated = 0;
				
				foreach( $user_ids as $user_id ) {
					if ( $current_user_id == $user_id )
						continue; 
					
					 if ( $this->approve_user( array( 'user_id' => $user_id ) ) )
						$users_activated++;
				}

				$sendback = add_query_arg( array( 'users_approved' => $users_activated , 'ids' => join( ',' , $user_ids ) ), $sendback );
				
				break;

			case 'ign_wsp_reject':

				$users_rejected = 0;
				foreach( $user_ids as $user_id ) {

					if ( $current_user_id == $user_id )
						continue; 
					
					 if ( $this->reject_user( array( 'user_id' => $user_id ) ) )
						$users_rejected++;
				}

				$sendback = add_query_arg( array( 'users_rejected' => $users_rejected , 'ids' => join( ',' , $user_ids ) ), $sendback );
				
				break;

			default: 
				break;
		}

		$sendback = remove_query_arg( array( 'action' , 'action2' , 'tags_input' , 'post_author' , 'comment_status', 'ping_status' , '_status',  'post', 'bulk_edit', 'post_view', 'ids' ), $sendback );

		wp_redirect( $sendback );
		
		exit();
	}
	
	public function bulk_action_notices() {
		global $pagenow;

		if ( $pagenow == 'users.php' ) {

			if ( ( isset( $_REQUEST[ 'users_approved' ] ) && (int) $_REQUEST[ 'users_approved' ] ) ||
				( isset( $_REQUEST[ 'users_rejected' ] ) && (int) $_REQUEST[ 'users_rejected' ] ) ||
				( isset( $_REQUEST[ 'users_activated' ] ) && (int) $_REQUEST[ 'users_activated' ] ) ||
				( isset( $_REQUEST[ 'users_deactivated' ] ) && (int) $_REQUEST[ 'users_deactivated' ] ) ) {

				if ( ! empty( $_REQUEST[ 'users_approved' ] ) ) {
					$action = __( 'approved' , 'ignitewoo_wholesale_pro' );
					$affected = $_REQUEST[ 'users_approved' ];
				} 
				
				if ( ! empty( $_REQUEST[ 'users_rejected' ] ) ) {
					$action = __( 'rejected' , 'ignitewoo_wholesale_pro' );
					$affected = $_REQUEST[ 'users_rejected' ];
				}
				
				$message = sprintf( _n( 'User %2$s.' , '%1$s users %2$s.' , $affected , 'ignitewoo_wholesale_pro' ) , number_format_i18n( $affected ) , $action );
				
				echo "<div class=\"updated\"><p>{$message}</p></div>";

			} elseif ( isset( $_REQUEST[ 'action' ] ) && in_array( $_REQUEST[ 'action' ], array( 'ign_wsp_approve', 'ign_wsp_reject' ) ) ) {

				if ( isset( $_REQUEST[ 'users' ] ) && count( $_REQUEST[ 'users' ] ) > 0 ) {

					if ( $_REQUEST[ 'action' ] == "wwlc_approve" )
						$action = __( 'approved' , 'ignitewoo_wholesale_pro' );
					if ( $_REQUEST[ 'action' ] == "wwlc_reject" )
						$action = __( 'rejected' , 'ignitewoo_wholesale_pro' );
				
					$message = sprintf( _n( 'User %2$s.' , '%1$s users %2$s.' , count( $_REQUEST[ 'users' ] ) , 'ignitewoo_wholesale_pro' ) , number_format_i18n( count( $_REQUEST[ 'users' ] ) ) , $action );
					
					echo "<div class=\"updated\"><p>{$message}</p></div>";
				
				}
			}
		}
	}
	*/
}
new IgniteWoo_Wholesale_Pro_User_Admin();
