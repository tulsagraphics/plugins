<?php
/*
 * Copyright (c) 2013 - IgniteWoo.com - All Rights Reserved
*/


class IgniteWoo_Wholesale_Pro_Categories {

	var $error = '';

	function __construct() {
	
		add_action( 'init', array( &$this, 'init' ), 1 );
	}

	function init() {

		add_action ( 'product_cat_edit_form_fields', array( &$this, 'extra_category_fields'), 2 );
		add_action( 'product_cat_add_form_fields', array( &$this, 'extra_category_fields'), 10, 2 );
		add_action( 'edited_product_cat', array( &$this, 'save_taxonomy_custom_meta' ), 10, 2 );  
		add_action( 'create_product_cat', array( &$this, 'save_taxonomy_custom_meta' ), 10, 2 );
	}
		

	//add extra fields to category edit form callback function
	function extra_category_fields( $tag = null, $other = null ) {    //check for existing featured ID
		global $wp_roles; 
		
		foreach( $wp_roles->roles as $role => $data ) { 

			if ( 'ignite_level_' != substr( $role, 0, 13 ) )
				continue;
			
			if ( is_object( $tag ) ) { 
				$t_id = $tag->term_id;
				$cat_meta = get_option( "product_cat_wsp_discount_$t_id" );
			} else { 
				$t_id = '';
				$cat_meta = '';
			}

			?>
			<tr class="form-field">
				<th scope="row" valign="top"><label for="cat_Image_url"><?php _e('Discount for ', 'ignitewoo_wholesale_pro' ); echo ' ' . $data['name'] ?></label></th>
				
				<td>
					<input type="text" name="product_cat_wsp[<?php echo $role ?>]" id="" style="width:60%;" value="<?php echo !empty( $cat_meta[ $role ] ) ? $cat_meta[ $role ] : ''; ?>"><br />
					<span class="description"><?php _e( 'Enter a percentage discount with a percent symbol', 'ignitewoo_wholesale_pro' ); ?></span>
				</td>
			</tr>
			
		<?php }
	}

	function save_taxonomy_custom_meta( $term_id ) {

		if ( empty( $_POST['product_cat_wsp'] ) )
			return;
		
		$t_id = $term_id;
		
		$cat_keys = array_keys( $_POST['product_cat_wsp'] );
		
		$term_meta = array();

		foreach ( $cat_keys as $key ) {

			if ( !empty( $_POST['product_cat_wsp'][ $key ] ) ) { 
				$term_meta[ $key ] = $_POST['product_cat_wsp'][$key];
			}
			
			if ( !empty( $term_meta ) ) { 
				update_option( "product_cat_wsp_discount_$t_id", $term_meta );
			} else { 
				delete_option( "product_cat_wsp_discount_$t_id" );
			}
		}
		
	}  
}

new IgniteWoo_Wholesale_Pro_Categories();