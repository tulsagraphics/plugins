<?php 
/**
 * Copyright (c) 2015-2017 - IgniteWoo.com - All Rights Reserved 
 *
 * Loads email classes & sets up actions for emails
 */

if ( ! defined( 'ABSPATH' ) ) exit;

class IGN_WSP_Emails {
	
	public static function init() {
		add_filter( 'woocommerce_email_classes', array( __CLASS__, 'add_emails' ) );
		// This helps make the email messages transactional/queued instead of going out instantly
		add_filter( 'woocommerce_email_actions', array( __CLASS__, 'add_email_actions' ) );
	}
	
	public static function add_emails( $email_classes ) {

		require( dirname( __FILE__ ) . '/class-wc-wholesale-email-approval.php' );
		require( dirname( __FILE__ ) . '/class-wc-wholesale-email-rejected.php' );
		
		$email_classes['IGN_WSP_Approval_Email'] = new IGN_WSP_Approval_Email();
		$email_classes['IGN_WSP_Rejected_Email'] = new IGN_WSP_Rejected_Email();
		
		return $email_classes;
	}
	
	public static function add_email_actions( $email_actions ) {
	
		$email_actions[] = 'ign_wsp_do_approval_email';
		$email_actions[] = 'ign_wsp_do_rejected_email';
		return $email_actions;
	}
}

IGN_WSP_Emails::init();
