<?php

// Compatibility helper for WC 2.7 and newer

// META  ===============================================
/**
 * Catch-all function to GET a single post meta value, or object meta for products, orders, coupons, customers
 * $type is only passed if the object must be loaded first because $object is only an id number
 * Example: ign_get_object_meta( 23, '_customer_stuff', true, 'WC_Order' );
 */

if ( !function_exists( 'ign_get_object_meta' ) ) { 
	function ign_get_object_meta( $object, $key, $single = true, $type = false ) { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) { 
			if ( !empty( $type ) ) { 
				$object = new $type( $object );
			}
			return $object->get_meta( $key, $single );
		} else { 
			if ( is_object( $object ) ) { 
				$id = isset( $object->id ) ? $object->id : false;
				if ( empty( $id ) )
					$id = isset( $object->ID ) ? $object->id : false;
				if ( empty( $id ) )
					return;
				return get_post_meta( $id, $key, $single );
			} else if ( is_numeric( $object ) ) { 
				return get_post_meta( $object, $key, $single );
			}
		}
	}
}
/**
 * Catch-all function to UPDATE post meta, or object meta for products, orders, coupons, customers 
 * $type is only passed if the object must be loaded first because $object is only an id number
 */
if ( !function_exists( 'ign_update_object_meta' ) ) { 
	function ign_update_object_meta( $object, $key, $value, $type = false ) { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) {
			// In this case object would be an ID number, and type would be the class such as WC_Order
			if ( !empty( $type ) ) { 
				$object = new $type( $object );
			}
			$object->update_meta_data( $key, $value );
			$object->save_meta_data();
			
		} else { 
			if ( is_object( $object ) ) { 
				$id = isset( $object->id ) ? $object->id : false;
				if ( empty( $id ) )
					$id = isset( $object->ID ) ? $object->id : false;
				if ( empty( $id ) )
					return;
				update_post_meta( $id, $key, $value );
			} else if ( is_numeric( $object ) ) { 
				update_post_meta( $object, $key, $value );
			}
		}
	}
}
/**
 * Catch-all function to GET a single post meta value, or object meta for products, orders, coupons, customers
 * $type is only passed if the object must be loaded first because $object is only an id number
 * Example: ign_get_object_meta( 23, '_customer_stuff', 'WC_Order' );
 */

if ( !function_exists( 'ign_delete_object_meta' ) ) { 
	function ign_delete_object_meta( $object, $key, $type = false ) { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) { 
			if ( !empty( $type ) ) { 
				$object = new $type( $object );
			}
			$object->delete_meta_data( $key );
			$object->save_meta_data();
		} else { 
			if ( is_object( $object ) ) { 
				$id = isset( $object->id ) ? $object->id : false;
				if ( empty( $id ) )
					$id = isset( $object->ID ) ? $object->id : false;
				if ( empty( $id ) )
					return;
				return delete_post_meta( $id, $key );
			} else if ( is_numeric( $object ) ) { 
				return delete_post_meta( $object, $key );
			}
		}
	}
}


// PRODUCTS ===============================================
if ( !function_exists( 'ign_get_product_id' ) ) { 
	function ign_get_product_id( $product, $variation = false ) { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) {
			// Not getting variation ID, but the main product ID
			if ( !$variation && 'variation' == $product->get_type() )
				return $product->get_parent_id();
			else if ( method_exists( $product, 'get_id' ) )
				return $product->get_id();
		} else if ( $variation && !empty( $product->variation_id ) )
			return $product->variation_id; 
		else if ( !empty( $product->id ) )
			return $product->id;
		else 
			return null;
	}
}

// PRODUCT PRICE ===============================================
if ( !function_exists( 'ign_get_product_price' ) ) { 
	function ign_get_product_price( $product, $context = 'ign_read' ) {
	
		if ( !is_object( $product ) )
			return;
			
		if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '<=' ) && !isset( $product->price ) )
			return;
			
		if ( !is_object( $product ) || !method_exists( $product, 'get_price' ) )
			return; 
			
		// $context determines if filters are run, possible other stuff. 
		// A context of "view" causes filters to trigger, this causes loops when 
		// used within an existing price filter, so we override with an arbitrary value
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) {
			if ( !method_exists( $product, 'get_id' ) && !method_exists( $product, 'get_parent_id' ) )
				return null;
			return $product->get_price( $context );
		} else 
			return $product->price;

	}
}

// ORDERS ===============================================
if ( !function_exists( 'ign_get_order_id' ) ) { 
	function ign_get_order_id( $order ) { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
			return $order->get_id();
		else if ( !empty( $order->id ) )
			return $order->id;
		else 
			return null;
	}
}
if ( !function_exists( 'ign_get_order_amount' ) ) { 
	function ign_get_order_amount( $order ) { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
			return $order->get_amount();
		return $order->amount;
	}
}
if ( !function_exists( 'ign_get_order_date' ) ) { 
	function ign_get_order_date( $order, $formatted = false ) { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) {
			$date = $order->get_date_created();
			if ( $formatted ) 
				$date = date( 'Y-m-d H:i:s', $date );
			return $date;		
		}
		return $order->order_date;
	}
}
if ( !function_exists( 'ign_get_order_address_info' ) ) { 
	function ign_get_order_address_info( $order, $element ) { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
			$method = 'get_' . $element; 
			return $order->$method(); // example "billing_first_name"
		return $order->amount;
	}
}


// COUPONS ===============================================
if ( !function_exists( 'ign_get_coupon_id' ) ) { 
	function ign_get_coupon_id( $coupon ) { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
			return $coupon->get_id();
		else if ( !empty( $coupon->id ) )
			return $coupon->id;
		else 
			return null;
	}
}
if ( !function_exists( 'ign_get_coupon_type' ) ) { 
	function ign_get_coupon_type( $coupon ) { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
			return $coupon->get_discount_type();
		else if ( !empty( $coupon->type ) )
			return $coupon->type;
		else 
			return null;
	}
}

if ( !function_exists( 'ign_get_coupon_amount' ) ) { 
	function ign_get_coupon_amount( $coupon ) { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
			return $coupon->get_amount();
		else if ( !empty( $coupon->amount ) )
			return $coupon->amount;
		else 
			return null;
	}
}

if ( !function_exists( 'ign_get_coupon_expires' ) ) { 
	function ign_get_coupon_expires( $coupon ) { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
			return $coupon->get_date_expires();
		else if ( !empty( $coupon->expiry ) )
			return $coupon->expiry;
		else 
			return null;
	}
}

if ( !function_exists( 'ign_get_coupon_code' ) ) { 
	function ign_get_coupon_code( $coupon ) { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
			return $coupon->get_code();
		else if ( !empty( $coupon->code ) )
			return $coupon->code;
		else 
			return null;
	}
}


if ( !function_exists( 'ign_get_individual_use' ) ) { 
	function ign_get_individual_use( $coupon ) { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
			return $coupon->get_individual_use();
		else if ( !empty( $coupon->individual_use ) )
			return $coupon->individual_use;
		else 
			return null;
	}
}
