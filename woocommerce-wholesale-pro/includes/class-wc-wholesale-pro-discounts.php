<?php
/*
 * Copyright © 2014 IgniteWoo.com - All Rights Reserved
*/

class IgniteWoo_Wholesale_Pro_Discounts { 

	public $plugin_url;
	
	public $plugin_path;
	
	public $compatible = false;
	
	public $discounts = null;

	function __construct() { 
		global $ign_wholesale_pro_suite;

		//if ( !$this->pre_init() )
		//	return;

		$this->plugin_url = $ign_wholesale_pro_suite->plugin_url;

		$this->plugin_path = $ign_wholesale_pro_suite->plugin_path;

		add_action( 'init', array( &$this, 'ignitewoo_dd_loader' ), 20 );

		add_action( 'wp_ajax_get_simulated_price' , array( &$this, 'get_simulated_price' ) );
		add_action( 'wp_ajax_nopriv_get_simulated_price' , array( &$this, 'get_simulated_price' ) );

		add_action( 'wp_head', array( &$this, 'maybe_add_hooks' ), 9999 );
		
		add_action( 'wp_head', array( &$this, 'wp_head' ), -1 );
		
		add_action( 'wp_footer', array( &$this, 'wp_footer' ), 999 );
	}

	function ignitewoo_dd_loader() { 
	
		//if ( !class_exists( 'Woocommerce' ) ) 
		//	return;

		//if ( !$this->compatible )
		//	return;

		if ( is_admin() ) { 

			require_once( dirname( __FILE__ ) . '/class-wc-wholesale-pro-product-edit.php' );

			global $ignitewoo_dd_product_edit;

			$ignitewoo_dd_product_edit = new IgniteWoo_Wholesale_Pro_Product_Admin();
		}

		//require_once( dirname( __FILE__ ) . '/class-wc-wholesale-pro-product-process.php' );
		require_once( dirname( __FILE__ ) . '/class-wc-wholesale-pro-product-process-new.php' );
	}
	
	function pre_init() { 
		global $ignitewoo_dd_pricing_base_version;

		$res = true;

		if ( class_exists( 'woocommerce_dynamic_pricing' ) ) { 

			add_action( 'admin_notices', array( &$this, 'warning' ), -1 );

			$res = false;
		}

		$this->compatible = $res; 

		return $res; 
	}


	function warning() { 

		echo '<div class="error" style="font-weight: bold; font-style: italic; color: #cf0000; font-size: 1.3em" ><p>';
		_e( 'Warning: IgniteWoo Quantity Discounts plugin is not compatible with the WooCommerce Dynamic Pricing plugin. Deactivate one or the other.', 'ignitewoo_wholesale_pro');
		echo '</div>';

	}


	function wp_head() { 
		global $woocommerce;
		
		if ( !is_product() ) 
			return;
			
		wp_register_script( 'jquery-blockui', $woocommerce->plugin_url() . '/assets/js/jquery-blockui/jquery.blockUI.min.js', array( 'jquery' ), '2.60', true );
		
		wp_enqueue_script( 'jquery-blockui' );
		
		$css = file_exists( get_stylesheet_directory() . '/woocommerce/product_discount_style.css' ) ? get_stylesheet_directory_uri() . '/woocommerce/product_discount_style.css' : $this->plugin_url . '/templates/product_discount_style.css';

		wp_enqueue_style( 'ignitewoo_wholesale_pro', $css );
	
	}
	
	
	function wp_footer() { 
	
		$js = file_exists( get_stylesheet_directory() . '/woocommerce/product_discounts.js' ) ? get_stylesheet_directory_uri() . '/woocommerce/product_discounts.js' : $this->plugin_url . '/templates/product_discounts.js';

		?>
		<script src="<?php echo $js ?>"></script>

		<?php 
	}
	

	function maybe_add_hooks() { 

		if ( !is_product() ) 
			return;
				
		$show_table = get_option( 'woocommerce_dd_show_discount_table', false );
		
		$table_place = get_option( 'woocommerce_dd_discount_table_placement', false );
		
		$show_calc = get_option( 'woocommerce_dd_show_discount_calculator', false );
		
		if ( 'yes' == $show_calc ) 
			add_action( 'woocommerce_after_add_to_cart_button', array( &$this, 'after_add_to_cart_button' ), -99 );

		if ( 'yes' == $show_table && $table_place ) {
		
			if ( 'the_content_top' == $table_place ) 
				add_filter( 'the_content', array( &$this, 'table_top_content' ), -1, 1 );
			else 
				add_action( $table_place, array( &$this, 'get_dynamic_discounts' ), 5 );
			
		}
	
	}
	
	function after_add_to_cart_button() { 
		global $woocommerce, $ign_wholesale_pro_suite, $ignitewoo_wsp_discounts_pricing;
			
		if ( !is_product() ) 
			return;

		if ( empty( $this->discounts ) )
			$this->discounts = $ignitewoo_wsp_discounts_pricing->get_cart_item_discounts();

		if ( empty( $this->discounts ) )
			return;

		$template_name = 'check_discounts.php'; 

		if ( ! defined( 'WC_TEMPLATE_PATH' ) )
			$template_path = $woocommerce->template_url;
		else
			$template_path = WC_TEMPLATE_PATH;
		
		$default_path = $ign_wholesale_pro_suite->plugin_path . '/templates/';

		$template = locate_template(
			array(
				trailingslashit( $template_path ) . $template_name,
				$template_name
			)
		);

		if ( !$template )
			$template = $default_path . $template_name;

		include( $template );

	}

        
	function get_dynamic_discounts() { 
		global $woocommerce, $ignitewoo_wsp_discounts_pricing, $discounts_to_display;

		if ( !is_product() ) 
			return;

		//if ( empty( $this->discounts ) )
			$discounts_to_display = $ignitewoo_wsp_discounts_pricing->get_cart_item_discounts( true );
		//else
		//	$discounts_to_display = $this->discounts;

		if ( empty( $discounts_to_display ) ) 
			return;

		if ( !defined( 'SORT_NATURAL' ) )
			define( 'SORT_NATURAL', 6 );
			
		ksort( $discounts_to_display, SORT_NATURAL );
			
		$template_name = 'product_discounts.php'; 
		
		if ( ! defined( 'WC_TEMPLATE_PATH' ) )
			$template_path = $woocommerce->template_url;
		else
			$template_path = WC_TEMPLATE_PATH;
		
		
		$default_path = $this->plugin_path . '/templates/';

		$template = locate_template(
			array(
				trailingslashit( $template_path ) . $template_name,
				$template_name
			)
		);

		if ( !$template )
			$template = $default_path . $template_name;

		include( $template );
		
	}
	
	
	function table_top_content( $content = '' ) { 

		$content = $this->get_dynamic_discounts() . $content;
		
		return $content; 
	
	}
	function get_simulated_price() { 
		global $woocommerce, $ignitewoo_wsp_discounts_pricing;

		$data = $_POST['item_data'];
		
		if ( empty( $data ) )
			die ( __( 'Error getting price', 'ignitewoo_wholesale_pro' ) );

		$data = explode( '&', $data );

		if ( !is_array( $data ) )
			die ( __( 'Error getting price', 'ignitewoo_wholesale_pro' ) );
			
		foreach( $data as $val ) {
		
			$vals = explode( '=', $val );
			
			$values[ $vals[0] ] = $vals[1]; 
		
		}
		
		if ( empty( $values['product_id'] ) && empty( $values['variation_id'] ) && isset( $values['add-to-cart'] ) )
			$values['product_id'] = $values['add-to-cart'];
		
		if ( empty( $values['product_id'] ) && empty( $values['variation_id'] ) )
			die ( __( 'Error getting price', 'ignitewoo_wholesale_pro' ) );

		if ( empty( $values['quantity'] ) || absint( $values['quantity'] ) <= 0 )
			die ( '<span style="color:#cf0000">' . __( 'Enter a quantity first', 'ignitewoo_wholesale_pro' ) . '</span>' );
		
		$type = wp_get_post_terms( $values['product_id'], 'product_type' );
		
		if ( empty( $type ) || is_wp_error( $type ) )
			die ( __( 'Error detecting product type', 'ignitewoo_wholesale_pro' ) );
		
		if ( 'variable' == $type[0]->slug && empty( $values['variation_id'] ) )
			die ( '<span style="color:#cf0000">' . __( 'SELECT A VARIATION', 'ignitewoo_wholesale_pro' ) . '</span>' );
		
		$attrs = array();
		
		foreach( $values as $key => $val ) { 
		
			if ( 'attribute_' == substr( $key, 0, 10 ) ) { 
				$attrs[ substr( $key, 10 ) ] = $val; 
				
			}
		
		}
		
		$product_data = wc_get_product( $values['variation_id'] ? $values['variation_id'] : $values['product_id'] );
		
		if ( !$product_data )
			die( __( 'Error locating product', 'ignitewoo_wholesale_pro' ) );


		$temp_cart = WC()->cart;
		
		WC()->cart->get_cart_from_session(); 

		WC()->cart->add_to_cart( $values['product_id'], $values['quantity'], $values['variation_id'], $attrs );
		
		WC()->cart->set_session();

		WC()->cart->get_cart_from_session(); 

		WC()->cart->calculate_totals();

		$price = $product_data->get_price();
		
		foreach( WC()->cart->get_cart( 'line_items' ) as $cart_item_key => $cart_item ) { 

			if ( empty( $values['variation_id'] ) && $values['product_id'] == $cart_item['product_id'] ) {

				$temp_cart_item = $cart_item; 

				$the_price = $ignitewoo_wsp_discounts_pricing->maybe_adjust_cart_item( $cart_item['data']->get_price(), $temp_cart_item );
				
				if ( false !== $the_price )
					$price = $the_price;

				$cart_item = $temp_cart_item;
				
				unset( $temp_cart_item );
				
				//$ignitewoo_wsp_discounts_pricing->maybe_adjust_pricing( $cart_item );
				
				//$price = get_option('woocommerce_tax_display_cart') == 'excl' ? $cart_item['data']->get_price_excluding_tax() : $cart_item['data']->get_price_including_tax(); 

				$qty = 1; 
				
				if ( 'excl' == get_option('woocommerce_tax_display_cart') ) { 
				
					if ( $cart_item['data']->is_taxable() && wc_prices_include_tax() ) {
						$tax_rates  = WC_Tax::get_base_tax_rates( $cart_item['data']->get_tax_class( 'unfiltered' ) );
						$taxes      = WC_Tax::calc_tax( $price * $qty, $tax_rates, true );
						$price      = WC_Tax::round( $price * $qty - array_sum( $taxes ) );
					} else {
						$price = $price * $qty;
					}
					
				} else { 
				
					$line_price   = $price * $qty;
					$return_price = $line_price;

					if ( $cart_item['data']->is_taxable() ) {
						if ( ! wc_prices_include_tax() ) {
							$tax_rates    = WC_Tax::get_rates( $cart_item['data']->get_tax_class() );
							$taxes        = WC_Tax::calc_tax( $line_price, $tax_rates, false );
							$tax_amount   = WC_Tax::get_tax_total( $taxes );
							$return_price = round( $line_price + $tax_amount, wc_get_price_decimals() );
						} else {
							$tax_rates      = WC_Tax::get_rates( $cart_item['data']->get_tax_class() );
							$base_tax_rates = WC_Tax::get_base_tax_rates( $cart_item['data']->get_tax_class( 'unfiltered' ) );

							/**
							* If the customer is excempt from VAT, remove the taxes here.
							* Either remove the base or the user taxes depending on woocommerce_adjust_non_base_location_prices setting.
							*/
							if ( ! empty( WC()->customer ) && WC()->customer->get_is_vat_exempt() ) {
								$remove_taxes = apply_filters( 'woocommerce_adjust_non_base_location_prices', true ) ? WC_Tax::calc_tax( $line_price, $base_tax_rates, true ) : WC_Tax::calc_tax( $line_price, $tax_rates, true );
								$remove_tax   = array_sum( $remove_taxes );
								$return_price = round( $line_price - $remove_tax, wc_get_price_decimals() );

							/**
							* The woocommerce_adjust_non_base_location_prices filter can stop base taxes being taken off when dealing with out of base locations.
							* e.g. If a product costs 10 including tax, all users will pay 10 regardless of location and taxes.
							* This feature is experimental @since 2.4.7 and may change in the future. Use at your risk.
							*/
							} elseif ( $tax_rates !== $base_tax_rates && apply_filters( 'woocommerce_adjust_non_base_location_prices', true ) ) {
								$base_taxes   = WC_Tax::calc_tax( $line_price, $base_tax_rates, true );
								$modded_taxes = WC_Tax::calc_tax( $line_price - array_sum( $base_taxes ), $tax_rates, false );
								$return_price = round( $line_price - array_sum( $base_taxes ) + array_sum( $modded_taxes ), wc_get_price_decimals() );
							}
						}
					}
					
					$price = $return_price;
				}
				
				$price = wc_price( $price );
			
				if ( version_compare( WOOCOMMERCE_VERSION, "2.1" ) >= 0 )
					$price = apply_filters( 'woocommerce_cart_item_price', $price, $cart_item, $cart_item_key );
				else
					$price = apply_filters( 'woocommerce_cart_item_price_html', $price, $cart_item, $cart_item_key );
			
				$key = $cart_item_key; 
				
				$qty = $cart_item['quantity'] - $values['quantity']; 
				
				break;
				
			} else if ( !empty( $values['variation_id'] ) && $values['product_id'] == $cart_item['product_id'] && $values['variation_id'] == $cart_item['variation_id']  ) { 

				$temp_cart_item = $cart_item; 

				$the_price = $ignitewoo_wsp_discounts_pricing->maybe_adjust_cart_item( $cart_item['data']->get_price(), $temp_cart_item );
				
				if ( false !== $the_price )
					$price = $the_price;

				$cart_item = $temp_cart_item;
				
				unset( $temp_cart_item );

				$qty = 1; 
				
				if ( 'excl' == get_option('woocommerce_tax_display_cart') ) { 
				
					if ( $cart_item['data']->is_taxable() && wc_prices_include_tax() ) {
						$tax_rates  = WC_Tax::get_base_tax_rates( $cart_item['data']->get_tax_class( 'unfiltered' ) );
						$taxes      = WC_Tax::calc_tax( $price * $qty, $tax_rates, true );
						$price      = WC_Tax::round( $price * $qty - array_sum( $taxes ) );
					} else {
						$price = $price * $qty;
					}
					
				} else { 
				
					$line_price   = $price * $qty;
					$return_price = $line_price;

					if ( $cart_item['data']->is_taxable() ) {
						if ( ! wc_prices_include_tax() ) {
							$tax_rates    = WC_Tax::get_rates( $cart_item['data']->get_tax_class() );
							$taxes        = WC_Tax::calc_tax( $line_price, $tax_rates, false );
							$tax_amount   = WC_Tax::get_tax_total( $taxes );
							$return_price = round( $line_price + $tax_amount, wc_get_price_decimals() );
						} else {
							$tax_rates      = WC_Tax::get_rates( $cart_item['data']->get_tax_class() );
							$base_tax_rates = WC_Tax::get_base_tax_rates( $cart_item['data']->get_tax_class( 'unfiltered' ) );

							/**
							* If the customer is excempt from VAT, remove the taxes here.
							* Either remove the base or the user taxes depending on woocommerce_adjust_non_base_location_prices setting.
							*/
							if ( ! empty( WC()->customer ) && WC()->customer->get_is_vat_exempt() ) {
								$remove_taxes = apply_filters( 'woocommerce_adjust_non_base_location_prices', true ) ? WC_Tax::calc_tax( $line_price, $base_tax_rates, true ) : WC_Tax::calc_tax( $line_price, $tax_rates, true );
								$remove_tax   = array_sum( $remove_taxes );
								$return_price = round( $line_price - $remove_tax, wc_get_price_decimals() );

							/**
							* The woocommerce_adjust_non_base_location_prices filter can stop base taxes being taken off when dealing with out of base locations.
							* e.g. If a product costs 10 including tax, all users will pay 10 regardless of location and taxes.
							* This feature is experimental @since 2.4.7 and may change in the future. Use at your risk.
							*/
							} elseif ( $tax_rates !== $base_tax_rates && apply_filters( 'woocommerce_adjust_non_base_location_prices', true ) ) {
								$base_taxes   = WC_Tax::calc_tax( $line_price, $base_tax_rates, true );
								$modded_taxes = WC_Tax::calc_tax( $line_price - array_sum( $base_taxes ), $tax_rates, false );
								$return_price = round( $line_price - array_sum( $base_taxes ) + array_sum( $modded_taxes ), wc_get_price_decimals() );
							}
						}
					}
					
					$price = $return_price;
				}

				$price = wc_price( $price );
				
				if ( version_compare( WOOCOMMERCE_VERSION, "2.1" ) >= 0 )
					$price = apply_filters( 'woocommerce_cart_item_price', $price, $cart_item, $cart_item_key );
				else
					$price = apply_filters( 'woocommerce_cart_item_price_html', $price, $cart_item, $cart_item_key );

				$key = $cart_item_key;
				
				$qty = $cart_item['quantity'] - $values['quantity']; 
				
				break; 
			}
		}

		WC()->cart = $temp_cart;
		
		WC()->cart->set_quantity( $key, $qty );
		
		WC()->cart->set_session();
		
		echo $price;
		
		die;
	}		
	
}
global $ignitewoo_wsp_discounts;
$ignitewoo_wsp_discounts = new IgniteWoo_Wholesale_Pro_Discounts();

// Template tag
function ign_wsp_insert_discount_table() { 
	global $ignitewoo_wsp_discounts;
 
	$ignitewoo_wsp_discounts->get_dynamic_discounts();
 
}

