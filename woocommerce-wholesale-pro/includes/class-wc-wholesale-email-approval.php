<?php 
/**
 * Copyright (c) 2015 - 2017 - IgniteWoo.com - All Rights Reserved 
 
 * This is the email class to send approval email notices to users
 */

if ( ! defined( 'ABSPATH' ) ) exit;

class IGN_WSP_Approval_Email extends WC_Email {
	
	public function __construct() {
		global $ignitewoo_vendors; 
		
		// set ID, this simply needs to be a unique name
		$this->id = 'ign_wsp_approval_email';
		
		// this is the title in WooCommerce Email settings
		$this->title = __( 'Wholesale Approval Email', 'ignitewoo_wholesale_pro' );
		
		// this is the description in WooCommerce email settings
		$this->description = __( "Notification sent to a user when the application to become a wholesale buyer is approved", 'ignitewoo_wholesale_pro' );
		
		// these are the default heading and subject lines that can be overridden using the settings
		$this->heading = __( "You're Approved!", 'ignitewoo_wholesale_pro' );
		$this->subject = __( "You're Approved!", 'ignitewoo_wholesale_pro' );
		
		$this->placeholders = array( 
			'{first_name}' => '',
			'{last_name}' => '', 
		);
		
		$this->manual = true; 
		$this->email_type = 'html';
		
		// Triggers
		add_action( 'ign_wsp_do_approval_email', array( $this, 'setup_and_send' ), 10, 10 );
		
		// Call parent constructor to load any other defaults not explicity defined here
		parent::__construct();
		
		$this->settings['email_type'] = 'html';

		// this sets the recipient to the settings defined below in init_form_fields()
		$this->recipient = '';
	}

	public function init_form_fields() {
	
		$this->form_fields = array(
			'enabled'    => array(
				'title'   => __( 'Enable', 'ignitewoo_wholesale_pro' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable this email notification', 'ignitewoo_wholesale_pro' ),
				'default' => 'no'
			),
			'subject'    => array(
				'title'       => __( 'Email Subject', 'ignitewoo_wholesale_pro' ),
				'type'        => 'text',
				'description' => sprintf( 'This controls the email subject line. Leave blank to use the default subject: <code>%s</code>.', $this->subject ),
				'placeholder' => __( "You're Approved!", 'ignitewoo_wholesale_pro' ),
				'default'     => ''
			),
			'heading'    => array(
				'title'       => __( 'Email Heading', 'ignitewoo_wholesale_pro' ),
				'type'        => 'text',
				'description' => sprintf( __( 'This controls the main heading contained within the email notification. Leave blank to use the default heading: <code>%s</code>.', 'ignitewoo_wholesale_pro' ), $this->heading ),
				'placeholder' => __( "You're Approved!", 'ignitewoo_wholesale_pro' ),
				'default'     => ''
			),
			'message'    => array(
				'title'       => __( 'Email Message', 'ignitewoo_wholesale_pro' ),
				'type'        => 'ign_editor',
				'description' => __( 'The message body. Use <code>{first_name}</code> or <code>{last_name}</code> to insert the first name or last name. Those only work if your registration form requires a first name or last name.', 'ignitewoo_wholesale_pro' ),
				'placeholder' => '',
				'default'     => __( 'Thank you for registering with us. Your account is now approved. ', 'ignitewoo_wholesale_pro' )
			),
			
			// This is here to force the type to HTML, it's a hidden setting field
			'email_type' => array(
				'title'       => '',
				'type'        => 'hidden',
				'description' => '',
				'default'     => 'html',
				'class'       => 'email_type',
			)
		);
	}

	public function get_subject() {
		if ( !empty( $this->settings['subject'] ) )
			return $this->settings['subject'];
		else 
			return $this->get_default_subject();
	}

	public function get_heading() { 
		if ( !empty( $this->settings['heading'] ) )
			return $this->settings['heading'];
		else 
			return $this->get_default_heading();
	}
	
	public function get_default_subject() { 
		return $this->subject;
	}
	
	public function get_default_heading() { 
		return $this->heading;
	}
	
	public function validate_ign_editor_field( $k, $v ) {
		return $_POST[ $this->id . '_message' ];
	}
	
	public function generate_ign_editor_html( $k, $v ) {

		$content = stripslashes( isset( $this->settings['message'] ) ? $this->settings['message'] : $v['default'] );
		
		$settings = array(
			'wpautop' => true,
			'media_buttons' => false,
			'textarea_name' => $this->id . '_message',
			'teeny' => true,
			'quicktags' => false,
			'tinymce' => array( 
					'toolbar1'=> 'formatselect,bold,italic,link,undo,redo',
			)
		);
		
		ob_start();
		?>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<label for="woocommerce_ign_wc_vendor_welcome_email_subject">
					<?php echo $v['title'] ?>
				</label>
			</th>
			<td class="forminp">
				<fieldset>
					<legend class="screen-reader-text"><span><?php echo $v['title'] ?> ?></span></legend>
					<p class="description"><?php echo $v['description'] ?></p>
					<?php wp_editor( $content, $this->id . '_' . $k, $settings ); ?>
					
				</fieldset>
			</td>
		</tr>
		<?php 
		return ob_get_clean();
	}
	
	public function setup_and_send( $user_id ) {

		if ( ! $this->is_enabled() )
			return;

		if ( empty( $user_id ) )
			return;
			
		$user = get_user_by( 'id', $user_id ); 
		
		if ( empty( $user ) || is_wp_error( $user ) )
			return;
			
		$this->recipient = $user->data->user_email;

		if ( empty( $this->recipient ) )
			return;
		
		$this->fname = get_user_meta( $user_id, 'billing_first_name', true ); 
		$this->lname = get_user_meta( $user_id, 'billing_last_name', true ); 
		
		$this->myaccount_page_url = get_permalink( wc_get_page_id( 'myaccount' ) );

		if ( !empty( $this->fname ) ) 
			$this->placeholders['{first_name}'] = $this->fname; 
			
		if ( !empty( $this->fname ) ) 
			$this->placeholders['{last_name}'] = $this->lname; 
					
		$this->sitename = get_option( 'blogname' );
		$this->sitename = wp_specialchars_decode( $this->sitename , ENT_QUOTES );

		$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
	}

	public function get_recipient() {
		return $this->recipient;
	}
	
	public function get_content() {
	
		$this->sending = true;
		
		// Don't let PHP output errors - which happen due to other plugins' code
		@ini_set( 'display_errors', false );
		
		$email_content = html_entity_decode( stripslashes( $this->settings['message'] ) );

		foreach( $this->placeholders as $p => $v )
			$email_content = str_replace( $p, $v, $email_content );
			
		ob_start();
	
		do_action( 'woocommerce_email_header', $this->heading, $this );
		echo $email_content;
		do_action( 'woocommerce_email_footer' );
		
		$email_content = ob_get_clean(); 
		
		return wordwrap( $email_content, 70 );
	}
	
	/*
	public function get_content_html() {
	}
	*/
	
}
