<?php 
/*
 * Copyright © 2014 IgniteWoo.com - All Rights Reserved
*/
class IgniteWoo_Wholesale_Pro_Discounts_Admin_Settings extends WC_Integration {


	function __construct() { 
		global $ignitewoo_wsp_discounts;
		
		$this->id = 'ignitewoo_wholesale_pro_suite';

		$this->plugin_url = $ignitewoo_wsp_discounts->plugin_url;
		
		$this->plugin_path = $ignitewoo_wsp_discounts->plugin_path;
		
		add_action( 'init', array( &$this, 'convert_role_and_prices' ), 1 );
		add_action( 'init', array( &$this, 'init' ), 99999 );
		add_filter ( 'woocommerce_settings_tabs_array', array( &$this, 'add_tab' ), 21, 1 );
		add_action( 'woocommerce_settings_tabs_wholesale_pro', array( &$this, 'settings' ) );
		add_action( 'woocommerce_update_options_wholesale_pro',  array( &$this, 'save_settings' ) );
		add_action( 'wp_ajax_ignitewoo_dd_create_new_ruleset', array( &$this, 'create_new_ruleset' ) );
		add_action( 'wp_ajax_ign_get_uniqid', array( &$this, 'get_id' ) );
	}
	
	function convert_role_and_prices() {
		global $wp_roles, $wpdb;
		
		if ( !empty( $_GET['wsp_dismiss'] ) ) { 
			update_option( '_wholesale_pro_suite_converted', true );
			$url = remove_query_arg( array( 'wsp_dismiss', 'wsp_convert' ) );
			wp_redirect( $url );
			die;
		}
		
		if ( empty( $_GET['wsp_convert'] ) )
			return;

		if ( class_exists( 'WP_Roles' ) )
		    if ( !isset( $wp_roles ) )
			$wp_roles = new WP_Roles();

		if ( is_object( $wp_roles ) ) {

			$new_role = false;

			foreach( $wp_roles->roles as $name => $role ) {
				if ( 'ignite_level_' == substr( $name, 0, 13 ) ) {
					$new_role = $name;
					break;
				}
			}

			if ( !$new_role )
				return;

			// Convert the former wholesale buyer role and associated prices if they class_exist
			$role = get_role( 'wholesale_buyer' ); 

			if ( empty( $role ) )
				return;
				
			// Convert all users with the role
			$users = get_users( array( 
				'role__in' => array( 'wholesale_buyer' ),
				'fields' => 'ID',
			));

			@set_time_limit( 0 );
			
			if ( !empty( $users ) ) foreach ( $users as $user ) { 
				$data = array(
					'ID'		 => $user,
					'role'       => $new_role,
				);

				$user_id = wp_update_user( $data );
			}
			
			@set_time_limit( 0 );
			
			// Convert all prices 
			$sql = "update " . $wpdb->postmeta . " set meta_key='_" . $new_role ."_price' where meta_key='wholesale_price'";
			$wpdb->query( $sql );

			update_option( '_wholesale_pro_suite_converted', true );
			
			remove_role( 'wholesale_buyer' );

			$url = remove_query_arg( array( 'wsp_convert', 'wsp_dismiss' ) );
			wp_redirect( $url );
			die;
		}
	}
	
	function get_id() { 
		die( json_encode( array( 'id' => uniqid() ) ) );
	}
	
	function init() { 
		wp_enqueue_script( 'jquery-ui-sortable' );
	}
	
	function add_tab( $tabs = '' ) { 
		$tabs['wholesale_pro'] = __( 'Wholesale Pro Suite', 'ignitewoo_wholesale_pro' );
		return $tabs;
	}

	function settings() { 
		global $wpdb, $ign_wholesale_pro_suite, $wp_roles;

		if ( class_exists( 'WP_Roles' ) ) 
		    if ( !isset( $wp_roles ) ) 
			$wp_roles = new WP_Roles();   

			
		$ign_wsp_roles = array();
		
		if ( is_object( $wp_roles ) ) { 
			
			foreach( $wp_roles->roles as $name => $role ) {
				if ( 'ignite_level_' == substr( $name, 0, 13 ) )
					$ign_wsp_roles[ $name ] = $role['name']; 	
			}
		}
		
		$sql = 'select ID, post_title from ' . $wpdb->posts . ' where post_type="page" and post_status="publish"';
		
		$pages = $wpdb->get_results( $sql );
		
		$page_data = array();
		
		if ( !empty( $pages ) )
		foreach( $pages as $k => $t ) 
			$page_data[ $t->ID ] = $t->post_title;
			
		
		$rules = get_option( 'woocommerce_dd_discount_rules', false );

		$show_table = get_option( 'woocommerce_dd_show_discount_table', false );
		
		$table_place = get_option( 'woocommerce_dd_discount_table_placement', false );
		
		$show_calc = get_option( 'woocommerce_dd_show_discount_calculator', false );
		
		$product_rules_override = get_option( 'woocommerce_dd_product_rules_override', false );
		
		$this->settings = get_option( 'woocommerce_'. $this->id . '_settings' );

		if ( empty( $this->settings ) ) {
			$this->settings['disable_coupons_error_msg'] = __( "Coupons are invalid for your purchase", 'ignitewoo_wholesale_pro' );
			$this->settings['show_savings_label'] = __( "You save", 'ignitewoo_wholesale_pro' );
			$this->settings['show_regular_price_label'] = __( "Regular price", 'ignitewoo_wholesale_pro' );
			$this->settings['ignitewoo_wsp_automatic_approval'] = 'no';
			$this->settings['ignitewoo_wsp_automatic_role'] = '';
			$this->settings['ignitewoo_wsp_pending_role'] = 'pending_wholesaler';
			$this->settings['ignitewoo_wsp_disallow_login'] = 'no';
			$this->settings['ignitewoo_wsp_register_default_country'] = 'US';
			$this->settings['ignitewoo_wsp_add_btn_to_myaccount'] = 'no';
			$this->settings['ignitewoo_wsp_add_btn_to_myaccount_logged_in'] = 'no';
			$this->settings['tier_filter'] = 'no';
			$this->settings['retail_filter'] = 'no';
			$this->settings['display_shop'] = 'excl';
			$this->settings['include_tax'] = 'excl';
			$this->settings['seq_order_number_prefix'] = '';
			$this->settings['login_required_for_prices'] = 'no';
			$this->settings['price_alt_type'] = 'nothing';
			$this->settings['price_alt_link'] = '';
			$this->settings['price_alt_text'] = '';
			
			$this->settings['reg_form_fields'] = unserialize( 'a:5:{s:5:"title";a:12:{i:0;s:18:"Billing First Name";i:1;s:17:"Billing Last Name";i:2;s:13:"Billing Email";i:3;s:15:"Billing Address";i:4;s:17:"Billing Address 2";i:5;s:12:"Billing City";i:6;s:15:"Billing Country";i:7;s:13:"Billing State";i:8;s:19:"Billing Postal Code";i:9;s:13:"Billing Phone";i:10;s:8:"Password";i:11;s:6:"Submit";}s:8:"required";a:12:{i:0;s:3:"yes";i:1;s:3:"yes";i:2;s:3:"yes";i:3;s:3:"yes";i:4;s:0:"";i:5;s:3:"yes";i:6;s:3:"yes";i:7;s:3:"yes";i:8;s:3:"yes";i:9;s:3:"yes";i:10;s:3:"yes";i:11;s:0:"";}s:10:"field_type";a:12:{i:0;s:4:"text";i:1;s:4:"text";i:2;s:4:"text";i:3;s:4:"text";i:4;s:4:"text";i:5;s:4:"text";i:6;s:4:"text";i:7;s:4:"text";i:8;s:4:"text";i:9;s:4:"text";i:10;s:8:"password";i:11;s:6:"button";}s:10:"field_name";a:12:{i:0;s:18:"billing_first_name";i:1;s:17:"billing_last_name";i:2;s:13:"billing_email";i:3;s:17:"billing_address_1";i:4;s:17:"billing_address_2";i:5;s:12:"billing_city";i:6;s:15:"billing_country";i:7;s:13:"billing_state";i:8;s:16:"billing_postcode";i:9;s:13:"billing_phone";i:10;s:22:"password_57e7dd3846a34";i:11;s:20:"button_57e7dd38469e4";}s:11:"field_align";a:12:{i:0;s:14:"form-row-first";i:1;s:13:"form-row-last";i:2;s:13:"form-row-wide";i:3;s:13:"form-row-wide";i:4;s:13:"form-row-wide";i:5;s:13:"form-row-wide";i:6;s:14:"form-row-first";i:7;s:13:"form-row-last";i:8;s:14:"form-row-first";i:9;s:13:"form-row-last";i:10;s:13:"form-row-wide";i:11;s:13:"form-row-wide";}}' );
			
		} else { 

			if ( empty( $this->settings['ignitewoo_wsp_automatic_role'] ) )
				$this->settings['ignitewoo_wsp_automatic_role'] = '';
				
			if ( empty( $this->settings['ignitewoo_wsp_pending_role'] ) )
				$this->settings['ignitewoo_wsp_pending_role'] = '';
				
			if ( empty( $this->settings['ignitewoo_wsp_disallow_login'] ) )
				$this->settings['ignitewoo_wsp_disallow_login'] = 'no';
		
			// NOTE: 
			// Flag to indicate the fix has been made to randomize custom fields (text, textarea, checkbox) in case there 
			// are more than one of any of those. 
			// Versions 2.1.36 and prior had a problem of not using unique field names for those field types. 
			// That was fix with randomization in the JS that adds the fields when clicked to add into the form. 
			if ( !get_option( 'ign_wholesale_reg_fields_fixed', false ) ) { 
			
			
				for( $i = 0; $i < count( $this->settings['reg_form_fields']['title'] ); $i++ ) { 

					if ( 'billing_' ==  substr( $this->settings['reg_form_fields']['field_name'][ $i ], 0, 8 ) )
						continue;
				
					$type = $this->settings['reg_form_fields']['field_type'][ $i ];
					
					if ( 'checkbox' == $type || 'text' == $type || 'textarea' == $type ) {
						$field_name = $type . '_' . uniqid() . rand( 0, 999999 );
						$this->settings['reg_form_fields']['field_name'][ $i ] = $field_name;
					}
				}
				
				update_option( 'ign_wholesale_reg_fields_fixed', true );
				
				update_option( 'woocommerce_'. $this->id . '_settings', $this->settings );
			}
		}

		$defaults = array( 
			'show_regular_price' => '',
			'show_regular_price_label' => '',
			'show_savings_label' => '',
			'show_savings' => '',
			'display_when_no_retail' => '',
			'tier_filter' => '',
			'retail_filter' => '',
			'alt_filter' => '',
			'disable_coupons' => '',
			'disable_coupons_error_msg' => '',
			'display_shop' => 'no',
			'display_cart' => 'excl',
			'include_tax' => 'no',
			'seq_order_number_prefix' => '',
			'login_required_for_prices' => 'no',
			'price_alt_type' => 'nothing',
			'price_alt_link' => '',
			'price_alt_text' => '',
		);

		$settings = wp_parse_args( $this->settings, $defaults );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'jquery-ui' );
		wp_enqueue_style( 'wsp-formbuilder-css', "//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" );

		
		?>
		<style>
		#woocommerce_extensions { display: none !important; }
		.woocommerce table.form-table select { 
			padding: 4px !important;
		}
		
		.ign_wholesale_nav_wrap .nav-tab { padding: 0px 10px; }
		.ign_wholesale_nav_wrap .nav-tab-wrapper { border-bottom: 1px solid #ccc; }
		.widefat.rules_table thead th, .widefat.rule_table thead th { 
			padding: 4px 10px;
		}
		.widefat thead tr th, .widefat thead tr td, .widefat tfoot tr th, .widefat tfoot tr td {
			background: #efefef none repeat scroll 0 0;
		}
		.add_pricing_rule {
			background:#008000;
			color:#fff;
			border-radius: 10px;
		}
		.delete_pricing_rule, .remove_rule_set {
			margin-left: 10px;
			background:#bf0000;
			color:#fff;
			border-radius: 10px;
			text-decoration: none;
		}
		.delete_pricing_rule:hover, .add_pricing_rule:hover {
			color: #fff;
		}
		.delete_pricing_rule, .add_pricing_rule {
			cursor: pointer;
			font-size: 14px;
			width: 14px;
			height: 14px;
			padding: 1px;
		}
		.add_pricing_rule .dashicons, .delete_pricing_rule .dashicons-before::before {
			font-size: 14px !important;
			height: 14px;
			position: relative;
			top: 3px;
			width: 18px;
		}
		.remove_rule_set {
			cursor: pointer;
			font-size: 16px;
			width: 16px;
			height: 16px;
		}
		.remove_rule_set .dashicons, .remove_rule_set .dashicons-before::before {
			font-size: 14px !important;
			position:relative;
			left: 1px;
			top: 1px;
			height: 14px;
			width: 14px;
		}
		.remove_rule_set:hover {
			color: #fff;
		}
		#ign_form_fields {
			border: 1px solid #ccc;
			width: 350px;
			margin-right: 20px;
		}
		#ign_form_fields li { 
			cursor: pointer;
		}
		#ign_form_fields, #ign_form_layout {
			background-color: #fff;
			min-height: 289px;
			list-style-type: none;
			margin: 0;
			padding: 10px;
			margin-right: 10px;
		}
		#ign_form_fields li, #ign_form_layout li {
			border-radius: 4px;
			background-color: #fff;
			margin: 0 5px 5px 5px;
			padding: 5px;
			width: 300px;
		}
		#ign_form_fields li {
			background-color: #f7f7f7;
			width: 150px;
			display:inline-block;
			
		}
		#ign_form_fields li .title_display {
			font-weight: bold;
			padding-left: 10px;
		}
		#ign_form_layout li {
			background-color: #f7f7f7;
			border: 1px solid #ccc;
			margin-bottom: 1.3em;
		}
		#ign_form_layout {
			border: 1px solid #ccc;
			width: 320px;
			min-height: 400px;
		}
		#ign_form_layout .move { 
			cursor: move;
			position: relative;
		}
		#ign_form_layout .remove { 
			cursor: pointer;
			color: #cf0000 !important;
			position: relative;
		}
		.ign_form_controls { 
			position:relative; 
			left:5px; 
			display:none;
		}
		.ign_form_field_data { 
			text-align:left; 
			display:inline-block;
		}
		.ign_form_field_wrap .field_type {
			background-color: #f7f7f7;
			display: none;
			font-size: 0.85em;
			margin-bottom: 0px;
			padding: 2px 0 0px 6px;
		}
		.ign_form_field_wrap .dashicons, .ign_form_field_wrap .dashicons-before::before {
			font-size: 16px;
		}
		.alt td { 
			background-color: #f7f7f7;
		}
		</style> 
		<script>
			jQuery( document ).ready( function($) { 
				$( 'div.settings_section' ).css( { 'margin-left': '0', 'display' : 'none' } );
				$( 'div.settings' ).show();
				$( '.ign_wholesale_nav_wrap .nav-tab' ).on( 'click', function(e) { 
					e.preventDefault();
					$( '.ign_wholesale_nav_wrap .nav-tab' ).removeClass( 'nav-tab-active' );
					$( this ).addClass( 'nav-tab-active' ).blur()
					
					var section = $( this ).data( 'section' );
					$( 'div.settings_section' ).hide();
					$( 'div.' + section ).addClass( 'nav-tab-active' ).show();
				})
			});
		</script>
		
		<h2><?php _e( 'Wholesale Pro Suite Settings', 'ignitewoo_wholesale_pro' ) ?> <span style="font-weight:normal;font-size:14px;">&ndash; <?php _e( 'Created by', 'ignitewoo_wholesale_pro' )?></span><a href="http://ignitewoo.com" title="Documentation available at IgniteWoo.com" target="_blank" style="text-decoration:none"><?php $this->get_ad_box(); ?></a><span style="font-weight:normal;font-size:14px;"> &ndash; <?php _e( 'Be sure to read the', 'ignitewoo_wholesale_pro' )?> <a href="http://ignitewoo.com/ignitewoo-software-documentation/" title="Documentation available at IgniteWoo.com" target="_blank" style="text-decoration:none"><?php _e( 'documentation', 'ignitewoo_wholesale_pro' )?></a></span></h2>

		<p></p>
		
		<?php
		$role = get_role( 'wholesale_buyer' ); 
		$converted = get_option( '_wholesale_pro_suite_converted', false );
		?>

		<?php if ( !empty( $role ) && empty( $converted ) ) { ?>
			<div style="border-left:3px solid #ccc; border-left-color: #46b450;background-color:#fff;padding:0.001em 10px">
			<p>
			<?php echo sprintf( __( "It appears that you were previously using WooCommerce Wholesale Pricing. <a href='%s' class='button-primary wsp_convert'>Migrate your users and prices</a> <a href='%s' class='button wsp_dismiss'>Dismiss</a>", 'ignitewoo_wholesale_pro' ), add_query_arg( array( 'wsp_convert' => true ) ), add_query_arg( array( 'wsp_dismiss' => true ) ) ) ?>
			</p>
			</div>
			<script>
			jQuery( document ).ready( function($) {
				$( '.wsp_dismiss' ).on( 'click', function(e) { 
					if ( !confirm( '<?php _e( "Are you sure? If you dismiss this notice you won\'t be given a chance to migrate later.", 'ignitewoo_wholesale_pro' ) ?>' ) )
						return false; 
				})
				$( '.wsp_convert' ).on( 'click', function(e) { 
					if ( !confirm( '<?php _e( "Make sure you backup your database before proceeding! When you proceed the migration may take a few minutes or longer. Please be patient.", 'ignitewoo_wholesale_pro' ) ?>' ) )
						return false; 
				})
			})
			</script>
		<?php } ?>
		
		<?php /*
		<p><?php _e( 'Configure the settings to control how your price discounts work in your store.', 'ignitewoo_wholesale_pro' ) ?></p>
		*/ ?>
		
		<div class="ign_wholesale_nav_wrap">
			<nav class="nav-tab-wrapper">
				<a class="nav-tab nav-tab-active" href="#" data-section="settings"><?php _e( 'Settings', 'ignitewoo_wholesale_pro' ) ?></a>
				<a class="nav-tab " href="#" data-section="roles"><?php _e( 'Roles', 'ignitewoo_wholesale_pro' ) ?></a>
				<a class="nav-tab " href="#" data-section="global"><?php _e( 'Global Pricing', 'ignitewoo_wholesale_pro' ) ?></a>
				<a class="nav-tab " href="#" data-section="rules"><?php _e( 'Qty Discount Rules', 'ignitewoo_wholesale_pro' ) ?></a>
				<a class="nav-tab " href="#" data-section="minmax"><?php _e( 'Min/Max Quantities', 'ignitewoo_wholesale_pro' ) ?></a>
				<a class="nav-tab " href="#" data-section="reg"><?php _e( 'Registration', 'ignitewoo_wholesale_pro' ) ?></a>
			</nav>
		</div>
		
		<div class="settings settings_section">
			<table class="form-table" style="width:99%;margin:0;padding:0">
				<tr>
					<th style="width: 120px; vertical-align:top">
						<h3 style="margin:0;font-size:1.2em"><label for="logo_image"><?php _e( 'Visibility', 'ignitewoo_wholesale_pro' ) ?></label></h3>
					</th>
					
				</tr>
				<tr>
					<th style="width: 120px; vertical-align:top">
						<h4 style="margin:0"><label for="logo_image"><?php _e( 'Role Products Only', 'ignitewoo_wholesale_pro' ) ?></label></h4>
					</th>
					<td>
						<input type="checkbox" value="yes" name="display_when_no_retail" <?php if ( '' != $settings['display_when_no_retail'] ) echo 'checked="checked"' ?>/> <?php _e('Enable wholesale-only products', 'ignitewoo_wholesale_pro' )?>
						<p class="description"><?php _e( 'Normally, if a product has no retail price set then no buy button appears when viewing the product.', 'ignitewoo_wholesale_pro' )?></p>
						<p class="description"><?php _e( 'When this feature is enabled products that have no retail price but do have a role price will be visible to, and purchasable by, role-based buyers.', 'ignitewoo_wholesale_pro' ) ?></p>
						<p class="description" style="font-weight:bold"><?php _e( 'When using this feature do not set the product regular price to 0 (zero) otherwise it will appear in your store as free! Instead leave the regular price field blank. ', 'ignitewoo_wholesale_pro' ) ?></p>

					</td>
				</tr>

				<tr>
					<th style="width: 120px; vertical-align:top">
						<h4 style="margin:0"><label for="logo_image"><?php _e( 'Role Product Filter', 'ignitewoo_wholesale_pro' ) ?></label></h4>
					</th>
					<td>
						<input type="checkbox" value="yes" name="tier_filter" <?php if ( '' != $settings['tier_filter'] ) echo 'checked="checked"' ?>/> <?php _e('Enable filtering for role-based buyers', 'ignitewoo_wholesale_pro' )?>
						<p class="description"><?php _e( 'When enabled role-based buyers will only see products that have role pricing set. No other products will appear in your store.', 'ignitewoo_wholesale_pro' ) ?></p>
					</td>
				</tr>

				<tr>
					<th style="width: 120px; vertical-align:top">
						<h4 style="margin:0"><label for="logo_image"><?php _e( 'Retail Filter', 'ignitewoo_wholesale_pro' ) ?></label></h4>
					</th>
					<td>
						<input type="checkbox" value="yes" name="retail_filter" <?php if ( '' != $settings['retail_filter'] ) echo 'checked="checked"' ?>/> <?php _e('Enable filtering for retail buyers', 'ignitewoo_wholesale_pro' )?>
						<p class="description"><?php _e( 'When enabled, retail buyers will only see products that have retail pricing set. Products that only have role prices products will not appear.', 'ignitewoo_wholesale_pro' ) ?></p>
					</td>
				</tr>
				
				<?php /*
				<tr>
					<th style="width: 120px; vertical-align:top">
						<h4 style="margin:0"><label for="logo_image"><?php _e( 'Alternative Filtering', 'ignitewoo_wholesale_pro' ) ?></label></h4>
					</th>
					<td>
						<input type="checkbox" value="yes" name="alt_filter" <?php if ( '' != $settings['alt_filter'] ) echo 'checked="checked"' ?>/> <?php _e('Enable alternative filtering', 'ignitewoo_wholesale_pro' )?>
						<p class="description"><?php _e( 'When enabled, if either Retail or Wholesale filters are enabled then an alternative filtering method will be used. Helpful with themes whose menus do not appear correctly when filtering is enabled', 'ignitewoo_wholesale_pro' ) ?></p>
					</td>
				</tr>
				*/ ?>
				
				
				
				<tr valign="top">
					<th class="titledesc" scope="row">
						<label for="roles">
						<?php _e( 'Login required to see prices', 'ignitewoo_wholesale_pro' )?>
						<span class="woocommerce-help-tip" data-tip="<?php _e( 'Hide all prices until the user logs in', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">	
						<select class="select2 login_required_for_prices" name="login_required_for_prices"> 
							<option value="no" <?php selected( $settings['login_required_for_prices'], 'no', true ) ?>><?php _e( 'No', 'ignitewoo_wholesale_pro' ) ?></option>
							<option value="yes" <?php selected( $settings['login_required_for_prices'], 'yes', true ) ?>><?php _e( 'Yes', 'ignitewoo_wholesale_pro' ) ?></option>
						</select>
					</td>
				</tr>
				<tr valign="top" class="price_alt_type">
					<th class="titledesc" scope="row">
						<label for="roles">
						<?php _e( 'Show instead of price', 'ignitewoo_wholesale_pro' )?>
						<span class="woocommerce-help-tip" data-tip="<?php _e( 'What to show instead of prices when --Login required to see prices-- is enabled', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">	
						<select class="select2 price_alt_type" name="price_alt_type"> 
							<option value="nothing" <?php selected( $settings['price_alt_type'], 'nothing', true ) ?>><?php _e( 'Nothing', 'ignitewoo_wholesale_pro' ) ?></option>
							<option value="link" <?php selected( $settings['price_alt_type'], 'link', true ) ?>><?php _e( 'Link', 'ignitewoo_wholesale_pro' ) ?></option>
							<option value="button" <?php selected( $settings['price_alt_type'], 'button', true ) ?>><?php _e( 'Button', 'ignitewoo_wholesale_pro' ) ?></option>
							<option value="text" <?php selected( $settings['price_alt_type'], 'text', true ) ?>><?php _e( 'Text', 'ignitewoo_wholesale_pro' ) ?></option>
						</select>
					</td>
				</tr>
				<tr valign="top" class="price_alt_link">
					<th class="titledesc" scope="row">
						<label for="roles">
							<span class="price_alt_link_desc"><?php _e( 'URL for button or link', 'ignitewoo_wholesale_pro' )?></span>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'Link for button or text when "Login required to see prices" is enabled', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">	
						<input type="text" name="price_alt_link" value="<?php echo $settings['price_alt_link'] ?>"> 
					</td>
				</tr>
				<tr valign="top" class="price_alt_text">
					<th class="titledesc" scope="row">
						<label for="roles">
							<span class="price_alt_text_desc"><?php _e( 'Text for link or button', 'ignitewoo_wholesale_pro' )?></span>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'Text to display by itself, or for the link or button when "Login required to see prices" is enabled', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">	
						<input type="text" name="price_alt_text" value="<?php echo $settings['price_alt_text'] ?>"> 
						
						<script>
						jQuery( document ).ready( function($) { 
							$( 'select.login_required_for_prices' ).on( 'change', function() { 
								if ( 'no' == $( this ).val() ) { 
									$( 'tr.price_alt_link, tr.price_alt_text, tr.price_alt_type' ).hide();
								} else { 
									$( 'tr.price_alt_type' ).show();
									$( 'select.price_alt_type' ).trigger( 'change' );
								}
							})
							$( 'select.price_alt_type' ).on( 'change', function() { 
								if ( 'nothing' == $( this ).val() ) { 
									$( 'tr.price_alt_link, tr.price_alt_text' ).hide();
								} else if ( 'link' == $( this ).val() ) { 
									$( 'tr.price_alt_link' ).show();
									$( 'tr.price_alt_text' ).show();
									$( 'span.price_alt_text_desc' ).html( '<?php _e( 'Text to display for link', 'ignitewoo_wholesale_pro' )?>' );
									$( 'span.price_alt_link_desc' ).html( '<?php _e( 'URL for link', 'ignitewoo_wholesale_pro' )?>' );
								} else if ( 'button' == $( this ).val() ) { 
									$( 'tr.price_alt_link' ).show();
									$( 'tr.price_alt_text' ).show();
									$( 'span.price_alt_text_desc' ).html( '<?php _e( 'Text to display for button', 'ignitewoo_wholesale_pro' )?>' );
									$( 'span.price_alt_link_desc' ).html( '<?php _e( 'URL for button', 'ignitewoo_wholesale_pro' )?>' );
								} else if ( 'text' == $( this ).val() ) { 
									$( 'tr.price_alt_link' ).hide();
									$( 'tr.price_alt_text' ).show();
									$( 'span.price_alt_text_desc' ).html( '<?php _e( 'Text to display', 'ignitewoo_wholesale_pro' )?>' );
								}
							})
							$( 'select.login_required_for_prices' ).trigger( 'change' );
							$( 'select.price_alt_type' ).trigger( 'change' );
						});
						</script>
					</td>
				</tr>
	
				
				
				<tr>
					<th style="width: 120px; vertical-align:top">
						<h3 style="margin:0;font-size:1.2em"><label for="logo_image"><?php _e( 'Coupons', 'ignitewoo_wholesale_pro' ) ?></label></h3>
					</th>
					
				</tr>
				<tr>
					<th style="width: 120px; vertical-align:top">
						<h4 style="margin:0"><label for="logo_image"><?php _e( 'Disable Coupons', 'ignitewoo_wholesale_pro' ) ?></label></h4>
					</th>
					<td>
						<input type="checkbox" value="yes" name="disable_coupons" <?php if ( '' != $settings['disable_coupons'] ) echo 'checked="checked"' ?>/> <?php _e('Disable coupons for role-based buyers', 'ignitewoo_wholesale_pro' )?>
						<p class="description"><?php _e( 'When this feature is enabled role-based buyers cannot use coupon codes when ordering unless you assign their role to the coupon.', 'ignitewoo_wholesale_pro' ) ?></p>

						<?php _e('Message', 'ignitewoo_wholesale_pro' )?> <input style="width: 400px" type="text" value="<?php echo $settings['disable_coupons_error_msg'] ?>" name="disable_coupons_error_msg"/>
						<p class="description"><?php _e( 'Enter the message displayed to role-based buyers when they try to use a coupon and coupons are disabled for role-based buyers. This field cannot be blank.', 'ignitewoo_wholesale_pro' ) ?></p>

					</td>
				</tr>
				
				
				<tr>
					<th style="width: 120px; vertical-align:top">
						<h3 style="margin:0;font-size:1.2em"><label for="logo_image"><?php _e( 'Orders', 'ignitewoo_wholesale_pro' ) ?></label></h3>
					</th>
					
				</tr>
				<?php //if ( class_exists( 'WC_Seq_Order_Number_Pro' ) ) { ?>
				<tr>
					<th style="width: 120px; vertical-align:top">
						<h4 style="margin:0"><label for="logo_image"><?php _e( 'Add Order Number Prefix', 'ignitewoo_wholesale_pro' ) ?></label></h4>
					</th>
					<td>
						<input type="text" name="seq_order_number_prefix" value="<?php if ( '' != $settings['seq_order_number_prefix'] ) echo $settings['seq_order_number_prefix' ] ?>"/> 
						<p class="description"><?php _e( 'When using Sequential Order Numbers Pro you can add this prefix to order numbers if the order is from a shopper that has one of your defined roles. Note that this prefix is in additional to any prefix you use in Sequential Order Numbers Pro', 'ignitewoo_wholesale_pro' ) ?></p>
					</td>
				</tr>
				<?php //} ?>
				
				<tr>
					<th style="width: 120px; vertical-align:top">
						<h3 style="margin:0;font-size:1.2em"><label for="logo_image"><?php _e( 'Price Display', 'ignitewoo_wholesale_pro' ) ?></label></h3>
					</th>
					
				</tr>
				<tr valign="top">
					<th class="titledesc" scope="row">
						<label for="roles">
						<?php _e( 'Prices entered with tax', 'ignitewoo_wholesale_pro' )?>
						<span class="woocommerce-help-tip" data-tip="<?php _e( 'How taxes are displayed in the shop, product category pages, and product pages for users that have a custom role. ', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">	
						<select class="select2" name="include_tax"> 
							<option value="no" <?php selected( $settings['include_tax'], 'no', true ) ?>><?php _e( 'I enter prices excluding tax', 'ignitewoo_wholesale_pro' ) ?></option>
							<option value="yes" <?php selected( $settings['include_tax'], 'yes', true ) ?>><?php _e( 'I enter prices including tax', 'ignitewoo_wholesale_pro' ) ?></option>
						</select>
					</td>
				</tr>
				<tr valign="top">
					<th class="titledesc" scope="row">
						<label for="roles">
						<?php _e( 'Display prices in the shop', 'ignitewoo_wholesale_pro' )?>
						<span class="woocommerce-help-tip" data-tip="<?php _e( 'How taxes are displayed in the shop, product category pages, and product pages for users that have a custom role. ', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">	
						<select class="select2" name="display_shop"> 
							<option value="excl" <?php selected( $settings['display_shop'], 'excl', true ) ?>><?php _e( 'Excluding tax', 'ignitewoo_wholesale_pro' ) ?></option>
							<option value="incl" <?php selected( $settings['display_shop'], 'incl', true ) ?>><?php _e( 'Including tax', 'ignitewoo_wholesale_pro' ) ?></option>
						</select>
					</td>
				</tr>
				<tr valign="top">
					<th class="titledesc" scope="row">
						<label for="roles">
						<?php _e( 'Display prices in cart and checkout', 'ignitewoo_wholesale_pro' )?>
						<span class="woocommerce-help-tip" data-tip="<?php _e( 'How taxes are displayed in the cart and checkout page for users that have a custom role.', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">	
						<select class="select2" name="display_cart"> 
							<option value="excl" <?php selected( $settings['display_cart'], 'excl', true ) ?>><?php _e( 'Excluding tax', 'ignitewoo_wholesale_pro' ) ?></option>
							<option value="incl" <?php selected( $settings['display_cart'], 'incl', true ) ?>><?php _e( 'Including tax', 'ignitewoo_wholesale_pro' ) ?></option>
						</select>
					</td>
				</tr>
				<tr valign="top">
					<th class="titledesc" scope="row">
						<label for="roles">
						<?php echo sprintf( __( 'Display %s Savings', 'ignitewoo_wholesale_pro' ), get_woocommerce_currency_symbol() ) ?>
						<span class="woocommerce-help-tip" data-tip="<?php _e( 'Check this to display the amount being saved.', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">	
						<input type="checkbox" value="yes" name="show_savings" <?php checked( $settings['show_savings'], 'yes', true ) ?>> 
						<input type="text" value="<?php echo $settings['show_savings_label'] ?>" name="show_savings_label"> 
					</td>
				</tr>
				<tr valign="top">
					<th class="titledesc" scope="row">
						<label for="roles">
						<?php _e( 'Display Regular Price', 'ignitewoo_wholesale_pro' )?>
						<span class="woocommerce-help-tip" data-tip="<?php _e( 'Check this to display the regular price.', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">	
						<input type="checkbox" value="yes" name="show_regular_price" <?php checked( $settings['show_regular_price'], 'yes', true ) ?>>  
						<input type="text" value="<?php echo $settings['show_regular_price_label'] ?>" name="show_regular_price_label"> 
					</td>
				</tr>
				<tr>
					<th style="width: 120px; vertical-align:top">
						<h3 style="margin:0;font-size:1.2em"><label for="logo_image"><?php _e( 'Orders', 'ignitewoo_wholesale_pro' ) ?></label></h3>
					</th>
					
				</tr>
				<tr valign="top">
					<th class="titledesc" scope="row">
						<label for="roles">
						<?php _e( 'Thank You Page Message', 'ignitewoo_wholesale_pro' )?>
						<span class="woocommerce-help-tip" data-tip="<?php _e( 'Optional. Displays above the order details on the thank you page after an order has been placed IF the shopper has one of your defined pricing roles.', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">	
						<textarea type="checkbox" value="yes" cols="80" rows="10" name="thankyou_message"><?php echo !empty( $settings['thankyou_message'] ) ? $settings['thankyou_message'] : '' ?></textarea>
						 
					</td>
				</tr>
			</table>
		</div>
		<div class="global settings_section" style="margin-left:-9999px">
			<table class="form-table">

				<tr>
					<th style="width: 120px; vertical-align:top">
						<h4 style="margin:0"><label for="logo_image">
							<?php _e( 'Global Discounts', 'ignitewoo_wholesale_pro' ) ?></label>
						</h4>
					</th>
					<td>
						<p class="description">
							<?php _e( 'You can define global discounts for specific roles. These discounts apply to all products unless you override the discounts by setting a specific price for the role when editing the product.', 'ignitewoo_wholesale_pro' ) ?> 
							<?php _e( 'These discounts are used to determine product base prices and any quantity discount rules you define are applied to the base price as calculated.', 'ignitewoo_wholesale_pro' ) ?><br/>
						</p>
						<p class="description">
							<?php _e( 'Be sure to define your roles first!', 'ignitewoo_wholesale_pro' ) ?><br/>
						</p>
					</td>
				</tr>
				<tr>
					<table class="widefat">
					<thead>
					<tr>
						<th style="width:200px"><?php _e( 'Role', 'ignitewoo_wholesale_pro' ) ?></th>
						<th><?php _e( 'Discount', 'ignitewoo_wholesale_pro' ) ?><span class="woocommerce-help-tip" data-tip="<?php _e( 'Enter a percentage amount with the percent symbol', 'ignitewoo_wholesale_pro' )?>"></span></th>
					</tr>
					</thead>
					<tbody>
					<tr>
					<td>
				<?php 

				asort( $wp_roles->roles );

				$i = 0;
				foreach( $wp_roles->roles as $role => $data ) { 

					if ( 'ignite_level_' != substr( $role, 0, 13 ) )
						continue;
						
					if ( 0 == $i % 2 )
						$alt = '';
					else 
						$alt = 'alt';
					
					$i++;
					
					?>
					
					<tr class="<?php echo $alt ?>">
						<td>
							<label class="">
								<span><?php echo stripslashes( $data['name'] ) ?></span> 
							</label>
						</td>
						<td>
							<input type="text" class="short small" style="width:75px" name="global_discounts[<?php echo $role ?>]" value="<?php if ( !empty( $this->settings['global_discounts'][ $role ] ) ) echo $this->settings['global_discounts'][ $role ] ?>"><?php _e( '%', 'ignitewoo_wholesale_pro' ) ?>
						</td>
					</tr>
				<?php } ?>
					</td>
					</tr>
					</tbody>
					</table>
				
			</table>
		</div>
		<div class="rules settings_section" style="margin-left:-9999px">
			<table class="form-table">

				<tr>
					<th style="width: 120px; vertical-align:top">
						<h4 style="margin:0"><label for="logo_image">
							<?php _e( 'Sitewide Discounts', 'ignitewoo_wholesale_pro' ) ?></label>
						</h4>
					</th>
					<td>
						<p class="description">
							<?php _e( 'You can enable sitewide discounts for products.', 'ignitewoo_wholesale_pro' ) ?> 
							<?php _e( 'These rules will be overridden by the per-product discount rules if set for  individual products.', 'ignitewoo_wholesale_pro' ) ?><br/>
							<?php _e( 'Rearrange rulesets by dragging and dropping them into a new order.', 'ignitewoo_wholesale_pro' ) ?><br/>
						</p>
					</td>
				</tr>		
				<tr>
					<th style="width: 200px; vertical-align:top">
						<h4 style="margin:0"><label for="logo_image">
							<?php _e( 'Enable Discounts Table', 'ignitewoo_wholesale_pro' ) ?></label>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'Show discount table on product pages.', 'ignitewoo_wholesale_pro' ) ?>"></span>
						</h4>
					</th>
					<td>
						<p class="description">
							<input type="checkbox" value="yes" name="dd_show_discount_table" <?php checked( $show_table, 'yes', true ) ?>>
							<?php _e( 'Enable', 'ignitewoo_wholesale_pro' ) ?>
						</p>
					</td>
				</tr>
				
				<tr>
					<th style="width: 120px; vertical-align:top">
						<h4 style="margin:0"><label for="logo_image">
							<?php _e( 'Discount Table Placement', 'ignitewoo_wholesale_pro' ) ?></label>
							
							<span class="woocommerce-help-tip" data-tip="<?php _e( "Where to show the discounts table. If the placement does not come out right it's probably because your theme is built differently than normal. Experiment, or insert the table manually. See the documentation.", 'ignitewoo_wholesale_pro' ) ?>"></span>
						</h4>
					</th>
					<td>
						<p class="description">
							<select name="dd_discount_table_placement" class="chosen" style="width:200px">
							
								<option value="the_content_top" <?php selected( $table_place, 'the_content_top', true ) ?>><?php _e( 'Before Product Description', 'ignitewoo_wholesale_pro' ) ?></option>
							
								<option value="woocommerce_before_add_to_cart_form" <?php selected( $table_place, 'woocommerce_before_add_to_cart_form', true ) ?>><?php _e( 'Before Add To Cart Form', 'ignitewoo_wholesale_pro' ) ?></option>
								
								<option value="woocommerce_after_add_to_cart_form" <?php selected( $table_place, 'woocommerce_after_add_to_cart_form', true ) ?>><?php _e( 'After Add To Cart Form', 'ignitewoo_wholesale_pro' ) ?></option>
							
								<option value="woocommerce_before_add_to_cart_button" <?php selected( $table_place, 'woocommerce_before_add_to_cart_button', true ) ?>><?php _e( 'Before Add To Cart Button', 'ignitewoo_wholesale_pro' ) ?></option>
								
								<option value="woocommerce_after_add_to_cart_button" <?php selected( $table_place, 'woocommerce_after_add_to_cart_button', true ) ?>><?php _e( 'After Add To Cart Button', 'ignitewoo_wholesale_pro' ) ?></option>
								
								<option value="woocommerce_after_single_product_summary" <?php selected( $table_place, 'woocommerce_after_single_product_summary', true ) ?>><?php _e( 'Before Information Tabs', 'ignitewoo_wholesale_pro' ) ?></option>
							</select>
						</p>
					</td>
				</tr>
				<tr>
					<th style="width: 120px; vertical-align:top">
						<h4 style="margin:0"><label for="logo_image">
							<?php _e( 'Enable Discount Calculator', 'ignitewoo_wholesale_pro' ) ?></label>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'Show discount calculator button on product pages.', 'ignitewoo_wholesale_pro' ) ?>"></span>
						</h4>
					</th>
					<td>
						<p class="description">
							<input type="checkbox" value="yes" name="dd_show_discount_calculator" <?php checked( $show_calc, 'yes', true ) ?>>
							<?php _e( 'Enable', 'ignitewoo_wholesale_pro' ) ?>
						</p>
					</td>
				</tr>
				
				<tr>
					<th style="width: 120px; vertical-align:top">
						<h4 style="margin:0"><label for="logo_image">
							<?php _e( 'Product Rules Override Global Rules', 'ignitewoo_wholesale_pro' ) ?></label>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'When enabled, if an individual product has any rules defined then global rules are never processed for that product. When disabled then product rules are processed first and then global rules are processed.', 'ignitewoo_wholesale_pro' ) ?>"></span>
						</h4>
					</th>
					<td>
						<p class="description">
							<input type="checkbox" value="yes" name="dd_product_rules_override" <?php checked( $product_rules_override, 'yes', true ) ?>>
							<?php _e( 'Enable', 'ignitewoo_wholesale_pro' ) ?>
						</p>
					</td>
				</tr>
				
			</table>
			<?php $this->sitewide_discount_rule_box() ?>
			
		</div>
		<div class="roles settings_section" style="margin-left:-9999px">
			<table class="form-table">
				<?php $this->role_settings() ?>
			</table>
		</div>
		<div class="minmax settings_section" style="margin-left:-9999px">
			<table class="form-table">
				<?php $this->init_minmax_fields() ?>
				<?php parent::admin_options(); ?>
			</table>
		</div>
		<div class="reg settings_section" style="margin-left:-9999px">
			<table class="form-table">
				<tr valign="top">
					<th class="titledesc" scope="row">
						<label for="roles"><?php _e( 'Automatic Approval', 'ignitewoo_wholesale_pro' )?>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'Automatically approve new applicants. If you enable this then you must choose a role to assign to the applicant when they are approved. If you do not enable this then you need to choose a role to assign to the applicant while their registration is pending.', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">
						<input type="checkbox" class="ignitewoo_wsp_automatic_approval" name="ignitewoo_wsp_automatic_approval" value="yes" <?php checked( $settings['ignitewoo_wsp_automatic_approval'], 'yes', true ) ?>> <?php _e( 'Enable', 'ignitewoo_wholesale_pro' )?>
						
						<div style="clear:both;margin-top:12px;font-style:italic" class="description">
							<?php echo sprintf( __( 'If you want to also automatically send an approval email notification then be sure to enable the <a href="%s" target="_blank">Wholesale Approval Email</a>', 'ignitewoo_wholesale_pro' ), admin_url( 'admin.php?page=wc-settings&tab=email' ) ) ?>
						</div>
						<script>
						jQuery( document ).ready( function($) { 
							$( '.ignitewoo_wsp_automatic_approval' ).on( 'change', function(e) {
								if ( $( this ).is( ':checked' ) ) { 
									$( '.ignitewoo_wsp_pending_role' ).hide();
									$( '.ignitewoo_wsp_disallow_login' ).hide();
									$( '.ignitewoo_wsp_automatic_role' ).show(750);
								} else { 
									$( '.ignitewoo_wsp_automatic_role' ).hide();
									$( '.ignitewoo_wsp_pending_role' ).show(750);
									$( '.ignitewoo_wsp_disallow_login' ).show(750);
								}
							});
							$( 'input[name="ignitewoo_wsp_automatic_approval"]' ).trigger( 'change' );
						});
						</script>
					</td>
				</tr>
				<tr valign="top" class="ignitewoo_wsp_automatic_role">
					<th class="titledesc" scope="row">
						<label for="roles"><?php _e( 'Automatic Approval Role', 'ignitewoo_wholesale_pro' )?>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'Choose a role for automatically approved registrants', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">
						<select class="chosen" name="ignitewoo_wsp_automatic_role" style="width:250px"> 
							<option value=""> &nbsp; </option>
							<?php 
							if ( empty( $ign_wsp_roles ) ) { 
								_e( 'You have not created any roles yet', 'ignitewoo_wholesale_pro' );
							} else { 
								foreach( $ign_wsp_roles as $role => $name ) {
								?>
								<option value="<?php echo $role ?>" <?php selected( $settings['ignitewoo_wsp_automatic_role'], $role, true ) ?>><?php echo $name ?></option>
								<?php
								}
							}
							?>
						</select>
					</td>
				</tr>
				<tr valign="top" class="ignitewoo_wsp_pending_role">
					<th class="titledesc" scope="row">
						<label for="roles"><?php _e( 'Pending Approval Role', 'ignitewoo_wholesale_pro' )?>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'Choose a role to pending registrant user account. For example: Pending Wholesaler', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">

						<select class="chosen" name="ignitewoo_wsp_pending_role" style="width:250px"> 
							<option value=""> &nbsp; </option>
							<?php 
						
							foreach( $wp_roles->roles as $role => $data ) {
							?>
							<option value="<?php echo $role ?>" <?php selected( $settings['ignitewoo_wsp_pending_role'], $role, true ) ?>><?php echo $data['name'] ?></option>
							<?php
							}
						
							?>
						</select>
					</td>
				</tr>
				<tr valign="top" class="ignitewoo_wsp_disallow_login">
					<th class="titledesc" scope="row">
						<label for="roles"><?php _e( 'Disallow Login While Pending', 'ignitewoo_wholesale_pro' )?>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'When enabled any user with your selected "pending" role cannot login to the site', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">
						<input type="checkbox" class="ignitewoo_wsp_disallow_login" name="ignitewoo_wsp_disallow_login" value="yes" <?php checked( $settings['ignitewoo_wsp_disallow_login'], 'yes', true ) ?>> <?php _e( 'Enable', 'ignitewoo_wholesale_pro' )?>
					</td>
				</tr>
				<tr>
					<th class="titledesc" scope="row">
						<label for="roles"><?php _e( 'Add Button to My Account Page', 'ignitewoo_wholesale_pro' )?>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'Add button to the My Account page linking to the registration page', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp">
						<input type="checkbox" name="ignitewoo_wsp_add_btn_to_myaccount" value="yes" <?php checked( $settings['ignitewoo_wsp_add_btn_to_myaccount'], 'yes', true ) ?>> <?php _e( 'Enable', 'ignitewoo_wholesale_pro' )?>
					</td>
				</tr>
				<tr>
					<th class="titledesc" scope="row">
						<label for="roles"><?php _e( 'My Account Button Text', 'ignitewoo_wholesale_pro' )?>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'Add button to the My Account page linking to the registration page', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp">
						<input type="text" name="ignitewoo_wsp_add_myaccount_btn_label" value="<?php echo isset(  $settings['ignitewoo_wsp_add_myaccount_btn_label'] ) ? $settings['ignitewoo_wsp_add_myaccount_btn_label'] : _e( 'Wholesale Registration', 'ignitewoo_wholesale_pro' ) ?>" style="width:250px">
					</td>
				</tr>
				<tr>
					<th class="titledesc" scope="row">
						<label for="roles"><?php _e( 'Add Button to My Account Page Logged In', 'ignitewoo_wholesale_pro' )?>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'Add button to the My Account page even when user is logged in', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp">
						<input type="checkbox" name="ignitewoo_wsp_add_btn_to_myaccount_logged_in" value="yes" <?php checked( $settings['ignitewoo_wsp_add_btn_to_myaccount_logged_in'], 'yes', true ) ?>> <?php _e( 'Enable', 'ignitewoo_wholesale_pro' )?>
					</td>
				</tr>
				<tr valign="top">
					<th class="titledesc" scope="row">
						<label for="roles"><?php _e( 'Registration Page', 'ignitewoo_wholesale_pro' )?>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'Select your registration page', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">
						<select class="chosen" name="ignitewoo_wsp_register_page" style="width:250px"> 
							<?php 
							if ( empty( $page_data ) ) { 
								_e( 'You have not created any pages yet', 'ignitewoo_wholesale_pro' );
							} else { 
								foreach( $page_data as $page_id => $name ) {
								?>
								<option value="<?php echo $page_id ?>" <?php selected( $settings['ignitewoo_wsp_register_page'], $page_id, true ) ?>><?php echo $name ?></option>
								<?php
								}
							}
							?>
						</select>
					</td>
				</tr>
				<tr valign="top">
					<th class="titledesc" scope="row">
						<label for="roles"><?php _e( 'Default Country', 'ignitewoo_wholesale_pro' )?>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'The default country when your form contains the country field', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp ign_roles">
						<select class="chosen" name="ignitewoo_wsp_register_default_country">
							<?php
							$countries = WC()->countries->get_countries();
							foreach( $countries as $k => $v ) {
								?>
								<option value="<?php echo $k ?>" <?php selected( $settings['ignitewoo_wsp_register_default_country'], $k, true ) ?>>
									<?php echo $v ?>
								</option>
								<?php
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<th class="titledesc" scope="row">
						<label for="roles"><?php _e( 'Admin Registration Notification', 'ignitewoo_wholesale_pro' )?>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'When a new user registers send a notification to these email addresses. Enter one or more email addresses separated by a comma. If you leave this empty the notice will be sent to the site administrator.', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp">
						<input type="text" name="ignitewoo_wsp_new_registration_to" value="<?php echo isset(  $settings['ignitewoo_wsp_new_registration_to'] ) ? $settings['ignitewoo_wsp_new_registration_to'] : '' ?>" style="width:250px">
					</td>
				</tr>
				
				<tr>
					<th class="titledesc" scope="row">
						<label for="roles"><?php _e( 'Google reCaptch Keys', 'ignitewoo_wholesale_pro' )?>
							<span class="woocommerce-help-tip" data-tip="<?php _e( 'If you intend to use the registration form and include an anti-spammer captcha using Google reCaptcha, paste in your site key and secret key. The captcha will not work without the keys.', 'ignitewoo_wholesale_pro' )?>"></span>
						</label>
					</th>
					<td class="forminp">
						<div style="padding-top: 7px">
							<?php _e( 'Site key', 'ignitewoo_wholesale_pro' )?><br/>
							<input type="text" name="ignitewoo_wsp_recaptcha_site_key" value="<?php echo isset(  $settings['ignitewoo_wsp_recaptcha_site_key'] ) ? $settings['ignitewoo_wsp_recaptcha_site_key'] : '' ?>" style="width:250px">
						</div>
						<div style="padding-top: 7px">
						<?php _e( 'Secret key', 'ignitewoo_wholesale_pro' )?><br/>
							<input type="text" name="ignitewoo_wsp_recaptcha_secret_key" value="<?php echo isset(  $settings['ignitewoo_wsp_recaptcha_secret_key'] ) ? $settings['ignitewoo_wsp_recaptcha_secret_key'] : '' ?>" style="width:250px">
						</div>
						<div style="padding-top: 7px">
							<?php echo sprintf( __( 'Get your keys from the <a href="%s" target="blank">Google reCaptcha</a>', 'ignitewoo_wholesale_pro' ), 'https://www.google.com/recaptcha/' ) ?>
						</div>
					</td>
				</tr>
				
			</table>
			<p style="font-weight:bold;font-size:1em;padding-top:1em;border-top:1px solid #ccc"><?php _e( 'Registration Form Designer', 'ignitewoo_wholesale_pro' )?></p>
			
			<table class="form-table">
				<tr valign="top">
					<td class="forminp">
						<script>
						ign_show_fields = false;
						
						jQuery( document ).ready( function( $ ) {
							$( "#ign_form_layout" ).sortable({
								handle: '.move',
							});

							$( '#ign_form_fields li' ).on( 'click', function() { 
								var item = $( this ).clone();
								var html = item.html().replace(/field_select_end/g, 'select').replace(/field_select/g, 'select').replace('field_option_end', 'option').replace(/field_option/g, 'option').replace(/<field/g, '<input');

								item.html( html );
								item.find( '.title_display' ).hide();
								item.find( '.field_type' ).show();
								item.find( 'input[type="text"], .ign_form_controls' ).css( 'display', 'inline' );
								item.find( '.ign_form_field_data' ).css( 'width', '91%' );
								var len = $( '#ign_form_layout li' ).length;
								item.find( 'input.required' ).attr( 'name', 'ign_form_field[required][' + len + ']' );
								
								var type = item.find( '.ign_form_field_type' ).val();
								
								if ( 'checkbox' == type || 'text' == type || 'textarea' == type || 'radio' == type ) { 
									var rand = Math.floor(Math.random() * ( 999999 - 10 + 1 ) + 1 );
									var name = item.find( '.ign_form_field_name' ).val();
									
									// Swap out radio option field names if any exist
									var opts = item.find( '.ign_form_radio_option' );
									if ( 'undefined' !== opts && opts.length > 0 ) { 
										$( opts ).each( function() { 
											var opt_name = $( this ).attr( 'name' );
											opt_name = opt_name.replace( name, name + rand.toString() );
											$( this ).attr( 'name', opt_name + rand.toString() );
										});
									}
									
									item.find( '.ign_form_field_name' ).val( name + rand.toString() );
								}
								
								item.find( '.ign_field_alignment, .ign_field_radio' ).show();
								
								$( '#ign_form_layout' ).append( item );
								
								$( '.wsp_field_added' ).show();
								ign_form_remove_field_hook();
								setTimeout( function() { 
									$( '.wsp_field_added' ).fadeOut( 500 );
								}, 2000 );
								hook_radio_options();
								
								$( '.ign_field_alignment select' ).unbind().on( 'change', function() { 
									$( this ).blur();
								});
							});
							
								
							function hook_radio_options() { 
								$( '.ign_form_add_option' ).unbind().on( 'click', function(e) { 
									var opt = $( this ).closest( 'div' ).clone();
									opt.find( '.ign_form_radio_option' ).val( '' );
									$( this ).closest( '.ign_field_radio' ).append( opt );
									hook_radio_options();
								});
								$( '.ign_form_remove_option' ).unbind().on( 'click', function(e) { 
									var opt = $( this ).closest( 'div' ).remove();		
									hook_radio_options();
								});
							}
							
							function ign_form_remove_field_hook() { 
								$( '#ign_form_layout .remove' ).unbind( 'click' ).on( 'click', function() {
									$( this ).closest( 'li' ).animate( { 'opacity' : 0.25, 'left' : "-=450" }, 250, function() { 
										$( this ).remove();
									});
								});
							}
							
							function ign_form_show_fields() { 
								var i = 0;
								$( '#ign_form_layout li' ).each( function() { 
									
									var html = $( this ).html().replace(/field_select_end/g, 'select').replace(/field_select/g, 'select').replace('field_option_end', 'option').replace(/field_option/g, 'option').replace(/<field/g, '<input');

									$( this ).html( html );
									$( this ).find( '.title_display' ).hide();
									$( this ).find( '.field_type' ).show();
									$( this ).find( 'input[type="text"], .ign_form_controls' ).css( 'display', 'inline' );
									$( this ).find( 'input.required' ).attr( 'name', 'ign_form_field[required][' + i + ']' );
									$( this ).find( '.ign_form_field_data' ).css( 'width', '91%' );
									$( this ).find( '.ign_field_alignment, .ign_field_radio' ).show();
									ign_form_remove_field_hook();
									
									$( '.ign_field_alignment select' ).unbind().on( 'change', function() { 
										$( this ).blur();
									});
									
									i++;
								});
								hook_radio_options();
							}
							
							if ( true == ign_show_fields )
								ign_form_show_fields();
							
							
							hook_radio_options();
							
						} );
						</script>
						<?php 
						$preset_fields = array(
							'billing_first_name' => __( 'Billing First Name', 'ignitewoo_wholesale_pro' ),
							'billing_last_name' => __( 'Billing Last Name', 'ignitewoo_wholesale_pro' ),
							'billing_company' => __( 'Billing Company', 'ignitewoo_wholesale_pro' ),
							'billing_address_1' => __( 'Billing Address', 'ignitewoo_wholesale_pro' ),
							'billing_address_2' => __( 'Billing Address 2', 'ignitewoo_wholesale_pro' ),
							'billing_city' => __( 'Billing City', 'ignitewoo_wholesale_pro' ),
							'billing_state' => __( 'Billing State', 'ignitewoo_wholesale_pro' ),
							'billing_country' => __( 'Billing Country', 'ignitewoo_wholesale_pro' ),
							'billing_postcode' => __( 'Billing Postal Code', 'ignitewoo_wholesale_pro' ),
							'billing_email' => __( 'Billing Email', 'ignitewoo_wholesale_pro' ),
							'billing_phone' => __( 'Billing Phone', 'ignitewoo_wholesale_pro' ),
							'billing_tax_vat' => __( 'Tax ID or VAT ID', 'ignitewoo_wholesale_pro' ),
						);
						?>
						<div style="float:left;">
							<p><?php _e( 'Click a field to add it to the bottom of the form.', 'ignitewoo_wholesale_pro' )?><br/><?php _e( 'Drag and drop fields to rearrange their order', 'ignitewoo_wholesale_pro' )?></p>
							<ul id="ign_form_fields" class="connectedSortable">
								<?php foreach( $preset_fields as $name => $label ) { ?>
									<li class="ui-state-default">
										<?php echo $this->gen_reg_form_field( $label, $name ); ?>
									</li>
								<?php } ?>
								
								<li class="ui-state-default">
									<?php echo $this->gen_reg_form_field_custom( __( 'Single Line Text Input', 'ignitewoo_wholesale_pro' ), 'text' ); ?>
								</li>
								<li class="ui-state-default">
									<?php echo $this->gen_reg_form_field_custom( __( 'Multi-line Text Input', 'ignitewoo_wholesale_pro' ), 'textarea' ); ?>
								</li>
								<li class="ui-state-default">
									<?php echo $this->gen_reg_form_field_custom( __( 'Checkbox', 'ignitewoo_wholesale_pro' ), 'checkbox' ); ?>
								</li>
								<li class="ui-state-default">
									<?php echo $this->gen_reg_form_field_custom( __( 'Radio Buttons', 'ignitewoo_wholesale_pro' ), 'radio' ); ?>
								</li>
								<li class="ui-state-default">
									<?php echo $this->gen_reg_form_field_custom( __( 'Password', 'ignitewoo_wholesale_pro' ), 'password' ); ?>
								</li>
								<li class="ui-state-default">
									<?php echo $this->gen_reg_form_field_custom( __( 'Google reCaptcha', 'ignitewoo_wholesale_pro' ), 'captcha' ); ?>
								</li>
								<li class="ui-state-default">
									<?php echo $this->gen_reg_form_field_custom( __( 'Submit Button', 'ignitewoo_wholesale_pro' ), 'button' ); ?>
								</li>
							</ul>
							<div class="wsp_field_added" style="height:20px;padding:5px;text-align:center;background-color:#E0FCE1;color:#333;display:none;margin-right:10px;margin-top:5px;border:1px solid #ccc">
								<?php _e( 'Field added', 'ignitewoo_wholesale_pro' ) ?>
							</div>
						</div>
						<div style="float:left;width:400px">

							<p><?php _e( 'Form Layout', 'ignitewoo_wholesale_pro' )?></p>
							<p><?php _e( 'When aligning fields left and right, always put the left field first.', 'ignitewoo_wholesale_pro' )?></p>
							<ul id="ign_form_layout" class="connectedSortable">
								<?php 
								if ( !empty( $this->settings['reg_form_fields'] ) ) { 
									for( $i = 0; $i < count( $this->settings['reg_form_fields']['title'] ); $i++ ) { 
										if ( 'billing_' == substr( $this->settings['reg_form_fields']['field_name'][ $i ], 0, 8 ) ) {
											$title = $this->settings['reg_form_fields']['title'][ $i ];
											$name = $this->settings['reg_form_fields']['field_name'][ $i ];
											$required = $this->settings['reg_form_fields']['required'][ $i ];
											
											$align = isset( $this->settings['reg_form_fields']['field_align'][ $i ] ) ? $this->settings['reg_form_fields']['field_align'][ $i ] : 'form-row-wide';
											
											$field = $this->gen_reg_form_field( $title, $name, $required, $align );
											echo '<li class="ui-state-default">' . $field . '</li>';
										} else { 
										
											$title = $this->settings['reg_form_fields']['title'][ $i ];
											$type = $this->settings['reg_form_fields']['field_type'][ $i ];
											
											$field_name = $this->settings['reg_form_fields']['field_name'][ $i ];

											$align = isset( $this->settings['reg_form_fields']['field_align'][ $i ] ) ? $this->settings['reg_form_fields']['field_align'][ $i ] : 'form-row-wide';
											
											$required = $this->settings['reg_form_fields']['required'][ $i ];
											
											$opts = ( !empty( $this->settings['reg_form_fields']['field_type_radio_options'][ $this->settings['reg_form_fields']['field_name'][ $i ] ] ) ) ? $this->settings['reg_form_fields']['field_type_radio_options'][ $this->settings['reg_form_fields']['field_name'][ $i ] ] : array();
												
											
											$field = $this->gen_reg_form_field_custom( $title, $type, $required, $field_name, $title, $align, $opts );
											echo '<li class="ui-state-default">' . $field . '</li>';
										}
									}
								}
								?>
							</ul>
							<?php if ( !empty( $this->settings['reg_form_fields'] ) ) { ?>
								<script>
								var ign_show_fields = true;
								</script>
							<?php } ?>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php
	}

	function gen_reg_form_field( $title, $field_name, $required = false, $align = 'form-row-wide' ) { 
		ob_start();
		?>
		<div class="ign_form_field_wrap">
			<div class="field_type">
				<div class="form_field_title" style="width:84%;display:inline-block">
					<?php echo $title ?>
				</div>
				<div class="ign_form_actions" style="display:inline-block">
					<span class="move dashicons dashicons-menu"></span>
					<span class="remove dashicons dashicons-dismiss"></span>
				</div>
			</div>
			<div class="ign_form_field_data">
				<span class="title_display"><?php echo $title ?></span>
				<field type="text" style="display:none;width:100%" value="<?php echo $title ?>" name="ign_form_field[title][]" />
				<field type="hidden" value="<?php echo $field_name ?>" name="ign_form_field[field_name][]" />
				<field type="hidden" value="text" name="ign_form_field[field_type][]" />
			</div>
			<div class="ign_form_controls">
				<field class="required" type="checkbox" value="yes" name="ign_form_field[required][]" title="<?php _e( 'Required', 'ignitewoo_wholesale_pro' ) ?>" <?php checked( $required, 'yes', true ) ?>/>
			</div>
			<div class="ign_field_alignment" style="width:91%;margin-top:10px;clear:both;display:none">
				<?php _e( 'Field alignment', 'ignitewoo_wholesale_pro' )?>
				<br/>
				<field_select select name="ign_form_field[field_align][]" style="width:100%;margin:10px 0">
					<field_option value="form-row-wide" <?php selected( $align, "form-row-wide", true ) ?>><?php _e( 'Full width', 'ignitewoo_wholesale_pro' )?></field_option_end>
					<field_option value="form-row-first" <?php selected( $align, "form-row-first", true ) ?>><?php _e( 'Align left', 'ignitewoo_wholesale_pro' )?></field_option_end>
					<field_option value="form-row-last" <?php selected( $align, "form-row-last", true ) ?>><?php _e( 'Align right', 'ignitewoo_wholesale_pro' )?></field_option_end>
				</select>
			</div>
		</div>
		<?php 
		return ob_get_clean();
	}
	
	function gen_reg_form_field_custom( $title, $type, $required = false, $field_name = '', $field_title = '', $align = 'form-row-full', $opts = array() ) { 
		if ( empty( $field_name ) ) 
			$field_name = $type . '_' . uniqid();
		ob_start();
		
		?>
		<div class="ign_form_field_wrap">
			<div class="field_type">
				<div class="form_field_title" style="width:84%;display:inline-block">
					<?php echo ucwords( $type ) ?>
				</div>
				<div class="ign_form_actions" style="display:inline-block">
					<span class="move dashicons dashicons-menu"></span>
					<span class="remove dashicons dashicons-dismiss"></span>
				</div>
			</div>
			<div class="ign_form_field_data">
				<span class="title_display"><?php echo $title ?></span>
				<field type="text" style="display:none;width:100%" value="<?php echo $field_title ?>" placeholder="<?php _e( 'Enter a label...', 'ignitewoo_wholesale_pro' )?>"  name="ign_form_field[title][]" />
				<field class="ign_form_field_name" type="hidden" value="<?php echo $field_name ?>" name="ign_form_field[field_name][]" />
				<field class="ign_form_field_type" type="hidden" value="<?php echo $type ?>" name="ign_form_field[field_type][]"/>
			</div>
			<?php if ( 'captcha' == $type ) { ?>
				<div class="ign_form_controls">
					<field class="required" type="checkbox" value="yes" name="ign_form_field[required][]" title="<?php _e( 'Required', 'ignitewoo_wholesale_pro' ) ?>" <?php checked( $required, 'yes', true ) ?>  <?php if ( 'button' == $type || 'captcha' == $type ) echo 'style="display:none"' ; ?> />
				</div>
			<?php } else { ?>
				<div class="ign_form_controls">
					<field class="required" type="checkbox" value="yes" name="ign_form_field[required][]" title="<?php _e( 'Required', 'ignitewoo_wholesale_pro' ) ?>" <?php checked( $required, 'yes', true ) ?>  <?php if ( 'button' == $type ) echo 'style="display:none"' ; ?> />
				</div>
			<?php } ?>
			<div class="ign_field_alignment" style="width:91%;margin-top:10px;clear:both;display:none">
				<?php _e( 'Field alignment', 'ignitewoo_wholesale_pro' )?>
				<br/>
				<field_select select name="ign_form_field[field_align][]" style="width:100%;margin:10px 0">
					<field_option value="form-row-wide" <?php selected( $align, "form-row-wide", true ) ?>><?php _e( 'Full width', 'ignitewoo_wholesale_pro' )?></field_option_end>
					<field_option value="form-row-first" <?php selected( $align, "form-row-first", true ) ?>><?php _e( 'Align left', 'ignitewoo_wholesale_pro' )?></field_option_end>
					<field_option value="form-row-last" <?php selected( $align, "form-row-last", true ) ?>><?php _e( 'Align right', 'ignitewoo_wholesale_pro' )?></field_option_end>
				</field_select_end>
			</div>	
			<?php if ( 'radio' == $type ) { ?>
				
				<div class="ign_field_radio" style="width:80%;margin-top:10px;clear:both;display:none">
					<?php _e( 'Options', 'ignitewoo_wholesale_pro' )?>
					<br/>
					<?php 
					// make sure that if the array has no elements add an empty one so that the loop
					// runs to show the first empty option field
					if ( empty( $opts ) )
						$opts = array( '' );
					?>
					<?php foreach( $opts as $val ) { ?>
						<div style="margin-top:8px"> 
							<field type="text" class="ign_form_radio_option" value="<?php echo $val ?>" name="ign_form_field[field_type_radio_options][<?php echo $field_name ?>][]" style="width:78%;"/>
							<span class="dashicons dashicons-plus ign_form_add_option" style="color:#46B450;padding-top:9px" title=" <?php _e( 'Add option', 'ignitewoo_wholesale_pro' )?> "></span> &nbsp; <span class="dashicons dashicons-minus ign_form_remove_option" style="color:#bc0000;font-weight:bold;padding-top:9px" title=" <?php _e( 'Remove option', 'ignitewoo_wholesale_pro' )?> "></span>
						</div>	
					<?php } ?>
				</div>
				
			<?php } ?>
		</div>
		<?php 
		
		return ob_get_clean();
	}
	
	function role_settings() { 
		global $wpdb, $wp_roles, $woocommerce;

		if ( !isset( $wp_roles->roles ) )
			return;
			
		$settings = $this->settings;
		
		$defaults = array(
			'show_regular_price' => '',
			'show_savings' => '',
			'show_regular_price_label' => __( 'Regularly', 'ignitewoo_wholesale_pro' ),
			'show_savings_label' => __( 'You Save', 'ignitewoo_wholesale_pro' ),
		);
		
		$settings = wp_parse_args( $settings, $defaults );
		
		$rate_types = array();
		
		$rates = $wpdb->get_results( "SELECT * FROM `{$wpdb->prefix}woocommerce_tax_rates` order by tax_rate_country" );
		
		if ( !empty( $rates ) ) {
			foreach( $rates as $r ) {
				$label = array();
				if ( !empty( $r->tax_rate_country ) )
				$label[] = $r->tax_rate_country;
				if ( !empty( $r->tax_rate_state ) )
					$label[] = $r->tax_rate_state;
				if ( !empty( $r->tax_rate_name ) )
					$label[] = $r->tax_rate_name;
				if ( !empty( $r->tax_rate ) )
					$label[] = $r->tax_rate;
				$label[] = !empty( $r->tax_rate_class ) ? $r->tax_rate_class : __( 'Standard Rates', 'ignitewoo_wholesale_pro' );
				$label = implode( ' - ', $label );
				$rate_types[ $r->tax_rate_id ] = $label;
			}
		}
		
		?>
		<script>
		jQuery( document ).ready( function( $ ) { 
			$( '.roles_table .select2' ).select2();
			
			$( "#ign_add_new_role" ).click( function() { 
			
				var uid = null;
				
				$.post( ajaxurl, { action:'ign_get_uniqid' }, function( data ) { 
				
					try { 
						var j = $.parseJSON( data );
						
						uid = j.id;
						
					} catch( err ) { 
				
						alert( '<?php _e( 'An error occurred. Try again.', 'ignitewoo_wholesale_pro' )?>');
						return false;
				
					}
				
					var html = '\
						<tr>\
						<td>\
							<label class="">\
							<input type="text" name="ignite_level_name[ignite_level_' + uid + ']" placeholder="<?php _e( 'Enter a role name','ignitewoo_wholesale_pro' )?>" value="">\
							</label>\
						</td>\
						<td>\
						</td>\
						</tr>\
					';
					
					$( '.roles_table' ).append( html );
					
					return false;
				
				});

			})
		})
		</script>
		
		<style>
			.help_tip.tiered { width: 16px; float: none !important; }
			.roles_table thead th { padding: 5px 10px; background-color: #f9f9f9; }
			.roles_table tbody td { padding: 10px }
		</style>
		
		<tr valign="top">
			<th class="titledesc" scope="row">
				<label for="roles">
				<?php _e( 'Pricing Roles', 'ignitewoo_wholesale_pro' )?>
				</label>
			</th>
			<td class="forminp ign_roles">	
			<td>
		</tr>
		</tbody>
		</table>
		<table class="roles_table widefat">
			<thead>
			<tr>
				<th style="width:70px">
					<strong><?php _e( 'Delete', 'ignitewoo_wholesale_pro' ) ?></strong>
					
					<span class="dashicons dashicon-editor-help woocommerce-help-tip" style="float:none" data-tip="<?php _e( 'When you delete a role then any users with that role will have their role changed to customer', 'ignitewoo_wholesale_pro' )?>"></span>
					
				</th>
				
				<th style="width:200px"><strong><?php _e( 'Role Name', 'ignitewoo_wholesale_pro' ) ?></strong></th>
				
				<th style="width:200px">
					<strong><?php _e( 'Disable Taxes', 'ignitewoo_wholesale_pro' ) ?></strong>
					<span class="dashicons dashicon-editor-help woocommerce-help-tip" style="float:none" data-tip="<?php _e( 'Disable taxes for shoppers with this role', 'ignitewoo_wholesale_pro' )?>">
				</th>
			
				<th>
					<strong><?php _e( 'Shipping', 'ignitewoo_wholesale_pro' ) ?></strong>
					<span class="dashicons dashicon-editor-help woocommerce-help-tip" style="float:none" data-tip="<?php _e( 'Optionally restrict shoppers with this role to specific shipping methods', 'ignitewoo_wholesale_pro' )?>"></span>
				</th>

				<th>
					<strong><?php _e( 'Payment', 'ignitewoo_wholesale_pro' ) ?></strong>
					<span class="dashicons dashicon-editor-help woocommerce-help-tip" style="float:none" data-tip="<?php _e( 'Optionally restrict shoppers with this role to specific payment gateways', 'ignitewoo_wholesale_pro' )?>"></span>
				</th>
				
				<th style="width:100px">
					<strong><?php _e( 'Backorders', 'ignitewoo_wholesale_pro' ) ?></strong>
					<span class="dashicons dashicon-editor-help woocommerce-help-tip" style="float:none" data-tip="<?php _e( 'Allow shoppers with this role to backorder an item when the item is not in stock', 'ignitewoo_wholesale_pro' )?>">
				</th>
			</tr>
			</thead>
			<tbody>
			<?php

			asort( $wp_roles->roles );
			
			$the_roles = $wp_roles->roles; 

			$i = 0;
			foreach( $the_roles as $role => $data ) { 

				if ( 'ignite_level_' != substr( $role, 0, 13 ) )
					continue;
					
				if ( 0 == $i % 2 )
					$alt = '';
				else 
					$alt = 'alt';
				
				$i++;
				
				?>
			<tr class="<?php echo $alt ?>">
				<td style="vertical-align:top;width:50px">
					<input type="checkbox" value="<?php echo $role ?>" style="" id="ignite_level_<?php echo $role ?>" name="ignite_level_delete[<?php echo $role ?>]" class="input-text wide-input "> 
					<?php // placeholder so a list of all unique roles are sent via POST for processing payment gateways and shipping methods ?>
					<input type="hidden" name="ign_role[<?php echo $role ?>]" value="x">
				</td>
				<td>
					<label class="">
						<span><?php echo stripslashes( $data['name'] ) ?></span> 
						<br/>
						<span class="dashicons dashicon-editor-help woocommerce-help-tip" style="float:none" data-tip="<?php _e( 'Meta key for importing prices with your CSV import tool of choice', 'ignitewoo_wholesale_pro' )?>"></span>
						<small><?php _e( 'Meta key', 'ignitewoo_wholesale_pro' ) ?>: <br/> <?php echo '_' . $role . '_price' ?></small>
					</label>
				</td>
				<td style="vertical-align:top">
					
					<select class="" style="width:150px" name="ignite_level_tax[<?php echo $role ?>]">
					
						<?php if ( !isset( $data['capabilities']['no_tax'] ) || true !== $data['capabilities']['no_tax'] ) $selected = 'selected="selected"'; else $selected = ''; ?>
						<option value="yes" <?php echo $selected ?>><?php _e( 'Taxable', 'ignitewoo_wholesale_pro' ) ?></option>
						
						<?php if ( isset( $data['capabilities']['no_tax'] ) &&  true === $data['capabilities']['no_tax'] ) $selected = 'selected="selected"'; else $selected = ''; ?>
						<option value="no" <?php echo $selected ?>><?php _e( 'Non-taxable', 'ignitewoo_wholesale_pro' ) ?></option>
						
					</select>
					
					<br/><span class="dashicons dashicon-editor-help woocommerce-help-tip" style="float:none" data-tip="<?php _e( 'Optionally exempt users from these tax types. Only applicable if the user is tax exempt. When no rates are selected then the shopper is exempt from all rates, otherwise the shopper is only exempt from the selected rates', 'ignitewoo_wholesale_pro' )?>"></span> <?php _e( 'Exempt from these tax types', 'ignitewoo_wholesale_pro' ) ?>
					
					<?php 
						$tax_classes = get_option( $role . '_tax_exemptions', '' ); 
						$tax_classes = explode( ',' , $tax_classes );
					?>
					<select class="select2" multiple="multiple" style="width:250px" name="ignite_level_tax_exemption[<?php echo $role ?>]">
						
						<?php foreach( $rate_types as $id => $label ) { ?>
							<?php 
							
							if ( in_array( $id, $tax_classes ) ) 
								$selected = 'selected="selected"';
							else 
								$selected = ''; 
							?>
							<option value="<?php echo $id ?>" <?php echo $selected ?>><?php echo $label ?></option>
						<?php } ?>
						
					</select>
					
				</td>
				<td>
					<?php 
					$methods = get_option( $role . '_shipping_methods', array() );
					
					$new_way = false; 

					foreach( $methods as $r ) { 
						if ( false !== strpos( $r, ':' ) )
							$new_way = true; 
					}

					?>
					
					<select class="select2" multiple="multiple" style="width:250px" name="ignite_level_shipping_methods[<?php echo $role ?>][]">
					
					<?php 
						
					$zones = array();
					
					if ( class_exists( 'WC_Shipping_Zone' ) ) {
					
						// Rest of the World zone
						$zone = new WC_Shipping_Zone(0);
						$zones[ $zone->get_id() ] = $zone->get_data();
						$zones[ $zone->get_id() ]['zone_name'] = __( 'Rest of the World', 'ignitewoo_wholesale_pro' );
						$zones[ $zone->get_id() ]['formatted_zone_location'] = $zone->get_formatted_location();
						$zones[ $zone->get_id() ]['shipping_methods']        = $zone->get_shipping_methods();

						// Add user configured zones
						$zones = array_merge( $zones, WC_Shipping_Zones::get_zones() );
					}

					/*
					if ( version_compare( WOOCOMMERCE_VERSION, '2.6', '>=' )) { 
						$zones = WC_Shipping_Zones::get_zones();
						$zero_zone = new WC_Shipping_Zone(0); 
						
					}
					*/
					
					if ( !empty( $zones ) ) { 
						$zone_methods = array();

						//$zero_zone_methods = $zero_zone->get_shipping_methods();
						/*
						if ( !empty( $zero_zone_methods ) ) { 
							foreach( $zero_zone_methods as $method => $data ) { 
								$methods_in_zone = array(
									'zone_id' => 0,
									'zone_name' => __( 'Rest of the World', 'ignitewoo_wholesale_pro' ),
									'method_instance_id' => $method,
									'method_id' => $data->id, //identifying string
									'title' => $data->title
								);
								$zone_methods[ 0 ][] = $methods_in_zone;
							}
						}
						*/
						
						$i = 0;
						
						foreach( $zones as $zone => $data ) { 
							
							$i++;
							
							foreach( $data['shipping_methods'] as $key => $vals ) { 	
								$methods_in_zone = array(
									//'zone_id' => $data['zone_id'],
									'zone_name' => $data['zone_name'],
									'method_instance_id' => $key,
									'method_id' => $vals->id, //identifying string
									'title' => $vals->title
								);
								$zone_methods[ $i ][] = $methods_in_zone;
							}
						}

						// The new way, as of 2.2.6, uses method_id and instance ID for keys.
						// Only necessary when zones are used. 
						// Convert the setting values.
						foreach( $zone_methods as $zm_id => $zm ) { 
							foreach( $zm as $key => $vals ) { 
							
								// If we've already converted to the new way via the admin saving the settings
								// then check if selected for method_id:method_instance_id
								// otherwise check if selected via only method_instance_id
								if ( !$new_way ) {
									if ( in_array( $vals['method_id'], $methods ) ) 
										$selected = 'selected="selected"';
									else 
										$selected = ''; 
								} else { 
									if ( in_array( $vals['method_id'] . ':' . $vals['method_instance_id'], $methods ) ) 
										$selected = 'selected="selected"';
									else 
										$selected = ''; 
								}
								$display_title = $vals['zone_name'] . ' - ' . $vals['title'];
								?>
								<option value="<?php echo $vals['method_id'] . ':' . $vals['method_instance_id'] ?>" <?php echo $selected ?>><?php echo $display_title ?></option>
							<?php 
							}
						}
						
					} else { 
					
						$shipping_methods = WC()->shipping->load_shipping_methods();

						?>
						<?php foreach( $shipping_methods as $key => $vals ) { ?>
							<?php 
							
							if ( in_array( $vals->id, $methods ) ) 
								$selected = 'selected="selected"';
							else 
								$selected = ''; 
							?>
							<option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo $vals->method_title ?></option>
						<?php } ?>
					<?php } ?>
					</select>
				</td>
				
				<td>
					<?php 
					$payment_gateways = WC()->payment_gateways();
					$role_gateways = get_option( $role . '_payment_gateways', array() );
					//$role_gateways = explode( ',', $role_gateways );
					?>
					
					<select class="select2" multiple="multiple" style="width:250px" name="ignite_level_payment_gateways[<?php echo $role ?>][]">

					<?php foreach( $payment_gateways->payment_gateways as $key => $vals ) { ?>
						<?php 
						if ( in_array( $vals->id, $role_gateways ) ) 
							$selected = 'selected="selected"';
						else 
							$selected = ''; 
						?>
						<option value="<?php echo $vals->id ?>" <?php echo $selected ?>><?php echo $vals->title ?></option>
					<?php } ?>
					</select>
				</td>
				
				<td style="vertical-align:top">
					<input type="checkbox" value="yes" style="" id="ignite_level_<?php echo $role ?>" name="ignite_level_backorders[<?php echo $role ?>]" class="input-text wide-input" <?php isset( $data['capabilities']['backorders'] ) ? checked( $data['capabilities']['backorders'], true, true ) : '' ?> > 
				</td>

			</tr>
			<?php } ?>
			</tbody>
		</table>
			
		<p><button type="button" class="button" id="ign_add_new_role"><?php _e( 'Add New Role', 'ignitewoo_wholesale_pro' )?></button></p>
		<?php 
	}
	
	function init_minmax_fields() {
		$this->form_fields = array(
			'general_settings' => 
				array(
					'title' 	=> __( '<span style="font-size: 1.05em;font-weight:bold">General</span>', 'ignitewoo_wholesale_pro'),
					'type' 		=> 'title',
					'description'	=> __( 'You can create and enable minimum/maximum rules for shoppers that have any of your custom', 'ignitewoo_wholesale_pro' ),
				),
			'enable_min_max' => 
				array(
					'title' 	=> __( 'Enable', 'ignitewoo_wholesale_pro'),
					'type' 		=> 'checkbox',
					'default' 	=> 'yes',
					'description'		=> '',
					'label'		=> __( 'Enable the rules', 'ignitewoo_wholesale_pro'),
					
				),
			'cart_notices' => 
				array(
					'title' 	=> __( '<span style="font-size: 1.05em;font-weight:bold">Cart Contents Messages</span>', 'ignitewoo_wholesale_pro'),
					'type' 		=> 'title',
					'default' 	=> 'yes',
					'description'	=> __( 'Messages that pertain to the contents of the cart as a whole. These relate to the rules that you establish below.', 'ignitewoo_wholesale_pro' ),
				),
			'min_cart_total_error_message' => 
				array(
					'title' 	=> __( 'Min Cart Total Message', 'ignitewoo_wholesale_pro'),
					'type' 		=> 'text',
					'default' 	=> __( 'Your cart subtotal must be at least {amount} to place your order.', 'ignitewoo_wholesale_pro' ),
					'description'	=> __( 'DEFAULT MESSAGE: Your cart subtotal must be at least {amount} to place your order.', 'ignitewoo_wholesale_pro' ),
					'desc_tip'	=> true,
					'css'		=> 'width: 55%',
				),
			'max_cart_total_error_message' => 
				array(
					'title' 	=> __( 'Max Cart Total Message', 'ignitewoo_wholesale_pro'),
					'type' 		=> 'text',
					'default' 	=> __( 'Your cart subtotal cannot be more than {amount} to place your order.', 'ignitewoo_wholesale_pro' ),
					'description'	=> __( 'DEFAULT MESSAGE: Your cart subtotal cannot be more than {amount} to place your order.', 'ignitewoo_wholesale_pro' ),
					'desc_tip'	=> true,
					'css'		=> 'width: 55%',
				),
			'min_cart_qty_error_message' => 
				array(
					'title' 	=> __( 'Min Cart Quantity Message', 'ignitewoo_wholesale_pro'),
					'type' 		=> 'text',
					'default' 	=> __( 'You must purchase at least {quantity} items to place your order.', 'ignitewoo_wholesale_pro' ),
					'description'	=> __( 'DEFAULT MESSAGE: You must purchase at least {quantity} items to place your order.', 'ignitewoo_wholesale_pro' ),
					'desc_tip'	=> true,
					'css'		=> 'width: 55%',
				),
			'min_product_qty_error_message' => 
				array(
					'title' 	=> __( 'Min Product Qty Message', 'ignitewoo_wholesale_pro'),
					'type' 		=> 'text',
					'default' 	=> __( 'You must purchase at least {quantity} of each individual product to place your order.', 'ignitewoo_wholesale_pro' ),
					'description'	=> __( 'DEFAULT MESSAGE: You must purchase at least {quantity} of each individual product to place your order.', 'ignitewoo_wholesale_pro' ),
					'desc_tip'	=> true,
					'css'		=> 'width: 55%',
				),
			'product_notices' => 
				array(
					'title' 	=> __( '<span style="font-size: 1.05em;font-weight:bold">Product Quantity Messages</span>', 'ignitewoo_wholesale_pro'),
					'type' 		=> 'title',
					'description'	=> __( 'Messages that pertain to the quantities of specific products in the cart. The relate to any min / max / increment rules you create in individual products.', 'ignitewoo_wholesale_pro' ),
				),
			'min_product_qty_required_message' => 
				array(
					'title' 	=> __( 'Min Product Quantity Required', 'ignitewoo_wholesale_pro'),
					'type' 		=> 'text',
					'default' 	=> __( 'The minimum allowed quantity for {product} is {quantity} - please increase the quantity in your cart.', 'ignitewoo_wholesale_pro' ),
					'description'	=> __( 'DEFAULT MESSAGE: The minimum allowed quantity for {product} is {quantity} - please increase the quantity in your cart.', 'ignitewoo_wholesale_pro' ),
					'desc_tip'	=> true,
					'css'		=> 'width: 55%',
				),
			'max_product_qty_allowed_message' => 
				array(
					'title' 	=> __( 'Max Product Quantity Allowed', 'ignitewoo_wholesale_pro'),
					'type' 		=> 'text',
					'default' 	=> __( 'The maximum allowed quantity for {product} is {quantity} - please decrease the quantity in your cart.', 'ignitewoo_wholesale_pro' ),
					'description'	=> __( 'DEFAULT MESSAGE: The maximum allowed quantity for {product} is {quantity} - please decrease the quantity in your cart.', 'ignitewoo_wholesale_pro' ),
					'desc_tip'	=> true,
					'css'		=> 'width: 55%',
				),
			'inc_product_qty_required_message' => 
				array(
					'title' 	=> __( 'Product Increments Required', 'ignitewoo_wholesale_pro'),
					'type' 		=> 'text',
					'default' 	=> __( '{product} must be bought in increments of {quantity}. Please add another {difference} to continue.', 'ignitewoo_wholesale_pro' ),
					'description'	=> __( 'DEFAULT MESSAGE: {product} must be bought in increments of {quantity}. Please add another {difference} to continue.', 'ignitewoo_wholesale_pro' ),
					'desc_tip'	=> true,
					'css'		=> 'width: 55%',
				),
			'order_notices' => 
				array(
					'title' 	=> __( '<span style="font-size: 1.05em;font-weight:bold">Minimum Order Count Messages</span>', 'ignitewoo_wholesale_pro'),
					'type' 		=> 'title',
					'description'	=> __( 'Messages that pertain to the minimum number of orders required.', 'ignitewoo_wholesale_pro' ),
				),
			'min_order_qty_required_message' => 
				array(
					'title' 	=> __( 'Min Order Quantity Required', 'ignitewoo_wholesale_pro'),
					'type' 		=> 'text',
					'default' 	=> __( 'These limitations are effective until you have at least {quantity} paid order(s)', 'ignitewoo_wholesale_pro' ),
					'description'	=> __( 'DEFAULT MESSAGE: These limitations are effective until you have at least {quantity} paid order(s)', 'ignitewoo_wholesale_pro' ),
					'desc_tip'	=> true,
					'css'		=> 'width: 55%',
				),
			'rules' => 
				array(
					'title' 	=> __( '<span style="font-size: 1.2em;font-weight:bold">Cart Contents Rules</span>', 'ignitewoo_wholesale_pro'),
					'type' 		=> 'rules',
					'default' 	=> '',
					'description'	=> __( 'If you leave the roles and users setting both empty for a given rule then the rule is valid for all non-logged-in shoppers.', 'ignitewoo_wholesale_pro' ),
					'desc_tip' 	=> true,
					'label'		=> ''
				),
		);
	}
	
	function validate_rules_field( $key ) { 

		if ( empty( $_POST[ 'woocommerce_ignitewoo_wholesale_pro_suite_' . $key ] ) )
			return;
		
		// Unset the template row vals - index 0
		foreach( $_POST[ 'woocommerce_ignitewoo_wholesale_pro_suite_' . $key ] as $s => $v ) {
			if ( isset( $v[0] ) )
				unset( $_POST[ 'woocommerce_ignitewoo_wholesale_pro_suite_' . $key ][$s][0] );
		}

		return $_POST[ 'woocommerce_ignitewoo_wholesale_pro_suite_' . $key ];
	}
	
	function generate_rules_html( $key, $settings ) { 

		if ( !isset( $wp_roles ) )
		    $wp_roles = new WP_Roles();

		$all_roles = $wp_roles->roles;
		
		ob_start();

		?>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<?php echo $settings['title']; ?>
			</th>
			<td></td>
		</tr>
		
		</tr>
			<td colspan="2" class="forminp rules_area" id="<?php echo $this->id; ?>">
				<style>
					.rule_table td .short { width: 80px; }
					.rule_table tr.alt { background: #f7f7f7 !important; }
					.rule_table .ajax_chosen_select_customer { width: 200px }
					.min_max_users { width: 200px }
					.rule_table .help_tip { 
						color: #333 !important;
						font-size: 18px !important;
					}
					.woocommerce table.form-table .select2-container {
						min-width:250px !important;
					}
					.woocommerce table.form-table .select2-selection__clear {
						display:none;
					}
				</style>
				
				<div style="margin-bottom:0.5em"><?php _e( 'The first matching rule always applies. Drag and drop the rules to rearrange the order.', 'ignitewoo_wholesale_pro' ) ?></div>
				<table class="widefat rule_table">
					<thead>
						<th style="width:26px"></th>
						<th><?php _e( 'Roles', 'ignitewoo_wholesale_pro' ) ?></th>
						<th><?php _e( 'Users', 'ignitewoo_wholesale_pro' ) ?></th>
						
						<th><?php _e( 'Min Subtotal', 'ignitewoo_wholesale_pro' ) ?><span class="dashicons dashicons-editor-help help_tip" data-tip="<?php _e( 'Minimum order subtotal allowed. Make sure this is less than the maximum subtotal allowed,  or leave this empty for no limits', 'ignitewoo_wholesale_pro' ) ?>"></span></th>
						
						<th><?php _e( 'Max Subtotal', 'ignitewoo_wholesale_pro' ) ?><span class="dashicons dashicons-editor-help help_tip" data-tip="<?php _e( 'Maximum order subtotal allowed. Make sure this is greater than the minimum subtotal allowed, leave this empty for no limits.', 'ignitewoo_wholesale_pro' ) ?>"></span></th>
						
						<th><?php _e( 'Min Qty', 'ignitewoo_wholesale_pro' ) ?><span class="dashicons dashicons-editor-help help_tip" data-tip="<?php _e( 'Minimum total quantity of all products required in the order to complete the purchase. Variations are counted as independent products', 'ignitewoo_wholesale_pro' ) ?>"></span></th>
						
						<th><?php _e( 'Per Item Qty', 'ignitewoo_wholesale_pro' ) ?><span class="dashicons dashicons-editor-help help_tip" data-tip="<?php _e( 'Minimum quantity required for each individual product in the cart. Variations are counted as independent products', 'ignitewoo_wholesale_pro' ) ?>"></span></th>
						
						<th><?php _e( 'Orders', 'ignitewoo_wholesale_pro' ) ?><span class="dashicons dashicons-editor-help help_tip" data-tip="<?php _e( 'This rule applies to shoppers with less than this number of orders', 'ignitewoo_wholesale_pro' ) ?>"></span></th>
					</thead>
					<tbody>
						<tr class="template" style="display:none">
							<td>
								<input type="checkbox" class="remove" value="" name="remove">
								<input type="hidden" value="x" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[rule_index][]">
							</td>
							<td>
								<label class="qty_labels"><?php _e( 'Roles', 'ignitewoo_wholesale_pro' ) ?></label>
								<br/>
								<select name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[roles][][]" class="select2" multiple="multiple" style="width:50px">
								
									<option value=""><?php echo '&nbsp;' ?></option>
									
									<?php foreach( $all_roles as $role_id => $role ) { ?>
									
									<?php $role_checked = (isset( $condition['args']['roles'] ) && is_array( $condition['args']['roles'] ) && in_array( $role_id, $condition['args']['roles'] )) ? 'selected="selected"' : ''; ?>
						
									<option <?php echo $role_checked ?> value="<?php echo $role_id ?>">
										<?php echo $role['name']; ?>
									</option>
									
									<?php } ?>
								</select>
					
							</td>
							<td style="overflow:auto">
								<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) { ?>
								<div class="select select2 select_users">
									<label class="qty_labels"><?php _e( 'Users', 'ignitewoo_wholesale_pro' ) ?> </label>
									<select class="min_max_users" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[users][]" data-placeholder="<?php _e( 'Search for a user&hellip;', 'ignitewoo_wholesale_pro' ); ?>" data-selected='' value="" multiple="multiple" data-multiple="true" data-allow_clear="true" data-action="woocommerce_json_search_customers" style="min-height:32px; width:150px"/></select>
								</div>
								<?php } else { ?>
								<div class="select select2 select_users">
									<label class="qty_labels"><?php _e( 'Users', 'ignitewoo_wholesale_pro' ) ?> </label>
									<br/>
									<input type="hidden" class="min_max_users" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[users][]" data-placeholder="<?php _e( 'Search for a user&hellip;', 'ignitewoo_wholesale_pro' ); ?>" data-selected='' value="" data-multiple="true" data-allow_clear="true" data-action="woocommerce_json_search_customers" style="min-height:32px; width:150px"/>
								</div>
								<?php } ?>
							</td>
							<td>
								<input type="number" step="0.01" class="short" value="" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[cart_total][]">
							</td>
							<td>
								<input type="number" step="0.01" class="short" value="" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[cart_total_max][]">
							</td>
							<td>
								<input type="number" class="short" value="" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[cart_qty][]">
							</td>
							<td>
								<input type="number" class="short" value="" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[product_qty][]">
							</td>
							<td>
								<input type="number" class="short" value="" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[order_qty][]">
							</td>
						</tr>
						<?php 
						if ( isset( $this->settings['rules']['rule_index'] ) )
						foreach( $this->settings['rules']['rule_index'] as $i => $vals ) { ?>
						
							<?php 
							$x = $i;
							?>
						
						<tr>
							<td>
								<input type="checkbox" class="remove" value="" name="remove">
								<input type="hidden" value="x" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[rule_index][<?php echo $x ?>]">
							</td>
							<td>
								<label class="qty_labels"><?php _e( 'Roles', 'ignitewoo_wholesale_pro' ) ?></label>
								<br/>
								<select name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[roles][<?php echo $x ?>][]" class="chosen select2" multiple="multiple" style="width:100px">
								
									<option value=""><?php echo '&nbsp;' ?></option>
									
									<?php foreach( $all_roles as $role_id => $role ) { ?>
									
									<?php $role_checked = in_array( $role_id, $this->settings['rules']['roles'][$x] ) ? 'selected="selected"' : ''; ?>
						
									<option <?php echo $role_checked ?> value="<?php echo $role_id ?>">
										<?php echo $role['name']; ?>
									</option>
									
									<?php } ?>
								</select>
					
							</td>
							<td style="overflow:auto">
							<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) { ?>

								<div class="select select2 select_users">
									<label class="qty_labels"><?php _e( 'Users', 'ignitewoo_wholesale_pro' ) ?></label> 
									<br/>
									<select class="min_max_users" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[users][<?php echo $x ?>][]" data-placeholder="<?php _e( 'Search for a user&hellip;', 'ignitewoo_wholesale_pro' ); ?>" data-selected='<?php echo esc_attr( json_encode( $user_strings ) ) ?>' value="<?php echo $this->settings['rules']['users'][$x] ?>" data-multiple="true" data-allow_clear="true" data-action="woocommerce_json_search_customers" style="min-height:32px;width:150px" multiple="multiple"/>
										<?php 
										foreach( $this->settings['rules']['users'][$x] as $user_id ) { 
											$user = get_user_by( 'id', $user_id);
											if ( empty( $user ) || is_wp_error( $user ) )
												continue;
											
											?>
											<option value="<?php echo $user_id ?>" selected="selected">
												<?php echo esc_html( $user->display_name ) . ' (#' . absint( $user->ID ) . ' &ndash; ' . esc_html( $user->user_email ) . ')'; ?>
											</option>
											<?php 
										} ?>
									</select>
								</div>
								
								<?php } else { ?>

								<div class="select select2 select_users">
									<label class="qty_labels"><?php _e( 'Users', 'ignitewoo_wholesale_pro' ) ?></label> 
									<?php
									if ( !empty( $this->settings['rules']['users'][$x] ) && is_array( $this->settings['rules']['users'][$x] ) )
										$this->settings['rules']['users'][$x] = implode( ',', $this->settings['rules']['users'][$x] );
										
									$user_strings = array();
									$user_id = '';
									if ( isset( $this->settings['rules']['users'][$x] ) && !empty( $this->settings['rules']['users'][$x] ) ) {
										$rules_users = explode( ',' , $this->settings['rules']['users'][$x] );
										if ( !empty( $rules_users ) )
										foreach( $rules_users as $pu ) { 
											if ( empty( $pu ) )
												continue;
											$user = get_user_by( 'id', $pu);
											if ( empty( $user ) || is_wp_error( $user ) )
												continue;
											$user_strings[ $pu ] = esc_html( $user->display_name ) . ' (#' . absint( $user->ID ) . ' &ndash; ' . esc_html( $user->user_email ) . ')';
										}
									}

									?>
									<br/>

									<input type="hidden" class="min_max_users" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[users][<?php echo $x ?>]" data-placeholder="<?php _e( 'Search for a user&hellip;', 'ignitewoo_wholesale_pro' ); ?>" data-selected='<?php echo esc_attr( json_encode( $user_strings ) ) ?>' value="<?php echo $this->settings['rules']['users'][$x] ?>" data-multiple="true" data-allow_clear="true" data-action="woocommerce_json_search_customers" style="width:100%;min-height:32px"/>
								</div>
							<?php } ?>
							</td>
							<td>
								<input type="number" step="0.01" class="short" value="<?php echo $this->settings['rules']['cart_total'][$x] ?>" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[cart_total][<?php echo $x ?>]">
							</td>
							<td>
								<input type="number" step="0.01" class="short" value="<?php echo $this->settings['rules']['cart_total_max'][$x] ?>" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[cart_total_max][<?php echo $x ?>]">
							</td>
							<td>
								<input type="number" step="1" class="short" value="<?php echo $this->settings['rules']['cart_qty'][$x] ?>" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[cart_qty][<?php echo $x ?>]">
							</td>
							<td>
								<input type="number" step="1" class="short" value="<?php echo $this->settings['rules']['product_qty'][$x] ?>" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[product_qty][<?php echo $x ?>]">
							</td>
							<td>
								<input type="number" step="1" class="short" value="<?php echo $this->settings['rules']['order_qty'][$x] ?>" name="woocommerce_ignitewoo_wholesale_pro_suite_<?php echo $key ?>[order_qty][<?php echo $x ?>]">
							</td>
						</tr>

						<?php } ?>
					</tbody>
				</table>
			
				<div style="margin-top: 0.5em">
					<button id="minmax_add_rule" class="button"><?php _e( 'Add new rule', 'ignitewoo_wholesale_pro' ) ?></button>
					
					<button id="minmax_remove_rule" class="button" style="margin-left: 1.5em"><?php _e( 'Remove selected rules', 'ignitewoo_wholesale_pro' ) ?></button>
				</div>
		
				<script>
				jQuery( document ).ready( function( $ ) { 
					$( '.rule_table' ).sortable({
						items: 'tr',
						cursor: 'move',
						scrollSensitivity: 40,
						forcePlaceholderSize: true,
						helper: 'clone',
						opacity: 0.65,
						stop: function() { 
							renumber_fields();
							$( '.rule_table tbody tr:even' ).not( '.template' ).each( function() {
								$( this ).addClass( 'alt' );
							});
							$( '.rule_table tbody tr:odd' ).not( '.template' ).each( function() {
								$( this ).removeClass( 'alt' );
							});
						}
					});
					
					$( '#minmax_add_rule' ).on( 'click', function(e) { 
						e.preventDefault();
						var row = $( '.rule_table .template' ).clone();
						row.removeClass( 'template' );
						$( '.rule_table' ).append( '<tr>' + row.html() + '</tr>' );
						
						$( '.rule_table tr:last-child' ).find( 'select.select2' ).addClass( 'enhanced' ).select2();
						wc_min_max_users_select();
						
						renumber_fields();
						return false;
					})
					
					$( '.rule_table tr' ).not( '.template' ).each( function() { 
						<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.2', '>=' ) ) { ?>
						$( this ).find( 'select.select2' ).addClass( 'enhanced' ).select2();
						<?php } else { ?>
						$( this ).find( 'select.select2' ).addClass( 'enhanced' ).chosen();
						<?php } ?>
						
					});
					
					$( '#minmax_remove_rule' ).on( 'click', function(e) { 
						e.preventDefault();
						if ( !confirm( '<?php _e( 'Remove selected rules?', 'ignitewoo_wholesale_pro' ) ?>' ) )
							return false;
						$( '.rule_table tr' ).each( function() { 
							if ( $( this ).find( '.remove' ).is( ':checked' ) )
								$( this ).remove();
						})
						renumber_fields();
						return false;
					});
					
					$( '.rule_table tbody tr:odd' ).not( '.template' ).each( function() {
						$( this ).removeClass( 'alt' );
					});
					
					function renumber_fields() {
						var field_index = 1;
						$( '.rule_table tbody tr' ).not( '.template' ).each( function() { 
						
							$( this ).find( "input[type='number']" ).each( function( index, element ) {
								var pos = nth_occurrence( element.name, '[', 2 );
								element.name = element.name.substr( 0,  pos  );
								element.name = element.name + '[' + field_index + ']';
							});
							$( this ).find( "input[type='hidden']" ).each( function( index, element ) {
								var pos = nth_occurrence( element.name, '[', 2 );
								element.name = element.name.substr( 0,  pos  );
								element.name = element.name + '[' + field_index + ']';
							});
							$( this ).find( "select" ).each( function( index, element ) {
								var pos = nth_occurrence( element.name, '[', 2 );
								element.name = element.name.substr( 0, pos );
								element.name = element.name + '[' + field_index + '][]';
							});

							field_index = parseInt( field_index ) + 1;
						});
						$( '.rule_table tbody tr:odd' ).not( '.template' ).each( function() {
							$( this ).removeClass( 'alt' );
						});
						$( '.rule_table tbody tr:even' ).not( '.template' ).each( function() {
							$( this ).addClass( 'alt' );
						});
					}
					function nth_occurrence (string, char, nth) {
						var first_index = string.indexOf( char );
						var length_up_to_first_index = first_index + 1;

						if ( nth == 1 ) {
							return first_index;
						} else {
							var string_after_first_occurrence = string.slice(length_up_to_first_index);
							var next_occurrence = nth_occurrence(string_after_first_occurrence, char, nth - 1);

							if (next_occurrence === -1) {
								return -1;
							} else {
								return length_up_to_first_index + next_occurrence;  
							}
						}
					}
					
					function cust_users_format_string() {
						var formatString = {
							formatMatches: function( matches ) {
								if ( 1 === matches ) {
									return wc_enhanced_select_params.i18n_matches_1;
								}

								return wc_enhanced_select_params.i18n_matches_n.replace( '%qty%', matches );
							},
							formatNoMatches: function() {
								return wc_enhanced_select_params.i18n_no_matches;
							},
							formatAjaxError: function( jqXHR, textStatus, errorThrown ) {
								return wc_enhanced_select_params.i18n_ajax_error;
							},
							formatInputTooShort: function( input, min ) {
								var number = min - input.length;

								if ( 1 === number ) {
									return wc_enhanced_select_params.i18n_input_too_short_1;
								}

								return wc_enhanced_select_params.i18n_input_too_short_n.replace( '%qty%', number );
							},
							formatInputTooLong: function( input, max ) {
								var number = input.length - max;

								if ( 1 === number ) {
									return wc_enhanced_select_params.i18n_input_too_long_1;
								}

								return wc_enhanced_select_params.i18n_input_too_long_n.replace( '%qty%', number );
							},
							formatSelectionTooBig: function( limit ) {
								if ( 1 === limit ) {
									return wc_enhanced_select_params.i18n_selection_too_long_1;
								}

								return wc_enhanced_select_params.i18n_selection_too_long_n.replace( '%qty%', limit );
							},
							formatLoadMore: function( pageNumber ) {
								return wc_enhanced_select_params.i18n_load_more;
							},
							formatSearching: function() {
								return wc_enhanced_select_params.i18n_searching;
							}
						};

						return formatString;
					}
					function wc_min_max_users_select() { 
						<?php 
						if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) {
							$action = 'wp_ajax_woocommerce_json_search_customers';
							$selector = 'select';
						} else {
							$action = 'woocommerce_json_search_customers';
							$selector = 'input';
						}
						?>

						$( '.rule_table tr, .var_rule_table tr' ).not( '.template' ).each( function() {

							$( this ).find( '<?php echo $selector ?>.min_max_users' ).not( '.enhanced' ).each( function() { 
							
								<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) { ?>

									var ign_select2_args = {
										allowClear:  $( this ).data( 'allow_clear' ) ? true : false,
										placeholder: $( this ).data( 'placeholder' ),
										minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '3',
										escapeMarkup: function( m ) {
											return m;
										},
										ajax: {
											url:         wc_enhanced_select_params.ajax_url,
											dataType:    'json',
											quietMillis: 250,
											data: function( params ) {
												return {
													term:     params.term,
													action:   'woocommerce_json_search_customers',
													security: wc_enhanced_select_params.search_customers_nonce,
													exclude:  $( this ).data( 'exclude' )
												};
											},
											processResults: function( data ) {
												var terms = [];
												if ( data ) {
													$.each( data, function( id, text ) {
														terms.push({
															id: id,
															text: text
														});
													});
												}
												return {
													results: terms
												};
											},
											cache: true
										}
									};
								<?php } else { ?>

									var ign_select2_args = {
										allowClear:  $( this ).data( 'allow_clear' ) ? true : false,
										placeholder: $( this ).data( 'placeholder' ),
										minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '3',
										escapeMarkup: function( m ) {
											return m;
										},
										ajax: {
											url: '<?php echo admin_url( 'admin-ajax.php' ) ?>',
											dataType: 'json',
											quietMillis: 250,
											data: function( term, page ) {
												return {
													term: term,
													action: '<?php echo $action ?>',
													security: '<?php echo wp_create_nonce( 'search-customers' ) ?>',
												};
											},
											results: function( data, page ) {
												var terms = [];
												if ( data ) {
													$.each( data, function( id, text ) {
														terms.push( { id: id, text: text } );
													});
												}
												return { results: terms };
											},
											cache: true
										}
									}
									ign_select2_args.multiple = true;
									ign_select2_args.initSelection = function( element, callback ) {
										var data     = $.parseJSON( element.attr( 'data-selected' ) );
										var selected = [];

										$( element.val().split( "," ) ).each( function( i, val ) {
											selected.push( { id: val, text: data[ val ] } );
										});

										return callback( selected );
									};

									ign_select2_args.formatSelection = function( data ) {
										return '<div class="selected-option" data-id="' + data.id + '">' + data.text + '</div>';
									};
								
								<?php } ?>
								ign_select2_args = $.extend( ign_select2_args, cust_users_format_string() );
								
								$( this ).select2( ign_select2_args ).addClass( 'enhanced' );
							})
						})
					}
					
					wc_min_max_users_select()
				
				})
				</script>
			</td>
		</tr>
		<?php
		
		return ob_get_clean();
	}

	function sitewide_discount_rule_box() {

		?>
		<script>
		jQuery( document ).ready( function($) { 
			//$( '#woocommerce-dd-sitewide-pricing-rules-wrap' ).bind( 'DOMSubtreeModified', function() { 
			$( '#woocommerce-dd-sitewide-pricing-rules-wrap .chosen' ).not( '.enhanced_select' ).each( function() { 
				$( 'select.chosen' ).addClass( 'enhanced_select' ).select2();	
			})
		});
		</script>
		<div id="woocommerce-pricing-category">

			<?php //settings_fields('_woocommerce_sitewide_rules'); ?>

			<?php 
			
			$pricing_rule_sets = get_option('_woocommerce_sitewide_rules', array()); 
			
			$set_index = 0;
			
			if ( !empty( $pricing_rule_sets ) )
			foreach( $pricing_rule_sets as $key => $vals ) { 
			
				$set_index = absint( str_replace( 'set_', '', $key ) );
				
				break;			
			}

			?>

			<div id="woocommerce-dd-sitewide-pricing-rules-wrap" class="inside" data-setindex="<?php echo $set_index; ?>">

				<?php $this->meta_box_javascript(); ?>

				<?php $this->meta_box_css(); ?>  

				<?php if ($pricing_rule_sets && is_array($pricing_rule_sets) && sizeof($pricing_rule_sets) > 0) : ?>
					<?php $this->create_rulesets($pricing_rule_sets); ?>
				<?php endif; ?>        

			</div>

			<button id="woocommerce-dd-pricing-add-ruleset" type="button" class="button button-secondary"> <?php _e( 'Add New Ruleset', 'ignitewoo_wholesale_pro' )?></button>

		</div>
		<?php
        }

	function create_rulesets($pricing_rule_sets, $index_offset = null ) {

		if ( empty( $index_offset ) )
			$index_offset = 0;
			
		$i = $index_offset;
	
		foreach ( $pricing_rule_sets as $name => $pricing_rule_set ) {

			$name = 'set_' . $index_offset; 

			$index_offset++;
			
			$dd_pricing_rules = isset($pricing_rule_set['rules']) ? $pricing_rule_set['rules'] : null;
			$pricing_conditions = isset($pricing_rule_set['conditions']) ? $pricing_rule_set['conditions'] : null;
			$collector = isset($pricing_rule_set['collector']) ? $pricing_rule_set['collector'] : null;

			$invalid = isset($pricing_rule_set['invalid']);
			$validation_class = $invalid ? 'invalid' : '';

			?>
			<div id="woocommerce-dd-sitewide-pricing-ruleset-<?php echo $name; ?>" class="woocommerce_sitewide_dd_pricing_ruleset <?php echo $validation_class; ?>">

				<h4 class="first"><?php _e( 'Sitewide Discount Ruleset', 'ignitewoo_wholesale_pro' ) ?><a href="#" data-name="<?php echo $name; ?>" class="delete_dd_pricing_ruleset remove_rule_set" style="float:right"><span class="dashicons dashicons-minus"></span></a></h4>    


				<div id="woocommerce-pricing-collector-<?php echo $name; ?>" class="section" style="" >
					<?php
					if (is_array($collector) && count($collector) > 0) {
						$this->create_collector($collector, $name);
					} else {
						$product_cats = array();
						$this->create_collector(array('type' => 'cat', 'args' => array('cats' => $product_cats)), $name);
					}
					?>
				</div>


				<div id="woocommerce-pricing-conditions-<?php echo $name; ?>" class="section">
				<?php
				if ( empty( $index_offset ) )
					$condition_index = 0;
				else 
					$condition_index = $index_offset;
					
				if (is_array($pricing_conditions) && sizeof($pricing_conditions) > 0):
					?>
					<input type="hidden" name="dd_pricing_rules[<?php echo $name; ?>][conditions_type]" value="all" />
					<?php

					foreach ($pricing_conditions as $condition) {
						$condition_index++;
					
						$this->create_condition($condition, $name, $condition_index);
					}
				else :
					?>
					<input type="hidden" name="dd_pricing_rules[<?php echo $name; ?>][conditions_type]" value="all" />
					<?php
					$this->create_condition(array('type' => 'apply_to', 'args' => array('applies_to' => 'everyone', 'roles' => array())), $name, 1 );
				endif;
				?>
				</div>

				<div class="section">
				<table id="woocommerce-dd-sitewide-pricing-rules-table-<?php echo $name; ?>" class="widefat rules_table" data-lastindex="<?php echo (is_array($dd_pricing_rules) && sizeof($dd_pricing_rules) > 0) ? count($dd_pricing_rules) : '1'; ?>">
					<thead>
					<th>
					<?php _e('Minimum Quantity', 'ignitewoo_wholesale_pro'); ?>
					</th>
					<th>
					<?php _e('Max Quantity', 'ignitewoo_wholesale_pro'); ?>
					</th>
					<th>
					<?php _e('Type', 'ignitewoo_wholesale_pro'); ?>
					</th>
					<th>
					<?php _e('Amount', 'ignitewoo_wholesale_pro'); ?>
					</th>
					<th>&nbsp;</th>
					</thead>
					<tbody>
					<?php
					$index = 0;
					if ( is_array( $dd_pricing_rules ) && sizeof( $dd_pricing_rules ) > 0) {
						foreach ( $dd_pricing_rules as $rule ) {
							$index++;
							$this->get_row( $rule, $name, $index );
						}
					} else {
						$this->get_row( array( 'to' => '', 'from' => '', 'amount' => '', 'type' => '' ), $name, 1 );
					}
					?>
					</tbody>
					<tfoot>
					</tfoot>
				</table>
				</div>
			</div><?php
		    }
        }

	function create_new_ruleset() {

		$set_index = $_POST['set_index'];

		$pricing_rule_sets = array();

		$pricing_rule_sets['set_' . $set_index] = array();

		$pricing_rule_sets['set_' . $set_index]['title'] = 'Rule Set ' . $set_index;

		$pricing_rule_sets['set_' . $set_index]['rules'] = array();

		$this->create_rulesets( $pricing_rule_sets, $set_index );

		die;
	}


	function create_condition($condition, $name, $condition_index) {
		global $wp_roles;

		switch ( $condition['type'] ) {

		    case 'apply_to':
			$this->create_condition_apply_to( $condition, $name, $condition_index );
			break;

		    default:
			break;
		}
	}


	function create_condition_apply_to( $condition, $name, $condition_index ) {
		global $wpdb;
		
		if ( !isset( $wp_roles ) )
		    $wp_roles = new WP_Roles();

		$all_roles = $wp_roles->roles;
		
		/*
		$sql = '
			SELECT DISTINCT ID, user_login, user_email, m1.meta_value AS fname, m2.meta_value AS lname
			FROM ' . $wpdb->users . '
			LEFT JOIN ' . $wpdb->usermeta . ' m1 ON ID = m1.user_id
			LEFT JOIN ' . $wpdb->usermeta . ' m2 ON ID = m2.user_id
			WHERE 
			m1.meta_key = "billing_first_name"
			AND
			m2.meta_key = "billing_last_name"
			ORDER BY m2.meta_value ASC 
		';

		$all_users = $wpdb->get_results( $sql );
		*/
		
		$div_style = ( $condition['args']['applies_to'] != 'roles' ) ? 'display:none;' : '';
		
		?>

		<div>
			<label for="pricing_rule_apply_to_<?php echo $name . '_' . $condition_index; ?>"><?php _e( 'Applies To:', 'ignitewoo_wholesale_pro'); ?></label>

			<input type="hidden" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][type]" value="apply_to" />
			
			<?php /*
			<input type="hidden" value="roles" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][args][applies_to]" >
			
			<input type="hidden" id="<?php echo $name; ?>_role_<?php echo $role_id; ?>" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][args][roles][]" value="dd_buyer" />
			*/ ?>
			
			<select style="width:200px" class="pricing_rule_apply_to chosen" id="pricing_rule_apply_to_<?php echo $name . '_' . $condition_index; ?>" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][args][applies_to]">
				<option <?php selected('everyone', $condition['args']['applies_to']); ?> value="everyone">Everyone</option>
				<option <?php selected('roles', $condition['args']['applies_to']); ?> value="roles"><?php _e( 'Specific Roles', 'ignitewoo_wholesale_pro');?></option>
				<option <?php selected( 'users', $condition['args']['applies_to'] ); ?> value="users"><?php _e( 'Specific Users', 'ignitewoo_wholesale_pro'); ?></option>
				<option <?php selected( 'unauthenticated', $condition['args']['applies_to'] ); ?> value="unauthenticated"><?php _e( 'Not Logged In', 'ignitewoo_wholesale_pro'); ?></option>
			</select>

			<div class="roles" style="<?php echo $div_style; ?> margin-top: 10px; width:600px">
				<?php $chunks = array_chunk($all_roles, ceil(count($all_roles) / 3), true); ?>

				<p class="description"><?php _e( 'Click in the box and start typing a role name to select roles. Add as many as you need.', 'ignitewoo_wholesale_pro' )?></p>
				
				<select class="chosen dd_plus_rule_roles" multiple="multiple" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][args][roles][]" style="width:600px" >
				
				<?php foreach ($chunks as $chunk) : ?>

					<?php foreach ($chunk as $role_id => $role) : ?>
					
					<?php $role_checked = (isset($condition['args']['roles']) && is_array($condition['args']['roles']) && in_array($role_id, $condition['args']['roles'])) ? 'selected="selected"' : ''; ?>
					
					<option <?php echo $role_checked ?> value="<?php echo $role_id; ?>">
							<?php echo $role['name']; ?>
					</option>

					<?php endforeach; ?>
				
				<?php endforeach; ?>
				</select>
			</div>
			
			<div style="clear:both;"></div>

			<?php $div_style = ( $condition['args']['applies_to'] != 'users' ) ? 'display:none;' : ''; ?>
			
			<div class="users" style="clear: both; z-index: 999999; <?php echo $div_style; ?> ">

				<p class="description"><?php _e( 'Click in the box and start typing a name to select users. Add as many as you need.', 'ignitewoo_wholesale_pro' )?></p>

				<select class="chosensss dd_plus_rule_users" multiple="multiple" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][args][users][]" style="width:600px">
					
					<?php 
					if ( isset( $condition['args']['users'] ) && is_array( $condition['args']['users'] ) ) {
						foreach( $condition['args']['users'] as $u ) { 
							$data = get_user_by( 'id', $u );
							if ( empty( $data ) || is_wp_error( $data ) )
								continue;
							$data = $data->data; 
							
							$formatted_name = '#' . $data->ID . ' &ndash; ';
							
							if ( $fname = ( get_user_meta( $data->ID, 'billing_first_name', true ) ) )
								$formatted_name .= $fname;
							if ( $lname = ( get_user_meta( $data->ID, 'billing_last_name', true ) ) )
								$formatted_name .= ' ' . $lname;
							if ( empty( $fname ) || empty( $lname ) )
								$formatted_name .= $data->user_login;
								
							$formatted_name .=  ' (' . $data->user_email . ')';
							
							?>
							<option <?php echo $u ?> value="<?php echo $data->ID ?>" selected="selected">
								<?php echo $formatted_name ?>
							</option>
							<?php
						}
					}
					
					?>
					
					<?php /*foreach( $all_users as $key => $data ) { ?>

						<?php $user_selected = (isset( $condition['args']['users'] ) && is_array( $condition['args']['users'] ) && in_array( $data->ID, $condition['args']['users'] )) ? 'selected="selected"' : ''; ?>

						<option <?php echo $user_selected ?> value="<?php echo $data->ID ?>">
							<?php echo $data->lname . ', ' . $data->fname . ' &mdash; ' . $data->user_email; ?>
						</option>

					<?php } */?>

				</select>

			</div>
		
			<div class="clear"></div>

		
		</div>
		<?php
	}


        function create_collector($collector, $name) {
		$terms = (array) get_terms('product_cat', array('get' => 'all'));
		?>
		<label for="pricing_rule_when_<?php echo $name; ?>"><?php _e('Quantities based on:', 'ignitewoo_wholesale_pro'); ?></label>
		
		<select title="Choose how to calculate the quantity.  This tallied amount is used in determining the min and max quantities used below in the Quantity Pricing section." class="dd_pricing_rule_when chosen" style="width: 200px" id="pricing_rule_when_<?php echo $name; ?>" name="dd_pricing_rules[<?php echo $name; ?>][collector][type]">
		
			<option title="Calculate quantity based on cart item quantity" <?php selected('cat_product', $collector['type']); ?> value="cart_item"><?php _e('Cart Line Item Quantity', 'ignitewoo_wholesale_pro'); ?></option>
			
			<option title="Calculate quantity based on total sum of the categories in the cart" <?php selected('cat', $collector['type']); ?> value="cat"><?php _e('Sum of Category', 'ignitewoo_wholesale_pro'); ?></option>
			
		</select>
		
		<div class="cats">   
			<label style="margin-top:10px;"><?php _e( 'Categories', 'ignitewoo_wholesale_pro' )?>:</label>

			<?php 
				$size = ceil( count( $terms ) / 3 );
				
				if ( $size )
					$chunks = array_chunk( $terms, $size ); 
				else
					$chunks = array();

			?>
			<style>
			.chosen-container.chosen-container-multi li input {
				height: 24px !important;
			}
			.chosen-container-multi .chosen-choices, .chosen-container .chosen-drop {
				width: 250px !important;
			}
			</style>

			<select class="chosen dd_plus_rule_cats chosen" multiple="multiple" name="dd_pricing_rules[<?php echo $name; ?>][collector][args][cats][]" style="width:200px !important">
						
			<?php foreach ($chunks as $chunk) : ?>

				<?php foreach ($chunk as $term) : ?>
				
					<?php $term_checked = (isset($collector['args']['cats']) && is_array($collector['args']['cats']) && in_array($term->term_id, $collector['args']['cats'])) ? 'selected="selected"' : ''; ?> 
					
					<option value="<?php echo $term->term_id; ?>" <?php echo $term_checked; ?>><?php echo $term->name; ?> </option>
					
					
				<?php endforeach; ?>

				
			<?php endforeach; ?>
			
			</select>
			
			<div style="clear:both;"></div>
		</div>
		<?php
        }


        function get_row($rule, $name, $index ) {
		?>
		<tr id="pricing_rule_row_<?php echo $name . '_' . $index; ?>">
		    <td>
				<input class="int_pricing_rule" id="pricing_rule_from_input_<?php echo $name . '_' . $index; ?>" type="text" name="dd_pricing_rules[<?php echo $name; ?>][rules][<?php echo $index ?>][from]" value="<?php echo $rule['from']; ?>" />
		    </td>
		    <td>
			    <input class="int_pricing_rule" id="pricing_rule_to_input_<?php echo $name . '_' . $index; ?>" type="text" name="dd_pricing_rules[<?php echo $name; ?>][rules][<?php echo $index ?>][to]" value="<?php echo $rule['to']; ?>" />
		    </td>
		    <td>
			    <select id="pricing_rule_type_value_<?php echo $name . '_' . $index; ?>" class="chosen" name="dd_pricing_rules[<?php echo $name; ?>][rules][<?php echo $index; ?>][type]">
				    <option <?php selected('price_discount', $rule['type']); ?> value="price_discount">Price Discount</option>
				    <option <?php selected('percentage_discount', $rule['type']); ?> value="percentage_discount">Percentage Discount</option>
				    <option <?php selected('fixed_price', $rule['type']); ?> value="fixed_price">Fixed Price</option>
			    </select>
		    </td>
		    <td>
			<input class="float_rule_pricing" id="pricing_rule_amount_input_<?php echo $name . '_' . $index; ?>" type="text" name="dd_pricing_rules[<?php echo $name; ?>][rules][<?php echo $index; ?>][amount]" value="<?php echo $rule['amount']; ?>" /> 
		    </td>
		    <td><a class="add_pricing_rule dashicons dashicons-plus" title="add another rule" data-index="<?php echo $index; ?>" data-name="<?php echo $name; ?>"><span style="cursor:pointer;"></span></a>
				<a <?php echo ($index > 1) ? '' : 'style="display:none;"'; ?> class="delete_pricing_rule dashicons dashicons-minus" data-index="<?php echo $index; ?>" data-name="<?php echo $name; ?>"><span class=""></span></a>
		    </td>
		</tr>
		<?php
        }


	function meta_box_javascript() {
		?>
		<script type="text/javascript">
																																
		jQuery(document).ready(function($) {
			var set_index = 0;
			var rule_indexes = new Array();

			$('.woocommerce_sitewide_dd_pricing_ruleset').each(function(){
				var length = $('table tbody tr', $(this)).length;
				if (length==1) {
					$('.delete_pricing_rule', $(this)).hide(); 
				}
			});

			$("#woocommerce-dd-pricing-add-ruleset").click(function(event) {
				event.preventDefault();

				//var set_index = $("#woocommerce-dd-sitewide-pricing-rules-wrap").data('setindex') + 1;

				var set_index = $( '.woocommerce_sitewide_dd_pricing_ruleset'). length;
				
				var btn = $( this );
				
				//set_index++;;
				
				$("#woocommerce-dd-sitewide-pricing-rules-wrap").data('setindex', set_index );
																																
				var data = {
					set_index:set_index,
					post:<?php echo isset($_GET['post']) ? $_GET['post'] : 0; ?>,
					action:'ignitewoo_dd_create_new_ruleset'
				}
				
				btn.block({
					message: null,
					overlayCSS: {
						background: '#333',
						opacity: 0.6
					}
				});
				
				$( '#woocommerce-dd-sitewide-pricing-rules-wrap' ).block({
					message: null,
					overlayCSS: {
						background: '#333',
						opacity: 0.6
					}
				});
				$.post( ajaxurl, data, function( response ) { 
					$( '#woocommerce-dd-sitewide-pricing-rules-wrap' ).append( response );
					//$( '#woocommerce-dd-sitewide-pricing-rules-wrap .chosen' ).select2( 'destroy' );
					dd_price_rules_select_users();

					$( '#woocommerce-dd-sitewide-pricing-rules-wrap .chosen' ).not( '.enhanced_select' ).each( function() { 
						$( 'select.chosen' ).addClass( 'enhanced_select' ).select2();	
					})
					btn.unblock();
					$( '#woocommerce-dd-sitewide-pricing-rules-wrap' ).unblock();
				});  
			});
			function dd_price_rules_select_users() { 
				var ign_select2_args = {
					allowClear:  $( this ).data( 'allow_clear' ) ? true : false,
					placeholder: $( this ).data( 'placeholder' ),
					minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '3',
					escapeMarkup: function( m ) {
						return m;
					},
					ajax: {
						url:         wc_enhanced_select_params.ajax_url,
						dataType:    'json',
						quietMillis: 250,
						delay: 250,
						data: function( params ) {
							return {
								term:     params.term,
								action:   'woocommerce_json_search_customers',
								security: wc_enhanced_select_params.search_customers_nonce,
								exclude:  $( this ).data( 'exclude' )
							};
						},
						results: function( data ) {
							var terms = [];
							if ( data ) {
								$.each( data, function( id, text ) {
									terms.push({
										id: id,
										text: text
									});
								});
							}
							return {
								results: terms
							};
						},
						processResults: function( data ) {
							var terms = [];
							if ( data ) {
								$.each( data, function( id, text ) {
									terms.push({
										id: id,
										text: text
									});
								});
							}
							return {
								results: terms
							};
						},
						cache: true
					}
				};
				$( '.dd_plus_rule_users' ).unbind().select2( ign_select2_args );
			};
			dd_price_rules_select_users();
			
			$('#woocommerce-dd-sitewide-pricing-rules-wrap').delegate('.pricing_rule_apply_to', 'change', function(event) {  
				var value = $(this).val();

				if ( value == 'unauthenticated' ) { 
					$( '.roles', $(this).parent()).hide();
					$( '.roles input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
					$( '.users', $(this).parent()).hide();     
					$( '.users input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
				} else if (value != 'roles' && value != 'users' ) {
					$( '.users', $(this).parent()).hide();
					$( '.users input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
					$( '.roles', $(this).parent()).hide();
					$( '.roles input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
				} else if ( value == 'roles' ) {
					$( '.users', $(this).parent()).hide();
					$( '.users input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
					$( '.roles', $(this).parent()).fadeIn();
				} else if ( value == 'users' ) { 
					$( '.roles', $(this).parent()).hide();
					$( '.roles input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
					$( '.users', $(this).parent()).fadeIn();                                                                                         
				}
			});

			$( '#woocommerce-dd-sitewide-pricing-rules-wrap' ).delegate( '.dd_pricing_rule_when', 'change', function(event) {  

			    var value = $(this).val();

			    if (value != 'cat' ) {
				$( '.cats', $(this).closest( 'div' )).fadeOut();
				$( '.cats input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );

			    } else {                                                            
				$( '.cats', $(this).closest( 'div' )).fadeIn();
			    }                                                              
			});
			

			$('.dd_pricing_rule_when').change();
			
			//Remove Pricing Set
			$('#woocommerce-dd-sitewide-pricing-rules-wrap').delegate('.delete_dd_pricing_ruleset', 'click', function(event) {  
				event.preventDefault();
				DeleteRuleSet( $(this).data('name') );
			});

			//Add Button
			$('#woocommerce-dd-sitewide-pricing-rules-wrap').delegate('.add_pricing_rule', 'click', function(event) {  
				event.preventDefault();
				InsertRule( $(this).data('index'), $(this).data('name') );
			});

			$('#woocommerce-dd-sitewide-pricing-rules-wrap').delegate('.delete_pricing_rule', 'click', function(event) {  
				event.preventDefault();
				DeleteRule($(this).data('index'), $(this).data('name'));
			});

			$('#woocommerce-dd-sitewide-pricing-rules-wrap').delegate('.int_pricing_rule', 'keydown', function(event) {  
				// Allow only backspace, delete and tab
				if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 ) {
				    // let it happen, don't do anything
				}
				else {
				    if (event.shiftKey && event.keyCode == 56){
					if ( $(this).val().length > 0) {
					    event.preventDefault();
					} else {
					    return true;    
					}
				    }else if (event.shiftKey){
					event.preventDefault();
				    } else if ( (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) ) {
					event.preventDefault(); 
				    } else {
					if ( $(this).val() == "*") {
						event.preventDefault();
					}
				    }
				}
			});

			$('#woocommerce-dd-sitewide-pricing-rules-wrap').delegate('.float_rule_pricing', 'keydown', function(event) {  
				// Allow only backspace, delete and tab
				if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 190 ) {
				    // let it happen, don't do anything
				}
				else {
				    if (event.shiftKey && event.keyCode == 56){
					if ( $(this).val().length > 0) {
					    event.preventDefault();
					} else {
					    return true;    
					}
				    }else if (event.shiftKey){
					event.preventDefault();
				    } else if ( (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) ) {
					event.preventDefault(); 
				    } else {
					if ( $(this).val() == "*") {
						event.preventDefault();
					}
				    }
				}
			});

			$("#woocommerce-dd-sitewide-pricing-rules-wrap").sortable(
			{ 
			    handle: 'h4.first',
			    containment: 'parent',
			    axis:'y'
			});

			function InsertRule(previousRowIndex, name) {

				var $index = $("#woocommerce-dd-sitewide-pricing-rules-table-" + name).data('lastindex') + 1;

				$("#woocommerce-dd-sitewide-pricing-rules-table-" + name).data('lastindex', $index );

				var html = '';
				html += '<tr id="pricing_rule_row_' + name + '_' + $index + '">';
				html += '<td>';
				html += '<input class="int_pricing_rule" id="pricing_rule_from_input_'  + name + '_' + $index + '" type="text" name="dd_pricing_rules[' + name + '][rules][' + $index + '][from]" value="" /> ';
				html += '</td>';
				html += '<td>';
				html += '<input class="int_pricing_rule" id="pricing_rule_to_input_' + name + '_' + $index + '" type="text" name="dd_pricing_rules[' + name + '][rules][' + $index + '][to]" value="" /> ';
				html += '</td>';
				html += '<td>';
				html += '<select id="pricing_rule_type_value_' + name + '_' + $index + '" class="chosen" name="dd_pricing_rules[' + name + '][rules][' + $index + '][type]">';
				html += '<option value="price_discount">Price Discount</option>';
				html += '<option value="percentage_discount">Percentage Discount</option>';
				html += '<option value="fixed_price">Fixed Price</option>';
				html += '</select>';
				html += '</td>';
				html += '<td>';
				html += '<input class="float_pricing_rule" id="pricing_rule_amount_input_' + $index + '" type="text" name="dd_pricing_rules[' + name + '][rules][' + $index + '][amount]" value="" /> ';
				html += '</td>';
				html += '<td>';
				html += '<a data-index="' + $index + '" data-name="' + name + '" class="add_pricing_rule dashicons dashicons-plus" title="add another rule"><span class="" style="cursor:pointer;"></span></a>';         
				html += '<a data-index="' + $index + '" data-name="' + name + '" class="delete_pricing_rule dashicons dashicons-minus"><span class=""></span></a>';         
				html += '</td>';
				html += '</tr>';
																																
				$('#pricing_rule_row_' + name + '_' + previousRowIndex).after(html);
				$('.delete_pricing_rule', "#woocommerce-dd-sitewide-pricing-rules-table-" + name).show();

			} 

			function DeleteRule(index, name) {
				if (confirm("Are you sure you would like to remove this price adjustment?")) {
					$('#pricing_rule_row_' + name + '_' + index).remove();
																																
					var $index = $('tbody tr', "#woocommerce-dd-sitewide-pricing-rules-table-" + name).length;
					if ($index > 1) {
					    $('.delete_pricing_rule', "#woocommerce-dd-sitewide-pricing-rules-table-" + name).show();
					} else {
					    $('.delete_pricing_rule', "#woocommerce-dd-sitewide-pricing-rules-table-" + name).hide();
					}
				}
			}

			function DeleteRuleSet(name) {
				if (confirm('Are you sure you would like to remove this dynamic price set?')){
					$('#woocommerce-dd-sitewide-pricing-ruleset-' + name ).slideUp().remove();  
				}
			}
																																
		    });
																																
		</script>
		<?php
        }


        function meta_box_css() {
		?>
		<style>
		#woocommerce-pricing-category div.section {
			margin-bottom: 10px;
		}

		#woocommerce-pricing-category label {
			display:block;
			font-weight: bold;
			margin-bottom:5px;
		}

		#woocommerce-pricing-category .list-column {
			float:left;
			margin-right:25px;
			margin-top:0px;
			margin-bottom: 0px;
		}

		#woocommerce-pricing-category .list-column label {
			margin-bottom:0px;
		}

		#woocommerce-dd-sitewide-pricing-rules-wrap {
			margin:10px;
		}

		#woocommerce-dd-sitewide-pricing-rules-wrap h4 {
			border-bottom: 1px solid #E5E5E5;
			padding-bottom: 6px;
			font-size: 1em;
			margin: 1em 0 1em;
		}

		#woocommerce-dd-sitewide-pricing-rules-wrap h4.first {
			margin-top:0px;
			cursor:move;
		}

		.woocommerce_sitewide_dd_pricing_ruleset {
			border-color:#dfdfdf;
			border-width:1px;
			border-style:solid;
			-moz-border-radius:3px;
			-khtml-border-radius:3px;
			-webkit-border-radius:3px;
			border-radius:3px;
			padding: 10px;
			border-style:solid;
			border-spacing:0;
			background-color:#F9F9F9;
			margin-bottom: 25px;
		}

		.woocommerce_sitewide_dd_pricing_ruleset.invalid {
			border-color:#EACBCC;
			background-color:#FFDFDF;
		}

		.woocommerce_sitewide_dd_pricing_ruleset th {
			background-color: #efefef;
		}

		.woocommerce_sitewide_dd_pricing_ruleset .selectit {
			font-weight: normal !important;
		}
		.woocommerce_sitewide_dd_pricing_ruleset .chzn-choices, .woocommerce_sitewide_dd_pricing_ruleset .chzn-drop, .chzn-choices .search-field input {
			min-width: 300px !important;
		}
		    
		</style>
		<?php	
        }

	function save_settings() {

		if ( isset( $_POST['dd_pricing_rules'] ) )
			update_option( '_woocommerce_sitewide_rules', $_POST['dd_pricing_rules'] );
		else
			update_option( '_woocommerce_sitewide_rules', '' );
			
		if ( !empty( $_POST['dd_show_discount_table'] ) )
			update_option( 'woocommerce_dd_show_discount_table', 'yes' );
		else 
			update_option( 'woocommerce_dd_show_discount_table', 'no' );
			
		if ( !empty( $_POST['dd_show_discount_calculator'] ) )
			update_option( 'woocommerce_dd_show_discount_calculator', 'yes' );
		else 
			update_option( 'woocommerce_dd_show_discount_calculator', 'no' );
		
		if ( !empty( $_POST['dd_product_rules_override'] ) )
			update_option( 'woocommerce_dd_product_rules_override', 'yes' );
		else 
			update_option( 'woocommerce_dd_product_rules_override', 'no' );

		if ( !empty( $_POST['dd_discount_table_placement'] ) )
			update_option( 'woocommerce_dd_discount_table_placement', $_POST['dd_discount_table_placement'] );
		else 
			update_option( 'woocommerce_dd_discount_table_placement', 'the_content_top' );	
		
		$this->process_role_options();
		
		$this->init_minmax_fields();

		$settings = get_option( 'woocommerce_'. $this->id . '_settings' );

		foreach( $this->form_fields as $key => $vals ) { 
	
			if ( 'rules' == $vals['type'] ) 
				$this->validate_rules_field( 'rules' );
	
			$value = '';
		
			if ( isset( $_POST[ 'woocommerce_' . $this->id . '_' . $key ] ) && !is_array( $_POST[ 'woocommerce_' . $this->id . '_' . $key ] ) )
				$value = strip_tags( $_POST[ 'woocommerce_' . $this->id . '_' . $key ] );
			
			else if ( isset( $_POST[ 'woocommerce_' . $this->id . '_' . $key ] ) && is_array( $_POST[ 'woocommerce_' . $this->id . '_' . $key ] ) )
				$value = $_POST[ 'woocommerce_' . $this->id . '_' . $key ];
				
			if ( !empty( $value ) && 'checkbox' == $vals['type'] ) 
				$value = 'yes';
			else if ( empty( $value ) && 'checkbox' == $vals['type'] ) 
				$value = 'no';
				
			$settings[ $key ] = $value;
		}
		
		
		
		if ( !empty( $_POST['seq_order_number_prefix'] ) )
			$settings['seq_order_number_prefix'] = $_POST['seq_order_number_prefix'];
		else 
			$settings['seq_order_number_prefix'] = '';
			
		if ( !empty( $_POST['global_discounts'] ) )
			$settings['global_discounts'] = $_POST['global_discounts'];
		else 
			$settings['global_discounts'] = array();
			
		if ( !empty( $_POST['thankyou_message'] ) )
			$settings['thankyou_message'] = $_POST['thankyou_message'];
		else 
			$settings['thankyou_message'] = '';

		update_option( 'woocommerce_'. $this->id . '_settings', $settings );
	}

	function process_role_options() {
		global $wp_roles, $wpdb;
		
		if ( !isset( $wp_roles->roles ) )
			return;

		if ( !empty( $_POST['ignite_level_name' ] ) ) { 
		
			foreach( $_POST['ignite_level_name' ] as $key => $irole ) { 

				if ( '' == trim( $irole ) )
					continue;
				
				foreach( $wp_roles->roles as $role => $data ) 
					if ( $role == $irole )
						continue;
				
				add_role( $key , __( trim( $irole ), 'ignitewoo_wholesale_pro' ), array(
					'read' => true,
					'edit_posts' => false,
					'delete_posts' => false
				) );
			}
		}
		
		if ( !empty( $_POST['ignite_level_tax' ] ) ) { 
		
			foreach( $_POST['ignite_level_tax' ] as $key => $irole ) { 
							
				$role = get_role( $key );
				
				if ( 'no' == $irole ) 
					$role->add_cap( 'no_tax' );
				else
					$role->remove_cap( 'no_tax' );
			}
		}

		if ( !empty( $_POST['ignite_level_tax_exemption' ] ) ) { 
		
			foreach( $_POST['ignite_level_tax_exemption' ] as $key => $irole ) { 
				// Store an option value of exempt tax types by tax db record id 
				// for the role where the key is the role key and the value
				// is one or more tax types separated by a comma
				update_option( $key . '_tax_exemptions', $irole );
			}
		} else { 
			foreach( $wp_roles->roles as $role => $data ) { 
				delete_option( $role . '_tax_exemptions' );
			}
		}

		if ( !empty( $_POST['ign_role'] ) ) foreach( $_POST['ign_role'] as $role => $data ) { 
			delete_option( $role . '_shipping_methods' );
		}

		if ( !empty( $_POST['ignite_level_shipping_methods' ] ) ) { 
		
			foreach( $_POST['ignite_level_shipping_methods' ] as $key => $methods ) { 
				// Store an option value of exempt tax types by tax db record id 
				// for the role where the key is the role key and the value
				// is one or more tax types separated by a comma

				update_option( $key . '_shipping_methods', $methods );
			}
		}

		if ( !empty( $_POST['ign_role'] ) ) foreach( $_POST['ign_role'] as $role => $data ) {
			delete_option( $role . '_payment_gateways' );
		}
		if ( !empty( $_POST['ignite_level_payment_gateways' ] ) ) { 
		
			foreach( $_POST['ignite_level_payment_gateways' ] as $key => $gateways ) { 
				// Store an option value of exempt tax types by tax db record id 
				// for the role where the key is the role key and the value
				// is one or more tax types separated by a comma
				update_option( $key . '_payment_gateways', $gateways );
			}
		}
		
		if ( version_compare( WOOCOMMERCE_VERSION, '2.2', '>=' ) ) {
			
			foreach( $wp_roles->roles as $role => $data ) { 

				if ( 'ignite_level_' != substr( $role, 0, 13 ) )
					continue;

				$role = get_role( $role );

				$role->remove_cap( 'backorders' );
			}
			
			if ( !empty( $_POST['ignite_level_backorders' ] ) ) { 
			
				foreach( $_POST['ignite_level_backorders' ] as $key => $irole ) { 
		
					$role = get_role( $key );

					if ( 'yes' == $irole ) 
						$role->add_cap( 'backorders' );
					else
						$role->remove_cap( 'backorders' );
				}
			}
		}
		
		if ( !empty( $_POST[ 'ignite_level_delete' ] ) ) { 
		
			$user_ids = $wpdb->get_results( 'select ID from ' . $wpdb->users . ' order by ID ASC', ARRAY_A );

			if ( !empty( $user_ids ) && !is_wp_error( $user_ids ) ) { 
				
				foreach( $_POST[ 'ignite_level_delete' ] as $key => $irole ) { 
					
					foreach ( ( array ) $user_ids as $user_id ) {

						$user = get_user_by( 'id', $user_id['ID'] );

						foreach ( ( array ) $user->roles as $role => $data ) {
							if ( $role == $irole ) { 
								$userdata = new WP_User( $user->data->ID );
								$userdata->remove_role( $irole );
								$userdata->add_role( 'customer' );
							}	
						}
					}
					remove_role( $key );
				}
			}
		}
		
		$settings = get_option( 'woocommerce_'. $this->id . '_settings' );

		$settings['show_regular_price'] =  isset( $_POST['show_regular_price'] ) ? $_POST['show_regular_price'] : '';
		$settings['show_regular_price_label'] = isset( $_POST['show_regular_price_label'] ) ? $_POST['show_regular_price_label'] : '';
		$settings['show_savings_label'] = isset( $_POST['show_savings_label'] ) ? $_POST['show_savings_label'] : '';
		$settings['show_savings'] = isset( $_POST['show_savings'] ) ? $_POST['show_savings'] : '';
		
		$settings['include_tax'] = isset( $_POST['include_tax'] ) ? $_POST['include_tax'] : 'no';
		$settings['display_cart'] = isset( $_POST['display_cart'] ) ? $_POST['display_cart'] : 'excl';
		$settings['display_shop'] = isset( $_POST['display_shop'] ) ? $_POST['display_shop'] : 'excl';
		
		$settings['display_when_no_retail'] = isset( $_POST['display_when_no_retail'] ) ? $_POST['display_when_no_retail'] : '';
		$settings['tier_filter'] = isset( $_POST['tier_filter'] ) ? $_POST['tier_filter'] : '';
		$settings['retail_filter'] = isset( $_POST['retail_filter'] ) ? $_POST['retail_filter'] : '';
		$settings['alt_filter'] = isset( $_POST['alt_filter'] ) ? $_POST['alt_filter'] : '';
		
		$settings['login_required_for_prices'] = $_POST['login_required_for_prices'];
		$settings['price_alt_type'] = isset( $_POST['price_alt_type'] ) ? $_POST['price_alt_type'] : '';
		$settings['price_alt_link'] = isset( $_POST['price_alt_link'] ) ? $_POST['price_alt_link'] : '';
		$settings['price_alt_text'] = isset( $_POST['price_alt_text'] ) ? $_POST['price_alt_text'] : '';
		
		$settings['disable_coupons']  = isset( $_POST['disable_coupons'] ) ? $_POST['disable_coupons'] : '';
		$settings['disable_coupons_error_msg']  = isset( $_POST['disable_coupons_error_msg'] ) ? $_POST['disable_coupons_error_msg'] : '';

		$settings['ignitewoo_wsp_automatic_approval']  = isset( $_POST['ignitewoo_wsp_automatic_approval'] ) ? $_POST['ignitewoo_wsp_automatic_approval'] : '';
		$settings['ignitewoo_wsp_add_btn_to_myaccount']  = isset( $_POST['ignitewoo_wsp_add_btn_to_myaccount'] ) ? $_POST['ignitewoo_wsp_add_btn_to_myaccount'] : '';
		
		$settings['ignitewoo_wsp_register_default_country']  = isset( $_POST['ignitewoo_wsp_register_default_country'] ) ? $_POST['ignitewoo_wsp_register_default_country'] : '';
		
		$settings['ignitewoo_wsp_new_registration_to']  = isset( $_POST['ignitewoo_wsp_new_registration_to'] ) ? trim( $_POST['ignitewoo_wsp_new_registration_to'] ) : '';
		
		$settings['ignitewoo_wsp_add_btn_to_myaccount_logged_in']  = isset( $_POST['ignitewoo_wsp_add_btn_to_myaccount_logged_in'] ) ? $_POST['ignitewoo_wsp_add_btn_to_myaccount_logged_in'] : '';
		$settings['ignitewoo_wsp_automatic_role']  = isset( $_POST['ignitewoo_wsp_automatic_role'] ) ? $_POST['ignitewoo_wsp_automatic_role'] : '';
		$settings['ignitewoo_wsp_pending_role']  = isset( $_POST['ignitewoo_wsp_pending_role'] ) ? $_POST['ignitewoo_wsp_pending_role'] : '';
		$settings['ignitewoo_wsp_disallow_login']  = isset( $_POST['ignitewoo_wsp_disallow_login'] ) ? $_POST['ignitewoo_wsp_disallow_login'] : '';
		$settings['ignitewoo_wsp_register_page']  = isset( $_POST['ignitewoo_wsp_register_page'] ) ? $_POST['ignitewoo_wsp_register_page'] : '';

		$settings['ignitewoo_wsp_add_myaccount_btn_label']  = isset( $_POST['ignitewoo_wsp_add_myaccount_btn_label'] ) ? $_POST['ignitewoo_wsp_add_myaccount_btn_label'] : '';
		
		$settings['ignitewoo_wsp_recaptcha_site_key']  = isset( $_POST['ignitewoo_wsp_recaptcha_site_key'] ) ? $_POST['ignitewoo_wsp_recaptcha_site_key'] : '';
		$settings['ignitewoo_wsp_recaptcha_secret_key']  = isset( $_POST['ignitewoo_wsp_recaptcha_secret_key'] ) ? $_POST['ignitewoo_wsp_recaptcha_secret_key'] : '';


		$settings['reg_form_fields'] = array();

		if ( !empty( $_POST['ign_form_field'] ) )
		for ( $i = 0; $i < count( $_POST['ign_form_field']['title'] ); $i++ ) { 
			if ( isset( $_POST['ign_form_field']['title'][$i] ) )
				$settings['reg_form_fields']['title'][] = $_POST['ign_form_field']['title'][ $i ];
			else 
				$settings['reg_form_fields']['title'][] = '';
			
			if ( isset( $_POST['ign_form_field']['required'][$i] ) )
				$settings['reg_form_fields']['required'][] = 'yes';
			else 
				$settings['reg_form_fields']['required'][] = '';
				
			if ( isset( $_POST['ign_form_field']['field_type'][$i] ) )
				$settings['reg_form_fields']['field_type'][] = $_POST['ign_form_field']['field_type'][ $i ];
			else 
				$settings['reg_form_fields']['field_type'][] = '';
				
			if ( isset( $_POST['ign_form_field']['field_name'][$i] ) )
				$settings['reg_form_fields']['field_name'][] = $_POST['ign_form_field']['field_name'][ $i ];
			else 
				$settings['reg_form_fields']['field_name'][] = '';
				
			if ( isset( $_POST['ign_form_field']['field_align'][$i] ) )
				$settings['reg_form_fields']['field_align'][] = $_POST['ign_form_field']['field_align'][ $i ];
			else 
				$settings['reg_form_fields']['field_align'][] = 'form-row-wide';

			if ( isset( $_POST['ign_form_field']['field_type_radio_options'][ $_POST['ign_form_field']['field_name'][$i]] ) )
				$settings['reg_form_fields']['field_type_radio_options'][ $_POST['ign_form_field']['field_name'][$i]] = $_POST['ign_form_field']['field_type_radio_options'][ $_POST['ign_form_field']['field_name'][$i]];		
		}

		update_option( 'woocommerce_'.  $this->id . '_settings', $settings );
	}
	
        function get_ad_box() {
		?>
		<img style="height:22px;vertical-align:middle;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAqCAMAAABIknNMAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAACXBIWXMAAAX/AAAF/wHJdq1WAAAA4VBMVEVMaXEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/ZgD////kUACdMgChNgBKFgDhTwD3WgAeCQBkIACTMAAEAwOrOgDzWgDWSgBqamrFQwDU1NTx8fH+YADf399aWlr5+fkjIyPo6OjoUwCxsbEUFBQ/EwCampoOCggxMTHIyMjyVwCIiIh+fn52JgC3PQBzc3NERES/v7/AQAC4uLiRkZExDgA9PT3MRgCpqaljY2OGLABTU1NMTExISEhZHACmpqanNwDcTQBtIwBQUFDrf1X2AAAAD3RSTlMARCaIA/hsxAqx3l/lk+EmJf9XAAAFCklEQVRYw82Y6VbaUBCAgwIBrZ1cCFG8SiBCgLCILG5QELda3/+BOjM30ZCQwjk9xc6PkI07X2ZPNC0suWxeP8jAP5TMgZ7P5tLaekkf6niLhH8rUmZA6odrIfZ0vAy7ESn1VNwER7A7oUfNHEUMkcvDziWfC5sgp8MXiB5iSH8JATJ8+mJ/l3q7ZnFceinz/n5AkJK7RDixxu3qW0XFpZ8XuW/B1ZJpLvnKsnL1VqS9mWmaL3jixTRLcIIHsmsGcgrjYHcGcGPbl/iHhm038X7btiX9eLX7677S1uwNBq90S6VUqFYUAnxT4ZD9ADwzjHP6nRskFi5Sxt8CnqkaRhFKeHByagRyAVawOwYYCjHEGx0hHgA6Qrh44AmSW1770uWDJ0ToWnd3c19llmPxexQh0PJTIVxshWAL8Qqyj1pGqE+IGlpEKLFp7Zrab/Vlqf8ym5V8ld8pIg8hinCMy1sXhnGsEIyS3ALhWYh71i16AE94IPtoEWfRwk0H4BovDG5xM42ExiEaIR9F6OKav+QStzOFcBa2QrdYLCJepVg8JYR2kWSGruYHvxFMYhPID9y/BDwvntknHsCUb1otUBiMmSiCyc8PqKesEAphBFBmOqVfRLgL/ttn9z8IVjIS4lraKgwW7Bo0xg0Qj9uPtM6cloIowpyVyjbpKSuLb4EgUUlfom7hgOwJ0aAN+oROeRSfAt0BLhlmVVIrZUkh4MJzTAbMuJJkBAyGzQgwEKKJajHspLxHTXJApmDD1CQ6xaXmVOeEWZF9LR9DaIcWRoQrCoZEhOrHn9HgN6gbn71J4d+hDal7F6JOwdryOe1YMOgxBNRphhDaV+iXRIRKl4QOhmT8mrBddLojHIn7ogGcKi35iBxUgzEnrqONQjuIIQQKAoQqBkMigp+ewFkwko54qotGn2MSg+MROEkcKhF1uumeY3NFDrTwpKYQMBWWMLlCmTDC2DBetkDAp52i7stbMcLA92SAwGHw4CN4cYSMloG1CG+0dIUR+hgM840IVBg8rEydhej9oApEKfLuI3BM+gjDyASV0Q7kGkcsZQgBzo12IsKxhVLxC8PgGbVNhdfgXCArSB/hj47Q18fCT+vcR+BYnG9OSlRZf8Cgs0Xtml1Q53CUN5QMjz7C2nDMJ2XE5ANhzJm5EQETbihuZUO0plyAKCMk94uW2iQl5f66ulBdQThhh29EeMX2gN0SDe9RWZC+OvYBp4Xql/HSlFpXHS01NPgIGAzbIGBhcDDYMCxbrM9TbXFEjavpF2iHW0WkQMfbVFW17MInQjUZ4bM6cosiTwvVqoCbA0CPk8Bh3wStYrVNabFmTW26DDPjE2GcjMDVccZnqVFTGmImsG48djtAU0OD43AqaWqoR0MhjW+SUQRy/bF5HEI4SUZQMvELAz0qxQB5QHawLQ5sGlOa7CV3OEKcRXxkWTO4FfyC84HAwbAJAaRQ2rxgVhv6gxtFRKel9t3mKgEPbvHxtaSWvfpEmG+DQB5wlfNpSkUmNS/WeUh5VwjRqpCNDPHjgsXhVW5fnE9gYpnQLVhnNEhjCfwFM9zyctWCxa8iZ5YvS5USrwucUeBxsVioZ+306m791X/uxsBxatGi4A/xSa8yf/2CI1n+eMfHK/7+Tl+nVsrSf/Raq33Vy306/IHhqz9xkB2OdhwP8igd++SV2qkz9L2Ej36ZHVlAP0ynE7497mWPdvLpc0XpbwUKf4vqhacoAAAAAElFTkSuQmCC">
		<?php 
	}
}

global $ignitewoo_wholesale_pro_admin_settings;

$ignitewoo_wholesale_pro_settings = new IgniteWoo_Wholesale_Pro_Discounts_Admin_Settings();

