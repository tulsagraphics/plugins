<?php
/*
 * Copyright © 2014 - 2017 IgniteWoo.com - All Rights Reserved
*/
class IgniteWoo_Wholesale_Pro_Pricing {

	// Counts for rule processing comparisons
	public static $variation_counts = array();

	public static $product_counts = array();

	public static $category_counts = array();

	// Avoids multiple discount checks for a given product
	public static $processed_products = array();
	
	// Stores product rules, if any are loaded, to avoid reading the DB more than once during a given page load 
	public static $product_rules = array();
	
	// Store global rules upon first read from the DB
	public static $global_rules = false;

	public function __construct() {
	
		// After WC loads the cart update item counts used for rule processing then check all items and maybe apply discounts
		add_filter( 'woocommerce_cart_loaded_from_session', array( &$this, 'update_counts_process_rules' ), 1, 2 );
		
		// Deprecated, for now anyway.
		//add_filter( 'woocommerce_get_cart_item_from_session', array( &$this, 'update_item_counts' ), 1, 2 );
		//add_filter( 'woocommerce_get_cart_item_from_session', array( &$this, 'maybe_adjust_pricing' ), 5, 2 );
		// the on_update_cart_item_quantity() method is now renamed to update_counts_process_rules()
		//add_action( 'woocommerce_after_cart_item_quantity_update', array( &$this, 'on_update_cart_item_quantity' ), 1, 2 );
		
		// Display a discounted price if a rule was applied to lower the pricer
		add_filter( 'woocommerce_cart_item_price', array( &$this, 'cart_item_price_html' ), 900, 3 );
		
		if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) ) {
			add_filter( 'woocommerce_get_product_price', array( &$this, 'woocommerce_get_price' ), 1000, 2 );
			add_filter( 'woocommerce_product_get_price', array( &$this, 'woocommerce_get_price' ), 1000, 2 );
			add_filter( 'woocommerce_product_variation_get_price', array( &$this, 'woocommerce_get_price' ), 1000, 2 );
		} else { 
			add_filter( 'woocommerce_get_price', array( &$this, 'woocommerce_get_price' ), 1000, 2 ); // 99999999, 2
		}
		
	}

	// Retrieve the discounted price from the cart item matching the product, if there is a discount price set 
	function woocommerce_get_price( $price, $product ) {

		if ( is_admin() && !defined( 'DOING_AJAX' ) )
			return $price;

		if ( sizeof( WC()->cart->cart_contents ) <= 0 )
			return $price; 
			
		if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) )
			$pid = $product->get_id(); 
		else 
			$pid = !empty( $product->variation_id ) ? $product->variation_id : $product->id;

		foreach ( WC()->cart->cart_contents as $cart_item_key => $cart_item  ) {
				
			if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) )
				$cid = $cart_item['data']->get_id(); 
			else 
				$cid = !empty( $cart_item['data']->variation_id ) ? $cart_item['data']->variation_id : $cart_item['data']->id;
				
			if ( $pid !== $cid )
				continue;
				
			if ( !isset( $cart_item['dprice'] ) )
				continue; 
				
			$price = $cart_item['dprice'];
		}

		return $price;
	}
	
	function maybe_adjust_pricing( &$cart_item, $values = false ) {

		// "oprice" is the original price known to WooCommerce
		// "dprice" is the discounted price if rules apply a discount 
		// Using these we can track pricing and revert it to the original when rules no longer apply
		
		// Avoid duplicate processing, sometimes depending on installed software, this hook might wind up running more than once
		if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) ) {
			$pid = $cart_item['data']->get_id();
		} else { 
			if ( isset( $cart_item['data']->variation_id ) )
				$pid = $cart_item['data']->variation_id;
			else 
				$pid = $cart_item['data']->id;
		}

		// Already processed this product? 
		if ( !empty( self::$processed_products[ $pid ] ) )
			return $cart_item; 
		else 
			self::$processed_products[ $pid ] = true;

		// We need the original price known to WC to process the rules against it.
		if ( !empty( $cart_item['oprice'] ) )
			$price = $cart_item['oprice'];
		else 
			$price = $cart_item['data']->get_price();

		// Maybe adjust the price
		$result = $this->maybe_adjust_cart_item( $price, $cart_item );

		// No adjust took place based on rules? 
		if ( false === $result ) {
			unset( $cart_item['oprice'] );
			unset( $cart_item['dprice'] );
		}

		if ( false === $result ) {
			if ( !empty( $cart_item['oprice'] ) ) { 
				if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) ) {
					$cart_item['data']->set_price( $cart_item['oprice'] );
				} else { 
					$cart_item['data']->price = $cart_item['oprice'];
					$cart_item['data']->sale_price =  $cart_item['oprice'];
					$cart_item['data']->regular_price = $cart_item['oprice'];
				}
			}
			unset( $cart_item['oprice'] );
			unset( $cart_item['dprice'] );
			return $cart_item;
		}

		// Adjustments took place, so store the data and set the product price
		$cart_item['dprice'] = $result;
		$cart_item['oprice'] = $price;

		if ( function_exists( 'wc_format_decimal' ) )
			$cart_item['dprice'] = wc_format_decimal( $cart_item['dprice'] );
		else
			$cart_item['dprice'] = woocommerce_format_total ( $cart_item['dprice'] );
		
		if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) ) {
			$cart_item['data']->set_price( $cart_item['dprice'] );
		} else { 
			$cart_item['data']->price = $cart_item['dprice'];
			$cart_item['data']->sale_price =  $cart_item['dprice'];
			$cart_item['data']->regular_price = $cart_item['dprice'];
		}
//var_dump( $result, $cart_item['dprice'], $cart_item['quantity'] );
		return $cart_item;
	}

	public function reset_item_counts() {
		self::$variation_counts = array();
		self::$product_counts = array();
		self::$category_counts = array();
	}

	public function update_item_counts( $cart_item ) {
		
		$_product = $cart_item['data'];
		
		$product_id = ign_get_product_id( $_product, false );
		
		if ( $_product->is_type( 'variation' ) )
			$variation_id = ign_get_product_id( $_product, true );

		if ( !empty( $variation_id ) ) { 
			if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
				$product_id = $_product->get_parent_id();
		}

		// Product counts
		self::$product_counts[ $product_id ] = isset( self::$product_counts[ $product_id ] ) ? self::$product_counts[ $product_id ] + $cart_item['quantity'] : $cart_item['quantity'];

		// Variation counts
		if ( isset( $variation_id ) && !empty( $variation_id ) )
		    self::$variation_counts[ $variation_id ] = isset( self::$variation_counts[ $variation_id ] ) ? self::$variation_counts[ $variation_id ] + $cart_item['quantity'] : $cart_item['quantity'];

		// Category counts
		$product_categories = wp_get_post_terms( $product_id, 'product_cat' );

		foreach ( $product_categories as $category )
		    self::$category_counts[$category->term_id] = isset( self::$category_counts[$category->term_id] ) ? self::$category_counts[$category->term_id] + $cart_item['quantity'] : $cart_item['quantity'];

		return $cart_item;
	}

	public function update_counts_process_rules( $cart_item = false, $quantity = false ) {

		$this->reset_item_counts();

		$cart = WC()->cart->get_cart(); 

		// Update item counts for rule processing
		foreach ( $cart as $cart_item_key => $cart_item_data ) { 
			$this->update_item_counts( $cart_item_data );
		}

		// Maybe adjust price and store the returned data into the cart
		foreach ( $cart as $cart_item_key => $cart_item_data ) { 
			$cart_item_data = $this->maybe_adjust_pricing( $cart_item_data );
			WC()->cart->cart_contents[ $cart_item_key ] = $cart_item_data;
		}

	}
	
	// This is for displaying a table of discounts on the frontend
	function get_cart_item_discounts( $for_display = false ) {
		global $product;

		$discounts = array();
				
		$conditions_met = 0;
		
		$product_id = ign_get_product_id( $product, false );

		$pricing_rule_sets = get_post_meta( $product_id, '_dd_pricing_rules', true );

		if ( !empty( $pricing_rule_sets ) && is_array( $pricing_rule_sets ) && sizeof($pricing_rule_sets) > 0 ) {
		
			foreach ( $pricing_rule_sets as $ruleset ) { 
		
				$conditions_met = 0;
		
				$pricing_conditions = $ruleset['conditions'];

				foreach ( $pricing_conditions as $condition )
					$conditions_met = $this->handle_condition( $condition, '' );

				if ( $conditions_met )
				foreach ( $ruleset['rules'] as $key => $value ) {

					$index = $ruleset['rules'][$key]['from'] . '_' . $ruleset['rules'][$key]['to'];
					
					if ( $for_display )
						$index .=  '_' . uniqid(); 
					
					if ( 'price_discount' == $ruleset['rules'][$key]['type'] )
						$price = wc_price( $ruleset['rules'][$key]['amount'] ) . __( ' off each', 'ignitewoo_wholesale_pro' );
					else if ( 'percentage_discount' == $ruleset['rules'][$key]['type'] )
						$price = $ruleset['rules'][$key]['amount'] . __( '% off each', 'ignitewoo_wholesale_pro' );
					if ( 'fixed_price' == $ruleset['rules'][$key]['type'] )
						$price = wc_price( $ruleset['rules'][$key]['amount'] ) . __( ' each', 'ignitewoo_wholesale_pro' );
					
					if ( isset( $ruleset['variation_rules']['args'] ) ) { 
						$applies_to = array( 
							'type' => $ruleset['collector']['type'],
							'args' => $ruleset['variation_rules']['args']
						);
					
					} else {
						$applies_to = array( 
							'type' => $ruleset['collector']['type'],
							'args' => array()
						);
					}
					
					$discounts[ $index ] = array( 
						'from' => $ruleset['rules'][$key]['from'],
						'to' => $ruleset['rules'][$key]['to'],
						'amount' => $ruleset['rules'][$key]['amount'],
						'type' => $ruleset['rules'][$key]['type'],
						'string' => $price,
						'applies_to' => $applies_to
					);
				}
			}

			if ( !empty( $discounts ) ) { 
				
				$override = get_option( 'woocommerce_dd_product_rules_override', 'no' );
				
				if ( !empty( $override ) && 'yes' == $override )
					return $discounts;
			}
		}
		
		
		$sitewide_rules = get_option( '_woocommerce_sitewide_rules', false );

		if ( isset( $sitewide_rules ) && is_array( $sitewide_rules ) && !empty( $sitewide_rules ) )
		foreach ( $sitewide_rules as $pricing_rule_set ) {

			$pricing_conditions = $pricing_rule_set['conditions'];

			$collector = $this->get_collector( $pricing_rule_set );

			if ( !$collector )
				$collector = array( 'type' => 'product' );
			
			$applies = false; 

			// Global rules can be one of two types:
			switch ( $collector['type'] ) {
				// Discount based on cart line item quanity, this type of rule applies to all items 
				case 'cart_item':
					$applies = true; 
					break;
				// Discount based on specific categories of products
				case 'cat' :
					if ( isset( $collector['args'] ) && isset( $collector['args']['cats'] ) && is_array( $collector['args']['cats'] ) ) {
						foreach ( $collector['args']['cats'] as $cat ) {
							if ( is_object_in_term( $product_id, 'product_cat', $cat ) ) {
								$applies = true; 
							}
						}
					}
					break;
			}
			
			if ( !$applies )
				continue;

			if ( is_array( $pricing_conditions ) && sizeof( $pricing_conditions ) > 0 ) {

				foreach ( $pricing_conditions as $condition )
					$conditions_met = $this->handle_condition( $condition, '' );

				if ( $conditions_met ) foreach( $pricing_rule_set['rules'] as $r ) {
					
					if ( 'price_discount' == $r['type'] )
						$price = wc_price( $r['amount'] ) . __( ' off each', 'ignitewoo_wholesale_pro' );
					else if ( 'percentage_discount' == $r['type'] )
						$price = $r['amount'] . __( '% off each', 'ignitewoo_wholesale_pro' );
					if ( 'fixed_price' == $r['type'] )
						$price = wc_price( $r['amount'] ) . __( ' each', 'ignitewoo_wholesale_pro' );
					
					$discounts[ $r['from'] . '_' . $r['to'] ] = $r;
					
					$discounts[ $r['from'] . '_' . $r['to'] ]['string'] = $price;

					if ( 'cat' == $collector['type'] )
						$discounts[ $r['from'] . '_' . $r['to'] ]['applies_to'] = array( 'type' => $collector['type'], 'args' => !empty( $collector['args'] ) ? $collector['args'] : array() );
					else
						$discounts[ $r['from'] . '_' . $r['to'] ]['applies_to'] = $collector['type'];
				}
			}
		}

		return $discounts;
	}
	
	public function maybe_adjust_cart_item( $price, $cart_item ) {

		//if ( !is_array( $cart_item ) )
		//	return false;
			
		$product = $cart_item['data'];
		$product_id = ign_get_product_id( $cart_item['data'], false );
		$variation_id = ign_get_product_id( $cart_item['data'], true );

		// Per Product rules take precendence if they exist - per documentation, so load those first
		
		// If rules for this product are not loaded then load any that exist, and store them to avoid further DB reads for this product
		// Use isset() because empty() return true when an array is empty, we need to know if rules were loaded for this product 
		// even if there are not rules for the product. 
		if ( !isset( self::$product_rules[ $product_id ] ) ) { 
			self::$product_rules[ $product_id ] = get_post_meta( $product_id, '_dd_pricing_rules', true );
			$product_rule_sets = self::$product_rules[ $product_id ];
		} 

		if ( empty( self::$product_rules[ $product_id ] ) )
			self::$product_rules[ $product_id ] = array();
			
		if ( empty( $product_rule_sets ) )
			$product_rule_sets = array();
			
		// If global rules are not loaded then load them and store them to avoid further DB reads
		if ( empty( self::$global_rules ) )
			self::$global_rules = get_option( '_woocommerce_sitewide_rules', array() );

		if ( empty( self::$global_rules ) )
			self::$global_rules = array();

		if ( is_array( self::$product_rules[ $product_id ] ) && is_array( self::$global_rules ) )
			$pricing_rule_sets = array_merge( self::$product_rules[ $product_id ], self::$global_rules );

		if ( empty( $pricing_rules_set ) || sizeof( $pricing_rule_sets ) <= 0 ) 
			return false; 

		foreach ( $pricing_rule_sets as $pricing_rule_set ) {

			$execute_rules = false;
			$conditions_met = 0;

			$variation_rules = isset( $pricing_rule_set['variation_rules'] ) ? $pricing_rule_set['variation_rules'] : '';

			if ( ( $product->is_type( 'variable' ) || $product->is_type('variation') ) && $variation_rules ) {
				if ( isset( $variation_id ) && isset( $variation_rules['args']['type'] ) && $variation_rules['args']['type'] == 'variations' ) {
					if ( !is_array( $variation_rules['args']['variations'] ) || !in_array( $variation_id, $variation_rules['args']['variations'] ) )
						continue;
				}
			}

			$pricing_conditions = $pricing_rule_set['conditions'];

			$collector = $this->get_collector( $pricing_rule_set );

			if ( !$collector )
				$collector = array( 'type' => 'product' );

			if ( is_array( $pricing_conditions ) && sizeof( $pricing_conditions ) > 0 ) {

				foreach ( $pricing_conditions as $condition )
					$conditions_met += $this->handle_condition( $condition, $cart_item );

				if ( $pricing_rule_set['conditions_type'] == 'all' )
					$execute_rules = $conditions_met == count( $pricing_conditions );
					
				elseif ( $pricing_rule_set['conditions_type'] == 'any' )
					$execute_rules = $conditions_met > 0;

			} else {
				$execute_rules = true;
			}

			if ( !$execute_rules )
				continue;

			$pricing_rules = $pricing_rule_set['rules'];
			
			$original_price = $price; //$cart_item['data']->get_price();

			if ( is_callable( 'wc_get_price_excluding_tax' ) )
				$original_price_ex_tax = wc_get_price_excluding_tax( $product );
			else 
				$original_price_ex_tax = $product->get_price_excluding_tax();

			$price_adjusted = $this->get_adjusted_price( $pricing_rules, $original_price, $collector, $cart_item );

			if ( $price_adjusted !== false && floatval( $original_price ) != floatval( $price_adjusted ) ) {
				return $price_adjusted; 
			}
			
		}
	
		return false;
	}

	private function get_adjusted_price( $pricing_rules, $price, $collector, $cart_item  ) {

		$result = false;

		if ( empty( $pricing_rules ) )
			return false; 

		foreach ( $pricing_rules as $rule ) {

			//if ( 'product' == $collector['type'] && !empty( $cart_item['variation_id'] ) && is_numeric( $cart_item['variation_id'] ) )
			//	$collector['type'] = 'variation';

			$quantity = $this->get_quantity_to_compare( $cart_item, $collector );

			if ( $rule['from'] == '*' ) {
				$rule['from'] = 0;
			}

			if ( $rule['to'] == '*' ) {
				$rule['to'] = $quantity;
			}

			// Out of range? Skip it. 
			if ( $quantity < $rule['from'] || $quantity > $rule['to'] ) 
				continue; 

			switch ( $rule['type'] ) {
				case 'price_discount':
					$adjusted = floatval( $price ) - floatval( $rule['amount'] );
					$result = $adjusted >= 0 ? $adjusted : 0;

					break;
				case 'percentage_discount':
					if ( $rule['amount'] > 0 )
						$rule['amount'] = $rule['amount'] / 100;
					$result = floatval( $price ) - ( floatval( $rule['amount'] ) * $price );
					break;

				case 'fixed_price':

					$result = $rule['amount'];
					break;

				default:
					$result = false;
					break;
			}

			// First matching rule applies, so break
			break;
		}

		return $result;
	}

	private function get_collector( $pricing_rule_set  ) {
	
		if ( !isset( $pricing_rule_set['collector'] ) )
			return;
			
		return $pricing_rule_set['collector'];
	}

	private function handle_condition( $condition, $cart_item  ) {
		global $user_ID; 

		$result = 0;

		switch ( $condition['type'] ) {
			case 'apply_to':
				if ( is_array( $condition['args'] ) && isset( $condition['args']['applies_to'] ) ) {

					if ( $condition['args']['applies_to'] == 'everyone' ) {
						$result = 1;
						
					} elseif ( $condition['args']['applies_to'] == 'unauthenticated' ) {
						if ( !is_user_logged_in() ) {
							$result = 1;
						}
						
					} elseif ( $condition['args']['applies_to'] == 'authenticated' ) {
						if ( is_user_logged_in() ) {
							$result = 1;
						}
						
					} elseif ( $condition['args']['applies_to'] == 'roles' && isset( $condition['args']['roles'] ) && is_array( $condition['args']['roles'] ) ) {
						if ( is_user_logged_in() ) {
						    foreach ( $condition['args']['roles'] as $role ) {
							    if ( current_user_can( $role ) ) {
								    $result = 1;
								    break;
							    }
						    } 
						}
						
					} elseif ( $condition['args']['applies_to'] == 'users' && isset( $condition['args']['users'] ) && is_array( $condition['args']['users'] ) ) {
						if ( is_user_logged_in() ) {
						    foreach ( $condition['args']['users'] as $key => $uid ) {
							    if ( $user_ID == $uid ) {
								    $result = 1;
								    break;
							    }
						    }
						    
						}
					}
				}
				break;

			default:
				break;
		}
		
		return $result;
	}


	private function get_quantity_to_compare( $cart_item, $collector  ) {

		$quantity = 0;

		if ( is_array( $cart_item ) ) { 
			$product_id = $cart_item['product_id'];
		} else {
			$product_id = ign_get_product_id( $cart_item, false );
			//$variation_id = ign_get_product_id( $cart_item, true );
		}

		switch ( $collector['type'] ) {
			case 'cart_item':
				$quantity = $cart_item['quantity'];
				break;

			case 'cat' :
				if ( isset( $collector['args'] ) && isset( $collector['args']['cats'] ) && is_array( $collector['args']['cats'] ) ) {
					foreach ( $collector['args']['cats'] as $cat ) {
						if ( isset( self::$category_counts[$cat] ) && is_object_in_term( $product_id, 'product_cat', $cat ) ) {
						    $quantity += self::$category_counts[$cat];
						}
					}
				}
				break;

			case 'product':
				if ( isset( self::$product_counts[ $cart_item['product_id'] ] ) ) {
				    $quantity += self::$product_counts[ $cart_item['product_id'] ];
				}
				break;

			case 'variation':
				if ( isset( self::$variation_counts[ $cart_item['variation_id'] ] ) ) {
				    $quantity += self::$variation_counts[ $cart_item['variation_id'] ];
				}
				break;
		}

		return $quantity;
	}
	
	public function cart_item_price_html( $html, $cart_item, $cart_item_key ) {

		if ( empty( $cart_item['dprice'] ) || empty( $cart_item['oprice'] ) )
			return $html; 

		$price = $cart_item['oprice'];
		$discounted_price = $cart_item['dprice'] ;
		$html = '<del>' . wc_price( $price ) . '</del> <ins>' . wc_price( $discounted_price ) . '</ins>';

		return $html;
	}
}

global $ignitewoo_wsp_discounts_pricing;

$ignitewoo_wsp_discounts_pricing = new IgniteWoo_Wholesale_Pro_Pricing();
