<?php 
/*
 * Copyright (c) 2016 - IgniteWoo.com - ALL RIGHTS RESERVED
 */


/*
WARNING: This code is set up as a WC Integration class but it is not used that way... 
		It's used to draw a meta box in the related CPT. 
		
WARNING: Ensure any custom generate_XXXX_html() fields have a name prefixed with "woocommerce_ignitewoo_quick_orders_"

*/

if ( !defined( 'ABSPATH' ) ) die;

if ( !class_exists( 'WC_Integration' ) ) 
	return;
	
if ( !function_exists( 'wc_help_tip' ) ) { 
	function wc_sanitize_tooltip( $var ) {
		return htmlspecialchars( wp_kses( html_entity_decode( $var ), array(
			'br'     => array(),  
			'em'     => array(),
			'strong' => array(),
			'small'  => array(),
			'span'   => array(),
			'ul'     => array(),
			'li'     => array(),
			'ol'     => array(),
			'p'      => array(),
		) ) );
	}
	function wc_help_tip( $tip, $allow_html = false ) {
		if ( $allow_html ) {
			$tip = wc_sanitize_tooltip( $tip );
		} else {
			$tip = esc_attr( $tip );
		}

		return '<span class="woocommerce-help-tip" data-tip="' . $tip . '"></span>';
	}
}
	
class IGN_Wholesale_Pro_Quick_Order_Settings extends WC_Integration {

	function __construct() {

		$this->id = 'ignitewoo_quick_orders';

		$this->method_title = __( 'Quick Orders', 'ignitewoo_wholesale_pro' );

		$this->method_description = sprintf( __( 'Adjust the settings and roles to suit your needs.<br/>Then create a new page, insert the shortcode <code>[wsp_quick_order_form]</code>, and publish the page.<br><em>See the documentation at <a href="%s" target="_blank">IgniteWoo.com</a> for details. At the site navigate to WooCommerce Plugins -> Documentation</em>.', 'ignitewoo_wholesale_pro' ), '//ignitewoo.com/ignitewoo-software-documentation/' );
		
		$this->init_form_fields();

		$this->init_settings();

		add_action( 'woocommerce_update_options_integration_' . $this->id , array( &$this, 'process_admin_options') );
		
		//add_action( 'admin_print_scripts', array( &$this, 'admin_print_scripts' ), 999 );
		
		$this->admin_print_scripts();

	}

	function init_form_fields() {
		global $wp_roles, $wp_version;

		$cats = get_terms( 'product_cat', array( 'hide_empty' => false ) );

		$the_cats = array();
		
		if ( !is_wp_error( $cats ) )
		foreach( $cats as $k => $t ) { 
			$the_cats[ $t->term_id ] = $t->name;
		}
		
		$exc_p = array();
		
		$roles = array( 'guest' => __( 'Guest - not logged in', 'ignitewoo_wholesale_pro' ) );
		
		if ( !empty( $wp_roles->roles ) ) {
			foreach( $wp_roles->roles as $role => $data ) { 
				$roles[ $role ] = $data['name'];
			}
		}

		$this->form_fields = array(
			'disable_search' => array( 
				'type' => 'checkbox',
				'title' => __( 'Disable Search Bar', 'ignitewoo_wholesale_pro' ),
				'default' => 'no',
				'description' => __( 'Optionally disable the search bar on the quick order form page', 'ignitewoo_wholesale_pro' ),
				'desc_tip' => true,
			),
			'per_page' => array( 
				'type' => 'number',
				'title' => __( 'Products Per Page', 'ignitewoo_wholesale_pro' ),
				'class' => 'small small-text',
				'default' => 30,
				'min' => 1,
				'max' => 999999,
				'css' => 'width: 75px; min-height:26px',
			),
			'disable_paging' => array( 
				'type' => 'checkbox',
				'title' => __( 'Disable paging', 'ignitewoo_wholesale_pro' ),
				'default' => 'no',
				'description' => __( 'Optionally disable paging on sites with smaller catalogs', 'ignitewoo_wholesale_pro' ),
				'desc_tip' => true,
			),
			'show_sku' => array( 
				'type' => 'checkbox',
				'title' => __( 'Show the SKU number', 'ignitewoo_wholesale_pro' ),
				'default' => 'no',
			),
			'show_image' => array( 
				'type' => 'checkbox',
				'title' => __( 'Show the product thumbnail', 'ignitewoo_wholesale_pro' ),
				'default' => 'no',
			),
			'sort_by' => array( 
				'type' => 'select',
				'title' => __( 'Sort by', 'ignitewoo_wholesale_pro' ),
				'default' => 'title',
				'options' => array(
					'ID' => __( 'ID', 'ignitewoo_wholesale_pro' ),
					'title' => __( 'Title', 'ignitewoo_wholesale_pro' ),
					'date' => __( 'Date published', 'ignitewoo_wholesale_pro' ),
					'rand' => __( 'Random order', 'ignitewoo_wholesale_pro' ),
					'menu_order' => __( 'Menu Order', 'ignitewoo_wholesale_pro' ),
				)
			),
			'sort_order' => array( 
				'type' => 'select',
				'title' => __( 'Sort Order', 'ignitewoo_wholesale_pro' ),
				'default' => 'ASC',
				'options' => array(
					'ASC' => __( 'Ascending', 'ignitewoo_wholesale_pro' ),
					'DESC' => __( 'Decending', 'ignitewoo_wholesale_pro' ),
				)
			),
			'include_categories' => array(
				'title' 		=> __( 'Include Categories', 'ignitewoo_wholesale_pro' ), 
				'type' 			=> 'multiselect', 
				'description'		=> __( 'Products with matching categories will be included in the quick order form (note that inclusions take precedence)', 'ignitewoo_wholesale_pro' ),
				'default' 		=> '',
				'class'			=> 'quick_order_exclude_categories chosen_select select',
				'options'		=> $the_cats,
				'desc_tip' => true,
			),
			'exclude_categories' => array(
				'title' 		=> __( 'Exclude Categories', 'ignitewoo_wholesale_pro' ), 
				'type' 			=> 'multiselect', 
				'description'		=> __( 'Products with matching categories will not be included in the quick order form.', 'ignitewoo_wholesale_pro' ),
				'default' 		=> '',
				'class'			=> 'quick_order_exclude_categories chosen_select select',
				'options'		=> $the_cats,
				'desc_tip' => true,
			),
			'include_products' => array(
				'title' 	=> __( 'Included Products', 'ignitewoo_wholesale_pro'),
				'description' 	=> __( 'Products to include in the quick order form (note that inclusions take precedence)', 'ignitewoo_wholesale_pro'),
				'type' 		=> 'product_excludes',
				'options' 	=> $exc_p,
				'default' 	=> '',
				'css'		=> 'width: 75%',
				'class'		=> '',
				'desc_tip' => true,
			),
			'exclude_products' => array(
				'title' 	=> __( 'Excluded Products', 'ignitewoo_wholesale_pro'),
				'description' 	=> __( 'Products to exclude from the quick order form', 'ignitewoo_wholesale_pro'),
				'type' 		=> 'product_excludes',
				'options' 	=> $exc_p,
				'default' 	=> '',
				'css'		=> 'width: 75%',
				'class'		=> '',
				'desc_tip' => true,
			),
			/*
			'users' => array(
				'title' 	=> __( 'Allowed Users', 'ignitewoo_wholesale_pro'),
				'description' 	=> __( 'Users allowed to access the page.', 'ignitewoo_wholesale_pro'),
				'type' 		=> 'users',
				//'options' 	=> $exc_p,
				'default' 	=> '',
				'css'		=> 'width: 75%',
				'class'		=> 'chosen_select select',
				'desc_tip' => true,
			),
			*/
			'roles' => array(
				'title' 	=> __( 'Allowed Roles', 'ignitewoo_wholesale_pro'),
				'description' 	=> __( 'User roles allowed to access the page. If you leave this empty then all users can access the page.', 'ignitewoo_wholesale_pro'),
				'type' 		=> 'multiselect',
				'options' 	=> $roles,
				'default' 	=> '',
				'css'		=> 'width: 50%; height:26px',
				'class'		=> 'chosen_select select',
				'desc_tip' => true,
			),
			'access_denied_message' => array( 
				'type' => 'textarea',
				'title' => __( 'Access Denied Message', 'ignitewoo_wholesale_pro' ),
				'description' 	=> __( 'Message displayed to users that are denied access to the page', 'ignitewoo_wholesale_pro'),
				'desc_tip' => true,
				'default' => __( 'You do not have access to this page', 'ignitewoo_wholesale_pro' ),
				'min' => 1,
				'max' => 999999,
				'css' => 'width: 50%',
			),
		);
		
		if ( version_compare( $wp_version, '4.2', '>=' ) ) { 

			$this->form_fields['sort_by']['options']['meta_value_num'] = __( 'SKU', 'ignitewoo_wholesale_pro' ); // cast SKU as num to ensure numeric ordering and not alpha ordering
		}
	}

	function admin_options() { 
		global $typenow; 
		
		if ( empty( $typenow ) || 'quick_order_forms' !== $typenow )
			return;
			
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) { 
		?>
		<style>
		.woocommerce-help-tip {
			float: right; 
		}
		</style>
		<?php
		}
		?>
		
		<style>
		.quick_order_include_categories, .quick_order_exclude_categories {
			width: 50%;
		}
		</style>
		<table class="form-table">
		<?php 
		
		if ( empty( $form_fields ) ) {
			$form_fields = $this->get_form_fields();
		}

		$html = '';
		
		$cats = get_terms( 'product_cat', array( 'hide_empty' => false ) );
		if ( !empty( $cats ) && !is_wp_error( $cats ) ) { 
			foreach( $cats as $c ) 
				$the_cats[ $c->term_id ] = $c->name;
		
		} else { 
			$the_cats = array();
		}
		

		foreach ( $form_fields as $k => $v ) {

			if ( ! isset( $v['type'] ) || ( $v['type'] == '' ) ) {
				$v['type'] = 'text'; // Default to "text" field type.
			}

			if ( 'exclude_categories' == $k || 'include_categories' == $k ) 
				$v['options'] = $the_cats;
				
			if ( method_exists( $this, 'generate_' . $v['type'] . '_html' ) ) {
				$html .= $this->{'generate_' . $v['type'] . '_html'}( $k, $v );
			} else {
				$html .= $this->{'generate_text_html'}( $k, $v );
			}
		}

		echo $html;
		
		?>
		</table>

		<?php
	}
	
	
	function generate_product_excludes_html( $key, $args ) { 

		ob_start();
		?>
		<tr valign="top">
		
			<th scope="row" class="titledesc">
				<label><?php echo $args['title'] ?></label>
				
				<?php 
				if ( 'exclude_products' == $key ) 
					echo wc_help_tip( __( 'Selected products will not appear in the quick order form', 'ignitewoo_wholesale_pro' ) ); 
				else 
					echo wc_help_tip( __( 'Selected products will appear in the quick order form', 'ignitewoo_wholesale_pro' ) );
				?>
			</th>
			<td>
				<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) { ?>
				
				<select class="wsp-product-search" multiple="multiple" style="width: 50%;" name="woocommerce_ignitewoo_quick_orders_<?php echo $key ?>[]" data-placeholder="<?php esc_attr_e( 'Search for a product&hellip;', 'ignitewoo_wholesale_pro' ); ?>" data-action="ign_search_products">
					<?php 
					$product_ids = array_filter( array_map( 'absint', $this->settings[ $key ] ) );
						foreach ( $product_ids as $product_id ) {
							$product = wc_get_product( $product_id );
							if ( is_object( $product ) ) {
								?>
								<option value="<?php echo $product_id ?>" selected="selected"><?php echo wp_kses_post( $product->get_formatted_name() ); ?></option>
								<?php
							}
						}	
					?>
				</select>
				
				<?php } else { ?>
				
					<input type="hidden" class="wsp-product-search" data-multiple="true" style="width: 50%;" name="woocommerce_ignitewoo_quick_orders_<?php echo $key ?>" data-placeholder="<?php esc_attr_e( 'Search for a product&hellip;', 'ignitewoo_wholesale_pro' ); ?>" data-action="ign_search_products" data-selected="<?php
					$product_ids = array_filter( array_map( 'absint', explode( ',', $this->settings[ $key ] ) ) );
					$json_ids    = array();

					foreach ( $product_ids as $product_id ) {
						$product = wc_get_product( $product_id );
						if ( is_object( $product ) ) {
							$json_ids[ $product_id ] = wp_kses_post( $product->get_formatted_name() );
						}
					}

					echo esc_attr( json_encode( $json_ids ) ); ?>" value="<?php echo implode( ',', array_keys( $json_ids ) ); ?>" /> 
				<?php } ?>
			</td>
		<?php
		return ob_get_clean();
	}

	function generate_users_html() { 
		global $woocommerce, $wpdb;

		ob_start();

		if ( !empty( $this->settings['users'] ) )
			$the_users = $this->settings['users'];
		else 
			$the_users = array();

		?>
		<tr valign="top">
			<th scope="row" class="titledesc">
			
				<label><?php _e( 'Allowed Users', 'ignitewoo_wholesale_pro' ) ?></label>
				<img height="16" width="16" src="<?php echo WC()->plugin_url() ?>/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Select which users can access the manual order page. Administrators and Shop Managers always have access', 'ignitewoo_wholesale_pro' ) ?>">
			</th>
			<td class="forminp">
				<fieldset>
				<select class="select_drop_users" name="woocommerce_ignitewoo_quick_orders_users[]" style="width:200px; height:40px" data-placeholder="<?php _e( 'Select users', 'ignitewoo_wholesale_pro' ) ?>" multiple>
				
					<?php if ( !empty( $the_users ) ) foreach( $the_users as $key => $val ) { ?>

						<?php 
						if ( isset( $this->settings['users'] ) && in_array( $val, $this->settings['users'] ) ) { 
							$selected = 'selected="selected"';
							$u = get_user_by( 'id', $val );
							$txt = $u->data->user_login . ' (#' . $val . ' ' . $u->data->user_email . ')';
						} else { 
							$selected = '';
						}	
						?>
					
						<option value="<?php echo $val ?>" <?php echo $selected ?>>
							<?php echo $txt ?>
						</option>
						
					<?php } else { ?>
					
						<option value=""></option>
					
					<?php } ?>

				</select>
				</fieldset>
				
			</td>
		</tr>
		<?php 
		
		return ob_get_clean();
	}
	
	function admin_print_scripts() { 

		$js_dir = str_replace( array( 'http:', 'https:' ), '', trailingslashit( ( plugins_url( '/../assets/js/', __FILE__ ) ) ) );

		wp_register_script( 'ign-product-search', $js_dir . 'product-search.js', array( 'jquery' ), WOOCOMMERCE_VERSION, true );
		
		wp_enqueue_script( 'ign-product-search' );
		
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
			$v = 'true';
		else 
			$v = 'false';
			
		$args = array(
			'wc_version' => WOOCOMMERCE_VERSION,
			'wc_version_gte_27' => $v,
		);
		
		wp_localize_script( 'ign-product-search', 'ign_search_products', $args );
	
		/*
		$js = "
		jQuery( 'select.select_drop_users' ).ajaxChosen({
				method:         'GET',
				url:            '" . admin_url( 'admin-ajax.php' ) . "',
				dataType:       'json',
				afterTypeDelay: 100,
				minTermLength:  2,
				data:           {
					action:   'woocommerce_json_search_customers',
					security: '" . wp_create_nonce( 'search-customers' ) . "'
				}
			}, function ( data ) {

				var terms = {};

				$.each( data, function ( i, val ) {
					terms[i] = val;
				});

				return terms;
			});	
		";
		
		wc_enqueue_js( $js );
		
		?>
		<script>
		
		jQuery( document ).ready( function() { 
			jQuery( '.select_drop, .select_drop_roles' ).chosen();
		})
		
		</script>

		<?php 
		*/
	}
	
	function process_admin_options() { 

		parent::process_admin_options();

		/*
		if ( isset( $_POST['users'] ) )
			$this->settings['users'] = array_unique( $_POST['users'] );
		else 
			$this->settings['users'] = array();
		*/

		if ( isset( $_POST['exclude_products'] ) )
			$this->settings['exclude_products'] = $_POST['exclude_products'];
		else 
			$this->settings['exclude_products'] = array();

		update_option( 'woocommerce_' . $this->id . '_settings', $this->settings );
	}
}
