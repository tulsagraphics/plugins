<?php 
/**
Copyright © 2014 IgniteWoo.com - All Rights Reserved
*/

if ( !defined( 'ABSPATH' ) ) die;

class IGN_WSP_Min_Max_Quantities_Admin {

	function __construct() {

		// Variations
		add_action( 'woocommerce_variation_options', array( &$this, 'variation_options' ), 10, 2 );
		add_action( 'woocommerce_product_after_variable_attributes', array( &$this, 'variation_panel' ), 10, 3 );

		// Meta
		add_action( 'woocommerce_product_options_general_product_data', array( &$this, 'write_panel' ) );
		add_action( 'woocommerce_process_product_meta', array( &$this, 'write_panel_save' ) );
		
		add_action( 'woocommerce_ajax_save_product_variations', array( &$this, 'write_panel_save' ), 5 );
		
		add_action( 'admin_footer', array( &$this, 'admin_footer' ) );
		
		//add_filter( 'woocommerce_screen_ids', array( $this, 'screen_ids' ) );
		
	}

	function admin_footer() { 
		wp_localize_script( 'wc-enhanced-select', 'wc_enhanced_select_params', array(
			'i18n_matches_1'            => _x( 'One result is available, press enter to select it.', 'enhanced select', 'ignitewoo_wholesale_pro' ),
			'i18n_matches_n'            => _x( '%qty% results are available, use up and down arrow keys to navigate.', 'enhanced select', 'ignitewoo_wholesale_pro' ),
			'i18n_no_matches'           => _x( 'No matches found', 'enhanced select', 'ignitewoo_wholesale_pro' ),
			'i18n_ajax_error'           => _x( 'Loading failed', 'enhanced select', 'ignitewoo_wholesale_pro' ),
			'i18n_input_too_short_1'    => _x( 'Please enter 1 or more characters', 'enhanced select', 'ignitewoo_wholesale_pro' ),
			'i18n_input_too_short_n'    => _x( 'Please enter %qty% or more characters', 'enhanced select', 'ignitewoo_wholesale_pro' ),
			'i18n_input_too_long_1'     => _x( 'Please delete 1 character', 'enhanced select', 'ignitewoo_wholesale_pro' ),
			'i18n_input_too_long_n'     => _x( 'Please delete %qty% characters', 'enhanced select', 'ignitewoo_wholesale_pro' ),
			'i18n_selection_too_long_1' => _x( 'You can only select 1 item', 'enhanced select', 'ignitewoo_wholesale_pro' ),
			'i18n_selection_too_long_n' => _x( 'You can only select %qty% items', 'enhanced select', 'ignitewoo_wholesale_pro' ),
			'i18n_load_more'            => _x( 'Loading more results&hellip;', 'enhanced select', 'ignitewoo_wholesale_pro' ),
			'i18n_searching'            => _x( 'Searching&hellip;', 'enhanced select', 'ignitewoo_wholesale_pro' ),
			'ajax_url'                  => admin_url( 'admin-ajax.php' ),
			'search_products_nonce'     => wp_create_nonce( 'search-products' ),
			'search_customers_nonce'    => wp_create_nonce( 'search-customers' )
		) );
	}
	
	public function screen_ids( $screen_ids ) {

		$screen_ids[] = 'product';

		return $screen_ids;
	}

	function write_panel() {
		global $woocommerce, $post, $wp_roles;

		if ( !isset( $wp_roles ) )
		    $wp_roles = new WP_Roles();

		$all_roles = $wp_roles->roles;
		?>
		<div class="options_group">

			<style>
			.woocommerce_options_panel .rule_table .chosen-container-multi { width: 300px !important; clear:both; }
			.qty_labels { margin: 0px !important; width: auto !important; float: left !important; padding-bottom: 5px !important; }
			.min_max_roles_select { clear:both; float:left; }
			.alt { background: #f7f7f7 !important; }
			</style>
			
			<p class=""><?php _e( 'Quantity Rules','ignitewoo_wholesale_pro' ) ?></p>

			<?php 
			woocommerce_wp_checkbox( array( 'id' => 'minmax_do_not_count', 'label' => __( 'Do not count', 'ignitewoo_wholesale_pro' ), 'description' => __( 'Don\'t count this product against the minimum order quantity & total rules.', 'ignitewoo_wholesale_pro' ) ) );

			woocommerce_wp_checkbox( array( 'id' => 'minmax_cart_exclude', 'label' => __( 'Exclude', 'ignitewoo_wholesale_pro' ), 'description' => __( 'Exclude this product from minimum order quantity & total rules. If this is the only item in the cart, rules will not apply.', 'ignitewoo_wholesale_pro' ) ) );
			?>

			<p style="margin-bottom:0.5em"><?php _e( 'The first matching rule always applies. Drag and drop the rules to rearrange the order.', 'ignitewoo_wholesale_pro' ) ?></p>
			<table class="widefat rule_table">
				<thead>
					<tr class="header">
					<th style="width:26px"></th>
					<th><?php _e( 'Rules', 'ignitewoo_wholesale_pro' ) ?><span class="dashicons dashicons-editor-help help_tip" data-tip="<?php _e( 'Rules with no users or roles apply to all non-logged-in shoppers', 'ignitewoo_wholesale_pro' ) ?>" style="float:none; margin-left: 5px; color: #333;font-size:18px"></th>
					<?php /*
					<th style="width:20%"><?php _e( 'Min Qty', 'ignitewoo_wholesale_pro' ) ?></th>
					<th style="width:20%"><?php _e( 'Max Qty', 'ignitewoo_wholesale_pro' ) ?></th>
					<th style="width:20%"><?php _e( 'Increments', 'ignitewoo_wholesale_pro' ) ?></th>
					*/ ?>
					</tr>
				</thead>
				<tbody>
					<tr class="template" style="display:none">
						<td>
							<input type="checkbox" class="remove" value="" name="remove">
							<input type="hidden" value="x" name="min_max_rules[rule_index][]">
						</td>
						<td style="overflow:visible">
							<div style="width:47%;float:left;margin-right:1em"> 
								<label class="qty_labels"><?php _e( 'Roles', 'ignitewoo_wholesale_pro' ) ?></label>
								<select name="min_max_rules[roles][][]" class="select2 min_max_roles_select" multiple="multiple" style="float:none;width:98%">
								
									<option value=""><?php echo '&nbsp;' ?></option>
									
									<?php foreach( $all_roles as $role_id => $role ) { ?>
									
									<?php $role_checked = (isset( $condition['args']['roles'] ) && is_array( $condition['args']['roles'] ) && in_array( $role_id, $condition['args']['roles'] )) ? 'selected="selected"' : ''; ?>
						
									<option <?php echo $role_checked ?> value="<?php echo $role_id ?>">
										<?php echo $role['name']; ?>
									</option>
									
									<?php } ?>
								</select>
							</div>
							<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '<' ) ) { ?>
							<div class="select select2 select_users" style="width:47%; float:left">
								<label class="qty_labels"><?php _e( 'Users', 'ignitewoo_wholesale_pro' ) ?> </label>
								<input type="hidden" class="min_max_users" name="min_max_rules[users][]" data-placeholder="<?php _e( 'Search for a user&hellip;', 'ignitewoo_wholesale_pro' ); ?>" data-selected='' value="" data-multiple="true" data-allow_clear="true" data-action="woocommerce_json_search_customers" style="width:100%;min-height:32px"/>
							</div>
							<?php } else { ?>
							<div style="width:47%; float:left">
								<label class="qty_labels"><?php _e( 'Users', 'ignitewoo_wholesale_pro' ) ?></label>
								<select name="min_max_rules[users][][]" class="ajax_select_customer" multiple="multiple" style="float:none;width:98%">
								</select>
							</div>
							<?php } ?>
							<div style="clear:both; width:30%; float:left; margin-top: 0.5em">
								<label class="qty_labels"><?php _e( 'Min Qty', 'ignitewoo_wholesale_pro' ) ?></label> <input type="number" step="1" class="" style="width:100px;float:left;clear:both" value="" name="min_max_rules[min][]">
							</div>
							<div style="width:30%; float:left;margin-top:0.5em">
								<label class="qty_labels"><?php _e( 'Max Qty', 'ignitewoo_wholesale_pro' ) ?></label> <input type="number" step="1" class="" value="" style="width:100px;float:left;clear:both" name="min_max_rules[max][]">
							</div>
							<div style="width:30%; float:left;margin-top:0.5em">
								<label class="qty_labels"><?php _e( 'Increments', 'ignitewoo_wholesale_pro' ) ?></label> <input type="number" step="1" class="" style="width:100px;float:left;clear:both" value="" name="min_max_rules[inc][]">
							</div>
						</td>
					</tr>
					<?php 
					$rules = get_post_meta( $post->ID, 'min_max_rules', true );
					
					if ( empty( $rules['rule_index'] ) ) {
						?>
						<tr class="no_rules_row">
							<td colspan="5"><?php _e( 'No rules defined for this product','ignitewoo_wholesale_pro' ) ?></td>
						</tr>
						<?php 
					} else { 
					
					foreach( $rules['rule_index'] as $i => $vals ) { ?>
					
						<?php 
						$x = $i;
						?>
					
					<tr>
						<td>
							<input type="checkbox" class="remove" value="" name="remove">
							<input type="hidden" value="<?php echo $x ?>" name="min_max_rules[rule_index][<?php echo $x ?>]">
						</td>
						<td style="overflow:visible">
						
							<div style="width:47%;margin-bottom:0.5em;float:left"> 
							<label class="qty_labels"><?php _e( 'Roles', 'ignitewoo_wholesale_pro' ) ?></label>
							<select name="min_max_rules[roles][<?php echo $x ?>][]" class="select2 min_max_roles_select" multiple="multiple" style="width:98%">
							
								<option value=""><?php echo '&nbsp;'?></option>
								
								<?php foreach( $all_roles as $role_id => $role ) { ?>
								
								<?php $role_checked = in_array( $role_id, $rules['roles'][$x] ) ? 'selected="selected"' : ''; ?>
					
								<option <?php echo $role_checked ?> value="<?php echo $role_id ?>">
									<?php echo $role['name']; ?>
								</option>
								
								<?php } ?>
							</select>
							</div>
							
							<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '<' ) ) { ?>
							
							<div class="select select2 select_users" style="width:47%; float:left">
								<label class="qty_labels"><?php _e( 'Users', 'ignitewoo_wholesale_pro' ) ?></label> 
								<?php
								if ( isset( $rules['users'][$x] ) && is_array( $rules['users'][$x] ) )
									$rules['users'][$x] = implode( ',', $rules['users'][$x] );
									
								if ( empty( $rules['users'][$x] ) )
									$rules['users'][$x] = '';
									
								$user_strings = array();
								$user_id = '';
								if ( isset( $rules['users'][$x] ) && !empty( $rules['users'][$x] ) ) {
									$rules_users = explode( ',' , $rules['users'][$x] );
									if ( !empty( $rules_users ) )
									foreach( $rules_users as $pu ) { 
										if ( empty( $pu ) )
											continue;
										$user = get_user_by( 'id', $pu);
										if ( empty( $user ) || is_wp_error( $user ) )
											continue;
										$user_strings[ $pu ] = esc_html( $user->display_name ) . ' (#' . absint( $user->ID ) . ' &ndash; ' . esc_html( $user->user_email ) . ')';
									}
								}
								?>
								<br/>
								<input type="hidden" class="min_max_users" name="min_max_rules[users][<?php echo $x ?>]" data-placeholder="<?php _e( 'Search for a user&hellip;', 'ignitewoo_wholesale_pro' ); ?>" data-selected='<?php echo esc_attr( json_encode( $user_strings ) ) ?>' value="<?php echo $rules['users'][$x] ?>" data-multiple="true" data-allow_clear="true" data-action="woocommerce_json_search_customers" style="min-width:100%;min-height:32px"/>
							</div>
							
							<?php } else { ?>
							
							<div style="width:47%; float:left">
								<label class="qty_labels"><?php _e( 'Users', 'ignitewoo_wholesale_pro' ) ?></label>
								<select name="min_max_rules[users][<?php echo $x ?>][]" class="ajax_select_customer" multiple="multiple" style="width:100%">
									<?php 
									if ( !empty( $rules['users'][$x] ) )
									foreach( $rules['users'][$x] as $u ) {
										$user = get_user_by( 'id', $u );
										echo '<option selected="selected" value="' . esc_attr( $user->ID ) . '" ' . selected( 1, 1, false ) . '>' . esc_html( $user->display_name ) . ' (#' . absint( $user->ID ) . ' &ndash; ' . esc_html( $user->user_email ) . ')</option>';
									}
									?>
								</select>
							</div>
							
							<?php } ?>
							
							<div style="clear:both; width:30%; float:left; margin-top: 0.5em">
							<label class="qty_labels"><?php _e( 'Min Qty', 'ignitewoo_wholesale_pro' ) ?></label> <input type="number" step="1" class="" style="width:100px;float:left;clear:both" value="<?php echo $rules['min'][$x] ?>" name="min_max_rules[min][<?php echo $x ?>]">
							</div>
							<div style="width:30%; float:left;margin-top:0.5em">
							<label class="qty_labels"><?php _e( 'Max Qty', 'ignitewoo_wholesale_pro' ) ?></label> <input type="number" step="1" class="" style="width:100px;float:left;clear:both" value="<?php echo $rules['max'][$x] ?>" name="min_max_rules[max][<?php echo $x ?>]">
							</div>
							<div style="width:30%; float:left;margin-top:0.5em">
							<label class="qty_labels"><?php _e( 'Increments', 'ignitewoo_wholesale_pro' ) ?></label> <input type="number" step="1" class="" style="width:100px;float:left;clear:both" value="<?php echo $rules['inc'][$x] ?>" name="min_max_rules[inc][<?php echo $x ?>]">
							</div>
						</td>
					</tr>

					<?php } ?>
					
					<?php } ?>
				</tbody>
			</table>
			<div style="margin-top: 0.5em; margin-bottom:1em;padding-left: 12px">
				<button id="minmax_add_rule" class="button"><?php _e( 'Add new rule', 'ignitewoo_wholesale_pro' ) ?></button>
				
				<button id="minmax_remove_rule" class="button" style="margin-left: 1.5em"><?php _e( 'Remove selected rules', 'ignitewoo_wholesale_pro' ) ?></button>
			</div>
			<script>

			jQuery( document ).ready( function( $ ) { 

				$( document ).on( 'woocommerce_variations_loaded', function() { qty_rules_init() } )
			
				function qty_rules_init() { 

					$( '.rule_table' ).sortable({
						items: 'tr',
						cursor: 'move',
						scrollSensitivity: 40,
						//forcePlaceholderSize: false,
						helper: function(e, tr) {
								var $originals = tr.children();
								var $helper = tr.clone();
								$helper.children().each(function(index) {
								$(this).width($originals.eq(index).width());
								});
							return $helper;
						},
						opacity: 0.65,
						stop: function() { 
							renumber_fields();
							$( '.rule_table tbody tr:odd' ).not( '.template' ).each( function() {
								$( this ).addClass( 'alt' );
							});
							$( '.rule_table tbody tr:even' ).not( '.template' ).each( function() {
								$( this ).removeClass( 'alt' );
							});
						}
					});
					$( '.var_rule_table' ).sortable({
						items: 'tr',
						cursor: 'move',
						scrollSensitivity: 40,
						//forcePlaceholderSize: false,
						helper: function(e, tr) {
								var $originals = tr.children();
								var $helper = tr.clone();
								$helper.children().each(function(index) {
								$(this).width($originals.eq(index).width());
								});
							return $helper;
						},
						opacity: 0.65,
						stop: function() { 
							renumber_var_fields(this);
							$( '.var_rule_table tbody tr:odd' ).not( '.template' ).each( function() {
								$( this ).addClass( 'alt' );
							});
							$( '.var_rule_table tbody tr:even' ).not( '.template' ).each( function() {
								$( this ).removeClass( 'alt' );
							});
						}
					});
					$( '#minmax_add_rule' ).on( 'click', function(e) { 
						e.preventDefault();
						var row = $( '.rule_table .template' ).clone();
						row.removeClass( 'template' );
						$( '.rule_table' ).append( '<tr>' + row.html() + '</tr>' );
						$( '.rule_table' ).find( 'tr.no_rules_row' ).remove();
						
						<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.2', '>=' ) ) { ?>
							$( '.rule_table tr:last-child' ).find( 'select.select2' ).addClass( 'enhanced' ).select2();
							
								wc_min_max_users_select();
							
							
						<?php } else { ?>
							$( '.rule_table tr:last-child' ).find( 'select.select2' ).addClass( 'enhanced' ).chosen();
							min_max_user_search();
							
						<?php } ?>
						
						renumber_fields();
						
						return false;
					})
					
					$( '.rule_table tr' ).not( '.template' ).each( function() { 
						<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.2', '>=' ) ) { ?>
						$( this ).find( 'select.select2' ).addClass( 'enhanced' ).select2();
						<?php } else { ?>
						$( this ).find( 'select.select2' ).addClass( 'enhanced' ).chosen();
						<?php } ?>
						
					});
					
									
					$( '.rule_table tbody tr:odd' ).not( '.template' ).each( function() {
						$( this ).addClass( 'alt' );
					});
					
					
					$( '#minmax_remove_rule' ).on( 'click', function(e) { 
						e.preventDefault();
						if ( !confirm( '<?php _e( 'Remove selected rules?', 'ignitewoo_wholesale_pro' ) ?>' ) )
							return false;
						$( '.rule_table tr' ).each( function() { 
							if ( $( this ).find( '.remove' ).is( ':checked' ) )
								$( this ).remove();
						})
						renumber_fields();
						return false;
					});
								
					
					$( '.var_rule_table tbody tr:odd' ).not( '.template' ).each( function() {
						$( this ).addClass( 'alt' );
					});

					function renumber_fields( ) {
						var field_index = 1;
						$( '.rule_table tbody tr' ).not( '.template' ).not( '.header' ).each( function() { 
						
							$( this ).find( "input[type='number']" ).each( function( index, element ) {
								var pos = nth_occurrence( element.name, '[', 2 );
								element.name = element.name.substr( 0,  pos  );
								element.name = element.name + '[' + field_index + ']';
							});
							$( this ).find( "input[type='hidden']" ).each( function( index, element ) {
								var pos = nth_occurrence( element.name, '[', 2 );
								element.name = element.name.substr( 0,  pos  );
								element.name = element.name + '[' + field_index + ']';
							});
							$( this ).find( "select.select2" ).each( function( index, element ) {
								var pos = nth_occurrence( element.name, '[', 2 );
								element.name = element.name.substr( 0, pos );
								element.name = element.name + '[' + field_index + '][]';
							});

							field_index = parseInt( field_index ) + 1;
						});
						$( '.rule_table tbody tr:even' ).not( '.template' ).not( '.header' ).each( function() {
							$( this ).removeClass( 'alt' );
						});
						$( '.rule_table tbody tr:odd' ).not( '.template' ).not( '.header' ).each( function() {
							$( this ).addClass( 'alt' );
						});
					}

					$( '.var_rule_table tr' ).not( '.template' ).each( function() { 
						<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.2', '>=' ) ) { ?>
						$( this ).find( 'select.select2' ).addClass( 'enhanced' ).select2();
						<?php } else { ?>
						$( this ).find( 'select.select2' ).addClass( 'enhanced' ).chosen();
						<?php } ?>
						
					});

					$( '.var_minmax_add_rule' ).on( 'click', function(e) { 

						e.preventDefault();
						var row = $( this ).closest( '.woocommerce_variable_attributes' ).find( '.var_rule_table tr.template' ).clone();
						row.removeClass( 'template' );
						$( this ).closest( '.woocommerce_variable_attributes' ).find( '.var_rule_table' ).append( '<tr>' + row.html() + '</tr>' );
						$( this ).closest( '.woocommerce_variable_attributes' ).find( '.var_rule_table tr.no_rules_row' ).remove();
						<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.2', '>=' ) ) { ?>
							$( this ).closest( '.woocommerce_variable_attributes' ).find( '.var_rule_table tr:last-child' ).find( 'select.select2' ).addClass( 'enhanced' ).select2();
							wc_min_max_users_select();
						<?php } else { ?>
							$( this ).closest( '.woocommerce_variable_attributes' ).find('.var_rule_table tr:last-child' ).find( 'select.select2' ).addClass( 'enhanced' ).chosen();
							min_max_user_search();
						<?php } ?>
						
						renumber_var_fields( this );
						return false;
					})
					$( '.var_minmax_remove_rule' ).on( 'click', function(e) { 
						e.preventDefault();
						if ( !confirm( '<?php _e( 'Remove selected rules?', 'ignitewoo_wholesale_pro' ) ?>' ) )
							return false;
						$( this ).closest( '.woocommerce_variable_attributes' ).find( '.var_rule_table tr' ).each( function() { 
							if ( $( this ).find( '.remove' ).is( ':checked' ) )
								$( this ).remove();
						})
						renumber_var_fields( this );
						return false;
					});
					
					function renumber_var_fields( item ) {
						var field_index = 1;
						$( item ).closest( '.woocommerce_variable_attributes' ).find( '.var_rule_table tr' ).not( '.template' ).not( '.header' ).each( function() { 
							$( this ).find( "input[type='number']" ).each( function( index, element ) {
								var pos = nth_occurrence( element.name, '[', 3 );
								element.name = element.name.substr( 0,  pos  );
								element.name = element.name + '[' + field_index + ']';					
								
							});
							$( this ).find( "input[type='hidden']" ).each( function( index, element ) {
								var pos = nth_occurrence( element.name, '[', 3 );
								element.name = element.name.substr( 0,  pos  );
								element.name = element.name + '[' + field_index + ']';
							});
							$( this ).find( "select.select2" ).each( function( index, element ) {
								var pos = nth_occurrence( element.name, '[', 3 );
								element.name = element.name.substr( 0, pos );
								element.name = element.name + '[' + field_index + '][]';
							});

							field_index = parseInt( field_index ) + 1;
						});
						$( item ).closest( '.woocommerce_variable_attributes' ).find( '.var_rule_table tr:even' ).not( '.template').not( '.header' ).each( function() {
							$( this ).addClass( 'alt' );
						});
						$( item ).closest( '.woocommerce_variable_attributes' ).find( '.var_rule_table tr:odd' ).not( '.template').not( '.header' ).each( function() {
							$( this ).removeClass( 'alt' );
						});
					}
					function nth_occurrence (string, char, nth) {
						var first_index = string.indexOf( char );
						var length_up_to_first_index = first_index + 1;

						if ( nth == 1 ) {
							return first_index;
						} else {
							var string_after_first_occurrence = string.slice(length_up_to_first_index);
							var next_occurrence = nth_occurrence(string_after_first_occurrence, char, nth - 1);

							if (next_occurrence === -1) {
								return -1;
							} else {
								return length_up_to_first_index + next_occurrence;  
							}
						}
					}
										
					function cust_users_format_string() {
						var formatString = {
							formatMatches: function( matches ) {
								if ( 1 === matches ) {
									return wc_enhanced_select_params.i18n_matches_1;
								}

								return wc_enhanced_select_params.i18n_matches_n.replace( '%qty%', matches );
							},
							formatNoMatches: function() {
								return wc_enhanced_select_params.i18n_no_matches;
							},
							formatAjaxError: function( jqXHR, textStatus, errorThrown ) {
								return wc_enhanced_select_params.i18n_ajax_error;
							},
							formatInputTooShort: function( input, min ) {
								var number = min - input.length;

								if ( 1 === number ) {
									return wc_enhanced_select_params.i18n_input_too_short_1;
								}

								return wc_enhanced_select_params.i18n_input_too_short_n.replace( '%qty%', number );
							},
							formatInputTooLong: function( input, max ) {
								var number = input.length - max;

								if ( 1 === number ) {
									return wc_enhanced_select_params.i18n_input_too_long_1;
								}

								return wc_enhanced_select_params.i18n_input_too_long_n.replace( '%qty%', number );
							},
							formatSelectionTooBig: function( limit ) {
								if ( 1 === limit ) {
									return wc_enhanced_select_params.i18n_selection_too_long_1;
								}

								return wc_enhanced_select_params.i18n_selection_too_long_n.replace( '%qty%', limit );
							},
							formatLoadMore: function( pageNumber ) {
								return wc_enhanced_select_params.i18n_load_more;
							},
							formatSearching: function() {
								return wc_enhanced_select_params.i18n_searching;
							}
						};

						return formatString;
					} 
					function wc_min_max_users_select() { 

						$( '.rule_table tr, .var_rule_table tr' ).not( '.template' ).each( function() {

							<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '<' ) ) { ?>

							$( this ).find( 'input.min_max_users' ).not( '.enhanced' ).each( function() { 

								var ign_select2_args = {
									allowClear:  $( this ).data( 'allow_clear' ) ? true : false,
									placeholder: $( this ).data( 'placeholder' ),
									minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '3',
									escapeMarkup: function( m ) {
										return m;
									},
									ajax: {
										url: '<?php echo admin_url( 'admin-ajax.php' ) ?>',
										dataType: 'json',
										quietMillis: 250,
										data: function( term, page ) {
											return {
												term: term,
												action: 'woocommerce_json_search_customers',
												security: '<?php echo wp_create_nonce( 'search-customers' ) ?>',
											};
										},
										results: function( data, page ) {
											var terms = [];
											if ( data ) {
												$.each( data, function( id, text ) {
													terms.push( { id: id, text: text } );
												});
											}
											return { results: terms };
										},
										cache: true
									}
								};
								
								if ( $( this ).data( 'multiple' ) === true ) {
									ign_select2_args.multiple = true;
									ign_select2_args.initSelection = function( element, callback ) {
										var data     = $.parseJSON( element.attr( 'data-selected' ) );
										var selected = [];

										$( element.val().split( "," ) ).each( function( i, val ) {
											selected.push( { id: val, text: data[ val ] } );
										});

										return callback( selected );
									};

									ign_select2_args.formatSelection = function( data ) {
										return '<div class="selected-option" data-id="' + data.id + '">' + data.text + '</div>';
									};
								} 
								
								ign_select2_args = $.extend( ign_select2_args, cust_users_format_string() );
								
								$( this ).select2( ign_select2_args ).addClass( 'enhanced' );
							})
							
							<?php } else { ?>
 
							$( this ).find( 'select.ajax_select_customer' ).not( '.enhanced' ).each( function() { 

								var ign_select2_args = {
									allowClear:  $( this ).data( 'allow_clear' ) ? true : false,
									placeholder: $( this ).data( 'placeholder' ),
									minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '3',
									escapeMarkup: function( m ) {
										return m;
									},
									ajax: {
										url: '<?php echo admin_url( 'admin-ajax.php' ) ?>',
										dataType:    'json',
										quietMillis: 250,
										data: function( params ) {
											return {
												term:     params.term,
												action:   'woocommerce_json_search_customers',
												security: '<?php echo wp_create_nonce( 'search-customers' ) ?>',
												exclude:  $( this ).data( 'exclude' )
											};
										},
										processResults: function( data ) {
											var terms = [];
											if ( data ) {
												$.each( data, function( id, text ) {
													terms.push({
														id: id,
														text: text
													});
												});
											}
											return {
												results: terms
											};
										},
										cache: true
									}
								};
								
								//ign_select2_args = $.extend( ign_select2_args, cust_users_format_string() );
								
								$( this ).select2( ign_select2_args ).addClass( 'enhanced' );
							})
							
							<?php } ?>
						})
					}
					
					wc_min_max_users_select()
					
				}
			
			qty_rules_init();
			
			})
			</script>

		</div>
		
		<?php 

		wc_enqueue_js( "
			jQuery('.checkbox.min_max_rules').on( 'change', function() {

				if ( jQuery(this).is( ':checked' ) ) {

					jQuery(this).closest('.woocommerce_variation').find( 'tr.min_max_rules' ).show();

				} else {

					jQuery(this).closest('.woocommerce_variation').find( 'tr.min_max_rules' ).hide();

				}

			}).change();
		" );
	}

	function write_panel_save( $post_id ) {

		$rules = isset( $_POST['min_max_rules'] ) ? $_POST['min_max_rules'] : false;

		foreach( $rules as $s => $v ) {

			if ( is_int( $s ) )
				unset( $rules[ $s ] );
			else if ( isset( $v[0] ) ) {
				unset( $rules[ $s ][0] );
			}
		}

		update_post_meta( $post_id, 'min_max_rules', $rules );
	
		update_post_meta( $post_id, 'minmax_do_not_count', empty( $_POST['minmax_do_not_count'] ) ? 'no' : 'yes' );

		update_post_meta( $post_id, 'minmax_cart_exclude', empty( $_POST['minmax_cart_exclude'] ) ? 'no' : 'yes' );

		if ( isset( $_POST['variable_post_id'] ) ) {

			$variable_post_id         = $_POST['variable_post_id'];
			$min_max_rules            = $_POST['var_min_max_rules'];
			$minmax_do_not_count      = $_POST['variation_minmax_do_not_count'];
			$minmax_cart_exclude      = $_POST['variation_minmax_cart_exclude'];

			$max_loop = max( array_keys( $_POST['variable_post_id'] ) );

			for ( $i = 0; $i <= $max_loop; $i ++ ) {

				if ( ! isset( $variable_post_id[ $i ] ) )
					continue;

				$variation_id = absint( $variable_post_id[ $i ] );

				if ( isset( $min_max_rules[ $i ] ) ) {
				
					foreach( $min_max_rules[ $i ] as $s => $v ) {

						if ( is_int( $s ) )
							unset( $min_max_rules[ $i ][ $s ] );
						else if ( isset( $v[0] ) ) {
							unset( $min_max_rules[ $i ][ $s ][0] );
						}
					}
			
					update_post_meta( $variation_id, 'min_max_rules', 'yes' );
					
					update_post_meta( $variation_id, 'var_min_max_rules', $min_max_rules[ $i ] );

					if ( isset( $minmax_do_not_count[ $i ] ) )
						update_post_meta( $variation_id, 'minmax_do_not_count', 'yes' );
					else
						update_post_meta( $variation_id, 'minmax_do_not_count', 'no' );

					if ( isset( $minmax_cart_exclude[ $i ] ) )
						update_post_meta( $variation_id, 'minmax_cart_exclude', 'yes' );
					else
						update_post_meta( $variation_id, 'minmax_cart_exclude', 'no' );

				} else {
					update_post_meta( $variation_id, 'min_max_rules', 'no' );
				}
			}
		}
	}

	function variation_options( $loop, $variation_data ) {
		?>
		<label><input type="checkbox" class="checkbox min_max_rules" name="min_max_rules[<?php echo $loop; ?>]" <?php if ( isset( $variation_data['min_max_rules'][0] ) ) checked( $variation_data['min_max_rules'][0], 'yes' ) ?> /> <?php _e( 'Min/Max Rules', 'ignitewoo_wholesale_pro' ); ?> <a class="tips" data-tip="<?php _e( 'Enable this option to override min/max settings at variation level', 'ignitewoo_wholesale_pro' ); ?>" href="#">[?]</a></label>
		<?php
	}

	function variation_panel( $loop, $variation_data, $variation = '' ) {
		global $wp_roles;
		
		$all_roles = $wp_roles->roles;
		
		?>
		<tr class="min_max_rules" style="display:none">
			<td colspan="2">
			<style>
			.var_rule_table { 
				background: #fff !important;
			}
			.var_rule_table tr td { padding-top: 10px !important; }
			.wc-metaboxes-wrapper .wc-metabox .var_rule_table input[type="checkbox"] {
				min-width: 26px !important;
			}
			.wc-metaboxes-wrapper .wc-metabox .var_rule_table .select2-container {
				clear:both;
			}
			.wc-metaboxes-wrapper .wc-metabox .var_rule_table td input.number_field {
				min-width: 99% !important;
				float:none;
				margin-top: 0.5em;
				margin-right: 5px;
			}
			</style>
			<p style="margin-bottom:0.5em"><?php _e( 'The first matching rule always applies. Drag and drop the rules to rearrange the order.', 'ignitewoo_wholesale_pro' ) ?></p>
			<table class="widefat var_rule_table">
				<thead>
					<tr class="header">
					<th style="width:30px !important"></th>
					<th><?php _e( 'Rules', 'ignitewoo_wholesale_pro' ) ?><img height="16" width="16" src="<?php echo WC()->plugin_url() ?>/assets/images/help.png" class="help_tip" data-tip="<?php echo esc_attr( $settings['description'] ) ?>" style="float:none; margin-left: 0.5em"></th>
					<?php /*
					<th style="width:20%"><?php _e( 'Min Qty', 'ignitewoo_wholesale_pro' ) ?></th>
					<th style="width:20%"><?php _e( 'Max Qty', 'ignitewoo_wholesale_pro' ) ?></th>
					<th style="width:20%"><?php _e( 'Increments', 'ignitewoo_wholesale_pro' ) ?></th>
					*/ ?>
					</tr>
				</thead>
				<tbody>
					<tr class="template" style="display:none">
						<td style="width:30px !important">
							<input type="checkbox" class="remove" value="" name="remove">
							<input type="hidden" value="x" name="var_min_max_rules[<?php echo $loop ?>][rule_index][]">
						</td>
						<td style="overflow:visible">
						
							<div style="width:47%; float:left; margin-right: 1em">
								<label class="qty_labels"><?php _e( 'Roles', 'ignitewoo_wholesale_pro' ) ?></label>
								<select name="var_min_max_rules[<?php echo $loop ?>][roles][][]" class="chosen select2" multiple="multiple" style="width:200px;position:relative;z-index:99999;overflow:visible">
								
									<option value=""><?php echo '&nbsp;' ?></option>
									
									<?php foreach( $all_roles as $role_id => $role ) { ?>
									
									<?php $role_checked = (isset( $condition['args']['roles'] ) && is_array( $condition['args']['roles'] ) && in_array( $role_id, $condition['args']['roles'] )) ? 'selected="selected"' : ''; ?>
						
									<option <?php echo $role_checked ?> value="<?php echo $role_id ?>">
										<?php echo $role['name']; ?>
									</option>
									
									<?php } ?>
								</select>
							</div>
							
							
							<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '<' ) ) { ?>
							<div class="select select2 select_users" style="width:47%; float:left">
								<label class="qty_labels"><?php _e( 'Users', 'ignitewoo_wholesale_pro' ) ?> </label>
								<input type="hidden" class="min_max_users" name="var_min_max_rules[<?php echo $loop ?>][users][]" data-placeholder="<?php _e( 'Search for a user&hellip;', 'ignitewoo_wholesale_pro' ); ?>" data-selected='' value="" data-multiple="true" data-allow_clear="true" data-action="woocommerce_json_search_customers" style="width:100%;min-height:32px"/>
							</div>
							<?php } else { ?>
							<div style="width:47%; float:left">
								<label class="qty_labels"><?php _e( 'Users', 'ignitewoo_wholesale_pro' ) ?></label>
								<select name="var_min_max_rules[<?php echo $loop ?>][users][][]" class="ajax_select_customer" multiple="multiple" style="float:none;width:98%">
								</select>
							</div>
							<?php } ?>
							
							
							<div style="width:33%;clear:both;float:left;margin-top:0.5em">
								<?php _e( 'Min Qty', 'ignitewoo_wholesale_pro' ) ?> <input type="number" step="1" class="number_field" value="" name="var_min_max_rules[<?php echo $loop ?>][min][]">
							</div>
							<div style="width:30%; float:left;margin-top:0.5em">
								<?php _e( 'Max Qty', 'ignitewoo_wholesale_pro' ) ?> <input type="number" step="1" class="number_field" value="" name="var_min_max_rules[<?php echo $loop ?>][max][]">
							</div>
							<div style="width:30%; float:left;margin-top:0.5em">
								<?php _e( 'Increments', 'ignitewoo_wholesale_pro' ) ?> <input type="number" step="1" class="number_field" value="" name="var_min_max_rules[<?php echo $loop ?>][inc][]">
							</div>
							
						</td>
					</tr>
					<?php 

					$rules = get_post_meta( $variation->ID, 'var_min_max_rules', true );
					
					$x = 0;
					
					if ( empty( $rules['rule_index'] ) ) {
					
						?>
						<tr class="no_rules_row">
							<td colspan="5"><?php _e( 'No rules defined for this variation','ignitewoo_wholesale_pro' ) ?></td>
						</tr>
						<?php
						
					} else { 
					
					foreach( $rules['rule_index'] as $i => $vals ) { 
					
						$x++;
					?>
					
					<tr>
						<td style="width:3% !important">
							<input type="checkbox" class="remove" value="" name="remove">
							<input type="hidden" value="<?php echo $x ?>" name="var_min_max_rules[<?php echo $loop ?>][rule_index][<?php echo $x ?>]">
						</td>
						<td style="overflow:visible">
							<div style="width:47%; float:left; margin-right: 1em">
							<label class="qty_labels"><?php _e( 'Roles', 'ignitewoo_wholesale_pro' ) ?></label>
							<select name="var_min_max_rules[<?php echo $loop ?>][roles][<?php echo $x ?>][]" class="chosen select2" multiple="multiple" style="width:200px">
							
								<option value=""><?php echo '&nbsp;' ?></option>
								
								<?php foreach( $all_roles as $role_id => $role ) { ?>
								
								<?php $role_checked = in_array( $role_id, $rules['roles'][$i] ) ? 'selected="selected"' : ''; ?>
					
								<option <?php echo $role_checked ?> value="<?php echo $role_id ?>">
									<?php echo $role['name']; ?>
								</option>
								
								<?php } ?>
							</select>
							</div>
							

							<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '<' ) ) { ?>
							
							<div class="select select2 select_users" style="width:47%; float:left">
								<label class="qty_labels"><?php _e( 'Users', 'ignitewoo_wholesale_pro' ) ?></label> 
								<?php
								if ( isset( $rules['users'][$x] ) && is_array( $rules['users'][$x] ) )
									$rules['users'][$x] = implode( ',', $rules['users'][$x] );
									
								if ( empty( $rules['users'][$x] ) )
									$rules['users'][$x] = '';
									
								$user_strings = array();
								$user_id = '';
								if ( isset( $rules['users'][$x] ) && !empty( $rules['users'][$x] ) ) {
									$rules_users = explode( ',' , $rules['users'][$x] );
									if ( !empty( $rules_users ) )
									foreach( $rules_users as $pu ) { 
										if ( empty( $pu ) )
											continue;
										$user = get_user_by( 'id', $pu);
										if ( empty( $user ) || is_wp_error( $user ) )
											continue;
										$user_strings[ $pu ] = esc_html( $user->display_name ) . ' (#' . absint( $user->ID ) . ' &ndash; ' . esc_html( $user->user_email ) . ')';
									}
								}
								?>
								<br/>
								<input type="hidden" class="min_max_users" name="var_min_max_rules[<?php echo $loop ?>][users][<?php echo $x ?>]" data-placeholder="<?php _e( 'Search for a user&hellip;', 'ignitewoo_wholesale_pro' ); ?>" data-selected='<?php echo esc_attr( json_encode( $user_strings ) ) ?>' value="<?php echo $rules['users'][$x] ?>" data-multiple="true" data-allow_clear="true" data-action="woocommerce_json_search_customers" style="min-width:100%;min-height:32px"/>
							</div>
							
							<?php } else { ?>
							
							<div style="width:47%; float:left">
								<label class="qty_labels"><?php _e( 'Users', 'ignitewoo_wholesale_pro' ) ?></label>
								<select name="var_min_max_rules[<?php echo $loop ?>][users][<?php echo $x ?>][]" class="ajax_select_customer" multiple="multiple" style="width:100%">
									<?php 
									if ( !empty( $rules['users'][$x] ) )
									foreach( $rules['users'][$x] as $u ) {
										$user = get_user_by( 'id', $u );
										echo '<option selected="selected" value="' . esc_attr( $user->ID ) . '" ' . selected( 1, 1, false ) . '>' . esc_html( $user->display_name ) . ' (#' . absint( $user->ID ) . ' &ndash; ' . esc_html( $user->user_email ) . ')</option>';
									}
									?>
								</select>
							</div>
							
							<?php } ?>
							
							<div style="width:30%;clear:both;float:left;margin-top:0.5em;margin-right:5px">
								<?php _e( 'Min Qty', 'ignitewoo_wholesale_pro' ) ?> <input type="number" step="1" class="number_field" value="<?php echo $rules['min'][$i] ?>" name="var_min_max_rules[<?php echo $loop ?>][min][<?php echo $x ?>]">
							</div>
							<div style="width:30%; float:left;margin-top:0.5em;margin-right:5px">
								<?php _e( 'Max Qty', 'ignitewoo_wholesale_pro' ) ?> <input type="number" step="1" class="number_field" value="<?php echo $rules['max'][$i] ?>" name="var_min_max_rules[<?php echo $loop ?>][max][<?php echo $x ?>]">
							</div>
							<div style="width:30%; float:left;margin-top:0.5em;margin-right:5px">
								<?php _e( 'Increments', 'ignitewoo_wholesale_pro' ) ?> <input type="number" step="1" class="number_field" value="<?php echo $rules['inc'][$i] ?>" name="var_min_max_rules[<?php echo $loop ?>][inc][<?php echo $x ?>]">
							</div>
						</td>
					</tr>

					<?php } ?>
					
					<?php } ?>
				</tbody>
			</table>
			<div style="margin-top: 0.5em; margin-bottom:1em">
				<button class="var_minmax_add_rule button"><?php _e( 'Add new rule', 'ignitewoo_wholesale_pro' ) ?></button>
				
				<button class="var_minmax_remove_rule button" style="margin-left: 1.5em"><?php _e( 'Remove selected rules', 'ignitewoo_wholesale_pro' ) ?></button>
			</div>
			</td>
		</tr>
		<tr class="min_max_rules" style="display:none">
			<td>

				<label><input type="checkbox" class="checkbox" name="variation_minmax_do_not_count[<?php echo $loop; ?>]" <?php if ( isset( $variation_data['minmax_do_not_count'][0] ) ) checked( $variation_data['minmax_do_not_count'][0], 'yes' ) ?> style="float:none"/> <?php _e( 'Do not count', 'ignitewoo_wholesale_pro' ); ?></label>
			</td>
			<td>
				<label><input type="checkbox" class="checkbox" name="variation_minmax_cart_exclude[<?php echo $loop; ?>]" <?php if ( isset( $variation_data['minmax_cart_exclude'][0] ) ) checked( $variation_data['minmax_cart_exclude'][0], 'yes' ) ?> style="float:none"/> <?php _e( 'Exclude', 'ignitewoo_wholesale_pro' ); ?></label>
			</td>
		</tr>
		<?php
	}
}

$IGN_WSP_Min_Max_Quantities_Admin = new IGN_WSP_Min_Max_Quantities_Admin();
