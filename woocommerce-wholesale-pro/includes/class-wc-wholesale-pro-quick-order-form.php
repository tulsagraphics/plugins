<?php
/*
 * Copyright (c) 2016 - IgniteWoo.com - ALL RIGHTS RESERVED
 */
 
class IGN_Wholesale_Pro_Quick_Orders {

	public $plugin_dir;
	
	public $settings = array();
	
	public $version_ok = true;
	
	public $inject_scripts = false; 

	public function __construct() { 
	
		$this->plugin_dir = dirname( __FILE__ );

		add_action( 'init', array( &$this, 'version_check' ), 11 );
		
		add_action( 'init', array( &$this, 'register_post_type' ), 99 );
		add_action( 'add_meta_boxes', array( &$this, 'add_meta_boxes' ), 15 );
		add_filter( 'save_post', array( &$this, 'save_post' ), 5 );
		add_filter( 'woocommerce_screen_ids', array( &$this, 'screen_ids' ) );
		
		add_action( 'wp_ajax_ign_add_to_cart_variable', array( &$this, 'add_to_cart_variable' ) );
		add_action( 'wp_ajax_nopriv_ign_add_to_cart_variable', array( &$this, 'add_to_cart_variable' ) );
		add_action( 'woocommerce_integrations', array( &$this, 'settings_init' ) );

		add_action( 'wp_ajax_ign_search_products', array( &$this, 'json_search_products' ) );
		add_action( 'wp_ajax_nopriv_ign_search_products', array( &$this, 'json_search_products' ) );

		// frontend
		add_action( 'wp_ajax_quick_order_product_search', array( &$this, 'frontend_search_products' ) );
		add_action( 'wp_ajax_nopriv_quick_order_product_search', array( &$this, 'frontend_search_products' ) );

		add_shortcode( 'quick_order_form', array( &$this, 'shortcode' ) );
		add_shortcode( 'wsp_quick_order_form', array( &$this, 'shortcode' ) );
		
		add_filter( 'wp_footer', array( &$this, 'wp_footer' ), 9 );
		
	}

	public function version_check() { 
		if ( version_compare( WOOCOMMERCE_VERSION, '2.2', '<' ) ) {
			add_action( 'admin_notices', array( &$this, 'version_notice' ) );
			$this->version_ok = false;
		}
	}
	
	public function version_notice() { 
		?>
		<div class="error" style="color:#DC3232"><p>WooCommerce Quick Order Forms requires WooCommerce 2.2 or newer to operate</p></div>
		<?php
	}

	public function settings_init( $integrations ) {
		require_once( dirname( __FILE__ ) . '/class-wc-wholesale-pro-quick-order-settings.php' );
		return $integrations;
	}

	public function wp_footer() { 
		if ( !$this->inject_scripts ) 
			return;

		$js_dir = str_replace( array( 'http:', 'https:' ), '', trailingslashit( ( plugins_url( '/../assets/js/', __FILE__ ) ) ) );
	 
		$img_dir = str_replace( array( 'http:', 'https:' ), '', trailingslashit( ( plugins_url( '/../assets/images/', __FILE__ ) ) ) );

		wp_dequeue_script( 'wc-add-to-cart' );
		
		wp_register_script( 'ign-add-to-cart', $js_dir . 'add-to-cart.js', array( 'jquery' ), WOOCOMMERCE_VERSION, true );
		
		wp_enqueue_script( 'ign-add-to-cart' );
		
		wp_register_script( 'ign-add-to-cart-variation', $js_dir . 'add-to-cart-variations.js', array( 'jquery' ), WOOCOMMERCE_VERSION, true );

		wp_dequeue_script( 'wc-add-to-cart-variation' );
		
		wp_enqueue_script( 'ign-add-to-cart-variation' );

		wp_localize_script( 'ign-add-to-cart', 'wc_add_to_cart_params', apply_filters( 'wc_add_to_cart_params', array(
			'ajax_url'                => WC()->ajax_url(),
			'ajax_loader_url'         => $img_dir . '/ajax-loader@2x.gif',
			'i18n_view_cart'          => '', //esc_attr__( 'View Cart', 'ignitewoo_wholesale_pro' ),
			'cart_url'                => get_permalink( wc_get_page_id( 'cart' ) ),
			'is_cart'                 => is_cart(),
			'cart_redirect_after_add' => get_option( 'woocommerce_cart_redirect_after_add' )
		) ) );

		wp_localize_script( 'ign-add-to-cart-variation', 'wc_add_to_cart_variation_params', apply_filters( 'wc_add_to_cart_variation_params', array(
			'i18n_no_matching_variations_text' => esc_attr__( 'Sorry, no products matched your selection. Please choose a different combination.', 'ignitewoo_wholesale_pro' ),
			'i18n_unavailable_text'            => esc_attr__( 'Sorry, this product is unavailable. Please choose a different combination.', 'ignitewoo_wholesale_pro' ),
		) ) );
		
		wp_register_script( 'ign-quick-search', $js_dir . 'search.js', array( 'jquery' ), WOOCOMMERCE_VERSION, true );
		
		wp_enqueue_script( 'ign-quick-search' );
		
		wp_localize_script( 'ign-quick-search', 'ign_quick_search_params', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
		
		wp_enqueue_script( 'jquery-blockui' );
		
		wp_enqueue_script( 'jquery-ui-core' );
	}
	
	public function shortcode( $attrs = array() ) { 
		global $products, $user_ID, $restricted_cats, $quick_order_settings, $qof_id;
		
		if ( !function_exists( 'wc_print_notices' ) ) { 
			return __( 'The quick order form shortcode will not render in the admin area of WordPress. View the form on the public side of your site', 'ignitewoo_wholesale_pro' );
		}
		
		$attrs = shortcode_atts( array(
			'id' => '',
			), 
			$attrs, 
			'quick_order_form' 
		);
		
		if ( empty( $attrs['id'] ) )
			return sprintf ( __( 'You must provide an ID parameter in the shortcode. Please read <a href="%s" target="_blank">the documentation</a>', 'ignitewoo_wholesale_pro' ), '//ignitewoo.com/ignitewoo-software-documentation/' );
	
		$quick_order_settings = get_post_meta( $attrs['id'], 'quick_order_settings', true );
		$quick_order_settings['form_id'] = $attrs['id'];
		$this->settings = $quick_order_settings;

		// Support for Restricted Category Access
		if ( function_exists( 'ign_get_user_restricted_categories' ) )
			$restricted_cats = ign_get_user_restricted_categories();
		else 
			$restricted_cats = null;
			
		if ( !$this->version_ok )
			return __( 'WooCommerce Quick Orders requires WooCommerce 2.2 or newer to operate', 'ignitewoo_wholesale_pro' );
			
		if ( empty( $this->settings ) )
			return __( 'Please configure the settings and save them at least once before using this plugin', 'ignitewoo_wholesale_pro' );

		$qof_id = $attrs['id'];
		
		/*
		$user_allowed = false;
		
		// Check users first if any are set. If user check disallows access check by role too
		
		if ( !empty( $this->settings['users'] ) ) { 
		
			foreach( $this->settings['users'] as $key => $uid ) { 
				if ( $uid == $user_ID )
					$user_allowed = true; 
			}
			
		} else { 
			$user_allowed = true;
		}
		*/
		
		$role_allowed = false; 
	
		if ( !empty( $this->settings['roles'] ) ) { 
			
			foreach( $this->settings['roles'] as $role ) { 
				if ( current_user_can( $role ) ) 
					$role_allowed = true; 
			}
			
		} else { 
			$role_allowed = true;
		}
		
		if ( !is_user_logged_in() && !empty( $this->settings['roles'] ) && in_array( 'guest', $this->settings['roles' ] ) )
			$role_allowed = true;
		
		//if ( !$user_allowed && !$role_allowed )
		if ( !$role_allowed )
			return wpautop( $this->settings['access_denied_message'] );
		
		$this->inject_scripts = true; 

		if ( class_exists( 'IgniteWoo_Restrict_Product_Categories' ) || class_exists( 'IgniteWoo_Tiered_Pricing_Filter' ) ) {
			$product_restrictions = apply_filters( 'loop_shop_post_in', array() );	
		} else { 
			$product_restrictions = null;
		}
		
		$products = $this->get_products( $product_restrictions );

		ob_start();
		$this->get_template( 'quick-order-form.php' );
		return ob_get_clean();
	}

	public function get_template( $template = '', $load = true ) { 
		
		if ( empty( $template ) )
			return;
		
		if ( file_exists( get_stylesheet_directory() . '/wholesale-pro/quick-orders/' . $template ) )
			$file = get_stylesheet_directory() . '/wholesale-pro/quick-orders/' . $template;
		else 
			$file = $this->plugin_dir . '/../templates/quick-orders/' . $template;

		if ( $load )
			load_template( $file, false );
		else
			return $file;
			
	}

	// Returns a query AND string to help filter out products if the filters are enabled 
	public function handle_filters() { 
		global $current_user, $ign_wholesale_pro_suite; 

		if ( empty( $current_user ) ) {
			if ( function_exists( 'wp_get_current_user' ) )
				$current_user = wp_get_current_user();
			else
				$current_user = get_currentuserinfo();
		}

		$tiered_role = false;

		if ( !empty( $current_user->roles ) ) {
			foreach( $current_user->roles as $role ) {
				if ( 'ignite_level_' == substr( $role, 0, 13 ) )
					$tiered_role = $role;
			}
		}
		
		if ( $tiered_role && 'yes' == $ign_wholesale_pro_suite->settings['tier_filter'] ) { 
			return array( 'result' => 'tier_filter', 'role' => $tiered_role );
		} else if ( !$tiered_role && 'yes' == $ign_wholesale_pro_suite->settings['retail_filter'] ) { 
			return array( 'result' => 'retail_filter' );
		}
		
		return array( 'result' => false ); 
		
	}
	public function get_products( $product_restrictions = array(), $frontend = false ) {
		global $max_number_pages, $wp_version;

		$current_page = get_query_var( 'paged' );

		$orderby = $this->settings['sort_by'];
		$order = $this->settings['sort_order'];
		$per_page = !empty( $this->settings['per_page'] ) ? $this->settings['per_page'] : 20;
		
		$sku_query = array(
			'key'    => '_sku',
			'value'    => '%*%*%*%*', // dummy val so that all SKUs are valid and considered even if SKU is empty
			//'type' => 'CHAR',
			'compare' => '!=',
		);
		
		if ( version_compare( $wp_version, '4.2', '>=' ) ) { 
			$args = array(
				'post_type'			=> array( 'product', 'variable' ),
				'post_status' 			=> 'publish',
				'ignore_sticky_posts'		=> 0,
				'orderby' 			=> $orderby,
				'order' 			=> $order,
				'posts_per_page' 		=> $per_page,
				'meta_query' 			=> array(
					/*
					'vis' => array(
						'key' 		=> '_visibility',
						'value' 	=> array( 'catalog', 'visible' ),
						'compare' 	=> 'IN'
					),
					*/
					'sku' => $sku_query
				)
			);
			
			// Sorting by SKU? If so overwrite meta_query
			if ( 'meta_value_num' !== $orderby ) { 
				unset( $args['meta_query']['sku'] );
			} else { 
				$args['meta_query']['relation'] = 'AND';
				$args['orderby'] = 'sku';
				
			}
			
		} else { 
			$args = array(
				'post_type'			=> array( 'product', 'variable' ),
				'post_status' 			=> 'publish',
				'ignore_sticky_posts'		=> 0,
				'orderby' 			=> $orderby,
				'order' 			=> $order,
				'posts_per_page' 		=> $per_page,
				/*
				'meta_query' 			=> array(
					array(
						'key' 		=> '_visibility',
						'value' 	=> array( 'catalog', 'visible' ),
						'compare' 	=> 'IN'
					)
				)
				*/
			);
		}

		// These would be product IDs from a 3rd party plugin such as Restricted Categories
		if ( !empty( $product_restrictions ) ) { 
			$args['post__in'] = $product_restrictions;
		}
		
		if ( !empty( $this->settings['include_products'] ) ) { 
			if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
				$args['post__in'] = $this->settings['include_products'];
			else 
				$args['post__in'] = explode( ',', $this->settings['include_products'] );
		}
			
		if ( !empty( $this->settings['exclude_products'] ) ) { 
			if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
				$args['post__not_in'] = $this->settings['exclude_products'];
			else 
				$args['post__not_in'] = explode( ',', $this->settings['exclude_products'] );
		}
		
		if ( !empty( $this->settings['include_categories'] ) ) {
			$args['tax_query']['relation'] = 'AND';
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'product_cat',
					'field'    => 'term_id',
					'terms'    => $this->settings['include_categories'],
					'operator' => 'IN',
				)
			);
		}
		
		if ( !empty( $this->settings['exclude_categories'] ) ) {
			$args['tax_query']['relation'] = 'AND';
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'product_cat',
					'field'    => 'term_id',
					'terms'    => $this->settings['exclude_categories'],
					'operator' => 'NOT IN',
				)
			);
		}

		if ( !empty( $current_page ) )
			$args['paged'] = $current_page;

		if ( !empty( $this->settings['paged'] ) )
			$args['paged'] = $quick_order_settings['paged'] = $this->settings['paged'];
			
		// If this is a frontend search add a meta query for SKU and run the query.
		// We will run a second query as a "s" arg search below and merge the results
		if ( $frontend ) {
			unset( $args['meta_query']['relation'] );
			$args['meta_query'] = array(
				array(
					'key'    => '_sku',
					'value'    => $this->settings['quick_order_search'],
					'compare' => 'LIKE',
				)
			);
		}
			
		// Obeying the tier filter and retail filter only takes affect here if the "Include Products" setting is empty 
		// because if it isn't the admin is specifically asking to have an exact list of products regardless of what kind 
		// of pricing is set for the products. 
		if ( empty( $this->settings['include_products'] ) ) { 
		
			$filter = $this->handle_filters();

			if ( 'tier_filter' == $filter['result'] && !empty( $filter['role'] ) ) { 
				
				$args['meta_query'] = array( 
					'relation' => 'AND',
					array( 
						'key' => '_' . $filter['role'] . '_price',
						'value' => '',
						'compare' => '!='
					)
				);
			
			} else if ( 'retail_filter' == $filter['result'] ) { 
				
				$args['meta_query'] = array( 
					'relation' => 'AND',
					array( 
						'relation' => 'OR',
						array( 
							'key' => '_price',
							'value' => '',
							'compare' => '!='
						),
						array( 
							'key' => '_sale_price',
							'value' => '',
							'compare' => '!='
						)
					)
				);
			}
		}
		
		// maybe merge with any existing post__in settings which are there from 3rd party plugins such as Restrict Categories
		/*
		if ( !empty( $this->settings['exclude_categories'] ) ) {
			
			$args['tax_query'][] = array(
					'taxonomy' => 'product_cat',
					'field'    => 'term_id',
					'terms'    => $this->settings['exclude_categories'],
					'operator' => 'IN',
				);
		}
		*/
		// ----------------------------------------------------------------------

		$products = new WP_Query( apply_filters( 'woocommerce_quick_order_products_query', $args ) );

		// SECOND QUERY: Run a search query and merge the results from the above meta query
		// Frontend search parms -------------------------------------------------
		if ( !empty( $this->settings['quick_order_search'] ) ) {
			// remove meta query vars
			unset( $args['meta_query'] );
			
			$args['s'] = $this->settings['quick_order_search'];
			//$args['posts_per_page'] = 3;
			
			$alt_products = new WP_Query( apply_filters( 'woocommerce_quick_order_products_query', $args ) );

			$products->posts = array_values( array_unique( array_merge( $products->posts, $alt_products->posts ), SORT_REGULAR ) );
			
			$products->post_count = count( $products->posts );
			
			$products->max_num_pages = $alt_products->max_num_pages;

		} 

		if ( empty( $max_num_pages ) )
			$max_number_pages = $products->max_num_pages; //ceil( $products->post_count / $this->settings['per_page'] );
	
		return $products;
	}
	
	public function frontend_search_products() {
		global $products, $quick_order_settings;

		if ( empty( $_POST['data'] ) ) 
			die; 

		$args = wp_parse_args( $_POST['data'] );

		$quick_order_settings = $this->settings = get_post_meta( $args['quick_order_form_id'], 'quick_order_settings', true );
		$quick_order_settings['form_id'] = $args['quick_order_form_id'];
		$quick_order_settings['paged'] = $args['paged'];

		// Stuff the args into the settings and call the products up
		
		if ( 'all' == $args['mode'] ) {
			$products = $this->get_products();
			ob_start();
			$this->get_template( 'product-table.php' );
			$out = ob_get_clean();
			die( $out );
		}
		
		if ( !empty( $args['cat'] ) && absint( $args['cat'] ) > 0 )
			$this->settings['include_categories'][] = $args['cat'];
			
		if ( !empty( $args['quick_order_search'] ) )
			$this->settings['quick_order_search'] = $args['quick_order_search'];
		
		if ( !empty( $args['paged'] ) )
			$this->settings['paged'] = $args['paged'];
			
		// Cannot search with a term or category
		if ( empty( $args['cat'] ) && empty( $args['quick_order_search'] ) )
			die;
			
		//if ( class_exists( 'IgniteWoo_Restrict_Product_Categories' ) || class_exists( 'IgniteWoo_Tiered_Pricing_Filter' ) ) {
			$product_restrictions = apply_filters( 'loop_shop_post_in', array() );	
		//} else { 
		//	$product_restrictions = null;
		//}

		$products = $this->get_products( $product_restrictions, $frontend = true );

		if ( empty( $products->posts ) ) 
			die( __( 'No matching products found', 'ignitewoo_wholesale_pro' ) );

		ob_start();
		$this->get_template( 'product-table.php' );
		$out = ob_get_clean();
		
		die( $out );
		
	}
	
	public function add_to_cart_variable() {
	
		$args = isset( $_POST['args'] ) ? $_POST['args'] : array();
		
		if ( empty( $args ) ) {
		
			$this->json_headers();

			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
				'error' => true,
			);

			echo json_encode( $data );
		}
	
		$args = wp_parse_args( $args );
		
		if ( empty( $args ) ) {
		
			$this->json_headers();

			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
				'error' => true,
			);

			echo json_encode( $data );
		}

		$product_id = $args['product_id'];
		$variation_id = $args['variation_id'];
		$quantity = empty( $args['quantity'] ) ? 1 : $args['quantity'];
		
		$variations = array();
		
		foreach( $args as $key => $val ) { 
			if ( false === strpos( $key, 'attribute_' ) )
				continue;
				
			$variations[ $key ] = $val; 
		}

		$cart_item_data = null;

		$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity, $variation_id, $variations, $cart_item_data );

		if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variations ) ) {

			do_action( 'woocommerce_ajax_added_to_cart', $product_id );

			if ( get_option( 'woocommerce_cart_redirect_after_add' ) == 'yes' ) {
				wc_add_to_cart_message( $product_id );
			}

			$this->get_refreshed_fragments();

		} else {

			$this->json_headers();

			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
				'error' => true,
				'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id )
			);

			echo json_encode( $data );
		}

		die();
	}

	private function json_headers() {
		header( 'Content-Type: application/json; charset=utf-8' );
	}
	
	public function get_refreshed_fragments() {

		$this->json_headers();

		ob_start();

		woocommerce_mini_cart();

		$mini_cart = ob_get_clean();

		// Fragments and mini cart are returned
		$data = array(
			'fragments' => apply_filters( 'add_to_cart_fragments', array(
					'div.widget_shopping_cart_content' => '<div class="widget_shopping_cart_content">' . $mini_cart . '</div>'
				)
			),
			'cart_hash' => WC()->cart->get_cart() ? md5( json_encode( WC()->cart->get_cart() ) ) : ''
		);

		die( json_encode( $data ) );
	}
	
	public static function json_search_products( $x = '', $post_types = array( 'product' ) ) {
		global $wpdb;

		ob_start();

		check_ajax_referer( 'search-products', 'security' );

		$term = (string) wc_clean( stripslashes( $_GET['term'] ) );

		if ( empty( $term ) ) {
			die();
		}

		$like_term = '%' . $wpdb->esc_like( $term ) . '%';

		if ( is_numeric( $term ) ) {
			$query = $wpdb->prepare( "
				SELECT ID FROM {$wpdb->posts} posts LEFT JOIN {$wpdb->postmeta} postmeta ON posts.ID = postmeta.post_id
				WHERE posts.post_status = 'publish'
				AND (
					posts.post_parent = %s
					OR posts.ID = %s
					OR posts.post_title LIKE %s
					OR (
						postmeta.meta_key = '_sku' AND postmeta.meta_value LIKE %s
					)
				)
			", $term, $term, $term, $like_term );
		} else {
			$query = $wpdb->prepare( "
				SELECT ID FROM {$wpdb->posts} posts LEFT JOIN {$wpdb->postmeta} postmeta ON posts.ID = postmeta.post_id
				WHERE posts.post_status = 'publish'
				AND (
					posts.post_title LIKE %s
					or posts.post_content LIKE %s
					OR (
						postmeta.meta_key = '_sku' AND postmeta.meta_value LIKE %s
					)
				)
			", $like_term, $like_term, $like_term );
		}

		$query .= " AND posts.post_type IN ('" . implode( "','", array_map( 'esc_sql', $post_types ) ) . "')";

		if ( ! empty( $_GET['exclude'] ) ) {
			$query .= " AND posts.ID NOT IN (" . implode( ',', array_map( 'intval', explode( ',', $_GET['exclude'] ) ) ) . ")";
		}

		if ( ! empty( $_GET['include'] ) ) {
			$query .= " AND posts.ID IN (" . implode( ',', array_map( 'intval', explode( ',', $_GET['include'] ) ) ) . ")";
		}

		if ( ! empty( $_GET['limit'] ) ) {
			$query .= " LIMIT " . intval( $_GET['limit'] );
		}

		$posts          = array_unique( $wpdb->get_col( $query ) );
		$found_products = array();

		if ( ! empty( $posts ) ) {
			foreach ( $posts as $post ) {
				$product = wc_get_product( $post );

				if ( ! current_user_can( 'read_product', $post ) ) {
					continue;
				}

				if ( ! $product || ( $product->is_type( 'variation' ) && empty( $product->parent ) ) ) {
					continue;
				}
				
				// Skip variations, but not the parents of variations
				if ( ! $product || ( $product->is_type( 'variation' ) ) ) {
					continue;
				}

				$found_products[ $post ] = rawurldecode( $product->get_formatted_name() );
			}
		}

		$found_products = apply_filters( 'woocommerce_json_search_found_products', $found_products );

		wp_send_json( $found_products );
	}
	
	public function register_post_type() { 
//die( 'asdf' );
		$res = register_post_type( 'quick_order_forms',
			apply_filters( 'woocommerce_register_post_type_product',
				array(
					'labels'              => array(
							'name'                  => __( 'Quick Order Forms', 'ignitewoo_wholesale_pro' ),
							'singular_name'         => __( 'Quick Order Form', 'ignitewoo_wholesale_pro' ),
							'menu_name'             => _x( 'Quick Order Forms', 'Admin menu name', 'ignitewoo_wholesale_pro' ),
							'add_new'               => __( 'Add Quick Order Form', 'ignitewoo_wholesale_pro' ),
							'add_new_item'          => __( 'Add New Quick Order Form', 'ignitewoo_wholesale_pro' ),
							'edit'                  => __( 'Edit', 'ignitewoo_wholesale_pro' ),
							'edit_item'             => __( 'Edit Quick Order Form', 'ignitewoo_wholesale_pro' ),
							'new_item'              => __( 'New Quick Order Form', 'ignitewoo_wholesale_pro' ),
							'view'                  => __( 'View Quick Order Form', 'ignitewoo_wholesale_pro' ),
							'view_item'             => __( 'View Quick Order Form', 'ignitewoo_wholesale_pro' ),
							'search_items'          => __( 'Search Quick Order Forms', 'ignitewoo_wholesale_pro' ),
							'not_found'             => __( 'No Quick Order Forms found', 'ignitewoo_wholesale_pro' ),
							'not_found_in_trash'    => __( 'No Quick Order Forms found in trash', 'ignitewoo_wholesale_pro' ),
							'parent'                => __( 'Parent Quick Order Form', 'ignitewoo_wholesale_pro' ),
						),
					'description'         => __( 'This is where you can add new quick order forms to your store.', 'ignitewoo_wholesale_pro' ),
					'public'              => false,
					'show_ui'             => true,
					'capability_type'     => 'product',
					'show_in_menu'		=> 'woocommerce',
					'map_meta_cap'        => true,
					'publicly_queryable'  => false,
					'exclude_from_search' => true,
					'hierarchical'        => false, // Hierarchical causes memory issues - WP loads all records!
					'rewrite'             => false,
					'query_var'           => false,
					'supports'            => array( 'title' ),
					'has_archive'         => false,
					'show_in_nav_menus'   => false,
				)
			)
		);
	}
	
	public function screen_ids( $screen_ids = null ) { 
		$screen_ids[] = 'quick_order_forms';
		return $screen_ids; 
	}
	
	public function add_meta_boxes() { 
		add_meta_box( 
			'qof-box',
			__( 'Settings', 'ignitewoo_wholesale_pro' ),
			array( &$this, 'render_meta_box' ),
			'quick_order_forms',
			'normal',
			'default'
		);
		add_meta_box( 
			'qof-shortcode-box',
			__( 'Shortcode','ignitewoo_wholesale_pro' ),
			array( &$this, 'render_shortcode_box' ),
			'quick_order_forms',
			'side',
			'high'
		);
	}
	
	public function render_meta_box() { 
		global $post;
		$box = new IGN_Wholesale_Pro_Quick_Order_Settings();
		$settings = get_post_meta( $post->ID, 'quick_order_settings', true );
		$box->settings = $settings;
		$box->admin_options();
	}
	
	public function render_shortcode_box() { 
		global $post;
		?>
		<p><?php _e( 'Use the following shortcode to inject this form into a post or page', 'ignitewoo_wholesale_pro' ); ?>:</p>
		<code>[wsp_quick_order_form id="<?php echo $post->ID ?>"]</code>
		<p><?php _e( 'Or use the following PHP code to inject this form', 'ignitewoo_wholesale_pro' ); ?>:</p>
		<code>&lt;?php wsp_quick_order_form(<?php echo $post->ID ?>)?&gt;</code>
		<?php
	}
	
	public function save_post( $post_id ) { 
	
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return;

		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) 
			return;
			
		if ( ! current_user_can( 'edit_post', $post_id ) )
			return;
			
		if ( false !== wp_is_post_revision( $post_id ) )
			return;
			
		$settings = array();
		
		foreach( $_POST as $key => $val ) {
		
			if ( 'woocommerce_ignitewoo_quick_orders_' !== substr( $key, 0, 35 ) )
				continue;
				
			$key = str_replace( 'woocommerce_ignitewoo_quick_orders_', '', $key );
		
			if ( ( 'show_sku' == $key || 'show_image' == $key || 'disable_paging' == $key || 'disable_search' == $key ) && 1 == $val ) {
				$val = 'yes';
			} else if ( ( 'show_sku' == $key || 'show_image' == $key || 'disable_paging' == $key || 'disable_search' == $key ) && 1 != $val ) { 
				$val = 'no';
			}
			
			$settings[ $key ] = $val;
		}

		update_post_meta( $post_id, 'quick_order_settings', $settings );

	}
}

new IGN_Wholesale_Pro_Quick_Orders();

function wsp_quick_order_form( $id = null ) { 
	if ( empty( $id ) )
		return;
	do_shortcode( '[wsp_quick_order_form id="' . $id . '"]' );

}

