<?php
/*
 * Copyright © 2014 IgniteWoo.com - All Rights Reserved
 */

class IgniteWoo_Wholesale_Pro_Product_Admin {
    
	public function __construct() {

		add_action( 'woocommerce_product_write_panel_tabs', array(&$this, 'on_product_write_panel_tabs' ), 99);

		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
			add_action( 'woocommerce_product_data_panels', array(&$this, 'product_data_panel' ), 99);
		else 
			add_action( 'woocommerce_product_write_panels', array(&$this, 'product_data_panel' ), 99);

		add_action( 'woocommerce_process_product_meta', array(&$this, 'process_meta_box' ), 1, 2);

		add_action( 'wp_ajax_ignitewoo_dd_product_create_empty_ruleset', array(&$this, 'create_empty_ruleset' ) );

		add_action ( 'admin_head', array( &$this, 'admin_head' ) );

		add_action( 'admin_enqueue_scripts', array( &$this, 'admin_scripts') );

	}

	function admin_scripts() {
	}

	function admin_head() { 
		?>

		<style>
		#dynamic_dd_pricing_data ul.product_data_tabs li.dynamic_pricing_options a{background-position:9px 9px;}
		#woocommerce-product-data ul.product_data_tabs li.dynamic_pricing_options a {
			<?php if ( version_compare( WOOCOMMERCE_VERSION, "2.1" ) >= 0 ) { ?>
			padding:5px 5px 5px 9px;
			<?php } else if ( version_compare( WOOCOMMERCE_VERSION, "2.0.0" ) >= 0 ) { ?>
			padding:5px 5px 5px 28px;
			<?php } else { ?>
			padding:9px 9px 9px 34px;
			<?php } ?>
			line-height:16px;
			border-bottom:1px solid #d5d5d5;
			text-shadow:0 1px 1px #fff;
			color:#21759B;
			background-position: 9px -695px;
			/* background: url("<?php echo WP_PLUGIN_URL ?>/woocommerce/assets/images/icons/wc-tab-icons.png") no-repeat scroll 6px -58px #F1F1F1; */
		}
		#woocommerce-product-data ul.product_data_tabs li.active a{background-color:#f8f8f8;border-bottom:1px solid #f8f8f8;}
		#dynamic_dd_pricing_data button { float:right; color: #333; font-weight: bold; }
		#dynamic_dd_pricing_data .section { display: block; margin-bottom: 10px; }
		#dynamic_dd_pricing_data .list-column { float:left; margin-bottom: 0; }
		#dynamic_dd_pricing_data.woocommerce_options_panel label,#dynamic_dd_pricing_data.woocommerce_options_panel input, #dynamic_dd_pricing_data.woocommerce_options_panel select {
			float:none;
		}
		#dynamic_dd_pricing_data.woocommerce_options_panel div.section {
			zoom: 1;
			margin: 9px 0 0;
			font-size: 12px;
			padding: 5px 9px;
		}
		#dynamic_dd_pricing_data.woocommerce_options_panel label {
			display:block;
			line-height: 24px;
			float: left;
			width: 250px;
			padding: 0;
			margin: 4px 0 0 5px;
		}
		#dynamic_dd_pricing_data table{ width:100%;position:relative;}
		#dynamic_dd_pricing_data table thead th{ background:#ececec;padding:7px 9px;font-size:11px;text-align:left;}
		#dynamic_dd_pricing_data table td{ padding:2px 9px;text-align:left;vertical-align:middle;border-bottom:1px dotted #ececec;}
		#dynamic_dd_pricing_data table td input, #dynamic_dd_pricing_data table td textarea{ width:100%;margin:0;display:block;}
		#dynamic_dd_pricing_data table td select{width:100%;}
		#dynamic_dd_pricing_data table td input, #dynamic_dd_pricing_data table td textarea{ font-size:14px;padding:4px;color:#555;}
		#dynamic_dd_pricing_data table td input, #dynamic_dd_pricing_data .list-column input{ font-size:14px;padding:4px;color:#555;width:100%}
		#dynamic_dd_pricing_data .list-column input  { width: 17px; }
		#dynamic_dd_pricing_data .rule_user_list { 
			border: 1px solid #ccc;
			margin-top: 2px;
			padding: 0px 10px;
			overflow: auto;
			width: 600px;
			height: 150px;
		}
		
		.dd_add_pricing_rule {
			background:#008000;
			color:#fff;
			border-radius: 10px;
		}
		.dd_delete_pricing_rule, .dd_delete_pricing_ruleset {
			margin-left: 10px;
			background:#bf0000;
			color:#fff;
			border-radius: 10px;
			text-decoration: none;
		}
		.dd_delete_pricing_rule:hover, .dd_add_pricing_rule:hover {
			color: #fff;
		}
		.dd_delete_pricing_rule span, .dd_add_pricing_rule span{
			cursor: pointer;
			font-size: 16px;
			padding-top: 2px;
		}
		.dd_delete_pricing_ruleset:hover {
			color: #fff;
		}
		</style>

	<?php
	}

	public function on_product_write_panel_tabs() {
		?>
		<li class="pricing_tab dynamic_pricing_options"><a href="#dynamic_dd_pricing_data"><?php _e( 'Qty Discounts', 'ignitewoo_wholesale_pro' ); ?></a></li>
		<?php
	}

	public function product_data_panel() {
		global $post;

		$pricing_rule_sets = get_post_meta( $post->ID, '_dd_pricing_rules', true);

		?>
		<div id="dynamic_dd_pricing_data" class="panel woocommerce_options_panel">
			<p class="description"><?php _e( 'Here you can add groups of rules that will be applied to buyers.', 'ignitewoo_wholesale_pro' ) ?></p>
			<p class="description"><?php _e( 'Rules are applied in the order seen, where the first matching is applied.', 'ignitewoo_wholesale_pro' ) ?></p>
			<p class="description" style="margin-bottom: 7px"><?php _e( 'You can also rearrange the order of groups by dragging and dropping them.', 'ignitewoo_wholesale_pro' ) ?></p>

			<div id="woocommerce-dd-pricing-rules-wrap" data-setindex="<?php echo count( $pricing_rule_sets); ?>">
				<?php $this->meta_box_javascript(); ?>
				<?php $this->meta_box_css(); ?>  
				<?php if ( $pricing_rule_sets && is_array( $pricing_rule_sets) && sizeof( $pricing_rule_sets) > 0) : ?>
					<?php $this->create_rulesets( $pricing_rule_sets); ?>
				<?php endif; ?>        
			</div>   
			<button title="<?php _e( 'Adds add another dynamic discount rule group.', 'ignitewoo_wholesale_pro' )?> " id="woocommerce-dd-pricing-add-ruleset" type="button" class="button">Add Discount Group</button>
			<div class="clear"></div>
		</div>
		<?php
	}


	function get_product( $product_id, $args = array() ) {

		$product = null;

		if ( version_compare( WOOCOMMERCE_VERSION, "2.0.0" ) >= 0 ) {

			// WC 2.0
			$product = wc_get_product( $product_id, $args );

		} else {

			// old style, get the product or product variation object
			if ( isset( $args['parent_id'] ) && $args['parent_id'] ) {

				$product = new WC_Product_Variation( $product_id, $args['parent_id'] );

			} else {

				// get the regular product, but if it has a parent, return the product variation object
				$product = new WC_Product( $product_id );

				if ( $product->get_parent() ) {
					$product = new WC_Product_Variation( $product->id, $product->get_parent() );
				}
			}
		}

		return $product;
	}

	
	public function create_rulesets( $pricing_rule_sets ) {
		global $ignitewoo_wsp_discounts;

		foreach ( $pricing_rule_sets as $pricing_rule_set) {

			$name = uniqid( 'set_' );

			$dd_pricing_rules = isset( $pricing_rule_set['rules'] ) ? $pricing_rule_set['rules'] : null;

			$pricing_conditions = isset( $pricing_rule_set['conditions'] ) ? $pricing_rule_set['conditions'] : null;

			$collector = isset( $pricing_rule_set['collector'] ) ? $pricing_rule_set['collector'] : null;

			$variation_rules = isset( $pricing_rule_set['variation_rules'] ) ? $pricing_rule_set['variation_rules'] : null;

			?>
			<script>
			jQuery( document ).ready( function() { 
				jQuery( '.woocommerce_dd_pricing_ruleset' ).not( 'enhancedd' ).each( function() { 
					setTimeout( function() { 
						jQuery( '.chosen' ).addClass( 'enhancedd').select2();	
						}, 500 );
				})
			});
			</script>
			
			<div id="woocommerce-pricing-ruleset-<?php echo $name; ?>" class="woocommerce_dd_pricing_ruleset">
				<div id="woocommerce-pricing-conditions-<?php echo $name; ?>" class="section">
					<h4 class="first"><?php _e( 'Discount Rule Group', 'ignitewoo_wholesale_pro' )?><a href="#" data-name="<?php echo $name; ?>" class="dd_delete_pricing_ruleset" style="float:right;"><span class="dashicons dashicons-plus" style="cursor:pointer margin:0 3px;" /></a></h4>
					<?php
					$condition_index = 0;

					if ( is_array( $pricing_conditions) && sizeof( $pricing_conditions) > 0 ) {
						?>
						<input type="hidden" name="dd_pricing_rules[<?php echo $name; ?>][conditions_type]" value="all" /> 

						<?php
						foreach ( $pricing_conditions as $condition ) { 

							$condition_index++;

							$this->create_condition( $condition, $name, $condition_index);

						} 

					} else {

						?>

						<input type="hidden" name="dd_pricing_rules[<?php echo $name; ?>][conditions_type]" value="all" /> 

						<?php

						$this->create_condition(array( 'type' => 'apply_to', 'args' => array( 'applies_to' => 'everyone', 'roles' => array())), $name, 1);

					}

					?>
				</div>

				<div id="woocommerce-pricing-collector-<?php echo $name; ?>" class="section" style="position:relative; z-index: 99">
					<?php
					if (is_array( $collector) && count( $collector) > 0) {

						$this->create_collector( $collector, $name);

					} else {

						$product_cats = array();

						$this->create_collector(array( 'type' => 'product', 'args' => array( 'cats' => $product_cats)), $name);
					}
					?>
				</div>

				<div id="woocommerce-pricing-variations-<?php echo $name; ?>" class="section" style="clear:both">
					<?php
					$variation_index = 0;

					if (is_array( $variation_rules) && count( $variation_rules) > 0) {

						$this->create_variation_selector( $variation_rules, $name);

					} else {

						$product_cats = array();

						$this->create_variation_selector(null, $name);

					}
					?>
				</div>
				<div class="clear"></div>
				<div class="section">
					<table id="woocommerce-pricing-rules-table-<?php echo $name; ?>" data-lastindex="<?php echo (is_array( $dd_pricing_rules) && sizeof( $dd_pricing_rules) > 0) ? count( $dd_pricing_rules) : '1'; ?>">
						<thead>
						<th style="width:22%">
							<?php _e( 'Minimum Quantity', 'ignitewoo_wholesale_pro' ); ?>
						</th>
						<th style="width:22%">
							<?php _e( 'Maximum Quantity', 'ignitewoo_wholesale_pro' ); ?>
						</th>
						<th style="width:22%">
							<?php _e( 'Discount Type', 'ignitewoo_wholesale_pro' ); ?>
						</th>
						<th style="width:22%">
							<?php _e( 'Discount Amount', 'ignitewoo_wholesale_pro' ); ?>
						</th>
						<th>&nbsp;</th>
						</thead>
						<tbody>
							<?php
							$index = 0;

							if (is_array( $dd_pricing_rules) && sizeof( $dd_pricing_rules) > 0 ) {

								foreach ( $dd_pricing_rules as $rule ) {

									$index++;

									$this->get_row( $rule, $name, $index );

								}

							} else {

								$this->get_row(array( 'to' => '', 'from' => '', 'amount' => '', 'type' => '' ), $name, 1 );

							}
							?>
						</tbody>
						<tfoot>
						</tfoot>
					</table>
				</div>
			</div>
			<?php
		}
	}

	public function create_empty_ruleset( $set_index ) {

		$pricing_rule_sets = array();

		$pricing_rule_sets['set_' . $set_index] = array();

		$pricing_rule_sets['set_' . $set_index]['title'] = 'Rule Set ' . $set_index;

		$pricing_rule_sets['set_' . $set_index]['rules'] = array();

		$this->create_rulesets( $pricing_rule_sets);

		die;
	}

	private function create_condition( $condition, $name, $condition_index ) {
		global $wp_roles;

		switch ( $condition['type'] ) {

		    case 'apply_to':
			$this->create_condition_apply_to( $condition, $name, $condition_index);
			break;

		    default:
			break;

		}
	}

	private function create_condition_apply_to( $condition, $name, $condition_index ) {
		global $wpdb;

		if ( !isset( $wp_roles ) )
			$wp_roles = new WP_Roles();

		$all_roles = $wp_roles->roles;

		/*
		$sql = '
			SELECT DISTINCT ID, user_login, user_email, m1.meta_value AS fname, m2.meta_value AS lname
			FROM ' . $wpdb->users . '
			LEFT JOIN ' . $wpdb->usermeta . ' m1 ON ID = m1.user_id
			LEFT JOIN ' . $wpdb->usermeta . ' m2 ON ID = m2.user_id
			WHERE 
			m1.meta_key = "billing_first_name"
			AND
			m2.meta_key = "billing_last_name"
			ORDER BY m2.meta_value ASC 
		';

		$all_users = $wpdb->get_results( $sql );
		*/
		$div_style = ( $condition['args']['applies_to'] != 'roles' ) ? 'display:none;' : '';
		?>

			<label for="pricing_rule_apply_to_<?php echo $name . '_' . $condition_index; ?>"><?php _e( 'Applies To', 'ignitewoo_wholesale_pro' )?>:</label>

			<input type="hidden" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][type]" value="apply_to" />

			<select title="<?php _e( 'Choose if this rule should apply to everyone, or to specific roles.  Useful if you only give discounts to existing customers, or if you have tiered pricing based on the users role.', 'ignitewoo_wholesale_pro' )?>" class="dd_pricing_rule_apply_to chosen" id="pricing_rule_apply_to_<?php echo $name . '_' . $condition_index; ?>" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][args][applies_to]" style="width:400px">
				<option <?php selected( 'everyone', $condition['args']['applies_to'] ); ?> value="everyone">Everyone</option>
				<option <?php selected( 'roles', $condition['args']['applies_to'] ); ?> value="roles">Specific Roles</option>
				<option <?php selected( 'users', $condition['args']['applies_to'] ); ?> value="users">Specific Users</option>
				<option <?php selected( 'unauthenticated', $condition['args']['applies_to'] ); ?> value="unauthenticated">Not Logged In</option>
			</select>

			<div class="roles" style="<?php echo $div_style; ?> margin-top: 10px;clear:both">
				<?php $chunks = array_chunk($all_roles, ceil(count($all_roles) / 3), true); ?>

				<p class="description"><?php _e( 'Click in the box and start typing a role name to select roles. Add as many as you need.', 'ignitewoo_wholesale_pro' )?></p>
				
				<select class="chosen dd_plus_rule_roles" multiple="multiple" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][args][roles][]" style="width:600px" >
				
				<?php foreach ($chunks as $chunk) : ?>

					<?php foreach ($chunk as $role_id => $role) : ?>
					
					<?php $role_checked = (isset($condition['args']['roles']) && is_array($condition['args']['roles']) && in_array($role_id, $condition['args']['roles'])) ? 'selected="selected"' : ''; ?>
					
					<option <?php echo $role_checked ?> value="<?php echo $role_id; ?>">
							<?php echo $role['name']; ?>
					</option>

					<?php endforeach; ?>
				
				<?php endforeach; ?>
				</select>
			</div>

			<div class="clear"></div>

			<div class="users" style="clear: both; z-index: 999999; <?php echo $div_style; ?> ">

				<p class="description"><?php _e( 'Click in the box and start typing a name to select users. Add as many as you need.', 'ignitewoo_wholesale_pro' )?></p>

				<select class="chosensss dd_plus_rule_users" multiple="multiple" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][args][users][]" style="width:600px">

					<?php 
					if ( isset( $condition['args']['users'] ) && is_array( $condition['args']['users'] ) ) {
						foreach( $condition['args']['users'] as $u ) { 
							$data = get_user_by( 'id', $u );
							if ( empty( $data ) || is_wp_error( $data ) )
								continue;
							$data = $data->data; 
							
							$formatted_name = '#' . $data->ID . ' &ndash; ';
							
							if ( $fname = ( get_user_meta( $data->ID, 'billing_first_name', true ) ) )
								$formatted_name .= $fname;
							if ( $lname = ( get_user_meta( $data->ID, 'billing_last_name', true ) ) )
								$formatted_name .= ' ' . $lname;
							if ( empty( $fname ) || empty( $lname ) )
								$formatted_name .= $data->user_login;
								
							$formatted_name .=  ' (' . $data->user_email . ')';
							
							?>
							<option <?php echo $u ?> value="<?php echo $data->ID ?>" selected="selected">
								<?php echo $formatted_name ?>
							</option>
							<?php
						}
					}
					
					?>
					<?php /*foreach( $all_users as $key => $data ) { ?>

						<?php $user_selected = (isset( $condition['args']['users'] ) && is_array( $condition['args']['users'] ) && in_array( $data->ID, $condition['args']['users'] )) ? 'selected="selected"' : ''; ?>

						<option <?php echo $user_selected ?> value="<?php echo $data->ID ?>">
							<?php echo $data->lname . ', ' . $data->fname . ' &mdash; ' . $data->user_email; ?>
						</option>

					<?php } */ ?>

				</select>

			</div>

			<div class="clear"></div>

		<?php
	}

	private function create_variation_selector( $condition, $name ) {
		global $post;

		$post_id = isset( $_POST['post'] ) ? intval( $_POST['post'] ) : $post->ID;

		//$product = new WC_Product( $post_id);
		
		$product = $this->get_product( $post_id );

		if ( !$product->has_child() )
			return;

		if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) )
			$all_variations = $product->get_children();
		else 
			$all_variations = $product->children;

		if ( version_compare( WOOCOMMERCE_VERSION, '2.4', '>=' ) && isset( $all_variations['all'] ) )
			$all_variations = $all_variations['all'];
			
		$div_style = ( $condition['args']['type'] != 'variations' ) ? 'display:none;' : '';


		$this->z = 0 ;
		
		?>

		<div>
			<label for="pricing_rule_variations_<?php echo $name; ?>"><?php _e( 'Product / Variations:', 'ignitewoo_wholesale_pro' ) ?></label>
			
			<select title="<?php _e( 'Choose what you would like to apply this pricing rule set to', 'ignitewoo_wholesale_pro' )?>" class="dd_pricing_rule_variations chosen" id="pricing_rule_variations_<?php echo $name; ?>" name="dd_pricing_rules[<?php echo $name; ?>][variation_rules][args][type]">
			
				<option <?php selected( 'product', $condition['args']['type'] ); ?> value="product"><?php _e( 'All Variations', 'ignitewoo_wholesale_pro' )?></option>
				<option <?php selected( 'variations', $condition['args']['type'] ); ?> value="variations"><?php _e( 'Specific Variations', 'ignitewoo_wholesale_pro' ) ?></option>
			</select>

			<div class="variations" style="<?php echo $div_style; ?>; clear:both">
			    <?php $chunks = array_chunk( $all_variations, ceil(count( $all_variations) / 3), true); ?>
			    
			    <?php foreach ( $chunks as $chunk) : ?>

			        <?php $this->z++ ?>
				<ul class="list-column">        
				    <?php foreach ( $chunk as $variation_id) : ?>

					<?php $variation_object = $this->get_product( $variation_id ); ?>
					
					<?php //$variation_object = new WC_Product_Variation( $variation_id); ?>
					
					<?php $variation_checked = (isset( $condition['args']['variations'] ) && is_array( $condition['args']['variations'] ) && in_array( $variation_id, $condition['args']['variations'] )) ? 'checked="checked"' : ''; ?>
					
					<li>
						<label for="variation_<?php echo $variation_id; ?>" class="selectit"> 
							<input <?php echo $variation_checked; ?> type="checkbox" id="variation_<?php echo $variation_id . $this->z; ?>" name="dd_pricing_rules[<?php echo $name; ?>][variation_rules][args][variations][]" value="<?php echo $variation_id; ?>" /> 
							<?php 
								//echo get_the_title( $variation_id ); 
								echo '#' . $variation_id . ' - ';
								$attrs = $variation_object->get_variation_attributes();
								if ( !empty( $attrs ) ) { 
									$a = array();
									foreach( $attrs as $key => $val ) { 
										$a[] = $variation_object->get_attribute( str_replace( 'attribute_', '', $key ) );
									}
									echo implode( ' &ndash; ', $a );
								}
							?>
						</label>
					</li>
					
				    <?php endforeach; ?>
				    
				</ul>
				
			    <?php endforeach; ?>

			</div>
			<div class="clear"></div>
		</div>
		<?php
	}


	private function create_collector( $collector, $name ) {

		$terms = (array) get_terms( 'product_cat', array( 'get' => 'all' ) );

		$div_style = ( $collector['type'] != 'cat' ) ? 'display:none;' : '';

		?>

		<label for="pricing_rule_when_<?php echo $name; ?>"><?php _e( 'Quantities based on:', 'ignitewoo_wholesale_pro' ); ?></label>
		
		<select title="<?php _e( 'Choose how to calculate the quantity.  This tallied amount is used in determining the min and max quantities used below in the Quantity Pricing section.', 'ignitewoo_wholesale_pro' )?>" class="dd_pricing_rule_when chosen" id="pricing_rule_when_<?php echo $name; ?>" name="dd_pricing_rules[<?php echo $name; ?>][collector][type]">
		
			<option title="<?php _e( 'Calculate quantity based on the Product ID', 'ignitewoo_wholesale_pro' )?>" <?php selected( 'product', $collector['type'] ); ?> value="product"><?php _e( 'Product Quantity', 'ignitewoo_wholesale_pro' ); ?></option>
			
			<option title="<?php _e( 'Calculate quantity based on the Variation ID', 'ignitewoo_wholesale_pro' )?>" <?php selected( 'variation', $collector['type'] ); ?> value="variation"><?php _e( 'Variation Quantity', 'ignitewoo_wholesale_pro' ); ?></option>
			
			<?php /*
			<option title="<?php _e( 'Calculate quantity based on the Cart Line Item', 'ignitewoo_wholesale_pro' )?>" <?php selected( 'cart_item', $collector['type'] ); ?> value="cart_item"><?php _e( 'Cart Line Item Quantity', 'ignitewoo_wholesale_pro' ); ?></option>
			

			<option title="<?php _e( 'Calculate quantity based on total amount of a category in the cart', 'ignitewoo_wholesale_pro' )?>" <?php selected( 'cat', $collector['type'] ); ?> value="cat"><?php _e( 'Quantity of Category', 'ignitewoo_wholesale_pro' ); ?></option>
			*/ ?>
		</select>
		<br />
		
		<?php /*
		<div class="cats" style="<?php echo $div_style; ?>">

			<?php $chunks = array_chunk($terms, ceil(count($terms) / 3)); ?>
				
			<select class="chosen dd_plus_rule_cats" multiple="multiple" name="dd_pricing_rules[<?php echo $name; ?>][collector][args][cats][]" >
						
			<?php foreach ($chunks as $chunk) : ?>

				<?php foreach ($chunk as $term) : ?>
				
					<?php $term_checked = (isset($collector['args']['cats']) && is_array($collector['args']['cats']) && in_array($term->term_id, $collector['args']['cats'])) ? 'selected="selected"' : ''; ?> 
					
					<option value="<?php echo $term->term_id; ?>" <?php echo $term_checked; ?>><?php echo $term->name; ?> </option>
					
					
				<?php endforeach; ?>

				
			<?php endforeach; ?>
			
			</select>
			
			<div style="clear:both;"></div>

		    <div class="clear"></div>
		</div>

		<?php
		*/
	}

	private function get_row( $rule, $name, $index ) {
		global $ignitewoo_wsp_discounts;
		?>
		<tr id="dd_pricing_rule_row_<?php echo $name . '_' . $index; ?>">
			<td>
				<input title="<?php _e( 'Apply this adjustment when the quantity in the cart starts at this value.  Use * for any.', 'ignitewoo_wholesale_pro' )?>" class="dd_int_pricing_rule" id="pricing_rule_from_input_<?php echo $name . '_' . $index; ?>" type="text" name="dd_pricing_rules[<?php echo $name; ?>][rules][<?php echo $index ?>][from]" value="<?php echo $rule['from']; ?>" />
			</td>
			<td>
			    <input title="<?php _e( 'Apply this adjustment when the quantity in the cart is less than this value.  Use * for any.', 'ignitewoo_wholesale_pro' )?>" class="dd_int_pricing_rule" id="pricing_rule_to_input_<?php echo $name . '_' . $index; ?>" type="text" name="dd_pricing_rules[<?php echo $name; ?>][rules][<?php echo $index ?>][to]" value="<?php echo $rule['to']; ?>" />
			</td>
			<td>
			    <select class="chosen" title="<?php _e( 'The type of adjustment to apply', 'ignitewoo_wholesale_pro' )?>" id="pricing_rule_type_value_<?php echo $name . '_' . $index; ?>" name="dd_pricing_rules[<?php echo $name; ?>][rules][<?php echo $index; ?>][type]" style="width:100%">
				    <option <?php selected( 'price_discount', $rule['type'] ); ?> value="price_discount">Price Discount</option>
				    <option <?php selected( 'percentage_discount', $rule['type'] ); ?> value="percentage_discount">Percentage Discount</option>
				    <option <?php selected( 'fixed_price', $rule['type'] ); ?> value="fixed_price">Fixed Price</option>
			    </select>
			</td>
			<td>
			    <input title="<?php _e( 'The value of the adjustment. Currency and percentage symbols are not required', 'ignitewoo_wholesale_pro' )?>" class="dd_float_pricing_rule" id="pricing_rule_amount_input_<?php echo $name . '_' . $index; ?>" type="text" name="dd_pricing_rules[<?php echo $name; ?>][rules][<?php echo $index; ?>][amount]" value="<?php echo $rule['amount']; ?>" /> 
			</td>
			<td width="60"><a class="dd_add_pricing_rule" data-index="<?php echo $index; ?>" data-name="<?php echo $name; ?>"><span class="dashicons dashicons-plus"></a><a <?php echo ( $index > 1) ? '' : 'style="display:none;"'; ?> class="dd_delete_pricing_rule" data-index="<?php echo $index; ?>" data-name="<?php echo $name; ?>"><span class="dashicons dashicons-minus"></a>
			</td>
		</tr>
		<?php
	}

	private function meta_box_javascript() {
		global $ignitewoo_wsp_discounts;

		?>
		<script type="text/javascript">

		    jQuery(document).ready(function( $ ) {
			var set_index = 0;
			var rule_indexes = new Array();

			$( '.woocommerce_dd_pricing_ruleset' ).each(function(){
				var length = $( 'table tbody tr', $(this) ).length;
				if (length==1) {
				    $( '.delete_pricing_rule', $(this)).hide(); 
				}
			});

			$("#woocommerce-dd-pricing-add-ruleset, .var_minmax_add_rule button").click(function(event) {
				event.preventDefault();

				var set_index = $("#woocommerce-dd-pricing-rules-wrap").data( 'setindex' ) + 1;

				var btn = $( this );
				
				$("#woocommerce-dd-pricing-rules-wrap").data( 'setindex', set_index );
				
				btn.block({
					message: null,
					overlayCSS: {
						background: '#333',
						opacity: 0.6
					}
				});
				
				$( '#woocommerce-dd-sitewide-pricing-rules-wrap' ).block({
					message: null,
					overlayCSS: {
						background: '#333',
						opacity: 0.6
					}
				});
				
				var data = {
					set_index:set_index,
					post:<?php echo isset( $_GET['post'] ) ? $_GET['post'] : 0; ?>,
					action:'ignitewoo_dd_product_create_empty_ruleset'
				}

				$.post(ajaxurl, data, function(response) { 
					$( '#woocommerce-dd-pricing-rules-wrap' ).append( response );
					dd_price_rules_select_users();
					$( '.dd_pricing_rule_apply_to').each( function() { 
						$(this).change();
						$( '#dynamic_dd_pricing_data  .chosen' ).not( '.enhanced_select' ).each( function() { 
							//$( 'select.chosen' ).addClass( 'enhanced_select' ).select2();	
						})
						btn.unblock();
						$( '#woocommerce-dd-sitewide-pricing-rules-wrap' ).unblock();
					});

				});                                                                                                    

			});
			function dd_price_rules_select_users() {
				var ign_select2_args = {
					allowClear:  $( this ).data( 'allow_clear' ) ? true : false,
					placeholder: $( this ).data( 'placeholder' ),
					minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '3',
					escapeMarkup: function( m ) {
						return m;
					},
					ajax: {
						url:         wc_enhanced_select_params.ajax_url,
						dataType:    'json',
						quietMillis: 250,
						delay: 250,
						data: function( params ) {
							return {
								term:     params.term,
								action:   'woocommerce_json_search_customers',
								security: wc_enhanced_select_params.search_customers_nonce,
								exclude:  $( this ).data( 'exclude' )
							};
						},
						results: function( data ) {
							var terms = [];
							if ( data ) {
								$.each( data, function( id, text ) {
									terms.push({
										id: id,
										text: text
									});
								});
							}
							return {
								results: terms
							};
						},
						processResults: function( data ) {
							var terms = [];
							if ( data ) {
								$.each( data, function( id, text ) {
									terms.push({
										id: id,
										text: text
									});
								});
							}
							return {
								results: terms
							};
						},
						cache: true
					}
				};
				$( '.dd_plus_rule_users' ).unbind().select2( ign_select2_args );
			};
			dd_price_rules_select_users();
			
			$( '#woocommerce-dd-pricing-rules-wrap' ).delegate( '.dd_pricing_rule_apply_to', 'change', function(event) {  
				var value = $(this).val();

				if (value != 'roles' && value != 'users' ) {
				    $( '.users', $(this).parent()).hide();
				    $( '.users input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
				    $( '.roles', $(this).parent()).hide();
				    $( '.roles input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
				} else if ( value == 'roles' ) {
				    $( '.users', $(this).parent()).hide();
				    $( '.users input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
				    $( '.roles', $(this).parent()).fadeIn();
				} else if ( value == 'users' ) { 
				    $( '.roles', $(this).parent()).hide();
				    $( '.roles input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
				    $( '.users', $(this).parent()).fadeIn();                               
				}

			});

			$( '.dd_pricing_rule_apply_to').each( function() { 
				$(this).change();
				//$("#dynamic_dd_pricing_data .dd_plus_rule_users").select2();
			});

			$( '#woocommerce-dd-pricing-rules-wrap' ).delegate( '.dd_pricing_rule_variations', 'change', function(event) {  
				var value = $(this).val();
				if (value != 'variations' ) {
				    $( '.variations', $(this).parent()).fadeOut();
				    $( '.variations input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
				} else {                                                            
				    $( '.variations', $(this).parent()).fadeIn();
				}                                                              
			});

			$( '#woocommerce-dd-pricing-rules-wrap' ).delegate( '.dd_pricing_rule_when', 'change', function(event) {  
			    var value = $(this).val();
			    if (value != 'cat' ) {
				$( '.cats', $(this).closest( 'div' )).fadeOut();
				$( '.cats input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );

			    } else {                                                            
				$( '.cats', $(this).closest( 'div' )).fadeIn();
			    }                                                              
			});

			//Remove Pricing Set
			$( '#woocommerce-dd-pricing-rules-wrap' ).delegate( '.dd_delete_pricing_ruleset', 'click', function(event) {  
			    event.preventDefault();
			    DeleteRuleSet( $(this).data( 'name' ) );
			});

			//Add Button
			$( '#woocommerce-dd-pricing-rules-wrap' ).delegate( '.dd_add_pricing_rule', 'click', function(event) {  
			    event.preventDefault();
			    InsertRule( $(this).data( 'index' ), $(this).data( 'name' ) );
			});

			//Remove Button                
			$( '#woocommerce-dd-pricing-rules-wrap' ).delegate( '.dd_delete_pricing_rule', 'click', function(event) {  
			    event.preventDefault();
			    DeleteRule( $(this).data( 'index' ), $(this).data( 'name' ) );
			});

			//Validation
			$( '#woocommerce-dd-pricing-rules-wrap' ).delegate( '.dd_int_pricing_rule', 'keydown', function(event) {  
			    // Allow only backspace, delete and tab
			    if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 190 ) {
				// let it happen, don't do anything
			    }
			    else {
				if (event.shiftKey && event.keyCode == 56){
				    if ( $(this).val().length > 0) {
					event.preventDefault();
				    } else {
					return true;    
				    }
				}else if (event.shiftKey){
				    event.preventDefault();
				} else if ( (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) ) {
				    event.preventDefault(); 
				} else {
				    if ( $(this).val() == "*") {
					    event.preventDefault();
				    }
				}
			    }
			});

			$( '#woocommerce-dd-pricing-rules-wrap' ).delegate( '.dd_float_pricing_rule', 'keydown', function(event) {  
				// Allow only backspace, delete, tab, and decimal
				if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 190 ) {

					    var v = jQuery( this ).val();

					    v = v.replace( /[^0-9\.]/g, "" );

					    jQuery( this ).val( v );

				} else if ( ( event.keyCode >= 48 ) && ( event.keyCode <= 57 ) )  {

					    var v = jQuery( this ).val();

					    v = v.replace( /[^0-9\.]/g, "" );

					    jQuery( this ).val( v );

				} else if ( event.keyCode < 48 || event.keyCode > 57 ) {

					    event.preventDefault(); 

					    var v = jQuery( this ).val();

					    v = v.replace( /[^0-9\.]/g, "" );

					    jQuery( this ).val( v );

				} else if ( event.keyCode < 96 || event.keyCode > 105 ) { 

					    event.preventDefault(); 

					    var v = jQuery( this ).val();

					    v = v.replace( /[^0-9\.]/g, "" );

					    jQuery( this ).val( v );
				}
			});

			$("#woocommerce-dd-pricing-rules-wrap").sortable(
			{ 
			    handle: 'h4.first',
			    containment: 'parent',
			    axis:'y'
			});
			
			function InsertRule(previousRowIndex, name) {

			    var $index = $("#woocommerce-pricing-rules-table-" + name).data( 'lastindex' ) + 1;
			    $("#woocommerce-pricing-rules-table-" + name).data( 'lastindex', $index );

			    var html = '';
			    html += '<tr id="dd_pricing_rule_row_' + name + '_' + $index + '">';
			    html += '<td>';
			    html += '<input class="dd_int_pricing_rule" id="pricing_rule_from_input_'  + name + '_' + $index + '" type="text" name="dd_pricing_rules[' + name + '][rules][' + $index + '][from]" value="" /> ';
			    html += '</td>';
			    html += '<td>';
			    html += '<input class="dd_int_pricing_rule" id="pricing_rule_to_input_' + name + '_' + $index + '" type="text" name="dd_pricing_rules[' + name + '][rules][' + $index + '][to]" value="" /> ';
			    html += '</td>';
			    html += '<td>';
			    html += '<select class="chosen" id="pricing_rule_type_value_' + name + '_' + $index + '" name="dd_pricing_rules[' + name + '][rules][' + $index + '][type]">';
			    html += '<option value="price_discount"><?php _e( 'Price Discount','ignitewoo_wholesale_pro' )?></option>';
			    html += '<option value="percentage_discount"><?php _e( 'Percentage Discount','ignitewoo_wholesale_pro' )?></option>';
			    html += '<option value="fixed_price"><?php _e( 'Fixed Price','ignitewoo_wholesale_pro' )?></option>';
			    html += '</select>';
			    html += '</td>';
			    html += '<td>';
			    html += '<input class="dd_float_pricing_rule" id="pricing_rule_amount_input_' + $index + '" type="text" name="dd_pricing_rules[' + name + '][rules][' + $index + '][amount]" value="" /> ';
			    html += '</td>';
			    html += '<td width="48">';
			    html += '<a data-index="' + $index + '" data-name="' + name + '" class="dd_add_pricing_rule"><span class="dashicons dashicons-plus" style="cursor:pointer"></a>';
			    html += '<a data-index="' + $index + '" data-name="' + name + '" class="dd_delete_pricing_rule"><span class="dashicons dashicons-minus" style="cursor:pointer"></a>';
			    html += '</td>';
			    html += '</tr>';
																																
			    $( '#dd_pricing_rule_row_' + name + '_' + previousRowIndex).after(html);
			    $( '.dd_delete_pricing_ruleset', "#woocommerce-pricing-rules-table-" + name).show();

			} 

			function DeleteRule(index, name) {
			    if (confirm( "<?php _e('Are you sure you would like to remove this price adjustment?','ignitewoo_wholesale_pro' ) ?>")) {
				$( '#dd_pricing_rule_row_' + name + '_' + index).remove();
																																
				var $index = $( 'tbody tr', "#woocommerce-pricing-rules-table-" + name).length;
				if ( $index > 1) {
				    $( '.dd_delete_pricing_ruleset', "#woocommerce-pricing-rules-table-" + name).show();
				} else {
				    $( '.dd_delete_pricing_ruleset', "#woocommerce-pricing-rules-table-" + name).hide();
				}
			    }
			}

			function DeleteRuleSet(name) {
			    if ( confirm( "<?php _e('Are you sure you would like to remove this dynamic price set?','ignitewoo_wholesale_pro' )?>" )){
				$( '#woocommerce-pricing-ruleset-' + name ).slideUp().remove();  
			    }
			}



		    });

		</script>
		<?php
	}

	public function meta_box_css() {
	    ?>
		<style>
		#woocommerce-pricing-product div.section { margin-bottom: 10px; }
		#woocommerce-pricing-product label { display:block; font-weight: bold; }
		#woocommerce-pricing-product .list-column { float:left; margin-right:25px; }
		#dynamic_dd_pricing_data { padding: 12px; margin: 0; overflow: hidden; zoom: 1; }
		#woocommerce-dd-pricing-rules-wrap h4 {  margin-bottom: 10px; border-bottom: 1px solid #E5E5E5; padding-bottom: 6px;  font-size: 1.2em; margin: 1em 0 1em; text-transform: none; }
		#woocommerce-dd-pricing-rules-wrap h4.first { margin-top:0px; cursor:move; }
		#woocommerce-dd-pricing-rules-wrap select { width:250px; }
		.woocommerce_dd_pricing_ruleset {
			border-color:#dfdfdf;
			border-width:1px;
			border-style:solid;
			-moz-border-radius:3px;
			-khtml-border-radius:3px;
			-webkit-border-radius:3px;
			border-radius:3px;
			padding: 0;
			border-style:solid;
			border-spacing:0;
			background-color:#fff;
			margin-bottom: 12px;
			margin-left: 20px;
		}
		</style>
	    <?php
	}

	public function process_meta_box( $post_id, $post ) {

		$dd_pricing_rules = array();

		$valid_rules = array();

		if ( isset( $_POST['dd_pricing_rules'] ) ) {

			$pricing_rule_sets = $_POST['dd_pricing_rules'];

			foreach ( $pricing_rule_sets as $key => $rule_set ) {

				$valid = true;

				foreach ( $rule_set['rules'] as $rule ) {

				    if (isset( $rule['to'] ) && isset( $rule['from'] ) && isset( $rule['amount'] ) )
					    $valid = $valid & true;
				    else
					    $valid = $valid & false;

				}

				if ( $valid )
				    $valid_rules[$key] = $rule_set;

			}

			update_post_meta( $post_id, '_dd_pricing_rules', $valid_rules);

		} else {

			delete_post_meta( $post_id, '_dd_pricing_rules' );

		}
	}

}
