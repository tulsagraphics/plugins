<?php
// Wholesale registration button, injecting into the My Account login/register page if the setting is enabled
?>
<p style="clear:both">
	<a href="<?php echo get_permalink( $reg_page ) ?>" class="ign_wsp_register_button button">
		<?php echo $btn_label ?>
	</a>
</p>