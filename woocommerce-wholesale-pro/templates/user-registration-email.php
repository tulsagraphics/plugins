<?php
/** 
* This template can be overridden by copying it to yourtheme/woocommerce/wholesale-pro/user-registration-email.php
*/
?>

<?php if ( !defined('ABSPATH' ) ) die; ?>

<?php do_action('woocommerce_email_header', $email_heading ); ?>

<p><?php _e( 'This message is from', 'ignitewoo_wholesale_pro' )?> <?php echo ' ' . $sitename; ?></p>

<p> <?php _e( 'Hi', 'ignitewoo_wholesale_pro' ); ?>, </p>
		
<p><?php echo sprintf( __( 'A person registered for access. User: <a href="%s">%s</a> (%s).', 'ignitewoo_wholesale_pro' ), admin_url( 'user-edit.php?user_id=' . $id ), $login, $email ) ?></p>

<?php // if there are any fields other than password fields then include those in the message ?>
<?php 
if ( !empty( $extra_data ) ) {

	?>
	<table style="width:100%" class="shop_table order_details"> 
	<?php 
	foreach( $extra_data as $item ) { 
		?>
		<tr>
			<td valign="top"><?php echo $item['title'] ?></td>
			<td valign="top"><?php echo wpautop( $item['content'] ) ?></td>
		</tr>
		<?php 
	}
	?>
	</table>
	<?php 
} 
?>

<p>
<?php echo sprintf( __( 'You can edit there account <a href="%s">here</a>', 'ignitewoo_wholesale_pro' ), admin_url( 'user-edit.php?user_id=' . $id ) ); ?>
</p>
<?php // draw a line before the "automated message" notice below: ?>
<div style="clear:both;border-top:1px solid #ccc;padding-top:1em"></div>

<p><?php echo __( "This is an automated message delivered by IgniteWoo's WooCommerce Wholesale Pro extension", 'ignitewoo_wholesale_pro' ) ?></p>

<div style="clear:both;"></div>

<?php do_action('woocommerce_email_footer'); ?>
