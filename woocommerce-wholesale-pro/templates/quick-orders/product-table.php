<?php 
/*
 * Copyright (c) 2016 - IgniteWoo.com - ALL RIGHTS RESERVED
 */

// Main table that displays products. 
// WARNING: Do not alter CSS class and ID names. 

global $products, $quick_order_settings, $post;

?>
<style>
.ignitewoo_quick_order_form_wrap .qty {
	width: 75px;
}
</style>


<div class="woocommerce columns-1">

<?php if ( $products->have_posts() ) { ?>

	<?php include( dirname( __FILE__ ) . '/page-navigation.php' ) ?>

	<table class="shop_table">
		<thead>
			<tr>
				<th class="product_title"><?php _e( 'Product', 'ignitewoo_wholesale_pro' ) ?></th>
				
				<?php if ( !empty( $quick_order_settings['show_sku'] ) && 'yes' == $quick_order_settings['show_sku'] ) { ?>
				<th class="product_sku"><?php _e( 'SKU', 'ignitewoo_wholesale_pro' ) ?></th>
				<?php } ?>
				
				<th><?php _e( 'Price', 'ignitewoo_wholesale_pro' ) ?></th>
				<th class="action" style="text-align:center"><?php _e( 'Action', 'ignitewoo_wholesale_pro' ) ?></th>
			</tr>
		</thead>
		<tbody>
		<?php 

		while ( $products->have_posts() ) {

			$products->the_post(); 
			
			$product = wc_get_product( $post->ID ); 

			if ( !is_object( $product ) ) { 
				continue;
			}

			if ( !$product->is_visible() )
				continue;

			if ( !$product->is_type( 'simple' ) && !$product->is_type( 'variable' ) )
				continue;

			// Disallow products without a price, free products have a price of zero (0) and those are Ok
			if ( $product->is_type( 'simple' ) && '' == $product->get_price() )
				continue;

		?>
		<tr class="product_row">
			<td class="product_info">
				<?php 

				if ( !empty( $quick_order_settings['show_image'] ) && 'yes' == $quick_order_settings['show_image'] ) {
					if ( has_post_thumbnail() ) {

						$image_title = esc_attr( get_the_title( get_post_thumbnail_id() ) );
						$image_link  = wp_get_attachment_url( get_post_thumbnail_id() );
						$image       = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_thumbnail' ), array(
							'title' => $image_title
							) );

						if ( method_exists( $product, 'get_gallery_image_ids' ) )
							$attachment_count = count( $product->get_gallery_image_ids() );
						else 
							$attachment_count = count( $product->get_gallery_attachment_ids() );

						if ( $attachment_count > 0 ) {
							$gallery = '[product-gallery]';
						} else {
							$gallery = '';
						}

						echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="%s" itemprop="image" class="woocommerce-main-image" title="%s" data-rel="prettyPhoto' . $gallery . '">%s</a>', $image_link, $image_title, $image ), $post->ID );

					} else {

						echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img class="attachment-shop_thumbnail" src="%s" alt="Placeholder" />', wc_placeholder_img_src() ), $post->ID );

					}
				}
			?>
				<div class="the_title"><?php the_title()?></div>
			</td>
			
			<?php if ( !empty( $quick_order_settings['show_sku'] ) && 'yes' == $quick_order_settings['show_sku'] ) { ?>
			<td class="product_sku"><?php echo $product->get_sku() ?></td>
			<?php } ?>
			
			<td class="quick_order_price"><?php echo $product->get_price_html() ?></td>
			
			<td class="quick_order_mini_form">
				<?php if ( $product->is_type( 'simple' ) ) { ?>
			
					<?php if ( $product->is_in_stock() || ( !$product->is_in_stock() && $product->backorders_allowed() ) ) { ?>
			
					<form class="cart" method="post" enctype='multipart/form-data'>

					<?php
						if ( ! $product->is_sold_individually() )
							woocommerce_quantity_input( array(
								'min_value' => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
								'max_value' => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product )
							) );
					?>

					<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $post->ID ); ?>" />

					<?php
						echo apply_filters( 'woocommerce_loop_add_to_cart_link',
							sprintf( '<button href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="button %s product_type_%s">%s</button>',
								esc_url( $product->add_to_cart_url() ),
								esc_attr( $post->ID ),
								esc_attr( $product->get_sku() ),
								$product->is_purchasable() && $product->is_in_stock() ? 'single_add_to_cart_button' : '',
								esc_attr( $product->get_type() ),
								esc_html( $product->add_to_cart_text() )
							),
						$product );
					?>
					</form>
					
					<?php } else { 
						_e( 'Out of stock', 'ignitewoo_wholesale_pro' );
					} 
					
				} else { 
					
					wp_enqueue_script( 'ign-add-to-cart-variation' );
					woocommerce_variable_add_to_cart();
				}
				?>
			</td>
		</tr>
		<?php 
		} // end of the loop. ?>
		</tbody>
	</table>
	<?php include( dirname( __FILE__ ) . '/page-navigation.php' ) ?>
<?php }

wp_reset_postdata();
?>
</div>
