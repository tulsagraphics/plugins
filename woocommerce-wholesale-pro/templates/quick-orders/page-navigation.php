<?php 
/*
 * Copyright (c) 2016 - IgniteWoo.com - ALL RIGHTS RESERVED
 */

// Page navigation. 
// WARNING: Do not alter CSS class and ID names. 

global $quick_order_settings, $max_number_pages;

if ( empty( $quick_order_settings['disable_paging'] ) || 'yes' !== $quick_order_settings['disable_paging'] ) { ?>
	<nav class="woocommerce-pagination" >
	<?php

	$current_page = get_query_var( 'paged' );
	
	if ( empty( $current_page ) && !empty( $quick_order_settings['paged'] ) )
		$current_page = $quick_order_settings['paged'];

	$args = array();
	
	if ( isset( $_POST['data'] ) ) 
		$post_args = wp_parse_args( $_POST['data'] );
	
	if ( isset( $post_args['quick_order_search'] ) )
		$args['quick_order_search'] = $post_args['quick_order_search'];
	
	if ( isset( $post_args['cat'] ) )
		$args['cat'] = $post_args['cat'];
		
	if ( !empty( $args ) ) {
		$args['action'] = 'quick_order_product_search';
		$args['quick_order_form_id'] = $quick_order_settings['form_id'];
	}

	echo paginate_links( apply_filters( 'woocommerce_pagination_args', array(
		'base'         => str_replace( 9999999, '%#%', get_pagenum_link( 9999999 ) ),
		'format' 	=> '%#%',
		'current'      => max( 1, $current_page ),
		'total'        => $max_number_pages,
		'prev_text'    => '&larr;',
		'next_text'    => '&rarr;',
		'type'         => 'list',
		'end_size'     => 3,
		'mid_size'     => 3,
		'add_args' 	=> $args
	) ) );
	?>
	</nav>
<?php } ?>
