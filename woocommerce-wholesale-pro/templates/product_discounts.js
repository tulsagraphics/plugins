jQuery( document ).ready( function( $ ) {

	$( '.variations_form' ).on( 'click', '.reset_variations', function( event ) {
		$( '.estimated_discounts' ).hide('fast');
		$( '.estimated_cost' ).html('');
		$( '.estimated_cost_note' ).hide('fast');
		return false;
	});
	
	$( '.variations_form' ).on( 'check_variations', function( event, variation ) {
		var all_set = true;
		$( '.variations_form' ).find('.variations select').each( function() {
			
			if ( $(this).val().length == 0 ) {
				all_set = false;
			} else {
				any_set = true;
			}
			
		})
		
		if ( !all_set )
			$( '.estimated_discounts' ).hide( 'slow' );

	})
		
	$( '.variations_form' ).on( 'found_variation', function( event, variation ) {
		$( '.estimated_discounts' ).show();
	})
		
	// If the product page is for a simple product then this would trigger: 
	if ( 0 == $( '.variations_form' ).length ) { 
		$( '.estimated_discounts' ).show();
	}
	
	$( '.check_discounts' ).click( function() { 
	
		btn = $( this );
		
		btn.block( {message: null, overlayCSS: {background: '#fff url(' + woocommerce_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.6}});

		var item_data = $( 'form.cart' ).serialize();
		
		var product_id = btn.data( 'pid' );
		
		item_data += '&product_id=' + product_id;
		
		$.post( woocommerce_params.ajax_url, { action: 'get_simulated_price', item_data:item_data }, function( data ) { 
		
			data = 'Estimated cost: ' + data;
			
			$( '.estimated_cost' ).html( data );
			$( '.estimated_cost_note' ).show();
			
			$( btn ).unblock();
		})
		
		return false;
	
	})
	
	$('.variations_form .variations select').change();
})
