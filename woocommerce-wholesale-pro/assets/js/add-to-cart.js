/*
 * Copyright (c) 2016 - IgniteWoo.com - ALL RIGHTS RESERVED
 * Portions are Copyright (c) 2012 - WooThemes- ALL RIGHTS RESERVED
 */
function qof_simple() { 
		
	jQuery( function( $ ) {

		$( '.single_add_to_cart_button' ).each( function() {
			$( this ).removeClass( 'alt' );
		})

		// wc_add_to_cart_params is required to continue, ensure the object exists
		if ( typeof wc_add_to_cart_params === 'undefined' )
			return false;

		setTimeout( function() { 

			$( '.single_add_to_cart_button, .add_to_cart_button' ).unbind();
			
			// Ajax add to cart
			$( '.single_add_to_cart_button, .add_to_cart_button' ).on( 'click', function(e) {

				e.preventDefault();
				
				$( this ).blur();
				
				// AJAX add to cart request
				var $thisbutton = $( this );

				if ( $thisbutton.is( '.product_type_simple' ) ) {

					if ( ! $thisbutton.attr( 'data-product_id' ) )
						return true;

					$thisbutton.removeClass( 'added' );
					$thisbutton.addClass( 'loading' );
					
					var qty = $thisbutton.closest( 'form' ).find( '.qty' ).val();

					var data = {
						action: 'woocommerce_add_to_cart',
						product_id: $thisbutton.attr( 'data-product_id' ),
						quantity: qty
					};

					// Trigger event
					$( 'body' ).trigger( 'adding_to_cart', [ $thisbutton, data ] );

					// Ajax action
					$.post( wc_add_to_cart_params.ajax_url, data, function( response ) {

						if ( ! response )
							return;

						var this_page = window.location.toString();

						this_page = this_page.replace( 'add-to-cart', 'added-to-cart' );

						if ( response.error && response.product_url ) {
							window.location = response.product_url;
							return;
						}

						// Redirect to cart option
						if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {

							window.location = wc_add_to_cart_params.cart_url;
							return;

						} else {

							$thisbutton.removeClass( 'loading' );

							fragments = response.fragments;
							cart_hash = response.cart_hash;

							// Block fragments class
							if ( fragments ) {
								$.each( fragments, function( key, value ) {
									$( key ).addClass( 'updating' );
								});
							}

							// Block widgets and fragments
							$( '.shop_table.cart, .updating, .cart_totals' ).fadeTo( '400', '0.6' ).block({ message: null, overlayCSS: { background: 'transparent url(' + wc_add_to_cart_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.6 } } );

							// Changes button classes
							//$thisbutton.addClass( 'added' );
							// Colorize background
							var trtemp = $thisbutton.closest( 'tr' );
							
							$( trtemp ).find( 'td' ).each( function() { 
								var savedbg = $( this ).css( 'background-color' );
								$( this ).css( { 'background-color' : "#defce0" } );
								setTimeout( function() { 
									$( trtemp ).find( 'td' ).css( { 'background-color' : savedbg } );
								}, 500 );
							});
							

							// View cart text
							/*
							if ( ! wc_add_to_cart_params.is_cart && $thisbutton.parent().find( '.added_to_cart' ).size() === 0 ) {
								$thisbutton.after( ' <a href="' + wc_add_to_cart_params.cart_url + '" class="added_to_cart wc-forward" title="' + 
									wc_add_to_cart_params.i18n_view_cart + '">' + wc_add_to_cart_params.i18n_view_cart + '</a>' );
							}
							*/
							
							// Replace fragments
							if ( fragments ) {
								$.each( fragments, function( key, value ) {
									$( key ).replaceWith( value );
								});
							}

							// Unblock
							$( '.widget_shopping_cart, .updating' ).stop( true ).css( 'opacity', '1' ).unblock();

							// Cart page elements
							$( '.shop_table.cart' ).load( this_page + ' .shop_table.cart:eq(0) > *', function() {

								$( 'div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)' ).addClass( 'buttons_added' ).append( '<input type="button" value="+" id="add1" class="plus" />' ).prepend( '<input type="button" value="-" id="minus1" class="minus" />' );

								$( '.shop_table.cart' ).stop( true ).css( 'opacity', '1' ).unblock();

								$( 'body' ).trigger( 'cart_page_refreshed' );
							});

							$( '.cart_totals' ).load( this_page + ' .cart_totals:eq(0) > *', function() {
								$( '.cart_totals' ).stop( true ).css( 'opacity', '1' ).unblock();
							});

							// Trigger event so themes can refresh other areas
							$( 'body' ).trigger( 'added_to_cart', [ fragments, cart_hash ] );
							$( 'body' ).trigger( 'wc_fragment_refresh', [ fragments, cart_hash ] );
						}
					});

							
					$( this ).blur();
				
					return false;

				} else { 
				
					$thisbutton.addClass( 'loading' );
					
					var v = $( this ).closest( 'form' ).serialize();
					
					var data = {
						action: 'ign_add_to_cart_variable',
						args: v
					};

					// Trigger event
					$( 'body' ).trigger( 'adding_to_cart', [ $thisbutton, data ] );

					// Ajax action
					$.post( wc_add_to_cart_params.ajax_url, data, function( response ) {
						//console.log( response );
						$thisbutton.removeClass( 'loading' );
						
						if ( ! response )
							return;

						var this_page = window.location.toString();

						this_page = this_page.replace( 'add-to-cart', 'added-to-cart' );
						
						fragments = response.fragments;
						cart_hash = response.cart_hash;

						// Block fragments class
						if ( fragments ) {
							$.each( fragments, function( key, value ) {
								$( key ).addClass( 'updating' );
							});
						}

						// Block widgets and fragments
						$( '.shop_table.cart, .updating, .cart_totals' ).fadeTo( '400', '0.6' ).block({ message: null, overlayCSS: { background: 'transparent url(' + wc_add_to_cart_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.6 } } );

						// Changes button classes
						//$thisbutton.addClass( 'added' );
						// Colorize background - turns green for 1/2 second when item added to the cart
						var trtemp = $thisbutton.closest( 'tr' );
						
						$( trtemp ).find( 'td' ).each( function() { 
							var savedbg = $( this ).css( 'background-color' );
							$( this ).css( { 'background-color' : "#defce0" } );
							setTimeout( function() { 
								$( trtemp ).find( 'td' ).css( { 'background-color' : savedbg } );
							}, 500 );
						});
						
						// View cart text
						/*
						if ( ! wc_add_to_cart_params.is_cart && $thisbutton.parent().find( '.added_to_cart' ).size() === 0 ) {
							$thisbutton.after( ' <a href="' + wc_add_to_cart_params.cart_url + '" class="added_to_cart wc-forward" title="' + 
								wc_add_to_cart_params.i18n_view_cart + '">' + wc_add_to_cart_params.i18n_view_cart + '</a>' );
						}
						*/
						
						// Replace fragments
						if ( fragments ) {
							$.each( fragments, function( key, value ) {
								$( key ).replaceWith( value );
							});
						}

						// Unblock
						$( '.widget_shopping_cart, .updating' ).stop( true ).css( 'opacity', '1' ).unblock();

						// Cart page elements
						$( '.shop_table.cart' ).load( this_page + ' .shop_table.cart:eq(0) > *', function() {

							$( 'div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)' ).addClass( 'buttons_added' ).append( '<input type="button" value="+" id="add1" class="plus" />' ).prepend( '<input type="button" value="-" id="minus1" class="minus" />' );

							$( '.shop_table.cart' ).stop( true ).css( 'opacity', '1' ).unblock();

							$( 'body' ).trigger( 'cart_page_refreshed' );
						});

						$( '.cart_totals' ).load( this_page + ' .cart_totals:eq(0) > *', function() {
							$( '.cart_totals' ).stop( true ).css( 'opacity', '1' ).unblock();
						});

						// Trigger event so themes can refresh other areas
						$( 'body' ).trigger( 'added_to_cart', [ fragments, cart_hash ] );
						$( 'body' ).trigger( 'wc_fragment_refresh', [ fragments, cart_hash ] );
					})
					
					
					
				}

				return true;
			});
		}, 1500 );
	});
}
qof_simple();
