jQuery( document ).ready( function($) { 

	/* State/Country select boxes */
	var states_json = ign_wsp_country_select_params.countries.replace( /&quot;/g, '"' ),
		states = $.parseJSON( states_json );

	$( document.body ).on( 'change', 'select.billing_country, input.billing_country', function() {
		// Grab wrapping element to target only stateboxes in same 'group'
		var $wrapper    = $( this ).closest('#ign_wsp_form');

		var country     = $( this ).val(),
			$statebox   = $wrapper.find( '.billing_state' ),
			$parent     = $statebox.parent(),
			input_name  = $statebox.attr( 'name' ),
			input_id    = $statebox.attr( 'id' ),
			value       = $statebox.val(),
			placeholder = $statebox.attr( 'placeholder' ) || $statebox.attr( 'data-placeholder' ) || '';

		if ( states[ country ] ) {

			if ( $.isEmptyObject( states[ country ] ) ) {
				$statebox.parent().hide().find( '.select2-container' ).remove();
				$statebox.replaceWith( '<input type="hidden" class="hidden '  + input_id + '" name="' + input_name + '" id="' + input_id + '" value="" placeholder="' + placeholder + '" />' );

				$( document.body ).trigger( 'country_to_state_changed', [ country, $wrapper ] );

			} else {

				var options = '',
					state = states[ country ];

				for( var index in state ) {

					if ( state.hasOwnProperty( index ) ) {
						options = options + '<option value="' + index + '">' + state[ index ] + '</option>';
					}
				}

				$statebox.parent().show();

				if ( $statebox.is( 'input' ) ) {
					// Change for select
					$statebox.replaceWith( '<select name="' + input_name + '" id="' + input_id + '" class="state_select ' + input_id + '" data-placeholder="' + placeholder + '"></select>' );
					$statebox = $wrapper.find( '.billing_state' );
				}

				$statebox.html( '<option value="">' + ign_wsp_country_select_params.i18n_select_state_text + '</option>' + options );
				$statebox.val( value ).change();
				$wrapper.find( 'p.billing_state_row' ).show();
				$( document.body ).trigger( 'country_to_state_changed', [country, $wrapper ] );
				
				$statebox.attr( 'data-required', 'required' );

			}
		} else {

			if ( $statebox.is( 'select' ) ) {	
				
				$parent.show().find( '.select2-container' ).remove();
				$statebox.replaceWith( '<input type="text" class="input-text ' + input_id + '" name="' + input_name + '" id="' + input_id + '" placeholder="' + placeholder + '" />' );
				
				$wrapper.find( 'p.billing_state_row' ).hide();

				$( document.body ).trigger( 'country_to_state_changed', [country, $wrapper ] );

			} else if ( $statebox.is( 'input[type="hidden"]' ) ) {
				$parent.show().find( '.select2-container' ).remove();
				$statebox.replaceWith( '<input type="text" class="input-text ' + input_id + '" name="' + input_name + '" id="' + input_id + '" placeholder="' + placeholder + '" />' );

				$( document.body ).trigger( 'country_to_state_changed', [country, $wrapper ] );
			} else { 
				$wrapper.find( '.billing_state_row' ).hide();
			}
		}

		$( document.body ).trigger( 'country_to_state_changing', [country, $wrapper ] );
	});

	$(function() {
		$( '.billing_country' ).change();
	});
});