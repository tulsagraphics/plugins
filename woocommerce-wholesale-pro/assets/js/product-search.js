/*
 * Copyright (c) 2016 - IgniteWoo.com - ALL RIGHTS RESERVED
 * Portions are Copyright (c) 2012 - WooThemes- ALL RIGHTS RESERVED
 */
jQuery( document ).ready( function( $ ) { 

	// WC 2.7 and newer uses Select2 v4.x, earlier WC used Select2 v3.x
	if ( 'true' == ign_search_products.wc_version_gte_27 ) { 
			
		// Ajax product search box
		$( ':input.wsp-product-search' ).filter( ':not(.enhanced)' ).each( function() {
			var select2_args = {
				allowClear:  $( this ).data( 'allow_clear' ) ? true : false,
				placeholder: $( this ).data( 'placeholder' ),
				minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '3',
				escapeMarkup: function( m ) {
					return m;
				},
				ajax: {
					url:         wc_enhanced_select_params.ajax_url,
					dataType:    'json',
					quietMillis: 250,
					delay: 250,
					data: function( params ) {
						return {
							term:     params.term,
							action:   $( this ).data( 'action' ) || 'ign_search_products',
							security: wc_enhanced_select_params.search_products_nonce,
							exclude:  $( this ).data( 'exclude' ),
							include:  $( this ).data( 'include' ),
							limit:    $( this ).data( 'limit' )
						};
					},
					processResults: function( data ) {
						var terms = [];
						if ( data ) {
							$.each( data, function( id, text ) {
								terms.push( { id: id, text: text } );
							});
						}
						return {
							results: terms
						};
					},
					cache: true
				}
			};

			$( this ).select2( select2_args ).addClass( 'enhanced' );
		});
		
	} else {

		$( ':input.wsp-product-search' ).filter( ':not(.enhanced)' ).each( function() {
			var select2_args = {
				allowClear:  $( this ).data( 'allow_clear' ) ? true : false,
				placeholder: $( this ).data( 'placeholder' ),
				minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '3',
				escapeMarkup: function( m ) {
					return m;
				},
				ajax: {
				url:         wc_enhanced_select_params.ajax_url,
				dataType:    'json',
				quietMillis: 250,
				data: function( term ) {
					return {
							term:     term,
							action:   $( this ).data( 'action' ) || 'ign_search_products',
							security: wc_enhanced_select_params.search_products_nonce,
							exclude:  $( this ).data( 'exclude' ),
							include:  $( this ).data( 'include' ),
							limit:    $( this ).data( 'limit' )
					};
				},
				results: function( data ) {
					var terms = [];
					if ( data ) {
							$.each( data, function( id, text ) {
								terms.push( { id: id, text: text } );
							});
						}
					return {
					results: terms
				};
				},
				cache: true
				}
			};

			select2_args.multiple = true;
			select2_args.initSelection = function( element, callback ) {
				var data     = $.parseJSON( element.attr( 'data-selected' ) );
				var selected = [];

				$( element.val().split( ',' ) ).each( function( i, val ) {
					selected.push({
						id: val,
						text: data[ val ]
					});
				});
				return callback( selected );
			};
			select2_args.formatSelection = function( data ) {
				return '<div class="selected-option" data-id="' + data.id + '">' + data.text + '</div>';
			};

			$( this ).select2( select2_args ).addClass( 'enhanced' );
		})
	}
})
