/*
 * Copyright (c) 2016 - IgniteWoo.com - ALL RIGHTS RESERVED
 */
jQuery( function( $ ) {

	xhr = null;
	
	// Ajax search
	$( document ).on( 'click', '.quick_order_product_search', function(e) {
		e.preventDefault();
		
		if (xhr) 
			xhr.abort();
		
		var data = $( this ).closest( 'form' ).serialize();
		
		var all = $( this ).data( 'all' );
		
		data += '&mode=' + all;

		$( 'div.woocommerce' ).block({
			message: null,
			overlayCSS: {
				background: '#fff',
				opacity: 0.6
			}
		});

		xhr = $.ajax({
			type: "post",
			url: ign_quick_search_params.ajaxurl,
			data: {
				action: "quick_order_product_search",
				data: data
			},
			success: function(response) {
				xhr = null;

				if (!response) {
					$( 'div.woocommerce' ).unblock();
					return;
				}
				
				$( '.ignitewoo_quick_order_form_wrap' ).html( response );
				// rehook the JS actions
				qof_variations();
				qof_simple();
				$( 'div.woocommerce' ).unblock();
				qof_page_numbers();
			},
			error: function( xhr, status, message ) { 
				console.log( status );
				console.log( message );
			}
		});
	
		return false;
	});
	
	function qof_page_numbers() { 
		
		$( '.page-numbers' ).unbind( 'click' ).on( 'click', function(e) { 
			e.preventDefault();
			
			var href = $( this ).attr( 'href' );
			href = href.split( '?' )
			if ( null !== href[1] )
				var data = href[1];
			else 
				return;
			
			if (xhr) 
				xhr.abort();
			
			$( 'div.woocommerce' ).block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});

			xhr = $.ajax({
				type: "post",
				url: ign_quick_search_params.ajaxurl,
				data: {
					action: "quick_order_product_search",
					data: data
				},
				success: function(response) {
					xhr = null;

					if (!response) {
						$( 'div.woocommerce' ).unblock();
						return;
					}
					
					$( '.ignitewoo_quick_order_form_wrap' ).html( response );
					// rehook the JS actions
					qof_variations();
					qof_simple();
					$( 'div.woocommerce' ).unblock();
					qof_page_numbers();
				},
				error: function( xhr, status, message ) { 
					console.log( status );
					console.log( message );
				}
			});
		
			return false;
		})
	}
	
	function qof_parse_query(qstr) {
		var query = {};
		var a = qstr.substr(1).split('&');
		for (var i = 0; i < a.length; i++) {
			var b = a[i].split('=');
			query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
		}
		return query;
	}
});