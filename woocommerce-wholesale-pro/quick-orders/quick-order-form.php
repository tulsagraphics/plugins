<?php
/*
 * Copyright (c) 2016 - IgniteWoo.com - ALL RIGHTS RESERVED
 */
 
//  Main template file that loads dependant templates. 
//  WARNING: Do not alter CSS class and ID names, do not remove any DIVs. 

global $products, $post, $max_number_pages, $quick_order_settings;

remove_all_actions( 'woocommerce_before_add_to_cart_form' );
remove_all_actions( 'woocommerce_after_add_to_cart_form' );
remove_all_actions( 'woocommerce_before_add_to_cart_button' );
remove_all_actions( 'woocommerce_after_add_to_cart_button' );

?>

<style>
.added:before {
	content: '' !important;
}
.woocommerce a.added_to_cart, .woocommerce-page a.added_to_cart {
	display: none !important;
}

.shop_table td, .shop_table th {
	vertical-align: middle;
	border: none;
}
.shop_table .action {
	min-width: 40%;
}
.shop_table .product_title {
	min-width: 40%;
	vertical-align: top;
}
.shop_table .product_sku {
	padding-right: 15px;
	vertical-align: middle;
}
.shop_table .quick_order_price {
	width: 5%;
}
.shop_table .product_info img {
	vertical-align: middle;
	display: inline-block;
}
.shop_table .product_info .the_title {
	display: inline-block;
	margin-left: 10px;
	vertical-align: middle;
}
.shop_table .quick_order_add_to_cart, .shop_table .quantity {
	display: inline-block;
	min-width: 50px;
}
.shop_table .add_to_cart_button {
	vertical-align: top;
}
.shop_table .quick_order_mini_form {
	text-align: right;
}
.shop_table .quick_order_mini_form .variations_form, .shop_table .quick_order_mini_form .variations_form .variations {
	border: none;
}
.shop_table .quick_order_mini_form .variations_form .variations_button .single_add_to_cart_button  {
	float: right;
	margin-left: 5px;
}

.shop_table .quick_order_mini_form .variations_form .variations {
	margin: 0;
	padding: 0;
}
.shop_table .quick_order_mini_form .variations_form .variations td {
	border: none !important;
	text-align: right;
	vertical-align: middle;
	padding: 10px;
}
.shop_table .quick_order_mini_form .variations_form .variations td.label label {
	vertical-align: middle;
	float:right;
}
.shop_table .quick_order_mini_form .variations_form .variations select {
	font-size: 1.1em;
	width: 100%;
	padding: 5px 0;
	vertical-align: middle;
}
.shop_table .quick_order_mini_form .variations_form .variations td.label {
	min-width: 75px;
}
.shop_table .quick_order_mini_form .variations_form td {
	border: none;
}
.shop_table .quick_order_mini_form .variations_form .stock {
	display: inline-block;
	padding-left: 15px;
}
.shop_table .quick_order_mini_form .variations_form .single_add_to_cart_button {
	vertical-align: top;
}
.shop_table .quick_order_mini_form .variations_form a#variations_clear, a.reset_variations { 
	position: relative;
	top: 5px;
}

.shop_table .attachment-shop_thumbnail {
	max-width: 40px !important;
}
.woocommerce .woocommerce-pagination ul li {
	display: inline-block;
	text-decoration: none;
}
.woocommerce .woocommerce-pagination .page-numbers { 
	text-align:center;
}
.woocommerce .woocommerce-pagination ul.page-numbers a, .page-numbers li > span { 
	border: 1px solid;
}
.woocommerce .quick_order_search { 
	position: relative;
	left: 0;
	display: inline-block;
	width: 100%;
	margin-bottom: 20px;
}
.woocommerce .quick_order_search #cat { 
	min-width: 180px;
	height:36px;
	vertical-align: middle;
}
.woocommerce .quick_order_search select { 
	display: inline-block;
	padding: 2px 0 2px 0;
	height:36px;
	vertical-align: middle;
	width: 30%
}
.woocommerce .quick_order_search input { 
	display: inline-block;
	padding: 5px;
	height:36px;
	vertical-align: middle;
	width: 42%;
}
.woocommerce .quick_order_search button { 
	display: inline-block;
	padding: 5px 10px;
	height:36px;
	border-radius: 4px;
	vertical-align: middle;
}
.woocommerce form a.button, .woocommerce form button.button,.woocommerce input.qty { 
	display: inline-block;
	border-radius: 4px;
	height:36px;
	vertical-align: inherit;
	padding: 5px;
	height:36px;
	min-width:50px;
}
.woocommerce .quantity input[type="number"] {
	-moz-appearance: initial;
}
.woocommerce form a.button, .woocommerce form button.button {
	display: inline-block;
	padding: 0.618em 1em;
	font-size: 100%;
	font-weight: 700;
}
</style>

<div class="woocommerce">
		
	<?php wc_print_notices(); ?>

	<?php 
		// Don't show search form if disabled. 
		if ( empty( $quick_order_settings['disable_search'] ) || 'no' == $quick_order_search['disable_search'] )
			include( dirname( __FILE__ ) . '/search-form.php' );
	?>
	
	<div class="ignitewoo_quick_order_form_wrap">
		<?php include( dirname( __FILE__ ) . '/product-table.php' ) ?>
	</div>
	
	
</div>
