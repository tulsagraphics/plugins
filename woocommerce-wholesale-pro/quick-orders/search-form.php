<?php 
/*
 * Copyright (c) 2016 - IgniteWoo.com - ALL RIGHTS RESERVED
 */

// Search from 
// WARNING: Do not alter CSS class and ID names. 
 
 
// Support for Restricted Category Access
global $restricted_cats, $qof_id;


?>
<form action="search" class="quick_order_search">
	<input type="hidden" name="quick_order_form_id" value="<?php echo $qof_id ?>">
	<input type="text" value="" name="quick_order_search" class="ign-quick-order-product-search" placeholder="<?php _e( 'Search...', 'ignitewoo_wholesale_pro' ) ?>">
	<?php 
	$args = array(
		'show_option_all'    => '',
		'show_option_none'   => __( 'All categories', 'ignitewoo_wholesale_pro' ),
		'option_none_value'  => '0',
		'orderby'            => 'ID', 
		'order'              => 'ASC',
		'show_count'         => 0,
		'hide_empty'         => 0, 
		'child_of'           => 0,
		'exclude'            => '',
		'echo'               => 1,
		'selected'           => 0,
		'hierarchical'       => 0, 
		'name'               => 'cat',
		'id'                 => '',
		'class'              => 'postform',
		'depth'              => 0,
		'tab_index'          => 0,
		'taxonomy'           => 'product_cat',
		'hide_if_empty'      => false,
		'value_field'	     => 'term_id',	
	);

	if ( !empty( $restricted_cats ) )
		$args['include'] = $restricted_cats;
		
	wp_dropdown_categories( $args );
	?>
	<button class="quick_order_product_search" data-all=""><?php _e( 'Search', 'ignitewoo_wholesale_pro' ) ?></button>
	<button class="quick_order_product_search" data-all="all"><?php _e( 'Show all', 'ignitewoo_wholesale_pro' ) ?></button>
</form>