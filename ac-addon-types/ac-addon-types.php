<?php
/*
Plugin Name: 	Admin Columns Pro - Toolset Types
Version:        1.3.2
Description: 	Add Toolset Types columns to Admin Columns Pro
Author:         Admin Columns
Author URI: 	https://www.admincolumns.com
Plugin URI: 	https://www.admincolumns.com
Text Domain: 	codepress-admin-columns
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! is_admin() ) {
	return;
}

define( 'ACA_TYPES_FILE', __FILE__ );

require_once 'classes/Dependencies.php';

function aca_types_loader() {
	$dependencies = new ACA_Types_Dependencies( plugin_basename( ACA_TYPES_FILE ) );
	$dependencies->check_acp( '4.3' );

	if ( ! class_exists( 'Types_Main', false ) ) {
		$dependencies->add_missing_plugin( __( 'Toolset Types', 'wpcf' ), $dependencies->get_search_url( 'Toolset Types' ) );
	}

	if ( $dependencies->has_missing() ) {
		return;
	}

	require_once 'bootstrap.php';
}

add_action( 'after_setup_theme', 'aca_types_loader' );