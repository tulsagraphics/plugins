<?php

AC\Autoloader::instance()->register_prefix( 'ACA\Types', plugin_dir_path( ACA_TYPES_FILE ) . 'classes/' );

$addon = new ACA\Types\Types( ACA_TYPES_FILE );
$addon->register();