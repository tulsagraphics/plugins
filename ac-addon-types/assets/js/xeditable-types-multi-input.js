( function( $ ) {
	"use strict";

	var Types_Multi_Input = function( options ) {
		this.init( 'types_multi_input', options, Types_Multi_Input.defaults );
	};

	$.fn.editableutils.inherit( Types_Multi_Input, $.fn.editabletypes.abstractinput );

	$.extend( Types_Multi_Input.prototype, {

		render : function() {
			var $container = this.$input;
			var input_type = 'text';

			if ( this.options.hasOwnProperty( 'subtype' ) ) {
				input_type = this.options.subtype;
				$container.find( '.single-input input' ).attr( 'type', input_type );

				if ( 'textarea' == input_type ) {
					$container.find( '.single-input input' ).remove();
					$container.find( '.single-input' ).prepend( '<textarea class="form-control input-sm small-text" name="types_multi_text[]"></textarea>' )
				}
			}

			var $template = $( '<div></div>' ).append( $container.find( '.single-input:first' ).clone() );

			$container.on( 'click', '.add', function() {
				$( this ).closest( '.single-input' ).after( $template.html() );
			} );

			$container.on( 'click', '.remove', function() {
				$( this ).closest( '.single-input' ).remove();
			} );
		},

		value2input : function( value ) {
			var $container = this.$input;
			var $template = $( '<div></div>' ).append( $container.find( '.single-input' ).clone() );
			if ( !value ) {
				return;
			}

			$.each( value, function( i, val ) {
				if ( i > 0 ) {
					$container.append( $template.html() );
				}
				$container.find( '.single-input:eq( ' + i + ' ) [name="types_multi_text[]"]' ).val( val );

			} );
		},

		input2value : function() {
			return this.$input.find( "[name='types_multi_text\\[\\]']" ).map( function() {return $( this ).val();} ).get();
		}
	} );

	var template = '';

	template += '<div class="multi-input-container">';

	template += '<div class="single-input">';
	template += '<input type="text" class="form-control input-sm small-text" name="types_multi_text[]">';
	template += '<span class="buttons"><span class="add"><span class="glyphicon glyphicon-plus"></span></span><span class="remove"><span class="glyphicon glyphicon-minus"></span></span></span>';
	template += '</div>';

	template += '</div>';

	Types_Multi_Input.defaults = $.extend( {}, $.fn.editabletypes.abstractinput.defaults, {
		tpl : template,
		subtype : 'text'
	} );

	$.fn.editabletypes.types_multi_input = Types_Multi_Input;
}( window.jQuery ) );

jQuery.fn.cacie_edit_types_multi_input = function( column, item ) {

	var el = jQuery( this );

	el.cacie_xeditable( {
		type : 'types_multi_input',
		subtype : column.editable.subtype,
		value : el.cacie_get_value( column, item )
	}, column, item );
};