<?php

namespace ACA\Types;

use ACP;

/**
 * @property Column $column
 */
class Editing extends ACP\Editing\Model {

	public function __construct( Column $column ) {
		parent::__construct( $column );
	}

	public function get_view_settings() {
		$data = array(
			'type' => 'text',
		);

		if ( $placeholder = $this->column->get_field()->get( 'placeholder' ) ) {
			$data['placeholder'] = $placeholder;
		}
		
		if ( ! $this->column->get_field()->is_required() ) {
			$data['clear_button'] = true;
		}

		return $data;
	}

	/**
	 * @param int $id
	 *
	 * @return array|mixed|string
	 */
	public function get_edit_value( $id ) {
		$value = $this->column->get_field()->get_raw_value( $id );

		if ( empty( $value ) ) {
			return false;
		}

		return $value;
	}

	/**
	 * @param int          $id
	 * @param string|array $value
	 */
	public function save( $id, $value ) {
		$this->update_metadata( $id, $value );
	}

	/**
	 * @param $id
	 * @param $value
	 */
	public function save_multi_input( $id, $value ) {
		$this->delete_metadata( $id );

		foreach ( $value as $single_value ) {
			$this->add_metadata( $id, $single_value );
		}
	}

	/**
	 * @param int $id
	 */
	public function delete_metadata( $id ) {
		delete_metadata( $this->column->get_meta_type(), $id, $this->column->get_meta_key(), null );
	}

	/**
	 * @param int   $id
	 * @param mixed $value
	 */
	public function add_metadata( $id, $value ) {
		add_metadata( $this->column->get_meta_type(), $id, $this->column->get_meta_key(), $value );
	}

	/**
	 * @param int   $id
	 * @param mixed $value
	 */
	public function update_metadata( $id, $value ) {
		update_metadata( $this->column->get_meta_type(), $id, $this->column->get_meta_key(), $value );
	}

}