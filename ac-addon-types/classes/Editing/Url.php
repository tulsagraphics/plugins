<?php

namespace ACA\Types\Editing;

use ACA\Types\Editing;

class Url extends Editing {

	public function get_view_settings() {
		$data = parent::get_view_settings();

		$validation = $this->column->get_field()->get( 'validate' );

		if ( $validation && isset( $validation['url']['active'] ) && 1 == $validation['url']['active'] ) {
			$data['type'] = 'url';
		}

		return $data;
	}

}