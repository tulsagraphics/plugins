<?php

namespace ACA\Types\Editing;

use ACA\Types\Editing;

class Email extends Editing {

	public function get_view_settings() {
		$data = parent::get_view_settings();

		$data['type'] = 'email';

		return $data;
	}

}