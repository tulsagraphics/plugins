<?php

namespace ACA\Types\Editing;

use ACA\Types\Editing;

class Repeatable extends Editing {

	public function get_view_settings() {
		$data = parent::get_view_settings();

		$data['type'] = 'types_multi_input';
		$data['subtype'] = 'text';
		$data['store_values'] = true;

		return $data;
	}

	public function save( $id, $value ) {
		$this->save_multi_input( $id, $value );
	}

}