<?php

namespace ACA\Types\Editing\Post;

use ACA\Types\Column;
use ACP;

/**
 * @property Column\Post\ParentPost $column
 */
class ParentPost extends ACP\Editing\Model\Meta {

	public function __construct( Column\Post\ParentPost $column ) {
		parent::__construct( $column );
	}

	public function get_view_settings() {
		return array(
			'type'               => 'select2_dropdown',
			'ajax_populate'      => true,
			'multiple'           => false,
			'clear_button'       => true,
			'store_single_value' => true,
		);
	}

	public function get_ajax_options( $request ) {
		return acp_editing_helper()->get_posts_list( array(
			's'         => $request['search'],
			'post_type' => $this->column->get_available_parent_post_types(),
			'paged'     => $request['paged'],
		) );
	}

	public function get_edit_value( $id ) {
		$edit_value = parent::get_edit_value( $id );
		if ( ! $edit_value ) {
			return false;
		}

		if ( ! $post = get_post( parent::get_edit_value( $id ) ) ) {
			return false;
		}

		return array(
			$post->ID => $post->post_title,
		);
	}

	public function saves( $id, $value ) {
		parent::save( $id, $value );
		wp_update_post( array( 'ID' => $id ) );
	}
}