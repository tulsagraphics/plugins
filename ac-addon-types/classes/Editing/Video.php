<?php

namespace ACA\Types\Editing;

class Video extends File {

	public function get_view_settings() {
		$data = parent::get_view_settings();
		$data['attachment']['library']['type'] = 'video';

		return $data;
	}

}