<?php

namespace ACA\Types\Editing;

use ACA\Types\Editing;

class Checkbox extends Editing {

	public function get_view_settings() {
		return array(
			'type'    => 'togglable',
			'options' => array( '', $this->column->get_field()->get( 'set_value' ) ),
		);
	}

}