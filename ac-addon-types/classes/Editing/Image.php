<?php

namespace ACA\Types\Editing;

class Image extends File {

	public function get_view_settings() {
		$data = parent::get_view_settings();
		$data['type'] = 'media';
		$data['attachment']['library']['type'] = 'image';

		return $data;
	}

}