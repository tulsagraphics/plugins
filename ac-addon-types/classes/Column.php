<?php

namespace ACA\Types;

use ACP;
use AC;
use AC\Collection;

abstract class Column extends AC\Column\Meta
	implements ACP\Editing\Editable, ACP\Sorting\Sortable, ACP\Filtering\Filterable, ACP\Export\Exportable {

	/**
	 * @return array
	 */
	abstract public function get_fields();

	/**
	 * @param int $id
	 *
	 * @return string
	 */
	abstract public function get_render_value( $id );

	public function __construct() {
		$this->set_type( 'column-types' );
		$this->set_label( 'Toolset Types' );
		$this->set_group( 'types' );
	}

	// Meta

	public function get_meta_key() {
		return $this->get_type_field_option( 'meta_key' );
	}

	public function get_value( $id ) {
		$value = $this->get_field()->get_value( $id );

		if ( $value instanceof Collection ) {
			$value = $value->filter()->implode( $this->get_separator() );
		}

		return $value;
	}

	protected function get_type_name() {
		return 'wpcf-fields';
	}

	/**
	 * @param string $property
	 *
	 * @return array|string|false
	 */
	public function get_type_field_option( $property ) {
		$field = $this->get_type_field();

		return $field && isset( $field[ $property ] ) ? $field[ $property ] : false;
	}

	public function is_repeatable() {
		$data = $this->get_type_field_option( 'data' );

		return isset( $data['repetitive'] ) && '1' === $data['repetitive'];
	}

	// Pro

	public function editing() {
		return $this->get_field()->editing();
	}

	public function filtering() {
		return $this->get_field()->filtering();
	}

	public function sorting() {
		return $this->get_field()->sorting();
	}

	public function export() {
		return $this->get_field()->export();
	}

	public function is_serialized() {
		return $this->get_field()->is_serialized();
	}

	/**
	 * Register settings
	 */
	protected function register_settings() {
		$this->add_setting( new Settings\Field( $this ) );
	}

	public function get_raw_value( $id ) {
		return $this->get_field()->get_raw_value( $id );
	}

	/**
	 * @return false|Field
	 */
	public function get_field() {
		// Convert field type to field class name
		$type = implode( array_map( 'ucfirst', explode( '_', str_replace( '-', '_', $this->get_type_field_option( 'type' ) ) ) ) );

		// Repeatable fields
		if ( $this->is_repeatable() ) {
			$class = 'ACA\Types\Field\Repeatable\\' . $type;

			if ( class_exists( $class ) ) {
				return new $class( $this );
			}
		}

		// Single fields
		$class = 'ACA\Types\Field\\' . $type;
		if ( class_exists( $class ) ) {
			return new $class( $this );
		}

		return new Field( $this );
	}

	public function get_type_field() {
		$field = wpcf_admin_fields_get_field( $this->get_type_field_id(), null, null, null, $this->get_type_name() );

		if ( ! $field ) {
			return false;
		}

		return $field;
	}

	public function get_type_field_id() {
		return (string) $this->get_setting( 'types_field' )->get_value();
	}

}