<?php

namespace ACA\Types\Column\Post;

use ACA\Types\Settings;
use AC;

class Relationship extends AC\Column {

	public function __construct() {
		$this->set_type( 'column-types_relationship' );
		$this->set_label( 'Toolset Types - Relationship' );
		$this->set_group( 'types' );
	}

	public function get_raw_value( $id ) {
		return new AC\Collection( toolset_get_related_posts( $id, $this->get_relationship_setting()->get_relationship(), $this->get_relationship_type() ) );
	}

	public function is_valid() {
		return apply_filters( 'toolset_is_m2m_enabled', false );
	}

	public function register_settings() {
		$this->add_setting( new \ACA\Types\Settings\Relationship( $this ) );
		//$this->add_setting( new AC\Settings\Column\Post( $this ) );
	}

	private function get_relationship_type() {
		$relationship = $this->get_relationship_setting()->get_relationship_object();
		$parent_type = $relationship->get_parent_type();

		if ( ! in_array( $this->get_post_type(), $parent_type->get_types() ) ) {
			return 'child';
		}

		return 'parent';

	}

	/**
	 * @return false|Settings\Relationship
	 */
	public function get_relationship_setting() {
		$setting = $this->get_setting( 'relationship' );

		if ( ! $setting instanceof Settings\Relationship ) {
			return false;
		}

		return $setting;
	}
}