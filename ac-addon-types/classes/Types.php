<?php

namespace ACA\Types;

use ACP;
use AC;

final class Types extends AC\Plugin {

	/**
	 * @var string
	 */
	protected $file;

	public function __construct( $file ) {
		$this->file = $file;
	}

	public function register() {
		if ( ! $this->load_types_api() ) {
			return;
		}

		add_action( 'ac/column_groups', array( $this, 'register_column_groups' ) );
		add_action( 'acp/column_types', array( $this, 'register_columns' ) );
		add_action( 'ac/table_scripts/editing', array( $this, 'table_scripts_editing' ) );
	}

	protected function get_file() {
		return $this->file;
	}

	protected function get_version_key() {
		return 'aca_types';
	}

	/**
	 * @param AC\Groups $groups
	 */
	public function register_column_groups( $groups ) {
		$groups->register_group( 'types', 'Toolset Types', 11 );
	}

	/**
	 * Load Types API functions
	 *
	 * @return bool
	 */
	private function load_types_api() {
		if ( ! defined( 'WPCF_EMBEDDED_TOOLSET_ABSPATH' ) ) {
			return false;
		}

		$calls = array(
			WPCF_EMBEDDED_TOOLSET_ABSPATH . '/types/embedded/frontend.php'        => array(
				'types_render_termmeta',
				'types_render_field',
				'types_render_usermeta',
			),
			WPCF_EMBEDDED_TOOLSET_ABSPATH . '/types/embedded/includes/fields.php' => array(
				'wpcf_admin_fields_get_fields_by_group',
				'wpcf_admin_fields_get_field',
				'wpcf_admin_get_groups_by_post_type',
			),
		);

		foreach ( $calls as $file => $functions ) {
			if ( ! is_readable( $file ) ) {
				return false;
			}

			require_once $file;

			foreach ( $functions as $function ) {
				if ( ! function_exists( $function ) ) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Add custom columns
	 *
	 * @param AC\ListScreen $list_screen
	 */
	public function register_columns( AC\ListScreen $list_screen ) {

		switch ( true ) {

			// Post and Media
			case $list_screen instanceof AC\ListScreenPost :
				$list_screen->register_column_type( new Column\Post );
				$list_screen->register_column_types_from_dir( 'ACA\Types\Column\Post' );

				break;
			case $list_screen instanceof AC\ListScreen\User :
				$list_screen->register_column_type( new Column\User );

				break;
			case $list_screen instanceof ACP\ListScreen\Taxonomy :
				$list_screen->register_column_type( new Column\Taxonomy );

				break;
		}
	}

	public function table_scripts_editing() {
		wp_enqueue_script( 'ac-xeditable-input-types-link', $this->get_url() . 'assets/js/xeditable-types-multi-input.js', array( 'jquery' ), $this->get_version() );
		wp_enqueue_style( 'aca-types-editing', $this->get_url() . 'assets/css/editing.css', array(), $this->get_version() );
	}

}