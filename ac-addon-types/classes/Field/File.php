<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACA\Types\Sorting;

class File extends Field {

	// Display

	public function get_value( $id ) {
		$value = $this->get_raw_value( $id );

		if ( ! $value ) {
			return false;
		}

		$label = $value;

		if ( $upload_dir = wp_upload_dir() ) {
			$label = str_replace( $upload_dir['baseurl'], '', $value );
		}

		return ac_helper()->html->link( $value, $label );
	}

	// Pro

	public function sorting() {
		return new Sorting( $this->column );
	}

	public function filtering() {
		return new Filtering\File( $this->column );
	}

	public function editing() {
		return new Editing\File( $this->column );
	}

	/**
	 * @param string $image_url
	 *
	 * @return int|null
	 */
	public function get_attachment_id_by_url( $image_url ) {
		if ( ! $image_url ) {
			return false;
		}

		$upload_dir = wp_get_upload_dir();

		$image = get_posts( array(
			'post_type'      => 'attachment',
			'fields'         => 'ids',
			'meta_query'     => array(
				array(
					'key'   => '_wp_attached_file',
					'value' => ltrim( str_replace( $upload_dir['baseurl'], '', $image_url ), '/' ),
				),
			),
			'posts_per_page' => 1,
		) );

		if ( ! $image ) {
			return false;
		}

		return $image[0];
	}

}