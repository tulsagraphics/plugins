<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACA\Types\Sorting;
use AC;

class Textfield extends Field {

	// Display

	public function get_value( $id ) {
		return $this->column->get_formatted_value( $this->get_raw_value( $id ) );
	}

	// Pro

	public function editing() {
		return new Editing( $this->column );
	}

	public function sorting() {
		return new Sorting( $this->column );
	}

	public function filtering() {
		return new Filtering( $this->column );
	}

	// Settings

	public function get_dependent_settings() {
		$this->column->add_setting(
			new AC\Settings\Column\CharacterLimit( $this->column )
		);
	}

}