<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACA\Types\Sorting;

class Email extends Field {

	public function editing() {
		return new Editing\Email( $this->column );
	}

	public function sorting() {
		return new Sorting( $this->column );
	}

	public function filtering() {
		return new Filtering( $this->column );
	}

}