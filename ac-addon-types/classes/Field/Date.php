<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;
use ACA\Types\Export;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACA\Types\Sorting;
use AC;

class Date extends Field {

	// Display

	public function get_value( $id ) {
		$time = $this->get_raw_value( $id );

		if ( ! $time ) {
			return false;
		};

		return $this->column->get_formatted_value( date( 'c', $time ) );
	}

	// Pro

	public function editing() {
		return new Editing\Date( $this->column );
	}

	public function filtering() {
		return new Filtering\Date( $this->column );
	}

	public function sorting() {
		return new Sorting( $this->column );
	}

	public function export() {
		return new Export\Field\Date( $this->column );
	}

	// Settings

	public function get_dependent_settings() {
		return array(
			new AC\Settings\Column\Date( $this->column ),
		);
	}

}