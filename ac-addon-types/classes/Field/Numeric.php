<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACA\Types\Sorting;

class Numeric extends Field {

	public function sorting() {
		$model = new Sorting( $this->column );
		$model->set_data_type( 'numeric' );

		return $model;
	}

	public function editing() {
		return new Editing\Numeric( $this->column );
	}

	public function filtering() {
		return new Filtering\Numeric( $this->column );
	}

}