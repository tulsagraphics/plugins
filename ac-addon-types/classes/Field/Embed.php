<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACA\Types\Sorting;

class Embed extends Field {

	// Display

	public function get_value( $id ) {
		return $this->get_raw_value( $id );
	}

	// Pro

	public function editing() {
		return new Editing\Embed( $this->column );
	}

	public function sorting() {
		return new Sorting( $this->column );
	}

	public function filtering() {
		return new Filtering( $this->column );
	}

}