<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;
use ACA\Types\Filtering;
use ACA\Types\Sorting;

class Video extends File {

	public function editing() {
		return new Editing\Video( $this->column );
	}

	public function sorting() {
		return new Sorting( $this->column );
	}

	public function filtering() {
		return new Filtering( $this->column );
	}

}