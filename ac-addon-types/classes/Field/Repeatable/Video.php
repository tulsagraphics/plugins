<?php

namespace ACA\Types\Field\Repeatable;

use ACA\Types\Editing;
use ACA\Types\Field;

class Video extends Field\File {

	public function editing() {
		return new Editing\Repeatable\Video( $this->column );
	}

}