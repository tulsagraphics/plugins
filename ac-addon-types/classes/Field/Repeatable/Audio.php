<?php

namespace ACA\Types\Field\Repeatable;

use ACA\Types\Editing;

class Audio extends File {

	public function editing() {
		return new Editing\Repeatable\Audio( $this->column );
	}

}