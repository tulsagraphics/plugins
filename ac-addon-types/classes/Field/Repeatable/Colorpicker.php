<?php

namespace ACA\Types\Field\Repeatable;

use ACA\Types\Field;
use ACA\Types\Editing;

class Colorpicker extends Field\Colorpicker {

	public function get_value( $id ) {
		$values = array();

		foreach ( (array) $this->get_raw_value( $id ) as $color ) {
			$values[] = ac_helper()->string->get_color_block( $color );
		}

		return implode( $values );
	}

	public function editing() {
		return new Editing\Repeatable\Color( $this->column );
	}

}