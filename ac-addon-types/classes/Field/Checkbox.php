<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACA\Types\Sorting;

class Checkbox extends Field {

	public function get_value( $id ) {
		$value = parent::get_value( $id );

		return ac_helper()->icon->yes_or_no( strlen( $value ) > 0 ) . ' ' . $value;
	}

	public function editing() {
		return new Editing\Checkbox( $this->column );
	}

	public function filtering() {
		return new Filtering\Checkbox( $this->column );
	}

	public function sorting() {
		return new Sorting( $this->column );
	}

}
