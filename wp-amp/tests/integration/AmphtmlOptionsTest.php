<?php

class AmphtmlOptionsTest extends \Codeception\TestCase\WPTestCase {

	public function setUp() {
		// before
		//        parent::setUp();
		deactivate_plugins( 'woocommerce/woocommerce.php' );
		// your set up methods here
	}

	public function tearDown() {
		// your tear down methods here

		// then
		parent::tearDown();
	}

	// tests
	public function testGetAmphtmlLink() {
		$post_id     = self::factory()->post->create( array (
			'post_type'   => 'page',
			'post_author' => 1

		) );
		$options     = new AMPHTML_Options();
		$main_url    = get_the_permalink( $post_id );
		$amphtml_url = user_trailingslashit( $main_url . AMPHTML()->get_endpoint() );
		$this->assertEquals( $amphtml_url, $options->get_amphtml_link( $main_url ) );

		$permalink_structure = get_option( 'permalink_structure' );
		update_option( 'permalink_structure', '' );
		$amphtml_url = add_query_arg( AMPHTML()->get_endpoint(), '1', $main_url );
		$this->assertEquals( $amphtml_url, $options->get_amphtml_link( $main_url ) );
		update_option( 'permalink_structure', $permalink_structure );

		update_post_meta( $post_id, 'amphtml-exclude', 'true' );
		$amphtml_url = $main_url;
		$this->assertEquals( $amphtml_url, $options->get_amphtml_link( $main_url ) );

		wp_delete_post( $post_id, true );
	}

}