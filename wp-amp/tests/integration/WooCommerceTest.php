<?php

class WooCommerceTest extends \Codeception\TestCase\WPTestCase {

	/**
	 * @var AMPHTML_Template
	 */
	public $template;

	public function setUp() {
		$plugin = ABSPATH . 'wp-content/plugins/woocommerce/woocommerce.php';
		include_once( $plugin );
		$this->run_activate_plugin( 'woocommerce/woocommerce.php' );
		$options        = new AMPHTML_Options();
		$this->template = new AMPHTML_Template( $options );
	}

	protected static function run_activate_plugin( $plugin ) {
		$current = get_option( 'active_plugins' );
		$plugin  = plugin_basename( trim( $plugin ) );
		add_filter( 'dbdelta_queries', '__return_empty_array' );
		if ( ! in_array( $plugin, $current ) ) {
			$current[] = $plugin;
			sort( $current );
			do_action( 'activate_' . trim( $plugin ) );
		}
		remove_filter( 'dbdelta_queries', '__return_empty_array' );

		return null;
	}


	public function tearDown() {
		// your tear down methods here
		deactivate_plugins( 'woocommerce/woocommerce.php' );
		// then
		parent::tearDown();
	}

	//    public function testGetProductImageLinks() {
	//        $prod_id = 99;
	//        $_pf = new WC_Product_Factory();
	//        $_product = $_pf->get_product($prod_id);
	//        $links = $this->template->get_product_image_links( $_product );
	//    }

}
