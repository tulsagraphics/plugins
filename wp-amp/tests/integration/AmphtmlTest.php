<?php

class AmphtmlTest extends \Codeception\TestCase\WPTestCase {


	public function setUp() {
		// before
		//		parent::setUp();

		// your set up methods here
		deactivate_plugins( 'woocommerce/woocommerce.php' );
	}

	public function tearDown() {
		// your tear down methods here
		update_option( 'amphtml_endpoint', 'amp' );
		// then
		parent::tearDown();
	}

	// tests
	public function testGetEndpoint() {

		$endpoint = 'test_amp';

		update_option( 'amphtml_endpoint', $endpoint );

		$this->assertEquals( $endpoint, AMPHTML()->get_endpoint() );

		update_option( 'amphtml_endpoint', '' );

		$amphtml = AMPHTML();

		$this->assertEquals( $amphtml::AMP_QUERY, AMPHTML()->get_endpoint() );
	}

	public function testIsAmp() {
		$urls = array ( //todo add all variation
			'/amp',
			'/author/themedemos/amp/',
			'/author/themedemos/amp/page/2/',
			'/news/our-company/amp/',
			'/category/uncategorized/amp/',
			'/category/uncategorized/amp/page/2/'

		);

		foreach ( $urls as $url ) {
			$_SERVER['REQUEST_URI'] = $url;
			$this->assertTrue( AMPHTML()->is_amp() );
		}
	}

	public function testExcluded() {
		global $wp_query;
		AMPHTML_Options::$fields['post_types']['value'] = array ( 'page' );

		$post_id = self::factory()->post->create( array (
			'post_type'   => 'page',
			'post_title'  => 'Unit Test Page2',
			'post_author' => 1
		) );

		update_post_meta( $post_id, 'amphtml-exclude', 'true' );
		$this->assertTrue( AMPHTML()->is_excluded( $post_id ) );

		update_post_meta( $post_id, 'amphtml-exclude', '' );
		set_post_type( $post_id, 'post' );
		$this->assertTrue( AMPHTML()->is_excluded( $post_id ) );

		wp_delete_post( $post_id, true );

		$post_id = '';
		$this->assertFalse( AMPHTML()->is_excluded( $post_id ) );

		AMPHTML_Options::$fields['archives']['value'] = array ( 'category' );

		$wp_query->is_archive = true;
		$wp_query->is_date    = true;

		$this->assertTrue( AMPHTML()->is_excluded( $post_id ) );

	}

	public function testIsAllowedPage() {
		global $wp_query;

		$allowed_pages                                = array ( 'date', 'author', 'category', 'tag', 'tax' );
		AMPHTML_Options::$fields['archives']['value'] = $allowed_pages;

		$wp_query->is_date = true;
		$this->assertTrue( AMPHTML()->is_allowed_page( $allowed_pages ) );

		$wp_query->is_date = false;
		$wp_query->is_tag  = true;
		$this->assertTrue( AMPHTML()->is_allowed_page( $allowed_pages ) );

		$wp_query->is_date     = false;
		$wp_query->is_tag      = false;
		$wp_query->is_category = true;
		$this->assertTrue( AMPHTML()->is_allowed_page( $allowed_pages ) );

		$wp_query->is_date     = false;
		$wp_query->is_tag      = false;
		$wp_query->is_category = false;
		$wp_query->is_author   = true;
		$this->assertTrue( AMPHTML()->is_allowed_page( $allowed_pages ) );

		$wp_query->is_date     = false;
		$wp_query->is_tag      = false;
		$wp_query->is_category = false;
		$wp_query->is_author   = false;
		$wp_query->is_tax      = true;
		$this->assertTrue( AMPHTML()->is_allowed_page( $allowed_pages ) );
	}

	public function testGetRedirectUrl() {
		global $wp;
		$_SERVER["REQUEST_URI"] = 'author/themedemos/amp';
		$query_obj_id           = '';
		$this->assertEquals( '', AMPHTML()->get_redirect_url( $wp, $query_obj_id ) );

		AMPHTML_Options::$fields['mobile_amp']['value'] = 1;
		$_SERVER["REQUEST_URI"]                         = 'author/themedemos';
		$_SERVER['HTTP_USER_AGENT']                     = 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.23 Mobile Safari/537.36';

		$url = home_url( $wp->request ) . '/' . AMPHTML()->get_endpoint() . '/';
		$this->assertEquals( $url, AMPHTML()->get_redirect_url( $wp, $query_obj_id ) );

		AMPHTML_Options::$fields['mobile_amp']['value'] = 0;
		$_SERVER['HTTP_USER_AGENT']                     = null;

	}

	public function testExcludedRedirectUrl() {
		global $wp;
		$post_id = self::factory()->post->create( array (
			'post_type'   => 'page',
			'post_title'  => 'Unit Test Page1',
			'post_author' => 1
		) );
		$url     = get_permalink( $post_id );
		$this->assertEquals( $url, AMPHTML()->get_excluded_redirect_url( $wp, $post_id ) );
		wp_delete_post( $post_id, true );

		$_SERVER["REQUEST_URI"] = 'author/themedemos/amp';
		$post_id                = '';

		$url = home_url( rtrim( $wp->request, AMPHTML()->get_endpoint() ) );
		$this->assertEquals( $url, AMPHTML()->get_excluded_redirect_url( $wp, $post_id ) );
	}

	public function testLoadAmphtml() {
		$mock = $this->getMockBuilder( 'AMPHTML' )->setMethods( array ( 'doA', 'doB', 'doC' ) )->getMock();
	}

	public function testGetQueriedObjecId() {
		global $wp_query, $wp;
		$wp->request = AMPHTML()->get_endpoint();
		$this->assertEquals( get_option( 'page_on_front' ), AMPHTML()->get_queried_object_id() );

		$wp_query->is_archive = true;
		$this->assertEquals( '', AMPHTML()->get_queried_object_id() );
	}

	public function testGetRelInfo() {
		global $wp, $wp_query;

		$post_id = self::factory()->post->create( array (
			'post_type'   => 'page',
			'post_name'   => 'test-page',
			'post_title'  => 'Unit Test Page',
			'post_author' => 1
		) );

		update_post_meta( $post_id, 'amphtml-exclude', 'true' );
		$wp_query->is_singular = true;
		$wp_query->post        = get_post( $post_id );

		$this->assertEquals( '', AMPHTML()->get_rel_info() );

		update_post_meta( $post_id, 'amphtml-exclude', '' );

		$wp->request = 'test-page';

		$perm_struct = get_option( 'permalink_structure' );

		update_option( 'permalink_structure', '/%postname%/' );

		$urls              = array (
			''                              => 'amp/',
			'author/themedemos'             => 'author/themedemos/amp/',
			'news/our-company'              => 'news/our-company/amp/',
			'page/3'                        => '/page/3/amp/',
			'author/themedemos/page/2'      => 'author/themedemos/page/2/amp/',
			'category/uncategorized'        => 'category/uncategorized/amp/',
			'category/uncategorized/page/2' => 'category/uncategorized/page/2/amp/'

		);
		$queried_object_id = AMPHTML()->get_queried_object_id();

		foreach ( $urls as $canonical => $amphtml ) {
			$wp->request = $canonical;
			preg_match( "/page\/(\d+)/", $canonical, $matches );
			if ( $matches ) {
				$wp_query->set( 'paged', $matches[1] );
			} else if ( isset( $wp_query->query_vars['paged'] ) ) {
				unset( $wp_query->query_vars['paged'] );
			}

			$url = home_url( $amphtml );
			$this->assertEquals( $url, AMPHTML()->get_rel_info() );
		}
		wp_delete_post( $post_id, true );
		update_option( 'permalink_structure', $perm_struct );

	}

	public function testGetSearchRedirectUrl() {
		$this->assertEquals( '/?s&amp=1', AMPHTML()->get_search_redirect_url() );
		set_query_var( 's', 'string' );
		$this->assertEquals( '/search/string/amp/', AMPHTML()->get_search_redirect_url() );
	}

	public function testIsPostsPage() {
		global $wp_query;
		$wp_query->is_home = true;
		update_option( 'show_on_front', 'page' );
		$this->assertEquals( true, AMPHTML()->is_posts_page() );
	}

	public function testIsHomePostsPage() {
		global $wp_query;
		update_option( 'show_on_front', 'posts' );
		$wp_query->is_home = true;
		$this->assertEquals( true, AMPHTML()->is_home_posts_page() );
	}

}