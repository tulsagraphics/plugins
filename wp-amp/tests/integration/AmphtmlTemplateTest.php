<?php

class AmphtmlTemplateTest extends \Codeception\TestCase\WPTestCase {

	/**
	 * @var AMPHTML_Template
	 */
	public $template;

	public function setUp() {
		// before
		//        parent::setUp();
		deactivate_plugins( 'woocommerce/woocommerce.php' );
		// your set up methods here
		$options        = new AMPHTML_Options();
		$this->template = new AMPHTML_Template( $options );


	}

	public function tearDown() {
		// your tear down methods here

		// then
		parent::tearDown();
	}

	// tests
	public function testGetTitle() {
		$options  = new AMPHTML_Options();
		$template = new AMPHTML_Template( $options );
		$title    = 'Simple Page';
		$post_id  = self::factory()->post->create( array (
			'post_type'   => 'page',
			'post_title'  => $title,
			'post_author' => 1
		) );
		$template->set_post( $post_id );
		$this->assertEquals( $title, $template->get_title( $post_id ) );
		wp_delete_post( $post_id, true );

	}

	public function testUpdateContentLinks() {
		$link    = array (
			'canonical' => "http://wp.dev/about-2/page-image-alignment/",
			'amphtml'   => "http://wp.dev/about-2/page-image-alignment/amp"
		);
		$content = '<div>
                        <ul class="sub-menu">
                            <li class="menu-item">
                                <a href="' . $link['canonical'] . '">Page Image Alignment</a>
                            </li>
                        </ul>
                    </div>';
		$content = $this->template->update_content_links( $content )->save();
		$this->assertNotFalse( strpos( $content, $link['amphtml'] ) );
	}

	public function testGetCanonicalUrl() {
		global $wp;
		$urls = array (
			'/'                              => 'amp',
			'author/themedemos/'             => 'author/themedemos/amp',
			'news/our-company/'              => 'news/our-company/amp',
			'page/3/'                        => 'amp/page/3/',
			'author/themedemos/page/2/'      => 'author/themedemos/amp/page/2/',
			'category/uncategorized/'        => 'category/uncategorized/amp',
			'category/uncategorized/page/2/' => 'category/uncategorized/amp/page/2/'

		);
		foreach ( $urls as $canonical => $amp ) {
			$wp->request = $amp;
			$this->assertEquals( home_url( $canonical ), $this->template->get_canonical_url() );
		}
	}

	public function testGetTemplatePath() {
		$file = get_stylesheet_directory() . DIRECTORY_SEPARATOR . AMPHTML()->get_plugin_folder_name() . DIRECTORY_SEPARATOR . 'searchfom.php';
		if ( ! file_exists( dirname( $file ) ) ) {
			mkdir( dirname( $file ), 0777, true );
		}
		$template = fopen( $file, 'w' );
		fwrite( $template, 'search-form' );
		fclose( $template );
		$template = $this->template->render( 'searchform' );
		unlink( $file );
	}

}