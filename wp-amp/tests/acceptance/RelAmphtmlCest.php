<?php


class RelAmphtmlCest {
	static $BASE_URL = 'http://wp.dev';

	static $AMP_ENDPOINT = 'amp';

	public function _before( AcceptanceTester $I ) {
		include_once '../../../wp-tests-config.php';
	}

	public function _after( AcceptanceTester $I ) {
	}

	/**
	 * @example { "url": "/", "desc": "home" }
	 * @example { "url": "/page/2/", "desc": "home+pagination" }
	 */
	public function testHomePage( AcceptanceTester $I, \Codeception\Example $example ) {
		update_option( 'show_on_front', 'posts' );

		$I->amOnPage( $example['url'] );
		$href_amphtml = self::$BASE_URL . $example['url'] . self::$AMP_ENDPOINT . '/';
		$I->seeElement( "//link[@rel='amphtml'][@href='$href_amphtml']" );
		$I->dontSee( '( ! )' );
	}

	/**
	 * @example { "url": "/blog/", "desc": "blog" }
	 * @example { "url": "/blog/page/2/", "desc": "blog+pagination" }
	 */
	public function testBlogPage( AcceptanceTester $I, \Codeception\Example $example ) {
		update_option( 'show_on_front', 'page' );
		update_option( 'page_on_front', 701 );
		update_option( 'page_for_posts', 703 );

		$I->amOnPage( $example['url'] );
		$href_amphtml = self::$BASE_URL . $example['url'] . self::$AMP_ENDPOINT . '/';
		$I->seeElement( "//link[@rel='amphtml'][@href='$href_amphtml']" );
		$I->dontSee( '( ! )' );
	}


	/**
	 * @example { "url": "/author/themedemos/", "desc": "author" }
	 * @example { "url": "/author/themedemos/page/2/", "desc": "author+pagination" }
	 * @example { "url": "/category/markup/", "desc": "category" }
	 * @example { "url": "/category/markup/page/2/", "desc": "category+pagination" }
	 * @example { "url": "/tag/content-2/", "desc": "tag" }
	 * @example { "url": "/tag/content-2/page/2/", "desc": "tag+pagination" }
	 * @example { "url": "/about-2/clearing-floats/", "desc": "page" }
	 * @example { "url": "/new-post-2/", "desc": "post" }
	 * @example { "url": "/2017/", "desc": "date" }
	 * @example { "url": "/2017/page/2/", "desc": "date+pagination" }
	 */
	public function testPagesCanonical( AcceptanceTester $I, \Codeception\Example $example ) {
		$href_amphtml = self::$BASE_URL . $example['url'] . self::$AMP_ENDPOINT . '/';

		$I->amOnPage( $example['url'] );
		$I->seeElement( "//link[@rel='amphtml'][@href='$href_amphtml']" );
		$I->dontSee( '( ! )' );
	}
}
