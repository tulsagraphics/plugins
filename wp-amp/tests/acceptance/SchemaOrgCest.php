<?php


class SchemaOrgCest {
	static $BASE_URL = 'http://wp.dev';

	public function _before( AcceptanceTester $I ) {
		include_once '../../../wp-tests-config.php';
	}

	public function _after( AcceptanceTester $I ) {
	}

	/**
	 * @example { "url": "/amp/", "desc": "home" }
	 * @example { "url": "/page/2/amp/", "desc": "home+pagination" }
	 */
	public function testHomePage( AcceptanceTester $I, \Codeception\Example $example ) {
		update_option( 'show_on_front', 'posts' );

		$this->_baseTest( $I, $example );
	}

	/**
	 * @example { "url": "/blog/amp/", "desc": "blog" }
	 * @example { "url": "/blog/page/2/amp/", "desc": "blog+pagination" }
	 */
	public function testBlogPage( AcceptanceTester $I, \Codeception\Example $example ) {
		update_option( 'show_on_front', 'page' );
		update_option( 'page_on_front', 701 );
		update_option( 'page_for_posts', 703 );

		$this->_baseTest( $I, $example );
	}


	/**
	 * @example { "url": "/author/themedemos/amp/", "desc": "author" }
	 * @example { "url": "/author/themedemos/page/2/amp/", "desc": "author+pagination" }
	 * @example { "url": "/category/markup/amp/", "desc": "category" }
	 * @example { "url": "/category/markup/page/2/amp/", "desc": "category+pagination" }
	 * @example { "url": "/tag/content-2/amp/", "desc": "tag" }
	 * @example { "url": "/tag/content-2/page/2/amp/", "desc": "tag+pagination" }
	 * @example { "url": "/about-2/clearing-floats/amp/", "desc": "page" }
	 * @example { "url": "/new-post-2/amp/", "desc": "post" }
	 * @example { "url": "/2017/amp/", "desc": "date" }
	 * @example { "url": "/2017/page/2/amp/", "desc": "date+pagination" }
	 */
	public function testIsSchemaOrg( AcceptanceTester $I, \Codeception\Example $example ) {
		$this->_baseTest( $I, $example );
	}

	public function _baseTest( AcceptanceTester $I, \Codeception\Example $example ) {
		$I->amOnPage( $example['url'] );
		$I->seeElement( "//script[contains(., 'schema.org')]" );
		$I->dontSee( '( ! )' );
	}
}
