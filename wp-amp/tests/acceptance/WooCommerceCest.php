<?php


class WooCommerceCest {

	static $BASE_URL = 'http://wp.dev';

	static $AMP_ENDPOINT = 'amp';

	const WOOCOMMERCE_PLUGIN = 'woocommerce/woocommerce.php';

	public function _before( AcceptanceTester $I ) {
		update_option( 'posts_per_page', 2 );
	}


	public function _after( AcceptanceTester $I ) {

	}

	/**
	 * @before activateWC
	 */
	public function testWoocommerceHomePape( AcceptanceTester $I ) {
		$shop_page_id = 1820;
		update_option( 'page_on_front', $shop_page_id );
		update_option( 'show_on_front', 'page' );
		$I->wantTo( 'ensure Shop Page as home page works' );
		$I->amOnPage( "/amp" );
		$I->seeElement( '.amphtml-price' );
		$I->dontSee( '( ! )' );
		update_option( 'page_on_front', 0 );
		update_option( 'show_on_front', 'posts' );
	}

	/**
	 * @example { "url": "/amp" }
	 * @example { "url": "/shop/amp" }
	 * @example { "url": "/product-category/posters/amp" }
	 * @example { "url": "/product-tag/sale/amp" }
	 * @example { "url": "/pa_color/black/amp" }
	 */
	public function testPagination( AcceptanceTester $I, \Codeception\Example $example ) {
		$I->amOnPage( $example['url'] );
		$I->click( '.next a' );
		$I->see( 'Previous', 'div.prev a' );
		$I->dontSee( '( ! )' );
	}

	/**
	 * @example { "url": "/product/woo-album-3/amp/", "desc": "product" }
	 * @example { "url": "/product-category/clothing/amp/", "desc": "product-category" }
	 * @example { "url": "/product-category/clothing/page/2/amp/", "desc": "product-category+pagination" }
	 * @example { "url": "/tag/content-2/amp/", "desc": "product-tag" }
	 * @example { "url": "/tag/content-2/page/2/amp/", "desc": "product-tag+pagination" }
	 * @example { "url": "/pa_color/black/amp/", "desc": "product-attr" }
	 */
	public function testCanonicalUrl( AcceptanceTester $I, \Codeception\Example $example ) {
		$href_canonical = self::$BASE_URL . substr( $example['url'], 0, - 4 );

		$I->amOnPage( $example['url'] );
		$I->seeElement( "//link[@rel='canonical'][@href='$href_canonical']" );
		$I->dontSee( '( ! )' );
	}

	/**
	 * @example { "url": "/product/woo-album-3/", "desc": "product" }
	 * @example { "url": "/product-category/clothing/", "desc": "product-category" }
	 * @example { "url": "/product-category/clothing/page/2/", "desc": "product-category+pagination" }
	 * @example { "url": "/tag/content-2/", "desc": "product-tag" }
	 * @example { "url": "/tag/content-2/page/2/", "desc": "product-tag+pagination" }
	 * @example { "url": "/pa_color/black/", "desc": "product-attr" }
	 */
	public function testAmphtmlUrl( AcceptanceTester $I, \Codeception\Example $example ) {
		$href_amphtml = self::$BASE_URL . $example['url'] . self::$AMP_ENDPOINT . '/';

		$I->amOnPage( $example['url'] );
		$I->seeElement( "//link[@rel='amphtml'][@href='$href_amphtml']" );
		$I->dontSee( '( ! )' );
	}

	/**
	 * @example { "url": "/product/woo-album-3/amp/", "desc": "product" }
	 * @example { "url": "/product-category/clothing/amp/", "desc": "product-category" }
	 * @example { "url": "/product-category/clothing/page/2/amp/", "desc": "product-category+pagination" }
	 * @example { "url": "/tag/content-2/amp/", "desc": "product-tag" }
	 * @example { "url": "/tag/content-2/page/2/amp/", "desc": "product-tag+pagination" }
	 * @example { "url": "/pa_color/black/amp/", "desc": "product-attr" }
	 */
	public function testSchemaOrg( AcceptanceTester $I, \Codeception\Example $example ) {
		$I->amOnPage( $example['url'] );
		$I->seeElement( "//script[contains(., 'schema.org')]" );
		$I->dontSee( '( ! )' );
	}

	protected function activateWC( AcceptanceTester $I ) {
		if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			$I->amOnPage( '/wp-admin' );
			$I->fillField( 'log', 'admin' );
			$I->fillField( 'pwd', 'admin123' );
			$I->click( 'wp-submit' );
			$I->amOnPage( '/wp-admin/plugins.php' );
			$I->click( '//a[@aria-label="Activate WooCommerce"]' );
		}
	}

	protected function deactivateWC() {
		deactivate_plugins( self::WOOCOMMERCE_PLUGIN );
	}

}
