<?php


class PaginationCeptCest {
	public function _before( AcceptanceTester $I ) {
		include_once '../../../wp-tests-config.php';
	}

	public function _after( AcceptanceTester $I ) {
	}

	/**
	 * @example { "url": "/amp" }
	 */
	public function testPostsFrontPage( AcceptanceTester $I, \Codeception\Example $example ) {
		//        $this->deactivateWC();
		update_option( 'show_on_front', 'posts' );
		$I->wantTo( 'ensure default front page works' );
		$I->amOnPage( $example['url'] );
		$I->click( 'div.next a' );
		$I->see( 'Previous', 'div.prev a' );
		$I->dontSee( '( ! )' );
	}

	/**
	 * @example { "url": "/author/themedemos/amp/" }
	 * @example { "url": "/category/markup/amp/" }
	 * @example { "url": "/tag/content-2/amp/" }
	 */
	public function testPagination( AcceptanceTester $I, \Codeception\Example $example ) {
		$I->amOnPage( $example['url'] );
		$I->click( '.next a' );
		$I->see( 'Previous', 'div.prev a' );
		$I->dontSee( '( ! )' );
	}

	/**
	 * @param AcceptanceTester $I
	 */
	public function test404( AcceptanceTester $I ) {
		$I->wantTo( 'ensure that 404 page works' );
		$I->amOnPage( '/new-post32/54236/amp/' );
		$I->see( 'Oops! That page can’t be found.' );
		$I->dontSee( '( ! )' );
	}

	public function testSearchNone( AcceptanceTester $I ) {
		$I->wantTo( 'ensure that search page with no result works' );
		$I->amOnPage( '/qwerqwer/amp' );
		$I->fillField( 's', 'asdfqwer' );
		$I->click( '.search-submit' );
		$I->seeElement( "//html[@amp]" );
		$I->dontSee( '( ! )' );
	}

	public function testSearch( AcceptanceTester $I ) {
		$I->wantTo( 'ensure that search page works' );
		$I->amOnPage( 'search/woo/amp' );
		$I->seeElement( "//html[@amp]" );
		$I->dontSee( '( ! )' );
	}

	public function testMobileRedirect( AcceptanceTester $I ) {
		update_option( 'amphtml_mobile_amp', 1 );
		$I->setHeader( 'User-Agent', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.23 Mobile Safari/537.36' );
		$I->amOnPage( '/qwerqwergdfgw' );
		$I->seeElement( "//html[@amp]" );
		$I->dontSee( '( ! )' );
		update_option( 'amphtml_mobile_amp', '' );
	}

	protected function deactivateWC() {
		deactivate_plugins( 'woocommerce/woocommerce.php' );
	}
}
