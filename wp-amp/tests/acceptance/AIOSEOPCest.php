<?php


class AIOSEOPCest {
	const PLUGIN_PATH = 'all-in-one-seo-pack/all_in_one_seo_pack.php';

	public function _before( AcceptanceTester $I ) {

	}

	public function _after( AcceptanceTester $I ) {

	}

	/**
	 * @before activatePlugin
	 */
	public function frontPage( AcceptanceTester $I ) {
		$title = $description = 'home';

		update_option( 'show_on_front', 'page' );
		update_option( 'page_on_front', 701 );
		update_option( 'page_for_posts', 703 );

		$I->amOnPage( '/amp' );

		$I->seeElement( "//title[contains(., '$title')]" );
		$I->seeElement( "//meta[@name='description'][contains(@content, '$description')]" );
		$I->dontSee( '( ! )' );

	}

	public function blog( AcceptanceTester $I ) {
		$title = $description = 'blog';

		$I->amOnPage( '/blog/amp' );

		$I->seeElement( "//title[contains(., '$title')]" );
		$I->seeElement( "//meta[@name='description'][contains(@content, '$description')]" );
		$I->dontSee( '( ! )' );

	}

	public function shopPage( AcceptanceTester $I ) {
		$title = $description = 'shop';

		$I->amOnPage( '/shop/amp' );

		$I->seeElement( "//title[contains(., '$title')]" );
		$I->seeElement( "//meta[@name='description'][contains(@content, '$description')]" );
		$I->dontSee( '( ! )' );

	}

	public function shopAsFrontPage( AcceptanceTester $I ) {
		$title       = 'home | Test Blog';
		$description = 'home';

		update_option( 'show_on_front', 'page' );
		update_option( 'page_on_front', 1820 );
		update_option( 'page_for_posts', 703 );

		$I->amOnPage( '/amp' );

		$I->seeElement( "//title[contains(., '$title')]" );
		$I->seeElement( "//meta[@name='description'][contains(@content, '$description')]" );
		$I->dontSee( '( ! )' );
	}

	/**
	 * @after deactivatePlugin
	 */
	public function latestPosts( AcceptanceTester $I ) {
		$title = $description = 'home';

		update_option( 'show_on_front', 'posts' );
		update_option( 'page_on_front', 0 );
		update_option( 'page_for_posts', 0 );

		$I->amOnPage( '/amp' );

		$I->seeElement( "//title[contains(., '$title')]" );
		$I->seeElement( "//meta[@name='description'][contains(@content, '$description')]" );
		$I->dontSee( '( ! )' );

	}

	protected function activatePlugin( AcceptanceTester $I ) {
		$label = 'Activate All In One SEO Pack';

		if ( ! is_plugin_active( self::PLUGIN_PATH ) ) {
			$I->amOnPage( '/wp-admin' );
			$I->fillField( 'log', 'admin' );
			$I->fillField( 'pwd', 'admin123' );
			$I->click( 'wp-submit' );
			$I->amOnPage( '/wp-admin/plugins.php' );
			$I->click( "//a[@aria-label='$label']" );
		}
	}

	protected function deactivatePlugin( AcceptanceTester $I ) {
		//        if ( is_plugin_active( self::PLUGIN_PATH ) ) {
		//            $I->amOnPage( '/wp-admin' );
		//            $I->fillField( 'log', 'admin' );
		//            $I->fillField( 'pwd', 'admin123' );
		//            $I->click( 'wp-submit' );
		//            $I->amOnPage( '/wp-admin/plugins.php' );
		//            $I->click( '//a[@aria-label="Deactivate All In One SEO Pack"]' );
		//        }
	}

}
