import { Component, ViewEncapsulation, ChangeDetectionStrategy, Input, OnChanges, OnDestroy } from '@angular/core';
import { FileEntry } from '../../uploads/file-entry';
import { PreviewFilesService } from '../preview-files.service';

@Component({
    selector: 'preview-container',
    templateUrl: './preview-container.component.html',
    styleUrls: ['./preview-container.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreviewContainerComponent implements OnChanges, OnDestroy {
    @Input() files: FileEntry[];

    constructor(public previewFiles: PreviewFilesService) {}

    ngOnChanges(changes) {
        this.previewFiles.set(this.files);
    }

    ngOnDestroy() {
        this.previewFiles.destroy();
    }
}
