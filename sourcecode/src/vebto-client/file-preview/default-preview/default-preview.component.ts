import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'default-preview',
  templateUrl: './default-preview.component.html',
  styleUrls: ['./default-preview.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DefaultPreviewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
