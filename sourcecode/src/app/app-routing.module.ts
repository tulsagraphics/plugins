import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { AuthGuard } from 'vebto-client/guards/auth-guard.service';
import { NOT_FOUND_ROUTES } from '../vebto-client/core/wildcard-routing.module';
import { HomepageComponent } from './homepage/homepage.component';
import { GuestGuard } from '../vebto-client/guards/guest-guard.service';

const routes: Routes = [
    {path: '', pathMatch: 'full', component: HomepageComponent, canActivate: [GuestGuard]},
    {path: 'admin', loadChildren: 'app/admin/app-admin.module#AppAdminModule', canLoad: [AuthGuard]},
    {path: 'drive', loadChildren: 'app/drive/drive.module#DriveModule'},
    {path: 'billing', loadChildren: 'vebto-client/billing/billing.module#BillingModule', canLoad: [AuthGuard]},
    ...NOT_FOUND_ROUTES,
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
