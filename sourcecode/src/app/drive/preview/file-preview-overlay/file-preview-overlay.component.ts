import {
    Component,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ElementRef,
    ViewChild,
    AfterViewInit,
    Inject,
} from '@angular/core';
import { DriveEntry } from '../../files/models/drive-entry';
import { OverlayPanelRef } from 'vebto-client/core/ui/overlay-panel/overlay-panel-ref';
import { OVERLAY_PANEL_DATA } from 'vebto-client/core/ui/overlay-panel/overlay-panel-data';
import { DriveContextMenuComponent } from '../../context-actions/components/drive-context-menu/drive-context-menu.component';
import { ContextMenu } from 'vebto-client/core/ui/context-menu/context-menu.service';
import { PreviewFilesService } from 'vebto-client/file-preview/preview-files.service';
import { DownloadEntries } from '../../state/actions/commands';
import { Store } from '@ngxs/store';

export interface FilePreviewOverlayData {
    entries: DriveEntry[];
}

@Component({
    selector: 'file-preview-overlay',
    templateUrl: './file-preview-overlay.component.html',
    styleUrls: ['./file-preview-overlay.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilePreviewOverlayComponent implements AfterViewInit {
    public entries: DriveEntry[] = [];
    @ViewChild('previewContainer', {read: ElementRef}) previewContainer: ElementRef;
    @ViewChild('moreOptionsButton', {read: ElementRef}) optionsButton: ElementRef;

    constructor(
        private store: Store,
        private el: ElementRef,
        private contextMenu: ContextMenu,
        private overlayRef: OverlayPanelRef,
        private previewFiles: PreviewFilesService,
        @Inject(OVERLAY_PANEL_DATA) public data: FilePreviewOverlayData
    ) {
        this.entries = data.entries;
    }

    ngAfterViewInit() {
        this.previewContainer.nativeElement.addEventListener('click', e => {
            if ( ! e.target.closest('.preview-object')) {
                this.overlayRef.close();
            }
        });
    }

    public openContextMenu() {
        const origin = this.optionsButton.nativeElement;
        this.contextMenu.open(DriveContextMenuComponent, origin, {data: {entry: this.previewFiles.getCurrent()}});
    }

    public closeOverlay() {
        this.overlayRef.close();
    }

    public downloadFiles() {
        const entries = this.previewFiles.getAllEntries();
        this.store.dispatch(new DownloadEntries(entries));
    }
}
