import { Actions, ofActionSuccessful, Store } from '@ngxs/store';
import { RouterNavigation, RouterState } from '@ngxs/router-plugin';
import { FolderPageOpened, RecentEntriesPageOpened, SharesFolderOpened, StarredPageOpened, TrashPageOpened } from '../actions/events';
import { DrivePageType } from '../models/available-pages';
import { DRIVE_PAGE_NAMES } from '../models/drive-page';
import { filter } from 'rxjs/operators';
import { CurrentUser } from 'vebto-client/auth/current-user';

const LINK_PREVIEW_PAGE = 's';

export class PageLoadHandler {
    constructor(
        private store: Store,
        private actions$: Actions,
        private currentUser: CurrentUser,
    ) {
        this.actions$
            .pipe(
                ofActionSuccessful(RouterNavigation),
                filter((action: RouterNavigation) => action.event.urlAfterRedirects.indexOf('drive') > -1),
                filter(() => this.currentUser.isLoggedIn()),
            )
            .subscribe(() => {
                const action = this.getPageLoadAction();
                if (action) this.store.dispatch(action);
            });
    }

    private getPageLoadAction() {
        const params = this.getPageUriParams();

        switch (params.pageName) {
            case DRIVE_PAGE_NAMES.RECENT:
                return new RecentEntriesPageOpened();
            case DRIVE_PAGE_NAMES.TRASH:
                return new TrashPageOpened();
            case DRIVE_PAGE_NAMES.SHARES:
                return new SharesFolderOpened();
            case DRIVE_PAGE_NAMES.STARRED:
                return new StarredPageOpened();
            case LINK_PREVIEW_PAGE:
                return null;
            default:
                return new FolderPageOpened(params.folderHash || 'root');
        }
    }

    private getPageUriParams() {
        // current drive uri parts
        let path = this.store.selectSnapshot(RouterState.url).split('/');

        // remove empty parts
        path = path.filter(x => !!x);

        return {pageName: path[1] as DrivePageType|string, folderHash: path[2]};
    }
}
