import { InjectionToken } from '@angular/core';
import { UploadInputConfig } from '../../vebto-client/uploads/upload-input-config';

export const DRIVE_UPLOAD_INPUT_CONFIG = new InjectionToken<UploadInputConfig>('DRIVE_UPLOAD_INPUT_CONFIG');