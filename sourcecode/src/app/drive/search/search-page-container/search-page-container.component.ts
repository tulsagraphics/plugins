import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { SearchPageOpened } from '../../state/actions/events';
import { DriveState } from '../../state/drive-state';
import { Observable } from 'rxjs';

@Component({
    selector: 'search-page-container',
    templateUrl: './search-page-container.component.html',
    styleUrls: ['./search-page-container.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchPageContainerComponent implements OnInit {
    @Select(DriveState.entriesEmpty) entriesEmpty: Observable<boolean>;

    constructor(
        private store: Store,
        private route: ActivatedRoute,
    ) {}

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.store.dispatch(
                new SearchPageOpened({...params})
            );
        });
    }
}
