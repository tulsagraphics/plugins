import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from 'vebto-client/guards/auth-guard.service';
import {CheckPermissionsGuard} from 'vebto-client/guards/check-permissions-guard.service';
import {AdminComponent} from 'vebto-client/admin/admin.component';
import {SettingsComponent} from 'vebto-client/admin/settings/settings.component';
import {SettingsResolve} from 'vebto-client/admin/settings/settings-resolve.service';
import {vebtoSettingsRoutes} from 'vebto-client/admin/settings/settings-routing.module';
import {vebtoAdminRoutes} from 'vebto-client/admin/admin-routing.module';

const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        canActivate: [AuthGuard, CheckPermissionsGuard],
        canActivateChild: [AuthGuard, CheckPermissionsGuard],
        data: {permissions: ['admin.access']},
        children: [
            {
                path: 'settings',
                component: SettingsComponent,
                resolve: {settings: SettingsResolve},
                data: {permissions: ['settings.view']},
                children: [
                    ...vebtoSettingsRoutes,
                ],
            },
            ...vebtoAdminRoutes,
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AppAdminRoutingModule {
}
