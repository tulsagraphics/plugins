<?php

namespace ACA\WC\Editing\ShopCoupon;

use ACP;

class Description extends ACP\Editing\Model {

	public function get_view_settings() {
		return array(
			'type' => 'textarea',
		);
	}

	public function save( $id, $value ) {
		$this->strategy->update( $id, array( 'post_excerpt' => $value ) );
	}

}