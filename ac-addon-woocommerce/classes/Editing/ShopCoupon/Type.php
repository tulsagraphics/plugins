<?php

namespace ACA\WC\Editing\ShopCoupon;

use ACP;

class Type extends ACP\Editing\Model {

	public function get_view_settings() {
		return array(
			'type'    => 'select',
			'options' => wc_get_coupon_types(),
		);
	}

	public function save( $id, $value ) {
		$coupon = new \WC_Coupon( $id );

		try {
			$coupon->set_discount_type( $value );
		} catch ( \WC_Data_Exception $e ) {
			return new \WP_Error( $e->getErrorCode(), $e->getMessage() );
		}

		return $coupon->save();
	}

}