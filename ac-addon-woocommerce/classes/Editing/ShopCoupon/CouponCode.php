<?php

namespace ACA\WC\Editing\ShopCoupon;

use ACP;

class CouponCode extends ACP\Editing\Model {

	public function get_view_settings() {
		return array(
			'type' => 'text',
			'js'   => array(
				'selector' => 'strong > a',
			),
		);
	}

	public function save( $id, $value ) {
		$this->strategy->update( $id, array( 'post_title' => $value ) );
	}

}