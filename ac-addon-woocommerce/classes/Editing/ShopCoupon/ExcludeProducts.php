<?php

namespace ACA\WC\Editing\ShopCoupon;

use ACP;

class ExcludeProducts extends ACP\Editing\Model {

	public function get_view_settings() {
		return array(
			'type'          => 'select2_dropdown',
			'ajax_populate' => true,
			'multiple'      => true,
		);
	}

	public function get_ajax_options( $request ) {
		return ac_addon_wc_helper()->search_products( $request['search'], array( 'paged' => $request['paged'] ) );
	}

	public function get_edit_value( $id ) {
		return ac_addon_wc_helper()->get_editable_posts_values( $this->column->get_raw_value( $id ) );
	}

	public function save( $id, $value ) {
		$coupon = new \WC_Coupon( $id );

		try {
			$coupon->set_excluded_product_ids( $value );
		} catch ( \WC_Data_Exception $e ) {
			return new \WP_Error( $e->getErrorCode(), $e->getMessage() );
		}

		return $coupon->save();
	}

}