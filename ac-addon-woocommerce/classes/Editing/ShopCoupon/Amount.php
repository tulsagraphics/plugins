<?php

namespace ACA\WC\Editing\ShopCoupon;

use ACP;

class Amount extends ACP\Editing\Model {

	public function save( $id, $value ) {
		$coupon = new \WC_Coupon( $id );

		try {
			$coupon->set_amount( $value );
		} catch ( \WC_Data_Exception $e ) {
			return new \WP_Error( $e->getErrorCode(), $e->getMessage() );
		}

		return $coupon->save();
	}

}