<?php

namespace ACA\WC\Editing\Product\Attributes;

use ACA\WC\Editing;
use ACP;

/**
 * @property \ACA\WC\Column\Product\Attributes $column
 */
class Taxonomy extends ACP\Editing\Model\Post\Taxonomy {

	public function __construct( \ACA\WC\Column\Product\Attributes $column ) {
		parent::__construct( $column );
	}

	/**
	 * Save attributes to product
	 *
	 * @param int   $id
	 * @param int[] $term_ids
	 */
	private function save_to_product( $id, $term_ids ) {
		if ( ! $term_ids ) {
			return;
		}

		$options = array();

		foreach ( $term_ids as $term_id ) {
			$options[ $term_id ] = ac_helper()->taxonomy->get_term_field( 'name', $term_id, $this->column->get_taxonomy() );
		}

		$model = new Editing\Product\Attributes( $this->column );
		$model->save( $id, $options );
	}

	/**
	 * @param int   $id
	 * @param array $value
	 */
	public function save( $id, $value ) {
		$term_ids = parent::save( $id, $value );

		$this->save_to_product( $id, $term_ids );
	}

}