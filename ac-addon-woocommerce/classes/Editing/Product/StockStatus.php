<?php

namespace ACA\WC\Editing\Product;

use ACA\WC\Column;
use ACP;

/**
 * @property Column\Product\StockStatus $column
 */
class StockStatus extends ACP\Editing\Model {

	public function __construct( Column\Product\StockStatus $column ) {
		parent::__construct( $column );
	}

	public function get_edit_value( $id ) {
		$product = wc_get_product( $id );

		if ( ! $product->is_type( 'simple' ) ) {
			return null;
		}

		return $this->column->get_raw_value( $id );
	}

	public function get_view_settings() {
		return array(
			'type'    => 'togglable',
			'options' => array( 'outofstock', 'instock' ),
		);
	}

	public function save( $id, $value ) {
		wc_update_product_stock_status( $id, wc_clean( $value ) );
	}

}