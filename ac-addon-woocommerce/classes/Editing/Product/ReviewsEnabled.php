<?php

namespace ACA\WC\Editing\Product;

use ACP;

class ReviewsEnabled extends ACP\Editing\Model {

	public function get_view_settings() {
		return array(
			'type'    => 'togglable',
			'options' => array( 'closed', 'open' ),
		);
	}

	public function save( $id, $value ) {
		$this->strategy->update( $id, array( 'comment_status' => $value ) );
	}

}