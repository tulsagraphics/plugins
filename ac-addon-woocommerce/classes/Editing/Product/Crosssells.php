<?php

namespace ACA\WC\Editing\Product;

use ACP;

class Crosssells extends ACP\Editing\Model {

	public function get_view_settings() {
		return array(
			'type'          => 'select2_dropdown',
			'ajax_populate' => true,
			'multiple'      => true,
		);
	}

	public function get_ajax_options( $request ) {
		return ac_addon_wc_helper()->search_products( $request['search'], array( 'paged' => $request['paged'] ) );
	}

	public function get_edit_value( $id ) {
		return ac_addon_wc_helper()->get_editable_posts_values( wc_get_product( $id )->get_cross_sell_ids() );
	}

	public function save( $id, $value ) {
		$product = wc_get_product( $id );
		$product->set_cross_sell_ids( $value );
		$product->save();
	}

}