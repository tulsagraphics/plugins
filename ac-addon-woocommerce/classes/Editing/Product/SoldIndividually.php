<?php

namespace ACA\WC\Editing\Product;

use ACP;

/**
 * @since 3.0
 */
class SoldIndividually extends ACP\Editing\Model {

	public function get_view_settings() {
		return array(
			'type'    => 'togglable',
			'options' => array( '', '1' ),
		);
	}

	public function save( $id, $value ) {
		$product = wc_get_product( $id );
		$product->set_sold_individually( $value );
		$product->save();
	}

}