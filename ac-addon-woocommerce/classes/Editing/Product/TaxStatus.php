<?php

namespace ACA\WC\Editing\Product;

use ACA\WC\Column;
use ACP;

/**
 * @property Column\Product\TaxStatus $column
 */
class TaxStatus extends ACP\Editing\Model {

	public function __construct( Column\Product\TaxStatus $column ) {
		parent::__construct( $column );
	}

	public function get_view_settings() {
		$settings = array(
			'type'    => 'select',
			'options' => $this->column->get_tax_status(),
		);

		return $settings;
	}

	public function save( $id, $value ) {
		$product = wc_get_product( $id );

		try {
			$product->set_tax_status( $value );
		} catch ( \WC_Data_Exception $e ) {
			return new \WP_Error( $e->getErrorCode(), $e->getMessage() );
		}

		return $product->save();
	}

}