<?php

namespace ACA\WC\Field\ShopOrder;

use ACA\WC\Column;
use ACA\WC\Export;
use ACA\WC\Field;
use ACP;

/**
 * @since 3.0
 * @property Column\ShopOrder\OrderDate $column
 */
abstract class OrderDate extends Field
	implements ACP\Export\Exportable, ACP\Sorting\Sortable {

	/**
	 * @param \WC_Order $order
	 *
	 * @return \WC_DateTime|false
	 */
	abstract public function get_date( \WC_Order $order );

	public function get_value( $id ) {
		$order = new \WC_Order( $id );

		$date = $this->get_date( $order );

		if ( ! $date ) {
			return false;
		}

		return $date->getTimestamp();
	}

	public function get_meta_key() {
		return false;
	}

	public function export() {
		return new Export\ShopOrder\OrderDate( $this->column );
	}

	public function sorting() {
		if ( $this->get_meta_key() ) {
			return new ACP\Sorting\Model\Meta( $this->column );
		}

		return new ACP\Sorting\Model\Value( $this->column );
	}

	public function filtering() {
		return new ACP\Filtering\Model\Disabled( $this->column );
	}

}