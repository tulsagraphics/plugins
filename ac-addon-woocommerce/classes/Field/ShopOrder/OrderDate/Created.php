<?php

namespace ACA\WC\Field\ShopOrder\OrderDate;

use ACA\WC\Field\ShopOrder\OrderDate;
use ACP;

/**
 * @since 3.0
 */
class Created extends OrderDate {

	public function set_label() {
		$this->label = __( 'Created', 'codepress-admin-columns' );
	}

	public function get_date( \WC_Order $order ) {
		return $order->get_date_created();
	}

	public function filtering() {
		return new ACP\Filtering\Model\Post\Date( $this->column );
	}

}