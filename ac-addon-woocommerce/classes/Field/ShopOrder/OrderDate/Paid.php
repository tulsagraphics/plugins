<?php

namespace ACA\WC\Field\ShopOrder\OrderDate;

use ACA\WC\Field\ShopOrder\OrderDate;
use ACA\WC\Filtering;

/**
 * @since 3.0
 */
class Paid extends OrderDate {

	public function set_label() {
		$this->label = __( 'Paid', 'codepress-admin-columns' );
	}

	public function get_date( \WC_Order $order ) {
		return $order->get_date_paid();
	}

	public function get_meta_key() {
		return '_date_paid';
	}

	public function filtering() {
		return new Filtering\ShopOrder\MetaDate( $this->column );
	}

}