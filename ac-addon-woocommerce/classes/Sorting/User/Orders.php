<?php

namespace ACA\WC\Sorting\User;

use ACP;

class Orders extends ACP\Sorting\Model {

	public function get_sorting_vars() {
		$values = array();

		foreach ( $this->strategy->get_results() as $id ) {
			$values[ $id ] = count( $this->column->get_raw_value( $id ) );
		}

		return array(
			'ids' => $this->sort( $values )
		);
	}

}