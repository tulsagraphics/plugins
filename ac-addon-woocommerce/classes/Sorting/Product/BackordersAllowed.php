<?php

namespace ACA\WC\Sorting\Product;

use ACP;

/**
 * @property \ACA\WC\Column\Product\BackordersAllowed $column
 */
class BackordersAllowed extends ACP\Sorting\Model {

	public function get_sorting_vars() {
		$post_ids = array();
		foreach ( $this->strategy->get_results() as $post_id ) {
			$post_ids[ $post_id ] = $this->column->get_backorders( $post_id );
		}

		return array(
			'ids' => $this->sort( $post_ids ),
		);
	}

}