<?php

namespace ACA\WC\Column\ShopCoupon;

use AC;
use ACP;
use ACA\WC\Editing;

/**
 * @since 1.0
 */
class CouponCode extends AC\Column
	implements ACP\Editing\Editable {

	public function __construct() {
		$this->set_type( 'coupon_code' );
		$this->set_original( true );
	}

	// Display

	public function get_value( $id ) {
		return null;
	}

	public function get_raw_value( $id ) {
		$coupon = new \WC_Coupon( $id );

		return $coupon->get_code();
	}

	// Pro

	public function editing() {
		return new Editing\ShopCoupon\CouponCode( $this );
	}

}