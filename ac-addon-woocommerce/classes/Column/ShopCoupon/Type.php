<?php

namespace ACA\WC\Column\ShopCoupon;

use ACP;
use ACA\WC\Export;
use ACA\WC\Editing;
use ACA\WC\Filtering;
use ACA\WC\Sorting;

/**
 * @since 1.0
 */
class Type extends ACP\Column\Meta
	implements ACP\Sorting\Sortable, ACP\Editing\Editable, ACP\Filtering\Filterable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'type' );
		$this->set_original( true );
	}

	public function get_value( $id ) {
		return null;
	}

	public function get_meta_key() {
		return 'discount_type';
	}

	public function get_raw_value( $id ) {
		$coupon = new \WC_Coupon( $id );

		return $coupon->get_discount_type();
	}

	// Pro

	public function editing() {
		return new Editing\ShopCoupon\Type( $this );
	}

	public function sorting() {
		return new Sorting\ShopCoupon\Type( $this );
	}

	public function filtering() {
		return new Filtering\ShopCoupon\Type( $this );
	}

	public function export() {
		return new Export\ShopCoupon\Type( $this );
	}

}