<?php

namespace ACA\WC\Column\ShopCoupon;

use AC;
use ACP;
use ACA\WC\Editing;
use ACA\WC\Export;

/**
 * @since 2.2
 */
class EmailRestrictions extends AC\Column
	implements ACP\Sorting\Sortable, ACP\Editing\Editable, ACP\Filtering\Filterable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'column-wc-email-restrictions' );
		$this->set_label( __( 'Email Restrictions', 'codepress-admin-columns' ) );
		$this->set_group( 'woocommerce' );
	}

	// Display

	public function get_value( $id ) {
		return implode( ', ', $this->get_raw_value( $id ) );
	}

	// Pro
	public function filtering() {
		return new ACP\Filtering\Model\Disabled( $this );
	}

	public function sorting() {
		return new ACP\Sorting\Model( $this );
	}

	public function editing() {
		return new Editing\ShopCoupon\EmailRestrictions( $this );
	}

	public function export() {
		return new Export\ShopCoupon\EmailRestrictions( $this );
	}

	// Common

	public function get_raw_value( $id ) {
		$coupon = new \WC_Coupon( $id );

		return $coupon->get_email_restrictions();
	}

}