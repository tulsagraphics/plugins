<?php

namespace ACA\WC\Column\ShopCoupon;

use ACP;
use ACA\WC\Filtering;
use ACA\WC\Editing;
use ACA\WC\Export;

/**
 * @since 1.0
 */
class Amount extends ACP\Column\Meta
	implements ACP\Sorting\Sortable, ACP\Editing\Editable, ACP\Filtering\Filterable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'amount' );
		$this->set_original( true );
	}

	// Meta

	public function get_meta_key() {
		return 'coupon_amount';
	}

	// Display

	public function get_value( $id ) {
		return null;
	}

	public function get_raw_value( $id ) {
		$coupon = new \WC_Coupon( $id );

		return $coupon->get_amount();
	}

	// Pro

	public function filtering() {
		return new Filtering\Numeric( $this );
	}

	public function sorting() {
		$model = new ACP\Sorting\Model( $this );
		$model->set_data_type( 'numeric' );

		return $model;
	}

	public function editing() {
		return new Editing\ShopCoupon\Amount( $this );
	}

	public function export() {
		return new Export\ShopCoupon\Amount( $this );
	}

}