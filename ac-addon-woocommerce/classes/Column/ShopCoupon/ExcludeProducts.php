<?php

namespace ACA\WC\Column\ShopCoupon;

use AC;
use ACP;
use ACA\WC\Editing;

/**
 * @since 1.1
 */
class ExcludeProducts extends AC\Column
	implements ACP\Editing\Editable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'column-wc-exclude_products' );
		$this->set_label( __( 'Excluded Products', 'codepress-admin-columns' ) );
		$this->set_group( 'woocommerce' );
	}

	// Display

	public function get_value( $post_id ) {
		$products = array();

		foreach ( $this->get_raw_value( $post_id ) as $id ) {
			$products[] = ac_helper()->html->link( get_edit_post_link( $id ), get_the_title( $id ) );
		}

		$value = implode( ', ', array_filter( $products ) );

		if ( ! $value ) {
			return $this->get_empty_char();
		}

		return $value;
	}

	public function get_raw_value( $id ) {
		$coupon = new \WC_Coupon( $id );

		return (array) $coupon->get_excluded_product_ids();
	}

	// Pro

	public function editing() {
		return new Editing\ShopCoupon\ExcludeProducts( $this );
	}

	public function export() {
		return new ACP\Export\Model\StrippedValue( $this );
	}

}