<?php

namespace ACA\WC\Column\ShopCoupon;

use AC;
use ACP;
use ACA\WC\Editing;
use ACA\WC\Export;

/**
 * @since 1.0
 */
class Description extends AC\Column
	implements ACP\Sorting\Sortable, ACP\Editing\Editable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'description' );
		$this->set_original( true );
	}

	// Display

	public function get_value( $id ) {
		return null;
	}

	public function get_raw_value( $post_id ) {
		return (string) ac_helper()->post->get_raw_field( 'post_excerpt', $post_id );
	}

	// Pro

	public function editing() {
		return new Editing\ShopCoupon\Description( $this );
	}

	public function sorting() {
		return new ACP\Sorting\Model( $this );
	}

	public function export() {
		return new Export\ShopCoupon\Description( $this );
	}

}