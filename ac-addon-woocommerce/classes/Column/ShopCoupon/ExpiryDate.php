<?php

namespace ACA\WC\Column\ShopCoupon;

use AC;
use ACP;
use ACA\WC\Editing;
use ACA\WC\Sorting;

/**
 * @since 1.0
 */
class ExpiryDate extends AC\Column\Meta
	implements ACP\Sorting\Sortable, ACP\Editing\Editable, ACP\Filtering\Filterable {

	public function __construct() {
		$this->set_type( 'expiry_date' );
		$this->set_original( true );
	}

	// Meta

	public function get_meta_key() {
		return 'expiry_date';
	}

	// Display

	public function get_value( $id ) {
		return null;
	}

	// Pro

	public function sorting() {
		return new Sorting\ShopCoupon\ExpiryDate( $this );
	}

	public function editing() {
		return new Editing\ShopCoupon\ExpiryDate( $this );
	}

	public function filtering() {
		return new ACP\Filtering\Model\MetaDate( $this );
	}

}