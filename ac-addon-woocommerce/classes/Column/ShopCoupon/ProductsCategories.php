<?php

namespace ACA\WC\Column\ShopCoupon;

use ACP;
use ACA\WC\Column;
use ACA\WC\Filtering;
use ACA\WC\Editing;

/**
 * @since 3.0
 */
class ProductsCategories extends Column\CouponProductCategories
	implements ACP\Filtering\Filterable, ACP\Editing\Editable {

	public function __construct() {
		parent::__construct();

		$this->set_type( 'column-wc-coupon_product_categories' );
		$this->set_label( __( 'Product Categories', 'woocommerce' ) );
	}

	public function get_meta_key() {
		return 'product_categories';
	}

	public function filtering() {
		return new Filtering\ShopCoupon\ProductCategories( $this );
	}

	public function editing() {
		return new Editing\ShopCoupon\ProductCategories( $this );
	}

}