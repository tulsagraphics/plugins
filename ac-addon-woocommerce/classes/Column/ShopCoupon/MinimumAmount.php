<?php

namespace ACA\WC\Column\ShopCoupon;

use ACP;
use ACA\WC\Filtering;
use ACA\WC\Editing;

/**
 * @since 1.1
 */
class MinimumAmount extends ACP\Column\Meta
	implements ACP\Editing\Editable, ACP\Sorting\Sortable, ACP\Filtering\Filterable {

	public function __construct() {
		$this->set_type( 'column-wc-minimum_amount' );
		$this->set_label( __( 'Minimum Amount', 'codepress-admin-columns' ) );
		$this->set_group( 'woocommerce' );
	}

	public function get_meta_key() {
		return 'minimum_amount';
	}

	public function get_value( $id ) {
		$amount = $this->get_raw_value( $id );

		if ( ! $amount ) {
			return $this->get_empty_char();
		}

		return wc_price( $amount );
	}

	public function filtering() {
		return new Filtering\Numeric( $this );
	}

	public function editing() {
		return new Editing\ShopCoupon\MinimumAmount( $this );
	}

	public function sorting() {
		return new ACP\Sorting\Model( $this );
	}

	public function get_raw_value( $id ) {
		$coupon = new \WC_Coupon( $id );

		return $coupon->get_minimum_amount();
	}

}