<?php

namespace ACA\WC\Column\ShopCoupon;

use AC;
use ACP;
use ACA\WC\Export;
use ACA\WC\Editing;

/**
 * @since 1.0
 */
class Usage extends AC\Column\Meta
	implements ACP\Sorting\Sortable, ACP\Editing\Editable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'usage' );
		$this->set_original( true );
	}

	public function get_meta_key() {
		return 'usage_count';
	}

	public function get_value( $id ) {
		return '';
	}

	// Pro

	public function editing() {
		return new Editing\ShopCoupon\Usage( $this );
	}

	public function sorting() {
		$model = new ACP\Sorting\Model\Meta( $this );
		$model->set_data_type( 'numeric' );

		return $model;
	}

	public function export() {
		return new Export\ShopCoupon\Usage( $this );
	}

}