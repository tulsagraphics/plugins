<?php

namespace ACA\WC\Column\ShopCoupon;

use AC;
use ACP;
use ACA\WC\Export;

/**
 * @since 2.2
 */
class Products extends AC\Column
	implements ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'products' );
		$this->set_original( true );
	}

	public function export() {
		return new Export\ShopCoupon\Products( $this );
	}

}