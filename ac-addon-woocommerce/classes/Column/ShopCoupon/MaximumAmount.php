<?php

namespace ACA\WC\Column\ShopCoupon;

use ACP;
use ACA\WC\Filtering;
use ACA\WC\Editing;

/**
 * @since 1.1
 */
class MaximumAmount extends ACP\Column\Meta
	implements ACP\Sorting\Sortable, ACP\Editing\Editable, ACP\Filtering\Filterable {

	public function __construct() {
		$this->set_type( 'column-wc-maximum_amount' );
		$this->set_label( __( 'Maximum Amount', 'codepress-admin-columns' ) );
		$this->set_group( 'woocommerce' );
	}

	// Meta

	public function get_meta_key() {
		return 'maximum_amount';
	}

	// Display

	public function get_value( $id ) {
		$amount = $this->get_raw_value( $id );

		if ( ! $amount ) {
			return $this->get_empty_char();
		}

		return wc_price( $amount );
	}

	// Pro

	public function filtering() {
		return new Filtering\Numeric( $this );
	}

	public function sorting() {
		return new ACP\Sorting\Model( $this );
	}

	public function editing() {
		return new Editing\ShopCoupon\MaximumAmount( $this );
	}

	// Common

	public function get_raw_value( $id ) {
		$coupon = new \WC_Coupon( $id );

		return $coupon->get_maximum_amount();
	}

}