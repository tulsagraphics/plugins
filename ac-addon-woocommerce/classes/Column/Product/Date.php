<?php

namespace ACA\WC\Column\Product;

use AC;
use ACP;

/**
 * @since 2.0
 */
class Date extends AC\Column
	implements ACP\Filtering\Filterable {

	public function __construct() {
		$this->set_type( 'date' );
		$this->set_original( true );
	}

	public function filtering() {
		return new ACP\Filtering\Model\Post\Date( $this );
	}

}