<?php

namespace ACA\WC\Column\Product;

use AC;
use ACP;

/**
 * @since 1.1
 */
class ProductType extends AC\Column
	implements ACP\Sorting\Sortable, ACP\Filtering\Filterable {

	public function __construct() {
		$this->set_type( 'column-wc-product_type' );
		$this->set_label( __( 'Type', 'codepress-admin-columns' ) );
		$this->set_group( 'woocommerce' );
	}

	public function get_value( $post_id ) {
		$product_type = $this->get_raw_value( $post_id );

		switch ( $product_type ) {
			case 'downloadable' :
				$label = __( 'Downloadable', 'woocommerce' );

				break;
			case 'virtual' :
				$label = __( 'Virtual', 'woocommerce' );

				break;
			default :
				$types = wc_get_product_types();

				$label = $product_type;

				if ( isset( $types[ $product_type ] ) ) {
					$label = $types[ $product_type ];
				}
		}

		return $label;
	}

	public function get_raw_value( $post_id ) {
		$product = wc_get_product( $post_id );
		$type = $product->get_type();

		if ( $product->is_downloadable() ) {
			$type = 'downloadable';
		}

		if ( $product->is_virtual() ) {
			$type = 'virtual';
		}

		return $type;
	}

	public function sorting() {
		return new ACP\Sorting\Model\Value( $this );
	}

	public function filtering() {
		return new ACP\Filtering\Model\Delegated( $this, 'dropdown_product_type' );
	}

}