<?php

namespace ACA\WC\Column\Product;

use AC;
use ACA\WC\Editing;
use ACA\WC\Export;
use ACP;

/**
 * @since 1.1
 */
class Stock extends AC\Column
	implements ACP\Editing\Editable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'is_in_stock' );
		$this->set_original( true );
	}

	// Pro

	public function editing() {
		return new Editing\Product\Stock( $this );
	}

	public function export() {
		return new Export\Product\Stock( $this );
	}

}