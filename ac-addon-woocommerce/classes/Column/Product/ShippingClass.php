<?php

namespace ACA\WC\Column\Product;

use AC;
use ACA\WC\Editing;
use ACP;

/**
 * @since 1.1
 */
class ShippingClass extends AC\Column
	implements ACP\Sorting\Sortable, ACP\Editing\Editable, ACP\Filtering\Filterable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'column-wc-shipping_class' );
		$this->set_label( __( 'Shipping Class', 'woocommerce' ) );
		$this->set_group( 'woocommerce' );
	}

	public function get_taxonomy() {
		return 'product_shipping_class';
	}

	// Display

	public function get_value( $post_id ) {
		$term = get_term_by( 'id', $this->get_raw_value( $post_id ), $this->get_taxonomy() );

		return ac_helper()->taxonomy->get_term_display_name( $term );
	}

	public function get_raw_value( $post_id ) {
		return wc_get_product( $post_id )->get_shipping_class_id();
	}

	// Pro

	public function sorting() {
		return new ACP\Sorting\Model\Post\Taxonomy( $this );
	}

	public function editing() {
		return new Editing\Product\ShippingClass( $this );
	}

	public function filtering() {
		return new ACP\Filtering\Model\Post\Taxonomy( $this );
	}

	public function export() {
		return new ACP\Export\Model\StrippedValue( $this );
	}

}