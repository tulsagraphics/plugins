<?php

namespace ACA\WC\Column\Product;

use AC;
use ACA\WC\Editing;
use ACA\WC\Export;
use ACP;

class SKU extends AC\Column
	implements ACP\Editing\Editable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'sku' );
		$this->set_original( true );
	}

	public function editing() {
		return new Editing\Product\SKU( $this );
	}

	public function export() {
		return new Export\Product\SKU( $this );
	}
}