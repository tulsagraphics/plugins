<?php

namespace ACA\WC\Column\Product;

use AC;
use ACA\WC\Filtering;
use ACP;

/**
 * @since 1.0
 */
class Thumb extends AC\Column\Meta
	implements ACP\Editing\Editable, ACP\Filtering\Filterable {

	public function __construct() {
		$this->set_type( 'thumb' );
		$this->set_original( true );
	}

	// Display

	public function get_value( $id ) {
		return null;
	}

	// Meta

	public function get_meta_key() {
		return '_thumbnail_id';
	}

	// Display

	public function get_raw_value( $post_id ) {
		return has_post_thumbnail( $post_id ) ? get_post_thumbnail_id( $post_id ) : false;
	}

	// Pro

	public function editing() {
		return new ACP\Editing\Model\Post\FeaturedImage( $this );
	}

	public function filtering() {
		return new Filtering\Product\Thumb( $this );
	}

}