<?php

namespace ACA\WC\Column\Product;

use AC;
use ACA\WC\Filtering;
use ACA\WC\Sorting;
use ACP;

/**
 * @since 3.0
 */
class Reviews extends AC\Column\Meta
	implements ACP\Filtering\Filterable, ACP\Sorting\Sortable {

	public function __construct() {
		$this->set_type( 'column-wc-product_reviews' );
		$this->set_label( __( 'Reviews', 'woocommerce' ) );
		$this->set_group( 'woocommerce' );
	}

	// Display

	public function get_value( $id ) {
		$count = $this->get_raw_value( $id );

		if ( ! $count ) {
			return $this->get_empty_char();
		}

		$link = add_query_arg( array( 'p' => $id, 'status' => 'approved' ), get_admin_url( null, 'edit-comments.php' ) );

		return ac_helper()->html->link( $link, $count );
	}

	public function get_raw_value( $post_id ) {
		$product = wc_get_product( $post_id );

		return $product->get_review_count();
	}

	// Meta

	public function get_meta_key() {
		return '_wc_review_count';
	}

	// Pro

	public function sorting() {
		return new ACP\Sorting\Model\Meta( $this );
	}

	public function filtering() {
		return new Filtering\Numeric( $this );
	}

}