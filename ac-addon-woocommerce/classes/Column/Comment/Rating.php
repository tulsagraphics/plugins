<?php

namespace ACA\WC\Column\Comment;

use AC;
use ACA\WC\Editing;
use ACA\WC\Filtering;
use ACA\WC\Sorting;
use ACP;

/**
 * @since 3.0
 */
class Rating extends AC\Column\Meta
	implements ACP\Editing\Editable, ACP\Filtering\Filterable, ACP\Sorting\Sortable {

	public function __construct() {
		$this->set_group( 'woocommerce' );
		$this->set_type( 'column-wc-comment_rating' );
		$this->set_label( __( 'Rating', 'woocommerce' ) );
	}

	// Meta

	public function get_meta_key() {
		return 'rating';
	}

	// Display

	public function get_value( $id ) {
		$rating = $this->get_raw_value( $id );

		if ( ! $rating ) {
			return $this->get_empty_char();
		}

		return ac_helper()->html->stars( $rating, 5 );
	}

	// Pro

	public function editing() {
		return new Editing\Comment\Rating( $this );
	}

	public function filtering() {
		return new Filtering\Numeric( $this );
	}

	public function sorting() {
		return new Sorting\Comment\Rating( $this );
	}

}