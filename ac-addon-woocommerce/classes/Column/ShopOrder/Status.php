<?php

namespace ACA\WC\Column\ShopOrder;

use AC;
use ACP;
use ACA\WC\Filtering;
use ACA\WC\Editing;

/**
 * @since 1.1
 */
class Status extends AC\Column
	implements ACP\Sorting\Sortable, ACP\Editing\Editable, ACP\Filtering\Filterable {

	public function __construct() {
		$this->set_type( 'order_status' );
		$this->set_original( true );
	}

	// Display

	public function get_value( $id ) {
		return null;
	}

	public function get_raw_value( $post_id ) {
		$order = new \WC_Order( $post_id );

		return $order->get_status();
	}

	protected function register_settings() {
		$width = $this->get_setting( 'width' );

		$width->set_default( 150 );
		$width->set_default( 'px', 'width_unit' );
	}

	// Pro

	public function sorting() {
		return new ACP\Sorting\Model( $this );
	}

	public function editing() {
		return new Editing\ShopOrder\Status( $this );
	}

	public function filtering() {
		return new Filtering\ShopOrder\Status( $this );
	}

	// Common

	public function get_order_status_options() {
		return wc_get_order_statuses();
	}

}