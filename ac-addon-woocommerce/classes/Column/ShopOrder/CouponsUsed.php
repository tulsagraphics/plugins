<?php

namespace ACA\WC\Column\ShopOrder;

use AC;
use ACP;
use ACA\WC\Filtering;

/**
 * @since 1.3
 */
class CouponsUsed extends AC\Column\Meta
	implements ACP\Filtering\Filterable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'column-wc-order_coupons_used' );
		$this->set_label( __( 'Coupons Used', 'codepress-admin-columns' ) );
		$this->set_group( 'woocommerce' );
	}

	// Meta

	public function get_meta_key() {
		return '_recorded_coupon_usage_counts';
	}

	// Display

	public function get_value( $post_id ) {
		$used_coupons = $this->get_raw_value( $post_id );

		if ( ! $used_coupons ) {
			return $this->get_empty_char();
		}

		$coupons = array();
		foreach ( $used_coupons as $code ) {
			$coupons[] = ac_helper()->html->link( get_edit_post_link( ac_addon_wc_helper()->get_coupon_id_from_code( $code ) ), $code );
		}

		return implode( ' | ', $coupons );
	}

	public function get_raw_value( $post_id ) {
		$order = new \WC_Order( $post_id );

		if ( ! $order ) {
			return array();
		}

		return $order->get_used_coupons();
	}

	// Pro

	public function filtering() {
		return new Filtering\ShopOrder\CouponUsed( $this );
	}

	public function export() {
		return new ACP\Export\Model\StrippedValue( $this );
	}

}