<?php

namespace ACA\WC\Column\ShopOrder;

use AC;
use ACP;
use ACA\WC\Filtering;
use ACA\WC\Settings;

/**
 * @since 2.0
 */
class Customer extends AC\Column\Meta
	implements ACP\Export\Exportable, ACP\Filtering\Filterable {

	public function __construct() {
		$this->set_label( 'Customer' );
		$this->set_type( 'column-wc-order_customer' );
		$this->set_group( 'woocommerce' );
	}

	public function get_meta_key() {
		return '_customer_user';
	}

	public function register_settings() {
		$this->add_setting( new Settings\ShopOrder\Customer( $this ) );
	}

	/**
	 * @return ACP\Export\Model
	 */
	public function export() {
		return new ACP\Export\Model\StrippedValue( $this );
	}

	/**
	 * @return ACP\Filtering\Model
	 */
	public function filtering() {

		switch ( $this->get_user_property() ) {
			case 'roles':

				return new Filtering\ShopOrder\CustomerRole( $this );
			default:

				return new ACP\Filtering\Model\Disabled( $this );
		}

	}

	/**
	 * @return string
	 */
	public function get_user_property() {
		$setting = $this->get_setting( 'user' );

		if ( ! $setting instanceof Settings\ShopOrder\Customer ) {
			return false;
		}

		return $setting->get_display_author_as();
	}

}