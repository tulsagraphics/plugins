<?php

namespace ACA\WC\Column\ShopOrder;

use AC;
use ACP;
use ACA\WC\Filtering;

/**
 * @since 3.0
 */
class Currency extends AC\Column\Meta
	implements ACP\Filtering\Filterable, ACP\Sorting\Sortable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_label( 'Currency' );
		$this->set_type( 'column-wc-order_currency' );
		$this->set_group( 'woocommerce' );
	}

	// Meta

	public function get_meta_key() {
		return '_order_currency';
	}

	// Display

	public function get_value( $id ) {
		$value = $this->get_raw_value( $id );

		if ( ! $value ) {
			return $this->get_empty_char();
		}

		return $value;
	}

	// Pro

	public function filtering() {
		return new Filtering\MetaWithoutEmptyOption( $this );
	}

	public function export() {
		return new ACP\Export\Model\RawValue( $this );
	}

	public function sorting() {
		return new ACP\Sorting\Model\Meta( $this );
	}

}