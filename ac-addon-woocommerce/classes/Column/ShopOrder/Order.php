<?php

namespace ACA\WC\Column\ShopOrder;

use AC;
use ACP;
use ACA\WC\Export;

/**
 * @since 2.0
 */
class Order extends AC\Column
	implements ACP\Filtering\Filterable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'order_title' );
		$this->set_original( true );
	}

	public function filtering() {
		return new ACP\Filtering\Model\Post\ID( $this );
	}

	public function export() {
		return new Export\ShopOrder\Order( $this );
	}

}