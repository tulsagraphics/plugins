<?php

namespace ACA\WC\Column\ShopOrder;

use AC;
use ACP;
use ACA\WC\Export;

/**
 * @since 2.0
 */
class OrderNotes extends AC\Column
	implements ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'order_notes' );
		$this->set_original( true );
	}

	public function export() {
		return new Export\ShopOrder\OrderNotes( $this );
	}
}