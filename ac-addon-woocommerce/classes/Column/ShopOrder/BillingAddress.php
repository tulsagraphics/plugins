<?php

namespace ACA\WC\Column\ShopOrder;

use AC;
use ACP;
use ACA\WC\Export;

class BillingAddress extends AC\Column
	implements ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'billing_address' );
		$this->set_original( true );
	}

	public function export() {
		return new Export\ShopOrder\BillingAddress( $this );
	}

}