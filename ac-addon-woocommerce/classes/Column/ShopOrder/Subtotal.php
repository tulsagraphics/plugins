<?php

namespace ACA\WC\Column\ShopOrder;

use AC;
use ACP;

class Subtotal extends AC\Column
	implements ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'column-wc-order-subtotal' );
		$this->set_label( __( 'Subtotal', 'woocommerce' ) );
		$this->set_group( 'woocommerce' );
	}

	// Display

	public function get_value( $post_id ) {
		$order = new \WC_Order( $post_id );

		return $order->get_subtotal_to_display();
	}

	public function export() {
		return new ACP\Export\Model\StrippedValue( $this );
	}

}