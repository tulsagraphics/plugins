<?php

namespace ACA\WC\Column\ProductVariation;

use AC;
use ACP;

/**
 * @since 3.0
 */
class Product extends AC\Column
	implements ACP\Sorting\Sortable {

	public function __construct() {
		$this->set_type( 'variation_product' );
		$this->set_original( true );
	}

	public function get_value( $id ) {
		$product_id = $this->get_raw_value( $id );

		return ac_helper()->html->link( get_edit_post_link( $product_id ) . '#variation_' . $id, get_the_title( $product_id ) );
	}

	public function get_raw_value( $id ) {
		return get_post_field( 'post_parent', $id );
	}

	public function sorting() {
		return new ACP\Sorting\Model\Post\PostParent( $this );
	}

}