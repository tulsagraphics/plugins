<?php

namespace ACA\WC\Column\ProductVariation;

use AC;
use ACA\WC\Editing;
use ACP;

/**
 * @since 3.0
 */
class SKU extends AC\Column\Meta
	implements ACP\Editing\Editable {

	public function __construct() {
		$this->set_type( 'variation_sku' );
		$this->set_label( __( 'SKU', 'woocommerce' ) );
		$this->set_original( true );
	}

	public function get_value( $id ) {
		$variation = new \WC_Product_Variation( $id );

		return $variation->get_sku();
	}

	public function get_meta_key() {
		return '_sku';
	}

	public function editing() {
		return new Editing\ProductVariation\SKU( $this );
	}

}