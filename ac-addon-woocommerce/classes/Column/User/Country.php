<?php

namespace ACA\WC\Column\User;

use ACP;
use ACA\WC\Settings;

/**
 * @since 3.1
 */
class Country extends ACP\Column\Meta
	implements ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'column-wc-user-country' );
		$this->set_label( __( 'Country', 'woocommerce' ) );
		$this->set_group( 'woocommerce' );
	}

	public function get_meta_key() {
		return $this->get_address_type() . '_country';
	}

	public function export() {
		return new ACP\Export\Model\Value( $this );
	}

	private function get_address_type() {
		return $this->get_setting( 'address_type' )->get_value();
	}

	public function register_settings() {
		$this->add_setting( new Settings\User\Country( $this ) );
	}

}