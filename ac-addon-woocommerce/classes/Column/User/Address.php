<?php

namespace ACA\WC\Column\User;

use ACA\WC\Editing;
use ACA\WC\Export;
use ACA\WC\Settings;
use ACP;

/**
 * @since 3.0.4
 */
class Address extends ACP\Column\Meta
	implements ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'column-wc-user-address' );
		$this->set_label( __( 'Address', 'woocommerce' ) );
		$this->set_group( 'woocommerce' );
	}

	public function get_raw_value( $id ) {
		$meta_key = $this->get_meta_key();

		if ( ! $meta_key ) {
			return wc_get_account_formatted_address( $this->get_address_type(), $id );
		}

		return parent::get_raw_value( $id );
	}

	public function get_meta_key() {
		if ( ! $this->get_address_property() ) {
			return false;
		}

		return $this->get_address_type() . '_' . $this->get_address_property();
	}

	public function export() {
		return new ACP\Export\Model\Value( $this );
	}

	public function filtering() {
		if ( ! $this->get_meta_key() ) {
			return new ACP\Filtering\Model\Disabled( $this );
		}

		return new ACP\Filtering\Model\Meta( $this );
	}

	public function sorting() {
		if ( ! $this->get_meta_key() ) {
			return new ACP\Sorting\Model\Disabled( $this );
		}

		return new ACP\Sorting\Model\Meta( $this );
	}

	public function editing() {
		switch ( $this->get_address_property() ) {
			case '' :
				return new ACP\Editing\Model\Disabled( $this );

			case 'country' :
				return new Editing\MetaCountry( $this );

			default :
				return new Editing\User\Address( $this );

		}
	}

	/**
	 * @return string e.g. billing or shipping
	 */
	private function get_address_type() {
		return $this->get_setting( 'address_type' )->get_value();
	}

	/**
	 * @return string e.g. city, country etc.
	 */
	private function get_address_property() {
		return $this->get_setting_address_property()->get_value();
	}

	/**
	 * @return Settings\Address|false
	 */
	public function get_setting_address_property() {
		$setting = $this->get_setting( 'address_property' );

		if ( ! $setting instanceof Settings\Address ) {
			return false;
		}

		return $setting;
	}

	public function register_settings() {
		$this->add_setting( new Settings\User\AddressType( $this ) );
	}

}