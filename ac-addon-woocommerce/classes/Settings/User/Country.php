<?php

namespace ACA\WC\Settings\User;

use AC;

/**
 * @since 3.1
 */
class Country extends AddressType
	implements AC\Settings\FormatValue {

	public function format( $country_code, $original_value ) {
		$countries = WC()->countries->get_countries();

		if ( ! isset( $countries[ $country_code ] ) ) {
			return false;
		}

		return $countries[ $country_code ];
	}

}