<?php

/**
 * @return \ACA\WC\Helper
 */
function ac_addon_wc_helper() {
	return new ACA\WC\Helper();
}

/**
 * @return \ACA\WC\WooCommerce
 */
function ac_addon_wc() {
	return new ACA\WC\WooCommerce( ACA_WC_FILE );
}