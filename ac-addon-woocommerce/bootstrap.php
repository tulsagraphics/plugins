<?php

require_once 'api.php';

AC\Autoloader::instance()->register_prefix( 'ACA\WC', plugin_dir_path( ACA_WC_FILE ) . 'classes/' );

$addon = new ACA\WC\WooCommerce( ACA_WC_FILE );
$addon->register();