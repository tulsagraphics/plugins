<?php
/*
Plugin Name: 		    Admin Columns Pro - WooCommerce
Version: 			    3.1.3
Description: 		    Powerful columns for the WooCommerce Product, Orders and Coupon overview screens
Author: 			    Admin Columns
Author URI: 		    https://www.admincolumns.com
Plugin URI: 		    https://www.admincolumns.com
Text Domain: 		    codepress-admin-columns
WC tested up to:        3.4.3
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! is_admin() ) {
	return;
}

define( 'ACA_WC_FILE', __FILE__ );

require_once 'classes/Dependencies.php';

function aca_wc_init() {
	$dependencies = new ACA_WC_Dependencies( plugin_basename( ACA_WC_FILE ) );
	$dependencies->check_acp( '4.3' );

	if ( ! class_exists( 'WooCommerce', false ) ) {
		$dependencies->add_missing_plugin( 'WooCommerce', $dependencies->get_search_url( 'WooCommerce' ) );
	} elseif ( version_compare( WC()->version, '3.0', '<' ) ) {
		$dependencies->add_missing( __( 'it needs at least WooCommerce version 3.0', 'codepress-admin-columns' ) );
	}

	if ( $dependencies->has_missing() ) {
		return;
	}

	require_once 'bootstrap.php';
}

add_action( 'after_setup_theme', 'aca_wc_init' );