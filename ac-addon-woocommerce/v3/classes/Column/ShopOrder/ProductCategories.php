<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @since NEWVERSION
 */
class ACA_WC_Column_ShopOrder_ProductCategories extends AC_Column
	implements ACP_Export_Column, ACP_Column_FilteringInterface {

	public function __construct() {
		$this->set_group( 'woocommerce' );
		$this->set_type( 'column-wc-product_categories' );
		$this->set_label( __( 'Product Categories', 'codepress-admin-columns' ) );
	}

	// Display

	public function get_value( $order_id ) {
		$terms = ac_helper()->taxonomy->get_term_links( $this->get_raw_value( $order_id ), 'product' );

		if ( empty( $terms ) ) {
			return $this->get_empty_char();
		}

		return ac_helper()->string->enumeration_list( $terms, 'and' );

	}

	public function get_taxonomy() {
		return 'product_cat';
	}

	public function get_raw_value( $order_id ) {
		$categories = array();
		$product_ids = ac_addon_wc_helper()->get_product_ids_by_order( $order_id );

		foreach ( $product_ids as $product_id ) {
			$terms = get_the_terms( $product_id, $this->get_taxonomy() );

			if ( ! $terms || is_wp_error( $terms ) ) {
				continue;
			}

			$categories = array_merge( $categories, $terms );
		}

		$result = array();

		foreach ( $categories as $category ) {
			$result[ $category->term_id ] = $category;
		}

		return $result;
	}

	// PRO

	public function export() {
		return new ACP_Export_Model_StrippedValue( $this );
	}

	public function filtering() {
		return new ACA_WC_Filtering_ShopOrder_ProductCategories( $this );
	}

}
