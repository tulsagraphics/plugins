<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @property ACA_WC_Column_User_Address $column
 */
class ACA_WC_Editing_User_Address extends ACP_Editing_Model_Meta {

	public function __construct( ACA_WC_Column_User_Address $column ) {
		parent::__construct( $column );
	}

	public function get_view_settings() {
		$settings = parent::get_view_settings();

		$settings['placeholder'] = $this->column->get_setting_address_property()->get_address_property_label();

		return $settings;
	}

}
