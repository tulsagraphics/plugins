<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @property ACA_WC_Column_ShopOrder_ProductCategories $column
 */
class ACA_WC_Filtering_ShopOrder_ProductCategories extends ACA_WC_Filtering_ShopOrder {

	public function get_filtering_data() {
		$options = array();

		$terms = get_terms( array(
			'taxonomy' => $this->column->get_taxonomy(),
		) );

		foreach ( $terms as $term ) {
			$options[ $term->term_id ] = $term->name;
		}

		return array(
			'options' => $options,
		);
	}

	public function get_filtering_vars( $vars ) {
		add_filter( 'posts_join', array( $this, 'join_by_order_itemmeta' ), 10, 2 );
		add_filter( 'posts_where', array( $this, 'filter_by_wc_product_ids' ), 10, 2 );

		return $vars;
	}

	public function filter_by_wc_product_ids( $where, WP_Query $query ) {
		if ( $query->is_main_query() ) {

			$alias = $this->get_meta_alias();
			$product_ids = implode( ',', $this->get_products_for_category( $this->get_filter_value() ) );

			$where .= "AND om_{$alias}.meta_value IN ({$product_ids}) AND om_{$alias}.meta_key = '_product_id'";
		}

		return $where;
	}

	private function get_products_for_category( $cat_id ) {
		$products = get_posts( array(
			'post_type'      => 'product',
			'posts_per_page' => -1,
			'fields'         => 'ids',
			'tax_query'      => array(
				array(
					'taxonomy' => 'product_cat',
					'field'    => 'term_id',
					'terms'    => $cat_id,
				),
			),
		) );

		return $products;
	}

}
