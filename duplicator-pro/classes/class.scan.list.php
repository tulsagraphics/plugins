<?php
class DUP_PRO_ScanListChunk
{
    const UNSETTED = "___UNSETTED___";

    public $index;

    public $filePath;

    public $count;

    public $startRange;

    public $endRange;

    public $changed = false;

    public $size = 0;

    private $_delimiter = ";\n";

    public function __construct($index,$filePath,$count,$startRange,$endRange,$size)
    {
        $this->index = $index;
        $this->filePath = $filePath;
        $this->count = $count;
        $this->startRange = $startRange;
        $this->endRange = $endRange;
        $this->size = $size;
    }
}

class DUP_PRO_ScanListHandler{
    public $chunks = array();

    public $currentChunk = null;

    public $chunkList = array();

    public $appendList = array();

    public $appendCount = 0;

    public $chunkSize = 0;

    public $appendListSize = 0;

    private $_delimiter = ";\n";



    public function getByOffset($offset)
    {
//        \DUP_PRO_Log::trace("I'm getting by offset");

        $this->setCurrentChunk($offset);
        return $this->chunkList[$this->convertOffset($offset)];
    }

    public function unsetByOffset($offset)
    {
        $this->setCurrentChunk($offset);
        $this->chunkList[$this->convertOffset($offset)] = DUP_PRO_ScanListChunk::UNSETTED;
        $this->currentChunk->changed = true;
    }

    public function hardUnset($offset)
    {
        $this->setCurrentChunk($offset);
        unset($this->chunkList[$this->convertOffset($offset)]);
        $this->chunkList = array_values($this->chunkList);
        $this->currentChunk->count--;
        $this->currentChunk->endRange--;
        for ($i=$this->currentChunk->index+1;$i<count($this->chunks);$i++){
            $this->chunks[$i]->startRange--;
            $this->chunks[$i]->endRange--;
        }
        $this->currentChunk->changed = true;
    }

    public function setByOffset($offset,$value)
    {
        $this->setCurrentChunk($offset);
        $this->chunkList[$this->convertOffset($offset)] = $value;
        $this->currentChunk->changed = true;
    }

    public function append($value)
    {
        $this->appendList[] = $value;
        $this->appendListSize += strlen($value.$this->_delimiter);
        $lastChunk = end($this->chunks);
        if ($this->appendListSize+$lastChunk->size > $this->chunkSize){
            $this->mergeAppendList();
            $this->createNewChunk();
        }
    }

    public function in_array($value)
    {

        $this->save();
        foreach ($this->chunks as $chunk){
            if($this->currentChunk != $chunk){
                $this->currentChunk = $chunk;
                $this->loadChunk();
            }
            if(in_array($value,$this->chunkList)){
                return true;
            }
        }

        if(in_array($value,$this->appendList)){
            return true;
        }

        return false;
    }

    private function convertOffset($offset)
    {
        return $offset-$this->currentChunk->startRange;
    }

    public function loadChunk()
    {
        $this->chunkList = null;
        $buffer = file_get_contents($this->currentChunk->filePath);
        if($buffer != ''){
            $this->chunkList = explode($this->_delimiter,$buffer);
        }else{
            $this->chunkList = array();
        }
    }

    private function isInCurrentChunk($offset)
    {
        if($this->currentChunk != null){
            return $offset >= $this->currentChunk->startRange && $offset <= $this->currentChunk->endRange;
        }else{
            return false;
        }
    }

    private function setCurrentChunk($offset)
    {
        if(!$this->isInCurrentChunk($offset)){

            $this->save();
            foreach ($this->chunks as $chunk){
                $this->currentChunk = $chunk;
                if($this->isInCurrentChunk($offset)){
                    $this->loadChunk();
                    break;
                }
            }
        }
    }

    public function mergeAppendList()
    {
        if($this->appendListSize > 0){
            $lastChunk = end($this->chunks);
            if($lastChunk->count > 0){
                file_put_contents($lastChunk->filePath,$this->_delimiter.implode($this->_delimiter,$this->appendList),FILE_APPEND);
            }else{
                file_put_contents($lastChunk->filePath,implode($this->_delimiter,$this->appendList));
            }
            $lastChunk->size += $this->appendListSize;
            $lastChunk->count += count($this->appendList);
            $lastChunk->endRange += $lastChunk->count;
            $this->currentChunk = $lastChunk;
            $this->loadChunk();
            $this->appendListSize = 0;
            $this->appendList = array();
        }
    }

    public function createNewChunk()
    {
        $lastChunk = end($this->chunks);
        $index = $lastChunk->index+1;
        $filePath = str_replace("s_".$lastChunk->index.".txt","s_".$index.".txt",$lastChunk->filePath);
        $this->chunks[$index] = new DUP_PRO_ScanListChunk($index,$filePath,0,$lastChunk->endRange+1,$lastChunk->endRange,0);
        file_put_contents($filePath,'');
    }

    public function save()
    {
        if(!empty($this->chunkList) && $this->currentChunk->changed){
            file_put_contents($this->currentChunk->filePath,implode($this->_delimiter,$this->chunkList));
            $this->currentChunk->changed = false;
        }
    }
}

/**
 *
 */

class DUP_PRO_ScanList implements Iterator,ArrayAccess,Countable {

    /**
     * @var string The type of this list either 'file' or 'dir';
     */
    private $_type;

    /**
     * @var string Path to the file holding the list
     */
    private $_filePath;

    /**
     * @var string Path to tmp folder with namehash appended
     */
    private $_basePath;

    /**
     * @var string The delimiter used in the list
     */
    private $_delimiter = ";\n";

    /**
     * @var int Number of item in list
     */
    private $_position = 0;

    /**
     * @var bool|resource File handle of list with a+ rights
     */
    private $_handle;

    /**
     * @var int Offset in list file in bytes
     */
    private $_fileOffset = 0;

    /**
     * @var int Bytes to read from file
     */
    private $_chunkSize = 262144; // 256*1024; // 1MB

    /**
     * @var int Number of items in list
     */
    private $_count = 0;

    /**
     * @var bool If count was changed and there's need to count again
     */
    private $_countChanged = true;

    /**
     * @var array Contains all the metaInfo of the different chunks of list
     */
    private $_listHandler = null;

    private $_isSplit = false;

    private $_autoSave = false;



    public function __construct($scanPath,$type,$chunkSize = 1048576,$autoSave = false)
    {
        $this->_basePath = str_replace("_scan.json","",$scanPath);
        switch ($type){
            case 'file':
                $this->_type = $type;
                $this->_filePath = $this->_basePath."_files.txt";
                break;
            case 'dir':
                $this->_type = $type;
                $this->_filePath = $this->_basePath."_dirs.txt";
                break;
            default:
                $this->_type = "unknown";
                throw new Exception("Unknown scan list type.");
        }

        $this->_handle = fopen($this->_filePath,"a+");
        if($this->_handle === false){
            throw new Exception("Couldn't open list.");
        }
        if(file_exists($scanPath) && $autoSave === true){
            //scan file exists, means this es a rescan which means
            //we have to empty the file/dir list so it won't skip directories
            ftruncate($this->_handle,0);
            unlink($scanPath);
        }
        $this->_listHandler = new DUP_PRO_ScanListHandler();
        $this->_chunkSize = ceil($chunkSize/2);
        $this->_listHandler->chunkSize = ceil($chunkSize/2);
        $this->_autoSave = $autoSave;
        $this->splitList();
    }
   
    public function current()
    {
        return $this->offsetGet($this->_position);
    }

    public function next()
    {
        ++$this->_position;
    }

    public function key()
    {
        return $this->_position;
    }

    public function rewind()
    {
        $this->_position = 0;
    }

    public function valid()
    {
        return $this->offsetExists($this->_position);
    }

    public function offsetExists($offset)
    {
        $this->_listHandler->mergeAppendList();
        return $offset < $this->count();
    }

    public function offsetGet($offset)
    {
        $this->_listHandler->mergeAppendList();
        $offset = (int)$offset;
        if($this->offsetExists($offset)){
            return $this->_listHandler->getByOffset($offset);
        }
    }

    public function offsetSet($offset, $value)
    {
        //Could Have only user listHandler->setByOffset
        //But that would take to long to execute
        //Only use ArrayAccess methods, if no workaround is possible
        if(is_null($offset)){
            $this->_listHandler->append($value);
            $this->_countChanged = true;
        }else{
            $offset = (int)$offset;
            if(is_int($offset)){
                if($this->offsetExists($offset)){
                    $this->_listHandler->mergeAppendList();
                    $this->_listHandler->setByOffset($offset,$value);
                }else if($offset == $this->count()){
                    $this->_listHandler->append($value);
                    $this->_countChanged = true;
                }else{
                    throw new Exception("Illegal offset. Offset is out of allowed range.");
                }
            }else{
                throw new Exception("Only integer offset allowed.");
            }
        }
    }

    public function offsetUnset($offset)
    {
        $this->_listHandler->mergeAppendList();
        $offset = (int)$offset;
        if($this->offsetExists($offset)){
            $this->_listHandler->unsetByOffset($offset);
        }

    }

    public function count()
    {
        $this->_listHandler->mergeAppendList();
        if($this->_countChanged){
            $this->_count = 0;
            foreach ($this->_listHandler->chunks as $chunk){
                $this->_count += $chunk->count;
            }
        }
        $this->_countChanged = false;
        return $this->_count;
    }

    public function has($value)
    {
        return $this->_listHandler->in_array($value);
    }

    public function clearUnsetted()
    {
        $i = 0;
        while($i<$this->count()){
            if($this[$i] == DUP_PRO_ScanListChunk::UNSETTED){
                $this->_countChanged = true;
                $this->_listHandler->hardUnset($i);
                $i--;
            }
            $i++;
        }
    }

    public function combineChunks()
    {
        $this->_listHandler->save();
        $this->_listHandler->mergeAppendList();
        if($this->_autoSave){
            ftruncate($this->_handle,0);
        }
        foreach ($this->_listHandler->chunks as $key=>$chunk){
            if($this->_autoSave){
                $buffer = file_get_contents($chunk->filePath);
                if($key == 0){
                    fwrite($this->_handle,$buffer);
                }else{
                    fwrite($this->_handle,$this->_delimiter.$buffer);
                }
            }
            unlink($chunk->filePath);
            unset($this->_listHandler->chunks[$key]);
            $buffer = null;
        }
    }

    public function splitList()
    {
        rewind($this->_handle);
        $counter = 0;
        while(!feof($this->_handle)){
            $buffer = fread($this->_handle,$this->_chunkSize);
            if($buffer != ''){
                $chunk = explode($this->_delimiter,$buffer);
                if(!feof($this->_handle) && substr($buffer,0,strlen($this->_delimiter)) !== $this->_delimiter){
                    $lineStart = ftell($this->_handle)-strlen($chunk[count($chunk)-1]);
                    fseek($this->_handle,$lineStart);
                    unset($chunk[count($chunk)-1]);
                }
                $filePath = $this->_basePath."_".$this->_type."s_$counter.txt";
                $chunkString = implode($this->_delimiter,$chunk);
                $chunkSize = strlen($chunkString);
                file_put_contents($filePath,$chunkString);
                $count = count($chunk);
                $rangeStart = $counter === 0 ? 0 : $this->_listHandler->chunks[$counter-1]->endRange+1;
                $rangeEnd = $counter === 0 ? $count-1 : $rangeStart+$count-1;
            }else{
                $filePath = $this->_basePath."_".$this->_type."s_$counter.txt";
                file_put_contents($filePath,'');
                $count = 0;
                $rangeStart = $counter === 0 ? 0 : $this->_listHandler->chunks[$counter-1]->endRange+1;
                $rangeEnd = $rangeStart-1;
                $chunkSize = 0;
            }
            $this->_listHandler->chunks[$counter] = new DUP_PRO_ScanListChunk($counter,$filePath,$count,$rangeStart,$rangeEnd,$chunkSize);
            $chunk = null;
            $counter++;
        }
        $this->_listHandler->currentChunk = $this->_listHandler->chunks[0];
        $this->_listHandler->loadChunk();
    }

    public function __destruct()
    {
        $this->combineChunks();
        $this->_handle = fclose($this->_handle);
    }

}