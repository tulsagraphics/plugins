/*
 Administration Javascript for Boilerplate extension
 */
jQuery( document ).ready(function($)
	{

		$( '#mainwp-bl-token-form form' ).live('submit', function() {
			$( this ).find( 'input[type=submit]' ).attr( 'disabled', 'disabled' );
			$( this ).find( '.link-loading' ).css( 'visibility', 'visible' );
			$( this ).find( '.field-error-msg' ).prev( 'br' ).remove();
			$( this ).find( '.error, .field-error-msg' ).remove();
			$( this ).find( '.field-error' ).removeClass( 'field-error' );
			var fields = {};
			fields.token_id = ($( this ).find( 'input[name=token_id]' ).size() > 0) ? $( 'input[name=token_id]' ).val() : '';
			fields.token_name = $( this ).find( 'input[name=token_name]' ).val();
			fields.token_description = $( this ).find( 'input[name=token_description]' ).val();
			fields.action = 'mainwp_boilerplate_save_token';
			var form_o = $( this );
			$.post(ajaxurl, fields, function(data) {
				if (data['success'] == true) {
					mainwp_boilerplate_load_tokens();
				} else {
					if (data['error']) {
						form_o.prepend( '<div class="error"><p>' + data['error'] + '</p></div>' ); }
					if (typeof(data['field_error']['token_name']) != 'undefined') {
						form_o.find( 'input[name=token_name]' ).addClass( 'field-error' ).after( '<br /><span class="field-error-msg">' + data['field_error']['token_name'] + '</span>' ); }
				}
				form_o.find( 'input[type=submit]' ).removeAttr( 'disabled' );
				form_o.find( '.link-loading' ).css( 'visibility', 'hidden' );
			}, 'json');
			return false;
		});

		$( '#btn_managetoken_add_token' ).live('click', function()
			{
			var parent = jQuery( this ).parents( '.managetoken-item' );
			$( '#bpl-managetokens-ajax-message-zone' ).hide();
			$( '#bpl-managetokens-ajax-error-zone' ).hide();
			var errors = [];
			if (parent.find( 'input[name="token_name"]' ).val().trim() == '') {
				errors.push( '<i class="fa fa-exclamation-circle"></i>' + __( 'Error: Token Name can not be empty.' ) ); }

			if (parent.find( 'input[name="token_description"]' ).val().trim() == '') {
				errors.push( '<i class="fa fa-exclamation-circle"></i> ' + __( 'Error: Token Description can not be empty.' ) ); }

			if (errors.length > 0) {
				$( '#bpl-managetokens-ajax-error-zone' ).html( errors.join( '<br />' ) ).show();
				return false;
			}
			var fields = {  token_name: parent.find( 'input[name="token_name"]' ).val(),
				token_description: parent.find( 'input[name="token_description"]' ).val(),
				action: 'mainwp_boilerplate_save_token'
			};
			parent.find( '.mainwp_more_loading' ).show();
			$.post(ajaxurl, fields, function(response) {
				parent.find( '.mainwp_more_loading' ).hide();
				if (response) {
					if (response['success']) {
						$( '#bpl-managetokens-ajax-message-zone' ).html( '<i class="fa fa-check-circle"></i> ' + __( 'New Token added successfully.' ) ).show();
						parent.before( response['row_data'] );
						parent.find( 'input[type=text]' ).val( '' );
					} else {
						if (response['error']) {
							$( '#bpl-managetokens-ajax-error-zone' ).html( response['error'] ).show(); } else {
							$( '#bpl-managetokens-ajax-error-zone' ).html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error.' ).show(); }
					}
				} else {
					$( '#bpl-managetokens-ajax-error-zone' ).html( '<i class="fa fa-exclamation-circle"></i> ' + __( 'Undefined Error.' ) ).show();
				}

			}, 'json');
			return false;
		});

		$( '.managetoken-edit' ).live('click', function()
			{
			var parent = jQuery( this ).closest( '.managetoken-item' );
			parent.find( '.text' ).hide();
			parent.find( '.actions-text' ).hide();
			parent.find( '.input' ).show();
			parent.find( '.actions-input' ).show();
			return false;
		});

		$( '.managetoken-cancel' ).live('click', function()
			{
			var parent = jQuery( this ).closest( '.managetoken-item' );
			parent.find( '.input' ).hide();
			parent.find( '.actions-input' ).hide();
			parent.find( '.token-name input[name=token_name]' ).val( parent.find( '.token-name span.text' ).attr( 'value' ) );
			parent.find( '.token-description input[name=token_description]' ).val( parent.find( '.token-description span.text' ).attr( 'value' ) );
			parent.find( '.text' ).show();
			parent.find( '.actions-text' ).show();
			return false;
		});

		$( '.managetoken-save' ).live('click', function()
			{
			var parent = jQuery( this ).closest( '.managetoken-item' );
			$( '#bpl-managetokens-ajax-message-zone' ).hide();
			$( '#bpl-managetokens-ajax-error-zone' ).hide();
			var errors = [];
			if (parent.find( 'input[name="token_name"]' ).val().trim() == '') {
				errors.push( __( '<i class="fa fa-exclamation-circle"></i> ' + 'Error: Token Name can not be empty.' ) ); }

			if (parent.find( 'input[name="token_description"]' ).val().trim() == '') {
				errors.push( __( '<i class="fa fa-exclamation-circle"></i> ' + 'Error: Token Description can not be empty.' ) ); }

			if (errors.length > 0) {
				$( '#bpl-managetokens-ajax-error-zone' ).html( errors.join( '<br />' ) ).show();
				return false;
			}
			parent.find( '.mainwp_more_loading' ).show();
			var fields = {  token_name: parent.find( 'input[name="token_name"]' ).val(),
				token_description: parent.find( 'input[name="token_description"]' ).val(),
				token_id: parent.attr( 'token_id' ),
				action: 'mainwp_boilerplate_save_token'
			};

			$.post(ajaxurl, fields, function(response) {
				parent.find( '.mainwp_more_loading' ).hide();
				if (response) {
					if (response['success']) {
						if (response['message']) {
							$( '#bpl-managetokens-ajax-message-zone' ).html( response['message'] ).show(); } else {
							$( '#bpl-managetokens-ajax-message-zone' ).html( '<i class="fa fa-check-circle"></i> ' + __( 'Token has been updated successfully' ) ).show(); }
							parent.html( response['row_data'] );
					} else {
						if (response['error']) {
							$( '#bpl-managetokens-ajax-error-zone' ).html( response['error'] ).show(); } else {
							$( '#bpl-managetokens-ajax-error-zone' ).html( '<i class="fa fa-exclamation-circle"></i> ' + 'Undefined Error.' ).show(); }
					}
				} else {
					$( '#bpl-managetokens-ajax-error-zone' ).html( '<i class="fa fa-exclamation-circle"></i> ' + __( 'Undefined Error.' ) ).show();
				}

			}, 'json');

			return false;
		});

		$( '.bpl_posts_list_delete_post' ).bind('click', function()
			{
			if ( ! confirm( __( 'Are you sure?' ) )) {
				return false; }
			var post_id = $( this ).attr( 'bpl-post-id' );
			$( 'tr#post-' + post_id ).find( '.bpl-row-ajax-info' ).html( '<i class="fa fa-spinner fa-pulse"></i>' ).show();
			$.ajax({
				url: ajaxurl,
				data: {
					action: 'mainwp_boilerplate_delete_post',
					post_id: post_id,
				},
				success: function(data) {
					data = $.parseJSON( data );
					if (data && data.success === true) {
						$( 'tr#post-' + post_id ).html( '<td colspan="3"><i class="fa fa-check-circle"></i> Item has been removed.</td>' );
						setTimeout(function() {
							$( 'tr#post-' + post_id ).fadeOut( 1000 );
						}, 2000);
					} else {
						$( 'tr#post-' + post_id ).find( '.bpl-row-ajax-info' ).html( '<i class="fa fa-exclamation-circle"></i> ' + 'Can\'t delete the item.' ).show();
					}
				}, type: 'POST'
			});
			return false;
		});

		$( '.managetoken-delete' ).live('click' ,function(){
			$( '#bpl-managetokens-ajax-message-zone' ).hide();
			$( '#bpl-managetokens-ajax-error-zone' ).hide();
			if (confirm( __( 'Are you sure?' ) )) {
				var parent = $( this ).closest( '.managetoken-item' );
				jQuery.post(ajaxurl, {
					action: 'mainwp_boilerplate_delete_token',
					token_id: parent.attr( 'token_id' )
					}, function(data) {
						if (data && data.success) {
							parent.html( '<td><span class="text">' + '<i class="fa fa-check-circle"></i> ' + __( 'Token has been deleted successfully.' ) + '</span></td>' ).fadeOut( 2000 );
						} else {
							jQuery( '#bpl-managetokens-ajax-message-zone' ).html( '<i class="fa fa-exclamation-circle"></i>' + __( 'Token can not be deleted.' ) ).show();
						}
					}, 'json');
				return false;
			}
			return false;
		});

});


function mainwp_boilerplate_load_tokens()
{
	jQuery( '#bpl_list_tokens' ).html( '<i class="fa fa-spinner fa-pulse"></i> ' );
	jQuery.get(ajaxurl, {
		'action': 'mainwp_boilerplate_load_tokens'
		}, function(data) {
			jQuery( '#bpl_list_tokens' ).html( data );
		});
}

jQuery( document ).ready(function($) {
	jQuery( '.mainwp-show-tut' ).on('click', function(){
		jQuery( '.mainwp-bp-tut' ).hide();
		var num = jQuery( this ).attr( 'number' );
		console.log( num );
		jQuery( '.mainwp-bp-tut[number="' + num + '"]' ).show();
		mainwp_setCookie( 'bp_quick_tut_number', jQuery( this ).attr( 'number' ) );
		return false;
	});

	jQuery( '#mainwp-bp-quick-start-guide' ).on('click', function () {
		if (mainwp_getCookie( 'bp_quick_guide' ) == 'on') {
			mainwp_setCookie( 'bp_quick_guide', '' ); } else {
			mainwp_setCookie( 'bp_quick_guide', 'on' ); }
			bp_showhide_quick_guide();
			return false;
	});
	jQuery( '#mainwp-bp-tips-dismiss' ).on('click', function () {
		mainwp_setCookie( 'bp_quick_guide', '' );
		bp_showhide_quick_guide();
		return false;
	});

	bp_showhide_quick_guide();

	jQuery( '#mainwp-bp-dashboard-tips-dismiss' ).on('click', function () {
		$( this ).closest( '.mainwp_info-box-yellow' ).hide();
		mainwp_setCookie( 'bp_dashboard_notice', 'hide', 2 );
		return false;
	});

});

bp_showhide_quick_guide = function(show, tut) {
	var show = mainwp_getCookie( 'bp_quick_guide' );
	var tut = mainwp_getCookie( 'bp_quick_tut_number' );

	if (show == 'on') {
		jQuery( '#mainwp-bp-tips' ).show();
		jQuery( '#mainwp-bp-quick-start-guide' ).hide();
		bp_showhide_quick_tut();
	} else {
		jQuery( '#mainwp-bp-tips' ).hide();
		jQuery( '#mainwp-bp-quick-start-guide' ).show();
	}

	if ('hide' == mainwp_getCookie( 'bp_dashboard_notice' )) {
		jQuery( '#mainwp-bp-dashboard-tips-dismiss' ).closest( '.mainwp_info-box-yellow' ).hide();
	}
}

bp_showhide_quick_tut = function() {
	var tut = mainwp_getCookie( 'bp_quick_tut_number' );
	jQuery( '.mainwp-bp-tut' ).hide();
	jQuery( '.mainwp-bp-tut[number="' + tut + '"]' ).show();
}

