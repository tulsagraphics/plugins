<?php
global $wpdb;
//$wpdb->query('DELETE FROM ' . $this->table_name('boilerplate_site_token') . " WHERE 1 = 1");
$tokens = Boilerplate_DB::get_instance()->get_tokens();

$site_tokens = array();
if ( $website ) {
	$site_tokens = Boilerplate_DB::get_instance()->get_site_tokens( $website->url ); }

$html = '<div class="mainwp_boilerplate_postbox postbox" section="' . $website->id . '"> 
                    <div class="handlediv"><br /></div> 
                    <h3 class="mainwp_box_title"><span><i class="fa fa-cog"></i> Boilerplate Settings</span></h3>
                    <div class="inside">';
if ( is_array( $tokens ) && count( $tokens ) > 0 ) {
	$html .= '<table class="form-table" style="width: 100%">';
	foreach ( $tokens as $token ) {
		if ( ! $token ) {
			continue; }
		$token_value = '';
		if ( isset( $site_tokens[ $token->id ] ) && $site_tokens[ $token->id ] ) {
			$token_value = htmlspecialchars( stripslashes( $site_tokens[ $token->id ]->token_value ) ); }

		$input_name = 'bpl_value_' . str_replace( array( '.', ' ', '-' ), '_', $token->token_name );
		$html .= '<tr>                      
                <th scope="row" class="token-name" >[' . stripslashes( $token->token_name ) . ']</th>        
                <td>                                        
                <input type="text" value="' . $token_value . '" class="regular-text" name="' . $input_name  . '"/>  
                </td>                           
        </tr>';
	}
	 $html .= '</table>';
} else {
	$html .= __( 'No Tokens Found.','boilerplate-extension' );
}
$html .= '<div class="mainwp_info-box"><strong><b>Note</b>: <em>Add or Edit Boilerplate Tokens in the <a target="_blank" href="' . admin_url( 'admin.php?page=Extensions-Boilerplate-Extension' ) . '">Boilerplate Extension Settings</a></em>.</strong></div>                                    
        </div></div>';
echo $html;
