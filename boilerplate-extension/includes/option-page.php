<div class="wrap mainwpoption" id="mainwp-boilerplate-option">
  <div class="clearfix"></div>
  <?php
	if ( isset( $_REQUEST['message'] ) ) : 
		?>
    <div class="updated">
      <p>
        <?php
        switch ( $_REQUEST['message'] ) {
          case '1':
            _e( 'Your settings have been saved.' );
          break;
          case '2':
            _e( 'Import success' );
          break;
          case '3':
            _e( 'Data is cleared!' );
          break;
          case '-1':
            _e( 'An error occured while trying to save' );
          break;
          case '-2':
            _e( 'Data file empty or error!' );
          /* case '4':
            _e("Do not link saved!"); */
          }
      ?>
    </p>
  </div>
  <?php endif; ?>
  <div class="" id="boilerplate_setting">
    <div id="bpl-managetokens-ajax-error-zone" class="mainwp_info-box-red" style="display:none"></div>

    <div id="bpl-managetokens-ajax-message-zone" class="mainwp_info-box-yellow" style="display:none"></div>
    <div class= "boilerplate_heading"><h3><?php _e( 'Avaiable Tokens','boilerplate-extension' ); ?></h3></div>

    <div id="bpl_list_tokens" class="postbox"></div>

    <div class="mainwp_info-box"><?php _e( 'Custom Token values can be edited in each individual Site\'s Edit page.','boilerplate-extension' ); ?></div>

    <?php require_once $this->plugin_dir .'/includes/boilerplate-list.php'; ?>
  </div>

</div>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    mainwp_boilerplate_load_tokens();
  })
</script>

