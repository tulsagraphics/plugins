<?php

$boilerplate_types = array(
	'bulkpost' => 'Boilerplate Posts',
	'bulkpage' => 'Boilerplate Pages',
);

$titleorder = '';
$orderby = 'post_title';

if ( isset( $_GET['orderby'] ) && isset( $_GET['order'] ) ) {
	if ( ('post_title' == $_GET['orderby']) ) {
		$orderby = 'post_title ' . ('asc' == $_GET['order'] ? 'asc' : 'desc');
		$titleorder = 'asc' == $_GET['order'] ? 'desc' : 'asc';
	}
} else {
	$titleorder = 'desc';
}

foreach ( $boilerplate_types as $type => $title ) {
	$args = array(
	'post_type' => $type,
				'posts_per_page' => -1,
				'post_status' => array( 'publish', 'pending', 'future', 'private' ),
				'post_parent' => null,
				'meta_key'   => '_mainwp_boilerplate',
				'meta_value' => 'yes',
	);
	$boilerplates = get_posts( $args );

	?>
	<div class= "boilerplate_heading"><h3><?php echo $title; ?></h3></div>
	<div>
	<?php
	if ( is_array( $boilerplates ) && count( $boilerplates ) > 0 ) {
		?>                       
		<table cellspacing="0" id="mainwp_posts_table" class="wp-list-table widefat">
			<thead>
				<tr class="tablesorter-headerRow">                                               
					<th class="manage-column sortable <?php echo $titleorder; ?>" scope="col">
						<a href="?page=Extensions-Boilerplate-Extension&orderby=post_title&ordertype=<?php echo $type; ?>&order=<?php echo ( '' == $titleorder ? 'asc' : $titleorder); ?>"><span><?php _e( 'Title','boilerplate-extension' ); ?></span><span class="sorting-indicator"></span></a>
					</th>                        
					<th class="manage-column" scope="col">
						<span><?php _e( 'Slug','boilerplate-extension' ); ?></span>
					</th>                   
					<th class="manage-column " scope="col" >
						<span><?php _e( 'Author','boilerplate-extension' ); ?></span></div>
					</th>                
					<th class="manage-column " scope="col">
						<span><?php _e( 'Date','boilerplate-extension' ); ?></span>
					</th> 
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th class="manage-column sortable <?php echo $titleorder; ?>" scope="col">
						<a href="?page=Extensions-Boilerplate-Extension&orderby=post_title&ordertype=<?php echo $type; ?>&order=<?php echo ('' == $titleorder ? 'asc' : $titleorder); ?>"><span><?php _e( 'Title','boilerplate-extension' ); ?></span><span class="sorting-indicator"></span></a>
					</th>                                                
					<th class="manage-column" scope="col">
						<span><?php _e( 'Slug','boilerplate-extension' ); ?></span>
					</th>
					<th class="manage-column" scope="col">
						<span><?php _e( 'Author','boilerplate-extension' ); ?></span>
					</th>                       
					<th class="manage-column" scope="col" >
						<span><?php _e( 'Date','boilerplate-extension' ); ?></span>
					</th>
				</tr>                       
			</tfoot>
			<tbody class="list:posts" id="the-pages-list">     
				<?php $this->render_boilerplates_table_content( $boilerplates, $type, $orderby ); ?>
                   
			</tbody>                    
            </table>
            <?php
	} else {
		echo '<span>' . __( 'No Saved Boilerplates','boilerplate-extension' ) . '</span>'; }
			$addpage = 'PostBoilerplateAdd';
	$addtitle = __( 'Boilerplate Post','boilerplate-extension' );
	if ( 'bulkpage' == $type ) {
		$addpage = 'PageBoilerplateAdd';
		$addtitle = __( 'Boilerplate Page','boilerplate-extension' );
	}

	?>
	<div class= "boilerplate_add_new_page">
        <a class="button-primary" id="mainwp_boilerplate_add_new_page" name="mainwp_boilerplate_add_new_page" href="<?php echo admin_url( 'admin.php?page='.$addpage ); ?>"><?php echo __( 'Add New ','boilerplate-extension' ) . $addtitle; ?></a>
		</div>  
		<div class="clear"></div>
	</div>   
		<?php
}
