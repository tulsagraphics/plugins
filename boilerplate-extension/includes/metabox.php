<table width="100%">
  <tbody>
    <?php
    $tokens = Boilerplate_DB::get_instance()->get_tokens();
    foreach ( (array) $tokens as $token ) : 
      if ( ! $token ) {
        continue;
      }           
    ?>
    <tr> 
      <td class="token-name"><a class="bpl_post_add_token" style="color: green">[<?php echo stripslashes( $token->token_name );?>]</a></td>
      <td class="token-description"><?php echo stripslashes( $token->token_description ); ?></td>
    </tr>
   <?php
endforeach; 
   ?>
  </tbody>
</table>
<input type="hidden" name="mainwp_boilerplate_nonce" value="<?php echo wp_create_nonce( 'boilerplate_' . $post->ID ); ?>">
<script type="text/javascript">
var lastOne = "title";
jQuery(document).ready(function($) {
  $('#wpadminbar').remove();
  $('input[name="post_title"]').focus(function() {
     lastOne = "title";
});
function bpl_getPos(obj) {
    var pos = 0;
    // IE Support
    if (document.selection) {
        obj.focus ();
        var range = document.selection.createRange ();
        range.moveStart ('character', -obj.value.length);
        pos = range.text.length;
    }
    // Firefox support
    else if (obj.selectionStart || obj.selectionStart == '0')
    pos = obj.selectionStart;
            return (pos);
}
    
$( 'a.bpl_post_add_token' ).on( 'click', function( e ) {   
      var replace_text = jQuery(this).html();      
      if (lastOne == "editor") {            
        tinyMCE.execCommand('mceInsertContent', false, replace_text);
      } else {
        var titleObj = $('input[name="post_title"]');
        var str = titleObj.val();                    
        var pos = bpl_getPos(titleObj[0]); 
        str = str.substring(0,pos) + replace_text + str.substring(pos, str.length)
        titleObj.val(str);
      }        
    });
});
</script>
<?php // put this element at last for css ?>
<p class="hide-if-no-js">
  <a target="_blank" style="text-decoration: none;" href="<?php echo get_site_url() ?>/wp-admin/admin.php?page=Extensions-Boilerplate-Extension"><?php echo '<i class="fa fa-plus-circle"></i> ' . __( 'Create New Token','boilerplate-extension' );?></a>
</p>
    

