<?php
/*
  Plugin Name: Boilerplate Extension
  Plugin URI: https://mainwp.com
  Description: MainWP Boilerplate extension allows you to create, edit and share repetitive pages across your network of child sites.
  Version: 2.9
  Author: MainWP
  Author URI: https://mainwp.com
  Documentation URI: https://mainwp.com/help/category/mainwp-extensions/boilerplate/
 */

if ( ! defined( 'MAINWP_BOILERPLATE_PLUGIN_FILE' ) ) {
	define( 'MAINWP_BOILERPLATE_PLUGIN_FILE', __FILE__ );
}

class BoilerplateExtension {
	public static $instance = null;
	public $plugin_name = 'Boilerplate Extension';
	public $plugin_handle = 'boilerplate-extension';
	public $settings_page_slug = 'Extensions-Boilerplate-Extension';
	protected $plugin_dir;
	protected $plugin_url;
	private $plugin_slug;
	protected $option;
	protected $option_handle = 'boilerplate_extension';
	protected $metaboxes;
	public $buffer = null;

	static function get_instance() {
		if ( null === BoilerplateExtension::$instance ) {
			BoilerplateExtension::$instance = new BoilerplateExtension();
		}
		return BoilerplateExtension::$instance;
	}

	public function __construct() {
		global $wpdb;

		$this->plugin_dir = plugin_dir_path( __FILE__ );
		$this->plugin_url = plugin_dir_url( __FILE__ );
		$this->plugin_slug = plugin_basename( __FILE__ );
		$this->option = get_option( $this->option_handle );

		$this->metaboxes = apply_filters( 'mainwp_getmetaboxes', null );

		new Boilerplate_Page();
		new Boilerplate_Post();

		require_once( ABSPATH . 'wp-includes' . DIRECTORY_SEPARATOR . 'version.php' );
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_filter( 'plugin_row_meta', array( &$this, 'plugin_row_meta' ), 10, 2 );

		add_action( 'mainwp_admin_menu', array( &$this, 'boilerplate_admin_menu' ), 9 );

		add_action( 'save_post', array( &$this, 'save_boilerplate_post' ), 9 ); // default bulkpost acction order is 10
		add_action( 'save_post', array( &$this, 'save_boilerplate_page' ), 9 ); // default bulkpost acction order is 10

		add_action( 'publish_bulkpost', array( &$this, 'publish_boilerplate_post' ), 9 ); // default bulkpost acction order is 10
		add_action( 'publish_bulkpage', array( &$this, 'publish_boilerplate_page' ), 9 ); // default bulkpost acction order is 10

		add_filter( 'mainwp-getsubpages-post', array( Boilerplate_Post::get_instance(), 'get_sub_pages' ) );
		add_filter( 'mainwp-getsubpages-page', array( Boilerplate_Page::get_instance(), 'get_sub_pages' ) );

		add_filter( 'post_updated_messages', array( &$this, 'post_updated_messages' ) );

		// move filter here to support front end
		// add hook to filter content
		add_filter( 'mainwp-pre-posting-posts', array( 'BoilerplateExtension', 'boilerplate_pre_posting_filter_content' ), 8, 2 );
	}


	public function admin_init() {
		wp_enqueue_style( $this->plugin_handle . '-admin-css', $this->plugin_url . 'css/admin.css' );
		if ( isset( $_REQUEST['page'] ) && ('Extensions-Boilerplate-Extension' == $_REQUEST['page'] || 'managesites' == $_REQUEST['page'] ) ) {
			wp_enqueue_script( $this->plugin_handle . '-admin-js', $this->plugin_url . 'js/admin.js' );
		}
		//add_action( 'mainwp-extension-sites-edit', array( &$this, 'boilerplate_site_edit_token' ),9,1 );
		add_action( 'wp_ajax_mainwp_boilerplate_load_tokens', array( &$this, 'load_tokens' ) );
		add_action( 'wp_ajax_mainwp_boilerplate_delete_token', array( &$this, 'delete_token' ) );
		add_action( 'wp_ajax_mainwp_boilerplate_save_token', array( &$this, 'save_token' ) );
		add_action( 'wp_ajax_mainwp_boilerplate_delete_site_post', array( &$this, 'boilerplate_delete_site_post' ) );

		add_action( 'mainwp_update_site', array( &$this, 'boilerplate_update_site_tokens' ), 8, 1 );
		add_action( 'mainwp_delete_site', array( &$this, 'boilerplate_delete_site_tokens' ), 8, 1 );
		// add hook to set post id for posts to site
		add_filter( 'mainwp-pre-posting-posts', array( &$this, 'boilerplate_pre_posting_set_post_id' ), 9, 2 );
		add_action( 'wp_ajax_mainwp_boilerplate_delete_post', array( &$this, 'boilerplate_delete_post' ) );
		// to support dripp boilerplate
		add_action( 'mainwp_dripper_update_post_meta', array( &$this, 'boilerplate_update_post_meta' ), 10, 2 );

		if ( isset( $_GET['boilerplate'] ) && 1 == $_GET['boilerplate'] ) {
			add_meta_box( 'mainwp-boilerplate-metabox', __( 'Boilerplate Tokens', 'mainwp' ), array( &$this, 'boilerplate_metabox' ), 'bulkpost', 'side', 'default' );
			add_meta_box( 'mainwp-boilerplate-metabox', __( 'Boilerplate Tokens', 'mainwp' ), array( &$this, 'boilerplate_metabox' ), 'bulkpage', 'side', 'default' );
		}

		add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_before_init' ) );
	}


	function table_name( $suffix ) {
		return Boilerplate_DB::get_instance()->table_name( $suffix );
	}

	public static function get_file_name() {

		return __FILE__;
	}

	public function plugin_row_meta( $plugin_meta, $plugin_file ) {

		if ( $this->plugin_slug != $plugin_file ) { return $plugin_meta; }

		$slug = basename($plugin_file, ".php");
		$api_data = get_option( $slug. '_APIManAdder');
		if (!is_array($api_data) || !isset($api_data['activated_key']) || $api_data['activated_key'] != 'Activated' || !isset($api_data['api_key']) || empty($api_data['api_key']) ) {
			return $plugin_meta;
		}

		$plugin_meta[] = '<a href="?do=checkUpgrade" title="Check for updates.">' . __( 'Check for updates now','boilerplate-extension' ) . '</a>';
		return $plugin_meta;
	}

	function post_updated_messages( $messages ) {

		$messages['post'][90] = __( 'Boilerplate saved. You have to select the sites you wish to publish to.','boilerplate-extension' );
		return $messages;
	}

	function boilerplate_admin_menu() {
		Boilerplate_Page::init_menu();
		Boilerplate_Post::init_menu();
	}

	function save_boilerplate_post( $post_id ) {
		$post = get_post( $post_id );
		if ( 'bulkpost' != $post->post_type && ( ! isset( $_POST['post_type'] ) || ('bulkpost' != $_POST['post_type'])) ) { return; }

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return; }

		if ( ! isset( $_POST['mainwp_boilerplate_nonce'] ) || ! wp_verify_nonce( $_POST['mainwp_boilerplate_nonce'], 'boilerplate_' . $post_id ) ) {
			return;
		}

		if ( isset( $_POST['mainwp_wpseo_metabox_save_values'] ) && ( ! empty( $_POST['mainwp_wpseo_metabox_save_values'] )) ) {
			return; }

		$out = 0;
		do_action( 'mainwp_bulkpost_metabox_handle', $post_id, $out );
		$this->boilerplate_metabox_handle( $post_id, 'bulkpost' );

		$message_id = 90;

		if ( $out == $post_id ) {
			/** @var $wpdb wpdb */
			global $wpdb;
			$wpdb->update( $wpdb->posts, array( 'post_status' => 'draft' ), array( 'ID' => $post_id ) );
			add_filter( 'redirect_post_location', create_function( '$location', 'return esc_url_raw(add_query_arg(array("message" => "' . $message_id . '", "hideall" => 1, "boilerplate" => 1), $location));' ) );
		} else {
			//Redirect to handle page! (to actually post the messages)
			wp_redirect( get_site_url() . '/wp-admin/admin.php?page=PostingBoilerplatePost&boilerplate=1&hideall=1&id=' . $post_id );
			die();
		}

	}

	function publish_boilerplate_post( $post_id ) {

		// check boilerplate nonce when publish
		if ( ! isset( $_POST['mainwp_boilerplate_nonce'] ) || ! wp_verify_nonce( $_POST['mainwp_boilerplate_nonce'], 'boilerplate_' . $post_id ) ) {
			return;
		}

		if ( isset( $_POST['mainwp_wpseo_metabox_save_values'] ) && ( ! empty( $_POST['mainwp_wpseo_metabox_save_values'] )) ) {
			return; }

		$out = 0;
		do_action( 'mainwp_bulkpost_metabox_handle', $post_id, $out );

		$this->boilerplate_metabox_handle( $post_id, 'bulkpost' );

		$message_id = 90;

		if ( $out == $post_id ) {
			/** @var $wpdb wpdb */
			global $wpdb;
			$wpdb->update( $wpdb->posts, array( 'post_status' => 'draft' ), array( 'ID' => $post_id ) );
			add_filter( 'redirect_post_location', create_function( '$location', 'return esc_url_raw(add_query_arg(array("message" => "' . $message_id .'", "hideall" => 1, "boilerplate" => 1), $location));' ) );
		} else {
			//Redirect to handle page! (to actually post the messages)
			wp_redirect( get_site_url() . '/wp-admin/admin.php?page=PostingBoilerplatePost&boilerplate=1&hideall=1&id=' . $post_id );
			die();
		}
	}

	function save_boilerplate_page( $post_id ) {

		$post = get_post( $post_id );
		if ( 'bulkpage' != $post->post_type && ( ! isset( $_POST['post_type'] ) || ('bulkpage' != $_POST['post_type'])) ) { return; }

		// verify if this is an auto save routine.
		// If it is our form has not been submitted, so we dont want to do anything
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return; }

		if ( ! isset( $_POST['mainwp_boilerplate_nonce'] ) || ! wp_verify_nonce( $_POST['mainwp_boilerplate_nonce'], 'boilerplate_' . $post_id ) ) {
			return;
		}

		if ( isset( $_POST['mainwp_wpseo_metabox_save_values'] ) && ( ! empty( $_POST['mainwp_wpseo_metabox_save_values'] )) ) {
			return; }

		$message_id = 90;
		$out = 0;
		do_action( 'mainwp_bulkpage_metabox_handle', $post_id, $out );

		//Read extra metabox
		$this->boilerplate_metabox_handle( $post_id, 'bulkpage' );

		if ( $out == $post_id ) {
			/** @var $wpdb wpdb */
			global $wpdb;
			$wpdb->update( $wpdb->posts, array( 'post_status' => 'draft' ), array( 'ID' => $post_id ) );
			add_filter( 'redirect_post_location', create_function( '$location', 'return esc_url_raw(add_query_arg(array("message" => "' . $message_id . '", "hideall" => 1, "boilerplate" => 1), $location));' ) );
		} else {
			//Redirect to handle page! (to actually post the messages)
			wp_redirect( get_site_url() . '/wp-admin/admin.php?page=PostingBoilerplatePage&boilerplate=1&hideall=1&id=' . $post_id );
			die();
		}

	}

	function publish_boilerplate_page( $post_id ) {

		// check boilerplate nonce when publish
		if ( ! isset( $_POST['mainwp_boilerplate_nonce'] ) || ! wp_verify_nonce( $_POST['mainwp_boilerplate_nonce'], 'boilerplate_' . $post_id ) ) {
			return;
		}

		if ( isset( $_POST['mainwp_wpseo_metabox_save_values'] ) && ( ! empty( $_POST['mainwp_wpseo_metabox_save_values'] )) ) {
			return; }

		$out = 0;
		do_action( 'mainwp_bulkpage_metabox_handle', $post_id, $out );

		//Read extra metabox

		$this->boilerplate_metabox_handle( $post_id, 'bulkpage' );

		$message_id = 90;

		if ( $out == $post_id ) {
			/** @var $wpdb wpdb */
			global $wpdb;
			$wpdb->update( $wpdb->posts, array( 'post_status' => 'draft' ), array( 'ID' => $post_id ) );
			add_filter( 'redirect_post_location', create_function( '$location', 'return esc_url_raw(add_query_arg(array("message" => "' . $message_id . '", "hideall" => 1, "boilerplate" => 1), $location));' ) );
		} else {
			$this->metaboxes->add_slug_handle( $post_id, 'bulkpage' );

			//Redirect to handle page! (to actually post the messages)
			wp_redirect( get_site_url() . '/wp-admin/admin.php?page=PostingBoilerplatePage&boilerplate=1&hideall=1&id=' . $post_id );
			die();
		}

	}

	//    function save_boilerplate_post($post_id) {
	//        $post = get_post($post_id);
	//        if ($post->post_type != 'bulkpost' && (!isset($_POST['post_type']) || ($_POST['post_type'] != 'bulkpost'))) return;
	//
	//        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
	//            return;
	//
	//        $this->boilerplate_metabox_handle($post_id, 'bulkpost');
	//    }
	//
	//    function save_boilerplate_page($post_id) {
	//        $post = get_post($post_id);
	//        if ($post->post_type != 'bulkpage' && (!isset($_POST['post_type']) || ($_POST['post_type'] != 'bulkpage'))) return;
	//
	//        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
	//            return;
	//        $this->boilerplate_metabox_handle($post_id, 'bulkpage');
	//    }
	//
	//    function publish_boilerplate_post($post_id) {
	//        $this->boilerplate_metabox_handle($post_id, 'bulkpost');
	//    }
	//
	//    function publish_boilerplate_page($post_id) {
	//        $this->boilerplate_metabox_handle($post_id, 'bulkpage');
	//    }

	function boilerplate_update_post_meta( $post, $post_type ) {
		if ( ! $post ) {
			return; }
		if ( $post->post_type == $post_type ) {
			update_post_meta( $post->ID, '_mainwp_boilerplate', 'yes' );
		}
	}

	function boilerplate_metabox_handle( $post_id, $post_type ) {

		if ( ! isset( $_POST['mainwp_boilerplate_nonce'] ) || ! wp_verify_nonce( $_POST['mainwp_boilerplate_nonce'], 'boilerplate_' . $post_id ) ) {
			return;
		}
		// verify if this is an auto save routine. If it is our form has not been submitted, so we dont want to do anything
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return; }

		// Check permissions
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return; }

		$post = get_post( $post_id );
		if ( $post->post_type == $post_type ) {
			update_post_meta( $post_id, '_mainwp_boilerplate', 'yes' );
			return;
		}
		return;
	}

	// This function calculate what post is create new, updated or will delete
	function bulkposting_posting_done_results( $post, $dbwebsites, $output ) {

		if ( ! $post ) {
			return; }

		if ( ! $post || ($post->post_type != 'bulkpost' && $post->post_type != 'bulkpage') ) {
			return; }

		if ( get_post_meta( $post->ID, '_mainwp_boilerplate', true ) != 'yes' ) {
			return; }

		$sites_posts = get_post_meta( $post->ID, '_mainwp_boilerplate_sites_posts', true );

		//        echo "<pre>";
		//        print_r($sites_posts);
		//        echo "</pre>";

		$just_posts = $previous_posts = $new_posts = $updated_posts = $delete_posts = array();

		if ( is_array( $sites_posts ) && isset( $sites_posts['delete_posts'] ) && is_array( $sites_posts['delete_posts'] ) ) {
			$delete_posts = $sites_posts['delete_posts']; }

		if ( is_array( $sites_posts ) && isset( $sites_posts['previous_posts'] ) ) {
			$previous_posts = $sites_posts['previous_posts'];
		}

		if ( is_array( $dbwebsites ) ) {
			foreach ( $dbwebsites as $website ) {
				if ( isset( $output->ok[ $website->id ] ) && ($output->ok[ $website->id ] == 1) && (isset( $output->added_id[ $website->id ] )) ) {
					$just_posts[ $website->id ] = array( 'post_id' => $output->added_id[ $website->id ], 'link' => $output->link[ $website->id ] );
				}
			}
		}

		if ( is_array( $previous_posts ) ) {
			foreach ( $previous_posts as $site_id => $_post ) {
				if ( isset( $just_posts[ $site_id ] ) && $just_posts[ $site_id ]['post_id'] == $_post['post_id'] ) {
					$updated_posts[ $site_id ] = $just_posts[ $site_id ];
				} else if ( ! isset( $just_posts[ $site_id ] ) ) {
					$delete_posts[ $site_id ] = $previous_posts[ $site_id ];
				}
			}
		}
		if ( is_array( $just_posts ) && is_array( $previous_posts ) ) {
			foreach ( $just_posts as $site_id => $_post ) {
				if ( ! isset( $previous_posts[ $site_id ] ) || ( isset( $previous_posts[ $site_id ] ) && $previous_posts[ $site_id ]['post_id'] != $_post['post_id']) ) {
					$new_posts[ $site_id ] = $_post;
					if ( isset( $delete_posts[ $site_id ] ) ) {
						unset( $delete_posts[ $site_id ] ); // just posted so remove from delete list if existed
					}
				}
			}
		}

		if ( ! is_array( $previous_posts ) || count( $previous_posts ) <= 0 ) {
			$new_posts = $just_posts;
		}
		$sites_posts = array();
		$sites_posts['previous_posts'] = $just_posts;
		$sites_posts['new_posts'] = $new_posts;
		$sites_posts['updated_posts'] = $updated_posts;
		$sites_posts['delete_posts'] = $delete_posts;
		update_post_meta( $post->ID, '_mainwp_boilerplate_sites_posts', $sites_posts );
	}


	public static function boilerplate_pre_posting_filter_content( $params, $website ) {
		global $wpdb;
		$new_post = unserialize( base64_decode( $params['new_post'] ) );
		if ( isset( $new_post['mainwp_post_id'] ) && $post_id = $new_post['mainwp_post_id'] ) {
			if ( 'yes' == get_post_meta( $post_id, '_mainwp_boilerplate', true ) ) {
				$tokens = Boilerplate_DB::get_instance()->get_tokens();
				$site_tokens = Boilerplate_DB::get_instance()->get_site_tokens( $website->url );
				$search_tokens = $replace_values = array();
				foreach ( $tokens as $token ) {
					$search_tokens[] = '[' . $token->token_name . ']';
					$replace_values[] = isset( $site_tokens[ $token->id ] ) ? $site_tokens[ $token->id ]->token_value : '';
				}
				$new_post['post_content'] = str_replace( $search_tokens, $replace_values, $new_post['post_content'] );
				$new_post['post_title'] = str_replace( $search_tokens, $replace_values, $new_post['post_title'] );
				$params['new_post'] = base64_encode( serialize( $new_post ) );
			}
		}
		return $params;
	}

	public function boilerplate_pre_posting_set_post_id( $params, $website ) {

		$new_post = unserialize( base64_decode( $params['new_post'] ) );
		if ( isset( $new_post['mainwp_post_id'] ) && $post_id = $new_post['mainwp_post_id'] ) {
			if ( 'yes' == get_post_meta( $post_id, '_mainwp_boilerplate', true ) ) {
				$sites_posts = get_post_meta( $post_id, '_mainwp_boilerplate_sites_posts', true );
				if ( is_array( $sites_posts ) && isset( $sites_posts['previous_posts'] ) ) {
					$previous_posts = $sites_posts['previous_posts'];
					if ( is_array( $previous_posts ) ) {
						if ( isset( $previous_posts[ $website->id ] ) && isset( $previous_posts[ $website->id ]['post_id'] ) && $previous_posts[ $website->id ]['post_id'] ) {
							// set ID to update post on child site
							$new_post['ID'] = $previous_posts[ $website->id ]['post_id'];
						}
					}
				}
			}
		}
		$params['new_post'] = base64_encode( serialize( $new_post ) );
		return $params;
	}

//	public function boilerplate_site_edit_token( $website ) {
//
//	}

	public static function renderSiteTokenValues( $post, $metabox ) {
		global $mainWPBoilerplateExtensionActivator;

		$websiteid = isset($metabox['args']['websiteid']) ? $metabox['args']['websiteid'] : null;
		$website = apply_filters( 'mainwp-getsites', $mainWPBoilerplateExtensionActivator->get_child_file(), $mainWPBoilerplateExtensionActivator->get_child_key(), $websiteid );

		if ( $website && is_array( $website ) ) {
			$website = current( $website );
		}

		if ( empty( $website ) )
			return;

		$tokens = Boilerplate_DB::get_instance()->get_tokens();

		$site_tokens = array();
		if ( $website ) {
			$site_tokens = Boilerplate_DB::get_instance()->get_site_tokens( $website['url'] ); }

		if ( is_array( $tokens ) && count( $tokens ) > 0 ) {
			$html .= '<table class="form-table" style="width: 100%">';
			foreach ( $tokens as $token ) {
				if ( ! $token ) {
					continue; }
				$token_value = '';
				if ( isset( $site_tokens[ $token->id ] ) && $site_tokens[ $token->id ] ) {
					$token_value = htmlspecialchars( stripslashes( $site_tokens[ $token->id ]->token_value ) ); }

				$input_name = 'bpl_value_' . str_replace( array( '.', ' ', '-' ), '_', $token->token_name );
				$html .= '<tr>
						<th scope="row" class="token-name" >[' . stripslashes( $token->token_name ) . ']</th>
						<td>
						<input type="text" value="' . $token_value . '" class="regular-text" name="' . $input_name  . '"/>
						</td>
				</tr>';
			}
			 $html .= '</table>';
		} else {
			$html .= __( 'No Tokens Found.','boilerplate-extension' );
		}
		$html .= '<div class="mainwp_info-box"><strong><b>Note</b>: <em>Add or Edit Boilerplate Tokens in the <a target="_blank" href="' . admin_url( 'admin.php?page=Extensions-Boilerplate-Extension' ) . '">Boilerplate Extension Settings</a></em>.</strong></div>';
		echo $html;

	}

	public function format_post_status( $status ) {

		if ( 'publish' == $status ) {
			return 'Published';
		} else if ( 'Future' == $status ) {
			return 'Scheduled';
		}
		return ucfirst( $status );
	}

	public function boilerplate_update_site_tokens( $websiteId ) {

		global $wpdb, $mainWPBoilerplateExtensionActivator;
		if ( isset( $_POST['submit'] ) ) {
			$website = apply_filters( 'mainwp-getsites', $mainWPBoilerplateExtensionActivator->get_child_file(), $mainWPBoilerplateExtensionActivator->get_child_key(), $websiteId );
			if ( $website && is_array( $website ) ) {
				$website = current( $website );
			}

			if ( ! $website ) {
				return; }

			$tokens = Boilerplate_DB::get_instance()->get_tokens();
			foreach ( $tokens as $token ) {
				$input_name = 'bpl_value_' . str_replace( array( '.', ' ', '-' ), '_', $token->token_name );
				if ( isset( $_POST[ $input_name ] ) ) {
					$token_value = $_POST[ $input_name ];

					// default token
					//                    if ($token->type == 1 && empty($token_value))
					//                        continue;

					$current = Boilerplate_DB::get_instance()->get_tokens_by( 'id', $token->id, $website['url'] );
					if ( $current ) {
						Boilerplate_DB::get_instance()->update_token_site( $token->id, $token_value, $website['url'] );
					} else {
						Boilerplate_DB::get_instance()->add_token_site( $token->id, $token_value, $website['url'] );
					}
				}
			}
		}
	}

	public function boilerplate_delete_site_tokens( $website ) {

		if ( $website ) {
			Boilerplate_DB::get_instance()->delete_site_tokens( $website->url );
		}
	}

	public function settings_page() {
		global $wpdb;
		include $this->plugin_dir . '/includes/option-page.php';
	}

	public function get_option( $key, $default = '' ) {
		if ( isset( $this->option[ $key ] ) ) {
			return $this->option[ $key ]; }
		return $default;
	}

	public function set_option( $key, $value ) {
		$this->option[ $key ] = $value;
		return update_option( $this->option_handle, $this->option );
	}

	public function boilerplate_metabox() {
		global $wpdb, $post;
		include( $this->plugin_dir . '/includes/metabox.php' );
	}

	public function tiny_mce_before_init( $initArray ) {

		global $post;

		if ( isset( $_GET['boilerplate'] ) && 1 == $_GET['boilerplate'] && ('bulkpost' == $post->post_type || 'bulkpage' == $post->post_type) ) {
			$initArray['setup'] = <<<JS
function(ed) {
    ed.onInit.add(
        function(ed) {
            ed.on('focus', function() {
                lastOne = "editor";
            });
        }
    );
}
JS;
		}

		return $initArray;
	}

	public function boilerplate_delete_post() {
		global $wpdb;
		$post_id = intval( $_REQUEST['post_id'] );
		$ret = array( 'success' => false );
		if ( $post_id && wp_delete_post( $post_id ) ) {
			$ret['success'] = true; }
		echo json_encode( $ret );
		exit;
	}

	public function render_boilerplates_table_content( $boilerplates, $type, $orderby ) {
		$page_slug = 'PostBoilerplateEdit';
		if ( 'bulkpage' == $type ) {
			$page_slug = 'PageBoilerplateEdit'; }

		$date_format = get_option( 'date_format' );
		$time_format = get_option( 'time_format' );

		foreach ( $boilerplates as $boilerplate ) {
				   $author_info = get_userdata( $boilerplate->post_author );

					?>
                    <tr valign="top" id="post-<?php echo $boilerplate->ID; ?>">
					   <td class="post-title page-title column-title">
						   <input type="hidden" value="<?php echo get_edit_post_link( $boilerplate->ID, true ); ?>" name="id" class="postId">
						   <strong>
							   <abbr title="<?php echo $boilerplate->post_title; ?>">
								   <a title="Edit <?php echo $boilerplate->post_title; ?>" href="?page=<?php echo $page_slug; ?>&post_id=<?php echo $boilerplate->ID; ?>" class="row-title"><?php echo $boilerplate->post_title; ?></a>
							   </abbr>
						   </strong>
						   <div class="row-actions">
							   <span class="edit">
								   <a title="Edit this item" href="?page=<?php echo $page_slug; ?>&post_id=<?php echo $boilerplate->ID; ?>"><i class="fa fa-pencil-square-o"></i> <?php _e( 'Edit','boilerplate-extension' ); ?></a>
							   </span>
							   <span class="delete">
								   | <a href="#" title="Delete this item" class="bpl_posts_list_delete_post" bpl-post-id=<?php echo $boilerplate->ID; ?>><i class="fa fa-trash-o"></i> <?php _e( 'Delete','boilerplate-extension' ); ?></a>
							   </span>
						   </div>
						   <span class="bpl-row-ajax-info hidden"></span>
					   </td>
					   <td class="tags column-tags"><?php echo $boilerplate->post_name; ?></td>

					   <td class="author column-author">
						   <span><?php echo $author_info->user_login; ?></span>
					   </td>
					   <td class="date column-date">
							<?php $post_time = strtotime( $boilerplate->post_date ); ?>
						   <abbr title="<?php echo $boilerplate->post_date; ?>"><?php echo date( $date_format, $post_time ) . ' ' . date( $time_format, $post_time );  ?></abbr>
						   <br><?php echo $this->format_post_status( $boilerplate->post_status ); ?>
					   </td>
                    </tr>
        <?php };
	}

	public function load_tokens() {
		$tokens = Boilerplate_DB::get_instance()->get_tokens();
		?>
        <div class="boilerplate_listtokens">
        <table width="100%">
            <tbody>
                <?php
				if ( is_array( $tokens ) && count( $tokens ) > 0 ) {
					foreach ( (array) $tokens as $token ) {
						if ( ! $token ) {
								continue; }
						echo $this->create_token_item( $token );
					}
				}
				?>
                <tr class= "managetoken-item bpl_add_token">
                     <td class="token-name">
                        <span class="actions-input input"><input type="text" value=""  name="token_name" placeholder="<?php _e( 'Enter a Token','boilerplate-extension' ); ?>"></span>
                    </td>
                    <td class="token-description">
                        <span class="actions-input input">
                            <input type="text" value="" class="token_description" name="token_description" placeholder="<?php _e( 'Enter a Token Description','boilerplate-extension' ); ?>">
                        </span>
                        <span class="mainwp_more_loading"><i class="fa fa-spinner fa-pulse"></i></span>
                    </td>
                    <td class="token-option"><input type="button" value="<?php _e( 'Save','boilerplate-extension' ); ?>" class="button-primary right" id="btn_managetoken_add_token"></td>
                </tr>

            </tbody>
        </table>
        </div>
			<?php
			exit;
	}

	private function create_token_item( $token, $with_tr = true ) {

		$colspan = $html = '';
		if ( $token->type == 1 ) {
			$colspan = ' colspan="2" '; }
		if ( $with_tr ) {
			$html = '<tr class="managetoken-item" token_id="' . $token->id . '">'; }

		$html .= '<td class="token-name">
                    <span class="text" ' . (($token->type == 1) ? '' : 'value="' . $token->token_name) . '">[' . stripslashes( $token->token_name ) . ']</span>' .
						(($token->type == 1) ? '' : '<span class="input hidden"><input type="text" value="' . htmlspecialchars( stripslashes( $token->token_name ) ) . '" name="token_name"></span>') .
				'</td>
                <td class="token-description" ' . $colspan . '>
                    <span class="text" ' . (($token->type == 1) ? '' : 'value="' . stripslashes( $token->token_description )) . '">' . stripslashes( $token->token_description ) . '</span>';
		if ( $token->type != 1 ) {
			$html .= '<span class="input hidden"><input type="text" value="' . htmlspecialchars( stripslashes( $token->token_description ) ) . '" name="token_description"></span>
                        <span class="mainwp_more_loading"><i class="fa fa-spinner fa-pulse"></i></span>';
		}
		$html .= '</td>';

		if ( $token->type == 0 ) {
			$html .= '<td class="token-option">
                    <span class="mainwp_group-actions actions-text" ><a class="managetoken-edit" href="#"><i class="fa fa-pencil-square-o"></i> ' .__( 'Edit','mainwp' ) .'</a> | <a class="managetoken-delete" href="#"><i class="fa fa-trash-o"></i> ' . __( 'Delete','mainwp' ) . '</a></span>
                    <span class="mainwp_group-actions actions-input hidden" ><a class="managetoken-save" href="#"><i class="fa fa-floppy-o"></i> ' . __( 'Save','mainwp' ) . '</a> | <a class="managetoken-cancel" href="#"><i class="fa fa-times-circle"></i> ' . __( 'Cancel','mainwp' ) . '</a></span>
                </td>';
		}
		if ( $with_tr ) {
			$html .= '</tr>'; }

		return $html;
	}

	public function delete_token() {
		global $wpdb;
		$ret = array( 'success' => false );
		$token_id = intval( $_POST['token_id'] );
		if ( Boilerplate_DB::get_instance()->delete_token_by( 'id', $token_id ) ) {
			$ret['success'] = true;
		}
		echo json_encode( $ret );
		exit;
	}

	public function boilerplate_delete_site_post() {
		global $wpdb, $mainWPBoilerplateExtensionActivator;
		$ret = array( 'status' => false );
		$site_id = intval( $_POST['site_id'] );
		$post_id = intval( $_POST['post_id'] );
		$boilerplate_id = intval( $_POST['boilerplate_id'] );

		if ( $site_id && $post_id ) {
			$post_data = array( 'id' => $post_id, 'action' => 'delete' );
			$ret = apply_filters( 'mainwp_fetchurlauthed', $mainWPBoilerplateExtensionActivator->get_child_file(), $mainWPBoilerplateExtensionActivator->get_child_key(), $site_id, 'post_action', $post_data );
			if ( $boilerplate_id && is_array( $ret ) && isset( $ret['status'] ) && 'SUCCESS' == $ret['status'] ) {
				$sites_posts = get_post_meta( $boilerplate_id, '_mainwp_boilerplate_sites_posts', true );
				if ( isset( $sites_posts['delete_posts'] ) && isset( $sites_posts['delete_posts'][ $site_id ] ) && isset( $sites_posts['delete_posts'][ $site_id ]['post_id'] ) && $sites_posts['delete_posts'][ $site_id ]['post_id'] == $post_id ) {
						unset( $sites_posts['delete_posts'][ $site_id ] );
						update_post_meta( $boilerplate_id, '_mainwp_boilerplate_sites_posts', $sites_posts );
				}
			}
		}
		echo json_encode( $ret );
		exit;
	}

	public function save_token() {
		global $wpdb;
		$return = array( 'success' => false, 'error' => '', 'message' => '' );
		$token_name = sanitize_text_field( $_POST['token_name'] );
		$token_description = sanitize_text_field( $_POST['token_description'] );

		// update
		if ( isset( $_POST['token_id'] ) && $token_id = intval( $_POST['token_id'] ) ) {
			$current = Boilerplate_DB::get_instance()->get_tokens_by( 'id', $token_id );
			if ( $current && $current->token_name == $token_name && $current->token_description == $token_description ) {
				$return['success'] = true;
				$return['message'] = __( 'The token does not change.' );
				$return['row_data'] = $this->create_token_item( $current, false );
			} else if ( ($current = Boilerplate_DB::get_instance()->get_tokens_by( 'token_name', $token_name )) && $current->id != $token_id ) {
				$return['error'] = __( 'The token name existed.' );
			} else if ( $token = Boilerplate_DB::get_instance()->update_token( $token_id, array( 'token_name' => $token_name, 'token_description' => $token_description ) ) ) {
				$return['success'] = true;
				$return['row_data'] = $this->create_token_item( $token, false );
			}
		} else { // add new
			if ( $current = Boilerplate_DB::get_instance()->get_tokens_by( 'token_name', $token_name ) ) {
				$return['error'] = __( 'The token name existed.' );
			} else {
				if ( $token = Boilerplate_DB::get_instance()->add_token( array( 'token_name' => $token_name, 'token_description' => $token_description, 'type' => 0 ) ) ) {
					$return['success'] = true;
					$return['row_data'] = $this->create_token_item( $token );
				} else {
					$return['error'] = __( 'Error: Add token failed.' ); }
			}
		}
		echo json_encode( $return );
		exit;
	}
}

function boilerplate_autoload( $class_name ) {
	$class_name = str_replace( '_', '-', strtolower( $class_name ) );
	//General system classes
	$class_file = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . str_replace( basename( __FILE__ ), '', plugin_basename( __FILE__ ) ) . 'class' . DIRECTORY_SEPARATOR . $class_name . '.class.php';
	if ( file_exists( $class_file ) ) {
		require_once( $class_file );
	}
}

if ( function_exists( 'spl_autoload_register' ) ) {
	spl_autoload_register( 'boilerplate_autoload' );
} else {
	function __autoload( $class_name ) {
		boilerplate_autoload( $class_name );
	}
}


function boilerplate_activate() {
	update_option( 'mainwp_boilerplate_activate_activated', 'yes' );
	Boilerplate_DB::get_instance()->install();
	$extensionActivator = new BoilerplateExtensionActivator();
	$extensionActivator->activate();
}

function boilerplate_deactivate() {
	$extensionActivator = new BoilerplateExtensionActivator();
	$extensionActivator->deactivate();
}

register_activation_hook( __FILE__, 'boilerplate_activate' );
register_deactivation_hook( __FILE__, 'boilerplate_deactivate' );

class BoilerplateExtensionActivator {
	protected $mainwpMainActivated = false;
	protected $childEnabled = false;
	protected $childKey = false;
	protected $childFile;
	protected $plugin_handle = 'boilerplate-extension';
	protected $product_id = 'Boilerplate Extension';
	protected $software_version = '2.9';

	public function __construct() {
		$this->childFile = __FILE__;
		add_filter( 'mainwp-getextensions', array( &$this, 'get_this_extension' ) );
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', false );
		if ( $this->mainwpMainActivated !== false ) {
			$this->activate_this();
		} else {
			add_action( 'mainwp-activated', array( &$this, 'activate_this' ) );
		}
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_action( 'admin_notices', array( &$this, 'mainwp_error_notice' ) );
	}

	function admin_init() {
		if ( get_option( 'mainwp_boilerplate_activate_activated' ) == 'yes' ) {
			delete_option( 'mainwp_boilerplate_activate_activated' );
			wp_redirect( admin_url( 'admin.php?page=Extensions' ) );
			return;
		}
	}

	function get_this_extension( $pArray ) {
		$pArray[] = array( 'plugin' => __FILE__, 'api' => $this->plugin_handle , 'mainwp' => true, 'callback' => array( &$this, 'settings' ), 'apiManager' => true );
		add_action('mainwp_postboxes_on_load_site_page', array( &$this, 'on_load_site_page_callback'), 10, 1);
		return $pArray;
	}


	function on_load_site_page_callback($websiteid) {
		$i = 1;
		if (!empty($websiteid)){
			add_meta_box(
				'blp-contentbox-' . $i++,
				'<i class="fa fa-cog"></i> ' . __( 'Token Values', 'boilerplate-extension' ),
				array( 'BoilerplateExtension', 'renderSiteTokenValues' ),
				'mainwp_postboxes_managesites_edit',
				'normal',
				'core',
				array( 'websiteid' => $websiteid )
			);
		}
	}

	function settings() {
		do_action( 'mainwp-pageheader-extensions', __FILE__ );
		BoilerplateExtension::get_instance()->settings_page();
		do_action( 'mainwp-pagefooter-extensions', __FILE__ );
	}


	function activate_this() {
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', $this->mainwpMainActivated );
		$this->childEnabled = apply_filters( 'mainwp-extension-enabled-check', __FILE__ );
		$this->childKey = $this->childEnabled['key'];
		if ( function_exists( 'mainwp_current_user_can' ) && ! mainwp_current_user_can( 'extension', 'boilerplate-extension' ) ) {
			return; }
		new BoilerplateExtension();
	}



	function get_enable_status() {
		return $this->childEnabled;
	}
	function mainwp_error_notice() {
		global $current_screen;
		if ( $current_screen->parent_base == 'plugins' && $this->mainwpMainActivated == false ) {
			echo '<div class="error"><p>Boilerplate Extension ' . __( 'requires <a href="https://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> to be activated in order to work. Please install and activate <a href="https://mainwp.com/" target="_blank">MainWP Dashboard Plugin</a> first.' ) . '</p></div>';
		}
	}

	public function get_child_key() {
		return $this->childKey;
	}

	public function get_child_file() {

		return $this->childFile;
	}

	public function activate() {
		$options = array(
                                'product_id' => $this->product_id,
                                'activated_key' => 'Deactivated',
                                'instance_id' => apply_filters( 'mainwp-extensions-apigeneratepassword', 12, false ),
                                'software_version' => $this->software_version,
                                //'on_load_callback' => array( &$this, 'on_load_page' )
                        );
		update_option( $this->plugin_handle . '_APIManAdder', $options );
	}

	public function deactivate() {
		update_option( $this->plugin_handle . '_APIManAdder', '' );
	}
}

global $mainWPBoilerplateExtensionActivator;
$mainWPBoilerplateExtensionActivator = new BoilerplateExtensionActivator();
