<?php
class Boilerplate_DB
{
	private $mainwp_boilerplate_db_version = '1.3';
	//Singleton
	private static $instance = null;
	private $table_prefix;
	public $default_tokens;

	static function get_instance() {

		if ( null === Boilerplate_DB::$instance ) {
			Boilerplate_DB::$instance = new Boilerplate_DB();
		}
		return Boilerplate_DB::$instance;
	}
	//Constructor
	function __construct() {

		global $wpdb;
		$this->table_prefix = $wpdb->prefix . 'mainwp_';
		$this->default_tokens = array(
		'url.site' => 'Display site URL',
										'name.site' => 'Display site Name',
										'state.site' => 'Display site State',
										'city.site' => 'Display site City',
										'address.site' => 'Display site Address',
		);
	}

	function table_name( $suffix ) {

		return $this->table_prefix . $suffix;
	}

	//Support old & new versions of wordpress (3.9+)
	public static function use_mysqli() {

		/** @var $wpdb wpdb */
		if ( ! function_exists( 'mysqli_connect' ) ) { return false; }

		global $wpdb;
		return ($wpdb->dbh instanceof mysqli);
	}

	//Installs new DB
	function install() {

		global $wpdb;
		$currentVersion = get_site_option( 'mainwp_boilerplate_db_version' );
		
		if ( $currentVersion == $this->mainwp_boilerplate_db_version ) { return; }
	
		$charset_collate = $wpdb->get_charset_collate();
		$sql = array();

		$tbl = 'CREATE TABLE `' . $this->table_name( 'boilerplate_token' ) . '` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`token_name` varchar(512) NOT NULL DEFAULT "",
`token_description` text NOT NULL,
`type` tinyint(1) NOT NULL DEFAULT 0';
		if ( '' == $currentVersion ) {
					$tbl .= ',
PRIMARY KEY  (`id`)  '; }
		$tbl .= ') ' . $charset_collate;
		$sql[] = $tbl;

		$tbl = 'CREATE TABLE `' . $this->table_name( 'boilerplate_site_token' ) . '` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`site_url` varchar(255) NOT NULL,
`token_id` int(12) NOT NULL,
`token_value` varchar(512) NOT NULL';
		if ( '' == $currentVersion ) {
					$tbl .= ',
PRIMARY KEY  (`id`)  '; }
		$tbl .= ') ' . $charset_collate;
		$sql[] = $tbl;

		error_reporting( 0 ); // make sure to disable any error output
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		foreach ( $sql as $query ) {
			dbDelta( $query );
		}
		//        global $wpdb;
		//        echo $wpdb->last_error;
		//        exit();
		foreach ( $this->default_tokens as $token_name => $token_description ) {
			$token = array(
			'type' => 1,
							'token_name' => $token_name,
							'token_description' => $token_description,
							);
			if ( $current = $this->get_tokens_by( 'token_name', $token_name ) ) {
				$this->update_token( $current->id, $token );
			} else {
				$this->add_token( $token );
			}
		}

		update_option( 'mainwp_boilerplate_db_version', $this->mainwp_boilerplate_db_version );
	}

	public function add_token( $token ) {

		/** @var $wpdb wpdb */
		global $wpdb;
		if ( ! empty( $token['token_name'] ) && ! empty( $token['token_description'] ) ) {
			if ( $current  = $this->get_tokens_by( 'token_name', $token['token_name'] ) ) {
				return false; }
			if ( $wpdb->insert( $this->table_name( 'boilerplate_token' ), $token ) ) {
				return $this->get_tokens_by( 'id', $wpdb->insert_id );
			}
		}
		return false;
	}

	public function update_token( $id, $token ) {

		 /** @var $wpdb wpdb */
		global $wpdb;
		if ( ! empty( $id ) && ! empty( $token['token_name'] ) && ! empty( $token['token_description'] ) ) {
			if ( $wpdb->update( $this->table_name( 'boilerplate_token' ), $token, array( 'id' => intval( $id ) ) ) ) {
				return $this->get_tokens_by( 'id', $id ); }
		}
		return false;
	}

	public function get_tokens_by( $by = 'id', $value = null, $site_url = '' ) {
		global $wpdb;

		if ( empty( $by ) || empty( $value ) ) {
			return null; }

		$sql = '';
		if ( 'id' == $by ) {
			$sql = $wpdb->prepare( 'SELECT * FROM ' . $this->table_name( 'boilerplate_token' ) . ' WHERE `id`=%d ', $value );
		} else if ( 'token_name' == $by ) {
			$sql = $wpdb->prepare( 'SELECT * FROM ' . $this->table_name( 'boilerplate_token' ) . " WHERE `token_name` = '%s' ", $value );
		}

		$token = null;
		if ( ! empty( $sql ) ) {
			$token = $wpdb->get_row( $sql ); }

		$site_url = trim( $site_url );

		if ( empty( $site_url ) ) {
			return $token; }

		if ( $token && ! empty( $site_url ) ) {
			$sql = 'SELECT * FROM ' . $this->table_name( 'boilerplate_site_token' ) .
					" WHERE site_url = '" . $this->escape( $site_url ). "' AND token_id = " . $token->id;
			$site_token = $wpdb->get_row( $sql );
			if ( $site_token ) {
				$token->site_token = $site_token;
				return $token;
			} else { 				return null; }
		}
		return null;
	}

	public function get_tokens() {
		global $wpdb;
		return $wpdb->get_results( 'SELECT * FROM ' . $this->table_name( 'boilerplate_token' ) . ' WHERE 1 = 1 ORDER BY type DESC, token_name ASC' );
	}

	public function get_site_tokens( $site_url ) {
		global $wpdb;
		$site_url = trim( $site_url );
		if ( empty( $site_url ) ) {
			return false; }
		$qry = ' SELECT st.* FROM ' . $this->table_name( 'boilerplate_site_token' ) . ' st ' .
				" WHERE st.site_url = '" . $site_url . "' ";
		//echo $qry;
		$site_tokens = $wpdb->get_results( $qry );
		$return = array();
		if ( is_array( $site_tokens ) ) {
			foreach ( $site_tokens as $token ) {
				$return[ $token->token_id ] = $token;
			}
		}
		// get default token value if empty
		$tokens = $this->get_tokens();
		if ( is_array( $tokens ) ) {
			foreach ( $tokens as $token ) {
				// check default tokens if it is empty
				if ( ! empty( $token ) && ($token->type == 1) && ( ! isset( $return[ $token->id ] ) || empty( $return[ $token->id ] )) ) {
					if ( ! isset( $return[ $token->id ] ) ) {
						$return[ $token->id ] = new stdClass(); }
					$return[ $token->id ]->token_value = $this->_get_default_token_site( $token->token_name, $site_url );
				}
			}
		}
		return $return;
	}

	public function _get_default_token_site( $token_name, $site_url ) {
		$website = apply_filters( 'mainwp_getwebsitesbyurl', $site_url );
		if ( empty( $this->default_tokens[ $token_name ] ) || ! $website ) {
			return false; }
		$website = current( $website );
		if ( is_object( $website ) ) {
			$url_site = $website->url;
			$name_site = $website->name;
		} else { 			return false; }

		switch ( $token_name ) {
			case 'url.site':
				$token_value = $url_site;
				break;
			case 'name.site':
				$token_value = $name_site;
				break;
			default:
				$token_value = '';
				break;
		}
		return $token_value;
	}

	public function add_token_site( $token_id, $token_value, $site_url ) {

		/** @var $wpdb wpdb */
		global $wpdb;

		if ( empty( $token_id ) ) {
			return false; }

		$website = apply_filters( 'mainwp_getwebsitesbyurl', $site_url );
		if ( empty( $website ) ) {
			return false; }

		if ( $wpdb->insert($this->table_name( 'boilerplate_site_token' ), array(
								'token_id' => $token_id,
								'token_value' => $token_value,
								'site_url' => $site_url,
							) ) ) {
				return $this->get_tokens_by( 'id', $token_id, $site_url );
		}

		return false;
	}

	public function update_token_site( $token_id, $token_value, $site_url ) {

		/** @var $wpdb wpdb */
		global $wpdb;

		if ( empty( $token_id ) ) {
			return false; }

		$website = apply_filters( 'mainwp_getwebsitesbyurl', $site_url );
		if ( empty( $website ) ) {
			return false; }

		$sql = 'UPDATE ' . $this->table_name( 'boilerplate_site_token' ) .
				" SET token_value = '" .$this->escape( $token_value ) . "' " .
				' WHERE token_id = ' . intval( $token_id ) .
				" AND site_url = '" . $this->escape( $site_url ) . "'";
		//echo $sql."<br />";
		if ( $wpdb->query( $sql ) ) {
			return $this->get_tokens_by( 'id', $token_id, $site_url );
		}

		return false;
	}

	public function delete_site_tokens( $token_id = null, $site_url = null ) {

		global $wpdb;
		if ( ! empty( $token_id ) ) {
			return $wpdb->query( $wpdb->prepare( 'DELETE FROM ' . $this->table_name( 'boilerplate_site_token' ) . ' WHERE token_id = %d ', $token_id ) ); } else if ( ! empty( $site_url ) ) {
			return $wpdb->query( $wpdb->prepare( 'DELETE FROM ' . $this->table_name( 'boilerplate_site_token' ) . ' WHERE site_url = %s ', $site_url ) ); }
			return false;
	}

	public function delete_token_by( $by = 'id', $value = null ) {
		global $wpdb;
		if ( 'id' == $by ) {
			if ( $wpdb->query( $wpdb->prepare( 'DELETE FROM ' . $this->table_name( 'boilerplate_token' ) . ' WHERE id=%d ', $value ) ) ) {
				$this->delete_site_tokens( $value );
				return true;
			}
		}
		return false;
	}

	protected function escape( $data ) {

		/** @var $wpdb wpdb */
		global $wpdb;

		if ( function_exists( 'esc_sql' ) ) { return esc_sql( $data ); } else { return $wpdb->escape( $data ); }
	}

	public function query( $sql ) {

		if ( null == $sql ) { return false; }

		/** @var $wpdb wpdb */
		global $wpdb;
		$result = @self::_query( $sql, $wpdb->dbh );

		if ( ! $result || (@self::num_rows( $result ) == 0) ) { return false; }
		return $result;
	}

	public static function _query( $query, $link ) {

		if ( self::use_mysqli() ) {
			return mysqli_query( $link, $query );
		} else {
			return mysql_query( $query, $link );
		}
	}

	public static function fetch_object( $result ) {

		if ( self::use_mysqli() ) {
			return mysqli_fetch_object( $result );
		} else {
			return mysql_fetch_object( $result );
		}
	}

	public static function free_result( $result ) {

		if ( self::use_mysqli() ) {
			return mysqli_free_result( $result );
		} else {
			return mysql_free_result( $result );
		}
	}

	public static function data_seek( $result, $offset ) {

		if ( self::use_mysqli() ) {
			return mysqli_data_seek( $result, $offset );
		} else {
			return mysql_data_seek( $result, $offset );
		}
	}

	public static function fetch_array( $result, $result_type = null ) {

		if ( self::use_mysqli() ) {
			return mysqli_fetch_array( $result, (null == $result_type ? MYSQLI_BOTH : $result_type) );
		} else {
			return mysql_fetch_array( $result, (null == $result_type ? MYSQL_BOTH : $result_type) );
		}
	}

	public static function num_rows( $result ) {

		if ( self::use_mysqli() ) {
			return mysqli_num_rows( $result );
		} else {
			return mysql_num_rows( $result );
		}
	}

	public static function is_result( $result ) {

		if ( self::use_mysqli() ) {
			return ($result instanceof mysqli_result);
		} else {
			return is_resource( $result );
		}
	}

	public function get_results_result( $sql ) {

		if ( null == $sql ) { return null; }
		/** @var $wpdb wpdb */
		global $wpdb;
		return $wpdb->get_results( $sql, OBJECT_K );
	}
}
