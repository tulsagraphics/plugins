<?php
class Boilerplate_Post
{
	private static $instance = null;

	static function get_instance() {

		if ( null === Boilerplate_Post::$instance ) {
			Boilerplate_Post::$instance = new Boilerplate_Post();
		}
		return Boilerplate_Post::$instance;
	}
	//Constructor
	function __construct() {

	}

	public static function init_menu() {

		add_submenu_page( 'mainwp_tab', 'Posting new boilerplate post', '<div class="mainwp-hidden">Boilerplate Post</div>', 'read', 'PostingBoilerplatePost', array( 'Boilerplate_Post', 'posting' ) ); //removed from menu afterwards
	}

	function get_sub_pages( $pArray ) {

		$pArray[] = array( 'slug' => 'BoilerplateAdd', 'title' => 'Add Boilerplate', 'callback' => array( &$this, 'render_bulk_add' ) );
		if ( isset( $_GET['page'] ) && 'PostBoilerplateEdit' == $_GET['page'] ) {
			$pArray[] = array( 'slug' => 'BoilerplateEdit', 'title' => 'Edit Boilerplate', 'callback' => array( &$this, 'render_bulk_edit' ), 'menu_hidden' => true, 'tab_link_hidden' => true );
		}
		return $pArray;
	}

	public function render_bulk_add() {

		do_action( 'mainwp-pageheader-post', 'BoilerplateAdd' );
		?> 
        <iframe scrolling="auto" id="mainwp_iframe"                    
                src="<?php echo get_site_url(); ?>/wp-admin/post-new.php?post_type=bulkpost&hideall=1&boilerplate=1">
        </iframe>        
        <?php
		do_action( 'mainwp-pagefooter-post', 'BoilerplateAdd' );
	}

	public function render_bulk_edit() {

		if ( ! isset( $_REQUEST['post_id'] ) ) {
			echo '<div class="error">' . __( 'Undefined error occurs.' ) . '</strong></div>';
			return;
		}

		do_action( 'mainwp-pageheader-post', 'BoilerplateEdit' );
		?>         
        <iframe scrolling="auto" id="mainwp_iframe"                    
                src="<?php echo get_site_url() . '/wp-admin/post.php?post_type=bulkpost&post=' . $_REQUEST['post_id'] . '&action=edit&hideall=1&boilerplate=1'; ?>">
        </iframe>        
        <?php
		do_action( 'mainwp-pagefooter-post', 'BoilerplateEdit' );
	}

	public static function posting() {

		//Posts the saved sites
		?>
    <div class="wrap">
        <h2><?php _e( 'New Boilerplate Post','boilerplate-extension' ); ?></h2>
        <?php
		if ( isset( $_GET['id'] ) ) {
			$id = $_GET['id'];
			$post = get_post( $id );
			if ( $post ) {
				//                die('<pre>'.print_r($post, 1).'</pre>');
				$selected_by = get_post_meta( $id, '_selected_by', true );
				$selected_sites = unserialize( base64_decode( get_post_meta( $id, '_selected_sites', true ) ) );
				$selected_groups = unserialize( base64_decode( get_post_meta( $id, '_selected_groups', true ) ) );

				/** @deprecated */
				$post_category = base64_decode( get_post_meta( $id, '_categories', true ) );

				$post_tags = base64_decode( get_post_meta( $id, '_tags', true ) );
				$post_slug = base64_decode( get_post_meta( $id, '_slug', true ) );
				$post_custom = get_post_custom( $id );

				include_once( ABSPATH . 'wp-includes' . DIRECTORY_SEPARATOR . 'post-thumbnail-template.php' );
				$post_featured_image = get_post_thumbnail_id( $id );
				$mainwp_upload_dir = wp_upload_dir();
				$new_post = array(
					'post_title' => $post->post_title,
					'post_content' => $post->post_content,
					'post_status' => $post->post_status, //was 'publish'
					'post_date' => $post->post_date,
					'post_date_gmt' => $post->post_date_gmt,
					'post_tags' => $post_tags,
					'post_name' => $post_slug,
					'post_excerpt' => $post->post_excerpt,
					'comment_status' => $post->comment_status,
					'ping_status' => $post->ping_status,
					'mainwp_post_id' => $post->ID,
				);
				// support external update meta, for example spinner to spin
				do_action( 'mainwp_boilerplate_update_post_meta', $post, 'bulkpost' );

				if ( null != $post_featured_image ) { //Featured image is set, retrieve URL
					$img = wp_get_attachment_image_src( $post_featured_image, 'full' );
					$post_featured_image = $img[0];
				}

				$sites = $groups = null;
				if ( 'site' == $selected_by ) { //Get all selected websites
					foreach ( $selected_sites as $k ) {
						$sites[] = $k;
					}
				} else { //Get all websites from the selected groups
					foreach ( $selected_groups as $k ) {
						$groups[] = $k;
					}
				}

				global $mainWPBoilerplateExtensionActivator;
				$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPBoilerplateExtensionActivator->get_child_file(), $mainWPBoilerplateExtensionActivator->get_child_key(), $sites, $groups );

				$output = new stdClass();
				$output->ok = array();
				$output->errors = array();

				if ( count( $dbwebsites ) > 0 ) {
					$post_data = array(
						'new_post' => base64_encode( serialize( $new_post ) ),
						'post_custom' => base64_encode( serialize( $post_custom ) ),
						'post_category' => base64_encode( $post_category ),
						'post_featured_image' => base64_encode( $post_featured_image ),
						'mainwp_upload_dir' => base64_encode( serialize( $mainwp_upload_dir ) ),
					);

					do_action( 'mainwp_fetchurlsauthed', $mainWPBoilerplateExtensionActivator->get_child_file(), $mainWPBoilerplateExtensionActivator->get_child_key(), $dbwebsites, 'newpost', $post_data, array( Boilerplate_Add::get_class_name(), 'posting_bulk_handler' ), $output );
				}

				foreach ( $dbwebsites as $website ) {
					if ( ($output->ok[ $website->id ] == 1) && (isset( $output->added_id[ $website->id ] )) ) {
						do_action( 'mainwp-bulkposting-done', $post, $website, $output );
					}
				}
				// calculate what post is create new, updated or will delete
				BoilerplateExtension::get_instance()->bulkposting_posting_done_results( $post, $dbwebsites, $output );

				$sites_posts = get_post_meta( $post->ID, '_mainwp_boilerplate_sites_posts', true );

				$new_posts = $sites_posts['new_posts'];
				$updated_posts = $sites_posts['updated_posts'];
				$delete_posts = $sites_posts['delete_posts'];
				$websites = apply_filters( 'mainwp-getsites', $mainWPBoilerplateExtensionActivator->get_child_file(), $mainWPBoilerplateExtensionActivator->get_child_key(), null );

				$all_sites = array();
				if ( is_array( $websites ) ) {
					foreach ( $websites as $website ) {
						$all_sites[ $website['id'] ] = $website;
					}
				}
			}

			?>
            <div id="message" class="updated">
                <?php
				if ( is_array( $new_posts ) ) {
					foreach ( $new_posts as $site_id => $_post ) {
						if ( isset( $all_sites[ $site_id ] ) ) {
							$website = $all_sites[ $site_id ];
							echo '<p>' . $website['name'] . ': New post created. <a href="' . $_post['link'] . '"  target=\"_blank\">View Post</a></p>';
						}
					}
				}

				if ( is_array( $updated_posts ) ) {
					foreach ( $updated_posts as $site_id => $_post ) {
						if ( isset( $all_sites[ $site_id ] ) ) {
							$website = $all_sites[ $site_id ];
							echo '<p>' . $website['name'] . ': Post updated. <a href="' . $_post['link'] . '"  target=\"_blank\">View Post</a></p>';
						}
					}
				}
				if ( is_array( $delete_posts ) ) {
					foreach ( $delete_posts as $site_id => $_post ) {
						if ( isset( $all_sites[ $site_id ] ) ) {
							$website = $all_sites[ $site_id ];
							echo '<p class="bpl_bulkpost_posting_item" type="post">' . $website['name'] . ': <span class="bpl_bulkpost_delete_status">Delete the post? <span class="bpl_bulkpost_delete_process hidden"></span><a href="#" class="bpl_bulkpost_delete_post" boilerplate_id="' . $post->ID . '" site_id="' . $site_id. '" post_id="' . $_post['post_id']. '">Delete</a> | <a href="' . $_post['link'] . '"  target=\"_blank\">View Post</a></span></p>';
						}
					}
				}
				foreach ( $dbwebsites as $website ) {
					if ( ! isset( $output->ok[ $website->id ] ) ) {
						echo $website->name .': ERROR - ' . $output->errors[ $website->id ];
					}
				}
				?>                    
            </div>
            <br/>
            <a href="<?php echo get_admin_url() ?>admin.php?page=PostBoilerplateAdd" class="add-new-h2" target="_top"><?php _e( 'Add New Boilerplate','boilerplate-extension' ); ?></a>
            <a href="<?php echo get_admin_url() ?>admin.php?page=Extensions-Boilerplate-Extension" class="add-new-h2" target="_top"><?php _e('Return
                to Boilerplate','boilerplate-extension'); ?></a>
            <?php
		} else {
			?>
            <div class="error below-h2">
                <p><strong><?php _e( 'ERROR','boilerplate-extension' ); ?></strong>: <?php _e( 'An undefined error occured.','boilerplate-extension' ); ?></p>
            </div>
            <br/>
            <a href="<?php echo get_admin_url() ?>admin.php?page=PostBoilerplateAdd" class="add-new-h2" target="_top"><?php _e( 'Add New Boilerplate','boilerplate-extension' ); ?></a>
            <a href="<?php echo get_admin_url() ?>admin.php?page=Extensions-Boilerplate-Extension" class="add-new-h2" target="_top"><?php _e('Return
                to Boilerplate','boilerplate-extension'); ?></a>
            <?php
		}
		?>
    </div>
	
	<script type="text/javascript">
		jQuery(document).ready(function($) { 
			$('.bpl_bulkpost_delete_post').live('click' ,function(){
				if (confirm(__('Are you sure?'))) {
					var parent = $(this).closest('.bpl_bulkpost_posting_item');		
					var type = parent.attr('type');
					parent.find('.bpl_bulkpost_delete_process').html('<i class="fa fa-spinner fa-pulse"></i> Deleting... ').show();			
					jQuery.post(ajaxurl, {
						action: 'mainwp_boilerplate_delete_site_post',
						post_id: $(this).attr('post_id'),
						site_id: $(this).attr('site_id'),
						boilerplate_id: $(this).attr('boilerplate_id')
					}, function(data) {
						parent.find('.bpl_bulkpost_delete_process').hide();
						if (data && data['status'] == 'SUCCESS') {
							parent.find('.bpl_bulkpost_delete_status').html('<i class="fa fa-check-circle"></i> '+'The ' + type + ' was deleted successfully.');                    
						}
						else 
						{
							parent.find('.bpl_bulkpost_delete_status').html('<i class="fa fa-exclamation-circle"></i> '+'Can not delete the ' + type + '.');                    
						}
					}, 'json');            
				}
				return false;   
			});
		});
	</script>
	
    <?php
	}
}
