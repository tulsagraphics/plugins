<?php
class Boilerplate_Add
{
	public static function get_class_name() {

		return __CLASS__;
	}

	public static function posting_bulk_handler( $data, $website, &$output ) {
		if ( preg_match( '/<mainwp>(.*)<\/mainwp>/', $data, $results ) > 0 ) {
			$result = $results[1];
			$information = unserialize( base64_decode( $result ) );
			if ( isset( $information['added'] ) ) {
				$output->ok[ $website->id ] = '1';
				if ( isset( $information['link'] ) ) {
					$output->link[ $website->id ] = $information['link']; }
				if ( isset( $information['added_id'] ) ) {
					$output->added_id[ $website->id ] = $information['added_id']; }
			} else if ( isset( $information['error'] ) ) {
				$output->errors[ $website->id ] = __( 'Error - ','boilerplate-extension' ) . $information['error'];
			} else {
				$output->errors[ $website->id ] = __( 'Undefined error - please reinstall the MainWP plugin on the client','boilerplate-extension' );
			}
		} else {
			$output->errors[ $website->id ] = apply_filters( 'mainwp_getErrorMessage', 'NOMAINWP', $website->url );
		}
	}
}

?>
