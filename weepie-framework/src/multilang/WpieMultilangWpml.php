<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * WpieMultilangWpml interface
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 1.2
 */
final class WpieMultilangWpml implements iWpieMultilangPlugin
{
	/**
	 * Flag if WPML API is loaded
	 *
	 * @since 1.2
	 *
	 * @var bool
	 */
	public $apiLoaded = false;

	/**
	 * The data for the current active language
	 *
	 * Data from WpieWpmlHelper::getActiveLangData()
	 *
	 * @since 1.2
	 *
	 * @var array
	 */
	public $activeLanguageData = array();

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->activeLanguageData = $this->getActiveLanguageData();

		$this->_includeApi();
	}


	public function isActive()
	{
		return WpieWpmlHelper::isActive();
	}

	public function isReady()
	{
		return WpieWpmlHelper::isReady();
	}

	public function getActiveLanguageData()
	{
		return WpieWpmlHelper::getActiveLanguageData();
	}

	public function getDefaultCode()
	{
		return WpieWpmlHelper::getDefaultCode();
	}

	public function getDefaultLocale()
	{
		$code = $this->getDefaultCode();
		$locale = WpieWpmlHelper::mapCodeToLocale( $code, $this->getLangs() );

		return $locale;
	}

	public function getActiveCode()
	{
		return WpieWpmlHelper::getActiveCode();
	}

	public function getActiveLocale()
	{
		return WpieWpmlHelper::getActiveLocale();
	}

	public function getActiveLangData() {
		return WpieWpmlHelper::getActiveLangData();
	}

	public function getAllCodes()
	{
		return WpieWpmlHelper::getAllCodes();
	}

	public function getAllLocales()
	{
		return WpieWpmlHelper::getAllLocales();
	}

	public function getLangs()
	{
		return $this->getAllLocales();
	}

	public function getParams()
	{
		$rtrn = new stdClass();
		$rtrn->isActive			= $this->isActive();
		$rtrn->isReady			= $this->isReady();
		$rtrn->defaultCode 		= $this->getDefaultCode();
		$rtrn->defaultLocale 	= $this->getDefaultLocale();
		$rtrn->activeCode		= $this->getActiveCode();
		$rtrn->activeLocale		= $this->getActiveLocale();
		$rtrn->allCodes			= $this->getAllCodes();
		$rtrn->allLocales		= $this->getAllLocales();

		return $rtrn;
	}

	public function switchToDefault()
	{
		return WpieWpmlHelper::switchLanguageToDefault();
	}

	public function switchLanguage( $code, $cookie  )
	{
		return WpieWpmlHelper::switchLanguage( $code, $cookie );
	}

	/**
	 * Inlude the WPML API code
	 *
	 * if succeeded, set flag self::apiLoaded to true
	 *
	 * @acces private
	 *
	 * @since 1.2
	 *
	 * @return boolean
	 */
	private function _includeApi()
	{
		if( defined( 'ICL_PLUGIN_PATH' ) ) {
			@require_once ICL_PLUGIN_PATH . '/inc/wpml-api.php';

			if( defined( 'WPML_API_SUCCESS' ) ) {
				$this->apiLoaded = true;
			}
			if( !defined( 'WPML_LOAD_API_SUPPORT' ) ) {
				define( 'WPML_LOAD_API_SUPPORT', true );
			}
		}
		else {
			return false;
		}
	}
}