<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * WpieException Class
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 1.4.3
 */
class WpieException extends Exception
{
	public function __construct( $message, $code = 0, Exception $previous = null ) 
	{
		parent::__construct( $message, $code, $previous );
	}
}