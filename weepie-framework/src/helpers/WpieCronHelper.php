<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * WpieCronHelper class
 *
 * Helper class for CRON processes
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 1.4.11
 */
class WpieCronHelper
{
	/**
	 * Determine if doing cron
	 *
	 * @uses wp_doing_cron()
	 *
	 * @since 1.4.12
	 *
	 * @return boolean
	 */
	public static function doingCron()
	{
		if( function_exists( 'wp_doing_cron' )  ) {
			return wp_doing_cron();
		}
	
		return ( defined( 'DOING_CRON' ) && DOING_CRON );
	}
}