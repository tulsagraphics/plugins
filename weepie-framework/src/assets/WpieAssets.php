<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * WpieAssets Class
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 1.4.0
 */
class WpieAssets
{
	/**
	 * Global plugin settings
	 *
	 * @since 1.4.0
	 *
	 * @var WpieGlobals
	 */
	private $globals;

	/**
	 * The plugins namespace
	 *
	 * @since 1.4.0
	 *
	 * @var string
	 */
	private $nameSpace;	
	
	/**
	 * Constructor
	 *
	 * @access public
	 *
	 * @param WpieGlobals $globals
	 *
	 * @since 1.4.0
	 */
	public function __construct( $globals )
	{
		if( !is_a( $globals, 'WpieGlobals' ) ) {
			throw new Exception( 'Parameter globals is not valid.' );
		}	
	
		$this->globals = $globals;
		$this->nameSpace = $globals->get( 'pluginNameSpace' );			
	}	
	
}