<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * Final class WpieGlobals 
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * 
 * @since 0.1
 */
final class WpieGlobals implements ArrayAccess, IteratorAggregate {
	
	/**
	 * The Plugins namespace
	 * 
	 * @since 1.0
	 * 
	 * @var string
	 */
	private $nameSpace;
	
	/**
	 * The plugin version
	 * 
	 * @since 1.4.9
	 * 
	 * @var string
	 */
	private $version;
	
	/**
	 * The collection
	 *
	 * @var array
	 */
	private $collection = array();	
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * 
	 * @param string $nameSpace
	 * @param string $pluginFile
	 * @param string $wpieFwFile
	 * @param string $version
	 * 
	 * @since 0.1
	 */
	public function __construct( $nameSpace = '', $pluginFile = '', $wpieFwFile = '', $version = '' )
	{
		try {		
			$this->nameSpace = $nameSpace;
			$this->version = $version;
			
			$this->setGlobals( $pluginFile, $wpieFwFile );			
		} catch( Exception $e ) {
			throw $e;
		}
	}
	
	
    /**
	 * Set the plugin globals
	 * 
	 * @param string $pluginFile - the Plugins file
	 * @param string $wpieFwFile - the WeePie Framework Plugins file
	 * 
	 * @since 1.4.13
	 */
	public function setGlobals( $pluginFile, $wpieFwFile )
	{
		// WeePie Framework URL's and Paths
		$wfPluginFolder = trim( plugin_basename( dirname( $wpieFwFile ) ), '\/' );
		$wfPluginUri = plugins_url( '/'.$wfPluginFolder );
		
		$pluginBase = plugin_basename( $pluginFile );
		$pluginDirName = ( preg_match( '/[^\/]+/', $pluginBase, $m ) ) ? $m[0] : '';		
		
		$this->set( 'wfPluginPath', WP_PLUGIN_DIR . '/'.$wfPluginFolder );
		$this->set( 'wfAssetsUri', $wfPluginUri . '/assets' );	
		$this->set( 'wfImgUri',  $this->get( 'wfAssetsUri' ) . '/img' );
		$this->set( 'wfJsUri',  $this->get( 'wfAssetsUri' ) . '/js' );
		$this->set( 'wfCssUri',  $this->get( 'wfAssetsUri' ) . '/css' );
		$this->set( 'wfTemplUri',  $wfPluginUri . '/templates' );
		$this->set( 'wfTemplPath',  $this->get( 'wfPluginPath' ) . '/templates' );
		
		$this->set( 'pluginNameSpace', $this->nameSpace ); 
		$this->set( 'pluginDirName', $pluginDirName );
		$this->set( 'pluginFile', $pluginBase );
		$this->set( 'pluginFileBase', basename( $this->get( 'pluginFile' ), '.php' ) );
		$this->set( 'version', $this->version );
		
		$this->set( 'pluginUri', plugins_url( '/' . $pluginDirName ) );
		$this->set( 'pluginPath', WP_PLUGIN_DIR . '/' . $pluginDirName );
		$this->set( 'pluginPathFile', $pluginFile );
		$this->set( 'settingsPath',  $this->get( 'pluginPath' ) . '/settings' );
		$this->set( 'settingsUri',  $this->get( 'pluginUri' ) . '/settings' );
		$this->set( 'templatePath',  $this->get( 'pluginPath' ) . '/templates' );
		$this->set( 'assetsPath',  $this->get( 'pluginPath' ) . '/assets' );
		$this->set( 'templatePathTheme', array( get_stylesheet_directory(), get_stylesheet_directory().'/'.$this->nameSpace ) );		
		$this->set( 'modulePath',  $this->get( 'pluginPath' ) . '/modules' );
		$this->set( 'moduleUri',  $this->get( 'pluginUri' ) . '/modules' );
		$this->set( 'assetsUri',  $this->get( 'pluginUri' ) . '/assets' );
		$this->set( 'cssUri',  $this->get( 'assetsUri' ) . '/css' );
		$this->set( 'jsUri',  $this->get( 'assetsUri' ) . '/js' );
		$this->set( 'imgUri',  $this->get( 'assetsUri' ) . '/img' );
		$this->set( 'jsNamespace', $this->nameSpace.'Data' );
		$this->set( 'optionModules', $this->nameSpace . '_modules' );
		$this->set( 'optionSettings', $this->nameSpace . '_settings' );
		$this->set( 'locale', ( '' !== ( $locale = get_locale() ) ) ? $locale : 'en_US' );	
	}
	
	/**
	 * Facilitate an iterator to loop through the collection
	 *
	 * @since 1.4.13
	 *
	 * @return \ArrayIterator
	 */
	public function getIterator ()
	{
		return new ArrayIterator( $this->collection );
	}
	
	/**
	 * @param string $offset
	 * 
	 * @since 1.4.13
	 *
	 * @return mixed
	 */
	public function offsetGet( $offset )
	{
		if( isset( $this->collection[$offset] ) ) {
			return $this->collection[$offset];
		}
	}
	
	/**
	 * Shortcut to offsetGet
	 *
	 * @param mixed $offset
	 * 
	 * @since 1.4.13
	 * 
	 * @return mixed
	 */
	public function get( $offset )
	{
		return $this->offsetGet( $offset );
	}
	
	/**
	 * @param mixed $offset
	 * @param mixed $value
	 *
	 * @since 1.4.13
	 *
	 * @return WpieCollection
	 */
	public function offsetSet( $offset, $value )
	{
		$this->collection[$offset] = $value;
	
		return $this;
	}
	
	/**
	 * Shortcut to offsetSet
	 *
	 * @param mixed $offset
	 * @param mixed $value
	 * 
	 * @since 1.4.13
	 *
	 * @return WpieCollection
	 */
	public function set( $offset, $value )
	{
		return $this->offsetSet( $offset, $value );
	}

	/**
	 * @param string $offset
	 * 
	 * @since 1.4.13
	 *
	 * @return WpieCollection
	 */
	public function offsetUnset( $offset )
	{
		if( isset( $this->collection[$offset] ) ) {
			unset( $this->collection[$offset] );
		}
	
		return $this;
	}	
	
	/**
	 * @param string $offset
	 * 
	 * @since 1.4.13
	 *
	 * @return boolean
	 */
	public function offsetExists( $offset )
	{
		if( isset( $this->collection[$offset] ) ) {
			return true;
		} else {
			return false;
		}
	}
}