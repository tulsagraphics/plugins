<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * WpieUpgrader Class
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 1.4.0
 */
final class WpieUpgrader
{
	/**
	 * The plugins namespace
	 *
	 * @since 1.4.0
	 *
	 * @var string
	 */
	private $nameSpace;

	/**
	 * Global plugin settings
	 *
	 * @since 1.4.0
	 *
	 * @var WpieGlobals
	 */
	private $globals;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * 
	 * @param WpieGlobals $globals
	 * 
	 * @since 1.4.0
	 */
	public function __construct( $globals )
	{
		$this->globals = $globals;
		$this->nameSpace = $this->globals->get( 'pluginNameSpace' );
	}
	
	/**
	 * Do upgrade dependend logic
	 *
	 * @access	public
	 *
	 * @since 	1.2
	 *
	 * @param 	string	$currVersionPlugin
	 * @param 	string	$newVersionPlugin
	 * @param	string	$wpiefwVersionOld
	 * @param 	string	$versionWpieFw
	 * @param 	bool	$networkWide
	 */
	public function doUpgradeDependendLogic( $currVersionPlugin, $newVersionPlugin, $wpiefwVersionOld, $versionWpieFw, $networkWide )
	{
		$pluginPath = $this->globals->get( 'pluginPath' );
		$upgradeFile = $pluginPath . '/upgrade.php';
			
		if( 0 === $currVersionPlugin ) {
			return;
		} elseif( file_exists( $upgradeFile ) ) {
			@include_once $upgradeFile;
		} else {
			return;
		}
					
		$wpieFwIs118orLower = version_compare( $wpiefwVersionOld, '1.1.8', '<=' );
			
		if( $wpieFwIs118orLower ) {
			$function = sprintf( '%s_wpiefw_upgrade_%s', $this->nameSpace, '118_and_early' );
		} else {
			$function = sprintf( '%s_wpiefw_upgrade_%s', $this->nameSpace, str_replace( '.', '', $versionWpieFw ) );
		}

		if( function_exists( $function ) ) {
			call_user_func( $function, $networkWide );
		}

		do_action(  $this->nameSpace . '_wpiefw_upgrade_logic', $currVersionPlugin, $newVersionPlugin, $wpiefwVersionOld, $versionWpieFw, $networkWide );
	}
}