<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * WpieHooksFrontend Class
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 1.4.0
 */
final class WpieHooksFrontend extends WpieHooks
{
	/**
	 * WpieAssets instance
	 * 
	 * @since 1.4.0
	 * 
	 * @var WpieAssets
	 */
	private $assets = false;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * 
	 * @since 1.4.0
	 * 
	 * @param WpieAssets $assets
	 * @param WpieGlobals $globals
	 */
	public function __construct( $assets, $globals )
	{
		if( !is_a( $assets, 'WpieAssets' ) ) {
			throw new Exception( 'Parameter assets is not valid.' );
		}		
		
		parent::__construct( $globals );
		
		$this->assets = $assets;
	}
	
	/**
	 * {@inheritDoc}
	 * @see WpieHooks::add()
	 */
	public function add()
	{
		try {		
			// Frontend hooks
			if( !is_admin() )
			{					
				if( $this->doFrontend ===  apply_filters( $this->nameSpace . '_do_frontend_hooks', true ) )
				{
					add_action( 'wp_enqueue_scripts', array($this, 'setStylesFrontend') );
					add_action( 'wp_enqueue_scripts', array($this, 'setScriptsFrontend') );
					add_action( 'wp_head', array($this, 'printScriptsFrontendVars'), 8 );
					add_action( 'wp_head', array($this, 'printScriptsFrontend') );
					add_action( 'template_redirect', array( $this, 'renderTemplate' ) );
		
					if( isset( $_REQUEST['action'] ) && 'wpie-footer-action' === $_REQUEST['action'] ) {
						add_action( 'wp_footer', array( $this, 'processAjaxRequest' ), 99999 );
					}
				}	
			} 	
		} catch ( Exception $e ) {
				
		}		
	}	
	
	/**
	 * Callback for the wp_enqueue_scripts hook: Enqueue scripts for the front-end
	 *
	 * @acces public
	 *
	 * @uses wp_enqueue_script()
	 * @uses [YOUR_PLUGIN_NAMESPACE]_scripts_frontend (3 params are passed: $wp_scripts, $exclude, $isScriptDebug)
	 * @uses [YOUR_PLUGIN_NAMESPACE]_exclude_scripts_frontend to let WeePie Framework Plugins filter the $exclude parameter
	 *
	 * @since 0.1
	 */
	public function setScriptsFrontend()
	{
		try {
			global $wp_scripts;
		
			$exclude =  array();
			$pluginPath = $this->globals->get( 'pluginPath' );
			$jsUri = $this->globals->get( 'jsUri' );
			$isScriptDebug = WpieMiscHelper::isScriptDebug();
			$ext = ( $isScriptDebug ) ? '.js' : '.min.js';
					
			// make sure jQuery is enqueued
			wp_enqueue_script( 'jquery' );
		
			if( file_exists( $pluginPath . '/assets/js/global' . $ext ) ) {					
				wp_enqueue_script( $this->nameSpace . '-global', $jsUri . '/global' . $ext, array( 'jquery' ), $this->globals->get('version') );
			}
		
			do_action( $this->nameSpace . '_scripts_frontend', $wp_scripts, apply_filters( $this->nameSpace . '_exclude_scripts_frontend', $exclude ), $isScriptDebug );
			
		} catch ( Exception $e ) {
			
		}
	}	
	
	/**
	 * Callback for the wp_head hook: setup global JavaScript parameters for the frontend
	 *
	 * @access public
	 *
	 * @uses [YOUR_PLUGIN_NAMESPACE]_script_frontent_vars to let WeePie Framework Plugins modify the $wpieVars array
	 * @uses json_encode() to safely create a JavaScript array
	 *
	 * @since 1.0
	 */
	public function printScriptsFrontendVars()
	{
		try {
			// WordPress installation URI
			$wpuri = get_bloginfo( 'wpurl' );
			// is multisite or not		
			$isMs = WpieMultisiteHelper::isMs();
		
			// build array with vars
			$wpieVars = array();
			$wpieVars['ns'] = $this->nameSpace;
			$wpieVars['nonce'] = wp_create_nonce( $this->nameSpace . '-action' );
			$wpieVars['wpurl'] = $wpuri;
			$wpieVars['domain'] = WpieMiscHelper::getHostWithoutSubdomain( $wpuri );
			$wpieVars['ajaxurl'] = admin_url( 'admin-ajax.php' );
			$wpieVars['referer'] = wp_get_referer();
			$wpieVars['currenturl'] = ( $isMs ) ? WpieMultisiteHelper::getCurrentUri() : home_url( add_query_arg( NULL, NULL ) );
			$wpieVars['isms'] = ( $isMs ) ? true : false;
			$wpieVars['mspath'] = ( $isMs ) ? WpieMultisiteHelper::getBlogDetail( 'path' ) : '/' ;
			$wpieVars['ssl'] = is_ssl();

			// fix for install with a sub folder setup
			// @since 1.1.8
			if( false !== strpos( $wpieVars['currenturl'] , '/' ) && !$isMs )
				$wpieVars['currenturl'] = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		
				$wpieVars = apply_filters( $this->nameSpace . '_script_frontend_vars' , $wpieVars );
					
				?>
				<script type='text/javascript'>
				/* <![CDATA[ */
				var <?php echo $this->globals->get( 'jsNamespace' ) ?> = <?php echo json_encode($wpieVars) ?>;
				/* ]]> */
				</script>
				<?php
		} catch ( Exception $e ) {
				
		}				
	}		
		
	/**
	 * Callback for the wp_head hook 
	 *
	 * @access public
	 * 
	 * @uses [YOUR_PLUGIN_NAMESPACE]_print_scripts_frontend to let WeePie Framework Plugins print styles for in the frontend head
	 * 
	 * @since 0.1 
	 */
	public function printScriptsFrontend()
	{			
		try { 
			do_action( $this->nameSpace . '_print_scripts_frontend' );
		} catch ( Exception $e ) {
				
		}		
	}			
			
			
	/**
	 * Callback for the wp_enqueue_scripts hook
	 * 
	 * @acces public
	 * 
	 * @uses [YOUR_PLUGIN_NAMESPACE]_print_scripts_frontend to let WeePie Framework Plugins enqueue styles for in the frontend head
	 * 
	 * @since 0.1
	 */
	public function setStylesFrontend()
	{
		try {
			global $wp_styles;
	
			$isModeDev = WpieMiscHelper::isRunningModeDev();
			do_action( $this->nameSpace . '_styles_frontend', $wp_styles, apply_filters( $this->nameSpace . '_exclude_styles_frontend', array() ), $isModeDev );
		} catch ( Exception $e ) {
				
		}		
	}
	
	/**
	 * Callback for the template_redirect hook: render front-end templates
	 *
	 * @access public
	 *
	 * @since 0.1
	 */
	public function renderTemplate()
	{
		try {		
			do_action( $this->nameSpace . '_render_templates' );
		} catch ( Exception $e ) {
				
		}			
	}	
}