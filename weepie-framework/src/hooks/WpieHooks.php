<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * iWpieHooks interface
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 1.4.0
 */
interface iWpieHooks
{
	/**
	 * Add all hooks
	 *
	 * @acces public
	 *
	 * @since 1.4.0
	 */	
	public function add();
}

/**
 * WpieHooks Class
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 1.4.0
 */
abstract class WpieHooks implements iWpieHooks
{
	/**
	 * Flag if frontend hooks should be added
	 * 
	 * @since 1.4.0
	 * 
	 * @var boolean
	 */
	public $doFrontend = true;
	
	/**
	 * WpieGlobals instance
	 * 
	 * @since 1.4.0
	 * 
	 * @var WpieGlobals
	 */
	protected $globals;
	
	/**
	 * The plugins namespace
	 * 
	 * @since 1.4.0
	 * 
	 * @var string
	 */
	protected $nameSpace = '';
	
	/**
	 * Constructor
	 *
	 * @access public
	 * 
	 * @param WpieGlobals $globals
	 * 
	 * @since 1.4.0
	 */
	public function __construct( $globals ) 
	{		
		if( !is_a( $globals, 'WpieGlobals' ) ) {
			throw new Exception( 'Parameter globals is not valid.' );
		}

		$this->globals = $globals;
		$this->nameSpace = $globals->get( 'pluginNameSpace' );
	}
	
	/**
	 * Callback for (WordPress) AJAX hook system 
	 * 
	 * This callback is also hooked to the 'wp_footer' action for AJAX requests made in the WordPress footer.
	 *  
	 * WeePie Framework Plugins should always provide the following REQUEST parameters in their JavaScript AJAX CALL:
	 * 
	 * 	- action: wpie-action
	 * 	- YOUR_PLUGIN_NAMESPACE_action: [UNIQUE_ACTION] to add an unique action for the AJAX CALL
	 * 	- nonce: [YOUR_PLUGIN_NAMESPACE]Data.nonce (WeePie Framework automaticly adds in some JavaScript parameters inlcuded a nonce
	 * 	- data  (optional): extra data to pass from client-side to here
	 * 
	 * Example for jQuery with Plugin namespace "myplugin" : $.post(mypluginData.ajaxurl, {action:'wpie-action', myplugin_action:'do-some-logic', nonce: mypluginData.nonce, data:{'foo':bar, 'food':bars}}, function(r) { ... });
	 * 
	 * If [YOUR_PLUGIN_NAMESPACE]_ajax_json_return returns 'content' as array index, the reponse is retured directly to the client without validating and json_encode
	 *  
	 * @uses filter [YOUR_PLUGIN_NAMESPACE]_ajax_json_return to let Modules interact with the WeePie Framework AJAX process
	 * @uses action [YOUR_PLUGIN_NAMESPACE]_after_process_ajax_request to let other Modules intertact with the AJAX process
	 * @uses WpieAjaxHelper::isValidData to validate the $return parameter. 
	 * 
	 * @link WeePieFramework::printScriptsFrontendVars()
	 * @link WeePieFramework::printScriptsAdminHeaderVars()
	 * 
	 * @since 1.0
	 * 
	 * @returns void on missing YOUR_PLUGIN_NAMESPACE_action, a raw output or an json encoded array 
	 */
	public function processAjaxRequest()
	{
		if( !isset( $_REQUEST[$this->nameSpace . '_action'] ) )
			return;
		
		//@TODO test additional if condition with WPIE_DOING_AJAX (set in WeePieFramework::start())			 
		//@TODO if has group meta, data is inside data[post_meta]
		
		$data = ( isset($_REQUEST['data']) ) ? $_REQUEST['data'] : null;
		$context = ( isset( $data['context'] ) ) ? $data['context'] : null;
		
		$r = array();
		$r['out'] = '';
		$hasError = false;
		
		$action = $_REQUEST[$this->nameSpace . '_action'];		
	
		if( false === check_ajax_referer( $this->nameSpace . '-action', 'nonce', false ) )
		{
			$r['out'] =  'ERROR: failed verifying nonce';
			$hasError = true;
		}
		
		if( false === $hasError )
		{	
			/**
			 * @todo add php doc
			 * @todo place this filter after {nameSpace}_validate_ajax_data ?
			 * @todo add 'data' entry to return validated data?
			 */
			do_action( $this->nameSpace . '_before_process_ajax_request', $action, $data );
			
			$validateReturn = array( 
					'is_valid' => true, 
					'fields' => array(), 
					'msg' => null );

			/**
			 * Let modules validate the ajax data for specific context
			 *
			 * @param	array $validateReturn
			 * @param	string the current AJAX action
			 * @param	array	the AJAX data
			 *
			 * @since	1.4.6
			 *
			 * @return	array
			 */
			$validateReturn = apply_filters( $this->nameSpace . '_validate_ajax_data_' . $context, $validateReturn, $action, $data );			
			
			/**
			 * Let modules validate the ajax data before processing it
			 * 
			 * @param	array	$validateReturn
			 * @param	string	the current AJAX action
			 * @param	array	the AJAX data
			 * @param	string	the context	- optional
			 * 
			 * @since	1.2.1
			 * 
			 * @return	array
			 */
			$validateReturn = apply_filters( $this->nameSpace . '_validate_ajax_data', $validateReturn, $action, $data, $context );
			
			if( isset( $validateReturn['is_valid'] ) && true == $validateReturn['is_valid'] )
			{				
				/**
				 * @todo add php doc
				 */
				$return = apply_filters( $this->nameSpace . '_ajax_json_return', null, $action, $data );
				
				// @todo: add $data param
				do_action( $this->nameSpace . '_after_process_ajax_request', $action, $return );
	
				if( is_array( $return ) && isset( $return['content'] ) ) {
					echo $return['content'];
					exit;
				}
				
				// allow data to be validated with bool false values
				// @since 1.1.8
				$allowFalse = ( is_array( $return ) && isset( $return['allow_false'] ) ) ? true : false;						
								
				$r['state'] = WpieAjaxHelper::isValidData( $return, $allowFalse );// 1 or 0
				$r['out'] = $return;
				if( WpieAjaxHelper::hasWpError( $return ) ) {				
					$r['wperrors'] = WpieAjaxHelper::getWpErrors( $return );					
				}
			} else {
				$r['state'] = '0';
				$r['out'] = $validateReturn;
			}
		} else {
				$r['state'] = '-1';
		}			
		
		echo json_encode( $r );
		exit;	
	}	
}