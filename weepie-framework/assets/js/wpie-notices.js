/*
 * Please see weepie-framework.php for more details.
 * 
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @version $Id: wpie-notices.js 71 2015-08-11 21:57:59Z Vincent Weber <vincent@webrtistik.nl> $
 * @since 1.1.8
 */
jQuery(function($) {
	
	var msgSelector = [
			'#setting-error-wpie_updated',
			'#setting-error-wpie_error',
			'.error',
			'.updated',
			'.notice',
			'.wpie-updated',
			'.wpie-error',
			'.wpie-notice',
			'.update-nag'
		],
		noticeClass = 'wpie-notice-msg',
		noticeHolderSelector = '#notice-holder';
		
	// add the specific class
	$.each(msgSelector, function(k,v) {
		
		$(v).addClass(noticeClass);
		
		if('.update-nag' === v) {
			$(v).css({'margin':'0'});
		}
	});

	// append WP and non WP messages to the notice holder
	$('.'+noticeClass).appendTo($(noticeHolderSelector)).css({'display':'block'});

	// hide the notice holder if its not an error
	$(noticeHolderSelector+':not(:has(.error)):not(:has(.update-nag))').delay(4000).animate({'height':'0px'}, 333, function(){ $(this).hide();});
});