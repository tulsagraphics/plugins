<?php
/**
 * Template for option groups
 *
 * Please see weepie-framework.php for more details.
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 1.2
 */
?>
<?php foreach( $form_fields as $field ): ?>

	<?php if(apply_filters( "{$namespace}_settings_escape_field_{$current_templ}", false, $field, $vars )) continue; ?>
	
	<?php if( WpieXmlSettingsHelper::isFormGroup( $field ) ):	
		$groupName = WpieXmlSettingsHelper::getFieldAttributeName( $field ); ?>	
		
	  <?php if(apply_filters( "{$namespace}_settings_escape_group", false, $groupName, $vars )) continue; ?>
		<?php if(apply_filters( "{$namespace}_settings_escape_group_{$current_templ}", false, $groupName, $vars )) continue; ?>
	
		<?php if( WpieXmlSettingsHelper::hasFormGroupTitle( $field ) )	: ?><h3><?php _e( trim($field->group_title) ) ?></h3><?php endif ?>							
		<?php if( WpieXmlSettingsHelper::hasFormGroupDescr( $field ) )	: ?><p><?php _e( trim($field->group_descr) ) ?></p><?php endif ?>					
		<?php if( WpieXmlSettingsHelper::hasFormGroupWarning( $field ) ): ?><p><span class="dashicons dashicons-megaphone"></span><span class="warning"><?php _e( trim($field->group_warning) ) ?></span></p><?php endif ?>

	<table<?php echo WpieXmlSettingsHelper::getFieldCssIdString( $field ) ?> class="form-table form-fields-<?php echo $namespace ?> form-group">
	
	<?php foreach( $field as $groupField ): ?>
	
	<?php if( WpieXmlSettingsHelper::isFormGroupMeta( $groupField ) ) continue; ?>
	
	<?php $name = (string)$groupField['name'] ?>

	<?php if(apply_filters( "{$namespace}_settings_escape_groupfield", false, $groupField, $vars )) continue; ?>
	<?php if(apply_filters( "{$namespace}_settings_escape_groupfield_{$current_templ}", false, $groupField, $vars )) continue; ?>
	
	<?php do_action( "{$setting}_groupfield_before_tr", $groupField, $vars ) ?>		
	
	  <tr valign="top" class="<?php echo $groupField->elem ?>">						
		<?php if( WpieXmlSettingsHelper::isInline($groupField) ): ?>
		
			<?php if( WpieXmlSettingsHelper::hasInlineTitle( $groupField ) ): ?><th scope="row"><?php _e( trim( $groupField->inline_title ), $namespace ) ?><?php if( WpieXmlSettingsHelper::hasInlineDescr( $groupField ) ): ?><span class="descr-i toggler"><i></i></span><?php endif ?></th><?php endif ?>
					
			<td colspan="2">							
			<?php $i=1; foreach( $groupField as $groupFieldInline ): $name = (string)$groupFieldInline['name'] ?>
			
				<?php if( WpieXmlSettingsHelper::isInlineTitle( $groupFieldInline ) ): continue; endif ?>
				<?php if( WpieXmlSettingsHelper::isInlineDescr( $groupFieldInline ) ): continue; endif ?>		
								
				<div class="inline">
				<?php $class = (WpieXmlSettingsHelper::isWpField($groupFieldInline)) ? 'WpieWpFormHelper' : 'WpieFormHelper' ?>
				<?php echo $class::formField( $groupFieldInline->elem, $name, (isset( ${$name} ) ? ${$name} : ''), $setting,
																				( WpieXmlSettingsHelper::hasAttr($groupFieldInline) ? WpieXmlSettingsHelper::getAttr($groupFieldInline) : array() ), 
																				( WpieXmlSettingsHelper::hasInnerHtml($groupFieldInline) ? WpieXmlSettingsHelper::getInnerHtml($groupFieldInline, $module_path, $vars) : '' ), 
																				( WpieXmlSettingsHelper::isSelect($groupFieldInline) ? WpieXmlSettingsHelper::getSelectOptions($groupFieldInline) : array() ) ) ?>
				</div>									
				<?php endforeach ?>						
			</td>
		<?php else: ?> 
		<?php if( WpieXmlSettingsHelper::hasTitle( $groupField ) ): ?><th scope="row"><label><?php  _e( trim($groupField->title), $namespace ) ?></label><?php if( WpieXmlSettingsHelper::hasDescr( $groupField ) ): ?><span class="descr-i toggler"><i></i></span><?php endif ?></th><?php endif ?>
			<td colspan="<?php echo ( WpieXmlSettingsHelper::hasTitle( $groupField ) ) ? 1 : 2 ?>">
			
				<?php do_action( "{$setting}_groupfield_before_td_content", $groupField, $vars ) ?>
				
				<?php $class = (WpieXmlSettingsHelper::isWpField($groupField)) ? 'WpieWpFormHelper' : 'WpieFormHelper' ?>			
				<?php echo $class::formField( $groupField->elem, $name, (isset( ${$name} ) ? ${$name} : ''), $setting,
																				( WpieXmlSettingsHelper::hasAttr($groupField) ? WpieXmlSettingsHelper::getAttr($groupField) : array() ), 
																				( WpieXmlSettingsHelper::hasInnerHtml($groupField) ? WpieXmlSettingsHelper::getInnerHtml($groupField, $module_path, $vars) : '' ), 
																				( WpieXmlSettingsHelper::isSelect($groupField) ? WpieXmlSettingsHelper::getSelectOptions($groupField) : array() ) ) ?>
														
			</td>													
		<?php endif ?>											
	  </tr>	  
	  <?php if( WpieXmlSettingsHelper::hasInlineDescr( $groupField ) ): ?><tr class="description" valign="top"><td colspan="2"><p class="togglethis" data-toggle-status="hide"><?php  _e( trim($groupField->inline_descr), $namespace ) ?></p></td></tr><?php
 			elseif( WpieXmlSettingsHelper::hasDescr( $groupField ) ): ?><tr class="description" valign="top"><td colspan="2"><p class="togglethis" data-toggle-status="hide"><?php  _e( trim($groupField->descr), $namespace ) ?></p></td></tr><?php endif ?>		  
	<?php endforeach; ?>
	
	</table>
	
	<?php elseif( WpieXmlSettingsHelper::isInline( $field ) ):	?>
	<table<?php echo WpieXmlSettingsHelper::getFieldCssIdString( $field ) ?> class="form-table form-fields-<?php echo $namespace ?>">
	  <tr valign="top" class="<?php echo (string)$field['name'] ?>">
	  <?php if( WpieXmlSettingsHelper::hasInlineTitle($field) ): ?><th scope="row"><?php _e( trim($field->inline_title), $namespace ) ?><?php if( WpieXmlSettingsHelper::hasInlineDescr( $field ) ): ?><span class="descr-i toggler"><i></i></span><?php endif ?></th><?php endif ?>
			<td>
				<?php $i=1; foreach( $field->field as $fieldInline ): $name = (string)$fieldInline['name'] ?>													
					<div class="inline">
					<?php $class = (WpieXmlSettingsHelper::isWpField($fieldInline)) ? 'WpieWpFormHelper' : 'WpieFormHelper' ?>
					<?php echo $class::formField( $fieldInline->elem, $name, (isset( ${$name} ) ? ${$name} : ''), $setting,
																					( WpieXmlSettingsHelper::hasAttr($fieldInline) ? WpieXmlSettingsHelper::getAttr($fieldInline) : array() ), 
																					( WpieXmlSettingsHelper::hasInnerHtml($fieldInline) ? WpieXmlSettingsHelper::getInnerHtml($fieldInline, $module_path, $vars) : '' ), 
																					( WpieXmlSettingsHelper::isSelect($fieldInline) ? WpieXmlSettingsHelper::getSelectOptions($fieldInline) : array() ) ) ?>
					</div>									
				<?php endforeach ?>				
			</td>			
	  </tr>
	<?php if( WpieXmlSettingsHelper::hasInlineDescr( $field ) ): ?><tr class="description" valign="top"><td colspan="2"><p class="togglethis" data-toggle-status="hide"><?php  _e( trim($field->inline_descr), $namespace ) ?></p></td></tr><?php endif ?>		
	</table>					
	<?php else: $name = (string)$field['name'] ?>
	<table<?php echo WpieXmlSettingsHelper::getFieldCssIdString( $field ) ?> class="form-table form-fields-<?php echo $namespace ?>">		
	  <tr valign="top" class="<?php echo $field->elem ?>">
			<?php if( WpieXmlSettingsHelper::hasTitle( $field ) ): ?><th scope="row"><?php _e( trim($field->title), $namespace ) ?><?php if( WpieXmlSettingsHelper::hasDescr( $field ) ): ?><span class="descr-i toggler"><i></i></span><?php endif ?></th><?php endif ?>
			<td colspan="<?php echo ( WpieXmlSettingsHelper::hasTitle( $field ) ) ? 1 : 2 ?>">	
							
			<?php do_action( "{$setting}_field_before_td_content", $field, $vars ) ?>
					
			<?php $class = (WpieXmlSettingsHelper::isWpField($field)) ? 'WpieWpFormHelper' : 'WpieFormHelper' ?>
			<?php echo $class::formField( $field->elem, $name, (isset( ${$name} ) ? ${$name} : ''), $setting,
																			( WpieXmlSettingsHelper::hasAttr($field) ? WpieXmlSettingsHelper::getAttr($field) : array() ), 
																			( WpieXmlSettingsHelper::hasInnerHtml($field) ? WpieXmlSettingsHelper::getInnerHtml($field, $module_path, $vars) : '' ), 
																			( WpieXmlSettingsHelper::isSelect($field) ? WpieXmlSettingsHelper::getSelectOptions($field) : array() ) ) ?>			
			</td>
	  </tr>	
	  <?php if( WpieXmlSettingsHelper::hasDescr( $field ) ): ?><tr class="description" valign="top"><td colspan="2"><p class="togglethis" data-toggle-status="hide"><?php  _e( trim($field->descr), $namespace ) ?></p></td></tr><?php endif ?>
	</table>  
	<?php endif; endforeach ?>
	
	<?php if($do_submit_btn): ?><p class="submit">	
	<?php 
	echo WpieFormHelper::formField(
		'submit',
		'submit',
		__( 'Save changes' ),
		false,
		array(
			'class' => 'button-submit button button-primary',
			'aria-label' => __( 'Save changes' )
		),
		__( 'Add', $namespace )
		)
	?>
	</p><?php endif ?>