<?php
/**
 * Template for managing custom lists
 *
 * Please see weepie-framework.php for more details.
 *
 * @author $Vincent Weber <vincent@webrtistik.nl> $
 * 
 * @since 1.2.1
 */
?>
<div class="wpie-list-wrapper wpie-list-layout-<?php echo $layout ?><?php if( $can_add ): ?> wpie-has-add<?php endif ?><?php if( $have_posts ): ?> wpie-list-has-posts<?php endif ?><?php if( $click_select ): ?> wpie-click-select<?php endif ?>"
	data-layout = "<?php echo $layout ?>"
	data-outer-w = "<?php echo $outer_w ?>"  
	data-context = "<?php echo $context ?>" 
	data-post-type = "<?php echo $post_type ?>"	
	data-can-add = "<?php echo $can_add ?>" 
	data-can-del = "<?php echo $can_del ?>" 
	data-can-save = "<?php echo $can_save ?>"
	data-click-select = "<?php echo $click_select ?>"	 		 
	data-has-title = "<?php echo $has_title ?>" 
	data-has-media = "<?php echo $has_media ?>" 
	data-group-meta = "<?php echo $group_meta ?>"
	data-group-meta-key = "<?php echo $group_meta_key ?>">

	<?php if( $can_add ): ?>
	<label for="wpie-list-new-post-<?php echo WpieMiscHelper::convertToHyphens( $context ) ?>"><?php _e( 'Add new item', 'weepie' ) ?>:</label><br/>		
	<?php 
	echo WpieFormHelper::formField(
		'textnoname',
		'wpieclb_new_list',
		'',
		false,
		array(
			'id' 	=> 'wpie-list-new-post-' . WpieMiscHelper::convertToHyphens( $context ),
			'class' => 'wpie-field-medium wpie-list-new-post',
			'placeholder' => sprintf( __( 'Enter a new %s name', 'weepie' ), WpieMiscHelper::convertToHyphens( $context ) ),
		)
	)
	?><span class="wpie-list-controls"><?php 
		echo WpieFormHelper::formField(
			'linkbutton',
			'',
			'',
			false,
			array(
				'class' => 'wpie-btn-add wpie-btn-add-post',
			),
			__( 'Add', 'weepie' )
			)
		?>		
	</span><span class="ajax-loading ajax-loading-wpie"></span>
	<?php endif ?>	
	<?php		
	/**
	 * Let others add content before displaying the posts list
	 * 
	 * @since 1.2.1
	 */
	echo $hook_before_list_posts;
	?>
	
	<?php if( $click_select ):
	echo WpieFormHelper::formField(
		'hidden',
		'',
		$selected,
		false,
		array( 'class' => 'hide wpie-list-selected-post' )
	);			
	endif ?>
	
	<p class="no-items-msg"><?php printf( __( 'There are no <strong>custom %s\'s</strong> to display yet.', 'weepie' ), WpieMiscHelper::convertToHyphens( $context ), __( 'Add', 'weepie' ) ) ?></p>
	
	<table id="wpie-list-posts-<?php echo WpieMiscHelper::convertToHyphens( $context ) ?>s" class="group wpie-list-posts-table wpie-list-rows-table">
	<?php if( $have_posts ): ?>
	
	<?php if( $has_heading ): ?>
	<tr class="wpie-list-heading wpie-list-row group">
	<?php foreach ( $headings as $heading ): ?>
	<th class="wpie-list-heading-item"><?php echo $heading ?></th>
	<?php endforeach ?>
	</tr>
	<?php endif ?>	
			
	<?php foreach( $posts as $post_id => $post ): ?>
	<?php WpieTemplate::get( $templ_list_item, 
							 array( 
							 		'post_id' => $post_id, 
							 		'post_type' => $post_type,							 		
							 		'title' => $post->post_title,
							 		'can_del' => $can_del,
							 		'can_save' => $can_save,
							 		'has_title' => $has_title, 
							 		'context' => $context,
							 		'layout' => $layout,
							 		'group_meta_key' => $group_meta_key,
							 		'click_select' => $click_select,
							 		'selected' => $selected
							 		
							 		) ) ?>
	<?php endforeach; endif ?>
	</table>
	<?php
	/**
	 * Let others add content after displaying the posts list
	 *
	 * @since 1.2.1
	 */
	echo $hook_after_list_posts; ?>	
</div>