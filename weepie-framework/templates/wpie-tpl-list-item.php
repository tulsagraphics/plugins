<?php
/**
 * Template for one lists item row
 *
 * Please see weepie-framework.php for more details.
 *
 * @author $Vincent Weber <vincent@webrtistik.nl> $
 * 
 * @since 1.2.1
 */
?>
<tr class="wpie-list-row<?php echo (( $click_select && ($selected === $post_id)) ? ' wpie-list-row-selected' : '' ) ?> wpie-list-row-<?php echo $post_id ?> group" data-post-id="<?php echo $post_id ?>">
	<?php if( $has_title ): ?>
	<td class="wpie-list-row-field field-title">	
		<?php 
		echo WpieFormHelper::formField(
			'textnoname',
			'',
			$title,
			false,
			array(
				'class' => 'wpie-list-row-field-post-title',
				'placeholder' => __( 'Enter a name for this item', 'weepie' ),
			)
		)
		?>				
	</td>
	<?php endif;
	
	$list_row_fields = apply_filters( 'wpie_row_fields_list_custom_posts_' . $context , array(), $post_id, $post_type );		
	foreach( $list_row_fields as $k => $field ): ?>
	<td class="wpie-list-row-field field-<?php echo WpieMiscHelper::convertToHyphens( $k ) ?><?php if( 'error' === $k ): ?> wpie-sett-field-err<?php endif ?>">
	<?php echo $field ?>
	</td>
	<?php endforeach ?>
	<td class="wpie-list-row-actions">
		<?php if( $can_save ): ?><span class="dashicons icon-save wpie-list-row-action wpie-btn-save"></span><?php endif ?>
		<?php if( $can_del ): ?><span class="dashicons dashicons-trash wpie-list-row-action wpie-btn-del"></span><?php endif ?>
		<span class="ajax-loading ajax-loading-wpie"></span>
	</td>
</tr>	