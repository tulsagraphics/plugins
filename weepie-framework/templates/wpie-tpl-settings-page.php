<?php
/**
 * Template for the admin settings page (parent)
 * 
 * Please see weepie-framework.php for more details.
 * 
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 1.2
 */
?>
<div class="wrap">	

	<div id="wpie-plugin-header">
		<div class="logo"></div>
		<h2><?php echo $title ?><?php echo ( $version ) ? ' v'.$version: '' ?></h2>
	</div>
		
	<div id="notice-holder"></div>
	
	<div id="wpie-nav-wrapper">
		<ul class="group<?php if( !$has_tabs ): ?> no-tabs<?php endif ?>">
		<?php if( $has_tabs ): ?>
			<?php foreach( $tabs as $k => $tab_data ): ?>		
			<li class="<?php echo $tab_data['class'] ?>"><a href="<?php echo $tab_data['uri']?>"><?php _e( $tab_data['tab'], 'weepie' ) ?></a></li>		
			<?php endforeach ?>
			<li id="instr-<?php echo $namespace ?>" class="wpie-plugin-instr"><a href="<?php echo $instructions_uri ?>" title="<?php esc_attr_e( "Read more details and intructions about the $title", $namespace ) ?>" target="_blank"><?php _e( 'Instruction guide ', 'weepie' ) ?> &rsaquo;</a></li>	
		<?php else: ?>		
			<li id="instr-<?php echo $namespace ?>" class="wpie-plugin-instr"><a href="<?php echo $instructions_uri ?>" title="<?php esc_attr_e( "Read more details and intructions about the $title", $namespace ) ?>" target="_blank"><?php _e( 'Instruction guide ', 'weepie' ) ?> &rsaquo;</a></li>		
		<?php endif ?>
		</ul>
	</div>	
	
	<div <?php if( $has_tabs ): ?>id="tab-<?php echo $current_tab ?>" <?php endif?>class="tab-content">				
	
		<?php if( $has_tabs ): ?>
		<form id="wpie-settings-form-<?php echo $current_tab ?>" class="<?php echo $namespace ?>-settings-form wpie-settings-form" data-setting-group="<?php echo $setting ?>" method="post" action="options.php">
				
		<?php echo WpieFormHelper::formField( 
				'hidden',
				"{$namespace}_tab",
				$current_tab,
				$setting ) ?>
					
		<?php settings_fields( $setting ) ?>	
		
		<?php if( 'tools' === $current_tab ): ?>	
		<h3><?php _e( 'Reset', 'weepie' ) ?></h3>
		<p><span class="dashicons dashicons-megaphone"></span><span class="warning"><?php _e( 'This action cannot be undone. All content will be set to default values!', 'weepie' ) ?></span></p>
		
		<?php echo WpieFormHelper::formField( 
				'buttonsubmitconfirm',
				'reset',
				'',
				'',
				array( '_msg' => __( 'Are you sure you want to RESET to default settings?', $namespace ), '_hidden_name' => 'wpie_do_reset' ),			
				__( 'Reset', $namespace )
				) ?>		
			
		<?php endif ?>
		
		<?php echo $table ?>				
		</form>
					
		<?php else: ?>
		<form class="<?php echo $namespace ?>-settings-form wpie-settings-form" data-setting-group="<?php echo $setting ?>" method="post" action="options.php">
		
		<?php echo WpieFormHelper::formField( 
				'hidden',
				"{$namespace}_tab",
				$current_tab,
				$setting ) ?>
				
		<?php settings_fields( $setting ) ?>	
		<?php echo $table ?>				

		</form>			
		<?php endif ?>		
				
	</div>
</div>