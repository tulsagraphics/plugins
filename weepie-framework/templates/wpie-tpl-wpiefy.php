<?php
/**
 * Template for WpieFy form
 * 
 * @since 1.2.3
 */
?>
<div class="wrap">
	<h1><?php printf( __( 'Validate your Envato Purchase code for: %s', 'weepie' ), $plugin_name ) ?></h1>
	<img width="160" height="100" src="<?php echo $logo_uri ?>" />
	<div class="wpiefy-wrap" id="<?php echo $tb_content_id ?>">
	
		<p><?php _e( 'In order to use this plugin, please enter the following fields and click on "Validate!".', 'weepie' ) ?></p>
		
		<div class="wpie-app-form-msg-wrap">
			<?php if( $has_err ): ?><div class="wpie-app-form-msg-err wpie-app-form-msg"><?php echo $msg_err ?></div><?php endif ?>
			<div class="wpie-app-form-msg-suc6 wpie-app-form-msg"><?php echo $msg_suc6 ?></div>
		</div>
		
		<form id="wpiefy-form" class="wpie-app-form" action="" method="post">
			
			<?php echo WpieFormHelper::formField(		
					'hidden',
					$field_name_ns,
					$wf_wpiefy_ns
					); ?>
			<?php echo WpieFormHelper::formField(		
					'hidden',
					$field_name_ac,
					$action
					); ?>
										
			<div class="wpie-form-field<?php echo ( in_array( $field_name_pc, $error_fields ) || $all_has_error ) ? ' has-err' : '' ?>">				
			<label for="wf_wpiefy_pc"><?php _e( 'Envato purchase code', 'weepie' ) ?></label>	
			<?php echo WpieFormHelper::formField(		
					'text',
					$field_name_pc,
					$wf_wpiefy_pc,
					'',
					array( 'placeholder' => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx' )				
					); ?>
			</div>
			<div class="wpie-form-field<?php echo ( in_array( $field_name_wc, $error_fields ) || $all_has_error ) ? ' has-err' : '' ?>">			
			<label for="wf_wpiefy_wc"><?php _e( 'WeePie code', 'weepie' ) ?></label>				
			<?php echo WpieFormHelper::formField(		
					'text',
					$field_name_wc,
					$wf_wpiefy_wc
					); ?>
				<span class="description"><?php printf( __( 'You should have recieved an email with your <strong>unique WeePie code</strong>, after you\'ve <a href="%s" target="_blank">registered</a> your <strong>Envato purchase code</strong>.', 'weepie' ), $uri_register ) ?></span>
			</div>			
			<?php if( !$wpiefied ): ?>
			<div class="wpie-form-field">
			<?php echo WpieFormHelper::formField(		
					'submit',
					'wf_wpiefy_send',
					'Validate!',
					'',
					$btn_attributes		
					); ?>
			</div>
			<?php endif ?>
			<?php if( $wpiefied ): ?>
			<div class="wpie-form-field">
			<?php echo WpieFormHelper::formField(		
				'button',
				'',
				'',
				'',
				$btn_attributes,
				__( 'Done', 'weepie' )
				); ?>
			</div>
			<?php endif ?>													
		</form>
		
	
	</div>
</div>