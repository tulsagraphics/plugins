<?php
/*
Main plugin file for WeePie Framework
 
@author $Author: Vincent Weber <vincent@webrtistik.nl> $

Plugin Name: WeePie Framework
Plugin URI:
Description: Framework to help WordPress developers building Plugins
Author: WeePie Plugins
Version: 1.4.14
Author URI: http://www.weepie-plugins.com/about-weepie-plugins/
License: GPL v3

WeePie Framework - A WordPress Plugin Framework to help WordPress developers building Plugins.

Copyright (C) 2013 - 2018, Vincent Weber webRtistik

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// Define paths
$pathWfSrc = dirname( __FILE__ ) . '/src';
$pathWfinc = dirname( __FILE__ ) . '/inc';

// Load source classes

// Interfaces
if( !interface_exists( 'iWpieMultilangPlugin' )          ) { require_once $pathWfSrc . '/multilang/WpieMultilangPlugin.php'; }

// Classes
if( !class_exists( 'WpieSrc' )                           ) { require_once $pathWfSrc . '/WpieSrc.php'; }
if( !class_exists( 'WpieException' )                     ) { require_once $pathWfSrc . '/notices/WpieException.php'; }
if( !class_exists( 'WpieNotices' )                       ) { require_once $pathWfSrc . '/notices/WpieNotices.php'; }
if( !class_exists( 'WpieAjaxHelper' )                    ) { require_once $pathWfSrc . '/helpers/WpieAjaxHelper.php'; }	
if( !class_exists( 'WpieXmlSettingsHelper' )             ) { require_once $pathWfSrc . '/helpers/WpieXmlSettingsHelper.php'; }
if( !class_exists( 'WpieFormHelper' )                    ) { require_once $pathWfSrc . '/helpers/WpieFormHelper.php'; }
if( !class_exists( 'WpieMiscHelper' )                    ) { require_once $pathWfSrc . '/helpers/WpieMiscHelper.php'; }
if( !class_exists( 'WpieMultisiteHelper' )               ) { require_once $pathWfSrc . '/helpers/WpieMultisiteHelper.php'; }
if( !class_exists( 'WpieMultilangHelper' )               ) { require_once $pathWfSrc . '/helpers/WpieMultilangHelper.php'; }
if( !class_exists( 'WpieRemoteHelper' )                  ) { require_once $pathWfSrc . '/helpers/WpieRemoteHelper.php'; }
if( !class_exists( 'WpieCookieHelper' )                  ) { require_once $pathWfSrc . '/helpers/WpieCookieHelper.php'; }
if( !class_exists( 'WpieCronHelper' )                    ) { require_once $pathWfSrc . '/helpers/WpieCronHelper.php'; }
if( !class_exists( 'WpieBaseSettings' )                  ) { require_once $pathWfSrc . '/settings/WpieBaseSettings.php'; }
if( !class_exists( 'WpiePluginSettings' )                ) { require_once $pathWfSrc . '/settings/WpiePluginSettings.php'; }
if( !class_exists( 'WpieBaseSettingProcessor' )          ) { require_once $pathWfSrc . '/settings/WpieBaseSettingProcessor.php'; }
if( !class_exists( 'WpiePluginSettingsProcessor' )       ) { require_once $pathWfSrc . '/settings/WpiePluginSettingsProcessor.php'; }
if( !class_exists( 'WpiePluginSettingsPage' )            ) { require_once $pathWfSrc . '/settings/WpiePluginSettingsPage.php'; }
if( !class_exists( 'WpieBaseMultilangProcessor' )        ) { require_once $pathWfSrc . '/multilang/WpieBaseMultilangProcessor.php'; }
if( !class_exists( 'WpieMultilangProcessor' )            ) { require_once $pathWfSrc . '/multilang/WpieMultilangProcessor.php'; }
if( !class_exists( 'WpieMultilangWpml' )                 ) { require_once $pathWfSrc . '/multilang/WpieMultilangWpml.php'; }
if( !class_exists( 'WpieTemplate' )                      ) { require_once $pathWfSrc . '/template/WpieTemplate.php'; }
if( !class_exists( 'WpiePluginModuleTemplateProcessor' ) ) { require_once $pathWfSrc . '/template/WpiePluginModuleTemplateProcessor.php'; }
if( !class_exists( 'WpiePluginModule' )                  ) { require_once $pathWfSrc . '/modules/WpiePluginModule.php'; }
if( !class_exists( 'WpieBaseModuleProcessor' )           ) { require_once $pathWfSrc . '/modules/WpieBaseModuleProcessor.php'; }
if( !class_exists( 'WpiePluginModuleProcessor' )         ) { require_once $pathWfSrc . '/modules/WpiePluginModuleProcessor.php'; }
if( !class_exists( 'WpieTaxonomy' )                      ) { require_once $pathWfSrc . '/posttype/WpieTaxonomy.php'; }
if( !class_exists( 'WpiePostType' )                      ) { require_once $pathWfSrc . '/posttype/WpiePostType.php'; }
if( !class_exists( 'WpieAssets' )                        ) { require_once $pathWfSrc . '/assets/WpieAssets.php'; }
if( !class_exists( 'WpieHooks' )                         ) { require_once $pathWfSrc . '/hooks/WpieHooks.php'; }
if( !class_exists( 'WpieHooksAdmin' )                    ) { require_once $pathWfSrc . '/hooks/WpieHooksAdmin.php'; }
if( !class_exists( 'WpieHooksFrontend' )                 ) { require_once $pathWfSrc . '/hooks/WpieHooksFrontend.php'; }
if( !class_exists( 'WpieShortcodes' )                    ) { require_once $pathWfSrc . '/shortcodes/WpieShortcodes.php'; }
if( !class_exists( 'WpieFy' )                            ) { require_once $pathWfSrc . '/wpie/WpieFy.php'; }
if( !class_exists( 'WpieCore' )                          ) { require_once $pathWfSrc . '/wpie/WpieCore.php'; }
if( !class_exists( 'WpieUpgrader' )                      ) { require_once $pathWfSrc . '/wpie/WpieUpgrader.php'; }
if( !class_exists( 'WpieGlobals' )                       ) { require_once $pathWfSrc . '/wpie/WpieGlobals.php'; }

// Framework core includes
if( !class_exists( $pathWfinc . '/core/WpieFwActivationHandler.php' )   ) { require_once $pathWfinc . '/core/WpieFwActivationHandler.php'; }
if( !class_exists( $pathWfinc . '/core/WpieFwDeactivationHandler.php' ) ) { require_once $pathWfinc . '/core/WpieFwDeactivationHandler.php'; }
// Handle framework activation and deactivation
WpieFwActivationHandler::addHooks( __FILE__ );
WpieFwDeactivationHandler::addHooks( __FILE__ );	

if( !class_exists( 'WeePieFramework' ) ) {
		
	/**
	 * WeePieFramework Class
	 * 
	 * This is the main Plugin class for WeePieFramework
	 * 
	 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
	 * @since 1.0
	 */
	class WeePieFramework extends WpieCore {
		
		/**
		 * Current version of the WeePie Framework
		 *
		 * @var string
		 * 
		 * @since 1.0.4
		 */
		const VERSION = '1.4.14';		
		
		/**
		 * Start a Plugin
		 * 
		 * @access protected
		 * 
		 * @param  string            $nameSpace   the plugins namespace
		 * @param  string 			 $file        the __FILE__ path of the Plugin that is extending WeePie Framework
		 * @param  (boolean|string)  $version     the Plugins current version
		 * @param  boolean           $doSettings  flag if settings should be initiated
		 * 
		 * @uses   WpieCore::engine()
		 * 
		 * @since  1.0 
		 */
		protected function start( $nameSpace = '', $file = '', $version = '', $doSettings = false ) 
		{	
			parent::engine( $nameSpace, $file, __FILE__, $version, $doSettings );
		}	
	}
}