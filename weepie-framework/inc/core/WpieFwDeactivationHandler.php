<?php
/**
 * Please see weepie-framework.php for more details.
 */

/**
 * WpieFwDeactivationHandler Class
 *
 * @author $Author: Vincent Weber <vincent@webrtistik.nl> $
 * @since 1.4.6
 */
class WpieFwDeactivationHandler
{
	/**
	 * The abolute WeePie Framework filepath
	 *
	 * @since 1.4.6
	 *
	 * @var string
	 */	
	private static $file = '';

	/**
	 * Add the hooks for deactivation
	 * 
	 * @param string $file
	 * 
	 * @uses register_deactivation_hook()
	 * 
	 * @since 1.4.6
	 */
	public static function addHooks( $file = '' )
	{
		self::$file = $file;
		register_deactivation_hook( self::$file, array( 'WpieFwDeactivationHandler', 'deactivate' ) );
	}

	/**
	 * Callback for the activate_{$file} action hook
	 *
	 * Set/update the current WeePie Framework version in the wp_options table
	 *
	 * @uses delete_site_option()
	 * @uses delete_option()
	 *
	 * @since 1.1
	 * @since 1.4.6 renamed to deactivate
	 */
	public static function deactivate( $networkDeactivating )
	{
		if( $networkDeactivating ) {
			update_site_option( 'wpiefw_active', '0' );
		} else {
			update_option( 'wpiefw_active', '0' );
		}
	}
}