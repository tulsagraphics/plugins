=== AME Branding Add-on ===
Contributors: whiteshadow
Tags: admin, branding, login, color scheme
Requires at least: 4.9
Tested up to: 4.9.8
Stable tag: 1.1

Adds more branding features to Admin Menu Editor Pro.

== Description ==

This add-on adds more branding features to Admin Menu Editor Pro. 

== Changelog ==

= 1.1 =
* Added import/export support.
* The custom admin color scheme now also applies to the Toolbar (a.k.a. Admin Bar) on the site front end.

= 1.0.1 =
* Fixed a bug where the "Lost your password" form would display a message saying "lostpassword" instead of the standard password recovery message. 
* Tested with WP 4.9.5.

= 1.0 =
* Initial release.