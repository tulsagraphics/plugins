== YITH WooCommerce Amazon S3 Storage ==

Contributors: yithemes
Tags: amazon s3 storage, woocommerce, products, themes, yit, e-commerce, shop, plugins
Requires at least: 2.6.14
Tested up to: 3.3.1
Stable tag: 1.0.9
Licence: GPLv2 or later
Licence URI: http://www.gnu.org/licences/gpl-2.0.html
Documentation: http://yithemes.com/docs-plugin/yith-amazon-s3-storage

== Changelog ==

= Version 1.0.9 - Released: Apr 2, 2018 =

 * Tweak: improvement of displaying the settings
 * Tweak: filter to choose whether to download the file from PHP or JS
 * Fix: load plugin textdomain
 * Fix: display correctly amazon urls of srcset
 * Dev: loading admin and frontend classes without PHP sessions

= Version 1.0.8 - Released: Feb 27, 2018 =

 * Tweak: setting the Amazon base url for all kind of credentials
 * Fix: changing string temporarlly to temporary

= Version 1.0.7 - Released: Feb 09, 2018 =

 * New: Support to WooCommerce 3.3.1
 * Tweak: Download links without javascript
 * Tweak: Customize the link of the order under the user account

= Version 1.0.6 - Released: Feb 05, 2018 =

 * Tweak: Showing download links and download counting without using the WooCommerce templates
 * Fix: download link compatible for mozilla and safari

= Version 1.0.5: Released: Jan 31, 2018 =

 * New: Checking the images in the content
 * New: Spanish translation .po and .mo
 * Tweak: Adding a filter to modify target blank
 * Tweak: Adding an additional link to the email to go to the order under account
 * Tweak: adding the counting of the downloads
 * Fix: Javascript error .select2 not found
 * Fix: Showing download urls on admin orders
 * Fix: Showing correct download url on emails
 * Fix: Configuring the valid time for downloads
 * Fix: Checking several images in content
 * Remove: Cleaning unnecessary code

= Version 1.0.4 - Released: Dec 29, 2017 =

 * New: Italian translation
 * New: Dutch translation
 * Update: plugin_fw

= Version 1.0.3 - Released: Nov 24, 2017 =

 * Fix: Compatibility of url for downloadable products in downloads of my account with WooCommerce 3.2.1

= Version 1.0.2 - Released: Nov 23, 2017 =

 * Fix: Compatibility of url for downloadable products with WooCommerce 3.2.1 for emails sent

= Version 1.0.1 - Released: Nov 16, 2017 =

 * Fix: Compatibility of url for downloadable products with WooCommerce 3.2.1

= Version 1.0.0 - Released: Jul 10, 2017 =

* First release

== Suggestions ==

If you have suggestions about how to improve YITH Amazon S3 for WooCommerce Premium, you can [write us](mailto:plugins@yithemes.com "Your Inspiration Themes") so we can bundle them into the next release of the plugin.

== Translators ==

If you have created your own language pack, or have an update for an existing one, you can send [gettext PO and MO file](http://codex.wordpress.org/Translating_WordPress "Translating WordPress")
[use](http://yithemes.com/contact/ "Your Inspiration Themes") so we can bundle it into YITH Quick Order Forms for WooCommerce Premium.
 = Available Languages =
 * English