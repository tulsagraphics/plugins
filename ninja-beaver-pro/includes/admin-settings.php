<div class="wrap <?php NJBAMySettingsPage::render_page_class(); ?>">
	<h2 class="fl-settings-heading">
		<?php NJBAMySettingsPage::render_page_heading(); ?>
	</h2>
	
	<?php NJBAMySettingsPage::render_update_message(); ?>
	<div class="fl-settings-nav">
		<ul>
			<?php NJBAMySettingsPage::render_nav_items(); ?>
		</ul>
	</div>
	<div class="fl-settings-content">
		<?php NJBAMySettingsPage::render_forms(); ?>
	</div>
</div>