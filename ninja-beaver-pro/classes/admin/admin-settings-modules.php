<?php
	global $njba_admin_option;
	if ( isset($_REQUEST['update']) ) {
		
		$modules = array_map( 'sanitize_text_field', $_POST['fl-modules'] );
		
		update_option( '_fl_builder_enabled_modules', $modules );
	}
	
?>
<?php
	$branding =get_option('njba_white_label_option');
	//$categories_name = ( array_key_exists( 'njba-builder-category', $branding ) ) ? $branding['njba-builder-category'] : '';
	$categories= njba_get_modules_cat();

	if(!empty($categories)){
		foreach($categories as $category){
			$categories_name[]=$category;
		}
	}
?>
<div id="fl-njba-modules-form" class="fl-settings-form">
	<h3 class="fl-settings-form-header"><?php //echo sprintf( __( '%s', 'bb-njba' ), NJBA_CAT ); ?></h3>
	<form id="modules-form" action="<?php NJBASettingsPage::render_form_action( 'modules' ); ?>" method="post">
		
		<div class="fl-settings-form-content">
			<p><?php _e('Check or uncheck modules below to enable or disable them.', 'fl-builder'); ?></p>
			<?php
			$categories         = FLBuilderModel::get_categorized_modules( true );
			//echo "<pre>"; print_r($categories);
			$enabled_modules    = FLBuilderModel::get_enabled_modules();
			$checked            = in_array('all', $enabled_modules) ? 'checked' : '';

			?>
			
			<?php foreach ( $categories as $title => $modules ) : 
					if(in_array($title,$categories_name)){
			?><h3 class="fl-settings-form-header"><?php echo $title; ?></h3>
							<?php
								
								foreach ( $modules as $module ) :
									$checked = in_array($module->slug, $enabled_modules) ? 'checked' : '';
								?>
								<p>
									<label>
										<input class="fl-module-cb" type="checkbox" name="fl-modules[]" value="<?php echo $module->slug; ?>" <?php echo $checked; ?> />
										<?php echo $module->name; // echo $module->enabled; ?>
									</label>
								</p>
								<?php endforeach; 
					}
			 endforeach; ?>
		</div>
		<p class="submit">
			<input type="submit" name="update" class="button-primary" value="<?php esc_attr_e( 'Save Module Settings', 'bb-njba' ); ?>" />
			
		</p>
	</form>
</div>