<?php
	$njba_admin_option_data = array('njba-plugin-name'   => '', 
                                      'njba-plugin-short-name' => '',
                                      'njba-plugin-desc' => '',
                                      'njba-author-name' => '',
                                      'njba-author-url' => '',
                                      'njba-admin-label' => '',
                                      'njba-builder-label' => '',
                                      'njba-builder-category' => '',
                                  );
    //$branding =get_option('njba_white_label_option');
	//$basename = plugin_basename( NJBA_MODULE_DIR . 'ninja-beaver-pro.php' );
	$plugin_name = ( array_key_exists( 'njba-plugin-name', $njba_admin_option_data ) ) ? $njba_admin_option_data['njba-plugin-name'] : '';
	if( $plugin_name == ''){
    	$plugin_name = 'Ninja Beaver Addon';
	}
?>
<div id="fl-njba-welcome-form" class="fl-settings-form">
	<h3 class="fl-settings-form-header"><?php _e('Welcome to the ', 'bb-njba'); ?><?php echo $plugin_name; ?><?php _e(' for Beaver Builder!', 'bb-njba'); ?></h3>
	<div class="fl-settings-form-content fl-welcome-page-content">
		<p><?php _e('Thank you for choosing the ', 'bb-njba'); ?><?php echo $plugin_name; ?><?php _e(' for Beaver Builder and welcome on board!', 'bb-njba'); ?></p>
	</div>
</div>
