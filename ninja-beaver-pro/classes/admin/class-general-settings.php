<div class="njba-admin-settings-section">
     <h1 class="njba-admin-settings-heading"> 
       <?php NJBASettingsPage::render_page_heading(); ?>
    </h1>
    
    <div class="njba-admin-settings-nav">
        <ul>
            <?php NJBASettingsPage::render_nav_items(); ?>
        </ul>
    </div>
    <div class="njba-settings-content">
        <?php NJBASettingsPage::render_forms(); ?>
    </div>
    
</div>