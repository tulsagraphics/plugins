<?php 
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
//ninja_beaver_pro_activate_license();
$license = get_option( 'ninja_beaver_license_key' );
$status  = get_option( 'ninja_beaver_license_status' );
?>
<div id="fl-njba-license-form" class="fl-settings-form">
	<h3 class="fl-settings-form-header"><?php _e( 'License', 'bb-njba' ); ?></h3>
	<div class="wrap">
	    <form name="njba_license_frm" method="post">
	    		<?php settings_fields('ninja_beaver_license'); ?>
				
					<?php if( false !== $license ) { 
				 			
				 			if( $status !== false && $status == 'valid' ) {
				 				?>
				 					<h3 class="njba-license-not-active"><?php _e( 'Updates &amp; Support Subscription', 'bb-njba' ); ?> - <span style="color: #00FF00;"><?php _e( 'Active!', 'bb-njba' ); ?></span></h3>
				 					<?php wp_nonce_field( 'njba_license_pro_nonce', 'njba_license_pro_nonce' ); ?>
				 					<input type="submit" class="button-secondary" name="njba_license__deactivate" value="<?php _e('Deactivate License','bb-njba'); ?>"/>
				 				<?php
				 			}
				 			else{
				 			?>
				 				<span class="njba-license-error"><?php _e( 'UPDATES UNAVAILABLE! Please enter your license key below to enable automatic updates.', 'bb-njba' ); ?></span>
				 				<h3 class="njba-license-not-active"><?php _e( 'Updates &amp; Support Subscription', 'bb-njba' ); ?> - <span style="color: #FF0000;"><?php _e( 'Not Active!', 'bb-njba' ); ?></span></h3>
				 				<?php wp_nonce_field( 'njba_license_pro_nonce', 'njba_license_pro_nonce' ); ?>
				 				<input type="submit" class="button-secondary" name="njba_license_activate" value="<?php _e('Activate License','bb-njba'); ?>"/>
				 			<?php
				 			}
					 	}
					 	?>
					<p><?php _e( 'Enter your ', 'bb-njba' ); ?><a href="https://www.ninjabeaveraddon.com/account/" target="_blank"><?php _e( 'license key', 'bb-njba' ); ?></a> <?php _e( 'to enable updates and support.', 'bb-njba' ); ?></p>
					<input type="text" placeholder="Enter your license key.." class="regular-text" id="njba_license_key" name="njba_license_key" value="<?php esc_attr_e( $license ); ?>">
					<?php //submit_button(); ?>
	       		
	    </form>
	</div>
	
</div>