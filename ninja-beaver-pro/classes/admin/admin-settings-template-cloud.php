<?php
	//$branding =get_option('njba_white_label_option');
	//$basename = plugin_basename( NJBA_MODULE_DIR . 'ninja-beaver-pro.php' );
	$njba_admin_option_data = array('njba-plugin-name'   => '', 
                                      'njba-plugin-short-name' => '',
                                      'njba-plugin-desc' => '',
                                      'njba-author-name' => '',
                                      'njba-author-url' => '',
                                      'njba-admin-label' => '',
                                      'njba-builder-label' => '',
                                      'njba-builder-category' => '',
                                  );
    $plugin_short_name = ( array_key_exists( 'njba-plugin-short-name', $njba_admin_option_data ) ) ? $njba_admin_option_data['njba-plugin-short-name'] : '';
	if( $plugin_short_name == ''){
    	$plugin_short_name = 'NJBA';
	}
?>
<div id="fl-njba-template-cloud-form" class="fl-settings-form njba-cloud-templates-fl-settings-form">
	<h3 class="fl-settings-form-header"><?php echo $plugin_short_name; ?><?php _e( ' Template', 'bb-njba' ); ?></h3>
	<form id="njba-cloud-templates-form" action="<?php NJBASettingsPage::render_form_action( 'template-cloud' ); ?>" method="post">
		<div class="fl-settings-form-content">
			<!-- Append all templates -->
			<div id="njba-cloud-templates-tabs">
				<div id="njba-cloud-templates-inner" class="wp-filter">
					<div class="filter-count">
						<span class="count"><?php echo NJBA_Cloud_Templates::get_cloud_templates_count('page-templates'); ?></span>
					</div>
					<ul class="njba-filter-links">
						<li class="active"><a href="#njba-cloud-templates-page-templates" data-count="<?php echo NJBA_Cloud_Templates::get_cloud_templates_count('page-templates'); ?>"> <?php _e('Page Templates', 'bb-njba'); ?> </a></li>
						<li><a href="#njba-cloud-templates-sections" data-count="<?php echo NJBA_Cloud_Templates::get_cloud_templates_count('sections'); ?>"> <?php _e('Sections', 'bb-njba'); ?> </a></li>
					</ul>
					
					<div class="njba-fetch-templates">
						<span class="button button-secondary njba-cloud-process" data-operation="fetch">
					    	<i class="dashicons dashicons-update " style="padding: 3px;"></i>
					    	<span class="msg"> <?php _e('Refresh', 'bb-njba'); ?> </span>
					   	</span>
					</div>
				</div>
				<div class="njba-cloud-templates-tabs-container">
					<div id="njba-cloud-templates-page-templates" style="display: block;">
						<?php
							//	Print Templates HTML
							NJBA_Cloud_Templates::template_html( 'page-templates' );
						?>
					</div>
					<div id="njba-cloud-templates-sections" style="display: none;">
						<?php
							//	Print Templates HTML
							NJBA_Cloud_Templates::template_html( 'sections' );
						?>
					</div>
					
				</div>
			</div>
			<br/>
		</div>
	</form>
</div>