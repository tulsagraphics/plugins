<div id="fl-njba-white-label-form" class="fl-settings-form njba-white-label-fl-settings-form">
	<h3 class="fl-settings-form-header"><?php _e( 'White Label', 'bb-njba' ); ?></h3>
	<form id="njba-white-label-form" action="<?php NJBASettingsPage::render_form_action( 'white-label' ); ?>" method="post">
		<div class="fl-settings-form-content">
			<?php
				
				$njba    = NJBASettingsPage::get_builder_njba_white_label();
				$checked = '';
				$label = 'Ninja Beaver';
				$njba_plugin_name = $njba_plugin_desc = $njba_author_name = $njba_author_url = $njba_plugin_short_name = $njba_admin_label = $njba_builder_label = $njba_builder_cat = '';
				if( is_array($njba) ) {
					
					//	Check Neble Disable branding
					$njba_plugin_name         = ( array_key_exists( 'njba-plugin-name', $njba ) ) ? $njba['njba-plugin-name'] : '';
					$njba_plugin_short_name   = ( array_key_exists( 'njba-plugin-short-name', $njba ) ) ? $njba['njba-plugin-short-name'] : '';
					$njba_plugin_desc         = ( array_key_exists( 'njba-plugin-desc' , $njba ) ) ? $njba['njba-plugin-desc' ] : '';
					$njba_author_name         = ( array_key_exists( 'njba-author-name' , $njba ) ) ? $njba['njba-author-name' ] : '';
					$njba_author_url          = ( array_key_exists( 'njba-author-url' , $njba ) ) ? $njba['njba-author-url' ] : '';
					$njba_admin_label          = ( array_key_exists( 'njba-admin-label' , $njba ) ) ? $njba['njba-admin-label' ] : '';
					$njba_builder_label          = ( array_key_exists( 'njba-builder-label' , $njba ) ) ? $njba['njba-builder-label' ] : '';
					$njba_builder_cat          = ( array_key_exists( 'njba-builder-category' , $njba ) ) ? $njba['njba-builder-category' ] : '';
				} ?>
			<?php /* Plugin Name*/ ?> 
			<div class="njba-white-label-fields" style="margin-top: 30px;">
			<h4 class="field-title"><?php _e( 'Plugin Name', 'bb-njba' ); ?></h4>
			<input type="text" name="njba-plugin-name" placeholder="Ninja Beaver Addon for Beaver Builder" value="<?php if($njba_plugin_name){ echo $njba_plugin_name; } ?>" class="regular-text njba-plugin-name" />
			</div>
			<?php /* Plugin Short Name*/ ?> 
			<div class="njba-branding-fields">
			<h4 class="field-title"><?php _e( 'Plugin Short Name', 'bb-njba' ); ?></h4>
			<input type="text" name="njba-plugin-short-name" placeholder="NJBA" value="<?php if($njba_plugin_short_name){ echo $njba_plugin_short_name; } ?>" class="regular-text njba-plugin-short-name" />
			</div>
		
			<?php /* Plugin Description */ ?> 
			<div class="njba-branding-fields">
			<h4 class="field-title"><?php _e( 'Plugin Description', 'bb-njba' ); ?></h4>
			<input type="text" name="njba-plugin-desc" placeholder="Plugin Description." value="<?php if($njba_plugin_desc){ echo $njba_plugin_desc; } ?>" class="regular-text njba-plugin-desc" />
			</div>
			
			<?php /* Author Name */ ?> 
			<div class="njba-branding-fields">
			<h4 class="field-title"><?php _e( 'Author / Agency Name', 'bb-njba' ); ?></h4>
			<input type="text" name="njba-author-name" placeholder="Ninja Team" value="<?php if($njba_author_name){ echo $njba_author_name; } ?>" class="regular-text njba-author-name" />
			</div>
			<?php /* Author URL */ ?>
			<div class="njba-branding-fields">
			<h4 class="field-title"><?php _e( 'Author / Agency URL', 'bb-njba' ); ?></h4>
			<input type="text" name="njba-author-url" placeholder="https://www.ninjabeaveraddon.com" value="<?php if($njba_author_url){ echo $njba_author_url; } ?>" class="regular-text njba-author-url" />
			</div>
			<?php /* Admin Lable */ ?> 
			<div class="njba-white-label-fields" style="margin-top: 30px;">
			<h4 class="field-title"><?php _e( 'Admin Label', 'bb-njba' ); ?></h4>
			<input type="text" name="njba-admin-label" placeholder="Ninja Beaver" value="<?php if($njba_admin_label){ echo $njba_admin_label; } else{ echo $label; } ?>" class="regular-text njba-admin-label" />
			</div>
			<?php /* Group Name */ ?> 
			<div class="njba-white-label-fields" style="margin-top: 30px;">
			<h4 class="field-title"><?php _e( 'Builder Group Name', 'bb-njba' ); ?></h4>
			<input type="text" id= "njba-builder-label" name="njba-builder-label" placeholder="NJBA Modules" value="<?php if($njba_builder_label){ echo $njba_builder_label; } ?>" class="regular-text njba_builder_label" />
			</div>
			<?php /* Group Name */ ?> 
			<div class="njba-white-label-fields" style="margin-top: 30px;">
			<h4 class="field-title"><?php _e( 'Builder Category Name', 'bb-njba' ); ?></h4>
			<input type="text" id= "njba-builder-category" name="njba-builder-category" placeholder="NJBA Modules" value="<?php if($njba_builder_cat){ echo $njba_builder_cat; } ?>" class="regular-text njba_builder_cat" />
			</div>
			
		
		</div>
		<p class="submit">
			<input type="submit" name="fl-save-njba-white-label" class="button-primary" value="<?php esc_attr_e( 'Save Settings', 'bb-njba' ); ?>" />
			
			<?php wp_nonce_field('white-label', 'fl-njba-branding-nonce'); ?>
		</p>
	</form>
</div>