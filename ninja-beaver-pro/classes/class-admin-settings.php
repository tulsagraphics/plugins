<?php
class NJBASettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;
    /**
     * Start up
     */
    public function __construct()
    {
         add_action( 'admin_menu', array( $this, 'njba_add_plugin_page' ) );
         add_action( 'admin_enqueue_scripts',array( $this, 'styles_scripts') );
         add_action( 'admin_init',array($this,'save_white_label_option'));
         add_filter( 'all_plugins', __CLASS__ . '::plugins_page' );
         $this->set_constants();
    }
    // Admin Styles & Script
    public function styles_scripts()
    {
          wp_enqueue_style( 'njba-admin-settings', NJBA_MODULE_URL . 'assets/css/njba-admin-settings.css', array(), NJBA_MODULE_VERSION );
          if(isset( $_REQUEST['page'] ) && 'njba-admin-setting' == $_REQUEST['page'] )
          { 
                 wp_register_script( 'njba-lazyload', NJBA_MODULE_URL . 'assets/js/jquery.lazyload.min.js', array(), NJBA_MODULE_VERSION );
                 wp_register_script( 'njba-cloud-templates-shuffle', NJBA_MODULE_URL . 'assets/js/jquery.shuffle.min.js', array(), NJBA_MODULE_VERSION );
                  wp_enqueue_script( 'njba-admin-menu', NJBA_MODULE_URL . 'assets/js/njba-admin-menu.js', array(), NJBA_MODULE_VERSION );
                 wp_register_script( 'njba-cloud-templates', NJBA_MODULE_URL . 'assets/js/njba-cloud-templates.js', array(), NJBA_MODULE_VERSION );
                 $NJBACloudTemplates = array(
                                                'ajaxurl'                => admin_url("admin-ajax.php"),
                                                'errorMessage'           => __( "Something went wrong!", "bb-njba" ),
                                                'successMessage'         => __( "Complete", "bb-njba" ),
                                                'successMessageFetch'    => __( "Refreshed!", "bb-njba" ),
                                                'errorMessageTryAgain'   => __( "Try Again!", "bb-njba" ),
                                                'successMessageDownload' => __( "Installed!", "bb-njba" ),
                                                'btnTextRemove'          => __( "Remove", "bb-njba" ),
                                                'btnTextDownload'        => __( "Install", "bb-njba" ),
                                                'btnTextInstall'         => __( "Installed", "bb-njba" ),
                                                'successMessageRemove'   => __( "Removed!", "bb-njba" ),
                                            );
                wp_localize_script( 'njba-cloud-templates', 'NJBACloudTemplates', $NJBACloudTemplates );
                wp_enqueue_script( 'njba-cloud-templates' );
                wp_enqueue_script( 'njba-lazyload' );
                wp_enqueue_script( 'njba-cloud-templates-shuffle' );
          }
    }
    
	/**
     * Add options page
     */
    public function njba_add_plugin_page()
    {
         
          $njba_admin_option_data = array('njba-plugin-name'   => '', 
                                      'njba-plugin-short-name' => '',
                                      'njba-plugin-desc' => '',
                                      'njba-author-name' => '',
                                      'njba-author-url' => '',
                                      'njba-admin-label' => '',
                                      'njba-builder-label' => '',
                                      'njba-builder-category' => '',
                                  );
          //$branding =get_option('njba_white_label_option');
          //$basename = plugin_basename( NJBA_MODULE_DIR . 'ninja-beaver-pro.php' );
          $admin_label  = ( array_key_exists( 'njba-admin-label', $njba_admin_option_data ) )  ? $njba_admin_option_data['njba-admin-label']  : '';
          $page_title = $admin_label;
          $menu_title = $admin_label;
          if( $admin_label == ''){
            $page_title = 'Ninja Beaver';
            $menu_title = 'Ninja Beaver';
          }
          $capability = 'manage_options';
          $menu_slug  = 'njba-admin-setting';
          $function   = array( $this, 'njba_admin_option');
          $icon_url   = 'dashicons-admin-generic';
          $position   = 81;
          add_menu_page( $page_title,$menu_title,$capability, $menu_slug, $function, $icon_url, $position );
          
    }
    /**
     * General Settings page
    */
    function njba_admin_option() {
       include('admin/class-general-settings.php');
    }
       
    /** 
     * Renders the nav items for the admin settings menu.
     */   
    static public function render_nav_items()
    {
        $options = get_option('njba_white_label_option');
        //echo "<pre>"; print_r($options); echo "<pre>"; 
        $items['welcome'] = array(
                    'title'     => __( 'Welcome', 'bb-njba' ),
                    'show'      => true,
                    'priority'  => 100
                );
        $items['license'] = array(
                    'title'     => __( 'License', 'bb-njba' ),
                    'show'      => true,
                    'priority'  => 200
                );
        $items['general'] = array(
                    'title'     => __( 'General', 'bb-njba' ),
                    'show'      => true,
                    'priority'  => 300
                );
        $items['modules'] = array(
                    'title'     => __( 'Modules', 'bb-njba' ),
                    'show'      => true,
                    'priority'  => 400
                );
       $items['template-cloud'] = array(
                    'title'     => __( 'Template', 'bb-njba' ),
                    'show'      => true,
                    'priority'  => 500
                );
        $items['white-label'] = array(
                    'title'     => __( 'White Label', 'bb-njba' ),
                    'show'      => true,
                    'priority'  => 600
                );
        $item_data = apply_filters( 'fl_builder_admin_settings_nav_items', $items );
       
        $sorted_data = array();
        foreach ( $item_data as $key => $data ) {
            $data['key'] = $key;
            $sorted_data[ $data['priority'] ] = $data;
        }
        ksort( $sorted_data );
        foreach ( $sorted_data as $data ) {
            if ( $data['show'] ) {
                echo '<li><a href="#' . $data['key'] . '">' . $data['title'] . '</a></li>';
            }
        }
    }
    /** 
     * Renders the admin settings forms.
	*/    
    static public function render_forms()
    {
	    // License
	    self::render_form( 'license' );
	    // Welcome
        self::render_form( 'welcome' );
        // General
        self::render_form( 'general' );
        self::render_form( 'modules' );
         // Template
        self::render_form( 'template-cloud' );
         // White Label
        self::render_form( 'white-label' );
        // Let extensions hook into form rendering.
        do_action( 'fl_builder_admin_settings_render_forms' );
    }
   /** 
     * Renders an admin settings form based on the type specified.
    */    
    static public function render_form( $type )
    {
        include NJBA_MODULE_DIR . 'classes/admin/admin-settings-' . $type . '.php';
    }
    /** 
     * Renders the plugins settings.
    */   
    static public function plugins_page($plugins) {
        $branding =get_option('njba_white_label_option');
        $basename = plugin_basename( NJBA_MODULE_DIR . 'ninja-beaver-pro.php' );
        //echo "<pre>"; print_r($branding); echo "<pre>"; 
        if ( isset( $plugins[ $basename ] ) && is_array( $branding ) ) {
            
            $plugin_name = ( array_key_exists( 'njba-plugin-name', $branding ) ) ? $branding['njba-plugin-name'] : '';
            $plugin_desc = ( array_key_exists( 'njba-plugin-desc', $branding ) ) ? $branding['njba-plugin-desc'] : '';
            $author_name = ( array_key_exists( 'njba-author-name', $branding ) ) ? $branding['njba-author-name'] : '';
            $author_url  = ( array_key_exists( 'njba-author-url', $branding ) )  ? $branding['njba-author-url']  : '';
            $admin_label  = ( array_key_exists( 'njba-admin-label', $branding ) )  ? $branding['njba-admin-label']  : '';
            $group_label  = ( array_key_exists( 'njba-builder-label', $branding ) )  ? $branding['njba-builder-label']  : '';
            $category_label  = ( array_key_exists( 'njba-builder-category', $branding ) )  ? $branding['njba-builder-category']  : '';
            if ( $plugin_name != '' ) {
                $plugins[ $basename ]['Name']  = $plugin_name;
                $plugins[ $basename ]['Title'] = $plugin_name;
            }
            if ( $plugin_desc != '' ) {
                $plugins[ $basename ]['Description'] = $plugin_desc;
            }
            if ( $author_name != '' ) {
                $plugins[ $basename ]['Author']     = $author_name;
                $plugins[ $basename ]['AuthorName'] = $author_name;
            }
            if ( $author_url != '' ) {
                $plugins[ $basename ]['AuthorURI'] = $author_url;
                $plugins[ $basename ]['PluginURI'] = $author_url;
            }
            if ( $admin_label != '' ) {
                $plugins[ $basename ]['AdminLabel'] = $admin_label;
            }
            if ( $group_label != '' ) {
                $plugins[ $basename ]['GroupLabel'] = $group_label;
            }
            if ( $category_label != '' ) {
                $plugins[ $basename ]['CategoryLabel'] = $category_label;
            }
        }
        return $plugins;
    }
    /** 
     * Renders the action for a form.
     */   
    static public function render_form_action( $type = '' )
    {
            echo admin_url( '/admin.php?page=njba-admin-setting#' . $type );
    }
    /** 
     * Set Constants
     */  
    function set_constants() {
        $branding         = $this->get_builder_njba_white_label();
        $branding_name    = __('NJBA', 'bb-njba');
        $branding_modules = __('NJBA Modules', 'bb-njba');
        //print_r($branding);
        //  Branding - %s
        //echo "<pre>"; print_r($branding); echo "<pre>"; 
        //echo "<pre>"; print_r($branding); echo "<pre>"; 
        if (is_array( $branding ) && array_key_exists( 'njba-plugin-short-name', $branding ) && $branding['njba-plugin-short-name'] != '') {
            $branding_name = $branding['njba-plugin-short-name'];
        }
        
        //  Branding - %s Modules
        if ( $branding_name != 'NJBA') {
            $branding_modules = sprintf( __( '%s Modules', 'bb-njba' ), $branding_name );
        }
         
        define( 'NJBA_PREFIX', $branding_name );
        define( 'NJBA_CAT', $branding_modules );           
    }
       
     /** 
     * Set Heading
     */  
    static public function render_page_heading()
    {
        echo '<span>' . sprintf( _x( '%s Settings', '%s stands for custom branded "NJBA" name.', 'bb-njba' ), NJBA_PREFIX ) . '</span>';
    }
    /** 
     * white label  Option Function
     */  
    static function get_builder_njba_white_label(){
            $options = get_option('njba_white_label_option');
            return $options;
    }
    /** 
     * white label  Option Function
     */  
    static public function save_white_label_option()
    {
       
        if ( isset( $_POST['fl-njba-branding-nonce'] ) && wp_verify_nonce( $_POST['fl-njba-branding-nonce'], 'white-label' ) ) {
            if( isset( $_POST['njba-plugin-name'] ) )           {   $njba['njba-plugin-name']         = wp_kses_post( $_POST['njba-plugin-name'] ); }
            if( isset( $_POST['njba-plugin-short-name'] ) )     {   $njba['njba-plugin-short-name']   = wp_kses_post( $_POST['njba-plugin-short-name'] );   }
            if( isset( $_POST['njba-plugin-desc'] ) )           {   $njba['njba-plugin-desc']         = wp_kses_post( $_POST['njba-plugin-desc'] ); }
            if( isset( $_POST['njba-author-name'] ) )           {   $njba['njba-author-name']         = wp_kses_post( $_POST['njba-author-name'] ); }
            if( isset( $_POST['njba-author-url'] ) )            {   $njba['njba-author-url']          = sanitize_text_field( $_POST['njba-author-url'] );   }
            if( isset( $_POST['njba-admin-label'] ) )            {   $njba['njba-admin-label']          = sanitize_text_field( $_POST['njba-admin-label'] );   }
            if( isset( $_POST['njba-builder-label'] ) ) { $njba['njba-builder-label'] = sanitize_text_field( $_POST['njba-builder-label'] ); } else { $njba['njba-builder-label'] = __( 'NJBA Modules', 'bb-njba' ); }
            if( isset( $_POST['njba-builder-category'] ) ) { $njba['njba-builder-category'] = sanitize_text_field( $_POST['njba-builder-category'] ); } else { $njba['njba-builder-category'] = __( 'NJBA Modules', 'bb-njba' ); }
            FLBuilderModel::update_admin_settings_option( 'njba_white_label_option', $njba, false );
        }
        //$admin_label = isset( $_POST['njba_admin_label'] ) ? sanitize_text_field( $_POST['njba_admin_label'] ) : 'Ninja Beaver';
        //self::update_option( 'njba_admin_label', $admin_label );
        /*$category_label         = isset( $_POST['njbawl_builder_label'] ) ? sanitize_text_field( $_POST['njbawl_builder_label'] ) : 'NJBA ' . __( 'Modules', 'bb-njba' );
        self::update_option( 'njbawl_builder_label', $category_label );*/
        
        
    }
}
if( is_admin() )
    $my_settings_page = new NJBASettingsPage();