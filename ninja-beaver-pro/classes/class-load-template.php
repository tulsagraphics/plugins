<?php
/**
 * NJBA_LOAD_TEMPLATE setup
 *
 * @since 1.1.0.4
 */
class NJBA_LOAD_TEMPLATE {
	
	public function __construct() {
		
		
		$this->load_templates();
		
	}
	
	//Load cloud templates
	 
	function load_templates() {
		if ( ! method_exists( 'FLBuilder', 'register_templates' ) ) {
			return;
		}
		$templates = get_site_option( '_njba_cloud_templats', false );
		
		if( is_array( $templates ) && count( $templates ) > 0 ) {
			foreach( $templates as $type => $type_templates ) {
				
				if ( $type == 'sections' ) {
					$group = 'Ninja Section';
				}else{
					$group = 'Ninja Templates';
				}
				
				//	Individual type array - [page-templates], [layout] or [row]
				if( $type_templates ) {
					foreach( $type_templates as $template_id => $template_data ) {
						
						//	Check [status] & [dat_url_local] exist
						if(isset( $template_data['status'] ) && $template_data['status'] == true && isset( $template_data['dat_url_local'] ) && !empty( $template_data['dat_url_local'] )) {
							
							FLBuilder::register_templates( $template_data['dat_url_local'] ,array('group' => $group) );
						}
					}
				}
			}
		}
	}
}
new NJBA_LOAD_TEMPLATE();