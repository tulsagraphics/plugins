﻿=== Ninja Beaver Add-ons for Beaver Builder ===

Contributors: njba,kirit-dholakiya
Tags: beaver addons, beaver builder, beaver builder add ons, beaver builder addon, beaver builder free, Ninja beaver addon
Requires at least: 4.0
Tested up to: 4.9.6
Stable tag: 1.2.2

Ninja Beaver Add-ons for Beaver Builder.

== Description ==

<b>Ninja Beaver Add-ons for Beaver Builder</b> features professional looking, easy to use highly functional extensions that can be used in a Beaver Builder page builder. This is really a premium plugin that you can get for free.
There are 45+ modules (in these version) easy to use, drag & drop, sections will speed up your website development process. You will be able to build professional websites without writing code.

== Installation ==

Ninja Beaver Addons for beaver builder works like any standard Wordpress plugin, and once enabled will automatically add a Modules in page builder module setings.

= Installing =

1. If installing, go to Plugins > Add New in the admin area, and search for 'Ninja Beaver Addons for Beaver Builder'.
2. Click install, once installed, activate and you're done!

= Manual Installation =

1. Use ftp to uploading the files to your server at wp-content/plugins.
2. Go to plugin page and activate plugin.