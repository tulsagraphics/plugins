<?php
class NJBAColumnSeparatorModule extends FLBuilderModule {
    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Column Separator', 'bb-njba'),
            'description'   => __('Addon to display Infobox.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'separator' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-column-separator/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-column-separator/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
            'partial_refresh'   => true,
            'icon'              => 'minus.svg',
        ));
        /**
         * Use these methods to enqueue css and js already
         * registered or to register and enqueue your own.
         */
        // Already registered
    }
    /** 
     * Use this method to work with settings data before
     * it is saved. You must return the settings object.
     *
     * @method update
     * @param $settings {object}
     */
    public function update($settings)
    {
        return $settings;
    }
    /**
     * This method will be called by the builder
     * right before the module is deleted.
     *
     * @method delete
     */
    public function delete()
    {
    }
}
FLBuilder::register_module('NJBAColumnSeparatorModule', array(
    'general' => array(
        'title' => __( 'General', 'bb-njba' ),
        'sections'  => array(
            'style' => array(
                'title' =>  __('Column Separator', 'bb-njba'),
                'fields'=> array(
                    'column_position'     => array(
                        'type'          => 'select',
                        'label'         => __( 'Column Separator Position', 'bb-njba' ),
                        'default'       => 'none',
                        'options'       => array(
                            'none'                  => __( 'None' , 'bb-njba' ),
                            'top'      => __( 'Top', 'bb-njba' ),
                            'bottom'      => __( 'Bottom', 'bb-njba' ),
                        ),
                        'toggle'    => array(
                            'none'              => array(
                                'fields'        => array( )
                            ),
                            'top'       => array(
                                'sections'      => array( 'njba_column_option' )
                            ),
                            'bottom'        => array(
                                'sections'      => array( 'njba_column_option' )
                            )
                        )
                    ),
                )
            ),
            'njba_column_option' => array(
                'title'     => __('Column Separator Option', 'bb-njba'),
                'fields'    => array(
                    'separator_shape' => array(
                        'type'          => 'select',
                        'label'         => __('Type', 'bb-njba'),
                        'default'       => 'triangle_svg',
                        'options'       => array(
                            'triangle_svg'              =>  __( 'Triangle', 'bb-njba' ),
                            'xlarge_triangle'           =>  __( 'Big Triangle', 'bb-njba' ),
                            'xlarge_triangle_left'      =>  __( 'Big Triangle Left', 'bb-njba' ),
                            'xlarge_triangle_right'     =>  __( 'Big Triangle Right', 'bb-njba' ),
                            'circle_svg'                =>  __( 'Half Circle', 'bb-njba' ),
                            'xlarge_circle'             =>  __( 'Curve Center', 'bb-njba' ),
                            'curve_up'                  =>  __( 'Curve Left', 'bb-njba' ),
                            'curve_down'                =>  __( 'Curve Right', 'bb-njba' ),
                            'tilt_left'                 =>  __( 'Tilt Left', 'bb-njba' ),
                            'tilt_right'                =>  __( 'Tilt Right', 'bb-njba' ),
                            'round_split'               =>  __( 'Round Split', 'bb-njba' ),
                            'waves'                     =>  __( 'Waves', 'bb-njba' ),
                            'clouds'                    =>  __( 'Clouds', 'bb-njba' ),
                            'multi_triangle'            =>  __( 'Multi Triangle', 'bb-njba' ),
                            'simple'                    => __('Simple','bb-njba'),
                        ),
                        'toggle'    => array(
                            'triangle_svg'              => array(
                                'fields'        => array( 'separator_shape_height', 'separator_shape_height_medium', 'separator_shape_height_small', 'njba_column_separator_color', 'njba_column_separator_color_opc' )
                            ),
                            'xlarge_triangle'           => array(
                                'fields'        => array( 'separator_shape_height', 'separator_shape_height_medium', 'separator_shape_height_small', 'njba_column_separator_color', 'njba_column_separator_color_opc' )
                            ),
                            'xlarge_triangle_left'      => array(
                                'fields'        => array( 'separator_shape_height', 'separator_shape_height_medium', 'separator_shape_height_small', 'njba_column_separator_color', 'njba_column_separator_color_opc' )
                            ),
                            'xlarge_triangle_right'     => array(
                                'fields'        => array( 'separator_shape_height', 'separator_shape_height_medium', 'separator_shape_height_small', 'njba_column_separator_color', 'njba_column_separator_color_opc' )
                            ),
                            'circle_svg'                => array(
                                'fields'        => array( 'separator_shape_height', 'separator_shape_height_medium', 'separator_shape_height_small', 'njba_column_separator_color', 'njba_column_separator_color_opc' )
                            ),
                            'xlarge_circle'             => array(
                                'fields'        => array( 'separator_shape_height', 'separator_shape_height_medium', 'separator_shape_height_small', 'njba_column_separator_color', 'njba_column_separator_color_opc' )
                            ),
                            'curve_up'                  => array(
                                'fields'        => array( 'separator_shape_height', 'separator_shape_height_medium', 'separator_shape_height_small', 'njba_column_separator_color', 'njba_column_separator_color_opc' )
                            ),
                            'curve_down'                => array(
                                'fields'        => array( 'separator_shape_height', 'separator_shape_height_medium', 'separator_shape_height_small', 'njba_column_separator_color', 'njba_column_separator_color_opc' )
                            ),
                            'tilt_left'                 => array(
                                'fields'        => array( 'separator_shape_height', 'separator_shape_height_medium', 'separator_shape_height_small', 'njba_column_separator_color', 'njba_column_separator_color_opc' )
                            ),
                            'tilt_right'                => array(
                                'fields'        => array( 'separator_shape_height', 'separator_shape_height_medium', 'separator_shape_height_small', 'njba_column_separator_color', 'njba_column_separator_color_opc' )
                            ),
                            'round_split'               => array(
                                'fields'        => array( 'separator_shape_height', 'separator_shape_height_medium', 'separator_shape_height_small', 'njba_column_separator_color', 'njba_column_separator_color_opc' )
                            ),
                            'waves'                     => array(
                                'fields'        => array( 'separator_shape_height', 'separator_shape_height_medium', 'separator_shape_height_small', 'njba_column_separator_color', 'njba_column_separator_color_opc' )
                            ),
                            'clouds'                    => array(
                                'fields'        => array( 'separator_shape_height', 'separator_shape_height_medium', 'separator_shape_height_small', 'njba_column_separator_color', 'njba_column_separator_color_opc' )
                            ),
                            'multi_triangle'            => array(
                                'fields'        => array( 'separator_shape_height', 'separator_shape_height_medium', 'separator_shape_height_small', 'njba_column_separator_color' )
                            )
                        )
                    ),
                    'separator_shape_height'   => array(
                        'type'          => 'text',
                        'label'         => __('Size', 'bb-njba'),
                        'default'       => '60',
                        'description'   => 'px',
                        'maxlength'     => '3',
                        'size'          => '6',
                        'placeholder'   => '60'
                    ),
                    'separator_shape_height_medium'   => array(
                        'type'          => 'text',
                        'label'         => __('Medium Device Size', 'bb-njba'),
                        'default'       => '',
                        'description'   => 'px',
                        'maxlength'     => '3',
                        'size'          => '6',
                    ),
                    'separator_shape_height_small'   => array(
                        'type'          => 'text',
                        'label'         => __('Small Device Size', 'bb-njba'),
                        'default'       => '',
                        'description'   => 'px',
                        'maxlength'     => '3',
                        'size'          => '6',
                    ),
                    'njba_column_separator_color' => array( 
                        'type'       => 'color',
                        'label'      => __('Background', 'bb-njba'),
                        'default'    => 'ffffff',
                        'show_reset' => true,
                        'help'       => __('Mostly, this should be background color of your adjacent Column section. (Default - White)', 'bb-njba'),
                    ),
                    'njba_column_separator_color_opc' => array( 
                        'type'        => 'text',
                        'label'       => __('Opacity', 'bb-njba'),
                        'default'     => '100',
                        'placeholder'   => '100',
                        'description' => '%',
                        'maxlength'   => '3',
                        'size'        => '5',
                    ),
                )
            )
        )
    )
));