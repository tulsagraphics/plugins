(function($) {
	NJBACountdown = function( settings )
	{
		this.timer_exp_text	= "";
		this.settings 	  = settings;
		this.nodeClass  = '.fl-node-' + settings.id;
        this.id         = settings.id;
		this.timertype	= settings.timertype;
		this.timerid	= '#countdown-' + settings.id;
		this.timer_date	= settings.timer_date;
		this.timer_format	= settings.timer_format;
		this.timer_layout	= settings.timer_layout;
		this.timer_labels	= settings.timer_labels;
		this.timer_labels_singular	= settings.timer_labels_singular;
		this.redirect_link = settings.redirect_link;
		this.redirect_link_target = settings.redirect_link_target;
		this.fixed_timer_action = settings.fixed_timer_action;
		this.evergreen_timer_action = settings.evergreen_timer_action;
		this.evergreen_date_days = settings.evergreen_date_days;
		this.evergreen_date_hour = settings.evergreen_date_hour;
		this.evergreen_date_minutes = settings.evergreen_date_minutes;
		this.evergreen_date_seconds = settings.evergreen_date_seconds;
		if ( settings.timer_exp_text ) {
			this.timer_exp_text	= settings.timer_exp_text;
		}
		if( this.timertype == "fixed" ) {
			this._initFixedTimer();
		}
		if( this.timertype == "evergreen" ) {
			this._initEverGreenTimer();
		}
		this._initCountdown();
	};
	NJBACountdown.prototype = {
		_initCountdown: function() {
			fixed_timer_action = this.fixed_timer_action;
			settings = this.settings;
			var action = '';
			if( this.timertype == "fixed" ) {
				action = this.fixed_timer_action;
			} else {
				action = this.evergreen_timer_action;
			}
			$.cookie( "countdown-" + settings.id + "expiremsg", null);
			$.cookie( "countdown-" + settings.id + "redirect", null);
			$.cookie( "countdown-" + settings.id + "redirectwindow", null);
			$.cookie( "countdown-" + settings.id + "hide", null);
			$.cookie( "countdown-" + settings.id + "reset", null);
			$.removeCookie( "countdown-" + settings.id + "expiremsg");
			$.removeCookie( "countdown-" + settings.id + "redirect");
			$.removeCookie( "countdown-" + settings.id + "redirectwindow");
			$.removeCookie( "countdown-" + settings.id + "hide");
			$.removeCookie( "countdown-" + settings.id + "reset");
			
			if( action == "msg") {
				$.cookie( "countdown-" + settings.id + "expiremsg", settings.expire_message, { expires: 365 } );
			} else if( action == "redirect") {
				$.cookie( "countdown-" + settings.id + "redirect", settings.redirect_link, { expires: 365 } );
				$.cookie( "countdown-" + settings.id + "redirectwindow", settings.redirect_link_target, { expires: 365 } );
			} else if( action == "hide") {
				$.cookie( "countdown-" + settings.id + "hide", "yes", { expires: 365 } );
			} else if( action == 'reset' ) {
				$.cookie( "countdown-" + settings.id + "reset", "yes", { expires: 365 } );
			}
		},
	
		_initFixedTimer: function() {
			var dateNow = new Date();
			if( ( dateNow.getTime() - this.timer_date.getTime() ) > 0 ) {
				if( this.fixed_timer_action == 'msg' ) {
					if( parseInt(window.location.href.toLowerCase().indexOf("?fl_builder")) === parseInt(-1) ) {
						$( this.timerid ).append(this.timer_exp_text);
					} else {
						$( this.timerid ).countdown({
							until: this.timer_date,
							format: this.timer_format,
							layout: this.timer_layout,
							labels: this.timer_labels.split(","),
				    		labels1: this.timer_labels_singular.split(","),
				        	expiryText: this.timer_exp_text
						});
					}
				} else if( this.fixed_timer_action == 'redirect' ) {
					if( parseInt(window.location.href.toLowerCase().indexOf("?fl_builder")) === parseInt(-1) ) {
						window.open( this.redirect_link, this.redirect_link_target );
					} else {
						$( this.timerid ).countdown({
							until: this.timer_date,
							format: this.timer_format,
							layout: this.timer_layout,
							labels: this.timer_labels.split(","),
				    		labels1: this.timer_labels_singular.split(","),
				        	expiryText: this.timer_exp_text
						});
					}
				} else if( this.fixed_timer_action == 'hide' ) {
					if( parseInt(window.location.href.toLowerCase().indexOf("?fl_builder")) === parseInt(-1) ) {
						$( this.timerid ).countdown('destroy');
					} else {
						$( this.timerid ).countdown({
							until: this.timer_date,
							format: this.timer_format,
							layout: this.timer_layout,
							labels: this.timer_labels.split(","),
				    		labels1: this.timer_labels_singular.split(","),
				        	expiryText: this.timer_exp_text
						});
					}
				} else {
					$( this.timerid ).countdown({
						until: this.timer_date,
						format: this.timer_format,
						layout: this.timer_layout,
						labels: this.timer_labels.split(","),
			    		labels1: this.timer_labels_singular.split(","),
					});
				}
			} else {
				// console.log('fix else');
				if( this.fixed_timer_action == 'msg' ) {
					if( parseInt(window.location.href.toLowerCase().indexOf("?fl_builder")) === parseInt(-1) ) {
						$( this.timerid ).countdown({
							until: this.timer_date,
							format: this.timer_format,
							layout: this.timer_layout,
							labels: this.timer_labels.split(","),
				    		labels1: this.timer_labels_singular.split(","),
				        	expiryText: this.timer_exp_text,
						});
					} else {
						$( this.timerid ).countdown({
							until: this.timer_date,
							format: this.timer_format,
							layout: this.timer_layout,
							labels: this.timer_labels.split(","),
				    		labels1: this.timer_labels_singular.split(","),
						});
					}
				} else if( this.fixed_timer_action == 'redirect' ) {
					$( this.timerid ).countdown({
						until: this.timer_date,
						format: this.timer_format,
						layout: this.timer_layout,
						labels: this.timer_labels.split(","),
			    		labels1: this.timer_labels_singular.split(","),
			        	expiryText: this.timer_exp_text,
			        	onExpiry: this._redirectCounter
					});
				} else if( this.fixed_timer_action == 'hide' ) {
					$( this.timerid ).countdown({
						until: this.timer_date,
						format: this.timer_format,
						layout: this.timer_layout,
						labels: this.timer_labels.split(","),
			    		labels1: this.timer_labels_singular.split(","),
			        	expiryText: this.timer_exp_text,
			        	onExpiry: this._destroyCounter
					});
				} else {
					$( this.timerid ).countdown({
						until: this.timer_date,
						format: this.timer_format,
						layout: this.timer_layout,
						labels: this.timer_labels.split(","),
			    		labels1: this.timer_labels_singular.split(","),
			        	expiryText: this.timer_exp_text
					});
				}
			}
		},
		_destroyCounter: function() {
			if( parseInt(window.location.href.toLowerCase().indexOf("?fl_builder")) === parseInt(-1) ) {
				jQuery( this ).countdown('destroy');
			}
		},
		_redirectCounter: function() {
			redirect_link = jQuery.cookie( jQuery(this)[0].id + "redirect" );
			redirect_link_target = jQuery.cookie( jQuery(this)[0].id + "redirectwindow" );
			if( parseInt(window.location.href.toLowerCase().indexOf("?fl_builder")) === parseInt(-1) ) {
				window.open( redirect_link, redirect_link_target );
			} else {
				return;
			}
		},
		_initEverGreenTimer: function() {
			var dateNow = new Date();
			if( ( dateNow.getTime() - this.timer_date.getTime() ) > 0 ) {
				if( this.evergreen_timer_action == 'msg' ) {
					if( parseInt(window.location.href.toLowerCase().indexOf("?fl_builder")) === parseInt(-1) ) {
						$( this.timerid ).append($.cookie( "countdown-" + this.settings.id + "expiremsg" ));
					} else {
						$( this.timerid ).countdown({
							until: this.timer_date,
							format: this.timer_format,
							layout: this.timer_layout,
							labels: this.timer_labels.split(","),
				    		labels1: this.timer_labels_singular.split(","),
						});
					}
				} else if( this.evergreen_timer_action == 'redirect' ) {
					if( parseInt(window.location.href.toLowerCase().indexOf("?fl_builder")) === parseInt(-1) ) {
						window.open( this.redirect_link, this.redirect_link_target );
					} else {
						$( this.timerid ).countdown({
							until: this.timer_date,
							format: this.timer_format,
							layout: this.timer_layout,
							labels: this.timer_labels.split(","),
				    		labels1: this.timer_labels_singular.split(","),
						});
					}
				} else if( this.evergreen_timer_action == 'hide' ) {
					if( parseInt(window.location.href.toLowerCase().indexOf("?fl_builder")) === parseInt(-1) ) {
						$( this.timerid ).countdown('destroy');
					} else {
						$( this.timerid ).countdown({
							until: this.timer_date,
							format: this.timer_format,
							layout: this.timer_layout,
							labels: this.timer_labels.split(","),
				    		labels1: this.timer_labels_singular.split(","),
						});
					}
				} else if( this.evergreen_timer_action == 'reset' ) {
					var today = new Date();
					today.setDate( today.getDate() + parseInt( jQuery.cookie( "countdown-" + this.settings.id +"-day" ) ) );
					today.setHours( today.getHours() + parseInt( jQuery.cookie( "countdown-" + this.settings.id +"-hour" ) ) );
					today.setMinutes( today.getMinutes() + parseInt( jQuery.cookie( "countdown-" + this.settings.id +"-min" ) ) );
					today.setSeconds( today.getSeconds() + parseInt( jQuery.cookie( "countdown-" + this.settings.id +"-sec" ) ) );
					
					var ever_green_dates_y = today.getFullYear();
					var ever_green_dates_m = today.getMonth();
					var ever_green_dates_d = today.getDate();
					var ever_green_dates_h = today.getHours();
					var ever_green_dates_i = today.getMinutes();
					var ever_green_dates_s = today.getSeconds();
					var newEverGreenDay = new Date( ever_green_dates_y, ever_green_dates_m, ever_green_dates_d, ever_green_dates_h, ever_green_dates_i, ever_green_dates_s );
					$( this.timerid ).countdown({
						until: newEverGreenDay,
						format: this.timer_format,
						layout: this.timer_layout,
						labels: this.timer_labels.split(","),
			    		labels1: this.timer_labels_singular.split(","),
			        	onExpiry: this._restartCountdown
					});
				} else {
					$( this.timerid ).countdown({
						until: this.timer_date,
						format: this.timer_format,
						layout: this.timer_layout,
						labels: this.timer_labels.split(","),
			    		labels1: this.timer_labels_singular.split(","),
					});
				}
			} else {
				if( this.evergreen_timer_action == 'msg' ) {
					if( parseInt(window.location.href.toLowerCase().indexOf("?fl_builder")) === parseInt(-1) ) {
						$( this.timerid ).countdown({
							until: this.timer_date,
							format: this.timer_format,
							layout: this.timer_layout,
							labels: this.timer_labels.split(","),
				    		labels1: this.timer_labels_singular.split(","),
				        	expiryText: $.cookie( "countdown-" + this.settings.id + "expiremsg" ),
						});
					} else {
						$( this.timerid ).countdown({
							until: this.timer_date,
							format: this.timer_format,
							layout: this.timer_layout,
							labels: this.timer_labels.split(","),
				    		labels1: this.timer_labels_singular.split(","),
						});
					}
				} else if( this.evergreen_timer_action == 'redirect' ) {
					$( this.timerid ).countdown({
						until: this.timer_date,
						format: this.timer_format,
						layout: this.timer_layout,
						labels: this.timer_labels.split(","),
			    		labels1: this.timer_labels_singular.split(","),
			        	onExpiry: this._redirectCounter
					});
				} else if( this.evergreen_timer_action == 'hide' ) {
					$( this.timerid ).countdown({
						until: this.timer_date,
						format: this.timer_format,
						layout: this.timer_layout,
						labels: this.timer_labels.split(","),
			    		labels1: this.timer_labels_singular.split(","),
			        	onExpiry: this._destroyCounter
					});
				} else if( this.evergreen_timer_action == 'reset' ) {
					$( this.timerid ).countdown({
						until: this.timer_date,
						format: this.timer_format,
						layout: this.timer_layout,
						labels: this.timer_labels.split(","),
			    		labels1: this.timer_labels_singular.split(","),
			        	onExpiry: this._restartCountdown
					});
				} else {
					$( this.timerid ).countdown({
						until: this.timer_date,
						format: this.timer_format,
						layout: this.timer_layout,
						labels: this.timer_labels.split(","),
			    		labels1: this.timer_labels_singular.split(","),
					});
				}
			}
		},
		_restartCountdown: function() {
			var today = new Date();
			today.setDate( today.getDate() + parseInt( jQuery.cookie( jQuery(this)[0].id +"-day" ) ) );
			today.setHours( today.getHours() + parseInt( jQuery.cookie( jQuery(this)[0].id +"-hour" ) ) );
			today.setMinutes( today.getMinutes() + parseInt( jQuery.cookie( jQuery(this)[0].id +"-min" ) ) );
			today.setSeconds( today.getSeconds() + parseInt( jQuery.cookie( jQuery(this)[0].id +"-sec" ) ) );
			
			var ever_green_dates_y = today.getFullYear();
			var ever_green_dates_m = today.getMonth();
			var ever_green_dates_d = today.getDate();
			var ever_green_dates_h = today.getHours();
			var ever_green_dates_i = today.getMinutes();
			var ever_green_dates_s = today.getSeconds();
			
			var everGreenDay = new Date( ever_green_dates_y, ever_green_dates_m, ever_green_dates_d, ever_green_dates_h, ever_green_dates_i, ever_green_dates_s );
			jQuery(this).countdown('option', {until: everGreenDay} );
		},
	};
})(jQuery);