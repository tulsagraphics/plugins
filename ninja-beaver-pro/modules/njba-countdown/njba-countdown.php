<?php
$date = date('Y/m/d h:i', time());
class NJBACountdownModule extends FLBuilderModule {
    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Countdown', 'bb-njba'),
            'description'   => __('Addon for countdown.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'creative' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-countdown/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-countdown/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'partial_refresh' => true, // Defaults to false and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
    public function enqueue_scripts() {
        
        $this->add_js('njba-countdown-plug', $this->url . 'js/jquery.plugin.js', array('jquery'), '', true );
        $this->add_js('njba-countdown-library', $this->url . 'js/jquery.countdown.js', array('jquery'), '', true );
        $this->add_js('njba-countdown-cookie', $this->url . 'js/jquery.cookie.js', array('jquery'), '', true );
        if(class_exists('FLBuilderModel') && FLBuilderModel::is_builder_active()){
            $this->add_js('njba-countdown-settings', $this->url . 'js/settings.js', array('jquery'), '', true );
        }
    }
    /** 
     * Use this method to work with settings data before
     * it is saved. You must return the settings object.
     *
     * @method update
     * @param $settings {object}
     */
    public function update($settings)
    {
        return $settings;
    }
    public function render_normal_countdown( $str1, $str2 ) {
        ob_start();
        
        ?><div class="njba-countdown-holding <?php echo $this->settings->timer_style; ?>"><div class="njba-countdown-digit-wrapper <?php echo $this->settings->timer_style; ?>"><<?php echo $this->settings->digit_tag_selection; ?> class="njba-count-down-digit <?php echo $this->settings->timer_style; ?>"><?php echo $str1; ?></<?php echo $this->settings->digit_tag_selection; ?>></div><div class="njba-countdown-unit-names"><<?php echo $this->settings->unit_tag_selection; ?> class="njba-count-down-unit <?php  echo $this->settings->timer_style; ?>"><?php echo $str2; ?></<?php echo $this->settings->unit_tag_selection; ?>></div></div><?php
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    /**
     * This method will be called by the builder
     * right before the module is deleted.
     *
     * @method delete
     */
    public function delete()
    {
    }
}
FLBuilder::register_module('NJBACountdownModule', array(
    'general'      => array( // Tab
        'title'         => __('General', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array( // Section Fields
                    'timer_type'       => array(
                        'type'          => 'select',
                        'label'         => __('Timer Type', 'bb-njba'),
                        'default'       => 'fixed',
                        'class'         => '',
                        'options'       => array(
                            'fixed'             => __('Fixed', 'bb-njba'),
                            'evergreen'             => __('Evergreen', 'bb-njba')
                        ),
                        'toggle'        => array(
                            'fixed'      => array(
                                'fields'     => array('fixed_date', 'fixed_timer_action' )
                            ),
                            'evergreen'      => array(
                                'fields'     => array( 'evergreen_date', 'evergreen_timer_action' ),
                            )
                        ),
                    ),
                    'fixed_date' => array(
                        'type'          => 'njba-normal-date',
                        'label'         => __( 'Select Date & Time', 'bb-njba' ),
                        'default'       => '',
                        'class'         => '',
                    ),
                    'evergreen_date' => array(
                        'type'          => 'njba-evergreen-date',
                        'label'         => __( 'Expire Countdown In', 'bb-njba' ),
                        'default'       => '',
                        'foo'           => 'bar'
                    ),
                    'fixed_timer_action'       => array(
                        'type'          => 'select',
                        'label'         => __('Action After Timer Expiry', 'bb-njba'),
                        'default'       => 'none',
                        'class'         => '',
                        'options'       => array(
                            'none'             => __('None', 'bb-njba'),
                            'hide'             => __('Hide Timer', 'bb-njba'),
                            'msg'         => __('Display Message', 'bb-njba'),
                            'redirect'         => __('Redirect User to New URL', 'bb-njba')
                        ),
                    ),
                    'evergreen_timer_action'       => array(
                        'type'          => 'select',
                        'label'         => __('Action After Timer Expiry', 'bb-njba'),
                        'default'       => 'none',
                        'class'         => '',
                        'options'       => array(
                            'none'             => __('None', 'bb-njba'),
                            'hide'             => __('Hide Timer', 'bb-njba'),
                            'reset'         => __('Reset Timer', 'bb-njba'),
                            'msg'         => __('Display Message', 'bb-njba'),
                            'redirect'         => __('Redirect User to New URL', 'bb-njba')
                        ),
                    ),
                    'expire_message'          => array(
                        'type'          => 'editor',
                        'label'         => '',
                        'media_buttons' => false,
                        'rows'          => 6,
                        'default'       => __('Enter message text here.','bb-njba'),
                    ),
                    'redirect_link'   => array(
                        'type'          => 'link',
                        'label'         => __('Enter URL', 'bb-njba'),
                    ),
                    'redirect_link_target'   => array(
                        'type'          => 'select',
                        'label'         => __('Link Target', 'bb-njba'),
                        'default'       => '_self',
                        'options'       => array(
                            '_self'         => __('Same Window', 'bb-njba'),
                            '_blank'        => __('New Window', 'bb-njba')
                        ),
                        'preview'       => array(
                            'type'          => 'none'
                        )
                    ),
                )
            ),
            'message'    =>  array(
                'title'     => __('Expiry Message Settings', 'bb-njba' ) ,
                'fields'    => array(
                    'message_font_family'       => array(
                        'type'          => 'font',
                        'label'         => __('Font Family', 'bb-njba'),
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 'Default'
                        ),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.njba-info-list-title'
                        )
                    ),
                    'message_font_size'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Font Size', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '',
                            'medium'        => '',
                            'small'         => '',
                        ),
                    ),
                    'message_line_height'    => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Line Height', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '',
                            'medium'        => '',
                            'small'         => '',
                        ),
                    ),
                    'message_color'        => array( 
                        'type'       => 'color',
                        'label' => __('Choose Color', 'bb-njba'),
                        'preview'         => array(
                            'type'            => 'css',
                            'selector'        => '.njba-info-list-title',
                            'property'        => 'color'
                        ),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                )
            ),
        )
    ),
    'style'      => array( // Tab
        'title'         => __('Styling', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'align_section'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array( // Section Fields
                    'counter_alignment' => array(
                        'type'          => 'select',
                        'label'         => __( 'Overall Alignment', 'bb-njba' ),
                        'default'       => 'center',
                        'class'         => '',
                        'options'       => array(
                            'left'             => __('Left', 'bb-njba'),
                            'right'             => __('Right', 'bb-njba'),
                            'center'             => __('Center', 'bb-njba'),
                        ),
                    ),
                    'space_between_unit'   => array(
                        'type'          => 'text',
                        'size'          => '8',
                        'label'         => __('Space Between Timer Unit & Digit', 'bb-njba'),
                        'placeholder'   => '10',
                        'description'   => 'px',
                    ),
                    'timer_out_spacing'       => array(
                        'type'          => 'text',
                        'size'          => '8',
                        'description'   => 'px',
                        'placeholder'   => '10',
                        'label'         => __('Space Between Elements', 'bb-njba'),
                        'help'         => __('This option controls the left-right spacing of each Countdown Element.', 'bb-njba'),
                        'class'         => '',
                    ), 
                )
            ),
            'style'       => array( // Section
                'title'         => __('Timer Digit Styling','bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'timer_style'       => array(
                        'type'          => 'select',
                        'label'         => __('Digit Area Shape', 'bb-njba'),
                        'default'       => 'normal',
                        'class'         => '',
                        'options'       => array(
                            'normal'             => __('Normal', 'bb-njba'),
                            'circle'             => __('Circle', 'bb-njba'),
                            'square'             => __('Square', 'bb-njba'),
                            'custom'             => __('Custom', 'bb-njba')
                        ),
                        'toggle'       => array(
                            
                            'normal' => array(
                                'fields' => array( 'normal_options' ),
                            ),
                            'circle' => array(
                                'fields' => array( 'digit_area_width', 'digit_border_color', 'timer_background_color', 'timer_background_color_opc', 'digit_area_width_desk', 'digit_area_width_med', 'digit_area_width_small', 'digit_border_width', 'digit_border_style', 'unit_position' ),
                            ),
                            'square' => array(
                                'fields' => array( 'digit_area_width', 'digit_border_color', 'timer_background_color', 'timer_background_color_opc', 'digit_area_width_desk', 'digit_area_width_med', 'digit_area_width_small', 'digit_border_width', 'digit_border_style', 'unit_position' ),
                            ),
                            'custom' => array(
                                'fields' => array( 'digit_area_width', 'digit_border_color', 'digit_border_radius', 'timer_background_color', 'timer_background_color_opc', 'digit_area_width_desk', 'digit_area_width_med', 'digit_area_width_small', 'digit_border_width', 'digit_border_style', 'unit_position' ),
                            )
                        )
                    ),
                    'timer_background_img_select'   => array(
                        'type'          => 'select',
                        'label'         => __('Show Background image', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'no',
                        'options'       => array(
                            'no'         => __('No', 'bb-njba'),
                            'yes'        => __('Yes', 'bb-njba')
                        ),
                        'toggle'        => array(
                            'yes'        => array(
                                'fields'    => array('timer_background_img')
                            )
                        ),
                    ),
                    'timer_background_img'         => array(
                        'type'          => 'photo',
                        'label'         => __('Photo', 'bb-njba'),
                        'show_remove'   => true,
                    ),
                    'timer_background_color' => array( 
                        'type'       => 'color',
                        'label'         => __('Digit Background Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    'timer_background_color_opc'     => array(
                        'type'        => 'text',
                        'label'       => __('Digit Background Opacity', 'bb-njba'),
                        'default'     => '100',
                        'description' => '%',
                        'maxlength'   => '3',
                        'size'        => '5',
                    ),
                    'digit_area_width'       => array(
                        'type'          => 'text',
                        'size'          => '8',
                        'description'   => 'px',
                        'placeholder'   => '100',
                        'label'         => __('Digit Area Width', 'bb-njba'),
                        'class'         => '',
                    ),
                    'digit_border_radius'       => array(
                        'type'          => 'text',
                        'size'          => '8',
                        'description'   => 'px',
                        'placeholder'   => '5',
                        'label'         => __('Digit Border Radius', 'bb-njba'),
                        'class'         => '',
                    ),
                    'digit_border_style'   => array(
                        'type'          => 'select',
                        'label'         => __('Digit Border Style', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'solid',
                        'options'       => array(
                            'solid'         => __('Solid', 'bb-njba'),
                            'dashed'        => __('Dashed', 'bb-njba'),
                            'dotted'        => __('Dotted', 'bb-njba')
                        ),
                    ),
                    'digit_border_width'       => array(
                        'type'          => 'text',
                        'size'          => '8',
                        'description'   => 'px',
                        'placeholder'   => '5',
                        'label'         => __('Digit Border Width', 'bb-njba'),
                        'class'         => '',
                    ),
                    'digit_border_color' => array( 
                        'type'       => 'color',
                        'label'         => __('Digit Border Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    'count_animation' => array(
                        'type'          => 'select',
                        'label'         => __( 'On Count Animation', 'bb-njba' ),
                        'default'       => 'none',
                        'class'         => '',
                        'options'       => array(
                            'none'             => __('None', 'bb-njba'),
                            'flash'             => __('Flash', 'bb-njba'),
                            'shake'             => __('Shake', 'bb-njba'),
                            'bounce'             => __('Bounce', 'bb-njba'),
                            'pulse'             => __('Pulse', 'bb-njba')
                        ),
                    ),
                )
            ),
            'timer_string'       => array( // Section
                'title'         => __('Timer Strings','bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'year_string'   => array(
                        'type'          => 'select',
                        'label'         => __('Years', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'Y',
                        'options'       => array(
                            'Y'         => __('Enable','bb-njba'),
                            ''          => __('Disable','bb-njba'),
                        ),
                    ),
                    'year_custom_label'   => array(
                        'type'          => 'select',
                        'label'         => __('Custom Label', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'no',
                        'options'       => array(
                            'yes'       => __('Yes','bb-njba'),
                            'no'        => __('No','bb-njba'),
                        ),
                    ),
                    'year_plural_label'   => array(
                        'type'          => 'text',
                        'label'         => __('Years Label ( Plural )', 'bb-njba'),
                        'placeholder'   => __('Years','bb-njba'),
                        'description'   => '',
                    ),
                    'year_singular_label'   => array(
                        'type'          => 'text',
                        'label'         => __('Year Label ( Singular )', 'bb-njba'),
                        'description'   => '',
                        'placeholder'   => __('Year','bb-njba'),
                    ),
                    
                    // Months
                    'month_string'   => array(
                        'type'          => 'select',
                        'label'         => __('Months', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'O',
                        'options'       => array(
                            'O'         => __('Enable','bb-njba'),
                            ''          => __('Disable','bb-njba'),
                        ),
                    ),
                    'month_custom_label'   => array(
                        'type'          => 'select',
                        'label'         => __('Custom Label', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'no',
                        'options'       => array(
                            'yes'       => __('Yes','bb-njba'),
                            'no'        => __('No','bb-njba'),
                        ),
                    ),
                    'month_plural_label'   => array(
                        'type'          => 'text',
                        'label'         => __('Months Label ( Plural )', 'bb-njba'),
                        'description'   => '',
                        'placeholder'   => __('Months','bb-njba'),
                    ),
                    'month_singular_label'   => array(
                        'type'          => 'text',
                        'label'         => __('Month Label ( Singular )', 'bb-njba'),
                        'description'   => '',
                        'placeholder'   => __('Month','bb-njba'),
                    ),
                    // Days
                    'day_string'   => array(
                        'type'          => 'select',
                        'label'         => __('Days', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'D',
                        'options'       => array(
                            'D'         => __('Enable','bb-njba'),
                            ''          => __('Disable','bb-njba'),
                        ),
                    ),
                    'day_custom_label'   => array(
                        'type'          => 'select',
                        'label'         => __('Custom Label', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'no',
                        'options'       => array(
                            'yes'       => __('Yes','bb-njba'),
                            'no'        => __('No','bb-njba'),
                        ),
                    ),
                    'day_plural_label'   => array(
                        'type'          => 'text',
                        'label'         => __('Days Label ( Plural )', 'bb-njba'),
                        'description'   => '',
                        'placeholder'   => __('Days','bb-njba'),
                    ),
                    'day_singular_label'   => array(
                        'type'          => 'text',
                        'label'         => __('Day Label ( Singular )', 'bb-njba'),
                        'description'   => '',
                        'placeholder'   => __('Day','bb-njba'),
                    ),
                    // Hours
                    'hour_string'   => array(
                        'type'          => 'select',
                        'label'         => __('Hours', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'H',
                        'options'       => array(
                            'H'         => __('Enable','bb-njba'),
                            ''          => __('Disable','bb-njba'),
                        ),
                    ),
                    'hour_custom_label'   => array(
                        'type'          => 'select',
                        'label'         => __('Custom Label', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'no',
                        'options'       => array(
                            'yes'       => __('Yes','bb-njba'),
                            'no'        => __('No','bb-njba'),
                        ),
                    ),
                    'hour_plural_label'   => array(
                        'type'          => 'text',
                        'label'         => __('Hours Label ( Plural )', 'bb-njba'),
                        'description'   => '',
                        'placeholder'   => __('Hours','bb-njba'),
                    ),
                    'hour_singular_label'   => array(
                        'type'          => 'text',
                        'label'         => __('Hour Label ( Singular )', 'bb-njba'),
                        'description'   => '',
                        'placeholder'   => __('Hour','bb-njba'),
                    ),
                    // Minutes
                    'minute_string'   => array(
                        'type'          => 'select',
                        'label'         => __('Minutes', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'M',
                        'options'       => array(
                            'M'         => __('Enable','bb-njba'),
                            ''          => __('Disable','bb-njba'),
                        ),
                    ),
                    'minute_custom_label'   => array(
                        'type'          => 'select',
                        'label'         => __('Custom Label', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'no',
                        'options'       => array(
                            'yes'       => __('Yes','bb-njba'),
                            'no'        => __('No','bb-njba'),
                        ),
                    ),
                    'minute_plural_label'   => array(
                        'type'          => 'text',
                        'label'         => __('Minutes Label ( Plural )', 'bb-njba'),
                        'description'   => '',
                        'placeholder'   => __('Minutes','bb-njba'),
                    ),
                    'minute_singular_label'   => array(
                        'type'          => 'text',
                        'label'         => __('Minute Label ( Singular )', 'bb-njba'),
                        'description'   => '',
                        'placeholder'   => __('Minute','bb-njba'),
                    ),
                    // Seconds
                    'second_string'   => array(
                        'type'          => 'select',
                        'label'         => __('Seconds', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'S',
                        'options'       => array(
                            'S'         => __('Enable','bb-njba'),
                            ''          => __('Disable','bb-njba'),
                        ),
                    ),
                    'second_custom_label'   => array(
                        'type'          => 'select',
                        'label'         => __('Custom Label', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'no',
                        'options'       => array(
                            'yes'       => __('Yes','bb-njba'),
                            'no'        => __('No','bb-njba'),
                        ),
                    ),
                    'second_plural_label'   => array(
                        'type'          => 'text',
                        'label'         => __('Seconds Label ( Plural )', 'bb-njba'),
                        'description'   => '',
                        'placeholder'   => __('Seconds','bb-njba'),
                    ),
                    'second_singular_label'   => array(
                        'type'          => 'text',
                        'label'         => __('Second Label ( Singular )', 'bb-njba'),
                        'description'   => '',
                        'placeholder'   => __('Second','bb-njba'),
                    ),
                )
            ),
        )
    ),
    'countdown_style'       => array( // Tab
        'title'         => __('Typography', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'digit_typography'    =>  array(
                'title'     => __('Timer Digit', 'bb-njba' ) ,
                'fields'    => array(
                    'digit_tag_selection'   => array(
                        'type'          => 'select',
                        'label' => __('Select Tag', 'bb-njba'),
                        'default'   => 'h3',
                        'options'       => array(
                            'h1'      => __('H1', 'bb-njba'),
                            'h2'      => __('H2', 'bb-njba'),
                            'h3'      => __('H3', 'bb-njba'),
                            'h4'      => __('H4', 'bb-njba'),
                            'h5'      => __('H5', 'bb-njba'),
                            'h6'      => __('H6', 'bb-njba'),
                        )
                    ),
                    'digit_font_family'       => array(
                        'type'          => 'font',
                        'label'         => __('Font Family', 'bb-njba'),
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 'Default'
                        ),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.njba-count-down-digit'
                        )
                    ),
                    'digit_font_size'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Font Size', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '',
                            'medium'        => '',
                            'small'         => '',
                        ),
                    ),
                    'digit_line_height'    => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Line Height', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '',
                            'medium'        => '',
                            'small'         => '',
                        ),
                    ),
                    'digit_color'        => array( 
                        'type'       => 'color',
                        'label' => __('Choose Color', 'bb-njba'),
                        'preview'         => array(
                            'type'            => 'css',
                            'selector'        => '.njba-count-down-digit',
                            'property'        => 'color'
                        ),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                )
            ),
            'unit_typography'    =>  array(
                'title'     => __('Timer Unit', 'bb-njba' ),
                'fields'    => array(
                    'unit_tag_selection'   => array(
                        'type'          => 'select',
                        'label' => __('Select Tag', 'bb-njba'),
                        'default'   => 'h3',
                        'options'       => array(
                            'h1'      => __('H1', 'bb-njba'),
                            'h2'      => __('H2', 'bb-njba'),
                            'h3'      => __('H3', 'bb-njba'),
                            'h4'      => __('H4', 'bb-njba'),
                            'h5'      => __('H5', 'bb-njba'),
                            'h6'      => __('H6', 'bb-njba'),
                        )
                    ),
                    'unit_font_family'       => array(
                        'type'          => 'font',
                        'label'         => __('Font Family', 'bb-njba'),
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 'Default'
                        ),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.njba-count-down-unit',
                        )
                    ),
                    'unit_font_size'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Font Size', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '',
                            'medium'        => '',
                            'small'         => '',
                        ),
                    ),
                    'unit_line_height'    => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Line Height', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '',
                            'medium'        => '',
                            'small'         => '',
                        ),
                    ),
                    'unit_color'        => array( 
                        'type'       => 'color',
                        'default'    => '',
                        'show_reset' => true,
                        'label' => __('Choose Color', 'bb-njba'),
                    ),
                )
            ),
        )
    )
));
?>