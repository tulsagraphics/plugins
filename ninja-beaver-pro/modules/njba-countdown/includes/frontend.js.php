<?php
	//Set Labels If User Entered Some
	$fixed_plural_year = ( isset( $settings->year_plural_label ) && $settings->year_plural_label != "" && $settings->year_custom_label == "yes" ) ? $settings->year_plural_label : "Years";
	$fixed_singular_year = ( isset( $settings->year_singular_label ) && $settings->year_singular_label != "" && $settings->year_custom_label == "yes" ) ? $settings->year_singular_label : "Year";
	$fixed_plural_month = ( isset( $settings->month_plural_label ) && $settings->month_plural_label != "" && $settings->month_custom_label == "yes" ) ? $settings->month_plural_label : "Months";
	$fixed_singular_month = ( isset( $settings->month_singular_label ) && $settings->month_singular_label != "" && $settings->month_custom_label == "yes" ) ? $settings->month_singular_label : "Month";
	$fixed_plural_day = ( isset( $settings->day_plural_label ) && $settings->day_plural_label != "" && $settings->day_custom_label == "yes" ) ? $settings->day_plural_label : "Days";
	$fixed_singular_day = ( isset( $settings->day_singular_label ) && $settings->day_singular_label != "" && $settings->day_custom_label == "yes" ) ? $settings->day_singular_label : "Day";
	$fixed_plural_hour = ( isset( $settings->hour_plural_label ) && $settings->hour_plural_label != "" && $settings->hour_custom_label == "yes" ) ? $settings->hour_plural_label : "Hours";
	$fixed_singular_hour = ( isset( $settings->hour_singular_label ) && $settings->hour_singular_label != "" && $settings->hour_custom_label == "yes" ) ? $settings->hour_singular_label : "Hour";
	$fixed_plural_minute = ( isset( $settings->minute_plural_label ) && $settings->minute_plural_label != "" && $settings->minute_custom_label == "yes" ) ? $settings->minute_plural_label : "Minutes";
	$fixed_singular_minute = ( isset( $settings->minute_singular_label ) && $settings->minute_singular_label != "" && $settings->minute_custom_label == "yes" ) ? $settings->minute_singular_label : "Minute";
	$fixed_plural_second = ( isset( $settings->second_plural_label ) && $settings->second_plural_label != "" && $settings->second_custom_label == "yes" ) ? $settings->second_plural_label : "Seconds";
	$fixed_singular_second = ( isset( $settings->second_singular_label ) && $settings->second_singular_label != "" && $settings->second_custom_label == "yes" ) ? $settings->second_singular_label : "Second";
?>
	var default_layout = '';
	<?php if( $settings->timer_type != "evergreen" ) { ?>
	default_layout += '{y<}'+ '<?php echo $module->render_normal_countdown( '{ynn}', '{yl}' ); ?>' +
		'{y>}';
	<?php } ?>
	
	default_layout += '{o<}'+ '<?php echo $module->render_normal_countdown( '{onn}', '{ol}' ); ?>' +
		'{o>}'+
		'{d<}'+ '<?php echo $module->render_normal_countdown( '{dnn}', '{dl}' ); ?>' +
		'{d>}'+
		'{h<}'+ '<?php echo $module->render_normal_countdown( '{hnn}', '{hl}' ); ?>' +
		'{h>}'+
		'{m<}'+ '<?php echo $module->render_normal_countdown( '{mnn}', '{ml}' ); ?>' +
		'{m>}'+
		'{s<}'+ '<?php echo $module->render_normal_countdown( '{snn}', '{sl}' ); ?>' +
		'{s>}';
(function ($) {
	<?php
	if( $settings->timer_type == "evergreen" ) {
		date_default_timezone_set("Asia/Kolkata");
		$todays_date = date( "Y-m-d H:i:s" );
		$todays_date = date( "Y-m-d H:i:s", strtotime( $todays_date . '+'. $settings->evergreen_date_days . ' days' ) );
		$todays_date = date( "Y-m-d H:i:s", strtotime( $todays_date . '+'. $settings->evergreen_date_hour . ' hour') );
		$todays_date = date( "Y-m-d H:i:s", strtotime( $todays_date . '+'. $settings->evergreen_date_minutes . ' minute') );
		$todays_date = date( "Y-m-d H:i:s", strtotime( $todays_date . '+'. $settings->evergreen_date_seconds . ' second') );
	?>
		if ( !$.cookie("countdown-<?php echo $id ; ?>" ) ) {
			$.cookie( "countdown-<?php echo $id ;?>", "<?php echo $todays_date; ?>", { expires: 365 } );
			$.cookie( "countdown-<?php echo $id ;?>-day", "<?php echo $settings->evergreen_date_days; ?>", { expires: 365 } );
			$.cookie( "countdown-<?php echo $id ;?>-hour", "<?php echo $settings->evergreen_date_hour; ?>", { expires: 365 } );
			$.cookie( "countdown-<?php echo $id ;?>-min", "<?php echo $settings->evergreen_date_minutes; ?>", { expires: 365 } );
			$.cookie( "countdown-<?php echo $id ;?>-sec", "<?php echo $settings->evergreen_date_seconds; ?>", { expires: 365 } );
			var ever_green_dates = $.cookie("countdown-<?php echo $id ; ?>" ).split(" ");
			var ever_green_dates_ymd = ever_green_dates[0].split("-");
			var ever_green_dates_y = ever_green_dates_ymd[0];
			var ever_green_dates_m = ever_green_dates_ymd[1];
			var ever_green_dates_d = ever_green_dates_ymd[2];
			var ever_green_dates_hms = ever_green_dates[1].split(":");
			var ever_green_dates_h = ever_green_dates_hms[0];
			var ever_green_dates_i = ever_green_dates_hms[1];
			var ever_green_dates_s = ever_green_dates_hms[2];
		} else if ( ( $.cookie("countdown-<?php echo $id ; ?>-day" ) && $.cookie("countdown-<?php echo $id ; ?>-day" ) != "<?php echo $settings->evergreen_date_days; ?>" ) || ( $.cookie("countdown-<?php echo $id ; ?>-hour" ) && $.cookie("countdown-<?php echo $id ; ?>-hour" ) != "<?php echo $settings->evergreen_date_hour;?>" ) || ( $.cookie("countdown-<?php echo $id ; ?>-min" ) && $.cookie("countdown-<?php echo $id ; ?>-min" ) != "<?php echo $settings->evergreen_date_minutes;?>" ) || ( $.cookie("countdown-<?php echo $id ; ?>-sec" ) && $.cookie("countdown-<?php echo $id ; ?>-sec" ) != "<?php echo $settings->evergreen_date_seconds;?>" ) ) {
			$.cookie( "countdown-<?php echo $id ;?>", "<?php echo $todays_date; ?>", { expires: 365 } );
			$.cookie( "countdown-<?php echo $id ;?>-day", "<?php echo $settings->evergreen_date_days; ?>", { expires: 365 } );
			$.cookie( "countdown-<?php echo $id ;?>-hour", "<?php echo $settings->evergreen_date_hour; ?>", { expires: 365 } );
			$.cookie( "countdown-<?php echo $id ;?>-min", "<?php echo $settings->evergreen_date_minutes; ?>", { expires: 365 } );
			$.cookie( "countdown-<?php echo $id ;?>-sec", "<?php echo $settings->evergreen_date_seconds; ?>", { expires: 365 } );
			var ever_green_dates = $.cookie("countdown-<?php echo $id ; ?>" ).split(" ");
			var ever_green_dates_ymd = ever_green_dates[0].split("-");
			var ever_green_dates_y = ever_green_dates_ymd[0];
			var ever_green_dates_m = ever_green_dates_ymd[1];
			var ever_green_dates_d = ever_green_dates_ymd[2];
			var ever_green_dates_hms = ever_green_dates[1].split(":");
			var ever_green_dates_h = ever_green_dates_hms[0];
			var ever_green_dates_i = ever_green_dates_hms[1];
			var ever_green_dates_s = ever_green_dates_hms[2];
		} else {
			var ever_green_dates = $.cookie("countdown-<?php echo $id ; ?>" ).split(" ");
			var ever_green_dates_ymd = ever_green_dates[0].split("-");
			var ever_green_dates_y = ever_green_dates_ymd[0];
			var ever_green_dates_m = ever_green_dates_ymd[1];
			var ever_green_dates_d = ever_green_dates_ymd[2];
			var ever_green_dates_hms = ever_green_dates[1].split(":");
			var ever_green_dates_h = ever_green_dates_hms[0];
			var ever_green_dates_i = ever_green_dates_hms[1];
			var ever_green_dates_s = ever_green_dates_hms[2];
		}
		var moduleDay = new Date( ever_green_dates_y, ever_green_dates_m - 1, ever_green_dates_d, ever_green_dates_h, ever_green_dates_i, ever_green_dates_s );
	<?php
	} else {
	?>
	var moduleDay = new Date( "<?php if( isset( $settings->fixed_date_year ) ){ echo $settings->fixed_date_year; } ?>", "<?php if( isset( $settings->fixed_date_month ) ) { echo $settings->fixed_date_month - 1; } ?>", "<?php if( isset( $settings->fixed_date_days ) ) { echo $settings->fixed_date_days; } ?>", "<?php if( isset( $settings->fixed_date_hour ) ) { echo $settings->fixed_date_hour; } ?>", "<?php if( isset( $settings->fixed_date_minutes ) ) { echo $settings->fixed_date_minutes; } ?>" );
	<?php
	}
	?>
	new NJBACountdown({
		id: '<?php echo $id; ?>',
		fixed_timer_action: '<?php echo $settings->fixed_timer_action; ?>',
		evergreen_timer_action: '<?php echo $settings->evergreen_timer_action; ?>',
		timertype: '<?php echo $settings->timer_type; ?>',
		timer_date: moduleDay,
		timer_format: '<?php if( isset( $settings->year_string ) ){ echo $settings->year_string; }?><?php if( isset( $settings->month_string ) ){ echo $settings->month_string; }?><?php if( isset( $settings->day_string ) ){ echo $settings->day_string; }?><?php if( isset( $settings->hour_string ) ){ echo $settings->hour_string; }?><?php if( isset( $settings->minute_string ) ){ echo $settings->minute_string; }?><?php if( isset( $settings->second_string ) ){ echo $settings->second_string; }?>',
		timer_layout: default_layout,
		redirect_link_target: '<?php echo ( $settings->redirect_link_target != '' ) ? $settings->redirect_link_target : ''; ?>',
		redirect_link: '<?php echo ( $settings->redirect_link != '' ) ? $settings->redirect_link : ''; ?>',
		expire_message: '<?php echo ( $settings->expire_message != '' ) ? preg_replace('/\s+/', ' ', $settings->expire_message) : ''; ?>',
		timer_labels: '<?php echo $fixed_plural_year; ?>,<?php echo $fixed_plural_month; ?>,,<?php echo $fixed_plural_day; ?>,<?php echo $fixed_plural_hour; ?>,<?php echo $fixed_plural_minute; ?>,<?php echo $fixed_plural_second; ?>',
		timer_labels_singular: 	'<?php echo $fixed_singular_year; ?>,<?php echo $fixed_singular_month; ?>,<?php echo $fixed_singular_day; ?>,<?php echo $fixed_singular_hour; ?>,<?php echo $fixed_singular_minute; ?>,<?php echo $fixed_singular_second; ?>',

		evergreen_date_days: '<?php echo isset( $settings->evergreen_date_days ) ? $settings->evergreen_date_days : ''; ?>',
		evergreen_date_hour: '<?php echo isset( $settings->evergreen_date_hours ) ? $settings->evergreen_date_hour : ''; ?>',
		evergreen_date_minutes: '<?php echo isset( $settings->evergreen_date_minutes ) ? $settings->evergreen_date_minutes : ''; ?>',
		evergreen_date_seconds: '<?php echo isset( $settings->evergreen_date_seconds ) ? $settings->evergreen_date_seconds : ''; ?>',

		<?php if( isset( $settings->fixed_timer_action ) && $settings->fixed_timer_action == "msg"){ ?>
		timer_exp_text: '<div class="njba-countdown-expire-message">'+ $.cookie( "countdown-<?php echo $id ;?>expiremsg" ) +'</div>'
		<?php } ?>
	});
})(jQuery);