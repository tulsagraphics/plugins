<?php
/**
 * @class NJBAContentGridModule
 */
class NJBAContentTilesModule extends FLBuilderModule {
    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Content Tiles', 'bb-njba'),
            'description'   => __('Display posts in various tile layouts.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'content' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-content-tiles/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-content-tiles/',
            'editor_export'     => false,
            'partial_refresh'   => true
        ));
        /**
         * Use these methods to enqueue css and js already
         * registered or to register and enqueue your own.
         */
        // Already registered
        add_action( 'wp_ajax_ct_get_post_tax', array( $this, 'get_post_taxonomies' ) );
        add_action( 'wp_ajax_nopriv_ct_get_post_tax', array( $this, 'get_post_taxonomies' ) );
       
    }
    /**
     * Get taxonomies
     */
  
    public function get_post_taxonomies()
    {
        $slug = sanitize_text_field( $_POST['post_type_slug'] );
        $taxonomies = FLBuilderLoop::taxonomies($slug);
        $html = '';
        $html .= '<option value="none">'. __('None', 'bb-njba') .'</option>';
        foreach ( $taxonomies as $tax_slug => $tax ) {
            $html .= '<option value="'.$tax_slug.'">'.$tax->label.'</option>';
        }
        echo $html;
    }
    /**
     * Renders the schema structured data for the current
     * post in the loop.
     *
     * @since 1.7.4
     * @return void
     */
    static public function njba_schema_meta()
    {
        // General Schema Meta
        echo '<meta itemscope itemprop="mainEntityOfPage" itemid="' . get_permalink() . '" />';
        echo '<meta itemprop="datePublished" content="' . get_the_time('Y-m-d') . '" />';
        echo '<meta itemprop="dateModified" content="' . get_the_modified_date('Y-m-d') . '" />';
        // Publisher Schema Meta
        echo '<div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">';
        echo '<meta itemprop="name" content="' . get_bloginfo( 'name' ) . '">';
        if ( class_exists( 'FLTheme' ) && 'image' == FLTheme::get_setting( 'fl-logo-type' ) ) {
            echo '<div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">';
            echo '<meta itemprop="url" content="' . FLTheme::get_setting( 'fl-logo-image' ) . '">';
            echo '</div>';
        }
        echo '</div>';
        // Author Schema Meta
        echo '<div itemscope itemprop="author" itemtype="http://schema.org/Person">';
        echo '<meta itemprop="url" content="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '" />';
        echo '<meta itemprop="name" content="' . get_the_author_meta( 'display_name', get_the_author_meta( 'ID' ) ) . '" />';
        echo '</div>';
        // Image Schema Meta
        if(has_post_thumbnail()) {
            $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full');
            if ( is_array( $image ) ) {
                echo '<div itemscope itemprop="image" itemtype="http://schema.org/ImageObject">';
                echo '<meta itemprop="url" content="' . $image[0] . '" />';
                echo '<meta itemprop="width" content="' . $image[1] . '" />';
                echo '<meta itemprop="height" content="' . $image[2] . '" />';
                echo '</div>';
            }
        }
        // Comment Schema Meta
        echo '<div itemprop="interactionStatistic" itemscope itemtype="http://schema.org/InteractionCounter">';
        echo '<meta itemprop="interactionType" content="http://schema.org/CommentAction" />';
        echo '<meta itemprop="userInteractionCount" content="' . wp_count_comments(get_the_ID())->approved . '" />';
        echo '</div>';
    }
    /**
     * Renders the schema itemtype for the current
     * post in the loop.
     *
     * @since 1.7.4
     * @return void
     */
    static public function njba_schema_itemtype()
    {
        global $post;
        if ( ! is_object( $post ) || ! isset( $post->post_type ) || 'post' != $post->post_type ) {
            echo 'http://schema.org/CreativeWork';
        }
        else {
            echo 'http://schema.org/BlogPosting';
        }
    }
    static public function njba_get_post_class( $count, $layout )
    {
        if ( $layout == 1 && $count == 2 ||  $layout == 3 && $count > 1  || $layout == 4 && $count > 1) {
            return 'njba-col-12';
        }
        
        if ( $layout != 3  && $count > 2  ) {
            return 'njba-col-6';
        }
        if ( $layout == 2  && $count > 1) {
            return 'njba-col-6';
        }
        
    }
    
}
/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('NJBAContentTilesModule', array(
            'layout'    => array(
            'title'     => __('Layout', 'bb-njba'),
            'sections'  => array(
                'layout'    => array(
                    'title'     => '',
                    'fields'    => array(
                        'layout'    => array(
                            'type'      => 'layout',
                            'default'   => 1,
                            'options'   => array(
                                1           => NJBA_MODULE_URL . 'modules/njba-content-tiles/images/layout-1.jpg',
                                2           => NJBA_MODULE_URL . 'modules/njba-content-tiles/images/layout-2.jpg',
                                3           => NJBA_MODULE_URL . 'modules/njba-content-tiles/images/layout-3.jpg',
                                4           => NJBA_MODULE_URL . 'modules/njba-content-tiles/images/layout-4.jpg',
                            ),
                            'toggle'    => array(
                                1           => array(
                                    'sections'  => array('small_grid')
                                ),
                                2           => array(
                                    'sections'  => array('small_grid')
                                ),
                                4           => array(
                                    'sections'  => array('small_grid')
                                )
                            )
                        )
                    )
                )
            )
        ),
        'content'   => array(
        'title'         => __('Content', 'bb-njba'),
            'file'          => plugin_dir_path( __FILE__ ) . 'includes/loop-settings.php',
        ),
		'style'      => array( // Tab
        'title'         => __('Style', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'style'       => array( // Section
                'title'         => __('Taxonomy Settings', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    
                    'tax_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Text Color', 'bb-njba'),
                        'show_reset'    => true,
                         'default'      => 'ffffff', 
                    ),
                    'tax_hover_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Hover Text Color', 'bb-njba'),
                        'show_reset'    => true,
                        'default'      => 'ffffff',   
                    ),
                    'tax_bg_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Background Color', 'bb-njba'),
                        'show_reset'    => true,
                         'default'      => '000000',
                    ),
                    'tax_bg_hover_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Hover Background Color', 'bb-njba'),
                        'show_reset'    => true,
                        'default'      => '000000',   
                    ),
                    'tax_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'            => 5,
                            'right'          => 5,
                            'bottom'        => 5,
                            'left'          => 5
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-left'
                            )
                            
                        ),
                    ),
                  
                   
               )
            ),
            'grid'       => array( // Section
                'title'         => __('Grid', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'post_height'  => array(
                        'type'          => 'text',
                        'label'         => __('Height', 'bb-njba'),
                        'default'       => '470',
                        'maxlength'     => '3',
                        'size'          => '4',
                        'description'   => 'px'
                    ),
                    'post_spacing'  => array(
                        'type'          => 'text',
                        'label'         => __('Post Spacing', 'bb-njba'),
                        'default'       => '10',
                        'maxlength'     => '3',
                        'size'          => '4',
                        'description'   => 'px'
                    ),
                 )
            ),
            
        )
    ),
	
    'typography'      => array( // Tab
		'title'         => __('Typography', 'bb-njba'), // Tab title
		'sections'      => array( // Tab Sections
			'general'       => array( // Section
				'title'         =>  __('Post Title', 'bb-njba'), // Section Title
				'fields'        => array( // Section Fields
					'post_title_tag'   => array(
                        'type'          => 'select',
                        'label'         => __('Tag', 'bb-njba'),
                        'default'       => 'h2',
                        'options'       => array(
                            'h1'      => __('H1', 'bb-njba'),
                            'h2'      => __('H2', 'bb-njba'),
                            'h3'      => __('H3', 'bb-njba'),
                            'h4'      => __('H4', 'bb-njba'),
                            'h5'      => __('H5', 'bb-njba'),
                            'h6'      => __('H6', 'bb-njba'),
                            'div'     => __('Div', 'bb-njba'),
                            'p'       => __('p', 'bb-njba'),
                            'span'    => __('span', 'bb-njba'),
                        )
                    ),
                    'post_title_alignment'         => array(
						'type'                      => 'select',
						'default'                   => 'left',
						'label'                     => __('Alignment', 'bb-njba'),
                        'options'                   => array(
                            'left'                      => __('Left', 'bb-njba'),
                            'right'                     => __('Right', 'bb-njba'),
                            'center'                    => __('Center', 'bb-njba'),
                        ),
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.njba-post-tile-post .njba-post-tile-title',
                            'property'      => 'text-align'
						)
					),
                    'post_text_transform'  => array(
                        'type'                  => 'select',
                        'label'                 => __('Text Transform', 'bb-njba'),
                        'default'               => 'none',
                        'options'               => array(
                            'none'                  => __('None', 'bb-njba'),
                            'capitalize'            => __('Capitalize', 'bb-njba'),
                            'lowercase'             => __('lowercase', 'bb-njba'),
                            'uppercase'             => __('UPPERCASE', 'bb-njba'),
                        )
                    ),
                    'post_title_font'          => array(
                        'type'          => 'font',
                        'default'		=> array(
                            'family'		=> 'Default',
                            'weight'		=> 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.njba-post-tile-post .njba-post-tile-title'
                        )
                    ),
                   'post_title_font_size'    => array(
					   'type'          => 'njba-simplify',
                       'label'         => __( 'Font Size', 'bb-njba' ),
                       'default'       => array(
                            'desktop' => '18',
                            'medium' => '16',
                            'small' => '12'
                        ),
						'maxlength'     => '3',
                        'size'          => '5', 
                        'description'       => 'px', 
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-post-tile-post .njba-post-tile-title',
                            'property'      => 'font-size',
                            'unit'          => 'px'
                        )
					),
                   'post_title_height'    => array(
                        'type'          => 'njba-simplify',
                       'label'         => __( 'Line Height', 'bb-njba' ),
                       'default'       => array(
                            'desktop' => '18',
                            'medium' => '16',
                            'small' => '12'
                        ),
                        'maxlength'     => '3',
                        'size'          => '5', 
                        'description'       => 'px', 
                     ),
                    'post_title_color'    => array(
						'type'          => 'color',
						'label'         => __('Color', 'bb-njba'),
						'default'       => 'ffffff',
						'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-post-tile-post .njba-post-tile-title',
                            'property'      => 'color',
                        )
					),
					'post_title_hover_color'    => array(
						'type'          => 'color',
						'label'         => __('Hover Color', 'bb-njba'),
						'default'       => 'ffffff',
						'show_reset'    => true,
					),
                    'post_title_margin'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'            => '',
                            'right'          => '',
                            'bottom'        => '',
                            'left'          => ''
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-left'
                            )
                            
                        ),
                    ),
				)
			), // Section
            'small_grid'    => array(
                'title'         => __('Small Grid', 'bb-njba'),
                'fields'        => array(
                        'small_font_size'    => array(
                           'type'          => 'njba-simplify',
                           'label'         => __( 'Font Size', 'bb-njba' ),
                           'default'       => array(
                                'desktop' => '12',
                                'medium' => '12',
                                'small' => '12'
                            ),
                            'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
                            'preview'       => array(
                                'type'          => 'css',
                                'selector'      => '<?php echo $settings->post_title_tag; ?>.njba-post-tile-title a',
                                'property'      => 'font-size',
                                'unit'          => 'px'
                            )
                        ),
                       'small_height'    => array(
                            'type'          => 'njba-simplify',
                           'label'         => __( 'Line Height', 'bb-njba' ),
                           'default'       => array(
                                'desktop' => '15',
                                'medium' => '15',
                                'small' => '15'
                            ),
                            'maxlength'     => '3',
                            'size'          => '5', 
                            'description'       => 'px',
                            'preview'       => array(
                                'type'          => 'css',
                                'selector'      => '<?php echo $settings->post_title_tag; ?>.njba-post-tile-title a',
                                'property'      => 'line-height',
                                'unit'          => 'px'
                            ) 
                         ),
                    ),
                
            ),
			'meta'       => array( // Section
				'title'         =>  __('Meta', 'bb-njba'), // Section Title
				'fields'        => array( // Section Fields
					'meta_alignment'         => array(
						'type'                      => 'select',
						'default'                   => 'left',
						'label'                     => __('Alignment', 'bb-njba'),
                        'options'                   => array(
                            'left'                      => __('Left', 'bb-njba'),
                            'right'                     => __('Right', 'bb-njba'),
                            'center'                    => __('Center', 'bb-njba'),
                        ),
					),
                    'meta_transform'  => array(
                        'type'                  => 'select',
                        'label'                 => __('Text Transform', 'bb-njba'),
                        'default'               => 'none',
                        'options'               => array(
                            'none'                  => __('None', 'bb-njba'),
                            'capitalize'            => __('Capitalize', 'bb-njba'),
                            'lowercase'             => __('lowercase', 'bb-njba'),
                            'uppercase'             => __('UPPERCASE', 'bb-njba'),
                        ),
                    ),
                    'meta_font'          => array(
                        'type'          => 'font',
                        'default'		=> array(
                            'family'		=> 'Default',
                            'weight'		=> 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.njba-post-tile-meta span'
                        )
                    ),
                   'meta_font_size'    => array(
					   'type'          => 'njba-simplify',
                       'label'         => __( 'Font Size', 'bb-njba' ),
                       'default'       => array(
                            'desktop' => '12',
                            'medium' => '12',
                            'small' => '12'
                        ),
						'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-post-tile-meta span',
                            'property'      => 'font-size',
                            'unit'          => 'px'
                        )
					),
                   'meta_height'    => array(
                        'type'          => 'njba-simplify',
                       'label'         => __( 'Line Height', 'bb-njba' ),
                       'default'       => array(
                            'desktop' => '15',
                            'medium' => '15',
                            'small' => '15'
                        ),
                        'maxlength'     => '3',
                        'size'          => '5', 
                        'description'       => 'px', 
                     ),
                    'meta_color'    => array(
						'type'          => 'color',
						'label'         => __('Color', 'bb-njba'),
						'default'       => 'FFFFFF',
						'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-post-tile-meta span',
                            'property'      => 'color',
                        )
					),
                    'meta_margin'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'            => '',
                            'right'          => '',
                            'bottom'        => '',
                            'left'          => ''
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-left'
                            )
                            
                        ),
                    ),
					
				)
			), // Section
			
		)
	),
	
    
));
