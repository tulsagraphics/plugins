<?php
FLBuilderModel::default_settings($settings, array(
	'post_type' 			=> 'post',
	'order_by'  			=> 'date',
	'order'     			=> 'DESC',
	'offset'    			=> 0,
	'no_results_message'	=> __('No result found.', 'bb-njba'),
	'users'     			=> '',
	'show_author'			=> '1',
	'show_date'				=> '1',
	'date_format'			=> 'default',
	'show_post_taxonomies'	=> '1',
	'post_taxonomies'		=> 'category',
	'meta_separator'		=> ' / ',
	'title_margin'			=> array(
		'top'					=> '0',
		'bottom'				=> '0'
	)
));
?>							
					 
	<div class="njba-post-tile-post njba-post-tile-post-<?php echo $count;?> <?php echo NJBAContentTilesModule::njba_get_post_class($count, $settings->layout);?>" itemscope itemtype="<?php NJBAContentTilesModule::njba_schema_itemtype(); ?>">
		<?php NJBAContentTilesModule::njba_schema_meta(); ?>
		<?php	if ( has_post_thumbnail() ) :
				
					$image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'large'); 
		?>
				<div class="njba-post-tile-image" style="background-image: url('<?php echo $image[0];?>')">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"></a>
				</div>
		<?php   else: ?>
				<div class="njba-post-tile-image" style="background-image: url(<?php echo $module->url;?>images/placeholder.jpg)"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"></a></div>
				
		    <?php   endif; ?>
		    	<div class="njba-post-tile-text">
					<div class="njba-post-tile-info">
						<?php
							if ( $settings->show_post_taxonomies == '1' && $settings->post_taxonomies != 'none' ) {
								$terms = wp_get_post_terms( get_the_ID(), $settings->post_taxonomies );
								$show_terms = array();
								foreach ( $terms as $term ) {
									$show_terms[] = $term->name;
								}
						?>
							<div class="njba-post-tile-category"><span><?php echo implode( $settings->meta_separator, $show_terms ); ?></span></div>
						<?php } ?>
						<<?php echo $settings->post_title_tag; ?> class="njba-post-tile-title" itemprop="headline">
							<?php the_title(); ?>
						</<?php echo $settings->post_title_tag; ?>>
					</div>
					<?php if($settings->show_author || $settings->show_date) : ?>
					<div class="njba-post-tile-meta">
						<?php if($settings->show_author && $count == 1) : ?>
							<span class="njba-post-tile-author">
							<?php
							printf(
								_x( '%s', '%s stands for author name.', 'bb-njba' ),
								'<a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '"><span>' . get_the_author_meta( 'display_name', get_the_author_meta( 'ID' ) ) . '</span></a>'
							);
							?>
							</span>
						<?php endif; ?>
						<?php if($settings->show_date && $count == 1) : ?>
							<?php if($settings->show_author) : ?>
								<span class="njba-meta-separator"> <?php echo $settings->meta_separator; ?> </span>
							<?php endif; ?>
							<span class="njba-post-tile-date">
								<?php FLBuilderLoop::post_date($settings->date_format); ?>
							</span>
						<?php endif; ?>
					</div>
					<?php endif; ?>
				</div>
				
	    </div>
					 
				
