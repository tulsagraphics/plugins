<?php
$percent = 60;
if ( $settings->layout > 1 ) {
	$percent = 50;
}
?>
.fl-node-<?php echo $id; ?> .njba-post-tile-post {
	margin-right: <?php echo $settings->post_spacing; ?>px;
	margin-bottom: <?php echo $settings->post_spacing; ?>px;
	position: relative;
	overflow: hidden;
	height: <?php echo $settings->post_height + $settings->post_spacing; ?>px;
}
.fl-node-<?php echo $id; ?> .njba-post-tile-left,
.fl-node-<?php echo $id; ?> .njba-post-tile-right {
	float: left;
	width: 50%;
}
<?php if ( $settings->layout == 4 ) { ?>
	.fl-node-<?php echo $id; ?> .njba-post-tile-left {
		width: 75%;
	}
	.fl-node-<?php echo $id; ?> .njba-post-tile-right {
		width: 25%;
	}
<?php } ?>
.fl-node-<?php echo $id; ?> .njba-col-12 {
	height: <?php echo ($settings->post_height * $percent) / 100; ?>px;
}
.fl-node-<?php echo $id; ?> .njba-col-6 {
	float: left;
	height: <?php echo $settings->post_height - (($settings->post_height * $percent) / 100); ?>px;
	width: calc(50% - <?php echo $settings->post_spacing; ?>px);
	<?php if ( $settings->layout == 4 ) { ?>
	width: 100%;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-post-tile-title{
	<?php if ( $settings->post_title_font['family'] != 'Default' ) { ?> <?php FLBuilderFonts::font_css( $settings->post_title_font ); ?><?php } ?>
   	<?php if ( $settings->post_title_font_size['desktop'] != '') { ?> font-size: <?php echo $settings->post_title_font_size['desktop']; ?>px;<?php } else{ ?> font-size: 18px;<?php } ?>
   	<?php if ( $settings->post_title_height['desktop'] != '') { ?> line-height: <?php echo $settings->post_title_height['desktop']; ?>px;<?php } else{ ?> line-height: 22px;<?php } ?>
	<?php if ( $settings->post_title_color != '') { ?> color: #<?php echo $settings->post_title_color; ?>;<?php } else{?> color: #ffffff;<?php } ?>
	<?php if ( $settings->post_text_transform != 'none') { ?> text-transform: <?php echo $settings->post_text_transform; ?>;<?php } else{?>     text-transform: none;<?php } ?>
	
	<?php if ( $settings->post_title_margin['top'] !='' ) { ?>margin-top: <?php echo $settings->post_title_margin['top']; ?>px;<?php } else{?> margin-top: 20px;<?php } ?>
	<?php if ( $settings->post_title_margin['bottom'] !='' ) { ?>margin-bottom: <?php echo $settings->post_title_margin['bottom']; ?>px;<?php }else{?> margin-bottom: 20px;<?php } ?>
	<?php if ( $settings->post_title_margin['left'] !='' ) { ?>margin-left: <?php echo $settings->post_title_margin['left']; ?>px;<?php } else{?> margin-left: 0px;<?php } ?>
	<?php if ( $settings->post_title_margin['right'] !='' ) { ?>margin-right: <?php echo $settings->post_title_margin['right']; ?>px;<?php }else{?> margin-right: 0px;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-post-tile-post:hover .njba-post-tile-title{
	<?php if ( $settings->post_title_hover_color != '') { ?> color: #<?php echo $settings->post_title_hover_color; ?>;<?php } else{ ?> color: #ffffff;<?php }?>
}
.fl-node-<?php echo $id; ?> .njba-col-6 .njba-post-tile-title {
	<?php if ( $settings->small_font_size['desktop'] != '') { ?> font-size: <?php echo $settings->small_font_size['desktop']; ?>px;<?php } else{ ?> font-size: 18px;<?php } ?>
   	<?php if ( $settings->small_height['desktop'] != '') { ?> line-height: <?php echo $settings->small_height['desktop']; ?>px;<?php } else{ ?> line-height: 22px;<?php } ?>
	
}
.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-post-tile-category span {
	display: inline-block;
	<?php if ( $settings->meta_font['family'] != 'Default' ) { ?> <?php FLBuilderFonts::font_css( $settings->meta_font ); ?><?php } ?>
   	<?php if ( $settings->meta_font_size['desktop'] != '') { ?> font-size: <?php echo $settings->meta_font_size['desktop']; ?>px;<?php } else{ ?> font-size: 12px;<?php } ?>
   	<?php if ( $settings->meta_height['desktop'] != '') { ?> line-height: <?php echo $settings->meta_height['desktop']; ?>px;<?php } else{ ?> line-height: 18px;<?php } ?>
	<?php if ( $settings->tax_color != '') { ?> color: #<?php echo $settings->tax_color; ?>;<?php } else{?> color: #ffffff;<?php } ?>
	<?php if ( $settings->meta_transform != 'none') { ?> text-transform: <?php echo $settings->meta_transform; ?>;<?php } else{?>     text-transform: none;<?php } ?>
	<?php if ( $settings->tax_bg_color != '') { ?> background: #<?php echo $settings->tax_bg_color; ?>;<?php } ?>
	<?php if ( $settings->tax_padding['top'] !='' ) { ?>padding-top: <?php echo $settings->tax_padding['top']; ?>px;<?php } else{?> padding-top: 15px;<?php } ?>
	<?php if ( $settings->tax_padding['bottom'] !='' ) { ?>padding-bottom: <?php echo $settings->tax_padding['bottom']; ?>px;<?php }else{?> padding-bottom: 15px;<?php } ?>
	<?php if ( $settings->tax_padding['left'] !='' ) { ?>padding-left: <?php echo $settings->tax_padding['left']; ?>px;<?php } else{?> padding-left: 0px;<?php } ?>
	<?php if ( $settings->tax_padding['right'] !='' ) { ?>padding-right: <?php echo $settings->tax_padding['right']; ?>px;<?php }else{?> padding-right: 0px;<?php } ?>
	
}
.fl-node-<?php echo $id; ?> .njba-post-tile-post:hover .njba-post-tile-category span {
	<?php if ( $settings->tax_hover_color != '') { ?> color: #<?php echo $settings->tax_hover_color; ?>;<?php } else{?> color: #ffffff;<?php } ?>
	<?php if ( $settings->tax_bg_hover_color != '') { ?> background: #<?php echo $settings->tax_bg_hover_color; ?>;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-post-tile-meta {
	<?php if ( $settings->meta_alignment ) { ?>text-align: <?php echo $settings->meta_alignment; ?>;<?php }  ?>
	<?php if ( $settings->meta_margin['top'] !='' ) { ?>margin-top: <?php echo $settings->meta_margin['top']; ?>px;<?php } else{?> margin-top: 20px;<?php } ?>
	<?php if ( $settings->meta_margin['bottom'] !='' ) { ?>margin-bottom: <?php echo $settings->meta_margin['bottom']; ?>px;<?php }else{?> margin-bottom: 20px;<?php } ?>
	<?php if ( $settings->meta_margin['left'] !='' ) { ?>margin-left: <?php echo $settings->meta_margin['left']; ?>px;<?php } else{?> margin-left: 20px;<?php } ?>
	<?php if ( $settings->meta_margin['right'] !='' ) { ?>margin-right: <?php echo $settings->meta_margin['right']; ?>px;<?php }else{?> margin-right: 20px;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-post-tile-author a {
		<?php if ( $settings->meta_color != '') { ?> color: #<?php echo $settings->meta_color; ?>;<?php } else{?> color: #ffffff;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-post-tile-author,
.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-post-tile-date,
.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-meta-separator {
	display: inline-block;
    position: relative;
	top: 0;
	<?php if ( $settings->meta_font['family'] != 'Default' ) { ?> <?php FLBuilderFonts::font_css( $settings->meta_font ); ?><?php } ?>
   	<?php if ( $settings->meta_font_size['desktop'] != '') { ?> font-size: <?php echo $settings->meta_font_size['desktop']; ?>px;<?php } else{ ?> font-size: 12px;<?php } ?>
   	<?php if ( $settings->meta_height['desktop'] != '') { ?> line-height: <?php echo $settings->meta_height['desktop']; ?>px;<?php } else{ ?> line-height: 18px;<?php } ?>
   	<?php if ( $settings->meta_transform != 'none') { ?> text-transform: <?php echo $settings->meta_transform; ?>;<?php } else{?>     text-transform: none;<?php } ?>
   	<?php if ( $settings->meta_color != '') { ?> color: #<?php echo $settings->meta_color; ?>;<?php } else{?> color: #000000;<?php } ?>
}
@media only screen and (max-width: 991px) {
	
	.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-post-tile-title{
		<?php if ( $settings->post_title_font_size['medium'] != '') { ?> font-size: <?php echo $settings->post_title_font_size['medium']; ?>px;<?php } ?>
	   	<?php if ( $settings->post_title_height['medium'] != '') { ?> line-height: <?php echo $settings->post_title_height['medium']; ?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-col-6 .njba-post-tile-title {
		<?php if ( $settings->small_font_size['medium'] != '') { ?> font-size: <?php echo $settings->small_font_size['medium']; ?>px;<?php } ?>
	   	<?php if ( $settings->small_height['medium'] != '') { ?> line-height: <?php echo $settings->small_height['medium']; ?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-post-tile-category span {
		<?php if ( $settings->meta_font_size['medium'] != '') { ?> font-size: <?php echo $settings->meta_font_size['medium']; ?>px;<?php } ?>
	   	<?php if ( $settings->meta_height['medium'] != '') { ?> line-height: <?php echo $settings->meta_height['medium']; ?>px;<?php } ?>
		
	}
	.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-post-tile-author,
	.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-post-tile-date,
	.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-meta-separator {
		<?php if ( $settings->meta_font_size['medium'] != '') { ?> font-size: <?php echo $settings->meta_font_size['medium']; ?>px;<?php } ?>
	   	<?php if ( $settings->meta_height['medium'] != '') { ?> line-height: <?php echo $settings->meta_height['medium']; ?>px;<?php } ?>
	   
	}
}
@media only screen and (max-width: 767px) {
	
	.fl-node-<?php echo $id; ?> .njba-post-tile-left,
	.fl-node-<?php echo $id; ?> .njba-post-tile-right {
		float: left;
		width: 100%;
	}
	.fl-node-<?php echo $id; ?> .njba-col-6 {
		width: 100%;
		
	}
	.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-post-tile-title{
		<?php if ( $settings->post_title_font_size['small'] != '') { ?> font-size: <?php echo $settings->post_title_font_size['small']; ?>px;<?php } ?>
	   	<?php if ( $settings->post_title_height['small'] != '') { ?> line-height: <?php echo $settings->post_title_height['small']; ?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-col-6 .njba-post-tile-title {
		<?php if ( $settings->small_font_size['small'] != '') { ?> font-size: <?php echo $settings->small_font_size['small']; ?>px;<?php } ?>
	   	<?php if ( $settings->small_height['small'] != '') { ?> line-height: <?php echo $settings->small_height['small']; ?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-post-tile-category span {
		<?php if ( $settings->meta_font_size['small'] != '') { ?> font-size: <?php echo $settings->meta_font_size['small']; ?>px;<?php } ?>
	   	<?php if ( $settings->meta_height['small'] != '') { ?> line-height: <?php echo $settings->meta_height['small']; ?>px;<?php } ?>
		
	}
	.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-post-tile-author,
	.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-post-tile-date,
	.fl-node-<?php echo $id; ?> .njba-post-tile-post .njba-meta-separator {
		<?php if ( $settings->meta_font_size['small'] != '') { ?> font-size: <?php echo $settings->meta_font_size['small']; ?>px;<?php } ?>
	   	<?php if ( $settings->meta_height['small'] != '') { ?> line-height: <?php echo $settings->meta_height['small']; ?>px;<?php } ?>
	   
	}
}
