
<?php
$number_gallery = count($settings->gallery);
	for($i=0; $i < $number_gallery; $i++) :
		
		$polaroid_rotate_id = $id.' .njba-out-side-unique-'.$i;
		$gallery = $settings->gallery[$i];
	
		$polaroid_rotate_css_array = array(
       						'background_color'                   => $settings->background_color,
            'background_color_opacity'           => $settings->background_color_opacity,
            'background_hover_color'                   => $settings->background_hover_color,
            'background_hover_color_opacity'                   => $settings->background_hover_color_opacity,
            'box_border_style'                   => $settings->box_border_style,
            'box_border_width'                   => $settings->box_border_width,
            'box_border_radius'                   => $settings->box_border_radius,
            'box_border_color'                   => $settings->box_border_color,
            'box_border_hover_color'                   => $settings->box_border_hover_color,
            'box_shadow'                   => $settings->box_shadow,
            'box_shadow_color'                   => $settings->box_shadow_color,
            'image_padding'                   => $settings->image_padding,
            'transition'                   => $settings->transition,
            
            'caption_text_color'                   => $settings->caption_text_color,
            'caption_text_hover_color'                   => $settings->caption_text_hover_color,
            'caption_alignment'                   => $settings->caption_alignment,
            'caption_padding'                   => $settings->caption_padding,
            'caption_font_family'                   => $settings->caption_font_family,
            'caption_font_size'                   => $settings->caption_font_size,
        					'caption_text_color'    => $settings->caption_text_color,//
                            'rotate_hover'  => $gallery->rotate_hover,
                            'rotate'        => $gallery->rotate,
                            'scale'  		=> $gallery->scale,
        );
		FLBuilder::render_module_css('njba-polaroid' , $polaroid_rotate_id, $polaroid_rotate_css_array);
  endfor;
?>
/*Magnificpopup*/
.admin-bar .mfp-wrap .mfp-close, 
.admin-bar .mfp-wrap .mfp-close:active, 
.admin-bar .mfp-wrap .mfp-close:hover, 
.admin-bar .mfp-wrap .mfp-close:focus{position: fixed;right: 30px;cursor: pointer !important;}
.admin-bar .mfp-bottom-bar{top: auto;bottom: -25px;}
.admin-bar .mfp-counter {display: block;}
<?php $width = 100 / $settings->show_col['desktop'];?>
	.fl-node-<?php echo $id; ?> .njba-out-side {
			width: <?php echo $width ?>%;
			padding: <?php echo $settings->col_padding/2; ?>px;
			float: left;
		}
	<?php if ( $settings->show_col['desktop'] > 1 ) { ?>
		.fl-node-<?php echo $id; ?> .njba-out-side:nth-child(<?php echo $settings->show_col['desktop']; ?>n+1){
			clear: left;
		}
		.fl-node-<?php echo $id; ?> .njba-out-side:nth-child(<?php echo $settings->show_col['desktop']; ?>n+0){
			clear: right;
		}
	<?php } ?>
<?php if($global_settings->responsive_enabled) { // Global Setting If started ?>
	@media ( max-width: <?php echo $global_settings->medium_breakpoint .'px'; ?> ) {
		
		
		
			.fl-node-<?php echo $id; ?> .njba-out-side {
				width: <?php echo 100/$settings->show_col['medium']; ?>%;
			}
			<?php if ( $settings->show_col['desktop'] > 1 ) { ?>
				.fl-node-<?php echo $id; ?> .njba-out-side:nth-child(<?php echo $settings->show_col['desktop']; ?>n+1),
				.fl-node-<?php echo $id; ?> .njba-out-side:nth-child(<?php echo $settings->show_col['desktop']; ?>n+0) {
					clear: none;
				}
			<?php } ?>
			
			.fl-node-<?php echo $id; ?> .njba-out-side:nth-child(<?php echo $settings->show_col['medium']; ?>n+1){
				clear: left;
			}
			.fl-node-<?php echo $id; ?> .njba-out-side:nth-child(<?php echo $settings->show_col['medium']; ?>n+0){
				clear: right;
			}
		
	}
	@media ( max-width: <?php echo $global_settings->responsive_breakpoint .'px'; ?> ) {
		
		
		
			.fl-node-<?php echo $id; ?> .njba-out-side {
				width: <?php echo 100/$settings->show_col['small']; ?>%;
			}
			<?php if ( $settings->show_col['desktop'] > 1 ) { ?>
				.fl-node-<?php echo $id; ?> .njba-out-side:nth-child(<?php echo $settings->show_col['desktop']; ?>n+1),
				.fl-node-<?php echo $id; ?> .njba-out-side:nth-child(<?php echo $settings->show_col['desktop']; ?>n+0) <?php if ( $settings->show_col['medium'] > 1 ) { ?>,
				.fl-node-<?php echo $id; ?> .njba-out-side:nth-child(<?php echo $settings->show_col['medium']; ?>n+1),
				.fl-node-<?php echo $id; ?> .njba-out-side:nth-child(<?php echo $settings->show_col['medium']; ?>n+0) <?php } ?>{
					clear: none;
				}
			<?php } ?>
			
			.fl-node-<?php echo $id; ?> .njba-out-side:nth-child(<?php echo $settings->show_col['small']; ?>n+1){
				clear: left;
			}
			.fl-node-<?php echo $id; ?> .njba-out-side:nth-child(<?php echo $settings->show_col['small']; ?>n+0){
				clear: right;
			}
		
	}
<?php } ?>
