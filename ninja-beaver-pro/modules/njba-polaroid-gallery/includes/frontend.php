<?php
	$number_gallery = count($settings->gallery);
	?>
<div class="njba-polaroid-gallery-main " >
	<?php		for($i=0; $i < $number_gallery; $i++) :
				if(!is_object($settings->gallery[$i])) {
					continue;
				}
				$gallery = $settings->gallery[$i];
				?>
				<div class="njba-out-side njba-out-side-unique-<?php echo $i;?> njba-polaroid-<?php  echo $gallery->link_type; ?>">
					<div class="njba-box njba-box-comman">
					<?php if($gallery->link_type =='url'){?>
						<a href="<?php if($gallery->link_url){echo $gallery->link_url;}?>" target="<?php if($gallery->link_target){echo $gallery->link_target;}?>">
					<?php } elseif($gallery->link_type =='lightbox'){?>
						<a href="<?php echo $gallery->photo_src; ?>" class="magnific njba-lightbox">
					<?php } ?>
						<?php if(!empty($gallery->photo)){ ?>
		               <img src="<?php echo $gallery->photo_src; ?>" class="njba-image-responsive"> 
		               <?php } ?>
		               <h1 class="njba-polaroid-gallery-caption-selector"><?php echo $gallery->caption; ?></h1>
		               <?php if($gallery->link_type =='url' || $gallery->link_type =='lightbox' ){?>
		            	</a>
		            	<?php }?>
		            </div>
	            </div>
			<?php endfor;?>
</div>	
