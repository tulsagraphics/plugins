
(function($) {
<?php
	$number_gallery = count($settings->gallery);
	for($i=0; $i < $number_gallery; $i++) :
		$gallery = $settings->gallery[$i];
?>
<?php if($gallery->link_type =='lightbox') : ?>
					
				
				$(document).ready(function() {
					 $('.njba-polaroid-gallery-main').each(function() {
						  var $container = $(this);
						  var $imageLinks = $container.find('.njba-lightbox');
						  var items = [];
						  $imageLinks.each(function() {
						    var $item = $(this);
						    var type = 'image';
						    if ($item.hasClass('magnific-video')) {
						      type = 'iframe';
						    }
						    var magItem = {
						      src: $item.attr('href'),
						      type: type
						    };
						    magItem.title = $item.data('title');    
						    items.push(magItem);
						    });
						  $imageLinks.magnificPopup({
						    mainClass: 'mfp-fade',
						    items: items,
						    gallery:{
						        enabled:true,
						        tPrev: $(this).data('prev-text'),
						        tNext: $(this).data('next-text')
						    },
						    type: 'image',
						    callbacks: {
						      beforeOpen: function() {
						        var index = $imageLinks.index(this.st.el);
						        if (-1 !== index) {
						          this.goTo(index);
						        }
						      }
						    }
						  });
						});            
    				});
<?php 
			
	 endif; 
?>
<?php  endfor;?>
})(jQuery);
	
