
<?php 
    if (  $settings->button_type  == 'button' ){
        $btn_id = $id;
         $btn_css_array = array(
        //Button Style/*
                            
                            'button_style'                   => $settings->button_style,
                            'button_background_color'        => $settings->button_background_color,
                            'button_background_hover_color'  => $settings->button_background_hover_color,
                            'button_text_color'              => $settings->button_text_color,
                            'button_text_hover_color'        => $settings->button_text_hover_color,
                            'button_border_style'            => $settings->button_border_style,
                            'button_border_color'            => $settings->button_border_color,
                            'button_border_hover_color'      => $settings->button_border_hover_color,
                            'button_border_width'            => $settings->button_border_width,
                            'button_border_radius'           => $settings->button_border_radius,
                            'button_box_shadow'              => $settings->button_box_shadow,
                            'button_box_shadow_color'        => $settings->button_box_shadow_color,
                            'button_padding'                 => $settings->button_padding,
                            'button_margin'                  => $settings->button_margin,
                            'button_font_family'             =>$settings->button_font_family,
                            'button_font_size'               =>$settings->button_font_size,
                          
        // Icon Style 
                           'icon_color'                     =>$settings->icon_color,
                            'icon_hover_color'               =>$settings->icon_hover_color,
                            'icon_padding'                   =>$settings->icon_padding,
                            'icon_margin'                    =>$settings->icon_margin,
                            'icon_font_size'                =>$settings->icon_font_size,
                            
                            'transition'                    =>$settings->transition,
                            'width'                         =>$settings->width,
                            'custom_width'                  =>$settings->custom_width,
                            'custom_height'                 =>$settings->custom_height,
                            'alignment'                     =>$settings->alignment,
        );
        FLBuilder::render_module_css('njba-button' , $btn_id ,$btn_css_array);
        //die();
    }
?>
<?php 
    if (  $settings->button_type  == 'image' ){ 
        $image_id = $id;
         $image_css_array = array(
        
                            
                            'style'                          => $settings->style,
                            'heading_font'                   => $settings->heading_font,
                            'first_font_size'                => $settings->first_font_size,
                            'first_font_color'               => $settings->first_font_color,
                            'font_size'                      => $settings->font_size,
                            'font_color'                     => $settings->font_color,
                            'inside_primary_border_color'    => $settings->inside_primary_border_color,
                            'inside_secondary_border_color'  => $settings->inside_secondary_border_color,
                            'hover_color'                    => $settings->hover_color,
                            'hover_opacity'                  => $settings->hover_opacity,
                            'content_box_margin1'            => $settings->content_box_margin1,
                            'caption_padding'                => $settings->caption_padding,
                            'before_padding'                 => $settings->before_padding,
                            'after_padding'                  => $settings->after_padding,
                            'inside_primary_border'          =>$settings->inside_primary_border,
                            'inside_secondary_border'        =>$settings->inside_secondary_border,
                            'transition'                     =>$settings->transition
                          
        
        );
        FLBuilder::render_module_css('njba-image-hover' , $image_id ,$image_css_array);
    }
?>
.fl-node-<?php echo $id; ?> .njba-modal-height-auto,
#modal-<?php echo $id; ?>.njba-modal-height-auto {
    display: block !important;
    position: absolute;
    top: -99999px;
    width: <?php echo $settings->modal_width; ?>px;
    visibility: hidden;
}
.fl-node-<?php echo $id; ?> .njba-modal-height-auto .njba-modal-overlay,
#modal-<?php echo $id; ?>.njba-modal-height-auto .njba-modal-overlay {
    display: none !important;
}
.fl-node-<?php echo $id; ?> .njba-modal,
#modal-<?php echo $id; ?> .njba-modal {
    <?php if ( $settings->modal_background == 'color' ) { ?>
     background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->modal_bg_color)) ?>, <?php echo $settings->modal_bg_opacity/100; ?>); 
   
    <?php } else { ?>
    background-image: url(<?php echo wp_get_attachment_url( $settings->modal_bg_photo ); ?>);
    background-size: <?php echo $settings->modal_bg_size; ?>;
    background-repeat: <?php echo $settings->modal_bg_repeat; ?>;
    <?php } ?>
    <?php if ( 'yes' == $settings->modal_shadow ) { ?>
    -webkit-box-shadow: <?php echo $settings->box_shadow_h; ?>px <?php echo $settings->box_shadow_v; ?>px <?php echo $settings->box_shadow_blur; ?>px <?php echo $settings->box_shadow_spread; ?>px <?php echo '#'.$settings->box_shadow_color; ?>;
    -moz-box-shadow: <?php echo $settings->box_shadow_h; ?>px <?php echo $settings->box_shadow_v; ?>px <?php echo $settings->box_shadow_blur; ?>px <?php echo $settings->box_shadow_spread; ?>px <?php echo '#'.$settings->box_shadow_color; ?>;
    -o-box-shadow: <?php echo $settings->box_shadow_h; ?>px <?php echo $settings->box_shadow_v; ?>px <?php echo $settings->box_shadow_blur; ?>px <?php echo $settings->box_shadow_spread; ?>px <?php echo '#'.$settings->box_shadow_color; ?>;
    box-shadow: <?php echo $settings->box_shadow_h; ?>px <?php echo $settings->box_shadow_v; ?>px <?php echo $settings->box_shadow_blur; ?>px <?php echo $settings->box_shadow_spread; ?>px <?php echo '#'.$settings->box_shadow_color; ?>;
    <?php } ?>
}
<?php if ( 'enabled' == $settings->modal_backlight ) { ?>
.fl-node-<?php echo $id; ?> .njba-modal:before,
#modal-<?php echo $id; ?> .njba-modal:before {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    background: inherit;
    z-index: -1;
    filter: blur(10px);
    -moz-filter: blur(10px);
    -webkit-filter: blur(10px);
    -o-filter: blur(10px);
    transition: all 2s linear;
    -moz-transition: all 2s linear;
    -webkit-transition: all 2s linear;
    -o-transition: all 2s linear;
}
<?php } ?>
.fl-node-<?php echo $id; ?> .njba-modal.layout-standard,
#modal-<?php echo $id; ?> .njba-modal.layout-standard {
    width: <?php echo $settings->modal_width; ?>px;
    height: <?php echo 'yes' == $settings->modal_height_auto ? 'auto' : $settings->modal_height . 'px'; ?>;
    max-width: 90%;
    <?php if ( 'none' != $settings->modal_border ) { ?>
    border<?php echo $settings->modal_border_position == 'default' ? '' : '-' . $settings->modal_border_position; ?>: <?php echo $settings->modal_border_width; ?>px <?php echo $settings->modal_border; ?> #<?php echo $settings->modal_border_color; ?>;
    <?php } ?>
    border-radius: <?php echo $settings->modal_border_radius; ?>px;
}
.fl-node-<?php echo $id; ?> .njba-modal.layout-fullscreen,
#modal-<?php echo $id; ?> .njba-modal.layout-fullscreen {
    margin-top: <?php echo $settings->modal_margin_top; ?>px;
    margin-left: <?php echo $settings->modal_margin_left; ?>px;
    margin-bottom: <?php echo $settings->modal_margin_bottom; ?>px;
    margin-right: <?php echo $settings->modal_margin_right; ?>px;
}
.fl-node-<?php echo $id; ?> .njba-modal .njba-modal-header,
#modal-<?php echo $id; ?> .njba-modal .njba-modal-header {
    background-color: <?php echo $settings->title_bg ? '#' . $settings->title_bg : 'transparent'; ?>;
    border-bottom: <?php echo $settings->title_border; ?>px <?php echo $settings->title_border_style; ?> #<?php echo $settings->title_border_color; ?>;
    <?php if ( 'fullscreen' != $settings->modal_layout ) { ?>
    border-top-left-radius: <?php echo $settings->modal_border_radius; ?>px;
    border-top-right-radius: <?php echo $settings->modal_border_radius; ?>px;
    <?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-modal .njba-modal-title,
#modal-<?php echo $id; ?> .njba-modal .njba-modal-title {
    padding: 10px <?php echo $settings->title_padding; ?>px;
    color: #<?php echo $settings->title_color; ?>;
    <?php if( $settings->title_font_family['family'] != 'Default' ) {
        FLBuilderFonts::font_css( $settings->title_font_family );
    } ?>
    font-size: <?php echo $settings->title_font_size; ?>px;
    text-align: <?php echo $settings->title_alignment; ?>;
}
.fl-node-<?php echo $id; ?> .njba-modal .njba-modal-content,
#modal-<?php echo $id; ?> .njba-modal .njba-modal-content {
    <?php echo ('' != $settings->content_text_color) ? 'color: #'.$settings->content_text_color.';' : ''; ?>
    padding: <?php echo $settings->modal_padding; ?>px;
    overflow-y: auto;
}
.fl-node-<?php echo $id; ?> .njba-modal .njba-modal-content.njba-modal-frame:before,
#modal-<?php echo $id; ?> .njba-modal .njba-modal-content.njba-modal-frame:before {
    background: url(<?php echo $module->url . 'loader.gif'; ?>) no-repeat;
    background-position: 50%;
    content: "";
    display: block;
    width: 32px;
    height: 32px;
    border-radius: 100%;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate3D(-50%, -50%, 0);
}
.fl-node-<?php echo $id; ?> .njba-modal .njba-modal-content-inner,
#modal-<?php echo $id; ?> .njba-modal .njba-modal-content-inner {
    position: relative;
    height: 100%;
    <?php if ( 'none' != $settings->content_border ) { ?>
    border: <?php echo $settings->content_border_width; ?>px <?php echo $settings->content_border; ?> #<?php echo $settings->content_border_color; ?>;
    border-radius: <?php echo $settings->content_border_radius; ?>px;
    <?php } ?>
    <?php if ( 'fullscreen' == $settings->modal_layout ) { ?>
    overflow-y: auto;
    <?php } else { ?>
    overflow: hidden;
    <?php } ?>
    padding: <?php echo $settings->content_padding; ?>px;
}
.fl-node-<?php echo $id; ?> .njba-modal-close,
#modal-<?php echo $id; ?> .njba-modal-close {
    <?php if ( 'none' == $settings->close_btn_toggle ) { ?>
    display: none;
    <?php } ?>
    background: <?php echo $settings->close_btn_bg ? '#' . $settings->close_btn_bg : 'transparent'; ?>;
    border: <?php echo '' == $settings->close_btn_border ? 0 : $settings->close_btn_border; ?>px solid #<?php echo $settings->close_btn_border_color; ?>;
    border-radius: <?php echo $settings->close_btn_border_radius; ?>px;
    width: <?php echo $settings->close_btn_size; ?>px;
    height: <?php echo $settings->close_btn_size; ?>px;
    position: absolute;
    z-index: 5;
    -webkit-transition: background 0.2s ease-in-out;
    -moz-transition: background 0.2s ease-in-out;
    transition: background 0.2s ease-in-out;
}
.fl-node-<?php echo $id; ?> .njba-modal-close:hover,
#modal-<?php echo $id; ?> .njba-modal-close:hover {
    background: <?php echo $settings->close_btn_bg_hover ? '#' . $settings->close_btn_bg_hover : 'transparent'; ?>;
    -webkit-transition: background 0.2s ease-in-out;
    -moz-transition: background 0.2s ease-in-out;
    transition: background 0.2s ease-in-out;
}
.fl-node-<?php echo $id; ?> .njba-modal-close.box-top-right,
#modal-<?php echo $id; ?> .njba-modal-close.box-top-right {
    top: <?php echo $settings->close_btn_top; ?>px;
    right: <?php echo $settings->close_btn_right; ?>px;
}
.fl-node-<?php echo $id; ?> .njba-modal-close.box-top-left,
#modal-<?php echo $id; ?> .njba-modal-close.box-top-left {
    top: <?php echo $settings->close_btn_top; ?>px;
    left: <?php echo $settings->close_btn_left; ?>px;
}
.fl-node-<?php echo $id; ?> .njba-modal-close.win-top-right,
#modal-<?php echo $id; ?> .njba-modal-close.win-top-right {
    top: <?php echo $settings->close_btn_top; ?>px;
    right: <?php echo $settings->close_btn_right; ?>px;
}
.fl-node-<?php echo $id; ?> .njba-modal-close.win-top-left,
#modal-<?php echo $id; ?> .njba-modal-close.win-top-left {
    top: <?php echo $settings->close_btn_top; ?>px;
    left: <?php echo $settings->close_btn_left; ?>px;
}
.fl-node-<?php echo $id; ?> .njba-modal-close .bar-wrap,
#modal-<?php echo $id; ?> .njba-modal-close .bar-wrap {
    width: 100%;
    height: 100%;
    -webkit-transition: background 0.2s ease-in-out;
    -moz-transition: background 0.2s ease-in-out;
    transition: background 0.2s ease-in-out;
}
.fl-node-<?php echo $id; ?> .njba-modal-close .bar-wrap span,
#modal-<?php echo $id; ?> .njba-modal-close .bar-wrap span {
    background: #<?php echo $settings->close_btn_color; ?>;
    height: <?php echo ( $settings->close_btn_weight == 0 || $settings->close_btn_weight == '' ) ? 1 : $settings->close_btn_weight; ?>px;
    margin-top: <?php echo ($settings->close_btn_weight == 1 || $settings->close_btn_weight == 0 || $settings->close_btn_weight == '') ? 0 : -1; ?>px;
    -webkit-transition: background 0.2s ease-in-out;
    -moz-transition: background 0.2s ease-in-out;
    transition: background 0.2s ease-in-out;
}
.fl-node-<?php echo $id; ?> .njba-modal-close:hover .bar-wrap span,
#modal-<?php echo $id; ?> .njba-modal-close:hover .bar-wrap span {
    background: #<?php echo $settings->close_btn_color_hover; ?>;
    -webkit-transition: background 0.2s ease-in-out;
    -moz-transition: background 0.2s ease-in-out;
    transition: background 0.2s ease-in-out;
}
.fl-node-<?php echo $id; ?> .njba-modal-container.<?php echo $settings->animation_load; ?>.animated,
#modal-<?php echo $id; ?> .njba-modal-container.<?php echo $settings->animation_load; ?>.animated {
    -webkit-animation-duration: <?php echo $settings->animation_load_duration; ?>s;
    animation-duration: <?php echo $settings->animation_load_duration; ?>s;
}
.fl-node-<?php echo $id; ?> .njba-modal-container.<?php echo $settings->animation_exit; ?>.animated,
#modal-<?php echo $id; ?> .njba-modal-container.<?php echo $settings->animation_exit; ?>.animated {
    -webkit-animation-duration: <?php echo $settings->animation_exit_duration; ?>s;
    animation-duration: <?php echo $settings->animation_exit_duration; ?>s;
}
.fl-node-<?php echo $id; ?> .njba-modal-overlay,
#modal-<?php echo $id; ?> .njba-modal-overlay {
	display: <?php echo $settings->overlay_toggle; ?>;
    background-color: <?php echo $settings->overlay_bg_color ? '#' . $settings->overlay_bg_color : 'transparent'; ?>;
    opacity: <?php echo $settings->overlay_opacity; ?>;
}
<?php if ( 'photo' == $settings->modal_type ) { ?>
<?php if ( 'fullscreen' != $settings->modal_layout ) { ?>
.fl-node-<?php echo $id; ?> .njba-modal .njba-modal-content img,
#modal-<?php echo $id; ?> .njba-modal .njba-modal-content img {
    border-radius: <?php echo $settings->modal_border_radius; ?>px;
}
<?php } ?>
<?php } ?>
@media only screen and (max-width: <?php echo $settings->media_breakpoint; ?>px) {
    .fl-node-<?php echo $id; ?> .njba-modal.layout-fullscreen,
    #modal-<?php echo $id; ?> .njba-modal.layout-fullscreen {
        top: 0 !important;
        margin 10px !important;
    }
    .fl-node-<?php echo $id; ?> .njba-modal.layout-standard,
    #modal-<?php echo $id; ?> .njba-modal.layout-standard {
        margin-top: 20px;
        margin-bottom: 20px;
    }
}
@media only screen and (max-width: 767px) {
    .fl-node-<?php echo $id; ?> .njba-modal.layout-standard,
    #modal-<?php echo $id; ?> .njba-modal.layout-standard {
        margin-top: 20px;
        margin-bottom: 20px;
    }
}
