
<div class="njba-modal-box"> 
<?php 
if ( 'onclick' == $settings->modal_load ) { ?>
	<div class="njba-modal-button">
	<?php	if (  $settings->button_type  == 'button' ){
      									 	$btn_settings = array(
							                       
							                        'button_text'					=> $settings->button_text,
							                        'buttton_icon_select'			=> $settings->buttton_icon_select,
							                        'button_font_icon'				=> $settings->button_font_icon,
							                       	'button_icon_aligment'			=> $settings->button_icon_aligment,
							                        'link'                  		=> 'javascript:void(0)',
							                        'link_target'            		=> '_self',
							                        'a_data' 						=> 'data-modal="'.$module->node.'"',
													'a_class'	             		=> 'njba-modal-trigger modal-'.$module->node,
													'button_style'					=> $settings->button_style
							                        
											);
											 FLBuilder::render_module_html('njba-button', $btn_settings);
		}elseif (  $settings->button_type  == 'image' ){ ?>
			<a href="#" id="trigger-<?php echo $module->node; ?>" class="njba-modal-trigger modal-<?php echo $module->node; ?>" data-modal="<?php echo $module->node; ?>" data-node="<?php echo $module->node; ?>">
	<?php									$image_settings = array(
							                       
							                        'photo'					        => $settings->photo,
							                        'photo_src'			            => $settings->photo_src,
							                        'caption'						=> $settings->caption,
							                        'style'							=> $settings->style,
							                        
											);
											 FLBuilder::render_module_html('njba-image-hover', $image_settings);
		?>
			</a>
	<?php
		}
	?>
	</div>
	<?php
}else { ?>
	<?php if ( FLBuilderModel::is_builder_active() ) { ?>
	<div class="njba-helper" style="text-align: center;">
		<h4><?php echo $module->name; ?></h4>
		<?php _e('Click here to edit the "modal-box" settings. This text is only for editing and will not appearance after you publish the changes.', 'bb-njba'); ?>
	</div>
	<?php } ?>
<?php }?>
</div>
<div id="modal-<?php echo $module->node; ?>" class="njba-modal-wrap<?php echo ('yes' == $settings->modal_height_auto && 'fullscreen' != $settings->modal_layout) ? ' njba-modal-height-auto' : ''; ?>">
	<div class="njba-modal-container">
		<div class="njba-modal-overlay"></div>
		<?php if ( 'win-top-right' == $settings->close_btn_position || 'win-top-left' == $settings->close_btn_position ) { ?>
			<div class="njba-modal-close <?php echo $settings->close_btn_position; ?>">
				<div class="bar-wrap">
					<span class="bar-1"></span>
					<span class="bar-2"></span>
				</div>
			</div>
		<?php } ?>
		<div class="njba-modal layout-<?php echo $settings->modal_layout; ?>">
			
			<div class="njba-modal-body">
				<?php if ( 'no' == $settings->modal_title_toggle ) { ?>
					<?php if ( 'win-top-right' != $settings->close_btn_position && 'win-top-left' != $settings->close_btn_position ) { ?>
						<div class="njba-modal-close <?php echo $settings->close_btn_position; ?> no-modal-header">
							<div class="bar-wrap">
								<span class="bar-1"></span>
								<span class="bar-2"></span>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
				<?php if ( 'yes' == $settings->modal_title_toggle ) { ?>
				<div class="njba-modal-header">
					<?php if ( 'box-top-right' == $settings->close_btn_position ) { ?>
					<h2 class="njba-modal-title"><?php echo $settings->modal_title; ?></h2>
					<?php } ?>
					<?php if ( 'win-top-right' != $settings->close_btn_position && 'win-top-left' != $settings->close_btn_position ) { ?>
						<div class="njba-modal-close <?php echo $settings->close_btn_position; ?>">
							<div class="bar-wrap">
								<span class="bar-1"></span>
								<span class="bar-2"></span>
							</div>
						</div>
					<?php } ?>
					<?php if ( 'box-top-left' == $settings->close_btn_position ) { ?>
					<h2 class="njba-modal-title"><?php echo $settings->modal_title; ?></h2>
					<?php } ?>
				</div>
				<?php } ?>
				<div class="njba-modal-content<?php echo ('url' == $settings->modal_type || 'video' == $settings->modal_type) ? ' njba-modal-frame' : '' ;?>">
					<div class="njba-modal-content-inner">
						<?php echo $module->get_modal_content(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
