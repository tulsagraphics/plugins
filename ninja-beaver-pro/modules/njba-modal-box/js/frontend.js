var NJBAModal;
;(function($) {
    NJBAModal = {
        settings: {},
        isActive: false,
        init: function(data) {
            NJBAModal.settings = data;
            if(parseInt(NJBAModal.settings.display_after) === 0 || NJBAModal.settings.display_after < 0 || NJBAModal.settings.display_after === '') {
                NJBAModal.cookie.remove();
            }
            if((NJBAModal.settings.exit_intent || NJBAModal.settings.auto_load) && NJBAModal.cookie.get() && !NJBAModal.settings.previewing){
                return;
            }
            if(!NJBAModal.settings.previewing && 'undefined' !== typeof NJBAModal.settings.visible && !NJBAModal.settings.visible) {
                return;
            }
            if(NJBAModal.isActive) {
                return;
            }
            NJBAModal.responsive();
            NJBAModal.show();
        },
        modal: function() {
            return $('#modal-'+NJBAModal.settings.id+' .njba-modal');
        },
        show: function() {
            if('fullscreen' !== NJBAModal.settings.layout) {
                if ( typeof NJBAModal.settings.height === 'undefined' ) {
                    $('#modal-'+NJBAModal.settings.id).addClass('njba-modal-height-auto');
                    var modalHeight = NJBAModal.modal().outerHeight();
                    $('#modal-'+NJBAModal.settings.id).removeClass('njba-modal-height-auto');
                    if('photo' === NJBAModal.settings.type) {
                        NJBAModal.modal().find('.njba-modal-content-inner img').css('max-width', '100%');
                    }
                    var topPos = ($(window).height() - modalHeight)/2;
                    if ( topPos < 0 ) {
                        topPos = 0;
                    }
                    NJBAModal.modal().css('top', topPos + 'px');
                } else {
                    NJBAModal.modal().css('top', ($(window).height() - NJBAModal.settings.height)/2 + 'px');
                }
            }
            setTimeout(function(){
                $('#modal-'+NJBAModal.settings.id).show();
                NJBAModal.modal().parent().removeClass(NJBAModal.settings.animation_load+' animated');
                NJBAModal.modal().addClass('modal-visible');
                NJBAModal.modal().parent()
                    .addClass(NJBAModal.settings.animation_load+' animated')
                    .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                        $(this).removeClass(NJBAModal.settings.animation_load+' animated');
                        if('url' == NJBAModal.settings.type) {
                            var src = NJBAModal.modal().find('.njba-modal-iframe').data('src');
                            if(NJBAModal.modal().find('.njba-modal-iframe').attr('src') === undefined) {
                                NJBAModal.modal().find('.njba-modal-iframe').attr('src', src);
                            }
                        }
                        if('video' == NJBAModal.settings.type) {
                            var src = '';
                            if(NJBAModal.modal().find('iframe, source').attr('src') === undefined) {
                                src = NJBAModal.modal().find('iframe, source').data('src');
                            } else {
                                src = NJBAModal.modal().find('iframe, source').attr('src');
                            }
                            if ( src ) {
                                if((src.search('youtube') !== -1 || src.search('vimeo') !== -1) && src.search('autoplay=1') == -1) {
                                    if(typeof src.split('?')[1] === 'string') {
                                        src = src + '&autoplay=1&rel=0';
                                    } else {
                                        src = src + '?autoplay=1&rel=0';
                                    }
                                }
                                NJBAModal.modal().find('iframe, source').attr('src', src);
                            }
                            if(NJBAModal.modal().find('video').length) {
                                NJBAModal.modal().find('video')[0].play();
                            }
                        }
                    });
                NJBAModal.isActive = true;
                if(NJBAModal.settings.exit_intent || NJBAModal.settings.auto_load){
                    if(!NJBAModal.settings.previewing) {
                        NJBAModal.cookie.set();
                    }
                }
                NJBAModal.adjust();
                NJBAModal.events();
            }, NJBAModal.settings.auto_load ? parseFloat(NJBAModal.settings.delay) * 1000 : 100);
        },
        hide: function() {
            NJBAModal.modal().parent()
                .removeClass(NJBAModal.settings.animation_exit+' animated')
                .addClass(NJBAModal.settings.animation_exit+' animated')
                .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                    $(this).removeClass(NJBAModal.settings.animation_exit+' animated').find('.njba-modal').removeClass('modal-visible');
                    $(this).find('.njba-modal-content').removeAttr('style');
                    $('#modal-'+NJBAModal.settings.id).removeAttr('style');
                    NJBAModal.isActive = false;
                    NJBAModal.reset();
                });
        },
        adjust: function() {
            var mH = 0, hH = 0, cH = 0, eq = 0;
            setTimeout(function(){
                if(NJBAModal.isActive) {
                    if('fullscreen' === NJBAModal.settings.layout){
                        var marginTop = parseInt(NJBAModal.modal().css('margin-top'));
                        var marginBottom = parseInt(NJBAModal.modal().css('margin-bottom'));
                        var modalHeight = $(window).height() - (marginTop + marginBottom);
                        NJBAModal.modal().css('height', modalHeight + 'px');
                    }
                    eq = 6;
                    mH = NJBAModal.modal().outerHeight(); // Modal height.
                    hH = NJBAModal.modal().find('.njba-modal-header').outerHeight(); // Header height.
                    if(NJBAModal.settings.auto_height && 'fullscreen' !== NJBAModal.settings.layout) {
                        return;
                    }
                    cP = parseInt(NJBAModal.modal().find('.njba-modal-content').css('padding')); // Content padding.
                    NJBAModal.modal().find('.njba-modal-content').css('height', mH - (hH + eq) + 'px');
                    if (!NJBAModal.settings.auto_height && NJBAModal.modal().find('.njba-modal-header').length === 0) {
                        NJBAModal.modal().find('.njba-modal-content').css('height', mH + 'px');
                    }
                    // Adjust iframe height.
                    if('url' === NJBAModal.settings.type) {
                        NJBAModal.modal().find('.njba-modal-iframe').css('height', NJBAModal.modal().find('.njba-modal-content-inner').outerHeight() + 'px');
                    }
                    if('video' === NJBAModal.settings.type) {
                        NJBAModal.modal().find('iframe').css({'height':'100%', 'width':'100%'});
                    }
                }
            }, NJBAModal.settings.auto_load ? parseFloat(NJBAModal.settings.delay) * 1000 : 0);
        },
        responsive: function() {
            if($(window).width() <= NJBAModal.settings.breakpoint){
                NJBAModal.modal().removeClass('layout-standard').addClass('layout-fullscreen');
            }
        },
        events: function() {
            $(document).keyup(function(e) {
                if(NJBAModal.settings.esc_exit && 27 == e.which && NJBAModal.isActive && $('form[data-type="njba-modal-box"]').length === 0) {
                    NJBAModal.hide();
                }
            });
            $('.njba-modal-close').on('click', function() {
                //console.log('#modal-'+NJBAModal.settings.id);
                //$('#modal-'+NJBAModal.settings.id).hide();
                NJBAModal.hide();
            });
            $(document).on('click', function(e) {
                if (NJBAModal.settings.click_exit && NJBAModal.isActive && !NJBAModal.settings.previewing && !NJBAModal.modal().is(e.target) && NJBAModal.modal().has(e.target).length === 0 && e.which) {
                    NJBAModal.hide();
                }
            });
        },
        cookie: {
            set: function() {
                if(parseInt(NJBAModal.settings.display_after) > 0) {
                    return $.cookie('njba_modal_'+NJBAModal.settings.id, NJBAModal.settings.display_after, {expires: NJBAModal.settings.display_after, path: '/'});
                } else {
                    NJBAModal.cookie.remove();
                }
            },
            get: function() {
                return $.cookie('njba_modal_'+NJBAModal.settings.id);
            },
            remove: function() {
                return $.cookie('njba_modal_'+NJBAModal.settings.id, 0, {expires: 0, path: '/'});
            }
        },
        reset: function() {
            if('url' == NJBAModal.settings.type || 'video' == NJBAModal.settings.type) {
                var src = NJBAModal.modal().find('iframe, source').attr('src');
                NJBAModal.modal().find('iframe, source').attr('data-src', src).removeAttr('src');
                if(NJBAModal.modal().find('video').length) {
                    NJBAModal.modal().find('video')[0].pause();
                }
            }
            NJBAModal.settings = {};
        }
    };
})(jQuery);
