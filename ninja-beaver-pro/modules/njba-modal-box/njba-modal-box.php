<?php
/**
 * @class NJBAModalBoxModule
 */
class NJBAModalBoxModule extends FLBuilderModule {
     /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Modal Box ', 'bb-njba'),
            'description'   => __('Addon to display modal box with new features.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'creative' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-modal-box/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-modal-box/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
         /**
         * Use these methods to enqueue css and js already
         * registered or to register and enqueue your own.
         */
        // Already registered
       
        $this->add_css( 'njba-modal-frontend-style', NJBA_MODULE_URL . 'modules/njba-modal-box/css/frontend.css' );
        $this->add_css( 'njba-animate-min-style', NJBA_MODULE_URL . 'modules/njba-modal-box/css/animate.min.css' );
        $this->add_css( 'njba-animate', NJBA_MODULE_URL . 'modules/njba-modal-box/css/animate.css' );
        $this->add_css('font-awesome');
       
        /*if ( class_exists( 'FLBuilderModel' ) && FLBuilderModel::is_builder_active() ) {
            $this->add_css( 'modal-settings-style', $this->url . 'css/settings.css' );
            $this->add_js( 'modal-settings-script', $this->url . 'js/settings.js', array(), '', true );
        }*/
        
         $this->add_js( 'jquery-cookie', $this->url . 'js/jquery.cookie.min.js', array('jquery') );
   }
   public function get_modal_content()
    {
        $modal_type = $this->settings->modal_type;
        switch($modal_type) {
            case 'photo':
                if ( isset( $this->settings->modal_type_photo_src ) ) {
                    return '<img src="' . $this->settings->modal_type_photo_src . '" style="max-width: 100%;"/>';
                }
            break;
            case 'video':
                global $wp_embed;
                return $wp_embed->autoembed($this->settings->modal_type_video);
            break;
            case 'url':
                return '<iframe data-src="' . $this->settings->modal_type_url . '" class="njba-modal-iframe" frameborder="0" width="100%" height="100%"></iframe>';
            break;
            case 'content':
                return $this->settings->modal_type_content;
            break;
            case 'html':
                return $this->settings->modal_type_html;
            break;
            default:
                return;
            break;
        }
    }
   
}
/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('NJBAModalBoxModule', array(
	'general'      => array( // Tab
		'title'         => __('General', 'bb-njba'), // Tab title
		'sections'      => array( // Tab Sections
			'modal_content' => array(
                'title'         => __('Content', 'bb-njba'),
                'fields'        => array(
                    'modal_title_toggle'    => array(
                        'type'                  => 'select',
                        'label'                 => __('Title Show', 'bb-njba'),
                        'default'               => 'yes',
                        'options'               => array(
                            'yes'                   => __('Yes', 'bb-njba'),
                            'no'                    => __('No', 'bb-njba')
                        ),
                        'toggle'                => array(
                            'yes'                   => array(
                                'fields'                => array('modal_title'),
                                'sections'              => array('modal_title')
                            )
                        ),
                        'trigger'               => array(
                            'yes'                   => array(
                                'fields'                => array('button_type')
                            ),
                            'no'                    => array(
                                'fields'                => array('button_type')
                            )
                        )
                    ),
                    'modal_title'      => array(
                        'type'          => 'text',
                        'label'         => __('Title', 'bb-njba'),
                        'default'       => __('Modal Title', 'bb-njba'),
                        'preview'       => array(
                            'type'          => 'text',
                            'selector'      => '.njba-modal-title'
                        )
                    ),
                    'modal_type'       => array(
                        'type'          => 'select',
                        'label'         => __('Type', 'bb-njba'),
                        'default'       => 'photo',
                        'options'       => array(
                            'photo'         => __('Photo', 'bb-njba'),
                            'video'         => __('Video', 'bb-njba'),
                            'url'           => __('URL', 'bb-njba'),
                            'content'       => __('Content', 'bb-njba'),
                            'html'          => __('Raw HTML', 'bb-njba'),
                            /*'templates'     => __('Saved Templates', 'bb-njba')*/
                        ),
                        'toggle'        => array(
                            'photo'        => array(
                                'fields'        => array('modal_type_photo')
                            ),
                            'video'         => array(
                                'fields'        => array('modal_type_video')
                            ),
                            'url'           => array(
                                'fields'        => array('modal_type_url')
                            ),
                            'content'       => array(
                                'fields'        => array('modal_type_content')
                            ),
                            'html'          => array(
                                'fields'        => array('modal_type_html')
                            ),
                        )
                    ),
                    'modal_type_photo'     => array(
                        'type'                  => 'photo',
                        'label'                 => __('Photo', 'bb-njba')
                    ),
                    'modal_type_video'     => array(
                        'type'                  => 'textarea',
                        'label'                 => __('Embed Code / URL', 'bb-njba'),
                        'rows'                  => 6
                    ),
                    'modal_type_url'       => array(
                        'type'                  => 'text',
                        'label'                 => __('URL', 'bb-njba'),
                        'placeholder'           => 'http://www.example.com',
                        'default'               => '',
                    ),
                    'modal_type_content'   => array(
                        'type'                  => 'editor',
                        'label'                 => '',
                        'preview'               => array(
                            'type'                  => 'text',
                            'selector'              => '.njba-modal-content-inner'
                        )
                    ),
                    'modal_type_html'      => array(
                        'type'                  => 'code',
                        'editor'                => 'html',
                        'label'                 => '',
                        'rows'                  => 15,
                        'preview'               => array(
                            'type'                  => 'text',
                            'selector'              => '.njba-modal-content'
                        )
                    ),
                   
                )
            ),
            'modal_box'       => array( // Section
                'title'             => __('Modal', 'bb-njba'), // Section Title
                'fields'            => array( // Section Fields
                    'modal_layout'      => array(
                        'type'              => 'select',
                        'label'             => __('Layout', 'bb-njba'),
                        'default'           => 'standard',
                        'options'           => array(
                            'standard'          => __('Standard', 'bb-njba'),
                            'fullscreen'        => __('Fullscreen', 'bb-njba')
                        ),
                        'toggle'            => array(
                            'standard'          => array(
                                'fields'            => array('modal_border', 'modal_border_radius', 'modal_width', 'modal_height_auto'),
                            ),
                            'fullscreen'        => array(
                                'fields'            => array('modal_margin_top', 'modal_margin_bottom', 'modal_margin_left', 'modal_margin_right')
                            )
                        ),
                        'hide'              => array(
                            'fullscreen'        => array(
                                'fields'            => array('modal_border', 'modal_border_radius')
                            )
                        ),
                        'help'              => __('Stying options are available in Modal Style tab.', 'bb-njba')
                    ),
                    'modal_width'       => array(
                        'type'              => 'text',
                        'label'             => __('Width', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => 550,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal',
                            'property'          => 'width',
                            'unit'              => 'px'
                        )
                    ),
                    'modal_height_auto' => array(
                        'type'          => 'select',
                        'label'         => __('Auto Height', 'bb-njba'),
                        'default'       => 'yes',
                        'options'       => array(
                            'yes'           => __('Yes', 'bb-njba'),
                            'no'            => __('No', 'bb-njba')
                        ),
                        'toggle'        => array(
                            'no'            => array(
                                'fields'        => array('modal_height')
                            )
                        )
                    ),
                    'modal_height'      => array(
                        'type'              => 'text',
                        'label'             => __('Height', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => 450,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal',
                            'property'          => 'height',
                            'unit'              => 'px'
                        )
                    ),
                    'modal_preview'         => array(
                        'type'                  => 'select',
                        'label'                 => __('Show Preview', 'bb-njba'),
                        'default'               => 'enabled',
                        'options'               => array(
                            'enabled'               => __('Yes', 'bb-njba'),
                            'disabled'              => __('No', 'bb-njba')
                        ),
                        'help'                  => __('You will be able to see the modal box preview by enabling this option.', 'bb-njba')
                    ),
                )
            ),
		)
    ),
    'settings'       => array( // Tab
        'title'         => __('Settings', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'modal_load'    => array(
                'title'         => __('Trigger', 'bb-njba'),
                'fields'        => array(
                    'modal_load'           => array(
                        'type'                  => 'select',
                        'label'                 => __('Trigger', 'bb-njba'),
                        'default'               => 'auto',
                        'options'               => array(
                            'auto'                  => __('Auto', 'bb-njba'),
                            'onclick'               => __('On Click', 'bb-njba'),
                            'exit_intent'           => __('Exit Intent', 'bb-njba'),
                            'other'                 => __('Other', 'bb-njba')
                        ),
                        'toggle'            => array(
                            'auto'              => array(
                                'sections'          => array('modal_load_auto'),
                                
                            ),
                            'onclick'           => array(
                                'fields'            => array('button_type'),
                                'sections'          => array('modal_load_onclick'),
                                //'tabs'              => array('modal_button_style')
                            ),
                            'exit_intent'       => array(
                                'sections'          => array('modal_exit_intent'),
                                
                            ),
                            'other'             => array(
                                'fields'             => array('modal_css_class'),
                                 
                            )
                        ),
                        'hide'              => array(
                            'auto'              => array(
                                
                                'tabs'              => array('modal_button_style','photo_style'),
                            ),
                            'exit_intent'       => array(
                                'tabs'              => array('modal_button_style','photo_style'),
                            ),
                             'other'       => array(
                                'tabs'              => array('modal_button_style','photo_style'),
                            ),
                        ),
                        'help'              => __('Other - modal can be triggered through any other element(s) on this page by providing modal CSS class to that element.', 'bb-njba')
                    ),
                    'modal_css_class'       => array(
                        'type'                  => 'text',
                        'label'                 => __('CSS Class', 'bb-njba'),
                        'default'               => '',
                        'disabled'              => 'disabled',
                        'class'                 => 'modal-trigger-class',
                        'help'                  => __('Add this CSS class to the element you want to trigger the modal with.', 'bb-njba')
                    ),
                    
                )
            ),
            'modal_load_auto'   => array(
                'title'             => __('Auto Load Settings', 'bb-njba'),
                'fields'            => array(
                    'modal_delay'       => array(
                        'type'              => 'text',
                        'label'             => __('Delay', 'bb-njba'),
                        'description'       => __('seconds', 'bb-njba'),
                        'class'             => 'input-small',
                        'default'           => 1,
                    ),
                    'display_after_auto'    => array(
                        'type'                  => 'text',
                        'label'                 => __('Display After', 'bb-njba'),
                        'default'               => 1,
                        'description'           => __('day(s)', 'bb-njba'),
                        'help'                  => __('If a user closes the modal box, it will be displayed only after the defined day(s).', 'bb-njba'),
                        'class'                 => 'input-small modal-display-after'
                    )
                )
            ),
            'modal_load_onclick'  => array(
                'title'             => __('On Click Settings', 'bb-njba'),
                'fields'            => array(
                    'button_type'       => array(
                        'type'              => 'select',
                        'label'             => __('Type', 'bb-njba'),
                        'default'           => 'button',
                        'options'           => array(
                            'button'            => __('Button', 'bb-njba'),
                            'image'             => __('Image', 'bb-njba'),
                            
                        ),
                        'toggle'            => array(
                            'button'            => array(
                                'fields'            => array('button_text','buttton_icon_select'),
                                'tabs'              => array('modal_button_style'),
                                
                            ),
                            'image'             => array(
                                'fields'            => array('photo','caption'),
                                'tabs'              => array('photo_style')
                            )
                        ),
                        'hide'              => array(
                                'button'              => array(
                                    'fields'            => array('photo','caption'),
                                     'tabs'              => array('photo_style')
                                ),
                                'image'       => array(
                                    'fields'            => array('button_text','buttton_icon_select'),
                                     'tabs'              => array('modal_button_style'),
                                ),
                        ),
                    ),
                    'photo'          => array(
                                'type'          => 'photo',
                                'label'         => __('Select Photo', 'bb-njba'),
                                'show_remove'   => true
                    ),
                    'caption'       => array(
                        'type'          => 'text',
                        'label'         => __('Caption', 'bb-njba'),
                        'default'   => __('Click Here', 'bb-njba'),
                        'preview'       => array(
                            'type'          => 'text',
                            'selector'      => '.caption-selector'
                        )
                    ),
                    'button_text'     => array(
                        'type'      => 'text',
                        'label'     => 'Text',
                        'default'   => __('Click Here', 'bb-njba'),
                        'preview'       => array(
                            'type'          => 'text',
                            'selector'      => '.njba-button-text'
                        )
                    ),
                    'buttton_icon_select'       => array(
                            'type'          => 'select',
                            'label'         => __('Icon Type', 'bb-njba'),
                            'default'       => 'none',
                            'options'       => array(
                                'none'              => __('None', 'bb-njba'),
                                'font_icon'         => __('Icon', 'bb-njba'),
                             ),
                            'toggle' => array(
                                'font_icon'    => array(
                                    'fields'   => array('button_font_icon','button_icon_aligment'),
                                    'sections' => array('icon_section'),
                                ),
                                'none'    => array(
                                    'fields'   => array(),
                                    'sections' => array(),
                                ),
                            ),
                    ),
                    'button_font_icon'          => array(
                                'type'          => 'icon',
                                'label'         => __('Select Icon', 'bb-njba'),
                                'show_remove'   => true
                    ),
                    'button_icon_aligment'       => array(
                                'type'          => 'select',
                                'label'         => __('Icon Position', 'bb-njba'),
                                'default'       => 'left',
                                'options'       => array(
                                    'left'      => __('Before Text', 'bb-njba'),
                                    'right'     => __('After Text', 'bb-njba')
                                ),
                    ),
                )
            ),
           
            'modal_exit_intent'  => array(
                'title'             => __('Exit Intent Settings', 'bb-njba'),
                'fields'            => array(
                    'display_after'      => array(
                        'type'              => 'text',
                        'label'             => __('Display After', 'bb-njba'),
                        'default'           => 1,
                        'description'       => __('day(s)', 'bb-njba'),
                        'help'              => __('If a user closes the modal box, it will be displayed only after the defined day(s).', 'bb-njba'),
                        'class'             => 'input-small modal-display-after'
                    )
                )
            ),
            'modal_esc_exit'    => array(
                'title'             => __('Exit Settings', 'bb-njba'),
                'fields'            => array(
                    'modal_esc'         => array(
                        'type'              => 'select',
                        'label'             => __('Esc to Exit', 'bb-njba'),
                        'default'           => 'enabled',
                        'options'           => array(
                            'enabled'           => __('Yes', 'bb-njba'),
                            'disabled'          => __('No', 'bb-njba')
                        ),
                        'help'              => __('Users can close the modal box by pressing Esc key.', 'bb-njba')
                    ),
                    'modal_click_exit'  => array(
                        'type'              => 'select',
                        'label'             => __('Click to Exit', 'bb-njba'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'bb-njba'),
                            'no'                => __('No', 'bb-njba'),
                        ),
                        'help'              => __('Users can close the modal box by clicking anywhere outside the modal.', 'bb-njba')
                    )
                )
            )
        )
    ),
    'photo_style'       => array( // Tab
        'title'         => __('Photo Style', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array( // Section Fields
                    'style'         => array(
                        'type'          => 'select',
                        'label'         => __('Photo Style', 'bb-njba'),
                        'default'       => 'simple',
                        'options'       => array(
                            '1'        => __('1', 'bb-njba'),
                            '2'          => __('2', 'bb-njba'),
                            '3'         => __('3', 'bb-njba'),
                            '4'     => __('4', 'bb-njba'),
                            '5'      => __('5', 'bb-njba'),
                            
                        ),
                        'toggle' => array(
                            '1' => array(
                                'fields' => array( 'heading_font', 'caption_padding', 'first_font_size', 'first_font_color', 'font_size', 'font_color',  'inside_primary_border_color', 'inside_secondary_border_color', 'hover_color', 'content_box_margin1', 'hover_opacity', 'transition' )
                            ),
                            '2' => array(
                                'fields' => array( 'heading_font', 'caption_padding', 'font_size', 'font_color', 'inside_primary_border', 'inside_primary_border_color', 'inside_secondary_border', 'inside_secondary_border_color', 'hover_color', 'content_box_margin1', 'hover_opacity', 'transition' )
                            ),
                            '3' => array(
                                'fields' => array( 'heading_font', 'caption_padding', 'font_size', 'font_color', 'hover_color', 'hover_opacity', 'transition', 'before_padding', 'after_padding' )
                            ),
                            '4' => array(
                                'fields' => array( 'heading_font', 'caption_padding', 'font_size', 'font_color', 'hover_color', 'hover_opacity', 'transition' )
                            ),
                            '5' => array(
                                'fields' => array( 'heading_font', 'caption_padding', 'font_size', 'font_color', 'hover_color', 'hover_opacity', 'transition' )
                            )
                        )
                    ),
                    'heading_font'          => array(
                        'type'          => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.njba-testimonials-heading'
                        )
                    ),
                   'first_font_size'    => array(
                        'type'          => 'text',
                        'size'          => '5',
                        'maxlength'     => '2',
                        'default'       => '30',
                        'label'         => __('First Font Size', 'bb-njba'),
                        'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '',
                            'property'      => 'font-size',
                            'unit'          => 'px'
                        )
                    ),
                    'first_font_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('First Font Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    
                    'font_size'    => array(
                        'type'          => 'text',
                        'size'          => '5',
                        'maxlength'     => '2',
                        'default'       => '18',
                        'label'         => __('Font Size', 'bb-njba'),
                        'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '',
                            'property'      => 'font-size',
                            'unit'          => 'px'
                        )
                    ),
                    'font_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('Font Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    
                    'inside_primary_border_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('Intside Primary Border Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    'inside_secondary_border_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('Intside Secondary Border Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    'hover_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('Hover Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    'hover_opacity' => array(
                        'type'          => 'text',
                        'label'         => __('Image Hover Opacity', 'bb-njba'),
                        'default'       => '100',
                        'description'   => '%',
                        'maxlength'     => '3',
                        'size'          => '5',
                        'placeholder'   => '100'
                    ),
                    'content_box_margin1'      => array(
                        'type'              => 'text',
                        'label'             => __('Margin', 'bb-njba'),
                        'size'              =>'5'
                    ),
                    'caption_padding'      => array(
                        'type'              => 'text',
                        'label'             => __('Font Padding', 'bb-njba'),
                        'size'              =>'5'
                    ),
                    'before_padding'      => array(
                        'type'              => 'text',
                        'label'             => __('Before Style Padding', 'bb-njba'),
                        'size'              =>'5'
                    ),
                    'after_padding'      => array(
                        'type'              => 'text',
                        'label'             => __('After Style Padding', 'bb-njba'),
                        'size'              =>'5'
                    ),
                    'inside_primary_border'      => array(
                        'type'      => 'select',
                        'label'     => __('Primary Border Style', 'bb-njba'),
                        'default'   => 'none',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'double'  => __('Double', 'bb-njba'),
                        ),
                    ),
                    'inside_secondary_border'      => array(
                        'type'      => 'select',
                        'label'     => __('Secondary Border Style', 'bb-njba'),
                        'default'   => 'none',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'double'  => __('Double', 'bb-njba'),
                        ),
                    ),
                    'transition'      => array(
                        'type'              => 'text',
                        'label'             => __('Transition', 'bb-njba'),
                        'size'              =>'5'
                    )
                )
            )
        )
    ),
    'modal_button_style' => array(
        'title' => __('Button ','bb-njba'),
        'sections' => array(
            'button_style_section' => array(
                'title' => __('Button','bb-njba'),
                'fields' => array(
                    'button_style'         => array(
                        'type'          => 'select',
                        'label'         => __('Style', 'bb-njba'),
                        'default'       => 'flat',
                        'class'         => 'creative_button_styles',
                        'options'       => array(
                            'flat'          => __('Flat', 'bb-njba'),
                            'gradient'      => __('Gradient', 'bb-njba'),
                            'transparent'   => __('Transparent', 'bb-njba'),
                            'threed'          => __('3D', 'bb-njba'),
                        ),
                        'toggle'        => array(
                            'flat'          => array(
                                'fields'        => array('button_background_color','hover_button_style','button_box_shadow','button_box_shadow_color'),
                                'sections' => array('transition_section')
                            ),
                            'gradient'          => array(
                                'fields'        => array('button_background_color')
                            ),
                            'threed'          => array(
                                'fields'        => array('button_background_color','hover_button_style'),
                                'sections' => array('transition_section')
                            ),
                            'transparent' => array(
                                'fields' => array('hover_button_style','button_box_shadow','button_box_shadow_color'),
                                'sections' => array('transition_section')
                            )
                        )
                    ),
                    'button_background_color' => array(
                        'type' => 'color',
                        'label' => __('Background Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'dfdfdf'
                    ),
                    'button_background_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Background Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'button_text_color' => array(
                        'type' => 'color',
                        'label' => __('Text Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '404040'
                    ),
                    'button_text_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Text Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff'
                    ),
                    'button_border_style'      => array(
                        'type'      => 'select',
                        'label'     => __('Border Style', 'bb-njba'),
                        'default'   => 'none',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'double'  => __('Double', 'bb-njba'),
                        ),
                        'toggle' => array(
                            'solid' => array(
                                'fields' => array('button_border_width','button_border_color','button_border_hover_color')
                            ),
                            'dotted' => array(
                                'fields' => array('button_border_width','button_border_color','button_border_hover_color')
                            ),
                            'dashed' => array(
                                'fields' => array('button_border_width','button_border_color','button_border_hover_color')
                            ),
                            'double' => array(
                                'fields' => array('button_border_width','button_border_color','button_border_hover_color')
                            ),
                        )
                    ),
                    'button_border_width' => array(
                        'type' => 'text',
                        'label' => __('Border Width','bb-njba'),
                        'default' => '1',
                        'size' => '5',
                        'description'       => _x( 'px', 'Value unit for spacer width. Such as: "10 px"', 'bb-njba' )
                    ),
                    'button_border_radius'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Border Radius', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top-left'          => 0,
                            'top-right'         => 0,
                            'bottom-left'       => 0,
                            'bottom-right'      => 0
                        ),
                        'options'           => array(
                             'top-left'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'top-right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom-left'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'bottom-right'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'button_border_color' => array(
                        'type' => 'color',
                        'label' => __('Border Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'button_border_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Border Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'button_box_shadow'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Box Shadow', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'left_right'          => 0,
                            'top_bottom'         => 0,
                            'blur'       => 0,
                            'spread'      => 0
                        ),
                        'options'           => array(
                            'left_right'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa fa-arrows-h'
                            ),
                            'top_bottom'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa fa-arrows-v'
                            ),
                            'blur'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa fa-circle-thin'
                            ),
                            'spread'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa fa-circle'
                            )
                            
                        )
                    ),
                    'button_box_shadow_color' => array(
                        'type' => 'color',
                        'label' => __('Box Shadow Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff'
                    ),
                    'button_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 20,
                            'right'         => 40,
                            'bottom'       => 20,
                            'left'      => 40
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'button_margin'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Button Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 0,
                            'right'         => 0,
                            'bottom'       => 0,
                            'left'      => 0
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    )
                )
            ),
            'button_typography' => array(
                'title' => __('Button Typography','bb-njba'),
                'fields' => array(
                    'button_font_family' => array(
                        'type' => 'font',
                        'label' => __('Font Family','bb-njba'),
                        'default' => array(
                            'family' => 'Default',
                            'weight' => 'Default'
                        ),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.njba-btn-main a.njba-btn'
                        )
                    ),
                    'button_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '18',
                            'medium' => '16',
                            'small' => ''
                        )
                    )
                )
            ),
            'icon_section' => array(
                'title' => __('Icon Settings', 'bb-njba'),
                'fields' => array(
                    'icon_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '18',
                            'medium' => '16',
                            'small' => ''
                        )
                    ),
                    'icon_color' => array(
                        'type' => 'color',
                        'label' => __('Icon Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'icon_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Icon Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'icon_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 0,
                            'right'         => 0,
                            'bottom'       => 0,
                            'left'      => 0
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'icon_margin'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 0,
                            'right'         => 0,
                            'bottom'       => 0,
                            'left'      => 0
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    )
                )
            ),
            
            'transition_section' =>array(
                'title' => __('Transition','bb-njba'),
                'fields' => array(
                    'transition' => array(
                        'type' => 'text',
                        'label' => __('Transition delay','bb-njba'),
                        'default' => 0.3,
                        'size' => '5',
                        'description' => 's'
                    ),
                ) 
            ),    
            
            'structure_section' =>array(
                'title' => __('Structure','bb-njba'),
                'fields' => array(
                    'width' => array(
                        'type' => 'select',
                        'label' => __('Width','bb-njba'),
                        'default' => 'auto',
                        'options' => array(
                            'auto' => __('Auto','bb-njba'),
                            'full_width' => __('Full Width','bb-njba'),
                            'custom' => __('Custom','bb-njba')
                        ),
                        'toggle' => array(
                            'auto' => array(
                                'fields' => array('alignment')
                            ),
                            'full_width' => array(
                                'fields' => array('')
                            ),
                            'custom' => array(
                                'fields' => array('custom_width','custom_height','alignment')
                            )
                        )
                    ),
                    'custom_width' => array(
                        'type' => 'text',
                        'label' => __('Custom Width','bb-njba'),
                        'default' => 200,
                        'size' => 10
                    ),
                    'custom_height' => array(
                        'type' => 'text',
                        'label' => __('Custom Height','bb-njba'),
                        'default' => 45,
                        'size' => 10
                    ),
                    'alignment' => array(
                        'type' => 'select',
                        'label' => __('Alignment','bb-njba'),
                        'default' => 'left',
                        'options' => array(
                            'left' => __('Left','bb-njba'),
                            'center' => __('Center','bb-njba'),
                            'right' => __('Right','bb-njba')
                        )   
                    )
                ) 
            ),
            
        )
    ),
    'style'       => array( // Tab
        'title'         => __('Modal Style', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'modal_title'   => array( // Section
                'title'         => __('Title', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'title_font_family' => array(
                        'type'          => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.njba-modal-title'
                        )
                    ),
                    'title_font_size'   => array(
                        'type'              => 'text',
                        'label'             => __('Font Size', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => '18',
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal-title',
                            'property'          => 'font-size',
                            'unit'              => 'px'
                        )
                    ),
                    'title_color'       => array(
                        'type'              => 'color',
                        'label'             => __('Color', 'bb-njba'),
                        'default'           => '444444',
                        'show_reset'    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal-title',
                            'property'          => 'color'
                        )
                    ),
                    'title_bg'          => array(
                        'type'              => 'color',
                        'label'             => __('Background Color', 'bb-njba'),
                        'default'           => 'ffffff',
                        'show_reset'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal-header',
                            'property'          => 'background-color'
                        )
                    ),
                    'title_border'      => array(
                        'type'              => 'text',
                        'label'             => __('Border Bottom', 'bb-njba'),
                        'default'           => 1,
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal-header',
                            'property'          => 'border-bottom-width',
                            'unit'              => 'px'
                        )
                    ),
                    'title_border_style' => array(
                        'type'              => 'select',
                        'label'             => __('Border Style', 'bb-njba'),
                        'default'           => 'solid',
                        'options'           => array(
                            'solid'         => __('Solid', 'bb-njba'),
                            'dashed'        => __('Dashed', 'bb-njba'),
                            'dotted'        => __('Dotted', 'bb-njba'),
                        ),
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal-header',
                            'property'          => 'border-bottom-style'
                        )
                    ),
                    'title_border_color' => array(
                        'type'              => 'color',
                        'label'             => __('Border Color', 'bb-njba'),
                        'default'           => 'eeeeee',
                        'show_reset'    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal-header',
                            'property'          => 'border-bottom-color'
                        )
                    ),
                    'title_padding'     => array(
                        'type'              => 'text',
                        'label'             => __('Side Padding', 'bb-njba'),
                        'default'           => 15,
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'preview'           => array(
                            'type'              => 'css',
                            'rules'             => array(
                                array(
                                    'selector'          => '.njba-modal-title',
                                    'property'          => 'padding-left',
                                    'unit'              => 'px'
                                ),
                                array(
                                    'selector'          => '.njba-modal-title',
                                    'property'          => 'padding-right',
                                    'unit'              => 'px'
                                )
                            )
                        )
                    ),
                    'title_alignment'   => array(
                        'type'              => 'select',
                        'label'             => __('Alignment', 'bb-njba'),
                        'default'           => 'left',
                        'options'           => array(
                            'left'              => __('Left', 'bb-njba'),
                            'center'            => __('Center', 'bb-njba'),
                            'right'             => __('Right', 'bb-njba'),
                        ),
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal-title',
                            'property'          => 'text-align'
                        )
                    )
                )
            ),
            'modal_bg'          => array( // Section
                'title'             => __('Background', 'bb-njba'), // Section Title
                'fields'            => array( // Section Fields
                    'modal_background'  => array(
                        'type'              => 'select',
                        'label'             => __('Background Type', 'bb-njba'),
                        'default'           => 'color',
                        'options'           => array(
                            'color'             => __('Color', 'bb-njba'),
                            'photo'             => __('Image', 'bb-njba')
                        ),
                        'toggle'            => array(
                            'color'             => array(
                                'fields'            => array('modal_bg_color', 'modal_bg_opacity')
                            ),
                            'photo'             => array(
                                'fields'            => array('modal_bg_photo', 'modal_bg_size', 'modal_bg_repeat')
                            )
                        )
                    ),
                    'modal_bg_color'    => array(
                        'type'              => 'color',
                        'label'             => __('Background Color', 'bb-njba'),
                        'default'           => 'ffffff',
                        'show_reset'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal',
                            'property'          => 'background-color'
                        )
                    ),
                    'modal_bg_opacity'  => array(
                        'type'              => 'text',
                        'label'             => __('Opacity', 'bb-njba'),
                        'default'           => 100,
                        'class'             => 'input-small',
                        'description'       => __('%', 'bb-njba'),
                    ),
                    'modal_bg_photo'    => array(
                        'type'              => 'photo',
                        'label'             => __('Background Image', 'bb-njba'),
                        'default'           => '',
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal',
                            'property'          => 'background-image'
                        )
                    ),
                    'modal_bg_size'     => array(
                        'type'          => 'select',
                        'label'         => __('Background Size', 'bb-njba'),
                        'default'       => 'cover',
                        'options'       => array(
                            'contain'   => __('Contain', 'bb-njba'),
                            'cover'     => __('Cover', 'bb-njba'),
                        ),
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal',
                            'property'          => 'background-size'
                        )
                    ),
                    'modal_bg_repeat'   => array(
                        'type'          => 'select',
                        'label'         => __('Background Repeat', 'bb-njba'),
                        'default'       => 'no-repeat',
                        'options'       => array(
                            'repeat-x'      => __('Repeat X', 'bb-njba'),
                            'repeat-y'      => __('Repeat Y', 'bb-njba'),
                            'no-repeat'     => __('No Repeat', 'bb-njba'),
                        ),
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal',
                            'property'          => 'background-repeat'
                        )
                    ),
                    'modal_backlight'   => array(
                        'type'              => 'select',
                        'label'             => __('Backlight Effect', 'bb-njba'),
                        'default'           => 'disabled',
                        'options'           => array(
                            'enabled'           => __('Yes', 'bb-njba'),
                            'disabled'          => __('No', 'bb-njba')
                        ),
                        'help'              => __('A color shadow of background image. It may incompatible with some browsers.', 'bb-njba')
                    )
                )
            ),
            'modal_box'         => array( // Section
                'title'             => __('Box', 'bb-njba'), // Section Title
                'fields'            => array( // Section Fields
                    'modal_border'      => array(
                        'type'              => 'select',
                        'label'             => __('Border', 'bb-njba'),
                        'default'           => 'none',
                        'options'           => array(
                            'none'              => __('None', 'bb-njba'),
                            'dashed'            => __('Dashed', 'bb-njba'),
                            'dotted'            => __('Dotted', 'bb-njba'),
                            'solid'             => __('Solid', 'bb-njba'),
                        ),
                        'toggle'        => array(
                            'dashed'        => array(
                                'fields'        => array('modal_border_width', 'modal_border_color', 'modal_border_position')
                            ),
                            'dotted'        => array(
                                'fields'        => array('modal_border_width', 'modal_border_color', 'modal_border_position')
                            ),
                            'solid'         => array(
                                'fields'        => array('modal_border_width', 'modal_border_color', 'modal_border_position')
                            )
                        )
                    ),
                    'modal_border_width' => array(
                        'type'              => 'text',
                        'label'             => __('Border Width', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => 1,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal',
                            'property'          => 'border-width',
                            'unit'              => 'px'
                        )
                    ),
                    'modal_border_color' => array(
                        'type'              => 'color',
                        'label'             => __('Border Color', 'bb-njba'),
                        'default'           => 'ffffff',
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal',
                            'property'          => 'border-color'
                        )
                    ),
                    'modal_border_position' => array(
                        'type'              => 'select',
                        'label'             => __('Border Position', 'bb-njba'),
                        'default'           => 'default',
                        'options'           => array(
                            'default'           => __('Default', 'bb-njba'),
                            'top'               => __('Top', 'bb-njba'),
                            'bottom'            => __('Bottom', 'bb-njba'),
                            'left'              => __('Left', 'bb-njba'),
                            'right'             => __('Right', 'bb-njba')
                        )
                    ),
                    'modal_border_radius'   => array(
                        'type'                  => 'text',
                        'label'                 => __('Round Corners', 'bb-njba'),
                        'description'           => 'px',
                        'default'               => 2,
                        'class'                 => 'input-small',
                        'preview'               => array(
                            'type'                  => 'css',
                            'rules'                 => array(
                                array(
                                    'selector'              => '.njba-modal',
                                    'property'              => 'border-radius',
                                    'unit'                  => 'px'
                                ),
                                array(
                                    'selector'              => '.njba-modal .njba-modal-header',
                                    'property'              => 'border-top-left-radius',
                                    'unit'                  => 'px'
                                ),
                                array(
                                    'selector'              => '.njba-modal .njba-modal-header',
                                    'property'              => 'border-top-right-radius',
                                    'unit'                  => 'px'
                                )
                            )
                        )
                    ),
                    'modal_padding'     => array(
                        'type'              => 'text',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => 10,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal-content',
                            'property'          => 'padding',
                            'unit'              => 'px'
                        )
                    ),
                    'modal_margin_top'  => array(
                        'type'              => 'text',
                        'label'             => __('Margin Top', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => 0,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal',
                            'property'          => 'margin-top',
                            'unit'              => 'px'
                        )
                    ),
                    'modal_margin_bottom' => array(
                        'type'              => 'text',
                        'label'             => __('Margin Bottom', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => 0,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal',
                            'property'          => 'margin-bottom',
                            'unit'              => 'px'
                        )
                    ),
                    'modal_margin_left' => array(
                        'type'              => 'text',
                        'label'             => __('Margin Left', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => 0,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal',
                            'property'          => 'margin-left',
                            'unit'              => 'px'
                        )
                    ),
                    'modal_margin_right' => array(
                        'type'              => 'text',
                        'label'             => __('Margin Right', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => 0,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal',
                            'property'          => 'margin-right',
                            'unit'              => 'px'
                        )
                    ),
                    'modal_shadow'      => array(
                        'type'              => 'select',
                        'label'             => __('Enable Shadow', 'bb-njba'),
                        'default'           => 'no',
                        'options'           => array(
                            'yes'               => __('Yes', 'bb-njba'),
                            'no'               => __('No', 'bb-njba')
                        ),
                        'toggle'            => array(
                            'yes'               => array(
                                'sections'         => array('modal_box_shadow')
                            )
                        )
                    )
                )
            ),
            'modal_box_shadow'   => array( // Section
                'title'             => __('Box Shadow', 'bb-njba'), // Section Title
                'fields'            => array( // Section Fields
                    'box_shadow_h'      => array(
                        'type'              => 'text',
                        'label'             => __('Horizontal', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => 0,
                    ),
                    'box_shadow_v'      => array(
                        'type'              => 'text',
                        'label'             => __('Vertical', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => 0,
                    ),
                    'box_shadow_blur'   => array(
                        'type'              => 'text',
                        'label'             => __('Blur', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => 18,
                    ),
                    'box_shadow_spread' => array(
                        'type'              => 'text',
                        'label'             => __('Spread', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => 2,
                    ),
                    'box_shadow_color' => array(
                        'type'              => 'color',
                        'label'             => __('Color', 'bb-njba'),
                        'default'           => '000000',
                    ),
                    /*'box_shadow_opacity' => array(
                        'type'              => 'text',
                        'label'             => __('Opacity', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => 0.5,
                    ),*/
                )
            ),
            'modal_content'      => array( // Section
                'title'             => __('Content', 'bb-njba'), // Section Title
                'fields'            => array( // Section Fields
                    'content_border'     => array(
                        'type'              => 'select',
                        'label'             => __('Border', 'bb-njba'),
                        'default'           => 'none',
                        'options'           => array(
                            'none'              => __('None', 'bb-njba'),
                            'dashed'            => __('Dashed', 'bb-njba'),
                            'dotted'            => __('Dotted', 'bb-njba'),
                            'solid'             => __('Solid', 'bb-njba'),
                        ),
                        'toggle'        => array(
                            'dashed'        => array(
                                'fields'        => array('content_border_width', 'content_border_color', 'content_border_radius')
                            ),
                            'dotted'        => array(
                                'fields'        => array('content_border_width', 'content_border_color', 'content_border_radius')
                            ),
                            'solid'         => array(
                                'fields'        => array('content_border_width', 'content_border_color', 'content_border_radius')
                            )
                        ),
                        'trigger'       => array(
                            'dashed'        => array(
                                'fields'        => array('content_border_width', 'content_border_color')
                            ),
                            'dotted'        => array(
                                'fields'        => array('content_border_width', 'content_border_color')
                            ),
                            'solid'         => array(
                                'fields'        => array('content_border_width', 'content_border_color')
                            )
                        )
                    ),
                    'content_border_width' => array(
                        'type'              => 'text',
                        'label'             => __('Border Width', 'bb-njba'),
                        'description'       => 'px',
                        'class'             => 'input-small',
                        'default'           => 1,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal-content-inner',
                            'property'          => 'border-width',
                            'unit'              => 'px'
                        )
                    ),
                    'content_border_color' => array(
                        'type'              => 'color',
                        'label'             => __('Border Color', 'bb-njba'),
                        'default'           => '555555',
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal-content-inner',
                            'property'          => 'border-color'
                        )
                    ),
                    'content_border_radius'  => array(
                        'type'                  => 'text',
                        'label'                 => __('Border Radius', 'bb-njba'),
                        'description'           => 'px',
                        'default'               => 0,
                        'class'                 => 'input-small',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.njba-modal-content-inner',
                            'property'              => 'border-radius',
                            'unit'                  => 'px'
                        )
                    ),
                    'content_text_color'     => array(
                        'type'                  => 'color',
                        'label'                 => __('Text Color', 'bb-njba'),
                        'show_reset'            => true
                    ),
                    'content_padding'        => array(
                        'type'                  => 'text',
                        'label'                 => __('Padding', 'bb-njba'),
                        'description'           => 'px',
                        'default'               => 10,
                        'class'                 => 'input-small',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.njba-modal-content-inner',
                            'property'              => 'padding',
                            'unit'                  => 'px'
                        )
                    )
                )
            ),
            'modal_overlay'      => array( // Section
                'title'             => __('Overlay', 'bb-njba'), // Section Title
                'fields'            => array( // Section Fields
                    'overlay_toggle'    => array(
                        'type'              => 'select',
                        'label'             => __('Overlay', 'bb-njba'),
                        'default'           => 'block',
                        'options'           => array(
                            'block'             => __('Show', 'bb-njba'),
                            'none'              => __('Hide', 'bb-njba')
                        ),
                        'toggle'            => array(
                            'block'             => array(
                                'fields'            => array('overlay_bg_color', 'overlay_opacity')
                            )
                        )
                    ),
                    'overlay_bg_color'  => array(
                        'type'              => 'color',
                        'label'             => __('Background Color', 'bb-njba'),
                        'default'           => '000000',
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal-overlay',
                            'property'          => 'background-color'
                        )
                    ),
                    'overlay_opacity'   => array(
                        'type'              => 'text',
                        'label'             => __('Opacity', 'bb-njba'),
                        'class'             => 'input-small',
                        'default'           => '0.8',
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.njba-modal-overlay',
                            'property'          => 'opacity'
                        )
                    )
                )
            ),
            'modal_close'      => array( // Section
                'title'             => __('Close Button', 'bb-njba'), // Section Title
                'fields'            => array( // Section Fields
                    'close_btn_toggle'   => array(
                        'type'              => 'select',
                        'label'             => __('Button', 'bb-njba'),
                        'default'           => 'block',
                        'options'           => array(
                            'block'             => __('Show', 'bb-njba'),
                            'none'              => __('Hide', 'bb-njba')
                        )
                    ),
                    'close_btn_color'    => array(
                        'type'              => 'color',
                        'label'             => __('Color', 'bb-njba'),
                        'default'           => 'ffffff',
                    ),
                    'close_btn_color_hover' => array(
                        'type'                  => 'color',
                        'label'                 => __('Color Hover', 'bb-njba'),
                        'default'               => 'dddddd',
                        'show_reset'    => true,
                    ),
                    'close_btn_bg'      => array(
                        'type'              => 'color',
                        'label'             => __('Background Color', 'bb-njba'),
                        'default'           => '3a3a3a',
                        'show_reset'        => true,
                    ),
                    'close_btn_bg_hover' => array(
                        'type'              => 'color',
                        'label'             => __('Background Color Hover', 'bb-njba'),
                        'default'           => 'b53030',
                        'show_reset'        => true,
                    ),
                    'close_btn_border'  => array(
                        'type'              => 'text',
                        'label'             => __('Border Width', 'bb-njba'),
                        'default'           => 1,
                        'description'       => 'px',
                        'class'             => 'input-small',
                    ),
                    'close_btn_border_color'    => array(
                        'type'                      => 'color',
                        'label'                     => __('Border Color', 'bb-njba'),
                        'default'                   => 'ffffff',
                        'show_reset'    => true,
                    ),
                    'close_btn_border_radius'   => array(
                        'type'                      => 'text',
                        'label'                     => __('Round Corners', 'bb-njba'),
                        'default'                   => 100,
                        'description'               => 'px',
                        'class'                     => 'input-small',
                    ),
                    'close_btn_size'          => array(
                        'type'                      => 'text',
                        'label'                     => __('Size', 'bb-njba'),
                        'default'                   => 25,
                        'description'               => 'px',
                        'class'                     => 'input-small',
                    ),
                    'close_btn_weight'          => array(
                        'type'                      => 'text',
                        'label'                     => __('Weight', 'bb-njba'),
                        'default'                   => 2,
                        'description'               => 'px',
                        'class'                     => 'input-small',
                    ),
                    'close_btn_position'    => array(
                        'type'                  => 'select',
                        'label'                 => __('Position', 'bb-njba'),
                        'default'               => 'box-top-right',
                        'options'               => array(
                            'box-top-right'         => __('Box - Top Right'),
                            'box-top-left'          => __('Box - Top Left'),
                            'win-top-right'         => __('Window - Top Right'),
                            'win-top-left'          => __('Window - Top Left')
                        ),
                        'toggle'                => array(
                            'box-top-right'         => array(
                                'fields'                => array('close_btn_top', 'close_btn_right')
                            ),
                            'box-top-left'          => array(
                                'fields'                => array('close_btn_top', 'close_btn_left')
                            ),
                            'win-top-right'         => array(
                                'fields'                => array('close_btn_top', 'close_btn_right')
                            ),
                            'win-top-left'          => array(
                                'fields'                => array('close_btn_top', 'close_btn_left')
                            )
                        )
                    ),
                    'close_btn_top'        => array(
                        'type'                      => 'text',
                        'label'                     => __('Top Margin', 'bb-njba'),
                        'default'                   => '-10',
                        'description'               => 'px',
                        'class'                     => 'input-small',
                    ),
                    'close_btn_left'        => array(
                        'type'                      => 'text',
                        'label'                     => __('Left Margin', 'bb-njba'),
                        'default'                   => '-10',
                        'description'               => 'px',
                        'class'                     => 'input-small',
                    ),
                    'close_btn_right'        => array(
                        'type'                      => 'text',
                        'label'                     => __('Right Margin', 'bb-njba'),
                        'default'                   => '-10',
                        'description'               => 'px',
                        'class'                     => 'input-small',
                    )
                )
            ),
            'modal_responsive'  => array( // Section
                'title'             => __('Responsive', 'bb-njba'), // Section Title
                'fields'            => array( // Section Fields
                    'media_breakpoint'  => array(
                        'type'              => 'text',
                        'label'             => __('Media Breakpoint', 'bb-njba'),
                        'default'           => 0,
                        'class'             => 'input-small modal-device-width',
                        'description'       => 'px',
                        'help'              => __('You can set a custom break point and devices with the same or below screen width will always display a full screen modal box.', 'bb-njba'),
                    )
                )
            )
        )
    ),
    'animation'     => array( // Tab
        'title'         => __('Animation', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'modal_anim_load' => array( // Section
                'title'         => __('Animation On Load', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'animation_load'    => array(
                        'type'                  => 'select',
                        'label'                 => __('Animation', 'bb-njba'),
                        'default'               => 'fadeIn',
                        'options'               => njba_modal_animations(),
                    ),
                    'animation_load_duration'  => array(
                        'type'                  => 'text',
                        'label'                 => __('Speed', 'bb-njba'),
                        'description'           => __('seconds', 'bb-njba'),
                        'class'                 => 'input-small',
                        'default'               => '0.5',
                    )
                )
            ),
            'modal_anim_exit' => array( // Section
                'title'         => __('Animation On Exit', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'animation_exit'    => array(
                        'type'                  => 'select',
                        'label'                 => __('Animation', 'bb-njba'),
                        'default'               => 'fadeOut',
                        'options'               => njba_modal_animations(),
                    ),
                    'animation_exit_duration'  => array(
                        'type'                  => 'text',
                        'label'                 => __('Speed', 'bb-njba'),
                        'description'           => __('seconds', 'bb-njba'),
                        'class'                 => 'input-small',
                        'default'               => '0.5',
                    )
                )
            )
        )
    )
));
	
function njba_modal_animations() {
    $animations = array(
        'bounce'                => __('Bounce', 'bb-njba'),
        'bounceIn'              => __('Bounce In', 'bb-njba'),
        'bounceOut'             => __('Bounce Out', 'bb-njba'),
        'fadeIn'                => __('Fade In', 'bb-njba'),
        'fadeInDown'            => __('Fade In Down', 'bb-njba'),
        'fadeInLeft'            => __('Fade In Left', 'bb-njba'),
        'fadeInRight'           => __('Fade In Right', 'bb-njba'),
        'fadeInUp'              => __('Fade In Up', 'bb-njba'),
        'fadeOut'               => __('Fade Out', 'bb-njba'),
        'fadeOutDown'           => __('Fade Out Down', 'bb-njba'),
        'fadeOutLeft'           => __('Fade Out Left', 'bb-njba'),
        'fadeOutRight'          => __('Fade Out Right', 'bb-njba'),
        'fadeOutUp'             => __('Fade Out Up', 'bb-njba'),
        'pulse'                 => __('Pulse', 'bb-njba'),
        'shake'                 => __('Shake', 'bb-njba'),
        'slideInDown'           => __('Slide In Down', 'bb-njba'),
        'slideInLeft'           => __('Slide In Left', 'bb-njba'),
        'slideInRight'          => __('Slide In Right', 'bb-njba'),
        'slideInUp'             => __('Slide In Up', 'bb-njba'),
        'slideOutDown'          => __('Slide Out Down', 'bb-njba'),
        'slideOutLeft'          => __('Slide Out Left', 'bb-njba'),
        'slideOutRight'         => __('Slide Out Right', 'bb-njba'),
        'slideOutUp'            => __('Slide Out Up', 'bb-njba'),
        'swing'                 => __('Swing', 'bb-njba'),
        'tada'                  => __('Tada', 'bb-njba'),
        'zoomIn'                => __('Zoom In', 'bb-njba'),
        'zoomOut'               => __('Zoom Out', 'bb-njba'),
    );
    return $animations;
}
