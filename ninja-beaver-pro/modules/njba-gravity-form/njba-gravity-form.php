<?php
/**
 * @class NJBAGravityFormModule
 */
class NJBAGravityFormModule extends FLBuilderModule {
    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Gravity Form', 'bb-njba'),
            'description'   => __('Addon to display form.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'form_style' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-gravity-form/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-gravity-form/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
            'partial_refresh' => true, // Set this to true to enable partial refresh.
            'icon'              => 'editor-table.svg',
        ));
        /**
         * Use these methods to enqueue css and js already
         * registered or to register and enqueue your own.
         */
        // Already registered
		$this->add_css('font-awesome');
		//$this->add_css('njba-form-frontend', NJBA_MODULE_URL . 'modules/njba-gravity-form/css/frontend.css');
		
    }
    /**
     * Use this method to work with settings data before
     * it is saved. You must return the settings object.
     *
     * @method update
     * @param $settings {object}
     */
    public function update($settings)
    {
        return $settings;
    }
    /**
     * This method will be called by the builder
     * right before the module is deleted.
     *
     * @method delete
     */
    public function delete()
    {
    }
}
 require_once NJBA_MODULE_DIR . 'modules/njba-gravity-form/includes/functions.php';
/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('NJBAGravityFormModule', array(
  
    'gravity_form'       => array( // Tab
        'title'         => __('General', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'select_form'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array( // Section Fields
                    'select_gravity_form_field' => array(
                        'type'          => 'select',
                        'label'         => __('Select Form', 'bb-njba'),
                        'default'       => '',
                        'options'       => count_gravity_forms()
                    ),                           
                )
            ),
             'form_settings'     => array(
                'title'             => __('Settings', 'bb-njba'),
                'fields'            => array(
                    'form_custom_title_desc'   => array(
                        'type'          => 'select',
                        'label'         => __('Custom Title & Description', 'bb-njba'),
                        'default'       => 'no',
                        'options'       => array(
                            'yes'      => __('Yes', 'bb-njba'),
                            'no'     => __('No', 'bb-njba'),
                        ),
                        'toggle' => array(
                            'yes'      => array(
                                'fields'  => array('custom_title', 'custom_description'),
                            ),
                            'no'    => array(
                                'fields'    => array('title_field', 'description_field')
                            )
                        )
                    ),
                    'title_field'   => array(
                        'type'          => 'select',
                        'label'         => __('Title', 'bb-njba'),
                        'default'       => 'true',
                        'options'       => array(
                            'true'      => __('Show', 'bb-njba'),
                            'false'     => __('Hide', 'bb-njba'),
                        ),
                    ),
                    'custom_title'      => array(
                        'type'          => 'text',
                        'label'         => __('Custom Title', 'bb-njba'),
                        'default'       => '',
                        'description'   => '',
                        'preview'       => array(
                            'type'      => 'text',
                            'selector'  => '.gravity-form-title',
                        )
                    ),
                    'description_field' => array(
                        'type'          => 'select',
                        'label'         => __('Description', 'bb-njba'),
                        'default'       => 'true',
                        'options'       => array(
                            'true'      => __('Show', 'bb-njba'),
                            'false'     => __('Hide', 'bb-njba'),
                        ),
                    ),
                    'custom_description'    => array(
                        'type'              => 'textarea',
                        'label'             => __('Custom Description', 'bb-njba'),
                        'default'           => '',
                        'placeholder'       => '',
                        'rows'              => '6',
                        'preview'           => array(
                            'type'          => 'text',
                            'selector'      => '.gravity-form-description',
                        )
                    ),
                    'labels_toggle'   => array(
                        'type'         => 'select',
                        'label'        => __('Labels', 'bb-njba'),
                        'default'      => 'block',
                        'options'      => array(
                            'block'    => __('Show', 'bb-njba'),
                            'none'     => __('Hide', 'bb-njba'),
                        ),
                    ),
                  
                    'form_tab_index'      => array(
                        'type'          => 'text',
                        'label'         => __('Tab Index', 'bb-njba'),
                        'class'         => 'bb-gf-input input-small',
                        'default'       => 1,
                    ),
                )
            ),
        )
    ),
     'style'       => array( // Tab
        'title'         => __('Style', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'form_setting'      => array( // Section
                'title'         => __('Form Background', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'form_bg_type' => array(
                            'type'          => 'select',
                            'label'         => __( 'Background Type', 'bb-njba' ),
                            'default'       => 'none',
                            'options'       => array(
                                'none'          => __( 'None', 'bb-njba' ),
                                'color'         => __( 'Color', 'bb-njba' ),
                                'image'         => __( 'Image', 'bb-njba' ),
                            ),
                            'toggle'    => array(
                                'color'     => array(
                                    'fields'    => array( 'form_bg_color', 'form_bg_color_opc' )
                                ),
                                'image' => array(
                                    'fields'    => array( 'form_bg_img', 'form_bg_img_pos', 'form_bg_img_size', 'form_bg_img_repeat' )
                                ),
                            ),
                    ),
                    'form_bg_img'         => array(
                        'type'          => 'photo',
                        'label'         => __( 'Photo', 'bb-njba' ),
                        'show_remove'   => true,
                    ),
                    'form_bg_img_pos' => array(
                            'type'          => 'select',
                            'label'         => __( 'Background Position', 'bb-njba' ),
                            'default'       => 'center center',
                            'options'       => array(
                                'left top'          => __( 'Left Top', 'bb-njba' ),
                                'left center'       => __( 'Left Center', 'bb-njba' ),
                                'left bottom'       => __( 'Left Bottom', 'bb-njba' ),
                                'center top'        => __( 'Center Top', 'bb-njba' ),
                                'center center'     => __( 'Center Center', 'bb-njba' ),
                                'center bottom'     => __( 'Center Bottom', 'bb-njba' ),
                                'right top'         => __( 'Right Top', 'bb-njba' ),
                                'right center'      => __( 'Right Center', 'bb-njba' ),
                                'right bottom'      => __( 'Right Bottom', 'bb-njba' ),
                            ),
                    ),
                    'form_bg_img_repeat' => array(
                            'type'          => 'select',
                            'label'         => __( 'Background Repeat', 'bb-njba' ),
                            'default'       => 'repeat',
                            'options'       => array(
                                'no-repeat'     => __( 'No Repeat', 'bb-njba' ),
                                'repeat'        => __( 'Repeat All', 'bb-njba' ),
                                'repeat-x'      => __( 'Repeat Horizontally', 'bb-njba' ),
                                'repeat-y'      => __( 'Repeat Vertically', 'bb-njba' ),
                            ),
                    ),
                    'form_bg_img_size' => array(
                            'type'          => 'select',
                            'label'         => __( 'Background Size', 'bb-njba' ),
                            'default'       => 'cover',
                            'options'       => array(
                                'contain'   => __( 'Contain', 'bb-njba' ),
                                'cover'     => __( 'Cover', 'bb-njba' ),
                                'initial'   => __( 'Initial', 'bb-njba' ),
                                'inherit'   => __( 'Inherit', 'bb-njba' ),
                            ),
                    ),
                    'form_bg_color' => array( 
                        'type'       => 'color',
                        'label'     => __( 'Background Color', 'bb-njba' ),
                        'default'    => '',
                        'show_reset' => true,
                        'preview'      => array(
                            'type'         => 'css',
                            'selector'     => '.njba-gravity-form-content',
                            'property'     => 'background-color'
                        )
                    ),
                    'form_bg_color_opc' => array( 
                        'type'        => 'text',
                        'label'     => __( 'Background Color Opacity', 'bb-njba' ),
                        'default'     => '',
                        'description' => '%',
                        'maxlength'   => '3',
                        'size'        => '5',
                    ),
                )
            ),
            'form_border'       => array(
                'title'             => __('Form Border', 'bb-njba'),
                'fields'            => array(
                    'form_border_width'      => array(
                        'type'          => 'text',
                        'label'         => __('Border Width', 'bb-njba'),
                        'description'   => 'px',
                        'class'         => 'bb-gf-input input-small',
                        'default'       => 0,
                        'preview'       => array(
                            'type'      => 'css',
                            'selector'  => '.njba-gravity-form-content',
                            'property'  => 'border-width',
                            'unit'      => 'px'
                        )
                    ),
                    'form_border_color'     => array(
                        'type'          => 'color',
                        'label'         => __('Border Color', 'bb-njba'),
                        'default'       => 'dddddd',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'      => 'css',
                            'selector'  => '.njba-gravity-form-content',
                            'property'  => 'border-color'
                        )
                    ),
                    'form_border_style'   => array(
                        'type'          => 'select',
                        'label'         => __('Border Style', 'bb-njba'),
                        'default'       => 'solid',
                        'options'   => array(
                            'solid'   => __('Solid', 'bb-njba'),
                          'dashed'  => __('Dashed', 'bb-njba'),
                          'dotted'  => __('Dotted', 'bb-njba'),
                        ),
                        'preview'       => array(
                            'type'      => 'css',
                            'selector'  => '.njba-gravity-form-content',
                            'property'  => 'border-style'
                        )
                    ),
                )
            ),
            'form_container'        => array(
                'title'                 => __('Corners & Padding', 'bb-njba'),
                'fields'                => array(
                    'form_border_radius'  => array(
                        'type'          => 'text',
                        'label'         => __('Round Corners', 'bb-njba'),
                        'description'   => 'px',
                        'default'       => 2,
                        'class'         => 'bb-gf-input input-small',
                        'preview'       => array(
                            'type'      => 'css',
                            'selector'  => '.njba-gravity-form-content',
                            'property'  => 'border-radius',
                            'unit'      => 'px'
                        )
                    ),
                    'form_padding'  => array(
                        'type'      => 'njba-multinumber',
                        'label'     => __('Padding', 'bb-njba'),
                        'description'   => 'px',
                        'default'       => array(
                            'top' => 15,
                            'right' => 15,
                            'bottom' => 15,
                            'left' => 15,
                        ),
                        'options'     => array(
                            'top' => array(
                                'maxlength' => 3,
                                'placeholder'   =>  __('Top', 'bb-njba'),
                                'tooltip'       => __('Top', 'bb-njba'),
                                'icon'    => 'fa-long-arrow-up',
                                'preview'       => array(
                                    'type'      => 'css',
                                    'selector'  => '.njba-gravity-form-content',
                                    'property'  => 'padding-top',
                                    'unit'      => 'px'
                                )
                            ),
                            'bottom' => array(
                                'maxlength' => 3,
                                'placeholder'   =>  __('Bottom', 'bb-njba'),
                                'tooltip'       => __('Bottom', 'bb-njba'),
                                'icon'    => 'fa-long-arrow-down',
                                'preview'       => array(
                                    'type'      => 'css',
                                    'selector'  => '.njba-gravity-form-content',
                                    'property'  => 'padding-bottom',
                                    'unit'      => 'px'
                                )
                            ),
                            'left' => array(
                                'maxlength' => 3,
                                'placeholder'   =>  __('Left', 'bb-njba'),
                                'tooltip'       => __('Left', 'bb-njba'),
                                'icon'    => 'fa-long-arrow-left',
                                'preview'       => array(
                                    'type'      => 'css',
                                    'selector'  => '.njba-gravity-form-content',
                                    'property'  => 'padding-left',
                                    'unit'      => 'px'
                                )
                            ),
                            'right' => array(
                                'maxlength' => 3,
                                'placeholder'   =>  __('Right', 'bb-njba'),
                                'tooltip'       => __('Right', 'bb-njba'),
                                'icon'    => 'fa-long-arrow-right',
                                'preview'       => array(
                                    'type'      => 'css',
                                    'selector'  => '.njba-gravity-form-content',
                                    'property'  => 'padding-right',
                                    'unit'      => 'px'
                                )
                           ),
                        ),
                    )
                )
            ),
            'general_style'    => array( // Section
                'title'         => __('Title & description Alignment', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'title_alignment'    => array(
                        'type'                      => 'select',
                        'label'                     => __('Title Alignment', 'bb-njba'),
                        'default'                   => 'left',
                        'options'                   => array(
                            'left'                  => __('Left', 'bb-njba'),
                            'center'                => __('Center', 'bb-njba'),
                            'right'                 => __('Right', 'bb-njba'),
                        ),
                        'preview'       => array(
                            'type'      => 'css',
                            'selector'  => '.gravity-form-title,.gform_title',
                            'property'  => 'text-align'
                        )
                    ),
                    'description_alignment'    => array(
                        'type'                      => 'select',
                        'label'                     => __('Description Alignment', 'bb-njba'),
                        'default'                   => 'left',
                        'options'                   => array(
                            'left'                  => __('Left', 'bb-njba'),
                            'center'                => __('Center', 'bb-njba'),
                            'right'                 => __('Right', 'bb-njba'),
                        ),
                        'preview'       => array(
                            'type'      => 'css',
                            'selector'  => '.gravity-form-description, span.gform_description',
                            'property'  => 'text-align'
                        )
                    ),
                )
            ),
        )
    ), 
      'input_style'           => array(
        'title'                 => __('Inputs', 'bb-njba'),
        'sections'              => array(
            'input_background'      => array(
                'title'                 => __('Colors', 'bb-njba'),
                'fields'                => array(
                    'input_field_text_color'    => array(
                        'type'                  => 'color',
                        'label'                 => __('Text Color', 'bb-njba'),
                        'default'               => '333333',
                        'show_reset'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.gform_wrapper .gfield input:not([type="radio"]):not([type="checkbox"]):not([type="submit"]):not([type="button"]):not([type="image"]):not([type="file"]), .gform_wrapper .gfield select, .gform_wrapper .gfield textarea',
                            'property'              => 'color'
                        )
                    ),
                    'input_field_bg_color'      => array(
                        'type'                  => 'color',
                        'label'                 => __('Background Color', 'bb-njba'),
                        'default'               => 'ffffff',
                        'show_reset'            => true,
                        'preview'               => array(
                            'type'              => 'css',
                            'selector'          => '.gform_wrapper .gfield input:not([type="radio"]):not([type="checkbox"]):not([type="submit"]):not([type="button"]):not([type="image"]):not([type="file"]), .gform_wrapper .gfield select, .gform_wrapper .gfield textarea',
                            'property'          => 'background-color'
                        )
                    ),
                    'input_field_background_opacity'    => array(
                        'type'                 => 'text',
                        'label'                => __('Background Opacity', 'bb-njba'),
                        'default'              => '1',
                        'description'          => __('between 0 to 1', 'bb-njba'),
                        'preview'              => array(
                            'type'             => 'css',
                            'selector'         => '.gform_wrapper .gfield input:not([type="radio"]):not([type="checkbox"]):not([type="submit"]):not([type="button"]):not([type="image"]):not([type="file"]), .gform_wrapper .gfield select, .gform_wrapper .gfield textarea',
                            'property'         => 'opacity',
                            'unit'              => '%'
                        )
                    ),
                    'input_desc_color'  => array(
                        'type'                  => 'color',
                        'label'                 => __('Description Color', 'bb-njba'),
                        'default'               => '000000',
                        'show_reset'            => true,
                        'preview'               => array(
                            'type'              => 'css',
                            'selector'          => '.gfield_description',
                            'property'          => 'color'
                        )
                    ),
                )
            ),
            'input_border'      => array(
                'title'             => __('Border', 'bb-njba'),
                'fields'            => array(
                    'input_field_border_color'  => array(
                        'type'                  => 'color',
                        'label'                 => __('Border Color', 'bb-njba'),
                        'default'               => '828282',
                        'show_reset'            => true,
                        'preview'               => array(
                            'type'              => 'css',
                            'selector'          => '.gform_wrapper .gfield input:not([type="radio"]):not([type="checkbox"]):not([type="submit"]):not([type="button"]):not([type="image"]):not([type="file"]), .gform_wrapper .gfield select, .gform_wrapper .gfield textarea',
                            'property'          => 'border-color'
                        )
                    ),
                    'input_field_border_width'    => array(
                        'type'                    => 'text',
                        'label'                   => __('Border Width', 'bb-njba'),
                        'description'             => 'px',
                        'default'                 => '1',
                        'preview'                 => array(
                            'type'                => 'css',
                            'rules'                 => array(
                                array(
                                    'selector'            => '.gform_wrapper .gfield input:not([type="radio"]):not([type="checkbox"]):not([type="submit"]):not([type="button"]):not([type="image"]):not([type="file"]), .gform_wrapper .gfield select, .gform_wrapper .gfield textarea',
                                    'property'            => 'border-width',
                                    'unit'                => 'px',
                                ),
                                array(
                                    'selector'            => '.gform_wrapper .gfield input:not([type="radio"]):not([type="checkbox"]):not([type="submit"]):not([type="button"]):not([type="image"]):not([type="file"]), .gform_wrapper .gfield select, .gform_wrapper .gfield textarea',
                                    'property'            => 'border-top-width',
                                    'unit'                => 'px',
                                ),
                                array(
                                    'selector'            => '.gform_wrapper .gfield input:not([type="radio"]):not([type="checkbox"]):not([type="submit"]):not([type="button"]):not([type="image"]):not([type="file"]), .gform_wrapper .gfield select, .gform_wrapper .gfield textarea',
                                    'property'            => 'border-bottom-width',
                                    'unit'                => 'px',
                                ),
                                array(
                                    'selector'            => '.gform_wrapper .gfield input:not([type="radio"]):not([type="checkbox"]):not([type="submit"]):not([type="button"]):not([type="image"]):not([type="file"]), .gform_wrapper .gfield select, .gform_wrapper .gfield textarea',
                                    'property'            => 'border-left-width',
                                    'unit'                => 'px',
                                ),
                                array(
                                    'selector'            => '.gform_wrapper .gfield input:not([type="radio"]):not([type="checkbox"]):not([type="submit"]):not([type="button"]):not([type="image"]):not([type="file"]), .gform_wrapper .gfield select, .gform_wrapper .gfield textarea',
                                    'property'            => 'border-right-width',
                                    'unit'                => 'px',
                                )
                            )
                        )
                    ),
                    'input_field_border_position'    => array(
                        'type'                    => 'select',
                        'label'                   => __('Border Position', 'bb-njba'),
                        'default'                 => 'border',
                        'options'                 => array(
                            'border'              => __('Default', 'bb-njba'),
                            'border-top'          => __('Top', 'bb-njba'),
                            'border-bottom'       => __('Bottom', 'bb-njba'),
                            'border-left'         => __('Left', 'bb-njba'),
                            'border-right'        => __('Right', 'bb-njba'),
                        ),
                    ),
                    'input_field_focus_color'      => array(
                        'type'                  => 'color',
                        'label'                 => __('Focus Border Color', 'bb-njba'),
                        'default'               => '5b5b5b',
                        'show_reset'            => true,
                    ),
                )
            ),
            'input_general'      => array( // Section
                'title'         => __('General', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'input_field_width'     => array(
                        'type'              => 'select',
                        'label'             => __('Full Width', 'bb-njba'),
                        'default'           => 'false',
                        'options'           => array(
                            'true'          => __('Yes', 'bb-njba'),
                            'false'         => __('No', 'bb-njba'),
                        )
                    ),
                    'input_field_text_alignment'    => array(
                        'type'                      => 'select',
                        'label'                     => __('Text Alignment', 'bb-njba'),
                        'default'                   => 'left',
                        'options'                   => array(
                            'left'                  => __('Left', 'bb-njba'),
                            'center'                => __('Center', 'bb-njba'),
                            'right'                 => __('Right', 'bb-njba'),
                        )
                    ),
                    'input_field_border_radius'    => array(
                        'type'                     => 'text',
                        'label'                    => __('Round Corners', 'bb-njba'),
                        'description'              => 'px',
                        'default'                  => '2',
                    ),
                     'input_field_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'default'           => array(
                            'top'          => 12,
                            'bottom'       => 12,
                            'left'         => 12,
                            'right'        => 12,
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up',
                                'default'           => '40',
                                'description'       => 'px',
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down',
                                'default'           => '40',
                                'description'       => 'px',
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left',
                                'default'           => '40',
                                'description'       => 'px',
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right',
                                'default'           => '40',
                                'description'       => 'px',
                            )
                        )
                    ),
                    'input_field_margin'    => array(
                        'type'              => 'text',
                        'label'             => __('Spacing Between Inputs', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => '10',
                    ),
                    'textarea_height' => array(
                        'type' => 'text',
                        'label' => __('Textarea Height', 'bb-njba'),
                        'placeholder' => '130',
                        'size' => '8',
                        'description' => __('px', 'bb-njba'),
                    ),
                )
            ),
            'placeholder_style'      => array( // Section
                'title'         => __('Placeholder', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'gf_input_placeholder_display'  => array(
                        'type'          => 'select',
                        'label'         => __('Show Placeholder', 'bb-njba'),
                        'default'       => 'block',
                        'options'       => array(
                            'block' => __('Yes', 'bb-njba'),
                            'none'  => __('No', 'bb-njba'),
                        ),
                        'toggle' => array(
                            'block' => array(
                                'fields' => array('gf_input_placeholder_color')
                            )
                        )
                    ),
                    'gf_input_placeholder_color'  => array(
                        'type'                  => 'color',
                        'label'                 => __('Color', 'bb-njba'),
                        'default'               => 'eeeeee',
                        'show_reset'            => true,
                    ),
                )
            ),
        )
    ),
    'button_style'      => array(
        'title'             => __('Button', 'bb-njba'),
        'sections'          => array(
            'button_bg'         => array(
                'title'             => __('Colors', 'bb-njba'),
                'fields'            => array(
                    'button_text_color' => array(
                        'type'          => 'color',
                        'label'         => __('Text Color', 'bb-njba'),
                        'default'       => '000000',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'      => 'css',
                            'selector'  => '.gform_wrapper .gform_footer .gform_button',
                            'property'  => 'color'
                        )
                    ),
                    'button_hover_text_color'    => array(
                        'type'                   => 'color',
                        'label'                  => __('Text Color Hover', 'bb-njba'),
                        'default'                => 'eeeeee',
                        'show_reset'             => true,
                        'preview'                => array(
                            'type'               => 'css',
                            'selector'           => '.gform_wrapper .gform_footer .gform_button:hover',
                            'property'           => 'color'
                        )
                    ),
                    'button_bg_color'   => array(
                        'type'          => 'color',
                        'label'         => __('Background Color', 'bb-njba'),
                        'default'       => '333333',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'      => 'css',
                            'selector'  => '.gform_wrapper .gform_footer .gform_button',
                            'property'  => 'background-color'
                        )
                    ),
                    'button_background_opacity'    => array(
                        'type'                 => 'text',
                        'label'                => __('Background Opacity', 'bb-njba'),
                        'default'              => '1',
                        'preview'              => array(
                            'type'             => 'css',
                            'selector'         => '.gform_wrapper .gform_footer .gform_button',
                            'property'         => 'opacity',
                            'unit'              =>  '%',
                        )
                    ),
                    'button_hover_bg_color'    => array(
                        'type'                 => 'color',
                        'label'                => __('Background Color Hover', 'bb-njba'),
                        'default'              => '000000',
                        'show_reset'           => true,
                        'preview'              => array(
                            'type'             => 'css',
                            'selector'         => '.gform_wrapper .gform_footer .gform_button:hover',
                            'property'         => 'background-color'
                        )
                    ),
                )
            ),
            'button_border'     => array(
                'title'             => __('Border', 'bb-njba'),
                'fields'            => array(
                    'button_border_width'    => array(
                        'type'               => 'text',
                        'label'              => __('Border Width', 'bb-njba'),
                        'description'        => 'px',
                        'default'            => '1',
                        'preview'            => array(
                            'type'           => 'css',
                            'selector'       => '.gform_wrapper .gform_footer .gform_button',
                            'property'       => 'border-width',
                            'unit'           => 'px'
                        )
                    ),
                    'button_border_color'    => array(
                        'type'               => 'color',
                        'label'              => __('Border Color', 'bb-njba'),
                        'default'            => '333333',
                        'show_reset'         => true,
                        'preview'            => array(
                            'type'           => 'css',
                            'selector'       => '.gform_wrapper .gform_footer .gform_button',
                            'property'       => 'border-color'
                        )
                    ),
                )
            ),
            'button_settings'       => array( // Section
                'title'             => __('Size & Alignment', 'bb-njba'), // Section Title
                'fields'            => array( // Section Fields
                    'button_width'  => array(
                        'type'      => 'select',
                        'label'     => __('Full Width', 'bb-njba'),
                        'default'   => 'false',
                        'options'   => array(
                            'true'  => __('Yes', 'bb-njba'),
                            'false' => __('No', 'bb-njba'),
                        ),
                        'toggle'    => array(
                            'false' => array(
                                'fields'    => array('button_width_size', 'button_alignment')
                            )
                        )
                    ),
                    'button_alignment'  => array(
                        'type'          => 'select',
                        'label'         => __('Button Alignment', 'bb-njba'),
                        'default'       => 'left',
                        'options'       => array(
                            'left'      => __('Left', 'bb-njba'),
                            'center'    => __('Center', 'bb-njba'),
                            'right'     => __('Right', 'bb-njba'),
                        )
                    ),
                )
            ),
            'button_corners'        => array(
                'title'                 => __('Corners & Padding', 'bb-njba'),
                'fields'                => array(
                    'button_border_radius'    => array(
                        'type'                => 'text',
                        'label'               => __('Round Corners', 'bb-njba'),
                        'description'         => 'px',
                        'default'             => '2',
                        'preview'             => array(
                            'type'            => 'css',
                            'selector'        => '.gform_wrapper .gform_footer .gform_button',
                            'property'        => 'border-radius',
                            'unit'            => 'px'
                        )
                    ),              
                    'button_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'default'           => array(
                            'top'          => 10,
                            'bottom'       => 10,
                            'left'         => 10,
                            'right'        => 10,
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up',
                                'default'           => '40',
                                'description'       => 'px',
                                'preview'           => array(
                                    'selector'          => '.gform_wrapper .gform_footer .gform_button',
                                    'property'          => 'padding-top',
                                    'unit'              =>  'px'
                                ),
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down',
                                'default'           => '40',
                                'description'       => 'px',
                                'preview'           => array(
                                    'selector'          => '.gform_wrapper .gform_footer .gform_button',
                                    'property'          => 'padding-bottom',
                                    'unit'              =>  'px'
                                ),
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left',
                                'default'           => '40',
                                'description'       => 'px',
                                'preview'           => array(
                                    'selector'          => '',
                                    'property'          => 'padding-left',
                                    'unit'              =>  'px'
                                ),
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right',
                                'default'           => '40',
                                'description'       => 'px',
                                'preview'           => array(
                                    'selector'          => '.gform_wrapper .gform_footer .gform_button',
                                    'property'          => 'padding-right',
                                    'unit'              =>  'px'
                                ),
                            )
                        )
                    ),
                )
            ),
        )
    ),
    'error_style'   => array(
        'title'         => __('Errors', 'bb-njba'),
        'sections'      => array(
            'form_error_styling'    => array( // Section
                'title'             => __('Errors Style', 'bb-njba'), // Section Title
                'fields'            => array( // Section Fields
                    'validation_error'  => array(
                        'type'          => 'select',
                        'label'         => __('Validation Error', 'bb-njba'),
                        'default'       => 'block',
                        'options'       => array(
                            'block'     => __('Show', 'bb-njba'),
                            'none'      => __('Hide', 'bb-njba'),
                        ),
                        'toggle' => array(
                            'block' => array(
                                'fields'    => array('validation_error_color'),
                                'sections'  => array('errors_typography')
                            )
                        )
                    ),
                    'validation_error_color'    => array(
                        'type'                  => 'color',
                        'label'                 => __('Error Description Color', 'bb-njba'),
                        'default'               => '790000',
                        'show_reset'            => true,
                        'preview'               => array(
                            'type'              => 'css',
                            'selector'          => '.gform_wrapper .validation_error',
                            'property'          => 'color'
                        )
                    ),
                    'validation_error_border_color'    => array(
                        'type'                         => 'color',
                        'label'                        => __('Error Border Color', 'bb-njba'),
                        'default'                      => '790000',
                        'show_reset'                   => true,
                        'preview'                      => array(
                            'type'                     => 'css',
                            'selector'                 => '.gform_wrapper .validation_error',
                            'property'                 => 'border-color'
                        )
                    ),
                    'form_error_field_background_color'    => array(
                        'type'                             => 'color',
                        'label'                            => __('Error Field Background Color', 'bb-njba'),
                        'default'                          => 'ffdfe0',
                        'show_reset'                       => true,
                        'preview'                          => array(
                            'type'                         => 'css',
                            'selector'                     => '.gform_wrapper .gfield.gfield_error',
                            'property'                     => 'color'
                        )
                    ),
                    'form_error_field_label_color'    => array(
                        'type'                        => 'color',
                        'label'                       => __('Error Field Label Color', 'bb-njba'),
                        'default'                     => '790000',
                        'show_reset'                  => true,
                        'preview'                     => array(
                            'type'                    => 'css',
                            'selector'                => '.gform_wrapper .gfield.gfield_error .gfield_label',
                            'property'                => 'color'
                        )
                    ),
                    'form_error_input_border_color'    => array(
                        'type'                         => 'color',
                        'label'                        => __('Error Field Input Border Color', 'bb-njba'),
                        'default'                      => '790000',
                        'show_reset'                   => true,
                        'preview'                      => array(
                            'type'                     => 'css',
                            'selector'                 => '.gform_wrapper .gfield_error .ginput_container input, .gform_wrapper .gfield_error .ginput_container select, .gform_wrapper .gfield_error .ginput_container textarea',
                            'property'                 => 'color'
                        )
                    ),
                    'form_error_input_border_width'    => array(
                        'type'                         => 'text',
                        'label'                        => __('Error Field Input Border Width', 'bb-njba'),
                        'description'                  => 'px',
                        'default'                      => '1',
                        'preview'                      => array(
                            'type'                     => 'css',
                            'selector'                 => '.gform_wrapper .gfield_error .ginput_container input, .gform_wrapper .gfield_error .ginput_container select, .gform_wrapper .gfield_error .ginput_container textarea',
                            'property'                 => 'border-width',
                            'unit'                     => 'px'
                        )
                    ),
                    'validation_message'   => array(
                        'type'             => 'select',
                        'label'            => __('Error Field Message', 'bb-njba'),
                        'default'          => 'block',
                        'options'          => array(
                            'block'        => __('Show', 'bb-njba'),
                            'none'         => __('Hide', 'bb-njba'),
                        ),
                        'toggle'    => array(
                            'block' => array(
                                'fields'    => array('validation_message_color')
                            )
                        )
                    ),
                    'validation_message_color'    => array(
                        'type'                    => 'color',
                        'label'                   => __('Error Field Message Color', 'bb-njba'),
                        'default'                 => '790000',
                        'show_reset'              => true,
                        'preview'                 => array(
                            'type'                => 'css',
                            'selector'            => '.gform_wrapper .gfield_error .validation_message',
                            'property'            => 'color'
                        )
                    ),
                )
            ),
        )
    ),
    
    'form_typography'       => array( // Tab
        'title'         => __('Typography', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'title_typography'       => array( // Section
                'title'         => __('Title', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'title_font_family' => array(
                        'type'          => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                        
                    ),
                    'title_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
                    ),
                     'title_line_height'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Line Height', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        )
                    ),
                    'title_color'       => array(
                        'type'          => 'color',
                        'label'         => __('Color', 'bb-njba'),
                        'default'       => '',
                        'show_reset'    => true,
                    ), 
                )
            ),
            'description_typography'    => array(
                'title' => __('Description', 'bb-njba'),
                'fields'    => array(
                    'description_font_family' => array(
                        'type'          => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                    ),
                    'description_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
                    ),
                    'description_line_height'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Line Height', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        )
                    ),
                    'description_color' => array(
                        'type'          => 'color',
                        'label'         => __('Color', 'bb-njba'),
                        'default'       => '',
                        'show_reset'    => true,
                    ),  
                )
            ),
            'label_typography'       => array( // Section
                'title'         => __('Label', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'label_font_family' => array(
                        'type'          => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                    ),
                    'label_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        )
                    ),
                    'form_label_color'  => array(
                        'type'          => 'color',
                        'label'         => __('Color', 'bb-njba'),
                        'default'       => '',
                        'show_reset'    => true,
                    ),
                )
            ),
            'input_typography'       => array( // Section
                'title'         => __('Input', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'input_font_family' => array(
                        'type'          => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                    ),
                    'input_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        )
                    ),
        
                )
            ),
            'button_typography'       => array( // Section
                'title'         => __('Button', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'button_font_family' => array(
                        'type'          => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                    ),
                    'button_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        )
                    ),
                    'button_text_transform'    => array(
                        'type'                      => 'select',
                        'label'                     => __('Text Transform', 'bb-njba'),
                        'default'                   => 'none',
                        'options'                   => array(
                            'none'                  => __('Default', 'bb-njba'),
                            'lowercase'                => __('lowercase', 'bb-njba'),
                            'uppercase'                 => __('UPPERCASE', 'bb-njba'),
                        )
                    ),
                )
            ),
        )
    )
));