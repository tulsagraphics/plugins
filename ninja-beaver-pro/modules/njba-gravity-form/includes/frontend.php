<?php
?>
<div class="njba-gravity-form-content">
	<h3 class="gravity-form-title">
	<?php if ( $settings->custom_title ) {
	 	echo $settings->custom_title;
	} ?>
	</h3>
	<p class="gravity-form-description">
	<?php if ( $settings->custom_description ) {
		echo $settings->custom_description;
	} ?>
	</p>
    <?php
    if ( $settings->select_gravity_form_field ) {
        echo do_shortcode( '[gravityform id='.absint( $settings->select_gravity_form_field ).' title='.$settings->title_field.' description='.$settings->description_field.'  ajax=true tabindex='. $settings->form_tab_index .']' );
    }
    ?>
</div>
