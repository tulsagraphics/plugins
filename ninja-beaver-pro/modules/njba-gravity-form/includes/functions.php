<?php
function count_gravity_forms(){
	 $options = array( '' => __('None', 'bb-njba') );
    if ( class_exists( 'GFForms' ) ) {
        $forms = RGFormsModel::get_forms( null, 'title' );
        if ( count( $forms ) ) {
            foreach ( $forms as $form )
            $options[$form->id] = $form->title;
        }
    }
    return $options;
  
}
