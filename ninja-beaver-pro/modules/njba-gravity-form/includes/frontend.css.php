
<?php $settings->input_field_background_opacity = ( $settings->input_field_background_opacity != '' ) ? $settings->input_field_background_opacity : '100'; ?>
<?php $settings->button_background_opacity = ( $settings->button_background_opacity != '' ) ? $settings->button_background_opacity : '100'; ?>
<?php $settings->form_bg_color_opc = ( $settings->form_bg_color_opc != '' ) ? $settings->form_bg_color_opc : '100'; ?>
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield input:not([type='radio']):not([type='checkbox']):not([type='submit']):not([type='button']):not([type='image']):not([type='file']),
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield input:focus,
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield select,
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield textarea {
    <?php if( $settings->input_field_text_color ) { ?>
    color: #<?php echo $settings->input_field_text_color; ?>;
    <?php } ?>
    <?php if( $settings->input_field_bg_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->input_field_bg_color )) ?>, <?php echo $settings->input_field_background_opacity/100; ?>); <?php } ?>
    border-width: 0;
    height:auto;
    border-color: <?php echo $settings->input_field_border_color ? '#' . $settings->input_field_border_color : 'transparent'; ?>;
    <?php if( $settings->input_field_border_radius >= 0 ) { ?>
    border-radius: <?php echo $settings->input_field_border_radius; ?>px;
    -moz-border-radius: <?php echo $settings->input_field_border_radius; ?>px;
    -webkit-border-radius: <?php echo $settings->input_field_border_radius; ?>px;
    -ms-border-radius: <?php echo $settings->input_field_border_radius; ?>px;
    -o-border-radius: <?php echo $settings->input_field_border_radius; ?>px;
    <?php } ?>
    <?php if( $settings->input_field_border_width >= 0 ) { ?>
    <?php echo $settings->input_field_border_position; ?>-width: <?php echo $settings->input_field_border_width; ?>px;
    <?php } ?>
    <?php echo ($settings->input_field_width == 'true') ? 'width: 100% !important;' : ''; ?>
    <?php if( $settings->input_field_padding['top'] != '' ) { ?> padding-top: <?php echo $settings->input_field_padding['top']; ?>px;<?php } ?>
    <?php if( $settings->input_field_padding['bottom'] != '' ) { ?>padding-bottom: <?php echo $settings->input_field_padding['bottom']; ?>px; <?php } ?>
    <?php if( $settings->input_field_padding['left'] != '' ) { ?>padding-left: <?php echo $settings->input_field_padding['left']; ?>px; <?php } ?>
    <?php if( $settings->input_field_padding['right'] != '' ) { ?>padding-right: <?php echo $settings->input_field_padding['right']; ?>px; <?php } ?>
    <?php if( $settings->input_field_text_alignment ) { ?>
    text-align: <?php echo $settings->input_field_text_alignment; ?>;
    <?php } ?>
    <?php if( $settings->input_font_family['family'] != 'Default' ) { ?>
    <?php FLBuilderFonts::font_css( $settings->input_font_family ); ?>
    <?php } ?>
    <?php if( $settings->input_font_size['desktop'] ) { ?> font-size: <?php echo $settings->input_font_size['desktop']; ?>px; <?php } ?>
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gform_title,
.fl-node-<?php echo $id; ?> .gravity-form-title {
    <?php if( $settings->title_color ) { ?>
    color: #<?php echo $settings->title_color; ?>;
    <?php } ?>
    display: <?php echo ($settings->title_field == 'false') ? 'none' : 'block'; ?>;
    <?php if( $settings->title_font_size['desktop'] ) { ?> font-size: <?php echo $settings->title_font_size['desktop']; ?>px; <?php } ?>
    <?php if( $settings->title_font_family['family'] != 'Default' ) { ?>
    <?php FLBuilderFonts::font_css( $settings->title_font_family ); ?>
    <?php } ?>
    <?php if( $settings->title_alignment ) { ?>
    text-align: <?php echo $settings->title_alignment; ?>;
    <?php } ?>
    <?php if( $settings->title_line_height['desktop'] != '' ) : ?>
    line-height: <?php echo $settings->title_line_height['desktop']; ?>px;
    <?php endif; ?>
}
.fl-node-<?php echo $id; ?> .gravity-form-title {
    display: <?php echo ($settings->form_custom_title_desc == 'yes') ? 'block' : 'none'; ?>;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gform_title {
    <?php if( $settings->form_custom_title_desc == 'yes' ) { ?>
    display: none;
    <?php } ?>
}
.fl-node-<?php echo $id; ?> .gform_wrapper span.gform_description,
.fl-node-<?php echo $id; ?> .gravity-form-description {
    <?php if( $settings->description_font_family['family'] != 'Default' ) { ?>
    <?php FLBuilderFonts::font_css( $settings->description_font_family ); ?>
    <?php } ?>
    <?php if( $settings->description_color ) { ?>
    color: #<?php echo $settings->description_color; ?>;
    <?php } ?>
    display: <?php echo ($settings->description_field == 'false') ? 'none' : 'block'; ?>;
    <?php if( $settings->description_font_size['desktop'] ) { ?> font-size: <?php echo $settings->description_font_size['desktop']; ?>px; <?php } ?>
    <?php if( $settings->description_alignment ) { ?>
    text-align: <?php echo $settings->description_alignment; ?>;
    <?php } ?>
     <?php if( $settings->description_line_height['desktop'] != '' ) : ?>
    line-height: <?php echo $settings->description_line_height['desktop']; ?>px;
    <?php endif; ?>
}
.fl-node-<?php echo $id; ?> .gravity-form-description {
    display: <?php echo ($settings->form_custom_title_desc == 'yes') ? 'block' : 'none'; ?>;
}
.fl-node-<?php echo $id; ?> .gform_wrapper span.gform_description {
    <?php if( $settings->form_custom_title_desc == 'yes' ) { ?>
    display: none;
    <?php } ?>
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield input:not([type='radio']):not([type='checkbox']):not([type='submit']):not([type='button']):not([type='image']):not([type='file']):focus,
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield select:focus,
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield textarea:focus {
    border-color: <?php echo $settings->input_field_focus_color ? '#' . $settings->input_field_focus_color : 'transparent'; ?>;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield .gfield_label {
    <?php if( $settings->form_label_color ) { ?>
	color: #<?php echo $settings->form_label_color; ?>;
    <?php } ?>
    <?php if( $settings->labels_toggle ) { ?>
    display: <?php echo $settings->labels_toggle; ?>;
    <?php } ?>
    <?php if( $settings->label_font_size['desktop'] ) { ?> font-size: <?php echo $settings->label_font_size['desktop']; ?>px; <?php } ?>
    <?php if( $settings->label_font_family['family'] != 'Default' ) { ?>
    <?php FLBuilderFonts::font_css( $settings->label_font_family ); ?>
    <?php } ?>
}
.fl-node-<?php echo $id; ?> .gform_wrapper .ginput_container label,
.fl-node-<?php echo $id; ?> .gform_wrapper table.gfield_list thead th,
.fl-node-<?php echo $id; ?> .gform_wrapper span.ginput_product_price_label,
.fl-node-<?php echo $id; ?> .gform_wrapper span.ginput_quantity_label,
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield_html {
    <?php if( $settings->form_label_color ) { ?>
    color: #<?php echo $settings->form_label_color; ?> !important;
    <?php } ?>
    <?php if( $settings->label_font_family['family'] != 'Default' ) { ?>
    <?php FLBuilderFonts::font_css( $settings->label_font_family ); ?>
    <?php } ?>
}
.fl-node-<?php echo $id; ?> .gform_wrapper span.ginput_product_price {
    <?php if( $settings->label_font_family['family'] != 'Default' ) { ?>
    <?php FLBuilderFonts::font_css( $settings->label_font_family ); ?>
    <?php } ?>
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield .gfield_description {
    <?php if( $settings->label_font_family['family'] != 'Default' ) { ?>
    <?php FLBuilderFonts::font_css( $settings->label_font_family ); ?>
    <?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-gravity-form-content {
    <?php if ( $settings->form_bg_type == 'color' ) { ?>
        <?php if( $settings->form_bg_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->form_bg_color )) ?>, <?php echo $settings->form_bg_color_opc/100; ?>); <?php } ?>
    <?php } elseif ( $settings->form_bg_type == 'image' ) { ?>
        background-image: url('<?php echo $settings->form_bg_img; ?>');
        background-position: <?php echo $settings->form_bg_img_pos; ?>;
        background-size: <?php echo $settings->form_bg_img_size; ?>;
        background-repeat: <?php echo $settings->form_bg_img_repeat; ?>;
        
    <?php } ?>
	
     <?php if( $settings->form_border_width >= 0 ) { ?>
    border-width: <?php echo $settings->form_border_width; ?>px;
    <?php } ?>
    <?php if( $settings->form_border_color ) { ?>
    border-color: #<?php echo $settings->form_border_color; ?>;
    <?php } ?>
    <?php if( $settings->form_border_style ) { ?>
    border-style: <?php echo $settings->form_border_style; ?>;
    <?php } ?>
    <?php if( $settings->form_border_radius >= 0 ) { ?>
    border-radius: <?php echo $settings->form_border_radius; ?>px;
    <?php } ?>
    <?php if( $settings->form_padding['top'] >= 0 ) { ?>
	padding-top: <?php echo $settings->form_padding['top']; ?>px;
	<?php } ?>
	<?php if( $settings->form_padding['right'] >= 0 ) { ?>
	padding-right: <?php echo $settings->form_padding['right']; ?>px;
	<?php } ?>
	<?php if( $settings->form_padding['bottom'] >= 0 ) { ?>
	padding-bottom: <?php echo $settings->form_padding['bottom']; ?>px;
	<?php } ?>
	<?php if( $settings->form_padding['left'] >= 0 ) { ?>
	padding-left: <?php echo $settings->form_padding['left']; ?>px;
	<?php } ?>
	
}
<?php if( $settings->gf_input_placeholder_color && $settings->gf_input_placeholder_display == 'block' ) { ?>
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield input::-webkit-input-placeholder {
    color: #<?php echo $settings->gf_input_placeholder_color; ?>;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield input:-moz-placeholder {
    color: #<?php echo $settings->gf_input_placeholder_color; ?>;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield input::-moz-placeholder {
    color: #<?php echo $settings->gf_input_placeholder_color; ?>;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield input:-ms-input-placeholder {
    color: #<?php echo $settings->gf_input_placeholder_color; ?>;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield textarea::-webkit-input-placeholder {
    color: #<?php echo $settings->gf_input_placeholder_color; ?>;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield textarea:-moz-placeholder {
    color: #<?php echo $settings->gf_input_placeholder_color; ?>;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield textarea::-moz-placeholder {
    color: #<?php echo $settings->gf_input_placeholder_color; ?>;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield textarea:-ms-input-placeholder {
    color: #<?php echo $settings->gf_input_placeholder_color; ?>;
}
<?php } ?>
<?php if( $settings->gf_input_placeholder_display == 'none' ) { ?>
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield input::-webkit-input-placeholder {
    color: transparent;
    opacity: 0;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield input:-moz-placeholder {
    color: transparent;
    opacity: 0;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield input::-moz-placeholder {
    color: transparent;
    opacity: 0;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield input:-ms-input-placeholder {
    color: transparent;
    opacity: 0;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield textarea::-webkit-input-placeholder {
    color: transparent;
    opacity: 0;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield textarea:-moz-placeholder {
    color: transparent;
    opacity: 0;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield textarea::-moz-placeholder {
    color: transparent;
    opacity: 0;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield textarea:-ms-input-placeholder {
    color: transparent;
    opacity: 0;
}
<?php } ?>
.fl-node-<?php echo $id; ?> .gform_wrapper .gform_footer .gform_button {
	 <?php if( $settings->button_text_color ) { ?>
		color: #<?php echo $settings->button_text_color; ?>;
    <?php } ?>
    width: <?php echo ($settings->button_width == 'true') ? '100%' : 'auto'; ?>;
    <?php if( $settings->button_bg_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->button_bg_color )) ?>, <?php echo $settings->button_background_opacity/100; ?>); <?php } ?>
    border: <?php echo $settings->button_border_width; ?>px solid <?php echo $settings->button_border_color ? '#' . $settings->button_border_color : 'transparent'; ?>;
    <?php if( $settings->button_padding['top'] != '' ) { ?> padding-top: <?php echo $settings->button_padding['top']; ?>px;<?php } ?>
	<?php if( $settings->button_padding['bottom'] != '' ) { ?>padding-bottom: <?php echo $settings->button_padding['bottom']; ?>px; <?php } ?>
	<?php if( $settings->button_padding['left'] != '' ) { ?>padding-left: <?php echo $settings->button_padding['left']; ?>px; <?php } ?>
	<?php if( $settings->button_padding['right'] != '' ) { ?>padding-right: <?php echo $settings->button_padding['right']; ?>px; <?php } ?>
	<?php if( $settings->button_border_radius >= 0 ) { ?>
    border-radius: <?php echo $settings->button_border_radius; ?>px;
    -moz-border-radius: <?php echo $settings->button_border_radius; ?>px;
    -webkit-border-radius: <?php echo $settings->button_border_radius; ?>px;
    -ms-border-radius: <?php echo $settings->button_border_radius; ?>px;
    -o-border-radius: <?php echo $settings->button_border_radius; ?>px;
    <?php } ?>
    <?php if( $settings->button_font_family['family'] != 'Default' ) { ?>
    <?php FLBuilderFonts::font_css( $settings->button_font_family ); ?>
    <?php } ?>
    <?php if( $settings->button_font_size['desktop'] ) { ?> font-size: <?php echo $settings->button_font_size['desktop']; ?>px; <?php } ?>   
    white-space: normal;
    text-transform: <?php echo $settings->button_text_transform; ?>;
	
}
.fl-node-<?php echo $id; ?> .gform_wrapper.gf_browser_ie .gform_footer .gform_button {
	<?php if( $settings->button_padding['top'] != '' ) { ?> padding-top: <?php echo $settings->button_padding['top']; ?>px;<?php } ?>
	<?php if( $settings->button_padding['bottom'] != '' ) { ?>padding-bottom: <?php echo $settings->button_padding['bottom']; ?>px; <?php } ?>
	<?php if( $settings->button_padding['left'] != '' ) { ?>padding-left: <?php echo $settings->button_padding['left']; ?>px; <?php } ?>
	<?php if( $settings->button_padding['right'] != '' ) { ?>padding-right: <?php echo $settings->button_padding['right']; ?>px; <?php } ?>
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gform_footer .gform_button:hover {
    <?php if( $settings->button_hover_text_color ) { ?>
	color: #<?php echo $settings->button_hover_text_color; ?>;
    <?php } ?>
	background: <?php echo $settings->button_hover_bg_color ? '#' . $settings->button_hover_bg_color : 'transparent'; ?>;
    <?php if( $settings->button_border_width >= 0 ) { ?>
	border-width: <?php echo $settings->button_border_width; ?>px;
    <?php } ?>
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gform_footer {
    <?php if( $settings->button_alignment ) { ?>
    text-align: <?php echo $settings->button_alignment; ?>;
    <?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-gravity-form-content .gform_wrapper ul li.gfield {
	list-style-type: none !important;
    <?php if( $settings->input_field_margin >= 0 ) { ?>
	margin-bottom: <?php echo $settings->input_field_margin; ?>px;
	
    <?php } ?>
}
.fl-node-<?php echo $id; ?> .gform_wrapper .validation_error,
.fl-node-<?php echo $id; ?> .gform_wrapper li.gfield.gfield_error,
.fl-node-<?php echo $id; ?> .gform_wrapper li.gfield.gfield_error.gfield_contains_required.gfield_creditcard_warning {
    <?php if( $settings->validation_error_color ) { ?>
    color: #<?php echo $settings->validation_error_color; ?> !important;
    <?php } ?>
    border-color: <?php echo $settings->validation_error_border_color ? '#' . $settings->validation_error_border_color : 'transparent'; ?> !important;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield.gfield_error {
    background-color: <?php echo ($settings->form_error_field_background_color) ? '#' . $settings->form_error_field_background_color : 'transparent'; ?>;
    Width: 100%;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield.gfield_error .gfield_label {
    <?php if( $settings->form_error_field_label_color ) { ?>
    color: #<?php echo $settings->form_error_field_label_color; ?>;
    <?php } ?>
    margin-left: 0;
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield_error input:not([type='radio']):not([type='checkbox']):not([type='submit']):not([type='button']):not([type='image']):not([type='file']),
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield_error .ginput_container select,
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield_error .ginput_container textarea {
    border-color: <?php echo ($settings->form_error_input_border_color) ? '#' . $settings->form_error_input_border_color : 'transparent'; ?>;
    <?php if( $settings->form_error_input_border_width >= 0 ) { ?>
    border-width: <?php echo $settings->form_error_input_border_width; ?>px !important;
    <?php } ?>
}
.fl-node-<?php echo $id; ?> .gform_wrapper .gfield_error .validation_message {
    <?php if( $settings->validation_message ) { ?>
    display: <?php echo $settings->validation_message; ?>;
    <?php } ?>
    <?php if( $settings->validation_message_color ) { ?>
    color: #<?php echo $settings->validation_message_color; ?>;
    <?php } ?>
}
<?php 
if( $settings->textarea_height != '' ) {?>
.fl-node-<?php echo $id; ?> .ginput_container textarea {
    min-height: <?php echo $settings->textarea_height; ?>px;
}
<?php } ?>
@media ( max-width: 991px ) {
	.fl-node-<?php echo $id; ?> .gform_wrapper .gfield .gfield_label {
		<?php if( $settings->label_font_size['medium'] ) { ?> font-size: <?php echo $settings->label_font_size['medium']; ?>px; <?php } ?>
	}
     .fl-node-<?php echo $id; ?> .gform_wrapper .gfield input:not([type='radio']):not([type='checkbox']):not([type='submit']):not([type='button']):not([type='image']):not([type='file']),
    .fl-node-<?php echo $id; ?> .gform_wrapper .gfield input:focus,
    .fl-node-<?php echo $id; ?> .gform_wrapper .gfield select,
    .fl-node-<?php echo $id; ?> .gform_wrapper .gfield textarea {
        <?php if( $settings->input_font_size['medium'] ) { ?> font-size: <?php echo $settings->input_font_size['medium']; ?>px; <?php } ?>   
    }
    .fl-node-<?php echo $id; ?> .gform_wrapper .gform_footer .gform_button {
        <?php if( $settings->button_font_size['medium'] ) { ?> font-size: <?php echo $settings->button_font_size['medium']; ?>px; <?php } ?>   
    }
    .fl-node-<?php echo $id; ?> .gform_wrapper span.gform_description,
    .fl-node-<?php echo $id; ?> .gravity-form-description {
        <?php if( $settings->description_font_size['medium'] ) { ?> font-size: <?php echo $settings->description_font_size['medium']; ?>px; <?php } ?>
        <?php if( $settings->description_line_height['medium'] != '' ) : ?>
                line-height: <?php echo $settings->description_line_height['medium']; ?>px;
        <?php endif; ?>
    }
    .fl-node-<?php echo $id; ?> .gform_wrapper .gform_title,
    .fl-node-<?php echo $id; ?> .gravity-form-title {
        
        <?php if( $settings->title_font_size['medium'] ) { ?> font-size: <?php echo $settings->title_font_size['medium']; ?>px; <?php } ?>
        <?php if( $settings->title_line_height['medium'] != '' ) : ?>
             line-height: <?php echo $settings->title_line_height['medium']; ?>px;
        <?php endif; ?>
    }
}
@media ( max-width: 767px ) {
	.fl-node-<?php echo $id; ?> .gform_wrapper .gfield .gfield_label {
		<?php if( $settings->label_font_size['small'] ) { ?> font-size: <?php echo $settings->label_font_size['small']; ?>px; <?php } ?>
	}
    .fl-node-<?php echo $id; ?> .gform_wrapper .gfield input:not([type='radio']):not([type='checkbox']):not([type='submit']):not([type='button']):not([type='image']):not([type='file']),
    .fl-node-<?php echo $id; ?> .gform_wrapper .gfield input:focus,
    .fl-node-<?php echo $id; ?> .gform_wrapper .gfield select,
    .fl-node-<?php echo $id; ?> .gform_wrapper .gfield textarea {
        <?php if( $settings->input_font_size['small'] ) { ?> font-size: <?php echo $settings->input_font_size['small']; ?>px; <?php } ?>	
    }
    .fl-node-<?php echo $id; ?> .gform_wrapper .gform_footer .gform_button {
        <?php if( $settings->button_font_size['small'] ) { ?> font-size: <?php echo $settings->button_font_size['small']; ?>px; <?php } ?>   
	}
    .fl-node-<?php echo $id; ?> .gform_wrapper span.gform_description,
    .fl-node-<?php echo $id; ?> .gravity-form-description {
 
        <?php if( $settings->description_font_size['small'] ) { ?> font-size: <?php echo $settings->description_font_size['small']; ?>px; <?php } ?>
        <?php if( $settings->description_line_height['small'] != '' ) : ?>
            line-height: <?php echo $settings->description_line_height['small']; ?>px;
        <?php endif; ?>
    }
    .fl-node-<?php echo $id; ?> .gform_wrapper .gform_title,
    .fl-node-<?php echo $id; ?> .gravity-form-title {
        <?php if( $settings->title_font_size['small'] ) { ?> font-size: <?php echo $settings->title_font_size['small']; ?>px; <?php } ?>
        <?php if( $settings->title_line_height['small'] != '' ) : ?>
             line-height: <?php echo $settings->title_line_height['small']; ?>px;
        <?php endif; ?>
    } 
}