<?php
/**
 * @class NJBAHoverBoxCaroselModule
 */
class NJBAHoverBoxCaroselModule extends FLBuilderModule {
    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Hover Box Carosel', 'bb-njba'),
            'description'   => __('Addon to display Hover Box Carosel.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'carousel' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-hover-box-carosel/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-hover-box-carosel/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'partial_refresh' => true, // Set this to true to enable partial refresh.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
         /**
         * Use these methods to enqueue css and js already
         * registered or to register and enqueue your own.
         */
        // Already registered
        $this->add_css('jquery-bxslider');
        $this->add_css('font-awesome');
        $this->add_js('jquery-bxslider');
    }
    /**
     * Use this method to work with settings data before
     * it is saved. You must return the settings object.
     *
     * @method update
     * @param $settings {object}
     */
    public function update($settings)
    {
       return $settings;
    }
    public function delete()
    {
    }
    public function njba_image_module($box_content){
           $html = '';
           $html = $box_content->box_image;
       return $html;
    }
    public function njba_image_hover_module($box_content){
            $html = '';
            $html = $box_content->hover_box_image;
        return $html;
    }
    public function njba_hover_box_sub_title_module($box_content){
            $html = '';
            $html = $box_content->hover_sub_title_text;
        return $html;
    }
    public function njba_hover_box_title_module($box_content){
            $html = '';
            $html = $box_content->hover_main_title_text;
        return $html;
    }
    public function njba_main_box_title_module($box_content){
            $html = '';
            $html = $box_content->main_title_text;
        return $html;
    }
    public function njba_button_module($box_content){
        $btn_settings = array(
            //Button text         
            'button_text'   => $box_content->more_link_text,
            //Button Link
            'link'      => $box_content->link,
            'link_target'       => $box_content->link_target,
        );
        return FLBuilder::render_module_html('njba-button', $btn_settings);
        
    }
    public function njba_separator_module($box_content){
            $html = '';
            $html = $box_content->separator_normal_width;
        return $html;
    }
    public function njba_icon_module($box_content){
            $html = '';
            $html .= $box_content->main_box_icon;
       return $html;
    }
   
}
/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('NJBAHoverBoxCaroselModule', array(
    'general'      => array( // Tab
        'title'         => __('General', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'heading'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array( // Section Fields
                    'box_layout_view'       => array(
                        'type'          => 'select',
                        'label'         => __('Layout', 'bb-njba'),
                        'default'       => 'box',
                        'options'       => array(
                            'grid'             => __('Grid', 'bb-njba'),
                            'carousel'          => __('Carousel', 'bb-njba')
                        ),
                        'toggle'        => array(
                            'carousel'      => array(
                                'sections'      => array( 'slider', 'carousel_section','arrow_nav','dot_nav' ),
                            ),
                            'grid'      => array(
                                'sections'      => array( 'box' ),
                            )
                        ),
                    ),
                )
            ),
            'box'       => array( // Section
                'title'         => __('Grid Settings', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'show_col'         => array(
                        'type'          => 'select',
                        'label'         => __('Show Columns', 'bb-njba'),
                        'default'       => 3,
                        'options'        => array(
                            '12'      => '1',
                            '6'       => '2',
                            '4'       => '3',
                            '3'       => '4',
                        ),
                    ),
                    
                )
            ),
            'slider'       => array( // Section
                'title'         => __('Carousel Settings', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'autoplay'         => array(
                        'type'          => 'select',
                        'label'         => __('Autoplay', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba')
                        ),
                    ),
                    'hover_pause'         => array(
                        'type'          => 'select',
                        'label'         => __('Pause on hover', 'bb-njba'),
                        'default'       => '1',
                        'help'          => __('Pause when mouse hovers over slider'),
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba'),
                        ),
                    ),
                    'transition'    => array(
                        'type'          => 'select',
                        'label'         => __('Mode', 'bb-njba'),
                        'default'       => 'horizontal',
                        'options'       => array(
                            'horizontal'    => _x( 'Horizontal', 'Transition type.', 'bb-njba' ),
                            'vertical'    => _x( 'Vertical', 'Transition type.', 'bb-njba' ),
                            'fade'          => __( 'Fade', 'bb-njba' )
                        ),
                    ),
                    'pause'         => array(
                        'type'          => 'text',
                        'label'         => __('Delay', 'bb-njba'),
                        'default'       => '4',
                        'maxlength'     => '4',
                        'size'          => '5',
                        'description'   => _x( 'seconds', 'Value unit for form field of time in seconds. Such as: "5 seconds"', 'bb-njba' )
                    ),
                    'speed'         => array(
                        'type'          => 'text',
                        'label'         => __('Transition Speed', 'bb-njba'),
                        'default'       => '0.5',
                        'maxlength'     => '4',
                        'size'          => '5',
                        'description'   => _x( 'seconds', 'Value unit for form field of time in seconds. Such as: "5 seconds"', 'bb-njba' )
                    ),
                    'loop'         => array(
                        'type'          => 'select',
                        'label'         => __('Loop', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba'),
                        ),
                    ),
                    'adaptive_height'   => array(
                        'type'              => 'select',
                        'label'             => __('Fixed Height', 'bb-njba'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'bb-njba'),
                            'no'                => __('No', 'bb-njba')
                        ),
                        'help'              => __('Fix height to the tallest item.', 'bb-njba')
                    )
                )
            ),
            'carousel_section'       => array( // Section
                'title'         => '',
                'fields'        => array( // Section Fields
                    'max_slides'         => array(
                        'type'          => 'njba-simplify',
                        'label'         => __('Maximum Slides'),
                        'default'       => array(
                                    'desktop' => '3',
                                    'medium'  => '2',
                                    'small'   => '1',
                        ),
                        'size'          => '5', 
                    ),
                     'slide_margin'         => array(
                        'type'          => 'njba-simplify',
                        'label'         => __('Slides Margin ', 'bb-njba'),
                        'default'       => array(
                                    'desktop' => '0',
                                    'medium'  => '0',
                                    'small'   => '0',
                        ),
                        'size'          => '5', 
                    ),
                   
                )
            ),
            'arrow_nav'       => array( // Section
                'title'         => '',
                'fields'        => array( // Section Fields
                    'arrows'       => array(
                        'type'          => 'select',
                        'label'         => __('Show Arrows', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba')
                        ),
                        'toggle'        => array(
                            '1'         => array(
                                'fields'        => array('arrows_size', 'arrow_background', 'arrow_color','arrow_border_width','arrow_border_style','arrow_border_color','arrow_border_color','arrow_border_radius')
                            )
                        )
                    ),
                    'arrows_size'         => array(
                        'type'          => 'text',
                        'label'         => __('Arrows Size', 'bb-njba'),
                        'default'       => '20',
                        'maxlength'     => '3',
                        'size'          => '5',
                        'description'   => 'px',
                        'help'          => __('Arrow Size.', 'bb-njba'),
                    ),
                    'arrow_background'       => array(
                        'type'          => 'color',
                        'label'         => __('Arrow Background', 'bb-njba'),
                        'default'       => 'dddddd',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-hover-box-carosel-main .njba-slider-nav a i',
                            'property'      => 'background'
                        )
                    ),
                    'arrow_color'       => array(
                        'type'          => 'color',
                        'label'         => __('Arrow Color', 'bb-njba'),
                        'default'       => '000000',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-hover-box-carosel-main .njba-slider-nav a i',
                            'property'      => 'color'
                        )
                    ),
                    'arrow_border_radius'    => array(
                        'type'          => 'text',
                        'default'       => '0',
                        'maxlength'     => '3',
                        'size'          => '5',
                        'label'         => __('Arrow Round Corners', 'bb-njba'),
                        'description'   => _x( 'px', 'Value unit for border radius. Such as: "5 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-hover-box-carosel-main .njba-slider-nav a i',
                            'property'      => 'border-radius'
                        )
                    ),
                )
            ),
            'dot_nav'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array( // Section Fields
                    'dots'       => array(
                        'type'          => 'select',
                        'label'         => __('Show Dots', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba'),
                        ),
                        'toggle'        => array(
                            '1'         => array(
                                'fields'        => array('dot_color', 'active_dot_color')
                            )
                        )
                    ),
                    'dot_color'       => array(
                        'type'          => 'color',
                        'label'         => __('Dot Color', 'bb-njba'),
                        'default'       => '999999',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-testimonials-wrap .bx-wranjbaer .bx-pager a',
                            'property'      => 'background'
                        )
                    ),
                    'active_dot_color'       => array(
                        'type'          => 'color',
                        'label'         => __('Active Dot Color', 'bb-njba'),
                        'default'       => '999999',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-testimonials-wrap .bx-wranjbaer .bx-pager a.active',
                            'property'      => 'background'
                        )
                    ),
                )
            )
        )
    ),
    'price_box'      => array( // Tab
        'title'         => __('Kit Boxs', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array( // Section Fields
                    'box_content'     => array(
                        'type'          => 'form',
                        'label'         => __('Box', 'bb-njba'),
                        'form'          => 'njba_boxcarosel_form', // ID from registered form below
                        'preview_text'  => 'main_title_text', // Name of a field to use for the preview text
                        'multiple'      => true
                    )
                )
            )
        )
    ),
    'simple_box'       => array(
        'title'     => __('Style', 'bb-njba'),
        'sections'  => array(
            'main_box_title'       => array( // Section
                'title'         => __('Kit Box Title', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'main_title_alignment'         => array(
                        'type'                      => 'select',
                        'default'                   => 'center',
                        'label'                     => __('Alignment', 'bb-njba'),
                        'options'                   => array(
                            'left'                      => __('Left', 'bb-njba'),
                            'right'                     => __('Right', 'bb-njba'),
                            'center'                    => __('Center', 'bb-njba'),
                        ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.freature-title',
                            'property'      => 'text-align'
                        )
                    ),
                    'main_title_font'          => array(
                        'type'          => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.freature-title'
                        )
                    ),
                    'main_title_font_size'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Font Size', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '28',
                            'medium'        => '24',
                            'small'         => '20',
                        ),
                        'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.freature-title',
                            'property'      => 'font-size',
                            'unit'          => 'px'
                        )
                    ),
                    'main_title_line_height'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Line Height', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '30',
                            'medium'        => '26',
                            'small'         => '22',
                        ),
                        'description'   => _x( 'px', 'Value unit for line height. Such as: "14 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.freature-title',
                            'property'      => 'line-height',
                            'unit'          => 'px'
                        )
                    ),
                    'main_title_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Color', 'bb-njba'),
                        'default'       => '000000',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.freature-title',
                            'property'      => 'color',
                        )
                    ),
                    'main_title_margin'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 10,
                            'right'      => 10,
                            'bottom'      => 10,
                            'left'      => 10
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up',
                                'preview'           => array(
                                    'selector'          => '.freature-title',
                                    'property'          => 'margin-top',
                                ),
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right',
                                'preview'           => array(
                                    'selector'          => '.freature-title',
                                    'property'          => 'margin-right',
                                ),
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down',
                                'preview'           => array(
                                    'selector'          => '.freature-title',
                                    'property'          => 'margin-bottom',
                                ),
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left',
                                'preview'           => array(
                                    'selector'          => '.freature-title',
                                    'property'          => 'margin-left',
                                ),
                            )
                        )
                    ),
                )
            ),
            'main_box_icon'       => array( // Section
                'title'         => __('Kit Box Icon', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'main_icon_size'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Font Size', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '60',
                            'medium'        => '50',
                            'small'         => '40',
                        ),
                        'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-infobox-two',
                            'property'      => 'font-size',
                            'unit'          => 'px'
                        )
                    ),
                    'main_icon_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Color', 'bb-njba'),
                        'default'       => '000000',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-infobox-two',
                            'property'      => 'color'
                        )
                    ),
                )
            ),
            'hover_box_title'       => array( // Section
                'title'         => __('Hover Kit Box Title', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'hover_title_alignment'         => array(
                        'type'                      => 'select',
                        'default'                   => 'center',
                        'label'                     => __('Alignment', 'bb-njba'),
                        'options'                   => array(
                            'left'                      => __('Left', 'bb-njba'),
                            'right'                     => __('Right', 'bb-njba'),
                            'center'                    => __('Center', 'bb-njba'),
                        ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-heading-title',
                            'property'      => 'text-align'
                        )
                    ),
                    'hover_title_font'          => array(
                        'type'          => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.njba-heading-title'
                        )
                    ),
                    'hover_title_font_size'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Font Size', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '28',
                            'medium'        => '24',
                            'small'         => '20',
                        ),
                        'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-heading-title',
                            'property'      => 'font-size',
                            'unit'          => 'px'
                        )
                    ),
                    'hover_title_line_height'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Line Height', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '30',
                            'medium'        => '26',
                            'small'         => '22',
                        ),
                        'description'   => _x( 'px', 'Value unit for line height. Such as: "14 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-heading-title',
                            'property'      => 'line-height',
                            'unit'          => 'px'
                        )
                    ),
                    'hover_title_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Color', 'bb-njba'),
                        'default'       => '000000',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-heading-title',
                            'property'      => 'color',
                        )
                    ),
                    'hover_title_margin'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 10,
                            'right'      => 10,
                            'bottom'      => 10,
                            'left'      => 10
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up',
                                'preview'           => array(
                                    'selector'          => '.njba-heading-title',
                                    'property'          => 'margin-top',
                                ),
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right',
                                'preview'           => array(
                                    'selector'          => '.njba-heading-title',
                                    'property'          => 'margin-right',
                                ),
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down',
                                'preview'           => array(
                                    'selector'          => '.njba-heading-title',
                                    'property'          => 'margin-bottom',
                                ),
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left',
                                'preview'           => array(
                                    'selector'          => '.njba-heading-title',
                                    'property'          => 'margin-left',
                                ),
                            )
                        )
                    ),
                )
            ),
            'hover_box_subtitle'       => array( // Section
                'title'         => __('Hover Kit Box Sub Title', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'hover_subtitle_alignment'         => array(
                        'type'                      => 'select',
                        'default'                   => 'center',
                        'label'                     => __('Alignment', 'bb-njba'),
                        'options'                   => array(
                            'left'                      => __('Left', 'bb-njba'),
                            'right'                     => __('Right', 'bb-njba'),
                            'center'                    => __('Center', 'bb-njba'),
                        ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-heading-title',
                            'property'      => 'text-align'
                        )
                    ),
                    'hover_subtitle_font'          => array(
                        'type'          => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.njba-heading-title'
                        )
                    ),
                    'hover_subtitle_font_size'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Font Size', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '28',
                            'medium'        => '24',
                            'small'         => '20',
                        ),
                        'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-heading-title',
                            'property'      => 'font-size',
                            'unit'          => 'px'
                        )
                    ),
                    'hover_subtitle_line_height'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Line Height', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '30',
                            'medium'        => '26',
                            'small'         => '22',
                        ),
                        'description'   => _x( 'px', 'Value unit for line height. Such as: "14 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-heading-title',
                            'property'      => 'line-height',
                            'unit'          => 'px'
                        )
                    ),
                    'hover_subtitle_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Color', 'bb-njba'),
                        'default'       => '000000',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-heading-title',
                            'property'      => 'color',
                        )
                    ),
                    'hover_subtitle_margin'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 10,
                            'right'      => 10,
                            'bottom'      => 10,
                            'left'      => 10
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up',
                                'preview'           => array(
                                    'selector'          => '.njba-heading-title',
                                    'property'          => 'margin-top',
                                ),
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right',
                                'preview'           => array(
                                    'selector'          => '.njba-heading-title',
                                    'property'          => 'margin-right',
                                ),
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down',
                                'preview'           => array(
                                    'selector'          => '.njba-heading-title',
                                    'property'          => 'margin-bottom',
                                ),
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left',
                                'preview'           => array(
                                    'selector'          => '.njba-heading-title',
                                    'property'          => 'margin-left',
                                ),
                            )
                        )
                    ),
                )
            ),
            'hover_box_separator'       => array( // Section
                'title'         => __('Hover Kit Box Separator', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'separator_margintb'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 20,
                            'bottom'      => 20
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up',
                                'preview'           => array(
                                    'selector'          => '.njba-heading-icon',
                                    'property'          => 'margin-top',
                                ),
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down',
                                'preview'           => array(
                                    'selector'          => '.njba-heading-icon',
                                    'property'          => 'margin-bottom',
                                ),
                            )
                        )
                    ),
                    'separator_border_width'    => array(
                        'type'          => 'text',
                        'default'       => '1',
                        'maxlength'     => '2',
                        'size'          => '5',
                        'label'         => __('Border Width', 'bb-njba'),
                        'description'   => 'px',
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-heading-separator-line',
                            'property'      => 'border-top',
                            'unit'          => 'px'
                        )
                    ),
                    'separator_border_style'      => array(
                        'type'      => 'select',
                        'label'     => __('Border Style', 'bb-njba'),
                        'default'   => 'none',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'double'  => __('Double', 'bb-njba'),
                        ),
                    ),
                    'separator_border_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Border Color', 'bb-njba'),
                        'default'       => '000000',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-heading-separator-line',
                            'property'      => 'border-color',
                        )
                    )
                )
            ),
            'hover_box_button'       => array( // Section
                'title'         => __('Hover Kit Box Button', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'button_background_color' => array(
                        'type' => 'color',
                        'label' => __('Background Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'dfdfdf'
                    ),
                    'button_background_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Background Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'button_text_color' => array(
                        'type' => 'color',
                        'label' => __('Text Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '404040'
                    ),
                    'button_text_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Text Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff'
                    ),
                    'button_border_style'      => array(
                        'type'      => 'select',
                        'label'     => __('Border Style', 'bb-njba'),
                        'default'   => 'none',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'double'  => __('Double', 'bb-njba'),
                        ),
                        'toggle' => array(
                            'solid' => array(
                                'fields' => array('button_border_width','button_border_color','button_border_hover_color')
                            ),
                            'dotted' => array(
                                'fields' => array('button_border_width','button_border_color','button_border_hover_color')
                            ),
                            'dashed' => array(
                                'fields' => array('button_border_width','button_border_color','button_border_hover_color')
                            ),
                            'double' => array(
                                'fields' => array('button_border_width','button_border_color','button_border_hover_color')
                            ),
                        )
                    ),
                    'button_border_width' => array(
                        'type' => 'text',
                        'label' => __('Border Width','bb-njba'),
                        'default' => '1',
                        'size' => '5',
                        'description'       => _x( 'px', 'Value unit for spacer width. Such as: "10 px"', 'bb-njba' )
                    ),
                    'button_border_radius'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Border Radius', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top-left'          => 0,
                            'top-right'         => 0,
                            'bottom-left'       => 0,
                            'bottom-right'      => 0
                        ),
                        'options'           => array(
                            'top-left'               => array(
                                'placeholder'       => __('Top Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'top-right'            => array(
                                'placeholder'       => __('Top Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom-left'            => array(
                                'placeholder'       => __('Bottom Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'bottom-right'            => array(
                                'placeholder'       => __('Bottom Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'button_border_color' => array(
                        'type' => 'color',
                        'label' => __('Border Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'button_border_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Border Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'button_box_shadow'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Box Shadow', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'left_right'          => 0,
                            'top_bottom'         => 0,
                            'blur'       => 0,
                            'spread'      => 0
                        ),
                        'options'           => array(
                            'left_right'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa fa-arrows-h'
                            ),
                            'top_bottom'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa fa-arrows-v'
                            ),
                            'blur'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa fa-circle-thin'
                            ),
                            'spread'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa fa-circle'
                            )
                            
                        )
                    ),
                    'button_box_shadow_color' => array(
                        'type' => 'color',
                        'label' => __('Box Shadow Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff'
                    ),
                    'button_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 20,
                            'right'         => 40,
                            'bottom'       => 20,
                            'left'      => 40
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'button_margin'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Button Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 0,
                            'right'         => 0,
                            'bottom'       => 0,
                            'left'      => 0
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'width' => array(
                        'type' => 'select',
                        'label' => __('Width','bb-njba'),
                        'default' => 'auto',
                        'options' => array(
                            'auto' => __('Auto','bb-njba'),
                            'full_width' => __('Full Width','bb-njba'),
                            'custom' => __('Custom','bb-njba')
                        ),
                        'toggle' => array(
                            'auto' => array(
                                'fields' => array('alignment')
                            ),
                            'full_width' => array(
                                'fields' => array('')
                            ),
                            'custom' => array(
                                'fields' => array('custom_width','custom_height','alignment')
                            )
                        )
                    ),
                    'custom_width' => array(
                        'type' => 'text',
                        'label' => __('Custom Width','bb-njba'),
                        'default' => 200,
                        'size' => 10
                    ),
                    'custom_height' => array(
                        'type' => 'text',
                        'label' => __('Custom Height','bb-njba'),
                        'default' => 45,
                        'size' => 10
                    ),
                    'alignment' => array(
                        'type' => 'select',
                        'label' => __('Alignment','bb-njba'),
                        'default' => 'left',
                        'options' => array(
                            'left' => __('Left','bb-njba'),
                            'center' => __('Center','bb-njba'),
                            'right' => __('Right','bb-njba')
                        )   
                    ),
                    'button_font_family' => array(
                        'type' => 'font',
                        'label' => __('Font Family','bb-njba'),
                        'default' => array(
                            'family' => 'Default',
                            'weight' => 'Default'
                        ),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.njba-btn-main a.njba-btn'
                        )
                    ),
                    'button_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '18',
                            'medium' => '16',
                            'small' => ''
                        )
                    )
                )
            ),
        ),
    ),
));
/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('njba_boxcarosel_form', array(
    'title' => __('Add Hover Box', 'bb-njba'),
    'tabs'  => array(
        'general_box'      => array(
            'title'         => __('General Box', 'bb-njba'),
            'sections'      => array(
                'main_image'       => array(
                    'title'         => __('Box Image', 'bb-njba'),
                    'fields'        => array(
                        'box_image'         => array(
                        'type'          => 'photo',
                        'label'         => __('Background Image', 'bb-njba'),
                        'show_remove'   => true
                        ),
                    )
                ),
                'main_title'       => array(
                    'title'         => __('Box Title', 'bb-njba'),
                    'fields'        => array(
                        'main_title_text'         => array(
                            'type'            => 'text',
                            'label'           => __('Main Title', 'bb-njba'),
                            'default'         => 'Main Title',
                            'preview'         => array(
                                'type'            => 'text',
                                'selector'        => '.freature-title'
                            )
                        )
                    )
                ),
                'main_icon'       => array(
                    'title'         => __('Box icon', 'bb-njba'),
                    'fields'        => array(
                        'photo'         => array(
                            'type'          => 'photo',
                            'label'         => __('Icon Image', 'bb-njba'),
                            'show_remove'   => true
                        ),
                    )
                )
            )
        ),
        'hover_box'     => array(
            'title'         => __('Hover Box', 'bb-njba'),
            'sections'      => array(
                'hover_main_title'       => array(
                    'title'         => __('Box Hover Main Title', 'bb-njba'),
                    'fields'        => array(
                        'hover_main_title_text'         => array(
                            'type'            => 'text',
                            'label'           => __('Hover Main Title Text', 'bb-njba'),
                            'default'         => 'Hover Main Title',
                            'preview'         => array(
                                'type'            => 'text',
                                'selector'        => '.hover-freature-title'
                            )
                        )
                    )
                ),
                'hover_sub_title'       => array(
                    'title'         => __('Kit Hover sub Title', 'bb-njba'),
                    'fields'        => array(
                        'hover_sub_title_text'         => array(
                            'type'            => 'text',
                            'label'           => __('Hover Sub Title Text', 'bb-njba'),
                            'default'         => 'Hover Main Title',
                            'preview'         => array(
                                'type'            => 'text',
                                'selector'        => '.hover-freature-discription'
                            )
                        )
                    )
                ),
                'hover_separator'       => array(
                    'title'         => __('Kit Hover sub Title', 'bb-njba'),
                    'fields'        => array(
                        'separator_normal_width'          => array(
                            'type'          => 'text',
                            'size'          => '5',
                            'maxlength'     => '3',
                            'default'       => '50',
                            'label'         => __('Separator Width', 'bb-njba'),
                            'description'   => _x( '%', 'Value unit for Separator Width. Such as: "50%"', 'bb-njba' ),
                            'preview'       => array(
                                'type'          => 'css',
                                'selector'      => '.njba-heading-icon',
                                'property'      => 'width',
                                'unit'          => '%'
                            )
                        )
                    )
                ),
                'hover_button'       => array(
                    'title'         => __('Box Hover sub Title', 'bb-njba'),
                    'fields'        => array(
                        'more_link_text' => array(
                        'type'          => 'text',
                        'label'         => __('More Link Text', 'bb-njba'),
                        'default'       => __('More', 'bb-njba'),
                        ),
                        'link'     => array(
                            'type'          => 'link',
                            'label'         =>  __('Link', 'bb-njba'),
                            'default'       =>  __('#', 'bb-njba'),
                            'placeholder'   => 'www.example.com',
                            'preview'       => array(
                                'type'          => 'none'
                            )
                        ),
                        'link_target'   => array(
                            'type'          => 'select',
                            'label'         =>  __('Link Target', 'bb-njba'),
                            'default'       =>  __('_self', 'bb-njba'),
                            'placeholder'   => 'www.example.com',
                            'options'   => array(
                                '_self'     =>  __('Same Window', 'bb-njba'),   
                                '_blank'    =>  __('New Window', 'bb-njba'),   
                            ),
                            'preview'   => array(
                                'type'      => 'none'
                            )
                        )
                    )
                )
            )
        )
    )
));