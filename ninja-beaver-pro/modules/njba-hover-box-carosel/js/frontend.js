(function($) {
	NJBAContentGrid = function(settings)
	{
		
		this.settings       = settings;
		this.nodeClass      = '.fl-node-' + settings.id;
		this.wrapperClass   = this.nodeClass + ' .njba-blog-posts-wrapper';
		this.postClass      = this.wrapperClass + ' .njba-post-wrapper';
		this.masonry		= settings.masonry == 'yes' ? true : false;
		if(this._hasPosts()) {
			this._initLayout();
			this._initInfiniteScroll();
		}
	};
	NJBAContentGrid.prototype = {
		settings        : {},
		nodeClass       : '',
		wrapperClass    : '',
		postClass       : '',
		gallery         : null,
		_hasPosts: function()
		{
			return $(this.postClass).length > 0;
		},
		_initLayout: function()
		{
			switch(this.settings.layout) {
				case 'grid':
				this._gridLayout();
				break;
			}
			$(this.postClass).css('visibility', 'visible');
		},
		_gridLayout: function()
		{
			var wrap = $(this.wrapperClass);
			var postFilterData = {
				itemSelector: '.njba-content-post',
				percentPosition: true,
				transitionDuration: '0.4s',
			};
			wrap.imagesLoaded( $.proxy( function() {
				var node = $(this.nodeClass);
                var base = this;
			}, this ) );
		},
		
		/*_gridLayoutMatchHeight: function()
		{
			
		},
*/
		_initInfiniteScroll: function()
		{
			if(this.settings.pagination == 'scroll' && typeof FLBuilder === 'undefined') {
				
				this._infiniteScroll();
			}
		},
		_infiniteScroll: function(settings)
		{
			var path 		= $(this.nodeClass + ' .njba-pagination a.next').attr('href'),
				pagePattern = /(.*?(\/|\&|\?)paged-[0-9]{1,}(\/|=))([0-9]{1,})+(.*)/,
				pageMatched = null,
				scrollData	= {
					navSelector     : this.nodeClass + ' .njba-pagination',
					nextSelector    : this.nodeClass + ' .njba-pagination a.next',
					itemSelector    : this.postClass,
					prefill         : true,
					bufferPx        : 200,
					loading         : {
						msgText         : 'Loading',
						finishedMsg     : '',
						img             : FLBuilderLayoutConfig.paths.pluginUrl + 'img/ajax-loader-grey.gif',
						speed           : 1
					}
				};
			// Define path since Infinitescroll incremented our custom pagination '/paged-2/2/' to '/paged-3/2/'.
			if ( pagePattern.test( path ) ) {
				scrollData.path = function( currPage ){
					pageMatched = path.match( pagePattern );
					path = pageMatched[1] + currPage + pageMatched[5];
					return path;
				}
			}
			$(this.wrapperClass).infinitescroll( scrollData, $.proxy(this._infiniteScrollComplete, this) );
			setTimeout(function(){
				$(window).trigger('resize');
			}, 100);
		},
		_infiniteScrollComplete: function(elements)
		{
			var wrap = $(this.wrapperClass);
			elements = $(elements);
			
			if(this.settings.layout == 'grid') {
				
				wrap.imagesLoaded( $.proxy( function() {
					elements.css('visibility', 'visible');
					wrap.find('.njba-grid-space').remove();
					wrap.append('<div class="njba-grid-space"></div>');
				}, this ) );
			}
			else if(this.settings.layout == 'masonry') {
				
				wrap.imagesLoaded( $.proxy( function() {
					//wrap.isotope('insert', elements);
					elements.css('visibility', 'visible');
					wrap.find('.njba-grid-space').remove();
					wrap.append('<div class="njba-grid-space"></div>');
				}, this ) );
			}
		}
	};
})(jQuery);
var NJBAContentFlipBox;
(function($) {
    
    NJBAContentFlipBox = function( settings ){
        this.id                             = settings.id;
        this.nodeClass                      = '.fl-node-' + settings.id;
        this._init();   
    };
    NJBAContentFlipBox.prototype = {
        nodeClass 						: this.nodeClass,
        id 								: this.id,
        _init: function() {
            
        	var id = this.id;
            
			if( !( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) ) {
				$('.fl-node-' + id + ' .njba-flip-box-outter').hover(function(event){
					event.stopPropagation();
					$(this).addClass('njba-hover');
				}, function(event) {
					event.stopPropagation();
					$(this).removeClass('njba-hover');
				});
			}			
            this._njbaFlipBoxAdjustHeight(); 
			
			setTimeout(function() {
				$('.njba-face').css('opacity', '1');
			}, 500);
        },
        _njbaFlipBoxAdjustHeight: function() {
        	var currentFlipBox = $( this.nodeClass ),
        		backFlipSection = currentFlipBox.find( '.njba-back .njba-flip-box-section' ),
        		frontFlipSection = currentFlipBox.find( '.njba-front .njba-flip-box-section' ),
                frontHeight = 0,
                backHeight = 0;
            setTimeout(function() {
                currentFlipBox.find( '.njba-face' ).css( 'height', '100%' );
                currentFlipBox.find( '.njba-flip-box-outter' ).css( 'height', '100%' );
                currentFlipBox.find( '.njba-flip-box-outter' ).parent().css( 'height', '100%' );
                frontHeight = parseInt( frontFlipSection.outerHeight() ),
                backHeight = parseInt( backFlipSection.outerHeight() );
				if( ( backHeight >= frontHeight ) ) {
					currentFlipBox.find(".njba-face").css('height', backHeight );
				} else {
					currentFlipBox.find(".njba-face").css('height', frontHeight );
				}
            }, 200);
        },
    };
        
})(jQuery);