<div class="njba-hover-box-carosel-main njba-hover-box-carosel-layout">
	<div class="njba-hover-box-carosel-body">
		<div class="njba-hover-box-carosel-wrapper">
		<?php
			$total_box_content = count($settings->box_content);
		?>
		<?php
    	for($i=0; $i < $total_box_content; $i++) :
			$box_content = $settings->box_content[$i];
			if($settings->box_layout_view === "grid")
			{
		?>
				<div class="njba-column-<?php echo $settings->show_col;?>">
		<?php 
			}
			else if($settings->box_layout_view === "carousel")
			{
		?>
				<div class="njba-carousel njba-slide-<?php echo $i;?>">
		<?php 
			}
		?>
				    <div class="freature-repair">
						<img src="<?php echo $box_content->box_image_src;?>">
					    <div class="freature-img-overlay"></div>
						<div class="freature-img">
					        <div class="freature-title"><?php echo $module->njba_main_box_title_module($box_content); ?></div>
					        <div class="freature-plus-icon"><span><img src="<?php echo $box_content->photo_src; ?>"></span></div>
					    </div>
					    <div class="freature-hover-box">
					    	<div class="freature-hover-table">
					            <div class="freature-hover-table-cell">
					                <div class="hover-freature-title"><?php echo $module->njba_hover_box_title_module($box_content); ?></div>
					                <hr>
					                <div class="hover-freature-discription"><?php echo $module->njba_hover_box_sub_title_module($box_content); ?></div>
					                <div class="freature-info"><?php echo $module->njba_button_module($box_content); ?></div>
					            </div>
					        </div>
					    </div>
					</div><!--njba-team-section-->
				</div>
		<?php 
			endfor;
		?>
		</div>
	</div>
</div>