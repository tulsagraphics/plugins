<?php
/**
 * @class NJBAAudioModule
 */
class NJBAAudioModule extends FLBuilderModule {
    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Audio', 'bb-njba'),
            'description'   => __('Addon to Listen Music.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'creative' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-audio/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-audio/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
            'partial_refresh' => true, // Set this to true to enable partial refresh.
            'icon'              => 'format-audio.svg',
        ));
       
    }
    
}
FLBuilder::register_module('NJBAAudioModule', array(
    'general'       => array(
        'title'         => __('General', 'bb-njba'),
        'sections'      => array(
            'general'       => array(
                'title'         => 'Audio Setup',
                'fields'        => array(
                    'audio_style'       => array(
                        'type'          => 'select',
                        'label'         => __('Style', 'bb-njba'),
                        'default'       => 'wordpress',
                        'options'       => array(
                            'style-1'          => __('Style 1', 'bb-njba'),
                            'style-2'          => __('Style 2', 'bb-njba'),
                            'style-3'          => __('Style 3', 'bb-njba'),
                            
                        ),
                        'toggle'        =>array(
                            'style-2'           =>array(
                                    'fields'    =>  array('photo'),
                            ),
                            'style-3'           =>array(
                                    'fields'    =>  array('photo'),
                            ),
                        ),
                        
                    ),
                    'audio_name'     => array(
                            'type'          => 'text',
                            'label'         => __('Audio Name', 'bb-njba'),
                            'placeholder'       => '',
                            'preview'      => array(
                                'type'         => 'text',
                                'selector'     => '.njba-audio-name h1',
                            )
                    ),
                    'artists_name'     => array(
                            'type'          => 'text',
                            'label'         => __('Artist Name', 'bb-njba'),
                            'placeholder'       => '',
                            'preview'      => array(
                                'type'         => 'text',
                                'selector'     => '.njba-audio-name p',
                            )
                    ),
                    'photo'         => array(
                        'type'          => 'photo',
                        'label'         => __('Photo', 'bb-njba'),
                        'show_remove'   => true,
                    ),
                    
                    'audios'          => array(
                        'type'          => 'multiple-audios',
                        'label'         => __( 'Select Audio', 'bb-njba' ),
                        'show_remove'   => true,
                        'toggle'        => array(
                            'single_audio'     => array(
                                'fields'      => array('autoplay', 'loop')
                            ),
                        )
                    ),
                    
                    /**
                     * Single audio options
                     */
                ),
            ),
            'audio_setings'       => array(
                'title'         => 'Settings',
                'fields'        => array(
                    'autoplay'       => array(
                        'type'          => 'select',
                        'label'         => __('Auto Play', 'bb-njba'),
                        'default'       => '0',
                        'options'       => array(
                            '0'             => __('No', 'bb-njba'),
                            '1'             => __('Yes', 'bb-njba')
                        ),
                        'preview'       => array(
                            'type'          => 'none'
                        )
                    ),
                    'loop'           => array(
                        'type'          => 'select',
                        'label'         => __('Loop', 'bb-njba'),
                        'default'       => '0',
                        'options'       => array(
                            '0'             => __('No', 'bb-njba'),
                            '1'             => __('Yes', 'bb-njba')
                        ),
                        'preview'       => array(
                            'type'          => 'none'
                        )
                    ),
                    'track_show'      => array(
                        'type'          => 'select',
                        'label'         => __('Track Show', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '0'             => __('No', 'bb-njba'),
                            '1'             => __('Yes', 'bb-njba')
                        ),
                        'preview'       => array(
                            'type'          => 'css',
                            'property'      => 'display',
                        )
                    ),
                    'volume_show'      => array(
                        'type'          => 'select',
                        'label'         => __('Volume Show', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '0'             => __('No', 'bb-njba'),
                            '1'             => __('Yes', 'bb-njba')
                        ),
                        'preview'       => array(
                            'type'          => 'css',
                            'property'      => 'display',
                        )
                    ),
                    
                    'artists'       => array(
                        'type'          => 'select',
                        'label'         => __('Show Artist Name', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '0'             => __('No', 'bb-njba'),
                            '1'             => __('Yes', 'bb-njba')
                        )
                    ),
                )
            )
        )
    ),
    'styles'                => array(
        'title'                     => __('Styles', 'bb-njba'),
        'sections'                  => array(
            'title_fonts'       => array(
                'title'             => __('Player Settings', 'bb-njba'),
                'fields'            => array(
                    'skin_bg_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Skin Color', 'bb-njba'),
                        'show_reset'    => true,
                        'default'       => 'ffffff',   
                    ),
                    'track_alignment' => array(
                        'type' => 'select',
                        'label' => __('Track Alignment','bb-njba'),
                        'default' => 'center',
                        'options' => array(
                            'left' => __('Left','bb-njba'),
                            'center' => __('Center','bb-njba'),
                            'right' => __('Right','bb-njba')
                        ),
                    ),
                    'track_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Track Color', 'bb-njba'),
                        'show_reset'    => true,
                        'default'       => 'ffffff', 
                    ),
                    'play_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
                    ),
                    'play_txt_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Player Text Color', 'bb-njba'),
                        'show_reset'    => true,
                        'default'       => 'ffffff',            
                    ),
                    'track_margin'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Track Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 5,
                            'bottom'       => 5,
                            
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                        ),
                    ),
                )
            )
        )
    ),
     'typography'                => array(
        'title'                     => __('Typography', 'bb-njba'),
        'sections'                  => array(
            'audio_name'       => array( // Section
                'title'         =>  __('Audio Name', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'name_tag'   => array(
                        'type'          => 'select',
                        'label'         => __('Tag', 'bb-njba'),
                        'default'       => 'h1',
                        'options'       => array(
                            'h1'      => __('H1', 'bb-njba'),
                            'h2'      => __('H2', 'bb-njba'),
                            'h3'      => __('H3', 'bb-njba'),
                            'h4'      => __('H4', 'bb-njba'),
                            'h5'      => __('H5', 'bb-njba'),
                            'h6'      => __('H6', 'bb-njba'),
                        )
                    ),
                    'name_alignment' => array(
                        'type' => 'select',
                        'label' => __('Alignment','bb-njba'),
                        'default' => 'center',
                        'options' => array(
                            'left' => __('Left','bb-njba'),
                            'center' => __('Center','bb-njba'),
                            'right' => __('Right','bb-njba')
                        ),
                    ),
                    'name_font_family' => array(
                        'type' => 'font',
                        'label' => __('Font Family','bb-njba'),
                        'default' => array(
                            'family' => 'Default',
                            'weight' => 'Default'
                        ),
                    ),
                    'name_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
                    ),
                    'name_color' => array(
                        'type' => 'color',
                        'label' => __('Text Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff',
                    ),
                    'name_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 5,
                            'right'         => 20,
                            'bottom'       => 5,
                            'left'         => 20
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                        ),
                    ),
                    
                )
            ), // Section
            'artist_name'       => array( // Section
                'title'         =>  __('Artist Name', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'artist_alignment' => array(
                        'type' => 'select',
                        'label' => __('Alignment','bb-njba'),
                        'default' => 'center',
                        'options' => array(
                            'left' => __('Left','bb-njba'),
                            'center' => __('Center','bb-njba'),
                            'right' => __('Right','bb-njba')
                        )   
                    ),
                    'artist_font_family' => array(
                        'type' => 'font',
                        'label' => __('Font Family','bb-njba'),
                        'default' => array(
                            'family' => 'Default',
                            'weight' => 'Default'
                        ),
                    ),
                    'artist_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '12',
                            'medium' => '12',
                            'small' => '12'
                        )
                    ),
                    'artist_color' => array(
                        'type' => 'color',
                        'label' => __('Text Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff'
                    ),
                    'artist_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 5,
                            'right'         => 20,
                            'bottom'       => 5,
                            'left'         => 20
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                        ),
                    ),
                )
            ), // Section
        ),
    ),
));