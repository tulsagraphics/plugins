.fl-wp-audio .wp-audio {
	position: absolute;
	top: 0;
	left: 0;
	width: 100% !important;
	height: 100% !important;
}
.njba-audio-main h1,
.njba-audio-main h2,
.njba-audio-main h3,
.njba-audio-main h4,
.njba-audio-main h5,
.njba-audio-main h6{margin:0px;}
.njba-audio-main .mejs-container, 
.njba-audio-main .mejs-container .mejs-controls, 
.njba-audio-main .mejs-embed, 
.njba-audio-main .mejs-embed body{background: none;}
.njba-audio-main .mejs-controls .mejs-button button{background: none;}
.njba-audio-main .mejs-button.mejs-playpause-button.mejs-play button:before {content: "";font-family: FontAwesome;font-size: 22px;color: #ffffff;}
.njba-audio-main .mejs-button.mejs-playpause-button.mejs-pause button:before{content: "";font-family: FontAwesome;font-size: 22px;color: #ffffff;}
.njba-audio-main .mejs-button.mejs-volume-button.mejs-mute button:before,
.njba-audio-main .mejs-button.mejs-volume-button.mejs-unmute button:before{content: "";font-family: FontAwesome;font-size: 22px;color: #ffffff;}
.njba-audio-main .njba-audio-main .mejs-time.mejs-currenttime-container {display: none !important;}
.njba-audio-main .mejs-container:focus {outline: none;box-shadow: none;}
.njba-audio-main span.mejs-currenttime {display: none;}
.njba-audio-main .mejs-controls .mejs-time-rail .mejs-time-loaded{background: none;}
.njba-audio-main .mejs-controls .mejs-time-rail .mejs-time-current {background: #ffffff;height:4px ;}
.njba-audio-main .mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-total, 
.njba-audio-main .mejs-controls .mejs-time-rail .mejs-time-total {background: rgba(255,255,255,.8);}
.njba-audio-main .mejs-controls .mejs-time-rail .mejs-time-handle {display: block; border-color: #ffffff;top: -3px;}
.njba-audio-main .mejs-controls .mejs-time-rail .mejs-time-total, 
.njba-audio-main .mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-total,
.njba-audio-main .mejs-controls .mejs-time-rail a,
.njba-audio-main .mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-current,
.njba-audio-main .mejs-controls .mejs-time-rail .mejs-time-loaded,
.njba-audio-main span.mejs-time-current{height:4px;}
.njba-audio-main .mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-handle {display: block;width: 10px ; height: 10px;background: #ffffff;border-radius: 20px;position: absolute;top: 7px;}
.njba-audio-main span.mejs-duration {font-size: 18px;}
.njba-audio-main .njba-audio-main .mejs-controls .mejs-time-rail .mejs-time-loaded{background:none;}
.njba-audio-main .mejs-controls {text-align: center;}
.njba-audio-main .mejs-container .mejs-controls div{display: inline-block;float: none;}
.njba-audio-main .mejs-controls a.mejs-horizontal-volume-slider {display: inline-block;float: none; vertical-align: top;}
.njba-audio-player-main {width: 100%;height: auto;background-color: #F18470;display: inline-block; display: flex;}
.njba-audio-player-main.style-1 .njba-audio-back {width: 100%;}
.njba-audio-player-main.style-2 .njba-audio-back {width: 70%;float: left;text-align: center;padding: 20px 0px; display: flex;}
.njba-audio-player-main.style-2 .njab-image-main {width: 30%;float: left;}
.njba-audio-player-main.style-3 {display:inline-block;}
.njba-audio-player-main.style-3 .njab-image-main {width: 100%;float: left;}
.njba-audio-player-main.style-3 .njba-audio-back{width: 100%;}
/*.njba-audio-name {padding-bottom: 35px;}*/
img.njba-responsive-image {width: 100%;}
.njba-audio-name h1 {font-size: 25px;color: #fff;font-weight: 600;margin-bottom: 5px;text-transform: capitalize;margin: 0;}
.njba-audio-name p {font-size: 14px;color: #fff;font-weight: 400;text-transform: capitalize;margin: 0;font-style: italic;padding-bottom:20px;}
.njba-audio-back-three {width: 100%;height: 100%;display: table-cell;vertical-align: middle;padding: 15px;}
.njba-audio-back-two {width: 100%;height: 100%;display: table;}
.fl-node-<?php echo $id; ?> .njba-audio-name <?php echo  $settings->name_tag;?> {
	<?php if( $settings->name_alignment ) { ?>text-align: <?php echo $settings->name_alignment; ?>;<?php } ?>
	<?php if( $settings->name_font_family['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->name_font_family ); ?><?php } ?>
	<?php if( $settings->name_color ) { ?>color: #<?php echo $settings->name_color; ?>;<?php } ?>
	<?php if( $settings->name_font_size['desktop'] ) { ?>font-size: <?php echo $settings->name_font_size['desktop']; ?>px;<?php } ?>
	<?php if( $settings->name_padding['top'] != '') { ?>padding-top: <?php echo $settings->name_padding['top']; ?>px;<?php }?>
	<?php if( $settings->name_padding['right'] != '') { ?>padding-right: <?php echo $settings->name_padding['right']; ?>px;<?php } ?>
	<?php if( $settings->name_padding['bottom'] != '') { ?>padding-bottom: <?php echo $settings->name_padding['bottom']; ?>px;<?php } ?>
	<?php if( $settings->name_padding['left'] != '') { ?>padding-left: <?php echo $settings->name_padding['left']; ?>px;<?php } ?>
	
}
.fl-node-<?php echo $id; ?> .njba-audio-name p {
	<?php if( $settings->artist_alignment ) { ?>text-align: <?php echo $settings->artist_alignment; ?>;<?php } ?>
	<?php if( $settings->artist_color ) { ?>color: #<?php echo $settings->artist_color; ?>;<?php } ?>
	<?php if( $settings->artist_font_family['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->artist_font_family ); ?><?php } ?>
	<?php if( $settings->artist_font_size['desktop'] ) { ?>font-size: <?php echo $settings->artist_font_size['desktop']; ?>px;<?php } ?>
	<?php if( $settings->artist_padding['top'] != '') { ?>padding-top: <?php echo $settings->artist_padding['top']; ?>px;<?php } ?>
	<?php if( $settings->artist_padding['right'] != '') { ?>padding-right: <?php echo $settings->artist_padding['right']; ?>px;<?php } ?>
	<?php if( $settings->artist_padding['bottom']!= '' ) { ?>padding-bottom: <?php echo $settings->artist_padding['bottom']; ?>px;<?php } ?>
	<?php if( $settings->artist_padding['left'] != '') { ?>padding-left: <?php echo $settings->artist_padding['left']; ?>px;<?php } ?>
	
}
.fl-node-<?php echo $id; ?> .njba-audio-main span.mejs-duration,
.fl-node-<?php echo $id; ?> .mejs-button.mejs-playpause-button.mejs-play button:before,
.fl-node-<?php echo $id; ?> .mejs-button.mejs-playpause-button.mejs-pause button:before,
.fl-node-<?php echo $id; ?> .mejs-button.mejs-volume-button.mejs-mute button:before,
.fl-node-<?php echo $id; ?> .mejs-button.mejs-volume-button.mejs-unmute button:before {
	<?php if( $settings->play_font_size['desktop'] ) { ?>font-size: <?php echo $settings->play_font_size['desktop']; ?>px;<?php } ?>
	<?php if( $settings->play_txt_color ) { ?>color: #<?php echo $settings->play_txt_color; ?>;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-audio-main .mejs-controls {
	<?php if( $settings->track_alignment ) { ?>text-align: <?php echo $settings->track_alignment; ?>;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-audio-player-main {
	<?php if( $settings->skin_bg_color ) { ?>background: #<?php echo $settings->skin_bg_color; ?>;<?php } ?>
}
.fl-node-<?php echo $id; ?> .mejs-container{
	<?php if( $settings->track_margin['top'] != '') { ?>margin-top: <?php echo $settings->track_margin['top']; ?>px;<?php } ?>
	<?php if( $settings->track_margin['bottom'] != '') { ?>margin-bottom: <?php echo $settings->track_margin['bottom']; ?>px;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-audio-main .mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-total, 
.fl-node-<?php echo $id; ?> .njba-audio-main .mejs-controls .mejs-time-rail .mejs-time-total {
		<?php if( $settings->track_color ) { 
					$track_color = njba_hex2rgba( $settings->track_color ,'0.5');
		?>
						background: <?php echo $track_color; ?>;
		<?php } ?>
		
}
<?php if($settings->volume_show =='0') {?>
	.fl-node-<?php echo $id; ?> .mejs-button.mejs-volume-button.mejs-mute,
	.fl-node-<?php echo $id; ?> a.mejs-horizontal-volume-slider{display:none !important;}
<?php }?>
<?php if($settings->track_show =='0') {?>
	.fl-node-<?php echo $id; ?> .mejs-button.mejs-playpause-button,
	.fl-node-<?php echo $id; ?> .mejs-time-rail,
	.fl-node-<?php echo $id; ?> .mejs-time.mejs-duration-container{display:none !important;}
<?php }?>
<?php if($global_settings->responsive_enabled) { // Global Setting If started ?>
	@media ( max-width: <?php echo $global_settings->medium_breakpoint .'px'; ?> ) {
	}
	@media ( max-width: <?php echo $global_settings->responsive_breakpoint .'px'; ?> ) {
		.fl-node-<?php echo $id; ?> .njba-audio-player-main.style-2 .njab-image-main,
		.fl-node-<?php echo $id; ?> .njba-audio-player-main.style-2 .njba-audio-back{
			    width: 100%;
		}
		.njba-audio-player-main{display: inline-block;}
		
	}
<?php } //die();?>
