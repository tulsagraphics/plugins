	<div class="njba-audio-main " itemscope itemtype="http://schema.org/AudioObject">
		<?php
					
			$loop       = isset($settings->loop) && $settings->loop ? ' loop="yes"' : '';
			$autoplay   = isset($settings->autoplay) && $settings->autoplay ? ' autoplay="yes"' : '';
		?>
				<div class="njba-audio-player-main <?php echo $settings->audio_style; ?>">
					<?php if($settings->audio_style =='style-2' || $settings->audio_style =='style-3'){ ?>
					<div class="njab-image-main">
						<?php if($settings->photo != ''){ ?>
				    		<img class="njba-responsive-image" src="<?php echo  $settings->photo_src; ?>" >
				    	<?php }?>
				    </div>
				    <?php  }?>
				    <div class="njba-audio-back">
				    	<div class="njba-audio-back-two">
					    	<div class="njba-audio-back-three">
						    	<div class="njba-audio-name">
			                        <div class="njba-audio-name-selector"><<?php echo  $settings->name_tag;?>><?php echo $settings->audio_name; ?></<?php echo  $settings->name_tag;?>></div>
			                       <?php if($settings->artists){ ?> <div class="njba-artists-name-selector"><p><strong><?php echo $settings->artists_name; ?></strong></p></div><?php  }?>
			                    </div>
						    	<?php 
						    	if(!empty($settings->audios)){
						    		$number_playlist = count($settings->audios);
									$audios = FLBuilderPhoto::get_attachment_data($settings->audios[0]);
						    		if(!empty($audios)){
							        	echo '<meta itemprop="url" content="' . $audios->url . '" />';
										echo '[audio src="' . $audios->url . '"' . $autoplay . $loop . ']';	
									}
								}
						        ?>
						    </div>
					    </div>
				    </div>
				</div><!--njba-audio-player-main-->
				
		<?php
		?>
	</div><!--njba-audio-main-->	
