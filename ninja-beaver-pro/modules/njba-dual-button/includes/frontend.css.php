
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one{
    
    <?php if($settings->button_one_background_color) { ?> background-color: <?php echo '#'.$settings->button_one_background_color; ?>; <?php } ?>
    <?php if($settings->button_one_text_color) { ?>color:<?php echo '#'.$settings->button_one_text_color;?>;<?php } ?>
	
	<?php if($settings->button_one_font_family['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->button_one_font_family ); ?><?php } ?>
	<?php if(isset($settings->button_one_font_size['desktop'])) { ?>font-size:<?php echo $settings->button_one_font_size['desktop'];?>px;<?php } ?>
	
	<?php if($settings->button_one_border_width >= 0) { ?>border-width: <?php echo $settings->button_one_border_width;?>px; <?php } ?>
	<?php if($settings->button_one_border_style) { ?>border-style:<?php echo $settings->button_one_border_style;?>;<?php } ?>
	<?php if($settings->button_one_border_color) { ?>border-color:<?php echo '#'.$settings->button_one_border_color;?>;<?php } ?>
	
	<?php if(isset($settings->button_one_border_radius['top_left']) ) { ?>border-top-left-radius:<?php echo $settings->button_one_border_radius['top_left'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_border_radius['top_right']) ) { ?>border-top-right-radius:<?php echo $settings->button_one_border_radius['top_right'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_border_radius['bottom_left']) ) { ?>border-bottom-left-radius:<?php echo $settings->button_one_border_radius['bottom_left'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_border_radius['bottom_right']) ) { ?>border-bottom-right-radius:<?php echo $settings->button_one_border_radius['bottom_right'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_padding['top']) ) { ?>padding-top:<?php echo $settings->button_one_padding['top'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_padding['right']) ) { ?>padding-right:<?php echo $settings->button_one_padding['right'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_padding['bottom']) ) { ?>padding-bottom:<?php echo $settings->button_one_padding['bottom'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_padding['left']) ) { ?>padding-left:<?php echo $settings->button_one_padding['left'];?>px;<?php } ?>
	<?php if($settings->button_one_transition) { ?>transition:<?php echo 'all ease '.$settings->button_one_transition;?>s;<?php } ?>
	box-shadow:<?php if(isset($settings->button_one_box_shadow['left_right']) ) { echo $settings->button_one_box_shadow['left_right']; } ?>px <?php if(isset($settings->button_one_box_shadow['top_bottom']) ) { echo $settings->button_one_box_shadow['top_bottom']; } ?>px <?php if(isset($settings->button_one_box_shadow['blur']) ) { echo $settings->button_one_box_shadow['blur']; } ?>px <?php if(isset($settings->button_one_box_shadow['spread']) ) { echo $settings->button_one_box_shadow['spread']; } ?>px #<?php echo $settings->button_one_box_shadow_color;?>;	
	
}
<?php $shadow_color = FLBuilderColor::adjust_brightness( njba_parse_color_to_hex( $settings->button_one_background_color ), 30, 'darken' ); ?>
<?php $shadow_color_hover = FLBuilderColor::adjust_brightness( njba_parse_color_to_hex( $settings->button_one_background_hover_color ), 30, 'darken' ); ?>
<?php if ($settings->button_one_style == 'gradient') {
	$bg_grad_start = "#" .FLBuilderColor::adjust_brightness( njba_parse_color_to_hex( $settings->button_one_background_color ), 80, 'lighten' );
	$bg_hover_grad_start = "#" . FLBuilderColor::adjust_brightness( njba_parse_color_to_hex( $settings->button_one_background_hover_color ), 80, 'lighten' );
?>
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one{
	background: linear-gradient(to top,  <?php echo $bg_grad_start; ?> 0%, #<?php echo $settings->button_one_background_color; ?> 100%); /* FF3.6+ */
	background: -moz-linear-gradient(to top,  <?php echo $bg_grad_start; ?> 0%, <?php echo $settings->button_one_background_color; ?> 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,<?php echo $bg_grad_start; ?>), color-stop(100%,<?php echo $settings->button_one_background_color; ?>)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(to top,  <?php echo $bg_grad_start; ?> 0%,<?php echo $settings->button_one_background_color; ?> 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(to top,  <?php echo $bg_grad_start; ?> 0%,<?php echo $settings->button_one_background_color; ?> 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(to top,  <?php echo $bg_grad_start; ?> 0%,<?php echo $settings->button_one_background_color; ?> 100%); /* IE10+ */
	}
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one:hover{
	background: linear-gradient(to bottom,  <?php echo $bg_hover_grad_start; ?> 0%, #<?php echo $settings->button_one_background_hover_color; ?> 100%); /* FF3.6+ */
	background: -moz-linear-gradient(to bottom,  <?php echo $bg_hover_grad_start; ?> 0%, #<?php echo $settings->button_one_background_hover_color; ?> 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,<?php echo $bg_hover_grad_start; ?>), color-stop(100%,<?php echo $settings->button_one_background_hover_color; ?>)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(to bottom,  <?php echo $bg_hover_grad_start; ?> 0%,<?php echo $settings->button_one_background_hover_color; ?> 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(to bottom,  <?php echo $bg_hover_grad_start; ?> 0%,<?php echo $settings->button_one_background_hover_color; ?> 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(to bottom,  <?php echo $bg_hover_grad_start; ?> 0%,<?php echo $settings->button_one_background_hover_color; ?> 100%); /* IE10+ */
	
	}
<?php } ?>
<?php if($settings->button_one_style == 'threed') {  ?>
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one{
		
		<?php if(isset($settings->button_one_border_radius['top']) ){ ?>border-top-left-radius:<?php echo $settings->button_one_border_radius['top'];?>px;<?php } ?>
		<?php if(isset($settings->button_one_border_radius['right']) ) { ?>border-top-right-radius:<?php echo $settings->button_one_border_radius['right'];?>px;<?php } ?>
		<?php if(isset($settings->button_one_border_radius['bottom']) ){ ?>border-bottom-left-radius:<?php echo $settings->button_one_border_radius['bottom'];?>px;<?php } ?>
		<?php if(isset($settings->button_one_border_radius['left']) ) { ?>border-bottom-right-radius:<?php echo $settings->button_one_border_radius['left'];?>px;<?php } ?>
		top:0;
		box-shadow:0 4px #<?php echo $shadow_color; ?>;
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one:hover{
		
		
		box-shadow:0 4px #<?php echo $shadow_color_hover; ?>;
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one:active{
    
    	top:4px;
		box-shadow:none;
	
}
<?php } ?>
<?php if($settings->button_one_style == 'transparent') {  ?>
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one{
		background-color: transparent;
}
<?php } ?>
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one:hover{
    <?php if($settings->button_one_background_hover_color) { ?>background-color: <?php echo '#'.$settings->button_one_background_hover_color; ?>;<?php } ?>
    <?php if($settings->button_one_text_hover_color) { ?>color:<?php echo '#'.$settings->button_one_text_hover_color;?>;<?php } ?>
    <?php if($settings->button_one_border_hover_color) { ?>border-color:<?php echo '#'.$settings->button_one_border_hover_color;?>;<?php } ?>
    box-shadow:<?php if(isset($settings->button_one_box_shadow['left_right']) ) { echo $settings->button_one_box_shadow['left_right']; } ?>px <?php if(isset($settings->button_one_box_shadow['top_bottom']) ) { echo $settings->button_one_box_shadow['top_bottom']; } ?>px <?php if(isset($settings->button_one_box_shadow['blur']) ) { echo $settings->button_one_box_shadow['blur']; } ?>px <?php if(isset($settings->button_one_box_shadow['spread']) ) { echo $settings->button_one_box_shadow['spread']; } ?>px #<?php echo $settings->button_one_box_shadow_color;?>;	
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one .dual-btn-icon > i{
	<?php if($settings->button_one_icon_color) { ?>color:<?php echo '#'.$settings->button_one_icon_color;?>;<?php } ?>
	<?php if(isset($settings->button_one_icon_font_size['desktop'])) { ?>font-size:<?php echo $settings->button_one_icon_font_size['desktop'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_icon_padding['top']) ) { ?>padding-top:<?php echo $settings->button_one_icon_padding['top'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_icon_padding['right']) ) { ?>padding-right:<?php echo $settings->button_one_icon_padding['right'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_icon_padding['bottom']) ) { ?>padding-bottom:<?php echo $settings->button_one_icon_padding['bottom'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_icon_padding['left']) ) { ?>padding-left:<?php echo $settings->button_one_icon_padding['left'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_icon_margin['top']) ) { ?>margin-top:<?php echo $settings->button_one_icon_margin['top'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_icon_margin['right']) ) { ?>margin-right:<?php echo $settings->button_one_icon_margin['right'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_icon_margin['bottom']) ) { ?>margin-bottom:<?php echo $settings->button_one_icon_margin['bottom'];?>px;<?php } ?>
	<?php if(isset($settings->button_one_icon_margin['left']) ) { ?>margin-left:<?php echo $settings->button_one_icon_margin['left'];?>px;<?php } ?>
	<?php if($settings->button_one_transition) { ?>transition:<?php echo 'all ease '.$settings->button_one_transition;?>s;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one:hover i{
	<?php if($settings->button_one_icon_hover_color) { ?>color:<?php echo '#'.$settings->button_one_icon_hover_color;?>;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-dual-btn{
	display: inline-flex;
    position: relative;
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-one, .fl-node-<?php echo $id; ?> .njba-dual-btn-two {
  		float:left;
  		align-items: center;
  		display:flex;
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-one > div, .fl-node-<?php echo $id; ?> .njba-dual-btn-two > div {
  		margin: 0 auto;
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-main{
		display: inline-flex;
    	position: relative;
}
<?php if($settings->button_width == 'full_width'){?>
	.fl-node-<?php echo $id; ?> .njba-dual-btn{
		width:100%;
	}
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main{
		width:100%;
	}
	.fl-node-<?php echo $id; ?> .njba-dual-btn-one, .fl-node-<?php echo $id; ?> .njba-dual-btn-two {
  		width: 100%;
  		display:flex;
  		text-align:center;
  		align-items:center;
	}
	.fl-node-<?php echo $id; ?> .njba-dual-btn-one span, .fl-node-<?php echo $id; ?> .njba-dual-btn-two span {
  		margin:0;
	}
<?php }?>
<?php if($settings->button_width == 'custom'){?>
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one, .fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two{
		<?php if($settings->button_custom_width){?> width:<?php echo $settings->button_custom_width;?>px;<?php }?>
		<?php if($settings->button_custom_height){?> min-height:<?php echo $settings->button_custom_height;?>px;<?php }?>
		<?php if($settings->button_custom_height){?> line-height: <?php echo $settings->button_custom_height;?>px;<?php }?>
		display: flex;
		text-align:center;
}
	<?php } ?>
.fl-node-<?php echo $id; ?> .njba-dual-btn-wrapper{
	
	<?php if($settings->button_alignment == 'left'){?>text-align:left;<?php }?>
	<?php if($settings->button_alignment == 'center'){?>text-align:center;<?php }?>
	<?php if($settings->button_alignment == 'right'){?>text-align:right;<?php }?>
}
<?php if($settings->button_join == 'no'){?>
.fl-node-<?php echo $id; ?> .njba-dual-btn-divider-no{
	<?php if($settings->button_spacing){?>width:<?php echo $settings->button_spacing;?>px;<?php } ?>
	<?php if($settings->button_type == 'vertical'){?>
		<?php if($settings->button_spacing){?>height:<?php echo $settings->button_spacing;?>px;<?php } ?>
	
	<?php } ?>	
}
<?php }?>
<?php if($settings->button_join == 'yes'){?>
.fl-node-<?php echo $id; ?> .njba-dual-btn-divider-yes{
	
	<?php if($settings->divider_text_icon_color){?>color:#<?php echo $settings->divider_text_icon_color;?>;<?php } ?>
	<?php if($settings->divider_background_color){?>background-color:#<?php echo $settings->divider_background_color;?>;<?php } ?>
	<?php if($settings->divider_border_width >= 0) { ?>border: <?php echo $settings->divider_border_width;?>px; <?php } ?>
	<?php if($settings->divider_border_style) { ?>border-style:<?php echo $settings->divider_border_style;?>;<?php } ?>
	<?php if($settings->divider_border_color) { ?>border-color:<?php echo '#'.$settings->divider_border_color;?>;<?php } ?>
	<?php if($settings->divider_border_radius) { ?>border-radius:<?php echo $settings->divider_border_radius;?>px;<?php } ?>
	<?php if($settings->divider_custom_width) { ?>width:<?php echo $settings->divider_custom_width;?>px;<?php } ?>
	<?php if($settings->divider_custom_height) { ?>height:<?php echo $settings->divider_custom_height;?>px;<?php } ?>
    box-sizing: content-box;
    display: inline-block;
    left: auto;
    margin: 0 auto;
    overflow: hidden;
    position: absolute;
    right: 0;
    top: 50%;
    transform: translate(50%,-50%);
    vertical-align: middle;
    z-index: 1;
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-main .njba-dual-btn .njba-dual-btn-divider-yes i{
	<?php if($settings->divider_text_icon_color){?>color:#<?php echo $settings->divider_text_icon_color;?>;<?php } ?>
	<?php if(isset($settings->divider_font_size['desktop']) ) { ?>font-size:<?php echo $settings->divider_font_size['desktop'];?>px;<?php } ?>
    <?php if($settings->divider_custom_width) { ?>width:<?php echo $settings->divider_custom_width;?>px;<?php } ?>
	<?php if($settings->divider_custom_height) { ?>height:<?php echo $settings->divider_custom_height;?>px;<?php } ?>
	<?php if($settings->divider_custom_height) { ?>line-height:<?php echo $settings->divider_custom_height;?>px;<?php } ?>
	text-align: center;
	display:block;
	font-style: normal;	
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one .njba-dual-btn-divider-yes img{
	<?php if($settings->divider_text_icon_color){?>color:#<?php echo $settings->divider_text_icon_color;?>;<?php } ?>
    <?php if($settings->divider_custom_width) { ?>width:<?php echo $settings->divider_custom_width;?>px;<?php } ?>
	<?php if($settings->divider_custom_height) { ?>height:<?php echo $settings->divider_custom_height;?>px;<?php } ?>
	<?php if($settings->divider_custom_height) { ?>line-height:<?php echo $settings->divider_custom_height;?>px;<?php } ?>
	text-align: center;
	display:block;	
	padding:5px;
}
<?php }?>
<?php if($settings->button_type == 'vertical'){?>
.fl-node-<?php echo $id; ?> .njba-dual-btn-vertical .njba-dual-btn-one, .fl-node-<?php echo $id; ?> .njba-dual-btn-vertical .njba-dual-btn-two {
	float:none;
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-vertical.njba-dual-btn{
	display: block;
  	margin:0;
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-main{
	flex-direction: column;	
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-vertical .njba-dual-btn-divider-yes{
	left: 50%;
    margin: 0;
	top: 100%;
    transform: translate(-50%, -50%);
}
<?php }?>
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two{
    
    <?php if($settings->button_two_background_color) { ?> background-color: <?php echo '#'.$settings->button_two_background_color; ?>; <?php } ?>
    <?php if($settings->button_two_text_color) { ?>color:<?php echo '#'.$settings->button_two_text_color;?>;<?php } ?>
	
	<?php if($settings->button_two_font_family['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->button_two_font_family ); ?><?php } ?>
	<?php if(isset($settings->button_two_font_size['desktop']) ){ ?>font-size:<?php echo $settings->button_two_font_size['desktop'];?>px;<?php } ?>
	
	<?php if($settings->button_two_border_width >= 0) { ?>border: <?php echo $settings->button_two_border_width;?>px; <?php } ?>
	<?php if($settings->button_two_border_style) { ?>border-style:<?php echo $settings->button_two_border_style;?>;<?php } ?>
	<?php if($settings->button_two_border_color) { ?>border-color:<?php echo '#'.$settings->button_two_border_color;?>;<?php } ?>
	<?php if(isset($settings->button_two_border_radius['top_left']) ){ ?>border-top-left-radius:<?php echo $settings->button_two_border_radius['top_left'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_border_radius['top_right']) ){ ?>border-top-right-radius:<?php echo $settings->button_two_border_radius['top_right'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_border_radius['bottom_left']) ){ ?>border-bottom-left-radius:<?php echo $settings->button_two_border_radius['bottom_left'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_border_radius['bottom_right']) ){ ?>border-bottom-right-radius:<?php echo $settings->button_two_border_radius['bottom_right'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_padding['top']) ){ ?>padding-top:<?php echo $settings->button_two_padding['top'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_padding['right']) ){ ?>padding-right:<?php echo $settings->button_two_padding['right'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_padding['bottom']) ){ ?>padding-bottom:<?php echo $settings->button_two_padding['bottom'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_padding['left']) ){ ?>padding-left:<?php echo $settings->button_two_padding['left'];?>px;<?php } ?>
	<?php if($settings->button_two_transition) { ?>transition:<?php echo 'all ease '.$settings->button_two_transition;?>s;<?php } ?>
	box-shadow:<?php if(isset($settings->button_two_box_shadow['left_right']) ) { echo $settings->button_two_box_shadow['left_right']; } ?>px <?php if(isset($settings->button_two_box_shadow['top_bottom']) ) { echo $settings->button_two_box_shadow['top_bottom']; } ?>px <?php if(isset($settings->button_two_box_shadow['blur']) ) { echo $settings->button_two_box_shadow['blur']; } ?>px <?php if(isset($settings->button_two_box_shadow['spread']) ) { echo $settings->button_two_box_shadow['spread']; } ?>px #<?php echo $settings->button_two_box_shadow_color;?>;
	
	
}
<?php if ($settings->button_two_style == 'gradient') {
	$bg_grad_start = "#" .FLBuilderColor::adjust_brightness( njba_parse_color_to_hex( $settings->button_two_background_color ), 80, 'lighten' );
	$bg_hover_grad_start = "#" . FLBuilderColor::adjust_brightness( njba_parse_color_to_hex( $settings->button_two_background_hover_color ), 80, 'lighten' );
?>
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two{
	background: linear-gradient(to top,  <?php echo $bg_grad_start; ?> 0%, #<?php echo $settings->button_two_background_color; ?> 100%); /* FF3.6+ */
	background: -moz-linear-gradient(to top,  <?php echo $bg_grad_start; ?> 0%, <?php echo $settings->button_two_background_color; ?> 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,<?php echo $bg_grad_start; ?>), color-stop(100%,<?php echo $settings->button_two_background_color; ?>)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(to top,  <?php echo $bg_grad_start; ?> 0%,<?php echo $settings->button_two_background_color; ?> 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(to top,  <?php echo $bg_grad_start; ?> 0%,<?php echo $settings->button_two_background_color; ?> 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(to top,  <?php echo $bg_grad_start; ?> 0%,<?php echo $settings->button_two_background_color; ?> 100%); /* IE10+ */
	}
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two:hover{
	background: linear-gradient(to bottom,  <?php echo $bg_hover_grad_start; ?> 0%, #<?php echo $settings->button_two_background_hover_color; ?> 100%); /* FF3.6+ */
	background: -moz-linear-gradient(to bottom,  <?php echo $bg_hover_grad_start; ?> 0%, #<?php echo $settings->button_two_background_hover_color; ?> 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,<?php echo $bg_hover_grad_start; ?>), color-stop(100%,<?php echo $settings->button_two_background_hover_color; ?>)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(to bottom,  <?php echo $bg_hover_grad_start; ?> 0%,<?php echo $settings->button_two_background_hover_color; ?> 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(to bottom,  <?php echo $bg_hover_grad_start; ?> 0%,<?php echo $settings->button_two_background_hover_color; ?> 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(to bottom,  <?php echo $bg_hover_grad_start; ?> 0%,<?php echo $settings->button_two_background_hover_color; ?> 100%); /* IE10+ */
	
	}
<?php } ?>
<?php if($settings->button_two_style == 'threed') {  ?>
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two{
		<?php $shadow_color = FLBuilderColor::adjust_brightness( njba_parse_color_to_hex( $settings->button_two_background_color ), 30, 'darken' ); ?>
		<?php if(isset($settings->button_two_border_radius['top']) ){ ?>border-top-left-radius:<?php echo $settings->button_two_border_radius['top'];?>px;<?php } ?>
		<?php if(isset($settings->button_two_border_radius['right']) ){ ?>border-top-right-radius:<?php echo $settings->button_two_border_radius['right'];?>px;<?php } ?>
		<?php if(isset($settings->button_two_border_radius['bottom']) ){ ?>border-bottom-left-radius:<?php echo $settings->button_two_border_radius['bottom'];?>px;<?php } ?>
		<?php if(isset($settings->button_two_border_radius['left']) ){ ?>border-bottom-right-radius:<?php echo $settings->button_two_border_radius['left'];?>px;<?php } ?>
		top:0;
		box-shadow:0 4px #<?php echo $shadow_color; ?>;
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two:hover{
		
		<?php $shadow_color_hover = FLBuilderColor::adjust_brightness( njba_parse_color_to_hex( $settings->button_two_background_hover_color ), 30, 'darken' ); ?>
		box-shadow:0 4px #<?php echo $shadow_color_hover; ?>;
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two:active{
    
    	top:4px;
		box-shadow:none;
	
}
<?php } ?>
<?php if($settings->button_two_style == 'transparent') {  ?>
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two{
		background-color: transparent;
}
<?php } ?>
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two:hover{
    <?php if($settings->button_two_background_hover_color) { ?>background-color: <?php echo '#'.$settings->button_two_background_hover_color; ?>;<?php } ?>
    <?php if($settings->button_two_text_hover_color) { ?>color:<?php echo '#'.$settings->button_two_text_hover_color;?>;<?php } ?>
    <?php if($settings->button_two_border_hover_color) { ?>border-color:<?php echo '#'.$settings->button_two_border_hover_color;?>;<?php } ?>
    
    box-shadow:<?php if(isset($settings->button_two_box_shadow['left_right']) ) { echo $settings->button_two_box_shadow['left_right']; } ?>px <?php if(isset($settings->button_two_box_shadow['top_bottom']) ) { echo $settings->button_two_box_shadow['top_bottom']; } ?>px <?php if(isset($settings->button_two_box_shadow['blur']) ) { echo $settings->button_two_box_shadow['blur']; } ?>px <?php if(isset($settings->button_two_box_shadow['spread']) ) { echo $settings->button_two_box_shadow['spread']; } ?>px #<?php echo $settings->button_two_box_shadow_color;?>;
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two .dual-btn-icon > i{
	<?php if($settings->button_two_icon_color) { ?>color:<?php echo '#'.$settings->button_two_icon_color;?>;<?php } ?>
	<?php if(isset($settings->button_two_icon_font_size['desktop']) ){ ?>font-size:<?php echo $settings->button_two_icon_font_size['desktop'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_icon_padding['top']) ){ ?>padding-top:<?php echo $settings->button_two_icon_padding['top'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_icon_padding['right']) ){ ?>padding-right:<?php echo $settings->button_two_icon_padding['right'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_icon_padding['bottom']) ){ ?>padding-bottom:<?php echo $settings->button_two_icon_padding['bottom'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_icon_padding['left']) ){ ?>padding-left:<?php echo $settings->button_two_icon_padding['left'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_icon_margin['top']) ){ ?>margin-top:<?php echo $settings->button_two_icon_margin['top'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_icon_margin['right']) ){ ?>margin-right:<?php echo $settings->button_two_icon_margin['right'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_icon_margin['bottom']) ){ ?>margin-bottom:<?php echo $settings->button_two_icon_margin['bottom'];?>px;<?php } ?>
	<?php if(isset($settings->button_two_icon_margin['left']) ){ ?>margin-left:<?php echo $settings->button_two_icon_margin['left'];?>px;<?php } ?>
	<?php if($settings->button_two_transition) { ?>transition:<?php echo 'all ease '.$settings->button_two_transition;?>s;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two:hover i{
	<?php if($settings->button_two_icon_hover_color) { ?>color:<?php echo '#'.$settings->button_two_icon_hover_color;?>;<?php } ?>
}
@media ( max-width: 991px ) {
	
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one{
		<?php if(isset($settings->button_one_font_size['medium']) ){ ?>font-size:<?php echo $settings->button_one_font_size['medium'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one .dual-btn-icon > i{
		<?php if(isset($settings->button_one_icon_font_size['medium']) ){ ?>font-size:<?php echo $settings->button_one_icon_font_size['medium'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main .njba-dual-btn .njba-dual-btn-divider-yes i{
		<?php if(isset($settings->divider_font_size['medium']) ){ ?>font-size:<?php echo $settings->divider_font_size['medium'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two{
		<?php if(isset($settings->button_two_font_size['medium']) ){ ?>font-size:<?php echo $settings->button_two_font_size['medium'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two .dual-btn-icon > i{
		<?php if(isset($settings->button_two_icon_font_size['medium']) ){ ?>font-size:<?php echo $settings->button_two_icon_font_size['medium'];?>px;<?php } ?>
	}
	
}
@media ( max-width: 767px ) {
	
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one{
		<?php if(isset($settings->button_one_font_size['small']) ){ ?>font-size:<?php echo $settings->button_one_font_size['small'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-one .dual-btn-icon > i{
		<?php if(isset($settings->button_one_icon_font_size['small']) ){ ?>font-size:<?php echo $settings->button_one_icon_font_size['small'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main .njba-dual-btn .njba-dual-btn-divider-yes i{
		<?php if(isset($settings->divider_font_size['small']) ){ ?>font-size:<?php echo $settings->divider_font_size['small'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two{
		<?php if(isset($settings->button_two_font_size['small']) ){ ?>font-size:<?php echo $settings->button_two_font_size['small'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-dual-btn-main a.njba-dual-btn-two .dual-btn-icon > i{
		<?php if(isset($settings->button_two_icon_font_size['small']) ){ ?>font-size:<?php echo $settings->button_two_icon_font_size['small'];?>px;<?php } ?>
	}
}
@media ( max-width: 480px ) {
.fl-node-<?php echo $id; ?> .njba-dual-btn-horizontal .njba-dual-btn-one, .fl-node-<?php echo $id; ?> .njba-dual-btn-horizontal .njba-dual-btn-two {
	float:none;
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-horizontal.njba-dual-btn{
	display: block;
  	margin:0;
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-main{
	<!-- flex-direction: column;	 -->
}
.fl-node-<?php echo $id; ?> .njba-dual-btn-horizontal .njba-dual-btn-divider-yes{
	left: 50%;
    margin: 0;
	top: 100%;
    transform: translate(-50%, -50%);
}
}