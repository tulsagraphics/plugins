<div class="njba-dual-btn-wrapper">
	<div class="njba-dual-btn-main">
		<div class="njba-dual-btn njba-dual-btn-<?php echo $settings->button_type;?>">
			<a href="<?php if($settings->button_one_link){echo $settings->button_one_link;}?>" target="<?php if($settings->button_one_link_target){echo $settings->button_one_link_target;}?>" class="njba-dual-btn-one njba-dual-btn-<?php echo $settings->button_one_style;?>">
				<div>
					<?php if($settings->button_one_icon_aligment == "left"){?>
						<?php $module->button_one_alignment(); ?>
					<?php }?>
					<span class="button-one-text-selector"><?php if($settings->button_one_text){ echo $settings->button_one_text;}?></span>
					<?php if($settings->button_one_icon_aligment == "right"){?>
						<?php $module->button_one_alignment(); ?>
					<?php }?>
				</div>
			</a>
			<span class="njba-dual-btn-divider-<?php echo $settings->button_join;?>">
					
				<?php if($settings->button_join == 'yes'){?>
					<?php if($settings->divider_text && $settings->select_divider == 'text'){ ?>
						 <i class="divider-text-selector"><?php echo $settings->divider_text;?></i> 
					<?php }?>
					<?php if($settings->divider_icon && $settings->select_divider == 'font_icon' ) {?>
							<i class="<?php echo $settings->divider_icon;?>"></i>
					<?php } ?>
					<?php if($settings->divider_custom_icon && $settings->select_divider == 'custom_icon' ) {?>
						  	<img src="<?php echo $settings->divider_custom_icon_src;?>" />
					<?php } ?>
					<?php } ?>
			</span>
		</div>	
		<?php if($settings->button_join == 'no'){?>
			<div class="njba-dual-btn-divider-<?php echo $settings->button_join;?>"></div>
		<?php }?>
		<div class="njba-dual-btn njba-dual-btn-<?php echo $settings->button_type;?>">
			<a href="<?php if($settings->button_two_link){echo $settings->button_two_link;}?>" target="<?php if($settings->button_two_link_target){echo $settings->button_two_link_target;}?>" class="njba-dual-btn-two njba-dual-btn-<?php echo $settings->button_two_style;?>">
				<div>
					<?php if($settings->button_two_icon_aligment == "left"){?>
						<?php $module->button_two_alignment(); ?>
					<?php }?>
					<span class="button-two-text-selector"><?php if($settings->button_two_text){ echo $settings->button_two_text;}?></span>
					<?php if($settings->button_two_icon_aligment == "right"){?>
						<?php $module->button_two_alignment(); ?>
					<?php }?>
				</div>
			</a> 
		</div> 
	</div>
</div>