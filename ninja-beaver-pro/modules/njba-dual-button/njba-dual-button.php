<?php
/**
 * @class NjbaDualButtonModule
 */
class NjbaDualButtonModule extends FLBuilderModule {
    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Dual Button', 'bb-njba'),
            'description'   => __('A module for Dual Button.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'content' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-dual-button/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-dual-button/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
            'partial_refresh' => true, // Set this to true to enable partial refresh.
            'icon'              => 'button.svg',
        ));
    }
    public function button_one_alignment(){
        if($this->settings->button_one_font_icon && $this->settings->buttton_one_icon_select == 'font_icon' ) {?>
            <span class="dual-btn-icon"><i class="<?php echo $this->settings->button_one_font_icon;?>"></i></span>
        <?php } ?>
        <?php if($this->settings->button_one_custom_icon && $this->settings->buttton_one_icon_select == 'custom_icon' ) {?>
            <span><img src="<?php echo $this->settings->button_one_custom_icon_src;?>" /></span>
        <?php }
    }
    public function button_two_alignment(){
        if($this->settings->button_two_font_icon && $this->settings->buttton_two_icon_select == 'font_icon' ) {?>
            <span class="dual-btn-icon"><i class="<?php echo $this->settings->button_two_font_icon;?>"></i></span>
        <?php } ?>
        <?php if($this->settings->button_two_custom_icon && $this->settings->buttton_two_icon_select == 'custom_icon' ) {?>
            <span><img src="<?php echo $this->settings->button_two_custom_icon_src;?>" /></span>
        <?php }
        
    }
}
/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('NjbaDualButtonModule', array(
    'general' => array(
        'title' => __('General','bb-njba'),
        'sections' => array(
            'button_general_settings' => array(
                'title' => __('General Settings','bb-njba'),
                'fields' => array(
                    'button_type'   => array(
                        'type'          => 'select',
                        'label'         =>  __('Button Type', 'bb-njba'),
                        'default'       =>  __('horizontal', 'bb-njba'),
                        'options'   => array(
                            'horizontal'     =>  __('Horizontal', 'bb-njba'),   
                            'vertical'    =>  __('Vertical', 'bb-njba'),   
                        )
                    ),
                    'button_join'   => array(
                        'type'          => 'select',
                        'label'         =>  __('Join Button', 'bb-njba'),
                        'default'       =>  __('yes', 'bb-njba'),
                        'options'   => array(
                            'yes'     =>  __('Yes', 'bb-njba'),   
                            'no'    =>  __('No', 'bb-njba'),   
                        ),
                        'toggle' => array(
                            'yes' => array(
                                'tabs' => array('divider_tab')
                            ),
                            'no' => array(
                                'fields' => array('button_spacing')
                            )
                        )
                    ),
                    'button_spacing' => array(
                        'type' => 'text',
                        'label' => __('Button Spacing','bb-njba'),
                        'default' => '10',
                        'description' => 'px',
                        'size' => '3'
                    ),
                    'button_width' => array(
                        'type' => 'select',
                        'label' => __('Width','bb-njba'),
                        'default' => 'auto',
                        'options' => array(
                            'auto' => __('Auto','bb-njba'),
                            'full_width' => __('Full Width','bb-njba'),
                            'custom' => __('Custom','bb-njba')
                        ),
                        'toggle' => array(
                            'auto' => array(
                                'fields' => array('button_alignment')
                            ),
                            'full_width' => array(
                                'fields' => array('')
                            ),
                            'custom' => array(
                                'fields' => array('button_custom_width','button_custom_height','button_alignment')
                            )
                        )
                    ),
                    'button_custom_width' => array(
                        'type' => 'text',
                        'label' => __('Custom Width','bb-njba'),
                        'default' => 200,
                        'size' => 10
                    ),
                    'button_custom_height' => array(
                        'type' => 'text',
                        'label' => __('Custom Height','bb-njba'),
                        'default' => 45,
                        'size' => 10
                    ),
                    'button_alignment' => array(
                        'type' => 'select',
                        'label' => __('Alignment','bb-njba'),
                        'default' => 'left',
                        'options' => array(
                            'left' => __('Left','bb-njba'),
                            'center' => __('Center','bb-njba'),
                            'right' => __('Right','bb-njba')
                        )   
                    )
                )
            )
        )
    ),
    'button_one_tab'       => array( // Tab
        'title'         => __('Button 1', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'button_one_section'     => array(
                'title'     => '',
                'fields'    => array(
                    'button_one_text'     => array(
                        'type'      => 'text',
                        'label'     => 'Text',
                        'default'   => __('HIRE US', 'bb-njba'),
                        'preview'       => array(
                            'type'          => 'text',
                            'selector'      => '.button-one-text-selector'
                        )
                    ),
                    
                )
            ),
            'button_one_link_section'  => array(
                'title'     =>  __('Link', 'bb-njba'), // Tab title',
                'fields'    => array(
                    'button_one_link'     => array(
                        'type'          => 'link',
                        'label'         =>  __('Link', 'bb-njba'),
                        'default'       =>  __('#', 'bb-njba'),
                        'placeholder'   => 'www.example.com',
                        'preview'       => array(
                            'type'          => 'none'
                        )
                    ),
                    'button_one_link_target'   => array(
                        'type'          => 'select',
                        'label'         =>  __('Link Target', 'bb-njba'),
                        'default'       =>  __('_self', 'bb-njba'),
                        'placeholder'   => 'www.example.com',
                        'options'   => array(
                            '_self'     =>  __('Same Window', 'bb-njba'),   
                            '_blank'    =>  __('New Window', 'bb-njba'),   
                        ),
                        'preview'   => array(
                            'type'      => 'none'
                        )
                    )
                )
            ),
            'button_one_icon_section'  => array(
                'title'     =>  __('Icon', 'bb-njba'), // Tab title',
                'fields'    => array(
                    'buttton_one_icon_select'       => array(
                    'type'          => 'select',
                    'label'         => __('Icon Type', 'bb-njba'),
                    'default'       => 'none',
                    'options'       => array(
                        'none'              => __('None', 'bb-njba'),
                        'font_icon'         => __('Icon', 'bb-njba'),
                        'custom_icon'       => __('Image', 'bb-njba')
                    ),
                    'toggle' => array(
                        'font_icon'    => array(
                            'fields'   => array('button_one_font_icon','button_one_icon_aligment'),
                            'sections' => array('button_one_icon_style_section','button_one_icon_typography'),
                        ),
                        'custom_icon'   => array(
                            'fields'  => array('button_one_custom_icon','button_one_icon_aligment'),
                            'sections' => array(''),
                        ),
                    )
                    ),
                    'button_one_font_icon'          => array(
                        'type'          => 'icon',
                        'label'         => __('Icon', 'bb-njba')
                    ),
                    'button_one_custom_icon'     => array(
                        'type'              => 'photo',
                        'label'         => __('Custom Image', 'bb-njba'),
                    ),
                    'button_one_icon_aligment'       => array(
                        'type'          => 'select',
                        'label'         => __('Icon Position', 'bb-njba'),
                        'default'       => 'left',
                        'options'       => array(
                            'left'      => __('Before Text', 'bb-njba'),
                            'right'     => __('After Text', 'bb-njba')
                        ),
                    )
                )
            ),
            'button_one_style_section' => array(
                'title' => __('Style','bb-njba'),
                'fields' => array(
                    'button_one_style'         => array(
                        'type'          => 'select',
                        'label'         => __('Style', 'bb-njba'),
                        'default'       => 'flat',
                        'class'         => 'creative_button_one_styles',
                        'options'       => array(
                            'flat'          => __('Flat', 'bb-njba'),
                            'gradient'      => __('Gradient', 'bb-njba'),
                            'transparent'   => __('Transparent', 'bb-njba'),
                            'threed'          => __('3D', 'bb-njba'),
                        ),
                        'toggle'        => array(
                            'flat'          => array(
                                'fields'        => array('button_one_background_color','button_one_box_shadow','button_one_box_shadow_color','button_one_box_shadow_hover_color'),
                                'sections' => array('button_one_transition_section')
                            ),
                            'gradient'          => array(
                                'fields'        => array('button_one_background_color')
                            ),
                            'threed'          => array(
                                'fields'        => array('button_one_background_color'),
                                'sections' => array('button_one_transition_section')
                            ),
                            'transparent' => array(
                                'fields' => array('button_one_box_shadow','button_one_box_shadow_color','button_one_box_shadow_hover_color'),
                                'sections' => array('button_one_transition_section')
                            )
                        )
                    ),
                    'button_one_background_color' => array(
                        'type' => 'color',
                        'label' => __('Background Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ff4081',
                        'preview'      => array(
                            'type'         => 'css',
                            'selector'     => '.njba-dual-btn-main a.njba-dual-btn-one',
                            'property'     => 'background'
                        )
                    ),
                    'button_one_background_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Background Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff'
                    ),
                    'button_one_text_color' => array(
                        'type' => 'color',
                        'label' => __('Text Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff',
                        'preview'      => array(
                            'type'         => 'css',
                            'selector'     => '.njba-dual-btn-main a.njba-dual-btn-one',
                            'property'     => 'color'
                        )
                    ),
                    'button_one_text_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Text Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ff4081'
                    ),
                    'button_one_box_shadow'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Box Shadow', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'left_right'          => 0,
                            'top_bottom'         => 0,
                            'blur'       => 2,
                            'spread'      => 0
                        ),
                        'options'           => array(
                            'left_right'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa fa-arrows-h'
                            ),
                            'top_bottom'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa fa-arrows-v'
                            ),
                            'blur'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa fa-circle-thin'
                            ),
                            'spread'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa fa-circle'
                            )
                            
                        )
                    ),
                    'button_one_box_shadow_color' => array(
                        'type' => 'color',
                        'label' => __('Box Shadow Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '8c8c8c'
                    ),
                    'button_one_box_shadow_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Box Shadow Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '8c8c8c'
                    ),
                    'button_one_border_style'      => array(
                        'type'      => 'select',
                        'label'     => __('Border Style', 'bb-njba'),
                        'default'   => 'solid',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'double'  => __('Double', 'bb-njba'),
                        ),
                        'toggle' => array(
                            'none' => array(
                                'fields' => array('')
                            ),
                            'solid' => array(
                                'fields' => array('button_one_border_width','button_one_border_color','button_one_border_hover_color')
                            ),
                            'dotted' => array(
                                'fields' => array('button_one_border_width','button_one_border_color','button_one_border_hover_color')
                            ),
                            'dashed' => array(
                                'fields' => array('button_one_border_width','button_one_border_color','button_one_border_hover_color')
                            ),
                            'double' => array(
                                'fields' => array('button_one_border_width','button_one_border_color','button_one_border_hover_color')
                            ),
                        )
                    ),
                    'button_one_border_width' => array(
                        'type' => 'text',
                        'label' => __('Border Width','bb-njba'),
                        'default' => '0',
                        'size' => '5',
                        'description'       => _x( 'px', 'Value unit for spacer width. Such as: "10 px"', 'bb-njba' )
                    ),
                    'button_one_border_radius'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Border Radius', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top_left'          => 0,
                            'top_right'         => 0,
                            'bottom_left'       => 0,
                            'bottom_right'      => 0
                        ),
                        'options'           => array(
                            'top_left'               => array(
                                'placeholder'       => __('Top-Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up',
                            ),
                            'top_right'            => array(
                                'placeholder'       => __('Top-Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right',
                            ),
                            'bottom_left'            => array(
                                'placeholder'       => __('Bottom-left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down',
                            ),
                            'bottom_right'            => array(
                                'placeholder'       => __('Bottom-Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left',
                            )
                        )
                    ),
                    'button_one_border_color' => array(
                        'type' => 'color',
                        'label' => __('Border Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000',
                        'preview'      => array(
                            'type'         => 'css',
                            'selector'     => '.njba-dual-btn-main a.njba-dual-btn-one',
                            'property'     => 'border-color'
                        )
                    ),
                    'button_one_border_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Border Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'button_one_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 15,
                            'right'         => 30,
                            'bottom'       => 15,
                            'left'      => 30
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    )
                )
            ),
            'button_one_icon_style_section' => array(
                'title' => __('Icon', 'bb-njba'),
                'fields' => array(
                    'button_one_icon_color' => array(
                        'type' => 'color',
                        'label' => __('Icon Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff'
                    ),
                    'button_one_icon_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Icon Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ff4081',
                        'preview'      => array(
                            'type'         => 'css',
                            'selector'     => '.njba-dual-btn-main a.njba-dual-btn-one .dual-btn-icon > i',
                            'property'     => 'color'
                        )
                    ),
                    'button_one_icon_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 0,
                            'right'         => 10,
                            'bottom'       => 0,
                            'left'      => 0
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'button_one_icon_margin'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 0,
                            'right'         => 0,
                            'bottom'       => 0,
                            'left'      => 0
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    )
                )
            ),
            'button_one_transition_section' =>array(
                'title' => __('Transition','bb-njba'),
                'fields' => array(
                    'button_one_transition' => array(
                        'type' => 'text',
                        'label' => __('Transition delay','bb-njba'),
                        'default' => 0.3,
                        'size' => '5',
                        'description' => 's'
                    )
                ) 
            )
        )
    ),
    'button_two_tab'       => array( // Tab
        'title'         => __('Button 2', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'button_two_section'     => array(
                'title'     => '',
                'fields'    => array(
                    'button_two_text'     => array(
                        'type'      => 'text',
                        'label'     => 'Text',
                        'default'   => __('CONTACT US', 'bb-njba'),
                        'preview'       => array(
                            'type'          => 'text',
                            'selector'      => '.button-two-text-selector'
                        )
                    ),
                    
                )
            ),
            'button_two_link_section'  => array(
                'title'     =>  __('Link', 'bb-njba'), // Tab title',
                'fields'    => array(
                    'button_two_link'     => array(
                        'type'          => 'link',
                        'label'         =>  __('Link', 'bb-njba'),
                        'default'       =>  __('#', 'bb-njba'),
                        'placeholder'   => 'www.example.com',
                    ),
                    'button_two_link_target'   => array(
                        'type'          => 'select',
                        'label'         =>  __('Link Target', 'bb-njba'),
                        'default'       =>  __('_self', 'bb-njba'),
                        'placeholder'   => 'www.example.com',
                        'options'   => array(
                            '_self'     =>  __('Same Window', 'bb-njba'),   
                            '_blank'    =>  __('New Window', 'bb-njba'),   
                        ),
                    )
                )
            ),
            'button_two_icon_section'  => array(
                'title'     =>  __('Icon', 'bb-njba'), // Tab title',
                'fields'    => array(
                    'buttton_two_icon_select'       => array(
                    'type'          => 'select',
                    'label'         => __('Icon Type', 'bb-njba'),
                    'default'       => 'none',
                    'options'       => array(
                        'none'              => __('None', 'bb-njba'),
                        'font_icon'         => __('Icon', 'bb-njba'),
                        'custom_icon'       => __('Image', 'bb-njba')
                    ),
                    'toggle' => array(
                        'font_icon'    => array(
                            'fields'   => array('button_two_font_icon','button_two_icon_aligment'),
                            'sections' => array('button_two_icon_style_section','button_two_icon_typography'),
                        ),
                        'custom_icon'   => array(
                            'fields'  => array('button_two_custom_icon','button_two_icon_aligment'),
                            'sections' => array(''),
                        ),
                    )
                    ),
                    'button_two_font_icon'          => array(
                        'type'          => 'icon',
                        'label'         => __('Icon', 'bb-njba')
                    ),
                    'button_two_custom_icon'     => array(
                        'type'              => 'photo',
                        'label'         => __('Custom Image', 'bb-njba'),
                    ),
                    'button_two_icon_aligment'       => array(
                        'type'          => 'select',
                        'label'         => __('Icon Position', 'bb-njba'),
                        'default'       => 'left',
                        'options'       => array(
                            'left'      => __('Before Text', 'bb-njba'),
                            'right'     => __('After Text', 'bb-njba')
                        ),
                    )
                )
            ),
            'button_two_style_section' => array(
                'title' => __('Style','bb-njba'),
                'fields' => array(
                    'button_two_style'         => array(
                        'type'          => 'select',
                        'label'         => __('Style', 'bb-njba'),
                        'default'       => 'flat',
                        'class'         => 'creative_button_two_styles',
                        'options'       => array(
                            'flat'          => __('Flat', 'bb-njba'),
                            'gradient'      => __('Gradient', 'bb-njba'),
                            'transparent'   => __('Transparent', 'bb-njba'),
                            'threed'          => __('3D', 'bb-njba'),
                        ),
                        'toggle'        => array(
                            'flat'          => array(
                                'fields'        => array('button_two_background_color','button_two_box_shadow','button_two_box_shadow_color','button_two_box_shadow_hover_color'),
                                'sections' => array('button_two_transition_section')
                            ),
                            'gradient'          => array(
                                'fields'        => array('button_two_background_color')
                            ),
                            'threed'          => array(
                                'fields'        => array('button_two_background_color'),
                                'sections' => array('button_two_transition_section')
                            ),
                            'transparent' => array(
                                'fields' => array('button_two_box_shadow','button_two_box_shadow_color','button_two_box_shadow_hover_color'),
                                'sections' => array('button_two_transition_section')
                            )
                        )
                    ),
                    'button_two_background_color' => array(
                        'type' => 'color',
                        'label' => __('Background Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff',
                        'preview'      => array(
                            'type'         => 'css',
                            'selector'     => '.njba-dual-btn-main a.njba-dual-btn-two',
                            'property'     => 'background'
                        )
                    ),
                    'button_two_background_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Background Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ff4081'
                    ),
                    'button_two_text_color' => array(
                        'type' => 'color',
                        'label' => __('Text Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ff4081',
                        'preview'      => array(
                            'type'         => 'css',
                            'selector'     => '.njba-dual-btn-main a.njba-dual-btn-two',
                            'property'     => 'color'
                        )
                    ),
                    'button_two_text_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Text Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff'
                    ),
                    'button_two_box_shadow'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Box Shadow', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'left_right'          => 0,
                            'top_bottom'         => 0,
                            'blur'       => 2,
                            'spread'      => 0
                        ),
                        'options'           => array(
                            'left_right'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa fa-arrows-h'
                            ),
                            'top_bottom'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa fa-arrows-v'
                            ),
                            'blur'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa fa-circle-thin'
                            ),
                            'spread'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa fa-circle'
                            )
                            
                        )
                    ),
                    'button_two_box_shadow_color' => array(
                        'type' => 'color',
                        'label' => __('Box Shadow Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '8c8c8c'
                    ),
                    'button_two_box_shadow_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Box Shadow Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '8c8c8c'
                    ),
                    'button_two_border_style'      => array(
                        'type'      => 'select',
                        'label'     => __('Border Style', 'bb-njba'),
                        'default'   => 'solid',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'double'  => __('Double', 'bb-njba'),
                        ),
                        'toggle' => array(
                            'none' => array(
                                'fields' => array('')
                            ),
                            'solid' => array(
                                'fields' => array('button_two_border_width','button_two_border_color','button_two_border_hover_color')
                            ),
                            'dotted' => array(
                                'fields' => array('button_two_border_width','button_two_border_color','button_two_border_hover_color')
                            ),
                            'dashed' => array(
                                'fields' => array('button_two_border_width','button_two_border_color','button_two_border_hover_color')
                            ),
                            'double' => array(
                                'fields' => array('button_two_border_width','button_two_border_color','button_two_border_hover_color')
                            ),
                        )
                    ),
                    'button_two_border_width' => array(
                        'type' => 'text',
                        'label' => __('Border Width','bb-njba'),
                        'default' => '0',
                        'size' => '5',
                        'description'       => _x( 'px', 'Value unit for spacer width. Such as: "10 px"', 'bb-njba' )
                    ),
                    'button_two_border_radius'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Border Radius', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top_left'          => 0,
                            'top_right'         => 2,
                            'bottom_left'       => 0,
                            'bottom_right'      => 2
                        ),
                        'options'           => array(
                            'top_left'               => array(
                                'placeholder'       => __('Top-Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up',
                            ),
                            'top_right'            => array(
                                'placeholder'       => __('Top-Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right',
                            ),
                            'bottom_left'            => array(
                                'placeholder'       => __('Bottom-left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down',
                            ),
                            'bottom_right'            => array(
                                'placeholder'       => __('Bottom-Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left',
                            )
                        )
                    ),
                    'button_two_border_color' => array(
                        'type' => 'color',
                        'label' => __('Border Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000',
                        'preview'      => array(
                            'type'         => 'css',
                            'selector'     => '.njba-dual-btn-main a.njba-dual-btn-two',
                            'property'     => 'border-color'
                        )
                    ),
                    'button_two_border_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Border Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'button_two_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 15,
                            'right'         => 30,
                            'bottom'       => 15,
                            'left'      => 30
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    )
                )
            ),
            'button_two_icon_style_section' => array(
                'title' => __('Icon', 'bb-njba'),
                'fields' => array(
                    'button_two_icon_color' => array(
                        'type' => 'color',
                        'label' => __('Icon Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ff4081',
                        'preview'      => array(
                            'type'         => 'css',
                            'selector'     => '.njba-dual-btn-main a.njba-dual-btn-two .dual-btn-icon > i',
                            'property'     => 'color'
                        )
                    ),
                    'button_two_icon_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Icon Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff'
                    ),
                    'button_two_icon_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 0,
                            'right'         => 0,
                            'bottom'       => 0,
                            'left'      => 10
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'button_two_icon_margin'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 0,
                            'right'         => 0,
                            'bottom'       => 0,
                            'left'      => 0
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    )
                )
            ),
            'button_two_transition_section' =>array(
                'title' => __('Transition','bb-njba'),
                'fields' => array(
                    'button_two_transition' => array(
                        'type' => 'text',
                        'label' => __('Transition delay','bb-njba'),
                        'default' => 0.3,
                        'size' => '5',
                        'description' => 's'
                    )
                ) 
            )
        )
    ),
    'divider_tab' => array(
        'title' => __('Divider','bb-njba'),
        'sections' => array(
            'divider_settings' => array(
                'title' => __('Divider Settings','bb-njba'),
                'fields' => array(
                    'select_divider' => array(
                        'type' => 'select',
                        'label' => __('Select Divider','bb-njba'),
                        'default' => 'text',
                        'options' => array(
                            'none' => __('None','bb-njba'),
                            'text' => __('Text','bb-njba'),
                            'font_icon' => __('Icon','bb-njba'),
                            'custom_icon' => __('Image','bb-njba'),
                        ),
                        'toggle' => array(
                            'none' => array(
                                'fields' => array('')
                            ),
                            'text' => array(
                                'fields' => array('divider_text','divider_text_icon_color','divider_background_color','divider_font_family','divider_font_size'),
                                'sections' => array('divider_typography','divider_style_section')
                            ),
                            'font_icon' => array(
                                'fields' => array('divider_icon','divider_text_icon_color','divider_background_color','divider_font_size'),
                                'sections' => array('divider_style_section','divider_typography')
                            ),
                            'custom_icon' => array(
                                'fields' => array('divider_custom_icon'),
                                'sections' => array('divider_style_section')
                            )
                        )
                    ),
                    'divider_text' => array(
                        'type' => 'text',
                        'label' => __('Text','bb-njba'),
                        'default' => 'OR',
                        'preview'       => array(
                            'type'          => 'text',
                            'selector'      => '.divider-text-selector'
                        )
                    ),
                    'divider_icon'          => array(
                        'type'          => 'icon',
                        'label'         => __('Icon', 'bb-njba')
                    ),
                    'divider_custom_icon'     => array(
                        'type'              => 'photo',
                        'label'         => __('Image', 'bb-njba'),
                    )
                )
            ),
            'divider_style_section' => array(
                'title' => __('Divider Style','bb-njba'),
                'fields' => array(
                    'divider_text_icon_color' => array( 
                        'type'       => 'color',
                        'label'         => __('Text / Icon Color', 'bb-njba'),
                        'default'    => 'ff4081',
                        'show_reset' => true,
                    ),
                    'divider_background_color' => array( 
                        'type'       => 'color',
                        'label'         => __('Background Color', 'bb-njba'),
                        'default'    => 'ffffff',
                        'show_reset' => true,
                    ),
                    'divider_border_style'      => array(
                        'type'      => 'select',
                        'label'     => __('Border Style', 'bb-njba'),
                        'default'   => 'solid',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'double'  => __('Double', 'bb-njba'),
                        ),
                        'toggle' => array(
                            'none' => array(
                                'fields' => array('')
                            ),
                            'solid' => array(
                                'fields' => array('divider_border_width','divider_border_radius','divider_border_color','divider_border_hover_color')
                            ),
                            'dotted' => array(
                                'fields' => array('divider_border_width','divider_border_radius','divider_border_color','divider_border_hover_color')
                            ),
                            'dashed' => array(
                                'fields' => array('divider_border_width','divider_border_radius','divider_border_color','divider_border_hover_color')
                            ),
                            'double' => array(
                                'fields' => array('divider_border_width','divider_border_radius','divider_border_color','divider_border_hover_color')
                            ),
                        )
                    ),
                    'divider_border_width' => array(
                        'type' => 'text',
                        'label' => __('Border Width','bb-njba'),
                        'default' => '1',
                        'size' => '5',
                        'description'       => _x( 'px', 'Value unit for spacer width. Such as: "10 px"', 'bb-njba' )
                    ),
                    'divider_border_radius' => array(
                        'type' => 'text',
                        'label' => __('Border Radius','bb-njba'),
                        'default' => '100',
                        'size' => '5',
                        'description'       => _x( 'px', 'Value unit for spacer width. Such as: "10 px"', 'bb-njba' )
                    ),
                    'divider_border_color' => array(
                        'type' => 'color',
                        'label' => __('Border Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000',
                        'preview'      => array(
                            'type'         => 'css',
                            'selector'     => '.njba-dual-btn-divider-yes',
                            'property'     => 'border-color'
                        )
                    ),
                    'divider_custom_width' => array(
                        'type' => 'text',
                        'label' => __('Custom Width','bb-njba'),
                        'default' => 30,
                        'size' => 10
                    ),
                    'divider_custom_height' => array(
                        'type' => 'text',
                        'label' => __('Custom Height','bb-njba'),
                        'default' => 30,
                        'size' => 10
                    )
                )
            )
        )
    ),
    'typography_tab' => array(
        'title' => __('Typography','bb-njba'),
        'sections' => array(
            'button_one_typography' => array(
                'title' => __('Button 1','bb-njba'),
                'fields' => array(
                    'button_one_font_family' => array(
                        'type' => 'font',
                        'label' => __('Font Family','bb-njba'),
                        'default' => array(
                            'family' => 'Default',
                            'weight' => 'Default'
                        ),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.njba-dual-btn-main a.njba-dual-btn-one'
                        )
                    ),
                    'button_one_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '20',
                            'medium' => '16',
                            'small' => ''
                        )
                    )
                )
            ),
            'button_one_icon_typography' => array(
                'title' => __('Icon','bb-njba'),
                'fields' => array(
                    'button_one_icon_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '20',
                            'medium' => '16',
                            'small' => ''
                        )
                    )
                )
            ),
            'button_two_typography' => array(
                'title' => __('Button 2','bb-njba'),
                'fields' => array(
                    'button_two_font_family' => array(
                        'type' => 'font',
                        'label' => __('Font Family','bb-njba'),
                        'default' => array(
                            'family' => 'Default',
                            'weight' => 'Default'
                        ),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.njba-dual-btn-main a.njba-dual-btn-two'
                        )
                    ),
                    'button_two_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '20',
                            'medium' => '16',
                            'small' => ''
                        )
                    )
                )
            ),
            'button_two_icon_typography' => array(
                'title' => __('Icon','bb-njba'),
                'fields' => array(
                    'button_two_icon_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '20',
                            'medium' => '16',
                            'small' => ''
                        )
                    )
                )
            ),
            'divider_typography' => array(
                'title' => __('Divider','bb-njba'),
                'fields' => array(
                    'divider_font_family' => array(
                        'type' => 'font',
                        'label' => __('Font Family','bb-njba'),
                        'default' => array(
                            'family' => 'Default',
                            'weight' => 'Default'
                        ),
                    ),
                    'divider_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '14',
                            'medium' => '12',
                            'small' => ''
                        )
                    )
                )
            )      
        )
    )
));