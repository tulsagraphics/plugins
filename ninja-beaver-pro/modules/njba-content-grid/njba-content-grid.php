<?php
/**
 * @class NJBAContentGridModule
 */
class NJBAContentGridModule extends FLBuilderModule {
    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Content Grid', 'bb-njba'),
            'description'   => __('Addon to display post.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'content' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-content-grid/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-content-grid/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
            'partial_refresh' => true
        ));
        /**
         * Use these methods to enqueue css and js already
         * registered or to register and enqueue your own.
         */
        // Already registered
        $this->add_css('jquery-bxslider');
		$this->add_css('font-awesome');
		$this->add_js('jquery-bxslider');
		$this->add_js('njba-content-grid-masonary', NJBA_MODULE_URL . 'modules/njba-content-grid/js/jquery-masonary.js');
        $this->add_js( 'jquery-imagesloaded');
        add_action( 'wp_ajax_ct_get_post_tax', array( $this, 'get_post_taxonomies' ) );
        add_action( 'wp_ajax_nopriv_ct_get_post_tax', array( $this, 'get_post_taxonomies' ) );
    }
    /**
     * @method enqueue_scripts
     */
    public function enqueue_scripts()
    {
        //$this->add_js( 'jquery-imagesloaded', $this->url . 'js/jquery.imagesloaded.js', array('jquery'), rand(), false );
       
        if(FLBuilderModel::is_builder_active() || $this->settings->pagination == 'scroll') {
            $this->add_js('jquery-infinitescroll');
        }
        
    }
    /**
     * Get taxonomies
     */
  
    public function get_post_taxonomies()
    {
        $slug = sanitize_text_field( $_POST['post_type_slug'] );
       // print_r($slug);
        $taxonomies = FLBuilderLoop::taxonomies($slug);
        $html = '';
        $html .= '<option value="none">'. __('None', 'bb-njba') .'</option>';
        foreach ( $taxonomies as $tax_slug => $tax ) {
            $html .= '<option value="'.$tax_slug.'">'.$tax->label.'</option>';
        }
        echo $html;
    }
    /**
     * Renders the schema structured data for the current
     * post in the loop.
     *
     * @since 1.7.4
     * @return void
     */
    static public function schema_meta()
    {
        // General Schema Meta
        echo '<meta itemscope itemprop="mainEntityOfPage" itemid="' . get_permalink() . '" />';
        echo '<meta itemprop="datePublished" content="' . get_the_time('Y-m-d') . '" />';
        echo '<meta itemprop="dateModified" content="' . get_the_modified_date('Y-m-d') . '" />';
        // Publisher Schema Meta
        echo '<div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">';
        echo '<meta itemprop="name" content="' . get_bloginfo( 'name' ) . '">';
        if ( class_exists( 'FLTheme' ) && 'image' == FLTheme::get_setting( 'fl-logo-type' ) ) {
            echo '<div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">';
            echo '<meta itemprop="url" content="' . FLTheme::get_setting( 'fl-logo-image' ) . '">';
            echo '</div>';
        }
        echo '</div>';
        // Author Schema Meta
        echo '<div itemscope itemprop="author" itemtype="http://schema.org/Person">';
        echo '<meta itemprop="url" content="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '" />';
        echo '<meta itemprop="name" content="' . get_the_author_meta( 'display_name', get_the_author_meta( 'ID' ) ) . '" />';
        echo '</div>';
        // Image Schema Meta
        if(has_post_thumbnail()) {
            $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full');
            if ( is_array( $image ) ) {
                echo '<div itemscope itemprop="image" itemtype="http://schema.org/ImageObject">';
                echo '<meta itemprop="url" content="' . $image[0] . '" />';
                echo '<meta itemprop="width" content="' . $image[1] . '" />';
                echo '<meta itemprop="height" content="' . $image[2] . '" />';
                echo '</div>';
            }
        }
        // Comment Schema Meta
        echo '<div itemprop="interactionStatistic" itemscope itemtype="http://schema.org/InteractionCounter">';
        echo '<meta itemprop="interactionType" content="http://schema.org/CommentAction" />';
        echo '<meta itemprop="userInteractionCount" content="' . wp_count_comments(get_the_ID())->approved . '" />';
        echo '</div>';
    }
    /**
     * Renders the schema itemtype for the current
     * post in the loop.
     *
     * @since 1.7.4
     * @return void
     */
    static public function schema_itemtype()
    {
        global $post;
        if ( ! is_object( $post ) || ! isset( $post->post_type ) || 'post' != $post->post_type ) {
            echo 'http://schema.org/CreativeWork';
        }
        else {
            echo 'http://schema.org/BlogPosting';
        }
    }
    // For Post Image
    public function post_image_render() {
            if($this->settings->show_image == "1"):
                 if ( has_post_thumbnail() ) :
                      //the_post_thumbnail($settings->image_size);
                       //$image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'large'); 
                    echo '<div class="njba-content-grid-image" itemscope="" itemprop="image" itemtype="http://schema.org/ImageObject">';
                        the_post_thumbnail($this->settings->image_size);
                    echo '</div>';     
                else:
                     echo '<div class="njba-content-grid-image" itemscope="" itemprop="image" itemtype="http://schema.org/ImageObject">
                         <img src="'.NJBA_MODULE_URL . 'modules/njba-content-grid/images/placeholder.jpg" class="njba-image-responsive" />';
                      echo '</div>';
                endif; 
             endif;
    }
    // For Meta

    public function content_meta($id) {
        $post_id = $id;
        echo '<ul>';

            if($this->settings->show_author == '1') :
                    echo '<li><a href="'.get_author_posts_url( get_the_author_meta($post_id)).'" itemscope="" itemprop="author" itemtype="http://schema.org/Person">'.get_the_author_meta( 'display_name', get_the_author_meta( $post_id ) ).'</a> <span>|</span> </li>';
            endif;
            if($this->settings->show_date == '1') :
                             
                    echo '<li>'; 
                        FLBuilderLoop::post_date($this->settings->date_format);
                    echo '<span>|</span> </li>';
            endif;
            if($this->settings->show_post_taxonomies == '1' && $this->settings->post_taxonomies != 'none' && $this->settings->post_grid_style_select != 'style-4') 
            {
                    $terms = wp_get_post_terms( get_the_ID(), $this->settings->post_taxonomies );
                    $show_terms = array();
                    $term_id = array();
                    foreach ( $terms as $term ) {
                        $show_terms[] = $term->name;
                         $term_id[] = $term->term_id;
                    }
                    if(!empty($term->term_id) && !empty($show_terms) ){
                        $count_id = count( $term_id);
                        for($i=0; $i<= $count_id; $i++){
                            if($i<$count_id){
                                 echo '<li><a href="'.esc_url( get_category_link( $term_id[$i] ) ).'">'.$show_terms[$i].'</a><span>,</span></li>';  
                            }
                        }
                    }
            } 
        echo '</ul>';
       
    }
    // for Excerpt Text
    public function excerpt_text(){
        if($this->settings->show_content) :  
             if($this->settings->content_type =='excerpt'){
                 the_excerpt(); 
            }
            else if($this->settings->content_type =='full'){
                                                            
                    echo '<p>'.get_the_content().'</p>';
            }
            else
            {
                     $more = '';
                    echo '<p>'.wp_trim_words( get_the_content(), $this->settings->content_length,  $more ).'...</p>';
             
            }
         endif; 
    } 
    // for Button Render
    public function button_render(){
        if($this->settings->show_more_link=='1'){
        $btn_settings = array(
            //Button text         
            'button_text'   => $this->settings->more_link_text,
              //Button Link
             'link'           => get_the_permalink(),
                                               
         );
          FLBuilder::render_module_html('njba-button', $btn_settings);
        }
    } 
    // for category Button Render
    public function category_button_render(){
         if($this->settings->show_post_taxonomies=='1' && $this->settings->post_taxonomies != 'none'){
           
            $terms = wp_get_post_terms( get_the_ID(), $this->settings->post_taxonomies );
            $show_terms = array();
            $term_id = array();
            foreach ( $terms as $term ) {
                $show_terms[] = $term->name;
                 $term_id[] = $term->term_id;
            }
            if(!empty($term->term_id) && !empty($show_terms) ){
                $count_id = count( $term_id);
                for($i=0; $i<= $count_id; $i++){
                    if($i<$count_id){
                       $btn_settings = array(
                             //category Button text 
                            'button_text'      => $show_terms[$i],
                             //category Button Link
                              'link'           => get_category_link( $term_id[$i] ),
                        );
                        FLBuilder::render_module_html('njba-button', $btn_settings);
                    }
                }
            }
        }
    } 
    /**
     * @method render_masonary_filters
     */
    public function render_masonary_filters() {
        $post_type = ( isset( $this->settings->post_type ) ) ? $this->settings->post_type : 'post';
        // Get taxonomies for given custom/default post type
        $taxonomies = get_object_taxonomies( $post_type, 'objects' );
        $data       = array();
        foreach($taxonomies as $tax_slug => $tax) {
            if(!$tax->public || !$tax->show_ui) {
                continue;
            }
            $data[$tax_slug] = $tax;
        }
        $taxonomies = $data;
        $cat = 'masonary_filter_' . $post_type;
        // Parse the categories
        if( isset( $this->settings->$cat ) ) {
            if( $this->settings->$cat != -1 ) {
                foreach($taxonomies as $tax_slug => $tax) {
                    $tax_value = '';
                    if( $this->settings->$cat == $tax_slug ) {
                        // New settings slug.
                        if(isset($this->settings->{'tax_' . $post_type . '_' . $tax_slug})) {
                            $tax_value = $this->settings->{'tax_' . $post_type . '_' . $tax_slug};
                        }
                        // Legacy settings slug.
                        else if(isset($this->settings->{'tax_' . $tax_slug})) {
                            $tax_value = $this->settings->{'tax_' . $tax_slug};
                        }
                        break;
                    }
                }
                $tax_value = ( $tax_value != '' ) ? explode( ',', $tax_value ) : array();
                $object_taxonomies = get_object_taxonomies( $post_type );
                if( !empty( $object_taxonomies ) ) {
                    $category_detail = get_terms( $this->settings->$cat );
                    $all_label			= empty( $this->settings->all_filter_label ) ? __('All', 'bb-njba') : $this->settings->all_filter_label;
                    if( count( $category_detail ) > 0 ) {
                        echo '<div class="njba-masonary-filters-wrapper">
                            <ul class="njba-masonary-filters">';
                        echo '<li class="njba-masonary-filter njba-masonary-current" data-filter="*">' . $all_label . '</li>';
                        foreach( $category_detail as $cat_details ){
                            if( !empty( $tax_value ) ) {
                                if( in_array( $cat_details->term_id, $tax_value ) ) {
                                    echo '<li class="njba-masonary-filter" data-filter=".'. $cat_details->taxonomy .'-' . $cat_details->slug . '">' . $cat_details->name . '</li>';
                                }
                            } else {
                                echo '<li class="njba-masonary-filter" data-filter=".category-' . $cat_details->slug . '">' . $cat_details->name . '</li>';
                            }
                                
                        }
                        echo '</ul>
                        </div>';
                    }
                }
            }
        }   
    }
}
/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('NJBAContentGridModule', array(
		'general'      => array( // Tab
        'title'         => __('Layout', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'layout'       => array( // Section
                'title'         => __('', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'layout'        => array(
                        'type'          => 'select',
                        'label'         => __('Layout Type', 'bb-njba'),
                        'default'       => 'grid',
                        'options'       => array(
                            'grid'          => __('Grid', 'bb-njba'),
                            'masonry'       => __('Masonry ', 'bb-njba'),
                            'carousel'          => __('Carousel', 'bb-njba'),
                            'flipbox'          => __('Flip Box', 'bb-njba'),
                        ),
                        'toggle'        => array(
                            'grid'          => array(
                                'sections'      => array('grid', 'image', 'content'),
                                'fields'        => array('show_col','show_author', 'pagination'),
                                'tabs'          => array( 'style' )
                            ),
                            'carousel'          => array(
                                 'tabs'          => array('carousel' ,'style'),
                                 'sections'      => array('content'),
                                /* 'fields'        => array('content_side')*/
                                
                            ),
                            'masonry'          => array(
                                'sections'      => array('grid', 'image', 'content','masonary_filter','filter_setting'),
                                'fields'        => array('show_col','show_author', 'pagination','all_filter_label'),
                                'tabs'          => array( 'style' )
                            ),
                            'flipbox'          => array(
                                'sections'      => array('grid', 'image', 'content'),
                                'fields'        => array('show_col','show_author', 'pagination'),
                                'tabs'          => array( 'flipbox_style' )
                            ),
                        )
                    ),
                   'post_grid_style_select'    => array(
                        'type'      => 'select',
                        'label'     => __('Select Style', 'bb-njba'),
                        'default'   => 'style-1',
                        'options'   => array(
							'style-1'  => __('Style 1', 'bb-njba'),
                            'style-2'  => __('Style 2', 'bb-njba'),
							'style-3'  => __('Style 3', 'bb-njba'),
                            'style-4'  => __('Style 4', 'bb-njba'),
                            'style-5'  => __('Style 5', 'bb-njba'),
                            'style-6'  => __('Style 6', 'bb-njba'),
                            'style-7'  => __('Style 7', 'bb-njba'),
							
                        ),
                        'toggle'        => array(
                            'style-1'          => array(
                                'fields'        => array('overly_color','overly_color_opacity','back_text_color'),
                                'sections'      => array('back_styles'),
                            ),
                            'style-3'          => array(
                                'fields'        => array('overly_color','overly_color_opacity'),
                                'sections'      => array(),
                            ),
                            'style-4'          => array(
                                'fields'        => array('overly_color','overly_color_opacity'),
                                'sections'      => array(),
                            ),
                            'style-5'          => array(
                                'fields'        => array('overly_color','overly_color_opacity','front_title_color','front_title_bc_color','front_title_bc_color_opc','front_date_color','front_date_bc_color','front_date_bc_color_opc'),
                                'sections'      => array('front_styles'),
                            ),
                            'style-6'          => array(
                                'fields'        => array('overly_color','overly_color_opacity','front_main_bc_color','front_main_bc_color_opc','front_main_overly_bc_color','front_main_overly_color_opc','front_border_color','front_title_color','front_date_color'),
                                'sections'      => array('front_styles'),
                            ),
                            'style-7'          => array(
                                'fields'        => array('front_main_overly_bc_color','front_main_overly_color_opc','front_title_color','back_gradient_bc_color_one','back_gradient_bc_color_two'),
                                'sections'      => array('front_styles','back_styles'),
                            ),
                            
                        )
						
                    ),
                    /*'post_masonry_layout_mode'    => array(
                        'type'      => 'select',
                        'label'     => __('Layout modes', 'bb-njba'),
                        'default'   => 'masonry',
                        'options'   => array(
                           'fitRows'                => __('Fit Rows', 'bb-njba'),
                           'vertical'               => __('Vertical', 'bb-njba'),
                             'masonry'                => __('Masonry', 'bb-njba'),
                        )
                    ),*/
                    'show_col'         => array(
                        'type'          => 'select',
                        'label'         => __('Show on Row', 'bb-njba'),
                        'default'       => 3,
                        'options'        => array(
                            '12'      => '1',
                            '6'       => '2',
                            '4'       => '3',
                            '3'       => '4',
                            '5'       => '5',
                            '2'       => '6',
                        ),
                    ),
                   
                    'overly_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Hover Background', 'bb-njba'),
                        'show_reset'    => true,
                                
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-pl-gst-style-3 .njba-pl-mask',
                            'property'      => 'background-color',
                        )
                    ),
                    'overly_color_opacity'    => array(
                        'type'          => 'text',
                        'label'         => __('Opacity', 'bb-njba'),
                        'default'       => '',
                        'maxlength'     => '3',
                        'size'          => '5', 
                        'description'       => '%', 
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-pl-gst-style-3 .njba-pl-mask',
                            'property'      => 'background-color',
                        )
                    ),
                  
                    'pagination'     => array(
                        'type'          => 'select',
                        'label'         => __('Pagination Style', 'bb-njba'),
                        'default'       => 'numbers',
                        'options'       => array(
                            'numbers'       => __('Numbers', 'bb-njba'),
                            /*'scroll'        => __('Scroll', 'bb-njba'),*/
                            'none'          => _x( 'None', 'Pagination style.', 'bb-njba' ),
                        ),
                        'toggle'        => array(
                            'numbers'          => array(
                                'tabs'        => array('pagination')
                            ),
                            'none'          => array(
                                'tabs'        => array()
                            ),
                           /* 'scroll'          => array(
                                'tabs'        => array(),
                                'fields'        => array('scroll_img'),
                            ),*/
                            
                        )
                    ),
                    'scroll_img'     => array(
                        'type'          => 'photo',
                        'label'         => __('Scroll Image', 'bb-njba'),
                        'show_remove'   => true
                    ),
                    'total_post'  => array(
                        'type'          => 'select',
                        'label'         => __('Total Posts', 'bb-njba'),
                        'default'       => 'all',
                        'options'       => array(
                            'all'          => __('All', 'bb-njba'),
                            'custom'         => __('Custom', 'bb-njba'),
                        ),
                        'toggle'    => array(
                            'custom'    => array(
                                'fields'    => array('total_posts_count')
                            )
                        )
                    ),
                    'total_posts_count' => array(
                        'type'          => 'text',
                        'label'         => __('Posts Count', 'bb-njba'),
                        'default'       => '20',
                        'size'          => '4',
                    ),
                    'posts_per_page' => array(
                        'type'          => 'text',
                        'label'         => __('Posts Per Page', 'bb-njba'),
                        'default'       => '6',
                        'size'          => '4'
                    ),
                    'no_results_message' => array(
                        'type'              => 'text',
                        'label'             => __('No Results Message', 'bb-njba'),
                        'default'           => __('No Posts Found.', 'bb-njba')
                    )
               )
            ),
            'grid'       => array( // Section
                'title'         => __('Grid', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    
                    'post_spacing'  => array(
                        'type'          => 'text',
                        'label'         => __('Post Spacing', 'bb-njba'),
                        'default'       => '20',
                        'maxlength'     => '3',
                        'size'          => '4',
                        'description'   => 'px'
                    ),
                 )
            ),
            'image'        => array(
                'title'         => __( 'Featured Image', 'bb-njba' ),
                'fields'        => array(
                    'show_image'    => array(
                        'type'          => 'select',
                        'label'         => __('Image', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '1'             => __('Show', 'bb-njba'),
                            '0'             => __('Hide', 'bb-njba')
                        ),
                        'toggle'        => array(
                            '1'             => array(
                                'fields'        => array('image_size')
                            )
                        )
                    ),
                  
                    'image_size'    => array(
                        'type'          => 'photo-sizes',
                        'label'         => __('Size', 'bb-njba'),
                        'default'       => 'medium'
                    ),
                )
            ),
            'content'       => array(
                'title'         => __( 'Content', 'bb-njba' ),
                'fields'        => array(
                    'show_content'  => array(
                        'type'          => 'select',
                        'label'         => __('Content', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '1'             => __('Show', 'bb-njba'),
                            '0'             => __('Hide', 'bb-njba')
                        ),
                        'toggle'        => array(
                            '1'             => array(
                                'fields'          => array('content_type','content_length')
                            ),
                            '0'             => array(
                                'fields'          => array()
                            )
                        )
                    ),
                    'content_type'  => array(
                        'type'          => 'select',
                        'label'         => __('Content Type', 'bb-njba'),
                        'default'       => 'excerpt',
                        'options'       => array(
                            'excerpt'        => __('Excerpt', 'bb-njba'),
                            'full'           => __('Full Text', 'bb-njba'),
                            'custom_length'  => __('Custom', 'bb-njba')
                        ),
                        'toggle'        => array(
                            'custom_length'             => array(
                                'fields'          => array('content_length')
                            )
                        )
                    ),
                    'content_length' => array(
                        'type'      => 'text',
                        'label'     => __('Content Limit', 'bb-njba'),
                        'help'      => __('Number of words to be displayed from the post content.', 'bb-njba'),
                        'default'   => '50',
                        'maxlenght' => 4,
                        'size'      => 5,
                        'description' => __('words', 'bb-njba'),
                    ),
                    'show_more_link' => array(
                        'type'          => 'select',
                        'label'         => __('More Link', 'bb-njba'),
                        'default'       => '0',
                        'options'       => array(
                            '1'             => __('Show', 'bb-njba'),
                            '0'             => __('Hide', 'bb-njba')
                        ),
                        'toggle'        => array(
                            '1'             => array(
                                'fields'          => array('more_link_text')
                            ),
                            '0'             => array(
                                'fields'          => array()
                            ),
                        )
                    ),
                    'more_link_text' => array(
                        'type'          => 'text',
                        'label'         => __('More Link Text', 'bb-njba'),
                        'default'       => __('More', 'bb-njba'),
                    ),
                )
            ),
            'front_styles'       => array( // Section
                'title'         => __('Front Styles', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'front_main_bc_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('Background Color', 'bb-njba'),
                        'default'    => '000000',
                        'show_reset' => true,
                    ),
                    'front_main_bc_color_opc'    => array( 
                        'type'        => 'text',
                        'label'       => __('Background Opacity', 'bb-njba'),
                        'default'     => '1',
                        'maxlength'   => '3',
                        'size'        => '50',
                        'description'   => '0 to 1'
                    ),
                    'front_main_overly_bc_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('Background Overly Color', 'bb-njba'),
                        'default'    => '000000',
                        'show_reset' => true,
                    ),
                    'front_main_overly_color_opc'    => array( 
                        'type'        => 'text',
                        'label'       => __('Background Overly Opacity', 'bb-njba'),
                        'default'     => '50',
                        'maxlength'   => '3',
                        'size'        => '5',
                        'description'   => '0 to 1'
                    ),
                    'front_border_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('Border Color', 'bb-njba'),
                        'default'    => 'dbdbdb',
                        'show_reset' => true,
                    ),
                    'front_title_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('Title Text Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    'front_title_bc_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('Title Background Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    'front_title_bc_color_opc'    => array( 
                        'type'        => 'text',
                        'label'       => __('Title Background Opacity', 'bb-njba'),
                        'default'     => '1',
                        'maxlength'   => '3',
                        'size'        => '5',
                        'description'   => '0 to 1'
                    ),
                    'front_date_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('Date Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    'front_date_bc_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('Date Background Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    'front_date_bc_color_opc'    => array( 
                        'type'        => 'text',
                        'label'       => __('Date Background Opacity', 'bb-njba'),
                        'default'     => '1',
                        'maxlength'   => '3',
                        'size'        => '5',
                        'description'   => '0 to 1'
                    ),
                )
            ),
            'back_styles'       => array( // Section
                'title'         => __('Back Styles', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'back_gradient_bc_color_one'    => array( 
                        'type'       => 'color',
                        'label'      => __('Gradient Color 1 ', 'bb-njba'),
                        'default'    => '000000',
                        'show_reset' => true,
                    ),
                    'back_gradient_bc_color_two'    => array( 
                        'type'       => 'color',
                        'label'      => __('Gradient Color 2 ', 'bb-njba'),
                        'default'    => '000000',
                        'show_reset' => true,
                    ),
                    'back_text_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('Text Color ', 'bb-njba'),
                        'default'    => '000000',
                        'show_reset' => true,
                    ),
                    
                )
            ),
        )
    ),
	'content'   => array(
		'title'         => __('Content', 'bb-njba'),
		'file'          => plugin_dir_path( __FILE__ ) . 'includes/loop-settings.php',
	),
    'flipbox_style'       => array( // Tab
        'title'         => __('FlipBox Styles', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('Flipbox Styles', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'flip_type'   => array(
                        'type'          => 'select',
                        'label'         => __('Flip Type', 'bb-njba'),
                        'default'       => 'horizontal_flip_left',
                        'options'       => array(
                            'horizontal_flip_left'      => __('Flip Horizontally From Left', 'bb-njba'),
                            'horizontal_flip_right'      => __('Flip Horizontally From Right', 'bb-njba'),
                            'vertical_flip_top'      => __('Flip Vertically From Top', 'bb-njba'),
                            'vertical_flip_bottom'      => __('Flip Vertically From Bottom', 'bb-njba'),
                        )
                    ),
                    'inner_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 10,
                            'right'         => 10,
                            'bottom'       => 10,
                            'left'      => 10
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'box_border_radius'    => array( 
                        'type'        => 'text',
                        'label'       => __('Box Border Radius', 'bb-njba'),
                        'default'     => '0',
                        'maxlength'   => '3',
                        'size'        => '5'
                    ),
                )
            ),
            
        )
    ),
    'carousel'      => array( // Tab
        'title'         => __('Carousel', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'heading'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array(
                    'autoplay'         => array(
                        'type'          => 'select',
                        'label'         => __('Autoplay', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba')
                        ),
                    ),
                    'hover_pause'         => array(
                        'type'          => 'select',
                        'label'         => __('Pause on hover', 'bb-njba'),
                        'default'       => '1',
                        'help'          => __('Pause when mouse hovers over slider'),
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba'),
                        ),
                    ),
                    'transition'    => array(
                        'type'          => 'select',
                        'label'         => __('Mode', 'bb-njba'),
                        'default'       => 'horizontal',
                        'options'       => array(
                            'horizontal'    => _x( 'Horizontal', 'Transition type.', 'bb-njba' ),
                            'vertical'    => _x( 'Vertical', 'Transition type.', 'bb-njba' ),
                            'fade'          => __( 'Fade', 'bb-njba' )
                        ),
                    ),
                    'pause'         => array(
                        'type'          => 'text',
                        'label'         => __('Delay', 'bb-njba'),
                        'default'       => '4',
                        'maxlength'     => '4',
                        'size'          => '5',
                        'description'   => _x( 'seconds', 'Value unit for form field of time in seconds. Such as: "5 seconds"', 'bb-njba' )
                    ),
                    'speed'         => array(
                        'type'          => 'text',
                        'label'         => __('Transition Speed', 'bb-njba'),
                        'default'       => '0.5',
                        'maxlength'     => '4',
                        'size'          => '5',
                        'description'   => _x( 'seconds', 'Value unit for form field of time in seconds. Such as: "5 seconds"', 'bb-njba' )
                    ),
                    'loop'         => array(
                        'type'          => 'select',
                        'label'         => __('Loop', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba'),
                        ),
                    ),
                    'adaptive_height'   => array(
                        'type'              => 'select',
                        'label'             => __('Fixed Height', 'bb-njba'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'bb-njba'),
                            'no'                => __('No', 'bb-njba')
                        ),
                        'help'              => __('Fix height to the tallest item.', 'bb-njba')
                    )
                    
                )
            ),
            'carousel_section'       => array( // Section
                'title'         => '',
                'fields'        => array( // Section Fields
                    'max_slides'         => array(
                        'type'          => 'njba-simplify',
                        'label'         => __('Maximum Slides'),
                        'default'       => array(
                                    'desktop' => '3',
                                    'medium'  => '2',
                                    'small'   => '1',
                        ),
                        'size'          => '5', 
                    ),
                     'slide_margin'         => array(
                        'type'          => 'njba-simplify',
                        'label'         => __('Slides Margin ', 'bb-njba'),
                        'default'       => array(
                                    'desktop' => '5',
                                    'medium'  => '5',
                                    'small'   => '5',
                        ),
                        'size'          => '5', 
                    ),
                )
            ),
            'arrow_nav'       => array( // Section
                'title'         => '',
                'fields'        => array( // Section Fields
                    'arrows'       => array(
                        'type'          => 'select',
                        'label'         => __('Show Arrows', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba')
                        ),
                        'toggle'        => array(
                            '1'         => array(
                                'fields'        => array('arrows_size', 'arrow_background', 'arrow_color','arrow_border_width','arrow_border_style','arrow_border_color','arrow_border_color','arrow_border_radius')
                            )
                        )
                    ),
                    
                    'arrow_background'       => array(
                        'type'          => 'color',
                        'label'         => __('Arrow Background', 'bb-njba'),
                        
                        'show_reset'    => true,
                    ),
                    'arrow_color'       => array(
                        'type'          => 'color',
                        'label'         => __('Arrow Color', 'bb-njba'),
                        
                        'show_reset'    => true,
                    ),
               )
            ),
            'dot_nav'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array( // Section Fields
                    'dots'       => array(
                        'type'          => 'select',
                        'label'         => __('Show Dots', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba'),
                        ),
                        'toggle'        => array(
                            '1'         => array(
                                'fields'        => array('dot_color', 'active_dot_color')
                            )
                        )
                    ),
                    'dot_color'       => array(
                        'type'          => 'color',
                        'label'         => __('Dot Color', 'bb-njba'),
                        
                        'show_reset'    => true,
                    ),
                    'active_dot_color'       => array(
                        'type'          => 'color',
                        'label'         => __('Active Dot Color', 'bb-njba'),
                       
                        'show_reset'    => true,
                    ),
                )
            )
            
        )
    ),
	
	'style'	=> array(
		'title'			=> __('Style', 'bb-njba'),
		'sections'		=> array(
			'col_setting'	=> array(
				'title'				=> __('Column Settings', 'bb-njba'),
				'fields'			=> array(
					'col_bg_color' => array(
                        'type' => 'color',
                        'label' => __('Background Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '',
                        'preview'      => array(
                            'type'         => 'css',
                            'selector'     => '.njba-content-grid',
                            'property'     => 'background'
                        )
                    ),
                    'col_border_style'      => array(
                        'type'      => 'select',
                        'label'     => __('Border Style', 'bb-njba'),
                        'default'   => 'none',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'double'  => __('Double', 'bb-njba'),
                        ),
                        'toggle' => array(
                            'solid' => array(
                                'fields' => array('col_border_width','col_border_color','col_border_hover_color','col_border_radius','col_box_shadow_color','col_box_shadow','col_box_shadow_hover_color')
                            ),
                            'dotted' => array(
                                'fields' => array('col_border_width','col_border_color','col_border_hover_color','col_border_radius','col_box_shadow_color','col_box_shadow','col_box_shadow_hover_color')
                            ),
                            'dashed' => array(
                                'fields' => array('col_border_width','col_border_color','col_border_hover_color','col_border_radius','col_box_shadow_color','col_box_shadow','col_box_shadow_hover_color')
                            ),
                            'double' => array(
                                'fields' => array('col_border_width','col_border_color','col_border_hover_color','col_border_radius','col_box_shadow_color','col_box_shadow','col_box_shadow_hover_color')
                            ),
                        )
                    ),
                    'col_border_width' => array(
                        'type' => 'text',
                        'label' => __('Border Width','bb-njba'),
                        'default' => '1',
                        'size' => '5',
                        'description'       => _x( 'px', 'Value unit for spacer width. Such as: "10 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'      => 'css',
                            'rules' => array(
                                array(
                                    'selector'  => '.njba-<?php echo $settings->post_grid_style_select; ?>',
                                    'property'  => 'border-width',
                                )
                            )
                        )
                    ),
                    'col_border_radius'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Border Radius', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => '0',
                            'right'         => '0',
                            'bottom'       => '0',
                            'left'      => '0'
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'col_border_color' => array(
                        'type' => 'color',
                        'label' => __('Border Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '',
                        'preview'       => array(
                            'type'      => 'css',
                            'rules' => array(
                                array(
                                    'selector'  => '.njba-<?php echo $settings->post_grid_style_select; ?>',
                                    'property'  => 'border-color',
                                )
                            )
                        )
                    ),
                    'col_border_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Border Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '',
                        'preview'       => array(
                            'type'      => 'css',
                            'rules' => array(
                                array(
                                    'selector'  => '.njba-<?php echo $settings->post_grid_style_select; ?>',
                                    'property'  => 'border-color',
                                )
                            )
                        )
                    ),
                    'col_box_shadow_color' => array(
                        'type' => 'color',
                        'label' => __('Box Shadow Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '',
                        'preview'       => array(
							'type'		=> 'css',
							'rules'	=> array(
								array(
									'selector'	=> '.njba-<?php echo $settings->post_grid_style_select; ?>',
									'property'	=> 'box-shadow',
								)
							)
						)
                    ),
                    'col_box_shadow_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Box Shadow Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '',
                        'preview'       => array(
                            'type'      => 'css',
                            'rules' => array(
                                array(
                                    'selector'  => '.njba-<?php echo $settings->post_grid_style_select; ?>',
                                    'property'  => 'box-shadow',
                                )
                            )
                        )
                    ),
                    'col_box_shadow'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Box Shadow', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'left_right'          => '0',
                            'top_bottom'         => '0',
                            'blur'       => '0',
                            'spread'      => '0'
                        ),
                        'options'           => array(
                            'left_right'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa fa-arrows-h'
                            ),
                            'top_bottom'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa fa-arrows-v'
                            ),
                            'blur'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa fa-circle-thin'
                            ),
                            'spread'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa fa-circle'
                            )
                            
                        ),
                        'preview'       => array(
							'type'		=> 'css',
							'rules'	=> array(
								array(
									'selector'	=> '.njba-<?php echo $settings->post_grid_style_select; ?>',
									'property'	=> 'box-shadow',
								)
							)
						)
                    ),
                    'col_border_radius'   => array(
                        'type'      => 'text',
                        'label'     => __('Border Radius', 'bb-njba'),
                        'size'      => 5,
                        'maxlength' => 3,
                        'default'   => '',
                        'description'   => 'px',
                        'preview'       => array(
                            'type'      => 'css',
							'selector'	=>'.njba-<?php echo $settings->post_grid_style_select; ?>',
							'property'	=> 'border-radius',
							'unit'		=> 'px'
                        ),
                    ),
               )
				
			),
            'filter_setting'   => array(
                'title'             => __('Filter Settings', 'bb-njba'),
                'fields'            => array(
                    'filter_btn_default_bccolor' => array(
                        'type' => 'color',
                        'label' => __('Default Background Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '',
                        'preview'       => array(
                            'type'      => 'css',
                            'rules' => array(
                                array(
                                    'selector'  => 'ul.njba-masonary-filters > li',
                                    'property'  => 'background',
                                )
                            )
                        )
                    ),
                    'filter_btn_hover_bccolor' => array(
                        'type' => 'color',
                        'label' => __('Active Background Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '',
                        'preview'       => array(
                            'type'      => 'css',
                            'rules' => array(
                                array(
                                    'selector'  => 'ul.njba-masonary-filters > li',
                                    'property'  => 'background',
                                )
                            )
                        )
                    ),
                    'filter_default_color' => array(
                        'type' => 'color',
                        'label' => __('Default Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '',
                        'preview'       => array(
                            'type'      => 'css',
                            'rules' => array(
                                array(
                                    'selector'  => 'ul.njba-masonary-filters > li',
                                    'property'  => 'color',
                                )
                            )
                        )
                    ),
                    'filter_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Active Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '',
                        'preview'       => array(
                            'type'      => 'css',
                            'rules' => array(
                                array(
                                    'selector'  => 'ul.njba-masonary-filters > li',
                                    'property'  => 'color',
                                )
                            )
                        )
                    ),
                    'filter_button_font_family' => array(
                        'type' => 'font',
                        'label' => __('Font Family','bb-njba'),
                        'default' => array(
                            'family' => 'Default',
                            'weight' => 'Default'
                        ),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => 'ul.njba-masonary-filters li'
                        )
                    ),
                    'filter_button_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
                        'preview'         => array(
                            'type'            => 'font-size',
                            'selector'        => 'ul.njba-masonary-filters li'
                        )
                    ),
                     'filter_button_border_style'      => array(
                        'type'      => 'select',
                        'label'     => __('Border Style', 'bb-njba'),
                        'default'   => 'none',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'double'  => __('Double', 'bb-njba'),
                        ),
                        'toggle' => array(
                            'solid' => array(
                                'fields' => array('filter_button_border_width','filter_button_border_radius','filter_button_border_color','filter_button_border_hover_color')
                            ),
                            'dotted' => array(
                                'fields' => array('filter_button_border_width','filter_button_border_radius','filter_button_border_color','filter_button_border_hover_color')
                            ),
                            'dashed' => array(
                                'fields' => array('filter_button_border_width','filter_button_border_radius','filter_button_border_color','filter_button_border_hover_color')
                            ),
                            'double' => array(
                                'fields' => array('filter_button_border_width','filter_button_border_radius','filter_button_border_color','filter_button_border_hover_color')
                            ),
                        )
                    ),
                    'filter_button_border_width' => array(
                        'type' => 'text',
                        'label' => __('Border Width','bb-njba'),
                        'default' => '1',
                        'size' => '5',
                        'description'       => _x( 'px', 'Value unit for spacer width. Such as: "10 px"', 'bb-njba' )
                    ),
                    'filter_button_border_radius'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Border Radius', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top-left'          => 0,
                            'top-right'         => 0,
                            'bottom-left'       => 0,
                            'bottom-right'      => 0
                        ),
                        'options'           => array(
                             'top-left'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'top-right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom-left'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'bottom-right'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'filter_button_border_color' => array(
                        'type' => 'color',
                        'label' => __('Border Color','bb-njba'),
                        'show_reset' => true,
                        'default' => ''
                    ),
                    'filter_button_border_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Border Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => ''
                    ),
                    
                    'filter_button_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'           => '',
                            'right'         => '' ,
                            'bottom'        => '',
                            'left'          => ''
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        ),
                    ),
                    
               )
            ),
		)
	),
	'pagination'	=> array(
		'title'			=> __('Pagination', 'bb-njba'),
		'sections'		=> array(
			'pagination_style'    => array(
				'title'         => __('General', 'bb-njba'),
				'fields'        => array(
					'pagination_spacing_v'   => array(
                        'type'      => 'text',
                        'label'     => __('Spacing Top/Bottom', 'bb-njba'),
                        'size'      => 5,
                        'maxlength' => 3,
                        'default'   => '',
                        'description'   => 'px',
                        'preview'       => array(
                            'type'      => 'css',
							'rules'		=> array(
								array(
									'selector'	=>'.njba-pagination ul.page-numbers li',
									'property'	=> 'padding-top',
									'unit'		=> 'px'
								),
								array(
									'selector'	=>'.njba-pagination ul.page-numbers li',
									'property'	=> 'padding-bottom',
									'unit'		=> 'px'
								)
							)
                        ),
                    ),
					'pagination_spacing'   => array(
                        'type'      => 'text',
                        'label'     => __('Spacing Left/Right', 'bb-njba'),
                        'size'      => 5,
                        'maxlength' => 3,
                        'default'   => '',
                        'description'   => 'px',
                        'preview'       => array(
                            'type'      => 'css',
							'selector'	=>'.njba-pagination li a.page-numbers, .njba-pagination li span.page-numbers',
							'property'	=> 'margin-right',
							'unit'		=> 'px'
                        ),
                    ),
					'pagination_padding'   => array(
                        'type'      => 'njba-multinumber',
                        'label'     => __('Padding', 'bb-njba'),
                        'description'   => 'px',
						'default'       => array(
                            'top'           => '',
                            'right'         => '' ,
                            'bottom'        => '',
                            'left'          => ''
                        ),
                    	'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'pagination_font_family' => array(
                        'type' => 'font',
                        'label' => __('Font Family','bb-njba'),
                        'default' => array(
                            'family' => 'Default',
                            'weight' => 'Default'
                        ),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => '.njba-pagination li span.page-numbers'
                        )
                    ),
                    'pagination_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
                    ),
				)
			),
			'pagination_colors'	=> array(
				'title'				=> __('Colors', 'bb-njba'),
				'fields'			=> array(
					'pagi_bg_color' => array(
                        'type' => 'color',
                        'label' => __('Default Background Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '',
                        'preview'       => array(
							'type'		=> 'css',
							'selector'	=> '.njba-pagination li a.page-numbers',
							'property'	=> 'background',
						)
                    ),
                    'pagi_activebg_color' => array(
                        'type' => 'color',
                        'label' => __('Active Background Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '',
                        'preview'       => array(
							'type'		=> 'css',
							'selector'	=> '.njba-pagination li a.page-numbers:hover',
							'property'	=> 'background',
						)
                    ),
                    'pagi_color' => array(
                        'type' => 'color',
                        'label' => __('Default Text Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '',
                    ),
                    'pagi_active_color' => array(
                        'type' => 'color',
                        'label' => __('Active Text Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '',
                    ),
					
				)
			),
			'pagination_border'	=> array(
				'title'				=> __('Border', 'bb-njba'),
				'fields'			=> array(
					'pagination_border'    => array(
                        'type'      => 'select',
                        'label'     => __('Border Style', 'bb-njba'),
                        'default'   => 'none',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                        ),
                        'toggle'    => array(
                            'dashed'   => array(
                                'fields'    => array('pagination_border_width', 'pagination_border_color')
                            ),
                            'dotted'   => array(
                                'fields'    => array('pagination_border_width', 'pagination_border_color')
                            ),
                            'solid'   => array(
                                'fields'    => array('pagination_border_width', 'pagination_border_color')
                            ),
                        ),
                    ),
                    'pagination_border_width'   => array(
                        'type'      => 'text',
                        'label'     => __('Border Width', 'bb-njba'),
                        'size'      => 5,
                        'maxlength' => 3,
                        'default'   => 1,
                        'description'   => 'px',
                    ),
                    'pagination_border_color'   => array(
                        'type'      => 'color',
                        'label'     => __('Border Color', 'bb-njba'),
                        'show_reset'   => true,
						'default'		=> '',
                    ),
                    'pagination_border_radius'   => array(
                        'type'      => 'text',
                        'label'     => __('Border Radius', 'bb-njba'),
                        'size'      => 5,
                        'maxlength' => 3,
                        'default'   => 0,
                        'description'   => 'px',
                    ),
				)
			),
		)
	),
	'typography'      => array( // Tab
		'title'         => __('Typography', 'bb-njba'), // Tab title
		'sections'      => array( // Tab Sections
			'general'       => array( // Section
				'title'         =>  __('Post Title', 'bb-njba'), // Section Title
				'fields'        => array( // Section Fields
					'post_title_tag'   => array(
                        'type'          => 'select',
                        'label'         => __('Tag', 'bb-njba'),
                        'default'       => 'h1',
                        'options'       => array(
                            'h1'      => __('H1', 'bb-njba'),
                            'h2'      => __('H2', 'bb-njba'),
                            'h3'      => __('H3', 'bb-njba'),
                            'h4'      => __('H4', 'bb-njba'),
                            'h5'      => __('H5', 'bb-njba'),
                            'h6'      => __('H6', 'bb-njba'),
                            'div'     => __('Div', 'bb-njba'),
                            'p'       => __('p', 'bb-njba'),
                            'span'    => __('span', 'bb-njba'),
                        )
                    ),
                    'post_title_alignment'         => array(
						'type'                      => 'select',
						'default'                   => 'center',
						'label'                     => __('Alignment', 'bb-njba'),
                        'options'                   => array(
                            'left'                      => __('Left', 'bb-njba'),
                            'right'                     => __('Right', 'bb-njba'),
                            'center'                    => __('Center', 'bb-njba'),
                        ),
					),
                    'post_title_font'          => array(
                        'type'          => 'font',
                        'default'		=> array(
                            'family'		=> 'Default',
                            'weight'		=> 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                    ),
                   'post_title_font_size'    => array(
					   'type'          => 'njba-simplify',
                       'label'         => __( 'Font Size', 'bb-njba' ),
                       'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
						'maxlength'     => '3',
                        'size'          => '5', 
                        'description'       => 'px', 
					),
                   'post_title_height'    => array(
                        'type'          => 'njba-simplify',
                       'label'         => __( 'Line Height', 'bb-njba' ),
                       'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
                        'maxlength'     => '3',
                        'size'          => '5', 
                        'description'       => 'px', 
                     ),
                    'post_title_color'    => array(
						'type'          => 'color',
						'label'         => __('Color', 'bb-njba'),
						'default'       => '',
						'show_reset'    => true,
					),
					'post_title_hover_color'    => array(
						'type'          => 'color',
						'label'         => __('Hover Color', 'bb-njba'),
						'default'       => '',
						'show_reset'    => true,
					),
                    'post_title_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'            => '',
                            'right'          => '',
                            'bottom'        => '',
                            'left'          => ''
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-left'
                            )
                            
                        ),
                    ),
				)
			), // Section
			'dateformate'       => array( // Section
				'title'         =>  __('Meta', 'bb-njba'), // Section Title
				'fields'        => array( // Section Fields
					'post_date_alignment'         => array(
						'type'                      => 'select',
						'default'                   => 'center',
						'label'                     => __('Alignment', 'bb-njba'),
                        'options'                   => array(
                            'left'                      => __('Left', 'bb-njba'),
                            'right'                     => __('Right', 'bb-njba'),
                            'center'                    => __('Center', 'bb-njba'),
                        ),
					),
                    'post_date_font'          => array(
                        'type'          => 'font',
                        'default'		=> array(
                            'family'		=> 'Default',
                            'weight'		=> 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                    ),
                   'post_date_font_size'    => array(
					   'type'          => 'njba-simplify',
                       'label'         => __( 'Font Size', 'bb-njba' ),
                       'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
						'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
					),
                   'post_date_height'    => array(
                        'type'          => 'njba-simplify',
                       'label'         => __( 'Line Height', 'bb-njba' ),
                       'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
                        'maxlength'     => '3',
                        'size'          => '5', 
                        'description'       => 'px', 
                     ),
                    'post_date_color'    => array(
						'type'          => 'color',
						'label'         => __('Color', 'bb-njba'),
						'default'       => '',
						'show_reset'    => true,
                        
					),
                    'post_date_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'            => '',
                            'right'          => '',
                            'bottom'        => '',
                            'left'          => ''
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-left'
                            )
                            
                        ),
                    ),
					
				)
			), // Section
			'content'       => array( // Section
				'title'         =>  __('Post Content', 'bb-njba'), // Section Title
				'fields'        => array( // Section Fields
					'post_content_alignment'         => array(
						'type'                      => 'select',
						'default'                   => 'center',
						'label'                     => __('Alignment', 'bb-njba'),
                        'options'                   => array(
                            'left'                      => __('Left', 'bb-njba'),
                            'right'                     => __('Right', 'bb-njba'),
                            'center'                    => __('Center', 'bb-njba'),
                        ),
					),
                    'post_content_font'          => array(
                        'type'          => 'font',
                        'default'		=> array(
                            'family'		=> 'Default',
                            'weight'		=> 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                    ),
                   'post_content_font_size'    => array(
					   'type'          => 'njba-simplify',
                       'label'         => __( 'Font Size', 'bb-njba' ),
                       'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
                        'maxlength'     => '3',
                        'size'          => '5', 
                        'description'       => 'px',
					),
                   'post_content_height'    => array(
                       'type'          => 'njba-simplify',
                       'label'         => __( 'Line Height', 'bb-njba' ),
                       'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
                        'maxlength'     => '3',
                        'size'          => '5', 
                        'description'       => 'px', 
                     ),
                    'post_content_color'    => array(
						'type'          => 'color',
						'label'         => __('Color', 'bb-njba'),
						'default'       => '#282828',
						'show_reset'    => true,
					),
                    'post_content_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'            => '',
                            'right'          => '',
                            'bottom'        => '',
                            'left'          => ''
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa fa-long-arrow-left'
                            )
                            
                        ),
                    ),
					
				)
			), // Section
			'button'       => array( // Section
				'title'         =>  __('Button', 'bb-njba'), // Section Title
				'fields'        => array( // Section Fields
					'alignment' => array(
                        'type' => 'select',
                        'label' => __('Alignment','bb-njba'),
                        'default' => 'center',
                        'options' => array(
                            'left' => __('Left','bb-njba'),
                            'center' => __('Center','bb-njba'),
                            'right' => __('Right','bb-njba')
                        )   
                    ),
					'button_font_family' => array(
                        'type' => 'font',
                        'label' => __('Font Family','bb-njba'),
                        'default' => array(
                            'family' => 'Default',
                            'weight' => 'Default'
                        ),
                    ),
                    'button_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        )
                    ),
					'button_background_color' => array(
                        'type' => 'color',
                        'label' => __('Background Color','bb-njba'),
                        'show_reset' => true,
                        'default' => ''
                    ),
                    'button_background_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Background Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => ''
                    ),
                    'button_text_color' => array(
                        'type' => 'color',
                        'label' => __('Text Color','bb-njba'),
                        'show_reset' => true,
                        'default' => ''
                    ),
                    'button_text_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Text Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => ''
                    ),
                    'button_border_style'      => array(
                        'type'      => 'select',
                        'label'     => __('Border Style', 'bb-njba'),
                        'default'   => 'none',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'double'  => __('Double', 'bb-njba'),
                        ),
                        'toggle' => array(
                            'solid' => array(
                                'fields' => array('button_border_width','button_border_radius','button_border_color','button_border_hover_color')
                            ),
                            'dotted' => array(
                                'fields' => array('button_border_width','button_border_radius','button_border_color','button_border_hover_color')
                            ),
                            'dashed' => array(
                                'fields' => array('button_border_width','button_border_radius','button_border_color','button_border_hover_color')
                            ),
                            'double' => array(
                                'fields' => array('button_border_width','button_border_radius','button_border_color','button_border_hover_color')
                            ),
                        )
                    ),
                    'button_border_width' => array(
                        'type' => 'text',
                        'label' => __('Border Width','bb-njba'),
                        'default' => '1',
                        'size' => '5',
                        'description'       => _x( 'px', 'Value unit for spacer width. Such as: "10 px"', 'bb-njba' )
                    ),
                    'button_border_radius'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Border Radius', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top-left'          => 0,
                            'top-right'         => 0,
                            'bottom-left'       => 0,
                            'bottom-right'      => 0
                        ),
                        'options'           => array(
                             'top-left'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'top-right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom-left'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'bottom-right'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'button_border_color' => array(
                        'type' => 'color',
                        'label' => __('Border Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'button_border_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Border Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'button_box_shadow'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Box Shadow', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'left_right'          => 0,
                            'top_bottom'         => 0,
                            'blur'       => 0,
                            'spread'      => 0
                        ),
                        'options'           => array(
                            'left_right'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa fa-arrows-h'
                            ),
                            'top_bottom'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa fa-arrows-v'
                            ),
                            'blur'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa fa-circle-thin'
                            ),
                            'spread'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa fa-circle'
                            )
                            
                        )
                    ),
                    'button_box_shadow_color' => array(
                        'type' => 'color',
                        'label' => __('Box Shadow Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff'
                    ),
                    'button_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 10,
                            'right'         => 20,
                            'bottom'       => 10,
                            'left'         => 20
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        ),
                    ),
					
				)
			), // Section
			/*'button'       => array( // Section
				'title'         =>  __('Button', 'bb-njba'), // Section Title
				'fields'        => array( // Section Fields
					
                    
				)
			), // Section*/
		)
	),
	
    
));
