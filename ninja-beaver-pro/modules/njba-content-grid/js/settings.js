(function($){
	FLBuilder.registerModuleHelper('njba-content-grid', {
		
		init: function()
		{
			var form = $('.fl-builder-njba-content-grid-settings'),
				layout = form.find('select[name=layout] option');
				pagination = form.find('select[name=pagination] option');
				
				$('.fl-builder-njba-content-grid-settings select[name=layout]').on('change', function() {
					var layout_option = this.value;
					if(layout_option == 'masonry'  ){
					  	  	for (var j = 0; j < pagination.length; j++) {
					  	  	
					  	  	 	if(pagination[j].value == 'numbers'){
					  	  	 		$('select[name=pagination] option:nth-child(1)').css({'display': 'none'});
					  	  	 		//$('.select[name=pagination] option:contains("None")').attr('selected', 'selected');
					  	  	 		$('.fl-builder-settings-tabs a:contains("Pagination")').css({'display': 'none'});
					  	  	 	}
					  	  	}
	  	            }
	  	            else if(layout_option == 'grid')
					 {
					 		for (var k = 0; k < pagination.length; k++) {
					  	  		$('select[name=pagination] option:nth-child(1)').show();
					  	  		$('.fl-builder-settings-tabs a:contains("Pagination")').show();
					  	  	}	
					}
				});
				/*var node = form.attr('data-node');
				console.log(node);
				form.find('select[name=post_type]').on('change', function() {
		            post_type_option = $('.fl-builder-njba-content-grid-settings').find('select[name=post_type] option');
		            var post_type = this.value;
		            for (var j = 0; j < post_type_option.length; j++) {
		                //var selected_value = $( ".fl-builder-njba-content-grid-settings select[name=post_type] option:selected" ).val();
		                if(post_type_option[j].value === post_type){
		                    console.log(post_type);
		                    
		                    console.log('=>'+post_type_option[j].value);
		                    form.find('table.fl-form-table.fl-loop-builder-filter.fl-loop-builder-' + post_type + '-filter').hide();
		                    
		                }else{
		                	 //console.log(post_type);
		                    form.find('table.fl-form-table.fl-loop-builder-filter.fl-loop-builder-' + post_type + '-filter').show();
		                }
		            }
		            
		        });*/
				
		},
		
		
	});
})(jQuery);