<?php 
		if($settings->layout == 'masonry'){
			$post_type = ( isset( $settings->post_type ) ) ? $settings->post_type : 'post';
	        $object_taxonomies = get_object_taxonomies( $post_type );
	      
	        if( !empty( $object_taxonomies ) ) {
	            $cat = 'masonary_filter_' . $post_type;
	           
	            if( isset( $settings->$cat ) ) {
	            	if( $settings->$cat != -1 ) {
		            	$category_detail = wp_get_post_terms( get_the_ID(), $settings->$cat );
		            	$class = '';
			            if( count( $category_detail ) > 0 ) {
			                foreach( $category_detail as $cat_details ) {
			                    $class .= ' njba-masonary-cat-' . $cat_details->slug . ' ';
			                }
			            }
		            }
	            }
	        } 
	    }
?>
<?php
FLBuilderModel::default_settings($settings, array(
	'post_type' 			=> 'post',
	'order_by'  			=> 'date',
	'order'     			=> 'DESC',
	'offset'    			=> 0,
	'no_results_message'	=> __('No result found.', 'bb-njba'),
	'users'     			=> '',
	'show_author'			=> '1',
	'show_date'				=> '1',
	'date_format'			=> 'default',
	'show_post_taxonomies'	=> '1',
	'post_taxonomies'		=> 'category',
	'meta_separator'		=> ' / ',
	'title_margin'			=> array(
		'top'					=> '0',
		'bottom'				=> '0'
	)
));
?>
<?php if($settings->post_grid_style_select == "style-1"){?>
			
	<?php if($settings->layout == 'grid'){ ?>
			<div class="njba-content-post njba-blog-posts-col-<?php  echo $settings->show_col; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>" itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
			
	<?php } else if($settings->layout == 'carousel'){ ?>
			<div class="njba-content-post njba-blog-posts-slide-<?php  echo $i; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>"  itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
	<?php }else if($settings->layout == 'masonry'){ ?>
			<div <?php post_class('njba-content-post njba-post-wrapper '. 'njba-blog-posts-col-' . $settings->show_col. ' njba-' . $settings->post_grid_style_select); ?> >
			<?php }?>
					<?php NJBAContentGridModule::schema_meta(); ?>
					<div class="njba-content-grid">
	            	<?php  $module->post_image_render();?>
					 <div class="njba-content-grid-contant">
		                	<div class="njba-content-grid-contant-sub">
		                    	<div class="njba-content-grid-vertical-center">
		                        	<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
		                        	
			                            <?php
			                            		
				                         	$module->content_meta(get_the_ID());
					                        $module->excerpt_text();
					                        $module->button_render();
										?>
									
		                        </div>
		                    </div>
		             </div>
		             <div class="njba-content-grid-contant njba-content-grid-contant_section">
                		<div class="njba-content-grid-contant-sub">
	                   		<div class="njba-content-grid-vertical-center">
	                        	<?php
		                         	
		                         	$module->excerpt_text();
		                         	$module->button_render();
								?>
	                        </div>
	                   </div>
	                </div>
	            </div>
			</div>
<?php  } else if($settings->post_grid_style_select == "style-2"){ ?>
	<?php if($settings->layout == 'grid'){ ?>
			<div class="njba-content-post njba-blog-posts-col-<?php  echo $settings->show_col; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>"  itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
			
	<?php } else if($settings->layout == 'carousel'){ ?>
			<div class="njba-content-post njba-blog-posts-slide-<?php  echo $i; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>"  itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
	<?php }else if($settings->layout == 'masonry'){ ?>
			<div <?php post_class('njba-content-post njba-post-wrapper '. 'njba-blog-posts-col-' . $settings->show_col. ' njba-' . $settings->post_grid_style_select); ?> >
	<?php }?>
				<?php NJBAContentGridModule::schema_meta(); ?>
							<div class="njba-content-grid">
								<?php  $module->post_image_render();?> 	                
				                <div class="njba-content-grid-contant">
				                	<div class="njba-content-grid-contant-sub">
				                    	<div class="njba-content-grid-vertical-center">
				                        	<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
				                            <?php
					                         	$module->content_meta(get_the_ID());
					                         	$module->excerpt_text();
					                         	$module->button_render();
											?>
				                        </div>
				                    </div>
				                </div>
				            </div>
			</div>
<?php  } else if($settings->post_grid_style_select == "style-3"){ ?>
		<?php if($settings->layout == 'grid'){ ?>
				<div class="njba-content-post njba-blog-posts-col-<?php  echo $settings->show_col; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>"  itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
		<?php } else if($settings->layout == 'carousel'){ ?>
				<div class="njba-content-post njba-blog-posts-slide-<?php  echo $i; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>"  itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
		<?php }else if($settings->layout == 'masonry'){ ?>
					<div <?php post_class('njba-content-post njba-post-wrapper '. 'njba-blog-posts-col-' . $settings->show_col. ' njba-' . $settings->post_grid_style_select); ?> >
			<?php }?>
				<?php NJBAContentGridModule::schema_meta(); ?>
				<div class="njba-content-grid">
					<?php  $module->post_image_render();?>       
	                <div class="njba-content-grid-contant">
	                	<div class="njba-content-grid-contant-sub">
	                    	<div class="njba-content-grid-vertical-center">
	                        	<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
	                           <?php
		                         	$module->content_meta(get_the_ID());
		                         	$module->excerpt_text();
		                         	$module->button_render();
								?>
	                        </div>
	                    </div>
	                </div>
	            </div>
			</div>
 <?php  } else if($settings->post_grid_style_select == "style-4"){ ?>
		<?php if($settings->layout == 'grid'){ ?>
				<div class="njba-content-post njba-blog-posts-col-<?php  echo $settings->show_col; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>"  itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
		<?php } else if($settings->layout == 'carousel'){ ?>
				<div class="njba-content-post njba-blog-posts-slide-<?php  echo $i; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>"  itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
		<?php }else if($settings->layout == 'masonry'){ ?>
				<div <?php post_class('njba-content-post njba-post-wrapper '. 'njba-blog-posts-col-' . $settings->show_col. ' njba-' . $settings->post_grid_style_select); ?> >
			<?php }?>
						<?php NJBAContentGridModule::schema_meta(); ?>
						<div class="njba-content-grid">	
							<div class="njba-content-grid-overlay-image">
								<?php  $module->post_image_render();?>
								<div class="njba-content-grid-overlay">
									<div class="njba-content-grid-overlay-wrapper">
										<div class="njba-content-grid-contant">
											<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
											<?php $module->content_meta(get_the_ID());?>
										</div>
									</div>
								</div>
							</div>
							<div class="njba-content-grid-category">
							<?php 	$module->category_button_render();	?>
							</div>
						</div>
				</div>
 <?php  }else if($settings->post_grid_style_select == "style-5"){ ?>
		<?php if($settings->layout == 'grid'){ ?>
				<div class="njba-content-post njba-blog-posts-col-<?php  echo $settings->show_col; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>"  itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
		<?php } else if($settings->layout == 'carousel'){ ?>
				<div class="njba-content-post njba-blog-posts-slide-<?php  echo $i; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>"  itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
		<?php }else if($settings->layout == 'masonry'){ ?>
				<div <?php post_class('njba-content-post njba-post-wrapper '. 'njba-blog-posts-col-' . $settings->show_col. ' njba-' . $settings->post_grid_style_select); ?> >
			<?php }?>
						<?php NJBAContentGridModule::schema_meta(); ?>
						<div class="njba-content-grid">
								<div class="njba-content-grid-image" itemscope="" itemprop="image" itemtype="http://schema.org/ImageObject">
									<?php  $module->post_image_render();
					            			if($settings->show_date == "1") :
				            		?>
				            			<div class="njba_front_inner"><span><?php FLBuilderLoop::post_date('d');?><br/><?php FLBuilderLoop::post_date('M');?></span></div>
				            		<?php endif;?>
				            		<<?php echo $settings->post_title_tag; ?> class="vertical_title"><span><?php the_title(); ?><i class="fa fa-circle" ></i></span>  </<?php echo $settings->post_title_tag; ?>>
								</div>       
				                <div class="njba-content-grid-contant njba-content-grid-contant_section">
				                	<div class="njba-content-grid-contant-sub">
					                   
					                    	<div class="njba-content-grid-vertical-center">
					                        	<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
					                           <?php
						                         	$module->content_meta(get_the_ID());
						                         	$module->excerpt_text();
						                         	$module->button_render();
												?>
					                        </div>
					                   
				                    </div>
				                </div>
	            		</div>
						
			       
				</div>
 <?php  } else if($settings->post_grid_style_select == "style-6"){ ?>
		<?php if($settings->layout == 'grid'){ ?>
				<div class="njba-content-post njba-blog-posts-col-<?php  echo $settings->show_col; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>"  itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
		<?php } else if($settings->layout == 'carousel'){ ?>
				<div class="njba-content-post njba-blog-posts-slide-<?php  echo $i; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>"  itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
		<?php }else if($settings->layout == 'masonry'){ ?>
				<div <?php post_class('njba-content-post njba-post-wrapper '. 'njba-blog-posts-col-' . $settings->show_col. ' njba-' . $settings->post_grid_style_select); ?> >
			<?php }?>
						<?php //NJBAContentGridModule::schema_meta(); ?>
						<div class="njba-content-grid njba-content-grid-wrapper">
								<div class="njba-content-grid-image" itemscope="" itemprop="image" itemtype="http://schema.org/ImageObject">
									<?php  $module->post_image_render();?>
				            			<div class="njba_front_inner">
				            				<?php if($settings->show_date == "1") :?>
					            				<div class="njba_count" style="background: url(<?php echo NJBA_MODULE_URL . 'modules/njba-content-grid/images/top-pic.png';?>)no-repeat top center;">
					            					<?php FLBuilderLoop::post_date('d');?><br/><?php FLBuilderLoop::post_date('M');?>
					            				</div>
					            			<?php endif;?>
					            				<<?php echo $settings->post_title_tag; ?> class="horizontal_title"><?php the_title(); ?></<?php echo $settings->post_title_tag; ?>>
				            			</div>
								</div>       
				                <div class="njba-content-grid-contant njba-content-grid-contant_section">
				                	<div class="njba-content-grid-contant-sub">
					                   
					                    	<div class="njba-content-grid-vertical-center">
					                        	<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
					                           <?php
						                         	$module->content_meta(get_the_ID());
						                         	$module->excerpt_text();
						                         	$module->button_render();
												?>
					                        </div>
					                   
				                    </div>
				                </div>
	            		</div>
						
			       
				</div>
 <?php  } else if($settings->post_grid_style_select == "style-7"){ ?>
		<?php if($settings->layout == 'grid'){ ?>
				<div class="njba-content-post njba-blog-posts-col-<?php  echo $settings->show_col; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>"  itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
		<?php } else if($settings->layout == 'carousel'){ ?>
				<div class="njba-content-post njba-blog-posts-slide-<?php  echo $i; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>"  itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
		<?php }else if($settings->layout == 'masonry'){ ?>
				<div <?php post_class('njba-content-post njba-post-wrapper '. 'njba-blog-posts-col-' . $settings->show_col. ' njba-' . $settings->post_grid_style_select); ?> >
			<?php }?>
						<?php //NJBAContentGridModule::schema_meta(); ?>
						<div class="njba-content-grid njba-content-grid-wrapper">
								<div class="njba-content-grid-image" itemscope="" itemprop="image" itemtype="http://schema.org/ImageObject">
									<?php  $module->post_image_render();?>
				            			<div class="njba_front_inner">
				            				<div class="njba_front_inner_wraper">
				            					<<?php echo $settings->post_title_tag; ?> class="horizontal_title"><?php the_title(); ?></<?php echo $settings->post_title_tag; ?>>
				            				</div>
				            			</div>
								</div>       
				                <div class="njba-content-grid-contant njba-content-grid-contant_section">
				                	<div class="njba-content-grid-contant-sub">
					                   
					                    	<div class="njba-content-grid-vertical-center">
					                        	<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
					                           <?php
						                         	$module->content_meta(get_the_ID());
						                         	$module->excerpt_text();
						                         	$module->button_render();
												?>
					                        </div>
					                   
				                    </div>
				                </div>
	            		</div>
						
			       
				</div>
 <?php  }?>		 
			
				
            
      				