<?php if($settings->post_grid_style_select == "style-1"){?>
		<div class="njba-content-post njba-blog-posts-col-<?php  echo $settings->show_col; ?> njba-module-content  njba-flip-box njba-flip-box-wrap njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?> " itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
			<div class="njba-flip-box  <?php echo $settings->flip_type; ?>" >
					<?php  ?>
					<div class="njba-content-grid njba-flip-box-outter">
						<div class="njba-face njba-front">
							<div class="njba-flip-box-section njba-flip-box-section-vertical-middle">
			            		<?php  $module->post_image_render();?>
			            		
			            		<<?php echo $settings->post_title_tag; ?> class="vertical_title"><?php the_title(); ?></<?php echo $settings->post_title_tag; ?>>
			            		<?php $module->content_meta(get_the_ID());?>
			            	</div>
			            </div>
			            <div class="njba-face njba-back">
			            	<div class="njba-flip-box-section njba-flip-box-section-vertical-middle">
								 <div class="njba-content-grid-contant">
				                	<div class="njba-content-grid-contant-sub">
				                    	<div class="njba-content-grid-vertical-center">
				                        	<?php
					                         	$module->excerpt_text();
					                         	$module->button_render();
											?>
				                        </div>
				                    </div>
				                </div>
				            </div>
			            </div>
	            </div>
	        </div>
		</div>
			
<?php  }elseif($settings->post_grid_style_select == "style-2"){?>
		<div class="njba-content-post njba-blog-posts-col-<?php  echo $settings->show_col; ?> njba-module-content  njba-flip-box njba-flip-box-wrap njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?> " itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
			<div class="njba-flip-box  <?php echo $settings->flip_type; ?>" >
					<?php  ?>
					<div class="njba-content-grid njba-flip-box-outter">
						<div class="njba-face njba-front">
							<div class="njba-flip-box-section njba-flip-box-section-vertical-middle">
			            		<div class="njba-content-grid">
									<?php  $module->post_image_render();?> 	                
					                <div class="njba-content-grid-contant">
					                	<div class="njba-content-grid-contant-sub">
					                    	<div class="njba-content-grid-vertical-center">
					                        	<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
					                            <?php
						                         	$module->content_meta(get_the_ID());
						                         	$module->excerpt_text();
						                         	$module->button_render();
												?>
					                        </div>
					                    </div>
					                </div>
					            </div>
			            	</div>
			            </div>
			            <div class="njba-face njba-back">
			            	<div class="njba-flip-box-section njba-flip-box-section-vertical-middle">
								 <div class="njba-content-grid-contant">
				                	<div class="njba-content-grid-contant-sub">
				                    	<div class="njba-content-grid-vertical-center">
				                        	<div class="njba-content-grid">
												<div class="njba-content-grid-vertical-center">
						                        	<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
						                            <?php
							                         	$module->content_meta(get_the_ID());
							                         	$module->excerpt_text();
							                         	$module->button_render();
													?>
						                        </div>
								                   
								            </div>
				                        </div>
				                    </div>
				                </div>
				                <?php  $module->post_image_render();?> 
				            </div>
			            </div>
	            </div>
	        </div>
		</div>
			
<?php  }elseif($settings->post_grid_style_select == "style-3"){?>
		<div class="njba-content-post njba-blog-posts-col-<?php  echo $settings->show_col; ?> njba-module-content  njba-flip-box njba-flip-box-wrap njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?> " itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
			<div class="njba-flip-box  <?php echo $settings->flip_type; ?>" >
					<?php  ?>
					<div class="njba-content-grid njba-flip-box-outter">
						<div class="njba-face njba-front">
							<div class="njba-flip-box-section njba-flip-box-section-vertical-middle">
			            		<?php  $module->post_image_render();?>
			            	</div>
			            </div>
			            <div class="njba-face njba-back">
			            	<div class="njba-flip-box-section njba-flip-box-section-vertical-middle">
								 <div class="njba-content-grid-contant">
				                	<div class="njba-content-grid-contant-sub">
				                    	<div class="njba-content-grid-vertical-center">
				                        	<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
				                           <?php
					                         	$module->content_meta(get_the_ID());
					                         	$module->excerpt_text();
					                         	$module->button_render();
											?>
				                        </div>
				                    </div>
				                </div>
				            </div>
			            </div>
	            </div>
	        </div>
		</div>
			
<?php  }
		elseif($settings->post_grid_style_select == "style-4"){?>
		<div class="njba-content-post njba-blog-posts-col-<?php  echo $settings->show_col; ?> njba-module-content  njba-flip-box njba-flip-box-wrap njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?> " itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
			<div class="njba-flip-box  <?php echo $settings->flip_type; ?>" >
					<?php  ?>
					<div class="njba-content-grid njba-flip-box-outter">
						<div class="njba-face njba-front">
							<div class="njba-flip-box-section njba-flip-box-section-vertical-middle">
			            		<div class="njba-content-grid-overlay-image">
									<?php  $module->post_image_render();?>
									<div class="njba-content-grid-overlay">
										<div class="njba-content-grid-overlay-wrapper">
											<div class="njba-content-grid-contant">
												<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
												<?php $module->content_meta(get_the_ID());?>
											</div>
										</div>
									</div>
								</div>
								<div class="njba-content-grid-category">
								<?php 	$module->category_button_render();	?>
								</div>
			            	</div>
			            </div>
			            <div class="njba-face njba-back">
			            	<div class="njba-flip-box-section njba-flip-box-section-vertical-middle">
								
				                	<div class="njba-content-grid">	
										<div class="njba-content-grid-overlay-image">
											<?php  $module->post_image_render();?>
											<div class="njba-content-grid-overlay">
												<div class="njba-content-grid-overlay-wrapper">
													<div class="njba-content-grid-contant">
														<?php
								                         	$module->excerpt_text();
								                        ?>
													</div>
												</div>
											</div>
										</div>
										<div class="njba-content-grid-category">
										<?php 	$module->button_render();	?>
										</div>
									</div>
				                
				            </div>
			            </div>
	            </div>
	        </div>
		</div>
			
<?php  }elseif($settings->post_grid_style_select == "style-5"){?>
		<div class="njba-content-post njba-blog-posts-col-<?php  echo $settings->show_col; ?> njba-module-content  njba-flip-box njba-flip-box-wrap njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?> " itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
			<div class="njba-flip-box  <?php echo $settings->flip_type; ?>" >
					<?php  ?>
					<div class="njba-content-grid njba-flip-box-outter">
						<div class="njba-face njba-front">
							<div class="njba-flip-box-section njba-flip-box-section-vertical-middle">
			            		<?php  $module->post_image_render();
			            			if($settings->show_date == "1") :
			            		?>
			            			<div class="njba_front_inner"><span><?php FLBuilderLoop::post_date('d');?><br/><?php FLBuilderLoop::post_date('M');?></span></div>
			            		<?php endif;?>
			            		<<?php echo $settings->post_title_tag; ?> class="vertical_title"><span><?php the_title(); ?><i class="fa fa-circle" ></i></span>  </<?php echo $settings->post_title_tag; ?>>
			            	</div>
			            </div>
			            <div class="njba-face njba-back">
			            	<div class="njba-flip-box-section njba-flip-box-section-vertical-middle">
								 <div class="njba-content-grid-contant">
				                	<div class="njba-content-grid-contant-sub">
				                    	<div class="njba-content-grid-vertical-center">
				                        	<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
				                           <?php
					                         	$module->content_meta(get_the_ID());
					                         	$module->excerpt_text();
					                         	$module->button_render();
											?>
				                        </div>
				                    </div>
				                </div>
				            </div>
			            </div>
	            </div>
	        </div>
		</div>
			
<?php  }elseif($settings->post_grid_style_select == "style-6"){?>
		<div class="njba-content-post njba-blog-posts-col-<?php  echo $settings->show_col; ?> njba-module-content  njba-flip-box-wrap njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?> " itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
			<div class="njba-flip-box  <?php echo $settings->flip_type; ?>" >
					<?php  ?>
					<div class="njba-content-grid njba-flip-box-outter">
						<div class="njba-face njba-front">
							<div class="njba-flip-box-section njba-flip-box-section-vertical-middle">
			            		<?php  $module->post_image_render();?>
			            			<div class="njba_front_inner">
			            				<?php if($settings->show_date == "1") :?>
				            				<div class="njba_count" style="background: url(<?php echo NJBA_MODULE_URL . 'modules/njba-content-grid/images/top-pic.png';?>)no-repeat top center;">
				            					<?php FLBuilderLoop::post_date('d');?><br/><?php FLBuilderLoop::post_date('M');?>
				            				</div>
				            			<?php endif;?>
				            				<<?php echo $settings->post_title_tag; ?> class="horizontal_title"><?php the_title(); ?></<?php echo $settings->post_title_tag; ?>>
			            			</div>
			            		
			            		
			            	</div>
			            </div>
			            <div class="njba-face njba-back">
			            	<div class="njba-flip-box-section njba-flip-box-section-vertical-middle">
								 <div class="njba-content-grid-contant">
				                	<div class="njba-content-grid-contant-sub">
				                    	<div class="njba-content-grid-vertical-center">
				                        	<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
				                           <?php
					                         	$module->content_meta(get_the_ID());
					                         	$module->excerpt_text();
					                         	$module->button_render();
											?>
				                        </div>
				                    </div>
				                </div>
				            </div>
			            </div>
	            </div>
	        </div>
		</div>
			
<?php  }elseif($settings->post_grid_style_select == "style-7"){?>
		<div class="njba-content-post njba-blog-posts-col-<?php  echo $settings->show_col; ?> njba-module-content  njba-flip-box-wrap njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?> " itemscope itemtype="<?php NJBAContentGridModule::schema_itemtype(); ?>">
			<div class="njba-flip-box  <?php echo $settings->flip_type; ?>" >
					<?php  ?>
					<div class="njba-content-grid njba-flip-box-outter">
						<div class="njba-face njba-front">
							<div class="njba-flip-box-section njba-flip-box-section-vertical-middle">
			            		<?php  $module->post_image_render();?>
			            			<div class="njba_front_inner">
			            				<div class="njba_front_inner_wraper">
			            					<<?php echo $settings->post_title_tag; ?> class="horizontal_title"><?php the_title(); ?></<?php echo $settings->post_title_tag; ?>>
			            				</div>
			            			</div>
			            		
			            		
			            	</div>
			            </div>
			            <div class="njba-face njba-back">
			            	<div class="njba-flip-box-section njba-flip-box-section-vertical-middle">
								 <div class="njba-content-grid-contant">
				                	<div class="njba-content-grid-contant-sub">
				                    	<div class="njba-content-grid-vertical-center">
				                        	<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
				                           <?php
					                         	$module->content_meta(get_the_ID());
					                         	$module->excerpt_text();
					                         	$module->button_render();
											?>
				                        </div>
				                    </div>
				                </div>
				            </div>
			            </div>
	            </div>
	        </div>
		</div>
			
<?php  } ?>		 
			
			
      				