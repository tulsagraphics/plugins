<?php
        $btn_css_array = array(
        //Button Style
        /*'button_style'      => $settings->button_style,*/
        'button_background_color'       => $settings->button_background_color,
        'button_background_hover_color'     => $settings->button_background_hover_color,
        'button_text_color'     =>$settings->button_text_color,
        'button_text_hover_color'     =>$settings->button_text_hover_color,
        'button_border_style'     =>$settings->button_border_style,
        'button_border_width'     =>$settings->button_border_width,
        'button_border_radius'     =>$settings->button_border_radius,
        'button_border_color'     =>$settings->button_border_color,
        'button_border_hover_color'     =>$settings->button_border_hover_color,
        'button_box_shadow'     =>$settings->button_box_shadow,
        'button_box_shadow_color'     =>$settings->button_box_shadow_color,
        'button_padding'     =>$settings->button_padding,
        'alignment'     =>$settings->alignment,
        //Button Typography
        'button_font_family'     =>$settings->button_font_family,
        'button_font_size'     =>$settings->button_font_size,
        );
        FLBuilder::render_module_css('njba-button' , $id, $btn_css_array);
?>
/***** For Carousel  css *****/
.fl-node-<?php echo $id; ?> .njba-style-1 .njba-content-grid:hover .njba-btn-main,
.fl-node-<?php echo $id; ?> .njba-style-1 .njba-content-grid:hover p {
    text-align: center;
}
.fl-node-<?php echo $id; ?> .njba-style-1 .njba-content-grid:hover p{
     <?php if( $settings->back_text_color ) { ?>color: #<?php echo $settings->back_text_color; ?>;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-blog-posts-carousel.njba-blog-posts-carousel-loaded a.bx-pager-link {
        <?php if( $settings->dot_color ) { ?>background: #<?php echo $settings->dot_color; ?><?php } ?>;
        opacity: 0.5;
}
.fl-node-<?php echo $id; ?> .njba-blog-posts-carousel.njba-blog-posts-carousel-loaded a.bx-pager-link.active {
        <?php if( $settings->active_dot_color ) { ?>background: #<?php echo $settings->active_dot_color; ?>;<?php } ?>
        opacity: 1;
}
.fl-node-<?php echo $id; ?> .njba-blog-posts-carousel .bx-wrapper .bx-controls-direction a.bx-next,
.fl-node-<?php echo $id; ?> .njba-blog-posts-carousel .bx-wrapper .bx-controls-direction a.bx-prev {
        
        <?php if( $settings->arrow_background ) { ?>background: #<?php echo $settings->arrow_background; ?>;<?php } ?>
        <?php if( $settings->arrow_color ) { ?>color: #<?php echo $settings->arrow_color; ?>;<?php } ?>
        
}
/*****For Column******/
.fl-node-<?php echo $id; ?> .njba-<?php echo $settings->post_grid_style_select; ?> .njba-content-grid {
         <?php if( $settings->col_bg_color ) { ?>background: #<?php echo $settings->col_bg_color; ?>;<?php } ?>
         
         box-shadow:<?php if ($settings->col_box_shadow['left_right'] != ''){ echo $settings->col_box_shadow['left_right'];?>px <?php } if ($settings->col_box_shadow['top_bottom'] != ''){  echo $settings->col_box_shadow['top_bottom'];?>px <?php } if ($settings->col_box_shadow['blur'] != ''){ echo $settings->col_box_shadow['blur'];?>px <?php } if ($settings->col_box_shadow['spread'] != ''){ echo $settings->col_box_shadow['spread'];?>px #<?php } echo $settings->col_box_shadow_color;?>;    
         <?php if($settings->col_border_width >= '0') { ?>border-width: <?php echo $settings->col_border_width;?>px; <?php } ?>
        <?php if($settings->col_border_style) { ?>border-style:<?php echo $settings->col_border_style;?>;<?php } ?>
        <?php if($settings->col_border_color) {?>border-color:<?php echo '#'.$settings->col_border_color;?>;<?php } ?>
        <?php if($settings->col_border_radius != ''){?> border-radius: <?php echo $settings->col_border_radius ;?>px ;<?php } ?>    
        
}
.fl-node-<?php echo $id; ?> .njba-<?php echo $settings->post_grid_style_select; ?> .njba-content-grid:hover {
        box-shadow:<?php if ($settings->col_box_shadow['left_right'] != ''){ echo $settings->col_box_shadow['left_right'];?>px <?php } if ($settings->col_box_shadow['top_bottom'] != ''){  echo $settings->col_box_shadow['top_bottom'];?>px <?php } if ($settings->col_box_shadow['blur'] != ''){ echo $settings->col_box_shadow['blur'];?>px <?php } if ($settings->col_box_shadow['spread'] != ''){ echo $settings->col_box_shadow['spread'];?>px #<?php } echo $settings->col_box_shadow_hover_color;?>;  
         
         <?php if($settings->col_border_hover_color) {?>border-color:<?php echo '#'.$settings->col_border_hover_color;?>;<?php } ?>
       
        
}
.fl-node-<?php echo $id; ?> .njba-blog-posts-grid .njba-post-wrapper,
.fl-node-<?php echo $id; ?> .njba-blog-posts-flipbox .njba-post-wrapper,
.fl-node-<?php echo $id; ?> .njba-blog-posts-masonry .njba-post-wrapper{
       <?php if( $settings->post_spacing != '' ) { ?>padding: <?php echo $settings->post_spacing; ?>px;<?php } ?> 
}
/***** For Pagination   css *****/
.fl-node-<?php echo $id; ?> .njba-pagination li a.page-numbers {
         <?php if( $settings->pagi_color ) { ?>color: #<?php echo $settings->pagi_color; ?>;<?php } ?>
        <?php if( $settings->pagi_bg_color ) { ?>background: #<?php echo $settings->pagi_bg_color; ?>;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-pagination li span.page-numbers, 
.fl-node-<?php echo $id; ?> .njba-pagination li a.page-numbers:hover {
         <?php if( $settings->pagi_active_color ) { ?>color: #<?php echo $settings->pagi_active_color; ?>;<?php } ?>
        <?php if( $settings->pagi_activebg_color ) { ?>background: #<?php echo $settings->pagi_activebg_color; ?>;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-pagination li a.page-numbers,
.fl-node-<?php echo $id; ?> .njba-pagination li span.page-numbers{
           <?php if($settings->pagination_border != 'none'){?> border: <?php echo $settings->pagination_border_width ;?>px <?php echo $settings->pagination_border ;?> #<?php echo $settings->pagination_border_color; ?>;<?php } ?>
           <?php if($settings->pagination_border != 'none'){?> border-radius: <?php echo $settings->pagination_border_radius ;?>px ;<?php } ?>
            <?php if( $settings->pagination_padding['top'] != '' ) { ?>padding-top:<?php echo $settings->pagination_padding['top'] ?>px;<?php } ?>
            <?php if( $settings->pagination_padding['right'] != '' ) { ?>padding-right:<?php echo $settings->pagination_padding['right'] ?>px;<?php } ?>
            <?php if( $settings->pagination_padding['bottom'] != '' ) { ?>padding-bottom:<?php echo $settings->pagination_padding['bottom'] ?>px;<?php } ?>
            <?php if( $settings->pagination_padding['left'] != '' ) { ?>padding-left:<?php echo $settings->pagination_padding['left'] ?>px;<?php } ?>
            <?php if( $settings->pagination_font_family['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->pagination_font_family ); ?><?php } ?>
            <?php if( $settings->pagination_font_size['desktop'] != '' ) { ?>font-size: <?php echo $settings->pagination_font_size['desktop']; ?>px;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-pagination ul.page-numbers li {
        margin-left: <?php echo $settings->pagination_spacing;?>px;
         margin-right: <?php echo $settings->pagination_spacing;?>px;
         padding-top: <?php echo $settings->pagination_spacing_v;?>px;
         padding-bottom: <?php echo $settings->pagination_spacing_v;?>px;
}
/***** For Post Title  css *****/
.fl-node-<?php echo $id; ?> .njba-content-grid-contant <?php echo $settings->post_title_tag; ?> {
        <?php if( $settings->post_title_color ) { ?>color: #<?php echo $settings->post_title_color; ?>;<?php } ?>
        <?php if( $settings->post_title_font['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->post_title_font ); ?><?php } ?>
        <?php if( $settings->post_title_font_size['desktop'] != '' ) { ?>font-size: <?php echo $settings->post_title_font_size['desktop']; ?>px;<?php } ?>
        <?php if( $settings->post_title_alignment ) { ?>text-align: <?php echo $settings->post_title_alignment; ?>;<?php } ?>
        <?php if( $settings->post_title_height['desktop'] != '' ) { ?>line-height: <?php echo $settings->post_title_height['desktop']; ?>px;<?php } ?>
        <?php if( $settings->post_title_padding['top'] != '' ) { ?>padding-top:<?php echo $settings->post_title_padding['top'] ?>px;<?php } ?>
        <?php if( $settings->post_title_padding['right'] != '' ) { ?>padding-right:<?php echo $settings->post_title_padding['right'] ?>px;<?php } ?>
        <?php if( $settings->post_title_padding['bottom'] != '' ) { ?>padding-bottom:<?php echo $settings->post_title_padding['bottom'] ?>px;<?php } ?>
        <?php if( $settings->post_title_padding['left'] != '' ) { ?>padding-left:<?php echo $settings->post_title_padding['left'] ?>px;<?php } ?>
} 
.fl-node-<?php echo $id; ?> .njba-content-grid-contant <?php echo $settings->post_title_tag; ?> a {
        <?php if( $settings->post_title_color ) { ?>color: #<?php echo $settings->post_title_color; ?>;<?php } ?>
        <?php if( $settings->post_title_font['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->post_title_font ); ?><?php } ?>
        <?php if( $settings->post_title_font_size['desktop'] != '' ) { ?>font-size: <?php echo $settings->post_title_font_size['desktop']; ?>px;<?php } ?>
        <?php if( $settings->post_title_alignment ) { ?>text-align: <?php echo $settings->post_title_alignment; ?>;<?php } ?>
        <?php if( $settings->post_title_height['desktop'] != '' ) { ?>line-height: <?php echo $settings->post_title_height['desktop']; ?>px;<?php } ?>
} 
.fl-node-<?php echo $id; ?> ..njba-content-grid-contant <?php echo $settings->post_title_tag; ?> a:hover {
         <?php if( $settings->post_title_hover_color ) { ?>color: #<?php echo $settings->post_title_hover_color; ?>;<?php } ?>
}
.fl-node-<?php echo $id; ?> <?php echo $settings->post_title_tag; ?>.horizontal_title {
        <?php if( $settings->post_title_font['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->post_title_font ); ?><?php } ?>
        <?php if( $settings->post_title_font_size['desktop'] != '' ) { ?>font-size: <?php echo $settings->post_title_font_size['desktop']; ?>px;<?php } ?>
        <?php if( $settings->post_title_height['desktop'] != '' ) { ?>line-height: <?php echo $settings->post_title_height['desktop']; ?>px;<?php } ?>
} 
<?php //die();?>
/***** For Post Content  css *****/
.fl-node-<?php echo $id; ?> .njba-content-grid-contant p {
        <?php if( $settings->post_content_color ) { ?>color: #<?php echo $settings->post_content_color; ?>;<?php } ?>
        <?php if( $settings->post_content_font['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->post_content_font ); ?><?php } ?>
        <?php if( $settings->post_content_font_size['desktop'] != ''  ) { ?>font-size: <?php echo $settings->post_content_font_size['desktop']; ?>px;<?php } ?>
        <?php if( $settings->post_content_alignment ) { ?>text-align: <?php echo $settings->post_content_alignment; ?>;<?php } ?>
        <?php if( $settings->post_content_height['desktop']!= ''  ) { ?>line-height: <?php echo $settings->post_content_height['desktop']; ?>px;<?php } ?>
        <?php if( $settings->post_content_padding['top'] != '' ) { ?>padding-top:<?php echo $settings->post_content_padding['top'] ?>px;<?php } ?>
        <?php if( $settings->post_content_padding['right'] != '' ) { ?>padding-right:<?php echo $settings->post_content_padding['right'] ?>px;<?php } ?>
        <?php if( $settings->post_content_padding['bottom'] != '' ) { ?>padding-bottom:<?php echo $settings->post_content_padding['bottom'] ?>px;<?php } ?>
        <?php if( $settings->post_content_padding['left'] != '' ) { ?>padding-left:<?php echo $settings->post_content_padding['left'] ?>px;<?php } ?>
} 
/***** For Post Date  css *****/
.fl-node-<?php echo $id; ?> .njba-content-grid-section-wrapper ul{ 
        <?php if( $settings->post_date_alignment ) { ?>text-align: <?php echo $settings->post_date_alignment; ?>;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-content-grid-section-wrapper ul li a, 
.fl-node-<?php echo $id; ?> .njba-content-grid-section-wrapper ul li,
.fl-node-<?php echo $id; ?> .njba-content-grid-section-wrapper ul li span{
        
        <?php if( $settings->post_date_color ) { ?>color: #<?php echo $settings->post_date_color; ?>;<?php } ?>
        <?php if( $settings->post_date_font['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->post_date_font ); ?><?php } ?>
        <?php if( $settings->post_date_font_size['desktop'] != '' ) { ?>font-size: <?php echo $settings->post_date_font_size['desktop']; ?>px;<?php } ?>
       <?php if( $settings->post_date_height['desktop'] != '' ) { ?>line-height: <?php echo $settings->post_date_height['desktop']; ?>px;<?php } ?>
        <?php if( $settings->post_date_padding['top'] != '' ) { ?>padding-top:<?php echo $settings->post_date_padding['top'] ?>px;<?php } ?>
        <?php if( $settings->post_date_padding['right'] != '' ) { ?>padding-right:<?php echo $settings->post_date_padding['right'] ?>px;<?php } ?>
        <?php if( $settings->post_date_padding['bottom'] != '' ) { ?>padding-bottom:<?php echo $settings->post_date_padding['bottom'] ?>px;<?php } ?>
        <?php if( $settings->post_date_padding['left'] != '' ) { ?>padding-left:<?php echo $settings->post_date_padding['left'] ?>px;<?php } ?>
} 
/******Style 1******/
<?php if($settings->post_grid_style_select == "style-1"){
        if($settings->layout == 'flipbox'){?>
            .fl-node-<?php echo $id; ?> .njba-style-1 .njba-face.njba-back {
                    <?php if( $settings->overly_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->overly_color)) ?>, <?php echo $settings->overly_color_opacity/100; ?>); <?php } ?>
            }
           
    <?php }else{?>
            .fl-node-<?php echo $id; ?> .njba-style-1 .njba-content-grid-contant.njba-content-grid-contant_section {
                    <?php if( $settings->overly_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->overly_color)) ?>, <?php echo $settings->overly_color_opacity/100; ?>); <?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-style-1 .vertical_title{
                <?php if( $settings->front_title_bc_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->front_title_bc_color)) ?>, <?php echo $settings->front_title_bc_color_opc/100; ?>); <?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-style-1 .vertical_title{
               <?php if( $settings->front_title_color ) { ?>color: #<?php echo $settings->front_title_color; ?>;<?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-style-1 .njba_front_inner{
                <?php if( $settings->front_date_bc_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->front_date_bc_color)) ?>, <?php echo $settings->front_date_bc_color_opc/100; ?>); <?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba_front_inner span{
               <?php if( $settings->front_date_color ) { ?>color: #<?php echo $settings->front_date_color; ?>;<?php } ?>
            }
    <?php }
    }?>
/*****Style 3 Overly****/
<?php if($settings->layout == 'flipbox'){?>
        .fl-node-<?php echo $id; ?> .njba-style-3 .njba-face.njba-back {
                <?php if( $settings->overly_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->overly_color)) ?>, <?php echo $settings->overly_color_opacity/100; ?>); <?php } ?>
        }
<?php }else{?>
        .fl-node-<?php echo $id; ?> .njba-style-3 .njba-content-grid-contant {
                <?php if( $settings->overly_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->overly_color)) ?>, <?php echo $settings->overly_color_opacity/100; ?>); <?php } ?>
        }
<?php }?>
/*****Style 4 Overly****/
.fl-node-<?php echo $id; ?> .njba-style-4:hover .njba-content-grid-overlay {
        <?php if( $settings->overly_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->overly_color)) ?>, <?php echo $settings->overly_color_opacity/100; ?>); <?php } ?>
}
/*****Style 5 Overly****/
<?php if($settings->post_grid_style_select == "style-5"){
        if($settings->layout == 'flipbox'){?>
            .fl-node-<?php echo $id; ?> .njba-style-5 .njba-face.njba-back {
                    <?php if( $settings->overly_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->overly_color)) ?>, <?php echo $settings->overly_color_opacity/100; ?>); <?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-style-5 .vertical_title{
                <?php if( $settings->front_title_bc_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->front_title_bc_color)) ?>, <?php echo $settings->front_title_bc_color_opc/100; ?>); <?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-style-5 .vertical_title{
               <?php if( $settings->front_title_color ) { ?>color: #<?php echo $settings->front_title_color; ?>;<?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-style-5 .njba-front .njba_front_inner{
                <?php if( $settings->front_date_bc_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->front_date_bc_color)) ?>, <?php echo $settings->front_date_bc_color_opc/100; ?>); <?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-front .njba_front_inner span{
               <?php if( $settings->front_date_color ) { ?>color: #<?php echo $settings->front_date_color; ?>;<?php } ?>
            }
    <?php }else{?>
            .fl-node-<?php echo $id; ?> .njba-style-5 .njba-content-grid-contant.njba-content-grid-contant_section {
                    <?php if( $settings->overly_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->overly_color)) ?>, <?php echo $settings->overly_color_opacity/100; ?>); <?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-style-5 .vertical_title{
                <?php if( $settings->front_title_bc_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->front_title_bc_color)) ?>, <?php echo $settings->front_title_bc_color_opc/100; ?>); <?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-style-5 .vertical_title{
               <?php if( $settings->front_title_color ) { ?>color: #<?php echo $settings->front_title_color; ?>;<?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-style-5 .njba_front_inner{
                <?php if( $settings->front_date_bc_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->front_date_bc_color)) ?>, <?php echo $settings->front_date_bc_color_opc/100; ?>); <?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba_front_inner span{
               <?php if( $settings->front_date_color ) { ?>color: #<?php echo $settings->front_date_color; ?>;<?php } ?>
            }
    <?php }
    }
?>
/*****Style 6 Overly****/
<?php if($settings->post_grid_style_select == "style-6"){
        if($settings->layout == 'flipbox'){?>
            .fl-node-<?php echo $id; ?> .njba-style-6 .njba-face.njba-back {
                    <?php if( $settings->overly_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->overly_color)) ?>, <?php echo $settings->overly_color_opacity/100; ?>); <?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-style-6 .njba_front_inner {
                    <?php if( $settings->front_main_overly_bc_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->front_main_overly_bc_color)) ?>, <?php echo $settings->front_main_overly_color_opc/100; ?>); <?php } ?>
            }
            
            .fl-node-<?php echo $id; ?> .njba-style-6 .horizontal_title{
               <?php if( $settings->front_title_color ) { ?>color: #<?php echo $settings->front_title_color; ?>;<?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-front .njba_front_inner .njba_count{
               <?php if( $settings->front_date_color ) { ?>color: #<?php echo $settings->front_date_color; ?>;<?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-content-grid-section-wrapper .njba-flip-box-wrap.njba-style-6 .njba-face.njba-front .njba-flip-box-section{
                 <?php if( $settings->front_main_bc_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->front_main_bc_color)) ?>, <?php echo $settings->front_main_bc_color_opc/100; ?>); <?php } ?>
                  <?php if($settings->front_border_color) {?>border-color:<?php echo '#'.$settings->front_border_color;?>;<?php } ?>
            }
    <?php }else{?>
            .fl-node-<?php echo $id; ?> .njba-style-6 .njba-content-grid-contant.njba-content-grid-contant_section {
                    <?php if( $settings->overly_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->overly_color)) ?>, <?php echo $settings->overly_color_opacity/100; ?>); <?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-style-6 .njba_front_inner {
                    <?php if( $settings->front_main_overly_bc_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->front_main_overly_bc_color)) ?>, <?php echo $settings->front_main_overly_color_opc/100; ?>); <?php } ?>
            }
            
            .fl-node-<?php echo $id; ?> .njba-style-6 .horizontal_title{
               <?php if( $settings->front_title_color ) { ?>color: #<?php echo $settings->front_title_color; ?>;<?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba_front_inner .njba_count{
               <?php if( $settings->front_date_color ) { ?>color: #<?php echo $settings->front_date_color; ?>;<?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-content-grid.njba-content-grid-wrapper{
                 <?php if( $settings->front_main_bc_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->front_main_bc_color)) ?>, <?php echo $settings->front_main_bc_color_opc/100; ?>); <?php } ?>
                  <?php if($settings->front_border_color) {?>border-color:<?php echo '#'.$settings->front_border_color;?>;<?php } ?>
            }
    <?php }
    }
?>
/*****Style 7 Overly****/
<?php if($settings->post_grid_style_select == "style-7"){
        if($settings->layout == 'flipbox'){?>
            .fl-node-<?php echo $id; ?> .njba-style-7 .njba-face.njba-back {
                     <?php if( $settings->back_gradient_bc_color_one && $settings->back_gradient_bc_color_two ) { ?>background: linear-gradient(<?php echo '#'.$settings->back_gradient_bc_color_one.', #'.$settings->back_gradient_bc_color_two;?>); <?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-style-7 .horizontal_title{
               <?php if( $settings->front_title_color ) { ?>color: #<?php echo $settings->front_title_color; ?>;<?php } ?>
            }
           .fl-node-<?php echo $id; ?> .njba-style-7 .njba_front_inner {
                    <?php if( $settings->front_main_overly_bc_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->front_main_overly_bc_color)) ?>, <?php echo $settings->front_main_overly_color_opc/100; ?>); <?php } ?>
            }
            
    <?php }else{?>
            .fl-node-<?php echo $id; ?> .njba-style-7 .njba-content-grid-contant.njba-content-grid-contant_section {
                    <?php if( $settings->back_gradient_bc_color_one && $settings->back_gradient_bc_color_two ) { ?>background: linear-gradient(<?php echo '#'.$settings->back_gradient_bc_color_one.', #'.$settings->back_gradient_bc_color_two;?>); <?php } ?>
            }
            .fl-node-<?php echo $id; ?> .njba-style-7 .horizontal_title{
               <?php if( $settings->front_title_color ) { ?>color: #<?php echo $settings->front_title_color; ?>;<?php } ?>
            }
           .fl-node-<?php echo $id; ?> .njba-style-7 .njba_front_inner {
                    <?php if( $settings->front_main_overly_bc_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->front_main_overly_bc_color)) ?>, <?php echo $settings->front_main_overly_color_opc/100; ?>); <?php } ?>
            }
            
    <?php }
    }
?>
.fl-node-<?php echo $id; ?> ul.njba-masonary-filters > li{
     <?php if( $settings->filter_default_color ) { ?>color: #<?php echo $settings->filter_default_color; ?>;<?php } ?>
     <?php if( $settings->filter_btn_default_bccolor ) { ?>background: #<?php echo $settings->filter_btn_default_bccolor; ?>;<?php } ?>
    <?php if( $settings->filter_button_font_family['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->filter_button_font_family ); ?><?php } ?>
    <?php if( $settings->filter_button_font_size['desktop'] != '' ) { ?>font-size: <?php echo $settings->filter_button_font_size['desktop']; ?>px;<?php } ?>
    <?php if( $settings->filter_button_border_style != 'none' ) { ?>border: <?php echo $settings->filter_button_border_width; ?>px <?php echo $settings->filter_button_border_style; ?> #<?php echo $settings->filter_button_border_color; ?>;<?php } ?>
    
    <!-- border-radius:<?php if ($settings->filter_button_border_radius['top'] != ''){ echo $settings->filter_button_border_radius['top'];?>px <?php } if ($settings->filter_button_border_radius['right'] != ''){  echo $settings->filter_button_border_radius['right'];?>px <?php } if ($settings->filter_button_border_radius['bottom'] != ''){ echo $settings->filter_button_border_radius['bottom'];?>px <?php } if ($settings->filter_button_border_radius['left'] != ''){ echo $settings->filter_button_border_radius['left'];?>px <?php } ?>;    --> 
    <?php if( $settings->filter_button_padding['top'] != '' ) { ?>padding-top:<?php echo $settings->filter_button_padding['top'] ?>px;<?php } ?>
    <?php if( $settings->filter_button_padding['right'] != '' ) { ?>padding-right:<?php echo $settings->filter_button_padding['right'] ?>px;<?php } ?>
    <?php if( $settings->filter_button_padding['bottom'] != '' ) { ?>padding-bottom:<?php echo $settings->filter_button_padding['bottom'] ?>px;<?php } ?>
    <?php if( $settings->filter_button_padding['left'] != '' ) { ?>padding-left:<?php echo $settings->filter_button_padding['left'] ?>px;<?php } ?>
}
.fl-node-<?php echo $id; ?> ul.njba-masonary-filters > li.njba-masonary-current,
.fl-node-<?php echo $id; ?> ul.njba-masonary-filters > li:hover{
     <?php if( $settings->filter_hover_color ) { ?>color: #<?php echo $settings->filter_hover_color; ?>;<?php } ?>
     <?php if( $settings->filter_btn_hover_bccolor ) { ?>background: #<?php echo $settings->filter_btn_hover_bccolor; ?>;<?php } ?>
   
         <?php if( $settings->filter_button_border_style != 'none' ) { ?>border: <?php echo $settings->filter_button_border_width; ?>px <?php echo $settings->filter_button_border_style; ?> #<?php echo $settings->filter_button_border_hover_color; ?>;<?php } ?>
    
}
@media ( max-width: 991px ) {
    .fl-node-<?php echo $id; ?> .njba-content-grid-contant <?php echo $settings->post_title_tag; ?> {
            <?php if( $settings->post_title_font_size['medium'] != '' ) { ?>font-size: <?php echo $settings->post_title_font_size['medium']; ?>px;<?php } ?>
            <?php if( $settings->post_title_height['medium'] != '' ) { ?>line-height: <?php echo $settings->post_title_height['medium']; ?>px;<?php } ?>
    } 
    .fl-node-<?php echo $id; ?> .njba-content-grid-contant <?php echo $settings->post_title_tag; ?> a{
           <?php if( $settings->post_title_font_size['medium'] != '' ) { ?>font-size: <?php echo $settings->post_title_font_size['medium']; ?>px;<?php } ?>
           <?php if( $settings->post_title_height['medium'] != '' ) { ?>line-height: <?php echo $settings->post_title_height['medium']; ?>px;<?php } ?>
    } 
   .fl-node-<?php echo $id; ?> .njba-content-grid-contant p {
            <?php if( $settings->post_content_font_size['medium'] != ''  ) { ?>font-size: <?php echo $settings->post_content_font_size['medium']; ?>px;<?php } ?>
            <?php if( $settings->post_content_height['medium'] != ''  ) { ?>line-height: <?php echo $settings->post_content_height['medium']; ?>px;<?php } ?>
    } 
    .fl-node-<?php echo $id; ?> .njba-content-grid-contant ul li a, 
    .fl-node-<?php echo $id; ?> .njba-content-grid-contant ul li,
    .fl-node-<?php echo $id; ?> .njba-content-grid-contant ul li span{
            <?php if( $settings->post_date_font_size['medium'] != '' ) { ?>font-size: <?php echo $settings->post_date_font_size['medium']; ?>px;<?php } ?>
            <?php if( $settings->post_date_height['medium'] != '' ) { ?>line-height: <?php echo $settings->post_date_height['medium']; ?>px;<?php } ?>
    } 
    .fl-node-<?php echo $id; ?> ul.njba-masonary-filters > li{
        
       <?php if( $settings->filter_button_font_size['medium'] != '' ) { ?>font-size: <?php echo $settings->filter_button_font_size['medium']; ?>px;<?php } ?>
        
    }
    .fl-node-<?php echo $id; ?> .njba-pagination li a.page-numbers,
    .fl-node-<?php echo $id; ?> .njba-pagination li span.page-numbers{
               <?php if( $settings->pagination_font_size['medium'] != '' ) { ?>font-size: <?php echo $settings->pagination_font_size['medium']; ?>px;<?php } ?>
    }
    .fl-node-<?php echo $id; ?> <?php echo $settings->post_title_tag; ?>.horizontal_title, {
             <?php if( $settings->post_title_font_size['medium'] != '' ) { ?>font-size: <?php echo $settings->post_title_font_size['medium']; ?>px;<?php } ?>
            <?php if( $settings->post_title_height['medium'] != '' ) { ?>line-height: <?php echo $settings->post_title_height['medium']; ?>px;<?php } ?>
    } 
}
@media ( max-width: 767px ) {
  .fl-node-<?php echo $id; ?> .njba-content-grid-contant <?php echo $settings->post_title_tag; ?> {
            <?php if( $settings->post_title_font_size['small'] != '' ) { ?>font-size: <?php echo $settings->post_title_font_size['small']; ?>px;<?php } ?>
            <?php if( $settings->post_title_height['small'] != '' ) { ?>line-height: <?php echo $settings->post_title_height['small']; ?>px;<?php } ?>
    } 
    .fl-node-<?php echo $id; ?> .njba-content-grid-contant <?php echo $settings->post_title_tag; ?> a{
           <?php if( $settings->post_title_font_size['small'] != '' ) { ?>font-size: <?php echo $settings->post_title_font_size['small']; ?>px;<?php } ?>
           <?php if( $settings->post_title_height['small'] != '' ) { ?>line-height: <?php echo $settings->post_title_height['small']; ?>px;<?php } ?>
    } 
   .fl-node-<?php echo $id; ?> .njba-content-grid-contant p {
            <?php if( $settings->post_content_font_size['small'] != ''  ) { ?>font-size: <?php echo $settings->post_content_font_size['small']; ?>px;<?php } ?>
            <?php if( $settings->post_content_height['small'] != ''  ) { ?>line-height: <?php echo $settings->post_content_height['small']; ?>px;<?php } ?>
    } 
    .fl-node-<?php echo $id; ?> .njba-content-grid-section-wrapper ul li a, 
    .fl-node-<?php echo $id; ?> .njba-content-grid-section-wrapper ul li,
    .fl-node-<?php echo $id; ?> .njba-content-grid-section-wrapper ul li span{
            <?php if( $settings->post_date_font_size['small'] != '' ) { ?>font-size: <?php echo $settings->post_date_font_size['small']; ?>px;<?php } ?>
            <?php if( $settings->post_date_height['small'] != '' ) { ?>line-height: <?php echo $settings->post_date_height['small']; ?>px;<?php } ?>
    } 
    .fl-node-<?php echo $id; ?> ul.njba-masonary-filters > li{
        
       <?php if( $settings->filter_button_font_size['small'] != '' ) { ?>font-size: <?php echo $settings->filter_button_font_size['small']; ?>px;<?php } ?>
        
    }
    .fl-node-<?php echo $id; ?> .njba-pagination li a.page-numbers,
    .fl-node-<?php echo $id; ?> .njba-pagination li span.page-numbers{
               <?php if( $settings->pagination_font_size['small'] != '' ) { ?>font-size: <?php echo $settings->pagination_font_size['small']; ?>px;<?php } ?>
    }
    .fl-node-<?php echo $id; ?> <?php echo $settings->post_title_tag; ?>.horizontal_title, {
             <?php if( $settings->post_title_font_size['small'] != '' ) { ?>font-size: <?php echo $settings->post_title_font_size['small']; ?>px;<?php } ?>
            <?php if( $settings->post_title_height['small'] != '' ) { ?>line-height: <?php echo $settings->post_title_height['small']; ?>px;<?php } ?>
    } 
}