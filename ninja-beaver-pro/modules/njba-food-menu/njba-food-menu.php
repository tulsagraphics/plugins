<?php
/**
 * @class NJBAFoodMenuModule
 */
		
class NJBAFoodMenuModule extends FLBuilderModule {
	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('Food Menu', 'bb-njba'),
			'description'   	=> __('Addon to display Resturant Food Menu.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat(),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-food-menu/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-food-menu/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
		));
	}
	
}
/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('NJBAFoodMenuModule', array(
	 'left_menu'      => array( // Tab
		'title'         => __('Left Menu Page', 'bb-njba'), // Tab title
		'sections'      => array( // Tab Sections
            'separator'      => array(
                'title'     => '',
                'fields'    => array(
                    'left_menu_name_panels'  => array(
                        'type'  => 'form',
                        'label' => __('Menu Name', 'bb-njba'),
                        'form'  => 'left_menu_form',
                        'preview_text'  => 'left_menu_name',
                        'multiple'  => true
                    ),
                ),
            ),
		)
	),
    /* 'middle_menu'      => array( // Tab
        'title'         => __('Middel Menu Page', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'separator'      => array(
                'title'     => '',
                'fields'    => array(
                    'middel_menu_name_panels'  => array(
                        'type'  => 'form',
                        'label' => __('Menu Name', 'bb-njba'),
                        'form'  => 'middel_menu_form',
                        'preview_text'  => 'middel_menu_name',
                        'multiple'  => true
                    ),
                ),
            ),
        )
    ),
     'right_menu'      => array( // Tab
        'title'         => __('Right Menu Page', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'separator'      => array(
                'title'     => '',
                'fields'    => array(
                    'right_menu_name_panels'  => array(
                        'type'  => 'form',
                        'label' => __('Menu Name', 'bb-njba'),
                        'form'  => 'right_menu_form',
                        'preview_text'  => 'right_menu_name',
                        'multiple'  => true
                    ),
                ),
            ),
        )
    ),*/
	 
));
/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('left_menu_form', array(
	'title' => __('Left Menu List', 'bb-njba'),
	'tabs'  => array(
		'general'      => array( // Tab
			'left_menu_name'         => __('Menu', 'bb-njba'), // Tab title
			'sections'      => array( // Tab Sections
                'content'          => array(
                    'left_menu_name'      => '',
                    'fields'     => array(
                        'left_menu_name'     => array(
                            'type'          => 'text',
                            'label'         => __('Menu Name', 'bb-njba')
                        ),
                        'left_menu_price'     => array(
                            'type'          => 'text',
                            'label'         => __('Price', 'bb-njba'),
                        ),
                        'left_menu_description'     => array(
                            'type'          => 'text',
                            'label'         => __('Description', 'bb-njba'),
                        ),
                   
                ),
                
			)
		),
	)
)
));
/*FLBuilder::register_settings_form('middel_menu_form', array(
    'title' => __('Middel Menu List', 'bb-njba'),
    'tabs'  => array(
        'general'      => array( // Tab
            'middel_menu_name'         => __('Menu', 'bb-njba'), // Tab title
            'sections'      => array( // Tab Sections
                'content'          => array(
                    'middel_menu_name'      => '',
                    'fields'     => array(
                        'middel_menu_name'     => array(
                            'type'          => 'text',
                            'label'         => __('Menu Name', 'bb-njba')
                        ),
                        'middel_menu_price'     => array(
                            'type'          => 'text',
                            'label'         => __('Price', 'bb-njba'),
                        ),
                        'middel_menu_description'     => array(
                            'type'          => 'text',
                            'label'         => __('Description', 'bb-njba'),
                        ),
                   
                ),
                
            )
        ),
    )
)
));
FLBuilder::register_settings_form('right_menu_form', array(
    'title' => __('Right Menu List', 'bb-njba'),
    'tabs'  => array(
        'general'      => array( // Tab
            'right_menu_name'         => __('Menu', 'bb-njba'), // Tab title
            'sections'      => array( // Tab Sections
                'content'          => array(
                    'right_menu_name'      => '',
                    'fields'     => array(
                        'right_menu_name'     => array(
                            'type'          => 'text',
                            'label'         => __('Menu Name', 'bb-njba')
                        ),
                        'right_menu_price'     => array(
                            'type'          => 'text',
                            'label'         => __('Price', 'bb-njba'),
                        ),
                        'right_menu_description'     => array(
                            'type'          => 'text',
                            'label'         => __('Description', 'bb-njba'),
                        ),
                   
                ),
                
            )
        ),
    )
)
));*/