<?php
/**
 * @class NJBATimelineModule
 */
class NJBATimelineModule extends FLBuilderModule {
    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Timeline', 'bb-njba'),
            'description'   => __('Addon to display content in timeline format.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'creative' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-timeline/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-timeline/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
            'partial_refresh'   => true,
        ));
    }
}
/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('NJBATimelineModule', array(
    'general'      => array( // Tab
        'title'         => __('General', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'timeline_structure_section'      => array(
                'title'     => '',
                'fields'    => array(
                    'timeline_structure'    => array(
                        'type'      => 'select',
                        'label'     => __('Timeline Start','bb-njba'),
                        'default'   => 'left',
                        'options'   => array(
                            'left' => __('Left', 'bb-njba'),
                            'right' => __('Right', 'bb-njba')
                        ),
                    ),
                    'timeline_start_box'    => array(
                        'type'      => 'select',
                        'label'     => __('Start Box Display','bb-njba'),
                        'default'   => 'yes',
                        'options'   => array(
                            'yes' => __('Yes', 'bb-njba'),
                            'no' => __('No', 'bb-njba')
                        ),
                        'toggle'    => array(
                            'yes' => array(
                                'fields'    => array('timeline_start_box_text'),
                            ),
                        ),
                    ),
                    'timeline_start_box_text'   => array(
                        'type'      => 'text',
                        'label'     => __('Start Text', 'bb-njba'),
                        'default'   => 'Start',
                    ),
                    'timeline_end_box'    => array(
                        'type'      => 'select',
                        'label'     => __('End Box Display','bb-njba'),
                        'default'   => 'yes',
                        'options'   => array(
                            'yes' => __('Yes', 'bb-njba'),
                            'no' => __('No', 'bb-njba')
                        ),
                        'toggle'    => array(
                            'yes' => array(
                                'fields'    => array('timeline_end_box_text'),
                            ),
                        ),
                    ),
                    'timeline_end_box_text'   => array(
                        'type'      => 'text',
                        'label'     => __('End Text', 'bb-njba'),
                        'default'   => 'End',
                    ),
                ),
            ),
            'connector_styling'   => array(
                'title'     => __('Connector Styling', 'bb-njba'),
                'fields'    => array(
                    'timeline_line_style'    => array(
                        'type'      => 'select',
                        'label'     => __('Connector Type', 'bb-njba'),
                        'default'   => 'solid',
                        'options'   => array(
                            'solid' => __('Solid', 'bb-njba'),
                            'dashed' => __('Dashed', 'bb-njba'),
                            'dotted' => __('Dotted', 'bb-njba'),
                        ),
                    ),
                    'timeline_line_width'   => array(
                        'type'      => 'text',
                        'label'     => __('Width', 'bb-njba'),
                        'size'      => 5,
                        'maxlength' => 2,
                        'default'   => 1,
                        'description'   => 'px'
                    ),
                    'timeline_line_color'   => array(
                        'type'      => 'color',
                        'label'     => __('Color', 'bb-njba'),
                        'show_reset'    => true,
                        'default'   => '000000'
                    ),
                ),
            ),
        )
    ),
    'content'      => array( // Tab
        'title'         => __('Content', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'general'      => array(
                'title'     => '',
                'fields'    => array(
                    'timeline'  => array(
                        'type'  => 'form',
                        'label' => __('Item', 'bb-njba'),
                        'form'  => 'njba_timeline_form',
                        'preview_text'  => 'title',
                        'multiple'      => true,
                        'preview'      => array(
                            'type'         => 'css',
                            'selector'     => '.njba-vertical-line',
                            'property'     => 'border-color'
                        )
                    ),
                ),
            ),
        )
    ),
    'style'       => array(
        'title' => __('Style', 'bb-njba'),
        'sections'  => array(
            'title' => array(
                'title' => __('Title', 'bb-njba'),
                'fields'    => array(
                    'title_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 10,
                            'right'         => 10,
                            'bottom'       => 10,
                            'left'      => 10
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                ),
            ),
            'content' => array(
                'title' => __('Content', 'bb-njba'),
                'fields'    => array(
                    'content_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 10,
                            'right'         => 10,
                            'bottom'       => 10,
                            'left'      => 10
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                ),
            ),
            'timeline_content_icon_style'  => array(
                'title' => __('Content Icon', 'bb-njba'),
                'fields'    => array(
                    'timeline_content_icon_margin'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 10,
                            'right'         => 10,
                            'bottom'       => 10,
                            'left'      => 10
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                ),
            ),
            'box_styling'   => array(
                'title' => __('Box Styling', 'bb-njba'),
                'fields'    => array(
                    'timeline_box_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 20,
                            'right'         => 20,
                            'bottom'       => 20,
                            'left'      => 20
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                ),
            ),
            'timeline_start_end_style'  => array(
                'title' => __('Timeline Start & End Box Style', 'bb-njba'),
                'fields'    => array(
                    'timeline_start_end_color'    => array(
                            'type'  => 'color',
                            'label' => __('Color', 'bb-njba'),
                            'show_reset'    => true,
                            'default'       => '008e6d',
                            'preview'      => array(
                                'type'         => 'css',
                                'selector'     => '.end_box,.start_box',
                                'property'     => 'color'
                            )
                    ),
                    'timeline_start_end_background_color'    => array(
                        'type'  => 'color',
                        'label' => __('Background Color', 'bb-njba'),
                        'show_reset'    => true,
                        'default'       => 'ffffff',
                        'preview'      => array(
                                'type'         => 'css',
                                'selector'     => '.end_box,.start_box',
                                'property'     => 'background'
                            )
                    ),
                    'timeline_start_end_border_style'  => array(
                        'type'      => 'select',
                        'label'     => __('Border Style', 'bb-njba'),
                        'default'   => 'solid',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                        ),
                        'toggle'    => array(
                            'solid' => array(
                                'fields'    => array('timeline_icon_border_color', 'timeline_icon_border_width'),
                            ),
                            'dashed' => array(
                                'fields'    => array('timeline_icon_border_color', 'timeline_icon_border_width'),
                            ),
                            'dotted' => array(
                                'fields'    => array('timeline_icon_border_color', 'timeline_icon_border_width'),
                            ),
                        ),
                    ),
                    'timeline_start_end_border_width' => array(
                        'type'  => 'text',
                        'label' => __('Border Width', 'bb-njba'),
                        'size'  => 5,
                        'maxlength' => 3,
                        'default'   => 2,
                        'description'   => 'px',
                    ),
                    'timeline_start_end_border_color' => array(
                        'type'      => 'color',
                        'label'     => __('Border Color', 'bb-njba'),
                        'show_reset'    => true,
                        'default'       => '00964d',
                        'preview'      => array(
                            'type'         => 'css',
                            'selector'     => '.end_box,.start_box',
                            'property'     => 'border-color'
                        )
                    ),
                    'timeline_start_end_border_radius'    => array(
                        'type'      => 'text',
                        'label'     => __('Round Corners', 'bb-njba'),
                        'size'      => 5,
                        'maxlength' => 3,
                        'default'   => 0,
                        'description'   => 'px',
                    ),
                    'timeline_start_end_height'    => array(
                        'type'      => 'text',
                        'label'     => __('Height', 'bb-njba'),
                        'size'      => 5,
                        'maxlength' => 3,
                        'default'   => 50,
                        'description'   => 'px',
                    ),
                ),
            ),
            'button_style_section' => array(
                'title' => __('Button','bb-njba'),
                'fields' => array(
                    'button_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 20,
                            'right'         => 20,
                            'bottom'       => 20,
                            'left'      => 20
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'button_margin'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Button Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 0,
                            'right'         => 10,
                            'bottom'       => 10,
                            'left'      => 10
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )       
                        )
                    ),
                )
            )
        ),
    ),
    'timeline_typography'    => array(
        'title'     => __('Typography', 'bb-njba'),
        'sections'  => array(
            'timeline_title_typography'    => array(
                'title'     => __('Title', 'bb-njba'),
                'fields'  => array(
                    'timeline_title_font' => array(
                        'type'  => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                    ),
                    'timeline_title_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '24',
                            'medium' => '20',
                            'small' => '16'
                        )
                    ),
                    'timeline_title_line_height'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Line Height', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '24',
                            'medium' => '20',
                            'small' => '16'
                        )
                    ),
                ),
            ),
            'timeline_text_typography'   => array(
                'title'     => __('Text', 'bb-njba'),
                'fields'    => array(
                    'timeline_text_font' => array(
                        'type'  => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                    ),
                    'timeline_text_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '18',
                            'medium' => '16',
                            'small' => '14'
                        )
                    ),
                    'timeline_text_line_height'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Line Height', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '24',
                            'medium' => '22',
                            'small' => '18'
                        )
                    )
                ),
            ),
            'button_typography' => array(
                'title'     => __('Button', 'bb-njba'),
                'fields'    => array(
                    'button_font_family' => array(
                        'type'  => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                    ),
                    'button_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '18',
                            'medium' => '16',
                            'small' => '14'
                        )
                    ),
                ),
            ),
            'timeline_content_icon_typography' => array(
                'title'     => __('Content Icon', 'bb-njba'),
                'fields'    => array(
                    'timeline_content_icon_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '50',
                            'medium' => '36',
                            'small' => '24'
                        )
                    ),
                ),
            ),
            'timeline_icon_typography' => array(
                'title'     => __('Timeline Icon', 'bb-njba'),
                'fields'    => array(
                    'timeline_icon_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '36',
                            'medium' => '24',
                            'small' => '18'
                        )
                    ),
                ),
            ),
            'timeline_year_typography' => array(
                'title'     => __('Timeline Label', 'bb-njba'),
                'fields'    => array(
                    'timeline_year_font_family' => array(
                        'type'  => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                    ),
                    'timeline_year_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '18',
                            'medium' => '16',
                            'small' => '14'
                        )
                    ),
                ),
            ),
            'timeline_start_end_typography' => array(
                'title'     => __('Timeline Start & End Box', 'bb-njba'),
                'fields'    => array(
                    'timeline_start_end_font_family' => array(
                        'type'  => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => ''
                        )
                    ),
                    'timeline_start_end_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '18',
                            'medium' => '16',
                            'small' => '14'
                        )
                    ),
                ),
            ),
        ),
    ),
));
/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('njba_timeline_form', array(
    'title' => __('Add Item', 'bb-njba'),
    'tabs'  => array(
        'general'      => array( // Tab
            'title'         => __('General', 'bb-njba'), // Tab title
            'sections'      => array( // Tab Sections
                'title'          => array(
                    'title'      => __('Title', 'bb-njba'),
                    'fields'     => array(
                        'title'     => array(
                            'type'          => 'text',
                            'label'         => __('Title','bb-njba'),
                            'default'       => "Title",
                            'preview'       => array(
                                'type'          => 'text',
                                'selector'      => '.njba-timeline-content'
                            )
                        ),
                    ),
                ),
                'box_icon'          => array(
                    'title'      => __('Box Icon', 'bb-njba'),
                    'fields'     => array(
                        'content_icon_image'       => array(
                            'type'          => 'select',
                            'label'         => __('Icon Type', 'bb-njba'),
                            'default'       => 'none',
                            'options'       => array(
                                'none'              => __('None', 'bb-njba'),
                                'font_icon'         => __('Icon', 'bb-njba'),
                                'custom_icon'       => __('Image', 'bb-njba')
                            ),
                            'toggle' => array(
                                'font_icon'    => array(
                                    'fields'   => array('content_icon'),
                                    'sections' =>array('timeline_content_icon_style_form') 
                                ),
                                'custom_icon'   => array(
                                    'fields'  => array('content_image'),
                                    'sections' => array('timeline_content_icon_style_form')
                                ),
                                'none'   => array(
                                    'fields'  => array(''),
                                    'sections' => array('')
                                ),
                            )
                        ),
                        'content_icon'          => array(
                            'type'          => 'icon',
                            'label'         => __('Icon', 'bb-njba')
                        ),
                        'content_image'     => array(
                            'type'              => 'photo',
                            'label'         => __('Custom Image', 'bb-njba'),
                        ),
                    ),
                ),
                'timeline_year_section'  => array(
                    'title'     => 'Timeline Label',
                    'fields'    => array(
                        'display_year'       => array(
                            'type'          => 'select',
                            'label'         => __('Display Timeline Label', 'bb-njba'),
                            'default'       => 'yes',
                            'options'       => array(
                                'yes'              => __('Yes', 'bb-njba'),
                                'no'         => __('No', 'bb-njba')
                            ),
                            'toggle' => array(
                                'yes'    => array(
                                    'fields'   => array('timeline_year'),
                                    'sections' => array('timeline_year_style')
                                ),
                                'no'   => array(
                                    'fields'  => array('')
                                ),
                            )
                        ),
                        'timeline_year'          => array(
                            'type'          => 'text',
                            'label'         => __('Text', 'bb-njba'),
                            'default'       => '2017',
                            'preview'       => array(
                                'type'          => 'text',
                                'selector'      => '.njba-timeline-date-selector'
                            )
                        ),
                    ),
                ),
                'content'       => array( // Section
                    'title'         => __('Content', 'bb-njba'), // Section Title
                    'fields'        => array( // Section Fields
                        'content'          => array(
                            'type'          => 'editor',
                            'label'         => '',
                            'default'       => 'Enter description here'
                        )
                    )
                ),
                'button'    => array(
                    'title' => __('Button', 'bb-njba'),
                    'fields'    => array(
                        'display_button'       => array(
                            'type'          => 'select',
                            'label'         => __('Display Button', 'bb-njba'),
                            'default'       => 'no',
                            'options'       => array(
                                'yes'              => __('Yes', 'bb-njba'),
                                'no'         => __('No', 'bb-njba')
                            ),
                            'toggle' => array(
                                'yes'    => array(
                                    'fields'   => array('button_text','link','link_target'),
                                    'sections' => array('button_style_section_form')
                                ),
                                'no'   => array(
                                    'fields'  => array(''),
                                    'sections' => array('')
                                ),
                            )
                        ),
                        'button_text'     => array(
                            'type'      => 'text',
                            'label'     => 'Text',
                            'default'   => __('Read More', 'bb-njba'),
                            'preview'       => array(
                                'type'          => 'text',
                                'selector'      => ''
                            )
                        ),
                        'link'     => array(
                            'type'          => 'link',
                            'label'         =>  __('Link', 'bb-njba'),
                            'default'       =>  __('#', 'bb-njba'),
                            'placeholder'   => 'www.example.com',
                        ),
                        'link_target'   => array(
                            'type'          => 'select',
                            'label'         =>  __('Link Target', 'bb-njba'),
                            'default'       =>  __('_self', 'bb-njba'),
                            'placeholder'   => 'www.example.com',
                            'options'   => array(
                                '_self'     =>  __('Same Window', 'bb-njba'),   
                                '_blank'    =>  __('New Window', 'bb-njba'),   
                            ),
                        )
                    ),
                ),
            )
        ),
        'timeline_icon_tab'  => array(
            'title' => __('Timeline Icon', 'bb-njba'),
            'sections'  => array(
                'timeline_icon'  => array(
                    'title'     => '',
                    'fields'    => array(
                        'timeline_icon_image'       => array(
                            'type'          => 'select',
                            'label'         => __('Timeline Icon Type', 'bb-njba'),
                            'default'       => 'none',
                            'options'       => array(
                                'none'              => __('None', 'bb-njba'),
                                'font_icon'         => __('Icon', 'bb-njba'),
                                'custom_icon'       => __('Image', 'bb-njba')
                            ),
                            'toggle' => array(
                                'font_icon'    => array(
                                    'fields'   => array('timeline_icon'),
                                    'sections' => array('timeline_icon_style')
                                ),
                                'custom_icon'   => array(
                                    'fields'  => array('timeline_image'),
                                    'sections' => array('timeline_icon_style')
                                ),
                            )
                        ),
                        'timeline_icon'          => array(
                            'type'          => 'icon',
                            'label'         => __('Icon', 'bb-njba')
                        ),
                        'timeline_image'     => array(
                            'type'              => 'photo',
                            'label'         => __('Custom Image', 'bb-njba'),
                        ),
                    ),
                ),
                'timeline_icon_style'  => array(
                    'title' => __('', 'bb-njba'),
                    'fields'    => array(
                        'timeline_icon_color'    => array(
                                'type'  => 'color',
                                'label' => __('Color', 'bb-njba'),
                                'show_reset'    => true,
                                'default'       => 'ffffff'
                        ),
                        'timeline_icon_background_color'    => array(
                            'type'  => 'color',
                            'label' => __('Background Color', 'bb-njba'),
                            'show_reset'    => true,
                            'default'       => '007c5f'
                        ),
                        'timeline_icon_border_style'  => array(
                            'type'      => 'select',
                            'label'     => __('Border Style', 'bb-njba'),
                            'default'   => 'none',
                            'options'   => array(
                                'none'  => __('None', 'bb-njba'),
                                'solid'  => __('Solid', 'bb-njba'),
                                'dashed'  => __('Dashed', 'bb-njba'),
                                'dotted'  => __('Dotted', 'bb-njba'),
                            ),
                            'toggle'    => array(
                                'solid' => array(
                                    'fields'    => array('timeline_icon_border_color', 'timeline_icon_border_width'),
                                ),
                                'dashed' => array(
                                    'fields'    => array('timeline_icon_border_color', 'timeline_icon_border_width'),
                                ),
                                'dotted' => array(
                                    'fields'    => array('timeline_icon_border_color', 'timeline_icon_border_width'),
                                ),
                            ),
                        ),
                        'timeline_icon_border_width' => array(
                            'type'  => 'text',
                            'label' => __('Border Width', 'bb-njba'),
                            'size'  => 5,
                            'maxlength' => 3,
                            'default'   => 1,
                            'description'   => 'px',
                        ),
                        'timeline_icon_border_color' => array(
                            'type'      => 'color',
                            'label'     => __('Border Color', 'bb-njba'),
                            'show_reset'    => true,
                            'default'       => '0a0000'
                        ),
                        'timeline_icon_border_radius'    => array(
                            'type'      => 'text',
                            'label'     => __('Round Corners', 'bb-njba'),
                            'size'      => 5,
                            'maxlength' => 3,
                            'default'   => 25,
                            'description'   => 'px',
                        ),
                        'timeline_icon_height'    => array(
                            'type'      => 'text',
                            'label'     => __('Height', 'bb-njba'),
                            'size'      => 5,
                            'maxlength' => 3,
                            'default'   => 50,
                            'description'   => 'px',
                        ),
                        'timeline_icon_width'    => array(
                            'type'      => 'text',
                            'label'     => __('Width', 'bb-njba'),
                            'size'      => 5,
                            'maxlength' => 3,
                            'default'   => 50,
                            'description'   => 'px',
                        ),
                    ),
                ),
            ),
        ),
        'style'       => array(
            'title' => __('Style', 'bb-njba'),
            'sections'  => array(
                'arrow' => array(
                    'title' => __('Arrow', 'bb-njba'),
                    'fields'    => array(
                        'arrow_color'  => array(
                            'type'  => 'color',
                            'label' => __('Color', 'bb-njba'),
                            'show_reset'    => true,
                            'default'   => '3babb3'
                        )
                    ),
                ),
                'title' => array(
                    'title' => __('Title', 'bb-njba'),
                    'fields'    => array(
                        'title_color'  => array(
                            'type'  => 'color',
                            'label' => __('Color', 'bb-njba'),
                            'show_reset'    => true,
                            'default'   => '000000'
                        ),
                        'title_background_color'  => array(
                            'type'  => 'color',
                            'label' => __('Background Color', 'bb-njba'),
                            'show_reset'    => true,
                            'default'   => '000000'
                        ),
                    ),
                ),
                'content' => array(
                    'title' => __('Content', 'bb-njba'),
                    'fields'    => array(
                        'text_color'    => array(
                            'type'      => 'color',
                            'label'     => __('Text Color', 'bb-njba'),
                            'show_reset'    => true,
                            'default'   => '000000'
                        ),
                    ),
                ),
                'box_styling'   => array(
                    'title' => __('Box Styling', 'bb-njba'),
                    'fields'    => array(
                        'timeline_box_background'   => array(
                            'type'  => 'color',
                            'label' => __('Background Color', 'bb-njba'),
                            'show_reset'    => true,
                            'default'       => '00afaa'
                        ),
                        'timeline_box_border_type'   => array(
                            'type'  => 'select',
                            'label' => __('Border Style', 'bb-njba'),
                            'default'    => 'none',
                            'options'   => array(
                                'none'  => __('None', 'bb-njba'),
                                'solid'  => __('Solid', 'bb-njba'),
                                'dashed'  => __('Dashed', 'bb-njba'),
                                'dotted'  => __('Dotted', 'bb-njba'),
                            ),
                            'toggle'    => array(
                                'dashed' => array(
                                    'fields'    => array('timeline_box_border_color', 'timeline_box_border_width'),
                                ),
                                'dotted' => array(
                                    'fields'    => array('timeline_box_border_color', 'timeline_box_border_width'),
                                ),
                                'solid' => array(
                                    'fields'    => array('timeline_box_border_color', 'timeline_box_border_width'),
                                ),
                            ),
                        ),
                        'timeline_box_border_width' => array(
                            'type'  => 'text',
                            'label' => __('Border Width', 'bb-njba'),
                            'size'  => 5,
                            'maxlength' => 3,
                            'default'   => 0,
                            'description'   => 'px',
                        ),
                        'timeline_box_border_color' => array(
                            'type'  => 'color',
                            'label' => __('Border Color', 'bb-njba'),
                            'show_reset'    => true,
                        ),
                        'timeline_box_border_radius' => array(
                            'type'  => 'text',
                            'label' => __('Round Corners', 'bb-njba'),
                            'size'  => 5,
                            'maxlength' => 3,
                            'default'   => 0,
                            'description'   => 'px',
                        ),
                        'timeline_box_shadow'   => array(
                            'type'      => 'select',
                            'label'     => __('Display Box Shadow?', 'bb-njba'),
                            'default'   => 'no',
                            'options'   => array(
                                'yes'    => __('Yes', 'bb-njba'),
                                'no'    => __('No', 'bb-njba'),
                            ),
                            'toggle'    => array(
                                'yes'   => array(
                                    'fields'    => array('timeline_box_shadow_options', 'timeline_box_shadow_color'),
                                ),
                            ),
                        ),
                        'timeline_box_shadow_options'      => array(
                            'type'              => 'njba-multinumber',
                            'label'             => __('Box Shadow', 'bb-njba'),
                            'description'       => 'px',
                            'default'           => array(
                                'left_right'          => 1,
                                'top_bottom'         => 2,
                                'blur'       => 2,
                                'spread'      => 0
                            ),
                            'options'           => array(
                                'left_right'               => array(
                                    'placeholder'       => __('Top', 'bb-njba'),
                                    'icon'              => 'fa fa-arrows-h'
                                ),
                                'top_bottom'            => array(
                                    'placeholder'       => __('Right', 'bb-njba'),
                                    'icon'              => 'fa fa-arrows-v'
                                ),
                                'blur'            => array(
                                    'placeholder'       => __('Bottom', 'bb-njba'),
                                    'icon'              => 'fa fa-circle-thin'
                                ),
                                'spread'            => array(
                                    'placeholder'       => __('Left', 'bb-njba'),
                                    'icon'              => 'fa fa-circle'
                                )
                                
                            )
                        ),
                        'timeline_box_shadow_color' => array(
                            'type'              => 'color',
                            'label'             => __('Color', 'bb-njba'),
                            'default'           => '007c5f',
                        )
                    ),
                ),
                'timeline_content_icon_style_form'  => array(
                    'title' => __('Content Icon', 'bb-njba'),
                    'fields'    => array(
                        'timeline_content_icon_color'    => array(
                                'type'  => 'color',
                                'label' => __('Color', 'bb-njba'),
                                'show_reset'    => true,
                                'default'       => '007c5f'
                        ),
                        'timeline_content_icon_background_color'    => array(
                            'type'  => 'color',
                            'label' => __('Background Color', 'bb-njba'),
                            'show_reset'    => true,
                            'default'       => '707070'
                        ),
                        'timeline_content_icon_border_style'  => array(
                            'type'      => 'select',
                            'label'     => __('Border Style', 'bb-njba'),
                            'default'   => 'solid',
                            'options'   => array(
                                'none'  => __('None', 'bb-njba'),
                                'solid'  => __('Solid', 'bb-njba'),
                                'dashed'  => __('Dashed', 'bb-njba'),
                                'dotted'  => __('Dotted', 'bb-njba'),
                            ),
                            'toggle'    => array(
                                'solid' => array(
                                    'fields'    => array('content_icon_border_color', 'content_icon_border_width'),
                                ),
                                'dashed' => array(
                                    'fields'    => array('content_icon_border_color', 'content_icon_border_width'),
                                ),
                                'dotted' => array(
                                    'fields'    => array('content_icon_border_color', 'content_icon_border_width'),
                                ),
                            ),
                        ),
                        'timeline_content_icon_border_width' => array(
                            'type'  => 'text',
                            'label' => __('Border Width', 'bb-njba'),
                            'size'  => 5,
                            'maxlength' => 3,
                            'default'   => 10,
                            'description'   => 'px',
                        ),
                        'timeline_content_icon_border_color' => array(
                            'type'      => 'color',
                            'label'     => __('Border Color', 'bb-njba'),
                            'show_reset'    => true,
                            'default'       => '0a0000'
                        ),
                        'timeline_content_icon_border_radius'    => array(
                            'type'      => 'text',
                            'label'     => __('Round Corners', 'bb-njba'),
                            'size'      => 5,
                            'maxlength' => 3,
                            'default'   => 75,
                            'description'   => 'px',
                        ),
                        'timeline_content_icon_height'    => array(
                            'type'      => 'text',
                            'label'     => __('Height', 'bb-njba'),
                            'size'      => 5,
                            'maxlength' => 3,
                            'default'   => 150,
                            'description'   => 'px',
                        ),
                        'timeline_content_icon_width'    => array(
                            'type'      => 'text',
                            'label'     => __('Width', 'bb-njba'),
                            'size'      => 5,
                            'maxlength' => 3,
                            'default'   => 150,
                            'description'   => 'px',
                        ),
                    ),
                ),
                'timeline_year_style'  => array(
                    'title' => __('Timeline Label Style', 'bb-njba'),
                    'fields'    => array(
                        'timeline_arrow_color'    => array(
                                'type'  => 'color',
                                'label' => __('Arrow Color', 'bb-njba'),
                                'show_reset'    => true,
                                'default'       => '008e6d'
                        ),
                        'timeline_year_color'    => array(
                                'type'  => 'color',
                                'label' => __('Color', 'bb-njba'),
                                'show_reset'    => true,
                                'default'       => '008e6d'
                        ),
                        'timeline_year_background_color'    => array(
                            'type'  => 'color',
                            'label' => __('Background Color', 'bb-njba'),
                            'show_reset'    => true,
                            'default'       => 'ffffff'
                        ),
                        'timeline_year_border_style'  => array(
                            'type'      => 'select',
                            'label'     => __('Border Style', 'bb-njba'),
                            'default'   => 'solid',
                            'options'   => array(
                                'none'  => __('None', 'bb-njba'),
                                'solid'  => __('Solid', 'bb-njba'),
                                'dashed'  => __('Dashed', 'bb-njba'),
                                'dotted'  => __('Dotted', 'bb-njba'),
                            ),
                            'toggle'    => array(
                                'solid' => array(
                                    'fields'    => array('timeline_icon_border_color', 'timeline_icon_border_width'),
                                ),
                                'dashed' => array(
                                    'fields'    => array('timeline_icon_border_color', 'timeline_icon_border_width'),
                                ),
                                'dotted' => array(
                                    'fields'    => array('timeline_icon_border_color', 'timeline_icon_border_width'),
                                ),
                            ),
                        ),
                        'timeline_year_border_width' => array(
                            'type'  => 'text',
                            'label' => __('Border Width', 'bb-njba'),
                            'size'  => 5,
                            'maxlength' => 3,
                            'default'   => 2,
                            'description'   => 'px',
                        ),
                        'timeline_year_border_color' => array(
                            'type'      => 'color',
                            'label'     => __('Border Color', 'bb-njba'),
                            'show_reset'    => true,
                            'default'       => '00964d'
                        ),
                        'timeline_year_border_radius'    => array(
                            'type'      => 'text',
                            'label'     => __('Round Corners', 'bb-njba'),
                            'size'      => 5,
                            'maxlength' => 3,
                            'default'   => 0,
                            'description'   => 'px',
                        )
                    ),
                ),
                'button_style_section_form' => array(
                    'title' => __('Button','bb-njba'),
                    'fields' => array(
                        'button_style'         => array(
                            'type'          => 'select',
                            'label'         => __('Style', 'bb-njba'),
                            'default'       => 'flat',
                            'class'         => 'creative_button_styles',
                            'options'       => array(
                                'flat'          => __('Flat', 'bb-njba'),
                                'gradient'      => __('Gradient', 'bb-njba'),
                                'transparent'   => __('Transparent', 'bb-njba'),
                                'threed'          => __('3D', 'bb-njba'),
                            ),
                            'toggle'        => array(
                                'flat'          => array(
                                    'fields'        => array('button_background_color','hover_button_style','button_box_shadow','button_box_shadow_color'),
                                    'sections' => array('transition_section')
                                ),
                                'gradient'          => array(
                                    'fields'        => array('button_background_color')
                                ),
                                'threed'          => array(
                                    'fields'        => array('button_background_color','hover_button_style'),
                                    'sections' => array('transition_section')
                                ),
                                'transparent' => array(
                                    'fields' => array('hover_button_style','button_box_shadow','button_box_shadow_color'),
                                    'sections' => array('transition_section')
                                )
                            )
                        ),
                        'button_background_color' => array(
                            'type' => 'color',
                            'label' => __('Background Color','bb-njba'),
                            'show_reset' => true,
                            'default' => 'dfdfdf'
                        ),
                        'button_background_hover_color' => array(
                            'type' => 'color',
                            'label' => __('Background Hover Color','bb-njba'),
                            'show_reset' => true,
                            'default' => '000000'
                        ),
                        'button_text_color' => array(
                            'type' => 'color',
                            'label' => __('Text Color','bb-njba'),
                            'show_reset' => true,
                            'default' => '404040'
                        ),
                        'button_text_hover_color' => array(
                            'type' => 'color',
                            'label' => __('Text Hover Color','bb-njba'),
                            'show_reset' => true,
                            'default' => 'ffffff'
                        ),
                        'button_border_style'      => array(
                            'type'      => 'select',
                            'label'     => __('Border Style', 'bb-njba'),
                            'default'   => 'none',
                            'options'   => array(
                                'none'  => __('None', 'bb-njba'),
                                'solid'  => __('Solid', 'bb-njba'),
                                'dotted'  => __('Dotted', 'bb-njba'),
                                'dashed'  => __('Dashed', 'bb-njba'),
                                'double'  => __('Double', 'bb-njba'),
                            ),
                            'toggle' => array(
                                'solid' => array(
                                    'fields' => array('button_border_width','button_border_color','button_border_hover_color')
                                ),
                                'dotted' => array(
                                    'fields' => array('button_border_width','button_border_color','button_border_hover_color')
                                ),
                                'dashed' => array(
                                    'fields' => array('button_border_width','button_border_color','button_border_hover_color')
                                ),
                                'double' => array(
                                    'fields' => array('button_border_width','button_border_color','button_border_hover_color')
                                ),
                            )
                        ),
                        'button_border_width' => array(
                            'type' => 'text',
                            'label' => __('Border Width','bb-njba'),
                            'default' => '1',
                            'size' => '5',
                            'description'       => _x( 'px', 'Value unit for spacer width. Such as: "10 px"', 'bb-njba' )
                        ),
                        'button_border_radius'      => array(
                            'type'              => 'njba-multinumber',
                            'label'             => __('Border Radius', 'bb-njba'),
                            'description'       => 'px',
                            'default'           => array(
                                'top'          => 0,
                                'right'         => 0,
                                'bottom'       => 0,
                                'left'      => 0
                            ),
                            'options'           => array(
                                'top'               => array(
                                    'placeholder'       => __('Top', 'bb-njba'),
                                    'icon'              => 'fa-long-arrow-up'
                                ),
                                'right'            => array(
                                    'placeholder'       => __('Right', 'bb-njba'),
                                    'icon'              => 'fa-long-arrow-right'
                                ),
                                'bottom'            => array(
                                    'placeholder'       => __('Bottom', 'bb-njba'),
                                    'icon'              => 'fa-long-arrow-down'
                                ),
                                'left'            => array(
                                    'placeholder'       => __('Left', 'bb-njba'),
                                    'icon'              => 'fa-long-arrow-left'
                                )
                                
                            )
                        ),
                        'button_border_color' => array(
                            'type' => 'color',
                            'label' => __('Border Color','bb-njba'),
                            'show_reset' => true,
                            'default' => '000000'
                        ),
                        'button_border_hover_color' => array(
                            'type' => 'color',
                            'label' => __('Border Hover Color','bb-njba'),
                            'show_reset' => true,
                            'default' => '000000'
                        ),
                        'button_box_shadow'      => array(
                            'type'              => 'njba-multinumber',
                            'label'             => __('Box Shadow', 'bb-njba'),
                            'description'       => 'px',
                            'default'           => array(
                                'left_right'          => 0,
                                'top_bottom'         => 0,
                                'blur'       => 0,
                                'spread'      => 0
                            ),
                            'options'           => array(
                                'left_right'               => array(
                                    'placeholder'       => __('Top', 'bb-njba'),
                                    'icon'              => 'fa fa-arrows-h'
                                ),
                                'top_bottom'            => array(
                                    'placeholder'       => __('Right', 'bb-njba'),
                                    'icon'              => 'fa fa-arrows-v'
                                ),
                                'blur'            => array(
                                    'placeholder'       => __('Bottom', 'bb-njba'),
                                    'icon'              => 'fa fa-circle-thin'
                                ),
                                'spread'            => array(
                                    'placeholder'       => __('Left', 'bb-njba'),
                                    'icon'              => 'fa fa-circle'
                                )
                                
                            )
                        ),
                        'button_box_shadow_color' => array(
                            'type' => 'color',
                            'label' => __('Box Shadow Color','bb-njba'),
                            'show_reset' => true,
                            'default' => 'ffffff'
                        ),
                        'width' => array(
                            'type' => 'select',
                            'label' => __('Width','bb-njba'),
                            'default' => 'auto',
                            'options' => array(
                                'auto' => __('Auto','bb-njba'),
                                'full_width' => __('Full Width','bb-njba'),
                                'custom' => __('Custom','bb-njba')
                            ),
                            'toggle' => array(
                                'auto' => array(
                                    'fields' => array('alignment')
                                ),
                                'full_width' => array(
                                    'fields' => array('')
                                ),
                                'custom' => array(
                                    'fields' => array('custom_width','custom_height','alignment')
                                )
                            )
                        ),
                        'custom_width' => array(
                            'type' => 'text',
                            'label' => __('Custom Width','bb-njba'),
                            'default' => 200,
                            'size' => 10
                        ),
                        'custom_height' => array(
                            'type' => 'text',
                            'label' => __('Custom Height','bb-njba'),
                            'default' => 45,
                            'size' => 10
                        ),
                    )
                )
            ),
        ),
        'structure'    => array(
            'title'     => __('Structure', 'bb-njba'),
            'sections'  => array(
                'structure_section'   => array(
                    'title'     => '',
                    'fields'    => array(
                        'structure_align'    => array(
                            'type'      => 'select',
                            'label'     => __('Alignment', 'bb-njba'),
                            'default'   => 'center',
                            'options'   => array(
                                'left' => __('Left', 'bb-njba'),
                                'center' => __('Center', 'bb-njba'),
                                'right' => __('Right', 'bb-njba'),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    )
));
