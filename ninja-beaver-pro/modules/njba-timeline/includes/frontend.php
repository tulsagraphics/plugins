<div class="njba-timeline-wrapper">
	<?php if($settings->timeline_start_box_text && $settings->timeline_start_box == 'yes'){?>
		<div class="start_box_main">
			<div class="start_box"><?php echo $settings->timeline_start_box_text; ?></div>
		</div>
	<?php } ?>
		    <div class="njba-vertical-line"></div>
		    <div class="njba-timeline-main">
		    	<?php
				$number_items = count($settings->timeline);
				if($settings->timeline_structure == 'left'){
					$start = 0;
				}else{
					$start = 1;			
				}
				for( $i = 0; $i < $number_items; $i++ ) {
					if(!is_object($settings->timeline[$i])) {
						continue;
					}
					if($i%2 == $start){
						$class = 'left';
						$class_icon = 'right';	
					}else{
						$class = 'right';	
						$class_icon = 'left';
					}
				$timeline = $settings->timeline[$i];
				$btn_settings = array(
			        //Button text         
			        'button_text'   => $timeline->button_text,
			        //Button Link
			        'link'      => $timeline->link,
			        'link_target'       => $timeline->link_target
			    );
				?>
		            <div class="njba-timeline-item njba-timeline-item-<?php echo $class;?> njba-timeline-item-<?php echo $i;?>">
		            	<?php if($timeline->display_year == 'yes') { ?>
			            	<div class="njba-timeline-date njba-timeline-date-<?php echo $class_icon;?>">
								<div class="njba-timeline-date-separator-arrow"></div>
								<h1 class="njba-timeline-date-selector"><?php echo $timeline->timeline_year; ?></h1>
							</div>
		                <?php } ?>
		                <div class="njba-timeline-item-box-main">
			                <div class="njba-timeline-item-box-main-inner njba-from-<?php echo $class;?>">
			                    <div class="njba-timeline-item-main">
			                        <div class="njba-timeline-separator-arrow"></div>
			                        <?php if($timeline->content_icon_image == 'font_icon' || $timeline->content_icon_image == 'custom_icon') { ?>
				                        <div class="njba-timeline-icon">
				                            <?php if($timeline->structure_align == 'center') { ?>
				                            	<div class="njba-timeline-content-icon-center">
				                            <?php } ?>
					                                <div class="njba-timeline-content-icon">
				                                		<div class="vertical-horizontal-center">
							                                <?php if($timeline->content_icon && $timeline->content_icon_image == 'font_icon') {?>
							                                    <i class="<?php echo $timeline->content_icon; ?>"></i>
							                                <?php }?>
							                                <?php if($timeline->content_image && $timeline->content_icon_image == 'custom_icon') {?>
							                                    <img src="<?php echo $timeline->content_image_src; ?>" />
							                                <?php }?>
				                                		</div>
					                                </div>
				                            <?php if($timeline->structure_align == 'center') { ?>
				                            	</div>
				                            <?php } ?>
				                        </div>
			                        <?php } ?>
			                        <div class="njba-timeline-content">
			                            <h2><?php echo $timeline->title; ?></h2>
				                        <h4><?php echo $timeline->content; ?></h4>
				                        <?php 
				                        	if($timeline->display_button == 'yes') {
												FLBuilder::render_module_html('njba-button', $btn_settings);
				                        	}
										?>
			                        </div>
			                    </div>
			                </div>
			                <div class="njba-timeline-round <?php if($timeline->timeline_icon_image == 'font_icon' || $timeline->timeline_icon_image == 'custom_icon') { echo "njba-round-icon"; } ?>">
			                	<?php if($timeline->timeline_icon && $timeline->timeline_icon_image == 'font_icon') {?>
			                        <i class="<?php echo $timeline->timeline_icon; ?>"></i>
			                    <?php }?>
			                    <?php if($timeline->timeline_image && $timeline->timeline_icon_image == 'custom_icon') {?>
			                        <img src="<?php echo $timeline->timeline_image_src; ?>" />
			                    <?php }?>
			                </div>
		                </div>
		            </div>
		        <?php } ?>
		   </div>
    <?php if($settings->timeline_end_box_text && $settings->timeline_end_box == 'yes'){?>
	    <div class="end_box_main">
			<div class="end_box"><?php echo $settings->timeline_end_box_text; ?></div>
		</div>
	<?php } ?>
</div>