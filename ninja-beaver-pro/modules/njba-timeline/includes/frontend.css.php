<?php		
$number_items = count($settings->timeline);
for( $i = 0; $i < $number_items; $i++ ) {
$btn_id = $id.' .njba-timeline-item-'.$i;
	$btn_css_array = array(
	    //Button Style
	    'button_style'      => $settings->timeline[$i]->button_style,
	    'button_background_color'       => $settings->timeline[$i]->button_background_color,
	    'button_background_hover_color'     => $settings->timeline[$i]->button_background_hover_color,
	    'button_text_color'     =>$settings->timeline[$i]->button_text_color,
	    'button_text_hover_color'     =>$settings->timeline[$i]->button_text_hover_color,
	    'button_border_style'     =>$settings->timeline[$i]->button_border_style,
	    'button_border_width'     =>$settings->timeline[$i]->button_border_width,
	    'button_border_radius'     =>$settings->timeline[$i]->button_border_radius,
	    'button_border_color'     =>$settings->timeline[$i]->button_border_color,
	    'button_border_hover_color'     =>$settings->timeline[$i]->button_border_hover_color,
	    'button_box_shadow'     =>$settings->timeline[$i]->button_box_shadow,
	    'button_box_shadow_color'     =>$settings->timeline[$i]->button_box_shadow_color,
	    'width'     =>$settings->timeline[$i]->width,
	    'custom_width'     =>$settings->timeline[$i]->custom_width,
	    'custom_height'     =>$settings->timeline[$i]->custom_height,
	     //Button Style
	    'button_padding'     =>$settings->button_padding,
	    'button_margin'     =>$settings->button_margin,
	    
	    //Button Typography
	    'button_font_family'     =>$settings->button_font_family,
	    'button_font_size'     =>$settings->button_font_size,
	);
	
	FLBuilder::render_module_css('njba-button' , $btn_id, $btn_css_array);
	if(!is_object($settings->timeline[$i])) {
		continue;
	}
	$timeline = $settings->timeline[$i];
	
	$timeline->timeline_box_shadow_options = (array)$timeline->timeline_box_shadow_options;
?>
.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-content h2{
	
	<?php if($timeline->title_color) { ?>color:#<?php echo $timeline->title_color;?>; <?php } ?>
	<?php if($timeline->title_color) { ?>background-color:#<?php echo $timeline->title_background_color;?>; <?php } ?>
	
}
.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-content h4{
	<?php if($timeline->text_color) { ?>color:#<?php echo $timeline->text_color;?>; <?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-item-main{
	
	<?php if($timeline->timeline_box_background) { ?>background-color:#<?php echo $timeline->timeline_box_background;?>; <?php } ?>
	<?php if($timeline->timeline_box_shadow == 'yes') { ?>
	box-shadow:<?php if(isset($timeline->timeline_box_shadow_options['left_right'] )) { echo $timeline->timeline_box_shadow_options['left_right']; } ?>px <?php if(isset($timeline->timeline_box_shadow_options['top_bottom'] )) { echo $timeline->timeline_box_shadow_options['top_bottom']; } ?>px <?php if(isset($timeline->timeline_box_shadow_options['blur'] )) { echo $timeline->timeline_box_shadow_options['blur']; } ?>px <?php if(isset($timeline->timeline_box_shadow_options['spread'] )) { echo $timeline->timeline_box_shadow_options['spread']; } ?>px #<?php echo $timeline->timeline_box_shadow_color;?>;
	<?php }?>
	<?php if($timeline->timeline_box_border_type) { ?>border-style:<?php echo $timeline->timeline_box_border_type;?>;<?php } ?>
	<?php if($timeline->timeline_box_border_width >= 0) { ?>border-width: <?php echo $timeline->timeline_box_border_width;?>px; <?php } ?>
	<?php if($timeline->timeline_box_border_color) {?>border-color:<?php echo '#'.$timeline->timeline_box_border_color;?>;<?php } ?>
	<?php if($timeline->timeline_box_border_radius) {?>border-radius:<?php echo $timeline->timeline_box_border_radius;?>px;<?php } ?>
	
}
.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?>.njba-timeline-item-left .njba-timeline-separator-arrow {
	right: -15px;
	top: 40px;
	border-left: 15px solid transparent;
	border-top: 15px solid transparent;
	border-bottom: 15px solid transparent;
	height: 15px;
	width: 15px;
	z-index: 1;
	<?php if($timeline->arrow_color) { ?>border-left-color:#<?php echo $timeline->arrow_color; ?>; <?php } ?>
	
}
.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?>.njba-timeline-item-right .njba-timeline-separator-arrow {
	left: -15px;
	top: 40px;
	right: auto;
	border-right: 15px solid transparent;
	border-top: 15px solid transparent;
	border-bottom: 15px solid transparent;
	height: 15px;
	width: 15px;
	z-index: 1;
	<?php if($timeline->arrow_color) { ?>border-right-color:#<?php echo $timeline->arrow_color;?>; <?php } ?>
	
}
.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-date-left .njba-timeline-date-separator-arrow{
	right: 36px;
	left: auto;	
	border-left: 15px solid transparent;
	border-top: 15px solid transparent;
	border-bottom: 15px solid transparent;
	height: 15px;
	width: 15px;
	z-index: 1;
	<?php if($timeline->timeline_arrow_color) { ?>border-left-color:#<?php echo $timeline->timeline_arrow_color; ?>; <?php } ?>
} 
 
.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-date-right .njba-timeline-date-separator-arrow{
	left:36px;
	right: auto;	
	border-right: 15px solid transparent;
	border-top: 15px solid transparent;
	border-bottom: 15px solid transparent;
	height: 15px;
	width: 15px;
	z-index: 1;
	<?php if($timeline->timeline_arrow_color) { ?>border-right-color:#<?php echo $timeline->timeline_arrow_color;?>; <?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-content-icon{
	<?php if($timeline->timeline_content_icon_background_color) { ?>background-color:#<?php echo $timeline->timeline_content_icon_background_color;?>; <?php } ?>	
	<?php if($timeline->timeline_content_icon_border_style) { ?>border-style:<?php echo $timeline->timeline_content_icon_border_style;?>;<?php } ?>
	<?php if($timeline->timeline_content_icon_border_width >= 0) { ?>border-width: <?php echo $timeline->timeline_content_icon_border_width;?>px; <?php } ?>
	<?php if($timeline->timeline_content_icon_border_color) {?>border-color:<?php echo '#'.$timeline->timeline_content_icon_border_color;?>;<?php } ?>
	<?php if($timeline->timeline_content_icon_border_radius) {?>border-radius:<?php echo $timeline->timeline_content_icon_border_radius;?>px;<?php } ?>
	<?php if($timeline->timeline_content_icon_height) {?>height:<?php echo $timeline->timeline_content_icon_height;?>px;<?php } ?>
	<?php if($timeline->timeline_content_icon_width) {?>width:<?php echo $timeline->timeline_content_icon_width;?>px;<?php } ?>
	
}
.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-content-icon i{
	
	<?php if($timeline->timeline_content_icon_color) { ?>color:#<?php echo $timeline->timeline_content_icon_color;?>; <?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-round{
	
	<?php if($timeline->timeline_icon_background_color) { ?>background-color:#<?php echo $timeline->timeline_icon_background_color;?>; <?php } ?>	
	<?php if($timeline->timeline_icon_border_style) { ?>border-style:<?php echo $timeline->timeline_icon_border_style;?>;<?php } ?>
	<?php if($timeline->timeline_icon_border_width >= 0) { ?>border-width: <?php echo $timeline->timeline_icon_border_width;?>px; <?php } ?>
	<?php if($timeline->timeline_icon_border_color) {?>border-color:<?php echo '#'.$timeline->timeline_icon_border_color;?>;<?php } ?>
	<?php if($timeline->timeline_icon_border_radius) {?>border-radius:<?php echo $timeline->timeline_icon_border_radius;?>px;<?php } ?>
	<?php if($timeline->timeline_icon_height) {?>height:<?php echo $timeline->timeline_icon_height;?>px;<?php } ?>
	<?php if($timeline->timeline_icon_width) {?>width:<?php echo $timeline->timeline_icon_width;?>px;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-round.njba-round-icon{
	
	<?php if($timeline->timeline_icon_background_color) { ?>background-color:#<?php echo $timeline->timeline_icon_background_color;?>; <?php } ?>	
	<?php if($timeline->timeline_icon_border_style) { ?>border-style:<?php echo $timeline->timeline_icon_border_style;?>;<?php } ?>
	<?php if($timeline->timeline_icon_border_width >= 0) { ?>border-width: <?php echo $timeline->timeline_icon_border_width;?>px; <?php } ?>
	<?php if($timeline->timeline_icon_border_color) {?>border-color:<?php echo '#'.$timeline->timeline_icon_border_color;?>;<?php } ?>
	<?php if($timeline->timeline_icon_border_radius) {?>border-radius:<?php echo $timeline->timeline_icon_border_radius;?>px;<?php } ?>
	<?php if($timeline->timeline_icon_height) {?>height:<?php echo $timeline->timeline_icon_height;?>px;<?php } ?>
	<?php if($timeline->timeline_icon_width) {?>width:<?php echo $timeline->timeline_icon_width;?>px;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-round.njba-round-icon i, .fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-round.njba-round-icon i:before{
	
	<?php if($timeline->timeline_icon_color) { ?>color:#<?php echo $timeline->timeline_icon_color;?>; <?php } ?>
	<?php if($timeline->timeline_icon_height) { ?>line-height:<?php echo ($timeline->timeline_icon_height);?>px;<?php } ?>	
		
}
.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-date i{
	
	<?php if($timeline->timeline_year_border_color) { ?>color:#<?php echo $timeline->timeline_year_border_color;?>; <?php } ?>
	
}
.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-date h1{
	<?php if($timeline->timeline_year_background_color) { ?>background-color:#<?php echo $timeline->timeline_year_background_color;?>; <?php } ?>	
	<?php if($timeline->timeline_year_border_style) { ?>border-style:<?php echo $timeline->timeline_year_border_style;?>;<?php } ?>
	<?php if($timeline->timeline_year_border_width >= 0) { ?>border-width: <?php echo $timeline->timeline_year_border_width;?>px; <?php } ?>
	<?php if($timeline->timeline_year_border_color) {?>border-color:<?php echo '#'.$timeline->timeline_year_border_color;?>;<?php } ?>
	<?php if($timeline->timeline_year_border_radius) {?>border-radius:<?php echo $timeline->timeline_year_border_radius;?>px;<?php } ?>
	<?php if($timeline->timeline_icon_color) { ?>color:#<?php echo $timeline->timeline_year_color;?>; <?php } ?>
	
}
<?php if($timeline->structure_align == 'center') { ?>
	.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-icon{	
		display:inline-block;
		width:100%;
		text-align:center;
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-content-icon{
	    float: none;
	    <?php if(isset($settings->timeline_content_icon_margin['top'] )) { ?>margin-top:<?php echo $settings->timeline_content_icon_margin['top'];?>px;<?php } ?>
		<?php if(isset($settings->timeline_content_icon_margin['right'] )) { ?>margin-right:<?php echo $settings->timeline_content_icon_margin['right'];?>px;<?php } ?>
		<?php if(isset($settings->timeline_content_icon_margin['bottom'] )) { ?>margin-bottom:<?php echo $settings->timeline_content_icon_margin['bottom'];?>px;<?php } ?>
		<?php if(isset($settings->timeline_content_icon_margin['left'] )) { ?>margin-left:<?php echo $settings->timeline_content_icon_margin['left'];?>px;<?php } ?>	
	    display:table;
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-content, .fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-btn-main{
		text-align:center;
	}
	
<?php } ?>
<?php if($timeline->structure_align == 'left') { ?>
	.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-content-icon{
	    float: left;
	    <?php if(isset($settings->timeline_content_icon_margin['top'] )) { ?>margin-top:<?php echo $settings->timeline_content_icon_margin['top'];?>px;<?php } ?>
		<?php if(isset($settings->timeline_content_icon_margin['right'] )) { ?>margin-right:<?php echo $settings->timeline_content_icon_margin['right'];?>px;<?php } ?>
		<?php if(isset($settings->timeline_content_icon_margin['bottom'] )) { ?>margin-bottom:<?php echo $settings->timeline_content_icon_margin['bottom'];?>px;<?php } ?>
		<?php if(isset($settings->timeline_content_icon_margin['left'] )) { ?>margin-left:<?php echo $settings->timeline_content_icon_margin['left'];?>px;<?php } ?>
	
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-content, .fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-btn-main{
		text-align:left;
	}
<?php } ?>
<?php if($timeline->structure_align == 'right') { ?>
	.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-content-icon{
	    float: right;
	    <?php if(isset($settings->timeline_content_icon_margin['top'] )) { ?>margin-top:<?php echo $settings->timeline_content_icon_margin['top'];?>px;<?php } ?>
		<?php if(isset($settings->timeline_content_icon_margin['right'] )) { ?>margin-right:<?php echo $settings->timeline_content_icon_margin['right'];?>px;<?php } ?>
		<?php if(isset($settings->timeline_content_icon_margin['bottom'] )) { ?>margin-bottom:<?php echo $settings->timeline_content_icon_margin['bottom'];?>px;<?php } ?>
		<?php if(isset($settings->timeline_content_icon_margin['left'] )) { ?>margin-left:<?php echo $settings->timeline_content_icon_margin['left'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-content, .fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-btn-main{
		text-align:right;
	}
<?php } ?>
@media( max-width: 1024px ) and ( min-width: 768px ) {
	.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-content, .fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-btn-main{
		text-align:center;
	}
	
}
@media( max-width: 550px ) {
	
	.fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-timeline-content, .fl-node-<?php echo $id; ?> .njba-timeline-item-<?php echo $i;?> .njba-btn-main{
		text-align:center;
	}	
}
<?php } ?>
.fl-node-<?php echo $id; ?> .njba-timeline-content h2{
		
	<?php if(isset($settings->title_padding['top'] )) { ?>padding-top:<?php echo $settings->title_padding['top'];?>px;<?php } ?>
	<?php if(isset($settings->title_padding['right'] )) { ?>padding-right:<?php echo $settings->title_padding['right'];?>px;<?php } ?>
	<?php if(isset($settings->title_padding['bottom'] )) { ?>padding-bottom:<?php echo $settings->title_padding['bottom'];?>px;<?php } ?>
	<?php if(isset($settings->title_padding['left'] )) { ?>padding-left:<?php echo $settings->title_padding['left'];?>px;<?php } ?>
	<?php if($settings->timeline_title_font['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->timeline_title_font ); ?><?php } ?>
	<?php if(isset($settings->timeline_title_font_size['desktop']) ) { ?>font-size:<?php echo $settings->timeline_title_font_size['desktop'];?>px;<?php } ?>
	<?php if(isset($settings->timeline_title_line_height['desktop']) ) { ?>line-height:<?php echo $settings->timeline_title_line_height['desktop'];?>px;<?php } ?>
	
}
.fl-node-<?php echo $id; ?> .njba-timeline-content h4{
	
	<?php if(isset($settings->content_padding['top'] )) { ?>padding-top:<?php echo $settings->content_padding['top'];?>px;<?php } ?>
	<?php if(isset($settings->content_padding['right'] )) { ?>padding-right:<?php echo $settings->content_padding['right'];?>px;<?php } ?>
	<?php if(isset($settings->content_padding['bottom'] )) { ?>padding-bottom:<?php echo $settings->content_padding['bottom'];?>px;<?php } ?>
	<?php if(isset($settings->content_padding['left'] )) { ?>padding-left:<?php echo $settings->content_padding['left'];?>px;<?php } ?>
	<?php if($settings->timeline_text_font['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->timeline_text_font ); ?><?php } ?>
	<?php if(isset($settings->timeline_text_font_size['desktop']) ){ ?>font-size:<?php echo $settings->timeline_text_font_size['desktop'];?>px;<?php } ?>
	<?php if(isset($settings->timeline_text_line_height['desktop']) ) { ?>line-height:<?php echo $settings->timeline_text_line_height['desktop'];?>px;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-timeline-item-main{
	
	<?php if(isset($settings->timeline_box_padding['top'] )) { ?>padding-top:<?php echo $settings->timeline_box_padding['top'];?>px;<?php } ?>
	<?php if(isset($settings->timeline_box_padding['right'] )) { ?>padding-right:<?php echo $settings->timeline_box_padding['right'];?>px;<?php } ?>
	<?php if(isset($settings->timeline_box_padding['bottom'] )) { ?>padding-bottom:<?php echo $settings->timeline_box_padding['bottom'];?>px;<?php } ?>
	<?php if(isset($settings->timeline_box_padding['left'] )) { ?>padding-left:<?php echo $settings->timeline_box_padding['left'];?>px;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-timeline-content-icon i{
		
	<?php if(isset($settings->timeline_content_icon_size['desktop']) ) { ?>font-size:<?php echo $settings->timeline_content_icon_size['desktop'];?>px;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-timeline-round.njba-round-icon i{
		
	<?php if(isset($settings->timeline_icon_size['desktop'])) { ?>font-size:<?php echo $settings->timeline_icon_size['desktop'];?>px;<?php } ?>
		
}
.fl-node-<?php echo $id; ?> .njba-timeline-date h1{
	<?php if($settings->timeline_year_font_family['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->timeline_year_font_family ); ?><?php } ?>
	<?php if(isset($settings->timeline_year_size['desktop']) ) { ?>font-size:<?php echo $settings->timeline_year_size['desktop'];?>px;<?php } ?>
	
}
.fl-node-<?php echo $id; ?> .start_box_main .start_box, .fl-node-<?php echo $id; ?> .end_box_main .end_box{
	<?php if($settings->timeline_start_end_background_color) { ?>background-color:#<?php echo $settings->timeline_start_end_background_color;?>; <?php } ?>	
	<?php if($settings->timeline_start_end_border_style) { ?>border-style:<?php echo $settings->timeline_start_end_border_style;?>;<?php } ?>
	<?php if($settings->timeline_start_end_border_width >= 0) { ?>border-width: <?php echo $settings->timeline_start_end_border_width;?>px; <?php } ?>
	<?php if($settings->timeline_start_end_border_color) {?>border-color:<?php echo '#'.$settings->timeline_start_end_border_color;?>;<?php } ?>
	<?php if($settings->timeline_start_end_border_radius) {?>border-radius:<?php echo $settings->timeline_start_end_border_radius;?>px;<?php } ?>
	<?php if($settings->timeline_start_end_color) { ?>color:#<?php echo $settings->timeline_start_end_color;?>; <?php } ?>
	<?php if($settings->timeline_start_end_font_family['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->timeline_start_end_font_family ); ?><?php } ?>
	<?php if(isset($settings->timeline_start_end_size['desktop']) ){ ?>font-size:<?php echo $settings->timeline_start_end_size['desktop'];?>px;<?php } ?>
	<?php if($settings->timeline_start_end_height) {?>height:<?php echo $settings->timeline_start_end_height;?>px;<?php } ?>
	<?php if($settings->timeline_start_end_height) {?>line-height:<?php echo $settings->timeline_start_end_height;?>px;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-vertical-line{
	<?php if($settings->timeline_line_style) { ?> border-style: <?php echo $settings->timeline_line_style;?>; <?php } ?>
    <?php if($settings->timeline_line_width) { ?> border-width: <?php echo $settings->timeline_line_width;?>px; <?php } ?>
    <?php if($settings->timeline_line_color) { ?> border-color: #<?php echo $settings->timeline_line_color;?>; <?php } ?>
}
@media( max-width: 1024px ) and ( min-width: 768px ) {
	.fl-node-<?php echo $id; ?> .njba-timeline-icon{
		display:inline-block;
		width:100%;
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-icon .njba-timeline-content-icon{
	    float: none;
	    margin: 0 auto;
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-content h2{
		<?php if(isset($settings->timeline_title_font_size['medium']) ){ ?>font-size:<?php echo $settings->timeline_title_font_size['medium'];?>px;<?php } ?>
		<?php if(isset($settings->timeline_title_line_height['medium']) ){ ?>line-height:<?php echo $settings->timeline_title_line_height['medium'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-content h4{
		<?php if(isset($settings->timeline_text_font_size['medium']) ){ ?>font-size:<?php echo $settings->timeline_text_font_size['medium'];?>px;<?php } ?>
		<?php if(isset($settings->timeline_text_line_height['medium']) ){ ?>line-height:<?php echo $settings->timeline_text_line_height['medium'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-content-icon i{
		<?php if(isset($settings->timeline_content_icon_size['medium']) ){ ?>font-size:<?php echo $settings->timeline_content_icon_size['medium'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-round.njba-round-icon i{
		<?php if(isset($settings->timeline_icon_size['medium']) ){ ?>font-size:<?php echo $settings->timeline_icon_size['medium'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-date h1{
		<?php if(isset($settings->timeline_year_size['medium']) ){ ?>font-size:<?php echo $settings->timeline_year_size['medium'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .start_box_main .start_box, .fl-node-<?php echo $id; ?> .end_box_main .end_box{
		<?php if(isset($settings->timeline_start_end_size['medium']) ){ ?>font-size:<?php echo $settings->timeline_start_end_size['medium'];?>px;<?php } ?>	
	}
}
@media( max-width: 767px ) {
	.fl-node-<?php echo $id; ?> .njba-timeline-content h2{
		<?php if(isset($settings->timeline_title_font_size['small']) ){ ?>font-size:<?php echo $settings->timeline_title_font_size['small'];?>px;<?php } ?>
		<?php if(isset($settings->timeline_title_line_height['small']) ){ ?>line-height:<?php echo $settings->timeline_title_line_height['small'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-content h4{
		<?php if(isset($settings->timeline_text_font_size['small']) ){ ?>font-size:<?php echo $settings->timeline_text_font_size['small'];?>px;<?php } ?>
		<?php if(isset($settings->timeline_text_line_height['small']) ){ ?>line-height:<?php echo $settings->timeline_text_line_height['small'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-content-icon i{
		<?php if(isset($settings->timeline_content_icon_size['small']) ){ ?>font-size:<?php echo $settings->timeline_content_icon_size['small'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-round.njba-round-icon i{
		<?php if(isset($settings->timeline_icon_size['small']) ){ ?>font-size:<?php echo $settings->timeline_icon_size['small'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-date h1{
		<?php if(isset($settings->timeline_year_size['small']) ){ ?>font-size:<?php echo $settings->timeline_year_size['small'];?>px;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .start_box_main .start_box, .fl-node-<?php echo $id; ?> .end_box_main .end_box{
		<?php if(isset($settings->timeline_start_end_size['small']) ){ ?>font-size:<?php echo $settings->timeline_start_end_size['small'];?>px;<?php } ?>	
		float:left;
	}
}
@media( max-width: 550px ) {
	.fl-node-<?php echo $id; ?> .njba-timeline-icon{
		display:inline-block;
		width:100%;
	}
	.fl-node-<?php echo $id; ?> .njba-timeline-icon .njba-timeline-content-icon{
	    float: none;
	    margin: 0 auto;
	}
 
}
