//script for scrolling animation
(function($) {
    $(document).ready(function(){
        if($('html').hasClass('fl-builder-edit') === false) {
            var $timeline_block = $('.njba-timeline-item');
            //hide timeline box untill the scrolldown
            $timeline_block.each(function(){
                $(this).find('.njba-timeline-item-box-main-inner').addClass('njba-is-hidden');
                $(this).find('.njba-timeline-date').addClass('njba-is-hidden');
                if($(this).offset().top <= $(window).scrollTop()+$(window).height()) {
                    $(this).find('.njba-timeline-item-box-main-inner.njba-from-left').removeClass('njba-is-hidden').addClass('fadein-left');
                    $(this).find('.njba-timeline-item-box-main-inner.njba-from-right').removeClass('njba-is-hidden').addClass('fadein-right');
                    $(this).find('.njba-timeline-date.njba-timeline-date-left').removeClass('njba-is-hidden').addClass('fadein-left');
                    $(this).find('.njba-timeline-date.njba-timeline-date-right').removeClass('njba-is-hidden').addClass('fadein-right');
                }
            });
            //show timeline box on scrolldown
            $(window).on('scroll', function(){
                $timeline_block.each(function(){
                    if( $(this).offset().top <= $(window).scrollTop()+($(window).height() / 1.5) && $(this).find('.njba-timeline-item-box-main-inner').hasClass('njba-is-hidden') ) {
                        $(this).find('.njba-timeline-item-box-main-inner.njba-from-left').removeClass('njba-is-hidden').addClass('fadein-left');
                        $(this).find('.njba-timeline-item-box-main-inner.njba-from-right').removeClass('njba-is-hidden').addClass('fadein-right');
                        $(this).find('.njba-timeline-date.njba-timeline-date-left').removeClass('njba-is-hidden').addClass('fadein-left');
                        $(this).find('.njba-timeline-date.njba-timeline-date-right').removeClass('njba-is-hidden').addClass('fadein-right');
                    }
                });
            });
        }
    });
})(jQuery);