<?php
/**
 * @class NJBAImageCarouselModule
 */
class NJBAImageCarouselModule extends FLBuilderModule {
    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Image Carousel', 'bb-njba'),
            'description'   => __('Addon to display Image Carousel.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'carousel' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-image-carousel/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-image-carousel/',
            'icon'              => 'slides.svg',
            'partial_refresh' => true, // Defaults to false and can be omitted.
        ));
        /**
         * Use these methods to enqueue css and js already
         * registered or to register and enqueue your own.
         */
        // Already registered
        $this->add_css('jquery-bxslider');
		$this->add_css('font-awesome');
		$this->add_js('jquery-bxslider');
    }
    /**
     * Use this method to work with settings data before
     * it is saved. You must return the settings object.
     *
     * @method update
     * @param $settings {object}
     */
    public function update($settings)
    {
       return $settings;
    }
    public function delete()
    {
    }
   
}
/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('NJBAImageCarouselModule', array(
    'photos'      => array( // Tab
        'title'         => __('Photos', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array( // Section Fields
                    'photos'     => array(
                        'type'          => 'form',
                        'label'         => __('Photos', 'bb-njba'),
                        'form'          => 'njba_photospanel_form', // ID from registered form below
                        'preview_text'  => 'caption_name', // Name of a field to use for the preview text
                        'multiple'      => true
                    ),
                )
            )
        )
    ),
	'general'      => array( // Tab
		'title'         => __('General', 'bb-njba'), // Tab title
		'sections'      => array( // Tab Sections
			'image_setting' => array(
                'title'         => __('Photo Settings', 'bb-njba'),
                'fields'        => array(
                    'show_captions' => array(
                        'type'          => 'select',
                        'label'         => __('Show Captions', 'bb-njba'),
                        'default'       => 'hover',
                        'options'       => array(
                            '0'                    => __('Never', 'bb-njba'),
                            'inside_top'           => __('Inside Top About Photo', 'bb-njba'),
                            'inside_below'         => __('Inside Below About Photo', 'bb-njba'),
                            'outside_top'          => __('Outside Top About Photo', 'bb-njba'),
                            'outside_below'        => __('Outside Below About Photo', 'bb-njba'),
                        ),
                        'toggle'        => array(
                            'inside_top'         => array(
                                'tabs'          => array( 'styles' ),
                                'sections'      => array( 'overly_setting' ),
                                'fields'        => array( 'hover_effects','caption_bg_color', 'caption_bg_color_opc' ),
                            ),
                            'inside_below'         => array(
                                'tabs'          => array( 'styles' ),
                                'sections'      => array( 'overly_setting' ),
                                'fields'        => array( 'hover_effects','caption_bg_color', 'caption_bg_color_opc' ),
                            ),
                            'outside_top'         => array(
                                'tabs'          => array( 'styles' ),
                                'sections'      => array( 'overly_setting' ),
                                'fields'        => array( 'hover_effects','caption_bg_color', 'caption_bg_color_opc' ),
                            ),
                            'outside_below'         => array(
                                'tabs'          => array( 'styles' ),
                                'sections'      => array( 'overly_setting' ),
                                'fields'        => array( 'hover_effects','caption_bg_color', 'caption_bg_color_opc' ),
                            ),
                            '0'                 => array(
                                'fields'        => array()
                            ),
                            
                        )
                    ),
                    'hover_effects' => array(
                        'type'          => 'select',
                        'label'         => __('Image Hover Effect', 'bb-njba'),
                        'default'       => 'zoom-in',
                        'options'       => array(
                            'none'          => __('None', 'bb-njba'),
                            'rotate-left' => __('Rotate Left', 'bb-njba'),
                            'rotate-right'=> __('Rotate Right', 'bb-njba'),
                            'zoom-in'       => __('Zoom In', 'bb-njba'),
                            'zoom-out'      => __('Zoom Out', 'bb-njba'),
                        )
                    ),
                    'click_action'  => array(
                        'type'          => 'select',
                        'label'         => __('Click Action', 'bb-njba'),
                        'default'       => 'lightbox',
                        'options'       => array(
                            'none'          => __( 'None', 'bb-njba' ),
                            'lightbox'      => __('Lightbox', 'bb-njba')
                        )
                        
                    ),
                    'overly_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Hover Background', 'bb-njba'),
                        'show_reset'    => true,
                                
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-image-box-overlay',
                            'property'      => 'background-color',
                        )
                    ),
                    'overly_color_opacity'    => array(
                        'type'          => 'text',
                        'label'         => __('Opacity', 'bb-njba'),
                        'default'       => '100',
                        'maxlength'     => '3',
                        'size'          => '5', 
                        'description'       => '%', 
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-image-box-overlay',
                            'property'      => 'background-color',
                        )
                    ),
                )
            ),
           
		)
    ),
    'carousel'      => array( // Tab
        'title'         => __('Carousel', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'heading'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array(
                    'autoplay'         => array(
                        'type'          => 'select',
                        'label'         => __('Autoplay', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba')
                        ),
                    ),
                    'hover_pause'         => array(
                        'type'          => 'select',
                        'label'         => __('Pause on hover', 'bb-njba'),
                        'default'       => '1',
                        'help'          => __('Pause when mouse hovers over slider'),
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba'),
                        ),
                    ),
                    'transition'    => array(
                        'type'          => 'select',
                        'label'         => __('Mode', 'bb-njba'),
                        'default'       => 'horizontal',
                        'options'       => array(
                            'horizontal'    => _x( 'Horizontal', 'Transition type.', 'bb-njba' ),
                            'vertical'    => _x( 'Vertical', 'Transition type.', 'bb-njba' ),
                            'fade'          => __( 'Fade', 'bb-njba' )
                        ),
                    ),
                    'pause'         => array(
                        'type'          => 'text',
                        'label'         => __('Delay', 'bb-njba'),
                        'default'       => '4',
                        'maxlength'     => '4',
                        'size'          => '5',
                        'description'   => _x( 'seconds', 'Value unit for form field of time in seconds. Such as: "5 seconds"', 'bb-njba' )
                    ),
                    'speed'         => array(
                        'type'          => 'text',
                        'label'         => __('Transition Speed', 'bb-njba'),
                        'default'       => '0.5',
                        'maxlength'     => '4',
                        'size'          => '5',
                        'description'   => _x( 'seconds', 'Value unit for form field of time in seconds. Such as: "5 seconds"', 'bb-njba' )
                    ),
                    'loop'         => array(
                        'type'          => 'select',
                        'label'         => __('Loop', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba'),
                        ),
                    ),
                    'adaptive_height'   => array(
                        'type'              => 'select',
                        'label'             => __('Fixed Height', 'bb-njba'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'bb-njba'),
                            'no'                => __('No', 'bb-njba')
                        ),
                        'help'              => __('Fix height to the tallest item.', 'bb-njba')
                    )
                    
                )
            ),
            'carousel_section'       => array( // Section
                'title'         => '',
                'fields'        => array( // Section Fields
                    'max_slides'         => array(
                        'type'          => 'njba-simplify',
                        'label'         => __('Maximum Slides'),
                        'default'       => array(
                                    'desktop' => '3',
                                    'medium'  => '2',
                                    'small'   => '1',
                        ),
                        'size'          => '5', 
                    ),
                     'slide_margin'         => array(
                        'type'          => 'njba-simplify',
                        'label'         => __('Slides Margin ', 'bb-njba'),
                        'default'       => array(
                                    'desktop' => '0',
                                    'medium'  => '0',
                                    'small'   => '0',
                        ),
                        'size'          => '5', 
                    ),
                )
            ),
            'arrow_nav'       => array( // Section
                'title'         => '',
                'fields'        => array( // Section Fields
                    'arrows'       => array(
                        'type'          => 'select',
                        'label'         => __('Show Arrows', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba')
                        ),
                        'toggle'        => array(
                            '1'         => array(
                                'fields'        => array('arrows_size', 'arrow_background', 'arrow_color','arrow_border_width','arrow_border_style','arrow_border_color','arrow_border_color','arrow_border_radius')
                            )
                        )
                    ),
                    
                    'arrow_background'       => array(
                        'type'          => 'color',
                        'label'         => __('Arrow Background', 'bb-njba'),
                        
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-image-carousel-main .bx-wrapper .bx-controls-direction a.bx-prev,.njba-image-carousel-main .bx-wrapper .bx-controls-direction a.bx-next',
                            'property'      => 'background'
                        )
                    ),
                    'arrow_color'       => array(
                        'type'          => 'color',
                        'label'         => __('Arrow Color', 'bb-njba'),
                        
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-image-carousel-main .bx-wrapper .bx-controls-direction a.bx-prev,.njba-image-carousel-main .bx-wrapper .bx-controls-direction a.bx-next',
                            'property'      => 'color'
                        )
                    ),
               )
            ),
            'dot_nav'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array( // Section Fields
                    'dots'       => array(
                        'type'          => 'select',
                        'label'         => __('Show Dots', 'bb-njba'),
                        'default'       => '1',
                        'options'       => array(
                            '1'             => __('Yes', 'bb-njba'),
                            '0'             => __('No', 'bb-njba'),
                        ),
                        'toggle'        => array(
                            '1'         => array(
                                'fields'        => array('dot_color', 'active_dot_color')
                            )
                        )
                    ),
                    'dot_color'       => array(
                        'type'          => 'color',
                        'label'         => __('Dot Color', 'bb-njba'),
                        
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.bx-pager.bx-default-pager a.bx-pager-link',
                            'property'      => 'background'
                        )
                    ),
                    'active_dot_color'       => array(
                        'type'          => 'color',
                        'label'         => __('Active Dot Color', 'bb-njba'),
                       
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.bx-pager.bx-default-pager a.bx-pager-link.active ',
                            'property'      => 'background'
                        )
                    ),
                )
            )
            
        )
    ),
	'styles'                => array(
        'title'                     => __('Styles', 'bb-njba'),
        'sections'                  => array(
            'heading_fonts'             => array(
                'title'                     => __('Caption Settings', 'bb-njba'),
                'fields'                    => array( // Section Fields
                    'caption_tag'   => array(
                        'type'          => 'select',
                        'label'         => __('Tag', 'bb-njba'),
                        'default'       => 'h2',
                        'options'       => array(
                            'h1'      => __('H1', 'bb-njba'),
                            'h2'      => __('H2', 'bb-njba'),
                            'h3'      => __('H3', 'bb-njba'),
                            'h4'      => __('H4', 'bb-njba'),
                            'h5'      => __('H5', 'bb-njba'),
                            'h6'      => __('H6', 'bb-njba'),
                            
                        )
                    ),
                    'caption_alignment'         => array(
						'type'                      => 'select',
						
						'label'                     => __('Alignment', 'bb-njba'),
                        'options'                   => array(
                            'left'                      => __('Left', 'bb-njba'),
                            'right'                     => __('Right', 'bb-njba'),
                            'center'                    => __('Center', 'bb-njba'),
                        ),
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.njba-image-box-content .njba-caption',
                            'property'      => 'text-align'
						)
					),
                    'caption_font'          => array(
                        'type'          => 'font',
                        'default'		=> array(
                            'family'		=> 'Default',
                            'weight'		=> 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'      => '.njba-image-box-content .njba-caption',
                        )
                    ),
                   'caption_font_size'    => array(
						'type'          => 'njba-simplify',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                                    'desktop' => '40',
                                    'medium'  => '22',
                                    'small'   => '18',
                        ),
                        'size'          => '5',
                        'maxlength'     => '2',
                        'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-image-box-content .njba-caption',
                            'property'      => 'font-size',
                            'unit'          => 'px'
                        )
					),
                   /*'caption_line_height'    => array(
                        'type'          => 'njba-simplify',
                        'label'         => __('Line Height', 'bb-njba'),
                        'default'       => array(
                                    'desktop' => '40',
                                    'medium'  => '22',
                                    'small'   => '18',
                        ),
                        'size'          => '5',
                        'maxlength'     => '2',
                        'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
                        
                     ),*/
                    'caption_color'    => array(
						'type'          => 'color',
						'label'         => __('Color', 'bb-njba'),
						'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-image-box-content .njba-caption',
                            'property'      => 'color',
                        )
					),
                    'caption_bg_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Background Color', 'bb-njba'),
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-image-box-content .njba-caption',
                            'property'      => 'color',
                        )
                    ),
                    'caption_bg_color_opc'    => array(
                        'type'          => 'text',
                        'label'         => __('Opacity', 'bb-njba'),
                        
                        'maxlength'     => '3',
                        'size'          => '5', 
                        'description'       => '%', 
                        'preview'       => array(
                            'type'          => 'css',
                           'selector'      => '.njba-image-box-content .njba-caption',
                            'property'      => 'color',
                        )
                    ),
                    
                )
            ),
            'videoicons_setting' => array(
                'title'         => __('Video Icon Settings', 'bb-njba'),
                'fields'        => array(
                    'videoicon_width'    => array(
                        'type'          => 'text',
                        'label'         => __('Icon Box Width', 'bb-njba'),
                        'maxlength'     => '3',
                        'size'          => '5',
                        'description'   => _x( 'px', 'Value unit for width. Such as: "5 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-image-carousel-main .magnific-video .njba-video_icon i',
                            'property'      => 'width',
                            
                        )
                    ),
                    'videoicon_height'    => array(
                        'type'          => 'text',
                        'label'         => __('Icon Box Height', 'bb-njba'),
                        'maxlength'     => '3',
                        'size'          => '5',
                        'description'   => 'px',
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-image-carousel-main .magnific-video .njba-video_icon i',
                            'property'      => 'height',
                            
                        )
                    ),
                    'videoicon_box_radius'    => array(
                        'type'          => 'text',
                        'maxlength'     => '3',
                        'size'          => '5',
                        'label'         => __('Round Corners', 'bb-njba'),
                        'description'   => _x( 'px', 'Value unit for border radius. Such as: "5 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-image-carousel-main .magnific-video .njba-video_icon i',
                            'property'      => 'border-radius'
                        )
                    ),
                    'videoicon_font_size'    => array(
                        'type'          => 'njba-simplify',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                                    'desktop' => '40',
                                    'medium'  => '22',
                                    'small'   => '18',
                        ),
                        'size'          => '5',
                        'maxlength'     => '2',
                        'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-image-carousel-main .magnific-video .njba-video_icon i',
                            'property'      => 'font-size',
                            'unit'          => 'px'
                        )
                    ),
                    'videoicon_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Color', 'bb-njba'),
                        
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-image-carousel-main .magnific-video .njba-video_icon i',
                            'property'      => 'color',
                            )
                    ),
                    'videoicon_bg_color'    => array(
                            'type'          => 'color',
                            'label'         => __('Background Color', 'bb-njba'),
                            
                            'show_reset'    => true,
                            'preview'       => array(
                                'type'          => 'css',
                                'selector'      => '.njba-image-carousel-main .magnific-video .njba-video_icon i',
                                'property'      => 'background',
                            )
                    ),
                    
                )
            ),
            
            
        )
    )
));
/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('njba_photospanel_form', array(
    'title' => __('Add Photo', 'bb-njba'),
    'tabs'  => array(
        'general'      => array( // Tab
            'title'         => __('General', 'bb-njba'), // Tab title
            'sections'      => array( // Tab Sections
                'photo_details'          => array(
                    'title'      => 'Photo Details',
                    'fields'     => array(
                        'photo'     => array(
                            'type'          => 'photo',
                            'label'         => __('Photo', 'bb-njba'),
                            'show_remove'   => true
                        ),
                        'caption_name'     => array(
                            'type'          => 'text',
                            'label'         => __('Caption', 'bb-njba'),
                            'placeholder'   => 'Caption',
                            'preview'       => array(
                                'type'          => 'none'
                            )
                        ),
                        'select_option'       => array(
                            'type'          => 'select',
                            'label'         => __('Lightbox Option', 'bb-njba'),
                            'default'       => '1',
                            'options'       => array(
                                '0'             => __('None', 'bb-njba'),
                                '1'             => __('Photo', 'bb-njba'),
                                '2'             => __('Video', 'bb-njba'),
                            ),
                            'toggle'        => array(
                                '1'         => array(
                                    'fields'        => array('photo_second')
                                ),
                                '2'         => array(
                                    'fields'        => array('video_link','video_icon'),
                                    'sections'      => array('videoicons_setting' )
                                ),
                                '0'         => array(),
                            )
                        ),
                        'photo_second'     => array(
                            'type'          => 'photo',
                            'label'         => __('Photo', 'bb-njba'),
                            'show_remove'   => true
                        ),
                        'video_link'     => array(
                            'type'          => 'text',
                            'label'         => __('Video Link', 'bb-njba'),
                            'placeholder'   => 'http://www.example.com',
                            'preview'       => array(
                                'type'          => 'none'
                            )
                        ),
                        'video_icon'          => array(
                            'type'          => 'icon',
                            'label'         => __('Icon For Video', 'bb-njba'),
                            'preview'   => 'none',
                            'show_remove' => true,
                            'preview'       => array(
                                'type'          => 'css',
                                'selector'      => '.njba-video_icon i',
                             )
                        ),
                        
                    ),
                ),
                
            )
        )
    )
));