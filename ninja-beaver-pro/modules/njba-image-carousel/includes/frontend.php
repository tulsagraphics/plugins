<?php
$number_photos = count($settings->photos);
?>
	<div class="njba-image-carousel-main">
		<div class="njba-image-carousel-body">
		    	<div class="njba-image-carousel-wrapper">
				    <?php 
				    		
				    		for($i=0; $i < $number_photos; $i++)
				    		{
				    			$img_carousel = $settings->photos[$i];
				    			$attachment = get_post( $img_carousel->photo );
				    			
				    ?>			
					    			<div class="njba-image-carousel-box njba-slide-id-<?php echo $i ?> ">
										<?php  if($img_carousel->select_option === "1"){ ?>
													<?php if($settings->click_action === 'lightbox' && $img_carousel->photo_second != null){ ?> 
						            							<a href="<?php echo $img_carousel->photo_second_src; ?>"  title="<?php echo $img_carousel->caption_name; ?>"
						            								 class="<?php  echo $settings->click_action;?> magnific njba-image-carousel" >
													<?php }  ?>
										<?php  }
												elseif($img_carousel->select_option === "2"){
					 									  if($settings->click_action === 'lightbox' && $img_carousel->video_link != null){ ?> 
																<a href="<?php echo $img_carousel->video_link; ?>"   title="<?php echo $img_carousel->caption_name; ?>"  class="magnific-video njba-image-carousel" >
													<?php }  
												}
												else{
					 									  if($settings->click_action === 'lightbox' && $img_carousel->photo != null){ ?> 
																<a href="<?php echo $img_carousel->photo_src; ?>"   title="<?php echo $img_carousel->caption_name; ?>" class="<?php  echo $settings->click_action;?> magnific njba-image-carousel" >
													<?php }  
												}
								 				if($img_carousel->photo != null )
												{ 
																if( $settings->show_captions != 'outside_below' && $settings->show_captions != 'inside_below'){
										?>
																	<div class="njba-image-box-content <?php echo $settings->show_captions; ?> ">
												                    	 <<?php echo $settings->caption_tag; ?> class="njba-caption"><?php echo $img_carousel->caption_name; ?></<?php echo $settings->caption_tag; ?>>
												                    </div>
						                 <?php 
						                 		 				}
										?>
												            		<div class="njba-image-box-img">
												            	        <img src="<?php echo $img_carousel->photo_src; ?>" title="<?php echo $img_carousel->caption_name; ?>" class="<?php echo $settings->hover_effects; ?> njba-image-responsive">
															            <div class="njba-video_overlay">
															            	<div class="njba-video_icon_table">
															            		<div class="njba-video_icon"><i class="<?php echo $img_carousel->video_icon; ?>" aria-hidden="true"></i></div>
															            	</div>
															            </div>
													                </div>
										<?php 	} 
											   				 if( $settings->show_captions != 'outside_top' && $settings->show_captions != 'inside_top'){
										?>
																	<div class="njba-image-box-content <?php echo $settings->show_captions; ?> ">
												                    	 <<?php echo $settings->caption_tag; ?> class="njba-caption"><?php echo $img_carousel->caption_name; ?></<?php echo $settings->caption_tag; ?>>
												                    </div>
						                 <?php 
						                 		 			}
						                 ?>
													                <div class="njba-image-box-overlay <?php echo $settings->hover_effects; ?>">
													                    <div class="njba-image-box-content njba-image-box-hover">
													                    	<?php if($img_carousel->select_option != "2" && $settings->show_captions != 'outside_below' && $settings->show_captions != 'outside_top' ){ ?>
													                            <<?php echo $settings->caption_tag; ?> class="njba-caption"><?php echo $img_carousel->caption_name; ?></<?php echo $settings->caption_tag; ?>>
													                        <?php } ?>
													                     </div>
													                </div>
							            <?php   
							               	 if($settings->click_action === 'lightbox'){   ?> 
															</a>
										<?php } ?>
									</div>
					<?php } ?>
				</div><!--njba-teams-wrapper-->
			
		</div><!--njba-teams-body-->
	</div><!--njba-teams-main-->
    
	
	
	
		