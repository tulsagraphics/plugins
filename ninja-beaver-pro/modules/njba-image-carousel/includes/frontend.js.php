
(function($) {
<?php if ( count($settings->photos ) > 1 ) : ?>
	
	 var window_width =$(window).width();
     var section_width =$(".fl-node-<?php echo $id; ?> .njba-image-carousel-main").width();
	// Create the slider.
	var sliderOptions = {
		auto : true,
		autoStart : <?php echo $settings->autoplay; ?>,
		autoHover : <?php echo $settings->hover_pause; ?>,
		<?php echo $settings->adaptive_height == 'no' ? 'adaptiveHeight: true,' : ''; ?>
		pause : <?php echo $settings->pause * 1000; ?>,
		mode : '<?php echo $settings->transition; ?>',
		speed : <?php echo $settings->speed * 1000;  ?>,
		infiniteLoop : <?php echo $settings->loop;  ?>,
		pager : <?php echo $settings->dots; ?>,
		
		nextText: '<i class="fa fa-angle-right"></i>',
		prevText: '<i class="fa fa-angle-left"></i>',
		controls : <?php echo $settings->arrows; ?>,
		onSliderLoad: function() {
			$('.fl-node-<?php echo $id; ?> .njba-image-carousel-main').addClass('njba-image-carousel-loaded');
		}
	};
	
	
	
    if(window_width > 990) {
        
        var max_slide = <?php if($settings->max_slides['desktop'] !=''){ echo $settings->max_slides['desktop'] ; } else{ echo '1'; }?>;
        var slide_margin = <?php if($settings->slide_margin['desktop'] !=''){ echo $settings->slide_margin['desktop'] ; } else{ echo '0'; }?>;
        var slide_width_cal =  section_width / max_slide ;
        var slide_width = slide_width_cal - slide_margin ;
        
        
        var carouselOptions = {
            minSlides : 1,
            maxSlides : max_slide,
            moveSlides : 1,
            slideWidth : slide_width,
            slideMargin : slide_margin,
        };
       
       
    }
    else if(window_width > 767 && window_width <= 990) {
        
        var max_slide = <?php if($settings->max_slides['medium'] !=''){ echo $settings->max_slides['medium'] ; } else{ echo '1'; }?>;
        var slide_margin = <?php if($settings->slide_margin['medium'] !=''){ echo $settings->slide_margin['medium'] ; } else{ echo '0'; }?>;
        var slide_width_cal =  section_width / max_slide ;
        var slide_width = slide_width_cal - slide_margin ;
        
        var carouselOptions = {
            minSlides : 1,
            maxSlides : max_slide,
            moveSlides : 1,
            slideWidth : slide_width,
            slideMargin : slide_margin,
        };
        
    }
    else if(window_width <= 767) {
        var max_slide = <?php if($settings->max_slides['small'] !=''){ echo $settings->max_slides['small'] ; } else{ echo '1'; }?>;
        var slide_margin = <?php if($settings->slide_margin['small'] !=''){ echo $settings->slide_margin['small'] ; } else{ echo '0'; }?>;
        var slide_width_cal =  section_width / max_slide ;
        var slide_width = slide_width_cal - slide_margin ;
        
        var carouselOptions = {
            minSlides : 1,
            maxSlides : max_slide,
            moveSlides : 1,
            slideWidth : slide_width,
            slideMargin : slide_margin,
        };
         
    }
	$('.fl-node-<?php echo $id; ?> .njba-image-carousel-wrapper').bxSlider($.extend({}, sliderOptions, carouselOptions));
<?php endif; ?>
<?php if($settings->click_action == 'lightbox') : ?>
					
				$(document).ready(function() {
					 $('.njba-image-carousel-wrapper').each(function() {
						  var $container = $(this);
						  var $imageLinks = $container.find('.njba-image-carousel');
						  var items = [];
						  $imageLinks.each(function() {
						    var $item = $(this);
						    var type = 'image';
						    if ($item.hasClass('magnific-video')) {
						      type = 'iframe';
						    }
						    var magItem = {
						      src: $item.attr('href'),
						      type: type
						    };
						    magItem.title = $item.data('title');    
						    items.push(magItem);
						    });
						  $imageLinks.magnificPopup({
						    mainClass: 'mfp-fade',
						    items: items,
						    gallery:{
						        enabled:true,
						        tPrev: $(this).data('prev-text'),
						        tNext: $(this).data('next-text')
						    },
						    type: 'image',
						    callbacks: {
						      beforeOpen: function() {
						        var index = $imageLinks.index(this.st.el);
						        if (-1 !== index) {
						          this.goTo(index);
						        }
						      }
						    }
						  });
						});            
    				});
<?php 
			
	 endif; 
?>
	
})(jQuery);
	
