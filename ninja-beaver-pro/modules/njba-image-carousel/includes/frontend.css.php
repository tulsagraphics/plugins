
.fl-node-<?php echo $id; ?> .njba-image-carousel-main.njba-image-carousel-loaded a.bx-pager-link {
	<?php if( $settings->dot_color ) { ?>background: #<?php echo $settings->dot_color; ?><?php } ?>;
	opacity: 0.5;
}
.fl-node-<?php echo $id; ?> .njba-image-carousel-main.njba-image-carousel-loaded a.bx-pager-link.active {
	<?php if( $settings->active_dot_color ) { ?>background: #<?php echo $settings->active_dot_color; ?>;<?php } ?>
	opacity: 1;
}
.fl-node-<?php echo $id; ?> .njba-image-carousel-main .magnific-video .njba-video_icon i{
		<?php if( $settings->videoicon_font_size['desktop'] != '' ) { ?>font-size:<?php echo $settings->videoicon_font_size['desktop']; ?>px;<?php } ?>
		<?php if( $settings->videoicon_color ) { ?>color:#<?php echo $settings->videoicon_color; ?>;<?php } ?>
		<?php  if( $settings->videoicon_bg_color ) { ?>background: #<?php echo $settings->videoicon_bg_color; ?>; <?php } ?>
		<?php if( $settings->videoicon_width ) { ?> width: <?php echo $settings->videoicon_width; ?>px; <?php } ?>
		<?php if( $settings->videoicon_height ) { ?> height: <?php echo $settings->videoicon_height; ?>px; <?php } ?>
		<?php if( $settings->videoicon_height ) { ?> line-height: <?php echo $settings->videoicon_height; ?>px; <?php } ?>
		<?php if( $settings->videoicon_box_radius ) { ?>border-radius: <?php echo $settings->videoicon_box_radius; ?>px;<?php } ?>
    	
		
}
.fl-node-<?php echo $id; ?> .njba-image-carousel-main .bx-wrapper .bx-controls-direction a.bx-next,
.fl-node-<?php echo $id; ?> .njba-image-carousel-main .bx-wrapper .bx-controls-direction a.bx-prev {
	
	<?php if( $settings->arrow_background ) { ?>background: #<?php echo $settings->arrow_background; ?>;<?php } ?>
	<?php if( $settings->arrow_color ) { ?>color: #<?php echo $settings->arrow_color; ?>;<?php } ?>
	
}
.fl-node-<?php echo $id; ?> .njba-image-box-content <?php echo $settings->caption_tag; ?>.njba-caption {
	<?php if( $settings->caption_alignment ) { ?>text-align: <?php echo $settings->caption_alignment; ?>;<?php } ?>
	<?php if( $settings->caption_color ) { ?>color:#<?php echo $settings->caption_color; ?>;<?php } ?>
	<?php if( $settings->caption_font_size['desktop'] != '' ) { ?>font-size:<?php echo $settings->caption_font_size['desktop']; ?>px;<?php } ?>
	<?php if( $settings->caption_font['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->caption_font ); ?><?php } ?>
		    margin: 0;
}
.fl-node-<?php echo $id; ?> .njba-image-carousel-main .njba-image-box-overlay {
	<?php if( $settings->overly_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->overly_color)) ?>, <?php echo $settings->overly_color_opacity/100; ?>); <?php } ?>
}
/*******Below caption Background************/
.fl-node-<?php echo $id; ?> .njba-image-box-content.inside_top ,
.fl-node-<?php echo $id; ?> .njba-image-box-content.inside_below ,
.fl-node-<?php echo $id; ?> .njba-image-box-content.outside_top ,
.fl-node-<?php echo $id; ?> .njba-image-box-content.outside_below {
	<?php if( $settings->caption_bg_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->caption_bg_color)) ?>, <?php echo $settings->caption_bg_color_opc/100; ?>); <?php } ?>
}
<?php if($global_settings->responsive_enabled) { // Global Setting If started ?>
	@media ( max-width: <?php echo $global_settings->medium_breakpoint .'px'; ?> ) {
		.fl-node-<?php echo $id; ?> .njba-image-box-content <?php echo $settings->caption_tag; ?>.njba-caption {
			<?php if( $settings->caption_font_size['medium'] != '') { ?>font-size:<?php echo $settings->caption_font_size['medium']; ?>px;<?php } ?>
		}
		.fl-node-<?php echo $id; ?> .njba-image-carousel-main .magnific-video .njba-video_icon i{
				<?php if( $settings->videoicon_font_size['medium'] != '') { ?>font-size:<?php echo $settings->videoicon_font_size['medium']; ?>px;<?php } ?>
		}
	}
	@media ( max-width: <?php echo $global_settings->responsive_breakpoint .'px'; ?> ) {
		.fl-node-<?php echo $id; ?> .njba-image-box-content <?php echo $settings->caption_tag; ?>.njba-caption {
			<?php if( $settings->caption_font_size['small'] != '' ) { ?>font-size:<?php echo $settings->caption_font_size['small']; ?>px;<?php } ?>
		}
		.fl-node-<?php echo $id; ?> .njba-image-carousel-main .magnific-video .njba-video_icon i{
				<?php if( $settings->videoicon_font_size['small'] != '' ) { ?>font-size:<?php echo $settings->videoicon_font_size['small']; ?>px;<?php } ?>
		}
	}
<?php } //die();?>
