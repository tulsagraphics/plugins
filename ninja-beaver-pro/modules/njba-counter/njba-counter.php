<?php
class NJBACounterModule extends FLBuilderModule {
    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Counter', 'bb-njba'),
            'description'   => __('Addon for display counter.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'content' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-counter/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-counter/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'partial_refresh' => true, // Defaults to false and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
        $this->add_js( 'jquery-waypoints' );
    }
    /** 
     * Use this method to work with settings data before
     * it is saved. You must return the settings object.
     *
     * @method update
     * @param $settings {object}
     */
    public function update($settings)
    {
        return $settings;
    }
    /**
     * This method will be called by the builder
     * right before the module is deleted.
     *
     * @method delete
     */
    public function delete()
    {
    }
    public function render_number(){
        $number = 0;//$this->settings->number ? $this->settings->number : 0;
        $layout = $this->settings->layout ? $this->settings->layout : 'default';
        $type   = $this->settings->number_type ? $this->settings->number_type : 'percent';
        $prefix = $type == 'percent' ? '' : $this->settings->number_prefix;
        $suffix = $type == 'percent' ? '%' : $this->settings->number_suffix;
        //$html = '<h1 class="njba-number-string"><span class="njba-number-int">'. $number .'</span></h1>';
        $html = '<h1 class="njba-counter-string">' . $prefix . '<span class="njba-counter-int">'. $number .'</span>' . $suffix . '</h1>';
        echo $html;
    }
    public function render_circle_bar(){
        $width = !empty( $this->settings->circle_width ) ? $this->settings->circle_width : 300;
        $pos = ( $width / 2 );
        $radius = $pos - 10;
        $dash = number_format( ( ( M_PI * 2 ) * $radius ), 2, '.', '');
        $html = '<div class="svg-container">';
        $html .= '<svg class="svg" viewBox="0 0 '. $width .' '. $width .'" version="1.1" preserveAspectRatio="xMinYMin meet">
            <circle class="njba-bar-bg" r="'. $radius .'" cx="'. $pos .'" cy="'. $pos .'" fill="transparent" stroke-dasharray="'. $dash .'" stroke-dashoffset="0"></circle>
            <circle class="njba-bar" r="'. $radius .'" cx="'. $pos .'" cy="'. $pos .'" fill="transparent" stroke-dasharray="'. $dash .'" stroke-dashoffset="'. $dash .'" transform="rotate(-90.1 '. $pos .' '. $pos .')"></circle>
        </svg>';
        $html .= '</div>';
        echo $html;
    }
    public function render_before_number_text(){
        $html = '';
        if( !empty( $this->settings->before_number_text ) ) {
            $html .= '<span class="njba-counter-before-text">' . esc_html( $this->settings->before_number_text ) . '</span>';
        }
        echo $html;
    }
    public function render_after_number_text(){
        $html = '';
        if( !empty( $this->settings->after_number_text ) ) {
            $html .= '<span class="njba-counter-after-text">' . esc_html( $this->settings->after_number_text ) . '</span>';
        }
        echo $html;
    }
    public function render_separator() {
        if( $this->settings->show_separator == 'yes' ) {
            $separator_settings = array(
                'separator_border_color'         => $this->settings->separator_color,
                'separator_normal_width'         => $this->settings->separator_width,
                'icon_position'                  => $this->settings->separator_alignment,
                'separator_border_style'         => $this->settings->separator_style
            );
            echo '<div class="njba-counter-separator">';
            FLBuilder::render_module_html('njba-separator', $separator_settings);
            echo '</div>';
        }
    }
}
FLBuilder::register_module('NJBACounterModule', array(
    'general'       => array( // Tab
        'title'         => __('General', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'counter_general'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array( // Section Fields
                    'layout' => array(
                        'type'          => 'select',
                        'label'         => __( 'Layout', 'bb-njba' ),
                        'default'       => 'default',
                        'options'       => array(
                            'default'       => __( 'Only Numbers', 'bb-njba' ),
                            'circle'        => __( 'Circle Counter', 'bb-njba' ),
                            'bars'          => __( 'Bars Counter', 'bb-njba' ),
                        ),
                        'toggle'        => array(
                            'default'   => array(
                                'sections'      => array( 'separator', 'overall_structure' ),
                                'fields'        => array( )
                            ),
                            'circle'        => array(
                                'sections'      => array( 'circle_bar_style', 'separator', 'overall_structure' ),
                                'fields'        => array( )
                            ),
                            'bars'          => array(
                                'sections'      => array( 'bar_style' ),
                                'fields'        => array( 'number_position' ),
                            ),
                        ),
                    ),
                    'number_type' => array(
                        'type'          => 'select',
                        'label'         => __( 'Number Type', 'bb-njba' ),
                        'default'       => 'standard',
                        'options'       => array(
                            'percent'       => __( 'Percent', 'bb-njba' ),
                            'normal'      => __( 'Normal', 'bb-njba' ),
                        ),
                        'toggle'        => array(
                            'percent'   => array(
                                'sections'      => array( ),
                                'fields'        => array( )
                            ),
                            'normal'        => array(
                                'sections'      => array( ),
                                'fields'        => array( 'max_number', 'number_prefix', 'number_suffix' )
                            )
                        ),
                    ),
                    'number'   => array(
                        'type'          => 'text',
                        'label'         => __('Number', 'bb-njba'),
                        'size'          => '8',
                        'placeholder'   => '',
                        'help'          => __('Enter Counter Value','bb-njba'),
                    ),
                    'max_number' => array(
                        'type'          => 'text',
                        'label'         => __('Total', 'bb-njba'),
                        'size'          => '5',
                        'help'          => __( 'The total number of units for this counter. For example, if the Number is set to 250 and the Total is set to 500, the counter will animate to 50%.', 'bb-njba' ),
                    ),
                    'number_position'   => array(
                        'type'          => 'select',
                        'label'         => __('Number Position', 'bb-njba'),
                        'size'          => '5',
                        'help'          => __( 'Where to display the number in relation to the bar.', 'bb-njba' ),
                        'default'       => 'default',
                        'options'       => array(
                            'none'      => __( 'None', 'bb-njba' ),
                            'default'   => __( 'Inside Bar', 'bb-njba' ),
                            'above'     => __( 'Above Bar', 'bb-njba' ),
                            'below'     => __( 'Below Bar', 'bb-njba' ),
                        ),
                    ),
                    'number_prefix' => array(
                        'type'          => 'text',
                        'label'         => __('Number Prefix', 'bb-njba'),
                        'size'          => '10',
                        
                    ),
                    'number_suffix' => array(
                        'type'          => 'text',
                        'label'         => __('Number Suffix', 'bb-njba'),
                        'size'          => '10',
                    ),
                    'before_number_text' => array(
                        'type'          => 'text',
                        'label'         => __('Before text', 'bb-njba'),
                        'size'          => '10',
                        
                    ),
                    'after_number_text' => array(
                        'type'          => 'text',
                        'label'         => __('After text', 'bb-njba'),
                        'size'          => '10',
                        
                    )
                )
            ),
            'separator'       => array( // Section
                'title'         => __('Separator ( Below Number )','bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'show_separator' => array(
                        'type'          => 'select',
                        'label'         => __( 'Show separator', 'bb-njba' ),
                        'default'       => 'no',
                        'options'       => array(
                            'yes'      => __( 'Yes', 'bb-njba' ),
                            'no'      => __( 'No', 'bb-njba' )
                        ),
                        'toggle'        => array(
                            'yes'      => array(
                                'fields'        => array( 'separator_color', 'separator_height', 'separator_width', 'separator_style', 'separator_top_margin', 'separator_bottom_margin' ),
                                'sections'      => array( 'seprator_margin_style' )
                            ),
                            'no'      => array()
                        )
                    ),
                    'separator_style' => array(
                        'type'          => 'select',
                        'label'         => __( 'Style', 'bb-njba' ),
                        'default'       => 'solid',
                        'options'       => array(
                            'solid'     => __( 'Solid', 'bb-njba' ),
                            'dashed'        => __( 'Dashed', 'bb-njba' ),
                            'dotted'        => __( 'Dotted', 'bb-njba' ),
                            'double'        => __( 'Double', 'bb-njba' ),
                        ),
                        'help'          => __( 'The type of border to use. Double borders must have a height of at least 3px to render properly.', 'bb-njba' ),
                    ),
                    'separator_color'    => array( 
                        'type'       => 'color',
                        'label'         => __('Separator Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                        
                    ),
                    'separator_height' => array(
                        'type'          => 'text',
                        'label'         => __('Thickness', 'bb-njba'),
                        'size'          => '5',
                        'placeholder'   => '1',
                        'description'   => 'px',
                        'help'          => __( 'Adjust thickness of border.', 'bb-njba' ),
                    ),
                    'separator_width' => array(
                        'type'          => 'text',
                        'label'         => __('Width', 'bb-njba'),
                        'size'          => '5',
                        'placeholder'   => '100',
                        'description'   => '%',
                    ),
                    'separator_alignment' => array(
                        'type'          => 'select',
                        'label'         => __( 'Alignment', 'bb-njba' ),
                        'default'       => 'inherit',
                        'options'       => array(
                            'inherit'   => __( 'Default', 'bb-njba' ),
                            'left'      => __( 'Left', 'bb-njba' ),
                            'right'     => __( 'Right', 'bb-njba' ),
                            'center'    => __( 'Center', 'bb-njba' ),
                        ),
                    ),
                )
            ),
            'animation'       => array( // Section
                'title'         => __('Animation','bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'animation_speed' => array(
                        'type'          => 'text',
                        'label'         => __('Animation Speed', 'bb-njba'),
                        'size'          => '5',
                        'placeholder'   => '1',
                        'description'   => __( 'second(s)', 'bb-njba' ),
                        'help'          => __( 'Number of seconds to complete the animation.', 'bb-njba' )
                    ),
                    'delay'          => array(
                        'type'          => 'text',
                        'label'         => __('Animation Delay', 'bb-njba'),
                        'size'          => '5',
                        'placeholder'   => '1',
                        'description'   => __( 'second(s)', 'bb-njba' )
                    ),
                )
            ),
        )
    ),
    'style'         => array( // Tab
        'title'         => __('Style', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'common_style'      => array(
                'title'         => __('Common style'),
                'fields'        => array(
                    'align'     => array(
                        'type'          => 'select',
                        'label'         => __( 'Overall Alignment', 'bb-njba' ),
                        'default'       => 'center',
                        'options'       => array(
                            'left'      => __( 'Left', 'bb-njba' ),
                            'right'     => __( 'Right', 'bb-njba' ),
                            'center'    => __( 'Center', 'bb-njba' ),
                        ),
                    )
                )
            ),
            'circle_bar_style'    => array(
                'title'         => __('Circle Bar Styles', 'bb-njba'),
                'fields'        => array(
                    'circle_width' => array(
                        'type'          => 'text',
                        'label'         => __('Circle Size', 'bb-njba'),
                        'placeholder'   => '300',
                        'maxlength'     => '4',
                        'size'          => '4',
                        'description'   => 'px',
                    ),
                    'circle_dash_width' => array(
                        'type'          => 'text',
                        'label'         => __('Circle Stroke Size', 'bb-njba'),
                        'placeholder'   => '10',
                        'maxlength'     => '2',
                        'size'          => '4',
                        'description'   => 'px',
                    ),
                    'circle_color'    => array( 
                        'type'       => 'color',
                        'label'         => __('Circle Foreground Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    'circle_bg_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('Circle Background Color', 'bb-njba'),
                        'default'    => 'fafafa',
                        'show_reset' => true,
                    ),
                )
            ),
            'bar_style'    => array(
                'title'         => __('Bar Styles', 'bb-njba'),
                'fields'        => array(
                    'bar_color'    => array( 
                        'type'       => 'color',
                        'label'         => __('Bar Foreground Color', 'bb-njba'),
                        'default'    => '000000',
                        'show_reset' => true,
                    ),
                    'bar_bg_color'    => array( 
                        'type'       => 'color',
                        'label'      => __('Bar Background Color', 'bb-njba'),
                        'default'    => 'fafafa',
                        'show_reset' => true,
                    ),
                )
            ),
        )
    ),
    'typography'    => array(
        'title'         => __('Typography', 'bb-njba'),
        'sections'      => array(
            'number_typography' => array(
                'title' => __('Number Text', 'bb-njba' ),
                'fields'    => array(
                    'num_font_family'       => array(
                        'type'          => 'font',
                        'label'         => __('Font Family', 'bb-njba'),
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 'Default'
                        ),
                        'preview'   => array(
                            'type'      => 'font',
                            'selector'  => '.njba-counter-string'
                        ),
                    ),
                    'num_font_size'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Font Size', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '26',
                            'medium'        => '24',
                            'small'         => '22',
                        ),
                    ),
                    'num_line_height'    => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Line Height', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '26',
                            'medium'        => '24',
                            'small'         => '22',
                        ),
                    ),
                    'num_color'        => array( 
                        'type'       => 'color',
                        'label'      => __('Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                )
            ),
            'suffix_typography' => array(
                'title' => __('Before / After Text', 'bb-njba' ),
                'fields'    => array(
                    'ba_font_family'       => array(
                        'type'          => 'font',
                        'label'         => __('Font Family', 'bb-njba'),
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 'Default'
                        ),
                        'preview'   => array(
                            'type'      => 'font',
                            'selector'  => '.njba-counter-after-text'
                        ),
                    ),
                    'ba_font_size'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Font Size', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '20',
                            'medium'        => '18',
                            'small'         => '18',
                        ),
                    ),
                    'ba_line_height'    => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Line Height', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '20',
                            'medium'        => '18',
                            'small'         => '18',
                        ),
                    ),
                    'ba_color'        => array( 
                        'type'       => 'color',
                        'label'      => __('Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                )
            ),
        )
    )
));