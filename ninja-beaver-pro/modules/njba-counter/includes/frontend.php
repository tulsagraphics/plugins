
<div class="njba-counter njba-counter-<?php echo $settings->layout;?>">
	<?php if($settings->layout == 'circle') { ?>
		<div class="njba-counter-circle-main">
			<div class="njba-counter-text">
				<?php $module->render_before_number_text(); ?>
				<?php $module->render_number(); ?>
				<?php $module->render_separator(); ?>
				<?php $module->render_after_number_text(); ?>
			</div>
			<?php $module->render_circle_bar(); ?>
		</div>
	<?php } elseif ($settings->layout == 'bars') { ?>
		<div class="njba-counter-text njba-counter-pos-<?php echo $settings->number_position; ?>">
			<?php $module->render_before_number_text(); ?>
			<?php $number_position = $settings->number_position ? $settings->number_position : 'default'; ?>
			<?php if($number_position == 'above') {?>
				<?php $module->render_number(); ?>
				<div class="njba-counter-bars-main">
					<div class="njba-counter-bar"></div>
				</div>
			<?php } elseif ($number_position == 'below') {
				?>
				<div class="njba-counter-bars-main">
					<div class="njba-counter-bar"></div>
				</div>
				<?php $module->render_number(); ?>
			<?php } else { ?>
			<div class="njba-counter-bars-main">
				<div class="njba-counter-bar">
					<?php /* Number Inside Bar */
						$module->render_number(); 
					?>
				</div>
			</div>
			<?php } ?>
			<?php $module->render_after_number_text(); ?>
		</div>
	<?php } else {
		$module->render_before_number_text();
		$module->render_number();
		$module->render_separator();
		$module->render_after_number_text();
		}  ?>
</div>