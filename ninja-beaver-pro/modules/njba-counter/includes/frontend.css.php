<?php
$settings->circle_width = ( trim($settings->circle_width) !== '' ) ? $settings->circle_width : '300';
$settings->circle_dash_width = ( trim($settings->circle_dash_width) !== '' ) ? $settings->circle_dash_width : '10';
?>
/* Number Text Typography */
.fl-node-<?php echo $id; ?> h1.njba-counter-string {
   	<?php if( $settings->num_font_family['family'] != "Default") { ?> <?php FLBuilderFonts::font_css( $settings->num_font_family ); ?> <?php } ?>
	<?php if( $settings->num_font_size['desktop'] != '' ) { ?> font-size: <?php echo $settings->num_font_size['desktop']; ?>px; <?php } ?>
	<?php if( $settings->num_line_height['desktop'] != '' ) { ?> line-height: <?php echo $settings->num_line_height['desktop']; ?>px; <?php } ?>
	<?php if( $settings->num_color != '' ) { ?> color: #<?php echo $settings->num_color; ?>; <?php } ?>
}
/* Before After Text Typography */
.fl-node-<?php echo $id; ?> .njba-counter-before-text,
.fl-node-<?php echo $id; ?> .njba-counter-after-text {
   	<?php if( $settings->ba_font_family['family'] != "Default") { ?> <?php FLBuilderFonts::font_css( $settings->ba_font_family ); ?>
	<?php } ?>
	<?php if( $settings->ba_font_size['desktop'] != '' ) { ?> font-size: <?php echo $settings->ba_font_size['desktop']; ?>px; <?php } ?>
	<?php if( $settings->ba_line_height['desktop'] != '' ) { ?> line-height: <?php echo $settings->ba_line_height['desktop']; ?>px; <?php } ?>
	<?php if( $settings->ba_color != '' ) { ?> color: #<?php echo $settings->ba_color; ?>; <?php } ?>
}
<?php if( isset( $settings->layout ) && $settings->layout == 'circle' ) : ?>
	.fl-node-<?php echo $id ?> .njba-counter-text{
		position: absolute;
		top: 50%;
		left: 50%;
		-webkit-transform: translate(-50%,-50%);
		   -moz-transform: translate(-50%,-50%);
		    -ms-transform: translate(-50%,-50%);
		        transform: translate(-50%,-50%);
	}
	.fl-node-<?php echo $id ?> .njba-counter-circle-main{
		<?php 
			if( !empty( $settings->circle_width ) ) {
				echo 'max-width: '. $settings->circle_width .'px;';
				echo 'max-height: '. $settings->circle_width .'px;';
			} else {
				echo 'max-width: 100px;';
				echo 'max-height: 100px;';				
			}
		?>
		text-align: <?php echo $settings->align; ?>;
		margin-top: 0;
		margin-bottom: 0;
		margin-left: <?php echo ( $settings->align != 'left' ) ? 'auto' : '0' ?>;
		margin-right: <?php echo ( $settings->align != 'right' ) ? 'auto' : '0' ?>;
		position: relative;
	}
	.fl-node-<?php echo $id ?> .svg circle{
	<?php 
		if( !empty( $settings->circle_dash_width ) ) {
			echo 'stroke-width: '. $settings->circle_dash_width .'px;';
		}
	?>
	}
	.fl-node-<?php echo $id ?> .svg .njba-bar-bg{
	<?php 
		if( !empty( $settings->circle_bg_color ) ) { ?>
			echo 'stroke: #'. $settings->circle_bg_color .';';
			stroke: rgb( <?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->circle_bg_color )) ?>);
		<?php } else {
			echo 'stroke: transparent;';
		}
	?>
	}
	.fl-node-<?php echo $id ?> .svg .njba-bar{
	<?php 
		echo 'stroke: '.  "#000000;";
	?>
		stroke: rgb( <?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->circle_color )) ?>);
	}
<?php elseif( isset( $settings->layout ) && $settings->layout == 'bars' ) : ?>
	.fl-node-<?php echo $id ?> .njba-counter-bars-main{
		width: 100%;
		/*background-color: <?php echo '#'.$settings->bar_bg_color ?>;*/
		<?php if( $settings->bar_bg_color ) { ?>background-color: rgb(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->bar_bg_color )) ?>); <?php } ?>
	}
	.fl-node-<?php echo $id ?> .njba-counter-bar{
		width: 0;
		/*background-color: <?php echo '#'.$settings->bar_color?>;*/
		<?php if( $settings->bar_color ) { ?>background-color: rgb(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->bar_color )) ?>); <?php } ?>
	}
<?php endif; ?>
.fl-node-<?php echo $id ?> .fl-module-content {
	text-align: <?php echo $settings->align; ?>;
}
/* Render Separator */
<?php if( $settings->show_separator == 'yes' ) {
	FLBuilder::render_module_css('njba-separator', $id, array(
		'separator_border_color'			=> $settings->separator_color,
		'separator_normal_width'			=> $settings->separator_width,
		'icon_position'		=> $settings->separator_alignment,
		'separator_border_style'			=> $settings->separator_style
	)); ?>
	.fl-builder-content .fl-node-<?php echo $id; ?> .njba-separator {
		<?php
		if( $settings->separator_top_margin != '' ) {
		?>
		margin-top: <?php echo $settings->separator_top_margin; ?>px;
		<?php
		}
		if( $settings->separator_bottom_margin != '' ) {
		?>
		margin-bottom: <?php echo $settings->separator_bottom_margin; ?>px;
		<?php
		}
		?>
	}
<?php } ?>
<?php if($global_settings->responsive_enabled) { // Global Setting If started 
	if( $settings->num_font_size['medium'] != "" || $settings->num_line_height['medium'] != "" || 
		$settings->ba_font_size['medium'] != "" || $settings->ba_line_height['medium'] != "" )
	{
		/* Medium Breakpoint media query */	
	?>
		@media ( max-width: <?php echo $global_settings->medium_breakpoint .'px'; ?> ) {
			/* Number Text Typography */
			.fl-node-<?php echo $id; ?> h1.njba-counter-string {
				<?php if( $settings->num_font_size['medium'] != '' ) { ?>
					font-size: <?php echo $settings->num_font_size['medium']; ?>px;
					line-height: <?php echo $settings->num_font_size['medium'] + 2; ?>px;
				<?php } ?>
				<?php if( $settings->num_line_height['medium'] != '' ) { ?>
					line-height: <?php echo $settings->num_line_height['medium']; ?>px;
				<?php } ?>
			}
			/* Before After Text */
			.fl-node-<?php echo $id; ?> .njba-counter-before-text, 
			.fl-node-<?php echo $id; ?> .njba-counter-after-text {
				<?php if( $settings->ba_font_size['medium'] != '' ) { ?>
					font-size: <?php echo $settings->ba_font_size['medium']; ?>px;
				<?php } ?>
				<?php if( $settings->ba_line_height['medium'] != '' ) { ?>
					line-height: <?php echo $settings->ba_line_height['medium']; ?>px;
				<?php } ?>
			}
		}		
	<?php
	}
	if( $settings->num_font_size['small'] != "" || $settings->num_line_height['small'] != "" || 
		$settings->ba_font_size['small'] != "" || $settings->ba_line_height['small'] != "" )
	{
		/* Small Breakpoint media query */	
	?>
		@media ( max-width: <?php echo $global_settings->responsive_breakpoint .'px'; ?> ) {
			/* Number Text Typography */
			.fl-node-<?php echo $id; ?> h1.njba-counter-string {
				<?php if( $settings->num_font_size['small'] != '' ) { ?>
					font-size: <?php echo $settings->num_font_size['small']; ?>px;
					line-height: <?php echo $settings->num_font_size['small'] + 2; ?>px;
				<?php } ?>
				<?php if( $settings->num_line_height['small'] != '' ) { ?>
					line-height: <?php echo $settings->num_line_height['small']; ?>px;
				<?php } ?>
			}
			/* Before After Text */
			.fl-node-<?php echo $id; ?> .njba-counter-before-text, 
			.fl-node-<?php echo $id; ?> .njba-counter-after-text {
				<?php if( $settings->ba_font_size['small'] != '' ) { ?>
					font-size: <?php echo $settings->ba_font_size['small']; ?>px;
				<?php } ?>
				<?php if( $settings->ba_line_height['small'] != '' ) { ?>
					line-height: <?php echo $settings->ba_line_height['small']; ?>px;
				<?php } ?>
			}
		}
	<?php
	}
} /* Typography responsive layout Ends here*/ ?>