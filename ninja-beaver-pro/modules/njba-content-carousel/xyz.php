<?php

function CreateFSVRSoapClient($use_beta = false) {

    $fsvr_escapia_net_username = get_option('fsvr_escapia_net_username', '');
    $fsvr_escapia_net_password = fsvr_decrypt(get_option('fsvr_escapia_net_password', ''));

    $client = new FSVRSoapClient($fsvr_escapia_net_username, $fsvr_escapia_net_password, $use_beta);

    return $client;
}


function fsvr_get_unit_codes_from_post_ids($post_id) {
    global $wpdb;

    $prepare_str_array = array();
    $prepare_values = array();
    foreach($post_id as $pid) {
        $prepare_str_array[]  = "%s";
        $prepare_values[] = $pid;
    }

    $prepare_str = implode(",", $prepare_str_array);

    $results = $wpdb->get_results(
        $wpdb->prepare("SELECT post_id, meta_value FROM $wpdb->postmeta WHERE meta_key = 'fsvr_unit_code' AND post_id in (" . $prepare_str . ")", $prepare_values )
        
    );

    $unit_code_lookup = array();

    foreach($results as $r) {
        $unit_code_lookup[$r->post_id] = $r->meta_value;
    }

    $unit_codes = array();
    foreach($post_id as $post_id) {
        $unit_codes[] = $unit_code_lookup[$post_id];
    }

    return $unit_codes;
}
?>