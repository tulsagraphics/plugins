<div class="njba-content-post njba-blog-posts-slide-<?php  echo $i; ?> njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>"  itemscope itemtype="<?php NJBAContentCarouselModule::schema_itemtype(); ?>">
	
	<?php NJBAContentCarouselModule::schema_meta(); ?>
	<?php
		//$fsvr_client = CreateFSVRSoapClient();
     	//$unitCodes = $module->get_unit_code_njba(get_the_ID());
     	//$unitInfo = $fsvr_client->getUnitInfo($unitCodes);

        $post_id = get_the_ID();
        //$unitCodes = get_post_meta($post_id,'fsvr_unit_code',true);
        $image_url = get_post_meta($post_id,'fsvr_unit_image_url',true);
        $description = get_post_meta($post_id,'fsvr_unit_description',true);



	?>
	<div class="featured-property">
        <div class="featured-property-img">
            <?php  $module->post_image_render($image_url);?>
        </div>
        <div class="featured-property-description">
            <div class="featured-property-title">
                <<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
                   <?php /*?> <?php
                        $module->content_meta(get_the_ID());
                    ?><?php */?>
            </div>
            <div class="featured-property-content"><?php $module->excerpt_text($description); ?></div>
            <div class="property-read-more">
                <?php $module->button_render(); ?>
            </div>
        </div>    
    </div>
</div>