<?php
// Get the query data.
$query = FLBuilderLoop::query($settings);
//echo"<pre>";print_r($settings);
$args = array( 
   	'post_type' => 'fsvr',
  	'posts_per_page' => $settings->posts_per_page,
  	'meta_query'    => array(
        array(
            "key" => "feature",
        	"value" => '1',
        	"compare" => "LIKE"
        )
    )
);
$query = new WP_Query( $args );
//print_r($query);
             
           
// Render the posts.
if($query->have_posts()) :
// echo '<pre>';
// print_r($query);
?>
<?php 
	if( $settings->layout == 'masonry' ) {
		$module->render_masonary_filters();
	}
?>
<div class="njba-content-grid-section-wrapper">
	<div class="njba-blog-posts-<?php echo $settings->layout; ?>" itemscope="itemscope" itemtype="http://schema.org/Blog">
		<div class="njba-blog-row">
			<div class="njba-blog-posts-wrapper njba-content-post-<?php echo $settings->layout; ?> ">
				<?php
				$i=1;
				while($query->have_posts()) {
					$query->the_post();
					if($settings->layout == 'grid' || $settings->layout == 'masonry' || $settings->layout == 'carousel')
					{
						include 'post-grid.php';
					}
					else if( $settings->layout == 'flipbox'){
						include 'post-flipbox.php';
					}
					
					$i++;
				}
				if($settings->layout == 'grid' || $settings->layout == 'masonry' || $settings->layout == 'flipbox')
				{
				?>
				<div class="njba-grid-space"></div>
				<?php } ?>
			</div>
			
		</div>
	</div>
	<?php 
	if($settings->layout == 'grid' || $settings->layout == 'masonry' || $settings->layout == 'flipbox'):
			if($settings->scroll_img != '' && $settings->pagination == 'scroll') { ?>
				<div id="post-loading" style="display: none;">
					<img alt="Loading..." src="<?php echo $settings->scroll_img_src;?>" >
				</div>
				
		<?php }else if($settings->scroll_img == '' && $settings->pagination == 'scroll') { ?>
				<div id="post-loading" style="display: none;">
					<img alt="Loading..." src="<?php echo NJBA_MODULE_URL;?>modules/njba-content-grid/images/loader.gif" >
				</div>
		<?php } 
	endif;
	?>
</div>
<?php endif; ?>
<?php
// Render the pagination.
if($settings->layout == 'grid' ):
	if($settings->pagination != 'none' && $query->have_posts()) :
	?>
	<div class="njba-pagination fl-builder-pagination"<?php if($settings->pagination == 'scroll') echo ' style="display:none;"'; ?>>
		<?php //FLBuilderLoop::pagination($query); 
				$total = 0;
				$page = 0;
				$paged = is_front_page() ? get_query_var('page') : get_query_var('paged');
				$total_posts_count = $settings->total_posts_count;
				$posts_aval = $query->found_posts;
				$permalink_structure = get_option('permalink_structure');
				//print_r($permalink_structure);
				if( $settings->total_post == 'custom' && $total_posts_count != $posts_aval ) {
					if( $total_posts_count > $posts_aval ) {
						$page = $posts_aval / $settings->posts_per_page;
						$total = $posts_aval % $settings->posts_per_page;
					}
					if( $total_posts_count < $posts_aval ) {
						$page = $total_posts_count / $settings->posts_per_page;
						$total = $total_posts_count % $settings->posts_per_page;
					}
					if( $total > 0 ) {
						$page = $page + 1;
					}
				}
				else {
					$page = $query->max_num_pages;
				}
				if(empty($permalink_structure)) {
					$format = '&paged=%#%';
				}
				else if ("/" == substr($permalink_structure, -1)) {
					$format = 'page/%#%/';
				}
				else {
					$format = '/page/%#%/';
				}
			    echo paginate_links(array(
			        'base' 		=> get_pagenum_link(1) . '%_%',
			        'format' 	=> $format,
			        'current' 	=> max( 1, $paged ),
			        'total' 	=> $page,
					'type'		=> 'list'
			    ));
		?>
	</div>
	<?php endif; ?>
	<?php
	// Render the empty message.
	if(!$query->have_posts() && (defined('DOING_AJAX') || isset($_REQUEST['fl_builder']))) :
	?>
	<div class="njba-post-grid-empty">
		<?php 
		if (isset($settings->no_results_message)) :
			echo $settings->no_results_message;
		else :
			_e( 'No posts found.', 'fl-builder' );
		endif; 
		?>
	</div>
		
	<?php
	endif;
endif;
wp_reset_postdata();
?>
