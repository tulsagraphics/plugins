(function($) {
<?php  if($settings->layout == 'carousel') : ?>
	
	var window_width = $(window).width();
   
    var section_width =$(".fl-node-<?php echo $id; ?> .njba-blog-posts-carousel").width();
	
	// Create the slider.
	var sliderOptions = {
		auto : true,
		autoStart : <?php echo $settings->autoplay; ?>,
		autoHover : <?php echo $settings->hover_pause; ?>,
		<?php echo $settings->adaptive_height == 'no' ? 'adaptiveHeight: true,' : ''; ?>
		pause : <?php echo $settings->pause * 1000; ?>,
		mode : '<?php echo $settings->transition; ?>',
		speed : <?php echo $settings->speed * 1000;  ?>,
		infiniteLoop : <?php echo $settings->loop;  ?>,
		pager : <?php echo $settings->dots; ?>,
		
		nextText: '<i class="fa fa-angle-right"></i>',
		prevText: '<i class="fa fa-angle-left"></i>',
		controls : <?php echo $settings->arrows; ?>,
		onSliderLoad: function() {
			$('.fl-node-<?php echo $id; ?> .njba-blog-posts-carousel').addClass('njba-blog-posts-carousel-loaded');
		}
	};
	 if(window_width > 990) {
	 	
	 	var max_slide = <?php if($settings->max_slides['desktop'] !=''){ echo $settings->max_slides['desktop'] ; } else{ echo '1'; }?>;
	 	var slide_margin = <?php if($settings->slide_margin['desktop'] !=''){ echo $settings->slide_margin['desktop'] ; } else{ echo '0'; }?>;
    	var slide_width_cal =  section_width / max_slide ;
	    var slide_width = slide_width_cal - slide_margin ;
		
        var carouselOptions = {
            minSlides : 1,
            maxSlides : max_slide,
            moveSlides : 1,
            slideWidth : slide_width,
            slideMargin : slide_margin,
        };
       
       
    }
    else if(window_width > 767 && window_width <= 990) {
    	
	 	var max_slide = <?php if($settings->max_slides['medium'] !=''){ echo $settings->max_slides['medium'] ; } else{ echo '1'; }?>;
	 	var slide_margin = <?php if($settings->slide_margin['medium'] !=''){ echo $settings->slide_margin['medium'] ; } else{ echo '0'; }?>;
    	var slide_width_cal =  section_width / max_slide ;
	    var slide_width = slide_width_cal - slide_margin ;
		
		var carouselOptions = {
            minSlides : 1,
            maxSlides : max_slide,
            moveSlides : 1,
            slideWidth : slide_width,
            slideMargin : slide_margin,
        };
        
	}
    else if(window_width <= 767) {
    	var max_slide = <?php if($settings->max_slides['small'] !=''){ echo $settings->max_slides['small'] ; } else{ echo '1'; }?>;
	 	var slide_margin = <?php if($settings->slide_margin['small'] !=''){ echo $settings->slide_margin['small'] ; } else{ echo '0'; }?>;
    	var slide_width_cal =  section_width / max_slide ;
	    var slide_width = slide_width_cal - slide_margin ;
		
        var carouselOptions = {
            minSlides : 1,
            maxSlides : max_slide,
            moveSlides : 1,
            slideWidth : slide_width,
            slideMargin : slide_margin,
        };
         
    }
   
	$('.fl-node-<?php echo $id; ?> .njba-blog-posts-wrapper').bxSlider($.extend({}, sliderOptions, carouselOptions));
<?php endif; 
 if($settings->layout == 'masonry') : ?>
 		
 		var $grid = $('.fl-node-<?php echo $id; ?> .njba-blog-posts-masonry').imagesLoaded( function() {
					$grid.isotope({
					  
						  	itemSelector: '.njba-content-post',
						  	columnWidth: '.njba-content-post',
						  
					});
			}); 
 		var nodeClass = '.fl-node-<?php echo $id; ?>';
 		var id ='<?php echo $id; ?>';
		
		var $grid = $('.fl-node-<?php echo $id; ?> .njba-blog-posts-masonry').isotope({
				  itemSelector: '.njba-content-post',
				  layoutMode: 'masonry',
				 
				  masonry: {
				    	columnWidth: '.njba-blog-posts-col-<?php  echo $settings->show_col; ?>'
		  		  }
		});
		
		
		jQuery( nodeClass ).find('.njba-masonary-filters .njba-masonary-filter').on('click', function(){
                jQuery( this ).siblings().removeClass( 'njba-masonary-current' );
                jQuery( this ).addClass( 'njba-masonary-current' );
                var value = jQuery( this ).data( 'filter' );
                $grid.isotope( { filter: value } );
                	var res = value.slice(1);
               
                	if(res == ''){
                			jQuery('.fl-node-<?php echo $id; ?> .njba-blog-posts-masonry').find('.njba-content-post').css({'visibility': 'visible', 'display': 'block'});
                	}
                	else{
	               		var acc = document.getElementsByClassName("njba-content-post");
						var i;
							for (i = 0; i < acc.length; i++) {
							//console.log(acc[i]);
							    if($(acc[i]).hasClass(res)){
							    	$('.fl-node-<?php echo $id; ?>' + acc[i]).css({'visibility': 'visible', 'display': 'block'});
								} 
								else  if($(acc[i]).hasClass('*')){
									$('.fl-node-<?php echo $id; ?>' + acc[i]).css({'visibility': 'visible', 'display': 'block'});
								}
								else{
									$('.fl-node-<?php echo $id; ?>' +  acc[i]).css({'visibility': 'hidden', 'display': 'none'});
								} 
							}
					}
        });
        jQuery( nodeClass + ' .njba-blog-posts-masonary' ).masonry({
                columnWidth: '.njba-content-post',
                itemSelector: '.njba-content-post'
        });
        var document_width, document_height;
        jQuery(document).ready( function() {
			var hashval = window.location.hash,
				hashval = hashval.replace( '#' , '' );
			if( hashval != '' ) {
				
				jQuery('.fl-node-<?php echo $id; ?> .njba-masonary-filters li').removeClass('njba-masonary-current');
				
				jQuery('.fl-node-<?php echo $id; ?> .njba-masonary-filters li[data-filter=".category-' + hashval + '"]').addClass('njba-masonary-current');
			    jQuery( '.fl-node-<?php echo $id; ?> .njba-masonary-filters .njba-masonary-filter.njba-masonary-current' ).trigger('click');
			}
			document_width = $( document ).width();
			document_height = $( document ).height();
           
		 });
        
		 
		 
		 
<?php endif; ?>	
<?php 
     
    /*if($settings->layout == 'masonry' || $settings->layout == 'grid') :
 		?>
				 $(window).scroll(function () {
				 	if ($(document).scrollTop() > 50) {
				       
				        $('.fl-node-<?php echo $id; ?> #infscr-loading').css({'visibility': 'hidden'});
				         
						$('.fl-node-<?php echo $id; ?> #post-loading').show().delay(5000).fadeOut();
				    }else{
				    	$('.fl-node-<?php echo $id; ?> #post-loading').hide();
					} 
				}); 
<?php 
	endif; */
?>
})(jQuery);
<?php 
     
    if($settings->layout == 'flipbox' ) :
 ?>
		(function($) {
			var document_width, document_height;
			var args = {
		    	id: '<?php echo $id; ?>'
			};
			jQuery(document).ready( function() {
				document_width = $( document ).width();
				document_height = $( document ).height();
			
				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
					$('.fl-node-<?php echo $id; ?> .njba-flip-box-outter').click(function(){
						if( $(this).hasClass( 'njba-hover' ) ){
					        $(this).removeClass('njba-hover');
					    } else {
					        $(this).addClass('njba-hover');
					    }
					});
				}
				
			});
			jQuery(window).load( function() {
				new NJBAContentFlipBox( args );
			});
			
			jQuery(window).resize( function() {
				if( document_width != $( document ).width() || document_height != $( document ).height() ) {
					document_width = $( document ).width();
					document_height = $( document ).height();
					new NJBAContentFlipBox( args );
				}
			});
			new NJBAContentFlipBox( args );
		})(jQuery);
<?php 
	endif; 
?>
;(function($) {
	new NJBAContentGrid({
		id: '<?php echo $id ?>',
		layout: '<?php echo $settings->layout; ?>',
		pagination: '<?php echo $settings->pagination; ?>',
		postSpacing: '<?php echo $settings->post_spacing; ?>',
		matchHeight: 'no',
		<?php if ($settings->layout == 'masonry') { ?>
			masonry: 'yes',
		<?php }else{ ?>
			masonry: 'no',
		<?php
			} 
		?>
		
	});
})(jQuery);
