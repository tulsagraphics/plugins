<?php 
/**
 * @class NjbaAnimatedHeadlinesModule
 */
class NjbaAnimatedHeadlinesModule extends FLBuilderModule {
	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __( 'Animated Headlines', 'bb-njba' ),
			'description'   	=> __( 'Awesome Animated Headlines module.', 'bb-njba' ),
			'group'         	=> njba_get_modules_group(),
			'category'			=> njba_get_modules_cat( 'content' ),
			'dir'           	=> NJBA_MODULE_DIR . 'modules/njba-animated-headlines/',
			'url'           	=> NJBA_MODULE_URL . 'modules/njba-animated-headlines/',
			'editor_export' 	=> true, // Defaults to true and can be omitted.
			'partial_refresh' => true, // Set this to true to enable partial refresh.
			'enabled'       	=> true, // Defaults to true and can be omitted.
		));
	}
}
/**
 * 
 */

FLBuilder::register_module( 'NjbaAnimatedHeadlinesModule', array(
	'general'       => array(
		'title'         => __('General', 'bb-njba'),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(
					'headline_style'	=> array(
						'type'			=> 'select',
						'label'			=> __('Style', 'bb-njba'),
						'default'		=> 'highlight',
						'options'		=> array(
							'highlight'		=> __('Highlighted', 'bb-njba'),
							'rotate'		=> __('Rotating', 'bb-njba')
						),
						'toggle'		=> array(
							'highlight'		=> array(
								'tabs'			=> array('style'),
								'fields'		=> array('headline_shape', 'highlighted_text', 'loop')
							),
							'rotate'		=> array(
								'fields'		=> array('rotating_text_style', 'animation_type')
							)
						)
					),
					'headline_shape'	=> array(
						'type'				=> 'select',
						'label'				=> __('Line Shape', 'bb-njba'),
						'default'			=> 'circle',
						'options'			=> array(
							'circle'			=> __('Circle', 'bb-njba'),
							'curly'				=> __('Curly', 'bb-njba'),
							'strikethrough'		=> __('Strikethrough', 'bb-njba'),
							'underline'			=> __('Underline', 'bb-njba'),
							'underline_zigzag'	=> __('Underline Zigzag', 'bb-njba'),
						)
					),
					'animation_type'  => array(
						'type'			=> 'select',
						'label'         => __( 'Animation Type', 'bb-njba' ),
						'default'       => 'typing',
						'options'       => array(
							'typing' 			=> __('Typing', 'bb-njba'),
							'clip' 				=> __('Clip', 'bb-njba'),
							'flip' 				=> __('Flip', 'bb-njba'),
							'swirl' 			=> __('Swirl', 'bb-njba'),
							'blinds' 			=> __('Blinds', 'bb-njba'),
							'drop-in' 			=> __('Drop-in', 'bb-njba'),
							'wave' 				=> __('Wave', 'bb-njba'),
							'slide' 			=> __('Slide', 'bb-njba'),
							'slide-down' 		=> __('Slide Down', 'bb-njba'),
						),
						'select'			=> array(
							'typing'			=> array(
								'fields'			=> array('animated_selection_bg_color', 'animated_selection_color')
							)
						)
					),
					'before_text'  	=> array(
						'type'          => 'text',
						'label'         => __('Before Text', 'bb-njba'),
						'default'       => __('Before text', 'bb-njba'),
						'help'			=> __('Text placed before animated text.', 'bb-njba'),
						'connections'   => array( 'string', 'html', 'url' ),
					),
					'highlighted_text'	=> array(
						'type'				=> 'text',
						'label'				=> __('Highlight Text', 'bb-njba'),
						'default'			=> __('Awesome', 'bb-njba'),
						'connections'   	=> array( 'string', 'html', 'url' ),
					),
					'rotating_text_style'	=> array(
						'type'          => 'textarea',
						'label'         => __(' Enter Rotating Text Here', 'bb-njba'),
						'default'       => __("Awesome\nCreative\nRotating", 'bb-njba'),
						'rows'          => '5',
						'help'			=> __('Text with animated effects. You can add multiple text by adding each on a new line.', 'bb-njba'),
						'connections'   => array( 'string', 'html', 'url' ),
					),
					'after_text'	=> array(
						'type'           => 'text',
						'label'          => __('After Text', 'bb-njba'),
						'default'        => __('Headline!', 'bb-njba'),
						'help'			 => __('Text placed at the end of animated text.', 'bb-njba'),
						'connections'   => array( 'string', 'html', 'url' ),
					),
					'alignment'     => array(
						'type'          => 'select',
						'label'         => __('Alignment', 'bb-njba'),
						'default'       => 'left',
						'options'       => array(
							'left'      =>  __('Left', 'bb-njba'),
							'center'    =>  __('Center', 'bb-njba'),
							'right'     =>  __('Right', 'bb-njba')
						),
						'preview'         => array(
							'type'            => 'css',
							'selector'        => '.njba-headline',
							'property'        => 'text-align'
						),
					),
					'loop'		=> array(
						'type'		=> 'select',
						'label'		=> __('Loop', 'bb-njba'),
						'default'	=> 'yes',
						'options'	=> array(
							'yes'		=> __('Yes', 'bb-njba'),
							'no'		=> __('No', 'bb-njba')
						)
					)
				)
			),
		)
	),
	'style'		=> array(
		'title'		=> __('Style', 'bb-njba'),
		'sections'	=> array(
			'shape_style'	=> array(
				'title'			=> __('Shape', 'bb-njba'),
				'fields'		=> array(
					'shape_color'	=> array(
						'type'			=> 'color',
						'label'			=> __('Color', 'bb-njba'),
						'default'		=> '',
						'show_reset'	=> true,
						'preview'       => array(
							'type'            => 'css',
							'selector'        => '.njba-headline-dynamic-wrapper path',
							'property'        => 'stroke'
						),
					),
					'shape_width'	=> array(
						'type'			=> 'text',
						'label'			=> __('Width', 'bb-njba'),
						'default'		=> '',
						'size'			=> 5,
						'description'	=> 'px',
						'preview'       => array(
							'type'            => 'css',
							'selector'        => '.njba-headline-dynamic-wrapper path',
							'property'        => 'stroke-width'
						),
					)
				)
			)
		)
	),
	'typography'	=> array(
		'title'			=> __('Typography', 'bb-njba'),
		'sections'		=> array(
			'text_tag'	=> array(
				'title'		=> '',
				'fields'	=> array(
					'headline_tag'   => array(
		                'type'          => 'select',
		                'label'         => __('Title Tag', 'bb-njba'),
		                'default'       => 'h3',
		                'options'       => array(
		                	'h1'	  => __('H1', 'bb-njba'),
		                    'h2'      => __('H2', 'bb-njba'),
		                    'h3'      => __('H3', 'bb-njba'),
		                    'h4'      => __('H4', 'bb-njba'),
		                    'h5'      => __('H5', 'bb-njba'),
		                    'h6'      => __('H6', 'bb-njba'),
		                )
		            ),
				)
			),
			'headline_typography' => array(
				'title' 			=> __('Headline Text', 'bb-njba' ),
                'fields'   			=> array(
                    'font_family'       => array(
                        'type'          => 'font',
                        'label'         => __('Font Family', 'bb-njba'),
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 'Default'
                        ),
                        'preview'	=> array(
                            'type'		=> 'font',
                            'selector'	=> '.njba-headline'
                    	),
                    ),
					'font_size'     => array(
                        'type'          => 'select',
						'label'         => __('Font Size', 'bb-njba'),
						'default'       => 'default',
						'options'       => array(
							'default'       =>  __('Default', 'bb-njba'),
							'custom'        =>  __('Custom', 'bb-njba')
						),
						'toggle'        => array(
							'custom'        => array(
								'fields'        => array('font_size_custom')
							)
						)
                    ),
                    'font_size_custom'  => array(
                        'type'              => 'unit',
						'label'             => __('Custom Font Size', 'bb-njba'),
						'description' 		=> 'px',
						'responsive' 		=> array(
							'placeholder' 		=> array(
								'default' 			=> '',
								'medium' 			=> '',
								'responsive' 		=> '',
							),
						),
						'preview'			=> array(
							'type' 				=> 'css',
							'selector'			=> '.njba-headline',
							'property'      	=> 'font-size',
							'unit'				=> 'px',
						),
                    ),
					'line_height'   => array(
                        'type'          => 'select',
						'label'         => __('Line Height', 'bb-njba'),
						'default'       => 'default',
						'options'       => array(
							'default'       =>  __('Default', 'bb-njba'),
							'custom'        =>  __('Custom', 'bb-njba')
						),
						'toggle'        => array(
							'custom'        => array(
								'fields'        => array('line_height_custom')
							)
						)
                    ),
                    'line_height_custom' => array(
						'type'              => 'unit',
						'label'             => __('Line Height Custom', 'bb-njba'),
						'responsive' 		=> array(
							'placeholder' 		=> array(
								'default' 			=> '',
								'medium' 			=> '',
								'responsive' 		=> '',
							),
						),
						'preview'			=> array(
							'type' 				=> 'css',
							'selector'			=> '.njba-headline',
							'property'      	=> 'line-height',
						),
                    ),
                    'color'        	=> array(
                        'type'       	=> 'color',
                        'label'      	=> __('Color', 'bb-njba'),
                        'default'    	=> '',
                        'show_reset' 	=> true,
                    	'preview'		=> array(
                            'type'			=> 'css',
                            'selector'		=> '.njba-headline',
                            'property'		=> 'color'
                    	),
                    ),
                )
            ),
			'animated_text_typography' => array(
				'title' => __('Animated Text', 'bb-njba' ),
                'fields'    => array(
                    'animated_font_family'       => array(
                        'type'          => 'font',
                        'label'         => __('Font Family', 'bb-njba'),
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 'Default'
                        ),
                        'preview'	=> array(
                            'type'		=> 'font',
                            'selector'	=> '.njba-headline-dynamic-wrapper'
                    	),
                    ),
					'animated_font_size'     => array(
                        'type'          => 'select',
						'label'         => __('Font Size', 'bb-njba'),
						'default'       => 'default',
						'options'       => array(
							'default'       =>  __('Default', 'bb-njba'),
							'custom'        =>  __('Custom', 'bb-njba')
						),
						'toggle'        => array(
							'custom'        => array(
								'fields'        => array('animated_font_size_custom')
							)
						)
                    ),
					'animated_font_size_custom'  => array(
                        'type'              => 'unit',
						'label'             => __('Custom Font Size', 'bb-njba'),
						'description' 		=> 'px',
						'responsive' 		=> array(
							'placeholder' 		=> array(
								'default' 			=> '',
								'medium' 			=> '',
								'responsive' 		=> '',
							),
						),
						'preview'			=> array(
							'type' 				=> 'css',
							'selector'			=> '.njba-headline-dynamic-wrapper',
							'property'      	=> 'font-size',
							'unit'				=> 'px',
						),
                    ),
					'animated_line_height'   => array(
                        'type'          => 'select',
						'label'         => __('Line Height', 'bb-njba'),
						'default'       => 'default',
						'options'       => array(
							'default'       =>  __('Default', 'bb-njba'),
							'custom'        =>  __('Custom', 'bb-njba')
						),
						'toggle'        => array(
							'custom'        => array(
								'fields'        => array('animated_line_height_custom')
							)
						)
                    ),
					'animated_line_height_custom'  => array(
                        'type'              => 'unit',
						'label'             => __('Line Height Custom', 'bb-njba'),
						'description' 		=> 'px',
						'responsive' 		=> array(
							'placeholder' 		=> array(
								'default' 			=> '',
								'medium' 			=> '',
								'responsive' 		=> '',
							),
						),
						'preview'			=> array(
							'type' 				=> 'css',
							'selector'			=> '.njba-headline-dynamic-wrapper',
							'property'      	=> 'font-size',
							'unit'				=> 'px',
						),
                    ),
                    'animated_color'        => array(
                        'type'       => 'color',
                        'label'      => __('Color', 'bb-njba'),
                        'default'    => '',
                        'show_reset' => true,
                    	'preview'	=> array(
                            'type'		=> 'css',
                            'selector'	=> '.njba-headline-dynamic-wrapper',
                            'property'	=> 'color'
                    	),
					),
					'animated_selection_bg_color' => array(
                        'type'       	=> 'color',
                        'label'      	=> __('Background Color', 'bb-njba'),
                        'default'    	=> '',
                        'show_reset' 	=> true,
                    	'preview'		=> array(
                            'type'			=> 'css',
                            'selector'		=> '.njba-headline-animation-type-typing .njba-headline-dynamic-wrapper.njba-headline-typing-selected',
                            'property'		=> 'background-color'
						),
						'help'			=> __('Leave this field empty for default color.', 'bb-njba')
					),
					'animated_selection_color' => array(
                        'type'       	=> 'color',
                        'label'      	=> __('Text Color', 'bb-njba'),
                        'default'    	=> '',
                        'show_reset' 	=> true,
                    	'preview'		=> array(
                            'type'			=> 'css',
                            'selector'		=> '.njba-headline-animation-type-typing .njba-headline-dynamic-wrapper.njba-headline-typing-selected .njba-headline-dynamic-text',
                            'property'		=> 'color'
						),
						'help'			=> __('Leave this field empty for default color.', 'bb-njba')
                    ),
                )
            ),
		)
	)
));
 ?>