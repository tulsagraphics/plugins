(function($) {
	NjbaAnimatedHeadlinesModule = function( settings ) {
		this.settings           = settings;
		this.nodeClass          = '.fl-node-' + settings.id;
		this.headline			= this.nodeClass + ' .njba-headline';
		this.dynamicwrapper		= this.nodeClass + ' .njba-headline-dynamic-wrapper';
		this.animationDelay     = 2500;
		// letters effect
		this.lettersDelay		= 50;
		// typing effect
		this.typeLettersDelay	= 150;
		this.selectionDuration	= 500;
		// clip effect
		this.revealDuration		= 600,
		this.revealAnimationDelay = 1500;
		this.typeAnimationDelay = this.selectionDuration + 800;
		this.classes			= {
			dynamicText: 			'njba-headline-dynamic-text',
			textActive: 			'njba-headline-text-active',
			textInactive: 			'njba-headline-text-inactive',
			letters: 				'njba-headline-letters',
			animationIn: 			'njba-headline-animation-in',
			typeSelected: 			'njba-headline-typing-selected'
		};
		this.elements			= {};
		this.fillWords();
		this.initHeadlines();
	};
  	NjbaAnimatedHeadlinesModule.prototype = {
	    settings        		: {},
	    nodeClass       		: '',
	    headline				: '',
		dynamicwrapper			: '',
		animationDelay			: 2500,
		lettersDelay			: 50,
		typeLettersDelay		: 150,
		selectionDuration		: 500,
		revealDuration			: 600,
		revealAnimationDelay	: 1500,
		svgPaths: {
			circle: [ 'M325,18C228.7-8.3,118.5,8.3,78,21C22.4,38.4,4.6,54.6,5.6,77.6c1.4,32.4,52.2,54,142.6,63.7 c66.2,7.1,212.2,7.5,273.5-8.3c64.4-16.6,104.3-57.6,33.8-98.2C386.7-4.9,179.4-1.4,126.3,20.7' ],
			curly: [ 'M3,146.1c17.1-8.8,33.5-17.8,51.4-17.8c15.6,0,17.1,18.1,30.2,18.1c22.9,0,36-18.6,53.9-18.6 c17.1,0,21.3,18.5,37.5,18.5c21.3,0,31.8-18.6,49-18.6c22.1,0,18.8,18.8,36.8,18.8c18.8,0,37.5-18.6,49-18.6c20.4,0,17.1,19,36.8,19 c22.9,0,36.8-20.6,54.7-18.6c17.7,1.4,7.1,19.5,33.5,18.8c17.1,0,47.2-6.5,61.1-15.6' ],
			strikethrough: ['M3,75h493.5'],
			underline: [ 'M7.7,145.6C109,125,299.9,116.2,401,121.3c42.1,2.2,87.6,11.8,87.3,25.7' ],
			underline_zigzag: [ 'M9.3,127.3c49.3-3,150.7-7.6,199.7-7.4c121.9,0.4,189.9,0.4,282.3,7.2C380.1,129.6,181.2,130.6,70,139 c82.6-2.9,254.2-1,335.9,1.3c-56,1.4-137.2-0.3-197.1,9' ]
		},
		getNextWord: function( $word )
		{
			return $word.is( ':last-child' ) ? $word.parent().children().eq( 0 ) : $word.next();
		},
		switchWord: function( $oldWord, $newWord )
		{
			$oldWord
				.removeClass( 'njba-headline-text-active' )
				.addClass( 'njba-headline-text-inactive' );
			$newWord
				.removeClass( 'njba-headline-text-inactive' )
				.addClass( 'njba-headline-text-active' );
		},
		singleLetters: function() {
			var classes = this.classes;
			this.elements.dynamicText.each( function() {
				var $word = $( this ),
					letters = $word.text().split( '' ),
					isActive = $word.hasClass( classes.textActive );
				$word.empty();
				letters.forEach( function( letter ) {
					var $i = $( '<i>' ).text( letter );
					if ( isActive ) {
						$i.addClass( classes.animationIn );
					}
					$word.append( $i );
				} );
				$word.css( 'opacity', 1 );
			} );
		},
		showLetter: function( $letter, $word, bool, duration ) {
			var self 			= this,
				classes 		= self.classes,
				animationType 	= self.settings.animation_type;
			$letter.addClass( classes.animationIn );
			if ( ! $letter.is( ':last-child' ) ) {
				setTimeout( function() {
					self.showLetter( $letter.next(), $word, bool, duration );
				}, duration );
			} else {
				if ( ! bool ) {
					setTimeout( function() {
						self.hideWord( $word );
					}, self.animationDelay );
				}
			}
		},
		hideLetter: function( $letter, $word, bool, duration ) {
			var self = this;
			$letter.removeClass( self.classes.animationIn );
			if ( ! $letter.is( ':last-child' ) ) {
				setTimeout( function() {
					self.hideLetter( $letter.next(), $word, bool, duration );
				}, duration );
			} else if ( bool ) {
				setTimeout( function() {
					self.hideWord( self.getNextWord( $word ) );
				}, self.animationDelay );
			}
		},
		showWord: function( $word, duration ) {
			var self 			= this,
				animationType 	= self.settings.animation_type;
			if ( 'typing' === animationType ) {
				self.showLetter( $word.find( 'i' ).eq( 0 ), $word, false, duration );
				$word
					.addClass( self.classes.textActive )
					.removeClass( self.classes.textInactive );
			} else if ( 'clip' === animationType ) {
				$(self.dynamicwrapper).animate( { 'width': $word.width() + 10 }, self.revealDuration, function() {
					setTimeout( function() {
						self.hideWord( $word );
					}, self.revealAnimationDelay );
				} );
			}
		},
		hideWord: function( $word ) {
			var self 			= this,
				classes 		= self.classes,
				animationType 	= self.settings.animation_type,
				nextWord 		= self.getNextWord( $word );
			if ( 'typing' === animationType ) {
				$(self.dynamicwrapper).addClass( classes.typeSelected );
				setTimeout( function() {
					$(self.dynamicwrapper).removeClass( classes.typeSelected );
					$word
						.addClass( classes.textInactive )
						.removeClass( classes.textActive )
						.children( 'i' )
						.removeClass( classes.animationIn );
				}, self.selectionDuration );
				setTimeout( function() {
					self.showWord( nextWord, self.typeLettersDelay );
				}, self.typeAnimationDelay );
			} else if ( $(self.headline).hasClass( classes.letters ) ) {
				var bool = $word.children( 'i' ).length >= nextWord.children( 'i' ).length;
				self.hideLetter( $word.find( 'i' ).eq( 0 ), $word, bool, self.lettersDelay );
				self.showLetter( nextWord.find( 'i' ).eq( 0 ), nextWord, bool, self.lettersDelay );
			} else if ( 'clip' === animationType ) {
				$(self.dynamicwrapper).animate( { width: '2px' }, self.revealDuration, function() {
					self.switchWord( $word, nextWord );
					self.showWord( nextWord );
				} );
			} else {
				self.switchWord( $word, nextWord );
				setTimeout( function() {
					self.hideWord( nextWord );
				}, self.animationDelay );
			}
		},
		animateHeadline: function() {
			var self 			= this,
				animationType 	= self.settings.animation_type,
				dynamicwrapper 	= $(self.dynamicwrapper);
			if ( 'clip' === animationType ) {
				dynamicwrapper.width( dynamicwrapper.width() + 10 );
			} else if ( 'typing' !== animationType ) {
				//assign to .njba-headline-dynamic-wrapper the width of its longest word
				var width = 0;
				self.elements.dynamicText.each( function() {
					var wordWidth = $( this ).width();
					if ( wordWidth > width ) {
						width = wordWidth;
					}
				} );
				dynamicwrapper.css( 'width', width );
			}
			//trigger animation
			setTimeout( function() {
				self.hideWord( self.elements.dynamicText.eq( 0 ) );
			}, self.animationDelay );
		},
		getSvgPaths: function( pathName ) {
			var pathsInfo = this.svgPaths[ pathName ],
				$paths = jQuery();
			pathsInfo.forEach( function( pathInfo ) {
				$paths = $paths.add( $( '<path>', { d: pathInfo } ) );
			} );
			return $paths;
		},
		fillWords: function()
		{
			var classes 		= this.classes,
				dynamicwrapper 	= $(this.dynamicwrapper),
				settings		= this.settings;
			if ( 'rotate' == this.settings.headline_style ) {
				var rotatingText = this.settings.rotating_text_style.split('|');
				rotatingText.forEach( function( word, index ) {
					var dynamicText = $('<span>', { 'class': classes.dynamicText }).html( word.replace( ' ', '&nbsp;' ) );
						
					if ( ! index ) {
						dynamicText.addClass( classes.textActive );
					}
					dynamicwrapper.append( dynamicText );
				} );
			} else {
				var dynamicText = $('<span>', { 'class': classes.dynamicText + ' ' + classes.textActive }).text( settings.highlighted_text );
				var svg = $('<svg>', {
					xmlns: 'http://www.w3.org/2000/svg',
					viewBox: '0 0 500 150',
					preserveAspectRatio: 'none'
				}).html(this.getSvgPaths( settings.headline_shape ));
				dynamicwrapper.append( dynamicText, svg[0].outerHTML );
			}
			this.elements.dynamicText = dynamicwrapper.children( '.' + classes.dynamicText );
		},
		rotateHeadline: function() {
			//insert <i> element for each letter of a changing word
			if ( $(this.headline).hasClass( this.classes.letters ) ) {
				this.singleLetters();
			}
			//initialise headline animation
			this.animateHeadline();
		},
		initHeadlines: function()
		{
			if ( 'rotate' === this.settings.headline_style ) {
				this.rotateHeadline();
			}
		},
		
  	};
})(jQuery);