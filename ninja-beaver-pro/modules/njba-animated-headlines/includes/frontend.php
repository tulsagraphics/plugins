<?php 
$class = 'njba-headline';
$loop = isset( $settings->loop ) && $settings->loop == 'yes' ? ' njba-headline-loop' : '';
if ( 'rotate' == $settings->headline_style ) {
	$class .= ' njba-headline-animation-type-' . $settings->animation_type;
	if ( in_array( $settings->animation_type, array( 'typing', 'swirl', 'blinds', 'wave' ) ) ) {
		$class .= ' njba-headline-letters';
	}
}
?>
<div class="njba-animated-headlines njba-headline-style-<?php echo $settings->headline_style; ?><?php echo $loop; ?>">
	<<?php echo $settings->headline_tag; ?> class="<?php echo $class; ?>">
		<?php if ( ! empty( $settings->before_text ) ) : ?>
			<span class="njba-headline-plain-text njba-headline-text-wrapper"><?php echo $settings->before_text; ?></span>
		<?php endif; ?>
		<?php if ( ! empty( $settings->rotating_text_style ) ) : ?>
			<span class="njba-headline-dynamic-wrapper njba-headline-text-wrapper" id="msg"></span>
		<?php endif; ?>
		<?php if ( ! empty( $settings->after_text ) ) : ?>
			<span class="njba-headline-plain-text njba-headline-text-wrapper"><?php echo $settings->after_text; ?></span>
		<?php endif; ?>
	</<?php echo $settings->headline_tag; ?>>
</div>