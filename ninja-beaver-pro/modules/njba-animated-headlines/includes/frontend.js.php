<?php
$rotating_text_style = str_replace( array("\r\n", "\n", "\r", "<br/>", "<br>"), '|', $settings->rotating_text_style );
?>
;(function($) {
    new NjbaAnimatedHeadlinesModule({
        id: '<?php echo $id; ?>',
        headline_style: '<?php echo $settings->headline_style; ?>',
        rotating_text_style: '<?php echo htmlspecialchars_decode($rotating_text_style); ?>',
        highlighted_text: '<?php echo $settings->highlighted_text; ?>',
        headline_shape: '<?php echo $settings->headline_shape; ?>',
        animation_type: '<?php echo $settings->animation_type; ?>',
    }); 
})(jQuery);