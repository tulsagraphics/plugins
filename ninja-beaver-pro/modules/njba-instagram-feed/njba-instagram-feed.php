<?php

/**
 * @class NJBAInstagramFeedModule
 */
class NJBAInstagramFeedModule extends FLBuilderModule {
	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __( 'Instagram Feed', 'bb-njba' ),
			'description'   	=> __( 'A module to fetch instagram photos.', 'bb-njba' ),
			'group'         	=> njba_get_modules_group(),
			'category'			=> njba_get_modules_cat( 'social' ),
			'dir'           	=> NJBA_MODULE_DIR . 'modules/njba-instagram-feed/',
			'url'           	=> NJBA_MODULE_URL . 'modules/njba-instagram-feed/',
			'editor_export' 	=> true, // Defaults to true and can be omitted.
			'enabled'       	=> true, // Defaults to true and can be omitted.
		));
		
		$this->add_js( 'jquery-imagesloaded' );

		$this->add_js( 'jquery-magnificpopup' );
		$this->add_css( 'jquery-magnificpopup' );

		$this->add_js( 'instafeed' );

		$this->add_css( 'jquery-swiper' );
		$this->add_js( 'jquery-swiper' );

		$this->add_js('jquery-masonry');
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('NJBAInstagramFeedModule', array(
	'general'       => array( // Tab
		'title'         => __( 'General', 'bb-njba' ), // Tab title
		'sections'      => array( // Tab Sections
			'account_settings'	=> array( // Section
				'title'				=> __( 'Authentication Settings', 'bb-njba' ), // Section Title
				'fields'        	=> array( // Section Fields
					'user_id'     	=> array(
						'type'          => 'text',
						'label'         => __( 'User ID', 'bb-njba' ),
						'default'       => '',
						'connections'	=> array('string')
					),
					'access_token'	=> array(
						'type'          => 'text',
						'label'         => __( 'Access Token', 'bb-njba' ),
						'default'       => '',
						'connections'	=> array('string')
					),
					'client_id'	=> array(
						'type'		=> 'text',
						'label'     => __( 'Client ID', 'bb-njba' ),
						'default'   => '',
						'connections'	=> array('string')
					),
				),
			),
			'feed_settings'	=> array(
				'title'			=> __( 'Feed Settings', 'bb-njba' ),
				'fields'        => array(
					'feed_by_tags'  => array(
						'type'          => 'select',
						'label'         => __( 'Feed by Hashtag', 'bb-njba' ),
						'default'       => 'no',
						'options'       => array(
							'yes'			=> __( 'Yes', 'bb-njba' ),
							'no'        	=> __( 'No', 'bb-njba' ),
						),
						'toggle'	=> array(
							'yes'	=> array(
								'fields'	=> array( 'tag_name' ),
							),
						),
					),
					'tag_name'     	=> array(
						'type'          => 'text',
						'label'         => __( 'Tag Name', 'bb-njba' ),
						'default'       => '',
						'connections'	=> array('string')
					),
					'images_count'		=> array(
						'type'          => 'text',
						'label'         => __( 'Images Count', 'bb-njba' ),
						'default'       => '12',
						'size'          => '5',
					),
					'image_resolution'  => array(
						'type'          => 'select',
						'label'         => __( 'Image Resolution', 'bb-njba' ),
						'default'       => 'standard_resolution',
						'options'       => array(
							'thumbnail'             => __( 'Thumbnail', 'bb-njba' ),
							'low_resolution'        => __( 'Low Resolution', 'bb-njba' ),
							'standard_resolution'   => __( 'Standard Resolution', 'bb-njba' ),
						),
					),
					'sort_by'	=> array(
						'type'			=> 'select',
						'label'         => __( 'Sort By', 'bb-njba' ),
						'default'       => 'none',
						'options'       => array(
							'none'              => __( 'None', 'bb-njba' ),
							'most-recent'       => __( 'Most Recent', 'bb-njba' ),
							'least-recent'      => __( 'Least Recent', 'bb-njba' ),
							'most-liked'        => __( 'Most Liked', 'bb-njba' ),
							'least-liked'       => __( 'Least Liked', 'bb-njba' ),
							'most-commented'    => __( 'Most Commented', 'bb-njba' ),
							'least-commented'   => __( 'Least Commented', 'bb-njba' ),
							'random'            => __( 'Random', 'bb-njba' ),
						),
					),
				),
			),
			'general'	=> array(
				'title'		=> __( 'General', 'bb-njba' ),
				'fields'    => array(
					'feed_layout'  => array(
						'type'          => 'select',
						'label'         => __( 'Layout', 'bb-njba' ),
						'default'       => 'grid',
						'options'       => array(
							'grid'           => __( 'Masonry Grid', 'bb-njba' ),
							'square-grid'    => __( 'Square Grid', 'bb-njba' ),
							'carousel'       => __( 'Carousel', 'bb-njba' ),
						),
						'toggle'	=> array(
							'grid'  => array(
								'fields'    => array( 'grid_columns', 'spacing' ),
							),
							'square-grid'  => array(
								'fields'    => array( 'grid_columns', 'spacing', 'image_custom_size' ),
							),
							'carousel'  => array(
								'tabs'		=> array( 'carousel' ),
								'fields'	=> array( 'image_custom_size' ),
							),
						),
					),
					'image_custom_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '300',
                        'label'         => __('Custom Size', 'bb-njba'),
                        'description'	=> 'px',
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        )
                    ),
					'grid_columns'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Grid Columns', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '3',
                            'medium' => '2',
                            'small' => '1'
                        )
                    ),
					'spacing'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Spacing', 'bb-njba'),
                        'description'	=> 'px',
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        )
                    ),
					'likes'	=> array(
						'type'		=> 'select',
						'label'     => __( 'Likes', 'bb-njba' ),
						'default'   => 'no',
						'options'   => array(
							'yes'		=> __( 'Yes', 'bb-njba' ),
							'no'		=> __( 'No', 'bb-njba' ),
						),
					),
					'comments'	=> array(
						'type'		=> 'select',
						'label'     => __( 'Comments', 'bb-njba' ),
						'default'  	=> 'no',
						'options'   => array(
							'yes'		=> __( 'Yes', 'bb-njba' ),
							'no'		=> __( 'No', 'bb-njba' ),
						),
					),
					'content_visibility'  => array(
						'type'          => 'select',
						'label'         => __( 'Content Visibility', 'bb-njba' ),
						'default'       => 'always',
						'options'       => array(
							'always'		=> __( 'Always', 'bb-njba' ),
							'hover'         => __( 'Hover', 'bb-njba' ),
						),
					),
					'image_popup'  => array(
						'type'          => 'select',
						'label'         => __( 'Open Image in Popup', 'bb-njba' ),
						'default'       => 'no',
						'options'       => array(
							'yes'           => __( 'Yes', 'bb-njba' ),
							'no'            => __( 'No', 'bb-njba' ),
						),
					),
					'profile_link'  => array(
						'type'          => 'select',
						'label'         => __( 'Show Link to Instagram Profile?', 'bb-njba' ),
						'default'       => 'no',
						'options'       => array(
							'yes'           => __( 'Yes', 'bb-njba' ),
							'no'            => __( 'No', 'bb-njba' ),
						),
						'toggle'	=> array(
							'yes'		=> array(
								'tabs'		=> array( 'typography' ),
								'sections'	=> array( 'feed_title' ),
								'fields'	=> array( 'insta_link_title', 'insta_profile_url', 'insta_title_icon', 'insta_title_icon_position' ),
							),
						),
					),
					'insta_link_title'	=> array(
						'type'				=> 'text',
						'label'         	=> __( 'Link Title', 'bb-njba' ),
						'default'       	=> __( 'Follow @example on instagram', 'bb-njba' ),
						'connections'		=> array('string')
					),
					'insta_profile_url'	=> array(
						'type'          	=> 'link',
						'label'         	=> __( 'Instagram Profile URL', 'bb-njba' ),
						'preview'       	=> array(
							'type'      	=> 'none',
						),
					),
					'insta_title_icon'  => array(
						'type'          	=> 'icon',
						'label'         	=> __( 'Title Icon', 'bb-njba' ),
						'preview'			=> 'none',
						'show_remove' 		=> true,
					),
					'insta_title_icon_position'  => array(
						'type'			=> 'select',
						'label'         => __( 'Icon Position', 'bb-njba' ),
						'default'       => 'before_title',
						'options'       => array(
							'before_title'		=> __( 'Before Title', 'bb-njba' ),
							'after_title'       => __( 'After Title', 'bb-njba' ),
						),
					),
				),
			),
		),
	),
	'carousel'  => array(
		'title'     => __( 'Carousel', 'bb-njba' ),
		'sections'  => array(
			'carousel_settings'     => array(
				'title'     => __( 'Image', 'bb-njba' ),
				'fields'    => array(
					'visible_items'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Visible Items', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '3',
                            'medium' => '2',
                            'small' => '1'
                        )
                    ),
					'images_gap'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Items Spacing', 'bb-njba'),
                        'description'	=> 'px',
						'default'       => array(
                            'desktop' => '10',
                            'medium' => '10',
                            'small' => '10'
                        )
                    ),
					'autoplay'	=> array(
						'type'		=> 'select',
						'label'		=> __( 'Auto Play', 'bb-njba' ),
						'default'   => 'yes',
						'options'   => array(
							'yes'		=> __( 'Yes', 'bb-njba' ),
							'no'        => __( 'No', 'bb-njba' ),
						),
						'toggle'	=> array(
							'yes'	=> array(
								'fields'	=> array( 'autoplay_speed' ),
							),
						),
					),
					'autoplay_speed'	=> array(
						'type'          => 'text',
						'label'         => __( 'Auto Play Speed', 'bb-njba' ),
						'default'       => '5000',
						'size'          => '5',
						'description'   => _x( 'ms', 'Value unit for form field of time in mili seconds. Such as: "5000 ms"', 'bb-njba' ),
					),
					'infinite_loop'		=> array(
						'type'          => 'select',
						'label'         => __( 'Infinite Loop', 'bb-njba' ),
						'default'       => 'no',
						'options'       => array(
							'yes'			=> __( 'Yes', 'bb-njba' ),
							'no'            => __( 'No', 'bb-njba' ),
						),
					),
					'grab_cursor'  => array(
						'type'          => 'select',
						'label'         => __( 'Grab Cursor', 'bb-njba' ),
						'default'       => 'no',
						'options'        => array(
							'yes'           => __( 'Yes', 'bb-njba' ),
							'no'            => __( 'No', 'bb-njba' ),
						),
					),
				),
			),
			'controls'		=> array(
				'title'         => __( 'Controls', 'bb-njba' ),
				'fields'        => array(
					'navigation'     => array(
						'type'          => 'select',
						'label'         => __( 'Arrows', 'bb-njba' ),
						'default'       => 'yes',
						'options'       => array(
							'yes'        	=> __( 'Yes', 'bb-njba' ),
							'no'            => __( 'No', 'bb-njba' ),
						),
						'toggle'		=> array(
							'yes'			=> array(
								'sections'		=> array( 'arrow_style' ),
							),
						),
					),
					'pagination'	=> array(
						'type'          => 'select',
						'label'         => __( 'Dots', 'bb-njba' ),
						'default'       => 'yes',
						'options'       => array(
							'yes'       	=> __( 'Yes', 'bb-njba' ),
							'no'			=> __( 'No', 'bb-njba' ),
						),
						'toggle'	=> array(
							'yes'	=> array(
								'sections'	=> array( 'dot_style' ),
							),
						),
					),
				),
			),
			'arrow_style'   => array( // Section
				'title' => __( 'Arrow Settings', 'bb-njba' ), // Section Title
				'fields' => array( // Section Fields
					'arrow_font_size'   => array(
						'type'          => 'text',
						'label'         => __( 'Arrow Size', 'bb-njba' ),
						'description'   => 'px',
						'size'      	=> 5,
						'maxlength' 	=> 3,
						'default'       => '24',
						'preview'       => array(
							'type'            => 'css',
							'selector'        => '.njba-instagram-feed .njba-swiper-button',
							'property'        => 'font-size',
							'unit'            => 'px',
						),
					),
					'arrow_bg_color'	=> array(
						'type'			=> 'color',
						'label'     	=> __( 'Background Color', 'bb-njba' ),
						'show_reset' 	=> true,
						'default'   	=> 'eaeaea',
						'preview'       => array(
							'type'            => 'css',
							'selector'        => '.njba-instagram-feed .njba-swiper-button',
							'property'        => 'background-color',
						),
					),
					'arrow_bg_hover'	=> array(
						'type'      	=> 'color',
						'label'     	=> __( 'Background Hover Color', 'bb-njba' ),
						'show_reset' 	=> true,
						'default'   	=> '4c4c4c',
						'preview'       => array(
							'type'            => 'css',
							'selector'        => '.njba-instagram-feed .njba-swiper-button:hover',
							'property'        => 'background-color',
						),
					),
					'arrow_color'	=> array(
						'type'			=> 'color',
						'label'			=> __( 'Arrow Color', 'bb-njba' ),
						'show_reset' 	=> true,
						'default'   	=> '000000',
						'preview'       => array(
							'type'			=> 'css',
							'selector'      => '.njba-instagram-feed .njba-swiper-button',
							'property'      => 'color',
						),
					),
					'arrow_color_hover'	=> array(
						'type'			=> 'color',
						'label'     	=> __( 'Arrow Hover Color', 'bb-njba' ),
						'show_reset' 	=> true,
						'default'   	=> 'eeeeee',
						'preview'       => array(
							'type'			=> 'css',
							'selector'      => '.njba-instagram-feed .njba-swiper-button:hover',
							'property'      => 'color',
						),
					),
					'arrow_border_style'	=> array(
						'type'      	=> 'select',
						'label'     	=> __( 'Border Style', 'bb-njba' ),
						'default'     	=> 'none',
						'options'       => array(
							'none'          => __( 'None', 'bb-njba' ),
							'solid'         => __( 'Solid', 'bb-njba' ),
							'dashed'        => __( 'Dashed', 'bb-njba' ),
							'dotted'        => __( 'Dotted', 'bb-njba' ),
						),
						'toggle'   => array(
							'solid'    => array(
								'fields'	=> array( 'arrow_border_width', 'arrow_border_color', 'arrow_border_hover' ),
							),
							'dashed'    => array(
								'fields'	=> array( 'arrow_border_width', 'arrow_border_color', 'arrow_border_hover' ),
							),
							'dotted'	=> array(
								'fields'	=> array( 'arrow_border_width', 'arrow_border_color', 'arrow_border_hover' ),
							),
							'double'    => array(
								'fields'   	=> array( 'arrow_border_width', 'arrow_border_color', 'arrow_border_hover' ),
							),
						),
						'preview'	=> array(
							'type'            => 'css',
							'selector'        => '.njba-instagram-feed .njba-swiper-button',
							'property'        => 'border-style',
							'unit'            => 'px',
						),
					),
					'arrow_border_width'	=> array(
						'type'          	=> 'text',
						'label'         	=> __( 'Border Width', 'bb-njba' ),
						'description'   	=> 'px',
						'size'      		=> 5,
						'maxlength' 		=> 3,
						'default'       	=> '1',
						'preview'         	=> array(
							'type'				=> 'css',
							'selector'        	=> '.njba-instagram-feed .njba-swiper-button',
							'property'        	=> 'border-width',
							'unit'            	=> 'px',
						),
					),
					'arrow_border_color'	=> array(
						'type'			=> 'color',
						'label'     	=> __( 'Border Color', 'bb-njba' ),
						'show_reset' 	=> true,
						'default'   	=> '',
						'preview'       => array(
							'type'			=> 'css',
							'selector'      => '.njba-instagram-feed .njba-swiper-button',
							'property'      => 'border-color',
						),
					),
					'arrow_border_hover'	=> array(
						'type'			=> 'color',
						'label'     	=> __( 'Border Hover Color', 'bb-njba' ),
						'show_reset' 	=> true,
						'default'   	=> '',
						'preview'      	=> array(
							'type'			=> 'css',
							'selector'      => '.njba-instagram-feed .njba-swiper-button:hover',
							'property'      => 'border-color',
						),
					),
					'arrow_border_radius'   => array(
						'type'          => 'text',
						'label'         => __( 'Round Corners', 'bb-njba' ),
						'description'   => 'px',
						'size'      	=> 5,
						'maxlength' 	=> 3,
						'default'       => '100',
						'preview'       => array(
							'type'			=> 'css',
							'selector'      => '.njba-instagram-feed .njba-swiper-button',
							'property'      => 'border-radius',
							'unit'          => 'px',
						),
					),
					'arrow_horizontal_padding' 	=> array(
						'type'          => 'text',
						'label'         => __( 'Horizontal Padding', 'bb-njba' ),
						'default'   	=> '13',
						'maxlength'     => 5,
						'size'          => 6,
						'description'   => 'px',
						'preview'		=> array(
							'type'			=> 'css',
							'rules'			=> array(
								array(
									'selector'	=> '.njba-image-carousel .njba-swiper-button',
									'property'	=> 'padding-left',
									'unit'		=> 'px',
								),
								array(
									'selector'	=> '.njba-instagram-feed .njba-swiper-button',
									'property'	=> 'padding-right',
									'unit'		=> 'px',
								),
							),
						),
					),
					'arrow_vertical_padding'	=> array(
						'type'          => 'text',
						'label'         => __( 'Vertical Padding', 'bb-njba' ),
						'default'   	=> '5',
						'maxlength'     => 5,
						'size'          => 6,
						'description'   => 'px',
						'preview'		=> array(
							'type'			=> 'css',
							'rules'			=> array(
								array(
									'selector'	=> '.njba-instagram-feed .njba-swiper-button',
									'property'	=> 'padding-top',
									'unit'		=> 'px',
								),
								array(
									'selector'	=> '.njba-instagram-feed .njba-swiper-button',
									'property'	=> 'padding-bottom',
									'unit'		=> 'px',
								),
							),
						),
					),
				),
			),
			'dot_style'	=> array( // Section
				'title' 	=> __( 'Dot Settings', 'bb-njba' ), // Section Title
				'fields' 	=> array( // Section Fields
					'dot_position'	=> array(
						'type'          => 'select',
						'label'         => __( 'Position', 'bb-njba' ),
						'default'       => 'outside',
						'options'       => array(
							'outside'        	=> __( 'Outside', 'bb-njba' ),
							'inside'            => __( 'Inside', 'bb-njba' ),
						),
					),
					'dot_bg_color'  => array(
						'type'          => 'color',
						'label'         => __( 'Background Color', 'bb-njba' ),
						'default'       => '666666',
						'show_reset'    => true,
						'preview'       => array(
							'type'			=> 'css',
							'selector'      => '.njba-instagram-feed .swiper-pagination-bullet',
							'property'      => 'background-color',
						),
					),
					'dot_bg_hover'      => array(
						'type'          => 'color',
						'label'         => __( 'Active Color', 'bb-njba' ),
						'default'       => '000000',
						'show_reset'    => true,
						'preview'       => array(
							'type'			=> 'css',
							'selector'      => '.njba-instagram-feed .swiper-pagination-bullet:hover, .njba-instagram-feed .swiper-pagination-bullet-active',
							'property'      => 'background-color',
						),
					),
					'dot_width'   => array(
						'type'          => 'text',
						'label'         => __( 'Size', 'bb-njba' ),
						'description'   => 'px',
						'size'      	=> 5,
						'maxlength' 	=> 3,
						'default'       => '10',
						'preview'       => array(
							'type'            => 'css',
							'rules'           => array(
								array(
									'selector'        => '.njba-instagram-feed .swiper-pagination-bullet',
									'property'        => 'width',
									'unit'            => 'px',
								),
								array(
									'selector'        => '.njba-instagram-feed .swiper-pagination-bullet',
									'property'        => 'height',
									'unit'            => 'px',
								),
							),
						),
					),
					'dot_border_radius'	=> array(
						'type'				=> 'text',
						'label'         	=> __( 'Round Corners', 'bb-njba' ),
						'description'   	=> 'px',
						'size'      		=> 5,
						'maxlength' 		=> 3,
						'default'       	=> '100',
						'preview'         	=> array(
							'type'				=> 'css',
							'selector'        	=> '.njba-instagram-feed .swiper-pagination-bullet',
							'property'        	=> 'border-radius',
							'unit'            	=> 'px',
						),
					),
				),
			),
		),
	),
	'style' => array(
		'title'     => __( 'Style', 'bb-njba' ),
		'sections'  => array(
			'image'		=> array(
				'title'		=> __( 'Image', 'bb-njba' ),
				'fields'    => array(
					'image_grayscale'	=> array(
						'type'          => 'select',
						'label'         => __( 'Grayscale Image', 'bb-njba' ),
						'default'       => 'no',
						'options'       => array(
							'yes'        	=> __( 'Yes', 'bb-njba' ),
							'no'            => __( 'No', 'bb-njba' ),
						),
					),
					'image_overlay_type'	=> array(
						'type'          	=> 'select',
						'label'         	=> __( 'Image Overlay Type', 'bb-njba' ),
						'default'       	=> 'none',
						'options'       	=> array(
							'none'        		=> __( 'None', 'bb-njba' ),
							'solid'        		=> __( 'Solid', 'bb-njba' ),
							'gradient'      	=> __( 'Gradient', 'bb-njba' ),
						),
						'toggle'	=> array(
							'solid'		=> array(
								'fields'	=> array( 'image_overlay_color', 'image_overlay_opacity' ),
							),
							'gradient'	=> array(
								'fields'	=> array( 'image_overlay_angle', 'image_overlay_color', 'image_overlay_secondary_color', 'image_overlay_gradient_type', 'image_overlay_opacity' ),
							),
						),
					),
					'image_overlay_color'	=> array(
						'type'          		=> 'color',
						'label'         		=> __( 'Overlay Color', 'bb-njba' ),
						'default'       		=> '',
						'show_reset'    		=> true,
					),
					'image_overlay_secondary_color'	=> array(
						'type'			=> 'color',
						'label'     	=> __( 'Overlay Secondary Color', 'bb-njba' ),
						'default'		=> '',
						'show_reset' 	=> true,
					),
					'image_overlay_gradient_type'	=> array(
						'type'			=> 'select',
						'label'         => __( 'Type', 'bb-njba' ),
						'default'       => 'linear',
						'options'       => array(
							'linear'		=> __( 'Linear', 'bb-njba' ),
							'radial'        => __( 'Radial', 'bb-njba' ),
						),
						'toggle'	=> array(
							'linear'	=> array(
								'fields'	=> array( 'image_overlay_angle' ),
							),
							'radial'	=> array(
								'fields'	=> array( 'image_overlay_gradient_position' ),
							),
						),
					),
					'image_overlay_angle'	=> array(
						'type'			=> 'text',
						'label'       	=> __( 'Angle', 'bb-njba' ),
						'default'     	=> '180',
						'maxlength'   	=> '3',
						'size'        	=> '5',
						'description'	=> __('degree', 'bb-njba')
					),
					'image_overlay_gradient_position'	=> array(
						'type'			=> 'select',
						'label'         => __( 'Position', 'bb-njba' ),
						'default'       => 'center center',
						'options'       => array(
							'center center'			=> __( 'Center Center', 'bb-njba' ),
							'center left'           => __( 'Center Left', 'bb-njba' ),
							'center right'          => __( 'Center Right', 'bb-njba' ),
							'top center'            => __( 'Top Center', 'bb-njba' ),
							'top left'            	=> __( 'Top Left', 'bb-njba' ),
							'top right'            	=> __( 'Top Right', 'bb-njba' ),
							'bottom center'         => __( 'Bottom Center', 'bb-njba' ),
							'bottom left'           => __( 'Bottom Left', 'bb-njba' ),
							'bottom right'          => __( 'Bottom Right', 'bb-njba' ),
						),
					),
					'image_overlay_opacity'	=> array(
						'type'			=> 'text',
						'label'       	=> __( 'Overlay Opacity', 'bb-njba' ),
						'default'     	=> '70',
						'description' 	=> '%',
						'maxlength'   	=> '3',
						'size'        	=> '5',
					),
					'likes_comments_color'	=> array(
						'type'			=> 'color',
						'label'     	=> __( 'Likes & Comments Color', 'bb-njba' ),
						'default'		=> '',
						'show_reset' 	=> true,
						'preview'       => array(
							'type'			=> 'css',
							'selector'      => '.njba-instagram-feed .njba-feed-item .njba-overlay-container',
							'property'      => 'color',
						),
					),
				),
			),
			'image_hover'	=> array(
				'title'     	=> __( 'Image Hover', 'bb-njba' ),
				'fields'    	=> array(
					'image_hover_grayscale'	=> array(
						'type'			=> 'select',
						'label'         => __( 'Grayscale Image', 'bb-njba' ),
						'default'       => 'no',
						'options'       => array(
							'yes'			=> __( 'Yes', 'bb-njba' ),
							'no'            => __( 'No', 'bb-njba' ),
						),
					),
					'image_hover_overlay_type'	=> array(
						'type'          => 'select',
						'label'         => __( 'Image Overlay Type', 'bb-njba' ),
						'default'       => 'none',
						'options'       => array(
							'none'        	=> __( 'None', 'bb-njba' ),
							'solid'        	=> __( 'Solid', 'bb-njba' ),
							'gradient'      => __( 'Gradient', 'bb-njba' ),
						),
						'toggle'    => array(
							'solid' 	=> array(
								'fields'    => array( 'image_hover_overlay_color', 'image_hover_overlay_opacity' ),
							),
							'gradient' => array(
								'fields'    => array( 'image_hover_overlay_angle', 'image_hover_overlay_color', 'image_hover_overlay_secondary_color', 'image_hover_overlay_gradient_type', 'image_hover_overlay_opacity' ),
							),
						),
					),
					'image_hover_overlay_color'	=> array(
						'type'			=> 'color',
						'label'         => __( 'Overlay Color', 'bb-njba' ),
						'default'       => '',
						'show_reset'    => true,
					),
					'image_hover_overlay_secondary_color'	=> array(
						'type'       	=> 'color',
						'label'     	=> __( 'Overlay Secondary Color', 'bb-njba' ),
						'default'		=> '',
						'show_reset' 	=> true,
					),
					'image_hover_overlay_gradient_type'	=> array(
						'type'			=> 'select',
						'label'         => __( 'Type', 'bb-njba' ),
						'default'       => 'linear',
						'options'       => array(
							'linear'        	=> __( 'Linear', 'bb-njba' ),
							'radial'            => __( 'Radial', 'bb-njba' ),
						),
						'toggle'	=> array(
							'linear'	=> array(
								'fields'	=> array( 'image_hover_overlay_angle' ),
							),
							'radial'	=> array(
								'fields'	=> array( 'image_hover_overlay_gradient_position' ),
							),
						),
					),
					'image_hover_overlay_angle'	=> array(
						'type'			=> 'text',
						'label'       	=> __( 'Angle', 'bb-njba' ),
						'default'     	=> '180',
						'maxlength'   	=> '3',
						'size'        	=> '5',
						'description'	=> __('degree', 'bb-njba')
					),
					'image_hover_overlay_gradient_position'	=> array(
						'type'			=> 'select',
						'label'         => __( 'Position', 'bb-njba' ),
						'default'       => 'center center',
						'options'       => array(
							'center center'			=> __( 'Center Center', 'bb-njba' ),
							'center left'           => __( 'Center Left', 'bb-njba' ),
							'center right'          => __( 'Center Right', 'bb-njba' ),
							'top center'            => __( 'Top Center', 'bb-njba' ),
							'top left'            	=> __( 'Top Left', 'bb-njba' ),
							'top right'            	=> __( 'Top Right', 'bb-njba' ),
							'bottom center'         => __( 'Bottom Center', 'bb-njba' ),
							'bottom left'           => __( 'Bottom Left', 'bb-njba' ),
							'bottom right'          => __( 'Bottom Right', 'bb-njba' ),
						),
					),
					'image_hover_overlay_opacity'	=> array(
						'type'			=> 'text',
						'label'       	=> __( 'Overlay Opacity', 'bb-njba' ),
						'default'     	=> '70',
						'description' 	=> '%',
						'maxlength'   	=> '3',
						'size'        	=> '5',
					),
					'likes_comments_hover_color'	=> array(
						'type'			=> 'color',
						'label'     	=> __( 'Likes & Comments Color', 'bb-njba' ),
						'default'		=> '',
						'show_reset' 	=> true,
					),
				),
			),
			'feed_title'	=> array(
				'title'			=> __( 'Feed Title', 'bb-njba' ),
				'fields'		=> array(
					'feed_title_position'	=> array(
						'type'          => 'select',
						'label'         => __( 'Position', 'bb-njba' ),
						'default'       => 'middle',
						'options'       => array(
							'top'			=> __( 'Top', 'bb-njba' ),
							'middle'        => __( 'Middle', 'bb-njba' ),
							'bottom'        => __( 'Bottom', 'bb-njba' ),
						),
					),
					'feed_title_bg_color'	=> array(
						'type'			=> 'color',
						'label'         => __( 'Background Color', 'bb-njba' ),
						'default'       => '',
						'show_reset'    => true,
						'show_alpha'	=> true,
						'preview'       => array(
							'type'			=> 'css',
							'selector'      => '.njba-instagram-feed .njba-instagram-feed-title-wrap',
							'property'      => 'background-color',
						),
					),
					'feed_title_bg_hover'	=> array(
						'type'          => 'color',
						'label'         => __( 'Background Hover Color', 'bb-njba' ),
						'default'       => '',
						'show_reset'    => true,
						'show_alpha'	=> true,
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.njba-instagram-feed .njba-instagram-feed-title-wrap:hover',
							'property'      => 'background-color',
						),
					),
					'feed_title_text_color'	=> array(
						'type'          => 'color',
						'label'         => __( 'Text Color', 'bb-njba' ),
						'default'       => '',
						'show_reset'    => true,
						'show_alpha'	=> true,
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.njba-instagram-feed .njba-instagram-feed-title-wrap .njba-instagram-feed-title',
							'property'      => 'color',
						),
					),
					'feed_title_text_hover'	=> array(
						'type'			=> 'color',
						'label'         => __( 'Text Hover Color', 'bb-njba' ),
						'default'       => '',
						'show_reset'    => true,
						'show_alpha'	=> true,
						'preview'       => array(
							'type'			=> 'css',
							'selector'      => '.njba-instagram-feed .njba-instagram-feed-title-wrap:hover .njba-instagram-feed-title',
							'property'      => 'color',
						),
					),
					'feed_title_border'	=> array(
						'type'		=> 'select',
						'label'     => __( 'Border', 'bb-njba' ),
						'default'   => 'none',
						'options'   => array(
							'none'		=> __( 'None', 'bb-njba' ),
							'solid'     => __( 'Solid', 'bb-njba' ),
							'dashed'    => __( 'Dashed', 'bb-njba' ),
							'dotted'    => __( 'Dotted', 'bb-njba' ),
							),
						'toggle'	=> array(
							'solid'		=> array(
								'fields'	=> array( 'feed_title_border_width', 'feed_title_border_color', 'feed_title_border_hover' ),
							),
							'dashed'    => array(
								'fields'   => array( 'feed_title_border_width', 'feed_title_border_color', 'feed_title_border_hover' ),
							),
							'dotted'    => array(
								'fields'   => array( 'feed_title_border_width', 'feed_title_border_color', 'feed_title_border_hover' ),
							),
							'double'	=> array(
								'fields'   => array( 'feed_title_border_width', 'feed_title_border_color', 'feed_title_border_hover' ),
							),
						),
					),
					'feed_title_border_width'	=> array(
						'type'			=> 'text',
						'label'         => __( 'Border Width', 'bb-njba' ),
						'description'   => 'px',
						'size'      	=> 5,
						'maxlength' 	=> 3,
						'default'       => '1',
						'preview'       => array(
							'type'			=> 'css',
							'selector'      => '.njba-instagram-feed .njba-instagram-feed-title-wrap',
							'property'      => 'border-width',
							'unit'          => 'px',
						),
					),
					'feed_title_border_color'	=> array(
						'type'				=> 'color',
						'label'     		=> __( 'Border Color', 'bb-njba' ),
						'show_reset' 		=> true,
						'default'   		=> '',
						'preview'         	=> array(
							'type'				=> 'css',
							'selector'        	=> '.njba-instagram-feed .njba-instagram-feed-title-wrap',
							'property'        	=> 'border-color',
						),
					),
					'feed_title_border_hover'	=> array(
						'type'			=> 'color',
						'label'     	=> __( 'Border Hover Color', 'bb-njba' ),
						'show_reset' 	=> true,
						'default'   	=> '',
						'preview'       => array(
							'type'			=> 'css',
							'selector'      => '.njba-instagram-feed .njba-instagram-feed-title-wrap:hover',
							'property'      => 'border-color',
						),
					),
					'feed_title_border_radius'	=> array(
						'type'          => 'text',
						'label'         => __( 'Round Corners', 'bb-njba' ),
						'description'   => 'px',
						'size'      	=> 5,
						'maxlength' 	=> 3,
						'default'       => 0,
						'preview'       => array(
							'type'			=> 'css',
							'selector'      => '.njba-instagram-feed .njba-instagram-feed-title-wrap',
							'property'      => 'border-radius',
							'unit'          => 'px',
						),
					),
					'feed_title_horizontal_padding'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Horizontal Padding', 'bb-njba'),
                        'description'	=> 'px',
						'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
                        'preview'	=> array(
							'type'		=> 'css',
							'rules'		=> array(
								array(
									'selector'	=> '.njba-instagram-feed .njba-instagram-feed-title-wrap',
									'property'	=> 'padding-left',
									'unit' 		=> 'px',
								),
								array(
									'selector'	=> '.njba-instagram-feed .njba-instagram-feed-title-wrap',
									'property'	=> 'padding-right',
									'unit' 		=> 'px',
								),
							),
						)
                    ),
					'feed_title_vertical_padding'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Vertical Padding', 'bb-njba'),
                        'description'	=> 'px',
						'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
                        'preview'	=> array(
							'type'		=> 'css',
							'rules'		=> array(
								array(
									'selector'	=> '.njba-instagram-feed .njba-instagram-feed-title-wrap',
									'property'	=> 'padding-top',
									'unit' 		=> 'px',
								),
								array(
									'selector'	=> '.njba-instagram-feed .njba-instagram-feed-title-wrap',
									'property'	=> 'padding-bottom',
									'unit' 		=> 'px',
								),
							),
						)
                    ),
				),
			),
		),
	),
	'typography'	=> array(
		'title'			=> __( 'Typography', 'bb-njba' ),
		'sections'  	=> array(
			'feed_title_typography'	=> array(
				'title'		=> __( 'Feed Title', 'bb-njba' ),
				'fields'	=> array(
					'feed_title_font'	=> array(
						'type'			=> 'font',
						'label'			=> __( 'Font', 'bb-njba' ),
						'default'		=> array(
							'family'		=> 'Default',
							'weight'		=> '400',
						),
						'preview'		=> array(
							'type'			=> 'font',
							'selector'  	=> '.njba-instagram-feed .njba-instagram-feed-title-wrap',
						),
					),
					'feed_title_font_size'	=> array(
						'type'			=> 'select',
						'label'			=> __( 'Font Size', 'bb-njba' ),
						'default'		=> 'default',
						'options'       => array(
							'default'		=> __( 'Default', 'bb-njba' ),
							'custom'        => __( 'Custom', 'bb-njba' ),
						),
						'toggle'	=> array(
							'custom'	=> array(
								'fields'	=> array( 'feed_title_custom_font_size' ),
							),
						),
					),
					'feed_title_custom_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Custom Font Size', 'bb-njba'),
                        'description'	=> 'px',
						'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
                        'preview'	=> array(
							'type'		=> 'css',
							'selector'	=> '.njba-instagram-feed .njba-instagram-feed-title-wrap',
							'property'	=> 'font-size',
							'unit' 		=> 'px',
						),
                    ),
					'feed_title_transform'	=> array(
						'type'			=> 'select',
						'label'         => __( 'Text Transform', 'bb-njba' ),
						'default'       => 'none',
						'options'       => array(
							'none'			=> __( 'None', 'bb-njba' ),
							'lowercase'     => __( 'lowercase', 'bb-njba' ),
							'uppercase'    	=> __( 'UPPERCASE', 'bb-njba' ),
						),
						'preview'	=> array(
							'type'		=> 'css',
							'selector'	=> '.njba-instagram-feed .njba-instagram-feed-title-wrap',
							'property'	=> 'text-transform',
						),
					),
					'feed_title_line_height'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Line Height', 'bb-njba'),
                        'description'	=> 'px',
						'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        ),
                        'preview'	=> array(
							'type'		=> 'css',
							'selector'	=> '.njba-instagram-feed .njba-instagram-feed-title-wrap',
							'property'	=> 'line-height',
						),
                    ),
					'feed_title_letter_spacing'	=> array(
						'type'			=> 'text',
						'label'         => __( 'Letter Spacing', 'bb-njba' ),
						'description'   => 'px',
						'size'      	=> 5,
						'maxlength' 	=> 3,
						'default'       => 0,
						'preview'		=> array(
							'type'			=> 'css',
							'selector'      => '.njba-instagram-feed .njba-instagram-feed-title-wrap',
							'property'      => 'letter-spacing',
							'unit'          => 'px',
						),
					),
				),
			),
		),
	),
));