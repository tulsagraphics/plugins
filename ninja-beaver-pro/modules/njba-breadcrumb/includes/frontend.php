<?php
global $post,$wp_query;
$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$custom_taxonomy_name = @$term->name; // will show the name
$custom_taxonomy_slug = @$term->slug; // will show the slug
if( ( $settings->homepage == 'homepage_image') && ( $settings->photo_src != '') ){
    $custom_photo_home_name = $settings->photo_src;
}
elseif( ( $settings->homepage == 'homepage_icon') && ( $settings->homepage_icon != '') ){
    $custom_icon_home_name = $settings->homepage_icon;
}
elseif( ( $settings->homepage == 'homepage_text') && ( $settings->homepage_name != '') ){
    $custom_text_home_name = $settings->homepage_name;
}
else{
    $default_home_name = get_the_title( get_option('page_on_front') );
} 
if ( !is_front_page() ) { ?>
	<div id="njba-daynamic-breadcrumb" class="njba-breadcrumb-preview njba-breadcrumb">
		<!-- <div class="item-home"><a class="bread-link bread-home" href="<?php echo get_home_url();?>" title="<?php echo $settings->home_text;?>"><?php echo $settings->home_text;?></a></div>
		<span><i class="<?php echo $settings->separator_icon;?>"></i></span> -->
		<?php
		if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {?>
                  
                <div class="item-current item-archive"><strong class="bread-current bread-archive"><?php echo post_type_archive_title($prefix, false);?></strong></div>
                  
        <?php } 
        else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
                  
                $post_type = get_post_type();
                if($post_type != 'post') {
                      
                    $post_type_object = get_post_type_object($post_type);
                    $post_type_archive = get_post_type_archive_link($post_type);?>
                  
                    <div class="item-cat item-custom-post-type-<?php echo $post_type;?>">
                    	<a class="bread-cat bread-custom-post-type-<?php echo $post_type;?>" href="<?php echo $post_type_archive;?>" title="<?php echo $post_type_object->labels->name;?>"><?php echo $post_type_object->labels->name;?></a>
                    </div>
                    <span><i class="<?php echo $settings->separator_icon;?>"></i></span>
            <?php }
                  
                $custom_tax_name = get_queried_object()->name;?>
                <div class="item-current item-archive"><strong class="bread-current bread-archive"><?php echo $custom_tax_name;?></strong></div>
                  
        <?php } 
        else if ( is_single() ) {
                  
                // If post is a custom post type
                $post_type = get_post_type();
                  
                // If it is a custom post type display name and link
                if($post_type != 'post') { 
                      
                    $post_type_object = get_post_type_object($post_type);
                    $post_type_archive = get_post_type_archive_link($post_type);?>
                  
                    <div class="item-cat item-custom-post-type-<?php echo $post_type;?>">
                    	<a class="bread-cat bread-custom-post-type-<?php echo $post_type;?>" href="<?php echo $post_type_archive;?>" title="<?php echo $post_type_object->labels->name;?>"><?php echo $post_type_object->labels->name;?></a>
                    </div>
                    <span><i class="<?php echo $settings->separator_icon;?>"></i></span>
                  
                <?php }
                  
                // Get post category info
                $category = get_the_category();
                 
                if(!empty($category)) {
                  
                    // Get last category post is in
                    $last_category = end(array_values($category));
                      
                    // Get parent any categories and create array
                    $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                    $cat_parents = explode(',',$get_cat_parents);
                      
                    // Loop through parent categories and store in variable $cat_display
                    $cat_display = '';
                    foreach($cat_parents as $parents) {
                        $cat_display .= '<div class="item-cat">'.$parents.'</div>';
                        $cat_display .= '<div class="separator"><span><i class=" ' . $settings->separator_icon . '"></i></span></div>';
                    }
                 
                }
                  
                // If it's a custom post type within a custom taxonomy
                $taxonomy_exists = taxonomy_exists($custom_taxonomy);
                if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                       
                    $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                    $cat_id         = $taxonomy_terms[0]->term_id;
                    $cat_nicename   = $taxonomy_terms[0]->slug;
                    $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                    $cat_name       = $taxonomy_terms[0]->name;
                   
                }
                  
                // Check if the post is in a category
                if(!empty($last_category)) {
                    echo $cat_display;?>
                    <div class="item-current item-<?php echo $post->ID;?>"><strong class="bread-current bread-<?php echo $post->ID;?>" title="<?php echo get_the_title();?>"><?php echo get_the_title();?></strong></div>
                      
                // Else if post is in a custom taxonomy
                <?php } else if(!empty($cat_id)) {?>
                      
                    <div class="item-cat item-cat-<?php echo $cat_id;?> item-cat-<?php echo $cat_nicename;?>">
                    	<a class="bread-cat bread-cat-<?php echo $cat_id;?> bread-cat-<?php echo $cat_nicename;?>" href="<?php echo $cat_link;?>" title="<?php echo $cat_name;?>"><?php echo $cat_name;?></a>
                    </div>
                    <span><i class="<?php echo $settings->separator_icon;?>"></i></span>
                    <div class="item-current item-<?php echo $post->ID;?>"><strong class="bread-current bread-<?php echo $post->ID;?>" title="<?php echo get_the_title();?>"><?php echo get_the_title();?></strong></div>
                  
                <?php } else { ?>
                      
                    <div class="item-current item-<?php echo $post->ID;?>"><strong class="bread-current bread-<?php echo $post->ID;?>" title="<?php echo get_the_title();?>"><?php echo get_the_title();?></strong></div>
                      
                <?php }
                  
        } 
        else if ( is_category() ) { ?>
                   
                <div class="item-current item-cat"><strong class="bread-current bread-cat"><?php echo single_cat_title('', false);?></strong></div>';
                   
        <?php 
        } 
        else if ( is_page() ) {
                   
                // Standard page
                if( $post->post_parent ){
                       
                    // If child page, get parents 
                    $anc = get_post_ancestors( $post->ID );
                    //echo "<pre>";print_r($anc);echo"</pre></br>";   
                    // Get parents in the right order
                    $anc = array_reverse($anc);
                    //echo "<pre>";print_r($anc);echo"</pre>";
                    $front_page = get_option('page_on_front');
                    //echo $front_page;
                    $home_title = get_the_title( get_option('page_on_front') );
                       
                    // Parent page loop
                    if ( !isset( $parents ) ) $parents = null;

                    if( ( $settings->homepage == 'homepage_image') && ( $settings->photo_src != '') ){ ?>
                        <div class="item-image-home"><a class="bread-link bread-home" href="<?php echo get_home_url();?>" title="<?php echo $home_title;?>"><img src="<?php echo $custom_photo_home_name; ?>" class="njba-img-responsive"></a></div>
                    <?php }
                    elseif( ( $settings->homepage == 'homepage_icon') && ( $settings->homepage_icon != '') ){ ?>
                        <div class="item-icon-home"><a class="bread-link bread-home" href="<?php echo get_home_url();?>" title="<?php echo $home_title;?>"><i class="<?php echo $custom_icon_home_name; ?>" aria-hidden="true"></i></a></div>
                    <?php }
                    elseif( ( $settings->homepage == 'homepage_text') && ( $settings->homepage_name != '') ){ ?>
                        <div class="item-text-home"><a class="bread-link bread-home" href="<?php echo get_home_url();?>" title="<?php echo $home_title;?>"><?php echo $custom_text_home_name;?></a></div>
                    <?php }
                    else{ ?>
                        <div class="item-default-home"><a class="bread-link bread-home" href="<?php echo get_home_url();?>" title="<?php echo $home_title;?>"><?php echo $default_home_name;?></a></div>
                    <?php } ?>
                    <span class="separator"><i class="<?php echo $settings->separator_icon;?>"></i></span>  
                    <?php
                    foreach ( $anc as $ancestor ) {
                        if( $ancestor == $front_page){}
                        else{
                        $parents .= '<div class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></div>';
                        $parents .= '<div class="separator separator-' . $ancestor . '"><span><i class="' . $settings->separator_icon . '"></i></span></div>';
                        }
                    }
                       
                    // Display parent pages
                    echo $parents;
                       
                    ?>
                    <div class="item-current item-<?php echo $post->ID;?>"><strong title="<?php echo get_the_title();?>"><?php echo get_the_title();?></strong></div>
                    <?php   
                } else { $home_title = get_the_title( get_option('page_on_front') ); ?>
                    <!-- <div class="item-home"><a class="bread-link bread-home" href="<?php// echo get_home_url();?>" title="<?php// echo $home_title;?>"><?php //echo $homepage_name;?></a></div> -->
                    <?php
                    if( ( $settings->homepage == 'homepage_image') && ( $settings->photo_src != '') ){ ?>
                        <div class="item-image-home"><a class="bread-link bread-home" href="<?php echo get_home_url();?>" title="<?php echo $home_title;?>"><img src="<?php echo $custom_photo_home_name; ?>" class="njba-img-responsive"></a></div>
                    <?php }
                    elseif( ( $settings->homepage == 'homepage_icon') && ( $settings->homepage_icon != '') ){ ?>
                        <div class="item-icon-home"><a class="bread-link bread-home" href="<?php echo get_home_url();?>" title="<?php echo $home_title;?>"><i class="<?php echo $custom_icon_home_name; ?>" aria-hidden="true"></i></a></div>
                    <?php }
                    elseif( ( $settings->homepage == 'homepage_text') && ( $settings->homepage_name != '') ){ ?>
                        <div class="item-text-home"><a class="bread-link bread-home" href="<?php echo get_home_url();?>" title="<?php echo $home_title;?>"><?php echo $custom_text_home_name;?></a></div>
                    <?php }
                    else{ ?>
                        <div class="item-default-home"><a class="bread-link bread-home" href="<?php echo get_home_url();?>" title="<?php echo $home_title;?>"><?php echo $default_home_name;?></a></div>
                    <?php } ?>
                    <span class="separator"><i class="<?php echo $settings->separator_icon;?>"></i></span>  
                    <div class="item-current item-<?php echo $post->ID;?>"><strong class="bread-current bread-<?php echo $post->ID;?>"><?php echo get_the_title();?></strong></div>
                     <?php  
                }
                   
            }
    else if ( is_tag() ) {
                   
                // Tag page
                   
                // Get tag information
                $term_id        = get_query_var('tag_id');
                $taxonomy       = 'post_tag';
                $args           = 'include=' . $term_id;
                $terms          = get_terms( $taxonomy, $args );
                $get_term_id    = $terms[0]->term_id;
                $get_term_slug  = $terms[0]->slug;
                $get_term_name  = $terms[0]->name;
                   
                // Display the tag name
                ?>
                <div class="item-current item-tag-<?php echo $get_term_id;?> item-tag-<?php echo $get_term_slug;?>"><strong class="bread-current bread-tag-<?php echo $get_term_id;?> bread-tag-<?php echo $get_term_slug;?>"><?php echo $get_term_name;?></strong></div>
            <?php   
            } 
    elseif ( is_day() ) { ?>
           
                <div class="item-year item-year-<?php echo get_the_time('Y');?>">
                	<a class="bread-year bread-year-<?php echo get_the_time('Y');?>" href="<?php echo get_year_link( get_the_time('Y') );?>" title="<?php echo get_the_time('Y');?>"><?php echo get_the_time('Y');?> Archives</a>
                </div>
                <div class="separator separator-<?php echo get_the_time('Y');?>"><span><i class="<?php echo $settings->separator_icon;?>"></i></span></div>
                   
                <div class="item-month item-month-<?php echo get_the_time('m');?>">
                	<a class="bread-month bread-month-<?php echo get_the_time('m');?>" href="<?php echo get_month_link( get_the_time('Y'), get_the_time('m') );?>" title="<?php echo get_the_time('M');?>"><?php echo get_the_time('M');?> Archives</a>
                </div>
                <div class="separator separator-<?php echo get_the_time('m');?>"><span><i class="<?php echo $settings->separator_icon;?>"></i></span></div>
                   
                <div class="item-current item-<?php echo get_the_time('j');?>"><strong class="bread-current bread-<?php echo get_the_time('j');?>"> <?php echo get_the_time('jS');?> <?php echo get_the_time('M');?> Archives</strong></div>
            <?php      
            }
    else if ( is_month() ) {
           
                // Month Archive
                   
                // Year link
                ?>
                <div class="item-year item-year-<?php echo get_the_time('Y');?>">
                	<a class="bread-year bread-year-<?php echo get_the_time('Y');?>" href="<?php echo get_year_link( get_the_time('Y') );?>" title="<?php echo get_the_time('Y');?>"><?php echo get_the_time('Y');?> Archives</a>
                </div>
                <div class="separator separator-<?php echo get_the_time('Y');?>"><span><i class="<?php echo $settings->separator_icon;?>"></i></span></div>
                   
                <div class="item-month item-month-<?php echo get_the_time('m');?>"><strong class="bread-month bread-month-<?php echo get_the_time('m');?>" title="<?php echo get_the_time('M');?>"><?php echo get_the_time('M');?> Archives</strong></div>
            <?php      
            } 
    else if ( is_year() ) {
           
                // Display year archive
    			?>
                <div class="item-current item-current-<?php echo get_the_time('Y');?>"><strong class="bread-current bread-current-<?php echo get_the_time('Y');?>" title="<?php echo get_the_time('Y');?>"><?php echo get_the_time('Y');?> Archives</strong></div>
            <?php       
            } 
    else if ( is_author() ) {
           
                // Auhor archive
                   
                // Get the author information
                global $author;
                $userdata = get_userdata( $author );
                   
                // Display author name
                ?>
                <div class="item-current item-current-<?php echo $userdata->user_nicename;?>"><strong class="bread-current bread-current-<?php echo $userdata->user_nicename;?>" title="<?php echo $userdata->display_name;?>">Author: <?php echo $userdata->display_name;?></strong></div>
            <?php  
            } 
    else if ( get_query_var('paged') ) { ?>
           
                <div class="item-current item-current-<?php echo get_query_var('paged');?>"><strong class="bread-current bread-current-<?php echo get_query_var('paged');?>" title="Page <?php echo get_query_var('paged');?>"><?php echo __('Page');?> <?php echo get_query_var('paged');?></strong></div>
            
            <?php       
            } 
    else if ( is_search() ) { ?>
       
                <div class="item-current item-current-<?php echo get_search_query();?>"><strong class="bread-current bread-current-<?php echo get_search_query();?>" title="Search results for: <?php echo get_search_query();?>">Search results for: <?php echo get_search_query();?></strong></div>
            
            <?php  
            } 
    elseif ( is_404() ) { ?>
           
                <div><?php echo "Error 404"; ?></div>
            
            <?php
            }
    ?>
    </div>
<?php }
?>
