.fl-node-<?php echo $id; ?> .njba-breadcrumb {
    <?php if( $settings->breadcrumb_alignment ) { ?>text-align: <?php echo $settings->breadcrumb_alignment; ?>;<?php } ?>
}

.fl-node-<?php echo $id; ?> .item-text-home > a, .item-default-home > a {
    <?php if( $settings->homepage_name_font['family'] != 'Default' ) { ?> <?php FLBuilderFonts::font_css( $settings->homepage_name_font ); ?> <?php } ?>
    <?php if(isset($settings->homepage_name_font_size['desktop'] )  ) { ?> font-size: <?php echo $settings->homepage_name_font_size['desktop']; ?>px; <?php } ?>
    <?php if(isset($settings->homepage_name_line_height['desktop'] ) ){ ?> line-height: <?php echo $settings->homepage_name_line_height['desktop']; ?>px; <?php } ?>
    <?php if($settings->homepage_name_color){ ?> color: #<?php echo $settings->homepage_name_color; ?>; <?php } ?>
    text-transform: <?php echo $settings->label_text_transform; ?>;
}
.fl-node-<?php echo $id; ?> .item-text-home > a:hover, .item-default-home > a:hover {
    <?php if($settings->homepage_name_hover_color){ ?> color: #<?php echo $settings->homepage_name_hover_color; ?>; <?php } ?>
    text-decoration: none;
}
.fl-node-<?php echo $id; ?> .item-image-home {
    <?php if( $settings->homepage_image_size != '' ) { ?>max-width: <?php echo $settings->homepage_image_size; ?>px;<?php } else { echo "max-width:100%;"; } ?>
    <?php if($settings->homepage_image_size != '') { echo 'width: '.$settings->homepage_image_size.'px;'; } else { echo 'width: auto;'; }?>
}
.fl-node-<?php echo $id; ?> .item-icon-home > a i{
    <?php if( !empty($settings->homepage_icon_size['desktop'] ) ) { ?>font-size: <?php echo $settings->homepage_icon_size['desktop']; ?>px;<?php } ?>
    <?php if($settings->home_icon_color) { ?> color: <?php echo '#'.$settings->home_icon_color; ?>; <?php } ?>
}
.fl-node-<?php echo $id; ?> .item-icon-home > a i:hover{
    <?php if($settings->home_icon_hover_color) { ?> color: <?php echo '#'.$settings->home_icon_hover_color; ?>; <?php } ?>
}
.fl-node-<?php echo $id; ?> .separator i {
    <?php if( $settings->separator_icon_color ) { ?>color: #<?php echo $settings->separator_icon_color; ?>;<?php } ?>
    <?php if( !empty($settings->separator_icon_font_size['desktop'] ) ) { ?>font-size: <?php echo $settings->separator_icon_font_size['desktop']; ?>px;<?php } ?>
    <?php if( !empty($settings->icon_padding['top'] ) ) { ?> padding-top: <?php echo $settings->icon_padding['top']; ?>px;<?php } ?>
    <?php if( !empty($settings->icon_padding['bottom'] ) ) { ?>padding-bottom: <?php echo $settings->icon_padding['bottom']; ?>px; <?php } ?>
    <?php if( !empty($settings->icon_padding['left'] ) ) { ?>padding-left: <?php echo $settings->icon_padding['left']; ?>px; <?php } ?>
    <?php if( !empty($settings->icon_padding['right'] ) ) { ?>padding-right: <?php echo $settings->icon_padding['right']; ?>px; <?php } ?>
    <?php if( !empty($settings->icon_margin['top'] ) ) { ?> margin-top: <?php echo $settings->icon_margin['top']; ?>px;<?php } ?>
    <?php if( !empty($settings->icon_margin['bottom'] ) ) { ?>margin-bottom: <?php echo $settings->icon_margin['bottom']; ?>px; <?php } ?>
    <?php if( !empty($settings->icon_margin['left'] ) ) { ?>margin-left: <?php echo $settings->icon_margin['left']; ?>px; <?php } ?>
    <?php if( !empty($settings->icon_margin['right'] ) ) { ?>margin-right: <?php echo $settings->icon_margin['right']; ?>px; <?php } ?>

}
.fl-node-<?php echo $id; ?> .item-parent a {
    <?php if( $settings->homepage_name_color ) { ?>color: #<?php echo $settings->homepage_name_color; ?>;<?php } ?>
    <?php if( $settings->homepage_name_font['family'] != 'Default' ) { ?> <?php FLBuilderFonts::font_css( $settings->homepage_name_font ); ?> <?php } ?>
    <?php if(isset($settings->homepage_name_font_size['desktop'] )  ) { ?> font-size: <?php echo $settings->homepage_name_font_size['desktop']; ?>px; <?php } ?>
    <?php if(isset($settings->homepage_name_line_height['desktop'] ) ){ ?> line-height: <?php echo $settings->homepage_name_line_height['desktop']; ?>px; <?php } ?>
    text-transform: <?php echo $settings->label_text_transform; ?>;
}
.fl-node-<?php echo $id; ?> .item-parent a:hover {
    <?php if( $settings->homepage_name_hover_color ) { ?>color: #<?php echo $settings->homepage_name_hover_color; ?>;<?php } ?>
    text-decoration: none;
}
.fl-node-<?php echo $id; ?> .item-current {
    <?php if( $settings->current_page_color ) { ?>color: #<?php echo $settings->current_page_color; ?>;<?php } ?>
    <?php if( $settings->homepage_name_font['family'] != 'Default' ) { ?> <?php FLBuilderFonts::font_css( $settings->homepage_name_font ); ?> <?php } ?>
    <?php if(isset($settings->homepage_name_font_size['desktop'] )  ) { ?> font-size: <?php echo $settings->homepage_name_font_size['desktop']; ?>px; <?php } ?>
    <?php if(isset($settings->homepage_name_line_height['desktop'] ) ){ ?> line-height: <?php echo $settings->homepage_name_line_height['desktop']; ?>px; <?php } ?>
    text-transform: <?php echo $settings->label_text_transform; ?>;
}
@media ( max-width: <?php echo $global_settings->medium_breakpoint .'px'; ?> ) {
    .fl-node-<?php echo $id; ?> .item-text-home > a, .item-default-home > a {
        <?php if(isset($settings->homepage_name_font_size['medium'] )  ) { ?> font-size: <?php echo $settings->homepage_name_font_size['medium']; ?>px; <?php } ?>
        <?php if(isset($settings->homepage_name_line_height['medium'] ) ){ ?> line-height: <?php echo $settings->homepage_name_line_height['medium']; ?>px; <?php } ?>
    }
    .fl-node-<?php echo $id; ?> .item-icon-home > a i{
        <?php if( !empty($settings->homepage_icon_size['medium'] ) ) { ?>font-size: <?php echo $settings->homepage_icon_size['medium']; ?>px;<?php } ?>
    }
    .fl-node-<?php echo $id; ?> .separator i {
        <?php if( !empty($settings->separator_icon_font_size['medium'] ) ) { ?>font-size: <?php echo $settings->separator_icon_font_size['medium']; ?>px;<?php } ?>
    }
    .fl-node-<?php echo $id; ?> .item-parent a {
        <?php if(isset($settings->homepage_name_font_size['medium'] )  ) { ?> font-size: <?php echo $settings->homepage_name_font_size['medium']; ?>px; <?php } ?>
        <?php if(isset($settings->homepage_name_line_height['medium'] ) ){ ?> line-height: <?php echo $settings->homepage_name_line_height['medium']; ?>px; <?php } ?>
    }
    .fl-node-<?php echo $id; ?> .item-current {
        <?php if(isset($settings->homepage_name_font_size['medium'] )  ) { ?> font-size: <?php echo $settings->homepage_name_font_size['medium']; ?>px; <?php } ?>
        <?php if(isset($settings->homepage_name_line_height['medium'] ) ){ ?> line-height: <?php echo $settings->homepage_name_line_height['medium']; ?>px; <?php } ?>
    }
}
@media ( max-width: <?php echo $global_settings->responsive_breakpoint .'px'; ?> ) {
    .fl-node-<?php echo $id; ?> .item-text-home > a, .item-default-home > a {
        <?php if(isset($settings->homepage_name_font_size['small'] )  ) { ?> font-size: <?php echo $settings->homepage_name_font_size['small']; ?>px; <?php } ?>
        <?php if(isset($settings->homepage_name_line_height['small'] ) ){ ?> line-height: <?php echo $settings->homepage_name_line_height['small']; ?>px; <?php } ?>
    }
    .fl-node-<?php echo $id; ?> .item-icon-home > a i{
        <?php if( !empty($settings->homepage_icon_size['small'] ) ) { ?>font-size: <?php echo $settings->homepage_icon_size['small']; ?>px;<?php } ?>
    }
    .fl-node-<?php echo $id; ?> .separator i {
        <?php if( !empty($settings->separator_icon_font_size['small'] ) ) { ?>font-size: <?php echo $settings->separator_icon_font_size['small']; ?>px;<?php } ?>
    }
    .fl-node-<?php echo $id; ?> .item-parent a {
        <?php if(isset($settings->homepage_name_font_size['small'] )  ) { ?> font-size: <?php echo $settings->homepage_name_font_size['small']; ?>px; <?php } ?>
        <?php if(isset($settings->homepage_name_line_height['small'] ) ){ ?> line-height: <?php echo $settings->homepage_name_line_height['small']; ?>px; <?php } ?>
    }
    .fl-node-<?php echo $id; ?> .item-current {
        <?php if(isset($settings->homepage_name_font_size['small'] )  ) { ?> font-size: <?php echo $settings->homepage_name_font_size['small']; ?>px; <?php } ?>
        <?php if(isset($settings->homepage_name_line_height['small'] ) ){ ?> line-height: <?php echo $settings->homepage_name_line_height['small']; ?>px; <?php } ?>
    }
}