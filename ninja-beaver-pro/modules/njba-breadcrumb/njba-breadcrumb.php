<?php
/**
 * @class NJBABreadcrumbModule
 */
class NJBABreadcrumbModule extends FLBuilderModule {
    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Breadcrumb', 'bb-njba'),
            'description'   => __('Breadcrumb to Pages.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'content' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-breadcrumb/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-breadcrumb/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'partial_refresh' => true, // Set this to true to enable partial refresh.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
       
    }
    
    
    
}
FLBuilder::register_module('NJBABreadcrumbModule', array(
    'simple_breadcrumb'       => array(
        'title'     => __('Breadcrumb', 'bb-njba'),
        'sections'  => array(
            'breadcrumb_home_title'       => array( // Section
                'title'         => __('Breadcrumb Home', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'homepage'         => array(
                        'type'                      => 'select',
                        'default'                   => 'homepage_text',
                        'label'                     => __('Select Home', 'bb-njba'),
                        'options'                   => array(
                            'homepage_text'         => __('Text', 'bb-njba'),
                            'homepage_icon'         => __('Icon', 'bb-njba'),
                            'homepage_image'        => __('Image', 'bb-njba'),
                        ),
                        'toggle'        => array(
                            'homepage_text'        => array(
                                'fields'        => array('homepage_name')
                            ),
                            'homepage_icon'        => array(
                                'fields'        => array('homepage_icon'),
                                'sections'      => array('home_icon')
                            ),
                            'homepage_image'        => array(
                                'fields'        => array('photo', 'homepage_image_size')
                            ),
                        )
                    ),
                    'homepage_name'        => array(
                        'type'            => 'text',
                        'label'           => __('Homepage Name', 'bb-njba'),  
                        'default'         => 'HOME',
                        'preview'       => array(
                            'type'          => 'text',
                            'selector'      => '.item-text-home > a, .item-default-home > a',
                        )
                    ),
                    'homepage_icon'          => array(
                        'type'          => 'icon',
                        'label'         => __('Homepage Icon', 'bb-njba'),
                        'show_remove' => true
                    ),
                    'photo'         => array(
                        'type'          => 'photo',
                        'label'         => __( 'Homepage Photo', 'bb-njba' ),
                        'connections'   => array( 'photo' ),
                        'show_remove'   => true,
                    ),
                    'homepage_image_size'     => array(
                        'type'          => 'text',
                        'label'         => __('Homepage Image Size', 'bb-njba'),
                        'placeholder'   => 'auto',
                        'help'      => __('This size is adjust your photo and it\'s Background.', 'bb-njba'),
                        'maxlength'     => '5',
                        'size'          => '6',
                        'description'   => 'px',
                    ),
                )
            ),
            'breadcrumb_icon'       => array( // Section
                'title'         => __('Separator Icon', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'separator_icon'          => array(
                        'type'          => 'icon',
                        'label'         => __('Icon', 'bb-njba')
                    ),
                    
                )
            ),
        ),
    ),
    'breadcrumb_style'       => array(
        'title'     => __('Style', 'bb-njba'),
        'sections'  => array(
            'breadcrumb_general'       => array( // Section
                'title'         => __('general', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'breadcrumb_alignment'         => array(
                        'type'                      => 'select',
                        'default'                   => 'center',
                        'label'                     => __('Alignment', 'bb-njba'),
                        'options'                   => array(
                            'left'                      => __('Left', 'bb-njba'),
                            'right'                     => __('Right', 'bb-njba'),
                            'center'                    => __('Center', 'bb-njba'),
                        ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-breadcrumb',
                            'property'      => 'text-align'
                        )
                    ),
                    'homepage_name_font'          => array(
                        'type'          => 'font',
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 300
                        ),
                        'label'         => __('Font', 'bb-njba'),
                        'preview'       => array(
                            'type'          => 'font',
                            'selector'      => '.item-text-home > a, .item-default-home > a',
                        )
                    ),
                    'homepage_name_font_size'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Font Size', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '28',
                            'medium'        => '24',
                            'small'         => '20',
                        ),
                        'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
                    ),
                    'homepage_name_line_height'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Line Height', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '30',
                            'medium'        => '26',
                            'small'         => '22',
                        ),
                        'description'   => _x( 'px', 'Value unit for line height. Such as: "14 px"', 'bb-njba' ),
                    ),
                    'homepage_name_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Link Color', 'bb-njba'),
                        'default'       => '000000',
                        'show_reset'    => true,
                    ),
                    'homepage_name_hover_color'    => array(
                        'type'          => 'color',
                        'label'         => __('Hover Link Color', 'bb-njba'),
                        'default'       => '000000',
                        'show_reset'    => true,
                    ),
                    'current_page_color'    => array(
                        'type'          => 'color',
                        'label'         => __('current Page Color', 'bb-njba'),
                        'default'       => '000000',
                        'show_reset'    => true,
                    ),
                    'label_text_transform'    => array(
                        'type'                      => 'select',
                        'label'                     => __('Text Transform', 'bb-njba'),
                        'default'                   => 'none',
                        'options'                   => array(
                            'none'                  => __('Default', 'bb-njba'),
                            'lowercase'                => __('lowercase', 'bb-njba'),
                            'uppercase'                 => __('UPPERCASE', 'bb-njba'),
                        )
                    ),
                )
            ),
            'home_icon'       => array( // Section
                'title'         => __('Homepage->Icon', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'home_icon_color' => array(
                        'type' => 'color',
                        'label' => __('Icon Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'FF0000'
                    ),
                    'home_icon_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Icon Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ADFF2F'
                    ),
                    'homepage_icon_size'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Size', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '28',
                            'medium'        => '24',
                            'small'         => '20',
                        ),
                        'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'selector'      => '.njba-heading-title',
                            'property'      => 'font-size',
                            'unit'          => 'px'
                        )
                    ),
                )
            ),
            'simple_breadcrumb_separator'       => array( // Section
                'title'         => __('Separator Icon', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'separator_icon_font_size'     => array(
                        'type'          => 'njba-simplify',
                        'label'         => __( 'Separator Size', 'bb-njba' ),
                        'default'       => array(
                            'desktop'       => '28',
                            'medium'        => '24',
                            'small'         => '20',
                        ),
                        'description'   => _x( 'px', 'Value unit for font size. Such as: "14 px"', 'bb-njba' ),
                    ),
                    'separator_icon_color' => array(
                        'type' => 'color',
                        'label' => __('Separator Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'icon_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Separator Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 0,
                            'right'         => 0,
                            'bottom'       => 0,
                            'left'      => 0
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'icon_margin'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Separator Margin', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 0,
                            'right'         => 0,
                            'bottom'       => 0,
                            'left'      => 0
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    )
                )
            ),
        ),
    ),
));