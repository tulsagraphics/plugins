<?php
FLBuilderModel::default_settings($settings, array(
	'post_type' 			=> 'post',
	'order_by'  			=> 'date',
	'order'     			=> 'DESC',
	'offset'    			=> 0,
	'no_results_message'	=> __('No result found.', 'bb-njba'),
	'users'     			=> '',
	'show_author'			=> '1',
	'show_date'				=> '1',
	'date_format'			=> 'default',
	'show_post_taxonomies'	=> '1',
	'post_taxonomies'		=> 'category',
	'meta_separator'		=> ' / ',
	'title_margin'			=> array(
		'top'					=> '0',
		'bottom'				=> '0'
	)
));
?>
<?php  if($settings->post_grid_style_select == "style-1"){ ?>
			<div class="njba-blog-posts-col-12 njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>" itemscope itemtype="<?php NJBAContentListModule::schema_itemtype(); ?>">
				<?php NJBAContentListModule::schema_meta(); ?>
				<div class="njba-content-grid">
					<?php  $module->post_image_render();?>  
					 <div class="njba-content-grid-contant">
				       	<div class="njba-content-grid-contant-sub">
				           	<div class="njba-content-grid-vertical-center">
				               	<<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
				               	<?php
					                   	$module->content_meta(get_the_ID());
					                   	$module->excerpt_text();
					                   	$module->button_render();
								?>
				            </div>
				         </div>
				    </div>
				</div>
			</div>
			<div class="njba-blog-separator"><span></span></div>
<?php  } ?>
<?php  if($settings->post_grid_style_select == "style-2"){ ?>
			<div class="njba-blog-posts-col-12 njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>" itemscope itemtype="<?php NJBAContentListModule::schema_itemtype(); ?>">
				<?php NJBAContentListModule::schema_meta(); ?>
				<div class="njba-content-grid">
					<div class="njba-content-grid-contant">
				        <div class="njba-content-grid-contant-sub">
				            <div class="njba-content-grid-vertical-center">
				                <<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
				                 <?php
					                   	$module->content_meta(get_the_ID());
					                   	$module->excerpt_text();
					                   	$module->button_render();
								?>
				            </div>
				        </div>
				    </div>
				    <?php  $module->post_image_render();?>  
				</div>
			</div>
			<div class="njba-blog-separator"><span></span></div>
<?php  } ?>
 			 
<?php  if($settings->post_grid_style_select == "style-3"){ ?>
			<div class="njba-blog-posts-col-12 njba-post-wrapper njba-<?php echo $settings->post_grid_style_select; ?>" itemscope itemtype="<?php NJBAContentListModule::schema_itemtype(); ?>">
				<?php NJBAContentListModule::schema_meta(); ?>
				<div class="njba-content-grid">
					<div class="njba-content-grid-contant">
				        <div class="njba-content-grid-contant-sub">
				            <div class="njba-content-grid-vertical-center">
				                <<?php echo $settings->post_title_tag; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></<?php echo $settings->post_title_tag; ?>>
				                <?php
					                $module->content_meta(get_the_ID());
					                $module->post_image_render();
					                $module->excerpt_text();
					                $module->button_render();
				                ?>
				            </div>
				        </div>
				    </div>
				</div>
			</div>
			<div class="njba-blog-separator"><span></span></div>
<?php  } ?>	
				
            
      				