<?php
// Get the query data.
$query = FLBuilderLoop::query($settings);
            
    
// Render the posts.
if($query->have_posts()) :
?>
<div class="njba-blog-posts-list" itemscope="itemscope" itemtype="http://schema.org/Blog">
	<div class="njba-blog-row">
		<div class="njba-blog-posts-wrapper">
		<?php
		$i=1;
		while($query->have_posts()) {
			$query->the_post();
			include 'post-grid.php';
			
			$i++;
		}
		?>
		</div>
	</div>
</div>
<?php endif; ?>
<?php
	if($settings->pagination != 'none' && $query->have_posts()) :
	?>
	<div class="njba-pagination"<?php if($settings->pagination == 'scroll') echo ' style="display:none;"'; ?>>
		<?php FLBuilderLoop::pagination($query); ?>
	</div>
	<?php endif; ?>
	<?php
	// Render the empty message.
	if(!$query->have_posts() && (defined('DOING_AJAX') || isset($_REQUEST['fl_builder']))) :
	?>
	<div class="njba--post-grid-empty">
		<?php 
		if (isset($settings->no_results_message)) :
			echo $settings->no_results_message;
		else :
			_e( 'No posts found.', 'fl-builder' );
		endif; 
		?>
	</div>
		
	<?php
	endif;
wp_reset_postdata();
?>
