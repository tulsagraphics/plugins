<?php
/**
 * @class NJBABeforeaftersliderModule
 */
class NJBABeforeaftersliderModule extends FLBuilderModule {
    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Before After Slider', 'bb-njba'),
            'description'   => __('An animated before after slider area.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'creative' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-after-before-slider/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-after-before-slider/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
            'partial_refresh' => true, // Set this to true to enable partial refresh.
            'icon'              => 'slides.svg',
        ));
        /**
         * Use these methods to enqueue css and js already
         * registered or to register and enqueue your own.
         */
        // Already registered
		$this->add_css('font-awesome');
        $this->add_css('njba-slider', $this->url . 'css/slider.css');
		
		$this->add_css('njba-slider-frontend', NJBA_MODULE_URL . 'modules/njba-after-before-slider/css/frontend.css');
        $this->add_js('njba-event-move', $this->url ."js/jquery.movement.js", array(), '', true);
        $this->add_js('njba-jquery-slider', $this->url ."js/jquery.slider.js", array(), '',true);
		
    }
    /**
     * Use this method to work with settings data before
     * it is saved. You must return the settings object.
     *
     * @method update
     * @param $settings {object}
     */
    public function update($settings)
    {
        return $settings;
    }
    /**
     * This method will be called by the builder
     * right before the module is deleted.
     *
     * @method delete
     */
    public function delete()
    {
    }
}
/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('NJBABeforeaftersliderModule', array(
    'general'      => array( // Tab
        'title'         => __('General', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('Before', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    'before_image'  => array(
                        'type'          => 'select',
                        'label'         => __('Select Before Image', 'bb-njba'),
                        'default'       => 'library',
                        'help'          => __('Image dimensions should be same to work slider properly','bb-njba'),
                        'options'       => array(
                            'library'       => __('Media Library', 'bb-njba'),
                            'url'           => __('URL', 'bb-njba')
                        ),
                        'toggle'        => array(
                            'library'       => array(
                                'fields'        => array('before_photo')
                            ),
                            'url'           => array(
                                'fields'        => array('before_photo_url' )
                            )
                        )
                    ),
                    'before_photo'         => array(
                        'type'          => 'photo',
                        'label'         => __('Photo', 'bb-njba'),
                        'show_remove'   => true,
                    ),
                    'before_photo_url'     => array(
                        'type'          => 'text',
                        'label'         => __('Photo URL', 'bb-njba'),
                        'placeholder'   => 'http://www.example.com/my-photo.jpg',
                    ),
                    'before_label_text'     => array(
                        'type'          => 'text',
                        'label'         => __('Before Label Text', 'bb-njba'),
                        'placeholder'   => __( 'Before', 'bb-njba' )
                    ),
                )
            ),
            'general_after'       => array( // Section
                'title'         => __('After', 'bb-njba'), // Section Title
                'fields'        => array( // Section Fields
                    // After Image
                    'after_image'  => array(
                        'type'          => 'select',
                        'label'         => __('Select After Image', 'bb-njba'),
                        'default'       => 'library',
                        'help'          => __('Image dimensions should be same to work slider properly','bb-njba'),
                        'options'       => array(
                            'library'       => __('Media Library', 'bb-njba'),
                            'url'           => __('URL', 'bb-njba')
                        ),
                        'toggle'        => array(
                            'library'       => array(
                                'fields'        => array('after_photo')
                            ),
                            'url'           => array(
                                'fields'        => array('after_photo_url' )
                            )
                        )
                    ),
                    'after_photo'         => array(
                        'type'          => 'photo',
                        'label'         => __('Photo', 'bb-njba'),
                        'show_remove'   => true,
                    ),
                    'after_photo_url'     => array(
                        'type'          => 'text',
                        'label'         => __('Photo URL', 'bb-njba'),
                        'placeholder'   => 'http://www.example.com/my-photo.jpg',
                    ),
                    'after_label_text'     => array(
                        'type'          => 'text',
                        'label'         => __('After Label Text', 'bb-njba'),
                        'placeholder'   => __( 'After', 'bb-njba' )
                    ),
                )
            ),
        )
    ),
     'style'      => array( // Tab
        'title'         => __('Style', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'style'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array( // Section Fields
                    'before_after_style'       => array(
                        'type'          => 'select',
                        'label'         => __('Before After Style', 'bb-njba'),
                        'default'       => 'horizontal',
                        'class'         => '',
                        'options'       => array(
                            'horizontal'             => __('Horizontal', 'bb-njba'),
                            'vertical'             => __('Vertical', 'bb-njba'),
                        ),
                        'toggle'       => array(
                            'horizontal' => array(
                                'fields' => array( 'slider_label_position' )
                            ),
                            'vertical' => array(
                                'fields' => array( 'slider_vertical_label_position' )
                            )
                        )
                    ),
                    'move_on_hover'   => array(
                        'type'          => 'select',
                        'label'         => __('Move on Hover', 'bb-njba'),
                        'description'   => '',
                        'default'       => 'false',
                        'options'       => array(
                            'true'         => __('Yes','bb-njba'),
                            'false'          => __('No','bb-njba'),
                        ),
                        'toggle' => array(
                            'false' => array(
                                'fields' => array( 'handle_back_overlay', 'handle_back_overlay_opc' )
                            )
                        )
                    ),
                    'initial_offset'     => array(
                        'type'          => 'select',
                        'label'         => __('Handle Initial Offset', 'bb-njba'),
                        'default'       => '0.5',
                        'options'       => array(
                            '0.1'             => __('0.1', 'bb-njba'),
                            '0.2'             => __('0.2', 'bb-njba'),
                            '0.3'             => __('0.3', 'bb-njba'),
                            '0.4'             => __('0.4', 'bb-njba'),
                            '0.5'             => __('0.5', 'bb-njba'),
                            '0.6'             => __('0.6', 'bb-njba'),
                            '0.7'             => __('0.7', 'bb-njba'),
                            '0.8'             => __('0.8', 'bb-njba'),
                            '0.9'             => __('0.9', 'bb-njba'),
                        ),
                    ),
                    'handle_color' => array( 
                        'type'       => 'color',
                        'label'      => __('Handle Color', 'fl-builder'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    'handle_thickness'     => array(
                        'type'          => 'text',
                        'size'          => 8,
                        'label'         => __('Handle Thickness', 'bb-njba'),
                        'placeholder'   => '5',
                        'description'   => "px"
                    ),
                    'handle_back_overlay' => array( 
                        'type'       => 'color',
                        'label'      => __('Slider Overlay Color', 'fl-builder'),
                        'default'    => '000000',
                        'show_reset' => true,
                        'help'       => __('The slider overlay color will be displayed on mouse hover.', 'bb-njba'),
                    ),
                    'handle_back_overlay_opc'     => array(
                        'type'        => 'text',
                        'label'       => __('Overlay Color Opacity', 'bb-njba'),
                        'default'     => '50',
                        'description' => '%',
                        'maxlength'   => '3',
                        'size'        => '5',
                    ),
                    'advance_opt'   => array(
                        'type'          => 'select',
                        'label'         => __('Display Advance Options', 'bb-njba'),
                        'description'   => '',
                        'default'       => '',
                        'options'       => array(
                            'Y'         => __('Yes','bb-njba'),
                            ''          => __('No','bb-njba'),
                        ),
                        'toggle'       => array(
                            'Y' => array(
                                'fields' => array( 'handle_circle_width', 'shadow_opt', 'handle_triangle_color', 'handle_triangle_size', 'handle_radius' )
                            )
                        )
                    ),
                    'handle_circle_width'     => array(
                        'type'          => 'text',
                        'size'          => '2',
                        'label'         => __('Circle Width', 'bb-njba'),
                        'placeholder'   => '40',
                        'description'   => 'px'
                    ),
                    'handle_radius'     => array(
                        'type'          => 'text',
                        'size'          => '2',
                        'label'         => __('Circle Radius', 'bb-njba'),
                        'placeholder'   => '100',
                        'description'   => 'px'
                    ),
                    'handle_triangle_size'     => array(
                        'type'          => 'text',
                        'size'          => '2',
                        'label'         => __('Triangle Size', 'bb-njba'),
                        'placeholder'   => __( '6', 'bb-njba' ),
                        'description'   => 'px'
                    ),
                    'handle_triangle_color' => array( 
                        'type'       => 'color',
                        'label' => __('Triangle Color', 'fl-builder'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    'shadow_opt'   => array(
                        'type'          => 'select',
                        'label'         => __('Handle Shadow', 'bb-njba'),
                        'description'   => '',
                        'default'       => '',
                        'options'       => array(
                            'Y'         => __('Yes','bb-njba'),
                            ''          => __('No','bb-njba'),
                        ),
                        'toggle'       => array(
                            'Y' => array(
                                'fields' => array( 'handle_shadow', 'handle_shadow_color' )
                            )
                        )
                    ),
                    'handle_shadow'     => array(
                        'type'          => 'text',
                        'size'          => '2',
                        'label'         => __('Handle Shadow Size', 'bb-njba'),
                        'placeholder'   => '5',
                        'description'   => 'px'
                    ),
                    'handle_shadow_color' => array( 
                        'type'       => 'color',
                        'label' => __('Handle Shadow Color', 'fl-builder'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                )
            ),
        )
    ),
    'label'       => array( // Tab
        'title'         => __('Label', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'label_style'    =>  array(
                'title'     => __('Label Style', 'bb-njba' ),
                'fields'    => array(
                    'label_color'        => array( 
                        'type'       => 'color',
                        'label' => __('Label Color', 'bb-njba'),
                        'preview'         => array(
                            'type'            => 'css',
                            'selector'        => '',
                            'property'        => 'color'
                        ),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    'label_back_color' => array( 
                        'type'       => 'color',
                        'label' => __('Label Background Color', 'fl-builder'),
                        'default'    => '',
                        'show_reset' => true,
                    ),
                    'slider_label_position'       => array(
                        'type'          => 'select',
                        'label'         => __('Label Position', 'bb-njba'),
                        'default'       => 'center',
                        'class'         => '',
                        'options'       => array(
                            'top'             => __('Top', 'bb-njba'),
                            'center'             => __('Center', 'bb-njba'),
                            'bottom'             => __('Bottom', 'bb-njba'),
                        ),
                    ),
                    'slider_vertical_label_position'       => array(
                        'type'          => 'select',
                        'label'         => __('Label Position', 'bb-njba'),
                        'default'       => 'center',
                        'class'         => '',
                        'options'       => array(
                            'left'             => __('Left', 'bb-njba'),
                            'center'             => __('Center', 'bb-njba'),
                            'right'             => __('Right', 'bb-njba'),
                        ),
                    ),
                    'slider_label_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Label Padding', 'bb-njba'),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up',
                                'default'           => '40',
                                'description'       => 'px',
                                'preview'           => array(
                                    'selector'          => '',
                                    'property'          => 'padding-top',
                                ),
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down',
                                'default'           => '40',
                                'description'       => 'px',
                                'preview'           => array(
                                    'selector'          => '',
                                    'property'          => 'padding-bottom',
                                ),
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left',
                                'default'           => '40',
                                'description'       => 'px',
                                'preview'           => array(
                                    'selector'          => '',
                                    'property'          => 'padding-left',
                                ),
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right',
                                'default'           => '40',
                                'description'       => 'px',
                                'preview'           => array(
                                    'selector'          => '',
                                    'property'          => 'padding-right',
                                ),
                            )
                        )
                    ),
                    'slider_label_letter_spacing'     => array(
                        'type'          => 'text',
                        'size'          => '2',
                        'label'         => __('Label Letter Spacing', 'bb-njba'),
                        'placeholder'   => '1',
                        'description'   => 'px'
                    ),
                     'border_style'      => array(
                        'type'      => 'select',
                        'label'     => __('Border Style', 'bb-njba'),
                        'default'   => 'solid',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'double'  => __('Double', 'bb-njba'),
                        ),
                    ),
                    'label_border'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Border', 'bb-njba'),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up',
                                'default'           => '40',
                                'description'       => 'px',
                                'preview'           => array(
                                    'selector'          => '',
                                    'property'          => '',
                                ),
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down',
                                'default'           => '40',
                                'description'       => 'px',
                                'preview'           => array(
                                    'selector'          => '',
                                    'property'          => '',
                                ),
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left',
                                'default'           => '40',
                                'description'       => 'px',
                                'preview'           => array(
                                    'selector'          => '',
                                    'property'          => '',
                                ),
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right',
                                'default'           => '40',
                                'description'       => 'px',
                                'preview'           => array(
                                    'selector'          => '',
                                    'property'          => '',
                                ),
                            )
                        )
                    ),
                     'border_color'    => array( 
                        'type'       => 'color',
                        'label'         => __('Border Color', 'bb-njba'),
                        'default'       => 'cccccc',
                        'show_reset' => true,
                    ),
            
                    'border_radius'    => array(
                        'type'          => 'text',
                        'default'       => '0',
                        'maxlength'     => '3',
                        'size'          => '5',
                        'label'         => __('Round Corners', 'bb-njba'),
                        'description'   => _x( 'px', 'Value unit for border radius. Such as: "5 px"', 'bb-njba' ),
                        'preview'       => array(
                            'type'          => 'css',
                            'rules'     => array(
                                array(
                                    'selector'      => '',
                                    'property'      => 'border-radius',
                                    'unit'          => 'px'
                                ),                              
                            ),
                        )
                    ),
                )
            ),
        )
    ),
    'typography'       => array( // Tab
        'title'         => __('Typography', 'bb-njba'), // Tab title
        'sections'      => array( // Tab Sections
            'label_typography'    =>  array(
                'title'     => __('Label Typography', 'bb-njba' ),
                'fields'    => array(
                    'slider_font_family'       => array(
                        'type'          => 'font',
                        'label'         => __('Font Family', 'bb-njba'),
                        'default'       => array(
                            'family'        => 'Default',
                            'weight'        => 'Default'
                        ),
                        'preview'         => array(
                            'type'            => 'font',
                            'selector'        => ''
                        )
                    ),
                    'slider_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        )
                    ),
                    'slider_line_height'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Line Height', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '',
                            'medium' => '',
                            'small' => ''
                        )
                    ),
                    'label_text_transform'    => array(
                        'type'                      => 'select',
                        'label'                     => __('Text Transform', 'bb-njba'),
                        'default'                   => 'none',
                        'options'                   => array(
                            'none'                  => __('Default', 'bb-njba'),
                            'lowercase'                => __('lowercase', 'bb-njba'),
                            'uppercase'                 => __('UPPERCASE', 'bb-njba'),
                        )
                    ),
                   
                )
            ),
        )
    )
));
