<?php $settings->handle_back_overlay_opc = ( $settings->handle_back_overlay_opc != '' ) ? $settings->handle_back_overlay_opc : '100'; ?>
<?php if ( isset( $settings->before_label_text ) && $settings->before_label_text != "" ) {?>
	.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-before-label:before
	{
	    content: "<?php echo $settings->before_label_text;?>"; 
	}
<?php } ?>
<?php if ( isset( $settings->after_label_text ) && $settings->after_label_text != "" ) {?>
	.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-after-label:before
	{
	    content: "<?php echo $settings->after_label_text;?>"; 
	}
<?php } ?>
.fl-node-<?php echo $id;?> .baslider-<?php echo $module->node ;?> .slider-before-label:before,
.fl-node-<?php echo $id;?> .baslider-<?php echo $module->node ;?> .slider-after-label:before{
	<?php if( $settings->slider_font_family['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->slider_font_family ); ?><?php } ?>
	<?php if( $settings->slider_font_size['desktop'] ) { ?> font-size: <?php echo $settings->slider_font_size['desktop']; ?>px; <?php } ?>
	<?php if( $settings->slider_line_height['desktop'] != '' ) : ?>
	line-height: <?php echo $settings->slider_line_height['desktop']; ?>px;
	<?php endif; ?>	
	text-transform: <?php echo $settings->label_text_transform; ?>;
}
.fl-node-<?php echo $id;?> .baslider-<?php echo $module->node ;?> .slider-before-label:before,
.fl-node-<?php echo $id;?> .baslider-<?php echo $module->node ;?> .slider-after-label:before {
	<?php if( isset( $settings->label_color ) && $settings->label_color != '' ) : ?>
		color: #<?php echo $settings->label_color; ?>;
	<?php endif; ?>
	<?php if( isset( $settings->label_back_color ) && $settings->label_back_color != '' ) : ?>
		background: #<?php echo $settings->label_back_color; ?>;
	<?php endif; ?>
	 <?php if( !empty($settings->slider_label_padding['top'] ) ) { ?> padding-top: <?php echo $settings->slider_label_padding['top']; ?>px;<?php } ?>
	<?php if( !empty($settings->slider_label_padding['bottom'] ) ) { ?>padding-bottom: <?php echo $settings->slider_label_padding['bottom']; ?>px; <?php } ?>
	<?php if( !empty($settings->slider_label_padding['left'] ) ) { ?>padding-left: <?php echo $settings->slider_label_padding['left']; ?>px; <?php } ?>
	<?php if( !empty($settings->slider_label_padding['right'] ) ) { ?>padding-right: <?php echo $settings->slider_label_padding['right']; ?>px; <?php } ?>
	<?php if( isset( $settings->slider_label_letter_spacing ) && $settings->slider_label_letter_spacing != '' ) : ?>
		letter-spacing: <?php echo $settings->slider_label_letter_spacing; ?>px;
	<?php endif; ?>
}
.fl-node-<?php echo $id;?> .baslider-<?php echo $module->node ;?> .slider-before-label,
.fl-node-<?php echo $id;?> .baslider-<?php echo $module->node ;?> .slider-after-label {
	<?php if( $settings->before_after_style == "horizontal" ):
			if( isset( $settings->slider_label_position ) && $settings->slider_label_position == 'center' ) : ?>
			-webkit-box-align:center;
	    	-ms-flex-align:center;
	        -ms-grid-row-align:center;
	        align-items:center;
		<?php endif; ?>
		<?php if( isset( $settings->slider_label_position ) && $settings->slider_label_position == 'top' ) : ?>
			-webkit-box-align:start;
    		-ms-flex-align:start;
            -ms-grid-row-align:flex-start;
	        align-items:flex-start;
			margin-top:10px;
		<?php endif; ?>
		<?php if( isset( $settings->slider_label_position ) && $settings->slider_label_position == 'bottom' ) : ?>
			-webkit-box-align:end;
	    	-ms-flex-align:end;
	        -ms-grid-row-align:flex-end;
	        align-items:flex-end;
			padding-bottom:10px;
		<?php endif; ?>
	<?php endif; ?>
	<?php if( $settings->before_after_style == "vertical" ):
			if( isset( $settings->slider_vertical_label_position ) && $settings->slider_vertical_label_position == 'center' ) : ?>
			justify-content:center;
		<?php endif; ?>
		<?php if( isset( $settings->slider_vertical_label_position ) && $settings->slider_vertical_label_position == 'left' ) : ?>
			justify-content:flex-start;
			padding-left:10px;
		<?php endif; ?>
		<?php if( isset( $settings->slider_vertical_label_position ) && $settings->slider_vertical_label_position == 'right' ) : ?>
			justify-content:flex-end;
			padding-right:10px;
		<?php endif; ?>
	<?php endif; ?>
}
.fl-node-<?php echo $id;?> .slider-before-label::before,
.fl-node-<?php echo $id;?> .slider-after-label::before {
  	<?php if( !empty($settings->label_border['top'] ) ) { ?> border-top: <?php echo $settings->label_border['top']; ?>px <?php echo $settings->border_style; ?> #<?php echo $settings->border_color; ?>;  <?php } ?>
	<?php if( !empty($settings->label_border['bottom'] ) ) { ?>border-bottom: <?php echo $settings->label_border['bottom']; ?>px <?php echo $settings->border_style; ?> #<?php echo $settings->border_color; ?>; <?php } ?>
	<?php if( !empty($settings->label_border['left'] ) ) { ?>border-left: <?php echo $settings->label_border['left']; ?>px <?php echo $settings->border_style; ?> #<?php echo $settings->border_color; ?>; <?php } ?>
	<?php if( !empty($settings->label_border['right'] )) { ?>border-right: <?php echo $settings->label_border['right']; ?>px <?php echo $settings->border_style; ?> #<?php echo $settings->border_color; ?>;  <?php } ?>
	<?php if( $settings->border_radius != null ) { ?>border-radius: <?php echo $settings->border_radius; ?>px;<?php } ?>
}
.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-handle:before,
.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-handle:after {
	background: #<?php echo $settings->handle_color;?>;
	<?php if ( $settings->before_after_style == "vertical" && $settings->handle_thickness != "" ) { ?>
	height: <?php echo $settings->handle_thickness; ?>px;
	margin-top: -<?php echo $settings->handle_thickness/2; ?>px;
	<?php } ?>
	<?php if ( $settings->before_after_style == "horizontal" && $settings->handle_thickness != "" ) { ?>
	width: <?php echo $settings->handle_thickness; ?>px;
	margin-left: -<?php echo $settings->handle_thickness/2; ?>px;
	<?php } ?>
}
<?php if ( $settings->handle_thickness == "" ) { $settings->handle_thickness = 5; } ?>
<?php if ( $settings->handle_circle_width == "" || $settings->advance_opt == '' ) { $settings->handle_circle_width = 40; } ?>
.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-handle {
	border-color: #<?php echo $settings->handle_color;?>;
	<?php if ( $settings->handle_thickness != "" ) { ?>
	
	border-width:<?php echo $settings->handle_thickness; ?>px;
	<?php } ?>
	
	<?php 
	if ( $settings->advance_opt == 'Y' && isset( $settings->handle_radius ) && $settings->handle_radius == "" ){
		$settings->handle_radius = 100;
	}
	if ( $settings->advance_opt == 'Y' && $settings->handle_radius != "" ) { ?>
	border-radius: <?php echo $settings->handle_radius; ?>px;
	<?php } ?>
	<?php if ( $settings->advance_opt == 'Y' && $settings->shadow_opt == "Y" ) {
		if ( $settings->handle_shadow == "" ) {
			$settings->handle_shadow = 5;
		}
		if ( $settings->handle_shadow_color == "" ) {
			$settings->handle_shadow_color = "#fcfcfc";
		}
	?>
		webkit-box-shadow: 0px 0px <?php echo $settings->handle_shadow;?>px #<?php echo $settings->handle_shadow_color;?>;
    	-moz-box-shadow: 0px 0px <?php echo $settings->handle_shadow;?>px #<?php echo $settings->handle_shadow_color;?>;
    	box-shadow: 0px 0px <?php echo $settings->handle_shadow;?>px #<?php echo $settings->handle_shadow_color;?>;
	<?php } ?>
	<?php if ( $settings->handle_circle_width != "" ) { ?>
	height: <?php echo $settings->handle_circle_width; ?>px;
	width: <?php echo $settings->handle_circle_width; ?>px;
	margin-left:-<?php echo $settings->handle_circle_width/2; ?>px;
	margin-top:-<?php echo $settings->handle_circle_width/2; ?>px;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-handle:before{
	<?php if ( $settings->before_after_style == "vertical" && $settings->handle_thickness != "" ) { ?>
	<?php if ( $settings->handle_circle_width != 45 ) { ?>
	margin-left: <?php echo $settings->handle_circle_width / 2; ?>px;
	<?php }else{ ?>
	margin-left: <?php echo $settings->handle_thickness + 10; ?>px;
	<?php } ?>
	box-shadow: 0 0 0 #<?php echo $settings->handle_color;?>;
	<?php } ?>
	<?php if ( $settings->before_after_style == "horizontal" && $settings->handle_thickness != "" ) { ?>
	<?php if ( $settings->handle_circle_width != 45 ) { ?>
	margin-bottom: <?php echo $settings->handle_circle_width / 2; ?>px;
	<?php }else{ ?>
	margin-bottom: <?php echo $settings->handle_thickness + 10; ?>px;
	<?php } ?>
	box-shadow: 0 0 0 #<?php echo $settings->handle_color;?>;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-handle:after{	
	<?php if ( $settings->before_after_style == "vertical" && $settings->handle_thickness != "" ) { ?>
	<?php if ( $settings->handle_circle_width != 45 ) { ?>
	margin-right: <?php echo $settings->handle_circle_width / 2; ?>px;
	<?php } else { ?>
	margin-right: <?php echo $settings->handle_thickness + 10; ?>px;
	<?php } ?>
	box-shadow: 0 0 0 #<?php echo $settings->handle_color;?>;
	<?php } ?>
	<?php if ( $settings->before_after_style == "horizontal" && $settings->handle_thickness != "" ) { ?>
	<?php if ( $settings->handle_circle_width != 45 ) { ?>
	margin-top: <?php echo $settings->handle_circle_width / 2; ?>px;
	<?php }else{ ?>
	margin-top: <?php echo $settings->handle_thickness + 10; ?>px;
	<?php } ?>
	box-shadow: 0 0 0 #<?php echo $settings->handle_color;?>;
	<?php } ?>
}
<?php if( $settings->move_on_hover == 'false' ) { ?>
.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-overlay:hover {
	<?php if( $settings->handle_back_overlay ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->handle_back_overlay )) ?>, <?php echo $settings->handle_back_overlay_opc/100; ?>); <?php } ?>
}
<?php } ?>
<?php if ( $settings->advance_opt == 'Y' && $settings->before_after_style == "vertical" ){ 
	if ( $settings->handle_triangle_size != "" ) { $triangle_size = $settings->handle_triangle_size; }else{ $triangle_size = 6; }
?>
.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-handle .slider-up-arrow {
	border-bottom: <?php echo $triangle_size;?>px solid #<?php echo $settings->handle_triangle_color;?>;
}
.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-handle .slider-down-arrow {
	border-top: <?php echo $triangle_size;?>px solid #<?php echo $settings->handle_triangle_color;?>;
}
<?php }
if ( $settings->advance_opt == 'Y' && $settings->before_after_style == "horizontal" ){ 
	if ( $settings->handle_triangle_size != "" ) { $triangle_size = $settings->handle_triangle_size; }else{ $triangle_size = 6; }
?>
.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-handle .slider-left-arrow {
	border-right: <?php echo $triangle_size;?>px solid #<?php echo $settings->handle_triangle_color;?>;
}
.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-handle .slider-right-arrow {
	border-left: <?php echo $triangle_size;?>px solid #<?php echo $settings->handle_triangle_color;?>;
}
<?php }
if( $settings->advance_opt == 'Y' ) {
?>
.fl-node-<?php echo $id; ?> .slider-left-arrow,
.fl-node-<?php echo $id; ?> .slider-right-arrow,
.fl-node-<?php echo $id; ?> .slider-up-arrow,
.fl-node-<?php echo $id; ?> .slider-down-arrow {
	border: <?php echo $triangle_size; ?>px inset transparent;
}
<?php
}
if ( $settings->advance_opt == '' && $settings->before_after_style == "vertical" ){ 
	$triangle_size = 6;
?>
.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-handle .slider-up-arrow {
	border-bottom: <?php echo $triangle_size;?>px solid #<?php echo $settings->handle_color;?>;
}
.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-handle .slider-down-arrow {
	border-top: <?php echo $triangle_size;?>px solid #<?php echo $settings->handle_color;?>;
}
<?php }
if ( $settings->advance_opt == '' && $settings->before_after_style == "horizontal" ){ 
	$triangle_size = 6;
?>
.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-handle .slider-left-arrow {
	border-right: <?php echo $triangle_size;?>px solid #<?php echo $settings->handle_color;?>;
}
.fl-node-<?php echo $id; ?> .baslider-<?php echo $module->node ;?> .slider-handle .slider-right-arrow {
	border-left: <?php echo $triangle_size;?>px solid #<?php echo $settings->handle_color;?>;
}
<?php } ?>
@media ( max-width: 991px ) {
.fl-node-<?php echo $id;?> .baslider-<?php echo $module->node ;?> .slider-before-label:before,
.fl-node-<?php echo $id;?> .baslider-<?php echo $module->node ;?> .slider-after-label:before{
	<?php if( $settings->slider_font_size['medium'] ) { ?> font-size: <?php echo $settings->slider_font_size['medium']; ?>px; <?php } ?>
	<?php if( $settings->slider_line_height['medium'] != '' ) : ?>
	line-height: <?php echo $settings->slider_line_height['medium']; ?>px;
	<?php endif; ?>	
}
}
@media ( max-width: 767px ) {
.fl-node-<?php echo $id;?> .baslider-<?php echo $module->node ;?> .slider-before-label:before,
.fl-node-<?php echo $id;?> .baslider-<?php echo $module->node ;?> .slider-after-label:before{
	<?php if( $settings->slider_font_size['small'] ) { ?> font-size: <?php echo $settings->slider_font_size['small']; ?>px; <?php } ?>
	<?php if( $settings->slider_line_height['small'] != '' ) : ?>
	line-height: <?php echo $settings->slider_line_height['small']; ?>px;
	<?php endif; ?>	
}	     
}
