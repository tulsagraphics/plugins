<div class="njba-module-content njba-before-after-slider">
  <div class="njba-module-content before-after-slider">
    <div class="njba-slider-container baslider-<?php echo $module->node; ?> njba-label-position-<?php echo ( $settings->before_after_style != "vertical" ) ? $settings->slider_label_position : $settings->slider_vertical_label_position; ?> <?php echo ( $settings->move_on_hover == 'true' ) ? 'njba-move-on-hover' : ''; ?>" <?php if( isset( $settings->before_after_style ) && $settings->before_after_style == "vertical" ){ echo "data-orientation='vertical'"; } ?>>
      <?php if( $settings->before_image == 'url' ) { ?>
      <?php if( isset( $settings->before_photo_url ) && $settings->before_photo_url != "" ){ ?>
      <img class="njba-before-img" src="<?php echo $settings->before_photo_url;?>"/>
      <?php } ?>
      <?php } else { ?>
      <?php if( isset( $settings->before_photo_src ) && $settings->before_photo_src != "" ){ ?>
      <img class="njba-before-img" src="<?php echo $settings->before_photo_src;?>"/>
      <?php } ?>
      <?php } ?>
      <?php if( $settings->after_image == 'url' ) { ?>
      <?php if( isset( $settings->after_photo_url ) && $settings->after_photo_url != "" ){ ?>
      <img class="njba-before-img" src="<?php echo $settings->after_photo_url;?>"/>
      <?php } ?>
      <?php } else { ?>
      <?php if( isset( $settings->after_photo_src ) && $settings->after_photo_src != "" ){ ?>
      <img class="njba-before-img" src="<?php echo $settings->after_photo_src;?>"/>
      <?php } ?>
      <?php } ?>
    </div>
  </div>
</div>
