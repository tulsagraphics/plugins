(function($) {
	var document_width, document_height;
	var args = {
	        id : '<?php echo $id; ?>',
	        before_after_style : '<?php echo $settings->before_after_style; ?>',
	        initial_offset : '<?php echo $settings->initial_offset; ?>',
	        move_on_hover : '<?php echo $settings->move_on_hover; ?>',
		};
	jQuery(document).ready(function($){
		document_width = jQuery( document ).width();
		document_height = jQuery( document ).height();
		setTimeout(function(){
			new NJBABeforeAfterSlider( args );
			jQuery(window).trigger('resize');
		}, 300);
		
	});
	jQuery(document).load(function() {
		new NJBABeforeAfterSlider( args );
		jQuery(window).trigger('resize');
	});
	jQuery(window).resize(function(){
		if( document_width != jQuery( document ).width() || document_height != jQuery( document ).height() ) {
			document_width = jQuery( document ).width();
			document_height = jQuery( document ).height();
			
			jQuery( ".baslider-<?php echo $id; ?>" ).css( 'width', '' );
			jQuery( ".baslider-<?php echo $id; ?>" ).css( 'height', '' );
			max = -1;
			jQuery( ".baslider-<?php echo $id; ?> img" ).each(function() {
				if( max < jQuery(this).width() ) {
					max = jQuery(this).width();
				}
			});
			jQuery( ".baslider-<?php echo $id; ?>" ).css( 'width', max + 'px' );
		}
	});
	new NJBABeforeAfterSlider( args );
	jQuery(window).trigger('resize');
})( jQuery );