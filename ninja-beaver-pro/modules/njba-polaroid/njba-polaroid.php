<?php
/**
 * @class NJBATestimonialsModule
 */
class NJBAPolaroidModule extends FLBuilderModule {
    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Polaroid', 'bb-njba'),
            'description'   => __('Addon to display Polaroid box.', 'bb-njba'),
            'group'         => njba_get_modules_group(),
            'category'      => njba_get_modules_cat( 'creative' ),
            'dir'           => NJBA_MODULE_DIR . 'modules/njba-polaroid/',
            'url'           => NJBA_MODULE_URL . 'modules/njba-polaroid/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
            'icon'              => 'format-image.svg',
            'partial_refresh' => true, // Set this to true to enable partial refresh.
        ));
        /**
         * Use these methods to enqueue css and js already
         * registered or to register and enqueue your own.
         */
        // Already registered
        
		$this->add_css('font-awesome');
		$this->add_css('njba-polaroid-frontend', NJBA_MODULE_URL . 'modules/njba-polaroid/css/frontend.css');
		
    }
	
}
/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('NJBAPolaroidModule', array(
	'general'       => array( // Tab
		'title'         => __('General', 'bb-njba'), // Tab title
		'sections'      => array( // Tab Sections
			'general'       => array( // Section
				'title'         => '', // Section Title
				'fields'        => array( // Section Fields
					'photo'         => array(
						'type'          => 'photo',
						'label'         => __('Photo', 'bb-njba')
					),
                    'caption'       => array(
                        'type'          => 'text',
                        'label'         => __('Caption', 'bb-njba'),
                        'preview'       => array(
                            'type'          => 'text',
                            'selector'      => '.njba-polaroid-caption-selector'
                        )
                    ),
					'align'         => array(
						'type'          => 'select',
						'label'         => __('Alignment', 'bb-njba'),
						'default'       => 'center',
						'options'       => array(
							'left'          => __('Left', 'bb-njba'),
							'center'        => __('Center', 'bb-njba'),
							'right'         => __('Right', 'bb-njba')
						)
					),
					'rotate' => array(
                        'type' => 'text',
                        'label' => __('Rotate','bb-njba'),
                        'default' => -45,
                        'size' => '3',
                        'description' => 'deg'
                    ),
                    'rotate_hover' => array(
                        'type' => 'text',
                        'label' => __('Hover Rotate','bb-njba'),
                        'default' => 0,
                        'size' => '3',
                        'description' => 'deg'
                    ),
                    'scale' => array(
                        'type' => 'text',
                        'label' => __('Scale','bb-njba'),
                        'default' => 1.1,
                        'size' => '2',
                        'description' => ''
                    ),
				)
			),
			'link'          => array(
				'title'         => __('Link', 'bb-njba'),
				'fields'        => array(
					'link_type'     => array(
						'type'          => 'select',
						'label'         => __('Link Type', 'bb-njba'),
						'options'       => array(
							''              => _x( 'None', 'Link type.', 'bb-njba' ),
							'url'           => __('URL', 'bb-njba'),
						),
						'toggle'        => array(
							''              => array(),
							'url'           => array(
								'fields'        => array('link_url', 'link_target')
							)
						),
						'help'          => __('Link type applies to how the image should be linked on click. You can choose a specific URL, the individual photo or a separate page with the photo.', 'bb-njba'),
						
					),
					'link_url'     => array(
						'type'          => 'link',
						'label'         => __('Link URL', 'bb-njba'),
						
					),
					'link_target'   => array(
						'type'          => 'select',
						'label'         => __('Link Target', 'bb-njba'),
						'default'       => '_self',
						'options'       => array(
							'_self'         => __('Same Window', 'bb-njba'),
							'_blank'        => __('New Window', 'bb-njba')
						),
						
					)
				)
			)
		)
	),
	'style_tab' => array(
        'title' => __('Style','bb-njba'),
        'sections' => array(
            'box_style_section' => array(
                'title' => __('Box','bb-njba'),
                'fields' => array(
                	'background_color'    => array( 
						'type'       => 'color',
                        'label'      => __('Background Color', 'bb-njba'),
						'default'    => '',
						'show_reset' => true,
					),
                    'background_color_opacity' => array(
						'type'          => 'text',
						'label'         => __('Background Color Opacity', 'bb-njba'),
						'default'       => '100',
						'description'   => '%',
						'maxlength'     => '3',
						'size'          => '5',
						'placeholder'   => '100'
					),
					'background_hover_color'    => array( 
						'type'       => 'color',
                        'label'      => __('Background Hover Color', 'bb-njba'),
						'default'    => '',
						'show_reset' => true,
					),
                    'background_hover_color_opacity' => array(
						'type'          => 'text',
						'label'         => __('Background Hover Color Opacity', 'bb-njba'),
						'default'       => '100',
						'description'   => '%',
						'maxlength'     => '3',
						'size'          => '5',
						'placeholder'   => '100'
					),
					'box_border_style'      => array(
                        'type'      => 'select',
                        'label'     => __('Border Style', 'bb-njba'),
                        'default'   => 'none',
                        'options'   => array(
                            'none'  => __('None', 'bb-njba'),
                            'solid'  => __('Solid', 'bb-njba'),
                            'dotted'  => __('Dotted', 'bb-njba'),
                            'dashed'  => __('Dashed', 'bb-njba'),
                            'double'  => __('Double', 'bb-njba'),
                        ),
                        'toggle' => array(
                            'solid' => array(
                                'fields' => array('box_border_width','box_border_color','box_border_hover_color')
                            ),
                            'dotted' => array(
                                'fields' => array('box_border_width','box_border_color','box_border_hover_color')
                            ),
                            'dashed' => array(
                                'fields' => array('box_border_width','box_border_color','box_border_hover_color')
                            ),
                            'double' => array(
                                'fields' => array('box_border_width','box_border_color','box_border_hover_color')
                            ),
                        )
                    ),
                    'box_border_width' => array(
                        'type' => 'text',
                        'label' => __('Box Border Width','bb-njba'),
                        'default' => '1',
                        'size' => '5',
                        'description'       => _x( 'px', 'Value unit for spacer width. Such as: "10 px"', 'bb-njba' )
                    ),
                    'box_border_radius'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Box Border Radius', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 0,
                            'right'         => 0,
                            'bottom'       => 0,
                            'left'      => 0
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'box_border_color' => array(
                        'type' => 'color',
                        'label' => __('Box Border Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'box_border_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Box Border Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '000000'
                    ),
                    'box_shadow'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Box Shadow', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'left_right'          => 0,
                            'top_bottom'         => 0,
                            'blur'       => 0,
                            'spread'      => 0
                        ),
                        'options'           => array(
                            'left_right'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa fa-arrows-h'
                            ),
                            'top_bottom'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa fa-arrows-v'
                            ),
                            'blur'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa fa-circle-thin'
                            ),
                            'spread'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa fa-circle'
                            )
                            
                        )
                    ),
                    'box_shadow_color' => array(
                        'type' => 'color',
                        'label' => __('Box Shadow Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff'
                    ),
                    'image_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Padding', 'bb-njba'),
                        'default'           => array(
                            'top'          => 12,
                            'bottom'       => 15,
                            'left'         => 0,
                            'right'        => 15,
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up',
                                'default'           => '40',
                                'description'       => 'px',
                               
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down',
                                'default'           => '40',
                                'description'       => 'px',
                                
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left',
                                'default'           => '40',
                                'description'       => 'px',
                                
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right',
                                'default'           => '40',
                                'description'       => 'px',
                               
                            )
                        )
                    )
                )
            ),
            'caption'       => array(
                'title'         => __('Caption', 'bb-njba'),
                'fields'        => array(
                    
                    'caption_text_color' => array(
                        'type' => 'color',
                        'label' => __('Caption Color','bb-njba'),
                        'show_reset' => true,
                        'default' => '404040'
                    ),
                    'caption_text_hover_color' => array(
                        'type' => 'color',
                        'label' => __('Caption Hover Color','bb-njba'),
                        'show_reset' => true,
                        'default' => 'ffffff'
                    ),
                    'caption_alignment' => array(
                        'type' => 'select',
                        'label' => __('Alignment','bb-njba'),
                        'default' => 'left',
                        'options' => array(
                            'left' => __('Left','bb-njba'),
                            'center' => __('Center','bb-njba'),
                            'right' => __('Right','bb-njba')
                        ),
                         'preview'      => array(
                            'type'         => 'css',
                            'selector'     => '.njba-box-comman h1',
                            'property'     => 'text-align'
                        )   
                    ),
                    'caption_padding'      => array(
                        'type'              => 'njba-multinumber',
                        'label'             => __('Caption Padding', 'bb-njba'),
                        'description'       => 'px',
                        'default'           => array(
                            'top'          => 0,
                            'right'         => 0,
                            'bottom'       => 0,
                            'left'      => 0
                        ),
                        'options'           => array(
                            'top'               => array(
                                'placeholder'       => __('Top', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-up'
                            ),
                            'right'            => array(
                                'placeholder'       => __('Right', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-right'
                            ),
                            'bottom'            => array(
                                'placeholder'       => __('Bottom', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-down'
                            ),
                            'left'            => array(
                                'placeholder'       => __('Left', 'bb-njba'),
                                'icon'              => 'fa-long-arrow-left'
                            )
                            
                        )
                    ),
                    'caption_font_family' => array(
                        'type' => 'font',
                        'label' => __('Caption Font Family','bb-njba'),
                        'default' => array(
                            'family' => 'Default',
                            'weight' => 'Default'
                        ),
                    ),
                    'caption_font_size'   => array(
                        'type'          => 'njba-simplify',
                        'size'          => '5',
                        'label'         => __('Caption Font Size', 'bb-njba'),
                        'default'       => array(
                            'desktop' => '18',
                            'medium' => '16',
                            'small' => ''
                        )
                    )
                    
                )   
            ),
            'transition_section' =>array(
                'title' => __('Transition','bb-njba'),
                'fields' => array(
                    'transition' => array(
                        'type' => 'text',
                        'label' => __('Transition','bb-njba'),
                        'default' => 0.3,
                        'size' => '5',
                        'description' => 's'
                    )
                ) 
            )
        )
    ),
	/*'caption_tab' => array(
        'title' => __('Caption','bb-njba'),
        'sections' => array(
        	     
        )
    )*/
));