.fl-node-<?php echo $id; ?> .njba-box-comman{
      <?php if( $settings->background_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->background_color)) ?>, <?php echo $settings->background_color_opacity/100; ?>); <?php } ?>
      <?php if( $settings->image_padding['top'] != '' ) { ?> padding-top: <?php echo $settings->image_padding['top']; ?>px;<?php } ?>
	<?php if( $settings->image_padding['bottom'] != '' ) { ?>padding-bottom: <?php echo $settings->image_padding['bottom']; ?>px; <?php } ?>
	<?php if( $settings->image_padding['left'] != '' ) { ?>padding-left: <?php echo $settings->image_padding['left']; ?>px; <?php } ?>
	<?php if( $settings->image_padding['right'] != '' ) { ?>padding-right: <?php echo $settings->image_padding['right']; ?>px; <?php } ?>
	<?php if($settings->box_border_radius['top'] != '' ) { ?>border-top-left-radius:<?php echo $settings->box_border_radius['top'];?>px;<?php } ?>
	<?php if($settings->box_border_radius['right'] != '' ) { ?>border-top-right-radius:<?php echo $settings->box_border_radius['right'];?>px;<?php } ?>
	<?php if($settings->box_border_radius['bottom'] != '' ) { ?>border-bottom-left-radius:<?php echo $settings->box_border_radius['bottom'];?>px;<?php } ?>
	<?php if($settings->box_border_radius['left'] != '' ) { ?>border-bottom-right-radius:<?php echo $settings->box_border_radius['left'];?>px;<?php } ?>
	<?php if($settings->box_border_width >= '0') { ?>border-width: <?php echo $settings->box_border_width;?>px; <?php } ?>
	<?php if($settings->box_border_style) { ?>border-style:<?php echo $settings->box_border_style;?>;<?php } ?>
	<?php if($settings->box_border_color) {?>border-color:<?php echo '#'.$settings->box_border_color;?>;<?php } ?>
	
	box-shadow:<?php if($settings->box_shadow['left_right'] !=''){ echo $settings->box_shadow['left_right']; ?>px <?php } if($settings->box_shadow['top_bottom'] !=''){ echo $settings->box_shadow['top_bottom']; ?>px <?php } if($settings->box_shadow['blur'] !=''){ echo $settings->box_shadow['blur']; ?>px <?php } if($settings->box_shadow['spread'] !=''){ echo $settings->box_shadow['spread']; ?>px <?php } ?> #<?php echo $settings->box_shadow_color;  ?>;
      
	<?php if($settings->transition) { ?>transition:<?php echo 'all ease '.$settings->transition;?>s;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-box-comman:hover{
	<?php if( $settings->background_hover_color ) { ?>background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->background_hover_color)) ?>, <?php echo $settings->background_hover_color_opacity/100; ?>); <?php } ?>
	<?php if($settings->box_border_hover_color) {?>border-color:<?php echo '#'.$settings->box_border_hover_color;?>;<?php } ?>
		
	transform: rotate(<?php echo $settings->rotate_hover;?>deg) scale(<?php echo $settings->scale;?>);
	
	
}
.fl-node-<?php echo $id; ?> .njba-box-comman h1{
	<?php if($settings->caption_text_color) { ?>color:<?php echo '#'.$settings->caption_text_color;?>;<?php } ?>
	
	<?php if($settings->caption_font_family['family'] != 'Default' ) { ?><?php FLBuilderFonts::font_css( $settings->caption_font_family ); ?><?php } ?>
	<?php if(isset( $settings->caption_font_size['desktop'])) { ?>font-size:<?php echo $settings->caption_font_size['desktop'];?>px;<?php } ?>
	
	<?php if( $settings->caption_padding['top'] != '' ) { ?> padding-top: <?php echo $settings->caption_padding['top']; ?>px;<?php } ?>
	<?php if( $settings->caption_padding['bottom'] != '' ) { ?>padding-bottom: <?php echo $settings->caption_padding['bottom']; ?>px; <?php } ?>
	<?php if( $settings->caption_padding['left'] != '' ) { ?>padding-left: <?php echo $settings->caption_padding['left']; ?>px; <?php } ?>
	<?php if( $settings->caption_padding['right'] != '' ) { ?>padding-right: <?php echo $settings->caption_padding['right']; ?>px; <?php } ?>
	<?php if($settings->caption_alignment == 'left'){?>text-align:left;<?php }?>
	<?php if($settings->caption_alignment == 'center'){?>text-align:center;<?php }?>
	<?php if($settings->caption_alignment == 'right'){?>text-align:right;<?php }?>
	
}
.fl-node-<?php echo $id; ?> .njba-box-comman:hover h1{
	<?php if($settings->caption_text_hover_color) { ?>color:<?php echo '#'.$settings->caption_text_hover_color;?>;<?php } ?>
}
.fl-node-<?php echo $id; ?> .njba-box{
	transform: rotate(<?php echo $settings->rotate;?>deg) scale(1);
}
@media only screen and (max-width: 768px) {
	.fl-node-<?php echo $id; ?> .njba-box-comman h1{
		<?php if(isset( $settings->caption_font_size['medium'])) { ?>font-size:<?php echo $settings->caption_font_size['medium'];?>px;<?php } ?>
	}
}
@media only screen and (max-width: 480px) {
	.fl-node-<?php echo $id; ?> .njba-box-comman h1{
		<?php if(isset( $settings->caption_font_size['small'])) { ?>font-size:<?php echo $settings->caption_font_size['small'];?>px;<?php } ?>
	}
}