jQuery( function( $ ) {
	var NJBAajaxQueue = (function() {
		var requests = [];
		return {
			add:  function(opt) {
			    requests.push(opt);
			},
			remove:  function(opt) {
			    if( jQuery.inArray(opt, requests) > -1 )
			        requests.splice($.inArray(opt, requests), 1);
			},
			run: function() {
			    var self = this,
			        oriSuc;
			    if( requests.length ) {
			        oriSuc = requests[0].complete;
			        requests[0].complete = function() {
			             if( typeof(oriSuc) === 'function' ) oriSuc();
			             requests.shift();
			             self.run.apply(self, []);
			        };   
			        jQuery.ajax(requests[0]);
			    } else {
			      self.tid = setTimeout(function() {
			         self.run.apply(self, []);
			      }, 1000);
			    }
			},
			stop:  function() {
			    requests = [];
			    clearTimeout(this.tid);
			}
		};
	}());
	jQuery(document).ready(function($) {
		
		//	Lazy Load
		 
		jQuery(".njba-template-screenshot img").lazyload({
		    effect : "fadeIn",
		    event : "sporty"
		});
		jQuery(window).bind("load", function() {
		    var timeout = setTimeout(function() {
		        jQuery(".njba-template-screenshot img").trigger("sporty")
		    }, 8000);
		});
		
		var grid = jQuery('.njba-templates-page-templates');
		grid.shuffle({
			itemSelector: '.njba-single-page-templates',
		});
		// ReShuffle - When user clicks a filter item
		jQuery('.njba-templates-filter a').click(function (e) {
			e.preventDefault();
			// set active class
			jQuery('.njba-templates-filter a').removeClass('active');
			jQuery(this).addClass('active');
			// get group name from clicked item
			var groupName = jQuery(this).attr('data-group');
			// reshuffle grid
			grid.shuffle('shuffle', groupName );
		});
		// ReShuffle - When user clicks a tab
		jQuery('body').on('click', '.njba-admin-settings-nav li a', function (event) {
			var hash = jQuery(this).attr('href') || '';
			if( hash == '#template-cloud' ) {
				jQuery( window ).trigger( "resize.shuffle" );
			}
		});
	});
	
	/**
	 *	Template Tabs
	 */
	//jQuery("#njba-cloud-templates-tabs").tabs();
	/**
	 * Templates Count
	 */
	jQuery('body').on('click', '.njba-filter-links a', function (event) {
		var count = jQuery(this).attr( 'data-count' ) || 0;
		jQuery('.filter-count .count').html( count );
		
	});
	// Process of cloud templates - (download, remove & fetch)
	 
	NJBAajaxQueue.run();
	jQuery('body').on('click', '.njba-cloud-process', function (event) {
		event.preventDefault();
		var btn             	= jQuery(this),
			meta_id             = btn.find('.template-dat-meta-id').val() || '',
			meta_type           = btn.find('.template-dat-meta-type').val() || '',
			btn_template        = btn.parents('.njba-template-block'),
			btn_template_image  = btn_template.find('.njba-template-screenshot');
			btn_template_groups = btn_template.attr( 'data-groups' ) || '',
			btn_operation       = btn.attr('data-operation') || '',
			errorMessage        = NJBACloudTemplates.errorMessage,
			successMessage      = NJBACloudTemplates.successMessage,
			processAJAX         = true;
		//	add processing class
		if( meta_id != 'undefined' ) {
			$('#' + meta_id ).addClass('njba-template-processing');
		}
		//	remove error message if exist
		if( btn_template_image.find('.notice').length ) {
			btn_template_image.find('.notice').remove();
		}
		if( '' != btn_operation ) {
			btn.find('i').addClass('njba-reloading-iconfonts');
			switch( btn_operation ) {
				case 'fetch':
									jQuery('.wp-filter').find('.njba-cloud-process i').addClass('njba-reloading-iconfonts');
									btn.parents('.njba-cloud-templates-not-found').find('.njba-cloud-process i').show();
									var dataAJAX 		=  	{
																action: 'njba_cloud_dat_file_fetch',
															};
					break;
				case 'download':
									var meta_dat_url   = btn.find('.template-dat-meta-dat_url').val() || '',
										successMessage = NJBACloudTemplates.successMessageDownload,
										dataAJAX       = {
															action: 'njba_cloud_dat_file',
															dat_file: meta_dat_url,
															dat_file_id: meta_id,
															// dat_file_name: meta_name,
															// dat_file_image: meta_image,
															dat_file_type: meta_type,
															// dat_file_dat_url: meta_dat_url,
														};
										if( meta_dat_url === '' ) {
											processAJAX = false;
										}
					break;
				case 'remove':
								var meta_url_local = btn.find('.template-dat-meta-dat_url_local').val() || '',
									successMessage = NJBACloudTemplates.successMessageRemove,
									dataAJAX       = {
														action: 'njba_cloud_dat_file_remove',
														dat_file_id: meta_id,
														dat_file_type: meta_type,
														dat_file_url_local: meta_url_local,
													};
									if( meta_id === '' ) {
										processAJAX = false;
									}
					break;
			}
			
			if( processAJAX ) {
		       	NJBAajaxQueue.add({
					url: NJBACloudTemplates.ajaxurl,
					type: 'POST',
					data: dataAJAX,
					success: function(data){
						/**
						 * Parse response
						 */
						data = JSON.parse( data );
						// console.log('data: ' + JSON.stringify( data ) );
						var status                 = ( data.hasOwnProperty('status') ) ? data['status'] : '';
						var msg                    = ( data.hasOwnProperty('msg') ) ? data['msg'] : '';
						var template_id            = ( data.hasOwnProperty('id') ) ? data['id'] : '';
						var template_type          = ( data.hasOwnProperty('type') ) ? data['type'] : '';
						var template_dat_url       = ( data.hasOwnProperty('dat_url') ) ? decodeURIComponent( data['dat_url'] ) : '';
						var template_dat_url_local = ( data.hasOwnProperty('dat_url_local') ) ? decodeURIComponent( data['dat_url_local'] ) : '';
						if( status == 'success' ) {
							//	remove processing class
							if( meta_id != 'undefined' ) {
								$('#' + meta_id ).removeClass('njba-template-processing');
							}
							switch( btn_operation ) {
								case 'remove':
													jQuery( window ).trigger( 'njba-template-removed' );
													btn.removeClass('button-secondary');
													btn.addClass('button-primary');
													btn.find('i').removeClass('njba-reloading-iconfonts dashicons-no dashicons-update');
													btn.find('i').addClass('dashicons-yes');
													btn.find('.msg').html( NJBACloudTemplates.successMessageRemove );
													setTimeout(function() {
														btn_template.attr('data-is-downloaded', '');
														btn_template.removeClass( 'njba-downloaded' );
														btn.attr('data-operation', 'download');
														var output   = '<i class="dashicons dashicons-update" style="padding: 3px;"></i>'
																	 + '<span class="msg"> '+NJBACloudTemplates.btnTextDownload+' </span>'
																	 + '<input type="hidden" class="template-dat-meta-id" value="'+ template_id +'" />'
																	 + '<input type="hidden" class="template-dat-meta-type" value="'+ template_type +'" />'
																	 + '<input type="hidden" class="template-dat-meta-dat_url" value="'+ template_dat_url +'" />';
														btn.html( output );
														btn.parents('.njba-template-actions').find('.njba-installed-btn').remove();
													}, 1000);
									break;
								case 'download':
													jQuery( window ).trigger( 'njba-template-downloaded', [ template_dat_url_local, template_id ] );
													btn.removeClass('button-secondary');
													btn.addClass('button-primary');
													btn.find('i').removeClass('njba-reloading-iconfonts dashicons-no dashicons-update');
													btn.find('i').addClass('dashicons-yes');
													btn.find('.msg').html( NJBACloudTemplates.successMessageDownload );
													setTimeout(function() {
														btn.attr('data-operation', 'remove');
														btn_template.attr('data-is-downloaded', 'true');
														btn_template.addClass( 'njba-downloaded' );
														var output   = '<i class="dashicons dashicons-no" style="padding: 3px;"></i>'
																	 + '<span class="msg"> '+NJBACloudTemplates.btnTextRemove+' </span>'
																	 + '<input type="hidden" class="template-dat-meta-id" value="'+ template_id +'" />'
																	 + '<input type="hidden" class="template-dat-meta-type" value="'+ template_type +'" />'
																	 + '<input type="hidden" class="template-dat-meta-dat_url_local" value="'+ template_dat_url_local +'" />';
														var outputInstalled = '<span class="button button-sucess njba-installed-btn">'
																			+ '<i class="dashicons dashicons-yes" style="padding: 3px;"></i>'
																			+ '<span class="msg">'+NJBACloudTemplates.btnTextInstall+'</span>'
																			+ '</span>';
														btn.html( output );
														btn.parents('.njba-template-actions').append( outputInstalled );
													}, 1000);
													return;
									break;
								case 'fetch':
													jQuery( window ).trigger( 'njba-template-fetched' );
													btn.parents('.wp-filter').find('.njba-cloud-process').removeClass('button-secondary');
													btn.parents('.wp-filter').find('.njba-cloud-process').addClass('button-primary');
													btn.parents('.wp-filter').find('.njba-cloud-process i').removeClass('njba-reloading-iconfonts dashicons-no dashicons-update');
													btn.parents('.wp-filter').find('.njba-cloud-process i').addClass('dashicons-yes');
													btn.parents('.wp-filter').find('.njba-cloud-process .msg').html( NJBACloudTemplates.successMessageFetch );
													location.reload();
									break;
							}
						} else {
							/**
							 * Something went wrong
							 */
							if( '' != msg ) {
	
								btn.find('.msg').html( NJBACloudTemplates.errorMessageTryAgain );
								btn.find('i').removeClass('njba-reloading-iconfonts');
								var message = '<div class="notice notice-error uct-notice is-dismissible"><p>' + msg + '	</p></div>';
								btn_template_image.append( message );
							} else {
								btn.find('.msg').html( status );								
							}
						}
					}
				});
			}
		}
	});
	jQuery('body').on('click', '.njba-filter-links a', function (event) {
		var href = jQuery(this).attr( 'href' );
		var id = href.replace('#', "")
		if(id == 'njba-cloud-templates-page-templates')
		{
			jQuery('#' + id ).show();
			jQuery('#njba-cloud-templates-sections').hide();
		}
		else if(id == 'njba-cloud-templates-sections')
		{
			jQuery('#' + id ).show();
			jQuery('#njba-cloud-templates-page-templates').hide();
		}
		
	});
} );
