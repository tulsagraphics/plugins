(function($){
	
	NJBAuilderAdminSettings = {
		
		_iconUploader: null,
	
		init: function()
		{
			this._bind();
			this._initNav();
			
		},
		
		_bind: function()
		{
			$('.njba-admin-settings-section .njba-admin-settings-nav a').on('click', NJBAuilderAdminSettings._navClicked);
			
		},
	
		
		_initNav: function()
		{
			var links  = $('.njba-admin-settings-section .njba-admin-settings-nav a'),
				hash   = window.location.hash,
				active = hash === '' ? [] : links.filter('[href~="'+ hash +'"]');
				
				
			$('a.fl-active').removeClass('fl-active');
			$('.njba-admin-settings-section .fl-settings-form').hide();
				
			if(hash === '' || active.length === 0) {
				active = links.eq(0);
			}
			
			active.addClass('fl-active');
			$('.njba-admin-settings-section #fl-njba-'+ active.attr('href').split('#').pop() +'-form').fadeIn();
		},
		
		_navClicked: function()
		{
			if($(this).attr('href').indexOf('#') > -1) {
				$('a.fl-active').removeClass('fl-active');
				$('.njba-admin-settings-section .fl-settings-form').hide();
				$(this).addClass('fl-active');
				//console.log(this)''
				$('.njba-admin-settings-section #fl-njba-'+ $(this).attr('href').split('#').pop() +'-form').fadeIn();
			}
		},
		
		
	};
	/* Initializes the builder's admin settings. */
	$(function(){
		NJBAuilderAdminSettings.init();
	});
})(jQuery);