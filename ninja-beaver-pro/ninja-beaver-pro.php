<?php
/**
 * Plugin Name: Ninja Beaver Pro
 * Plugin URI: https://www.ninjabeaveraddon.com
 * Description: A set of custom, improvement, impressive modules for Beaver Builder.
 * Version: 1.3.2
 * Author: Ninja Team 
 * Author URI: https://www.ninjabeaveraddon.com
 * Copyright: (c) 2017-2018 Ninja Beaver Addons
 * Text Domain: bb-njba
 * Domain Path: /languages
 */
if( !class_exists( "BB_NJBA_Addon" ) ) {
	$njba_cat = esc_html__( 'NJBA Module', 'bb-njba' );
	
	if( !defined( 'NJBA_MODULE_DIR' ) ) {
		define( 'NJBA_MODULE_DIR', plugin_dir_path( __FILE__ ) );
	}
	if( !defined( 'NJBA_MODULE_URL' ) ) {
		define( 'NJBA_MODULE_URL', plugins_url( '/', __FILE__ ) );
	}
	if( !defined( 'NJBA_MODULE_CAT' ) ) {
		define( 'NJBA_MODULE_CAT', $njba_cat );
	}
	if( !defined( 'NJBA_MODULE_VERSION' ) ) {
		define( 'NJBA_MODULE_VERSION', '1.3.2');
	}
	if( !defined( 'NJBA_PLUGIN_FILE' ) ) {
		define('NJBA_PLUGIN_FILE', __FILE__ ); 
	}
	
	function njba_get_modules_group()
	{
		$njba    = NJBASettingsPage::get_builder_njba_white_label();
		$njba_builder_label = '';
		if( is_array($njba) ) {
			$njba_builder_label = ( array_key_exists( 'njba-builder-label' , $njba ) ) ? $njba['njba-builder-label' ] : esc_html__( 'NJBA Modules', 'bb-njba' );
		}
		if( $njba_builder_label == ''){
			$njba_builder_label = esc_html__( 'NJBA Modules', 'bb-njba' );
			return $njba_builder_label;
		}
		else{
			return $njba_builder_label;
		}
	}
	function njba_get_modules_cat($category = '')
	{
		$njba    = NJBASettingsPage::get_builder_njba_white_label();
		$njba_builder_cat = '';
		if( is_array($njba) ) {
			$njba_builder_cat = ( array_key_exists( 'njba-builder-category' , $njba ) ) ? $njba['njba-builder-category' ] : esc_html__( 'NJBA', 'bb-njba' );
		}
		if( $njba_builder_cat == '')
		{
		 	$njba_builder_cat = esc_html__( 'NJBA', 'bb-njba' );
		}
		$default = 'default';
		$new = 'new';
		$cats = array(
			'social'	=> sprintf(__('Social Modules - %s', 'bb-njba'), $njba_builder_cat),
			'carousel'		=> sprintf(__('Carousel Modules - %s', 'bb-njba'), $njba_builder_cat),
			'content'		=> sprintf(__('Content Modules - %s', 'bb-njba'), $njba_builder_cat),
			'creative'		=> sprintf(__('Creative Modules - %s', 'bb-njba'), $njba_builder_cat),
			'form_style'	=> sprintf(__('Form Style Modules - %s', 'bb-njba'), $njba_builder_cat),
			'separator'	=> sprintf(__('Separator Modules - %s', 'bb-njba'), $njba_builder_cat),
			// 'default'	=> sprintf(__('NJBA Modules - %s', 'bb-njba'), $njba_builder_cat),
			// 'new'	=> sprintf(__('%s - %s', 'bb-njba'), $category, $njba_builder_cat),
		);
		if ( empty( $category ) ) {
			return $cats/*[$default]*/;
		}

		if ( isset( $cats[$category] ) ) {
			return $cats[$category];
		} else {
			return $category;
		}

		
	}
	function ninja_beaver_plugin_updater() {
		// retrieve our license key from the DB
		$license_key = trim( get_option( 'ninja_beaver_license_key' ) );
		// setup the updater
		$edd_updater = new EDD_SL_Plugin_Updater( NJBA_STORE_URL, NJBA_PLUGIN_FILE, array(
				'version' 	=> '1.3.2', 				// current version number
				'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
				'item_name' => NINJA_BEAVER_PRO, 	// name of this plugin
				'item_id'	=> NINJA_BEAVER_ITEM_ID,
				'author' 	=> 'Ninja Team',  // author of this plugin
				'url'		=> home_url(),
				'beta'		=> false
			)
		);
	}
	add_action( 'admin_init', 'ninja_beaver_plugin_updater', 0 );
	function ninja_beaver_sanitize_license( $new ) {
		$old = get_option( 'ninja_beaver_license_key' );
		if( $old && $old != $new ) {
			delete_option( 'ninja_beaver_license_status' ); // new license has been entered, so must reactivate
		}
		return $new;
	}
	
	function ninja_beaver_pro_license_callback() {
		register_setting('ninja_beaver_license', 'ninja_beaver_license_key', 'ninja_beaver_sanitize_license' );
		
		// listen for our activate button to be clicked
		if( isset( $_POST['njba_license_activate'] ) ) {
			
			// run a quick security check
		 	if( ! check_admin_referer( 'njba_license_pro_nonce', 'njba_license_pro_nonce' ) )
				return; // get out if we didn't click the Activate button
			update_option('ninja_beaver_license_key',$_POST['njba_license_key'] );
			// retrieve the license from the database
			$license = trim( get_option( 'ninja_beaver_license_key' ) );
			// data to send in our API request
			$api_params = array(
				'edd_action' => 'activate_license',
				'license'    => $license,
				'item_name'  => urlencode( NINJA_BEAVER_PRO ), // the name of our product in EDD
				'item_id'	 => NINJA_BEAVER_ITEM_ID,
				'url'        => home_url()
			);
				
			// Call the custom API.
			$response = wp_remote_post( NJBA_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
			
			
			// make sure the response came back okay
			if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
				if ( is_wp_error( $response ) ) {
					$message = $response->get_error_message();
				} else {
					$message = __( 'An error occurred, please try again.' );
				}
			} else {
				$license_data = json_decode( wp_remote_retrieve_body( $response ) );
				
				//print_r($license_data);
				if ( false === $license_data->success ) {
					switch( $license_data->error ) {
						case 'expired' :
							$message = sprintf(
								__( 'Your license key expired on %s.','bb-njba' ),
								date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires, current_time( 'timestamp' ) ) )
							);
							break;
						case 'revoked' :
							$message = __( 'Your license key has been disabled.','bb-njba' );
							break;
						case 'missing' :
							$message = __( 'Invalid license.' );
							break;
						case 'invalid' :
						case 'site_inactive' :
							$message = __( 'Your license is not active for this URL.','bb-njba' );
							break;
						case 'item_name_mismatch' :
							$message = sprintf( __( 'This appears to be an invalid license key for %s.','bb-njba' ), EDD_SAMPLE_ITEM_NAME );
							break;
						case 'no_activations_left':
							$message = __( 'Your license key has reached its activation limit.','bb-njba' );
							break;
						default :
							$message = __( 'An error occurred, please try again.','bb-njba' );
							break;
					}
				}
			}
			// Check if anything passed on a message constituting a failure
			if ( ! empty( $message ) ) {
				$base_url = admin_url( '/admin.php?page=' . NINJA_BEAVER_LICENSE_PAGE );
				$redirect = add_query_arg( array( 'sl_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );
				wp_redirect( $redirect );
				exit();
			}
			
			update_option( 'ninja_beaver_license_status', $license_data->license );
			wp_redirect( admin_url( '/admin.php?page=' . NINJA_BEAVER_LICENSE_PAGE ) );
			exit();
		}
		// listen for our activate button to be clicked
		if( isset( $_POST['njba_license__deactivate'] ) ) {
			// run a quick security check
		 	if( ! check_admin_referer( 'njba_license_pro_nonce', 'njba_license_pro_nonce' ) )
				return; // get out if we didn't click the Activate button
			// retrieve the license from the database
			$license = trim( get_option( 'ninja_beaver_license_key' ) );
			// data to send in our API request
			$api_params = array(
				'edd_action' => 'deactivate_license',
				'license'    => $license,
				'item_name'  => urlencode( NINJA_BEAVER_PRO ), // the name of our product in EDD
				'item_id'	 => NINJA_BEAVER_ITEM_ID,
				'url'        => home_url()
			);
			// Call the custom API.
			$response = wp_remote_post( NJBA_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
			// make sure the response came back okay
			if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
				if ( is_wp_error( $response ) ) {
					$message = $response->get_error_message();
				} else {
					$message = __( 'An error occurred, please try again.','bb-njba' );
				}
				
				$base_url = admin_url( '/admin.php?page=' . NINJA_BEAVER_LICENSE_PAGE );
				$redirect = add_query_arg( array( 'sl_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );
				//wp_redirect( $redirect );
				exit();
			}
			// decode the license data
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );
			// $license_data->license will be either "deactivated" or "failed"
			if( $license_data->license == 'deactivated' ) {
				delete_option( 'ninja_beaver_license_status' );
			}
			wp_redirect( admin_url( '/admin.php?page=' . NINJA_BEAVER_LICENSE_PAGE ) );
			exit();
		}
	}
	add_action('admin_init', 'ninja_beaver_pro_license_callback');
	/**
	 * This is a means of catching errors from the activation method above and displaying it to the customer
	 */
	function ninja_beaver_pro_admin_notices() {
		if ( isset( $_GET['sl_activation'] ) && ! empty( $_GET['message'] ) ) {
			switch( $_GET['sl_activation'] ) {
				case 'false':
					$message = urldecode( $_GET['message'] );
					?>
							<div class="error">
							  <p><?php echo $message; ?></p>
							</div>
					<?php
					break;
				case 'true':
				default:
					// Developers can put a custom success message here for when activation is successful if they way.
					break;
			}
		}
	}
	add_action( 'admin_notices', 'ninja_beaver_pro_admin_notices' );
		
	class BB_NJBA_Addon {
		public function __construct()
	    {
	       	add_action( 'init', array( $this, 'njba_load_modules')  );
			add_action( 'wp_enqueue_scripts', array( $this, 'njab_load_scripts') );
			add_filter('body_class',array( $this, 'njba_body_classes'));
			add_action( 'plugins_loaded', array( $this,'load_plugin_textdomain' ) );
		}
		
			static public function module_cat( $cat ) {
			    return class_exists( 'FLBuilderUIContentPanel' ) ? $cat : NJBA_MODULE_CAT;
			}
			/**
			 * Ninja modules
			 */
			function njba_load_modules() {
				if ( class_exists( 'FLBuilder' ) ) {
					add_option( 'ninja_beaver_license_key', '' );
					add_option( 'ninja_beaver_license_status', '' );
					
					$njba_admin_option_data = array('google_static_map_api_key'   => '', 
									                    'enable_row_sep' => '',
									                    'facebook_app_id' => '',
									                );
					$njba_options = get_option('njba_options',$njba_admin_option_data);
					$njba_white_label_option = add_option('njba_white_label_option'); 
					$njba_extensions_lists = add_option( 'njba_extensions_lists', '' );
					
					if($njba_options == ''){
						$njba_admin_options = add_option( 'njba_options', $njba_admin_option_data );
					}

					$enable_row_sep   = ( array_key_exists( 'enable_row_sep', $njba_options ) && $njba_options['enable_row_sep'] == 1 )  ? ' checked' : ''; 
					if($enable_row_sep){
					    	require_once 'includes/row.php';
					}
					/* admin settings*/
					require_once 'classes/class-admin-settings.php';
					/*class fields*/
				    require_once 'classes/class-module-fields.php';
				    /* Load Template*/
					require_once 'classes/class-load-template.php';
					/*Template Install*/
				    require_once 'classes/class-njba-cloud-templates.php';
				    require_once 'modules/njba-testimonials/njba-testimonials.php';
				    require_once 'modules/njba-teams/njba-teams.php';
				    require_once 'modules/njba-image-carousel/njba-image-carousel.php';
				    require_once 'modules/njba-logo-grid-carousel/njba-logo-grid-carousel.php';
				    require_once 'modules/njba-content-grid/njba-content-grid.php';
				    require_once 'modules/njba-content-list/njba-content-list.php';
				    require_once 'modules/njba-content-tiles/njba-content-tiles.php';
				    require_once 'modules/njba-slider/njba-slider.php';
				    require_once 'modules/njba-modal-box/njba-modal-box.php';
				    require_once 'modules/njba-gallery/njba-gallery.php';
				    require_once 'modules/njba-audio/njba-audio.php';
				    require_once 'modules/njba-quote-box/njba-quote-box.php';
				    require_once 'modules/njba-opening-hours/njba-opening-hours.php';
				    require_once 'modules/njba-breadcrumb/njba-breadcrumb.php';
				    require_once 'modules/njba-heading/njba-heading.php';
				    require_once 'modules/njba-button/njba-button.php';
				    require_once 'modules/njba-infobox-two/njba-infobox-two.php';
				    require_once 'modules/njba-dual-button/njba-dual-button.php';
				    require_once 'modules/njba-advance-cta/njba-advance-cta.php';
				    require_once 'modules/njba-price-box/njba-price-box.php';
				    require_once 'modules/njba-accordion/njba-accordion.php';
				   	require_once 'modules/njba-infobox/njba-infobox.php';
				    require_once 'modules/njba-separator/njba-separator.php';
				    require_once 'modules/njba-spacer/njba-spacer.php';
				    require_once 'modules/njba-hover-box-carosel/njba-hover-box-carosel.php';
				    require_once 'modules/njba-advanced-tabs/njba-advanced-tabs.php';
				    require_once 'modules/njba-image-hover/njba-image-hover.php';
				    require_once 'modules/njba-alert-box/njba-alert-box.php';
				    require_once 'modules/njba-contact-form/njba-contact-form.php';
				    require_once 'modules/njba-infolist/njba-infolist.php';
				    require_once 'modules/njba-icon-img/njba-icon-img.php';
				    require_once 'modules/njba-counter/njba-counter.php';
				    require_once 'modules/njba-subscribe-form/njba-subscribe-form.php';
				    require_once 'modules/njba-timeline/njba-timeline.php';
				    require_once 'modules/njba-after-before-slider/njba-after-before-slider.php';
				    require_once 'modules/njba-static-map/njba-static-map.php';
				    require_once 'modules/njba-highlight-box/njba-highlight-box.php';
				    require_once 'modules/njba-image-panels/njba-image-panels.php';
				    require_once 'modules/njba-social-share/njba-social-share.php';
				    require_once 'modules/njba-polaroid/njba-polaroid.php';
				    require_once 'modules/njba-flip-box/njba-flip-box.php';
				    require_once 'modules/njba-image-hover-two/njba-image-hover-two.php';
				    require_once 'modules/njba-polaroid-gallery/njba-polaroid-gallery.php';
				    require_once 'modules/njba-img-separator/njba-img-separator.php';
				    require_once 'modules/njba-column-separator/njba-column-separator.php';
				    require_once 'modules/njba-countdown/njba-countdown.php';
				    require_once 'modules/njba-gravity-form/njba-gravity-form.php';
				    require_once 'modules/njba-facebook-button/njba-facebook-button.php';
				    require_once 'modules/njba-facebook-comments/njba-facebook-comments.php';
				    require_once 'modules/njba-facebook-embed/njba-facebook-embed.php';
				    require_once 'modules/njba-facebook-page/njba-facebook-page.php';
				    require_once 'modules/njba-twitter-buttons/njba-twitter-buttons.php';
				    require_once 'modules/njba-twitter-grid/njba-twitter-grid.php';
				    require_once 'modules/njba-twitter-timeline/njba-twitter-timeline.php';
				    require_once 'modules/njba-twitter-tweet/njba-twitter-tweet.php';
				    require_once 'modules/njba-content-carousel/njba-content-carousel.php';
				    require_once 'modules/njba-animated-headlines/njba-animated-headlines.php';
				}
			}
			function load_plugin_textdomain(){
				if ( function_exists( 'get_user_locale' ) ) {
					$locale = apply_filters( 'plugin_locale', get_user_locale(), 'bb-njba' );
				} else {
					$locale = apply_filters( 'plugin_locale', get_locale(), 'bb-njba' );
				}
				//Setup paths to current locale file
				$mofile_global = trailingslashit( WP_LANG_DIR ) . 'plugins/bb-plugin/' . $locale . '.mo';
				$mofile_local  = trailingslashit( NJBA_MODULE_DIR ) . 'languages/' . $locale . '.mo';
				if ( file_exists( $mofile_global ) ) {
					//Look in global /wp-content/languages/plugins/bb-plugin/ folder
					return load_textdomain( 'bb-njba', $mofile_global );
				} elseif ( file_exists( $mofile_local ) ) {
					//Look in local /wp-content/plugins/bb-plugin/languages/ folder
					return load_textdomain( 'bb-njba', $mofile_local );
				}
				//Nothing found
				return false;
			}
			/**
			 * Ninja modules Scripts
			 */
			function njab_load_scripts()
			{
					if ( class_exists( 'FLBuilderModel' ) && FLBuilderModel::is_builder_active() ) {
						wp_enqueue_style( 'njba-fields-style', NJBA_MODULE_URL . 'assets/css/njba-fields.css', array(), rand() );
						wp_enqueue_script( 'njba-fields-script', NJBA_MODULE_URL . 'assets/js/fields.js', array( 'jquery' ), rand(), true );
					}
					wp_register_style( 'jquery-swiper', NJBA_MODULE_URL . 'assets/css/swiper.min.css', array(), rand() );
					wp_register_script( 'njba-twitter-widgets', NJBA_MODULE_URL . 'assets/js/twitter-widgets.js', array('jquery'), rand(), true );
					wp_register_script( 'instafeed', NJBA_MODULE_URL . 'assets/js/instafeed.min.js', array('jquery'), NJBA_MODULE_URL, true );
					wp_register_script( 'jquery-swiper', NJBA_MODULE_URL . 'assets/js/swiper.jquery.min.js', array('jquery'), rand(), true );

			}
			/**
			 * Ninja modules body class
			 */
			function njba_body_classes($classes) {
			   	 $classes[] = 'bb-njba';
				 return $classes;
			}
	}
	new BB_NJBA_Addon();
	// this is the URL our updater / license checker pings. This should be the URL of the site with EDD installed
	if( !defined( 'NJBA_STORE_URL' ) ) {
		define( 'NJBA_STORE_URL', 'https://www.ninjabeaveraddon.com/' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file
	}
	// the name of your product. This should match the download name in EDD exactly
	if( !defined( 'NINJA_BEAVER_PRO' ) ) {
		define( 'NINJA_BEAVER_PRO', 'Ninja Beaver Pro' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file
	}
	// the name of the settings page for the license input to be displayed
	if( !defined( 'NINJA_BEAVER_LICENSE_PAGE' ) ) {
		define( 'NINJA_BEAVER_LICENSE_PAGE', 'njba-admin-setting' );
	}
	if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
		// load our custom updater
		include( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
	}
	if( !defined( 'NINJA_BEAVER_ITEM_ID' ) ) {
		define( 'NINJA_BEAVER_ITEM_ID', 3149 );
	}
    add_action( 'admin_notices', 'woo_njba_admin_notice' );
	add_action( 'network_admin_notices', 'woo_njba_admin_notice' );
	function woo_njba_admin_notice() {
		global $pagenow;

		if(empty(get_option( 'woo-njba-notice-dismissed' ))){
			$url = admin_url( 'index.php' );
			$learn_more = "https://www.woobeaveraddons.com/";
			$documentation = "https://www.woobeaveraddons.com/category/docs/";
			$image = "https://www.woobeaveraddons.com/wp-content/uploads/2017/12/woo-logo.png";
			echo '<div class="notice notice-info is-dismissible woo-info"><div class="info-image"><p>';
			echo sprintf( __( "<img src='$image'>", 'bb-njba' ), $url );
		    echo '</p></div><div class="info-descriptions"><div class="info-descriptions-title"><h3><strong>Introducing Woo Beaver</strong></h3></div><p>';
			echo sprintf( __( "You can create page templates for single product and category pages. you can also use single product module, product list module, grid modules and add to cart modules for woocommerce. You can create single product template for specific category products or also specific products too. You can easily set rules for it.</br></br><a href='$learn_more' target='_blank'>Learn More</a>   <a href='$documentation' target='_blank'>Documentation</a>", 'bb-njba' ), $url );
		    echo '</p></div></div>';
		}
  	}

  	add_action('admin_footer','woo_njba_admin_notice_script');
  	function woo_njba_admin_notice_script(){

  		if(empty(get_option( 'woo-njba-notice-dismissed' ))){
  		?>
  		<script type="text/javascript">
  		jQuery(document).on( 'click', '.woo-info .notice-dismiss', function() {
		    jQuery.ajax({
		        url: ajaxurl,
		        data: {
		            action: 'dismiss_woo_njba'
		        }
		    })

		})
		</script>
  		<?php
  		}
  	}
  	add_action( 'wp_ajax_dismiss_woo_njba', 'set_dismiss_woo_njba_option' );
	function set_dismiss_woo_njba_option() {
		update_option( 'woo-njba-notice-dismissed','yes');
		echo 'success';
		exit();
	}

}
else
{
	// Display admin notice for activating beaver builder
	add_action( 'admin_notices', 'njba_admin_notices' );
	add_action( 'network_admin_notices', 'njba_admin_notices' );
	function njba_admin_notices() {
		$url = admin_url( 'plugins.php' );
		echo '<div class="notice notice-error"><p>';
		echo sprintf( __( "You currently have two versions of <strong> Ninja Beaver Addon for Beaver Builder</strong> active on this site. Please <a href='%s'>deactivate one</a> before continuing.", 'bb-njba' ), $url );
	    echo '</p></div>';
  	}
}