﻿Version 1.3.2 - Friday, 14 September, 2018
- Fixed: Slider Module Minimum Height

Version 1.3.1 - Tuesday, 28 August, 2018
- Fixed: Minor Bug
- Few other enhancement
- Major Update In "Live Preview"
- now supported to "Partial Refresh"
- Working In All Module's

Version 1.3.0 - Friday, 03 August, 2018
- Fixed: Minor Bug
- Few other enhancement

Version 1.2.9.1 - Monday, 31 July, 2018
- Fixed: Minor Bug

Version 1.2.9 - Saturday, 28 July, 2018
- Fixed: Minor Bug
- Few other enhancement

Version 1.2.8 - Monday, 09 July, 2018
- Fixed: Facebook APP ID undefined Index
- Few other enhancement

Version 1.2.7 - Wednesday, 05 July, 2018
- New: Animated Headlines - Add New Module
-Fixed: Minor Bug in white label options
- Few Enhancement

Version 1.2.6 - Monday, 18 June, 2018
-Fixed: Minor Bug in white label options

Version 1.2.5 - Friday, 15 June, 2018
-Fixed: Minor Bug in white label options

Version 1.2.4 - Sunday, 10 June, 2018
- New: Facebook Button - Add New Module
- New: Facebook Comments - Add New Module
- New: Facebook Embed - Add New Module
- New: Facebook Page - Add New Module
- New: Twitter Timeline - Add New Module
- New: Twitter Grid - Add New Module
- New: Twitter Tweet - Add New Module
- New: Twitter Buttons - Add New Module
- Fixed: slider Modules - Fixed image size issue
- Few Enhancement

Version 1.2.3 - Saturday, 26 May, 2018
- Few Enhancement

Version 1.2.2 - Friday, 25 May, 2018
- Enhancement: Added an few more option in White Label settings
- Bug fixed: Row Seperator default triangle option removed and set normal option default
- Tested with Latest wordpress version 4.9.6

Version 1.2.1 - Monday, 16 May, 2018
- New: add content grid style
- New: add subscribe form style
- Few Enhancement

Version 1.2 - Monday, 7 May, 2018
- New: breadcrumb module
- New: hover box carosel module
- New: wpml compatibility with .po and .mo file
- Few Enhancement

Version 1.1.8 - Monday, 1 January, 2018
- New: White Label Option
- Few Enhancement

Version 1.1.7 - Monday, 11 December, 2017
- New: Content Grid - Add New Style
- Fixed: Contact Form - Fixed js issue
- Few Enhancement

Version 1.1.6 - Friday, 1 December, 2017
- Fixed: Gallery- Fixed js issue conflict with masonry js

Version 1.1.5 - Friday, 17 November, 2017
- New: Compatibility with Beaver Builder 2.0
- New: Ninja Beaver Addons Dashboard Settings
- New: Add Page Templates And Row Section Option.
- Fixed: Testimonials - Fixed Layout Option Css issue admin lightbox
- Fixed: Content Grid - Fixed Layout Option Css issue admin lightbox
- Fixed: Highlight Box - Fixed Layout Option Css issue admin lightbox
- Fixed: Logo Grid Carousel - Fixed Layout Option Css issue admin lightbox
- Fixed: Modal Box - Fixed Layout Option Css issue admin lightbox
- Fixed: Price Box - Fixed Layout Option Css issue admin lightbox
- Fixed: Separator - Fixed Layout Option Css issue admin lightbox
- Fixed: Timeline - Fixed Layout Option Css issue admin lightbox
- Fixed: Teams - Fixed Layout Option Css issue admin lightbox 
- Fixed: Heading - Fixed Css issue when open admin lightbox 
- Fixed: Info Box - Fixed Css issue when open admin lightbox 
- Fixed: Info Box Two- Fixed Css issue when open admin lightbox 
- Fixed: Quote Box- Fixed Css issue when open admin lightbox 
- Fixed: Field- Fixed Css issue when open admin lightbox 
- Few Enhancement

Version 1.1.4 - Monday, 09 October, 2017
-Fixed: Purchase More Addons List.

Version 1.1.3 - Monday, 18 September, 2017
-Fixed: Contact Form Sending Mail Issue.

Version 1.1.2 - Saturday, 16 September, 2017
-Fixed: Minor Bug

Version 1.1.1 - Monday, 28 August, 2017
-Fixed: Minor Bug

Version 1.1 - Friday, 18 August, 2017
-Fixed: PHP 5.3.10 compatibility errors
-Fixed: Google Static Map – Map not display in PHP 5.3.10 version
-Fixed: Gallery – Image Size issue
-Fixed: Row Separator – Custom class conflict when row separator is disable
-Fixed: Testimonials – Image Padding Option issue

Version 1.0 - Friday, 2 June, 2017
-Initial Release