# Copyright (C) 2018 SkyVerge
# This file is distributed under the GNU General Public License v3.0.
msgid ""
msgstr ""
"Project-Id-Version: WooCommerce Cost of Goods 2.8.0\n"
"Report-Msgid-Bugs-To: "
"https://woocommerce.com/my-account/marketplace-ticket-form/\n"
"POT-Creation-Date: 2018-09-05 06:28:06+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2018-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"

#. Plugin Name of the plugin/theme
msgid "WooCommerce Cost of Goods"
msgstr ""

#: includes/admin/class-wc-cog-admin-orders.php:96
#: includes/admin/class-wc-cog-admin-orders.php:347
msgid "Cost of Goods"
msgstr ""

#: includes/admin/class-wc-cog-admin-orders.php:171
msgid "Should be:"
msgstr ""

#: includes/admin/class-wc-cog-admin-products.php:130
#: includes/admin/class-wc-cog-admin-products.php:150
#: includes/admin/class-wc-cog-admin-products.php:238
#: includes/admin/class-wc-cog-admin-products.php:544
#. translators: Placeholder: %s - currency symbol
msgid "Cost of Good (%s)"
msgstr ""

#: includes/admin/class-wc-cog-admin-products.php:153
msgid "Default cost for product variations"
msgstr ""

#: includes/admin/class-wc-cog-admin-products.php:184
msgid "Set cost"
msgstr ""

#: includes/admin/class-wc-cog-admin-products.php:376
msgid "Cost of Good"
msgstr ""

#: includes/admin/class-wc-cog-admin-products.php:381
msgid "— No Change —"
msgstr ""

#: includes/admin/class-wc-cog-admin-products.php:382
msgid "Change to:"
msgstr ""

#: includes/admin/class-wc-cog-admin-products.php:383
msgid "Increase by (fixed amount or %):"
msgstr ""

#: includes/admin/class-wc-cog-admin-products.php:384
msgid "Decrease by (fixed amount or %):"
msgstr ""

#: includes/admin/class-wc-cog-admin-products.php:394
msgid "Enter Cost:"
msgstr ""

#: includes/admin/class-wc-cog-admin-products.php:474
#: includes/admin/class-wc-cog-admin-products.php:588
msgid "Cost"
msgstr ""

#: includes/admin/class-wc-cog-admin-reports.php:70
msgid "Profit"
msgstr ""

#: includes/admin/class-wc-cog-admin-reports.php:73
msgid "Profit by date"
msgstr ""

#: includes/admin/class-wc-cog-admin-reports.php:79
msgid "Profit by product"
msgstr ""

#: includes/admin/class-wc-cog-admin-reports.php:85
msgid "Profit by category"
msgstr ""

#: includes/admin/class-wc-cog-admin-reports.php:96
msgid "Product Valuation"
msgstr ""

#: includes/admin/class-wc-cog-admin-reports.php:102
msgid "Total Valuation"
msgstr ""

#: includes/admin/class-wc-cog-admin-reports.php:103
msgid ""
"Total valuation provides the value of all inventory within your store at "
"both the cost of the good, as well as the total value of inventory at the "
"retail price (regular price, or sale price if set). Stock count must be set "
"to be included in this valuation."
msgstr ""

#: includes/admin/class-wc-cog-admin-reports.php:355
msgid "In stock"
msgstr ""

#: includes/admin/class-wc-cog-admin-reports.php:357
msgid "Out of stock"
msgstr ""

#: includes/admin/class-wc-cog-admin.php:154
msgid "Cost of Goods Options"
msgstr ""

#: includes/admin/class-wc-cog-admin.php:161
msgid "Exclude these item(s) from income when calculating profit. "
msgstr ""

#: includes/admin/class-wc-cog-admin.php:162
msgid "Fees charged to customer (e.g. Checkout Add-Ons, Payment Gateway Based Fees)"
msgstr ""

#: includes/admin/class-wc-cog-admin.php:171
msgid "Shipping charged to customer"
msgstr ""

#: includes/admin/class-wc-cog-admin.php:180
msgid "Tax charged to customer"
msgstr ""

#: includes/admin/class-wc-cog-admin.php:215
msgid "Apply Costs to Previous Orders"
msgstr ""

#: includes/admin/class-wc-cog-admin.php:216
msgid ""
"This will apply costs to previous orders based on your selection when "
"\"Apply Costs\" is clicked and cannot be reversed."
msgstr ""

#: includes/admin/class-wc-cog-admin.php:230
msgid "Apply costs to orders that do not have costs set"
msgstr ""

#: includes/admin/class-wc-cog-admin.php:240
msgid "Apply costs to all orders, overriding previous costs"
msgstr ""

#: includes/admin/class-wc-cog-admin.php:267
msgid "Choose carefully, this action is not reversible!"
msgstr ""

#: includes/admin/class-wc-cog-admin.php:273
msgid "Apply Costs"
msgstr ""

#: includes/admin/class-wc-cog-admin.php:316
msgid ""
"Are you sure you want to apply costs to all previous orders that have not "
"already had costs generated? This cannot be reversed! Note that this can "
"take some time in shops with a large number of orders."
msgstr ""

#: includes/admin/class-wc-cog-admin.php:317
msgid ""
"Are you sure you want to apply costs to ALL previous orders, overriding "
"those with existing costs? This cannot be reversed! Note that this can take "
"some time in shops with a large number of orders."
msgstr ""

#: includes/admin/class-wc-cog-admin.php:318
msgid "Oops! Something went wrong. Please try again."
msgstr ""

#: includes/admin/class-wc-cog-admin.php:319
msgid "Cost of goods applied to previous orders."
msgstr ""

#: includes/admin/class-wc-cog-admin.php:320
msgid "Applying costs of goods to previous orders..."
msgstr ""

#: includes/admin/reports/abstract-wc-cog-admin-report.php:63
msgid "Year"
msgstr ""

#: includes/admin/reports/abstract-wc-cog-admin-report.php:64
msgid "Last Month"
msgstr ""

#: includes/admin/reports/abstract-wc-cog-admin-report.php:65
msgid "This Month"
msgstr ""

#: includes/admin/reports/abstract-wc-cog-admin-report.php:66
msgid "Last 7 Days"
msgstr ""

#: includes/admin/reports/abstract-wc-cog-admin-report.php:87
msgid "Date"
msgstr ""

#: includes/admin/reports/abstract-wc-cog-admin-report.php:104
#: includes/admin/reports/class-wc-cog-admin-report-product-valuation.php:192
msgid "Export CSV"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-product-valuation.php:161
msgid "Value at Retail"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-product-valuation.php:162
msgid "Value at Cost"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-product-valuation.php:186
msgid "Product"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-product-valuation.php:196
msgid ""
"Note: Product Valuation export is in progress, please do not refresh the "
"page! When the export is complete, the download will start automatically."
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-product-valuation.php:208
msgid "Search for a product"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-category.php:111
#. translators: Placeholders: %1$s - formatted total profit amount surrounded
#. by <strong> tags, e.g. <strong>$66.77</strong>, %1$s - product category
#. name, e.g. t-shirts
msgid "%1$s total profit in %2$s"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-category.php:138
msgid "Categories"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-category.php:156
msgid "Select categories&hellip;"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-category.php:170
msgid "None"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-category.php:171
msgid "All"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-category.php:172
#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:223
msgid "Show"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-category.php:226
msgid "&larr; Choose a category to view stats"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-date.php:64
#. translators: Placeholders: %1$s is the formatted net sales amount with
#. surrounding <strong> tags, e.g. <strong>$7.77</strong>
msgid "%1$s net sales in this period"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-date.php:65
msgid ""
"This is the sum of the order totals after any refunds and excluding fees, "
"taxes, and shipping (unless you have toggled the settings to include them)"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-date.php:73
#. translators: Placeholders: %1$s is the formatted total cost of goods amount
#. with surrounding <strong> tags, e.g. <strong>$2.00</strong>
msgid "%1$s total cost of goods in this period"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-date.php:74
msgid "This is the sum of the item cost of goods after any refunds"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-date.php:82
#. translators: Placeholders: %1$s is the formatted total profit amount with
#. surrounding <strong> tags, e.g. <strong>$5.77</strong>
msgid "%1$s total profit in this period"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-date.php:83
msgid "This is the sum of the order profit after any refunds"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-date.php:91
#. translators: Placeholders: %1$s is the total orders count with surrounding
#. <strong> tags, e.g. <strong>7</strong>
msgid "%1$s orders placed"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-date.php:99
#. translators: Placeholders: %1$s is the formatted average profit per order
#. amount with surrounding <strong> tags, e.g. <strong>$1.77</strong>
msgid "%1$s average profit per order in this period"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-date.php:100
msgid "This is the average profit per order after any refunds"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-date.php:181
msgid "Number of orders"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-date.php:189
msgid "Net Sales amount"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-date.php:199
#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:406
msgid "Cost of Goods Sold"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-date.php:209
#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:416
msgid "Profit amount"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:96
#. translators: Placeholders: %1$s is the formatted total product sales with
#. surrounding <strong> tags, e.g. <strong>$7.77</strong>
msgid "%1$s sales for the selected items"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:104
#. translators: Placeholders: %1$s is the formatted total product cost of goods
#. with surrounding <strong> tags, e.g. <strong>$4.77</strong>
msgid "%1$s cost of goods for the selected items"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:112
#. translators: Placeholders: %1$s is the formatted total product profit with
#. surrounding <strong> tags, e.g. <strong>$3.00</strong>
msgid "%1$s profit for the selected items"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:120
#. translators: Placeholders: %1$s is the the total number of purchased items
#. with surrounding <strong> tags, e.g. <strong>5</strong>
msgid "%1$s purchases for the selected items"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:147
msgid "Product Search"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:160
msgid "Showing reports for:"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:185
msgid "Reset"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:207
#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:218
msgid "Search for a product&hellip;"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:246
msgid "Least Profitable Sellers"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:249
msgid "Most Profitable Sellers"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:291
#. translators: Placeholders: %1$s - profit margin as a percentage, e.g. 85.4%
msgid "%1$s%% profit margin"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:292
msgid "total profit"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:299
msgid "No products found in range"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:303
msgid "show most profitable"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:305
msgid "show least profitable"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:352
msgid "&larr; Choose a product to view stats"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:388
msgid "Number of items sold"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-profit-by-product.php:396
msgid "Sales amount"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-total-valuation.php:55
msgid "at cost"
msgstr ""

#: includes/admin/reports/class-wc-cog-admin-report-total-valuation.php:62
msgid "at retail"
msgstr ""

#: includes/class-wc-cog-ajax.php:111
#. translators: Placeholder: %s - possible error (could be an empty string)
msgid "Database error while applying product costs. %s"
msgstr ""

#: includes/class-wc-cog-ajax.php:174
msgid "Applying costs job ID missing."
msgstr ""

#: includes/class-wc-cog-ajax.php:180
msgid "Applying costs job process could not be found."
msgstr ""

#: includes/class-wc-cog-lifecycle.php:121
#: includes/class-wc-cog-lifecycle.php:196
#. translators: Placeholders: %s - error messages
msgid "Error upgrading <strong>WooCommerce Cost of Goods</strong>: %s"
msgstr ""

#: includes/class-wc-cog-lifecycle.php:123
#: includes/class-wc-cog-lifecycle.php:198
msgid "&laquo; Go Back"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://www.woocommerce.com/products/woocommerce-cost-of-goods/"
msgstr ""

#. Description of the plugin/theme
msgid ""
"A full-featured cost of goods management extension for WooCommerce, with "
"detailed reporting for total cost and profit"
msgstr ""

#. Author of the plugin/theme
msgid "SkyVerge"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://www.woocommerce.com"
msgstr ""