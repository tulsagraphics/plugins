# Copyright 2015-2018 Steven Henty.
# Translators:
# FX Bénard <fxb@wp-translations.org>, 2017
msgid ""
msgstr ""
"Project-Id-Version: gravityflowformconnector\n"
"Report-Msgid-Bugs-To: https://www.gravityflow.io\n"
"POT-Creation-Date: 2018-06-17 13:22:04+00:00\n"
"PO-Revision-Date: 2018-MO-DA HO:MI+ZONE\n"
"Last-Translator: FX Bénard <fxb@wp-translations.org>, 2017\n"
"Language-Team: Russian (Russia) (https://www.transifex.com/gravityflow/teams/50678/ru_RU/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru_RU\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Gravity Flow Build Script\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-Bookmarks: \n"
"X-Poedit-Country: United States\n"
"X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Textdomain-Support: yes\n"

#: class-form-connector.php:106
msgid "Manage Settings"
msgstr ""

#: class-form-connector.php:107
msgid "Uninstall"
msgstr ""

#: class-form-connector.php:354
msgid "Submission received."
msgstr ""

#: class-form-connector.php:416 class-form-connector.php:426
msgid "The link to this form is no longer valid."
msgstr ""

#: class-form-connector.php:465
msgid "This form is no longer valid."
msgstr ""

#: class-form-connector.php:476
msgid "This form is no longer accepting submissions."
msgstr ""

#: class-form-connector.php:486
msgid "Your input is no longer required."
msgstr ""

#: class-form-connector.php:493
msgid "There was a problem with you submission. Please use the link provided."
msgstr ""

#: includes/class-step-delete-entry.php:22
#: includes/class-step-delete-entry.php:32
msgid "Delete an Entry"
msgstr ""

#: includes/class-step-delete-entry.php:36
#: includes/class-step-new-entry.php:37
#: includes/class-step-update-entry.php:46
msgid "Site"
msgstr ""

#: includes/class-step-delete-entry.php:42
#: includes/class-step-new-entry.php:43
#: includes/class-step-update-entry.php:52
msgid "This site"
msgstr ""

#: includes/class-step-delete-entry.php:44
#: includes/class-step-new-entry.php:44
#: includes/class-step-update-entry.php:53
msgid "A different site"
msgstr ""

#: includes/class-step-delete-entry.php:51
#: includes/class-step-new-entry.php:49
#: includes/class-step-update-entry.php:58
msgid "Site Url"
msgstr ""

#: includes/class-step-delete-entry.php:60
#: includes/class-step-new-entry.php:58
#: includes/class-step-update-entry.php:67
msgid "Public Key"
msgstr "Публичный Ключ"

#: includes/class-step-delete-entry.php:69
#: includes/class-step-new-entry.php:67
#: includes/class-step-update-entry.php:76
msgid "Private Key"
msgstr "Закрытый ключ"

#: includes/class-step-delete-entry.php:78
msgid "Delete Action"
msgstr ""

#: includes/class-step-delete-entry.php:84
msgid "Permanently delete the entry"
msgstr ""

#: includes/class-step-delete-entry.php:88
msgid "Move the entry to the trash"
msgstr ""

#: includes/class-step-delete-entry.php:102
#: includes/class-step-update-entry.php:104
msgid "Entry ID Field"
msgstr ""

#: includes/class-step-delete-entry.php:104
msgid ""
"Select the field which will contain the entry ID of the entry that will be "
"deleted. This is used to lookup the entry so it can be deleted."
msgstr ""

#: includes/class-step-delete-entry.php:123
#: includes/class-step-update-entry.php:128
msgid "Entry ID (Self)"
msgstr ""

#: includes/class-step-delete-entry.php:177
msgid "Moved entry to the trash."
msgstr ""

#: includes/class-step-delete-entry.php:182
msgid "Scheduled entry for deletion on workflow completion."
msgstr ""

#: includes/class-step-form-submission.php:20
#: includes/class-step-form-submission.php:43
msgid "Form Submission"
msgstr ""

#: includes/class-step-form-submission.php:28
#: includes/class-step-new-entry.php:26
#: includes/class-step-update-entry.php:32
msgid "Select a Form"
msgstr ""

#: includes/class-step-form-submission.php:36
msgid "Select"
msgstr "Выбор"

#: includes/class-step-form-submission.php:37
msgid "Conditional Routing"
msgstr "Условная маршрутизация"

#: includes/class-step-form-submission.php:51
msgid "Assignee Policy"
msgstr "Политика представителя"

#: includes/class-step-form-submission.php:52
msgid ""
"Define how this step should be processed. If all assignees must complete "
"this step then the entry will require input from every assignee before the "
"step can be completed. If the step is assigned to a role only one user in "
"that role needs to complete the step."
msgstr ""
"Определите, как следует обработать этот шаг. Если все представители должны "
"завершить этот шаг, то запись будет требовать ввода данных от каждого "
"представителя перед этапом завершения. Если шаг назначен на роль, только "
"один пользователь с этой ролью должен закончить шаг."

#: includes/class-step-form-submission.php:57
msgid "At least one assignee must complete this step"
msgstr ""

#: includes/class-step-form-submission.php:61
msgid "All assignees must complete this step"
msgstr "Все представители должны завершить этот шаг"

#: includes/class-step-form-submission.php:70
msgid "Assignee email"
msgstr ""

#: includes/class-step-form-submission.php:74
msgid "Please submit the following form: {workflow_form_submission_link}"
msgstr ""

#: includes/class-step-form-submission.php:80
#: includes/class-step-new-entry.php:76
#: includes/class-step-update-entry.php:85
msgid "Form"
msgstr "Форма"

#: includes/class-step-form-submission.php:81
msgid "Select the form to be used for this form submission step."
msgstr ""

#: includes/class-step-form-submission.php:88
msgid ""
"Select the page to be used for the form submission. This can be the Workflow"
" Submit Page in the WordPress Admin Dashboard or you can choose a page with "
"either a Gravity Flow submit shortcode or a Gravity Forms shortcode."
msgstr ""

#: includes/class-step-form-submission.php:89
msgid "Submission Page"
msgstr ""

#: includes/class-step-form-submission.php:100
#: includes/class-step-new-entry.php:88 includes/class-step-new-entry.php:106
#: includes/class-step-update-entry.php:148
msgid "Field Mapping"
msgstr ""

#: includes/class-step-form-submission.php:104
#: includes/class-step-new-entry.php:93
#: includes/class-step-update-entry.php:152
msgid "Field"
msgstr "Поле"

#: includes/class-step-form-submission.php:105
#: includes/class-step-new-entry.php:94
#: includes/class-step-update-entry.php:153
msgid "Value"
msgstr "Значение"

#: includes/class-step-form-submission.php:108
#: includes/class-step-new-entry.php:97 includes/class-step-new-entry.php:110
#: includes/class-step-update-entry.php:156
msgid "Mapping"
msgstr ""

#: includes/class-step-form-submission.php:108
#: includes/class-step-new-entry.php:97 includes/class-step-new-entry.php:110
#: includes/class-step-update-entry.php:156
msgid ""
"Map the fields of this form to the selected form. Values from this form will"
" be saved in the entry in the selected form"
msgstr ""

#: includes/class-step-form-submission.php:159
msgid "Pending."
msgstr ""

#: includes/class-step-form-submission.php:205
#: includes/class-step-new-entry.php:382
msgid "Select a Field"
msgstr ""

#: includes/class-step-form-submission.php:212
#: includes/class-step-new-entry.php:389
msgid "Select a %s Field"
msgstr ""

#: includes/class-step-form-submission.php:220
#: includes/class-step-new-entry.php:397
msgid "Entry ID"
msgstr "ID записи"

#: includes/class-step-form-submission.php:221
#: includes/class-step-new-entry.php:398
msgid "Entry Date"
msgstr ""

#: includes/class-step-form-submission.php:222
#: includes/class-step-new-entry.php:399
msgid "User IP"
msgstr ""

#: includes/class-step-form-submission.php:223
#: includes/class-step-new-entry.php:400
msgid "Source Url"
msgstr ""

#: includes/class-step-form-submission.php:224
#: includes/class-step-new-entry.php:401
msgid "Created By"
msgstr "Создано"

#: includes/class-step-form-submission.php:261
#: includes/class-step-form-submission.php:268
#: includes/class-step-form-submission.php:288
#: includes/class-step-new-entry.php:439 includes/class-step-new-entry.php:446
#: includes/class-step-new-entry.php:466
msgid "Full"
msgstr "По ширине"

#: includes/class-step-form-submission.php:275
#: includes/class-step-new-entry.php:453
msgid "Selected"
msgstr ""

#: includes/class-step-form-submission.php:530
msgid "Open Form"
msgstr ""

#: includes/class-step-form-submission.php:585
msgid "User"
msgstr "Пользователь"

#: includes/class-step-form-submission.php:589
msgid "Email"
msgstr "Электронная почта"

#: includes/class-step-form-submission.php:593
msgid "Role"
msgstr "Роль"

#: includes/class-step-form-submission.php:610
msgid "Pending Submission"
msgstr ""

#: includes/class-step-form-submission.php:614
msgid "Complete"
msgstr "Завершено"

#: includes/class-step-form-submission.php:616
msgid "Queued"
msgstr "В очереди"

#: includes/class-step-form-submission.php:721
msgid "Default - WordPress Admin Dashboard: Workflow Submit Page"
msgstr ""

#: includes/class-step-form-submission.php:790
msgid "Processed"
msgstr ""

#: includes/class-step-new-entry.php:20 includes/class-step-new-entry.php:33
msgid "New Entry"
msgstr ""

#: includes/class-step-new-entry.php:122
msgid "Store New Entry ID"
msgstr ""

#: includes/class-step-new-entry.php:125
msgid "Store the ID of the new entry."
msgstr ""

#: includes/class-step-new-entry.php:190
msgid "Processed."
msgstr ""

#: includes/class-step-update-entry.php:20
#: includes/class-step-update-entry.php:42
#: includes/class-step-update-entry.php:203
msgid "Update an Entry"
msgstr ""

#: includes/class-step-update-entry.php:92
msgid "Action"
msgstr "Действие"

#: includes/class-step-update-entry.php:106
msgid ""
"Select the field which will contain the entry ID of the entry that will be "
"updated. This is used to lookup the entry so it can be updated."
msgstr ""

#: includes/class-step-update-entry.php:138
msgid "Approval Status Field"
msgstr ""

#: includes/class-step-update-entry.php:176
#: includes/class-step-update-entry.php:186
msgid "Assignee"
msgstr "Назначено"

#: includes/class-step-update-entry.php:241
msgid "Approval"
msgstr "Утверждение"

#: includes/class-step-update-entry.php:244
msgid "User Input"
msgstr "Ввод данных пользователем"

#: includes/class-step-update-entry.php:504
msgid "Select an assignee"
msgstr ""

#: includes/class-step-update-entry.php:510
msgid "User (created_by)"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "Gravity Flow Form Connector"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://gravityflow.io"
msgstr ""

#. Description of the plugin/theme
msgid "Form Connector Extension for Gravity Flow."
msgstr ""

#. Author of the plugin/theme
msgid "Gravity Flow"
msgstr ""
