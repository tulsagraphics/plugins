<?php
/**
 * New item form
 *
 * @package snax 1.11
 * @subpackage Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}
?>

<?php do_action( 'snax_frontend_submission_form_before_new_link' ); ?>

<input type="hidden" name="snax-add-link-item-nonce" value="<?php echo esc_attr( wp_create_nonce( 'snax-add-link-item' ) ); ?>"/>

<div class="snax-new-links">

	<p class="snax-new-links-url">
		<input class="snax-link-url" size="100" placeholder="<?php esc_attr_e( 'Paste link url', 'snax' ); ?>" />
		<span class="snax-hint">
			<?php esc_html_e( 'e.g.: http://bimber.bringthepixel.com/main/this-santa-really-can-tell-how-to-drink-and-hack-your-hangover/', 'snax' ); ?>
		</span>
	</p>

	<p class="snax-new-links-actions">
		<a href="#" class="g1-button g1-button-simple g1-button-m snax-add-link-item"><?php esc_html_e( 'Add', 'snax' ); ?></a>
	</p>
</div>

<?php do_action( 'snax_frontend_submission_form_after_new_link' ); ?>
