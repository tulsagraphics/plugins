<?php
/**
 * Snax Formats
 *
 * @package snax
 * @subpackage Formats
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

require_once 'functions.php';
require_once 'taxonomy.php';
require_once 'story/loader.php';
require_once 'lists/loader.php';
require_once 'quizzes/loader.php';
require_once 'polls/loader.php';
require_once 'meme/loader.php';
require_once 'audio/loader.php';
require_once 'video/loader.php';
require_once 'image/loader.php';
require_once 'gallery/loader.php';
require_once 'embed/loader.php';
//require_once 'link/loader.php';
