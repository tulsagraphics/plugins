<?php
/**
 * Snax Format Loader
 *
 * @package snax
 * @subpackage Formats
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

require_once 'functions.php';

if ( is_admin() ) {
	require_once 'settings.php';
}