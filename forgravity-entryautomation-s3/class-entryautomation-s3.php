<?php

namespace ForGravity\EntryAutomation\Extensions;

use ForGravity\Entry_Automation;
use ForGravity\EntryAutomation\Extension;

use Exception;
use GFAddOn;
use GFCommon;

use Aws\S3\S3Client;
use League\Flysystem\AwsS3v2\AwsS3Adapter;
use League\Flysystem\Filesystem;

/**
 * Entry Automation Amazon S3 Add-On.
 *
 * @since     1.0
 * @author    ForGravity
 * @copyright Copyright (c) 2017, Travis Lopes
 */
class S3 extends Extension {

	/**
	 * Contains an instance of this class, if available.
	 *
	 * @since  1.0
	 * @access private
	 * @var    object $_instance If available, contains an instance of this class.
	 */
	private static $_instance = null;

	/**
	 * Defines the version of Entry Automation Amazon S3 Extension.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_version Contains the version, defined from entryautomation-s3.php
	 */
	protected $_version = FG_ENTRYAUTOMATION_S3_VERSION;

	/**
	 * Defines the plugin slug.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_slug The slug used for this plugin.
	 */
	protected $_slug = 'forgravity-entryautomation-s3';

	/**
	 * Defines the main plugin file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_path The path to the main plugin file, relative to the plugins folder.
	 */
	protected $_path = 'forgravity-entryautomation-s3/entryautomation-s3.php';

	/**
	 * Defines the full path to this class file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_full_path The full path.
	 */
	protected $_full_path = __FILE__;

	/**
	 * Defines the title of this Extension.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_title The title of the Extension.
	 */
	protected $_title = 'Entry Automation Amazon S3 Extension';

	/**
	 * Defines the short title of the Extension.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_short_title The short title.
	 */
	protected $_short_title = 'Amazon S3 Extension';

	/**
	 * Contains an instance of the Amazon S3 client, if available.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    S3Client $api If available, contains an instance of the Amazon S3 Client.
	 */
	private $api = null;

	/**
	 * Get instance of this class.
	 *
	 * @since  1.0
	 * @access public
	 * @static
	 *
	 * @return S3
	 */
	public static function get_instance() {

		if ( null === self::$_instance ) {
			self::$_instance = new self;
		}

		return self::$_instance;

	}

	/**
	 * Register needed hooks.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function init() {

		parent::init();

		add_filter( 'fg_entryautomation_plugin_settings', array( $this, 'extension_settings_fields' ) );

		add_filter( 'fg_entryautomation_export_settings_fields', array( $this, 'settings_fields' ), 10, 1 );
		add_action( 'fg_entryautomation_after_export', array( $this, 'upload_file' ), 10, 3 );

	}

	/**
	 * Register extension settings.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $settings Entry Automation plugin settings.
	 *
	 * @return array
	 */
	public function extension_settings_fields( $settings ) {

		$settings[] = array(
			'title'  => esc_html__( 'Amazon S3 Settings', 'forgravity_entryautomation_s3' ),
			'fields' => array(
				array(
					'name'              => 's3AccessKey',
					'label'             => esc_html__( 'AWS Access Key ID', 'forgravity_entryautomation_s3' ),
					'type'              => 'text',
					'class'             => 'medium',
					'feedback_callback' => array( $this, 'initialize_api' ),
				),
				array(
					'name'              => 's3AccessSecret',
					'label'             => esc_html__( 'AWS Secret Access Key', 'forgravity_entryautomation_s3' ),
					'type'              => 'text',
					'class'             => 'large',
					'feedback_callback' => array( $this, 'initialize_api' ),
				),
			),
		);

		return $settings;

	}





	// # ACTION SETTINGS -----------------------------------------------------------------------------------------------

	/**
	 * Adds Export to S3 settings tab.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $fields Entry Automation settings fields.
	 *
	 * @uses   Entry_Automation::is_current_section()
	 *
	 * @return array
	 */
	public function settings_fields( $fields ) {

		// If API is not initialized, return.
		if ( ! $this->initialize_api() ) {
			return $fields;
		}

		// Add Amazon S3 section.
		$fields[] = array(
			'id'         => 'section-export-s3',
			'class'      => 'entryautomation-feed-section ' . fg_entryautomation()->is_current_section( 'section-export-s3', false, true ),
			'tab'        => array(
				'label' => esc_html__( 'S3 Settings', 'forgravity_entryautomation_s3' ),
				'icon'  => ' fa-amazon',
			),
			'dependency' => array( 'field' => 'action', 'values' => array( 'export' ) ),
			'fields'     => array(
				array(
					'name'    => 's3Enable',
					'label'   => esc_html__( 'Upload Export File', 'forgravity_entryautomation_s3' ),
					'type'    => 'checkbox',
					'onclick' => "jQuery( this ).parents( 'form' ).submit()",
					'choices' => array(
						array(
							'name'  => 's3Enable',
							'label' => esc_html__( 'Upload export file to your Amazon S3 account', 'forgravity_entryautomation_s3' ),
						),
					),
				),
				array(
					'name'       => 's3Bucket',
					'label'      => esc_html__( 'S3 Bucket', 'forgravity_entryautomation_s3' ),
					'type'       => 'select',
					'required'   => true,
					'dependency' => array( 'field' => 's3Enable', 'values' => array( '1' ) ),
					'onchange'   => "jQuery( this ).parents( 'form' ).submit()",
					'choices'    => $this->get_buckets_as_choices(),
				),
				array(
					'name'              => 's3Path',
					'label'             => esc_html__( 'Destination Folder', 'forgravity_entryautomation_s3' ),
					'type'              => 'text',
					'class'             => 'medium',
					'required'          => true,
					'default_value'     => '/',
					'feedback_callback' => array( $this, 'validate_path' ),
					'tooltip'           => sprintf(
						'<h6>%s</h6>%s',
						esc_html__( 'Destination Folder', 'forgravity_entryautomation_s3' ),
						esc_html__( 'Enter the path to the folder the export file will be uploaded to.', 'forgravity_entryautomation_s3' )
					),
				),
			),
		);

		return $fields;

	}

	/**
	 * Get S3 buckets as feed settings choices.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   AwsS3Adapter::getListBucketsIterator()
	 * @uses   GFAddOn::log_error()
	 * @uses   S3::initialize_api()
	 *
	 * @return array
	 */
	public function get_buckets_as_choices() {

		// If API is not initialized, return.
		if ( ! $this->initialize_api() ) {
			return array();
		}

		// Initialize choices array.
		$choices = array(
			array(
				'label' => esc_html__( 'Select a Bucket', 'forgravity_entryautomation_s3' ),
				'value' => '',
			),
		);

		try {

			// Get buckets.
			$buckets = $this->api->getListBucketsIterator();

		} catch ( Exception $e ) {

			// Log that we could not get buckets.
			$this->log_error( __METHOD__ . '(): Unable to get buckets; ' . $e->getMessage() );

			return $choices;

		}

		// Loop through buckets.
		foreach ( $buckets as $bucket ) {

			// Add bucket as choice.
			$choices[] = array(
				'label' => esc_html( $bucket['Name'] ),
				'value' => $bucket['Name'],
			);

		}

		return $choices;

	}





	// # ON EXPORT -----------------------------------------------------------------------------------------------------

	/**
	 * Handle uploading file upon export.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array  $task      The current Task meta.
	 * @param array  $form      The current Form object.
	 * @param string $file_path Path to export file.
	 *
	 * @uses   Filesystem::writeStream()
	 * @uses   GFAddOn::log_error()
	 * @uses   S3::initialize_api()
	 */
	public function upload_file( $task, $form, $file_path ) {

		// If Amazon S3 is not enabled for task, return.
		if ( ! rgar( $task, 's3Enable' ) ) {
			return;
		}

		/**
		 * Modify the Amazon S3 bucket before uploading.
		 *
		 * @since 1.0
		 *
		 * @param string $bucket    Amazon S3 bucket.
		 * @param string $file_path Export file path.
		 * @param array  $task      Entry Automation Task meta.
		 * @param array  $form      The Form object.
		 */
		$bucket = gf_apply_filters( [
			'fg_entryautomation_s3_bucket',
			$form['id'],
			$task['task_id'],
		], rgar( $task, 's3Bucket' ), $file_path, $task, $form );

		// Get Flysystem adapter.
		$filesystem = $this->get_filesystem( $bucket );

		// If API could not be initialized, return.
		if ( ! $filesystem ) {
			fg_entryautomation()->log_error( __METHOD__ . '(): Unable to upload export file to Amazon S3 because API could not be initialized.' );
			return;
		}

		// Get file name.
		$file_name = basename( $file_path );

		/**
		 * Modify the Amazon S3 file path before uploading.
		 *
		 * @since 1.0
		 *
		 * @param string $remote_file_path Destination file path.
		 * @param string $file_name        Export file name.
		 * @param array  $task             Entry Automation Task meta.
		 * @param array  $form             The Form object.
		 */
		$remote_file_path = gf_apply_filters( [
			'fg_entryautomation_s3_file_path',
			$form['id'],
			$task['task_id'],
		], trailingslashit( rgar( $task, 's3Path' ) ) . $file_name, $file_name, $task, $form );

		try {

			// Upload file.
			$uploaded = $filesystem->writeStream( $remote_file_path, fopen( $file_path, 'r' ) );

		} catch ( \Exception $e ) {

			// Log that file could not be uploaded.
			fg_entryautomation()->log_error( __METHOD__ . '(): Unable to upload export file to Amazon S3; ' . $e->getMessage() );

		}

		// Log that file could not be uploaded.
		if ( ! $uploaded ) {
			fg_entryautomation()->log_error( __METHOD__ . '(): Unable to upload export file to Amazon S3; ' . $e->getMessage() );
		}

	}





	// # HELPER METHODS ------------------------------------------------------------------------------------------------

	/**
	 * Validate Amazon S3 credentials.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   GFAddOn::log_debug()
	 * @uses   GFAddOn::log_error()
	 * @uses   GFAddOn::get_plugin_settings()
	 * @uses   GFAddOn::get_posted_settings()
	 * @uses   GFAddOn::is_plugin_settings()
	 * @uses   GFAddOn::is_postback()
	 * @uses   S3Client::factory()
	 * @uses   S3Client::listBuckets()
	 *
	 * @return bool|null
	 */
	public function initialize_api() {

		// If API is already initialized, return.
		if ( ! is_null( $this->api ) ) {
			return true;
		}

		// Load needed libraries.
		require_once 'includes/vendor/autoload.php';

		// Get the plugin settings.
		$settings = $this->is_plugin_settings( 'entryautomation' ) && $this->is_postback() ? $this->get_posted_settings() : fg_entryautomation()->get_plugin_settings();

		// If access key or access secret are not provided, return.
		if ( ! rgar( $settings, 's3AccessKey' ) || ! rgar( $settings, 's3AccessSecret' ) ) {
			return null;
		}

		// Log that we are validating API credentials.
		$this->log_debug( __METHOD__ . '(): Validating API info.' );

		try {

			// Initialize new Amazon S3 client.
			$s3 = S3Client::factory( array(
				'key'    => $settings['s3AccessKey'],
				'secret' => $settings['s3AccessSecret'],
			) );

			// Try getting available buckets.
			$buckets = $s3->listBuckets();

			// Log that credentials are valid.
			$this->log_debug( __METHOD__ . '(): API credentials are valid.' );

			// Assign Amazon S3 client to the class.
			$this->api = $s3;

			return true;

		} catch ( Exception $e ) {

			// Log that credentials are invalid.
			$this->log_error( __METHOD__ . '(): API credentials are invalid; ' . $e->getMessage() );

		}

		return false;

	}

	/**
	 * Get Flysystem adapter.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param string $bucket S3 bucket.
	 *
	 * @uses   S3::initialize_api()
	 *
	 * @return bool|Filesystem
	 */
	public function get_filesystem( $bucket = '' ) {

		// If API could not be initialized or bucket is not provided, return.
		if ( ! $this->initialize_api() || rgblank( $bucket ) ) {
			return false;
		}

		// Initialize Flysystem.
		$adapter    = new AwsS3Adapter( $this->api, $bucket );
		$filesystem = new Filesystem( $adapter );

		return $filesystem;

	}

}

