<?php
/**
 * Plugin Name: Entry Automation Amazon S3 Extension
 * Plugin URI: http://forgravity.com/plugins/entry-automation/
 * Description: Upload your Entry Automation export files to your Amazon S3 bucket
 * Version: 1.0
 * Author: ForGravity
 * Author URI: http://forgravity.com
 * Text Domain: forgravity_entryautomation_s3
 * Domain Path: /languages
 **/

if ( ! defined( 'FG_EDD_STORE_URL' ) ) {
	define( 'FG_EDD_STORE_URL', 'https://forgravity.com' );
}

define( 'FG_ENTRYAUTOMATION_S3_VERSION', '1.0' );
define( 'FG_ENTRYAUTOMATION_S3_EDD_ITEM_ID', 3318 );

// Initialize plugin updater.
add_action( 'init', array( 'EntryAutomation_S3_Bootstrap', 'updater' ), 0 );

// If Gravity Forms is loaded, bootstrap the Entry Automation Amazon S3 Add-On.
add_action( 'gform_loaded', array( 'EntryAutomation_S3_Bootstrap', 'load' ), 6 );

/**
 * Class EntryAutomation_S3_Bootstrap
 *
 * Handles the loading of the Entry Automation Amazon S3 Add-On.
 */
class EntryAutomation_S3_Bootstrap {

	/**
	 * If Entry Automation exists, Entry Automation S3 Add-On is loaded.
	 *
	 * @access public
	 * @static
	 */
	public static function load() {

		if ( ! function_exists( 'fg_entryautomation' ) ) {
			return;
		}

		if ( ! class_exists( '\ForGravity\EntryAutomation\EDD_SL_Plugin_Updater' ) ) {
			require_once( fg_entryautomation()->get_base_path() . '/includes/EDD_SL_Plugin_Updater.php' );
		}

		require_once( 'class-entryautomation-s3.php' );

		GFAddOn::register( '\ForGravity\EntryAutomation\Extensions\S3' );

	}

	/**
	 * Initialize plugin updater.
	 *
	 * @access public
	 * @static
	 */
	public static function updater() {

		// Get Entry Automation instance.
		$entry_automation = function_exists( 'fg_entryautomation' ) ? fg_entryautomation() : false;

		// If Entry Automation could not be retrieved, exit.
		if ( ! $entry_automation ) {
			return;
		}

		// Get license key.
		$license_key = fg_entryautomation()->get_license_key();

		new ForGravity\EntryAutomation\EDD_SL_Plugin_Updater(
			FG_EDD_STORE_URL,
			__FILE__,
			array(
				'version' => FG_ENTRYAUTOMATION_S3_VERSION,
				'license' => $license_key,
				'item_id' => FG_ENTRYAUTOMATION_S3_EDD_ITEM_ID,
				'author'  => 'ForGravity',
			)
		);

	}
}

/**
 * Returns an instance of the S3 class
 *
 * @see    S3::get_instance()
 *
 * @return bool|ForGravity\EntryAutomation\Extensions\S3
 */
function fg_entryautomation_s3() {
	return class_exists( '\ForGravity\EntryAutomation\Extensions\S3' ) ? ForGravity\EntryAutomation\Extensions\S3::get_instance() : false;
}
