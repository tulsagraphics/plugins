=== Entry Automation Amazon S3 Extension ===
Contributors: travislopes
Tags: automation, entry, export, upload, amazon, s3, submissions, gravity forms
Requires at least: 4.2
Tested up to: 4.9.2
License: GPL-3.0+
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Upload your Entry Automation export files to your Amazon S3 bucket.

== Description ==

Entry Automation Amazon S3 Extension allows your Entry Automation export tasks to be uploaded to an Amazon S3 bucket.

Entry Automation Amazon S3 Extension requires [Entry Automation](https://forgravity.com/plugins/entry-automation/) [Gravity Forms](https://forgravity.com/gravityforms).

= Requirements =

1. [Purchase and install Gravity Forms](https://forgravity.com/gravityforms)
2. WordPress 4.2+
3. Gravity Forms 1.9.14+

= Support =

If you have any problems, please contact support: https://forgravity.com/support/

== Installation ==

1.  Download the zipped file.
1.  Extract and upload the contents of the folder to your /wp-contents/plugins/ folder.
1.  Navigate to the WordPress admin Plugins page and activate the "Entry Automation Amazon S3 Extension" plugin.

== ChangeLog ==

= 1.0 =
- It's all new!