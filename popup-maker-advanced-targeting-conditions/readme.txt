=== Popup Maker - Advanced Targeting Conditions ===
Contributors: wppopupmaker, danieliser
Author URI: https://wppopupmaker.com/
Plugin URI: https://wppopupmaker.com/extensions/advanced-targeting-conditions/
Tags: 
Requires at least: 3.6
Tested up to: 4.9.4
Requires Popup Maker: 1.7.0
Stable tag: 1.4.0


== Description ==


== Changelog ==

= v1.4.0 - 2/25/2018 =
* Feature: New Conditions
  * User viewed popup, with optional settings for testing if they viewed it more/less than a specific number.
  * Keyboard modifiers allow testing if user was holding ctrl, alt or shift keys.
* Improvement: Updated for full Popup Maker v1.7 support.
  * Leveraged the new AssetCache reducing the need to load an extra JS file for this extension.
  * Autoloader
  * Upgrade routines.

= v1.3.0 - 09/01/2017 =
* Feature: New Conditions
  * HTML Element Exists
  * HTML Element Has Class
  * HTML Element Visible On Screen
* Tweak: Moved check for current URL inside the test itself for better accuracy on single page sites.
* Fix: Added check for missing variables.
* Fix: Bug in time on page condition for passive triggers (exit intent).

= v1.2.0 =
* Feature: New Conditions
  * User has more/less than x page views.
  * User has been on site more than x minutes.
  * User has visited all of selected pages.
* Fix: Bug in User Role conditions.
* Fix: Bug in Query Arg Exists condition.

= v1.1.0 =
**Requires:** Popup Maker v1.4.19+
* Feature: Moved nearly all conditions to client side (JS) which is 10x more accurate than server side checks.
* Feature: New Conditions
  * URL Ends With
  * URL Begins With
  * URL Regex Search
  * Referrer Ends With
  * Referrer Begins With
  * Referrer Regex Search
  * Browser Width (Min / Max)
  * Browser Height (Min / Max)
  * Device is Phone
  * Device is Tablet
  * Device Screen Width (Min / Max)
  * Device Screen Height (Min / Max)
  * Custom JS Based Function Checks
* Improvement: Refactored plugin to be more reliable.
* Improvement: Migrated to use the new Popup Maker v1.4+ Conditions interface.

= v1.0.3 =
* Fix: Always shows an update available.

= v1.0.2 =
* Fix: Replaced usage of deprecated filter.
* Fix: Incorrect array key in custom function filter.

= v1.0.1 =
* Added Argument Does Not Exist option.

= v1.0 =
* Initial Release