(function ($) {
    "use strict";

    window.pum_vars = window.pum_vars || {
        page: 0
    };

    var referrer = document.referrer,
        _md,
        md = function () {
            if (_md === undefined) {
                _md = new MobileDetect(window.navigator.userAgent);
            }

            return _md;
        },
        pages_viewed = PUM.getCookie('pum_alm_pages_viewed') || [],
        current_page = parseInt(pum_vars.page) || -1,
        first_activity = PUM.getCookie('pum_alm_first_activity') || false,
        last_activity = PUM.getCookie('pum_alm_last_activity') || false,
        now = new Date(),
        query_args,
        key_pressed = {
            ctrl: false,
            alt: false,
            shift: false
        },
        popup_open_counts = PUM.getCookie('pum_alm_popup_open_counts') || {};

    if (typeof pages_viewed === 'string') {
        pages_viewed = pages_viewed.split(',');
    }

    if (pages_viewed.length) {
        for (var i = 0; pages_viewed.length > i; i++) {
            pages_viewed[i] = parseInt(pages_viewed[i].trim());
        }
    }

    if (current_page >= 0 && pages_viewed.indexOf(current_page) === -1) {
        pages_viewed.push(current_page);
        $.pm_cookie('pum_alm_pages_viewed', pages_viewed.join(','), {expires: '1 month'}, '/');
    }

    // If no first activity, or the last activity is to old.
    if (!first_activity || (last_activity + 3600 < now.getTime())) {
        first_activity = new Date();
        first_activity = first_activity.getTime();
        $.pm_cookie('pum_alm_first_activity', first_activity, {expires: '1 day'}, '/');
    }

    $.pm_cookie('pum_alm_last_activity', now.getTime(), {expires: '1 day'}, '/');

    $(document).on('ready keypress scroll click touchstart mousedown mousemove', function (event) {
        now = new Date();
        $.pm_cookie('pum_alm_last_activity', now.getTime(), {expires: '1 day'}, '/');
    });

    if (typeof popup_open_counts === 'string') {
        popup_open_counts = JSON.parse(popup_open_counts);
    }

    if (popup_open_counts.length) {
        for (var i = 0; popup_open_counts.length > i; i++) {
            popup_open_counts[i] = parseInt(popup_open_counts[i].trim());
        }
    }

    /**
     * Increases popup open count.
     *
     * @param popupID
     */
    function increase_popup_open_count(popupID) {
        if (popup_open_counts[popupID] === undefined) {
            popup_open_counts[popupID] = 0;
        }

        popup_open_counts[popupID]++;

        $.pm_cookie('pum_alm_popup_open_counts', JSON.stringify(popup_open_counts), {expires: '100 years'}, '/');
    }

    $(document)
        .on('keydown', function (event) {
            if (event.which === 16) {
                key_pressed.shift = true;
            }
            if (event.which === 17) {
                key_pressed.ctrl = true;
            }
            if (event.which === 18) {
                key_pressed.alt = true;
            }
        })
        .on('keyup', function (event) {
            if (event.which === 16) {
                key_pressed.shift = false;
            }
            if (event.which === 17) {
                key_pressed.ctrl = false;
            }
            if (event.which === 18) {
                key_pressed.alt = false;
            }
        })
        .on('pumAfterOpen', '.pum', function () {
            increase_popup_open_count(PUM.getSetting(this, 'id'));
        });

    function elementInView($element, fullyInView) {
        var viewportTop = $(window).scrollTop(),
            viewportBottom = viewportTop + $(window).height(),
            elementTop = $element.offset().top,
            elementBottom = elementTop + $element.height();

        fullyInView = fullyInView || false;

        if (fullyInView) {
            return viewportTop < elementTop && elementBottom < viewportBottom;
        } else {
            return (viewportTop <= elementTop && elementTop <= viewportBottom) || (viewportTop <= elementBottom && elementBottom <= viewportBottom)
        }
    }

    function validRegex(string) {
        var isValid = true,
            regex;

        try {
            regex = new RegExp(string);
        } catch (e) {
            isValid = false;
        }

        if (!isValid) {
            alert("Invalid regular expression");
            return false;
        }

        return regex;
    }

    function escapeRegExp(str) {
        if (!str) {
            return str;
        }
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function get_browser() {
        var ua = navigator.userAgent, tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return {name: 'IE', version: (tem[1] || '')};
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\bOPR\/(\d+)/);
            if (tem !== null) {
                return {name: 'Opera', version: tem[1]};
            }
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) !== null) {
            M.splice(1, 1, tem[1]);
        }
        return {
            name: M[0],
            version: M[1]
        };
    }

    function getQueryParameters(str) {
        var qso = {},
            qs = (str || document.location.search);

        // Check for an empty query string
        if (qs === "") {
            return qso;
        }

        // Normalize the query string
        qs = qs.replace(/(^\?)/, '')
            .replace(/;/g, '&');
        while (qs.indexOf("&&") !== -1) {
            qs = qs.replace(/&&/g, '&');
        }
        qs = qs.replace(/([\&]+$)/, '');

        // Break the query string into parts
        qs = qs.split("&");

        function decode_string(n) {
            return decodeURIComponent(n);
        }

        // Build the query string object
        for (var i = 0; i < qs.length; i++) {
            var qi = qs[i].split("=");
            qi = $.map(qi, decode_string);
            if (qso[qi[0]] !== undefined) {

                // If a key already exists then make this an object
                if (typeof(qso[qi[0]]) === "string") {
                    var temp = qso[qi[0]];
                    if (qi[1] === "") {
                        qi[1] = null;
                    }
                    qso[qi[0]] = [];
                    qso[qi[0]].push(temp);
                    qso[qi[0]].push(qi[1]);

                } else if (typeof(qso[qi[0]]) === "object") {
                    if (qi[1] === "") {
                        qi[1] = null;
                    }
                    qso[qi[0]].push(qi[1]);
                }
            } else {
                // If no key exists just set it as a string
                if (qi[1] === "") {
                    qi[1] = null;
                }
                qso[qi[0]] = qi[1];
            }
        }

        return qso;
    }

    query_args = getQueryParameters();

    $.extend($.fn.popmake.conditions, {

        // region Popup
        popup_views: function (condition) {
            var popupID = parseInt(condition.settings.selected) || false,
                morethan = parseFloat(condition.settings.morethan) || false,
                lessthan = parseFloat(condition.settings.lessthan) || false,
                view_count = parseInt(popup_open_counts[popupID]) || 0;

            if (!popupID) {
                return false;
            }

            if (morethan && !lessthan) {
                return view_count > morethan;
            }

            if (lessthan && !morethan) {
                return lessthan > view_count;
            }

            if (lessthan && morethan) {
                return lessthan > view_count && view_count > morethan;
            }

            return false
        },
        // endregion

        // region URL
        url_is: function (condition) {
            var search = condition.settings.search || false,
                regex = new RegExp("^" + escapeRegExp(search) + "$") || false,
                url = document.location.toString();

            if (!search) {
                return true;
            }

            return regex.test(url);
        },
        url_contains: function (condition) {
            var search = condition.settings.search || false,
                regex = new RegExp(escapeRegExp(search)) || false,
                url = document.location.toString();

            if (!search) {
                return true;
            }

            return regex.test(url);
        },
        url_begins_with: function (condition) {
            var search = condition.settings.search || false,
                regex = new RegExp("^" + escapeRegExp(search)),
                url = document.location.toString();

            if (!search) {
                return true;
            }

            return regex.test(url);
        },
        url_ends_with: function (condition) {
            var search = condition.settings.search || false,
                regex = new RegExp(escapeRegExp(search) + "$"),
                url = document.location.toString();

            if (!search) {
                return true;
            }

            return regex.test(url);
        },
        url_regex: function (condition) {
            var search = condition.settings.search || false,
                regex = validRegex(search),
                url = document.location.toString();

            if (!search) {
                return true;
            }

            if (!regex) {
                return false;
            }

            return regex.test(url);
        },
        // endregion

        // region Query Args
        query_arg_exists: function (condition) {
            var arg = condition.settings.arg_name || false;

            if (!arg) {
                return false;
            }

            return arg in query_args;
        },
        query_arg_is: function (condition) {
            var arg = condition.settings.arg_name || false,
                value = condition.settings.arg_value || false;

            if (!arg) {
                return false;
            }

            if (typeof query_args[arg] === 'undefined') {
                return false;
            }

            return query_args[arg] === value;
        },
        // endregion

        // region Browser
        browser_is: function (condition) {
            var browsers = condition.settings.selected || [],
                i;

            for (i = 0; browsers.length > i; i++) {
                if (browsers[i] === get_browser().name) {
                    return true;
                }
            }

            return false;
        },
        browser_version: function (condition) {
            var morethan = parseFloat(condition.settings.morethan) || false,
                lessthan = parseFloat(condition.settings.lessthan) || false,
                version = parseFloat(get_browser().version);

            if (morethan && !lessthan) {
                return version > morethan;
            }

            if (lessthan && !morethan) {
                return lessthan > version;
            }

            if (lessthan && morethan) {
                return lessthan > version && version > morethan;
            }

            return false;
        },
        browser_width: function (condition) {
            var width = window.innerWidth,
                morethan = parseFloat(condition.settings.morethan) || false,
                lessthan = parseFloat(condition.settings.lessthan) || false;

            if (morethan && !lessthan) {
                return width > morethan;
            }

            if (lessthan && !morethan) {
                return lessthan > width;
            }

            if (lessthan && morethan) {
                return lessthan > width && width > morethan;
            }

            return !lessthan && !morethan;
        },
        browser_height: function (condition) {
            var height = window.innerHeight,
                morethan = parseFloat(condition.settings.morethan) || false,
                lessthan = parseFloat(condition.settings.lessthan) || false;

            if (morethan && !lessthan) {
                return height > morethan;
            }

            if (lessthan && !morethan) {
                return lessthan > height;
            }

            if (lessthan && morethan) {
                return lessthan > height && height > morethan;
            }

            return !lessthan && !morethan;
        },
        // endregion

        // region HTML
        html_element_exists: function (condition) {
            var selector = condition.settings.selector || '',
                $element = $(selector);

            return $element.length > 0;
        },
        html_element_has_class: function (condition) {
            var selector = condition.settings.selector || '',
                _class = condition.settings.class || '',
                $element = $(selector).filter('.' + _class);

            return $element.length > 0;
        },
        html_element_on_screen: function (condition) {
            var selector = condition.settings.selector || '',
                $element = $(selector),
                entirely = condition.settings.entirely !== undefined && condition.settings.entirely;

            if (!$element.length) {
                return false;
            }

            return elementInView($element, entirely);
        },
        // endregion

        // region Device
        device_is_mobile: function () {
            return md().mobile();
        },
        device_is_phone: function () {
            return md().phone();
        },
        device_is_tablet: function () {
            return md().tablet();
        },
        device_is_brand: function (condition) {
            var brands = condition.settings.selected || [],
                i;

            for (i = 0; brands.length > i; i++) {
                if (md().is(brands[i])) {
                    return true;
                }
            }

            return false;
        },
        device_screen_width: function (condition) {
            var width = window.screen.width,
                morethan = parseFloat(condition.settings.morethan) || false,
                lessthan = parseFloat(condition.settings.lessthan) || false;

            if (morethan && !lessthan) {
                return width > morethan;
            }

            if (lessthan && !morethan) {
                return lessthan > width;
            }

            if (lessthan && morethan) {
                return lessthan > width && width > morethan;
            }

            return !lessthan && !morethan;
        },
        device_screen_height: function (condition) {
            var height = window.screen.height,
                morethan = parseFloat(condition.settings.morethan) || false,
                lessthan = parseFloat(condition.settings.lessthan) || false;

            if (morethan && !lessthan) {
                return height > morethan;
            }

            if (lessthan && !morethan) {
                return lessthan > height;
            }

            if (lessthan && morethan) {
                return lessthan > height && height > morethan;
            }

            return !lessthan && !morethan;
        },
        // endregion

        // region Referrer
        referrer_is: function (condition) {
            var search = condition.settings.search || false,
                regex = new RegExp("^" + escapeRegExp(search) + "$") || false;

            if (!search) {
                return true;
            }

            return regex.test(referrer);
        },
        referrer_contains: function (condition) {
            var search = condition.settings.search || false,
                regex = new RegExp(escapeRegExp(search)) || false;

            if (!search) {
                return true;
            }

            return regex.test(referrer);
        },
        referrer_begins_with: function (condition) {
            var search = condition.settings.search || false,
                regex = new RegExp("^" + escapeRegExp(search));

            if (!search) {
                return true;
            }

            return regex.test(referrer);
        },
        referrer_ends_with: function (condition) {
            var search = condition.settings.search || false,
                regex = new RegExp(escapeRegExp(search) + "$");

            if (!search) {
                return true;
            }

            return regex.test(referrer);
        },
        referrer_regex: function (condition) {
            var search = condition.settings.search || false,
                regex = validRegex(search);

            if (!search) {
                return true;
            }

            if (!regex) {
                return false;
            }

            return regex.test(referrer);
        },
        referrer_is_search_engine: function (condition) {
            var search = condition.settings.search || [],
                regex;

            if (typeof search !== 'string') {
                search = search.join('|');
            }

            regex = validRegex(search);

            return regex.test(referrer);
        },
        referrer_is_external: function () {
            return referrer !== '' && referrer.indexOf(location.protocol + "//" + location.host) === -1;
        },
        // endregion

        // region Cookies
        cookie_exists: function (condition) {
            var arg = condition.settings.cookie_name || false;

            if (!arg) {
                return false;
            }

            return $.fn.popmake.cookie.process(arg) !== undefined;
        },
        cookie_is: function (condition) {
            var arg = condition.settings.cookie_name || false,
                value = condition.settings.cookie_value || false;

            if (!arg) {
                return false;
            }

            if ($.fn.popmake.cookie.process(arg) === undefined) {
                return false;
            }

            return $.fn.popmake.cookie.process(arg) === value;
        },
        page_views: function (condition) {
            var count = parseInt(PUM.getCookie('pum_alm_page_views')) || 0,
                morethan = parseFloat(condition.settings.morethan) || false,
                lessthan = parseFloat(condition.settings.lessthan) || false;

            if (typeof count !== 'number') {
                count = 0;
            }

            count++;

            $.pm_cookie('pum_alm_page_views', count, {expires: '1 day'}, '/');

            if (morethan && !lessthan) {
                return count > morethan;
            }

            if (lessthan && !morethan) {
                return lessthan > count;
            }

            if (lessthan && morethan) {
                return lessthan > count && count > morethan;
            }

            return !lessthan && !morethan;
        },
        time_on_site: function (condition) {
            var now = new Date(),
                morethan = parseFloat(condition.settings.morethan) || false,
                lessthan = parseFloat(condition.settings.lessthan) || false,
                time_on;

            time_on = now.getTime() - first_activity;

            if (morethan && !lessthan) {
                return time_on > (morethan * 60 * 1000);
            }

            if (lessthan && !morethan) {
                return (lessthan * 60 * 1000) > time_on;
            }

            if (lessthan && morethan) {
                return (lessthan * 60 * 1000) > time_on && time_on > (morethan * 60 * 1000);
            }

            return !lessthan && !morethan;
        },
        has_viewed_page: function (condition) {
            var required = condition.settings.selected || [];

            if (typeof required === 'string') {
                required = required.split(',');
            }

            if (required.length) {
                for (var i = 0; required.length > i; i++) {
                    var int = parseInt(required[i].trim());

                    if (int >= 0 && pages_viewed.indexOf(int) === -1) {
                        return false;
                    }
                }
            }

            return true;

        },

        // endregion

        holding_ctrl: function (condition) {
            return key_pressed.ctrl;
        },
        holding_shift: function (condition) {
            return key_pressed.shift;
        },
        holding_alt: function (condition) {
            return key_pressed.alt;
        },
        js_function: function (condition) {
            if (condition.settings.function_name === undefined || typeof window[condition.settings.function_name] !== 'function') {
                return false;
            }

            return window[condition.settings.function_name]();
        }
    });

}(jQuery));