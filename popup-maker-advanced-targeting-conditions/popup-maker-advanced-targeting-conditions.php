<?php
/**
 * Plugin Name: Popup Maker - Advanced Targeting Conditions
 * Plugin URI: https://wppopupmaker.com/extensions/advanced-targeting-conditions/
 * Description: Adds advanced targeting conditions.
 * Author: WP Popup Maker
 * Version: 1.4.0
 * Author URI: https://wppopupmaker.com/
 * Text Domain: popup-maker-advanced-targeting-conditions
 * Requires Popup Maker: 1.7.0
 *
 * @author       WP Popup Maker
 * @copyright    Copyright (c) 2016, WP Popup Maker
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @param array $autoloaders
 *
 * @return array
 */
function pum_atc_autoloader( $autoloaders = array() ) {
	return array_merge( $autoloaders, array(
		array(
			'prefix' => 'PUM_ATC_',
			'dir'    => dirname( __FILE__ ) . '/classes/',
		),
	) );
}

add_filter( 'pum_autoloaders', 'pum_atc_autoloader' );

/**
 * Class PUM_ATC
 */
class PUM_ATC {

	/**
	 * @var int $download_id for EDD.
	 */
	public static $ID = 5108;

	/**
	 * @var string
	 */
	public static $NAME = 'Advanced Targeting Conditions';

	/**
	 * @var string Plugin Version
	 */
	public static $VER = '1.4.0';

	/**
	 * @var string Required Version of Popup Maker
	 */
	public static $REQUIRED_CORE_VER = '1.7.0';

	/**
	 * @var int DB Version
	 */
	public static $DB_VER = 2;

	/**
	 * @var string Plugin Directory
	 */
	public static $URL = '';

	/**
	 * @var string Plugin URL
	 */
	public static $DIR = '';

	/**
	 * @var string Plugin FILE
	 */
	public static $FILE = '';

	/**
	 * @var self $instance
	 */
	private static $instance;

	/**
	 * @return self
	 */
	public static function instance() {
		if ( ! self::$instance ) {
			self::$instance = new self;
			self::$instance->setup_constants();
			self::$instance->load_textdomain();
			self::$instance->includes();
			self::$instance->init();
		}

		return self::$instance;
	}

	/**
	 * Set up plugin constants.
	 */
	public static function setup_constants() {
		self::$DIR  = plugin_dir_path( __FILE__ );
		self::$URL  = plugins_url( '/', __FILE__ );
		self::$FILE = __FILE__;
	}

	/**
	 * Include necessary files
	 */
	private function includes() {
	}

	/**
	 * Initialize everything
	 */
	private function init() {
		PUM_ATC_Site::init();
		PUM_ATC_Conditions::init();
		PUM_ATC_Upgrades::init();

		// Handle licensing
		if ( class_exists( 'PUM_Extension_License' ) ) {
			new PUM_Extension_License( self::$FILE, self::$NAME, self::$VER, 'WP Popup Maker', null, null, self::$ID );
		}
	}

	/**
	 * Internationalization
	 */
	public function load_textdomain() {
		load_plugin_textdomain( 'popup-maker-advanced-targeting-conditions', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}

}


/**
 * Get the ball rolling.
 */
function pum_atc_init() {
	if ( ( ! class_exists( 'Popup_Maker' ) && ! class_exists( 'PUM' ) ) || version_compare( Popup_Maker::$VER, PUM_ATC::$REQUIRED_CORE_VER, '<' ) ) {
		if ( ! class_exists( 'PUM_Extension_Activation' ) ) {
			require_once 'includes/pum-sdk/class-pum-extension-activation.php';
		}

		$activation = new PUM_Extension_Activation( plugin_dir_path( __FILE__ ), basename( __FILE__ ), PUM_ATC::$REQUIRED_CORE_VER );
		$activation->run();
	} else {
		PUM_ATC::instance();
	}
}

add_action( 'plugins_loaded', 'pum_atc_init' );

register_activation_hook( __FILE__, 'PUM_ATC_Activator::activate' );
register_deactivation_hook( __FILE__, 'PUM_ATC_Deactivator::deactivate' );