<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Class PUM_ATC_Conditions
 */
class PUM_ATC_Conditions {

	/**
	 * Initialization
	 */
	public static function init() {
		add_filter( 'pum_registered_conditions', array( __CLASS__, 'get_conditions' ) );
		add_filter( 'pum_condition_sort_order', array( __CLASS__, 'condition_sort_order' ) );
	}

	/**
	 * Merges all the Advanced Conditions into the available conditions array.
	 *
	 * @param array $conditions
	 *
	 * @return array
	 */
	public static function get_conditions( $conditions = array() ) {

		# region User conditions
		$conditions['user_is_logged_in']  = array(
			'group'    => __( 'User', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Is Logged In', 'popup-maker-advanced-targeting-conditions' ),
			'callback' => 'is_user_logged_in',
		);
		$conditions['user_has_role']      = array(
			'group'    => __( 'User', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Has Role', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'selected' => array(
					'placeholder' => __( 'Select User Roles', 'popup-maker-advanced-targeting-conditions' ),
					'type'        => 'select',
					'select2'     => true,
					'multiple'    => true,
					'as_array'    => true,
					'options'     => self::allowed_user_roles(),
				),
			),
			'callback' => array( 'PUM_ATC_ConditionCallbacks', 'user_has_role' ),
		);
		$conditions['user_has_commented'] = array(
			'group'    => __( 'User', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Has Commented', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'morethan' => array(
					'label' => 'More Than (optional)',
					'type'  => 'number',
					'std'   => 0,
				),
				'lessthan' => array(
					'label' => 'Less Than (optional)',
					'type'  => 'number',
					'std'   => 0,
				),
			),
			'callback' => array( 'PUM_ATC_ConditionCallbacks', 'user_has_commented' ),
		);

		$conditions['popup_views'] = array(
			'advanced' => true,
			'group'    => __( 'User', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Has Viewed Popup', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'selected' => array(
					'label'     => __( 'Select a popup', 'popup-maker-advanced-targeting-conditions' ),
					'type'      => 'postselect',
					'post_type' => 'popup',
					'multiple'  => false,
					'required'  => true,
				),
				'morethan' => array(
					'label' => 'More Than (optional)',
					'type'  => 'rangeslider',
					'std'   => 0,
					'min'   => 0,
					'step'  => 1,
					'max'   => 100,
					'unit'  => 'views',
				),
				'lessthan' => array(
					'label' => 'Less Than (optional)',
					'type'  => 'rangeslider',
					'std'   => 0,
					'min'   => 0,
					'step'  => 1,
					'max'   => 100,
					'unit'  => 'views',
				),
			),
		);


		$conditions['page_views'] = array(
			'advanced' => true,
			'group'    => __( 'User', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Has Viewed X Pages', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'morethan' => array(
					'label' => __( 'More Than (optional)', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'number',
					'std'   => 0,
					'min'   => 0,
					'step'  => 1,
					'max'   => 100,
					'unit'  => 'px',
				),
				'lessthan' => array(
					'label' => __( 'Less Than (optional)', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'number',
					'std'   => 0,
					'min'   => 0,
					'step'  => 1,
					'max'   => 100,
					'unit'  => 'px',
				),
			),
		);

		$conditions['has_viewed_page'] = array(
			'advanced' => true,
			'group'    => __( 'User', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Has Viewed Selected Pages', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'selected' => array(
					'label'       => __( 'Comma list of IDs', 'popup-maker-advanced-targeting-conditions' ),
					'type'        => 'text',
					'placeholder' => '41,85,199',
				),
			),
		);

		$conditions['time_on_site'] = array(
			'advanced' => true,
			'group'    => __( 'User', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Time On Site', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'morethan' => array(
					'label' => __( 'More Than (minutes, optional)', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'number',
					'std'   => 0,
					'min'   => 0,
					'step'  => 1,
					'max'   => 100,
					'unit'  => 'M',
				),
				'lessthan' => array(
					'label' => __( 'Less Than (minutes, optional)', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'number',
					'std'   => 0,
					'min'   => 0,
					'step'  => 1,
					'max'   => 100,
					'unit'  => 'M',
				),
			),
		);

		# endregion

		# region URL conditions
		$conditions['url_is']          = array(
			'advanced' => true,
			'group'    => __( 'URL', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'URL Is', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'search' => array(
					'label' => __( 'Exact URL', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'text',
				),
			),
		);
		$conditions['url_contains']    = array(
			'advanced' => true,
			'group'    => __( 'URL', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'URL Contains', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'search' => array(
					'label' => __( 'Contains', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'text',
				),
			),
		);
		$conditions['url_begins_with'] = array(
			'advanced' => true,
			'group'    => __( 'URL', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'URL Begins With', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'search' => array(
					'label' => __( 'Begins With', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'text',
				),
			),
		);
		$conditions['url_ends_with']   = array(
			'advanced' => true,
			'group'    => __( 'URL', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'URL Ends With', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'search' => array(
					'label' => __( 'Ends With', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'text',
				),
			),
		);
		$conditions['url_regex']       = array(
			'advanced' => true,
			'group'    => __( 'URL', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'URL Regex Search', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'search' => array(
					'label' => __( 'Valid RegExp', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'text',
				),
			),
		);
		# endregion

		# region Query Arg conditions
		$conditions['query_arg_exists'] = array(
			'advanced' => true,
			'group'    => __( 'URL', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Query Argument Exists', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'arg_name' => array(
					'placeholder' => __( '?argument_name=value', 'popup-maker-advanced-targeting-conditions' ),
					'label'       => __( 'Agument Name', 'popup-maker-advanced-targeting-conditions' ),
					'type'        => 'text',
				),
			),
		);
		$conditions['query_arg_is']     = array(
			'advanced' => true,
			'group'    => __( 'URL', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Query Argument Is', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'arg_name'  => array(
					'placeholder' => __( '?argument_name=value', 'popup-maker-advanced-targeting-conditions' ),
					'label'       => __( 'Argument Name', 'popup-maker-advanced-targeting-conditions' ),
					'type'        => 'text',
				),
				'arg_value' => array(
					'placeholder' => __( '?argument_name=value', 'popup-maker-advanced-targeting-conditions' ),
					'label'       => __( 'Agument Value', 'popup-maker-advanced-targeting-conditions' ),
					'type'        => 'text',
				),
			),
		);
		# endregion

		# region Referrer conditions
		$conditions['referrer_is']               = array(
			'advanced' => true,
			'group'    => __( 'Referrer', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Referrer URL Is', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'search' => array(
					'label' => __( 'URL', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'text',
				),
			),
		);
		$conditions['referrer_contains']         = array(
			'advanced' => true,
			'group'    => __( 'Referrer', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Referrer URL Contains', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'search' => array(
					'label' => __( 'Contains', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'text',
				),
			),
		);
		$conditions['referrer_begins_with']      = array(
			'advanced' => true,
			'group'    => __( 'Referrer', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Referrer URL Begins With', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'search' => array(
					'label' => __( 'Begins With', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'text',
				),
			),
		);
		$conditions['referrer_ends_with']        = array(
			'advanced' => true,
			'group'    => __( 'Referrer', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Referrer URL Ends With', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'search' => array(
					'label' => __( 'Ends With', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'text',
				),
			),
		);
		$conditions['referrer_regex']            = array(
			'advanced' => true,
			'group'    => __( 'Referrer', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Referrer Regex Search', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'search' => array(
					'label' => __( 'Valid RegExp', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'text',
				),
			),
		);
		$conditions['referrer_is_search_engine'] = array(
			'advanced' => true,
			'group'    => __( 'Referrer', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Referrer Is Search Engine', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'search' => array(
					'placeholder' => __( 'Select Search Engines', 'popup-maker-advanced-targeting-conditions' ),
					'type'        => 'select',
					'select2'     => true,
					'multiple'    => true,
					'as_array'    => true,
					'options'     => array(
						__( 'www.google.com', 'popup-maker-advanced-targeting-conditions' )   => __( 'Google', 'popup-maker-advanced-targeting-conditions' ),
						__( 'www.bing.com', 'popup-maker-advanced-targeting-conditions' )     => __( 'Bing', 'popup-maker-advanced-targeting-conditions' ),
						__( 'search.yahoo.com', 'popup-maker-advanced-targeting-conditions' ) => __( 'Yahoo', 'popup-maker-advanced-targeting-conditions' ),
					),
				),
			),
		);
		$conditions['referrer_is_external']      = array(
			'advanced' => true,
			'group'    => __( 'Referrer', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Referrer Is External URL', 'popup-maker-advanced-targeting-conditions' ),
		);
		# endregion

		# region Browser conditions
		$conditions['browser_is']      = array(
			'advanced' => true,
			'group'    => __( 'Browser', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Is Specific Browser', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'selected' => array(
					'placeholder' => __( 'Select Browsers', 'popup-maker-advanced-targeting-conditions' ),
					'type'        => 'select',
					'select2'     => true,
					'multiple'    => true,
					'as_array'    => true,
					'options'     => array(
						'Internet Explorer' => __( 'Internet Explorer' ),
						'Firefox'           => __( 'Firefox' ),
						'Safari'            => __( 'Safari' ),
						'Chrome'            => __( 'Chrome' ),
						'Android Browser'   => __( 'Android Browser' ),
						'Opera'             => __( 'Opera' ),
					),
				),
			),
		);
		$conditions['browser_version'] = array(
			'advanced' => true,
			'group'    => __( 'Browser', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Version Is', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'morethan' => array(
					'label' => 'Higher Than',
					'type'  => 'number',
				),
				'lessthan' => array(
					'label' => 'Lower Than',
					'type'  => 'number',
				),
			),
		);
		$conditions['browser_width']   = array(
			'advanced' => true,
			'group'    => __( 'Browser', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Browser Width', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'morethan' => array(
					'label' => 'More Than (optional)',
					'type'  => 'number',
					'std'   => 0,
					'min'   => 0,
					'step'  => 1,
					'max'   => 1080,
					'unit'  => 'px',
				),
				'lessthan' => array(
					'label' => 'Less Than (optional)',
					'type'  => 'number',
					'std'   => 0,
					'min'   => 0,
					'step'  => 1,
					'max'   => 1080,
					'unit'  => 'px',
				),
			),
		);
		$conditions['browser_height']  = array(
			'advanced' => true,
			'group'    => __( 'Browser', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Browser Height', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'morethan' => array(
					'label' => 'More Than (optional)',
					'type'  => 'number',
					'std'   => 0,
					'min'   => 0,
					'step'  => 1,
					'max'   => 1080,
					'unit'  => 'px',
				),
				'lessthan' => array(
					'label' => 'Less Than (optional)',
					'type'  => 'number',
					'std'   => 0,
					'min'   => 0,
					'step'  => 1,
					'max'   => 1080,
					'unit'  => 'px',
				),
			),
		);
		# endregion

		# region Device conditions
		$conditions['device_is_mobile']     = array(
			'advanced' => true,
			'group'    => __( 'Device', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Device Is Mobile', 'popup-maker-advanced-targeting-conditions' ),
		);
		$conditions['device_is_phone']      = array(
			'advanced' => true,
			'group'    => __( 'Device', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Device Is Phone', 'popup-maker-advanced-targeting-conditions' ),
		);
		$conditions['device_is_tablet']     = array(
			'advanced' => true,
			'group'    => __( 'Device', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Device Is Tablet', 'popup-maker-advanced-targeting-conditions' ),
		);
		$conditions['device_is_brand']      = array(
			'advanced' => true,
			'group'    => __( 'Device', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Device Is Brand', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'selected' => array(
					'placeholder' => __( 'Select Brands', 'popup-maker-advanced-targeting-conditions' ),
					'type'        => 'select',
					'select2'     => true,
					'multiple'    => true,
					'as_array'    => true,
					'options'     => array(
						'iPhone'     => __( 'iPhone', 'popup-maker-advanced-targeting-conditions' ),
						'BlackBerry' => __( 'BlackBerry', 'popup-maker-advanced-targeting-conditions' ),
						'HTC'        => __( 'HTC', 'popup-maker-advanced-targeting-conditions' ),
						'Nexus'      => __( 'Nexus', 'popup-maker-advanced-targeting-conditions' ),
						'Motorola'   => __( 'Motorola', 'popup-maker-advanced-targeting-conditions' ),
						'Dell'       => __( 'Dell', 'popup-maker-advanced-targeting-conditions' ),
						'Samsung'    => __( 'Samsung', 'popup-maker-advanced-targeting-conditions' ),
						'LG'         => __( 'LG', 'popup-maker-advanced-targeting-conditions' ),
						'Sony'       => __( 'Sony', 'popup-maker-advanced-targeting-conditions' ),
						'Asus'       => __( 'Asus', 'popup-maker-advanced-targeting-conditions' ),
						'Micromax'   => __( 'Micromax', 'popup-maker-advanced-targeting-conditions' ),
						'Palm'       => __( 'Palm', 'popup-maker-advanced-targeting-conditions' ),
						'Vertu'      => __( 'Vertu', 'popup-maker-advanced-targeting-conditions' ),
						'Pantech'    => __( 'Pantech', 'popup-maker-advanced-targeting-conditions' ),
						'Fly'        => __( 'Fly', 'popup-maker-advanced-targeting-conditions' ),
						'iMobile'    => __( 'iMobile', 'popup-maker-advanced-targeting-conditions' ),
						'SimValley'  => __( 'SimValley', 'popup-maker-advanced-targeting-conditions' ),
						'Wolfgang'   => __( 'Wolfgang', 'popup-maker-advanced-targeting-conditions' ),
						'Alcatel'    => __( 'Alcatel', 'popup-maker-advanced-targeting-conditions' ),
						'Nintendo'   => __( 'Nintendo', 'popup-maker-advanced-targeting-conditions' ),
						'Amoi'       => __( 'Amoi', 'popup-maker-advanced-targeting-conditions' ),
						'INQ'        => __( 'INQ', 'popup-maker-advanced-targeting-conditions' ),
						'Generic'    => __( 'Generic', 'popup-maker-advanced-targeting-conditions' ),
					),
				),
			),
		);
		$conditions['device_screen_width']  = array(
			'advanced' => true,
			'group'    => __( 'Device', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Screen Width', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'morethan' => array(
					'label' => 'More Than (optional)',
					'type'  => 'number',
					'std'   => 0,
					'min'   => 0,
					'step'  => 1,
					'max'   => 1080,
					'unit'  => 'px',
				),
				'lessthan' => array(
					'label' => 'Less Than (optional)',
					'type'  => 'number',
					'std'   => 0,
					'min'   => 0,
					'step'  => 1,
					'max'   => 1080,
					'unit'  => 'px',
				),
			),
		);
		$conditions['device_screen_height'] = array(
			'advanced' => true,
			'group'    => __( 'Device', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Screen Height', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'morethan' => array(
					'label' => 'More Than (optional)',
					'type'  => 'number',
					'std'   => 0,
					'min'   => 0,
					'step'  => 1,
					'max'   => 1080,
					'unit'  => 'px',
				),
				'lessthan' => array(
					'label' => 'Less Than (optional)',
					'type'  => 'number',
					'std'   => 0,
					'min'   => 0,
					'step'  => 1,
					'max'   => 1080,
					'unit'  => 'px',
				),
			),
		);

		/*
		array(
			__( 'Windows' )	=> 'Windows',
			__( 'Macintosh' )	=> 'Macintosh',
			__( 'Linux' )	=> 'Linux',
		)
		array(
			__( 'iOS', 'popup-maker-advanced-targeting-conditions' )	=> 'iOS',
			__( 'AndroidOS', 'popup-maker-advanced-targeting-conditions' )	=> 'AndroidOS',
			__( 'BlackBerryOS', 'popup-maker-advanced-targeting-conditions' )	=> 'BlackBerryOS',
			__( 'PalmOS', 'popup-maker-advanced-targeting-conditions' )	=> 'PalmOS',
			__( 'SymbianOS', 'popup-maker-advanced-targeting-conditions' )	=> 'SymbianOS',
			__( 'WindowsMobileOS', 'popup-maker-advanced-targeting-conditions' )	=> 'WindowsMobileOS',
			__( 'WindowsPhoneOS', 'popup-maker-advanced-targeting-conditions' )	=> 'WindowsPhoneOS',
			__( 'MeeGoOS', 'popup-maker-advanced-targeting-conditions' )	=> 'MeeGoOS',
			__( 'MaemoOS', 'popup-maker-advanced-targeting-conditions' )	=> 'MaemoOS',
			__( 'JavaOS', 'popup-maker-advanced-targeting-conditions' )	=> 'JavaOS',
			__( 'webOS', 'popup-maker-advanced-targeting-conditions' )	=> 'webOS',
			__( 'badaOS', 'popup-maker-advanced-targeting-conditions' )	=> 'badaOS',
			__( 'BREWOS', 'popup-maker-advanced-targeting-conditions' )	=> 'BREWOS',
		)
		 */
		# endregion

		# region Cookie conditions
		$conditions['cookie_exists'] = array(
			'advanced' => true,
			'group'    => __( 'Cookie', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Cookie Exists', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'cookie_name' => array(
					'label'       => __( 'Cookie Name', 'popup-maker-advanced-targeting-conditions' ),
					'placeholder' => __( 'my-cookie', 'popup-maker-advanced-targeting-conditions' ),
					'type'        => 'text',
				),
			),
		);
		$conditions['cookie_is']     = array(
			'advanced' => true,
			'group'    => __( 'Cookie', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Cookie Is', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'cookie_name'  => array(
					'label'       => __( 'Cookie Name', 'popup-maker-advanced-targeting-conditions' ),
					'placeholder' => __( 'page_count', 'popup-maker-advanced-targeting-conditions' ),
					'type'        => 'text',
				),
				'cookie_value' => array(
					'label'       => __( 'Cookie Value', 'popup-maker-advanced-targeting-conditions' ),
					'placeholder' => __( '10', 'popup-maker-advanced-targeting-conditions' ),
					'type'        => 'text',
				),
			),
		);
		# endregion

		#region HTML conditions
		$conditions['html_element_exists']    = array(
			'advanced' => true,
			'group'    => __( 'HTML', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'HTML Element Exists', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'selector' => array(
					'label' => __( 'Element Selector (jQuery/CSS)', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'text',
				),
			),
		);
		$conditions['html_element_has_class'] = array(
			'advanced' => true,
			'group'    => __( 'HTML', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'HTML Element Has Class', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'selector' => array(
					'label' => __( 'Element Selector (jQuery/CSS)', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'text',
				),
				'class'    => array(
					'label' => __( 'Class to test for', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'text',
				),
			),
		);
		$conditions['html_element_on_screen'] = array(
			'advanced' => true,
			'group'    => __( 'HTML', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'HTML Element On Screen', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'selector' => array(
					'label' => __( 'Element Selector (jQuery/CSS)', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'text',
				),
				'entirely' => array(
					'label' => __( 'Only if element is completely visible on screen', 'popup-maker-advanced-targeting-conditions' ),
					'type'  => 'checkbox',
				),
			),
		);
		#endregion


		# region Keyboard/mouse conditions
		$conditions['holding_ctrl']  = array(
			'advanced' => true,
			'group'    => __( 'Keyboard', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Holding Ctrl', 'popup-maker-advanced-targeting-conditions' ),
		);
		$conditions['holding_shift'] = array(
			'advanced' => true,
			'group'    => __( 'Keyboard', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Holding Shift', 'popup-maker-advanced-targeting-conditions' ),
		);
		$conditions['holding_alt']   = array(
			'advanced' => true,
			'group'    => __( 'Keyboard', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Holding ALt', 'popup-maker-advanced-targeting-conditions' ),
		);
		#endregion


		# region Function conditions
		$conditions['php_function'] = array(
			'group'    => __( 'Custom', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Custom PHP Function', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'function_name' => array(
					'label'       => __( 'Custom Function Name', 'popup-maker-advanced-targeting-conditions' ),
					'placeholder' => __( 'user_is_logged_in', 'popup-maker-advanced-targeting-conditions' ),
					'type'        => 'text',
				),
			),
			'callback' => array( 'PUM_ATC_ConditionCallbacks', 'php_function' ),
		);
		$conditions['js_function']  = array(
			'advanced' => true,
			'group'    => __( 'Custom', 'popup-maker-advanced-targeting-conditions' ),
			'name'     => __( 'Custom JS Function', 'popup-maker-advanced-targeting-conditions' ),
			'fields'   => array(
				'function_name' => array(
					'label'       => __( 'Custom Function Name', 'popup-maker-advanced-targeting-conditions' ),
					'placeholder' => __( 'callback_function', 'popup-maker-advanced-targeting-conditions' ),
					'type'        => 'text',
				),
			),
		);

		# endregion

		return $conditions;
	}

	/**
	 * Gets a filterable array of the allowed user roles.
	 *
	 * @return array|mixed
	 */
	public static function allowed_user_roles() {
		global $wp_roles;

		static $roles;

		if ( ! isset( $roles ) ) {
			$roles = apply_filters( 'pum_atc_user_roles', $wp_roles->role_names );

			if ( ! is_array( $roles ) || empty( $roles ) ) {
				$roles = array();
			}

		}

		return $roles;
	}

	/**
	 * Allows sorting of the advanced condition groups.
	 *
	 * @param array $order
	 *
	 * @return array
	 */
	public static function condition_sort_order( $order = array() ) {

		$order[ __( 'User', 'popup-maker-advanced-targeting-conditions' ) ]     = 20;
		$order[ __( 'URL', 'popup-maker-advanced-targeting-conditions' ) ]      = 20;
		$order[ __( 'Referrer', 'popup-maker-advanced-targeting-conditions' ) ] = 20;
		$order[ __( 'Browser', 'popup-maker-advanced-targeting-conditions' ) ]  = 20;
		$order[ __( 'Device', 'popup-maker-advanced-targeting-conditions' ) ]   = 20;
		$order[ __( 'Keyboard', 'popup-maker-advanced-targeting-conditions' ) ] = 20;
		$order[ __( 'Custom', 'popup-maker-advanced-targeting-conditions' ) ]   = 20;

		return $order;
	}

}
