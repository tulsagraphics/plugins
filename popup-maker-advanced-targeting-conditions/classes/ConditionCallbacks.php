<?php
/*******************************************************************************
 * Copyright (c) 2018, WP Popup Maker
 ******************************************************************************/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Class PUM_ATC_ConditionCallbacks
 */
class PUM_ATC_ConditionCallbacks {

	/**
	 * Checks if a user has one of the selected roles.
	 *
	 * @param array $condition
	 *
	 * @return bool
	 */
	public static function user_has_role( $condition = array() ) {

		if ( ! is_user_logged_in() ) {
			return false;
		} elseif ( empty( $condition['settings']['selected'] ) ) {
			return true;
		}

		$selected = $condition['settings']['selected'];

		// Get Enabled Roles to check for.
		$user_roles     = array_keys( PUM_ATC_Conditions::allowed_user_roles() );
		$required_roles = array_intersect( $user_roles, $selected );

		if ( empty( $required_roles ) ) {
			return true;
		}

		$check = false;
		foreach ( $required_roles as $role ) {
			if ( current_user_can( $role ) ) {
				$check = true;
				break;
			}
		}

		return $check;
	}

	/**
	 * Checks if user has commented.
	 *
	 * Accepts more than & less than arguments as well.
	 *
	 * @param array $condition
	 *
	 * @return bool
	 */
	public static function user_has_commented( $condition = array() ) {
		if ( ! is_user_logged_in() ) {
			return false;
		}

		$settings = $condition['settings'];
		$morethan = $settings['morethan'];
		$lessthan = $settings['lessthan'];


		$user_ID = get_current_user_id();
		$args    = array(
			'user_id' => $user_ID, // use user_id
			'count'   => true //return only the count
		);

		$comments = get_comments( $args );

		if ( $morethan && ! $lessthan ) {
			return $comments > $morethan;
		}

		if ( $lessthan && ! $morethan ) {
			return $lessthan > $comments;
		}

		if ( $lessthan && $morethan ) {
			return $lessthan > $comments && $comments > $morethan;
		}

		return $comments > 0;
	}

	/**
	 * Calls a custom function by name and returns a boolean representation.
	 *
	 * @param array $condition
	 *
	 * @return bool
	 */
	public static function php_function( $condition = array() ) {
		if ( ! empty( $condition['settings']['function_name'] ) && is_callable( $condition['settings']['function_name'] ) ) {
			return (bool) call_user_func( $condition['settings']['function_name'] );
		}

		return false;
	}

}
