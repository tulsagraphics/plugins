<?php
/*******************************************************************************
 * Copyright (c) 2018, WP Popup Maker
 ******************************************************************************/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class PUM_ATC_Site
 */
class PUM_ATC_Site {

	/**
	 *
	 */
	public static function init() {
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'assets' ) );
		add_filter( 'pum_generated_js', array( __CLASS__, 'generated_js' ) );
		add_action( 'pum_preload_popup', array( __CLASS__, 'enqueue_popup_assets' ) );
		add_filter( 'pum_vars', array( __CLASS__, 'pum_vars' ) );
	}

	/**
	 * Enqueue assets if AssetCache is disabled.
	 */
	public static function assets() {
		if ( ! PUM_AssetCache::writeable() ) {
			wp_register_script( 'pum-atc', PUM_ATC::$URL . '/assets/js/pum-atc-site' . PUM_Site_Assets::$suffix . '.js', array( 'jquery', 'popup-maker-site' ), PUM_ATC::$VER, true );
			wp_localize_script( 'pum-atc', 'pum_atc_vars', array(
				'page' => self::get_page_id(),
			) );
		}
	}

	/**
	 * @param array $js
	 *
	 * @return array
	 */
	public static function generated_js( $js = array() ) {
		$js['atc'] = array(
			'content'  => file_get_contents( PUM_ATC::$DIR . '/assets/js/pum-atc-site' . PUM_Site_Assets::$suffix . '.js' ),
			'priority' => 5,
		);

		return $js;
	}

	/**
	 * @param int $popup_id
	 */
	public static function enqueue_popup_assets( $popup_id = 0 ) {

		$popup = pum_get_popup( $popup_id );

		if ( ! pum_is_popup( $popup ) || ! wp_script_is( 'pum-atc', 'registered' ) ) {
			return;
		}

		$mobile_detect_conditions = array(
			'device_is_mobile',
			'device_is_phone',
			'device_is_tablet',
			'device_is_brand',
		);

		if ( $popup->has_conditions( array( 'js_only' => true ) ) ) {
			wp_enqueue_script( 'pum-atc' );

			foreach ( $popup->get_conditions( array( 'js_only' => true ) ) as $condition ) {
				if ( in_array( $condition['target'], $mobile_detect_conditions ) ) {
					wp_enqueue_script( 'mobile-detect' );
				}
			}
		}
	}

	/**
	 * @param array $vars
	 *
	 * @return array
	 */
	public static function pum_vars( $vars = array() ) {
		return array_merge( $vars, array(
			'page' => self::get_page_id(),
		) );
	}

	/**
	 * @return false|int
	 */
	public static function get_page_id() {
		if ( is_home() || is_front_page() ) {
			return 0;
		} else if ( is_single() ) {
			return get_the_ID();
		}
		return - 1;
	}

}
