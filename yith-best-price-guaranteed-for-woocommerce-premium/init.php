<?php
/**
 * Plugin Name: YITH Best Price Guaranteed for WooCommerce
 * Plugin URI: http://yithemes.com/themes/plugins/yith-best-price-guaranteed-for-woocommerce
 * Description: Enable price matching to let your users suggest a lower price found for the same product elsewhere and guarantee always the best price
 * Version: 1.2.4
 * Author: YITHEMES
 * Author URI: http://yithemes.com/
 * Text Domain: yith-best-price-guaranteed-for-woocommerce
 * Domain Path: /languages/
 * WC requires at least: 3.2.0
 * WC tested up to: 3.4.x
 *
 * @author  YIThemes
 * @package YITH Best Price Guaranteed for WooCommerce Premium
 * @version 1.2.4
 */
/*  Copyright 2018  Your Inspiration Themes  (email : plugins@yithemes.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/



if ( !function_exists( 'is_plugin_active' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}

if ( !function_exists( 'yith_plugin_registration_hook' ) ) {
    require_once 'plugin-fw/yit-plugin-registration-hook.php';
}
register_activation_hook( __FILE__, 'yith_plugin_registration_hook' );

function yith_wcbpg_pr_install_woocommerce_admin_notice() {
    ?>
    <div class="error">
        <p><?php _e( 'YITH Best Price Guaranteed for WooCommerce Premium is enabled but not effective. It requires WooCommerce in order to work.', 'yit' ); ?></p>
    </div>
    <?php
}

if ( !defined( 'YITH_WCBPG_VERSION' ) ) {
    define( 'YITH_WCBPG_VERSION', '1.2.4' );
}

if ( !defined( 'YITH_WCBPG_PREMIUM' ) ) {
    define( 'YITH_WCBPG_PREMIUM', '1' );
}

if ( !defined( 'YITH_WCBPG_INIT' ) ) {
    define( 'YITH_WCBPG_INIT', plugin_basename( __FILE__ ) );
}

if ( !defined( 'YITH_WCBPG' ) ) {
    define( 'YITH_WCBPG', true );
}

if ( !defined( 'YITH_WCBPG_FILE' ) ) {
    define( 'YITH_WCBPG_FILE', __FILE__ );
}

if ( !defined( 'YITH_WCBPG_URL' ) ) {
    define( 'YITH_WCBPG_URL', plugin_dir_url( __FILE__ ) );
}

if ( !defined( 'YITH_WCBPG_DIR' ) ) {
    define( 'YITH_WCBPG_DIR', plugin_dir_path( __FILE__ ) );
}

if ( !defined( 'YITH_WCBPG_TEMPLATE_PATH' ) ) {
    define( 'YITH_WCBPG_TEMPLATE_PATH', YITH_WCBPG_DIR . 'templates/' );
}

if ( !defined( 'YITH_WCBPG_ASSETS_URL' ) ) {
    define( 'YITH_WCBPG_ASSETS_URL', YITH_WCBPG_URL . 'assets' );
}

if ( !defined( 'YITH_WCBPG_ASSETS_PATH' ) ) {
    define( 'YITH_WCBPG_ASSETS_PATH', YITH_WCBPG_DIR . 'assets' );
}

if ( !defined( 'YITH_WCBPG_INCLUDES_PATH' ) ) {
    define( 'YITH_WCBPG_INCLUDES_PATH', YITH_WCBPG_DIR . 'includes' );
}

if ( !defined( 'YITH_WCBPG_SLUG' ) ) {
    define( 'YITH_WCBPG_SLUG', 'yith-best-price-guaranteed-for-woocommerce' );
}

if ( !defined( 'YITH_WCBPG_SECRET_KEY' ) ) {
    define( 'YITH_WCBPG_SECRET_KEY', 'F1r0ZiYQ1DZom3MiDg9J' );
}


if( !defined( 'YITH_YWBPG_ASSETS_DIR' ) ){
    define( 'YITH_YWBPG_ASSETS_DIR', YITH_WCBPG_DIR . 'assets' );
}


function yith_wcbpg_pr_init(){
    load_plugin_textdomain( 'yith-best-price-guaranteed-for-woocommerce', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    require_once ( 'includes/class.yith-wcbpg.php' );
    require_once ( 'includes/class.yith-wcbpg-register-enqueue-scripts.php');
    require_once ( 'includes/class.yith-wcbpg-admin.php' );
    require_once ( 'includes/class.yith-wcbpg-best-price.php' );
    require_once ( 'includes/class.yith-wcbpg-frontend.php' );
    require_once ( 'includes/class.yith-wcbpg-notifier.php');
    require_once ( 'includes/class.yith-wcbpg-ajax.php' );
    require_once ( 'includes/class.yith-wcbpg-cart.php' );
    if( WC()->version >= '3.4.0' ){
        require_once ( 'includes/class.yith-wcbpg-privacy.php' );
    }



    YITH_WCBPG();

}
add_action( 'yith_wcbpg_pr_init','yith_wcbpg_pr_init' );

function yith_wcbpg_pr_install(){
    if( !function_exists('WC') ){
        add_action('admin_notices','yith_wcbpg_pr_install_woocommerce_admin_notice');
    }
    do_action('yith_wcbpg_pr_init');
}

add_action('plugins_loaded','yith_wcbpg_pr_install',11);

/* Plugin Framework Version Check */
if ( !function_exists( 'yit_maybe_plugin_fw_loader' ) && file_exists( plugin_dir_path( __FILE__ ) . 'plugin-fw/init.php' ) ) {
    require_once( plugin_dir_path( __FILE__ ) . 'plugin-fw/init.php' );
}
yit_maybe_plugin_fw_loader( plugin_dir_path( __FILE__ ) );