(function($) {

    var addToCartVariationsForm = $('form.variations_form');

    /******************************************************************
     /* OPEND AND CLOSE POPUP
     *****************************************************************/
    //----- OPEN
    $(document).on('click','[data-popup-open]', function(e)  {

        if( addToCartVariationsForm.find('button[type=submit]').is('.disabled') ){
            alert(yith_wcbpg_frontend.select_variation);
            e.preventDefault();
            return;
        }

        var productID = jQuery(this).attr('data-id');
        var variationID = addToCartVariationsForm.find('.variation_id').val();
        var postData = {
            product_id  : productID,
            variation_id : variationID,
            action      : 'yith_wcbpg_set_content_modal',
            context     : 'frontend'
        };
        $.ajax( {
            type    : "POST",
            data    : postData,
            url     : yith_wcbpg_frontend.ajaxurl,
            success : function ( response ) {
                $('#wcbpg-popup').html(response);
            },
            complete: function () {

            },
            error: function(){

            }
        } );
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

        e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click');

    $(document).on('click','[data-popup-close]', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
        $('#wcbpg-popup').empty();
        e.preventDefault();
    });

    $(document).click(function(e) {
        if ( $('.popup-inner').length != 0 &&  !$(e.target).closest('.popup-inner').length) {
            $('[data-popup-close]').trigger('click');
        }
    });

    /******************************************************************
    /* VALIDATE FORM FIELDS
     *****************************************************************/
    var price_decimal_separator = yith_wcbpg_frontend.woocommerce_price_decimal_separator;
    var charcode_price_decimal_separator = price_decimal_separator.charCodeAt(0);

    $(document).on('keypress','#wcbpg_product_price',function (event) {
        return isNumber(event, this)
    });

    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode != charcode_price_decimal_separator || $(element).val().indexOf(price_decimal_separator) != -1) &&    
            (charCode < 48 || charCode > 57)
        ){
            $(element).parents('.wrap-input').addClass('error');
            $(element).parent().append('<span class="tooltiptext">' + yith_wcbpg_frontend.price_tooltip_error_message +'</span>');
            return false;
        }else{
            $(element).parents('.wrap-input').removeClass('error');
            $(element).parent().find('.tooltiptext').remove();
            return true;
        }

    }

    $(document).on('focusout change','input:not([type="submit"],#wcbg_product_id)',function() {
        if ( $(this).hasClass('required') && (  ($(this).val() == '' && !$(this).is('#wcbpg_gdpr_checkbox')) || ( $(this).is('#wcbpg_gdpr_checkbox') && !$(this).is(':checked')) ) ) {
            if (!$(this).parents('.wrap-input').hasClass('error')) {
                $(this).parents('.wrap-input').addClass('error');
                $(this).attr("placeholder", yith_wcbpg_frontend.required_field)
            }
        }else {
            if ($(this).is('#wcbpg_user_email')) {
                var email = $(this).val();
                var is_valid = validateEmail(email);
                if( is_valid == true ){
                    $(this).parents('.wrap-input').removeClass('error');
                }else{
                    $(this).parents('.wrap-input').addClass('error');
                }
            }
            else{
                if ($(this).parents('.wrap-input').hasClass('error')) {
                    $(this).parents('.wrap-input').removeClass('error');
                }
            }

        }
    });

    var validateEmail = function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    var validateFormFields = function( form ){
        var error = false;
        form.find('input:not([type="submit"])') .each(function(){
            if( $(this).parents('.wrap-input').hasClass('error') ){
                error = true;
            }else if( $(this).hasClass('required') && (  ($(this).val() == '' && !$(this).is('#wcbpg_gdpr_checkbox')) || ( $(this).is('#wcbpg_gdpr_checkbox') && !$(this).is(':checked')) ) ){
                $(this).parents('.wrap-input').addClass('error');
                error = true;
            }
        });
        return error;
    };

    /*
     End validate form fields
     */

    var formBestPrice = $( "form#best-price" );

    var nChange = 1;
    // $('#wcbpg_open_popup').on( 'click', function() {
    //     var variationID = $('.woocommerce-variation-add-to-cart').find('input.variation_id').val();
    //     formBestPrice.find('#wcbg_product_id').attr('value',variationID);
    // });

    $(document).on('submit','form#best-price',function(e){
        e.preventDefault();
        var popup = $('#wcbpg-popup');
        var error = validateFormFields($(this));
        if( error == true ){
            alert(yith_wcbpg_frontend.error_fields);
        }else{
            var formData = $(this).serialize();
            var postData = {
                formData: formData,
                action: 'yith_wcbpg_insert_post',
                context: 'frontend'
            };
            block_params    = {
                message        : null,
                overlayCSS     : {
                    background: '#000',
                    opacity   : 0.6
                },
                ignoreIfBlocked: true
            };
            popup.block(block_params);
            $.ajax( {
                type    : "POST",
                data    : postData,
                url     : yith_wcbpg_frontend.ajaxurl,
                success : function ( response ) {
                    popup.unblock();
                    var html = '<div class="wcbpg-success-box">'+ yith_wcbpg_frontend.success +'</div><a class="popup-close" data-popup-close="wcbpg-popup" href="#">x</a>';
                    var popup_inner = popup.find('.popup-inner');
                    $(popup_inner).fadeOut('slow', function() {
                        $(popup_inner).html(html).css("padding", "30px").fadeIn('slow');
                    });
                },
                complete: function () {

                },
                error: function(){
                    alert(yith_wcbpg_frontend.ajaxurl.error);
                }
            } );
        }



    });

})(jQuery);



