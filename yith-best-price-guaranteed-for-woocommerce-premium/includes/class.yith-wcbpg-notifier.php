<?php
/**
 * Main class
 *
 * @author  Yithemes
 * @package YITH Best Price Guaranteed for WooCommerce
 * @version 1.0.0
 */
if ( !defined( 'YITH_WCBPG' ) ) {
    exit;
} // Exit if accessed directly

if( !class_exists( 'YITH_WCBPG_Notifier' ) ) {
    class YITH_WCBPG_Notifier{
        /**
         * Single instance of the class
         *
         * @var \YITH_WCBPG
         * @since 1.0.0
         */
        protected static $instance;


        /**
         * Returns single instance of the class
         *
         * @return \YITH_WCBPG
         * @since 1.0.0
         */
        public static function get_instance()
        {
            if (is_null(self::$instance)) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        public function __construct()
        {
            add_filter( 'woocommerce_email_classes', array( $this, 'add_email_classes' ) );
            /**
             * Add an email action for sending the best priced emails
             */
            add_filter( 'woocommerce_email_actions', array( $this, 'add_best_price_guaranteed_trigger_action' ) );

        }


        /**
         * add email classes to woocommerce
         *
         * @param array $emails
         *
         * @return array
         *
         * @access public
         * @since  1.0.0
         * @author Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function add_email_classes( $emails ){
            $emails[ 'yith-wcbpg-new-best-price-admin' ]   =   include( YITH_WCBPG_DIR . 'includes/emails/class.yith-wcbpg-admin-notification.php' );
            $emails[ 'yith-wcbpg-new-coupon-user' ]   =   include( YITH_WCBPG_DIR . 'includes/emails/class.yith-wcbpg-user-coupon.php' );
            return $emails;
        }


        /**
         * Add an email action for sending notifications to admin and customers
         *
         * @param array $actions list of current actions
         *
         * @return array
         */
        function add_best_price_guaranteed_trigger_action( $actions ) {
            //  Add trigger action for sending digital gift card
            $actions[] = 'yith-wcbpg-new-best-price-admin';
            $actions[] = 'yith-wcbpg-new-coupon-user';

            return $actions;
        }
        

    }
}


/**
 * Unique access to instance of YITH_WCBPG_Notifier class
 *
 * @return \YITH_WCBPG_Notifier
 * @since 1.0.0
 */
function YITH_WCBPG_Notifier() {
    return YITH_WCBPG_Notifier::get_instance();
}