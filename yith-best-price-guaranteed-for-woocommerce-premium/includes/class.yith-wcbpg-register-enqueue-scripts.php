<?php
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if( !class_exists( 'YITH_WCBPG_Register_Enqueue_Scripts' ) ){
    class YITH_WCBPG_Register_Enqueue_Scripts{

        /**
         * Single instance of the class
         *
         * @var \YITH_WCBPG_Register_Enqueue_Scripts
         * @since 1.0.0
         */
        protected static $instance;

        /**
         * Returns single instance of the class
         *
         * @return \YITH_WCBPG_Register_Enqueue_Scripts
         * @since 1.0.0
         */
        public static function get_instance()
        {
            if (is_null(self::$instance)) {
                self::$instance = new self;
            }

            return self::$instance;
        }
        

        /*
         * Register and enqueue all frontend scripts for plugin
         */
        public static function enqueue_frontend_scripts(){
            $suffix = (defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
            wp_register_style( 'yith-wcbpg-frontend',YITH_WCBPG_ASSETS_URL. '/css/frontend.css',array(),'1.0.0','all' );
            wp_register_script( 'yith-wcbpg-frontend', YITH_WCBPG_ASSETS_URL. '/js/frontend' . $suffix . '.js',array('jquery','jquery-blockui'),YITH_WCBPG_VERSION,true );

            wp_enqueue_style( 'yith-wcbpg-frontend' );
            wp_enqueue_script( 'yith-wcbpg-frontend' );

            $inline_style = self::get_inline_style();
            wp_add_inline_style( 'yith-wcbpg-frontend', $inline_style );
        }

        /*
         * eRegister and enqueue all admin scripts for the plugin
         */
        public static function enqueue_admin_scripts(){
            $suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
            wp_register_style( 'yith-wcbpg-admin',YITH_WCBPG_ASSETS_URL. '/css/admin.css',array(),YITH_WCBPG_VERSION,'all' );
            wp_register_script( 'yith-wcbpg-admin', YITH_WCBPG_ASSETS_URL. '/js/admin' . $suffix . '.js',array('jquery','jquery-blockui'),YITH_WCBPG_VERSION,true );

            wp_enqueue_style( 'yith-wcbpg-admin' );
            wp_enqueue_script( 'yith-wcbpg-admin' );
        }

        public static function get_inline_style(){
            return '
                #wcbpg-popup .popup-inner {background: '. get_option('yith_wcbpg_popup_background_color') .'}
                #wcbpg-popup input {border-color: '. get_option('yith_wcbpg_input_border_color') .'; background-color: '. get_option('yith_wcbpg_input_background_color') .'}
                #wcbpg-popup label {color: '. get_option('yith_wcbpg_label_color') .'}
                #wcbpg-popup input#wcbpg_send_price {color: '. get_option('yith_wcbpg_button_color') .'; background-color: '. get_option('yith_wcbpg_button_background_color') .'}
                #wcbpg-popup .popup-close{ color: '. get_option('yith_wcbpg_close_button_color') .'; background-color: '. get_option('yith_wcbpg_close_button_background_color') .' }
                #wcbpg-popup h3.title{color: '. get_option('yith_wcbpg_popup_title_color').'; background-color: '. get_option('yith_wcbpg_popup_title_background_color').'; border-bottom: 1px solid '. get_option('yith_wcbpg_popup_title_border_color').' }
                #wcbpg-popup .popup-inner form{background-color: '. get_option('yith_wcbpg_popup_form_background_color').'; border: 1px solid '. get_option('yith_wcbpg_popup_form_border_color') .'}
                #wcbpg-popup .popup-inner .product-info{border: 1px solid '. get_option('yith_wcbpg_popup_product_info_border_color') .'}
                #wcbpg-popup .popup-inner .product-info h4.product-name{color: '. get_option('yith_wcbpg_popup_product_name_color') .'}
            ';
        }

    }
}