<?php
/**
 * Main class
 *
 * @author  Yithemes
 * @package YITH Best Price Guaranteed for WooCommerce
 * @version 1.0.0
 */
if (!defined('YITH_WCBPG')) {
    exit;
} // Exit if accessed directly

if (!class_exists('YITH_WCBPG_Admin')) {
    class YITH_WCBPG_Admin
    {
        /**
         * Single instance of the class
         *
         * @var \YITH_WCBPG_Admin
         * @since 1.0.0
         */
        protected static $instance;

        /**
         * @var $_panel Panel Object
         */
        protected $_panel;


        /**
         * @var string panel page
         */
        protected $_panel_page = 'yith_wcbpg_panel';


        public $best_price;

        /**
         * @var string Post Type name
         */
        protected $_post_type_name = 'yith_best_price';

        /**
         * Returns single instance of the class
         *
         * @return \YITH_WCBPG_Admin
         * @since    1.0.0
         * @author   Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public static function get_instance()
        {
            if (is_null(self::$instance)) {
                self::$instance = new self;
            }

            return self::$instance;
        }


        /**
         * Constructor
         *
         * @since    1.0.0
         * @return  mixed
         * @author   Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function __construct()
        {
            $post_type_name = $this->_post_type_name;
            add_action('admin_menu', array($this, 'register_panel'), 5);
            add_action('init', array($this, 'set_custom_post_type'));
            add_action('add_meta_boxes', array($this, 'add_meta_box_to_cpt'), 10);
            add_filter("manage_{$post_type_name}_posts_columns", array($this, 'set_best_prices_columns'));
            add_action("manage_{$post_type_name}_posts_custom_column", array(
                $this,
                'manage_best_prices_column'
            ), 10, 2);
            add_action('admin_enqueue_scripts', array($this, 'localize_scripts'));

            // register plugin to licence/update system
            add_action('wp_loaded', array($this, 'register_plugin_for_activation'), 99);
            add_action('admin_init', array($this, 'register_plugin_for_updates'));

            // Add Capabilities to Administrator and Shop Manager
            add_action('init', array($this, 'add_capabilities'), 20);

            // Edit saved data
            add_action('save_post',array($this,'edit_saved_data'));
        }

        /**
         * Post type name getter
         *
         * @return string The post type name
         */
        public function get_post_type_name()
        {
            return $this->_post_type_name;
        }

        /**
         * Add a panel under YITH Plugins tab
         *
         * @return   void
         * @since    1.0
         * @author   Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         * @use      /Yit_Plugin_Panel class
         * @see      plugin-fw/lib/yit-plugin-panel.php
         */
        public function register_panel()
        {

            if (!empty($this->_panel)) {
                return;
            }

            $admin_tabs = array(
                'settings' => __('Settings', 'yith-best-price-guaranteed-for-woocommerce'),
                'labels' => __('Labels & Messages', 'yith-best-price-guaranteed-for-woocommerce'),
                'colors' => __('Colors', 'yith-best-price-guaranteed-for-woocommerce')
            );

            $args = array(
                'create_menu_page' => true,
                'parent_slug' => '',
                'page_title' => _x('Best Price Guaranteed', 'plugin name in admin page title', 'yith-best-price-guaranteed-for-woocommerce'),
                'menu_title' => _x('Best Price Guaranteed', 'plugin name in admin WP menu', 'yith-best-price-guaranteed-for-woocommerce'),
                'capability' => 'manage_options',
                'page' => $this->_panel_page,
                'parent' => '',
                'parent_page' => 'yit_plugin_panel',
                'admin-tabs' => $admin_tabs,
                'options-path' => YITH_WCBPG_DIR . '/plugin-options'
            );

            /* === Fixed: not updated theme  === */
            if (!class_exists('YIT_Plugin_Panel_WooCommerce')) {
                require_once('../plugin-fw/lib/yit-plugin-panel-wc.php');
            }

            $this->_panel = new YIT_Plugin_Panel_WooCommerce($args);
        }


        /**
         * Register custom post type Best Price
         *
         * @return void
         * @since    1.0.0
         * @author   Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */

        public function set_custom_post_type()
        {
            $labels = array(
                'name' => _x('Best Price', 'post type general name', 'yith-best-price-guaranteed-for-woocommerce'),
                'singular_name' => _x('Best Price', 'post type singular name', 'yith-best-price-guaranteed-for-woocommerce'),
                'menu_name' => _x('Best Price', 'admin menu', 'yith-best-price-guaranteed-for-woocommerce'),
                'name_admin_bar' => _x('Best Price', 'add new on admin bar', 'yith-best-price-guaranteed-for-woocommerce'),
                'edit_item' => __('Edit Best Price', 'yith-best-price-guaranteed-for-woocommerce'),
                'view_item' => __('View Best Price', 'yith-best-price-guaranteed-for-woocommerce'),
                'all_items' => __('All Best Prices', 'yith-best-price-guaranteed-for-woocommerce'),
                'search_items' => __('Search Best Prices', 'yith-best-price-guaranteed-for-woocommerce'),
                'parent_item_colon' => __('Parent Best Prices:', 'yith-best-price-guaranteed-for-woocommerce'),
                'not_found' => __('No best prices found.', 'yith-best-price-guaranteed-for-woocommerce'),
                'not_found_in_trash' => __('No best prices found in Trash.', 'yith-best-price-guaranteed-for-woocommerce')
            );

            $args = array(
                'labels' => $labels,
                'description' => __('Description.', 'yith-best-price-guaranteed-for-woocommerce'),
                'public' => true,
                'publicly_queryable' => false,
                'show_ui' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => array('slug' => $this->_post_type_name),
                'has_archive' => true,
                'hierarchical' => false,
                'menu_icon' => 'dashicons-smiley',
                'menu_position' => null,
                'supports' => false,
                'capability_type' => $this->_post_type_name,
                'capabilities' => array(
                    'create_posts' => 'do_not_allow',
                    'delete_posts' => 'delete_' . $this->_post_type_name . 's'
                ),
            );


            register_post_type($this->_post_type_name, $args);
        }

        /**
         * Add badge management capabilities to Admin and Shop Manager
         *
         *
         * @access public
         * @since  1.0.0
         * @author Francesco Licandro <francesco.licandro@yithemes.com>
         */
        public function add_capabilities()
        {
            $obj = new stdClass();
            $obj->map_meta_cap = true;
            $obj->capability_type = $this->_post_type_name;
            $obj->capabilities = array();

            $caps = get_post_type_capabilities($obj);

            // gets the admin and shop_mamager roles
            $admin = get_role('administrator');
            $shop_manager = get_role('shop_manager');

            $caps = get_object_vars($caps);

            foreach ($caps as $cap => $value) {
                if ($admin) {
                    $admin->add_cap($value);
                }

                if ($shop_manager) {
                    $shop_manager->add_cap($value);
                }
            }
        }

        /**
         * Add metabox to custom post type Best Price
         *
         * @return void
         * @since    1.0.0
         * @author   Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function add_meta_box_to_cpt()
        {
            add_meta_box('user_reporting_details', __('Product details', 'yith-best-price-guaranteed-for-woocommerce'), array(
                $this,
                'print_meta_box'
            ), $this->_post_type_name, 'normal', 'high');
        }

        /**
         * Print metabox in post type "Best Price"
         *
         * @return void
         * @since    1.0.0
         * @author   Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function print_meta_box()
        {
            global $post;
            $best_price = new YITH_WCBPG_Best_Price($post->ID);
            $product_name = yit_get_prop($best_price->product, 'name');
            $link = get_edit_post_link(yit_get_base_product_id($best_price->product));
            $order_coupon = $best_price->is_coupon_used();
            ?>
        <p>
            <b><?php _e('Product', 'yith-best-price-guaranteed-for-woocommerce'); ?></b>:
            <a href="<?php echo $link; ?>" target="_blank"><?php echo $product_name; ?></a>
        </p>
        <p>
            <b><?php _e('User', 'yith-best-price-guaranteed-for-woocommerce'); ?></b>:
            <?php if (email_exists($best_price->user_email)): ?>
                <?php $user = get_user_by('email', $best_price->user_email); ?>
                <a href="<?php echo get_edit_user_link($user->ID) ?>" target="_blank"><?php echo $user->display_name ?></a>
            <?php else : ?>
                <?php echo ($best_price->user_name != '') ? $best_price->user_name : __('Unregistered', 'yith-best-price-guaranteed-for-woocommerce'); ?>
            <?php endif; ?>
        </p>
        <p>
            <b><?php _e('Email address', 'yith-best-price-guaranteed-for-woocommerce'); ?></b>: <?php echo $best_price->user_email; ?>
        </p>
        <p>
            <b><?php _e('Suggested price', 'yith-best-price-guaranteed-for-woocommerce'); ?></b>:
            <?php if( $best_price->coupon_code != '' ): ?>
                <?php echo $best_price->suggested_price; ?>
            <?php else: ?>
                <input type="text" value="<?php echo $best_price->suggested_price; ?>" name="yith-ywcbg-suggested-price">
            <?php endif; ?>
        </p>
        <p>
            <b><?php _e('Website', 'yith-best-price-guaranteed-for-woocommerce'); ?></b>:
            <a href="<?php echo 'http://' . $best_price->site_url ?>"
               target="_blank"><?php echo $best_price->site_url; ?></a>
        </p>
        <?php if (!isset($best_price->note)): ?>
            <b><?php _e('Notes', 'yith-best-price-guaranteed-for-woocommerce'); ?></b>:
            <br> <?php echo $best_price->note; ?>
        <?php endif; ?>
        <p>
            <?php if( $best_price->admin_note != '' ): ?>
                <b><?php _e('Admin note', 'yith-best-price-guaranteed-for-woocommerce'); ?></b>:
                <p><?php echo $best_price->admin_note; ?></p>
            <?php else: ?>
                <?php echo sprintf( __('%1$sAdmin note%2$s: write a custom note you want show in coupon email (%3$ssave post before agree the proposal%4$s)', 'yith-best-price-guaranteed-for-woocommerce'),'<b>','</b>','<u>','</u>' ) ?>
                <textarea class="yith-ywcbg-admin-note" name="yith-ywcbg-admin-note"></textarea>
            <?php endif; ?>
        </p>


        <?php if ($best_price->coupon_code != ''): ?>
            <p>
                <b><?php _e('You have accepted your user\'s request', 'yith-best-price-guaranteed-for-woocommerce') ?></b><br>
                <b><?php _e('Coupon code', 'yith-best-price-guaranteed-for-woocommerce'); ?></b>: <?php echo $best_price->coupon_code; ?>
            </p>
            <?php if (empty($order_coupon)): ?>
                <p>
                    <?php _e('The coupon code has not been used yet', 'yith-best-price-guaranteed-for-woocommerce'); ?>
                </p>
            <?php else: ?>
                <?php $link_to_order = get_edit_post_link($order_coupon); ?>
                <p>
                    <?php _e('The coupon code has been used. ', 'yith-best-price-guaranteed-for-woocommerce'); ?>
                    <a href="<?php echo $link_to_order; ?>"><?php _e('View order', 'yith-best-price-guaranteed-for-woocommerce') ?></a>
                </p>
            <?php endif; ?>
        <?php elseif( $best_price->user_email == 'anonymous' ): ?>
            <p>
                <?php _e('User associated to this request has been erased, you can\'t process your offer','yith-best-price-guaranteed-for-woocommerce'); ?>
            </p>
        <?php else: ?>
            <div class="wrap-button">
                <button id="generate-coupon" class="button-primary"
                        data-best-price-id="<?php echo $post->ID; ?>"
                >
                    <?php _e('Agree on price', 'yith-best-price-guaranteed-for-woocommerce'); ?>
                </button>
                <p>
                    <?php _e('Click on this button to allow the user to purchase the product at the agreed price','yith-best-price-guaranteed-for-woocommerce') ?>
                </p>
            </div>
        <?php endif; ?>
        <?php
        }

        /**
         * Set columns of post type "Best Price"
         *
         * @param $existing_columns
         *
         * @return array
         * @since    1.0.0
         * @author   Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function set_best_prices_columns($existing_columns)
        {
            $columns = array();
            $columns['cb'] = $existing_columns['cb'];
            $columns['best_price_id'] = __('ID', 'yith-best-price-guaranteed-for-woocommerce');
            $columns['product_name'] = __('Product name', 'yith-best-price-guaranteed-for-woocommerce');
            $columns['product_best_price'] = __('Best price', 'yith-best-price-guaranteed-for-woocommerce');
            $columns['user'] = __('User', 'yith-best-price-guaranteed-for-woocommerce');
            $columns['date'] = __('Date', 'yith-best-price-guaranteed-for-woocommerce');
            $columns['coupon_status'] = __('Coupon status', 'yith-best-price-guaranteed-for-woocommerce');

            return $columns;
        }

        /**
         * Manage columns for "Best Price" posy type
         *
         * @return void
         * @since    1.0.0
         * @author   Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */

        public function manage_best_prices_column($column, $post_id)
        {
            $best_price = new YITH_WCBPG_Best_Price($post_id);
            switch ($column) {
                case 'best_price_id' :
                    echo '<a href="' . get_edit_post_link($post_id) . '"><b>#' . $post_id . '</b></a>';
                    break;

                case 'coupon_status' :
                    echo '<div alt="' . $best_price->status['dashicon_alt'] . '" class="wcbpg-coupon-status dashicons ' . $best_price->status['dashicon_css_class'] . '"><span class="label">' . $best_price->status['display'] . '</span></div>';
                    break;

                case 'product_name' :
                    $product_id = get_post_meta($post_id, '_wcbg_product_id', true);
                    $variation_id = get_post_meta($post_id, '_wcbg_variation_id', true);
                    $product = ( isset($variation_id) && $variation_id != ''  ) ? wc_get_product( $variation_id ) : wc_get_product( $product_id );
                    if( ! is_bool($product) )
                        echo '<a href="' . get_edit_post_link($product_id) . '" target="_blank"> ' . $product->get_name() . '</a>';
                    break;

                case 'product_best_price' :
                    $user_price = get_post_meta($post_id, '_wcbpg_suggested_price', true);
                    echo $user_price;
                    break;

                case 'user' :
                    $user_email = get_post_meta($post_id, '_wcbpg_user_email', true);
                    if (email_exists($user_email)):
                        $user = get_user_by('email', $user_email);
                        echo '<a href="' . get_edit_user_link($user->ID) . '" target="_blank">' . $user->user_email . '</a>';
                    else:
                        echo __('Unregistered', 'yith-best-price-guaranteed-for-woocommerce');
                    endif;
                    break;

                case 'date' :
                    break;
            }
        }

        /**
         * Localize admin scripts
         *
         * @return void
         * @since    1.0.0
         * @author   Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function localize_scripts()
        {
            wp_register_script('yith-wcbpg-admin', YITH_WCBPG_ASSETS_URL . '/js/admin.js', array('jquery'), '1.0.0', true);
            wp_localize_script('yith-wcbpg-admin', 'yith_wcbpg_admin', array(
                'ajaxurl' => admin_url('admin-ajax.php'),
                'success' => __('The email has been successfully sent', 'yith-best-price-guaranteed-for-woocommerce'),
                'error' => __('An error has occurred. Please, try again.', 'yith-best-price-guaranteed-for-woocommerce')
            ));
        }


        /**
         * Register plugins for activation tab
         *
         * @return void
         * @since    1.0.0
         * @author   Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function register_plugin_for_activation()
        {
            if (!class_exists('YIT_Plugin_Licence')) {
                require_once(YITH_WCBPG_DIR . 'plugin-fw/lib/yit-plugin-licence.php');
            }

            YIT_Plugin_Licence()->register(YITH_WCBPG_INIT, YITH_WCBPG_SECRET_KEY, YITH_WCBPG_SLUG);
        }

        /**
         * Register plugins for update tab
         *
         * @return void
         * @since    1.0.0
         * @author   Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function register_plugin_for_updates()
        {
            if (!class_exists('YIT_Upgrade')) {
                require_once(YITH_WCBPG_DIR . 'plugin-fw/lib/yit-upgrade.php');
            }

            YIT_Upgrade()->register(YITH_WCBPG_SLUG, YITH_WCBPG_INIT);
        }


        public function edit_saved_data(){
            global $post;
            if( isset( $_POST['yith-ywcbg-suggested-price'] ) ) update_post_meta($post->ID, '_wcbpg_suggested_price', $_POST['yith-ywcbg-suggested-price']);
            if( isset( $_POST['yith-ywcbg-admin-note'] ) ) update_post_meta($post->ID, '_wcbpg_admin_note', $_POST['yith-ywcbg-admin-note']);
        }

    }
}


/**
 * Unique access to instance of YITH_WCBPG_Admin class
 *
 * @return \YITH_WCBPG_Admin
 * @since 1.0.0
 */
function YITH_WCBPG_Admin()
{
    return YITH_WCBPG_Admin::get_instance();
}