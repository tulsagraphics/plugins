<?php
/**
 * Main class
 *
 * @author  Yithemes
 * @package YITH Best Price Guaranteed for WooCommerce
 * @version 1.0.0
 */
if ( !defined( 'YITH_WCBPG' ) ) {
    exit;
} // Exit if accessed directly

if( !class_exists( 'YITH_WCBPG_Best_Price' ) ) {
    class YITH_WCBPG_Best_Price{
        /**
         * Single instance of the class
         *
         * @var \YITH_WCBPG_Best_Price
         * @since 1.0.0
         */
        protected static $instance;

        public $post_id;
        public $product;
        public $status;

        /**
         * @var $_panel Panel Object
         */
        protected $_panel;
        /**
         * @var string panel page
         */
        protected $_panel_page = 'yith_wcbpg_best_price_post';
        

        public function __construct( $id )
        {
            $this->post_id              =   $id;
            $this::_get_product();
            $this::set_status();
        }

        /**
         * Magic method __get to get object attribute
         *
         * @param $key
         * @return mixed
         * @since 1.0.0
         * @author  Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function __get( $key ) {
            $value = get_post_meta( $this->post_id, '_wcbpg_' . $key, true );

            if ( !empty( $value ) ) {
                $this->$key = $value;
            }

            return $value;
        }

        /**
         * Magic method __isset to control value of key
         *
         * @param $key
         * @return mixed
         * @since 1.0.0
         * @author  Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function __isset( $key ){ 
            $value = get_post_meta( $this->post_id, '_wcbpg_' . $key, true );

            if ( !empty( $value ) ) {
                $this->$key = $value;
            }

            return empty($value);
        }


        /**
         * Set attribute "product"
         *
         * @return void
         * @since 1.0.0
         * @author  Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        private function _get_product(){
            $product_id                 =   get_post_meta( $this->post_id, '_wcbg_product_id',true);
            $variation_id               =   get_post_meta( $this->post_id, '_wcbg_variation_id',true);
            $this->product = ( isset( $variation_id ) && $variation_id!= '') ? wc_get_product( $variation_id ) : wc_get_product( $product_id );
        }


        /**
         * Check if coupon is used by user
         *
         * @return bool|int
         * @since 1.0.0
         * @author  Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */

        public function is_coupon_used(){
            $wc_orders = get_posts( array(
                'post_type'   => 'shop_order',
                'post_status' => array_keys( wc_get_order_statuses() ),
                'numberposts'
            ) );
            foreach( $wc_orders as $wc_order ):
                $order_id = yit_get_prop( $wc_order ,'ID');
                $order = new WC_Order( $order_id );
                $used_coupons = $order->get_used_coupons();
                if( is_array( $used_coupons ) && !empty( $used_coupons ) ):
                    if( in_array( wc_strtolower($this->coupon_code), $used_coupons ) ) return $order_id;
                endif;
            endforeach;
            return false;
        }


        /**
         * Get product title
         * @return string
         * @since 1.0.0
         * @author Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        
        public function get_product_title(){
            $product_title = yit_get_prop( $this->product,'name' ) ;
            return $product_title;
        }

        /**
         * Get username by the email
         * @return string
         * @since 1.0.0
         * @author Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function get_display_name_by_email( $type="username" ){
            $user = get_user_by( 'email',$this->user_email );
            $user_display_name = '';
            if( is_object($user) ){
                $user_display_name = ($type == 'username') ? $user->user_login : $user->first_name;
            }else{
                $user_display_name = ($this->user_name != '') ? $this->user_name :  __( 'customer', 'yith-best-price-guaranteed-for-woocommerce' );
            }
            return $user_display_name;
        }



        private function set_status(){
            if($this->coupon_code != ''){
                $coupon = new WC_Coupon( $this->coupon_code );
                if( $coupon->get_usage_count() != 0 ){
                    $this->status["name"] = 'usued';
                    $this->status["display"] = __( 'used','yith-best-price-guaranteed-for-woocommerce' );
                    $this->status['dashicon_css_class'] = "dashicons-yes";
                    $this->status['dashicon_alt']   = '#f147';
                }else{
                    $this->status["name"] = 'unusued';
                    $this->status["display"] = __( 'unused','yith-best-price-guaranteed-for-woocommerce' );
                    $this->status['dashicon_css_class'] = "dashicons-no-alt";
                    $this->status['dashicon_alt']   = '#f335';
                }
            }else{
                $this->status["name"] = 'unsent';
                $this->status["display"] = __( 'unsent','yith-best-price-guaranteed-for-woocommerce' );
                $this->status['dashicon_css_class'] = "dashicons-minus";
                $this->status['dashicon_alt']   = '#f158';
            } 
        }
        
        





    }
}