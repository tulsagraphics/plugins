<?php
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists ( "WC_Email" ) ) {
    require_once ( WC ()->plugin_path () . '/includes/emails/class-wc-email.php' );
}

if ( !class_exists( 'YITH_WCBPG_User_Coupon' ) ) :

    /**
     * User Notification
     *
     * An email sent to the admin when a user sent a message bu form of product page
     *
     * @class       YITH_WCBPG_User_Coupon
     * @version     1.0.0
     */
    class YITH_WCBPG_User_Coupon extends WC_Email {

        /**
         * @var YITH_WCBPG
         */        
        public $user_email;
        
        public $custom_message;

        public $link;
        /**
         * Constructor.
         */
        public function __construct() {
            $this->object;
            $this->id          = 'yith-wcbpg-new-coupon-user';
            $this->title       = __( 'Best Price Guaranteed - Coupon', 'yith-best-price-guaranteed-for-woocommerce' );
            $this->description = __( 'This is the email users receive with the coupon code that will allow them buy the product at the suggested price', 'yith-best-price-guaranteed-for-woocommerce' );
            $this->heading     = __( 'Coupon code', 'yith-best-price-guaranteed-for-woocommerce' );
            $this->subject     = __( 'Discount code for the selected product', 'yith-best-price-guaranteed-for-woocommerce' );
            $this->reply_to    = '';
            $this->customer_email = true;

            $this->template_html  = 'emails/coupon-user-notification.php';
            $this->template_plain = 'emails/plain/coupon-user-notification.php';
            $this->template_base = YITH_WCBPG_TEMPLATE_PATH;

            // Triggers for this email
            add_action( 'yith-wcbpg-new-coupon-user_notification',array( $this, 'trigger'),10, 1 );
            add_action( 'yith_wcbpg_coupon_product_details', array( $this,'yit_wcbpg_coupon_product_details' ), 10, 2 );

            // Other settings
            $this->custom_message = $this->get_option( 'custom_message',__( 'Dear {username}, in reply to your price suggestion,
             we decided to approve your request. Click on the link below to purchase the product at the agreed price', 'yith-best-price-guaranteed-for-woocommerce' ) );
     

            parent::__construct ();

        }


        public function trigger( $post_id ) {

            $this->object = new YITH_WCBPG_Best_Price( $post_id );
            $this->recipient = $this->object->user_email;
            $this->link = get_site_url() . '?&wcbpg_id=' . $this->object->post_id;
            if( ( $this->object->coupon_code != '' ) ){

                $this->find['username']      = '{username}';
                $this->replace['username']   = $this->object->get_display_name_by_email('username');
                $this->find['first_name']    = '{first_name}';
                $this->replace['first_name'] = $this->object->get_display_name_by_email('first_name');

            }


            $result = $this->send(
                $this->get_recipient(),
                $this->get_subject(),
                $this->get_content(),
                $this->get_headers(),
                $this->get_attachments()
            );

            return $result;

        }

        /**
         * get_content_html function.
         *
         * @since 0.1
         * @return string
         */
        public function get_content_html () {
            ob_start ();
            wc_get_template ( $this->template_html, array (
                'best_price'        =>  $this->object,
                'link'              =>  $this->link,
                'email_heading'     =>  $this->get_heading (),
                'email_type'        =>  $this->email_type,
                'custom_message'    =>  $this->format_string($this->custom_message),
                'sent_to_admin'     =>  false,
                'plain_text'        =>  false,
                'email'             =>  $this,
            ),
                '',
                $this->template_base );

            return ob_get_clean ();
        }

        /**
         * get_content_plain function.
         *
         * @access public
         * @return string
         */
        function get_content_plain() {
            ob_start();
            wc_get_template( $this->template_plain, array(
                'best_price'        =>  $this->object,
                'link'              =>  $this->link,
                'email_heading'     => $this->get_heading(),
                'custom_message'    =>  $this->format_string($this->custom_message),
                'email'             => $this,
                'sent_to_admin'     =>  false,
                'plain_text'        =>  true,

            ), '', $this->template_base );

            return ob_get_clean();
        }

        /**
         * Initialise settings form fields.
         */
        public function init_form_fields() {
            $this->form_fields = array(
                'enabled'    => array(
                    'title'   => __( 'Enable/Disable', 'yith-best-price-guaranteed-for-woocommerce' ),
                    'type'    => 'checkbox',
                    'label'   => __( 'Enable this email notification', 'yith-best-price-guaranteed-for-woocommerce' ),
                    'default' => 'yes'
                ),
                'subject'    => array(
                    'title'       => __( 'Subject', 'yith-best-price-guaranteed-for-woocommerce' ),
                    'type'        => 'text',
                    'description' => sprintf( __( 'This controls the email subject line. Leave blank to use the default subject: <code>%s</code>.', 'woocommerce' ), $this->subject ),
                    'placeholder' => '',
                    'default'     => '',
                    'desc_tip'    => true
                ),
                'heading'    => array(
                    'title'       => __( 'Email Heading', 'yith-best-price-guaranteed-for-woocommerce' ),
                    'type'        => 'text',
                    'description' => sprintf( __( 'This controls the main heading contained within the email notification. Leave blank to use the default heading: <code>%s</code>.', 'woocommerce' ), $this->heading ),
                    'placeholder' => '',
                    'default'     => '',
                    'desc_tip'    => true
                ),
                'custom_message' => array(
                    'title'       => __( 'Custom Message', 'yith-best-price-guaranteed-for-woocommerce' ),
                    'type'        => 'textarea',
                    'description' => __( 'Text of the email sent to users including the coupon code.','yith-best-price-guaranteed-for-woocommerce' ),
                    'placeholder' => 'Dear {username}, in reply to your price suggestion, we decided to approve your request.',
                    'default'     => __( 'Dear {username}, in reply to your price suggestion, we decided to approve your request.', 'yith-best-price-guaranteed-for-woocommerce' )
                ),
                'email_type' => array(
                    'title'       => __( 'Email type', 'yith-best-price-guaranteed-for-woocommerce' ),
                    'type'        => 'select',
                    'description' => __( 'Choose the format for the email sent.', 'yith-best-price-guaranteed-for-woocommerce' ),
                    'default'     => 'html',
                    'class'       => 'email_type wc-enhanced-select',
                    'options'     => $this->get_email_type_options(),
                    'desc_tip'    => true
                )
            );

        }

        public function yit_wcbpg_coupon_product_details( $best_price, $link ){
            wc_get_template ( 'emails/coupon-product-details.php', array (
                'best_price'        => $best_price,
                'link'              => $this->link
            ),
                '',
                YITH_WCBPG_TEMPLATE_PATH );
        }

    }

endif;

return new YITH_WCBPG_User_Coupon();