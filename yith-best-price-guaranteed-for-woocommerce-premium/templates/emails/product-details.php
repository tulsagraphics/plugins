<div class="ywcbpg-email-notification admin">
    <table>
        <thead>
        <tr>
            <th class="product-name" colspan="2"><?php _e('Product','yith-best-price-guaranteed-for-woocommerce');?></th>
            <th><?php _e('Price','yith-best-price-guaranteed-for-woocommerce'); ?></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <img src="<?php echo $best_price->product->get_image_id() ? current( wp_get_attachment_image_src( $best_price->product->get_image_id(), 'thumbnail' ) ) : wc_placeholder_img_src()?>" alt="<?php _e( 'Product image', 'woocommerce' ) ;?>">
            </td>
            <td>
                <?php echo $best_price->get_product_title(); ?>
            </td>
            <td>
                <span><?php echo yit_get_display_price($best_price->product) . ' ' . get_woocommerce_currency_symbol(); ?></span>
            </td>
        </tr>
        </tbody>
    </table>
    <div class="user-info">
        <?php if( email_exists( $best_price->user_email ) ): ?>
            <?php $user = get_user_by( 'email',$best_price->user_email ); ?>
        <?php endif; ?>
        <h3>
            <?php _e( 'Message details','yith-best-price-guaranteed-for-woocommerce' ); ?>
        </h3>
        <ul>
            <li>
                <?php
                $user_display_name = '';
                if( isset($user) ){
                    $user_display_name = $user->user_login;
                }else{
                    $user_display_name = ($best_price->user_name != '') ? $best_price->user_name :  __( 'Unregistered', 'yith-best-price-guaranteed-for-woocommerce' );
                }
                ?>
                <b><?php _e( 'User','yith-best-price-guaranteed-for-woocommerce' );?></b>: <?php echo $user_display_name ?>
            </li>
            <li>
                <b><?php _e('Email address','yith-best-price-guaranteed-for-woocommerce');?></b>: <?php echo $best_price->user_email; ?>
            </li>
            <li>
                <b><?php _e('Suggested price','yith-best-price-guaranteed-for-woocommerce');?></b>: <?php echo $best_price->suggested_price; ?>
            </li>
            <li>
                <b><?php _e('Qualifying competitor website','yith-best-price-guaranteed-for-woocommerce');?></b>: <a href="<?php echo $best_price->site_url; ?>" target="_blank"><?php echo $best_price->site_url; ?></a>
            </li>
            <li>
                <?php if( !isset( $best_price->note )): ?>
                    <b><?php _e('Notes','yith-best-price-guaranteed-for-woocommerce');?></b>: <?php echo $best_price->note; ?>
                <?php endif; ?>
            </li>           
        </ul>
    </div>
    <a href="<?php echo $link_to_post;?>"><?php _e( 'View request details','yith-best-price-guaranteed-for-woocommerce' ); ?></a>
</div>


