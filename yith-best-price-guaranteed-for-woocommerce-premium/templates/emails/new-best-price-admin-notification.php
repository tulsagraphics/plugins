<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email );

_e('This product has been found at a lower price on another website. Verify information added by the user and decide whether agreeing on the suggested price or not','yith-best-price-guaranteed-for-woocommerce');

do_action( 'yith-wcbpg-product-details', $best_price , $link_to_post);


/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
?>
