<div class="ywcbpg-email-notification user">
    <h3><?php _e( 'The selected product','yith-best-price-guaranteed-for-woocommerce' ); ?></h3>
    <div class="product-info">
        <img src="<?php echo $best_price->product->get_image_id() ? current( wp_get_attachment_image_src( $best_price->product->get_image_id(), 'thumbnail' ) ) : wc_placeholder_img_src()?>" alt="<?php _e( 'Product image', 'woocommerce' ) ;?>">
        <p class="product-name"><?php echo $best_price->get_product_title(); ?></p>
        <p>
            <b><?php _e( 'Base price','yith-best-price-guaranteed-for-woocommerce' ) ?>:</b> <span class="line-through" style="text-decoration: line-through;"><?php echo get_woocommerce_currency_symbol() . ' ' . yit_get_display_price($best_price->product) ?></span>
        </p>
        <p class="suggested-price">
            <b><?php _e( 'Special price for you','yith-best-price-guaranteed-for-woocommerce' ); ?>:</b> <span><?php echo get_woocommerce_currency_symbol() . ' ' .  apply_filters('ywbpg_suggested_price',$best_price->suggested_price); ?></span>
        </p>
        <p>
            <?php echo $best_price->admin_note ?>
        </p>
    </div>
</div>
<a href="<?php echo $link; ?>" target="_blank" title="Purchase now"><?php _e( 'Purchase now','yith-best-price-guaranteed-for-woocommerce' ); ?></a>




