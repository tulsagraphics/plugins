<?php 
$product = isset( $variation_id ) ? new WC_Product_Variation( $variation_id ) : new WC_Product( $product_id );
$hide_product_info = get_option('yith-wcbpg-hide-product-info');
$is_note_required = get_option('yith-wcbpg-is-note-required'); 
$show_note = get_option('yith-wcbpg-show-note');
$is_site_url_required = get_option('yith-wcbpg-is-site-url-required');
$show_site_url = get_option('yith-wcbpg-show-site-url');
$show_name = get_option('yith-wcbpg-show-name');
$is_name_required = get_option('yith-wcbpg-is-name-required');
$user = wp_get_current_user();
$modal_width = get_option('yith-wcbpg-modal-width') != 0 ? get_option('yith-wcbpg-modal-width').'px' : 'auto';
$modal_height = get_option('yith-wcbpg-modal-height') != 0 ? get_option('yith-wcbpg-modal-height').'px' : 'auto';
$inline_style = "width: ". $modal_width ."; height: ". $modal_height ."";
$additional_field_title = get_option('yith-wcbpg-additional-field-title');
$additional_field_message = get_option('yith-wcbpg-additional-field-message');
$show_checkbox_gdpr = get_option('yith-wcbpg-show-gdpr-checkbox');
$label_gdpr_checkbox = get_option( 'yith-wcbpg-label-gdpr-checkbox-popup', '' );
$label_gdpr_checkbox = function_exists( 'wc_replace_policy_page_link_placeholders' ) ? wp_kses_post( wc_replace_policy_page_link_placeholders( $label_gdpr_checkbox ) ) : $label_gdpr_checkbox;
?>

<div class="popup-inner" style="<?php echo $inline_style; ?>">
    <h3 class="title"><?php echo get_option( 'yith-wcbpg-popup-title' ) ?></h3>
    <div class="wrap-content <?php if( $hide_product_info == 'yes' ) echo 'no-product-info' ?>">
        <?php if( $hide_product_info != 'yes' ): ?>
            <div class="product-info">
                <?php $id = (isset($variation_id) && $variation_id != '' ) ? $variation_id : $product_id; ?>
                <?php echo get_the_post_thumbnail( $id, 'single_product_archive_thumbnail_size' ) ?>
                <h4 class="product-name"><?php echo isset($product) ? $product->get_name() : get_the_title() ; ?></h4>
            </div>
        <?php endif; ?>
        <form action="" method="POST" id="best-price">
            <?php if( is_user_logged_in() ): ?>
                <input type="hidden" id="wcbpg_user_email" name="wcbpg_user_email" value="<?php echo $user->user_email; ?>">
            <?php else: ?>
                <?php if( $show_name == 'yes' ): ?>
                    <div class="wrap-input">
                        <label for="wcbpg_user_name">
                            <?php if($is_name_required == 'yes') echo '<span class="required-alert">*</span>' ?>
                            <?php echo get_option('yith-wcbpg-user-name-popup'); ?>
                        </label>
                        <input <?php if($is_name_required == 'yes') echo 'class="required"'; ?> type="text" id="wcbpg_user_name" name="wcbpg_user_name" value="">
                    </div>
                <?php  endif; ?>    
                <div class="wrap-input">
                    <label for="wcbpg_user_email">
                        <span class="required-alert">*</span>
                        <?php echo get_option('yith-wcbpg-email-popup'); ?>
                    </label>
                    <input class="required" type="text" id="wcbpg_user_email" name="wcbpg_user_email" value="">
                </div>
            <?php endif; ?>
            <?php if( $show_site_url == 'yes' ): ?>
                <div class="wrap-input">
                    <label for="wcbpg_site_url">
                        <span class="required-alert">*</span>
                        <?php echo get_option('yith-wcbpg-site-popup'); ?>
                    </label>
                    <input type="text" name="wcbpg_site_url" id="wcbpg_site_url" value="" <?php if($is_site_url_required == 'yes') echo 'class="required"'; ?>>
                </div>
            <?php endif; ?>
            <div class="wrap-input">
                <label for="wcbpg_product_price">
                    <span class="required-alert">*</span>
                    <?php echo get_option('yith-wcbpg-price-popup'); ?>
                </label>
                <div class="price tooltip">
                    <input class="required" type="text" name="wcbpg_product_price" id="wcbpg_product_price" value="">
                    <span class="currency"><?php echo get_woocommerce_currency_symbol(); ?></span>
                </div>
            </div>
            <?php if($show_note == 'yes'): ?>
                <div class="wrap-input">
                    <label for="wcbpg_note">
                        <?php if($is_note_required == 'yes') echo '<span class="required-alert">*</span>' ?>
                        <?php echo get_option('yith-wcbpg-note-popup'); ?>
                    </label>
                    <input <?php if($is_note_required == 'yes') echo 'class="required"' ?> type="text" name="wcbpg_note" id="wcbpg_note" value="">
                </div>
            <?php endif; ?>
            <?php if( !empty($additional_field_title) ): ?>
                <?php echo html_entity_decode($additional_field_title) ?>
            <?php endif; ?>
            <?php if( !empty($additional_field_message) ): ?>
                <p><?php echo html_entity_decode($additional_field_message) ?></p>
            <?php endif; ?>
            <?php if($show_checkbox_gdpr == 'yes'): ?>
                <div class="wrap-input">
                    <input class="required" type="checkbox" name="wcbpg_gdpr_checkbox" id="wcbpg_gdpr_checkbox" value="1">
                    <label for="wcbpg_gdpr_checkbox">
                        <span class="required-alert">*</span>
                        <?php echo $label_gdpr_checkbox; ?>
                    </label>
                </div>
            <?php endif; ?>
            <input type="hidden" id="wcbg_product_id" name="wcbg_product_id" value="<?php echo $product_id ?>">
            <?php if( isset($variation_id) ): ?>
                <input type="hidden" id="wcbg_variation_id" name="wcbg_variation_id" value="<?php echo $variation_id ?>">
            <?php endif; ?>
            <input type="submit" id="wcbpg_send_price" value="<?php _e('Send','yith-best-price-guaranteed-for-woocommerce'); ?>">
        </form>
    </div>
    <a class="popup-close" data-popup-close="wcbpg-popup" href="#">x</a>
</div>

