= 1.3.2 = Released on Sep 11, 2018

* New: Support to WooCommerce 3.4.5.
* New: Support to WordPress 4.9.8.
* Update: Spanish translation.
* Update: Plugin Core.
* Fix: Issue on saving new address using PHP 7.2.

= 1.3.1 = Released on May 30, 2018

* New: Support to WooCommerce 3.4.1.
* New: Support to WordPress 4.9.6.
* Update: Italian language.
* Update: Dutch language.
* Update: Plugin Core.
* Fix: Select shipping address container width.
* Fix: Documentation url.
* Fix: "Shipping Address" popup style and responsive.
* Fix: Prevent multiple submit for shipping address form.

= 1.3.0 = Released on Feb 02, 2018

* New: Support to WooCommerce 3.3.0.
* New: Support to WordPress 4.9.2.
* Update: Plugin Core.
* Update: Language files.
* Fix: Error message when there is no shipping method for selected country.
* Fix: Auto select last address added in shipping address selection.

= 1.2.0 = Released on Oct 16, 2017

* New: Support to WooCommerce 3.2.1.
* New: Support to WordPress 4.8.2.
* New: Compatibility with WooCommerce Composite Products.
* New: Dutch Translation.
* Update: Plugin Core.
* Update: Language files.
* Fix: Disable button for variable products if no product options are selected.

= 1.1.0 = Released on Apr 04, 2017

* New: Support to WooCommerce 3.0.0 RC2.
* New: Support to WordPress 4.7.3.
* Update: Plugin Core.

= 1.0.5 = Released on Jan 19, 2017

* New: Shortcode [yith_wocc_button] for print custom one click button.
* New: Spanish Translation.
* New: Italian Translation.
* New: Support to WooCommerce 2.6.13.
* New: Support to WordPress 4.7.1.
* Update: Plugin Core.
* Update: Language Files.
* Fix: If no payment was required for order skip status Pending.

= 1.0.4 = Released on Jun 13, 2016

* New: Support to WooCommerce 2.6 RC1
* Update: Plugin Core.

= 1.0.3 = Released on Apr 22, 2016

* New: Compatibility with WordPress 4.5.
* New: Added minimized js files. Plugin loads full files version if the constant "SCRIPT_DEBUG" is defined and is true.
* Update: Changed text domain from yith-wocc to yith-woocommerce-one-click-checkout.
* Update: Plugin Core.

= 1.0.2 = Released on Feb 04, 2016

* New: Compatibility with YITH WooCommerce Customize My Account Page.
* New: Shortcode [yith_wocc_myaccount] to print the my-account plugin section.
* Update: Language file .pot
* Update: Plugin Core.

= 1.0.1 = Released on Jan 12, 2016

* New: Compatibility with WooCommerce 2.5 RC2.
* Update: Plugin Core.

= 1.0.0 = Released on Sep 18, 2015

* Initial release
