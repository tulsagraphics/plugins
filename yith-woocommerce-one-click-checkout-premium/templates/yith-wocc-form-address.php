<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
?>

<div class="woocommerce">
	<?php
	if( ! ywocc_is_wc_26() ) {
		wc_print_notices();
	} ?>
	<form method="post">

		<?php foreach ( $address as $key => $field ) : ?>

			<?php woocommerce_form_field( $key, $field, ! empty( $_POST[ $key ] ) ? wc_clean( $_POST[ $key ] ) : $field['value'] ); ?>

		<?php endforeach; ?>

		<p>
			<?php if( isset( $_GET['edit'] ) ) : ?>
				<input type="hidden" name="address_edit" value="<?php echo $_GET['edit'] ?>">
			<?php endif; ?>
			<input type="submit" class="button" name="save_address" value="<?php echo esc_attr( __( 'Save Address', 'yith-woocommerce-one-click-checkout' ) ); ?>" />
			<?php wp_nonce_field( $action ); ?>
			<input type="hidden" name="_action_form" value="<?php echo $action ?>"/>
		</p>

	</form>
</div>