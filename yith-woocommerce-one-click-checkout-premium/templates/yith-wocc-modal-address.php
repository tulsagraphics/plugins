<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
?>

<div id="yith-wocc-modal-overlay"></div>

<div id="yith-wocc-modal">

	<div class="yith-wocc-modal-content">

		<a href="#" class="yith-wacp-close fa fa-close"></a>

        <?php echo isset( $content ) ? $content : ''; ?>

	</div>

</div>