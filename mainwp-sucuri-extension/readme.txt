=== Child Sucuri Extension ===
Plugin Name: MainWP Sucuri Extension
Plugin URI: https://mainwp.com
Description: MainWP Sucuri Extension.
Version: 1.2
Author: MainWP
Author URI: https://mainwp.com

== Installation ==
1. Please install plugin "MainWP Dashboard" and active it before install MainWP Sucuri Extension plugin (get the MainWP Dashboard plugin from url:https://mainwp.com)
2. Upload the `mainwp-sucuri-extension` folder to the `/wp-content/plugins/` directory
3. Activate the Sucuri Extension plugin through the 'Plugins' menu in WordPress

== Screenshots ==
1. Enable or Disable extension on the "Extensions" page in the dashboard

== Changelog ==

= 1.2 - 4-20-2018 =
* Fixed: added support for the WP Cli scan command ( example: wp mainwp-sucuri scan [<siteid>] )

= 1.1 - 2-26-2018 =
* Updated: plugin info

= 1.0 - 2-17-2016 =
* Fixed: Translation issue
* Fixed: Compatibility with MainWP 3.0 version
* Added: An auto update warning if the extension is not activated
* Added: Support for the new API management
* Added: Support for WP-CLI
* Updated: "Check for updates now" link is not vidible if extension is not activated

= 0.2.0 - 9-25-2015 =
* Updated: Refactored code to meet WordPress coding standards

= 0.1.2 - 6-26-2015 =
* Added: An option to disable SSL certificate verification when scanning child sites

= 0.1.1 = 
* Updated: Quick start guide layout

= 0.1.0 =
* Fixed: Potential Security issue - Internal Code Audit

= 0.0.9 =
* Added: Support for the API Manager

= 0.0.8 =
* Fixed: Notification email template format

= 0.0.7 =
* Tweaked: Notification email template format

= 0.0.6 =
* Added: Support for the Client Reports extension
* Added: Additional Plugin Info
* Added: Redirection to the Extensions page

= 0.0.5 =
* Notification added

= 0.0.4 =
* Update quick guide

= 0.0.3 =
* New Feature Added: Extension is now saving Security Scan Reports
* New Feature Added: Security Scan Notifications

= 0.0.2 =
* Removed some lines of comments

= 0.0.1 =
* First version

