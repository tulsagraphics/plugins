<?php
/*
  Plugin Name: MainWP Sucuri Extension
  Plugin URI: https://mainwp.com
  Description: MainWP Sucuri Extension enables you to scan your child sites for various types of malware, spam injections, website errors, and much more. Requires the MainWP Dashboard.
  Version: 1.2
  Author: MainWP
  Author URI: https://mainwp.com
  Documentation URI: https://mainwp.com/help/category/mainwp-extensions/sucuri/
 */


if ( ! defined( 'MAINWP_SUCURI_PLUGIN_FILE' ) ) {
	define( 'MAINWP_SUCURI_PLUGIN_FILE', __FILE__ );
}

class MainWP_Sucuri_Extension {

	public static $instance = null;
	public $plugin_handle = 'mainwp-sucuri-extension';
	protected $plugin_url;
	public $plugin_slug;

	static function get_instance() {
		if ( null == MainWP_Sucuri_Extension::$instance ) {
			MainWP_Sucuri_Extension::$instance = new MainWP_Sucuri_Extension(); }
		return MainWP_Sucuri_Extension::$instance;
	}

	public function __construct() {
		$this->plugin_url = plugin_dir_url( __FILE__ );
		$this->plugin_slug = plugin_basename( __FILE__ );
		
		add_action( 'init', array( &$this, 'localization' ) );
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_filter( 'plugin_row_meta', array( &$this, 'plugin_row_meta' ), 10, 2 );
        add_action( 'mainwp_delete_site', array( &$this, 'on_delete_site' ), 10, 1 );            
		MainWP_Sucuri_DB::get_instance()->install();
		MainWP_Sucuri::get_instance()->init();        
	}
	
	public function localization() {
		load_plugin_textdomain( 'mainwp-sucuri-extension', false,  dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}

	public function plugin_row_meta( $plugin_meta, $plugin_file ) {
		if ( $this->plugin_slug != $plugin_file ) {
			return $plugin_meta; }
			
		$slug = basename($plugin_file, ".php");
		$api_data = get_option( $slug. '_APIManAdder');		
		if (!is_array($api_data) || !isset($api_data['activated_key']) || $api_data['activated_key'] != 'Activated' || !isset($api_data['api_key']) || empty($api_data['api_key']) ) {
			return $plugin_meta;
		}
		
		$plugin_meta[] = '<a href="?do=checkUpgrade" title="Check for updates.">Check for updates now</a>';
		return $plugin_meta;
	}

	public function admin_init() {
		//if (isset($_REQUEST['page']) && $_REQUEST['page'] == 'ManageSitesSecurityScan')
		//{
		wp_enqueue_style( 'mainwp-securi-extension', $this->plugin_url . 'css/mainwp-sucuri.css' );
		wp_enqueue_script( 'mainwp-securi-extension', $this->plugin_url . 'js/mainwp-sucuri.js' );
		//}
	}
    
     public function on_delete_site( $website ) {
        if ( $website ) {
            MainWP_Sucuri_DB::get_instance()->delete_sucuri_by_site_url($website->url);            
        }
	}  
    
}

function mainwp_sucuri_extension_autoload( $class_name ) {
	$allowedLoadingTypes = array( 'class' );
	$class_name = str_replace( '_', '-', strtolower( $class_name ) );
	foreach ( $allowedLoadingTypes as $allowedLoadingType ) {
		$class_file = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . str_replace( basename( __FILE__ ), '', plugin_basename( __FILE__ ) ) . $allowedLoadingType . DIRECTORY_SEPARATOR . $class_name . '.' . $allowedLoadingType . '.php';
		if ( file_exists( $class_file ) ) {
			require_once( $class_file );
		}
	}
}

if ( function_exists( 'spl_autoload_register' ) ) {
	spl_autoload_register( 'mainwp_sucuri_extension_autoload' );
} else {

	function __autoload( $class_name ) {
		mainwp_sucuri_extension_autoload( $class_name );
	}
}


register_activation_hook( __FILE__, 'mainwp_sucuri_extension_activate' );
register_deactivation_hook( __FILE__, 'mainwp_sucuri_extension_deactivate' );

function mainwp_sucuri_extension_activate() {
	update_option( 'mainwp_sucuri_extension_activated', 'yes' );
	$extensionActivator = new MainWP_Sucuri_Extension_Activator();
	$extensionActivator->activate();	
}

function mainwp_sucuri_extension_deactivate() {
	$extensionActivator = new MainWP_Sucuri_Extension_Activator();
	$extensionActivator->deactivate();
}

class MainWP_Sucuri_Extension_Activator {

	protected $mainwpMainActivated = false;
	protected $childEnabled = false;
	protected $childKey = false;
	protected $childFile;
	protected $plugin_handle = 'mainwp-sucuri-extension';
	protected $product_id = 'MainWP Sucuri Extension';
	protected $software_version = '1.2';

	public function __construct() {
		$this->childFile = __FILE__;
		add_filter( 'mainwp-getextensions', array( &$this, 'get_this_extension' ) );
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', false );

		if ( $this->mainwpMainActivated !== false ) {
			$this->activate_this_plugin();
		} else {
			add_action( 'mainwp-activated', array( &$this, 'activate_this_plugin' ) );
		}
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_action( 'admin_notices', array( &$this, 'mainwp_error_notice' ) );
        if ( defined( 'WP_CLI' ) && WP_CLI ) {
			MainWP_Sucuri_WP_CLI_Command::init();
		}        
	}

	function admin_init() {
		if ( get_option( 'mainwp_sucuri_extension_activated' ) == 'yes' ) {
			delete_option( 'mainwp_sucuri_extension_activated' );
			wp_redirect( admin_url( 'admin.php?page=Extensions' ) );
			return;
		}
	}

	function get_this_extension( $pArray ) {
		$pArray[] = array( 'plugin' => __FILE__, 'api' => $this->plugin_handle, 'mainwp' => true, 'callback' => array( &$this, 'settings' ), 'apiManager' => true );
		return $pArray;
	}

	function settings() {
		do_action( 'mainwp-pageheader-extensions', __FILE__ );
		if ( $this->childEnabled ) {
			MainWP_Sucuri::sucuri_qsg();
			?><div class="mainwp_info-box-yellow"><?php _e( 'This extension does not have the settings page. To use this extension use the "Security Scan" link in the <a href="admin.php?page=managesites" title="Manage Sites">Manage Sites</a> table. In case you need help, please review this <a href="http://docs.mainwp.com/category/mainwp-extensions/mainwp-sucuri-extension" target="_blank">documentation</a>.', 'mainwp-sucuri-extension' ); ?></div><?php
		} else {
			?><div class="mainwp_info-box-yellow"><strong><?php _e( 'The Extension has to be enabled to change the settings.', 'mainwp-sucuri-extension' ); ?></strong></div><?php
		}
		do_action( 'mainwp-pagefooter-extensions', __FILE__ );
	}

	function activate_this_plugin() {
		$this->mainwpMainActivated = apply_filters( 'mainwp-activated-check', $this->mainwpMainActivated );
		$this->childEnabled = apply_filters( 'mainwp-extension-enabled-check', __FILE__ );		
		$this->childKey = $this->childEnabled['key'];
		if ( function_exists( 'mainwp_current_user_can' ) && ! mainwp_current_user_can( 'extension', 'mainwp-sucuri-extension' ) ) {
			return; 			
		}
		new MainWP_Sucuri_Extension();
	}

	function mainwp_error_notice() {
		global $current_screen;
		if ( $current_screen->parent_base == 'plugins' && $this->mainwpMainActivated == false ) {
			echo '<div class="error"><p>MainWP Sucuri Extension ' . __( 'requires <a href="https://mainwp.com" target="_blank">MainWP Dashboard Plugin</a> to be activated in order to work. Please install and activate <a href="https://mainwp.com" target="_blank">MainWP Dashboard Plugin</a> first.', 'mainwp-sucuri-extension' ) . '</p></div>';
		}
	}

	public function get_child_key() {
		return $this->childKey;
	}

	public function get_child_file() {
		return $this->childFile;
	}

	public function is_enabled() {
		return $this->childFile ? true : false;
	}

	public function update_option( $option_name, $option_value ) {
		$success = add_option( $option_name, $option_value, '', 'no' );

		if ( ! $success ) {
			$success = update_option( $option_name, $option_value );
		}

		return $success;
	}

	public function activate() {
		$options = array(
		'product_id' => $this->product_id,
			'activated_key' => 'Deactivated',
			'instance_id' => apply_filters( 'mainwp-extensions-apigeneratepassword', 12, false ),
			'software_version' => $this->software_version,
		);
		$this->update_option( $this->plugin_handle . '_APIManAdder', $options );
	}

	public function deactivate() {
		$this->update_option( $this->plugin_handle . '_APIManAdder', '' );
	}
}

global $mainWPSucuriExtensionActivator;
$mainWPSucuriExtensionActivator = new MainWP_Sucuri_Extension_Activator();
