=== GravityView Math ===
Tags: gravityview, math
Requires at least: 4.0
Tested up to: 4.9.5
Stable tag: trunk
Contributors: gravityview
License: GPL 2 or higher

Calculations. Uses the [Hoa Math](https://github.com/hoaproject/Math) library.

== Installation ==

1. Upload plugin files to your plugins folder, or install using WordPress' built-in Add New Plugin installer
2. Activate the plugin
3. Follow the instructions on [the Math by GravityView documentation page](http://docs.gravityview.co/category/370-math-by-gravityview)

== Changelog ==

= 1.1.1 on May 8, 2018 =

* Fixed: Aggregate form data not calculating with Gravity Forms 2.3
* Fixed: `.mo` translation files weren't being generated
* Updated: Dutch, Turkish, and Spanish translations (thanks, jplobaton, SilverXp, and suhakaralar!)

= 1.1 on April 28, 2018 =

* Fixed: Compatibility with Gravity Forms 2.3
* Updated: Dutch and Turkish translations

= 1.0.3 on May 24, 2017 =

* Fixed: Don't link to entry in debug mode if the entry doesn't exist 👻
* Fixed: Incorrect argument passed to Gravity Forms Add-On registration function
* Fixed: Compatibility issue with the (excellent) [Gravity Forms Utility plugin](https://gravityplus.pro/gravity-forms-utility/) - thanks, Naomi C Bush!

= 1.0.2 on December 15 =

* Fixed PHP error when there are no values to calculate
* Updated German translation (Thank you, Hubert Test!)

= 1.0.1 on September 14 =

* Fix potential error blocking activation

= 1.0 =

* Launch!