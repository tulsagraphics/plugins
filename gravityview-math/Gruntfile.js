module.exports = function( grunt ) {

	'use strict';
	var banner = '/**\n * <%= pkg.homepage %>\n * Copyright (c) <%= grunt.template.today("yyyy") %>\n * This file is generated automatically. Do not edit.\n */\n';
	// Project configuration
	grunt.initConfig( {

		pkg: grunt.file.readJSON( 'package.json' ),

		// Pull in the latest translations
		exec: {
			transifex: 'tx pull -a',

			// Create a ZIP file
			zip: {
				cmd: function( filename = 'gravityview-math' ) {

					// First, create the full archive
					var command = 'git-archive-all gravityview-math.zip &&';

					command += 'unzip -o gravityview-math.zip &&';

					command += 'zip -r ../' + filename + '.zip "gravityview-math" &&';

					command += 'rm -rf gravityview-math/ && rm -f gravityview-math.zip';

					return command;
				}
			},

			bower: 'bower install'
		},

		makepot: {
			target: {
				options: {
					domainPath: '/languages',
					mainFile: 'gravityview-math.php',
					potFilename: 'gravityview-math.pot',
					potHeaders: {
						poedit: true,
						'x-poedit-keywordslist': true
					},
					type: 'wp-plugin',
					updateTimestamp: false,
					exclude: [ 'node_modules/.*', 'php-tests/.*', 'bin/.*', 'vendor/.*', 'tmp/.*' ],
					processPot: function( pot, options ) {
						pot.headers['language'] = 'en_US';

						var translation,
							excluded_meta = [
								'Math by GravityView',
								'Perform calculations inside or outside GravityView using the <code>[gv_math]</code> shortcode. Requires PHP 5.4.',
								'https://gravityview.co/extensions/math/',
								'GravityView',
								'https://gravityview.co/'
							];

						for ( translation in pot.translations[''] ) {
							if ( 'undefined' !== typeof pot.translations[''][ translation ].comments.extracted ) {
								if ( excluded_meta.indexOf( pot.translations[''][ translation ].msgid ) >= 0 ) {
									console.log( 'Excluded meta: ' + pot.translations[''][ translation ].msgid );
									delete pot.translations[''][ translation ];
								}
							}
						}

						return pot;
					}
				}
			}
		},

		dirs: {
			lang: 'languages'
		},

		// Convert the .po files to .mo files
		potomo: {
			dist: {
				options: {
					poDel: false
				},
				files: [{
					expand: true,
					cwd: '<%= dirs.lang %>',
					src: ['*.po','*.pot'],
					dest: '<%= dirs.lang %>',
					ext: '.mo',
					nonull: true
				}]
			}
		},

		// Add textdomain to all strings, and modify existing textdomains in included packages.
		addtextdomain: {
			options: {
				textdomain: 'gravityview-math',    // Project text domain.
				updateDomains: [ 'gravityview', 'gravity-view', 'gravityforms', 'edd_sl', 'edd', 'easy-digital-downloads' ]  // List of text domains to replace.
			},
			target: {
				files: {
					src: [
						'*.php',
						'**/*.php',
						'!node_modules/**',
						'!tests/**',
						'!tmp/**',
						'!vendor/**'
					]
				}
			}
		}

	} );

	grunt.loadNpmTasks( 'grunt-exec' );
	grunt.loadNpmTasks( 'grunt-potomo' );
	grunt.loadNpmTasks( 'grunt-wp-i18n' );
	grunt.loadNpmTasks( 'grunt-wp-readme-to-markdown' );

	// Translation stuff
	grunt.registerTask( 'translate', [ 'exec:transifex', 'potomo', 'addtextdomain', 'makepot' ] );

	grunt.registerTask( 'default', ['translate']);

	grunt.util.linefeed = '\n';

};
