<?php

/**
 * @class AutomateWoo_Legacy
 */
abstract class AutomateWoo_Legacy {

	/** @var AutomateWoo\Database_Tables */
	protected $database_tables;

	/**
	 * @var AutomateWoo\Session_Tracker
	 * @deprecated
	 */
	public $session_tracker;


	/**
	 * @deprecated since 3.6
	 *
	 * @param $id
	 * @return AutomateWoo\Unsubscribe|bool
	 */
	function get_unsubscribe( $id ) {
		return AutomateWoo\Unsubscribe_Factory::get( $id );
	}


	/**
	 * @deprecated since 3.8
	 * @return AutomateWoo\Database_Tables
	 */
	function database_tables() {
		if ( ! isset( $this->database_tables ) ) {
			$this->database_tables = new AutomateWoo\Database_Tables();
		}
		return $this->database_tables;
	}


}
