<?php

namespace AutomateWoo;

if ( ! defined( 'ABSPATH' ) ) exit;

class Exception extends \Exception {}
