<?php
/**
 * Override this template by copying it to yourtheme/automatewoo/unsubscribe-success.php
 */

if ( ! defined( 'ABSPATH' ) ) exit;

?>
	<div class="aw-unsubscribe confirmed">
		<strong><?php echo __( 'You have been unsubscribed.', 'automatewoo') ?></strong>
	</div>

