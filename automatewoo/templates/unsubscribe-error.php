<?php
/**
 * Override this template by copying it to yourtheme/automatewoo/unsubscribe-error.php
 */

if ( ! defined( 'ABSPATH' ) ) exit;

?>
	<div class="aw-unsubscribe failed">
		<strong><?php echo __( 'There was an error unsubscribing you.', 'automatewoo') ?></strong>
	</div>

