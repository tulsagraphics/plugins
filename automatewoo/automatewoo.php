<?php
/**
 * Plugin Name: AutomateWoo
 * Plugin URI: http://automatewoo.com
 * Description: Powerful marketing automation for your WooCommerce store.
 * Version: 4.3.2
 * Author: Prospress
 * Author URI: http://prospress.com
 * License: GPLv3
 * License URI: http://www.gnu.org/licenses/gpl-3.0
 * Text Domain: automatewoo
 * Domain Path: /languages
 *
 * WC requires at least: 2.6
 * WC tested up to: 3.4
 */

// Copyright (c) Prospress Inc. All rights reserved.
//
// Released under the GPLv3 license
// http://www.gnu.org/licenses/gpl-3.0
//
// **********************************************************************
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// **********************************************************************

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'AUTOMATEWOO_NAME', __( 'AutomateWoo', 'automatewoo' ) );
define( 'AUTOMATEWOO_SLUG', 'automatewoo' );
define( 'AUTOMATEWOO_VERSION', '4.3.2' );
define( 'AUTOMATEWOO_FILE', __FILE__ );
define( 'AUTOMATEWOO_PATH', dirname( __FILE__ ) );
define( 'AUTOMATEWOO_MIN_PHP_VER', '5.4' );
define( 'AUTOMATEWOO_MIN_WP_VER', '4.4.0' );
define( 'AUTOMATEWOO_MIN_WC_VER', '2.6.0' );


/**
 * @class AutomateWoo_Loader
 * @since 2.9
 */
class AutomateWoo_Loader {

	static $errors = array();


	static function init() {
		add_action( 'admin_notices', array( __CLASS__, 'admin_notices' ), 8 );
		add_action( 'plugins_loaded', array( __CLASS__, 'load' ), 8 ); // load core before addons
		add_action( 'plugins_loaded', array( __CLASS__, 'load_textdomain' ) );
	}


	static function load() {
		if ( self::check() ) {
			include AUTOMATEWOO_PATH . '/includes/automatewoo.php';
		}
	}


	static function load_textdomain() {
		load_plugin_textdomain( 'automatewoo', false, "automatewoo/languages" );
	}


	/**
	 * @return bool
	 */
	static function check() {
		$passed = true;

		$inactive_text = '<strong>' . sprintf( __( '%s is inactive.', 'automatewoo' ), AUTOMATEWOO_NAME ) . '</strong>';

		if ( version_compare( phpversion(), AUTOMATEWOO_MIN_PHP_VER, '<' ) ) {
			self::$errors[] = sprintf( __( '%s The plugin requires PHP version %s or newer.' , 'automatewoo' ), $inactive_text, AUTOMATEWOO_MIN_PHP_VER );
			$passed = false;
		}
		elseif ( ! self::is_woocommerce_version_ok() ) {
			self::$errors[] = sprintf(__( '%s The plugin requires WooCommerce version %s or newer.', 'automatewoo' ), $inactive_text, AUTOMATEWOO_MIN_WC_VER );
			$passed = false;
		}
		elseif ( ! self::is_wp_version_ok() ) {
			self::$errors[] = sprintf(__( '%s The plugin requires WordPress version %s or newer.', 'automatewoo' ), $inactive_text, AUTOMATEWOO_MIN_WP_VER );
			$passed = false;
		}

		return $passed;
	}


	/**
	 * @return bool
	 */
	static function is_woocommerce_version_ok() {
		if ( ! function_exists( 'WC' ) ) return false;
		if ( ! AUTOMATEWOO_MIN_WC_VER ) return true;
		return version_compare( WC()->version, AUTOMATEWOO_MIN_WC_VER, '>=' );
	}


	/**
	 * @return bool
	 */
	static function is_wp_version_ok() {
		global $wp_version;
		if ( ! AUTOMATEWOO_MIN_WP_VER ) return true;
		return version_compare( $wp_version, AUTOMATEWOO_MIN_WP_VER, '>=' );
	}


	static function admin_notices() {
		if ( empty( self::$errors ) ) return;
		echo '<div class="notice notice-error"><p>';
		echo implode( '<br>', self::$errors );
		echo '</p></div>';
	}

}

AutomateWoo_Loader::init();
