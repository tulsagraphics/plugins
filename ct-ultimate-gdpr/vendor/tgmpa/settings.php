<?php

/** TGM Plugin Activation settings
 * @see http://tgmpluginactivation.com/configuration/
 */

// init tgmpa settings
add_action( 'init', 'ct_ultimate_gdpr_register_acf_plugin' );
function ct_ultimate_gdpr_register_acf_plugin() {

	$config = array(
		'id'           => 'advanced-custom-fields-pro',
		// Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',
		// Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins',
		// Menu slug.
		'parent_slug'  => 'themes.php',
		// Parent menu slug.
		'capability'   => 'manage_options',
		// Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,
		// Show admin notices or not.The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme
		'dismissable'  => false,
		// If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => esc_html__( 'There is a new version of Advanced Custom Fields plugin available!', 'ct-ultimate-gdpr' ),
		// If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,
		// Automatically activate plugins after installation or not.
		'message'      => '',
		// Message to output right before the plugins table.
	);

	$plugins = array(

		array(
			'name'               => 'Advanced Custom Fields PRO', // The plugin name.
			'slug'               => 'advanced-custom-fields-pro', // The plugin slug (typically the folder name).
			'source'             => plugin_dir_path(__DIR__) . 'acf/advanced-custom-fields-pro.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => apply_filters( 'ct_plugin_op_version', '1.2', 'advanced-custom-fields-pro' ), // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => 'acf_pro', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),

	);

	tgmpa( $plugins, $config );

}