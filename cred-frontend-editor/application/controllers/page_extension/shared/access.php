<?php

namespace OTGS\Toolset\CRED\Controller\Forms\Shared\PageExtension;

/**
 * Form Access metabox extension.
 * 
 * @since 2.1
 * @todo Review this HTML layout, FGS
 */
class Access {

    /**
     * Generate the section for the Access integration information.
     *
     * @param object $form
     * @param array $callback_args
     * 
     * @since 2.1
     */
    public function print_metabox_content( $form, $callback_args = array() ) {
        $access_condition = new \Toolset_Condition_Plugin_Access_Active();
        $is_access_active = $access_condition->is_met();
        $form = $form->filter( 'raw' );
        $form_type = $form->post_type;

        ?>
        <table class="access-form-texts">
        <tbody>
        <div class="cred-notification <?php echo ($is_access_active ? "cred-info" :"cred-error"); ?>">
            <div class="<?php echo ($is_access_active ? "cred-info" :"cred-error"); ?>">
                <?php
                $txt_anchor = (isset($form_type) && $form_type == 'cred-user-form') ? "__CRED_CRED_USER_GROUP" : "__CRED_CRED_GROUP";
                if ($is_access_active) {
                    ?>
                    <p>
                        <i class="fa fa-info-circle"></i> 
                        <?php printf(
                                __('To control who can see and use this form, go to the %1$sAccess settings%2$s.', 'wp-cred'), 
                                '<a target="_parent" href="' 
                                    . admin_url( 'admin.php?page=types_access&tab=cred-forms' ) 
                                    . '">',
                                '</a>'
                                ); ?>
                    </p>    
                    <?php
                } else {
                    ?>
                    <p>
                        <i class="fa fa-warning"></i> 
                        <?php printf(
                                __('This Form will be accessible to everyone, including guest (not logged in). They will be able to submit/edit content using this form.<br>To control who can use the form, please install the %1$sAccess plugin%2$s.', 'wp-cred'), 
                                '<a target="_blank" href="https://toolset.com/home/toolset-components/#access">',
                                '</a>'
                            ); ?>
                    </p>
                    <?php
                }
                ?>
            </div>
        </div>
        </tbody>
        </table>
        <?php
   }
}