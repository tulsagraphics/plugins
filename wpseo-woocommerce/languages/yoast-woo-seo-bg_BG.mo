��          �       <      <     =     C     I  �   U     �               +  
   1  3   <  4   p  m   �       6   *  )   a     �  :   �  �   �     �  
   �     �  )  
     4     M  !   V     x     �  b   �  m     �   v     $  ^   ;  L   �     �  :   �   Admin Brand Breadcrumbs If you have product attributes for the following types, select them here, the plugin will make sure they're used for the appropriate Schema.org and OpenGraph markup. Manufacturer Price Schema & OpenGraph additions Stock Team Yoast The short description for this product is too long. The short description for this product is too short. This extension to WooCommerce and Yoast SEO makes sure there's perfect communication between the two plugins. Yoast SEO: WooCommerce You should write a short description for this product. Your short description has a good length. https://yoast.com https://yoast.com/wordpress/plugins/yoast-woocommerce-seo/ PO-Revision-Date: 2018-09-10 10:12:03+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.3.1
Language: bg
Project-Id-Version: WooCommerce SEO
 Администратор Марка Пътечка Ако разполагате с продуктови атрибути за следните типове, изберете ги оттук. Разширението ще се погрижи те да се използват със съответните кодове на Schema.org и OpenGraph. Производител Цена Schema & OpenGraph добавки Наличности Екипът на Yoast Краткото описание на този продукт е прекалено голямо. Краткото описание за този продукт е с недостатъчна дължина. Това разширение за WooCommerce и WordPress SEO от Yoast се грижи за отличната комуникация между двата модула. Yoast SEO: WooCommerce Трябва да напишете кратко описание на този продукт. Краткото описание е с достатъчна дължина. https://yoast.com https://yoast.com/wordpress/plugins/yoast-woocommerce-seo/ 