/* global YoastSEO */
/* global wpseoLocalL10n */

/* global YoastSEO: true, wpseoLocalL10n */


if ( typeof YoastSEO !== 'undefined' && typeof YoastSEO.analysisWorker !== 'undefined' ) {
	new YoastLocalSEOPagesPlugin();
}
else {
	jQuery( window ).on( 'YoastSEO:ready', () => new YoastLocalSEOPagesPlugin() );
}

class YoastLocalSEOPagesPlugin {
	constructor() {
		this.bindEventListener();
		this.loadWorkerScripts();
	}

	bindEventListener() {
		const elem = document.getElementById( 'wpseo_business_city' );
		if( elem !== null){
			elem.addEventListener( 'change', YoastSEO.app.refresh );
		}
	}

	loadWorkerScripts() {
		if ( typeof YoastSEO === 'undefined' || typeof YoastSEO.analysis === "undefined" || typeof YoastSEO.analysis.worker === "undefined" ) {
			return;
		}

		YoastSEO.analysis.worker.loadScript( wpseoLocalL10n.pages_script_url )
			.then( () => YoastSEO.analysis.worker.sendMessage( 'initializePages', wpseoLocalL10n, 'YoastLocalSEO' ) );
	}
}
