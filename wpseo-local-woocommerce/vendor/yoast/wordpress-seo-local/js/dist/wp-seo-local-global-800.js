(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

jQuery(document).ready(function ($) {
    $('#use_multiple_locations').click(function () {
        if ($(this).is(':checked')) {
            $('#use_multiple_locations').attr('disabled', true);
            $('#single-location-settings').slideUp(function () {
                $('#multiple-locations-settings').slideDown();
                $('#sl-settings').slideDown();
                $('#opening-hours-hours').slideUp(function () {
                    $('#use_multiple_locations').removeAttr('disabled');
                });
            });
            $('.open_247_wrapper').slideUp();
        } else {
            $('#use_multiple_locations').attr('disabled', true);
            $('#multiple-locations-settings').slideUp(function () {
                $('#single-location-settings').slideDown();
                if (!$('#hide_opening_hours').is(':checked')) {
                    $('#opening-hours-hours').slideDown();
                }
                $('#sl-settings').slideUp();
                $('#use_multiple_locations').removeAttr('disabled');
            });
            $('.open_247_wrapper').slideDown();
        }
    });

    $('#hide_opening_hours').click(function () {
        if ($(this).is(':checked')) {
            $('#opening-hours-hours, #opening-hours-settings').slideUp();
        } else {
            $('#opening-hours-settings').slideDown();
            if (!$('#use_multiple_locations').is(':checked')) {
                $('#opening-hours-hours').slideDown();
            }
        }
    });
    $('#multiple_opening_hours, #wpseo_multiple_opening_hours').click(function () {
        if ($(this).is(':checked')) {
            $('.opening-hours .opening-hours-second').slideDown();
        } else {
            $('.opening-hours .opening-hours-second').slideUp();
        }
    });
    $('#opening_hours_24h').click(function () {
        $('#opening-hours-container select').each(function () {
            $(this).find('option').each(function () {
                if ($('#opening_hours_24h').is(':checked')) {
                    // Use 24 hour
                    if ($(this).val() != 'closed') {
                        $(this).text($(this).val());
                    }
                } else {
                    // Use 12 hour
                    if ($(this).val() != 'closed') {
                        // Split the string between hours and minutes
                        var time = $(this).val().split(':');

                        // use parseInt to remove leading zeroes.
                        var hour = parseInt(time[0]);
                        var minutes = time[1];
                        var suffix = 'AM';

                        // if the hours number is greater than 12, subtract 12.
                        if (hour >= 12) {
                            if (hour > 12) {
                                hour = hour - 12;
                            }
                            suffix = 'PM';
                        }
                        if (hour == 0) {
                            hour = 12;
                        }

                        $(this).text(hour + ':' + minutes + ' ' + suffix);
                    }
                }
            });
        });
    });

    $('#open_247').on('click', function () {
        if (!$('#use_multiple_locations').is(":checked")) {
            maybeCloseOpeningHours(this);
            $('.open_247_wrapper').show();
        }
    });

    if (!$('#use_multiple_locations').is(":checked")) {
        maybeCloseOpeningHours('input#open_247');
    } else {
        $('.open_247_wrapper').hide();
    }

    $('#wpseo_open_247').on('click', function () {
        maybeCloseOpeningHours(this);
    });

    $('#opening-hours-hours .opening-hours').map(function (key, val) {
        maybeDisableHours($('.openinghours-wrapper', val), true);
    });

    $('#wpseo-local-metabox .opening-hours').map(function (key, val) {
        maybeDisableHours($('.openinghours-wrapper', val), true);
    });

    $('.wpseo_open_24h input').on('click', function () {
        var elem = $('.openinghours-wrapper', $(this).closest('.opening-hours'));
        maybeDisableHours(elem, false);
    });

    function maybeDisableHours(elem, initial) {
        if ($(elem).data('disabled') === true) {
            $('select', elem).attr('disabled', false);
            elem.data('disabled', false);
        } else {
            if (initial === true) {
                if ($('.wpseo_open_24h input', elem.context).is(":checked")) {
                    $('select', elem).attr('disabled', true);
                    elem.data('disabled', true);
                }
            } else {
                $('select', elem).attr('disabled', true);
                elem.data('disabled', true);
            }
        }
    }

    function maybeCloseOpeningHours(elem) {
        if ($(elem).is(':checked')) {
            $('#opening-hours-hours, .opening-hours').slideUp();
        } else {
            $('#opening-hours-hours, .opening-hours').slideDown();
        }
    }

    $('.widget-content').on('click', '#wpseo-checkbox-multiple-locations-wrapper input[type=checkbox]', function () {
        wpseo_show_all_locations_selectbox($(this));
    });

    // Show locations metabox before WP SEO metabox
    if ($('#wpseo_locations').length > 0 && $('#wpseo_meta').length > 0) {
        $('#wpseo_locations').insertBefore($('#wpseo_meta'));
    }

    $('.openinghours_from').change(function () {
        var to_id = $(this).attr('id').replace('_from', '_to_wrapper');
        var second_id = $(this).attr('id').replace('_from', '_second');

        if ($(this).val() == 'closed') {
            $('#' + to_id).css('display', 'none');
            $('#' + second_id).css('display', 'none');
        } else {
            $('#' + to_id).css('display', 'inline');
            $('#' + second_id).css('display', 'block');
        }
    }).change();
    $('.openinghours_from_second').change(function () {
        var to_id = $(this).attr('id').replace('_from', '_to_wrapper');

        if ($(this).val() == 'closed') {
            $('#' + to_id).css('display', 'none');
        } else {
            $('#' + to_id).css('display', 'inline');
        }
    }).change();
    $('.openinghours_to').change(function () {
        var from_id = $(this).attr('id').replace('_to', '_from');
        var to_id = $(this).attr('id').replace('_to', '_to_wrapper');
        if ($(this).val() == 'closed') {
            $('#' + to_id).css('display', 'none');
            $('#' + from_id).val('closed');
        }
    });
    $('.openinghours_to_second').change(function () {
        var from_id = $(this).attr('id').replace('_to', '_from');
        var to_id = $(this).attr('id').replace('_to', '_to_wrapper');
        if ($(this).val() == 'closed') {
            $('#' + to_id).css('display', 'none');
            $('#' + from_id).val('closed');
        }
    });

    if ($('.set_custom_images').length > 0) {
        if (typeof wp !== 'undefined' && wp.media && wp.media.editor) {
            $('.wrap').on('click', '.set_custom_images', function (e) {
                e.preventDefault();
                var button = $(this);
                var id = button.attr('data-id');
                wp.media.editor.send.attachment = function (props, attachment) {
                    if (attachment.hasOwnProperty('sizes')) {
                        var url = attachment.sizes[props.size].url;
                    } else {
                        var url = attachment.url;
                    }

                    $('#' + id + '_image_container').attr('src', url);
                    $('.wpseo-local-' + id + '-wrapper .wpseo-local-hide-button').show();
                    $('#hidden_' + id).attr('value', attachment.id);
                };
                wp.media.editor.open(button);
                return false;
            });
        }
    }

    $('.remove_custom_image').on('click', function (e) {
        e.preventDefault();

        var id = $(this).attr('data-id');
        $('#' + id).attr('src', '').hide();
        $('#hidden_' + id).attr('value', '');
        $('.wpseo-local-' + id + '-wrapper .wpseo-local-hide-button').hide();
    });

    // Copy location data
    $('#wpseo_copy_from_location').change(function () {
        var location_id = $(this).val();

        if (location_id == '') return;

        $.post(wpseo_local_data.ajaxurl, {
            location_id: location_id,
            security: wpseo_local_data.sec_nonce,
            action: 'wpseo_copy_location'
        }, function (result) {
            if (result.charAt(result.length - 1) == 0) {
                result = result.slice(0, -1);
            } else if (result.substring(result.length - 2) == "-1") {
                result = result.slice(0, -2);
            }

            var data = $.parseJSON(result);
            if (data.success == 'true' || data.success == true) {

                for (var i in data.location) {
                    var value = data.location[i];

                    if (value != null && value != '' && typeof value != 'undefined') {
                        if (i == 'is_postal_address' || i == 'multiple_opening_hours') {
                            if (value == '1') {
                                $('#wpseo_' + i).attr('checked', 'checked');
                                $('.opening-hours .opening-hour-second').slideDown();
                            }
                        } else if (i.indexOf('opening_hours') > -1) {
                            $('#' + i).val(value);
                        } else {
                            $('#wpseo_' + i).val(value);
                        }
                    }
                }
            }
        });
    });
});

window.wpseo_show_all_locations_selectbox = function (obj) {
    $ = jQuery;

    $obj = $(obj);
    var parent = $obj.parents('.widget-inside');
    var $locationsWrapper = $('#wpseo-locations-wrapper', parent);

    if ($obj.is(':checked')) {
        $locationsWrapper.slideUp();
    } else {
        $locationsWrapper.slideDown();
    }
};

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJqcy9zcmMvd3Atc2VvLWxvY2FsLWdsb2JhbC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FDQUEsT0FBTyxRQUFQLEVBQWlCLEtBQWpCLENBQXVCLFVBQVUsQ0FBVixFQUFhO0FBQ2hDLE1BQUUseUJBQUYsRUFBNkIsS0FBN0IsQ0FBbUMsWUFBWTtBQUMzQyxZQUFJLEVBQUUsSUFBRixFQUFRLEVBQVIsQ0FBVyxVQUFYLENBQUosRUFBNEI7QUFDeEIsY0FBRSx5QkFBRixFQUE2QixJQUE3QixDQUFrQyxVQUFsQyxFQUE4QyxJQUE5QztBQUNBLGNBQUUsMkJBQUYsRUFBK0IsT0FBL0IsQ0FBdUMsWUFBWTtBQUMvQyxrQkFBRSw4QkFBRixFQUFrQyxTQUFsQztBQUNBLGtCQUFFLGNBQUYsRUFBa0IsU0FBbEI7QUFDQSxrQkFBRSxzQkFBRixFQUEwQixPQUExQixDQUFrQyxZQUFZO0FBQzFDLHNCQUFFLHlCQUFGLEVBQTZCLFVBQTdCLENBQXdDLFVBQXhDO0FBQ0gsaUJBRkQ7QUFHSCxhQU5EO0FBT0EsY0FBRSxtQkFBRixFQUF1QixPQUF2QjtBQUNILFNBVkQsTUFXSztBQUNELGNBQUUseUJBQUYsRUFBNkIsSUFBN0IsQ0FBa0MsVUFBbEMsRUFBOEMsSUFBOUM7QUFDQSxjQUFFLDhCQUFGLEVBQWtDLE9BQWxDLENBQTBDLFlBQVk7QUFDbEQsa0JBQUUsMkJBQUYsRUFBK0IsU0FBL0I7QUFDQSxvQkFBSSxDQUFDLEVBQUUscUJBQUYsRUFBeUIsRUFBekIsQ0FBNEIsVUFBNUIsQ0FBTCxFQUE4QztBQUMxQyxzQkFBRSxzQkFBRixFQUEwQixTQUExQjtBQUNIO0FBQ0Qsa0JBQUUsY0FBRixFQUFrQixPQUFsQjtBQUNBLGtCQUFFLHlCQUFGLEVBQTZCLFVBQTdCLENBQXdDLFVBQXhDO0FBQ0gsYUFQRDtBQVFBLGNBQUUsbUJBQUYsRUFBdUIsU0FBdkI7QUFFSDtBQUNKLEtBekJEOztBQTJCQSxNQUFFLHFCQUFGLEVBQXlCLEtBQXpCLENBQStCLFlBQVk7QUFDdkMsWUFBSSxFQUFFLElBQUYsRUFBUSxFQUFSLENBQVcsVUFBWCxDQUFKLEVBQTRCO0FBQ3hCLGNBQUUsK0NBQUYsRUFBbUQsT0FBbkQ7QUFDSCxTQUZELE1BR0s7QUFDRCxjQUFFLHlCQUFGLEVBQTZCLFNBQTdCO0FBQ0EsZ0JBQUksQ0FBQyxFQUFFLHlCQUFGLEVBQTZCLEVBQTdCLENBQWdDLFVBQWhDLENBQUwsRUFBa0Q7QUFDOUMsa0JBQUUsc0JBQUYsRUFBMEIsU0FBMUI7QUFDSDtBQUNKO0FBQ0osS0FWRDtBQVdBLE1BQUUsd0RBQUYsRUFBNEQsS0FBNUQsQ0FBa0UsWUFBWTtBQUMxRSxZQUFJLEVBQUUsSUFBRixFQUFRLEVBQVIsQ0FBVyxVQUFYLENBQUosRUFBNEI7QUFDeEIsY0FBRSxzQ0FBRixFQUEwQyxTQUExQztBQUNILFNBRkQsTUFHSztBQUNELGNBQUUsc0NBQUYsRUFBMEMsT0FBMUM7QUFDSDtBQUNKLEtBUEQ7QUFRQSxNQUFFLG9CQUFGLEVBQXdCLEtBQXhCLENBQThCLFlBQVk7QUFDdEMsVUFBRSxpQ0FBRixFQUFxQyxJQUFyQyxDQUEwQyxZQUFZO0FBQ2xELGNBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxRQUFiLEVBQXVCLElBQXZCLENBQTRCLFlBQVk7QUFDcEMsb0JBQUksRUFBRSxvQkFBRixFQUF3QixFQUF4QixDQUEyQixVQUEzQixDQUFKLEVBQTRDO0FBQ3hDO0FBQ0Esd0JBQUksRUFBRSxJQUFGLEVBQVEsR0FBUixNQUFpQixRQUFyQixFQUErQjtBQUMzQiwwQkFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLEVBQUUsSUFBRixFQUFRLEdBQVIsRUFBYjtBQUNIO0FBQ0osaUJBTEQsTUFLTztBQUNIO0FBQ0Esd0JBQUksRUFBRSxJQUFGLEVBQVEsR0FBUixNQUFpQixRQUFyQixFQUErQjtBQUMzQjtBQUNBLDRCQUFJLE9BQU8sRUFBRSxJQUFGLEVBQVEsR0FBUixHQUFjLEtBQWQsQ0FBb0IsR0FBcEIsQ0FBWDs7QUFFQTtBQUNBLDRCQUFJLE9BQU8sU0FBUyxLQUFLLENBQUwsQ0FBVCxDQUFYO0FBQ0EsNEJBQUksVUFBVSxLQUFLLENBQUwsQ0FBZDtBQUNBLDRCQUFJLFNBQVMsSUFBYjs7QUFFQTtBQUNBLDRCQUFJLFFBQVEsRUFBWixFQUFnQjtBQUNaLGdDQUFJLE9BQU8sRUFBWCxFQUFlO0FBQ1gsdUNBQU8sT0FBTyxFQUFkO0FBQ0g7QUFDRCxxQ0FBUyxJQUFUO0FBQ0g7QUFDRCw0QkFBSSxRQUFRLENBQVosRUFBZTtBQUNYLG1DQUFPLEVBQVA7QUFDSDs7QUFFRCwwQkFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLE9BQU8sR0FBUCxHQUFhLE9BQWIsR0FBdUIsR0FBdkIsR0FBNkIsTUFBMUM7QUFDSDtBQUNKO0FBQ0osYUEvQkQ7QUFnQ0gsU0FqQ0Q7QUFrQ0gsS0FuQ0Q7O0FBcUNBLE1BQUUsV0FBRixFQUFlLEVBQWYsQ0FBa0IsT0FBbEIsRUFBMkIsWUFBWTtBQUNuQyxZQUFJLENBQUMsRUFBRSx5QkFBRixFQUE2QixFQUE3QixDQUFnQyxVQUFoQyxDQUFMLEVBQWtEO0FBQzlDLG1DQUF1QixJQUF2QjtBQUNBLGNBQUUsbUJBQUYsRUFBdUIsSUFBdkI7QUFDSDtBQUNKLEtBTEQ7O0FBT0EsUUFBSSxDQUFDLEVBQUUseUJBQUYsRUFBNkIsRUFBN0IsQ0FBZ0MsVUFBaEMsQ0FBTCxFQUFrRDtBQUM5QywrQkFBdUIsZ0JBQXZCO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsVUFBRSxtQkFBRixFQUF1QixJQUF2QjtBQUNIOztBQUVELE1BQUUsaUJBQUYsRUFBcUIsRUFBckIsQ0FBd0IsT0FBeEIsRUFBaUMsWUFBWTtBQUN6QywrQkFBdUIsSUFBdkI7QUFDSCxLQUZEOztBQUlBLE1BQUUscUNBQUYsRUFBeUMsR0FBekMsQ0FBNkMsVUFBVSxHQUFWLEVBQWUsR0FBZixFQUFvQjtBQUM3RCwwQkFBa0IsRUFBRSx1QkFBRixFQUEyQixHQUEzQixDQUFsQixFQUFtRCxJQUFuRDtBQUNILEtBRkQ7O0FBSUEsTUFBRSxxQ0FBRixFQUF5QyxHQUF6QyxDQUE2QyxVQUFVLEdBQVYsRUFBZSxHQUFmLEVBQW9CO0FBQzdELDBCQUFrQixFQUFFLHVCQUFGLEVBQTJCLEdBQTNCLENBQWxCLEVBQW1ELElBQW5EO0FBQ0gsS0FGRDs7QUFJQSxNQUFFLHVCQUFGLEVBQTJCLEVBQTNCLENBQThCLE9BQTlCLEVBQXVDLFlBQVk7QUFDL0MsWUFBSSxPQUFPLEVBQUUsdUJBQUYsRUFBMkIsRUFBRSxJQUFGLEVBQVEsT0FBUixDQUFnQixnQkFBaEIsQ0FBM0IsQ0FBWDtBQUNBLDBCQUFrQixJQUFsQixFQUF3QixLQUF4QjtBQUNILEtBSEQ7O0FBS0EsYUFBUyxpQkFBVCxDQUEyQixJQUEzQixFQUFpQyxPQUFqQyxFQUEwQztBQUN0QyxZQUFJLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxVQUFiLE1BQTZCLElBQWpDLEVBQXVDO0FBQ25DLGNBQUUsUUFBRixFQUFZLElBQVosRUFBa0IsSUFBbEIsQ0FBdUIsVUFBdkIsRUFBbUMsS0FBbkM7QUFDQSxpQkFBSyxJQUFMLENBQVUsVUFBVixFQUFzQixLQUF0QjtBQUNILFNBSEQsTUFHTztBQUNILGdCQUFJLFlBQVksSUFBaEIsRUFBc0I7QUFDbEIsb0JBQUksRUFBRSx1QkFBRixFQUEyQixLQUFLLE9BQWhDLEVBQXlDLEVBQXpDLENBQTRDLFVBQTVDLENBQUosRUFBNkQ7QUFDekQsc0JBQUUsUUFBRixFQUFZLElBQVosRUFBa0IsSUFBbEIsQ0FBdUIsVUFBdkIsRUFBbUMsSUFBbkM7QUFDQSx5QkFBSyxJQUFMLENBQVUsVUFBVixFQUFzQixJQUF0QjtBQUNIO0FBQ0osYUFMRCxNQUtPO0FBQ0gsa0JBQUUsUUFBRixFQUFZLElBQVosRUFBa0IsSUFBbEIsQ0FBdUIsVUFBdkIsRUFBbUMsSUFBbkM7QUFDQSxxQkFBSyxJQUFMLENBQVUsVUFBVixFQUFzQixJQUF0QjtBQUNIO0FBQ0o7QUFDSjs7QUFFRCxhQUFTLHNCQUFULENBQWdDLElBQWhDLEVBQXNDO0FBQ2xDLFlBQUksRUFBRSxJQUFGLEVBQVEsRUFBUixDQUFXLFVBQVgsQ0FBSixFQUE0QjtBQUN4QixjQUFFLHNDQUFGLEVBQTBDLE9BQTFDO0FBQ0gsU0FGRCxNQUVPO0FBQ0gsY0FBRSxzQ0FBRixFQUEwQyxTQUExQztBQUNIO0FBQ0o7O0FBRUQsTUFBRSxpQkFBRixFQUFxQixFQUFyQixDQUF3QixPQUF4QixFQUFpQyxpRUFBakMsRUFBb0csWUFBWTtBQUM1RywyQ0FBbUMsRUFBRSxJQUFGLENBQW5DO0FBQ0gsS0FGRDs7QUFJQTtBQUNBLFFBQUksRUFBRSxrQkFBRixFQUFzQixNQUF0QixHQUErQixDQUEvQixJQUFvQyxFQUFFLGFBQUYsRUFBaUIsTUFBakIsR0FBMEIsQ0FBbEUsRUFBcUU7QUFDakUsVUFBRSxrQkFBRixFQUFzQixZQUF0QixDQUFtQyxFQUFFLGFBQUYsQ0FBbkM7QUFDSDs7QUFFRCxNQUFFLG9CQUFGLEVBQXdCLE1BQXhCLENBQStCLFlBQVk7QUFDdkMsWUFBSSxRQUFRLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxJQUFiLEVBQW1CLE9BQW5CLENBQTJCLE9BQTNCLEVBQW9DLGFBQXBDLENBQVo7QUFDQSxZQUFJLFlBQVksRUFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLElBQWIsRUFBbUIsT0FBbkIsQ0FBMkIsT0FBM0IsRUFBb0MsU0FBcEMsQ0FBaEI7O0FBRUEsWUFBSSxFQUFFLElBQUYsRUFBUSxHQUFSLE1BQWlCLFFBQXJCLEVBQStCO0FBQzNCLGNBQUUsTUFBTSxLQUFSLEVBQWUsR0FBZixDQUFtQixTQUFuQixFQUE4QixNQUE5QjtBQUNBLGNBQUUsTUFBTSxTQUFSLEVBQW1CLEdBQW5CLENBQXVCLFNBQXZCLEVBQWtDLE1BQWxDO0FBQ0gsU0FIRCxNQUlLO0FBQ0QsY0FBRSxNQUFNLEtBQVIsRUFBZSxHQUFmLENBQW1CLFNBQW5CLEVBQThCLFFBQTlCO0FBQ0EsY0FBRSxNQUFNLFNBQVIsRUFBbUIsR0FBbkIsQ0FBdUIsU0FBdkIsRUFBa0MsT0FBbEM7QUFDSDtBQUNKLEtBWkQsRUFZRyxNQVpIO0FBYUEsTUFBRSwyQkFBRixFQUErQixNQUEvQixDQUFzQyxZQUFZO0FBQzlDLFlBQUksUUFBUSxFQUFFLElBQUYsRUFBUSxJQUFSLENBQWEsSUFBYixFQUFtQixPQUFuQixDQUEyQixPQUEzQixFQUFvQyxhQUFwQyxDQUFaOztBQUVBLFlBQUksRUFBRSxJQUFGLEVBQVEsR0FBUixNQUFpQixRQUFyQixFQUErQjtBQUMzQixjQUFFLE1BQU0sS0FBUixFQUFlLEdBQWYsQ0FBbUIsU0FBbkIsRUFBOEIsTUFBOUI7QUFDSCxTQUZELE1BR0s7QUFDRCxjQUFFLE1BQU0sS0FBUixFQUFlLEdBQWYsQ0FBbUIsU0FBbkIsRUFBOEIsUUFBOUI7QUFDSDtBQUNKLEtBVEQsRUFTRyxNQVRIO0FBVUEsTUFBRSxrQkFBRixFQUFzQixNQUF0QixDQUE2QixZQUFZO0FBQ3JDLFlBQUksVUFBVSxFQUFFLElBQUYsRUFBUSxJQUFSLENBQWEsSUFBYixFQUFtQixPQUFuQixDQUEyQixLQUEzQixFQUFrQyxPQUFsQyxDQUFkO0FBQ0EsWUFBSSxRQUFRLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxJQUFiLEVBQW1CLE9BQW5CLENBQTJCLEtBQTNCLEVBQWtDLGFBQWxDLENBQVo7QUFDQSxZQUFJLEVBQUUsSUFBRixFQUFRLEdBQVIsTUFBaUIsUUFBckIsRUFBK0I7QUFDM0IsY0FBRSxNQUFNLEtBQVIsRUFBZSxHQUFmLENBQW1CLFNBQW5CLEVBQThCLE1BQTlCO0FBQ0EsY0FBRSxNQUFNLE9BQVIsRUFBaUIsR0FBakIsQ0FBcUIsUUFBckI7QUFDSDtBQUNKLEtBUEQ7QUFRQSxNQUFFLHlCQUFGLEVBQTZCLE1BQTdCLENBQW9DLFlBQVk7QUFDNUMsWUFBSSxVQUFVLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxJQUFiLEVBQW1CLE9BQW5CLENBQTJCLEtBQTNCLEVBQWtDLE9BQWxDLENBQWQ7QUFDQSxZQUFJLFFBQVEsRUFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLElBQWIsRUFBbUIsT0FBbkIsQ0FBMkIsS0FBM0IsRUFBa0MsYUFBbEMsQ0FBWjtBQUNBLFlBQUksRUFBRSxJQUFGLEVBQVEsR0FBUixNQUFpQixRQUFyQixFQUErQjtBQUMzQixjQUFFLE1BQU0sS0FBUixFQUFlLEdBQWYsQ0FBbUIsU0FBbkIsRUFBOEIsTUFBOUI7QUFDQSxjQUFFLE1BQU0sT0FBUixFQUFpQixHQUFqQixDQUFxQixRQUFyQjtBQUNIO0FBQ0osS0FQRDs7QUFTQSxRQUFJLEVBQUUsb0JBQUYsRUFBd0IsTUFBeEIsR0FBaUMsQ0FBckMsRUFBd0M7QUFDcEMsWUFBSSxPQUFPLEVBQVAsS0FBYyxXQUFkLElBQTZCLEdBQUcsS0FBaEMsSUFBeUMsR0FBRyxLQUFILENBQVMsTUFBdEQsRUFBOEQ7QUFDMUQsY0FBRSxPQUFGLEVBQVcsRUFBWCxDQUFjLE9BQWQsRUFBdUIsb0JBQXZCLEVBQTZDLFVBQVUsQ0FBVixFQUFhO0FBQ3RELGtCQUFFLGNBQUY7QUFDQSxvQkFBSSxTQUFTLEVBQUUsSUFBRixDQUFiO0FBQ0Esb0JBQUksS0FBSyxPQUFPLElBQVAsQ0FBWSxTQUFaLENBQVQ7QUFDQSxtQkFBRyxLQUFILENBQVMsTUFBVCxDQUFnQixJQUFoQixDQUFxQixVQUFyQixHQUFrQyxVQUFVLEtBQVYsRUFBaUIsVUFBakIsRUFBNkI7QUFDM0Qsd0JBQUksV0FBVyxjQUFYLENBQTBCLE9BQTFCLENBQUosRUFBd0M7QUFDcEMsNEJBQUksTUFBTSxXQUFXLEtBQVgsQ0FBaUIsTUFBTSxJQUF2QixFQUE2QixHQUF2QztBQUNILHFCQUZELE1BRU87QUFDSCw0QkFBSSxNQUFNLFdBQVcsR0FBckI7QUFDSDs7QUFFRCxzQkFBRSxNQUFNLEVBQU4sR0FBVyxrQkFBYixFQUFpQyxJQUFqQyxDQUFzQyxLQUF0QyxFQUE2QyxHQUE3QztBQUNBLHNCQUFFLGtCQUFrQixFQUFsQixHQUF1QixtQ0FBekIsRUFBOEQsSUFBOUQ7QUFDQSxzQkFBRSxhQUFhLEVBQWYsRUFBbUIsSUFBbkIsQ0FBd0IsT0FBeEIsRUFBaUMsV0FBVyxFQUE1QztBQUNILGlCQVZEO0FBV0EsbUJBQUcsS0FBSCxDQUFTLE1BQVQsQ0FBZ0IsSUFBaEIsQ0FBcUIsTUFBckI7QUFDQSx1QkFBTyxLQUFQO0FBQ0gsYUFqQkQ7QUFrQkg7QUFDSjs7QUFFRCxNQUFFLHNCQUFGLEVBQTBCLEVBQTFCLENBQTZCLE9BQTdCLEVBQXNDLFVBQVUsQ0FBVixFQUFhO0FBQy9DLFVBQUUsY0FBRjs7QUFFQSxZQUFJLEtBQUssRUFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLFNBQWIsQ0FBVDtBQUNBLFVBQUUsTUFBTSxFQUFSLEVBQVksSUFBWixDQUFpQixLQUFqQixFQUF3QixFQUF4QixFQUE0QixJQUE1QjtBQUNBLFVBQUUsYUFBYSxFQUFmLEVBQW1CLElBQW5CLENBQXdCLE9BQXhCLEVBQWlDLEVBQWpDO0FBQ0EsVUFBRSxrQkFBa0IsRUFBbEIsR0FBdUIsbUNBQXpCLEVBQThELElBQTlEO0FBQ0gsS0FQRDs7QUFTQTtBQUNBLE1BQUUsMkJBQUYsRUFBK0IsTUFBL0IsQ0FBc0MsWUFBWTtBQUM5QyxZQUFJLGNBQWMsRUFBRSxJQUFGLEVBQVEsR0FBUixFQUFsQjs7QUFFQSxZQUFJLGVBQWUsRUFBbkIsRUFDSTs7QUFFSixVQUFFLElBQUYsQ0FBTyxpQkFBaUIsT0FBeEIsRUFBaUM7QUFDN0IseUJBQWEsV0FEZ0I7QUFFN0Isc0JBQVUsaUJBQWlCLFNBRkU7QUFHN0Isb0JBQVE7QUFIcUIsU0FBakMsRUFJRyxVQUFVLE1BQVYsRUFBa0I7QUFDakIsZ0JBQUksT0FBTyxNQUFQLENBQWMsT0FBTyxNQUFQLEdBQWdCLENBQTlCLEtBQW9DLENBQXhDLEVBQTJDO0FBQ3ZDLHlCQUFTLE9BQU8sS0FBUCxDQUFhLENBQWIsRUFBZ0IsQ0FBQyxDQUFqQixDQUFUO0FBQ0gsYUFGRCxNQUdLLElBQUksT0FBTyxTQUFQLENBQWlCLE9BQU8sTUFBUCxHQUFnQixDQUFqQyxLQUF1QyxJQUEzQyxFQUFpRDtBQUNsRCx5QkFBUyxPQUFPLEtBQVAsQ0FBYSxDQUFiLEVBQWdCLENBQUMsQ0FBakIsQ0FBVDtBQUNIOztBQUVELGdCQUFJLE9BQU8sRUFBRSxTQUFGLENBQVksTUFBWixDQUFYO0FBQ0EsZ0JBQUksS0FBSyxPQUFMLElBQWdCLE1BQWhCLElBQTBCLEtBQUssT0FBTCxJQUFnQixJQUE5QyxFQUFvRDs7QUFFaEQscUJBQUssSUFBSSxDQUFULElBQWMsS0FBSyxRQUFuQixFQUE2QjtBQUN6Qix3QkFBSSxRQUFRLEtBQUssUUFBTCxDQUFjLENBQWQsQ0FBWjs7QUFFQSx3QkFBSSxTQUFTLElBQVQsSUFBaUIsU0FBUyxFQUExQixJQUFnQyxPQUFPLEtBQVAsSUFBZ0IsV0FBcEQsRUFBaUU7QUFDN0QsNEJBQUksS0FBSyxtQkFBTCxJQUE0QixLQUFLLHdCQUFyQyxFQUErRDtBQUMzRCxnQ0FBSSxTQUFTLEdBQWIsRUFBa0I7QUFDZCxrQ0FBRSxZQUFZLENBQWQsRUFBaUIsSUFBakIsQ0FBc0IsU0FBdEIsRUFBaUMsU0FBakM7QUFDQSxrQ0FBRSxxQ0FBRixFQUF5QyxTQUF6QztBQUNIO0FBQ0oseUJBTEQsTUFNSyxJQUFJLEVBQUUsT0FBRixDQUFVLGVBQVYsSUFBNkIsQ0FBQyxDQUFsQyxFQUFxQztBQUN0Qyw4QkFBRSxNQUFNLENBQVIsRUFBVyxHQUFYLENBQWUsS0FBZjtBQUNILHlCQUZJLE1BR0E7QUFDRCw4QkFBRSxZQUFZLENBQWQsRUFBaUIsR0FBakIsQ0FBcUIsS0FBckI7QUFDSDtBQUNKO0FBQ0o7QUFDSjtBQUNKLFNBbENEO0FBbUNILEtBekNEO0FBMENILENBdlFEOztBQXlRQSxPQUFPLGtDQUFQLEdBQTRDLFVBQVUsR0FBVixFQUFlO0FBQ3ZELFFBQUksTUFBSjs7QUFFQSxXQUFPLEVBQUUsR0FBRixDQUFQO0FBQ0EsUUFBSSxTQUFTLEtBQUssT0FBTCxDQUFhLGdCQUFiLENBQWI7QUFDQSxRQUFJLG9CQUFvQixFQUFFLDBCQUFGLEVBQThCLE1BQTlCLENBQXhCOztBQUVBLFFBQUksS0FBSyxFQUFMLENBQVEsVUFBUixDQUFKLEVBQXlCO0FBQ3JCLDBCQUFrQixPQUFsQjtBQUNILEtBRkQsTUFHSztBQUNELDBCQUFrQixTQUFsQjtBQUNIO0FBQ0osQ0FiRCIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJqUXVlcnkoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgkKSB7XG4gICAgJCgnI3VzZV9tdWx0aXBsZV9sb2NhdGlvbnMnKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICgkKHRoaXMpLmlzKCc6Y2hlY2tlZCcpKSB7XG4gICAgICAgICAgICAkKCcjdXNlX211bHRpcGxlX2xvY2F0aW9ucycpLmF0dHIoJ2Rpc2FibGVkJywgdHJ1ZSk7XG4gICAgICAgICAgICAkKCcjc2luZ2xlLWxvY2F0aW9uLXNldHRpbmdzJykuc2xpZGVVcChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgJCgnI211bHRpcGxlLWxvY2F0aW9ucy1zZXR0aW5ncycpLnNsaWRlRG93bigpO1xuICAgICAgICAgICAgICAgICQoJyNzbC1zZXR0aW5ncycpLnNsaWRlRG93bigpO1xuICAgICAgICAgICAgICAgICQoJyNvcGVuaW5nLWhvdXJzLWhvdXJzJykuc2xpZGVVcChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICQoJyN1c2VfbXVsdGlwbGVfbG9jYXRpb25zJykucmVtb3ZlQXR0cignZGlzYWJsZWQnKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgJCgnLm9wZW5fMjQ3X3dyYXBwZXInKS5zbGlkZVVwKCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAkKCcjdXNlX211bHRpcGxlX2xvY2F0aW9ucycpLmF0dHIoJ2Rpc2FibGVkJywgdHJ1ZSk7XG4gICAgICAgICAgICAkKCcjbXVsdGlwbGUtbG9jYXRpb25zLXNldHRpbmdzJykuc2xpZGVVcChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgJCgnI3NpbmdsZS1sb2NhdGlvbi1zZXR0aW5ncycpLnNsaWRlRG93bigpO1xuICAgICAgICAgICAgICAgIGlmICghJCgnI2hpZGVfb3BlbmluZ19ob3VycycpLmlzKCc6Y2hlY2tlZCcpKSB7XG4gICAgICAgICAgICAgICAgICAgICQoJyNvcGVuaW5nLWhvdXJzLWhvdXJzJykuc2xpZGVEb3duKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICQoJyNzbC1zZXR0aW5ncycpLnNsaWRlVXAoKTtcbiAgICAgICAgICAgICAgICAkKCcjdXNlX211bHRpcGxlX2xvY2F0aW9ucycpLnJlbW92ZUF0dHIoJ2Rpc2FibGVkJyk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICQoJy5vcGVuXzI0N193cmFwcGVyJykuc2xpZGVEb3duKCk7XG5cbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgJCgnI2hpZGVfb3BlbmluZ19ob3VycycpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKCQodGhpcykuaXMoJzpjaGVja2VkJykpIHtcbiAgICAgICAgICAgICQoJyNvcGVuaW5nLWhvdXJzLWhvdXJzLCAjb3BlbmluZy1ob3Vycy1zZXR0aW5ncycpLnNsaWRlVXAoKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICQoJyNvcGVuaW5nLWhvdXJzLXNldHRpbmdzJykuc2xpZGVEb3duKCk7XG4gICAgICAgICAgICBpZiAoISQoJyN1c2VfbXVsdGlwbGVfbG9jYXRpb25zJykuaXMoJzpjaGVja2VkJykpIHtcbiAgICAgICAgICAgICAgICAkKCcjb3BlbmluZy1ob3Vycy1ob3VycycpLnNsaWRlRG93bigpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSk7XG4gICAgJCgnI211bHRpcGxlX29wZW5pbmdfaG91cnMsICN3cHNlb19tdWx0aXBsZV9vcGVuaW5nX2hvdXJzJykuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoJCh0aGlzKS5pcygnOmNoZWNrZWQnKSkge1xuICAgICAgICAgICAgJCgnLm9wZW5pbmctaG91cnMgLm9wZW5pbmctaG91cnMtc2Vjb25kJykuc2xpZGVEb3duKCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAkKCcub3BlbmluZy1ob3VycyAub3BlbmluZy1ob3Vycy1zZWNvbmQnKS5zbGlkZVVwKCk7XG4gICAgICAgIH1cbiAgICB9KTtcbiAgICAkKCcjb3BlbmluZ19ob3Vyc18yNGgnKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAgICQoJyNvcGVuaW5nLWhvdXJzLWNvbnRhaW5lciBzZWxlY3QnKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICQodGhpcykuZmluZCgnb3B0aW9uJykuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgaWYgKCQoJyNvcGVuaW5nX2hvdXJzXzI0aCcpLmlzKCc6Y2hlY2tlZCcpKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIFVzZSAyNCBob3VyXG4gICAgICAgICAgICAgICAgICAgIGlmICgkKHRoaXMpLnZhbCgpICE9ICdjbG9zZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLnRleHQoJCh0aGlzKS52YWwoKSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAvLyBVc2UgMTIgaG91clxuICAgICAgICAgICAgICAgICAgICBpZiAoJCh0aGlzKS52YWwoKSAhPSAnY2xvc2VkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gU3BsaXQgdGhlIHN0cmluZyBiZXR3ZWVuIGhvdXJzIGFuZCBtaW51dGVzXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgdGltZSA9ICQodGhpcykudmFsKCkuc3BsaXQoJzonKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gdXNlIHBhcnNlSW50IHRvIHJlbW92ZSBsZWFkaW5nIHplcm9lcy5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBob3VyID0gcGFyc2VJbnQodGltZVswXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgbWludXRlcyA9IHRpbWVbMV07XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgc3VmZml4ID0gJ0FNJztcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gaWYgdGhlIGhvdXJzIG51bWJlciBpcyBncmVhdGVyIHRoYW4gMTIsIHN1YnRyYWN0IDEyLlxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGhvdXIgPj0gMTIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoaG91ciA+IDEyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhvdXIgPSBob3VyIC0gMTI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN1ZmZpeCA9ICdQTSc7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaG91ciA9PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaG91ciA9IDEyO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLnRleHQoaG91ciArICc6JyArIG1pbnV0ZXMgKyAnICcgKyBzdWZmaXgpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pXG4gICAgfSk7XG5cbiAgICAkKCcjb3Blbl8yNDcnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICghJCgnI3VzZV9tdWx0aXBsZV9sb2NhdGlvbnMnKS5pcyhcIjpjaGVja2VkXCIpKSB7XG4gICAgICAgICAgICBtYXliZUNsb3NlT3BlbmluZ0hvdXJzKHRoaXMpO1xuICAgICAgICAgICAgJCgnLm9wZW5fMjQ3X3dyYXBwZXInKS5zaG93KClcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgaWYgKCEkKCcjdXNlX211bHRpcGxlX2xvY2F0aW9ucycpLmlzKFwiOmNoZWNrZWRcIikpIHtcbiAgICAgICAgbWF5YmVDbG9zZU9wZW5pbmdIb3VycygnaW5wdXQjb3Blbl8yNDcnKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICAkKCcub3Blbl8yNDdfd3JhcHBlcicpLmhpZGUoKVxuICAgIH1cblxuICAgICQoJyN3cHNlb19vcGVuXzI0NycpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgbWF5YmVDbG9zZU9wZW5pbmdIb3Vycyh0aGlzKTtcbiAgICB9KTtcblxuICAgICQoJyNvcGVuaW5nLWhvdXJzLWhvdXJzIC5vcGVuaW5nLWhvdXJzJykubWFwKGZ1bmN0aW9uIChrZXksIHZhbCkge1xuICAgICAgICBtYXliZURpc2FibGVIb3VycygkKCcub3BlbmluZ2hvdXJzLXdyYXBwZXInLCB2YWwpLCB0cnVlKTtcbiAgICB9KTtcblxuICAgICQoJyN3cHNlby1sb2NhbC1tZXRhYm94IC5vcGVuaW5nLWhvdXJzJykubWFwKGZ1bmN0aW9uIChrZXksIHZhbCkge1xuICAgICAgICBtYXliZURpc2FibGVIb3VycygkKCcub3BlbmluZ2hvdXJzLXdyYXBwZXInLCB2YWwpLCB0cnVlKTtcbiAgICB9KTtcblxuICAgICQoJy53cHNlb19vcGVuXzI0aCBpbnB1dCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgbGV0IGVsZW0gPSAkKCcub3BlbmluZ2hvdXJzLXdyYXBwZXInLCAkKHRoaXMpLmNsb3Nlc3QoJy5vcGVuaW5nLWhvdXJzJykpO1xuICAgICAgICBtYXliZURpc2FibGVIb3VycyhlbGVtLCBmYWxzZSk7XG4gICAgfSk7XG5cbiAgICBmdW5jdGlvbiBtYXliZURpc2FibGVIb3VycyhlbGVtLCBpbml0aWFsKSB7XG4gICAgICAgIGlmICgkKGVsZW0pLmRhdGEoJ2Rpc2FibGVkJykgPT09IHRydWUpIHtcbiAgICAgICAgICAgICQoJ3NlbGVjdCcsIGVsZW0pLmF0dHIoJ2Rpc2FibGVkJywgZmFsc2UpO1xuICAgICAgICAgICAgZWxlbS5kYXRhKCdkaXNhYmxlZCcsIGZhbHNlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGlmIChpbml0aWFsID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgaWYgKCQoJy53cHNlb19vcGVuXzI0aCBpbnB1dCcsIGVsZW0uY29udGV4dCkuaXMoXCI6Y2hlY2tlZFwiKSkge1xuICAgICAgICAgICAgICAgICAgICAkKCdzZWxlY3QnLCBlbGVtKS5hdHRyKCdkaXNhYmxlZCcsIHRydWUpO1xuICAgICAgICAgICAgICAgICAgICBlbGVtLmRhdGEoJ2Rpc2FibGVkJywgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkKCdzZWxlY3QnLCBlbGVtKS5hdHRyKCdkaXNhYmxlZCcsIHRydWUpO1xuICAgICAgICAgICAgICAgIGVsZW0uZGF0YSgnZGlzYWJsZWQnLCB0cnVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIG1heWJlQ2xvc2VPcGVuaW5nSG91cnMoZWxlbSkge1xuICAgICAgICBpZiAoJChlbGVtKS5pcygnOmNoZWNrZWQnKSkge1xuICAgICAgICAgICAgJCgnI29wZW5pbmctaG91cnMtaG91cnMsIC5vcGVuaW5nLWhvdXJzJykuc2xpZGVVcCgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgJCgnI29wZW5pbmctaG91cnMtaG91cnMsIC5vcGVuaW5nLWhvdXJzJykuc2xpZGVEb3duKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAkKCcud2lkZ2V0LWNvbnRlbnQnKS5vbignY2xpY2snLCAnI3dwc2VvLWNoZWNrYm94LW11bHRpcGxlLWxvY2F0aW9ucy13cmFwcGVyIGlucHV0W3R5cGU9Y2hlY2tib3hdJywgZnVuY3Rpb24gKCkge1xuICAgICAgICB3cHNlb19zaG93X2FsbF9sb2NhdGlvbnNfc2VsZWN0Ym94KCQodGhpcykpO1xuICAgIH0pO1xuXG4gICAgLy8gU2hvdyBsb2NhdGlvbnMgbWV0YWJveCBiZWZvcmUgV1AgU0VPIG1ldGFib3hcbiAgICBpZiAoJCgnI3dwc2VvX2xvY2F0aW9ucycpLmxlbmd0aCA+IDAgJiYgJCgnI3dwc2VvX21ldGEnKS5sZW5ndGggPiAwKSB7XG4gICAgICAgICQoJyN3cHNlb19sb2NhdGlvbnMnKS5pbnNlcnRCZWZvcmUoJCgnI3dwc2VvX21ldGEnKSk7XG4gICAgfVxuXG4gICAgJCgnLm9wZW5pbmdob3Vyc19mcm9tJykuY2hhbmdlKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHRvX2lkID0gJCh0aGlzKS5hdHRyKCdpZCcpLnJlcGxhY2UoJ19mcm9tJywgJ190b193cmFwcGVyJyk7XG4gICAgICAgIHZhciBzZWNvbmRfaWQgPSAkKHRoaXMpLmF0dHIoJ2lkJykucmVwbGFjZSgnX2Zyb20nLCAnX3NlY29uZCcpO1xuXG4gICAgICAgIGlmICgkKHRoaXMpLnZhbCgpID09ICdjbG9zZWQnKSB7XG4gICAgICAgICAgICAkKCcjJyArIHRvX2lkKS5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpO1xuICAgICAgICAgICAgJCgnIycgKyBzZWNvbmRfaWQpLmNzcygnZGlzcGxheScsICdub25lJyk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAkKCcjJyArIHRvX2lkKS5jc3MoJ2Rpc3BsYXknLCAnaW5saW5lJyk7XG4gICAgICAgICAgICAkKCcjJyArIHNlY29uZF9pZCkuY3NzKCdkaXNwbGF5JywgJ2Jsb2NrJyk7XG4gICAgICAgIH1cbiAgICB9KS5jaGFuZ2UoKTtcbiAgICAkKCcub3BlbmluZ2hvdXJzX2Zyb21fc2Vjb25kJykuY2hhbmdlKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHRvX2lkID0gJCh0aGlzKS5hdHRyKCdpZCcpLnJlcGxhY2UoJ19mcm9tJywgJ190b193cmFwcGVyJyk7XG5cbiAgICAgICAgaWYgKCQodGhpcykudmFsKCkgPT0gJ2Nsb3NlZCcpIHtcbiAgICAgICAgICAgICQoJyMnICsgdG9faWQpLmNzcygnZGlzcGxheScsICdub25lJyk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAkKCcjJyArIHRvX2lkKS5jc3MoJ2Rpc3BsYXknLCAnaW5saW5lJyk7XG4gICAgICAgIH1cbiAgICB9KS5jaGFuZ2UoKTtcbiAgICAkKCcub3BlbmluZ2hvdXJzX3RvJykuY2hhbmdlKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGZyb21faWQgPSAkKHRoaXMpLmF0dHIoJ2lkJykucmVwbGFjZSgnX3RvJywgJ19mcm9tJyk7XG4gICAgICAgIHZhciB0b19pZCA9ICQodGhpcykuYXR0cignaWQnKS5yZXBsYWNlKCdfdG8nLCAnX3RvX3dyYXBwZXInKTtcbiAgICAgICAgaWYgKCQodGhpcykudmFsKCkgPT0gJ2Nsb3NlZCcpIHtcbiAgICAgICAgICAgICQoJyMnICsgdG9faWQpLmNzcygnZGlzcGxheScsICdub25lJyk7XG4gICAgICAgICAgICAkKCcjJyArIGZyb21faWQpLnZhbCgnY2xvc2VkJyk7XG4gICAgICAgIH1cbiAgICB9KTtcbiAgICAkKCcub3BlbmluZ2hvdXJzX3RvX3NlY29uZCcpLmNoYW5nZShmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBmcm9tX2lkID0gJCh0aGlzKS5hdHRyKCdpZCcpLnJlcGxhY2UoJ190bycsICdfZnJvbScpO1xuICAgICAgICB2YXIgdG9faWQgPSAkKHRoaXMpLmF0dHIoJ2lkJykucmVwbGFjZSgnX3RvJywgJ190b193cmFwcGVyJyk7XG4gICAgICAgIGlmICgkKHRoaXMpLnZhbCgpID09ICdjbG9zZWQnKSB7XG4gICAgICAgICAgICAkKCcjJyArIHRvX2lkKS5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpO1xuICAgICAgICAgICAgJCgnIycgKyBmcm9tX2lkKS52YWwoJ2Nsb3NlZCcpO1xuICAgICAgICB9XG4gICAgfSk7XG5cbiAgICBpZiAoJCgnLnNldF9jdXN0b21faW1hZ2VzJykubGVuZ3RoID4gMCkge1xuICAgICAgICBpZiAodHlwZW9mIHdwICE9PSAndW5kZWZpbmVkJyAmJiB3cC5tZWRpYSAmJiB3cC5tZWRpYS5lZGl0b3IpIHtcbiAgICAgICAgICAgICQoJy53cmFwJykub24oJ2NsaWNrJywgJy5zZXRfY3VzdG9tX2ltYWdlcycsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIHZhciBidXR0b24gPSAkKHRoaXMpO1xuICAgICAgICAgICAgICAgIHZhciBpZCA9IGJ1dHRvbi5hdHRyKCdkYXRhLWlkJyk7XG4gICAgICAgICAgICAgICAgd3AubWVkaWEuZWRpdG9yLnNlbmQuYXR0YWNobWVudCA9IGZ1bmN0aW9uIChwcm9wcywgYXR0YWNobWVudCkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoYXR0YWNobWVudC5oYXNPd25Qcm9wZXJ0eSgnc2l6ZXMnKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHVybCA9IGF0dGFjaG1lbnQuc2l6ZXNbcHJvcHMuc2l6ZV0udXJsO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHVybCA9IGF0dGFjaG1lbnQudXJsO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgJCgnIycgKyBpZCArICdfaW1hZ2VfY29udGFpbmVyJykuYXR0cignc3JjJywgdXJsKTtcbiAgICAgICAgICAgICAgICAgICAgJCgnLndwc2VvLWxvY2FsLScgKyBpZCArICctd3JhcHBlciAud3BzZW8tbG9jYWwtaGlkZS1idXR0b24nKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICQoJyNoaWRkZW5fJyArIGlkKS5hdHRyKCd2YWx1ZScsIGF0dGFjaG1lbnQuaWQpO1xuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgd3AubWVkaWEuZWRpdG9yLm9wZW4oYnV0dG9uKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgICQoJy5yZW1vdmVfY3VzdG9tX2ltYWdlJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgIHZhciBpZCA9ICQodGhpcykuYXR0cignZGF0YS1pZCcpO1xuICAgICAgICAkKCcjJyArIGlkKS5hdHRyKCdzcmMnLCAnJykuaGlkZSgpO1xuICAgICAgICAkKCcjaGlkZGVuXycgKyBpZCkuYXR0cigndmFsdWUnLCAnJyk7XG4gICAgICAgICQoJy53cHNlby1sb2NhbC0nICsgaWQgKyAnLXdyYXBwZXIgLndwc2VvLWxvY2FsLWhpZGUtYnV0dG9uJykuaGlkZSgpO1xuICAgIH0pO1xuXG4gICAgLy8gQ29weSBsb2NhdGlvbiBkYXRhXG4gICAgJCgnI3dwc2VvX2NvcHlfZnJvbV9sb2NhdGlvbicpLmNoYW5nZShmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBsb2NhdGlvbl9pZCA9ICQodGhpcykudmFsKCk7XG5cbiAgICAgICAgaWYgKGxvY2F0aW9uX2lkID09ICcnKVxuICAgICAgICAgICAgcmV0dXJuO1xuXG4gICAgICAgICQucG9zdCh3cHNlb19sb2NhbF9kYXRhLmFqYXh1cmwsIHtcbiAgICAgICAgICAgIGxvY2F0aW9uX2lkOiBsb2NhdGlvbl9pZCxcbiAgICAgICAgICAgIHNlY3VyaXR5OiB3cHNlb19sb2NhbF9kYXRhLnNlY19ub25jZSxcbiAgICAgICAgICAgIGFjdGlvbjogJ3dwc2VvX2NvcHlfbG9jYXRpb24nXG4gICAgICAgIH0sIGZ1bmN0aW9uIChyZXN1bHQpIHtcbiAgICAgICAgICAgIGlmIChyZXN1bHQuY2hhckF0KHJlc3VsdC5sZW5ndGggLSAxKSA9PSAwKSB7XG4gICAgICAgICAgICAgICAgcmVzdWx0ID0gcmVzdWx0LnNsaWNlKDAsIC0xKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYgKHJlc3VsdC5zdWJzdHJpbmcocmVzdWx0Lmxlbmd0aCAtIDIpID09IFwiLTFcIikge1xuICAgICAgICAgICAgICAgIHJlc3VsdCA9IHJlc3VsdC5zbGljZSgwLCAtMik7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHZhciBkYXRhID0gJC5wYXJzZUpTT04ocmVzdWx0KTtcbiAgICAgICAgICAgIGlmIChkYXRhLnN1Y2Nlc3MgPT0gJ3RydWUnIHx8IGRhdGEuc3VjY2VzcyA9PSB0cnVlKSB7XG5cbiAgICAgICAgICAgICAgICBmb3IgKHZhciBpIGluIGRhdGEubG9jYXRpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHZhbHVlID0gZGF0YS5sb2NhdGlvbltpXTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAodmFsdWUgIT0gbnVsbCAmJiB2YWx1ZSAhPSAnJyAmJiB0eXBlb2YgdmFsdWUgIT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpID09ICdpc19wb3N0YWxfYWRkcmVzcycgfHwgaSA9PSAnbXVsdGlwbGVfb3BlbmluZ19ob3VycycpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodmFsdWUgPT0gJzEnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyN3cHNlb18nICsgaSkuYXR0cignY2hlY2tlZCcsICdjaGVja2VkJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJy5vcGVuaW5nLWhvdXJzIC5vcGVuaW5nLWhvdXItc2Vjb25kJykuc2xpZGVEb3duKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSBpZiAoaS5pbmRleE9mKCdvcGVuaW5nX2hvdXJzJykgPiAtMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyMnICsgaSkudmFsKHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyN3cHNlb18nICsgaSkudmFsKHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfSk7XG59KTtcblxud2luZG93Lndwc2VvX3Nob3dfYWxsX2xvY2F0aW9uc19zZWxlY3Rib3ggPSBmdW5jdGlvbiAob2JqKSB7XG4gICAgJCA9IGpRdWVyeTtcblxuICAgICRvYmogPSAkKG9iaik7XG4gICAgdmFyIHBhcmVudCA9ICRvYmoucGFyZW50cygnLndpZGdldC1pbnNpZGUnKTtcbiAgICB2YXIgJGxvY2F0aW9uc1dyYXBwZXIgPSAkKCcjd3BzZW8tbG9jYXRpb25zLXdyYXBwZXInLCBwYXJlbnQpO1xuXG4gICAgaWYgKCRvYmouaXMoJzpjaGVja2VkJykpIHtcbiAgICAgICAgJGxvY2F0aW9uc1dyYXBwZXIuc2xpZGVVcCgpO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgJGxvY2F0aW9uc1dyYXBwZXIuc2xpZGVEb3duKCk7XG4gICAgfVxufVxuIl19
