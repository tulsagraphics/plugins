<?php
/**
 * @package WPSEO_Local\Tests
 */

/**
 * Global functions that need to be mocked.
 *
 * @param string $string The string to be escaped.
 *
 * @return mixed
 */
function esc_html( $string ) {
	return $string;
}
