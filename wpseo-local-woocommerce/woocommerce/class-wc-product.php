<?php
/**
 * Yoast SEO: Local for WooCommerce plugin file.
 *
 * @package    YoastSEO_Local_WooCommerce
 * @deprecated 8.2.0
 */

_deprecated_file( __FILE__, 'Yoast SEO: Local for WooCommerce 8.2.0', 'plugin-folder/woocommerce/product/class-product.php' );

/**
 * @deprecated 8.3.0 Duplicate class which wasn't used.
 *                   Use `Yoast_Product_WPSEO_Local` instead.
 */
class Yoast_WCSEO_Local_Product extends Yoast_Product_WPSEO_Local {}
