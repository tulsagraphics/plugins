<?php
/* 
Plugin Name: Media Grid
Plugin URI: http://www.lcweb.it/media-grid
Description: The revolutionary engine to create layout-free grids. Supporting any multimedia type and bringing an unique lightbox!
Author: Luca Montanari
Version: 6.301
Author URI: https://lcweb.it
*/  


/////////////////////////////////////////////
/////// MAIN DEFINES ////////////////////////
/////////////////////////////////////////////

// plugin path
$wp_plugin_dir = substr(plugin_dir_path(__FILE__), 0, -1);
define('MG_DIR', $wp_plugin_dir);

// plugin url
$wp_plugin_url = substr(plugin_dir_url(__FILE__), 0, -1);
define('MG_URL', $wp_plugin_url);

// plugin version
define('MG_VER', 6.301);


// timthumb url - also for MU
if(is_multisite()){ define('MG_TT_URL', MG_URL . '/classes/timthumb_MU.php'); }
else { define( 'MG_TT_URL', MG_URL . '/classes/timthumb.php'); }




/////////////////////////////////////////////
/////// FORCING DEBUG ///////////////////////
/////////////////////////////////////////////

if(isset($_REQUEST['mg_php_debug'])) {
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);	
}




/////////////////////////////////////////////
/////// MULTILANGUAGE SUPPORT ///////////////
/////////////////////////////////////////////

function mg_multilanguage() {
	$param_array = explode(DIRECTORY_SEPARATOR, MG_DIR);
 	$folder_name = end($param_array);
	load_plugin_textdomain( 'mg_ml', false, $folder_name . '/languages'); 
}
add_action('init', 'mg_multilanguage', 1);




/////////////////////////////////////////////
/////// MAIN SCRIPT & CSS INCLUDES //////////
/////////////////////////////////////////////


// global script enqueuing
function mg_global_scripts() {
	wp_enqueue_script('jquery');
	wp_enqueue_style('mg-fontawesome', MG_URL . '/css/font-awesome/css/font-awesome.min.css', 999, '4.7.0');


	// admin
	if (is_admin()) {  
		wp_enqueue_style('mg_admin', MG_URL . '/css/admin.css', 999, MG_VER);
		wp_enqueue_style('mg_settings', MG_URL . '/settings/settings_style.css', 999, MG_VER);	
		
		// chosen
		wp_enqueue_style( 'lcwp-chosen-style', MG_URL.'/js/chosen/chosen.css', 999);
		
		// lcweb switch
		wp_enqueue_style( 'lc-switch', MG_URL.'/js/lc-switch/lc_switch.css', 999);
		
		// colorpicker
		wp_enqueue_style( 'mg-colpick', MG_URL.'/js/colpick/css/colpick.css', 999);
		
		// LCWP jQuery ui
		wp_enqueue_style( 'lcwp-ui-theme', MG_URL.'/css/ui-wp-theme/jquery-ui-1.8.17.custom.css', 999);
		
		// sortable
		wp_enqueue_script('jquery-ui-sortable');
		
		// slider
		wp_enqueue_script('jquery-ui-slider');
		
		// lightbox and thickbox
		wp_enqueue_style('thickbox');
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
	}
	
	
	// frontend
	if (!is_admin()) {
		// frontent JS on header or footer
		if(get_option('mg_js_head') != '1') {
			wp_enqueue_script('mg-frontend-js', MG_URL.'/js/frontend.js', 100, MG_VER, true);		
		} else { 
			wp_enqueue_script('mg-frontend-js', MG_URL.'/js/frontend.js', 900, MG_VER);
		}

		// frontend css
		if(!get_option('mg_inline_css') && !get_option('mg_force_inline_css')) {
			wp_enqueue_style('mg-custom-css', MG_URL. '/css/custom.css', 100, MG_VER);	
		}
		else {add_action('wp_head', 'mg_inline_css', 989);}	
	}
}
add_action('wp_enqueue_scripts', 'mg_global_scripts', 900);
add_action('admin_enqueue_scripts', 'mg_global_scripts');


// use frontend CSS inline
function mg_inline_css(){
	echo '<style type="text/css">';
	include_once(MG_DIR.'/frontend_css.php');
	echo '</style>';
}





/////////////////////////////////////////////
/////// MAIN INCLUDES ///////////////////////
/////////////////////////////////////////////

// admin menu and cpt and taxonomy
include_once(MG_DIR . '/admin_menu.php');

// taxonomy options 
include_once(MG_DIR . '/taxonomy_options.php');

// mg items metaboxes
include_once(MG_DIR . '/metaboxes.php');

// post types metabox
include_once(MG_DIR . '/post_types_metabox.php');

// shortcode
include_once(MG_DIR . '/shortcode.php');

// tinymce button
include_once(MG_DIR . '/tinymce_implementation.php');

// ajax
include_once(MG_DIR . '/ajax.php');

// dynamic javascript and CSS for footer
include_once(MG_DIR . '/dynamic_footer.php');

// grid preview
include_once(MG_DIR . '/grid_preview.php');

// lightbox
include_once(MG_DIR . '/lightbox.php');

// lghtbox comments
include_once(MG_DIR . '/classes/lb_comments.php');



// retrieve deeplinks
include_once(MG_DIR . '/deeplinks_retrieval.php');

// lightbox deeplink launch
include_once(MG_DIR . '/lightbox_deeplink.php');



// visual composer integration
include_once(MG_DIR . '/builders_integration/visual_composer.php');

// cornerstone integration
include_once(MG_DIR . '/builders_integration/cornerstone.php');

// elementor integration
include_once(MG_DIR . '/builders_integration/elementor.php');






////////////
// AVOID issues with bad servers in settings redirect
function mg_settings_redirect_trick() {
	ob_start();
}
add_action('admin_init', 'mg_settings_redirect_trick', 1);
////////////



////////////
// EASY WP THUMBS + forcing system
function mg_ewpt() {
	if(get_option('mg_ewpt_force')) {$_REQUEST['ewpt_force'] = true;}
	include_once(MG_DIR . '/classes/easy_wp_thumbs.php');	
}
add_action('init', 'mg_ewpt', 1);
////////////



////////////
// actions performed on plugin's activation
include_once(MG_DIR . '/on_activation.php');
register_activation_hook(__FILE__, 'mg_on_activation');
////////////




////////////
// AUTO UPDATE DELIVER
if(!isset($GLOBALS['is_mg_bundle'])) {
	include_once(MG_DIR . '/classes/lc_plugin_auto_updater.php');
	function mg_auto_updates() {
		$upd = new lc_wp_autoupdate(__FILE__, 'http://updates.lcweb.it', 'lc_updates', 'mg_init_custom_css');
	}
	add_action('admin_init', 'mg_auto_updates', 1);
}
////////////




//////////////////////////////////////////////////
// REMOVE WP HELPER FROM PLUGIN PAGES

function mg_remove_wp_helper() {
	$cs = get_current_screen();
	$hooked = array('mg_items_page_mg_settings', 'mg_items_page_mg_builder');
	
	if(is_object($cs) && in_array($cs->base, $hooked)) {
		echo '
		<style type="text/css">
		#screen-meta-links {display: none;}
		</style>';	
	}
	
	//var_dump(get_current_screen()); // debug
}
add_action('admin_head', 'mg_remove_wp_helper', 999);
