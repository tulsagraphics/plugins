<?php 
include_once(MG_DIR . '/functions.php');
$banners_baseurl = MG_URL .'/img/addons/'; 

$addons = mg_addons_db();
$missing = mg_addons_not_installed();
?>


<style type="text/css">
#mgaa_wrap,
#mgaa_wrap *,
#mgaa_wrap *:before,
#mgaa_wrap *:after {
	box-sizing: border-box;
	
	-webkit-transition: all .3s ease;
	transition: 		all .3s ease;
}
#mgaa_wrap {
	background-color: #555;
	border-radius: 3px;
	margin: 30px;
	padding: 30px;	
	text-align: center;	
}

#mgaa_h1 {
	background: #f1f1f1;
	border-radius: 0 0 5px 5px;
	color: #5d5d5d;
	display: inline-block;
	padding: 10px 10% 40px;
	text-align: center;
	font-size: 25px;
	line-height: normal;
	margin: -30px 30px 50px;
}
#mgaa_h1 img {
	display: block;
	margin: 0 auto 18px;
}
#mgaa_h1 small {
	display: block;
	font-size: 68%;
	margin-top: 15px;
	font-weight: normal;
	text-shadow: 0 0 0 #bbb;	
	border-bottom: 1px solid #7fc241;
	padding-bottom: 6px;
}


#mgaa_bundle_wrap {
	background: #f1f1f1;
	border-radius: 5px 5px 0 0;
	color: #5d5d5d;	
	display: inline-block;
	padding: 20px 10% 30px;
	text-align: center;
	font-size: 25px;
	margin: 50px 30px -30px;
}
#mgaa_h2 {
	border-radius: 2px;
	text-align: center;
	font-size: 20px;
	margin: 30px 30px 20px;
	line-height: normal;	
}
#mgaa_h2 small {
	display: block;
	font-size: 82%;
	margin-top: 8px;
	font-weight: normal;
	text-shadow: 0 0 0 #bbb;
	padding-bottom: 6px;
}

#mgaa_banners_wrap {
	text-align: center;	
}
#mgaa_banners_wrap a {
	display: inline-block;
	width: calc(50% - 50px);
	max-width: 590px;
	margin: 25px;
	border-radius: 3px;
	line-height: 0;	
	overflow: hidden;
	position: relative;
}
#mgaa_banners_wrap a.mgaa_owned {
	cursor: default;	
}
#mgaa_banners_wrap a,
#mgaa_banners_wrap a:focus {
	box-shadow: 0 0 13px rgba(0,0,0,0.5);
}
#mgaa_banners_wrap img {
	width: 100%;	
}


#mgaa_banners_wrap span {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: rgba(0,0,0,0.4);
	color: #fff;
	text-shadow: 0 0 2px rgba(0,0,0,0.7);	
	text-align: center;
}
#mgaa_banners_wrap span::before {
    content: "";
	width: 190px;
	height: 190px;
	background: rgba(30,30,30,0.8);
	display: block;
	border-radius: 100%;
	position: absolute;
	top: calc(50% - 95px);
	left: calc(50% - 95px);
	box-shadow: 0 0 0 4px #fff;
}
#mgaa_banners_wrap a:not(.mgaa_owned) span {
	top: 20%;
	opacity: 0;
	-webkit-transform: scale(0.7);	
	transform: scale(0.7);	
	
	-webkit-transition-duration: 	.2s;
	transition-duration: 			.2s;
}
#mgaa_banners_wrap a:not(.mgaa_owned):hover span {
	top: 0;
	opacity: 1;
	-webkit-transform: none;	
	transform: none;	
}

#mgaa_banners_wrap span * {
	display: block;
	width: 100%;
	line-height: 26px;
	position: relative;
	z-index: 100;
}
#mgaa_banners_wrap span strong {
	font-size: 31px;
	top: calc(50% - 40px);
}
#mgaa_banners_wrap span i {
	font-size: 43px;
	top: calc(50% - 13px);	
}

#mgaa_bundle_btn {
	display: inline-block;
	background: #7fc241 url("<?php echo MG_URL ?>/img/lc_pattern.png") repeat scroll top left;
	text-decoration: none;
	font-size: 25px;
	color: #fff;	
	line-height: 40px;
	padding: 12px 40px;
	border-radius: 4px;
	margin-top: 10px;
}
#mgaa_bundle_btn,
#mgaa_bundle_btn:focus {
	box-shadow: none;
	border-bottom: 4px solid #6c9e3d;	
}
#mgaa_bundle_btn:hover {
	-webkit-transform: scale(1.02);
	transform: scale(1.02);
}
#mgaa_bundle_btn img {
	width: 30px;
	position: relative;
	bottom: -4px;
	display: inline-block;
	margin-right: 15px;
}


@media screen and (max-width:1584px) {
	#mgaa_banners_wrap a {
		display: block;
		width: 100%;
		margin: 0 auto 45px;
	}	
	#mgaa_banners_wrap a:last-child {
		margin-bottom: 0;	
	}
}

@media screen and (max-width:600px) {
	#mgaa_h1,
	#mgaa_bundle_wrap {
		margin-left: 0;
		margin-right: 0;
	}
	#mgaa_banners_wrap span *,
	#mgaa_banners_wrap span:before {
		-webkit-transform: scale(0.7);
		transform: scale(0.7);	
	}
}
@media screen and (max-width:490px) {
	#mgaa_banners_wrap span *,
	#mgaa_banners_wrap span:before {
		-webkit-transform: scale(0.6);
		transform: scale(0.6);	
	}
}
</style>




<div id="mgaa_wrap">
    <h1 id="mgaa_h1">
    	<img src="<?php echo $banners_baseurl ?>mg_logo.png" alt="media grid logo" />
    	Media Grid plugin is just the beginning
        <small>Add-ons push its capabilities to the maximum, offering incredible possibilities!</small>
	</h1>
    
    
    <div id="mgaa_banners_wrap">
        <?php 
        foreach($addons as $id => $data) {
            $owned_class 	= (in_array($id, $missing)) ? '' : 'mgaa_owned';
            $link 			= ($owned_class) ? 'javascript:void(0)' : $data['link'] .'?ref=LCweb&mgaa=true';
			$target 		= ($owned_class) ? '' : 'target="_blank"'; 
			 
			$txt = ($owned_class) ? '<strong>Installed!</strong><i class="dashicons dashicons-thumbs-up"></i>' : '<strong>Check it!</strong> <i class="dashicons dashicons-cart"></i>'; 
			 
            echo '
            <a href="'. $link .'" '. $target .' class="'. $owned_class .'" title="'. addslashes($data['descr']) .'">'.
                '<img src="'. $banners_baseurl . $id .'.png" alt="'. $data['name'] .'" />'.
				'<span>'. $txt .'</span>'.
            '</a>';
        }
        ?>
    </div>
    
    
    <div id="mgaa_bundle_wrap">
        <h2 id="mgaa_h2">Need everything? Get the bundle and save now up to 17%!
        <small>Future add-ons will be included for free!</small></h2>
        

        <a id="mgaa_bundle_btn" href="https://codecanyon.net/item/media-grid-wordpress-bundle-pack/21876571?ref=LCweb&license=regular&open_purchase_for_item_id=21876571&lc_outbound=true&mgaa=true" target="_blank">
            <img src="<?php echo $banners_baseurl ?>bundle_logo.png" /> Get the bundle!
        </a>
	</div>
</div>