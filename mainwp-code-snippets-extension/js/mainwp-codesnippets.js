/*
 * MainWp Code Snippets Extension
 */
jQuery( document ).ready(function($) {
	var atts = {
		lineNumbers: true,
		matchBrackets: true,
		styleActiveLine: true,
		theme: "erlang-dark",
		lineWrapping: true,
		mode: "application/x-httpd-php",
		indentUnit: 4,
		indentWithTabs: true,
		enterMode: "keep",
		tabMode: "shift"
	};
	var mainwp_snp_editor = null;
	if (jQuery( '#snp_snippet_code' ).length > 0) {
		 mainwp_snp_editor = CodeMirror.fromTextArea( document.getElementById( "snp_snippet_code" ), atts );
	}

		$( '#maiwnp_snippet_btn_run' ).live('click', function(event){
				var cf = confirm( "Are you sure you want to run this code?" );
			if (cf == false) {
				return;
			}
				jQuery( '#mainwp_code_snippet_result_output' ).html( '' );

				$( '#selected_sites' ).removeClass( 'form-invalid' );
				$( '#selected_groups' ).removeClass( 'form-invalid' );
				$( '#snp_snippet_title' ).removeClass( 'form-invalid' );

				var errors = [];
			if ($.trim( $( '#snp_snippet_title' ).val() ) == '') {
				errors.push( __( 'Snippet title is required!' ) );
				$( '#snp_snippet_title' ).addClass( 'form-invalid' );
			}

				var code = mainwp_snp_editor.getValue();
				code = code.replace( '|^[\s]*<\?(php)?|', '' );
				code = code.replace( '|\?>[\s]*$|', '' );

			if (code == '') {
				errors.push( __( 'Snippet content is required!' ) );
			}

			if (errors.length > 0) {
				jQuery( '#snippet-error-box' ).html( errors.join( '<br />' ) );
				jQuery( '#snippet-error-box' ).show();
				return false;
			} else {
				jQuery( '#snippet-error-box' ).html( "" );
				jQuery( '#snippet-error-box' ).hide();
			}

				jQuery( '#maiwnp_snippet_running_box' ).show();
				var snippet_id = jQuery( '#mainwp_snippet_id_value' ).val();
				var current_type = jQuery( '#mainwp_snippet_type_value' ).val();
				var selected_type = jQuery( 'input[name="snp_snippet_type"]:checked' ).val();

			if (snippet_id > 0 && (current_type === 'S' || selected_type === 'S' || current_type === 'C' || selected_type === 'C')) {
					var data = {
						action:'mainwp_snippet_clear_on_site_loading',
						snippetId: snippet_id
				};
					jQuery( this ).attr( 'disabled','disabled' );
					//jQuery( '#snippet_submit_loading' ).find( '.status' ).html( '' );
					//jQuery( '#snippet_submit_loading' ).show();
					jQuery.post(ajaxurl, data, function (response) {
							jQuery( '#maiwnp_snippet_btn_run' ).removeAttr( 'disabled' );
							//jQuery( '#snippet_submit_loading' ).hide();
						if (current_type === 'S' || current_type === 'C') {
							var title = __( 'Searching for the snippet' );
							if (response !== 'NOSITES') {
								jQuery( '#mainwp_code_snippet_result_output' ).html( response );
								jQuery( '#mainwp_code_snippet_result_output_title' ).html( title );
								mainwp_snippet_clear_start();
							} else {
								jQuery( '#mainwp_code_snippet_result_output' ).append( '<div class="mainwp-notice mainwp-notice-yellow">' + __( 'No selected sites. Please select your child sites firts.' ) + '</div>' );
								mainwp_snippet_save( true, false ); // avoid clear on sites
							}
						} else { // selected_type = S
							mainwp_snippet_save( true, false );
						}
					});

			} else {
				if (selected_type === 'S' || selected_type === 'C') {
					mainwp_snippet_save( true, false ); // update on sites
				} else {
					mainwp_snippet_save( false, false ); // do not update on sites
				}
			}
		})

	$( '#maiwnp_snippet_btn_save' ).live('click', function(event){
		jQuery( '#maiwnp_snippet_running_box' ).hide();
		$( '#snp_snippet_title' ).removeClass( 'form-invalid' );
		var errors = [];
		if ($.trim( $( '#snp_snippet_title' ).val() ) == '') {
			errors.push( __( 'Snippet title is required!' ) );
			$( '#snp_snippet_title' ).addClass( 'form-invalid' );
		}

		var code = mainwp_snp_editor.getValue();
		code = code.replace( '|^[\s]*<\?(php)?|', '' );
		code = code.replace( '|\?>[\s]*$|', '' );

		if (code == '') {
			errors.push( __( 'Snippet content is required!' ) );
		}

		if (errors.length > 0) {
			jQuery( '#snippet-error-box' ).html( errors.join( '<br />' ) );
			jQuery( '#snippet-error-box' ).show();
			return false;
		} else {
			jQuery( '#snippet-error-box' ).html( "" );
			jQuery( '#snippet-error-box' ).hide();
		}

		mainwp_snippet_save( false, true ); // do not update on sites

	});

	$( '.snippet_list_delete_item' ).live('click', function(){
		var type = $( this ).attr( 'type' );
		if (type === "S" || type === "C") {
			mainwp_snippet_delete_popup();
			$( 'input[name="delete_snippetid"]' ).val( $( this ).attr( 'id' ) );
		} else if (type === 'R') {
			mainwp_snippet_delete( $( this ) );
		}
		return false;
	})

	mainwp_snippet_delete = function(pItem) {
		var data = {
			action:'mainwp_snippet_delete_snippet',
			snippet_id: pItem.attr( 'id' )
		}
		var parent = pItem.closest( 'tr' );
		parent.find( '.snp-row-ajax-info' ).html( '<i class="fa fa-spinner fa-pulse fa-fw"></i> Deleting...' ).show();
		$.post(ajaxurl, data, function(response){
			if (response && response === 'SUCCESS') {
				parent.html( '<td colspan="4">The Code Snippet has been deleted successfully.</td>' );
			} else {
				parent.find( '.snp-row-ajax-info' ).html( 'Snippet could not be deleted.' );
				parent.find( '.snp-row-ajax-info' ).css( 'color','#a00' );
			}
		})
		return false;
	}

	$( '#snippet-btn-delete' ).live('click', function(){
		var snippetid = $( 'input[name="delete_snippetid"]' ).val();
		delete_on_site = $( 'input[name="delete_snippet_child_site"]:radio:checked' ).val();
		if ( delete_on_site == 1 ) {
			location.href = 'admin.php?page=Extensions-Mainwp-Code-Snippets-Extension&deleteonsites=1&id=' + snippetid;
			return;
		}

		var data = {
			action:'mainwp_snippet_delete_snippet',
			snippet_id: snippetid
		}

		$( '#snp_delete_running' ).show();
		$.post(ajaxurl, data, function(response){
			$( '#snp_delete_running' ).hide();
			if (response && response === 'SUCCESS') {
				$( '#mainwp-snippet-delete-result' ).html( '<div class="mainwp-notice mainwp-notice-yellow">Snippet has been deleted successfully.</div>' ); }

			setTimeout(function() {
				location.href = 'admin.php?page=Extensions-Mainwp-Code-Snippets-Extension';
			}, 3000);
		})
		return false;
	})

	 mainwp_snippet_run = function() {
		var errors = [];
		var selected_sites = [];
		var selected_groups = [];

		if (jQuery( '#select_by' ).val() == 'site') {
			jQuery( "input[name='selected_sites[]']:checked" ).each(function (i) {
				selected_sites.push( jQuery( this ).val() );
			});
		} else {
			jQuery( "input[name='selected_groups[]']:checked" ).each(function (i) {
				selected_groups.push( jQuery( this ).val() );
			});
		}

		if (selected_groups.length == 0 && selected_sites.length == 0) {
			jQuery( '#mainwp_code_snippet_result_output_log' ).append( '<div class="mainwp-notice mainwp-notice-yellow">' + __( "Please select wanted child sites first!" ) + '</div>' );
			return;
		}

		var code = mainwp_snp_editor.getValue();
		code = code.replace( '|^[\s]*<\?(php)?|', '' );
		code = code.replace( '|\?>[\s]*$|', '' );

		if (code == '') {
			errors.push( __( 'Snippet content is required!' ) );
		}

		if (errors.length > 0) {
			jQuery( '#snippet-error-box' ).html( errors.join( '<br />' ) );
			jQuery( '#snippet-error-box' ).show();
			return false;
		} else {
			jQuery( '#snippet-error-box' ).html( "" );
			jQuery( '#snippet-error-box' ).hide();
		}

		var data = {
			action:'mainwp_snippet_run_snippet_loading',
			'groups[]':selected_groups,
			'sites[]':selected_sites
			};

			//jQuery( '#snippet_submit_loading' ).find( '.status' ).html( '<i class="fa fa-spinner fa-pulse fa-fw"></i> Executing the code snippet...' );
			//jQuery( '#snippet_submit_loading' ).show();
			jQuery.post(ajaxurl, data, function (response) {
				   //jQuery( '#snippet_submit_loading' ).hide();
				   var title = __( 'Executing the snippet on selected site(s)' );
				   jQuery( '#mainwp_code_snippet_result_output' ).html( response );
				   jQuery( '#mainwp_code_snippet_result_output_title' ).html( title );
				   jQuery( '#maiwnp_snippet_running_box' ).show();
				   mainwp_snippet_run_start();
			});
	 }

	mainwp_snippet_run_start = function() {
		mainwp_snippet_init_start();
		mainwp_snippet_run_start_next();
	}

	mainwp_snippet_clear_start = function() {
		mainwp_snippet_init_start();
		mainwp_snippet_clear_sites_start_next();
	}

	mainwp_snippet_update_start = function() {
		mainwp_snippet_init_start();
		mainwp_snippet_update_sites_start_next();
	}

	mainwp_snippet_save = function(doUpdate, saveOnly) {
		var selected_sites = [];
		var selected_groups = [];
		if (jQuery( '#select_by' ).val() == 'site') {
			jQuery( "input[name='selected_sites[]']:checked" ).each(function (i) {
				selected_sites.push( jQuery( this ).val() );
			});
		} else {
			jQuery( "input[name='selected_groups[]']:checked" ).each(function (i) {
				selected_groups.push( jQuery( this ).val() );
			});
		}
		var selected_type = jQuery( 'input[name="snp_snippet_type"]:checked' ).val();
		var snippet_id = jQuery( '#mainwp_snippet_id_value' ).val();
		var data = {
			action:'mainwp_snippet_save_snippet',
			snippet_title: jQuery( '#snp_snippet_title' ).val(),
			code: mainwp_snp_editor.getValue(),
			desc: jQuery( '#snp_snippet_desc' ).val(),
			type: selected_type,
			snippet_id: snippet_id,
			sites: selected_sites,
			groups: selected_groups,
			select_by: jQuery( '#select_by' ).val()
		};

		if (doUpdate) {
			jQuery( '#maiwnp_snippet_running_box' ).show(); }
			var title = __( 'Saving' );
		jQuery( '#mainwp_code_snippet_result_output_title' ).html( title );
		//jQuery( '#snippet_submit_loading' ).find( '.status' ).html( 'Saving the code snippet' );
		//jQuery( '#snippet_submit_loading' ).show();
		jQuery.post(ajaxurl, data, function (response) {
			//jQuery( '#snippet_submit_loading' ).hide();

			if (saveOnly) {
				var id_param = '';
				if (response['id']) {
					id_param = '&id=' + response['id']; } else if (snippet_id) {
					id_param = '&id=' + snippet_id; }

					if (response && response['status'] == 'SUCCESS') {
						location.href = 'admin.php?page=Extensions-Mainwp-Code-Snippets-Extension&message=1' + id_param;
					} else {
						location.href = 'admin.php?page=Extensions-Mainwp-Code-Snippets-Extension&message=-1' + id_param; }
					return;

			} else if (selected_type == 'R') {
				mainwp_snippet_run();
				return;
			}

			if (response && response['status'] == 'SUCCESS') {
				jQuery( '#mainwp_snippet_id_value' ).val( response['id'] );
				jQuery( '#mainwp_snippet_slug_value' ).val( response['slug'] );
				jQuery( '#mainwp_snippet_type_value' ).val( response['type'] );
				//jQuery( '#snippet_submit_loading' ).after( '<span id="mainwp_snippet_save_status">Saved</span>' );
				jQuery( '#mainwp_snippet_save_status' ).fadeOut( 3000 );
				jQuery( '#mainwp_code_snippet_result_output_log' ).append( '<div><i class="fa fa-check" aria-hidden="true"></i> ' + __( "Saved successfully!" ) + '</div>');
				if (doUpdate && (selected_type === 'S' || selected_type === 'C')) {
					var data = {
						action:'mainwp_snippet_update_site_loading',
						snippetId: response['id']
					};
					jQuery.post(ajaxurl, data, function (response) {
						var title = __( 'Saving the snippet on selected site(s)' );
						if (response !== 'NOSITES') {
							jQuery( '#mainwp_code_snippet_result_output' ).html( response );
							jQuery( '#mainwp_code_snippet_result_output_title' ).html( title );
							mainwp_snippet_update_start();
						} else {
							jQuery( '#mainwp_code_snippet_result_output_title' ).html( __( 'Process completed.' ) );
							jQuery( '#mainwp_code_snippet_result_output_log' ).append( '<div><i class="fa fa-check" aria-hidden="true"></i> ' + __( "No selected sites to proceed." ) + '</div>' );
						}

					});

				} else {
					jQuery( '#mainwp_code_snippet_result_output_log' ).append( '<div><i class="fa fa-check" aria-hidden="true"></i> ' + __( "Snippet saved successfully!" ) + '</div>' );
					mainwp_snippet_process_done();
				}
			} else {
				jQuery( '#mainwp_code_snippet_result_output_log' ).append( '<div><i class="fa fa-times" aria-hidden="true"></i> ' + __( "Saving process failed!" ) + '</div>' );
			}
		},'json');
	}

	mainwp_snippet_run_start_next = function() {
		if (snippet_TotalThreads == 0) {
			snippet_TotalThreads = jQuery( '.mainwpSnippetItem[status="queue"]' ).length; }
		while ((siteToRun = jQuery( '.mainwpSnippetItem[status="queue"]:first' )) && (siteToRun.length > 0)  && (snippet_CurrentThreads < snippet_MaxThreads)) {
			mainwp_snippet_run_start_specific( siteToRun );
		}
	}

	mainwp_snippet_run_start_specific = function (pSiteToRun)
	{
		snippet_CurrentThreads++;
		pSiteToRun.attr( 'status', 'progress' );
		var statusEl = pSiteToRun.find( '.status' ).html( '<i class="fa fa-spinner fa-fw fa-pulse" aria-hidden="true"></i> ' + 'Executing...' );
		var resultEl = pSiteToRun.find( '.run_result' );
			var data = {
				action:'mainwp_snippet_run_snippet',
				siteId: pSiteToRun.attr( 'siteid' ),
				code: mainwp_snp_editor.getValue()
		};

		jQuery.post(ajaxurl, data, function (response) {
				pSiteToRun.attr( 'status', 'done' );
			if ( ! response || response === 'FAIL') {
				statusEl.html( __( 'Undefined error! Please try again.' ) ).show();
				statusEl.css( 'color', '#a00' );
			} else if (response === 'CODEEMPTY') {
				statusEl.html( __( 'Snippet content is empty!' ) ).show();
				statusEl.css( 'color', '#a00' );
			} else {
				if ( response['error']) {
					statusEl.html( response['error'] ).show();
					statusEl.css( 'color', '#a00' );
				} else if (response['status'] === 'SUCCESS') {
					statusEl.html( 'Done!' ).show();
				} else if (response['status'] === 'FAIL') {
					statusEl.html( 'Process failed! Please try again.' ).show();
					statusEl.css( 'color', '#a00' );
				}
				if ( response['result'] !== '' ) {
					resultEl.html( response['result'] ).show();
				}
			}

				snippet_CurrentThreads--;
				snippet_FinishedThreads++;
			if (snippet_FinishedThreads == snippet_TotalThreads && snippet_FinishedThreads != 0) {
				jQuery( '#mainwp_code_snippet_result_output_log' ).append( '<div><i class="fa fa-check" aria-hidden="true"></i> ' + __( "Process completed successfully!" ) + '</div>' );
				jQuery( '#mainwp_code_snippet_result_output_title' ).html( 'Process completed.' );
			}
				mainwp_snippet_run_start_next();
		}, 'json');
	};

	mainwp_snippet_update_sites_start_next = function() {
		if (snippet_TotalThreads == 0) {
			snippet_TotalThreads = jQuery( '.mainwpSnippetUpdateSitesItem[status="queue"]' ).length; }

		while ((siteToRun = jQuery( '.mainwpSnippetUpdateSitesItem[status="queue"]:first' )) && (siteToRun.length > 0)  && (snippet_CurrentThreads < snippet_MaxThreads)) {
			mainwp_snippet_update_sites_start_specific( siteToRun );
		}
	}

	mainwp_snippet_update_sites_start_specific = function(pSiteToRun) {
		snippet_CurrentThreads++;
		pSiteToRun.attr( 'status', 'progress' );
		var statusEl = pSiteToRun.find( '.status' ).html( '<i class="fa fa-spinner fa-fw fa-pulse" aria-hidden="true"></i> ' + 'Executing...' );
		var type = jQuery( '#mainwp_snippet_type_value' ).val();
		var data = {
			action:'mainwp_snippet_update_site',
			siteId: pSiteToRun.attr( 'siteid' ),
			code: mainwp_snp_editor.getValue(),
			snippetSlug: jQuery( '#mainwp_snippet_slug_value' ).val(),
			type: type
		};

		jQuery.post(ajaxurl, data, function (response)
			{
			pSiteToRun.attr( 'status', 'done' );
			if ( ! response || response === 'FAIL') {
				statusEl.html( __( 'Undefined error! Please try again.' ) ).show();
				statusEl.css( 'color', '#a00' );
			} else if (response === 'CODEEMPTY') {
				statusEl.html( __( 'Snippet content is empty.' ) ).show();
				statusEl.css( 'color', '#a00' );
			} else {
				if ( response['error']) {
					statusEl.html( response['error'] ).show();
					statusEl.css( 'color', '#a00' );
				} else if (response['status'] === 'SUCCESS') {
					statusEl.html( 'Done!' ).show();
				} else if (response['status'] === 'FAIL') {
					statusEl.html( 'No changes.' ).show();
					statusEl.css( 'color', '#a00' );
				}
			}
			snippet_CurrentThreads--;
			snippet_FinishedThreads++;
			if (snippet_FinishedThreads == snippet_TotalThreads && snippet_FinishedThreads != 0) {
				jQuery( '#mainwp_code_snippet_result_output_log' ).append( '<div><i class="fa fa-check" aria-hidden="true"></i> ' + __( "Snipped saved successfully!" ) + '</div>' );
				if (type !== 'C') { // do not run if snippet code go to wp-config file
					mainwp_snippet_run();
				}
			}
			mainwp_snippet_update_sites_start_next();
		}, 'json');
	}

});

	var snippet_MaxThreads = 3;
	var snippet_CurrentThreads = 0;
	var snippet_TotalThreads = 0;
	var snippet_FinishedThreads = 0;

	mainwp_snippet_init_start = function() {
		snippet_MaxThreads = 3;
		snippet_CurrentThreads = 0;
		snippet_TotalThreads = 0;
		snippet_FinishedThreads = 0;
	}

	mainwp_snippet_process_done = function() {
		setTimeout(function() {
			location.href = 'admin.php?page=Extensions-Mainwp-Code-Snippets-Extension&message=1';
		}, 3000);
	}

	mainwp_snippet_clear_sites_start_next = function() {
		if (snippet_TotalThreads == 0) {
			snippet_TotalThreads = jQuery( '.mainwpSnippetClearSitesItem[status="queue"]' ).length; }
		while ((siteToRun = jQuery( '.mainwpSnippetClearSitesItem[status="queue"]:first' )) && (siteToRun.length > 0)  && (snippet_CurrentThreads < snippet_MaxThreads)) {
			mainwp_snippet_clear_sites_start_specific( siteToRun );
		}
	}

	mainwp_snippet_clear_sites_start_specific = function(pSiteToRun) {
		snippet_CurrentThreads++;
		pSiteToRun.attr( 'status', 'progress' );
		var statusEl = pSiteToRun.find( '.status' ).html( '<i class="fa fa-spinner fa-fw fa-pulse" aria-hidden="true"></i> ' + 'Executing...' );
		var data = {
			action:'mainwp_snippet_clear_on_site',
			siteId: pSiteToRun.attr( 'siteid' ),
			snippetSlug: jQuery( '#mainwp_snippet_slug_value' ).val(),
			type: jQuery( '#mainwp_snippet_type_value' ).val()
		};

		jQuery.post(ajaxurl, data, function (response)
			{
			pSiteToRun.attr( 'status', 'done' );
			if ( ! response || response === 'FAIL') {
				statusEl.html( __( 'Undefined error! Please try again.' ) ).show();
				statusEl.css( 'color', '#a00' );
			} else {
				if ( response['error']) {
					statusEl.html( response['error'] ).show();
					statusEl.css( 'color', '#a00' );
				} else if (response['status'] === 'SUCCESS') {
					statusEl.html( 'Done!' ).show();
				} else if (response['status'] === 'FAIL') {
					statusEl.html( 'No changes.' ).show();
					statusEl.css( 'color', '#a00' );
				}
			}
			snippet_CurrentThreads--;
			snippet_FinishedThreads++;
			if (snippet_FinishedThreads == snippet_TotalThreads && snippet_FinishedThreads != 0) {
				mainwp_snippet_save( true, false );
			}
			mainwp_snippet_clear_sites_start_next();
		}, 'json');
	}

	mainwp_snippet_delete_popup = function ()
{
		jQuery( '#mainwp-snippet-delete-popup' ).dialog({
			resizable: false,
			height: 280,
			width: 500,
			modal: true,
			close: function(event, ui) {jQuery( '#mainwp-snippet-delete-popup' ).dialog( 'destroy' ); location.href = 'admin.php?page=Extensions-Mainwp-Code-Snippets-Extension';}});
	};

	mainwp_snippet_delete_sites_start_next = function() {
		if (snippet_TotalThreads == 0) {
			snippet_TotalThreads = jQuery( '.mainwpSnippetDeleteSitesItem[status="queue"]' ).length; }
		while ((siteToRun = jQuery( '.mainwpSnippetDeleteSitesItem[status="queue"]:first' )) && (siteToRun.length > 0)  && (snippet_CurrentThreads < snippet_MaxThreads)) {
			mainwp_snippet_delete_sites_start_specific( siteToRun );
		}
	}

	mainwp_snippet_delete_sites_start_specific = function(pSiteToRun) {
		snippet_CurrentThreads++;
		pSiteToRun.attr( 'status', 'progress' );
		var statusEl = pSiteToRun.find( '.status' ).html( '<i class="fa fa-spinner fa-pulse fa-fw"></i> ' + 'Deleting...' );
		var data = {
			action:'mainwp_snippet_delete_on_site',
			siteId: pSiteToRun.attr( 'siteid' ),
			snippetSlug: jQuery( '#mainwp_snippet_slug_value' ).val(),
			type: jQuery( '#mainwp_snippet_type_value' ).val()
		};

		jQuery.post(ajaxurl, data, function (response)
			{
			pSiteToRun.attr( 'status', 'done' );
			if ( ! response || response === 'FAIL') {
				statusEl.html( __( 'Undefined error! Please try again.' ) ).show();
				statusEl.css( 'color', '#a00' );
			} else {
				if ( response['error']) {
					statusEl.html( response['error'] ).show();
					statusEl.css( 'color', '#a00' );
				} else if (response['status'] === 'SUCCESS') {
					statusEl.html( 'Successful!' ).show();
				} else if (response['status'] === 'FAIL') {
					statusEl.html( 'No changes.' ).show();
					statusEl.css( 'color', '#a00' );
				}
			}

			snippet_CurrentThreads--;
			snippet_FinishedThreads++;
			if (snippet_FinishedThreads == snippet_TotalThreads && snippet_FinishedThreads != 0) {
				//jQuery( '#maiwnp_snippet_delete_ajax_box' ).html( 'Snippet has been deleted successfully!' ).fadeIn();
				var data = {
					action:'mainwp_snippet_delete_snippet',
					snippet_id: jQuery( '#mainwp_snippet_delete_id' ).val()
				}
				jQuery.post(ajaxurl, data, function(response){
					var mess;
					if (response && response === 'SUCCESS') {
						mess = __( 'Snippet has been deleted successfully!' );
					} else {
						mess = __( 'Snippet could not be deleted.' );
					}
						jQuery( '#maiwnp_snippet_delete_ajax_box' ).html( '<div class="mainwp-notice mainwp-notice-green">' + mess + '</div>' )
						setTimeout(function() {
							location.href = 'admin.php?page=Extensions-Mainwp-Code-Snippets-Extension';
						}, 3000);
				})
			}
			mainwp_snippet_delete_sites_start_next();
		}, 'json');
	}
