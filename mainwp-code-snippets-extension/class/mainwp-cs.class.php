<?php

class MainWP_CS
{
	public static $instance = null;
	protected $option_handle = 'mainwp_child_branding_options';
	protected $option;

	static function get_instance() {

		if ( null === MainWP_CS::$instance ) { MainWP_CS::$instance = new MainWP_CS(); }
			return MainWP_CS::$instance;
	}

	public function __construct() {
		$this->option = get_option( $this->option_handle );
	}

	public function init() {
		add_action( 'wp_ajax_mainwp_snippet_run_snippet_loading', array( $this, 'run_snippet_loading' ) );
		add_action( 'wp_ajax_mainwp_snippet_run_snippet', array( $this, 'run_snippet' ) );
		add_action( 'wp_ajax_mainwp_snippet_delete_snippet', array( $this, 'delete_snippet' ) );
		add_action( 'wp_ajax_mainwp_snippet_save_snippet', array( $this, 'save_snippet' ) );
		add_action( 'wp_ajax_mainwp_snippet_clear_on_site_loading', array( $this, 'snippet_clear_site_loading' ) );
		add_action( 'wp_ajax_mainwp_snippet_clear_on_site', array( $this, 'delete_snippet_site' ) ); // same delete
		add_action( 'wp_ajax_mainwp_snippet_update_site_loading', array( $this, 'update_snippet_site_loading' ) );
		add_action( 'wp_ajax_mainwp_snippet_update_site', array( $this, 'update_snippet_site' ) );
		add_action( 'wp_ajax_mainwp_snippet_delete_on_site', array( $this, 'delete_snippet_site' ) );
	}

	public function get_option( $key = null, $default = '' ) {
		if ( isset( $this->option[ $key ] ) ) {
			return $this->option[ $key ]; }
		return $default;
	}

	public function set_option( $key, $value ) {
		$this->option[ $key ] = $value;
		return update_option( $this->option_handle, $this->option );
	}

	public function delete_snippet() {
		$id = $_POST['snippet_id'];
		if ( ! $id ) {
			die( 'FAIL' ); }
		if ( MainWP_CS_DB::get_instance()->remove_codesnippet( $id ) ) {
			die( 'SUCCESS' ); }
		die( 'FAIL' );
	}

	public function save_snippet() {
		if ( isset( $_POST['snippet_title'] ) ) {
			$snippet = array(
			   'code' => $_POST['code'],
			   'title' => $_POST['snippet_title'],
			   'description' => $_POST['desc'],
			);

			if ( $_POST['snippet_id'] ) {
				$snippet['id'] = $_POST['snippet_id'];
			} else { // create new snippet
				$snippet['snippet_slug'] = MainWP_CS_Utility::rand_string( 5 );
			}

			$snippet['type'] = 'R';
			if ( isset( $_POST['type'] ) ) {
				$snippet['type'] = $_POST['type']; }

			$selected_wp = $selected_group = array();
			if ( isset( $_POST['select_by'] ) ) {
				if ( isset( $_POST['sites'] ) && is_array( $_POST['sites'] ) ) {
					foreach ( $_POST['sites'] as $selected ) {
						$selected_wp[] = $selected;
					}
				}
				if ( isset( $_POST['groups'] ) && is_array( $_POST['groups'] ) ) {
					foreach ( $_POST['groups'] as $selected ) {
						$selected_group[] = $selected;
					}
				}
			}

			$snippet['sites'] = base64_encode( serialize( $selected_wp ) );
			$snippet['groups']  = base64_encode( serialize( $selected_group ) );

			if ( false !== ($get_snippet = MainWP_CS_DB::get_instance()->update_codesnippet( $snippet )) ) {
				$return = array();
				$return['id'] = $get_snippet->id;
				$return['slug'] = $get_snippet->snippet_slug;
				$return['type'] = $get_snippet->type;
				$return['status'] = 'SUCCESS';
				die( json_encode( $return ) );
			}
		}
		die( json_encode( array() ) );
	}

	public function run_snippet_loading() {
		global $mainWPCSExtensionActivator;
		$sites = $groups = array();
		if ( is_array( $_POST['sites'] ) && count( $_POST['sites'] ) > 0 ) {
			$sites = $_POST['sites']; }

		if ( is_array( $_POST['groups'] ) && count( $_POST['groups'] ) > 0 ) {
			$groups = $_POST['groups']; }

		$dbwebsites = apply_filters( 'mainwp-getdbsites', $mainWPCSExtensionActivator->get_child_file(), $mainWPCSExtensionActivator->get_child_key(), $sites, $groups );

		if ( ! is_array( $dbwebsites ) || count( $dbwebsites ) <= 0 ) {
			die( __( 'Websites could not be found. Please make sure your child site have been connected properly.', 'mainwp-code-snippets-extension' ) );
		}

		foreach ( $dbwebsites as $website ) {
			?>
			<div style="padding: 10px 0; border-bottom: 1px solid #e5e5e5;">
				<strong><a href="<?php echo $website->url; ?>" title="<?php _e( 'Visit site', 'mainwp-code-snippets-extension'); ?>"><?php echo stripslashes( $website->name ); ?></a></strong>
				<span class="mainwpSnippetItem" siteid="<?php echo $website->id; ?>" status="queue"><span class="status"><?php _e( 'Queue', 'mainwp-code-snippets-extension' ); ?></span>
					<div class="run_result" style="padding: 5px; background: #fafafa; border: 1px solid #e5e5e5; display: none;"></div>
				</span>
			</div>
			<?php
		}
		exit;
	}

	function run_snippet() {
		$siteid = $_POST['siteId'];
		$code = stripslashes( $_POST['code'] );
		$code = preg_replace( '|^[\s]*<\?(php)?|', '', $code );
		$code = preg_replace( '|\?>[\s]*$|', '', $code );
		$code = trim( $code );
		if ( empty( $siteid ) ) {
			die( json_encode( 'FAIL' ) ); } else if ( empty( $code ) ) {
			die( json_encode( 'CODEEMPTY' ) ); }

			global $mainWPCSExtensionActivator;
			$post_data = array( 'action' => 'run_snippet', 'code' => $code );
			$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPCSExtensionActivator->get_child_file(), $mainWPCSExtensionActivator->get_child_key(), $siteid, 'code_snippet', $post_data );
			die( json_encode( $information ) );
	}


	public static function rende_delete_snippet_on_sites( $snippetId ) {
		global $mainWPCSExtensionActivator;
		$return = array();
		if ( empty( $snippetId ) ) {
			   echo '<div class="mainwp-notice mainwp-notice-red">' . __( 'Error: snippet ID is empty!', 'mainwp-code-snippets-extension' ) . '</div>';
			   return;
		} else {
			$snippet = MainWP_CS_DB::get_instance()->get_codesnippet_by( 'id', $snippetId );
			if ( ! is_object( $snippet ) ) {
				echo '<div class="mainwp-notice mainwp-notice-red">' . __( 'Error: Snippet could not be found!', 'mainwp-code-snippets-extension' ) . '</div>';
				return;
			}
		}

		$websites = apply_filters( 'mainwp-getsites', $mainWPCSExtensionActivator->get_child_file(), $mainWPCSExtensionActivator->get_child_key(), null );
		if ( is_array( $websites ) && count( $websites ) > 0 ) {
		?>
		<div class="postbox">
			<button type="button" class="handlediv" aria-expanded="true"><span class="screen-reader-text">Toggle panel: <i class="fa fa-code" aria-hidden="true"></i> Delting Snippet</span><span class="toggle-indicator" aria-hidden="true"></span></button>
			<h3 class="hndle ui-sortable-handle mainwp_box_title"><span><i class="fa fa-code" aria-hidden="true"></i> <?php _e( 'Deleting Snippet', 'mainwp-code-snippets-extension' ); ?></span></h3>
			<div class="inside">
				<?php
				foreach ( $websites as $website ) {
					?>
					<div>
						<strong><a href="<?php echo $website->url; ?>"><?php echo $website['name']; ?></a></strong>:
						<span class="mainwpSnippetDeleteSitesItem" snippetid="<?php echo $snippedId; ?>" siteid="<?php echo $website['id']; ?>" status="queue">
							<span class="status"><?php _e( 'Queue', 'mainwp-code-snippets-extension' ); ?></span>
						</span>
					</div>
					<?php
				}
				?>
		    <input type="hidden" id="mainwp_snippet_delete_id" value="<?php echo $snippet->id; ?>">
		    <input type="hidden" id="mainwp_snippet_slug_value" name="mainwp_snippet_slug_value" value="<?php echo $snippet->snippet_slug; ?>">
		    <input type="hidden" id="mainwp_snippet_type_value" name="mainwp_snippet_type_value" value="<?php echo $snippet->type; ?>">
		    <div id="maiwnp_snippet_delete_ajax_box"></div>
				<script>
	          jQuery(document).ready(function($) {
	              mainwp_snippet_delete_sites_start_next();
	          })
	      </script>
			</div>
		</div>
		<?php
		} else {
			echo '<div class="mainwp-notice mainwp-notice-yellow">' . __( 'Please select child sites first.', 'mainwp-code-snippets-extension' ) . '</div>';
		}

	}

	function delete_snippet_site() {
		$siteid = $_POST['siteId'];
		$snippetslug = $_POST['snippetSlug'];
		$type = $_POST['type'];
		if ( empty( $siteid ) || empty( $snippetslug ) || empty( $type ) ) {
			die( json_encode( 'FAIL' ) ); }

		global $mainWPCSExtensionActivator;
		$post_data = array( 'action' => 'delete_snippet', 'slug' => $snippetslug, 'type' => $type );
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPCSExtensionActivator->get_child_file(), $mainWPCSExtensionActivator->get_child_key(), $siteid, 'code_snippet', $post_data );
		die( json_encode( $information ) );
	}

	function snippet_clear_site_loading() {
		global $mainWPCSExtensionActivator;
		$snippet = self::check_snippet_post_value();
		//$dbwebsites = $this->get_snippet_dbsites($snippet);
		//print_r($dbwebsites);

		$dbwebsites = array();
		$websites = apply_filters( 'mainwp-getsites', $mainWPCSExtensionActivator->get_child_file(), $mainWPCSExtensionActivator->get_child_key(), null );

		if ( is_array( $websites ) ) {
			foreach ( $websites as $website ) {
				$dbsite = apply_filters( 'mainwp-getdbsites', $mainWPCSExtensionActivator->get_child_file(), $mainWPCSExtensionActivator->get_child_key(), array( $website['id'] ), null );
				$dbsite = current( $dbsite );
				$dbwebsites[ $website['id'] ] = $dbsite;
			}
		}

		if ( ! is_array( $dbwebsites ) ||  count( $dbwebsites ) <= 0 ) {
			die( 'NOSITES' );
		}
		foreach ( $dbwebsites as $website ) {
			?>
			<div style="padding: 10px 0; border-bottom: 1px solid #e5e5e5;">
				<strong><a href="<?php echo $website->url; ?>" title="<?php _e( 'Visit site', 'mainwp-code-snippets-extension'); ?>"><?php echo stripslashes( $website->name ); ?></a></strong>:
				<span class="mainwpSnippetClearSitesItem" snippetid="<?php echo $snippet->id; ?>" siteid="<?php echo $website->id; ?>" status="queue"><span class="status"><?php _e( 'Queue', 'mainwp-code-snippets-extension' ); ?></span>
					<!-- <div class="run_result" style="padding: 5px; background: #fafafa; border: 1px solid #e5e5e5;"></div> -->
				</span>
			</div>
			<?php
		}
		die();
	}

	public static function check_code_snippet( $code ) {
		$code = stripslashes( $code );
		$code = preg_replace( '|^[\s]*<\?(php)?|', '', $code );
		$code = preg_replace( '|\?>[\s]*$|', '', $code );
		$code = trim( $code );

		if ( empty( $code ) ) {
			return 'CODEEMPTY'; }
		return $code;
	}

	public static function check_snippet_post_value() {
		$snippet_id = $_POST['snippetId'];
		if ( empty( $snippet_id ) ) {
			die( '<div class="mainwp-notice mainwp-notice-red">' . __( 'Error: empty code snipped ID.', 'mainwp-code-snippets-extension' ) . '</div>' );
		}

		$snippet = MainWP_CS_DB::get_instance()->get_codesnippet_by( 'id', $snippet_id );
		if ( ! is_object( $snippet ) ) {
			die( '<div class="mainwp-notice mainwp-notice-red">' . __( 'EError: Empty code snippet.', 'mainwp-code-snippets-extension' ) . '</div>' );
		}
		return $snippet;
	}

	function get_snippet_dbsites( $snippet ) {
		global $mainWPCSExtensionActivator;
		$sites = unserialize( base64_decode( $snippet->sites ) );
		$groups = unserialize( base64_decode( $snippet->groups ) );
		return apply_filters( 'mainwp-getdbsites', $mainWPCSExtensionActivator->get_child_file(), $mainWPCSExtensionActivator->get_child_key(), $sites, $groups );
	}

	public static function update_snippet_site_loading() {
		$snippet = self::check_snippet_post_value();
		$code = self::check_code_snippet( $snippet->code );
		if ( 'CODEEMPTY' === $code ) {
			die( '<div class="mainwp-notice mainwp-notice-red">' . __( 'Error: Empty code snippet.', 'mainwp-code-snippets-extension' ) . '</div>' );
		}
		$dbwebsites = MainWP_CS::get_instance()->get_snippet_dbsites( $snippet );

		if ( ! is_array( $dbwebsites ) ||  count( $dbwebsites ) <= 0 ) {
			die( 'NOSITES' );
		}
		foreach ( $dbwebsites as $website ) {
			?>
			<div style="padding: 10px 0; border-bottom: 1px solid #e5e5e5;">
				<strong><a href="<?php echo $website->url; ?>" title="<?php _e( 'Visit site', 'mainwp-code-snippets-extension'); ?>"><?php echo stripslashes( $website->name ); ?></a></strong>:
				<span class="mainwpSnippetUpdateSitesItem" snippetid="<?php echo $snippet->id; ?>" siteid="<?php echo $website->id; ?>" status="queue"><span class="status"><?php _e( 'Queue', 'mainwp-code-snippets-extension' ); ?></span>
					<!-- <div class="run_result" style="padding: 5px; background: #fafafa; border: 1px solid #e5e5e5;"></div> -->
				</span>
			</div>
			<?php
		}
		die();
	}

	function update_snippet_site() {
		$siteid = $_POST['siteId'];
		$snippetslug = $_POST['snippetSlug'];
		$type = $_POST['type'];

		if ( empty( $siteid ) || empty( $snippetslug ) ) {
			die( json_encode( 'FAIL' ) ); }

		$code = self::check_code_snippet( $_POST['code'] );

		if ( 'CODEEMPTY' === $code ) {
			die( json_encode( 'CODEEMPTY' ) ); }

		global $mainWPCSExtensionActivator;
		$post_data = array( 'action' => 'save_snippet', 'code' => $code, 'slug' => $snippetslug, 'type' => $type );
		$information = apply_filters( 'mainwp_fetchurlauthed', $mainWPCSExtensionActivator->get_child_file(), $mainWPCSExtensionActivator->get_child_key(), $siteid, 'code_snippet', $post_data );
		die( json_encode( $information ) );
	}

	public static function render_settings() {

		$snippet = false;
		if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
			$snippet = MainWP_CS_DB::get_instance()->get_codesnippet_by( 'id' , $_GET['id'] );
		}
		$_code = $_title = $_desc = $sites = $_slug = $_type = '';
		$type_run = $type_save = $type_config = '';
		$selected_sites = $selected_groups = array();
		$code_id = 0;
		if ( is_object( $snippet ) ) {
			$code_id = $snippet->id;
			$_code = $snippet->code;
			$_title = $snippet->title;
			$_desc = $snippet->description;
			$_slug = $snippet->snippet_slug;
			$selected_sites = unserialize( base64_decode( $snippet->sites ) );
			$selected_groups = unserialize( base64_decode( $snippet->groups ) );
			$_type = $snippet->type;
			if ( 'R' == $_type ) {
				$type_run = 'checked'; } else if ( 'S' == $_type ) {
				$type_save = 'checked'; } else if ( 'C' == $_type ) {
					$type_config = 'checked'; }
		}

		if ( empty( $type_run ) && empty( $type_save ) && empty( $type_config ) ) {
			$type_run = 'checked'; }

		if ( ! is_array( $selected_sites ) ) {
			$selected_sites = array(); }
		if ( ! is_array( $selected_groups ) ) {
			$selected_groups = array(); }
		?>
		<div  class="mainwp-notice mainwp-notice-red hidden" id="snippet-error-box"></div>
		<div class="metabox-holder columns-1" id="code-snippets-metabox-holder">
			<div id="normal-sortables" class="meta-box-sortables ui-sortable">
				<div class="postbox" id="mainwp-code-snippets-options">
					<button type="button" class="handlediv" aria-expanded="true"><span class="screen-reader-text">Toggle panel: <i class="fa fa-code" aria-hidden="true"></i> Code Snippets Settings</span><span class="toggle-indicator" aria-hidden="true"></span></button>
					<h2 class="hndle ui-sortable-handle"><span><i class="fa fa-code" aria-hidden="true"></i> <?php _e( 'Code Snippets', 'mainwp-code-snippets-extension' ); ?></span></h2>
					<div class="inside">
						<form method="POST" id="mainwp_snippet_edit_form" action="admin.php?page=Extensions-Mainwp-Code-Snippets-Extension">
						<input type="hidden" id="mainwp_snippet_id_value" name="mainwp_snippet_id_value" value="<?php echo $code_id; ?>">
            <input type="hidden" id="mainwp_snippet_slug_value" name="mainwp_snippet_slug_value" value="<?php echo $_slug; ?>">
            <input type="hidden" id="mainwp_snippet_type_value" name="mainwp_snippet_type_value" value="<?php echo $_type; ?>">
						<table class="form-table">
							<tbody>
								<tr>
									<th scope="row"><?php _e( 'Snippet title', 'mainwp-code-snippets-extension' ); ?></th>
									<td><input type="text" id="snp_snippet_title" name="snp_snippet_title" size="35" value="<?php echo stripslashes( $_title ); ?>"/></td>
								</tr>
								<tr>
									<th scope="row"><?php _e( 'Snippet description', 'mainwp-code-snippets-extension' ); ?></th>
									<td><textarea id="snp_snippet_desc" name="snp_snippet_desc" cols="100" rows="5"><?php echo stripslashes( esc_textarea( $_desc ) ); ?></textarea></td>
								</tr>
								<tr>
									<th scope="row"><?php _e( 'Snippet type', 'mainwp-code-snippets-extension' ); ?></th>
									<td>
										<span class="mainwp-radio">
												<input type="radio" id="rad-snippet-type-save" value="S" <?php echo $type_save; ?> name="snp_snippet_type">
												<label for="rad-snippet-type-save"></label>
										</span>
										<label for="rad-snippet-type-save"><?php _e( 'This Code Snippet executes a function on the Child Site' ); ?></label><br/>
										<span class="mainwp-radio">
												<input type="radio" id="rad-snippet-type-run" value="R" <?php echo $type_run; ?> name="snp_snippet_type">
												<label for="rad-snippet-type-run"></label>
										</span>
										<label for="rad-snippet-type-run"><?php _e( 'This Code Snippet only returns information from Child Site' ); ?></label><br/>
										<span class="mainwp-radio">
												<input type="radio" id="rad-snippet-type-config" value="C" <?php echo $type_config; ?> name="snp_snippet_type">
												<label for="rad-snippet-type-config"></label>
										</span>
										<label for="rad-snippet-type-config"><?php _e( 'This Code Snippet goes to the wp-config.php file' ); ?></label>
									</td>
								</tr>
							</tbody>
						</table>
						<input type="hidden" name="snp_security" value="<?php echo wp_create_nonce( 'mainwp-save-snippet' ); ?>">
						<textarea id="snp_snippet_code" name="snp_snippet_code" rows="20" spellcheck="false" style="font-family: monospace; width: 100%;"><?php echo ! empty( $_code ) ? esc_textarea( stripslashes( $_code ) ) : ''; ?></textarea>
						</form>
					</div>
					<div class="mainwp-postbox-actions-mid">
						<strong><?php _e( 'MainWP is not responsible for the code that you run on your sites.', 'mainwp-code-snippets-extension' ); ?></strong><br/>
						<?php _e( 'Use this tool with extreme care and at your own risk. It is recommended that you run any code on a test site before releasing on live sites.', 'mainwp-code-snippets-extension' );?>
					</div>
					<div class="inside">
						<!-- <div id="snippet_submit_loading" class="hidden mainwp-notice"><i class="fa fa-spinner fa-pulse fa-fw"></i> <span class="status"></span></div>-->
						<div class="mainwp-left mainwp-cols-1-2">
							<a href="?page=Extensions-Mainwp-Code-Snippets-Extension" class="button button-hero button-primary"><?php _e( 'New Snippet' ,'mainwp-code-snippets-extension' ); ?></a>
						</div>
						<div class="mainwp-right mainwp-cols-1-2 mainwp-align-right">
							<input type="button" id="maiwnp_snippet_btn_save" class="button button-hero" value="<?php _e( 'Save Snippet', 'mainwp-code-snippets-extension' ); ?>"/>
							<input type="button" id="maiwnp_snippet_btn_run" class="button-primary button  button-hero" value="<?php _e( 'Run Snippet', 'mainwp-code-snippets-extension' ); ?>">
						</div>
						<div style="clear: both;"></div>
					</div>
				</div>
				<div style="clear:both;"></div>
				<div class="postbox hidden" id="maiwnp_snippet_running_box">
					<button type="button" class="handlediv" aria-expanded="true"><span class="screen-reader-text">Toggle panel: <i class="fa fa-code" aria-hidden="true"></i> <?php _e( 'Console', 'mainwp-code-snippets-extension' ); ?></span><span class="toggle-indicator" aria-hidden="true"></span></button>
					<h2 class="hndle ui-sortable-handle"><span><i class="fa fa-code" aria-hidden="true"></i> <?php _e( 'Console', 'mainwp-code-snippets-extension' ); ?></span></h2>
					<div class="mainwp-postbox-actions-top">
						<div id="mainwp_code_snippet_result_output_title" style="margin-bottom: 10px; font-size: 14px; font-weight: bold;"></div>
						<div id="mainwp_code_snippet_result_output_log" class="mainwp-small" style="background: #e5e5e5; padding: 5px;"></div>
					</div>
					<div class="inside" id="mainwp_code_snippet_result">
						<div id="mainwp_code_snippet_result_output"></div>
					</div>
				</div>
			</div>
		</div>
		<?php do_action( 'mainwp_select_sites_box', __( 'Select Sites', 'mainwp' ), 'checkbox', true, true, 'mainwp_select_sites_box_right', '', $selected_sites, $selected_groups ); ?>
		<div style="clear:both;"></div>
    <?php
	}

	public static function render_list() {
		$titleorder = $descorder = $dateorder = $typeorder = '';
		$orderby = 'date desc';
		if ( isset( $_GET['orderby'] ) && isset( $_GET['order'] ) ) {
			if ( 'title' == $_GET['orderby'] ) {
				$orderby = 'title ' . ('asc' == $_GET['order'] ? 'asc' : 'desc');
				$titleorder = 'asc' == $_GET['order'] ? 'desc' : 'asc';
			} else if ( 'description' == $_GET['orderby'] ) {
				$orderby = 'description ' . ('asc' == $_GET['order'] ? 'asc' : 'desc');
				$descorder = 'asc' == $_GET['order'] ? 'desc' : 'asc';
			} else if ( 'date' == $_GET['orderby'] ) {
				$orderby = 'date ' . ('asc' == $_GET['order'] ? 'asc' : 'desc');
				$dateorder = 'asc' == $_GET['order'] ? 'desc' : 'asc';
			} else if ( 'type' == $_GET['orderby'] ) {
				$orderby = 'type ' . ('asc' == $_GET['order'] ? 'asc' : 'desc');
				$typeorder = 'asc' == $_GET['order'] ? 'desc' : 'asc';
			}
		} else {
			$titleorder = 'desc';
		}

		$snippets = MainWP_CS_DB::get_instance()->get_codesnippet_by( 'all', null, null, $orderby );
		?>
      <table cellspacing="0" id="mainwp_posts_table" class="wp-list-table widefat">
          <thead>
              <tr class="tablesorter-headerRow">
                  <th class="manage-column sortable <?php echo $titleorder; ?>" scope="col">
                      <a href="?page=Extensions-Mainwp-Code-Snippets-Extension&orderby=title&order=<?php echo ('' == $titleorder ? 'asc' : $titleorder); ?>"><span><?php _e( 'Code Snippet', 'mainwp-code-snippets-extension' ); ?></span><span class="sorting-indicator"></span></a>
                  </th>
                  <th class="manage-column sortable <?php echo $typeorder; ?>" scope="col">
                      <a href="?page=Extensions-Mainwp-Code-Snippets-Extension&orderby=type&order=<?php echo ('' == $typeorder ? 'asc' : $typeorder); ?>"><span><?php _e( 'Type', 'mainwp-code-snippets-extension' ); ?></span><span class="sorting-indicator"></span></a>
                  </th>
                  <th class="manage-column sortable <?php echo $descorder; ?>" scope="col">
                      <a href="?page=Extensions-Mainwp-Code-Snippets-Extension&orderby=description&order=<?php echo ('' == $descorder ? 'asc' : $descorder); ?>"><span><?php _e( 'Description', 'mainwp-code-snippets-extension' ); ?></span><span class="sorting-indicator"></span></a>
                  </th>
                  <th class="manage-column sortable <?php echo $dateorder; ?>" scope="col" >
                      <a href="?page=Extensions-Mainwp-Code-Snippets-Extension&orderby=date&order=<?php echo ('' == $dateorder ? 'asc' : $dateorder); ?>"><span><?php _e( 'Last Edited', 'mainwp-code-snippets-extension' ); ?></span><span class="sorting-indicator"></span></a>
                  </th>
              </tr>
          </thead>
          <tfoot>
              <tr>
                  <th class="manage-column sortable <?php echo $titleorder; ?>" scope="col">
                      <a href="?page=Extensions-Mainwp-Code-Snippets-Extension&orderby=title&order=<?php echo ('' == $titleorder ? 'asc' : $titleorder); ?>"><span><?php _e( 'Code Snippet', 'mainwp-code-snippets-extension' ); ?></span><span class="sorting-indicator"></span></a>
                  </th>
                  <th class="manage-column sortable <?php echo $typeorder; ?>" scope="col">
                      <a href="?page=Extensions-Mainwp-Code-Snippets-Extension&orderby=type&order=<?php echo ('' == $typeorder ? 'asc' : $typeorder); ?>"><span><?php _e( 'Type', 'mainwp-code-snippets-extension' ); ?></span><span class="sorting-indicator"></span></a>
                  </th>
                  <th class="manage-column sortable <?php echo $descorder; ?>" scope="col">
                      <a href="?page=Extensions-Mainwp-Code-Snippets-Extension&orderby=description&order=<?php echo ('' == $descorder ? 'asc' : $descorder); ?>"><span><?php _e( 'Description', 'mainwp-code-snippets-extension' ); ?></span><span class="sorting-indicator"></span></a>
                  </th>
                  <th class="manage-column sortable <?php echo $dateorder; ?>" scope="col" >
                      <a href="?page=Extensions-Mainwp-Code-Snippets-Extension&orderby=date&order=<?php echo ('' == $dateorder ? 'asc' : $dateorder); ?>"><span><?php _e( 'Last Edited', 'mainwp-code-snippets-extension' ); ?></span><span class="sorting-indicator"></span></a>
                  </th>
              </tr>
          </tfoot>
          <tbody class="list:posts" id="the-pages-list">
            <?php
						if ( is_array( $snippets ) && count( $snippets ) > 0 ) {
							self::render_table_content( $snippets, $orderby );
						} else {
							echo '<tr><td colspan="4">' . __( 'No saved snippets.', 'mainwp-code-snippets-extension' ) . '</td></tr>';
						}
						?>
          </tbody>
        </table>
        <div class="mainwp-diablog-box-container">
            <div id="mainwp-snippet-delete-popup" title="Delete Snippet" class="hidden">
                <div id="mainwp-snippet-delete-result" style="padding: 0 1em;">
                    <p><?php _e( 'Would you like to keep the existing snippet on the child sites?', 'mainwp-code-snippets-extension' );?></p>
                    <p><label><input type="radio" value="0" name="delete_snippet_child_site"> <?php _e( 'Yes' ,'mainwp-code-snippets-extension' ); ?></label></p>
                    <p><label><input type="radio" value="1" checked name="delete_snippet_child_site"> <?php _e( 'No' ,'mainwp-code-snippets-extension' ); ?></label></p>
                </div>
								<div style="padding: 0 1em;">
	                <p><input type="submit" value="<?php _e( 'Delete Snippet', 'mainwp-code-snippets-extension' ); ?>" class="button button-hero button-primary" id="snippet-btn-delete"></p>
	                <p><i class="hidden fa fa-snipper fa-pulse fa-fw" id="snp_delete_running"></i></p>
	                <input type="hidden" name="delete_snippetid" value="" />
								</div>
            </div>
        </div>

        <?php
			}

		public static function render_table_content( $snippets, $orderby ) {
			foreach ( $snippets as $snippet ) {
			?>
			   <tr valign="top" id="post-<?php echo $snippet->id; ?>">
				   <td class="column-title">
					   <strong><a title="Load <?php echo $snippet->title; ?>" href="?page=Extensions-Mainwp-Code-Snippets-Extension&id=<?php echo $snippet->id; ?>"><?php echo stripslashes( $snippet->title ); ?></a></strong>
					   <div class="row-actions">
						   <span class="edit">
							   <a title="<?php _e( 'Load this snippet', 'mainwp-code-snippets-extension' ); ?>" href="?page=Extensions-Mainwp-Code-Snippets-Extension&id=<?php echo $snippet->id; ?>"><?php _e( 'Load snippet', 'mainwp-code-snippets-extension' ); ?></a>
						   </span>
						   <span class="delete">
							   | <a href="#" title="<?php _e( 'Delete this snippet', 'mainwp-code-snippets-extension' ); ?>" class="snippet_list_delete_item" type="<?php echo $snippet->type; ?>" id="<?php echo $snippet->id; ?>" ><?php _e( 'Delete', 'mainwp-code-snippets-extension' ); ?></a>
						   </span>
					   </div>
					   <span class="snp-row-ajax-info hidden"></span>
				   </td>
				   <td class="tags column-tags"><?php echo ( $snippet->type == 'S' ) ? __( 'Executes Function', 'mainwp-code-snippets-extension' ) : ($snippet->type == 'R' ? __( 'Return Information', 'mainwp-code-snippets-extension' ) : ( $snippet->type == 'C' ? __( 'Executes in wp-config.php', 'mainwp-code-snippets-extension' ) : '' ) ); ?></td>
				   <td class="tags column-tags"><?php echo stripslashes( $snippet->description ); ?></td>
				   <td class="date column-date">
						<?php
						   $date_format = get_option( 'date_format' );
						   $time_format = get_option( 'time_format' );
						?>
                <abbr><?php echo date( $date_format, $snippet->date ) . ' ' . date( $time_format, $snippet->date ); ?></abbr>
            </td>
        </tr>
        <?php
			}
	}

	public static function render() {

		$class = 'mainwp-notice-green';
		if ( isset( $_GET['message'] ) ) {
			if ( 1 == $_GET['message'] ) {
				$message = __( 'The Code Snippet has been saved.', 'mainwp-code-snippets-extension' );
			} else if ( -1 == $_GET['message'] ) {
				$message = __( 'The Code Snippet saved false.', 'mainwp-code-snippets-extension' );
				$class = 'mainwp-notice-red';
			}
		}

		if ( ! empty( $message ) ) {
		?>
      <div id="ajax-information-zone" class="mainwp-notice <?php echo $class; ?>"><?php echo $message; ?></div>
    <?php
		}

		if ( isset( $_GET['id'] ) && isset( $_GET['deleteonsites'] ) && 1 == $_GET['deleteonsites'] ) {
			self::rende_delete_snippet_on_sites( $_GET['id'] );
			return;
		}

		self::render_settings();
		?>
		<div id="code-snippets-table-holder">
			<?php
			self::render_list();
			?>
		</div>
		<div style="clear: both;"></div>
		<?php
	}
}
