<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * OPC Dummy Gateway
 */
class WC_Gateway_OPC_Hold_Order extends WC_Payment_Gateway {

	/**
	* Constructor for the gateway.
	*/
	public function __construct( $instance_id = 0 ) {
		$this->id                 = 'phone_manual_on_hold';
		$this->icon               = apply_filters('woocommerce_cheque_icon', '');
		$this->has_fields         = false;
		$this->method_title       = __( 'Phone & Manual Orders &ndash; Hold Order', 'manual_phone_orders' );
		$this->method_description = __( 'Allows WooCommerce Phone Orders & Manual Orders to place orders on hold for later processing', 'manual_phone_orders' );

		$this->instance_id = absint( $instance_id );
		
		//if ( $this->instance_id > 0 || ( version_compare( WC_VERSION, '2.6', '>' ) && is_admin() && !defined( 'DOING_AJAX' ) ) ) { 
			$this->supports = array(
				'settings',
				'products', 
				'subscriptions',
				'subscription_cancellation', 
				'subscription_suspension', 
				'subscription_reactivation',
				'subscription_amount_changes',
				'subscription_date_changes',
				'subscription_payment_method_change'
			);
		//}
	
		// Load the settings.
		$this->init();


		// Actions
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_thankyou_cheque', array( $this, 'thankyou_page' ) );

		// Customer Emails
		// add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );

	}
	
	function init() { 
	
		if ( $this->instance_id > 0 ) { 
			$this->instance_form_fields = $this->get_the_settings();
			$this->init_form_fields();
			$this->init_settings();
		} else { 
			$this->init_form_fields();
			$this->init_settings();
		}
	
	
		// Define user set variables
		$this->title        = $this->get_option( 'title' );
		$this->description  = $this->get_option( 'description' );
		$this->instructions = $this->get_option( 'instructions', $this->description );
		
	}

	function get_instance_form_fields() { 
		return $this->get_the_settings(); 
	}
	
	// Init form fields - modeled on WooCommerce core code
	function init_form_fields() {
		$this->form_fields = $this->get_the_settings();
	}
	
	/**
	* Initialise Gateway Settings Form Fields
	*/
	public function get_the_settings() {

		return array(
				'enabled' => array(
					'title'   => __( 'Enable/Disable', 'manual_phone_orders' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable Gateway', 'manual_phone_orders' ),
					'default' => ''
				),
				'title' => array(
					'title'       => __( 'Title', 'manual_phone_orders' ),
					'type'        => 'text',
					'description' => __( 'This controls the title which the user sees during checkout.', 'manual_phone_orders' ),
					'default'     => __( 'Hold Order', 'manual_phone_orders' ),
					'desc_tip'    => true,
				),
				'description' => array(
					'title'       => __( 'Description', 'manual_phone_orders' ),
					'type'        => 'textarea',
					'description' => __( 'Payment method description seen on the manual/phone order page.', 'manual_phone_orders' ),
					'default'     => __( 'This gateway places the order on hold', 'manual_phone_orders' ),
					'desc_tip'    => true,
				),
				'instructions' => array(
					'title'       => __( 'Notice', 'manual_phone_orders' ),
					'type'        => 'textarea',
					'description' => __( 'The message shown on the order page after you place this order on hold.', 'manual_phone_orders' ),
					'default'     => __( 'This order has been placed on hold. You can reload it from the manual/phone order page.', 'manual_phone_orders' ),
					'desc_tip'    => true,
				),
			);

	}

	/**
	* Output for the order received page.
	*/
	public function thankyou_page() {
		if ( $this->instructions )
			echo wpautop( wptexturize( $this->instructions ) );
	}

	/**
	* Add content to the WC emails.
	*
	* @access public
	* @param WC_Order $order
	* @param bool $sent_to_admin
	* @param bool $plain_text
	*/
	
	public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
		return;
		/*
		if ( $this->instructions && ! $sent_to_admin && 'phone_manual_dummy' === $order->payment_method && $order->has_status( 'on-hold' ) ) {
			echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
		}
		*/
	}
	
	/**
	* Process the payment and return the result
	*
	* @param int $order_id
	* @return array
	*/
	public function process_payment( $order_id ) {

		$order = wc_get_order( $order_id );

		// Mark as on-hold (we're awaiting the cheque)
		$order->update_status( 'on-hold', __( 'Order placed on hold', 'manual_phone_orders' ) );

		// Reduce stock levels
		$order->reduce_order_stock();

		// Remove cart
		WC()->cart->empty_cart();

		// Return thankyou redirect
		return array(
			'result' 	=> 'success',
			'redirect'	=> $this->get_return_url( $order )
		);
	}
}
