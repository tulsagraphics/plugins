<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * OPC Dummy Gateway
 */
class WC_Gateway_OPC_Dummy extends WC_Payment_Gateway {

	/**
	* Constructor for the gateway.
	*/
	public function __construct( $instance_id = 0 ) {
		$this->id                 = 'phone_manual_dummy';
		$this->icon               = apply_filters('woocommerce_cheque_icon', '');
		$this->has_fields         = false;
		$this->method_title       = __( 'Phone & Manual Orders &ndash; Invoice', 'manual_phone_orders' );
		$this->method_description = __( 'Allows the use of WooCommerce Phone Orders & Manual Orders without the need to enter payment information. Useful when you want to send your customers an invoice, etc.', 'manual_phone_orders' );

		$this->instance_id = absint( $instance_id );
		
		//if ( $this->instance_id > 0 || ( version_compare( WC_VERSION, '2.6', '>' ) && is_admin() && !defined( 'DOING_AJAX' ) ) ) { 
			$this->supports = array(
				'settings',
				'instance-settings',
				'products', 
				'subscriptions',
				'subscription_cancellation', 
				'subscription_suspension', 
				'subscription_reactivation',
				'subscription_amount_changes',
				'subscription_date_changes',
				'subscription_payment_method_change'
				//'instance-settings-modal',
			);
		//}

		$this->has_fields = true;
		
		// Load the settings.
		$this->init();

		// Actions
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_thankyou_' . $this->id, array( &$this, 'thankyou_page' ), 10, 1 );

		// Customer Emails
		add_action( 'woocommerce_email_before_order_table', array( &$this, 'email_instructions' ), 10, 3 );

	}
	
	function init() { 
	
		if ( $this->instance_id > 0 ) { 
			$this->instance_form_fields = $this->get_the_settings();
			$this->init_form_fields();
			$this->init_settings();
		} else { 
			$this->init_form_fields();
			$this->init_settings();
		}
	
		// Define user set variables
		$this->title        = $this->get_option( 'title' );
		$this->description  = $this->get_option( 'description' );
		$this->instructions = $this->get_option( 'instructions', $this->description );
		
	}

	function get_instance_form_fields() { 
		return $this->get_the_settings(); 
	}
	
	// Init form fields - modeled on WooCommerce core code
	function init_form_fields() {
		$this->form_fields = $this->get_the_settings();
	}
	
	function payment_fields() { 
		?>
		<input type="checkbox" name="send_invoice_now" value="1" style="vertical-align:text-top"> 
		<?php _e( 'Check this box to automatically send invoice when the order is placed.', 'manual_phone_orders' );
		
	}
	/**
	* Initialise Gateway Settings Form Fields
	*/
	public function get_the_settings() {

		return array(
			'enabled' => array(
				'title'   => __( 'Enable/Disable', 'manual_phone_orders' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable Gateway', 'manual_phone_orders' ),
				'default' => ''
			),
			'title' => array(
				'title'       => __( 'Title', 'manual_phone_orders' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'manual_phone_orders' ),
				'default'     => __( 'Invoice Payment', 'manual_phone_orders' ),
				'desc_tip'    => true,
			),
			'description' => array(
				'title'       => __( 'Description', 'manual_phone_orders' ),
				'type'        => 'textarea',
				'description' => __( 'Payment method description that the customer will see on your checkout.', 'manual_phone_orders' ),
				'default'     => __( 'Please send your make payment to Store Name, Store Street, Store Town, Store State / County, Store Postcode.', 'manual_phone_orders' ),
				'desc_tip'    => true,
			),
			'instructions' => array(
				'title'       => __( 'Instructions', 'manual_phone_orders' ),
				'type'        => 'textarea',
				'description' => __( 'Instructions that will be added to the thank you page and emails.', 'manual_phone_orders' ),
				'default'     => '',
				'desc_tip'    => true,
			),
			'order_status' => array(
				'title'       => __( 'Order Status', 'manual_phone_orders' ),
				'type'        => 'select',
				'description' => __( 'The default order status to set for the order when this payment gateway is used to place the order', 'manual_phone_orders' ),
				'default'     => 'wc-on-hold',
				'desc_tip'    => true,
				'options'     => wc_get_order_statuses(),
			),
		);
	}

	/**
	* Output for the order received page.
	*/
	public function thankyou_page( $order_id = false ) {
		if ( $this->instructions )
			echo wpautop( wptexturize( $this->instructions ) );
	}

	/**
	* Add content to the WC emails.
	*
	* @access public
	* @param WC_Order $order
	* @param bool $sent_to_admin
	* @param bool $plain_text
	*/
	public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
		if ( method_exists( $order, 'get_payment_method' ) ) 
			$method = $order->get_payment_method();
		else 
			$method = $order->payment_method;
			
		if ( $this->instructions && ! $sent_to_admin && 'phone_manual_dummy' === $method && $order->has_status( 'on-hold' ) ) {
			echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
		}
	}

	/**
	* Process the payment and return the result
	*
	* @param int $order_id
	* @return array
	*/
	public function process_payment( $order_id ) {

		$order = wc_get_order( $order_id );
		
		if ( empty( $order ) )
			return;

		// Mark as on-hold (we're awaiting the cheque)
		if ( empty( $this->settings['order_status'] ) )
			$order->update_status( 'on-hold', __( 'Awaiting payment', 'manual_phone_orders' ) );
		else 
			$order->update_status( $this->settings['order_status'] );
			
		if ( in_array( $this->settings['order_status'], array( 'processing', 'completed' ) ) )
			$order->payment_complete();
		else 
			// Reduce stock levels
			$order->reduce_order_stock();

		// Remove cart
		WC()->cart->empty_cart();

		// Don't send invoice if the order has these statues, WC sends a receipt on its own already.
		if ( !empty( $_REQUEST['send_invoice_now'] ) && !in_array( $order->get_status(), array( 'processing', 'completed' ) ) ) { 

			do_action( 'woocommerce_before_resend_order_emails', $order, 'customer_invoice' );

			// Send the customer invoice email.
			WC()->payment_gateways();
			WC()->shipping();
			WC()->mailer()->customer_invoice( $order );

			// Note the event.
			$order->add_order_note( __( 'Order details automatically sent to customer.', 'manual_phone_orders' ), false, true );

			do_action( 'woocommerce_after_resend_order_email', $order, 'customer_invoice' );
		}
		
		// Return thankyou redirect
		return array(
			'result' 	=> 'success',
			'redirect'	=> $this->get_return_url( $order )
		);
	}
}
