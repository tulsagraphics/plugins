<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * OPC Dummy Gateway
 */
class WC_Gateway_OPC_Paid_In_Person extends WC_Payment_Gateway {

	/**
	* Constructor for the gateway.
	*/
	public function __construct( $instance_id = 0 ) {
		$this->id                 = 'phone_manual_paid_in_person';
		$this->icon               = apply_filters('woocommerce_cheque_icon', '');
		$this->has_fields         = false;
		$this->method_title       = __( 'Phone & Manual Orders &ndash; Paid in Person', 'manual_phone_orders' );
		$this->method_description = __( 'Allows the use of WooCommerce Phone Orders & Manual Orders without the need to enter payment information. Useful when you customers pay you in person via cash, check, etc.', 'manual_phone_orders' );

		$this->instance_id = absint( $instance_id );
		
		//if ( $this->instance_id > 0 || ( version_compare( WC_VERSION, '2.6', '>' ) && is_admin() && !defined( 'DOING_AJAX' ) ) ) { 
			$this->supports = array(
				'settings',
				'instance-settings',
				'products', 
				'subscriptions',
				'subscription_cancellation', 
				'subscription_suspension', 
				'subscription_reactivation',
				'subscription_amount_changes',
				'subscription_date_changes',
				'subscription_payment_method_change'
				//'instance-settings-modal',
			);
		//}
	
		// Load the settings.
		$this->init();

		// Actions
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

	}
	
	function init() { 
	
		if ( $this->instance_id > 0 ) { 
			$this->instance_form_fields = $this->get_the_settings();
			$this->init_form_fields();
			$this->init_settings();
		} else { 
			$this->init_form_fields();
			$this->init_settings();
		}
	
	
		// Define user set variables
		$this->title        = $this->get_option( 'title' );
		$this->description  = $this->get_option( 'description' );
		$this->instructions = $this->get_option( 'instructions', $this->description );
		
	}

	function get_instance_form_fields() { 
		return $this->get_the_settings(); 
	}
	
	// Init form fields - modeled on WooCommerce core code
	function init_form_fields() {
		$this->form_fields = $this->get_the_settings();
	}
	
	/**
	* Initialise Gateway Settings Form Fields
	*/
	public function get_the_settings() {

		return array(
			'enabled' => array(
				'title'   => __( 'Enable/Disable', 'manual_phone_orders' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable Gateway', 'manual_phone_orders' ),
				'default' => ''
			),
			'title' => array(
				'title'       => __( 'Title', 'manual_phone_orders' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'manual_phone_orders' ),
				'default'     => __( 'Paid in Person', 'manual_phone_orders' ),
				'desc_tip'    => true,
			),
			'description' => array(
				'title'       => __( 'Description', 'manual_phone_orders' ),
				'type'        => 'textarea',
				'description' => __( 'Payment method description that the customer will see on your checkout.', 'manual_phone_orders' ),
				'default'     => __( 'Use this if the customer paid in person', 'manual_phone_orders' ),
				'desc_tip'    => true,
			),
			'order_status' => array(
				'title'       => __( 'Order Status', 'manual_phone_orders' ),
				'type'        => 'select',
				'description' => __( 'The default order status to set for the order when this payment gateway is used to place the order', 'manual_phone_orders' ),
				'default'     => 'wc-on-hold',
				'desc_tip'    => true,
				'options'     => wc_get_order_statuses(),
			),
		);
	}


	/**
	* Add content to the WC emails.
	*
	* @access public
	* @param WC_Order $order
	* @param bool $sent_to_admin
	* @param bool $plain_text
	*/
	public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
		if ( method_exists( $order, 'get_payment_method' ) ) 
			$method = $order->get_payment_method();
		else 
			$method = $order->payment_method;
			
		if ( $this->instructions && ! $sent_to_admin && 'phone_manual_dummy' === $method && $order->has_status( 'on-hold' ) ) {
			echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
		}
	}

	/**
	* Process the payment and return the result
	*
	* @param int $order_id
	* @return array
	*/
	public function process_payment( $order_id ) {

		$order = wc_get_order( $order_id );

		if ( empty( $this->settings['order_status'] ) )
			$order->update_status( 'on-hold', __( 'Awaiting payment', 'manual_phone_orders' ) );
		else 
			$order->update_status( $this->settings['order_status'] );
			
		if ( in_array( $this->settings['order_status'], array( 'processing', 'completed' ) ) )
			$order->payment_complete();
		else 
			// Reduce stock levels
			$order->reduce_order_stock();

		// Remove cart
		WC()->cart->empty_cart();

		// Return thankyou redirect
		return array(
			'result' 	=> 'success',
			'redirect'	=> $this->get_return_url( $order )
		);
	}
}
