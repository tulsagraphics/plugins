<?php
/*
Plugin Name: WooCommerce Phone Orders & Manual Orders
Plugin URI: http://ignitewoo.com
Description: Lets you create a page with products and checkout all on one page
Version: 3.0.34
Author: IgniteWoo.com
Author URI: http://ignitewoo.com
Text Domain: manual_phone_orders
Domain Path: languages/
WC requires at least: 3.0
WC tested up to: 3.3.3
*/

/**
Copyright (c) 2014 - 2017 - IgniteWoo.com - All Rights Reserved


NOTE:

	DEVELOPERS: 

	Gateways can test to see if they should load content for the checkout page by calling a method as seen below. The code checks to see if the manual/phone order plugin is active, checks to ensure as $post has been loaded by WP, and then calls "$ign_opc->is_checkout()" to ask the manual/phone order plugin if the current page is its manual/phone order page. The method call return true if it is the right page and false if not.

	global $ign_opc, $post;

	if ( isset( $ign_opc ) && isset( $post->ID ) ) {
		if ( !$ign_opc->is_checkout() )
			return;
	}	

	
TODO: Modify the flow so that when a custom is assigned the entire user context switches to the customer's account. This greatly simplies all the user account issues and provides greater compatibility.
*/

class IGN_Manual_Phone_Orders {

	var $opc = false;

	var $style = '';

	var $products = array();

	function __construct() {

		add_action( 'init', array( &$this, 'init' ), 4 );

		add_action( 'plugins_loaded', array( &$this, 'plugins_loaded' ), 8 );

		// Add the gateway to the list of available payment gateway
		add_filter( 'woocommerce_payment_gateways', array( &$this, 'add_dummy_gateway' ) );
		
		// This is a bit hacky, it tries to set the customer as the active global user so that payment gateways 
		// can hopefully obtained saved cards for the customer properly. Somewhere during the call to 
		// woocommerce_checkout_payment() the user context is switched to the logged in user, which 
		// gives us the wrong user context, so use this hacky way of setting it to customer. 
		// This filter runs after WC handles getting the enabled gateways.
		add_filter( 'woocommerce_available_payment_gateways', array( &$this, 'force_customer_user' ) );

		// Add the shipping module
		add_filter( 'woocommerce_shipping_methods', array( &$this, 'add_shipping_method' ) );

		//add_filter( 'woocommerce_product_backorders_allowed', array( &$this, 'backorders_allowed' ), 9999, 2 );

		//add_filter( 'woocommerce_is_sold_individually', array( &$this, 'sold_individually' ), 9999, 2 );

		// Add this hook to replace the shortcode with an error message when the user does not have access,
		// if the user has access the core class will load and replace the shortcode.
		add_shortcode( 'woocommerce_manual_phone_order', array( &$this, 'access_is_denied' ) );

		add_action( 'init', array( &$this, 'load_plugin_textdomain' ) );

	}

	// This is a bit hacky, it tries to set the customer as the active global user so that payment gateways 
	// can hopefully obtained saved cards for the customer properly 
	function force_customer_user( $gateways = array() ) { 
		$this->switch_user_context_to_customer( 'customer' );
		return $gateways;
	}
	
	function load_plugin_textdomain() {

		$locale = apply_filters( 'plugin_locale', get_locale(), 'manual_phone_orders' );

		load_textdomain( 'manual_phone_orders', WP_LANG_DIR.'/woocommerce/manual_phone_orders-'.$locale.'.mo' );

		$plugin_rel_path = apply_filters( 'ignitewoo_translation_file_rel_path', dirname( plugin_basename( __FILE__ ) ) . '/languages' );

		load_plugin_textdomain( 'manual_phone_order', false, $plugin_rel_path );
	}

	public function init() {
		global $user_ID;

		$this->opc_user_id = $user_ID;

		if ( !$this->has_access() && !current_user_can( 'administrator' ) ) {
			return;
		}

		require_once( dirname( __FILE__ ) . '/woocommerce-manual-order-core.php' );

	}


	public function access_is_denied( $attrs = array() ) {

		if ( !$this->has_access() && !current_user_can( 'administrator' ) ) {
			return '<p class="ignitewoo_one_page_access_denied" style="font-weight:bold; font-style:italic">' . __( 'You do not have access to this page', 'manual_phone_orders' ) . '</p>';
		}
	}

	public function plugins_loaded() {

		if ( version_compare( WOOCOMMERCE_VERSION, '2.3', '<=' ) )
			add_action( 'admin_notices', array( $this, 'version_mismatch' ), 1, 1 );

		// Deprecated
		// require_once( dirname( __FILE__ ) . '/woocommerce-one-page-checkout-user-switcher.php' );

		if ( is_admin() && ( current_user_can( 'shop_manager' ) || current_user_can( 'administrator' ) ) ) {
			require_once( dirname( __FILE__ ) . '/woocommerce-manual-order-integration.php' );
		}
	}

	public function version_mismatch() {
		?>
		<div class="updated fade error" id="message"><p><strong style="color:#DD3D36"><?php _e( 'WooCommerce Manual Orders & Phone Orders Pro requires WooCommerce 2.3 or newer to operate correctly! ', 'manual_phone_orders' )?></strong></p></div>
		<?php
	}

	public function has_access() {
		global $user_ID;

		// Helps the built-in gateways load during order review updates via Ajax
		if ( isset( $_POST['post_data'] ) ) {

			$args = wp_parse_args( $_POST['post_data'] );

			if ( isset( $args['opc_admin_user_id'] ) ) {
				if ( user_can( absint( $args['opc_admin_user_id'] ), 'administrator' ) || user_can( absint( $args['opc_admin_user_id'] ), 'shop_manager' ) )
					return true;
			}
		}
		
		if ( empty( $this->opc_user_id ) )
			$this->switch_user_context_to_customer( 'opc_user' );

		if ( !empty( $_POST['opc_customer_selected'] ) /*&& wp_verify_nonce( $_POST['opc_customer_selected'], 'opc_customer_selected' ) */ )
			$this->switch_user_context_to_customer( 'opc_user' );
		
		if ( current_user_can( 'administrator' ) || current_user_can( 'shop_manager' ) )
			return true;

		/* opc_admin_user_id = the currently logged in user, no need to switch to customer user when checking access permissions
		if ( isset( $_POST['opc_admin_user_id'] ) ) {
			$this->switch_user_context_to_customer( 'customer' );
		}
		*/

		$opts = get_option( 'woocommerce_ignitewoo_manual_order_settings' );

		if ( empty( $opts ) )
			return false;

		if ( 'yes' !== $opts['enable'] )
			return false;

		if ( !is_user_logged_in() )
			return false;

		$has_access = false;

		if ( !empty( $opts['roles'] ) )
		foreach( $opts['roles'] as $role ) {

			if ( current_user_can( $role ) )
				$has_access = true;
		}

		if ( !empty( $opts['users'] ) )
		foreach( $opts['users'] as $user_id ) {

			if ( $user_ID == $user_id  )
				$has_access = true;
		}

		return $has_access;

	}

	public function switch_user_context_to_customer( $who ) {
		global $user_ID, $current_user;

		//var_dump( debug_backtrace()[1]['function'] );

		// failsafe to ensure we get a customer ID if it exists in the request

		if ( isset( $_POST['post_data'] ) ) {
			$args = wp_parse_args( $_POST['post_data'] );
			if ( isset( $args['ignitewoo_one_page_checkout_customer_id'] ) )
				$_POST['ignitewoo_one_page_checkout_customer_id'] = $args['ignitewoo_one_page_checkout_customer_id'];
		}

		$this->opc_customer_id = isset( $_POST['ignitewoo_one_page_checkout_customer_id'] ) ? absint( $_POST['ignitewoo_one_page_checkout_customer_id'] ) : null;

		if ( empty( $this->opc_customer_id ) && isset( $_POST['one_page_customer_user'] ) )
			$this->opc_customer_id = $_POST['one_page_customer_user'];
			
		if ( empty( $this->opc_customer_id ) && isset( $_POST['ignitewoo_one_page_checkout_customer_id'] ) )
			$this->opc_customer_id = $_POST['ignitewoo_one_page_checkout_customer_id'];
			
		// Only set this once so that it remains consistent and we don't lose track of who the admin user is.
		if ( empty( $this->opc_user_id ) )
			$this->opc_user_id = $user_ID;

		if ( 'opc_user' == $who ) {
			$wp_user = wp_set_current_user( $this->opc_user_id );
			$user_ID = $this->opc_user_id;
			$current_user = $wp_user;
		} else if ( 'customer' == $who ) {

			//$uid = isset( $this->opc_customer_id ) ? $this->opc_customer_id : $this->opc_user_id;
			$uid = isset( $this->opc_customer_id ) ? $this->opc_customer_id : $this->opc_user_id;
			$wp_user = wp_set_current_user( $uid );
			$user_ID = $uid;
			$current_user = $wp_user;
		}

	}

	// Always allow backorders because it's an admin or shop manager adding the order
	public function backorders_allowed( $allowed, $product ) {

		//if ( !isset( $_POST['action'] ) || empty( $_POST['opc_checkout_flag'] ) )
		//	return $allowed;

		//if ( !isset( $_POST['action'] ) || ( 'woocommerce_add_to_cart_opc' != $_POST['action'] && 'woocommerce_add_to_cart_opc_variable' !== $_POST['action'] ) && 'ign_one_page_searchable_add' !== $_POST['action'] )
		//	return $allowed;

		return true;

	}

	// Always allow backorders because it's an admin or shop manager adding the order
	public function sold_individually( $sold_individually, $product ) {

		//if ( !isset( $_POST['action'] ) || empty( $_POST['opc_checkout_flag'] ) )
		//	return $sold_individually;

		//if ( !isset( $_POST['action'] ) || ( 'woocommerce_add_to_cart_opc' != $_POST['action'] && 'woocommerce_add_to_cart_opc_variable' !== $_POST['action'] ) && 'ign_one_page_searchable_add' !== $_POST['action'] )
		//	return $sold_individually;

		return false;

	}


	function add_dummy_gateway( $methods ) {
		$methods = $this->invoice_gateway( $methods );
		$methods = $this->hold_order_gateway( $methods );
		return $methods;
	}

	function hold_order_gateway( $methods = array() ) {
		global $typenow;

		// ------------- The "Place order on hold" gatway: ---------------

		// Gotta let it load when viewing orders so the method title appears correctly in the order
		if ( 'shop_order' == $typenow || ( is_admin() && ( isset( $_GET['page'] ) && 'wc-settings' == $_GET['page'] ) ) ) {
			require_once( dirname( __FILE__ ) . '/woocommerce-one-page-checkout-hold-order-gateway.php' );

			$methods['wc_gateway_opc_hold_order'] = 'WC_Gateway_OPC_Hold_Order';
			return $methods;
		}

		// For the admin area, so plugins such as role-based methods can pickup the gateway for adjusting permission settings
		if ( is_admin() && !defined( 'DOING_AJAX' ) ) {
			require_once( dirname( __FILE__ ) . '/woocommerce-one-page-checkout-hold-order-gateway.php' );

			$methods['wc_gateway_opc_hold_order'] = 'WC_Gateway_OPC_Hold_Order';
			return $methods;
		}
		
		if ( is_admin() && defined( 'DOING_AJAX' ) ) {
			if ( !$this->has_access() )
				return $methods;
		}

		if ( isset( $_POST['opc_checkout_flag'] ) ) {
			// Do nothing
		} else if ( isset( $_POST['post_data'] ) ) {
			// Check for the checkout flag, if exists allow the custom gateway when enabled
			$args = wp_parse_args( $_POST['post_data'] );

			if ( empty( $args ) || empty( $args['opc_checkout_flag'] ) )
				return $methods;

		} else if ( isset( $_POST['payment_method'] ) && 'phone_manual_on_hold' == $_POST['payment_method'] ) {

			// do nothing, just a placeholder to allow the gateway to work since if that var is set the permission already passed.
			// this happens when the place order button is clicked since our custom checkout flag var isn't set at that point.

		} else {
			// No post data and no flag then don't allow the custom gateway
			return $methods;
		}

		require_once( dirname( __FILE__ ) . '/woocommerce-one-page-checkout-hold-order-gateway.php' );

		$methods['wc_gateway_opc_hold_order'] = 'WC_Gateway_OPC_Hold_Order';

		return $methods;

	}

	function invoice_gateway( $methods = array() ) {
		global $typenow, $wp;

		// Gotta let it load when viewing orders so the method title appears correctly in the order
		if ( 'shop_order' == $typenow || ( is_admin() && ( isset( $_GET['page'] ) && 'wc-settings' == $_GET['page'] ) ) ) {
			require_once( dirname( __FILE__ ) . '/woocommerce-one-page-checkout-dummy-gateway.php' );
			require_once( dirname( __FILE__ ) . '/woocommerce-one-page-checkout-paid-in-person-gateway.php' );
			
			$methods['wc_gateway_opc_dummy'] = 'WC_Gateway_OPC_Dummy';
			$methods['wc_gateway_opc_paid_in_person'] = 'WC_Gateway_OPC_Paid_In_Person';
			return $methods;
		}

		// For the admin area, so plugins such as role-based methods can pickup the gateway for adjusting permission settings
		if ( is_admin() && !defined( 'DOING_AJAX' ) ) {
			require_once( dirname( __FILE__ ) . '/woocommerce-one-page-checkout-dummy-gateway.php' );
			require_once( dirname( __FILE__ ) . '/woocommerce-one-page-checkout-paid-in-person-gateway.php' );
			
			$methods['wc_gateway_opc_dummy'] = 'WC_Gateway_OPC_Dummy';
			$methods['wc_gateway_opc_paid_in_person'] = 'WC_Gateway_OPC_Paid_In_Person';
			return $methods;
		}
		
		if ( is_admin() && defined( 'DOING_AJAX' ) ) {
			if ( !$this->has_access() )
				return $methods;
		}

		// Check for the checkout flag, if exists allow the custom gateway when enabled
		if ( isset( $_POST['opc_checkout_flag'] ) ) {
			// Do nothing
		} else if ( isset( $_POST['post_data'] ) ) {

			$args = wp_parse_args( $_POST['post_data'] );

			if ( empty( $args ) || empty( $args['opc_checkout_flag'] ) )
				return $methods;

		} else if ( isset( $_POST['payment_method'] ) && 'phone_manual_dummy' == $_POST['payment_method'] ) {

			// do nothing, just a placeholder to allow the gateway to work since if that var is set the permission already passed.
			// this happens when the place order button is clicked since our custom checkout flag var isn't set at that point.

		// If we're on the Thank You page for an order, might need to display info and/or send an invoice automatically
		// so let the gateways load.
		} else if ( isset( $wp->query_vars['order-received'] ) ) {
			require_once( dirname( __FILE__ ) . '/woocommerce-one-page-checkout-dummy-gateway.php' );
			require_once( dirname( __FILE__ ) . '/woocommerce-one-page-checkout-paid-in-person-gateway.php' );

			$methods['wc_gateway_opc_dummy'] = 'WC_Gateway_OPC_Dummy';
			$methods['wc_gateway_opc_paid_in_person'] = 'WC_Gateway_OPC_Paid_In_Person';
		} else {
			// No post data and no flag then don't allow the custom gateway
			return $methods;
		}

		require_once( dirname( __FILE__ ) . '/woocommerce-one-page-checkout-dummy-gateway.php' );
		require_once( dirname( __FILE__ ) . '/woocommerce-one-page-checkout-paid-in-person-gateway.php' );

		$methods['wc_gateway_opc_dummy'] = 'WC_Gateway_OPC_Dummy';
		$methods['wc_gateway_opc_paid_in_person'] = 'WC_Gateway_OPC_Paid_In_Person';

		return $methods;

	}


	function add_shipping_method( $methods = array() ) {
		global $typenow;

		// Gotta let it load when viewing orders so the method title appears correctly in the order
		if ( 'shop_order' == $typenow || ( is_admin() && ( isset( $_GET['page'] ) && 'wc-settings' == $_GET['page'] ) ) ) {

			require_once( dirname( __FILE__ ) . '/woocommerce-one-page-checkout-dummy-shipping-method.php' );

			$methods['phone_manual_dummy_shipping'] = 'WC_Shipping_OPC_Dummy_Gateway';

			return $methods;
		}

		$this->switch_user_context_to_customer( 'opc_user' );
		
		if ( is_admin() && defined( 'DOING_AJAX' ) ) {
			if ( !$this->has_access() )
				return $methods;
		}

		//$this->switch_user_context_to_customer( 'customer' );
		
		if ( isset( $_POST['opc_checkout_flag'] ) ) {

		} else if ( isset( $_POST['post_data'] ) ) {

			$args = wp_parse_args( $_POST['post_data'] );

			if ( empty( $args ) || empty( $args['opc_checkout_flag'] ) )
				return $methods;

		}

		require_once( dirname( __FILE__ ) . '/woocommerce-one-page-checkout-dummy-shipping-method.php' );

		$methods['phone_manual_dummy_shipping'] = 'WC_Shipping_OPC_Dummy_Gateway';

		return $methods;
	}
}


global $ign_opc;
$ign_opc = new IGN_Manual_Phone_Orders();












































































if ( ! function_exists( 'ignitewoo_queue_update' ) )
	require_once( dirname( __FILE__ ) . '/ignitewoo_updater/ignitewoo_update_api.php' );

$this_plugin_base = plugin_basename( __FILE__ );

add_action( "after_plugin_row_" . $this_plugin_base, 'ignite_plugin_update_row', 1, 2 );

ignitewoo_queue_update( plugin_basename( __FILE__ ), '2a0aca041ccc02ea8b50ffcac00b9583', '22611' );
