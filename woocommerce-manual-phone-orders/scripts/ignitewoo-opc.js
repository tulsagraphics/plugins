/* 
 *
 * Copyright (c) 2014 - IgniteWoo.com - All Rights Reserved 
 *
 * Portions Copyright (c) 2014 - WooThemes
 * 
 */ 

// Clears the entire cart
function hook_clear_entire_cart() { 
	setTimeout( function() { 
		jQuery( '.clear_entire_cart' ).unbind().on( 'click', function(e) {
			e.preventDefault();
			jQuery( this ).blur();
			if ( !confirm( opc_enhanced_select_params.confirm ) )
				return false; 
			jQuery.post( wc_checkout_params.ajax_url, { action: 'ign_opc_clear_cart' }, function( response ) { 
				jQuery( '#opc_searchable_layout').html( response );
				opc_update_checkout();
			});
		});
	}, 1500 );
}

function hook_update_the_cart() {
	jQuery( '.update_the_cart_totals_cart' ).unbind().on( 'click', function( e ) { 
		e.preventDefault();
		e.stopPropagation();
		
		jQuery( '.cart_contents_buttons input[type="button"], .cart_contents_buttons button' ).attr( 'disabled', 'disabled' );
		
		var items = [];
		var x = 0;
		var $thisbutton;
		
		jQuery( '.one_page_checkout_form tr' ).find( 'td input.qty' ).each( function() { 
			
			//if ( 0 == x )
				ign_opc_empty_cart = true;
			//else
			//	ign_opc_empty_cart = false;
			
			x++;
			
			$thisbutton = jQuery( this ).parent().find( '.qty' );
			
			$qty = $thisbutton.val();

			var customer_id = jQuery( 'input[name="ignitewoo_one_page_checkout_customer_id"]' ).val();
			
			if ( '' !== $thisbutton.attr( 'data-variation_id' ) && '0' !== $thisbutton.attr( 'data-variation_id' ) ) {
				
				var price = $thisbutton.closest( 'tr' ).find( '.opc_changeable_price' ).val();
				
				if ( 'undefined' == typeof price || '' == price )
					price = $thisbutton.closest( 'tr' ).find( '.opc_price' ).val();

				var data = {
					product_id: $thisbutton.attr( 'data-product_id' ),
					variation_id: $thisbutton.attr( 'data-variation_id' ),
					variation_data: $thisbutton.data( 'attributes' ),
					qty: $qty,
					price: price,
					adjust_qty: true,
					ignitewoo_one_page_checkout_customer_id: customer_id,
					empty_cart: ign_opc_empty_cart
				};

			} else { 

				var price = $thisbutton.closest( 'tr' ).find( '.opc_changeable_price' ).val();
				
				if ( 'undefined' == typeof price || '' == price )
					price = $thisbutton.closest( 'tr' ).find( '.opc_price' ).val();

				var data = {
					product_id: $thisbutton.attr( 'data-product_id' ),
					qty: $qty,
					adjust_qty: true,
					price: price,
					ignitewoo_one_page_checkout_customer_id: customer_id,
					empty_cart: ign_opc_empty_cart
				};
			}
			
			items.push( data );
		});
		
		// Wait for items, then trigger Ajax, when Ajax is done unblock the buttons
		jQuery.when( items ).then( function() { 
			// Call this function to get everything into the cart, because it's already coded to do that 
			// even if it expects different $thisbutton object, no big deal.
			ign_opc_do_ajax( $thisbutton, { action: 'woocommerce_update_the_cart_opc', items: items } );
		}).then( function() { 
			jQuery( '.cart_contents_buttons input[type="button"], .cart_contents_buttons button' ).removeAttr( 'disabled'  );	
		});
	});
}

jQuery( document ).ready( function($) { 

	function trigger_price_change() { 
		if ( 'undefined' == typeof item_changing_price || 0 == item_changing_price.length ) 
			return; 
			
		item_changing_price.closest('tr').find( '.qty' ).trigger('change');
	}

	hook_clear_entire_cart();
	hook_update_the_cart();
	
	// ensure the object exists
	if ( typeof wc_add_to_cart_params === 'undefined' )
		return false;

	// default = empty cart ever time AJAX runs
	if ( typeof ign_opc_empty_cart === 'undefined' )
		ign_opc_empty_cart = true;

	opc_atc = '';
	
	var opc_key_delay = ( function() {
		var timer = 0;
		return function( callback, ms ){
			clearTimeout ( timer );
			timer = setTimeout( callback, ms );
		};
	})();

	/*
	$( '#pp_added' ).prettyPhoto({
		theme: 'pp_woocommerce',
		default_height: 150,
		social_tools: false,
		opacity: 0.8,
		horizontal_padding: 10,
		show_title: false,
		allow_resize: false,
		allow_expand: false,
		overlay_gallery: false,
	});
	*/
	
	// Single product layout stuff. Delayed so that other scripts can fire up first because we want to unhook stuff from the add to cart button
	setTimeout( function() { 
		
		// unhook any JS  attached to this button
		jQuery( 'div.one_page_checkout_page div.summary:not(.donation_wrap) .single_add_to_cart_button' ).unbind(); 
		
		jQuery( document ).on( 'click', 'div.summary:not(.donation_wrap) .single_add_to_cart_button', function( e ) {

			e.preventDefault();
			
			var $thisbutton = $( this );
			
			var thiswrap = $( this ).closest( '.opc_single' );
			
			var $qty = thiswrap.find( '.qty' ).val();

			if ( '' == $qty || NaN == $qty )
				return;

			if ( thiswrap.hasClass( 'product-type-variable' ) ) {

				var pid = thiswrap.find( 'form.cart' ).find( 'input[name="add-to-cart"]' ).val();
				
				if ( pid.length <= 0 || null == pid || NaN == pid )
					return;
				
				var vid = thiswrap.find( 'form.cart' ).find( 'input[name="variation_id"]' ).val();
				
				if ( vid.length <= 0 || null == vid || NaN == vid )
					return;

				var data = {
					action: 'woocommerce_add_to_cart_opc_variable',
					product_id: pid,
					args: thiswrap.find( 'form.cart' ).serialize(),
					adjust_qty: true,
					empty_cart: false
				};

			} else if ( thiswrap.hasClass( 'product-type-simple' ) ) {
				
				var pid = thiswrap.find( 'form.cart' ).find( 'input[name="add-to-cart"]' ).val();
				
				if ( pid.length <= 0 || null == pid || NaN == pid )
					return;
				
				var gift_cert_input = thiswrap.find( 'input[name="gcp"]' );
				
				if ( gift_cert_input.length >= 0 ) { 
				
					var data = {
						action: 'woocommerce_add_to_cart_opc_gift_cert',
						product_id: pid,
						qty: $qty,
						gcp: gift_cert_input.val(),
						empty_cart: ign_opc_empty_cart
					};
				
				} else { 
					
					var data = {
						action: 'woocommerce_add_to_cart_opc',
						product_id: pid,
						qty: $qty,
						adjust_qty: true,
						empty_cart: false,
					};
				}
				
			} else if ( thiswrap.hasClass( 'product-type-grouped' ) ) {

				var pid = thiswrap.find( 'form.cart' ).find( 'input[name="add-to-cart"]' ).val();
				
				if ( pid.length <= 0 || null == pid || NaN == pid )
					return;
				
				var data = {
					action: 'woocommerce_add_to_cart_opc',
					type: 'grouped',
					product_id: pid,
					args: thiswrap.find( 'form.cart' ).serialize(),
					adjust_qty: true,
					empty_cart: false
				};

			}
			
			// IgniteWoo Product Add-on Forms support, send then entire set of form vars
			var form_vars = $( this ).closest( 'form.cart' ).serializeArray();
			
			$( form_vars ).each( function() {
				if ( 'vfb-' == this.name.substr( 0, 4 ) )
					data[ this.name ] = this.value;
			})

			ign_opc_do_ajax( $thisbutton, data )
			
		})
		
	}, 750 );
	
	$( '.opc_load_order' ).on( 'click', function(e) { 
		e.preventDefault();
		
		var oid = $( '#order_search' ).val();
		if ( null == oid || oid.length < 1 ) {
			alert( opc_enhanced_select_params.select_order );
			return; 
		}
		
		var n = $( '#_wpnonce' ).val();
		if ( null == n || n.length < 1 ) {
			alert( opc_enhanced_select_params.security_error );
			return; 
		}
		
		var overwrite_order = $( '.one_page_load_order_id_as' ).is( ':checked' );

		if ( overwrite_order && !confirm( opc_enhanced_select_params.load_order_confirm ) ) 
			return false;
		
		$.post( wc_add_to_cart_params.ajax_url, { action: 'opc_load_order', one_page_load_order_id: oid, _wpnonce: n, overwrite_order: overwrite_order }, function( data ) {
			if ( null == data || data.length < 0 || '-1' == data ) { 
				alert( opc_enhanced_select_params.security_error );
				return; 
			}

			try { 

				var res = $.parseJSON( data );
			} catch( err ) { 
				alert( 'Error parsing data' );
				return;

			}	

			if ( 'true' == res.success || true === res.success ) { 

				// Autopopulate the customer info 
				if ( 'undefined' !== typeof res.customer_id && 0 !== res.customer_id ) {
					// Support for SelectWoo - select an option by adding it first
					// https://select2.org/programmatic-control/add-select-clear-items
					if ( 'true' == wc_32_or_newer || true == wc_32_or_newer ) { 

						var option = new Option( 'xxxx', res.customer_id, true, true);
						$( '.opc-customer-search' ).append(option).trigger('change');

					} else { 
						$( '.opc-customer-search' ).select2('val', res.customer_id, true );
					}
					$( '#one_page_searchable_customer_search' ).submit();
				} else { 
					// Manually populate data - guest user
					if ( '' !== res.billing ) { 
						$.each( res.billing, function( k, v ) { 
							if ( 'state' == k || 'country' == k ) {
								$( '#billing_' + k + ' option[value=' + v + ']').attr('selected','selected');
								// update the select2 box
								// $( '#billing_' + k ).select2('data',{id:v,text:v}).trigger('change');
							} else { 
								$( '#billing_' + k ).val( v );
							}
						});
						$.each( res.billing, function( k, v ) { 
							if ( 'country' == k ) {
								if ( opc_enhanced_select_params.wc_version_gte_27 ) 
									$( '#billing_' + k ).val( v ).trigger('change');
								else 
									$( '#billing_' + k ).select2('data',{id:v,text:v}).trigger('change');
							}
						});
						$.each( res.billing, function( k, v ) { 
							if ( 'state' == k ) {
								if ( opc_enhanced_select_params.wc_version_gte_27 ) 
									$( '#billing_' + k ).val( v ).trigger('change');
								else 
									$( '#billing_' + k ).select2('data',{id:v,text:v}).trigger('change');
							}
						});
					}
					if ( '' !== res.shipping ) { 
						$.each( res.shipping, function( k, v ) { 
							if ( 'state' == k || 'country' == k ) {
								$( '#shipping_' + k + ' option[value=' + v + ']').attr('selected','selected');
								// update the select2 box
								$( '#shipping_' + k ).select2('data',{id:v,text:v}).trigger('change');
							} else { 
								$( '#shipping_' + k ).val( v );
							}
						})
					}
				}
				
				// Inject the order ID into the OPC form
				$( 'form.checkout' ).prepend( '<input type="hidden" name="opc_reloaded_order" value="' + oid + '">' );
				// Update WC order review
				opc_update_checkout();
				
				// Update OPC items table
				$.post( wc_checkout_params.ajax_url, { action: 'ign_one_page_update_items_table', nonce: ign_one_page_checkout.add_products_nonce }, function( data ) { 
					$( '#opc_searchable_layout').html( data );
					hook_clear_entire_cart();
					hook_update_the_cart();
				});
			}
		});
	});
	
	
	// Ajax add to cart - List style
	$( document ).on( 'click', '.opc_product_item', function( e ) {
		
		e.preventDefault();
		
		// Don't let it run more than once for the same item
		if ( opc_atc ) 
			return;
			
		// AJAX add to cart request
		var $thisbutton = jQuery( this );
		
		opc_add_to_cart( $thisbutton );

		return false;

	});
	

	$( document ).on( 'click', '#opc_table_layout .plus, #opc_table_layout .minus, .one_page_product_list .plus, .one_page_product_list .minus', function( e ) {

		if ( 'yes' == opc_enhanced_select_params.disable_autorefresh )
			return;
		
		var $thisfield = jQuery( this ).parent().find( '.qty' );
		
		opc_key_delay( function() {
						
			// Don't let it run more than once for the same item
			if ( opc_atc ) 
				return;
			
			opc_add_to_cart( $thisfield );

			return false;
			
		}, 250 );
	})
		
	$( document ).on( 'keyup change', '#opc_table_layout .qty, .one_page_product_list .qty', function( e ) {

		if ( 'yes' == opc_enhanced_select_params.disable_autorefresh )
			return;
		
		e.preventDefault();

		var $thisfield = jQuery( this );

		opc_key_delay( function() {
						
			// Don't let it run more than once for the same item
			if ( opc_atc ) 
				return;

			opc_add_to_cart( $thisfield );

			return false;
			
		}, 1000 );
	});
	
	function opc_changeable_price() { 
		$( '.opc_changeable_price' ).unbind().on( 'keyup', function( e ) {

			e.preventDefault();
			
			item_changing_price = jQuery( this );

			if ( 'undefined' !== typeof item_changing_price && item_changing_price.length > 0 && item_changing_price.val().length > 0 ) {
				$( this ).closest( 'table' ).find( '.opc_price' ).val( item_changing_price.val() );
			} else {
				var p = $( this ).parent().find( '.opc_regular_price' ).val();
				$( this ).closest( 'table' ).find( '.opc_regular_price' ).val( p );
			}

			opc_key_delay( function() {
				trigger_price_change();
			}, 1000 );

		});
	}
	
	opc_changeable_price();
	one_page_remove_from_cart();
	
	/*
	// Donations
	$( '.opc_remove_from_cart' ).unbind().on( 'click', function( e ) { 

		e.preventDefault();
		e.stopPropagation();
		
		var item_row = $( this ).closest( 'tr' );
		
		var data = { 
			action: 'woocommerce_add_to_cart_opc_product_remove',
			product_id: $( this ).data('product_id' ),
			variation_id: $( this ).data('variation_id' ),
		}

		xhr = jQuery.ajax({
			type:		'POST',
			url:		wc_checkout_params.ajax_url,
			data:		data,
			success:	function( response ) {
	
				hook_clear_entire_cart();
				hook_update_the_cart();
						
				if ( response ) {
					
					try {

						if ( response.indexOf( '<!--WC_START-->' ) >= 0 )
							response = response.split( '<!--WC_START-->' )[1]; // Strip off before after WC_START

						if ( response.indexOf( '<!--WC_END-->' ) >= 0 )
							response = response.split( '<!--WC_END-->' )[0]; // Strip off anything after WC_END

						// Parse
						result = jQuery.parseJSON( response );

						if ( result.result === 'failure' || result.result == 'success' )
							throw 'Result failures';
						
						item_row.fadeOut( 500, function() { item_row.remove() } );
						
					}
					catch( err ) {

						if ( typeof result !== 'undefined' ) { 
								
							// Remove old errors
							jQuery( '.woocommerce-error, .woocommerce-message' ).remove();

							// Add new errors
							if ( result.messages ) {
								jQuery( '.woocommerce-checkout .woocommerce' ).prepend( result.messages );
							}
							
							// Scroll to top
							jQuery( 'html, body' ).animate({
								scrollTop: ( jQuery( '.woocommerce-checkout .woocommerce' ).offset().top - 100 )
							}, 1000 );

							//return;
							
						}
					}
					
					// Refresh mini-cart
					jQuery.post( wc_checkout_params.ajax_url, { action: 'get_refreshed_fragments' }, function( response ) { 

						try { 
							
							fragments = response.fragments;
							cart_hash = response.cart_hash;
						}
						catch( err ) {
							return;
						}
						
						fragments = response.fragments;
						cart_hash = response.cart_hash;
						// Block
						jQuery( '.shop_table.cart, .updating, .cart_totals' ).fadeTo( '400', '0.5' ).block({ message: null, overlayCSS: { background: 'transparent url(' + wc_add_to_cart_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.5 } } );
						if ( fragments ) {
							jQuery.each( fragments, function( key, value ) {
								jQuery( key ).addClass( 'updating' );
							});
							jQuery.each( fragments, function( key, value ) {
								jQuery( key ).replaceWith( value );
							});
						}
						// Unblock
						jQuery( '.widget_shopping_cart, .updating' ).stop( true ).css( 'opacity', '1' ).unblock();
					
						// Mini-cart
						jQuery( 'body' ).trigger( 'added_to_cart', [ fragments, cart_hash ] );
						
						// Update order review
						opc_update_checkout();
					})
				}
			}
		});
	});
	*/
	
	$( 'form.checkout').find( '.ignitewoo_one_page_tax_exempt_field' ).remove();

	jQuery( '.ignitewoo_one_page_tax_exempt' ).on( 'click', function() {
		if ( $( this ).is( ':checked' ) )
			$( 'form.checkout').append( '<input class="ignitewoo_one_page_tax_exempt_field" type="hidden" name="ignitewoo_one_page_tax_exempt" value="yes">' );
		else 
			$( 'form.checkout').find( '.ignitewoo_one_page_tax_exempt_field' ).remove();
		
		opc_update_checkout();
	})
	
	jQuery( 'body' ).bind( 'opc_update_checkout', function() {
		//clearTimeout( updateTimer );
		opc_update_checkout();
	});
	
	jQuery( '.one_page_product_list_item' ).on( 'click', function() { 
		jQuery( this ).find( 'input' ).attr( 'checked', 'checked' );
		
		opc_add_to_cart( jQuery( this ).find( '.opc_product_item' ) );

	});
	
	function opcEnhancedSelectFormatString() {
		var formatString = {
			formatMatches: function( matches ) {
				if ( 1 === matches ) {
					return opc_enhanced_select_params.i18n_matches_1;
				}

				return opc_enhanced_select_params.i18n_matches_n.replace( '%qty%', matches );
			},
			formatNoMatches: function() {
				return opc_enhanced_select_params.i18n_no_matches;
			},
			formatAjaxError: function() {
				return opc_enhanced_select_params.i18n_ajax_error;
			},
			formatInputTooShort: function( input, min ) {
				var number = min - input.length;

				if ( 1 === number ) {
					return opc_enhanced_select_params.i18n_input_too_short_1;
				}

				return opc_enhanced_select_params.i18n_input_too_short_n.replace( '%qty%', number );
			},
			formatInputTooLong: function( input, max ) {
				var number = input.length - max;

				if ( 1 === number ) {
					return opc_enhanced_select_params.i18n_input_too_long_1;
				}

				return opc_enhanced_select_params.i18n_input_too_long_n.replace( '%qty%', number );
			},
			formatSelectionTooBig: function( limit ) {
				if ( 1 === limit ) {
					return opc_enhanced_select_params.i18n_selection_too_long_1;
				}

				return opc_enhanced_select_params.i18n_selection_too_long_n.replace( '%qty%', limit );
			},
			formatLoadMore: function() {
				return opc_enhanced_select_params.i18n_load_more;
			},
			formatSearching: function() {
				return opc_enhanced_select_params.i18n_searching;
			}
		};

		return formatString;
	}
	
	// Ajax configure product button
	$( 'input.one_page_configure_product' ).on( 'click', function() { 
		
		$( '.ign_opc_open_popup_link' ).remove();
	
		jQuery( '.one_page_checkout_page' ).block({ message: null, overlayCSS: { background: 'transparent url(' + wc_add_to_cart_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.5 } } );
		
		if ( 'true' == opc_enhanced_select_params.wc_version_gte_27 )
			selector = 'select';
		else 
			selector = ':input';
		
		var items = $( selector + ".opc-product-search" ).val();
		
		$.post( wc_checkout_params.ajax_url, { action: 'ign_one_page_configure_product', nonce: ign_one_page_checkout.add_products_nonce, items: items }, function( data ) { 
			if ( null == data || data.length < 1 ) {
				alert( 'Error getting product' );
				jQuery( '.one_page_checkout_page' ).css( 'opacity', '1' ).unblock();
				return; 
			}
			
			try { 
				var j = $.parseJSON( data ); 
			} catch(err) {
				alert( 'Error parsing data.' );
				jQuery( '.one_page_checkout_page' ).css( 'opacity', '1' ).unblock();
				return;
			}
			
			if ( false === j.view || j.view.length < 1 ) { 
				alert( 'Error parsing data' );
				jQuery( '.one_page_checkout_page' ).css( 'opacity', '1' ).unblock();
				return;
			}
			
			$( '.one_page_checkout_page' ).append( j.view );
			
			var customer_id = $( 'input[name="ignitewoo_one_page_checkout_customer_id"]' ).val();
			
			//$( '#ign_configured_view .cart' ).prepend( '<input type="hidden" name="one_page_customer_user" value="' + customer_id + '">' );
			
			$('.one_page_checkout_page .ign_opc_open_popup_link').prettyPhoto({
				hook: 'data-rel',
				social_tools: false,
				theme: 'pp_woocommerce',
				horizontal_padding: 20,
				opacity: 0.8,
				deeplinking: false,
				callback: function() { 
					jQuery( '.ign_opc_open_popup_link' ).remove();
					setTimeout( function() {
						opc_update_checkout();
						$.post( wc_checkout_params.ajax_url, { action: 'ign_one_page_update_items_table', nonce: ign_one_page_checkout.add_products_nonce, ignitewoo_one_page_checkout_customer_id: customer_id }, function( data ) { 
							$( '#opc_searchable_layout').html( data );
							hook_clear_entire_cart();
							hook_update_the_cart();
							opc_changeable_price();
							$( 'input.opc-product-search').select2( 'val', '');
							$( 'select.opc-product-search option').each( function() { 
								$( this ).removeAttr( 'selected' );
							})
							$( 'select.opc-product-search').trigger( 'change' );
						});
					}, 100 );
				},
				markup: '<div class="pp_pic_holder" id="ign_opc_pf_wrap"> \
						<div class="ppt">&nbsp;</div> \
						<div class="pp_top"> \
							<div class="pp_left"></div> \
							<div class="pp_middle"></div> \
							<div class="pp_right"></div> \
						</div> \
						<div class="pp_content_container"> \
							<div class="pp_left"> \
							<div class="pp_right"> \
								<p class="pp_description"></p> \
								<div class="pp_content"> \
									<div class="pp_loaderIcon"></div> \
									<div class="pp_fade"> \
										<a href="#" class="pp_expand" title="Expand the image">Expand</a> \
										<div class="pp_hoverContainer"> \
											<a class="pp_next" href="#">next</a> \
											<a class="pp_previous" href="#">previous</a> \
										</div> \
										<div id="pp_full_res"></div> \
										<div class="pp_details"> \
											<div class="pp_nav"> \
												<a href="#" class="pp_arrow_previous">Previous</a> \
												<p class="currentTextHolder">0/0</p> \
												<a href="#" class="pp_arrow_next">Next</a> \
											</div> \
											{pp_social} \
											<a class="pp_close" href="#">Close</a> \
										</div> \
									</div> \
								</div> \
							</div> \
							</div> \
						</div> \
						<div class="pp_bottom"> \
							<div class="pp_left"></div> \
							<div class="pp_middle"></div> \
							<div class="pp_right"></div> \
						</div> \
					</div> \
					<div class="pp_overlay"></div>',
			});
		
			jQuery( '.one_page_checkout_page' ).css( 'opacity', '1' ).unblock();
			
			$( '.one_page_checkout_page .ign_opc_open_popup_link' ).trigger( 'click' );
			
		})
	})

	$( '.one_page_searchable_add_btn' ).click( function( e ) { 
		e.preventDefault();
		
		if ( 'true' == opc_enhanced_select_params.wc_version_gte_27 )
			selector = 'select';
		else 
			selector = ':input';
	
		$( '.one_page_searchable_add_btn' ).hide();
		$( '.one_page_configure_product' ).hide();
		
		jQuery( '#one_page_searchable_customer_search' ).fadeTo( '400', '0.5' ).block({ message: null, overlayCSS: { background: 'transparent url(' + wc_add_to_cart_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.6 } } );

		var items = $( selector + ".opc-product-search" ).val();

		var customer_id = $( "input[name='ignitewoo_one_page_checkout_customer_id']" ).val()
		
		var post_data = 'ignitewoo_one_page_checkout_customer_id=' + customer_id;
		
		$( '.opc-search-cats' ).val( '-1' ).trigger( 'change' );
		
		ign_opc_key_delay = ( function() {
			var timer = 0;
			return function( callback, ms ){
				clearTimeout ( timer );
				timer = setTimeout( callback, ms );
			};
		})();

		$.post( wc_checkout_params.ajax_url, { action: 'ign_one_page_searchable_add', nonce: ign_one_page_checkout.add_products_nonce, items: items, ignitewoo_one_page_checkout_customer_id: customer_id, post_data: post_data }, function( data ) { 
			
			$( '#opc_searchable_layout').html( data );
			hook_clear_entire_cart();
			hook_update_the_cart();
			$( 'input.opc-product-search').select2( 'val', '');
			$( 'select.opc-product-search option').each( function() { 
				$( this ).removeAttr( 'selected' );
			})
			$( 'select.opc-product-search').trigger( 'change' );
			

			jQuery( '#one_page_searchable_customer_search' ).fadeTo( '400', '1.0' ).unblock();
			
			opc_update_checkout();
			opc_changeable_price();
			one_page_remove_from_cart();
			
			jQuery( '.opc_changeable_price' ).on( 'keyup', function( e ) {

				e.preventDefault();
				
				item_changing_price = jQuery( this );

				ign_opc_key_delay( function() {

					trigger_price_change();
					
				}, 1000 );

			});
		});
	});
	
	if ( $( '.ajax_chosen_select_customer' ).length ) { 

		jQuery( 'select.ajax_chosen_select_customer' ).ajaxChosen(
			{
				method:         'GET',
				url:            ign_one_page_ajax_url,
				dataType:       'json',
				afterTypeDelay: 100,
				minTermLength:  2,
				data:           {
					action:   'woocommerce_json_search_customers',
					security: opc_enhanced_select_params.search_customers_nonce,
					opc: true 
				}
			}, function ( data ) {

				var terms = {};

				$.each( data, function ( i, val ) {
					terms[i] = val;
				});

				return terms;
			} 
		);
	}
		
	$( ':input.opc-product-search' ).unbind( 'change' ).on( 'change', function(e) { 
		var v = $( this ).val();

		if ( null == v || v.length < 1 ) {
			// hide configure button 
			$( '.one_page_configure_product' ).hide();
			// show add to order button 
			$( '.one_page_searchable_add_btn' ).hide();
			return; 
		}
		
		$( '.one_page_configure_product' ).show();
		$( '.one_page_searchable_add_btn' ).show();

	});
	
	var search_cats = $( '.opc-search-cats' )
	if ( 'undefined' !== search_cats && search_cats.length > 0 ) 
		search_cats.select2();
	
	if ( 'true' == opc_enhanced_select_params.wc_version_gte_27 )
		selector = 'select';
	else 
		selector = ':input';

	// Ajax product search box
	$( selector + '.opc-product-search' ).each( function() {
		
		if ( 'true' == opc_enhanced_select_params.wc_version_gte_27 ) { 
			var select2_args = {
				allowClear:  $( this ).data( 'allow_clear' ) ? true : false,
				placeholder: $( this ).data( 'placeholder' ),
				minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '3',
				escapeMarkup: function( m ) {
					return m;
				},
				ajax: {
					url: ign_one_page_ajax_url,
					dataType:    'json',
					quietMillis: 250,
					delay: 250,
					data: function( params ) {
						var customer_id = $( "input[name='ignitewoo_one_page_checkout_customer_id']" ).val();
						return {
							term:     params.term,
							action:   'opc_json_search_products_and_variations',
							security:  ign_one_page_search_products_nonce,
							exclude:  $( this ).data( 'exclude' ),
							ignitewoo_one_page_checkout_customer_id: customer_id,
							opc_search_cat: $( '.opc-search-cats' ).val()
						};
					},
					processResults: function( data ) {
						var terms = [];
						if ( data ) {
							$.each( data, function( id, text ) {
								terms.push({
									id: id,
									text: text
								});
							});
						}
						return {
							results: terms
						};
					},
					cache: true
				}
			};
			
			$( this ).select2( select2_args ).addClass( 'enhanced' );
			
		} else { 
			
			var select2_args = {
				allowClear: true,
				placeholder: $( this ).data( 'placeholder' ),
				minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '3',
				escapeMarkup: function( m ) {
					return m;
				},
				ajax: {
					url: ign_one_page_ajax_url,
					dataType:    'json',
					quietMillis: 250,
					delay: 250,
					allowClear: true,
					val: '',
					data: function( term ) {

						var customer_id = $( "input[name='ignitewoo_one_page_checkout_customer_id']" ).val();				
						return {
								term:     term,
								action:   'opc_json_search_products_and_variations',
								security:  ign_one_page_search_products_nonce,
								exclude:  $( this ).data( 'exclude' ),
								ignitewoo_one_page_checkout_customer_id: customer_id
						};
					},
					results: function( data ) {
						var terms = [];
						if ( data ) {
								$.each( data, function( id, text ) {
									terms.push( { id: id, text: text } );
								});
							}
						return {
						results: terms
					};
				},
				cache: true
				}
			};

			if ( $( this ).data( 'multiple' ) === true ) {
				select2_args.multiple = true;
				select2_args.initSelection = function( element, callback ) {
					var data     = $.parseJSON( element.attr( 'data-selected' ) );
					var selected = [];

					$( element.val().split( ',' ) ).each( function( i, val ) {
						selected.push({
							id: val,
							text: data[ val ]
						});
					});
					return callback( selected );
				};
				select2_args.formatSelection = function( data ) {
					return '<div class="selected-option" data-id="' + data.id + '">' + data.text + '</div>';
				};
			} else {
				select2_args.multiple = false;
				select2_args.initSelection = function( element, callback ) {
					var data = {
						id: element.val(),
						text: element.attr( 'data-selected' )
					};
					return callback( data );
				};
			}
		
			$( this ).removeAttr( 'data-selected' );
			$( this ).select2('destroy').select2( select2_args ).addClass( 'enhanced' );
		}
		
		//select2_args = $.extend( select2_args, opcEnhancedSelectFormatString() );
	});
	
	$( ':input.opc-customer-search' ).filter( ':not(.enhanced)' ).each( function() {

		if ( 'true' == opc_enhanced_select_params.wc_version_gte_27 ) { 
			var select2_args = {
				allowClear:  $( this ).data( 'allow_clear' ) ? true : false,
				placeholder: $( this ).data( 'placeholder' ),
				minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '3',
				delay: 250,
				quietMillis: 250,
				escapeMarkup: function( m ) {
					return m;
				},
				ajax: {
					url: ign_one_page_ajax_url,
					dataType:    'json',
					quietMillis: 250,
					delay: 250,
					data: function( params ) {
						return {
							term:     params.term,
							action:   'opc_json_search_customers',
							security: ign_one_page_search_customers_nonce,
							exclude:  $( this ).data( 'exclude' )
						};
					},
					processResults: function( data ) {
						var terms = [];
						if ( data ) {
							$.each( data, function( id, text ) {
								terms.push({
									id: id,
									text: text
								});
							});
						}
						return {
							results: terms
						};
					},
					cache: true
				}
			};
		} else { 
			var select2_args = {
				allowClear:  true,
				placeholder: $( this ).data( 'placeholder' ),
				minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '3',
				delay: 250,
				quietMillis: 250,
				escapeMarkup: function( m ) {
					return m;
				},
				ajax: {
					url: ign_one_page_ajax_url,
					dataType:    'json',
					quietMillis: 250,
					delay: 250,
					data: function( term ) {
						return {
								term:     term,
								action:   'opc_json_search_customers',
								security: ign_one_page_search_customers_nonce
						};
				},
				results: function( data ) {
					var terms = [];
					if ( data ) {
						$.each( data, function( id, text ) {
							terms.push({
								id: id,
								text: text
							});
						});
					}
					return { results: terms };
				},
				cache: true
				}
			};
			if ( $( this ).data( 'multiple' ) === true ) {
				/*
				select2_args.multiple = true;
				select2_args.initSelection = function( element, callback ) {
					var data     = $.parseJSON( element.attr( 'data-selected' ) );
					var selected = [];

					$( element.val().split( ',' ) ).each( function( i, val ) {
						selected.push({
							id: val,
							text: data[ val ]
						});
					});
					return callback( selected );
				};
				select2_args.formatSelection = function( data ) {
					return '<div class="selected-option" data-id="' + data.id + '">' + data.text + '</div>';
				};
				*/
			} else {
				select2_args.multiple = false;
				select2_args.initSelection = function( element, callback ) {
					var data = {
						id: element.val(),
						text: element.attr( 'data-selected' )
					};
					return callback( data );
				};
			}

			//select2_args = $.extend( select2_args, opcEnhancedSelectFormatString() );
		}
		
		$( this ).select2( select2_args ).addClass( 'enhanced' );
	});
	
	// Ajax order search box
	if ( 'true' == opc_enhanced_select_params.wc_version_gte_27 ) {
		$( 'select.opc-order-search' ).filter( ':not(.enhanced)' ).each( function() {
			
			var select2_args = {
				allowClear:  $( this ).data( 'allow_clear' ) ? true : false,
				placeholder: $( this ).data( 'placeholder' ),
				minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '2',
				escapeMarkup: function( m ) {
					return m;
				},
				ajax: {
					url: ign_one_page_ajax_url,
					dataType:    'json',
					delay:       250,
					data: function( params ) {
						return {
							term:     params.term,
							action:   'opc_json_search_orders',
							security:  ign_one_page_search_products_nonce,
							exclude:  $( this ).data( 'exclude' )
						};
					},
					processResults: function( data, params ) {
						var terms = [];
						if ( data ) {
							$.each( data, function( id, text ) {
								terms.push( { id: id, text: text } );
							});
						}
						return {
							results: terms
						};
					},
					cache: false
				}
			}
			$( this ).select2( select2_args ).addClass( 'enhanced' );
		})
		
	} else {
		$( ':input#order_search' ).val('').removeClass( 'enhanced' );
		$( ':input.opc-order-search' ).filter( ':not(.enhanced)' ).each( function() {
			var select2_args = {
				allowClear: true,
				placeholder: $( this ).data( 'placeholder' ),
				multiple: false,
				minimumInputLength: '2',
				initSelection: function( element, callback ) {
					var data = {
						id: element.val(),
						text: element.attr( 'data-selected' )
					};
					return callback( data );
				},
				escapeMarkup: function( m ) {
					return m;
				},
				ajax: {
					url: ign_one_page_ajax_url,
					dataType:    'json',
					quietMillis: 250,
					allowClear: true,
					val: '',
					data: function( term ) {

						return {
								term:     term,
								action:   'opc_json_search_orders',
								security:  ign_one_page_search_products_nonce,
								exclude:  $( this ).data( 'exclude' )
						};
					},
					results: function( data ) {
						var terms = [];
						if ( data ) {
								$.each( data, function( id, text ) {
									terms.push( { id: id, text: text } );
								});
							}
						return {
							results: terms
						};
					},
					cache: true
				}
			};
			//$( this ).removeAttr( 'data-selected' );
			//$( this ).select2( select2_args ).addClass( 'enhanced' );
		
			$( this ).select2( select2_args ).addClass( 'enhanced' );
		});
	}
	
	$( '.opc_arbitrary' ).on( 'click', function(e) {
		e.preventDefault();
		var options = {
			title: opc_enhanced_select_params.arbitrary_product_title,
			type: "html",
			htmlSelector: "#arbitrary_form_wrap",
			escapeKey: true,
			fadeInDuration: .300
		};
		$( '#arbitrary_form_wrap' ).IgniteWooSimplePopup( options );
		return false;
	});
	
	$( '.opc_arbitrary_cancel' ).on( 'click', function(e) {
		e.preventDefault();
		$( '.ign-simple-popup-content .close' ).trigger( 'click' );
		//$( '#arbitrary_form_wrap' ).hide( 300 );
		return false;
	});

	$( '.opc_arbitrary_submit' ).on( 'click', function(e) {
		e.preventDefault();
		
		$( '#ign-simple-popup .arbitrary_price, #ign-simple-popup .arbitrary_qty, #ign-simple-popup .arbitrary_title' ).removeClass( 'arbitrary_error' );
		
		var test = $( '#ign-simple-popup .arbitrary_title' ).val();
		var qty = $( '#ign-simple-popup .arbitrary_qty' ).val();
		var test3 = $( '#ign-simple-popup .arbitrary_price' ).val();
		var errors = false;
		
		if ( 'undefined' == typeof test || test.length <= 0 || '' == test ) {
			$( '#ign-simple-popup .arbitrary_title' ).addClass( 'arbitrary_error' );
			errors = true;
		}

		if ( 'undefined' == typeof qty || qty.length <= 0 || '' == qty ) { 
			$( '#ign-simple-popup .arbitrary_qty' ).addClass( 'arbitrary_error' );
			errors = true;
		}

		if ( 'undefined' == typeof test3 || test3.length <= 0 || '' == test3 || 'NaN' == parseFloat( test3 ) || parseFloat( test3 ) < 0 ) {
			$( '#ign-simple-popup .arbitrary_price' ).addClass( 'arbitrary_error' );
			errors = true;
		}
		
		if ( errors ) {
			return false;
		}
		
		$( '#ign-simple-popup .arbitrary_price, #ign-simple-popup .arbitrary_qty, #ign-simple-popup .arbitrary_title' ).removeClass( 'arbitrary_error' );
		
		// Add block and spinner
		jQuery( '#ign-simple-popup .arbitrary_form' ).fadeTo( '400', '0.3' ).block({ message: null, overlayCSS: { background: 'transparent url(' + wc_add_to_cart_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.5 } } );
		
		var data = $( '#ign-simple-popup .arbitrary_form' ).serialize();
		
		$.post( ign_one_page_ajax_url, { action: 'add_arbitrary_product', data: data }, function( result ) {
			// Unblock
			jQuery( '#ign-simple-popup .arbitrary_form' ).fadeTo( '10', '1' ).unblock();
			
			if ( 'done' !== result ) { 
				alert( result );
			} else {
				// close popup
				$( '.ign-simple-popup-content .close' ).trigger( 'click' );
				
				// Refresh the table of items in the order
				$.post( wc_checkout_params.ajax_url, { action: 'ign_one_page_update_items_table', nonce: ign_one_page_checkout.add_products_nonce }, function( data ) { 
					$( '#opc_searchable_layout').html( data );
					hook_clear_entire_cart();
					hook_update_the_cart();
					// refresh the order review
					opc_update_checkout();
				});
			}
			
		}).fail(function(jqXHR){
			//if ( 500 == jqXHR.status || 0 == jqXHR.status ) {
				// internal server error or something else
				jQuery( '#ign-simple-popup .arbitrary_form' ).fadeTo( '10', '1' ).unblock();
				alert( opc_enhanced_select_params.ajax_error_messsage + ' ' + jqXHR.status + ': ' + jqXHR.statusText );
			//}
		});
		
		return false;
	});
	
	$( '.opc_load_order_link' ).on( 'click', function(e) {
		e.preventDefault();
		$( this ).blur();
		$( '#one_page_load_order' ).toggle('fast');
		return false;
	})
	
	$( '.fee_submit' ).on( 'click', function(e) { 
		e.preventDefault();
		var label = $( '.fee_name' ).val();
		var label = label.replace(/[^A-Za-z0-9\s!?]/g,'');
		var label = label.trim();
		var amount = $( '.fee_amount' ).val();
		var taxable = $( '.fee_taxable' ).is( ':checked' );
		$.post( ign_one_page_ajax_url, { action: 'opc_add_fee', n: ign_one_page_checkout.add_fees_nonce, label: label, amount: amount, taxable: taxable }, function( data ) { 
			
			if ( !taxable ) {
				taxable = opc_enhanced_select_params.no_val;
				is_taxable = 'no';
			} else {
				taxable = opc_enhanced_select_params.yes_val;
				is_taxable = 'yes';
			}
	
			var replaced = false;
			
			$( '.fee_label' ).each( function() { 
				if ( $( this ).text() != label )
					return;
				
				$( this ).closest( 'tr' ).replaceWith( '<tr><td class="fee_action"><a href="#" class="opc_remove_fee" data-amount="' + amount + '" data-label="' + label + '">&times;</a></td><td class="fee_label">' + label + '</td><td>' + amount + '</td><td>' + taxable + '</td></tr>');
				
				replaced = true;
			})
			
			if ( replaced ) { 
				
				$( '.opc_fee_inputs' ).each( function() { 
					
					var fee_label = $( this ).find( '.opc_fee_label' ).val();

					if ( fee_label !== label )
						return;
					
					$( this ).remove();
					
					$( 'form.checkout').append( '<div class="opc_fee_inputs"><input type="hidden" class="opc_fee_item" name="opc_fee[]" value="' + amount + '"><input type="hidden" class="opc_fee_label" name="opc_fee_label[]" value="' + label + '"><input type="hidden" class="opc_fee_taxable" name="opc_fee_taxable[]" value="' + is_taxable + '"></div>' );	
				
				});
			}
			
			if ( !replaced ) { 
				
				$( '.opc_fee_table' ).append( '<tr><td class="fee_action"><a href="#" class="opc_remove_fee" data-amount="' + amount + '" data-label="' + label + '">&times;</a></td><td class="fee_label">' + label + '</td><td>' + amount + '</td><td>' + taxable + '</td></tr>' );
				
				$( 'form.checkout').append( '<div class="opc_fee_inputs"><input type="hidden" class="opc_fee_item" name="opc_fee[]" value="' + amount + '"><input type="hidden" class="opc_fee_label" name="opc_fee_label[]" value="' + label + '"><input type="hidden" class="opc_fee_taxable" name="opc_fee_taxable[]" value="' + is_taxable + '"></div>' );
			}
			
			$( '.opc_fee_wrap' ).show( 'fast' );
			
			$( '#fee_form input[type="text"]' ).each( function() { 
				$( this ).val('');
			})
			
			opc_remove_fee_hooks();
			
			opc_update_checkout();
		})
	})
	
	function opc_remove_fee_hooks() { 

		$( '.opc_remove_fee' ).unbind( 'click' ).on( 'click', function(e) { 
			e.preventDefault();

			var label = $( this ).data( 'label' );
			
			$( '.opc_fee_inputs' ).each( function() { 
				var fee_label = $( this ).find( '.opc_fee_label' ).val();

				if ( fee_label !== label )
					return;
				
				$( this ).remove();
			})
			
			$( '.opc_fee_table tr' ).each( function() { 
				var fee_label = $( this ).find( '.fee_label' ).text();
				if ( fee_label !== label )
					return;
				$( this ).remove();
			})
			
			if ( $( '.opc_fee_table tr' ).length <= 1 )
				$( '.opc_fee_wrap' ).hide();
				
			opc_update_checkout();
			
			return false;
		})
	}
});

jQuery( document ).ready( function($ ) { 
		
	jQuery("#wp-admin-bar-my-account .ab-sub-wrapper").css({ visibility:"hidden", display: "block" });

	if ( false == opc_enhanced_select_params.is_admin || 'undefined' == typeof( opc_enhanced_select_params.is_admin )  )
		return;

	jQuery('.chosen-container').width( 250 );
		
	jQuery( 'select.ajax_chosen_switch_to_user_search_users' ).ajaxChosen({
		method:         'GET',
		url:            ign_one_page_ajax_url,
		dataType:       'json',
		afterTypeDelay: 100,
		minTermLength:  2,
		data:           {
			action:   'woocommerce_json_login_as_customers_search',
			security: ign_one_page_search_customers_nonce
		}
	}, function ( data ) {

		var terms = {};

		$(".searched-switch-links").html('');

		$.each( data, function ( i, val ) {
			terms[val.id] = val.label;
			if ( val.id !== undefined ) {
				$(".searched-switch-links").append("<div data-id='"+val.id+"' data-link='"+val.link+"'></div>");
			}
		});

		return terms;
		
	}).on("change", function() {
		
			var user_id = $(this).val()

			if ( user_id != "") {
				var link = $(".searched-switch-links").find("div[data-id='"+user_id+"']").attr("data-link");
				$("#wp-admin-bar-search-users").append("<a class='sac-switch-link' href='"+link+"'>Switch</a>");
				window.location = link;
			}
			else {
				$(".switch-to-user-switch-button").remove(".sac-switch-link");
			}
	});
	
	jQuery("#wp-admin-bar-my-account .ab-sub-wrapper").css({ visibility:"", display: "" });

});

	
function one_page_remove_from_cart() { 
	xhr = false;
	
	jQuery( '.opc_remove_from_cart' ).unbind().on( 'click', function( e ) { 

		e.preventDefault();
		e.stopPropagation();
		
		if ( xhr ) {
			xhr.abort();
		}

		
		/*
		if ( jQuery( '.ajax_chosen_select_products_and_variations' ).is( 'select' ) ) { 
			
			var vals = jQuery( 'select.ajax_chosen_select_products_and_variations ' ).val();
			
		} else if ( jQuery( '.ajax_chosen_select_products_and_variations' ).is( 'input' ) ) { 
		
			var vals = jQuery( 'input.ajax_chosen_select_products_and_variations' ).val();
			
		}
		*/

		var vals = jQuery( 'input.opc-product-search' ).val();
		
		var data = { 
			action: 'woocommerce_add_to_cart_opc_product_remove',
			product_id: jQuery( this ).data('product_id' ),
			variation_id: jQuery( this ).data('variation_id' ),
			selected: vals,
		}

		var remove_item = jQuery( this ).closest( 'tr' );
		
		xhr = jQuery.ajax({
			type:		'POST',
			url:		wc_checkout_params.ajax_url,
			data:		data,
			success:	function( response ) {
				if ( response ) {
					try {

						if ( response.indexOf( '<!--WC_START-->' ) >= 0 )
							response = response.split( '<!--WC_START-->' )[1]; // Strip off before after WC_START

						if ( response.indexOf( '<!--WC_END-->' ) >= 0 )
							response = response.split( '<!--WC_END-->' )[0]; // Strip off anything after WC_END

						// Parse
						result = jQuery.parseJSON( response );

						if ( result.result === 'failure' || result.result == 'success' )
							throw 'Result failures';
		
						//remove_item.fadeOut( 300, function() { remove_item.remove(); });

						jQuery.post( wc_checkout_params.ajax_url, { action: 'ign_one_page_update_items_table', nonce: ign_one_page_checkout.add_products_nonce }, function( data ) { 
							jQuery( '#opc_searchable_layout').html( data );
							hook_clear_entire_cart();
							hook_update_the_cart();
						});
						
						opc_update_checkout();


						/* DEPRECATED
						jQuery( 'input.opc-product-search').find( "option" ).attr( "selected", false );
						
						// Remove dup options by value
						var seen = {};
						jQuery( 'select.ajax_chosen_select_products_and_variations').find( "option" ).each(function() {
							var txt = jQuery(this).val();
							if (seen[txt])
								jQuery(this).remove();
							else
								seen[txt] = true;
						});
						*/
						/*
						if ( typeof result.sel !== undefined ) {

							var $select = jQuery('input.opc-product-search');
							
							var pid_removed = result.sel.toString();

							var values = $select.select2( 'val' );
							
							var i = values.indexOf( pid_removed );


							if ( i >= 0 ) {
								values.splice( i, 1 );
								$select.select2( 'val', values );
							}

							
							//for( var s in result.sel ) { 
							//	jQuery( 'select.ajax_chosen_select_products_and_variations option[value="' + result.sel[s] + '"]' ).prop( 'selected', true );
							//};
							
							
						}
						*/
						
						//jQuery( 'select.ajax_chosen_select_products_and_variations').trigger( "chosen:updated" );
						
					}
					catch( err ) {

						if ( typeof result !== 'undefined' ) { 
							
							/*
							// Remove old errors
							jQuery( '.woocommerce-error, .woocommerce-message' ).remove();

							// Add new errors
							if ( result.messages ) {
								jQuery( '.woocommerce-checkout .woocommerce' ).prepend( result.messages );
							}
							
							// Scroll to top
							jQuery( 'html, body' ).animate({
								scrollTop: ( jQuery( '.woocommerce-checkout .woocommerce' ).offset().top - 100 )
							}, 1000 );

							//return;
							*/ 
							
							
							/*
							if ( jQuery( '.ajax_chosen_select_products_and_variations' ).is( 'select' ) ) { 
								
								jQuery( '.ajax_chosen_select_products_and_variations option[value="' + data.product_id + '"]' ).removeAttr( 'selected' );
								
							} else if ( jQuery( '.ajax_chosen_select_products_and_variations' ).is( 'input' ) ) { 
							
								var vals = jQuery( '.ajax_chosen_select_products_and_variations' ).val();
								
								vals = vals.split( ',' );
								
								var index = vals.indexOf( data.product_id );
							
								vals = vals.splice( index, 1 );
								
								vals = vals.join;
								
								jQuery( '.ajax_chosen_select_products_and_variations' ).val( vals )
								
							}
							*/
							
						}
					}
					
					// Refresh mini-cart
					jQuery.post( wc_checkout_params.ajax_url, { action: 'get_refreshed_fragments' }, function( response ) { 

						try { 
							
							fragments = response.fragments;
							cart_hash = response.cart_hash;
						}
						catch( err ) {
							return;
						}
						
						fragments = response.fragments;
						cart_hash = response.cart_hash;
						// Block
						jQuery( '.shop_table.cart, .updating, .cart_totals' ).fadeTo( '400', '0.5' ).block({ message: null, overlayCSS: { background: 'transparent url(' + wc_add_to_cart_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.5 } } );
						if ( fragments ) {
							jQuery.each( fragments, function( key, value ) {
								jQuery( key ).addClass( 'updating' );
							});
							jQuery.each( fragments, function( key, value ) {
								jQuery( key ).replaceWith( value );
							});
						}
						// Unblock
						jQuery( '.widget_shopping_cart, .updating' ).stop( true ).css( 'opacity', '1' ).unblock();
					
						// Mini-cart
						jQuery( 'body' ).trigger( 'added_to_cart', [ fragments, cart_hash ] );
						
						// Update order review
						opc_update_checkout();
						
						//ign_opc_key_delay( function() {

							trigger_price_change();
							
						//}, 1000 );
					})
				}
			}
		});
	})
}


function opc_add_to_cart( $thisbutton ) {

	if ( undefined == ign_opc_empty_cart || 'undefined' == ign_opc_empty_cart || null == ign_opc_empty_cart )
		ign_opc_empty_cart = false;

	var customer_id = jQuery( 'input[name="ignitewoo_one_page_checkout_customer_id"]' ).val();
	
	if ( $thisbutton.is( '.qty' ) ) { 

		$qty = $thisbutton.val();

		if ( '' !== $thisbutton.attr( 'data-variation_id' ) && '0' !== $thisbutton.attr( 'data-variation_id' ) ) {

			var data = {
				action: 'woocommerce_add_to_cart_opc_variable',
				product_id: $thisbutton.attr( 'data-product_id' ),
				variation_id: $thisbutton.attr( 'data-variation_id' ),
				variation_data: $thisbutton.data( 'attributes' ),
				cart_item_key: $thisbutton.attr( 'data-cart_item_key' ),
				qty: $qty,
				price: $thisbutton.closest( 'table' ).find( '.opc_price' ).val(),
				adjust_qty: true,
				ignitewoo_one_page_checkout_customer_id: customer_id,
				empty_cart: ign_opc_empty_cart
			};

		} else { 
			
			var data = {
				action: 'woocommerce_add_to_cart_opc',
				product_id: $thisbutton.attr( 'data-product_id' ),
				qty: $qty,
				adjust_qty: true,
				price: $thisbutton.closest( 'table' ).find( '.opc_price' ).val(),
				ignitewoo_one_page_checkout_customer_id: customer_id,
				empty_cart: ign_opc_empty_cart
			};

		}
		
		ign_opc_do_ajax( $thisbutton, data )
		
	} else if ( $thisbutton.is( '.opc_product_type_variable' ) ) {

		if ( ! $thisbutton.attr( 'data-product_id' ) )
			return true;
			
		if ( ! $thisbutton.attr( 'data-variation_id' ) )
			return true;
			
		if ( ! $thisbutton.attr( 'data-attributes' ) )
			return true;
			
		if ( ! $thisbutton.data( 'attributes' ) )
			return true;
			
		var $attribs = $thisbutton.data( 'attributes' );
		
		var $qty = $thisbutton.attr( 'data-quantity' );
					
		if ( !$qty )
			$qty = 1; 
		
		var data = {
			action: 'woocommerce_add_to_cart_opc_variable',
			product_id: $thisbutton.attr( 'data-product_id' ),
			variation_id: $thisbutton.attr( 'data-variation_id' ),
			variation_data: $attribs,
			cart_item_key: $thisbutton.attr( 'data-cart_item_key' ),
			qty: $qty,
			price: $thisbutton.closest( 'table' ).find( '.opc_price' ).val(),
			ignitewoo_one_page_checkout_customer_id: customer_id,
			empty_cart: ign_opc_empty_cart
		};

		ign_opc_do_ajax( $thisbutton, data )
		
	} else if ( $thisbutton.is( '.opc_product_type_simple' ) ) {

		if ( ! $thisbutton.attr( 'data-product_id' ) )
			return true;

		$thisbutton.removeClass( 'added' );
		$thisbutton.addClass( 'loading' );

		var $qty = $thisbutton.attr( 'data-quantity' );
					
		if ( !$qty )
			$qty = 1; 

		var data = {
			action: 'woocommerce_add_to_cart_opc',
			product_id: $thisbutton.attr( 'data-product_id' ),
			qty: $qty,
			price: $thisbutton.closest( 'table' ).find( '.opc_price' ).val(),
			ignitewoo_one_page_checkout_customer_id: customer_id,
			empty_cart: ign_opc_empty_cart
		};

		ign_opc_do_ajax( $thisbutton, data );
		
	}
	
}

opc_xhr = false;

function ign_opc_do_ajax( $thisbutton, data ) {

	// Check if a donation is being submitted, if so don't fire Ajax again until it is complete
	if ( opc_xhr ) { 
		opc_xhr.abort();
	}

	// Trigger event
	jQuery( 'body' ).trigger( 'adding_to_cart', [ $thisbutton, data ] );

	//opc_atc = $thisbutton.attr( 'data-product_id' );
	
	jQuery( $thisbutton.closest( '.one_page_product_list' ) ).block({ message: null, overlayCSS: { background: '#fff url(' + wc_checkout_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.5 } });

	// Ajax action
	opc_xhr = jQuery.post( wc_add_to_cart_params.ajax_url, data, function( response ) {

		//opc_atc = '';

		if ( !response ) { 
		
			$thisbutton.removeClass( 'loading' );
			
			jQuery( $thisbutton.closest( '.one_page_product_list' ) ).unblock();
		
			return;
		}
		

		try {

			if ( response.indexOf( '<!--WC_START-->' ) >= 0 )
				response = response.split( '<!--WC_START-->' )[1]; // Strip off before after WC_START

			if ( response.indexOf( '<!--WC_END-->' ) >= 0 )
				response = response.split( '<!--WC_END-->' )[0]; // Strip off anything after WC_END

			// Parse
			result = jQuery.parseJSON( response );

			if ( result.result === 'failure' || result.result !== 'success' )
				throw 'Result failure';
			
			
		}
		catch( err ) {

			if ( typeof result !== 'undefined' && 'failure' !== result.result ) { 
					
				// Remove old errors
				jQuery( '.woocommerce-error, .woocommerce-message' ).remove();

				// Add new errors
				if ( result.messages ) {
					jQuery( '.woocommerce-checkout .woocommerce' ).prepend( result.messages );
				}
				
				// Scroll to top
				jQuery( 'html, body' ).animate({
					scrollTop: ( jQuery( '.woocommerce-checkout .woocommerce' ).offset().top - 100 )
				}, 1000 );

				//return;

			}
		}

		var this_page = window.location.toString();

		this_page = this_page.replace( 'add-to-cart', 'added-to-cart' );

		$thisbutton.removeClass( 'loading' );
	
		// Refresh mini-cart
		jQuery.post( wc_checkout_params.ajax_url, { action: 'get_refreshed_fragments' }, function( response ) { 

			try { 
				
				fragments = response.fragments;
				cart_hash = response.cart_hash;
			}
			catch( err ) {
				return;
			}

			// Block fragments class
			if ( fragments ) {
				jQuery.each( fragments, function( key, value ) {
					jQuery( key ).addClass( 'updating' );
				});
			}

			// Block widgets and fragments
			jQuery( '.shop_table.cart, .updating, .cart_totals' ).fadeTo( '400', '0.5' ).block({ message: null, overlayCSS: { background: 'transparent url(' + wc_add_to_cart_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.5 } } );

			// Changes button classes
			$thisbutton.addClass( 'added' );

			// View cart text
			//if ( ! wc_add_to_cart_params.is_cart && $thisbutton.parent().find( '.added_to_cart' ).size() === 0 ) {
			//	$thisbutton.after( ' <a href="' + wc_add_to_cart_params.cart_url + '" class="added_to_cart wc-forward" title="' + 
			//		wc_add_to_cart_params.i18n_view_cart + '">' + wc_add_to_cart_params.i18n_view_cart + '</a>' );
			//}

			// Replace fragments
			if ( fragments ) {
				jQuery.each( fragments, function( key, value ) {
					jQuery( key ).replaceWith( value );
				});
			}
			
			// Trigger event so themes can refresh other areas
			jQuery( 'body' ).trigger( 'added_to_cart', [ fragments, cart_hash ] );
		});

		// Unblock
		jQuery( '.widget_shopping_cart, .updating' ).stop( true ).css( 'opacity', '1' ).unblock();

		// Cart page elements
		jQuery( '.shop_table.cart' ).load( this_page + ' .shop_table.cart:eq(0) > *', function() {

			jQuery( 'div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)' ).addClass( 'buttons_added' ).append( '<input type="button" value="+" id="add1" class="plus" />' ).prepend( '<input type="button" value="-" id="minus1" class="minus" />' );

			jQuery( '.shop_table.cart' ).stop( true ).css( 'opacity', '1' ).unblock();

			jQuery( 'body' ).trigger( 'cart_page_refreshed' );
		});

		jQuery( '.cart_totals' ).load( this_page + ' .cart_totals:eq(0) > *', function() {
			jQuery( '.cart_totals' ).stop( true ).css( 'opacity', '1' ).unblock();
		});

		jQuery( $thisbutton.closest( '.one_page_product_list' ) ).unblock();
		
		//jQuery( 'body' ).trigger('opc_update_checkout' );

		opc_update_checkout();
		
		one_page_remove_from_cart();
		
		/*
		item_field = $thisbutton.closest( 'li' );
		
		//item_field.css( 'border-color', 'rgba( 0, 152, 0, 1 )' );
		border_css = $thisbutton.closest( 'css', 'border-color' );
				
		jQuery( { alpha:1 } ).animate( { alpha:0 }, {
			duration: 250,
			step: function(){
				item_field.css( 'border-color','rgba( 0, 168, 0, ' + this.alpha + ')');
			},
			complete: function() {
				item_field.css( 'border-color', border_css );
			}
		});
		*/
	});
}

// Update cart totals
function opc_update_checkout() {

	//if ( xhr ) { 
	//	xhr.abort();
		
	//}

	/*
	jQuery( '#pp_added' ).trigger( 'click' );
	
	if ( typeof ign_cart_updated_notice_timeout == 'undefined' )
		ign_cart_updated_notice_timeout = 1000;
	
	setTimeout( function() { 
		jQuery.prettyPhoto.close( 'slow' );
	}, ign_cart_updated_notice_timeout );
	*/
	
	
	var shipping_methods = [];

	jQuery( 'select.shipping_method, input[name^=shipping_method][type=radio]:checked, input[name^=shipping_method][type=hidden]' ).each( function( index, input ) {
		shipping_methods[ jQuery( this ).data( 'index' ) ] = jQuery( this ).val();
	} );

	var payment_method = jQuery( '#order_review input[name=payment_method]:checked' ).val(),
		country			= jQuery( '#billing_country' ).val(),
		state			= jQuery( '#billing_state' ).val(),
		postcode		= jQuery( 'input#billing_postcode' ).val(),
		city			= jQuery( '#billing_city' ).val(),
		address			= jQuery( 'input#billing_address_1' ).val(),
		address_2		= jQuery( 'input#billing_address_2' ).val(),
		s_country,
		s_state,
		s_postcode,
		s_city,
		s_address,
		s_address_2;

	if ( jQuery( '#ship-to-different-address input' ).is( ':checked' ) || jQuery( '#ship-to-different-address input' ).size() === 0 ) {
		s_country		= jQuery( '#shipping_country' ).val();
		s_state			= jQuery( '#shipping_state' ).val();
		s_postcode		= jQuery( 'input#shipping_postcode' ).val();
		s_city			= jQuery( '#shipping_city' ).val();
		s_address		= jQuery( 'input#shipping_address_1' ).val();
		s_address_2		= jQuery( 'input#shipping_address_2' ).val();
	} else {
		s_country		= country;
		s_state			= state;
		s_postcode		= postcode;
		s_city			= city;
		s_address		= address;
		s_address_2		= address_2;
	}

	jQuery( '#order_methods, #order_review' ).block({ message: null, overlayCSS: { background: '#fff url(' + wc_checkout_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.5 } });

	var data = {
		action:					'woocommerce_update_order_review',
		security:					wc_checkout_params.update_order_review_nonce,
		shipping_method:				shipping_methods,
		payment_method:				payment_method,
		country:					country,
		state:					state,
		postcode:					postcode,
		city:						city,
		address:					address,
		address_2:					address_2,
		s_country:					s_country,
		s_state:					s_state,
		s_postcode:					s_postcode,
		s_city:					s_city,
		s_address:					s_address,
		s_address_2:				s_address_2,
		post_data:					jQuery( 'form.checkout' ).serialize()
	};

	if ( jQuery( '.ignitewoo_one_page_tax_exempt' ).is( ':checked' ) )
		data.post_data += '&ignitewoo_one_page_tax_exempt=yes';
	
	xhr = jQuery.ajax({
		type:		'POST',
		url:		wc_checkout_params.ajax_url,
		data:		data,
		success:	function( data ) {

			if ( data && data.fragments ) {
				jQuery.each( data.fragments, function ( key, value ) {
					jQuery( key ).replaceWith( value );
					jQuery( key ).unblock();
				} );
			}
			
			if ( data && data.fragments ) {
				
				var $form = jQuery( '.one_page_checkout_page' );
				
				if ( 'failure' == data.result ) {
					
					if ( 'true' === data.reload ) {
						window.location.reload();
						return;
					}

					jQuery( '.woocommerce-error, .woocommerce-message' ).remove();

					// Add new errors
					if ( data.messages ) {
						$form.prepend( data.messages );
					} else {
						$form.prepend( data );
					}

					// Lose focus for all fields
					$form.find( '.input-text, select' ).blur();

					// Scroll to top
					jQuery( 'html, body' ).animate( {
						scrollTop: ( jQuery( '.woocommerce-checkout' ).offset().top - 100 )
					}, 1000 );

				} else if ( 'success' == data.result && jQuery( data.messages ).length > 0 ) {

					
					// Add messages
					if ( data.messages ) {
						$form.prepend( data.messages );
					} else {
						$form.prepend( data );
					}

					// Lose focus for all fields
					$form.find( '.input-text, select' ).blur();

					// Scroll to top
					//jQuery( 'html, body' ).animate( {
					//	scrollTop: ( jQuery( '.woocommerce-checkout' ).offset().top - 100 )
					//}, 1000 );
					
				}
				
				//jQuery( '#order_review' ).html( jQuery.trim( data.fragments ) );

			
				
				jQuery( '.woocommerce-checkout-payment, .woocommerce-checkout-review-order, .woocommerce-checkout-review-order-table, #order_methods, #order_review' ).unblock();

				//jQuery( '#order_review' ).find( 'input[name=payment_method]:checked' ).trigger('click');
				jQuery( '.woocommerce-checkout' ).find( 'input[name=payment_method]:checked' ).triggerHandler( 'click' );
				
				jQuery( 'body' ).trigger('updated_checkout' );

			}
			
			if ( data && ( '2.2' == wc_version_info ) ) { 
			
				jQuery( '#order_review' ).html( jQuery.trim( data ) );
				jQuery( '#order_review' ).find( 'input[name=payment_method]:checked' ).trigger('click');
				jQuery( 'body' ).trigger('updated_checkout' );
				
			}

		}
	});
}


/**
 * Simple Popup for adding arbitrary products
 * 
 * Based on Simple-Popup by daankuijsten
 * 
 * MIT License
 * Copyright (c) 2016 - daankuijsten
 * Copyright (c) 2017 - IgniteWoo
 */
(function ($) {

	"use strict";

	$.fn.IgniteWooSimplePopup = function(options) {
		/**
		* Javascript this
		*/
		var that = this;

		/**
		* The data to be inserted in the popup
		*/
		var data;

		/**
		* Determined type, based on type option (because we have possible value "auto")
		*/
		var determinedType;

		/**
		* Different types are supported:
		*
		* "auto"   Will first try "data", then "html" and else it will fail.
		* "data"   Looks at current HTML "data-content" attribute for content
		* "html"   Needs a selector of an existing HTML tag
		*/
		var types = [
			"auto",
			"data",
			"html",
		];

		/**
		* Default values
		*/
		var settings = $.extend( {
			title: "",				  // Form title
			type: "auto",                   // Type to get content
			htmlSelector: null,             // HTML selector for popup content
			width: "600px",                 // Width popup
			height: "auto",                 // Height popup
			background: "#fff",             // Background popup
			backdrop: 0.7,                  // Backdrop opactity or falsy value
			backdropBackground: "#000",     // Backdrop background (any css here)
			inlineCss: true,                // Inject CSS via JS
			escapeKey: true,                // Close popup when "escape" is pressed"
			closeCross: true,               // Display a closing cross
			fadeInDuration: 0.3,            // The time to fade the popup in, in seconds
			fadeInTimingFunction: "ease",   // The timing function used to fade the popup in
			fadeOutDuration: 0.3,           // The time to fade the popup out, in seconds
			fadeOutimingFunction: "ease",   // The timing function used to fade the popup out
			beforeOpen: function(){},
			afterOpen: function(){},
			beforeClose: function(){},
			afterClose: function(){}
		}, options );

		/**
		* A selector string to filter the descendants of the selected elements that trigger the event.
		*/
		var selector = this.selector;

		/**
		* init
		*
		* Set the onclick event, determine type, validate the settings, set the data and start popup.
		*
		* @returns {this} jQuery object
		*/
		function init() {
			validateSettings();

			determinedType = determineType();
			data = setData();

			startPopup();

			return that;
		}

		/**
		* validateSettings
		*
		* Check for some settings if they are correct
		*
		* @returns {void}
		*/
		function validateSettings() {
			if (	settings.type !== "auto"
				&& settings.type !== "data"
				&& settings.type !== "html"
			) {
				throw new Error("simplePopup: Type must me \"auto\", \"data\" or \"html\"");
			}

			if (settings.backdrop > 1 || settings.backdrop < 0) {
				throw new Error("simplePopup: Please enter a \"backdrop\" value <= 1 of >= 0");
			}

			if (settings.fadeInDuration < 0 || Number(settings.fadeInDuration) !== settings.fadeInDuration) {
				throw new Error("simplePopup: Please enter a \"fadeInDuration\" number >= 0");
			}

			if (settings.fadeOutDuration < 0 || Number(settings.fadeOutDuration) !== settings.fadeOutDuration) {
				throw new Error("simplePopup: Please enter a \"fadeOutDuration\" number >= 0");
			}
		}

		/**
		* determineType
		*
		* Check what type we have (and with that where we need to look for the data)
		*
		* @returns {boolean|string} The type of the data or false
		*/
		function determineType() {
			// Type HTML
			if (settings.type === "html") {
			return "html";
			}

			// Type DATA
			if (settings.type === "data") {
			return "data";
			}

			// Type AUTO
			if (settings.type === "auto") {
			if(that.data("content")) {
				return "data";
			}

			if ($(settings.htmlSelector).length) {
				return "html";
			}

			throw new Error("simplePopup: could not determine type for \"type: auto\"");
			}

			return false;
		}

		/**
		* setData
		*
		* Set the data variable based on the type
		*
		* @returns {boolean|string} The HTML or text to disply in the popup or false
		*/
		function setData() {
			// Type HTML
			if (determinedType === "html") {
				if (!settings.htmlSelector) {
					throw new Error("simplePopup: for \"type: html\" the \"htmlSelector\" option must point to your popup html");
				}

				if (!$(settings.htmlSelector).length) {
					throw new Error("simplePopup: the \"htmlSelector\": \"" + settings.htmlSelector + "\" was not found");
				}

				// Clone with events
				return $(settings.htmlSelector).clone(true,true);
			}

			// Type DATA
			if (determinedType === "data") {
				data = that.data("content");

				if (!data) {
					throw new Error("simplePopup: for \"type: data\" the \"data-content\" attribute can not be empty");
				}

				return data;
			}

			return false;
		}

		/**
		* startPopup
		*
		* Insert popup HTML, maybe bind escape key and maybe start the backdrop
		*
		* @returns {void}
		*/
		function startPopup() {
			if (settings.backdrop) {
				startBackdrop();
			}

			if (settings.escapeKey) {
				bindEscape();
			}

			insertPopupHtml();
		}

		/**
		* insertPopupHtml
		*
		* Create the popup HTML and append it to the body. Maybe set the CSS.
		*
		* @returns {void}
		*/
		function insertPopupHtml() {
			
			var content = $("<div/>", {
			"class": "ign-simple-popup-content",
			"html": data
			});

			var html = $("<div/>", {
			"id": "ign-simple-popup",
			"class": "hide-it"
			});
						
			if (settings.inlineCss) {
				content.css("width", settings.width);
				content.css("height", settings.height);
				content.css("background", settings.background);
			}

			bindClickPopup(html);

			// When we have a closeCross, create the element, bind click close and append it to
			// the content
			if (settings.closeCross) {
				var closeButton = $("<div/>", {
					"class": "close",
					"html": "<div class='simple-popup-title'>" + settings.title + "</div>"
				});

				bindClickClose(closeButton);
				content.append(closeButton);
			}

			html.append(content);

			// Call the beforeOpen callback
			settings.beforeOpen(html);

			$("body").append(html);
			$( '.ign-simple-popup-content div:first-child' ).show();

			// Use a timeout, else poor CSS is to slow to see the difference
			setTimeout(function() {
				var html = $("#ign-simple-popup");

				// Set the fade in effect
				if (settings.inlineCss) {
					html = setFadeTimingFunction(html, settings.fadeInTimingFunction);
					html = setFadeDuration(html, settings.fadeInDuration);
				}

				html.removeClass("hide-it");
			});

			// Poll to check if the popup is faded in
			var intervalId = setInterval(function() {
				if ($("#ign-simple-popup").css("opacity") === "1") {
					clearInterval(intervalId);

					// Call the afterOpen callback
					settings.afterOpen(html);
				}
			}, 100);
		}

		/**
		* stopPopup
		*
		* Stop the popup and remove it from the DOM. Because it can fade out, use and interval
		* to check if opacity has reached 0. Maybe remove backdrop and maybe unbind the escape
		* key
		*
		* @returns {void}
		*/
		function stopPopup() {
			// Call the beforeClose callback
			var html = $("#ign-simple-popup");
			settings.beforeClose(html);

			// Set the fade out effect
			if (settings.inlineCss) {
				html = setFadeTimingFunction(html, settings.fadeOutTimingFunction);
				html = setFadeDuration(html, settings.fadeOutDuration);
			}

			$("#ign-simple-popup").addClass("hide-it");

			// Poll to check if the popup is faded out
			var intervalId = setInterval(function() {
				if ($("#ign-simple-popup").css("opacity") === "0") {
					$("#ign-simple-popup").remove();
					clearInterval(intervalId);

					// Call the afterClose callback
					settings.afterClose();
				}
			}, 100);

			if (settings.backdrop) {
				stopBackdrop();
			}

			if (settings.escapeKey) {
				unbindEscape();
			}
		}

		/**
		* bindClickPopup
		*
		* When clicked outside the popup, close the popup. Use e.target to determine if
		* "ign-simple-popup" was clicked or "ign-simple-popup-content"
		*
		* @param {string} html The html of the popup
		* @returns {void}
		*/
		function bindClickPopup(html) {
			$(html).on("click", function(e) {
				if ($(e.target).prop("id") === "ign-simple-popup") {
					stopPopup();
				}
			});
		}

		/**
		* bindClickClose
		*
		* When clicked on the close cross, close the popup
		*
		* @param {string} html The html of the popup
		* @returns {void}
		*/
		function bindClickClose(html) {
			$(html).on("click", function(e) {
				stopPopup();
			});
		}

		/**
		* startBackdrop
		*
		* Insert the backdrop HTML
		*
		* @returns {void}
		*/
		function startBackdrop() {
			insertBackdropHtml();
		}

		/**
		* stopBackdrop
		*
		* Stop the backdrop and remove it from the DOM. Because it can fade out, use and interval
		* to check if opacity has reached 0.
		*
		* @returns {void}
		*/
		function stopBackdrop() {
			var backdrop = $("#ign-simple-popup-backdrop");

			// Set the fade out effect
			if (settings.inlineCss) {
				backdrop = setFadeTimingFunction(backdrop, settings.fadeOutTimingFunction);
				backdrop = setFadeDuration(backdrop, settings.fadeOutDuration);
			}

			backdrop.addClass("hide-it");

			// Poll to check if the popup is faded out
			var intervalId = setInterval(function() {
				if ($("#ign-simple-popup-backdrop").css("opacity") === "0") {
					$("#ign-simple-popup-backdrop").remove();
					clearInterval(intervalId);
				}
			}, 100);
		}

		/**
		* insertBackdropHtml
		*
		* Create the backdrop HTML and append it to the body. Maybe set the CSS.
		*
		* @returns {void}
		*/
		function insertBackdropHtml() {
			var content = $("<div/>", {
				"class": "ign-simple-popup-backdrop-content"
			});

			var html = $("<div/>", {
				"id": "ign-simple-popup-backdrop",
				"class": "hide-it"
			});

			if (settings.inlineCss) {
				content.css("opacity", settings.backdrop);
				content.css("background", settings.backdropBackground);
			}

			html.append(content);
			$("body").append(html);

			// Use a timeout, else poor CSS doesn't see the difference
			setTimeout(function() {
				var backdrop = $("#ign-simple-popup-backdrop");

				// Set the fade in effect
				if (settings.inlineCss) {
					backdrop = setFadeTimingFunction(backdrop, settings.fadeInTimingFunction);
					backdrop = setFadeDuration(backdrop, settings.fadeInDuration);
				}

				backdrop.removeClass("hide-it");
			});
		}

		/**
		* bindEscape
		*
		* Bind the escape key to stop popup
		*
		* @returns {void}
		*/
		function bindEscape() {
			$(document).on("keyup.escapeKey", function(e) {
				if (e.keyCode === 27) {
					stopPopup();
				}
			});
		}

		/**
		* unbindEscape
		*
		* Unbind the escape key
		*
		* @returns {void}
		*/
		function unbindEscape() {
			$(document).unbind("keyup.escapeKey");
		}

		/**
		* setFadeTimingFunction
		*
		* @param {object} object - The object to set the timing function on
		* @param {string} timingFunction - The type of timing
		* @returns {object} The object with the time function set
		*/
		function setFadeTimingFunction(object, timingFunction) {
			object.css("-webkit-transition-timing-function", timingFunction);
			object.css("-moz-transition-timing-function", timingFunction);
			object.css("-ms-transition-timing-function", timingFunction);
			object.css("-o-transition-timing-function", timingFunction);
			object.css("transition-timing-function", timingFunction);
			return object;
		}

		/**
		* setFadeDuration
		*
		* @param {object} object - The object to set the duration on
		* @param {float} duration - The duration of the fade
		* @returns {object} The object with the duration set
		*/
		function setFadeDuration(object, duration) {
			object.css("-webkit-transition-duration", duration + "s");
			object.css("-moz-transition-duration", duration + "s");
			object.css("-ms-transition-duration", duration + "s");
			object.css("-o-transition-duration", duration + "s");
			object.css("transition-duration", duration + "s");
			return object;
		}

		return init();
	};
}(jQuery));
