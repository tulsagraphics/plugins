<?php
/*
Copyright (c) 2014 - IgniteWoo.com - All Rights Reserved 

Copyright (c) 2009-2015 John Blackbourn

The code in this file is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

*/

class IgniteWoo_Manual_Orders_User_Switching {

	public function __construct() {
	
		add_filter( 'user_has_cap',                    array( $this, 'filter_user_has_cap' ), 10, 3 );
		
		add_filter( 'map_meta_cap',                    array( $this, 'filter_map_meta_cap' ), 10, 4 );
		
		add_filter( 'user_row_actions',                array( $this, 'filter_user_row_actions' ), 10, 2 );
		
		add_action( 'plugins_loaded',                  array( $this, 'action_plugins_loaded' ) );
		
		add_action( 'init',                            array( $this, 'action_init' ) );
		
		add_action( 'all_admin_notices',               array( $this, 'action_admin_notices' ), 1 );
		
		add_action( 'wp_logout',                       'user_switching_clear_olduser_cookie' );
		
		add_action( 'wp_login',                        'user_switching_clear_olduser_cookie' );

		add_filter( 'ms_user_row_actions',             array( $this, 'filter_user_row_actions' ), 10, 2 );
		
		add_filter( 'login_message',                   array( $this, 'filter_login_message' ), 1 );
		
		add_action( 'wp_footer',                       array( $this, 'action_wp_footer' ) );

		add_action( 'personal_options',                array( $this, 'action_personal_options' ) );
		
		add_action( 'admin_bar_menu',                  array( $this, 'action_custom_admin_bar_menu' ), 11 );

		add_action( 'admin_bar_menu',                  array( $this, 'action_admin_bar_menu' ), 999 );
		
		add_action('wp_ajax_woocommerce_json_login_as_customers_search', array( $this, 'customers_search') );
	}

	public function action_custom_admin_bar_menu( WP_Admin_Bar $wp_admin_bar ) {
		global $current_user, $ign_opc;

		if ( !$ign_opc->has_access() )
			return;

		if ( !function_exists( 'is_admin_bar_showing' ) )
			return;

		if ( $old_user = self::get_old_user() ) {

			self::build_switch_to_user_menu( $old_user );

		} else {

			self::build_default_originating_user_menu();

		}

	}
	
	public function build_switch_to_user_menu( $old_user ) {
		global $woocommerce, $wp_admin_bar;

		$wp_admin_bar->remove_node( "my-account" );

		$old_user_id = $old_user->ID;

		$old_avatar = get_avatar( $old_user_id, 26 );
		
		$old_howdy  = sprintf( __('Howdy, %1$s', 'manual_phone_orders'), $old_user->data->display_name );
		
		$old_class  = empty( $old_avatar ) ? '' : 'with-avatar';
		
		$old_profile_url  = get_edit_profile_url( $old_user_id );

		$user_id      = get_current_user_id();
		
		$current_user = wp_get_current_user();
		
		$profile_url  = get_edit_profile_url( $user_id );

		if ( ! $user_id )
			return;

		$avatar = get_avatar( $user_id, 26 );
		
		$logged_in_as  = ' <span style="font-weight:bold;color:yellow">' . sprintf( __('Logged in as %1$s', 'manual_phone_orders'), $current_user->display_name ) . '</span>';
		
		$class  = empty( $avatar ) ? '' : 'with-avatar';

		$top_heading_shop_as_user = '<span class="top-howdy top-howdy-main">';
		
		$top_heading_shop_as_user .= $old_howdy . $old_avatar;
		
		$top_heading_shop_as_user .= '</span>';
		
		$top_heading_shop_as_user .= '<span class="top-howdy top-howdy-secondry">';
		
		$top_heading_shop_as_user .= $logged_in_as . $avatar;
		
		$top_heading_shop_as_user .= '</span>';

		$wp_admin_bar->add_menu( array(
			'id'        => 'my-account',
			'parent'    => 'top-secondary',
			'title'     => $top_heading_shop_as_user,
			'href'      => $profile_url,
			'meta'      => array(
				'class'     => "switch-to-user ".$class,
				'title'     => $logged_in_as,
			),
		) );

		$wp_admin_bar->remove_node('user-actions');

		$wp_admin_bar->add_group( array(
			'parent'	=> 'my-account',
			'id'		=> 'logged-in-as-actions',
			'meta'		=> array(
				'class'		=> 'switch-to-user-profile-menu switch-to-user-profile-menu-second-user'
			)
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'logged-in-as-actions',
			'id'     => 'main-avatar',
			'title'  => get_avatar( $user_id, 64 )
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'logged-in-as-actions',
			'id'     => 'sac-heading-h3',
			'title'  => '<br/>&nbsp;<br/>', // __( 'Logged in as', 'manual_phone_orders' ),
			'meta'   => array(
				'tabindex'	=> -1
			)
		));

		/* Compile new user display details */
		$new_user_name = $current_user->display_name;
		
		$new_user_email = $current_user->user_email;

		$new_user_details = sprintf(__('
			<div class="logged-in-as-details-holder">
				<div class="logged-in-as-name">%1$s</div>
				<div class="logged-in-as-email">%2$s</div>
			</div>
		','manual_phone_orders'), $new_user_name, $new_user_email);
	}

	/**
	 * Add menu options on originating users my account menu in admin menu bar
	 *
	 */
	public function build_default_originating_user_menu() {
		global $woocommerce, $wp_admin_bar;

		$wp_admin_bar->add_group( array(
			'parent'	=> "my-account",
			'id'		=> 'switch-to-user',
			'meta'		=> array(
			'class'		=> 'switch-to-user-profile-menu switch-to-user-profile-main-user'
			)
		));

		$wp_admin_bar->add_menu( array(
			'parent' => "switch-to-user",
			'id'     => 'switch3',
			'meta'   => array(
				'tabindex'	=> -1
			),
			'title'  => __( 'Switch to a Customer Account<br/>
				<select id="switch_to_user_search_users" name="login_as_user_search_users" class="ajax_chosen_switch_to_user_search_users" data-placeholder="'.__( 'Search for Customer &hellip;', 'manual_phone_orders' ).'">
					<option value=""></option>
				</select>
				<div class="searched-switch-links"></div>
				<div class="switch-to-user-switch-button"></div>
			', "ignitewoo_one_page" ),

		));

	}
	
	
	/**
	 * Define the names of our cookies.
	 */
	public function action_plugins_loaded() {

		// User Switching's auth_cookie
		if ( !defined( 'USER_SWITCHING_COOKIE' ) ) {
			define( 'USER_SWITCHING_COOKIE', 'wordpress_user_sw_' . COOKIEHASH );
		}

		// User Switching's secure_auth_cookie
		if ( !defined( 'USER_SWITCHING_SECURE_COOKIE' ) ) {
			define( 'USER_SWITCHING_SECURE_COOKIE', 'wordpress_user_sw_secure_' . COOKIEHASH );
		}

		// User Switching's logged_in_cookie
		if ( !defined( 'USER_SWITCHING_OLDUSER_COOKIE' ) ) {
			if ( defined( 'OLDUSER_COOKIE' ) ) {
				trigger_error( sprintf(
					'The OLDUSER_COOKIE constant is deprecated. See <code>%s()</code>.',
					__METHOD__
				), ( WP_DEBUG ? E_USER_WARNING : E_USER_NOTICE ) );
				define( 'USER_SWITCHING_OLDUSER_COOKIE', OLDUSER_COOKIE );
			} else {
				define( 'USER_SWITCHING_OLDUSER_COOKIE', 'wordpress_user_sw_olduser_' . COOKIEHASH );
			}
		}

	}

	/**
	 * Output the 'Switch To' link on the user editing screen if we have permission to switch to this user.
	 *
	 * @param WP_User $user User object for this screen.
	 */
	public function action_personal_options( WP_User $user ) {

		if ( ! $link = self::maybe_switch_url( $user ) ) {
			return;
		}

		?>
		<tr>
			<th scope="row"><?php _ex( 'User Switching', 'User Switching title on user profile screen', 'manual_phone_orders' ); ?></th>
			<td><a href="<?php echo $link; ?>"><?php _e( 'Switch To', 'manual_phone_orders' ); ?></a></td>
		</tr>
		<?php
	}

	/**
	 * Return whether or not the current logged in user is being remembered in the form of a persistent browser cookie
	 * (ie. they checked the 'Remember Me' check box when they logged in). This is used to persist the 'remember me'
	 * value when the user switches to another user.
	 *
	 * @return bool Whether the current user is being 'remembered' or not.
	 */
	public static function remember() {

		$current     = wp_parse_auth_cookie( '', 'logged_in' );
		
		$cookie_life = apply_filters( 'auth_cookie_expiration', 172800, get_current_user_id(), false );

		# Here we calculate the expiration length of the current auth cookie and compare it to the default expiration.
		# If it's greater than this, then we know the user checked 'Remember Me' when they logged in.
		return ( ( $current['expiration'] - time() ) > $cookie_life );

	}

	/**
	 * Load localisation files and route actions depending on the 'action' query var.
	 */
	public function action_init() {

		//load_plugin_textdomain( 'user-switching', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );

		if ( !isset( $_REQUEST['action'] ) ) {
			return;
		}

		switch ( $_REQUEST['action'] ) {

			# We're attempting to switch to another user:
			case 'switch_to_user':
			
				$user_id = absint( $_REQUEST['user_id'] );

				//Check authentication:
				if ( !current_user_can( 'switch_to_user', $user_id ) ) {
					wp_die( __( 'Could not switch users.', 'manual_phone_orders' ) );
				}

				//Check intent:
				check_admin_referer( "switch_to_user_{$user_id}" );

				//Switch user:
				$user = switch_to_user( $user_id, self::remember() );
				if ( $user ) {

					$redirect_to = self::get_redirect( $user );

					# Redirect to the dashboard or the home URL depending on capabilities:
					$args = array( 'user_switched' => 'true' );
					
					if ( $redirect_to ) {
						wp_safe_redirect( add_query_arg( $args, $redirect_to ) );
					} else if ( !current_user_can( 'read' ) ) {
						wp_redirect( add_query_arg( $args, home_url() ) );
					} else {
						wp_redirect( add_query_arg( $args, admin_url() ) );
					}
					
					die();

				} else {
				
					wp_die( __( 'Could not switch users.', 'manual_phone_orders' ) );
					
				}
				
				break;
				
			# We're attempting to switch back to the originating user:
			case 'switch_to_olduser':

				# Fetch the originating user data:
				if ( !$old_user = self::get_old_user() ) {
					wp_die( __( 'Could not switch users.', 'manual_phone_orders' ) );
				}

				# Check authentication:
				if ( !self::authenticate_old_user( $old_user ) ) {
					wp_die( __( 'Could not switch users.', 'manual_phone_orders' ) );
				}

				# Check intent:
				check_admin_referer( "switch_to_olduser_{$old_user->ID}" );

				# Switch user:
				if ( switch_to_user( $old_user->ID, self::remember(), false ) ) {

					$redirect_to = self::get_redirect( $old_user );
					
					$args = array( 'user_switched' => 'true', 'switched_back' => 'true' );
					
					if ( $redirect_to ) {
						wp_safe_redirect( add_query_arg( $args, $redirect_to ) );
					} else {
						wp_redirect( add_query_arg( $args, admin_url( 'users.php' ) ) );
					}
					
					die();
					
				} else {
				
					wp_die( __( 'Could not switch users.', 'manual_phone_orders' ) );
				}
				
				break;

			# We're attempting to switch off the current user:
			/*
			case 'switch_off':

				$user = wp_get_current_user();

				# Check authentication:
				if ( !current_user_can( 'switch_off' ) ) {
					wp_die( __( 'Could not switch off.', 'manual_phone_orders' ) );
				}

				# Check intent:
				check_admin_referer( "switch_off_{$user->ID}" );

				# Switch off:
				if ( switch_off_user() ) {
					$redirect_to = self::get_redirect();
					$args = array( 'switched_off' => 'true' );
					if ( $redirect_to ) {
						wp_safe_redirect( add_query_arg( $args, $redirect_to ) );
					} else {
						wp_redirect( add_query_arg( $args, home_url() ) );
					}
					die();
				} else {
					wp_die( __( 'Could not switch off.', 'manual_phone_orders' ) );
				}
				break;
			*/
		}

	}

	/**
	 * Fetch the URL to redirect to for a given user (used after switching).
	 *
	 * @param  WP_User|null A WP_User object (optional).
	 * @return string The URL to redirect to.
	 */
	protected static function get_redirect( WP_User $user = null ) {

		if ( isset( $_REQUEST['redirect_to'] ) and !empty( $_REQUEST['redirect_to'] ) ) {
			$redirect_to = self::remove_query_args( $_REQUEST['redirect_to'] );
		} else {
			$redirect_to = '';
		}

		if ( $user ) {
		
			$requested_redirect_to = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '';
			
			$redirect_to = apply_filters( 'login_redirect', $redirect_to, $requested_redirect_to, $user );
		}

		return $redirect_to;

	}

	/**
	 * Display the 'Switched to {user}' and 'Switch back to {user}' messages in the admin area.
	 */
	public function action_admin_notices() {
		$user = wp_get_current_user();

		if ( $old_user = self::get_old_user() ) {

			?>
			<div id="user_switching" class="updated">
				<p><span class="dashicons dashicons-admin-users" style="color:#56c234"></span>
				<?php
					if ( isset( $_GET['user_switched'] ) ) {
						printf( __( 'Switched to %1$s (%2$s).', 'manual_phone_orders' ), $user->display_name, $user->user_login );
					}
					$url = add_query_arg( array(
						'redirect_to' => urlencode( self::current_url() )
					), self::switch_back_url( $old_user ) );
					printf( ' <a href="%s">%s</a>.', $url, sprintf( __( 'Switch back to %1$s (%2$s)', 'manual_phone_orders' ), $old_user->display_name, $old_user->user_login ) );
				?></p>
			</div>
			<?php

		} else if ( isset( $_GET['user_switched'] ) ) {

			?>
			<div id="user_switching" class="updated">
				<p><?php
					if ( isset( $_GET['switched_back'] ) ) {
						printf( __( 'Switched back to %1$s (%2$s).', 'manual_phone_orders' ), $user->display_name, $user->user_login );
					} else {
						printf( __( 'Switched to %1$s (%2$s).', 'manual_phone_orders' ), $user->display_name, $user->user_login );
					}
				?></p>
			</div>
			<?php

		}
	}

	/**
	 * Validate the old user cookie and return its user data.
	 *
	 * @return bool|WP_User False if there's no old user cookie or it's invalid, WP_User object if it's present and valid.
	 */
	public static function get_old_user() {
	
		$cookie = user_switching_get_olduser_cookie();
		
		if ( !empty( $cookie ) ) {
		
			if ( $old_user_id = wp_validate_auth_cookie( $cookie, 'logged_in' ) ) {
				return get_userdata( $old_user_id );
			}
		}
		
		return false;
	}

	public static function get_previous_switched_user() {
	
		$cookie = user_switching_get_olduser_cookie();
		
		if ( !empty( $cookie ) ) {
		
			if ( $old_user_id = wp_validate_auth_cookie( end( $cookie ), 'switched_user' ) )
				return get_userdata( $old_user_id );
		}
		
		return false;
	}
	
	/**
	 * Authenticate an old user by verifying the latest entry in the auth cookie.
	 * 
	 * @param  WP_User $user A WP_User object (usually from the logged_in cookie).
	 * @return bool Whether verification with the auth cookie passed.
	 */
	public static function authenticate_old_user( WP_User $user ) {
		$cookie = user_switching_get_auth_cookie();
		if ( !empty( $cookie ) ) {

			if ( IgniteWoo_Manual_Orders_User_Switching::secure_auth_cookie() ) {
			
				$scheme = 'secure_auth';
				
			} else {
			
				$scheme = 'auth';
			}
			
			if ( $old_user_id = wp_validate_auth_cookie( end( $cookie ), $scheme ) ) {
			
				return ( $user->ID == $old_user_id );
			}
		}
		
		return false;
	}

	/**
	 * Adds a 'Switch back to {user}' link to the account menu in WordPress' admin bar.
	 *
	 * @param WP_Admin_Bar $wp_admin_bar The admin bar object
	 */
	public function action_admin_bar_menu( WP_Admin_Bar $wp_admin_bar ) {

		if ( !function_exists( 'is_admin_bar_showing' ) ) {
			return;
		}
		
		if ( !is_admin_bar_showing() ) {
			return;
		}

		if ( method_exists( $wp_admin_bar, 'get_node' ) and $wp_admin_bar->get_node( 'user-actions' ) ) {
			$parent = 'user-actions';
		} else if ( get_option( 'show_avatars' ) ) {
			$parent = 'my-account';
		} else {
			$parent = 'my-account';
		}

		if ( $old_user = self::get_old_user() ) {

			$wp_admin_bar->add_menu( array(
				'parent' => $parent,
				'id'     => 'switch-back',
				'title'  => sprintf( __( 'Switch back to %1$s (%2$s)', 'manual_phone_orders' ), $old_user->display_name, $old_user->user_login ),
				'href'   => add_query_arg( array(
					'redirect_to' => urlencode( self::current_url() )
				), self::switch_back_url( $old_user ) )
			) );

		}

		/*
		if ( current_user_can( 'switch_off' ) ) {

			$url = self::switch_off_url( wp_get_current_user() );
			if ( !is_admin() ) {
				$url = add_query_arg( array(
					'redirect_to' => urlencode( self::current_url() )
				), $url );
			}

			$wp_admin_bar->add_menu( array(
				'parent' => $parent,
				'id'     => 'switch-off',
				'title'  => __( 'Switch Off', 'manual_phone_orders' ),
				'href'   => $url
			) );

		}
		*/
	}

	/**
	 * Adds a 'Switch back to {user}' link to the WordPress footer if the admin toolbar isn't showing.
	 */
	public function action_wp_footer() {

		if ( !is_admin_bar_showing() and $old_user = self::get_old_user() ) {
		
			$link = sprintf( __( 'Switch back to %1$s (%2$s)', 'manual_phone_orders' ), $old_user->display_name, $old_user->user_login );
			
			$url = add_query_arg( array(
				'redirect_to' => urlencode( self::current_url() )
			), self::switch_back_url( $old_user ) );
			
			echo '<p id="user_switching_switch_on" style="margin:1em 1em;padding:1em;font-weight:bold;color: yellow !important;background-color: #333;"><a href="' . $url . '" style="color:yellow; text-decoration:none;font-size:1.2em">' . $link . '</a></p>';
		}

	}

	/**
	 * Adds a 'Switch back to {user}' link to the WordPress login screen.
	 *
	 * @param  string $message The login screen message.
	 * @return string The login screen message.
	 */
	public function filter_login_message( $message ) {

		if ( $old_user = self::get_old_user() ) {
		
			$link = sprintf( __( 'Switch back to %1$s (%2$s)', 'manual_phone_orders' ), $old_user->display_name, $old_user->user_login );
			
			$url = self::switch_back_url( $old_user );
			
			if ( isset( $_REQUEST['redirect_to'] ) and !empty( $_REQUEST['redirect_to'] ) ) {
			
				$url = add_query_arg( array( 'redirect_to' => urlencode( $_REQUEST['redirect_to'] ) ), $url );
			}
			
			$message .= '<p class="message"><span class="dashicons dashicons-admin-users" style="color:#56c234"></span> <a href="' . $url . '">' . $link . '</a></p>';
		}

		return $message;

	}

	/**
	 * Adds a 'Switch To' link to each list of user actions on the Users screen.
	 *
	 * @param  array   $actions The actions to display for this user row.
	 * @param  WP_User $user    The user object displayed in this row.
	 * @return array The actions to display for this user row.
	 */
	public function filter_user_row_actions( array $actions, WP_User $user ) {

		if ( ! $link = self::maybe_switch_url( $user ) ) {
		
			return $actions;
			
		}

		$actions['switch_to_user'] = '<a href="' . $link . '">' . __( 'Switch&nbsp;To', 'manual_phone_orders' ) . '</a>';

		return $actions;
	}

	/**
	 * Helper function. Returns the switch to or switch back URL for a given user.
	 *
	 * @param  WP_User $user The user to be switched to.
	 * @return string|bool The required URL, or false if there's no old user or the user doesn't have the required capability.
	 */
	public static function maybe_switch_url( WP_User $user ) {

		$old_user = self::get_old_user();

		if ( $old_user and ( $old_user->ID == $user->ID ) ) {
		
			return self::switch_back_url( $old_user );
			
		} else if ( current_user_can( 'switch_to_user', $user->ID ) ) { 
		
			return self::switch_to_url( $user );
			
		} else {
		
			return false;
		}

	}

	/**
	 * Helper function. Returns the nonce-secured URL needed to switch to a given user ID.
	 *
	 * @param  WP_User $user The user to be switched to.
	 * @return string The required URL.
	 */
	public static function switch_to_url( WP_User $user ) {
	
		return wp_nonce_url( add_query_arg( array(
			'action'  => 'switch_to_user',
			'user_id' => $user->ID
		), wp_login_url() ), "switch_to_user_{$user->ID}" );
	}

	/**
	 * Helper function. Returns the nonce-secured URL needed to switch back to the originating user.
	 *
	 * @param  WP_User $user The old user.
	 * @return string        The required URL.
	 */
	public static function switch_back_url( WP_User $user ) {
	
		return wp_nonce_url( add_query_arg( array(
			'action' => 'switch_to_olduser'
		), wp_login_url() ), "switch_to_olduser_{$user->ID}" );
	}

	/**
	 * Helper function. Returns the nonce-secured URL needed to switch off the current user.
	 *
	 * @param  WP_User $user The user to be switched off.
	 * @return string        The required URL.
	 */
	public static function switch_off_url( WP_User $user ) {
	
		return wp_nonce_url( add_query_arg( array(
			'action' => 'switch_off'
		), wp_login_url() ), "switch_off_{$user->ID}" );
	}

	/**
	 * Helper function. Returns the current URL.
	 *
	 * @return string The current URL.
	 */
	public static function current_url() {
		return ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	}

	/**
	 * Helper function. Removes a list of common confirmation-style query args from a URL.
	 *
	 * @param  string $url A URL.
	 * @return string The URL with the listed query args removed.
	 */
	public static function remove_query_args( $url ) {
	
		return remove_query_arg( array(
			'user_switched', 'switched_off', 'switched_back',
			'message', 'update', 'updated', 'settings-updated', 'saved',
			'activated', 'activate', 'deactivate', 'enabled', 'disabled',
			'locked', 'skipped', 'deleted', 'trashed', 'untrashed'
		), $url );
	}

	/**
	 * Helper function. Should User Switching's equivalent of the 'logged_in' cookie be secure?
	 *
	 * This is used to set the 'secure' flag on the old user cookie, for enhanced security.
	 * 
	 * @link https://core.trac.wordpress.org/ticket/15330
	 *
	 * @return bool Should the old user cookie be secure?
	 */
	public static function secure_olduser_cookie() {
		return ( is_ssl() and ( 'https' === parse_url( home_url(), PHP_URL_SCHEME ) ) );
	}

	/**
	 * Helper function. Should User Switching's equivalent of the 'auth' cookie be secure?
	 * 
	 * This is used to determine whether to set a secure auth cookie or not.
	 *
	 * @return bool Should the auth cookie be secure?
	 */
	public static function secure_auth_cookie() {
		return ( is_ssl() and ( 'https' === parse_url( wp_login_url(), PHP_URL_SCHEME ) ) );
	}

	/**
	 * Filter the user's capabilities so they can be added/removed on the fly.
	 *
	 * This is used to grant the 'switch_to_user' capability to a user if they have the ability to edit the user
	 * they're trying to switch to (and that user is not themselves), and to grant the 'switch_off' capability to
	 * a user if they can edit users.
	 *
	 * Important: This does not get called for Super Admins. See filter_map_meta_cap() below.
	 *
	 * @param  array $user_caps     User's capabilities.
	 * @param  array $required_caps Actual required capabilities for the requested capability.
	 * @param  array $args          Arguments that accompany the requested capability check:
	 *                              [0] => Requested capability from current_user_can()
	 *                              [1] => Current user ID
	 *                              [2] => Optional second parameter from current_user_can()
	 * @return array User's capabilities.
	 */
	public function filter_user_has_cap( array $user_caps, array $required_caps, array $args ) {

		if ( 'switch_to_user' === $args[0] ) {
		
			if ( isset( $args[1] ) && isset( $args[2] ) )
				$user_caps['switch_to_user'] = ( user_can( $args[1], 'edit_user', $args[2] ) and ( $args[2] != $args[1] ) );
				
		} else if ( 'switch_off' === $args[0] ) {
		
			$user_caps['switch_off'] = user_can( $args[1], 'edit_users' );
			
		}
		
		return $user_caps;
	}

	/**
	 * Filters the actual required capabilities for a given capability or meta capability.
	 *
	 * This is used to add the 'do_not_allow' capability to the list of required capabilities when a super admin
	 * is trying to switch to themselves. It affects nothing else as super admins can do everything by default.
	 *
	 * @param  array  $required_caps Actual required capabilities for the requested action.
	 * @param  string $cap           Capability or meta capability being checked.
	 * @param  string $user_id       Current user ID.
	 * @param  array  $args          Arguments that accompany this capability check.
	 * @return array  Required capabilities for the requested action.
	 */
	public function filter_map_meta_cap( array $required_caps, $cap, $user_id, array $args ) {
	
		if ( isset( $args[0] ) && ( 'switch_to_user' === $cap ) and ( $args[0] == $user_id ) ) {
			
			$required_caps[] = 'do_not_allow';
		}
		return $required_caps;
	}
	
	
	public function customers_search() {

		check_ajax_referer( 'search-customers', 'security' );

		$user_id = get_current_user_id();

		header( 'Content-Type: application/json; charset=utf-8' );

		$term = woocommerce_clean( urldecode( stripslashes( $_GET['term'] ) ) );

		if ( empty( $term ) )
			die();

		//$default = isset( $_GET['default'] ) ? $_GET['default'] : __( 'Search for Customer', 'manual_phone_orders' );

		//$found_customers = array( '' => $default );

		add_action( 'pre_user_query', array( $this, 'json_search_customer_name' ) );

		$customers_query = new WP_User_Query( array(
			'fields'			=> 'all',
			'orderby'			=> 'display_name',
			'search'			=> '*' . $term . '*',
			'search_columns'	=> array( 'ID', 'user_login', 'user_email', 'user_nicename' )
		) );

		remove_action( 'pre_user_query', array( $this, 'json_search_customer_name' ) );

		$customers = $customers_query->get_results();

		if ( $customers ) {
			foreach ( $customers as $customer ) {
				if ( $user_id != $customer->ID ) {
	
					$link = self::ajax_search_switch_to_url( $customer->ID );

					$found_customers[] = array( "id" => $customer->ID, "label" => $customer->display_name . ' (#' . $customer->ID . ' &ndash; ' . sanitize_email( $customer->user_email ) . ')', "link" => $link );
				}
			}
		}

		echo json_encode( $found_customers );

		die();
	}
	
	public static function ajax_search_switch_to_url( $user_id ) {
	
		return wp_nonce_url( add_query_arg( array(
			'action'  => 'switch_to_user',
			'user_id' => $user_id
		), wp_login_url() ), "switch_to_user_{$user_id}" );
	}
	
	public function json_search_customer_name( $query ) {
		global $wpdb;

		$term = ( function_exists( 'wc_clean' ) ) ? wc_clean( stripslashes( $_GET['term'] ) ) : sanitize_text_field( stripslashes( $_GET['term'] ) );
		
		if ( method_exists( $wpdb, 'esc_like' ) ) {
			$term = $wpdb->esc_like( $term );
		} else {
			$term = like_escape( $term );
		}

		$query->query_from  .= " INNER JOIN {$wpdb->usermeta} AS user_name ON {$wpdb->users}.ID = user_name.user_id AND ( user_name.meta_key = 'first_name' OR user_name.meta_key = 'last_name' ) ";
		
		$query->query_where .= $wpdb->prepare( " OR user_name.meta_value LIKE %s ", '%' . $term . '%' );
	}

}

if ( !function_exists( 'user_switching_set_olduser_cookie' ) ) {
	/**
	* Sets authorisation cookies containing the originating user information.
	*
	* @param int  $old_user_id The ID of the originating user, usually the current logged in user.
	* @param bool $pop         Pop the latest user off the auth cookie, instead of appending the new one. Default false.
	*/
	function user_switching_set_olduser_cookie( $old_user_id, $pop = false ) {
	
		$secure_auth_cookie    = IgniteWoo_Manual_Orders_User_Switching::secure_auth_cookie();
		
		$secure_olduser_cookie = IgniteWoo_Manual_Orders_User_Switching::secure_olduser_cookie();
		
		$expiration            = time() + 172800; # 48 hours
		
		$auth_cookie           = user_switching_get_auth_cookie();
		
		$olduser_cookie        = wp_generate_auth_cookie( $old_user_id, $expiration, 'logged_in' );

		if ( $secure_auth_cookie ) {
		
			$auth_cookie_name = USER_SWITCHING_SECURE_COOKIE;
			
			$scheme = 'secure_auth';
			
		} else {
		
			$auth_cookie_name = USER_SWITCHING_COOKIE;
			
			$scheme = 'auth';
		}

		if ( $pop ) {
			array_pop( $auth_cookie );
		} else {
			array_push( $auth_cookie, wp_generate_auth_cookie( $old_user_id, $expiration, $scheme ) );
		}

		setcookie( $auth_cookie_name, json_encode( $auth_cookie ), $expiration, SITECOOKIEPATH, COOKIE_DOMAIN, $secure_auth_cookie, true );
		
		setcookie( USER_SWITCHING_OLDUSER_COOKIE, $olduser_cookie, $expiration, COOKIEPATH, COOKIE_DOMAIN, $secure_olduser_cookie, true );
	}
}


if ( !function_exists( 'user_switching_clear_olduser_cookie' ) ) {

	/**
	* Clears the cookies containing the originating user, or pops the latest item off the end if there's more than one.
	* 
	* @param bool $clear_all Whether to clear the cookies or just pop the last user information off the end.
	*/
	function user_switching_clear_olduser_cookie( $clear_all = true ) {
	
		$auth_cookie = user_switching_get_auth_cookie();
		
		if ( !empty( $auth_cookie ) ) {
			array_pop( $auth_cookie );
		}
		if ( $clear_all or empty( $auth_cookie ) ) {
		
			$expire = time() - 31536000;
			
			setcookie( USER_SWITCHING_COOKIE,         ' ', $expire, SITECOOKIEPATH, COOKIE_DOMAIN );
			
			setcookie( USER_SWITCHING_SECURE_COOKIE,  ' ', $expire, SITECOOKIEPATH, COOKIE_DOMAIN );
			
			setcookie( USER_SWITCHING_OLDUSER_COOKIE, ' ', $expire, COOKIEPATH, COOKIE_DOMAIN );
			
		} else {

			if ( IgniteWoo_Manual_Orders_User_Switching::secure_auth_cookie() ) {
				$scheme = 'secure_auth';
			} else {
				$scheme = 'auth';
			}

			if ( $old_user_id = wp_validate_auth_cookie( end( $auth_cookie ), $scheme ) ) {
				user_switching_set_olduser_cookie( $old_user_id, true );
			}

		}
	}
}

if ( !function_exists( 'user_switching_get_olduser_cookie' ) ) {

	/**
	* Gets the value of the cookie containing the originating user.
	*
	* @return string|bool The old user cookie, or boolean false if there isn't one.
	*/
	function user_switching_get_olduser_cookie() {
	
		if ( isset( $_COOKIE[USER_SWITCHING_OLDUSER_COOKIE] ) ) {
			return stripslashes( $_COOKIE[USER_SWITCHING_OLDUSER_COOKIE] );
		} else {
			return false;
		}
	}
}

if ( !function_exists( 'user_switching_get_auth_cookie' ) ) {

	/**
	* Gets the value of the auth cookie containing the list of originating users.
	*
	* @return array Array of originating user authentication cookies. Empty array if there are none.
	*/
	function user_switching_get_auth_cookie() {
	
		if ( IgniteWoo_Manual_Orders_User_Switching::secure_auth_cookie() ) {
			$auth_cookie_name = USER_SWITCHING_SECURE_COOKIE;
		} else {
			$auth_cookie_name = USER_SWITCHING_COOKIE;
		}

		if ( isset( $_COOKIE[$auth_cookie_name] ) ) {
			$cookie = json_decode( stripslashes( $_COOKIE[$auth_cookie_name] ) );
		}
		if ( !isset( $cookie ) or !is_array( $cookie ) ) {
			$cookie = array();
		}
		return $cookie;
	}
}

if ( !function_exists( 'switch_to_user' ) ) {
	/**
	* Switches the current logged in user to the specified user.
	*
	* @param  int  $user_id      The ID of the user to switch to.
	* @param  bool $remember     Whether to 'remember' the user in the form of a persistent browser cookie. Optional.
	* @param  bool $set_old_user Whether to set the old user cookie. Optional.
	* @return bool|WP_User WP_User object on success, false on failure.
	*/
	function switch_to_user( $user_id, $remember = false, $set_old_user = true ) {
	
		if ( !$user = get_userdata( $user_id ) ) {
			return false;
		}

		if ( $set_old_user and is_user_logged_in() ) {
		
			$old_user_id = get_current_user_id();
			
			user_switching_set_olduser_cookie( $old_user_id );
			
		} else {
		
			$old_user_id = false;
			
			user_switching_clear_olduser_cookie( false );
		}

		wp_clear_auth_cookie();
		
		wp_set_auth_cookie( $user_id, $remember );
		
		wp_set_current_user( $user_id );

		if ( $set_old_user ) {
			do_action( 'switch_to_user', $user_id, $old_user_id );
		} else {
			do_action( 'switch_back_user', $user_id, $old_user_id );
		}

		return $user;
	}

}


if ( !function_exists( 'current_user_switched' ) ) {
	/**
	* Helper function. Did the current user switch into their account?
	*
	* @return bool|WP_User False if the user isn't logged in or they didn't switch in; old user object (which evaluates to
	*                      true) if the user switched into the current user account.
	*/
	function current_user_switched() {
		if ( !is_user_logged_in() ) {
			return false;
		}

		return IgniteWoo_Manual_Orders_User_Switching::get_old_user();
	}
}

/*
if ( !function_exists( 'switch_off_user' ) ) {
	
	// Switches off the current logged in user. This logs the current user out while retaining a cookie allowing them to log
	// straight back in using the 'Switch back to {user}' system.
	//
	// @return bool True on success, false on failure.
	//
	function switch_off_user() {
		if ( !$old_user_id = get_current_user_id() ) {
			return false;
		}

		user_switching_set_olduser_cookie( $old_user_id );
		wp_clear_auth_cookie();

		do_action( 'switch_off_user', $old_user_id );

		return true;
	}
}
*/


new IgniteWoo_Manual_Orders_User_Switching;
