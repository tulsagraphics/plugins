<?php
/*
	Manual/Phone Order template

	Copyright (c) 2014, 2017 - IgniteWoo.com

	IgniteWoo does not provide support for customizations!
	-----------------------------------------------------
*/

global $ign_opc_core;

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">

		<?php
		wp_head();
		?>
	</head>

	<body <?php body_class(); ?>>
		<div id="page" class="site">
			<div id="content">
			<?php 
			//the_content();
			echo do_shortcode( '[woocommerce_manual_phone_order style="searchable"]' );
			?>
			</div>
		</div>
		
		<?php wp_footer(); ?>
		
	</body>
</html>
