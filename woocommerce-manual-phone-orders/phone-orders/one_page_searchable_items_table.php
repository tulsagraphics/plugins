<?php
/* 
	Manual/Phone Order template - Table layout for controlling the list of products

	Copyright (c) 2014, 2017 - IgniteWoo.com

	IgniteWoo does not provide support for customizations!
	-----------------------------------------------------
*/


global $cart_products, $display_images, $ign_opc, $ign_opc_core, $user_ID;

if ( empty( $cart_products ) ) { 
	
	?>
	<p class="opc_cart_is_empty"><?php _e( 'The cart is currently empty', 'manual_phone_orders' )?></p>
	
	<?php
	
	return;
	
}
	

/* ***** NOTES ******

"$display_images = false" will display the display of product thumbnails

ARRAY DATA LAYOUT: 

$_product['data'] - contains the product object as set by WooCommerce get_product()

$_product['attrs'] - contains any visible attributes or variation attributes for the product. This is used to display a list of attribute names/values

$_product['url_parms'] - contains a formatted name/value pair string of attributes for a product variation

******************* */
?>
<style>
.one_page_product_table_item table {
	width: 100%;
}
</style>

<table id="opc_table_layout" class="one_page_product_list opc_manual_order_table" style="border-collapse: separate; margin-bottom:0 !important; width: 100%">
	<?php 
	$ign_opc->switch_user_context_to_customer( 'opc_user' );
	
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) { 
		// Make sure 
		$ign_opc->switch_user_context_to_customer( 'customer' );
	?>
	<tr class="one_page_product_table_item">
		<td colspan="5">
			<form action="" method="post" name="one_page_checkout_form" class="one_page_checkout_form opc_cart">
			
			<table style="border:none; border-collapse: separate; margin-bottom:0 !important"> 
			
			<?php 
			$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
			$variation_id = $cart_item['variation_id']; 
			if ( version_compare( WOOCOMMERCE_VERSION, '2.4', '>=' ) )
				$type = $_product->get_type();
			else 
				$type = $_product->product_type;
				
			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 ) {
				
				// 
				//$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
				
				$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->get_permalink( $cart_item ), $cart_item, $cart_item_key );

				$attrs = $cart_item['data']->get_attributes();
				
				if ( empty( $attrs ) )
					$attrs = array();
					
				$data_attrs = $ign_opc_core->array_implode( '=', '&', $attrs );

				$class = ( 'simple' == $type ) ? 'opc_product_type_simple' : 'opc_product_type_variable';

				?>
				<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
				
					<td class="product-remove" style="width:2%;">
						<?php
							if ( function_exists( 'wc_get_cart_remove_url' ) )
								$remove_url = wc_get_cart_remove_url( $cart_item_key );
							else 
								$remove_url = WC()->cart->get_remove_url( $cart_item_key );
							
							$remove_link = apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
								'<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
								esc_url( $remove_url ),
								__( 'Remove this item', 'manual_phone_orders' ),
								esc_attr( $product_id ),
								esc_attr( $_product->get_sku() )
							), $cart_item_key );
							
							if ( false !== $pos = strpos( $remove_link, '<a' ) ) {
								$data = '<a data-product_id="' . $product_id . '" data-variation_id="' .  $variation_id . '"';
								$remove_link = preg_replace( '/<a/', $data, $remove_link );
								
								$data = 'class="remove opc_remove_from_cart ';
								$remove_link = preg_replace( '/class="/', $data, $remove_link );
							}
							
							echo $remove_link;
						?>
						
					</td>

					<td class="product-thumbnail" style="width:10%;">
						<?php
						
							
							$image = get_the_post_thumbnail( $product_id, apply_filters( 'single_product_large_thumbnail_size', 'shop_thumbnail' ), array() );
							
							if ( $image && !is_wp_error( $image ) ) {
								echo $image;
							} else {
								echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" style="width:75px" />', wc_placeholder_img_src(), __( 'Placeholder', 'manual_phone_orders' ) ), $product_id );
							}
		
							/* If you want the image linked to the single product page use this instead: 
							$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

							if ( ! $product_permalink ) {
								echo $thumbnail;
							} else {
								printf( '<a href="%s" target="_blank">%s</a>', esc_url( $product_permalink ), $thumbnail );
							}
							*/
						?>
					</td>

					<td class="product-name" data-title="<?php _e( 'Product', 'manual_phone_orders' ); ?>" >
						<?php
						
							if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) ) { 
								$data = $ign_opc_core->get_product_name( $_product );
								$title = $data['name'];
							} else { 
								$title = $_product->get_title(); 
							}
								
							if ( ! $product_permalink ) {
								echo apply_filters( 'woocommerce_cart_item_name', $title, $cart_item, $cart_item_key ) . '&nbsp;';
							} else {
								echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $title ), $cart_item, $cart_item_key );
							}

							// Meta data
							if ( function_exists( 'wc_get_formatted_cart_item_data' ) )
								$item_data = wc_get_formatted_cart_item_data( $cart_item );
							else 
								$item_data = WC()->cart->get_item_data( $cart_item );
								
							echo $item_data;

							// Backorder notification
							if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
								echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'manual_phone_orders' ) . '</p>';
							}
						?>
					</td>

					<td class="product-price" data-title="<?php _e( 'Price', 'manual_phone_orders' ); ?>" style="width:25%;">
						<?php
						// Ask for product price so that plugins that don't display it can be detected 
						// by virtue of not returning a price. 
						$the_price = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); 
						
						//$the_price = $_product->get_price();
						
						// $the_price plain :
						
						$the_price_amt = strip_tags( $the_price ); 
						$the_price_amt = str_replace( get_woocommerce_currency_symbol(), '', $the_price_amt );

						?>
						
						<div>
							<?php 
						
							if ( $ign_opc_core->user_can_change_price() && !empty( $the_price ) && in_array( $type, array( 'simple', 'variation' ) ) ) { 	
								
								// Get regular price, without any discounts or alterations hopefully
								$price = $cart_item['data']->get_regular_price(); 
								//$price = isset( $cart_item['_price_altered'] ) ? $cart_item['_price_altered'] : $cart_item['data']->get_price(); 
							
								// Display the regular price 
								if ( !empty( $the_price ) && ( wc_price( wc_format_localized_price( $price ) ) !== $the_price ) ) { 
								
									echo '<strike>' . wc_price( wc_format_localized_price( $price ) ) . '</strike>&nbsp; ';
									echo wc_format_localized_price( $the_price );
								} else { 
									echo wc_price( wc_format_localized_price( $price ) );
								}
								echo '&nbsp;';
								// Store the price in a hidden field. Then display a field where the price can be changed. 
								// When changed the hidden opc_price field is updated via JS since it is used as the product price. 
								// If this field isn't changed then all other pricing filters operate as expected,
								// or if this field IS changed then phone/manual orders should in theory be able to enforce the edited price since its filters
								// try to run last among all price filters on a site.
								?>
								<input type="hidden" class="small opc_price" value="<?php echo wc_format_localized_price( $the_price_amt ) ?>">
								<input type="hidden" class="small opc_regular_price" value="<?php echo wc_format_localized_price( $the_price_amt ) ?>">
								<input type="text" class="small opc_changeable_price" value="">
							<?php 
							} else {

								$price = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
							
								echo wc_format_localized_price( $price ); 
							}
							?>
							
						</div>
					</td>

					<td class="product-quantity" data-title="<?php _e( 'Quantity', 'manual_phone_orders' ); ?>" style="width:15%;">
						<?php  if ( in_array( $type, array( 'simple', 'variation' ) ) ) {
						
							$min_value = apply_filters( 'woocommerce_quantity_input_min', 0, $cart_item['data'] );
							
							$max_value = ''; //apply_filters( 'woocommerce_quantity_input_max', $_product['data']->backorders_allowed() ? '' : $_product['data']->get_stock_quantity(), $_product['data'] );
							
							$qty = IGN_Manual_Phone_Orders_Core::get_qty_in_cart( $product_id, $variation_id );
							
							$max = method_exists( $_product, 'get_max_purchase_quantity' ) ? $_product->get_max_purchase_quantity() : $_product->get_stock_quantity();
							
							$product_quantity = woocommerce_quantity_input( array(
									'input_name'  => "cart[{$cart_item_key}][qty]",
									'input_value' => $cart_item['quantity'],
									'max_value'   => $_product->backorders_allowed() ? '' : $max,
									'min_value'   => '0',
								), $_product, false );
							
							// Add data attributes for JS
							if ( false !== $pos = strpos( $product_quantity, '<input' ) ) {
								
								//$data = '<input data-product_id="' . $product_id . '" data-variation_id="' .  $variation_id . '" data-attributes="' . esc_html( $data_attrs ) . '"';
								
								// Ryder Ross contributed this tweak, handles attributes with $ followed by numbers
								$escaped_attrs = preg_replace('/\$(\d)/', '\\\$$1', esc_html($data_attrs));
								
								
								$data = '<input data-product_id="' . $product_id . '" data-variation_id="' .  $variation_id . '" data-attributes="' . $escaped_attrs . '" data-cart_item_key="' . $cart_item_key . '"';

								$product_quantity = preg_replace( '/<input/', $data, $product_quantity );
							}
							
							echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
							
						} else { 
							echo $cart_item['quantity'];
						}
						?>
					</td>
				</tr>
				<?php } ?>
			</table>
			</form>
		</td>
	<?php } ?>
	</tr>
</table>

<div class="cart_contents_buttons" style="text-align:right">
	<input type="button" class="clear_entire_cart button" value="<?php _e( 'Clear entire cart', 'manual_phone_orders' ) ?>" />
	<?php if ( !empty( $ign_opc_core->settings['disable_autorefresh'] ) && 'yes' == $ign_opc_core->settings['disable_autorefresh'] ) { ?>
		<input type="button" class="update_the_cart_totals_cart button" value="<?php _e( 'Update cart totals', 'manual_phone_orders' ) ?>" />
	<?php } ?>
</div>

<script>
jQuery( document ).ready( function() { 
	//one_page_remove_from_cart();
})
</script>
