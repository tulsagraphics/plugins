<?php
/**
 * Checkout shipping information form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */
global $ign_opc, $user_ID;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
if ( function_exists( 'wc_ship_to_billing_address_only' ) )
	$ship_to_billing_address_only = wc_ship_to_billing_address_only();
else 
	$ship_to_billing_address_only = WC()->cart->ship_to_billing_address_only();
?>
<div class="woocommerce-shipping-fields">

	<?php
		if ( empty( $_POST ) || isset( $_POST['opc_customer_selected'] ) ) {
			$ship_to_different_address = get_option( 'woocommerce_ship_to_destination' ) === 'shipping' ? true : false;
			$ship_to_different_address = apply_filters( 'woocommerce_ship_to_different_address_checked', $ship_to_different_address );

		} else {
			$ship_to_different_address = $checkout->get_value( 'ship_to_different_address' );
		}
	?>

	<h3 id="ship-to-different-address">
		<label for="ship-to-different-address-checkbox" class="checkbox"><?php _e( 'Ship to a different address?', 'manual_phone_orders' ); ?></label>
		<input id="ship-to-different-address-checkbox" class="input-checkbox" <?php checked( $ship_to_different_address, 1 ); ?> type="checkbox" name="ship_to_different_address" value="1" />
	</h3>

	<div class="shipping_address">

		<?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>

		<?php 
		// Ensure the correct user account is loaded and WC loads a customer object for that user
		if ( !empty( $_POST['one_page_customer_user'] ) ) {
			$ign_opc->switch_user_context_to_customer( 'customer' );
			WC()->customer = new WC_Customer( $user_ID ); 
		}
		?>
		
		<?php foreach ( $checkout->checkout_fields['shipping'] as $key => $field ) : ?>

			<?php 
			
			// Make sure when "Guest" is selected the form fields are emptied
			if ( empty( $_POST['one_page_customer_user'] ) )
				$val = '';
			// Or if the user has no billing info leave fields empty
			else if ( !empty( $_POST['one_page_customer_user'] ) && false == get_user_meta( absint( $_POST['one_page_customer_user'] ), 'billing_first_name', true ) )
				$val = '';
			else 
				$val = $checkout->get_value( $key );

			$default = !empty( $ign_opc_core->settings['default_country'] ) ? explode( ':', $ign_opc_core->settings['default_country'] ) : array();

			if ( empty( $val ) && 'billing_state' == $key ) 
				$val = !empty( $default[1] ) ? $default[1] : 'TX';
			else if ( empty( $val ) && 'billing_country' == $key ) 
				$val = !empty( $default[0] ) ? $default[0] : 'US';
		
			woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
			
			?>

		<?php endforeach; ?>
		
		<?php 
			// Switch back to the currently logged in user
			$ign_opc->switch_user_context_to_customer( 'opc_user' ); 
			WC()->customer = new WC_Customer( $user_ID ); 
		?>

		<?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>

	</div>


	<?php do_action( 'woocommerce_before_order_notes', $checkout ); ?>

	<?php if ( apply_filters( 'woocommerce_enable_order_notes_field', get_option( 'woocommerce_enable_order_comments', 'yes' ) === 'yes' ) ) : ?>

		<?php if ( ! WC()->cart->needs_shipping() || $ship_to_billing_address_only ) : ?>

			<h3><?php _e( 'Additional Information', 'manual_phone_orders' ); ?></h3>

		<?php endif; ?>

		<?php foreach ( $checkout->checkout_fields['order'] as $key => $field ) : ?>

			<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

		<?php endforeach; ?>

	<?php endif; ?>

	<?php do_action( 'woocommerce_after_order_notes', $checkout ); ?>
</div>
