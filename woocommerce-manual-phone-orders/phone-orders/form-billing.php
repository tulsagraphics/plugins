<?php
/**
 * Checkout billing information form
 *
 * @author 	WooThemes & IgniteWoo
 * @package 	Manual-Orders-Phone-Orders/Templates
 * @version     2.1.2
 * @mods	Modified to always show account creation fields
 * @use-with	For use with IgniteWoo Manual Order & Phone Order extension for WooCommerce
 */

global $ign_opc, $ign_opc_core, $user_ID;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( function_exists( 'wc_ship_to_billing_address_only' ) )
	$ship_to_billing_address_only = wc_ship_to_billing_address_only();
else 
	$ship_to_billing_address_only = WC()->cart->ship_to_billing_address_only();
	
/** @global WC_Checkout $checkout */
?>
<div class="woocommerce-billing-fields">
	<?php if ( $ship_to_billing_address_only && WC()->cart->needs_shipping() ) : ?>

		<h3><?php _e( 'Billing &amp; Shipping', 'manual_phone_orders' ); ?></h3>

	<?php else : ?>

		<h3><?php _e( 'Billing Details', 'manual_phone_orders' ); ?></h3>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

	<?php 
	// Ensure the correct user account is loaded and WC loads a customer object for that user
	if ( !empty( $_POST['one_page_customer_user'] ) ) {
		$ign_opc->switch_user_context_to_customer( 'customer' );
		WC()->customer = new WC_Customer( $user_ID ); 
	}
	?>
	
	<?php foreach ( $checkout->checkout_fields['billing'] as $key => $field ) : ?>

		<?php 
			// Make sure when "Guest" is selected the form fields are emptied
			if ( empty( $_POST['one_page_customer_user'] ) )
				$val = '';
			// Or if the user has no billing info leave fields empty
			else if ( !empty( $_POST['one_page_customer_user'] ) && false == get_user_meta( absint( $_POST['one_page_customer_user'] ), 'billing_first_name', true ) )
				$val = '';
			else 
				$val = $checkout->get_value( $key );

			$default = !empty( $ign_opc_core->settings['default_country'] ) ? explode( ':', $ign_opc_core->settings['default_country'] ) : array();

			if ( empty( $val ) && 'billing_state' == $key ) 
				$val = !empty( $default[1] ) ? $default[1] : 'TX';
			else if ( empty( $val ) && 'billing_country' == $key ) 
				$val = !empty( $default[0] ) ? $default[0] : 'US';

			woocommerce_form_field( $key, $field, $val ); 
		?>

	<?php endforeach; ?>

	<?php 
		// Switch back to the currently logged in user
		$ign_opc->switch_user_context_to_customer( 'opc_user' ); 
		WC()->customer = new WC_Customer( $user_ID ); 
	?>
	
	<?php do_action('woocommerce_after_checkout_billing_form', $checkout ); ?>

	<?php 

	if ( empty( $_POST ) || empty($_POST['one_page_customer_user'] ) || ( isset( $_POST['one_page_customer_user'] ) && '' == $_POST['one_page_customer_user'] ) ) : 
		?>
		<p class="form-row form-row-wide create-account">
			<input class="input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true) ?> type="checkbox" name="createaccount" value="1" style="vertical-align:text-top;margin-right:5px"/> 
			<label for="createaccount" class="checkbox">
				<?php _e( 'Create an account?', 'manual_phone_orders' ); ?>
			</label>
		</p>

		<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

		<?php if ( ! empty( $checkout->checkout_fields['account'] ) ) : ?>

			<div class="create-account">

				<p><?php _e( 'Fill in the information below:', 'manual_phone_orders' ); ?></p>

				<?php foreach ( $checkout->checkout_fields['account'] as $key => $field ) : ?>

					<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

				<?php endforeach; ?>

				<div class="clear"></div>

			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>

	<?php endif; ?>
</div>
