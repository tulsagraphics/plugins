<?php /*

Insert a hidden link that for which a click is automatically trigged any time the cart is updated.

This relies on the prettyPhoto Javascript included with WooCommerce for its functionality.

CSS to control font etc is in the plugin's CSS file

*/ ?>
<a style="display:none" id="pp_added" href="#ign_one_page_cart_updated"></a>
<div id="ign_one_page_cart_updated" class="hide" style="display:none">
	<div class="ign_one_page_cart_updated_wrap">
		<p><?php _e( 'Cart updated', 'manual_phone_orders' ) ?></p>
	</div>
</div>
<?php /* 
DO NOT REMOVE THE FOLLOWING JAVASCRIPT VAR 

This controls how long the notice appears on the screen. The value is in milliseconds. 1000 = 1 second
*/ ?>
<script>
ign_cart_updated_notice_timeout = 1250
</script>
