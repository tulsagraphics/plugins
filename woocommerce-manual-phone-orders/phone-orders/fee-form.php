<div class="opc_add_fee_wrap">
	<p class="fee_heading" style="font-size:1.1em"><strong><?php _e( 'Add Fees', 'manual_phone_orders' ) ?></strong></p>
	<p style="font-style:italic"><?php _e( 'If you want to add an existing customer to the order do that before adding fees!', 'manual_phone_orders' ) ?></p>
	<div id="fee_form">
		<input type="text" value="" name="fee_name" class="fee_name" value="" placeholder="<?php _e( 'Fee label (must be unique)', 'manual_phone_orders' )?>">
		<input type="text" value="" name="fee_amount" class="fee_amount" value="" placeholder="<?php _e( 'Amount', 'manual_phone_orders' )?>" >
		<input type="checkbox" value="" name="fee_taxable" class="fee_taxable" style="vertical-align:middle"> <?php _e( 'Taxable?', 'manual_phone_orders' )?> &nbsp; 
		<input type="submit" value="<?php _e( 'Add fee', 'manual_phone_orders' ) ?>" name="fee_submit" class="fee_submit">
	</div>
</div>
<div class="opc_fee_wrap" style="display:none">
	<p class="fee_heading"><strong><?php _e( 'FEES', 'manual_phone_orders' ) ?></strong></p>
	<table class="opc_fee_table">
		<thead> 
			<tr>
				<th style="width:5%"><?php _e( 'Action', 'manual_phone_orders' ) ?></th>
				<th><?php _e( 'Label', 'manual_phone_orders' ) ?></th>
				<th><?php _e( 'Amount', 'manual_phone_orders' ) ?></th>
				<th><?php _e( 'Taxable', 'manual_phone_orders' ) ?></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
