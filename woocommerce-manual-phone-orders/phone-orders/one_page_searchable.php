<?php
/*
	Manual/Phone Order template

	Copyright (c) 2014, 2017 - IgniteWoo.com

	IgniteWoo does not provide support for customizations!
	-----------------------------------------------------
*/

// disable the problematic Storefront sticky order review stuff
wp_deregister_script( 'storefront-sticky-payment' );

global $products, $ign_opc_core;

// Set this value to false to remove product thumbnails
$display_images = false;

?>

<script>
	// empty cart when new items are added from this page
	ign_opc_empty_cart = true;
</script>

<?php
wp_register_script( 'jquery-tiptip', WC()->plugin_url() . '/assets/js/jquery-tiptip/jquery.tipTip.js', array( 'jquery' ), WC_VERSION, true );
wp_enqueue_script( 'jquery-tiptip' );
// Forms below to select a customer and products
?>

<script>
jQuery( document ).ready( function($) {
	var tiptip_args = {
		'attribute': 'data-tip',
		'fadeIn': 50,
		'fadeOut': 50,
		'delay': 200
	};
	$( '.woocommerce-help-tip' ).tipTip( tiptip_args );
})
</script>

<div class="woocommerces one_page_checkout_page">

	<div class="woocommerce-info"><a href="#" class="opc_load_order_link"><?php _e( 'Click here to load an existing order', 'manual_phone_orders' ) ?></a></div>

	<form id="one_page_load_order" style="display:none">
		<?php wp_nonce_field( 'woocommerce-order_again' ); ?>
		<div style="clear:both; margin-left: 10px; height: 33px">

			<?php if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) ) { ?>
				<select class="opc-order-search" id="order_search" name="one_page_load_order_id" data-placeholder="<?php _e( 'Order # or customer name', 'manual_phone_orders' ) ?>" data-selected="" value="" data-allow_clear="true" data-minimum_input_length="2" style="width:235px;float:left"></select>
			<?php } else { ?>
				<input class="opc-order-search" id="order_search" name="one_page_load_order_id" data-placeholder="<?php _e( 'Order # or customer name', 'manual_phone_orders' ) ?>" data-selected="" value="" data-allow_clear="true" data-minimum_input_length="2" style="width:235px;float:left"/>
			<?php } ?>

			<input type="checkbox" class="one_page_load_order_id_as" name="one_page_load_order_id_as" value="overwrite" style="margin-right:7px; margin-left: 10px; vertical-align:middle"><?php _e( 'Load and overwrite', 'manual_phone_orders' ) ?>

			<span class="woocommerce-help-tip dashicons dashicons-editor-help" data-tip="<?php _e( 'When this box is checked the selected order will be loaded, its status will be immediately set to \'Pending Payment\', and when you submit the order the existing order will be permanently and completely overwritten!', 'manual_phone_orders' ) ?>"></span>

			<a href="#" class="opc_load_order button" style="margin-left: 10px"><?php _e( 'Load order', 'manual_phone_orders' ) ?></a>
		</div>
	</form>

<form id="one_page_searchable_customer_search" action="" method="post">

	<h3><?php _e( 'Choose Customer', 'manual_phone_orders' )?> &nbsp; (<span class="opc_choose_products_desc">Search by user login, first or last name, or ID</span>)</h3>
	<?php
	$user_string = '';
	$userid     = '';
	if ( !empty( $_POST['one_page_customer_user'] ) || !empty( $_POST['ignitewoo_one_page_checkout_customer_id'] ) ) {
		$userid = !empty( $_POST['one_page_customer_user'] ) ? absint( $_POST['one_page_customer_user'] ) : absint( $_POST['ignitewoo_one_page_checkout_customer_id'] );
		$user = get_user_by( 'id', $userid );
		$user_string = esc_html( $user->display_name ) . ' (#' . absint( $user->ID ) . ' &ndash; ' . esc_html( $user->user_email ) . ')';
		$placeholder = esc_attr( 'Guest', 'woocommerce' );
	} else {
		$placeholder = esc_attr( 'Guest', 'woocommerce' );
	}
	wp_enqueue_script( 'select2js', WC()->plugin_url() . '/assets/js/select2/select2.min.js', array( 'jquery' ), WC()->version, true );
	wp_enqueue_style( 'select2css', WC()->plugin_url() . '/assets/css/select2.css', array(), WC()->version, true );

	if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '<' ) ) {
	?>
		<input type="hidden" class="opc-customer-search" id="customer_user" name="one_page_customer_user" data-placeholder="<?php echo $placeholder ?>" data-selected="<?php echo htmlspecialchars( $user_string ); ?>" value="<?php echo $userid; ?>" data-allow-clear="true" />

	<?php } else { ?>

		<select  class="opc-customer-search" id="customer_user" name="one_page_customer_user" data-placeholder="<?php echo $placeholder ?>" data-allow-clear="true" />
			<?php if ( !empty( $user_string ) && !empty( $userid ) ) { ?>
				<option value="<?php echo $userid; ?>"><?php echo htmlspecialchars( $user_string ); ?></option>
			<?php } ?>
		</select>

	<?php } ?>

	<p>
		<input type="submit" class="submit button" style="margin-top:14px;float:none !important" value="<?php _e( "Assign Customer to Order", 'manual_phone_orders' )?>">
		<?php IGN_Manual_Phone_Orders_Core::add_tax_exempt_field() ?>
		<?php wp_nonce_field( "opc_customer_selected", "opc_customer_selected" ); ?>
	</p>

	<h3>
		<?php _e( 'Choose Products', 'manual_phone_orders' )?> &nbsp; (<span class="opc_choose_products_desc"><?php _e( 'Search by title or SKU, or narrow by category and title', 'manual_phone_orders' ) ?></span>) &nbsp; <span><input type="button" href="#arbitrary_form_wrap" class="opc_arbitrary opc_arbitrary_product_btn button" value="<?php _e( 'Add arbitrary product', 'manual_phone_orders' ) ?>"></span>
	</h3>
	
	<?php IGN_Manual_Phone_Orders_Core::get_chosen_product_input(); ?>

	<p>
		<input type="submit" class="submit button one_page_searchable_add_btn" value="<?php _e( "Add to Order", 'manual_phone_orders' )?>" style="display:none">
		<input type="button" class="submit button one_page_configure_product" value="<?php _e( "Configure product", 'manual_phone_orders' )?>" style="display:none">
	</p>

</form>

<?php // Placeholder for the order item table controls ?>
<div id="opc_searchable_layout">
	<?php IGN_Manual_Phone_Orders_Core::display_cart_contents_table(); ?>
</div>

<div style="clear:both; margin-bottom: 1em"></div>

<?php $ign_opc_core->load_template( 'fee-form.php' ); ?>

<?php
/* Add arbitrary product form - Displayed via the popup script */
?>

<div id="arbitrary_form_wrap" style="display:none">
	<form class="arbitrary_form" id="arbitrary_form" action="" method="post">
		<p style="margin-top:18px;font-style:italic">
			<?php _e( "Add a product to the order that doesn't exist in your store catalog yet.", 'manual_phone_orders' ) ?>
			<br/>
			<?php _e( 'Title, quantity, and price are required. To make an item free set to the price to 0 (zero).', 'manual_phone_orders' ) ?>
		</p>
		<table style="border:none;margin-bottom: 0;border-collapse: collapse;">
		<tr>
			<td><?php _e( 'Title', 'manual_phone_orders' ) ?></td>
			<td><input class="arbitrary_title" type="text" name="title" value="" style="width:100%" placeholder="<?php _e( 'Enter a title', 'manual_phone_orders' ) ?>"></td>
		</tr>
		<tr>
			<td><?php _e( 'SKU', 'manual_phone_orders' ) ?></td>
			<td><input class="arbitrary_sku" type="text" name="sku" value="" style="width:50%"></td>
		</tr>
		<tr>
			<td><?php _e( 'Quantity', 'manual_phone_orders' ) ?></td>
			<td><input class="arbitrary_qty wc_input_decimal" type="number" name="quantity" value="1" min="1" ></td>
		</tr>
		<tr>
			<td><?php _e( 'Price', 'manual_phone_orders' ) ?></td>
			<td><input class="arbitrary_price wc_input_decimal" type="text" name="price" value=""></td>
		</tr>
		<tr>
			<td><?php _e( 'Weight', 'manual_phone_orders' ) ?></td>
			<td><input class="arbitrary_weight wc_input_decimal" type="text" name="weight" value=""></td>
		</tr>
		<tr>
			<td><?php _e( 'Dimensions', 'manual_phone_orders' ) ?></td>
			<td>
				<p class="form-field dimensions_field">
					<span class="wrap">
						<input id="product_length" placeholder="Length" class="input-text wc_input_decimal" size="6" name="length" value="" type="text">
						<input placeholder="Width" class="input-text wc_input_decimal" size="6" name="width" value="" type="text">
						<input placeholder="Height" class="input-text wc_input_decimal last" size="6" name="height" value="" type="text">
					</span>
				</p>
			</td>
		</tr>
		<tr>
			<td></td>
			<td style="text-align:right">
				<button class="opc_arbitrary_cancel button" style="background-color:#ececec;color:#333" onclick="this.form.reset();"><?php _e( 'Cancel', 'manual_phone_orders' ) ?></button>
				<button class="opc_arbitrary_submit button"><?php _e( 'Add to order', 'manual_phone_orders' ) ?></button>
			</td>
		</tr>
		</table>
	</form> 
</div>
