<?php
/**
Copyright (c) 2014 - 2017 - IgniteWoo.com - All Rights Reserved
*/

class IGN_Manual_Phone_Orders_Core {

	var $opc = false;

	var $style = '';

	var $products = array();

	var $settings = false;

	function __construct() {

		$this->shortcode = 'woocommerce_manual_phone_order';

		$this->plugin_url = plugins_url( __FILE__ );

		add_action( 'init', array( &$this, 'remove_hooks' ), 10 );

		add_action( 'init', array( &$this, 'init' ), 5 );

		add_action( 'wp_loaded', array( &$this, 'adjust_hooks' ), 5 );

		add_action( 'admin_enqueue_scripts', array( &$this, 'admin_head' ), 1 );

		add_shortcode( $this->shortcode, array( &$this, 'one_page' ), 1 ,1 );

		//add_filter( 'woocommerce_get_checkout_page_id', array( &$this, 'get_checkout_page_id' ), 1, 9999999 );

	}

	public static function plugin_url() {
		return untrailingslashit( plugins_url( '/', __FILE__ ) );
	}


	public static function plugin_path() {
		return untrailingslashit( plugin_dir_path( __FILE__ ) );
	}

	public function init() {

		if ( !$this->has_access() )
			return;

		require_once( 'ign-compat-functions.php' );

		add_action( 'wp', array( &$this, 'check_for_shortcode' ), 1 );

		add_action( 'wp_head', array( &$this, 'no_cache' ), -99 );
		add_action( 'wp_head', array( &$this, 'wp_head' ), 1 );

		add_action( 'init', array( &$this, 'is_tax_exempt' ), 99999999 );
		add_action( 'woocommerce_init', array( &$this, 'is_tax_exempt' ), 99999999 );

		add_action( 'woocommerce_before_checkout_form', array( &$this, 'before_checkout_form' ), 99999999 );

		add_action( 'woocommerce_checkout_order_review', array( &$this, 'checkout_after_order_review' ), 1 );

		//add_action( 'woocommerce_checkout_before_order_review', array( &$this, 'checkout_before_order_review' ), 1 );

		add_action( 'woocommerce_before_checkout_process', array( &$this, 'before_checkout_process' ), -99 );

		//add_filter( 'woocommerce_is_checkout', array( &$this, 'is_checkout' ), 999, 9999999 );
		//add_action( 'wp_enqueue_scripts', array( $this, 'maybe_add_checkout_hook' ), -99 );

		add_action( 'woocommerce_add_to_cart', array( &$this,'maybe_save_new_price' ), 1, 5 );

		add_action( 'woocommerce_before_calculate_totals', array( &$this, 'maybe_calculate_new_prices' ), 1, 1 );

		if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) ) {
			add_filter( 'woocommerce_product_get_price', array( &$this, 'maybe_return_price' ), 9999999, 2 );
			add_filter( 'woocommerce_get_product_price', array( &$this, 'maybe_return_price' ), 999, 2 );
			add_filter( 'woocommerce_product_variation_get_price', array( &$this, 'maybe_return_price' ), 999, 2 );	
		} else {
			add_filter( 'woocommerce_get_price', array( &$this, 'maybe_return_price' ), 9999999, 2 );
		}

		add_action( 'wp_ajax_woocommerce_add_to_cart_opc_variable', array( &$this, 'add_variation_to_cart' ), 1 );
		add_action( 'wp_ajax_nopriv_woocommerce_add_to_cart_opc_variable', array( &$this, 'add_variation_to_cart' ), 1 );

		add_action( 'wp_ajax_woocommerce_add_to_cart_opc', array( &$this, 'add_item_to_cart' ), 1 );
		add_action( 'wp_ajax_nopriv_woocommerce_add_to_cart_opc', array( &$this, 'add_item_to_cart' ), 1 );
		
		// Updates multiple items in the cart with one Ajax call via the Update the Cart button
		add_action( 'wp_ajax_woocommerce_update_the_cart_opc', array( &$this, 'update_items_to_cart' ), 1 );
		add_action( 'wp_ajax_nopriv_woocommerce_update_the_cart_opc', array( &$this, 'update_items_to_cart' ), 1 );
		
		add_action( 'wp_ajax_woocommerce_add_to_cart_opc_donation', array( &$this, 'add_to_cart_donation' ), 1 );
		add_action( 'wp_ajax_nopriv_woocommerce_add_to_cart_opc_donation', array( &$this, 'add_to_cart_donation' ), 1 );

		add_action( 'wp_ajax_woocommerce_add_to_cart_opc_gift_cert', array( &$this, 'add_to_cart_gift_cert' ), 1 );
		add_action( 'wp_ajax_nopriv_woocommerce_add_to_cart_opc_gift_cert', array( &$this, 'add_to_cart_gift_cert' ), 1 );

		add_action( 'wp_ajax_woocommerce_add_to_cart_opc_product_remove', array( &$this, 'add_to_cart_product_remove' ), 1 );
		add_action( 'wp_ajax_nopriv_woocommerce_add_to_cart_opc_product_remove', array( &$this, 'add_to_cart_product_remove' ), 1 );

		//add_action( 'wp_ajax_woocommerce_add_to_cart_opc_product_remove', array( &$this, 'add_to_cart_product_remove' ), 1 );
		//add_action( 'wp_ajax_nopriv_woocommerce_add_to_cart_opc_product_remove', array( &$this, 'add_to_cart_product_remove' ), 1 );

		//add_action( 'wp_ajax_woocommerce_update_order_review', array( &$this, 'update_order_review' ), -1 );
		//add_action( 'wp_ajax_nopriv_update_order_review', array( &$this, 'update_order_review' ), -1 );

		add_action( 'wp_ajax_get_refreshed_fragments', array( &$this, 'get_refreshed_fragments' ), -1 );
		add_action( 'wp_ajax_nopriv_get_refreshed_fragments', array( &$this, 'get_refreshed_fragments' ), -1 );

		add_action( 'wp_ajax_ign_one_page_searchable_add', array( &$this, 'ign_one_page_searchable_add' ), -1 );
		add_action( 'wp_ajax_nopriv_ign_one_page_searchable_add', array( &$this, 'ign_one_page_searchable_add' ), -1 );

		add_action( 'wp_ajax_opc_json_search_products_and_variations', array( &$this, 'json_search_products_and_variations' ), -1 );
		add_action( 'wp_ajax_nopriv_opc_json_search_products_and_variations', array( &$this, 'json_search_products_and_variations' ), -1 );

		add_action( 'wp_ajax_opc_add_fee', array( &$this, 'maybe_check_fee' ), -1 );
		add_action( 'wp_ajax_nopriv_opc_add_fee', array( &$this, 'maybe_check_fee' ), -1 );
		
		
		add_action( 'wp_ajax_add_arbitrary_product', array( &$this, 'maybe_add_arbitrary_product' ), -1 );
		add_action( 'wp_ajax_nopriv_add_arbitrary_product', array( &$this, 'maybe_add_arbitrary_product' ), -1 );
		
		// Remove arbitrary products after the order is processing or completed
		add_action( 'woocommerce_order_status_completed', array( &$this, 'maybe_delete_arbitrary_product' ), 9999999, 1 );
		add_action( 'woocommerce_order_status_processing', array( &$this, 'maybe_delete_arbitrary_product' ), 9999999, 1 );
		register_activation_hook( __FILE__, array( &$this, 'maybe_schedule_cron_hook' ) );
		add_action( 'remove_arbitrary_products_hook', array( &$this, 'remove_arbitrary_products' ) );
						
		//add_action( 'pre_user_query', array( &$this, 'customer_query_meta' ), -1 );

		add_action( 'woocommerce_cart_calculate_fees', array( &$this, 'maybe_add_fee' ) );

		// Load and work with an existing order
		add_action( 'wp_ajax_opc_json_search_orders', array( &$this, 'json_search_orders' ), -1 );
		add_action( 'wp_ajax_nopriv_opc_json_search_orders', array( &$this, 'json_search_orders' ), -1 );
		add_action( 'wp_ajax_opc_load_order', array( &$this, 'load_order' ), -1 );
		add_action( 'wp_ajax_nopriv_opc_load_order', array( &$this, 'load_order' ), -1 );
		add_action( 'woocommerce_create_order', array( &$this, 'maybe_alter_order_status' ), 2, -1 );

		add_action( 'wp_ajax_ign_opc_clear_cart', array( &$this, 'clear_cart' ), -1 );
		add_action( 'wp_ajax_nopriv_ign_opc_clear_cart', array( &$this, 'clear_cart' ), -1 );
		
		
		// Popup stuff
		add_action( 'wp_ajax_ign_one_page_configure_product', array( &$this, 'configure_product' ), -1 );
		// not implemented yet
		//add_action( 'wp_ajax_ign_one_page_check_product_type', array( &$this, 'check_product_type' ), -1 );
		add_action( 'wp_enqueue_scripts', array( &$this, 'maybe_inject_prettyphoto_css' ), 999999 );
		add_action( 'wp_ajax_ign_one_page_update_items_table', array( &$this, 'update_items_table' ), -1 );

		add_action( 'woocommerce_thankyou', array( &$this, 'thankyou_page_additions' ), 50 );

		add_action( 'woocommerce_checkout_billing', array( &$this, 'checkout_billing' ), -1 );

		add_action( 'woocommerce_checkout_shipping', array( &$this, 'checkout_shipping' ), -1 );

		// DEPRECATED
		//add_filter( 'woocommerce_json_search_customers_query', array( &$this, 'filter_user_search' ), 1, 4 );
		//add_filter( 'woocommerce_customer_search_customers', array( &$this, 'filter_user_search' ), 1, 4 );
		
		// WC 3.0 and newer (there was no 2.7 etc.)
		add_action( 'wp_ajax_opc_json_search_customers', array( &$this, 'json_search_customers' ), -1 );
		add_action( 'wp_ajax_nopriv_opc_json_search_customers', array( &$this, 'json_search_customers' ), -1 );
		
		add_action( 'admin_menu', array( &$this, 'add_new_order_link' ), 9999 );

		add_action( 'woocommerce_review_order_before_order_total', array( &$this, 'add_shipping_form' ), 1 );

		add_action( 'wp_ajax_set_custom_shipping_cost', array( &$this, 'set_custom_shipping_cost' ) );
		add_action( 'wp_ajax_nopriv_set_custom_shipping_cost', array( &$this, 'set_custom_shipping_cost' ) );

		add_action( 'woocommerce_checkout_update_order_meta', array( &$this, 'add_clerk_info' ), 5, 2 );
		add_action( 'add_meta_boxes', array( &$this, 'clerk_info_box' ), 10, 2 );

		// Prevent redirect to cart if that setting is on in WC and the popup configurator is being used
		if ( !empty( $_POST['ign_one_page_popup'] ) )
			add_filter( 'option_woocommerce_cart_redirect_after_add', array( $this, 'prevent_redirect_after_add' ) );

		$this->settings = get_option( 'woocommerce_ignitewoo_manual_order_settings' );

		add_filter( 'template_include', array( &$this, 'maybe_get_phone_manual_order_page_template' ), 99, 1 );

	}

	// Checks product type so that JS can hide show add to order or configure buttons
	function check_product_type() {
		if ( empty( $_POST['id'] ) || absint( $_POST['id'] ) <= 0 )
			die( '-1' );

		$p = wc_get_product( absint( $_POST['id'] ) );

		if ( $p->is_type( 'simple' ) || $p->is_type( 'variation' ) )
			die( 'simple' );

		die( 'other' );
	}

	function configure_product() {
		//global $post, $product, $wp_query;

		if ( empty( $_POST['items'] ) || absint( $_POST['items'] ) <= 0 )
			die( '-1' );

		if ( is_array( $_POST['items'] ) )
			$item = $_POST['items'][0];
		else
			$item = $_POST['items'];

		$link = get_permalink( absint( $item ) );

		if ( empty( $link ) )
			die( '-2' );

		if ( !empty( $_SERVER['HTTPS'] ) && 'off' !== $_SERVER['HTTPS'] )
			$link = str_replace( 'http://', 'https://', $link );

		$args = array(
			'ign_opc_pop' => 1,
			'iframe' => 'true',
			'width' => '100%',
			'height' => '100%'
		);

		$link = add_query_arg( $args, $link );

		ob_start();

		?>
		<a class="ign_opc_open_popup_link" href="<?php echo $link ?>" title="<?php _e( 'Choose your options, add the item to the cart, then close this window. Note that any special pricing for the customer will be reflected after you add the items to the order and close this window.', 'manual_phone_orders' )?>"></a>
		<?php

		$out = ob_get_clean();

		die( json_encode( array( 'view' => $out ) ) );

	}

	// Prevent redirect to cart if that setting is on in WC and the popup configurator is being used
	function prevent_redirect_after_add( $val = null ) {
		return false;
	}

	// For the popup title
	function maybe_inject_prettyphoto_css() {
		wp_enqueue_style( 'ign_prettyPhoto_pop', plugins_url( '/assets/prettyphoto-pop.css', __FILE__  ) );
        }

	function customer_query_meta( $wp_user_query ) {
		global $wpdb;

		if ( empty( $_GET['term'] ) )
			return;

		$term = wc_clean( stripslashes( $_GET['term'] ) );

		$wp_user_query->query_from .= ' left join ' . $wpdb->usermeta . ' ign_mm on ID=ign_mm.user_id ';

		$wp_user_query->query_where .= ' OR ( ign_mm.meta_key="billing_company" and ign_mm.meta_value LIKE "%' . $term . '%"  ) OR ( ign_mm.meta_key="billing_phone" and ign_mm.meta_value LIKE "%' . $term . '%" ) OR ( ign_mm.meta_key="billing_first_name" and ign_mm.meta_value LIKE "%' . $term . '%"  ) OR ( ign_mm.meta_key="billing_last_name" and ign_mm.meta_value LIKE "%' . $term . '%" )';

		return $wp_user_query;
	}

	function json_search_products_and_variations() {
		$this->json_search_products( '', array( 'product', 'product_variation' ) );
	}

	function json_search_products( $x = '', $post_types = array( 'product', 'product_variation' ) ) {
		global $ign_opc;

		ob_start();

		check_ajax_referer( 'search-products', 'security' );

		//$term    = (string) wc_clean( stripslashes( $_GET['term'] ) );

		remove_all_filters( 'pre_get_posts' );

		$term    = (string) stripslashes( $_GET['term'] );
		$exclude = array();

		if ( empty( $term ) ) {
			die();
		}

		if ( ! empty( $_GET['exclude'] ) ) {
			$exclude = array_map( 'intval', explode( ',', $_GET['exclude'] ) );
		}

		$args = array(
			'post_type'      => $post_types,
			'post_status'    => 'publish',
			'posts_per_page' => -1,
			's'              => $term,
			'fields'         => 'ids',
			'exclude'        => $exclude
		);

		if ( is_numeric( $term ) ) {

			if ( false === array_search( $term, $exclude ) ) {
				$posts2 = get_posts( array(
					'post_type'      => $post_types,
					'post_status'    => 'publish',
					'posts_per_page' => -1,
					'post__in'       => array( 0, $term ),
					'fields'         => 'ids'
				) );
			} else {
				$posts2 = array();
			}

			$posts3 = get_posts( array(
				'post_type'      => $post_types,
				'post_status'    => 'publish',
				'posts_per_page' => -1,
				'post_parent'    => $term,
				'fields'         => 'ids',
				'exclude'        => $exclude
			) );

			$posts4 = get_posts( array(
				'post_type'      => $post_types,
				'post_status'    => 'publish',
				'posts_per_page' => -1,
				'meta_query'     => array(
					array(
						'key'     => '_sku',
						'value'   => $term,
						'compare' => 'LIKE'
					)
				),
				'fields'         => 'ids',
				'exclude'        => $exclude
			) );

			$posts = array_unique( array_merge( get_posts( $args ), $posts2, $posts3, $posts4 ) );

		} else {

			$args2 = array(
				'post_type'      => $post_types,
				'post_status'    => 'publish',
				'posts_per_page' => -1,
				'meta_query'     => array(
					array(
					'key'     => '_sku',
					'value'   => $term,
					'compare' => 'LIKE'
					)
				),
				'fields'         => 'ids',
				'exclude'        => $exclude
			);

			// Category search ONLY works on terms, not SKUs because WP_Query can't do "meta_query OR tax_query" queries
			if ( !empty( $_GET['opc_search_cat'] ) && absint( $_GET['opc_search_cat'] ) > 1 ) { 
			
				$args2['s'] = $term;
				
				unset( $args2['meta_query'] );
			
				//$args2['relation'] = 'OR';
				
				$args2['tax_query'] = array(
					array(
						'taxonomy' => 'product_cat',
						'field'	=> 'term_id',
						'terms'	=> array( $_GET['opc_search_cat'] ),
						'operator'  => 'IN',
					)
				);
				$posts = array_unique( get_posts( $args2 ) );

			} else { 
				$posts = array_unique( array_merge( get_posts( $args ), get_posts( $args2 ) ) );
			}

		}

		$found_products = array();

		if ( ! empty( $posts ) ) {

			// Switch so that the correct prices show in case pricing plugins alter prices
			if ( !empty( $_REQUEST['ignitewoo_one_page_checkout_customer_id'] ) ) {
				$_POST['ignitewoo_one_page_checkout_customer_id'] = $_REQUEST['ignitewoo_one_page_checkout_customer_id'];
				if ( method_exists( $ign_opc, 'switch_user_context_to_customer' ) )
					$ign_opc->switch_user_context_to_customer( 'customer' );
			}
			
			foreach ( $posts as $post ) {
				$product = wc_get_product( $post );

				if ( empty( $product ) || is_bool( $product ) )
					continue;

				
				//if ( ! current_user_can( 'read_product', $post ) ) {
				//	continue;
				//}
				
				$parent_id = ign_get_product_parent_id( $product );
				
				if ( $product->is_type( 'variation' ) && empty( $parent_id ) )
					continue;

				$data = $this->get_product_name( $product );

				$found_products[ $post ] = rawurldecode( $data['name'] ) . $data['price'] . $data['stock'];

				// When searching with a category filter then variation children do not wind up in the results, 
				// so add them. 

				if ( !empty( $_GET['opc_search_cat'] ) && absint( $_GET['opc_search_cat'] ) > 1 ) { 
					if ( $product->is_type( 'variable' ) ) { 
						foreach( $product->get_children() as $child_id ) {
							$product = wc_get_product( $child_id );
							$data = $this->get_product_name( $product );
							$found_products[ $child_id ] = rawurldecode( $data['name'] ) . $data['price'] . $data['stock'];
						}
							
					}
				}
			}
		}

		$found_products = apply_filters( 'woocommerce_opc_json_search_found_products', $found_products );

		wp_send_json( $found_products );
	}

	public function get_product_name( $product ) { 
	
		if ( $product->is_type( 'simple' ) )
			$price = ' &ndash; ' . wc_price( $product->get_price() );
		else
			$price = '';

		$stock = '';

		if ( !empty( $this->settings['enable_stock'] ) && 'yes' == $this->settings['enable_stock'] ) {
			if ( 'yes' == get_option( 'woocommerce_manage_stock' ) ) {
				$stock = $product->get_stock_quantity();
				if ( null !== $stock )
					$stock = ' &ndash; (' . $stock . ' ' . __( 'in stock', 'manual_phone_orders' ) . ')';
			}
		}

		if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) ) {

			if ( $product->is_type( 'variation' ) ) {
				$attrs = $product->get_variation_attributes();
				if ( !empty( $attrs ) ) {
					$a = array();
					foreach( $attrs as $key => $val ) {
						$a[] = $product->get_attribute( str_replace( 'attribute_', '', $key ) );
					}
					if ( method_exists( $product, 'get_name' ) )
						$name = $product->get_name() . ' &ndash; ' . implode( ' &ndash; ', $a ) . ' (#' . $product->get_id() . ')';
					else 
						$name = $product->get_formatted_name();
					
				} else {
					$name = $product->get_formatted_name();
				}
			} else {
				$name = $product->get_formatted_name();
			}
		} else {
			$name = $product->get_formatted_name();
		}
		
		return array( 'name' => $name, 'price' => $price, 'stock' => $stock );
	}
	
	public function json_search_orders() {
		ob_start();

		check_ajax_referer( 'search-products', 'security' );

		//$term = (string) wc_clean( stripslashes( $_GET['term'] ) );
		$term = (string) stripslashes( $_GET['term'] );

		$posts1 = get_posts( array(
			'post_type'      => 'shop_order',
			'post_status'    => 'any',
			'posts_per_page' => -1,
			'include'	=> array( $term ),
			'fields'         => 'ids',
		) );


		$posts2 = get_posts( array(
			'post_type'      => 'shop_order',
			'post_status'    => 'any',
			'posts_per_page' => -1,
			'meta_query'     => array(
				array(
					'key'     => '_billing_first_name',
					'value'   => $term,
					'compare' => 'LIKE'
				)
			),
			'fields'         => 'ids',
		) );

		$posts3 = get_posts( array(
			'post_type'      => 'shop_order',
			'post_status'    => 'any',
			'posts_per_page' => -1,
			'meta_query'     => array(
				array(
					'key'     => '_billing_last_name',
					'value'   => $term,
					'compare' => 'LIKE'
				)
			),
			'fields'         => 'ids',
		) );

		$posts4 = get_posts( array(
			'post_type'      => 'shop_order',
			'post_status'    => 'any',
			'posts_per_page' => -1,
			'meta_query'     => array(
				array(
					'key'     => '_billing_company',
					'value'   => $term,
					'compare' => 'LIKE'
				)
			),
			'fields'         => 'ids',
		) );

		if ( class_exists( 'WC_Seq_Order_Number_Pro' ) || class_exists( 'WC_Seq_Order_Number' ) ) {
			$posts5 = get_posts( array(
				'post_type'      => 'shop_order',
				'post_status'    => 'any',
				'posts_per_page' => -1,
				'meta_query'     => array(
					array(
						'key'     => '_order_number',
						'value'   => $term,
						'compare' => 'LIKE'
					)
				),
				'fields'         => 'ids',
			) );
		} else if ( class_exists( 'WCJ_Order_Numbers' ) ) {
			$posts5 = get_posts( array(
				'post_type'      => 'shop_order',
				'post_status'    => 'any',
				'posts_per_page' => -1,
				'meta_query'     => array(
					array(
						'key'     => '_wcj_order_number',
						'value'   => $term,
						'compare' => 'LIKE'
					)
				),
				'fields'         => 'ids',
			) );
		} else {
			$posts5 = array();
		}

		$posts = array_unique( array_merge( $posts1, $posts2, $posts3, $posts4, $posts5 ) );

		$found_orders = array();

		if ( ! empty( $posts ) ) {
			foreach ( $posts as $post ) {
				$fname = get_post_meta( $post, '_billing_first_name', true );
				$lname = get_post_meta( $post, '_billing_last_name', true );
				if ( class_exists( 'WC_Seq_Order_Number_Pro' ) || class_exists( 'WC_Seq_Order_Number' ) )
					$oid = get_post_meta( $post, '_order_number', true );
				else
					$oid = $post;
				$found_orders[ $post ] = '#' . $oid . ' &ndash; ' . $fname . ' ' . $lname;
			}
		}

		$found_orders = apply_filters( 'ign_opc_json_search_found_orders', $found_orders, $term );

		wp_send_json( $found_orders );

	}

	function load_order() {
		global $ign_opc; 
		
		if ( !wp_verify_nonce( $_POST['_wpnonce'], 'woocommerce-order_again' ) )
			die( '-1' );

		$order = wc_get_order( absint( $_POST['one_page_load_order_id'] ) );

		$order_id = ign_get_order_id( $order );

		if ( empty( $order_id ) ) {
			return;
		}

		$ign_opc->switch_user_context_to_customer( 'opc_user' );
		
		$overwrite_order = ( 'true' == $_POST['overwrite_order'] ) ? true : false;

		// NOTE: State is after country in the list so the Select2 can update automatically via our JS
		// otherwise WC js resets the state when country is set when state comes before country
		$billing = apply_filters( 'woocommerce_order_formatted_billing_address', array(
			'first_name'    => $order->billing_first_name,
			'last_name'     => $order->billing_last_name,
			'company'       => $order->billing_company,
			'address_1'     => $order->billing_address_1,
			'address_2'     => $order->billing_address_2,
			'city'          => $order->billing_city,
			'postcode'      => $order->billing_postcode,
			'country'       => $order->billing_country,
			'state'         => $order->billing_state,
			'email'		=> $order->billing_email,
			'phone'		=> $order->billing_phone,
		), $order );

		if ( $order->shipping_address_1 || $order->shipping_address_2 ) {

			// Formatted Addresses
			$shipping = apply_filters( 'woocommerce_order_formatted_shipping_address', array(
				'first_name'    => $order->shipping_first_name,
				'last_name'     => $order->shipping_last_name,
				'company'       => $order->shipping_company,
				'address_1'     => $order->shipping_address_1,
				'address_2'     => $order->shipping_address_2,
				'city'          => $order->shipping_city,
				'postcode'      => $order->shipping_postcode,
				'country'       => $order->shipping_country,
				'state'         => $order->shipping_state,
			), $order );

			$shipping = WC()->countries->get_formatted_address( $address );
		} else {
			$shipping = array();
		}

		WC()->cart->empty_cart();

		// Copy products from the order to the cart
		foreach ( $order->get_items() as $item ) {
			// Load all product info including variation data
			$product_id   = (int) apply_filters( 'woocommerce_add_to_cart_product_id', $item['product_id'] );
			$quantity     = (int) $item['qty'];
			$variation_id = (int) $item['variation_id'];
			$variations   = array();
			$cart_item_data = apply_filters( 'woocommerce_order_again_cart_item_data', array(), $item, $order );

			foreach ( $item['item_meta'] as $meta_name => $meta_value ) {
				if ( taxonomy_is_product_attribute( $meta_name ) ) {
					$variations[ $meta_name ] = $meta_value;
				} elseif ( meta_is_product_attribute( $meta_name, $meta_value[0], $product_id ) ) {
					$variations[ $meta_name ] = $meta_value[0];
				}
			}

			$key = WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variations, $cart_item_data );

			// Set the price to be whatever is saved in the order, in case the manual/phone order user changed the price
			if ( !empty( $key ) ) { 
				$price = $order->get_item_total( $item, false, true );
				WC()->cart->cart_contents[ $key ]['_price_altered'] = $price;
				WC()->cart->set_session();		
			}
		}

		if ( $overwrite_order ) {
			/*
			WARNING: For an existing order to be loaded it must have a status of pending or failed otherwise
			WC will check the order status and empty the cart if the "order_awaiting_payment" has a status
			other than pending or failed. Therefore we set the status to pending here to ensure that the contents
			of the order can be loaded into the cart and when the order is placed that existing order can be
			overwritten.
			*/
			$order->update_status( 'pending' );
			
			WC()->session->set( 'order_awaiting_payment', $order_id );
			
			if ( method_exists( WC()->session, 'save_data' ) )
				WC()->session->save_data();
				
		} else if ( isset( WC()->session->order_awaiting_payment ) ) {
		
			WC()->session->set( 'order_awaiting_payment', false );
			
			if ( method_exists( WC()->session, 'save_data' ) )
				WC()->session->save_data();
		}

		die( json_encode( array( 'success' => 'true', 'customer_id' => $order->customer_user, 'billing' => $billing, 'shipping' => $shipping ) ) );

	}


	// This is used when someone loads an order and does not check the box to indicate that it should be loaded as a new order
	// Therefore the existing order status is set to pending or failed so that WC will overwrite it when the order is placed
	function maybe_alter_order_status() {

		$awaiting_payment = WC()->session->get( 'order_awaiting_payment' );

		if ( !isset( $awaiting_payment ) )
			return false;

		$order_id = absint( WC()->session->get( 'order_awaiting_payment' ) );

		if ( empty( $order_id ) )
			return false; 
			
		$order = wc_get_order( $order_id );

		//$order_id = ign_get_order_id( $order );

		if ( !empty( $order ) ) {
			// Set the status to pending so that WC will overwrite it
			$order->update_status( 'pending' );
		}
	}

	function remove_hooks() {

		if ( version_compare( WOOCOMMERCE_VERSION, '2.4', '>=' ) && $this->has_access() && isset( $_REQUEST['post_data'] ) /* && !defined( 'DOING_AJAX' ) */ ) {

			$args = wp_parse_args( $_REQUEST['post_data'] );

			if ( empty( $args['opc_checkout_flag'] ) && empty( $args['ignitewoo_one_page_checkout'] ) )
				return;

			remove_action( 'wc_ajax_update_order_review', array( 'WC_AJAX', 'update_order_review' ), 10 );
			remove_action( 'wc_ajax_nopriv_update_order_review', array( 'WC_AJAX', 'update_order_review' ), 10 );

			remove_action( 'wp_ajax_woocommerce_update_order_review', array( 'WC_AJAX', 'update_order_review' ), 10 );
			remove_action( 'wp_ajax_nopriv_woocommerce_update_order_review', array( 'WC_AJAX', 'update_order_review' ), 10 );
		}

		add_action( 'wp_ajax_woocommerce_update_order_review', array( &$this, 'update_order_review' ), -99 );
		add_action( 'wp_ajax_nopriv_update_order_review', array( &$this, 'update_order_review' ), -99 );
		add_action( 'wc_ajax_update_order_review', array( &$this, 'update_order_review' ) );

	}

	function add_new_order_link() {
		global $submenu, $wpdb;

		if ( !current_user_can( 'administrator' ) && !current_user_can( 'shop_manager' ) )
			return;

		$sql = 'select ID from ' . $wpdb->posts . ' where post_content like "%[woocommerce_manual_phone_order%" and post_status="publish"';

		$res = $wpdb->get_var( $sql );

		if ( empty( $res ) )
			return;

		$url = get_the_permalink( $res );

		$insert = array( __( 'Add New Order','manual_phone_orders' ) , 'edit_shop_orders', $url );

		$temp = $submenu['woocommerce'];

		if ( is_array( $temp ) )
			array_splice( $temp, 2, 0, '' );

		$submenu['woocommerce'] = $temp;

		$submenu['woocommerce'][2] = $insert;

		//$submenu['woocommerce'][] = array( __( 'Add New Order' ) , 'edit_shop_orders', $url);
	}


	// Helps cache plugins and browsers avoid caching the page and its elements
	public function no_cache() {

		if ( !$this->opc )
			return;

		if ( ! defined( 'DONOTCACHEPAGE' ) ) {
                        define( "DONOTCACHEPAGE", true );
                }

                if ( ! defined( 'DONOTCACHEOBJECT' ) ) {
                        define( "DONOTCACHEOBJECT", true );
                }

                if ( ! defined( 'DONOTCACHEDB' ) ) {
                        define( "DONOTCACHEDB", true );
                }

                nocache_headers();

	}


	public function wp_head() {

		if ( !$this->opc )
			return;

		$stylesheet = get_stylesheet_directory() . '/' . WC()->template_path() . '/checkout/one_page_checkout.css';

		if ( file_exists( $stylesheet ) )
			$url = get_stylesheet_uri() . '/' . WC()->template_path() . '/checkout/one_page_checkout.css';
		else
			$url = $this->plugin_url() . '/assets/one_page_checkout.css';

		/*
		if ( version_compare( WOOCOMMERCE_VERSION, '2.3', '<=' ) )
			$ver = '2.2';
		else
			$ver = '2.3';
		*/
		
		$ver = WOOCOMMERCE_VERSION; 
		
		if ( version_compare( WOOCOMMERCE_VERSION, '3.2', '>=' ) )
			$wc_32_or_newer = 'true';
		else 
			$wc_32_or_newer = 'false';
			
		// Make this has a value, it's a newer setting people might not have seen and adjusted yet
		if ( empty( $this->settings['disable_autorefresh'] ) )
			$this->settings['disable_autorefresh'] = 'no';
			
		$js = "
		ign_one_page_search_products_nonce = '". wp_create_nonce( 'search-products' ) . "';
		ign_one_page_search_customers_nonce = '". wp_create_nonce( 'search-customers' ) . "';
		ign_one_page_ajax_url = '" . admin_url('admin-ajax.php') . "';
		wc_version_info = '" . $ver . "';
		wc_32_or_newer = '" . $wc_32_or_newer . "';
		";

		echo '<script>'.$js.'</script>';

		wp_register_style( 'opc_styles', $url );

		wp_enqueue_style( 'opc_styles' );

		wp_register_script( 'opc-script', $this->plugin_url() . '/scripts/ignitewoo-opc.js?time=' . time(), array( 'jquery' ), time(), true );

		wp_enqueue_script( 'opc-script' );

		wp_localize_script( 'opc-script', 'opc_enhanced_select_params', array(
			'ajax_url'                  => admin_url( 'admin-ajax.php' ),
			'i18n_matches_1'            => _x( 'One result is available, press enter to select it.', 'enhanced select', 'manual_phone_orders' ),
			'i18n_matches_n'            => _x( '%qty% results are available, use up and down arrow keys to navigate.', 'enhanced select', 'manual_phone_orders' ),
			'i18n_no_matches'           => _x( 'No matches found', 'enhanced select', 'manual_phone_orders' ),
			'i18n_ajax_error'           => _x( 'Loading failed', 'enhanced select', 'manual_phone_orders' ),
			'i18n_input_too_short_1'    => _x( 'Please enter 1 or more characters', 'enhanced select', 'manual_phone_orders' ),
			'i18n_input_too_short_n'    => _x( 'Please enter %qty% or more characters', 'enhanced select', 'manual_phone_orders' ),
			'i18n_input_too_long_1'     => _x( 'Please delete 1 character', 'enhanced select', 'manual_phone_orders' ),
			'i18n_input_too_long_n'     => _x( 'Please delete %qty% characters', 'enhanced select', 'manual_phone_orders' ),
			'i18n_selection_too_long_1' => _x( 'You can only select 1 item', 'enhanced select', 'manual_phone_orders' ),
			'i18n_selection_too_long_n' => _x( 'You can only select %qty% items', 'enhanced select', 'manual_phone_orders' ),
			'i18n_load_more'            => _x( 'Loading more results&hellip;', 'enhanced select', 'manual_phone_orders' ),
			'i18n_searching'            => _x( 'Searching&hellip;', 'enhanced select', 'manual_phone_orders' ),
			'select_order'            => _x( 'Select an order first', 'enhanced select', 'manual_phone_orders' ),
			'security_error'            => _x( 'Security problem. Reload the page and try again', 'enhanced select', 'manual_phone_orders' ),
			'load_order_confirm'            => _x( 'Are you sure that want to load this order and overwrite it? Doing this will immedidately set the order status to "Pending Payment" and when you submit the order the existing order will be permanently and completely overwritten!', 'enhanced select', 'manual_phone_orders' ),
			'confirm' => __( 'Are your sure?', 'manual_phone_orders' ),
			'search_products_nonce'     => wp_create_nonce( 'search-products' ),
			'search_customers_nonce'    => wp_create_nonce( 'search-customers' ),
			'set_shipping_cost_first' => _x( 'Set a shipping cost first. Use 0 for free shipping.', 'input', 'manual_phone_orders' ),
			'yes_val' => _x( 'Yes', 'input', 'manual_phone_orders' ),
			'no_val' => _x( 'No', 'input', 'manual_phone_orders' ),
			'wc_version_gte_27' => version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ? 'true' : 'false',
			'arbitrary_product_title' => __( 'Add an arbitrary product', 'manual_phone_orders' ),
			'ajax_error_messsage' => __( 'An error occurred', 'manual_phone_orders' ),
			'disable_autorefresh' => $this->settings['disable_autorefresh'],
			
			
		) );

		// Checkout page scripts from WC
		add_action( 'wp_head', array( $this, 'load_scripts_styles' ), 9999 );

		// Maybe load customer info it that was submitted via the form
		$this->ignitewoo_one_page_get_customer();

	}

	public function admin_head() {

		if ( !$this->has_access() || !function_exists( 'WC' ) )
			return;

		$stylesheet = get_stylesheet_directory() . '/' . WC()->template_path() . '/checkout/one_page_checkout.css';

		if ( file_exists( $stylesheet ) )
			$url = get_stylesheet_uri() . '/' . WC()->template_path() . '/checkout/one_page_checkout.css';
		else
			$url = $this->plugin_url() . '/assets/one_page_checkout.css';

		wp_register_script( 'ajax-chosen', $this->plugin_url() . '/scripts/ajax-chosen/lib/ajax-chosen.min.js', array('jquery', 'chosen'), WC_VERSION );

		wp_register_script( 'chosen', $this->plugin_url() . '/scripts/chosen/chosen.jquery.min.js', array('jquery'), WC_VERSION );

		wp_enqueue_script( 'chosen' );
		wp_enqueue_script( 'ajax-chosen' );

		wp_register_style( 'chosen-style', $this->plugin_url() . '/scripts/chosen/chosen.css', WC_VERSION );
		wp_enqueue_style( 'chosen-style' );

		if ( defined( 'FORCE_SSL_ADMIN' ) ) {
			$js = "
				ign_one_page_search_customers_nonce = '". wp_create_nonce('search-customers') . "';
				ign_one_page_ajax_url = '" . str_replace( 'http://', 'https://', admin_url('admin-ajax.php') ) . "';
			";
		} else {
			$js = "
				ign_one_page_search_customers_nonce = '". wp_create_nonce('search-customers') . "';
				ign_one_page_ajax_url = '" . admin_url('admin-ajax.php') . "';
			";
		}

		echo '<script>'.$js.'</script>';

		wp_register_style( 'opc_styles', $url );

		wp_enqueue_style( 'opc_styles' );

		wp_register_script( 'opc-script', $this->plugin_url() . '/scripts/ignitewoo-opc.js', array( 'jquery' ), WC_VERSION, true );

		wp_enqueue_script( 'opc-script' );

		// Used by JS to determine is AjaxChosen should be used or not
		if ( is_admin() )
			$is_admin = true;
		else
			$is_admin = false;

		wp_localize_script( 'opc-script', 'opc_enhanced_select_params', array(
			'i18n_matches_1'            => _x( 'One result is available, press enter to select it.', 'enhanced select', 'manual_phone_orders' ),
			'i18n_matches_n'            => _x( '%qty% results are available, use up and down arrow keys to navigate.', 'enhanced select', 'manual_phone_orders' ),
			'i18n_no_matches'           => _x( 'No matches found', 'enhanced select', 'manual_phone_orders' ),
			'i18n_ajax_error'           => _x( 'Loading failed', 'enhanced select', 'manual_phone_orders' ),
			'i18n_input_too_short_1'    => _x( 'Please enter 1 or more characters', 'enhanced select', 'manual_phone_orders' ),
			'i18n_input_too_short_n'    => _x( 'Please enter %qty% or more characters', 'enhanced select', 'manual_phone_orders' ),
			'i18n_input_too_long_1'     => _x( 'Please delete 1 character', 'enhanced select', 'manual_phone_orders' ),
			'i18n_input_too_long_n'     => _x( 'Please delete %qty% characters', 'enhanced select', 'manual_phone_orders' ),
			'i18n_selection_too_long_1' => _x( 'You can only select 1 item', 'enhanced select', 'manual_phone_orders' ),
			'i18n_selection_too_long_n' => _x( 'You can only select %qty% items', 'enhanced select', 'manual_phone_orders' ),
			'i18n_load_more'            => _x( 'Loading more results&hellip;', 'enhanced select', 'manual_phone_orders' ),
			'i18n_searching'            => _x( 'Searching&hellip;', 'enhanced select', 'manual_phone_orders' ),
			'ajax_url'                  => admin_url( 'admin-ajax.php' ),
			'search_products_nonce'     => wp_create_nonce( 'search-products' ),
			'search_customers_nonce'    => wp_create_nonce( 'search-customers' ),
			'is_admin'		    => $is_admin,
		) );
	}

	// Ensure context is switched so the checkout nonce is correct
	public function woocommerce_review_order_before_submit() { 
		global $ign_opc;
		$ign_opc->switch_user_context_to_customer( 'opc_user' );

	}
	
	public function adjust_hooks() {

		if ( !$this->has_access() )
			return;

		add_action( 'woocommerce_is_checkout', array( &$this, 'is_checkout' ), 1, 999999 );
		add_action( 'woocommerce_review_order_before_submit', array( &$this, 'woocommerce_review_order_before_submit' ), 1 );
/*
remove_all_filters( 'wp_ajax_woocommerce_checkout' );
remove_all_filters( 'wc_ajax_checkout' );

remove_action( 'wp_ajax_woocommerce_checkout', array( 'WC_AJAX', 'checkout' ) );
remove_action( 'wp_ajax_nopriv_woocommerce_checkout', array( 'WC_AJAX', 'checkout' ) );
remove_action( 'init', array( 'WC_Form_Handler', 'checkout_action' ), 20 );
remove_action( 'wp_loaded', array( 'WC_Form_Handler', 'checkout_action' ), 20 );

add_action( 'wc_ajax_checkout', array( &$this, 'checkout' ) );
add_action( 'wp_ajax_woocommerce_checkout', array( &$this, 'checkout' ) );
add_action( 'wp_ajax_nopriv_woocommerce_checkout', array( &$this, 'checkout' ) );
add_action( 'wp_loaded', array( &$this, 'checkout_action' ), 20 );
*/
		
		if ( !empty( $_POST['ignitewoo_one_page_checkout'] ) && 'searchable' == $_POST['ignitewoo_one_page_checkout'] ) {

			remove_action( 'wp_ajax_woocommerce_checkout', array( 'WC_AJAX', 'checkout' ) );
			remove_action( 'wp_ajax_nopriv_woocommerce_checkout', array( 'WC_AJAX', 'checkout' ) );
			remove_action( 'init', array( 'WC_Form_Handler', 'checkout_action' ), 20 );
			remove_action( 'wp_loaded', array( 'WC_Form_Handler', 'checkout_action' ), 20 );

			add_action( 'wp_ajax_woocommerce_checkout', array( &$this, 'checkout' ) );
			add_action( 'wp_ajax_nopriv_woocommerce_checkout', array( &$this, 'checkout' ) );
			add_action( 'wp_loaded', array( &$this, 'checkout_action' ), 20 );
		}
	}

	/*
	public function get_checkout_page_id( $page_id = null ) {

		$opts = get_option( 'woocommerce_ignitewoo_manual_order_settings' );

		if ( empty( $opts['manual_order_page'] ) )
			return $page_id;

		if ( !empty( $opts['manual_order_page'] ) )
			return $opts['manual_order_page'];

		if ( $this->check_for_shortcode( false ) )
			return $page_id;

		return $page_id;
	}
	*/

	public function is_checkout() {

		if ( empty( $this->settings ) )
			$this->settings = get_option( 'woocommerce_ignitewoo_manual_order_settings' );

		if ( empty( $this->settings['manual_order_page'] ) )
			return false;

		$order_page = is_page( $this->settings['manual_order_page'] );
		
		if ( $order_page )
			return true;
		
		// Helpful for woocommerce_update_order_review Ajax,
		// hopefully gets required plugins to load if they need to.
		if ( !empty( $_POST['post_data'] ) ) { 
		
			$data = wp_parse_args( $_POST );
			
			if ( !empty( $data['opc_checkout_flag'] ) )
				return true;
			else if ( !empty( $data['ignitewoo_one_page_checkout'] ) )
				return true;
		}
	}

	public function checkout() {

		if ( ! defined( 'WOOCOMMERCE_CHECKOUT' ) ) {
			define( 'WOOCOMMERCE_CHECKOUT', true );
		}

		$this->maybe_disable_terms();

		$this->process_checkout();

		die(0);
	}


	public function checkout_action() {

		// For non-JS enabled order placement
		if ( isset( $_POST['woocommerce_checkout_place_order'] ) || isset( $_POST['woocommerce_checkout_update_totals'] ) ) {

			if ( sizeof( WC()->cart->get_cart() ) == 0 ) {
				wp_redirect( get_permalink( wc_get_page_id( 'cart' ) ) );
				exit;
			}

			if ( ! defined( 'WOOCOMMERCE_CHECKOUT' ) ) {
				define( 'WOOCOMMERCE_CHECKOUT', true );
			}

			$this->process_checkout();
		}
	}



	public static function is_donation( $_product ) {
		global $WC_Donations;

		if ( !is_object( $_product ) )
			return;

		$product_id = ign_get_product_id( $_product, false );

		if ( isset( $WC_Donations ) && ( $d_args = $WC_Donations->is_donation( $product_id ) ) )
			return $d_args;

	}

	// Checks if the product is a gift cert product set where the shopper can set their own price
	public static function is_gift_cert( $product ) {

		$product_id = ign_get_product_id( $_product, false );

 		if ( '1' == get_post_meta( $product_id, 'ignite_gift_enabled', true ) ) {

			if ( '1' == get_post_meta( $product_id, 'ignite_buyer_sets_price', true ) )
				return true;
			else
				return false;

		} else {

			return false;

		}

		return false;

	}

	public function check_for_shortcode( $with_filters = true )  {
		global $post;

		if ( empty( $post ) || empty( $post->post_content ) )
			return;

		if ( false === strpos( $post->post_content, '[' . $this->shortcode ) )
			return;

		if ( !$with_filters )
			return true;

		add_filter( 'woocommerce_available_variation', array( $this, 'set_min_qty' ) , 999, 3 );

		add_filter( 'woocommerce_product_single_add_to_cart_text', array( $this, 'add_to_cart_text' ) , 999, 3 );

		// Add hidden field for Ajax detection
		add_action( 'woocommerce_checkout_before_customer_details', array( &$this, 'before_customer_details' ), 1 );

		$this->opc = true;

		nocache_headers();

		return true;

	}



	public function set_min_qty( $attribs = array(), $_product, $_variation ) {
		global $product;

		$attribs['min_qty'] = 0;

		return $attribs;

	}


	public function add_to_cart_text( $text = '' ) {
		global $product;

		if ( isset( $product->opc_donation ) )
			return $text;

		return apply_filters( 'ignitewoo_one_page_update_cart_label', __( 'Update cart', 'manual_phone_orders' ) );
	}

	/*
	public static function remove_product_link() {
		global $product;

		if ( empty( $product->opc_donation ) && empty( $product->opc_gift_cert ) )
			return;

		$product_id = ign_get_product_id( $_product, false );

		?>
		<p class="opc_remove_product_link">
			<a href="#" class="opc_remove_from_cart" data-product_id="<?php echo $product_id ?>" ><?php echo apply_filters( 'ignitewoo_one_page_remove_from_cart_label', __( 'Remove from cart', 'manual_phone_orders' ) ) ?></a>
		</p>
		<?php
	}
	*/

	public function array_implode( $glue, $separator, $array ) {

		if ( ! is_array( $array ) )
			return $array;

		$string = array();

		foreach ( $array as $key => $val ) {

			if ( is_object( $val ) )
				$val = (array) $val;

			if ( is_array( $val ) )
				$val = implode( ',', $val );

			$string[] = "{$key}{$glue}{$val}";
		}

		return implode( $separator, $string );
	}


	// Load the product template
	public function before_checkout_form() {
		global $products;

		if ( !$this->opc )
			return;

		if ( 'searchable' != $this->style && empty( $this->products ) )
			return;

		$products = $this->products;

		unset( $this->products );

		if ( empty( $this->style ) )
			$this->style = 'searchable';

		$template_path = $default_path = dirname( __FILE__ ) . '/phone-orders/';

		// load template via WC
		switch( $this->style ) {
			/* NOT IMPLEMENTED - POSSIBLE FUTURE LAYOUTS
			case 'list':
				$template_name = 'one_page_list.php';
				$this->load_template( $template_name );
				break;

			case 'table':
				$template_name = 'one_page_table.php';
				$this->load_template( $template_name );
				break;

			case 'single':
				$template_name = 'one_page_single.php';
				$this->load_template( $template_name );
				break;

			case 'single_consolidated':
				$template_name = 'one_page_single_consolidated.php';
				$this->load_template( $template_name );
				break;
			*/
			case 'searchable':
				$template_name = 'one_page_searchable.php';
				$this->load_template( $template_name );
				break;
			default:
				// Load a custom template file
				//$template_name = $attrs['style'];
				//$this->load_template( $template_name );
				break;
		}
	}


	function ignitewoo_one_page_get_customer() {

		if ( empty( $_POST ) || empty( $_POST['one_page_customer_user'] ) )
			return;

		$usermeta = get_user_meta( absint(  $_POST['one_page_customer_user'] ) );

		if ( empty( $usermeta ) )
			return;

		foreach( $usermeta as $key => $data ) {

			if ( 0 === strpos( $key, 'billing_' ) || 0 === strpos( $key, 'shipping_' ) ) {
				$_POST[ $key ] = $data[0];
			}
		}

		add_action( 'woocommerce_checkout_order_review', array( &$this, 'one_page_checkout_after_order_review' ), 1 );
	}

	// Inject a hidden field with the customer ID number ( if any ) so that it is available for processing the order
	function one_page_checkout_after_order_review() {
		?>
		<input type="hidden" name="ignitewoo_one_page_checkout_customer_id" value="<?php echo absint(  $_POST['one_page_customer_user'] ) ?>" />
		<?php
	}

	function checkout_after_order_review() {

		if ( 'searchable' !== $this->style )
			return;

		?>

		<input type="hidden" name="ignitewoo_one_page_checkout" value="searchable" />
		<?php

	}

	// Formerly checkout_before_order_review()
	function add_tax_exempt_field() {
		if ( 'searchable' !== $this->style )
			return;

		/*
		?>
		<p class="form-row form-row-wide" style="width: 100px"><input class="ignitewoo_one_page_tax_exempt" type="checkbox" name="ignitewoo_one_page_tax_exempt" value="yes" /> <?php _e( 'Tax Exempt', 'manual_phone_orders' )?></p>
		<?php
		*/
		
		?>
		<input class="ignitewoo_one_page_tax_exempt" style="margin-left:14px; float:none !important;vertical-align:text-top" type="checkbox" name="ignitewoo_one_page_tax_exempt" value="yes"/> <?php _e( 'Tax Exempt', 'manual_phone_orders' )?>
		<?php

	}

	function is_tax_exempt() {
		global $ign_tax_exempt, $ign_opc;
		
		$data = array();

		if ( isset( $_POST['post_data'] ) )
			$data = wp_parse_args( $_POST['post_data'] );
		else if ( empty( $_POST['opc_checkout_flag'] ) && empty( $_POST['ignitewoo_one_page_tax_exempt'] ) )
			return;

		//$ign_opc->switch_user_context_to_customer( 'customer' );
		
		// If the IgniteWoo Tax Exempt plugin is in use and the user is set to exempt or and the phone/manual checkbox is checked, set exemption
		if ( ( isset( $data['ignitewoo_one_page_tax_exempt'] ) && 'yes' == $data['ignitewoo_one_page_tax_exempt'] ) || ( isset( $ign_tax_exempt ) && $ign_tax_exempt->is_user_exempt() ) ) {
			WC()->customer->set_is_vat_exempt( true );
		// Otherwise if the phone/manual order exempt box is checked the set exempt
		} else if ( ( isset( $data['ignitewoo_one_page_tax_exempt'] ) && 'yes' == $data['ignitewoo_one_page_tax_exempt'] ) ) {
			WC()->customer->set_is_vat_exempt( true );
		} else if ( ( isset( $_POST['ignitewoo_one_page_tax_exempt'] ) && 'yes' == $_POST['ignitewoo_one_page_tax_exempt'] ) ) {
			WC()->customer->set_is_vat_exempt( true );
		// No exemption set
		} else {
			WC()->customer->set_is_vat_exempt( false );
		}
		
		//$ign_opc->switch_user_context_to_customer( 'opc_user' );

	}

	function maybe_add_fee() {

		if ( is_admin() && ! defined( 'DOING_AJAX' ) )
			return;

		if ( isset( $_POST['post_data'] ) )
			$data = wp_parse_args( $_POST['post_data'] );
		else if ( isset( $_POST['opc_fee'] ) )
			$data = $_POST;

		if ( !empty( $data['opc_fee'] ) ) {

			for( $i = 0; $i < count( $data['opc_fee'] ); $i++ ) {
				$amount = $data['opc_fee'][ $i ];
				$label = $data['opc_fee_label'][ $i ];
				$taxable = ( 'yes' == $data['opc_fee_taxable'][ $i ] ) ? true : false;
				WC()->cart->add_fee( $label, $amount, $taxable, '' );
			}
		}
	}

	function maybe_check_fee() {

		if ( empty( $_POST['n'] ) || empty( $_POST['label'] ) || empty( $_POST['amount'] ) )
			die( __( 'Error processing fee. Please provide a label and fee.', 'manual_phone_orders' ) );

		if ( !wp_verify_nonce( $_POST['n'], 'ign-add-fees' ) )
			die( __( 'Security error processing fee', 'manual_phone_orders' ) );

		$amount = floatval( $_POST['amount'] );

		if ( !is_numeric( $amount ) )
			die( __( 'Amount must be valid number', 'manual_phone_orders' ) );

		$label = trim( $_POST['label'] );

		if ( empty( $label ) )
			die( __( 'You must set a unique label', 'manual_phone_orders' ) );

		die( 'ok' );
	}

	public function maybe_add_arbitrary_product() { 
		
		if ( empty( $_POST ) ) 
			die( 'Error' );
			
		if ( empty( $_POST['data'] ) )
			die( 'Error' );

		$data = wp_parse_args( $_POST['data'] );

		if ( empty( $data['title'] ) || empty( $data['quantity'] ) || empty( $data['price'] ) )
			die( __( 'Error: title, price, and quantity are required', 'manual_phone_orders' ) );

		// Create a new product, set the status to publish and the catalog visibility to hidden 
		$post = array(
			'post_author' => 1,
			'post_content' => __( 'This product was temporarily created for a manual/order order. This product will be automatically deleted.', 'manual_phone_orders' ),
			'post_status' => "publish",
			'post_title' => $data['title'],
			'post_parent' => '',
			'post_type' => "product",
		);

		$post_id = wp_insert_post( $post );

		if ( empty( $post_id ) || is_wp_error( $post_id ) )
			die( __( 'Error adding arbitray product', 'manual_phone_orders' ) );
			
		// wp_set_object_terms( $post_id, 'Races', 'product_cat' );
		wp_set_object_terms($post_id, 'simple', 'product_type');

		if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) ) { 
			
			$product = wc_get_product( $post_id );
			
			$product->set_price( $data['price'] );
			$product->set_regular_price( $data['price'] );
			
			$product->set_catalog_visibility( 'hidden' );
			$product->set_stock_status( 'instock' );
			$product->set_sku( $data['sku'] );
			
			if ( empty( $data['weight'] ) ) {
				$product->set_downloadable( 'yes' );
				$product->set_virtual( 'yes' );
			} else { 
				$product->set_downloadable( 'no' );
				$product->set_virtual( 'no' );
			}
			
			$product->set_weight( $data['weight'] );
			$product->set_length( $data['length'] );
			$product->set_width( $data['width'] );
			$product->set_height( $data['height'] );

			$product->set_purchase_note( '' );
			$product->set_featured( '' );
			
			$product->add_meta_data( '_ign_opc_arbitrary_product', current_time( 'timestamp', true ) );
			
			$product->save();
			
		} else { 
			update_post_meta($post_id, '_sku', $data['sku'] );
			
			update_post_meta( $post_id, '_visibility', 'hidden' );
			update_post_meta( $post_id, '_stock_status', 'instock');
			update_post_meta( $post_id, 'total_sales', '0');
			
			if ( empty( $data['weight'] ) ) {
				update_post_meta( $post_id, '_downloadable', 'yes');
				update_post_meta( $post_id, '_virtual', 'yes');
			} else { 
				update_post_meta( $post_id, '_downloadable', 'no');
				update_post_meta( $post_id, '_virtual', 'no');
			}
			
			update_post_meta( $post_id, '_weight', $data['weight'] );
			update_post_meta( $post_id, '_length', $data['length'] );
			update_post_meta( $post_id, '_width', $data['width'] );
			update_post_meta( $post_id, '_height', $data['height'] );
			
			update_post_meta( $post_id, '_price', $data['price'] );
			update_post_meta( $post_id, '_regular_price', $data['price'] );
			update_post_meta( $post_id, '_sale_price', "" );
			
			update_post_meta( $post_id, '_purchase_note', "" );
			update_post_meta( $post_id, '_featured', "no" );
			
			update_post_meta( $post_id, '_product_attributes', array());
			update_post_meta( $post_id, '_sale_price_dates_from', "" );
			update_post_meta( $post_id, '_sale_price_dates_to', "" );
			update_post_meta( $post_id, '_sold_individually', "" );
			update_post_meta( $post_id, '_manage_stock', "no" );
			update_post_meta( $post_id, '_backorders', "no" );
			update_post_meta( $post_id, '_stock', "" );
						
			// Add custom post meta so we can remove these products either when the order is written
			// or later during a catchall clean up cron routine.
			// NOTE: The products are delete in the "thankyou_page_additions()" function, if the order isn't on hold
			update_post_meta( $post_id, '_ign_opc_arbitrary_product', current_time( 'timestamp', true ) );
		}

		// Set visibility 
		$terms = array( 'exclude-from-search', 'exclude-from-catalog' );

		wp_set_post_terms( $post_id, $terms, 'product_visibility', false );
		
		// Add to cart 
		$cart_item_key = self::add_to_cart( $post_id, $data['quantity'] );
		
		// Add this custom cart item data, this plugin uses it to know what they price should be.
		// And this, in theory, will prevent other pricing tools from overriding it.
		if ( !empty( $cart_item_key ) ) {
			WC()->cart->cart_contents[ $cart_item_key ]['_price_altered'] = $data['price'];
		}
		
		//
		die( 'done' );
	}
	
	// Based on WC core code
	public function create_order_gt_27( $data ) {
		global $ign_opc;

		if ( !$this->has_access() )
			return;

		// Disallow other plugins from intercepting the order to create their own
		remove_all_filters( 'woocommerce_create_order' );
		add_action( 'woocommerce_create_order', array( &$this, 'maybe_alter_order_status' ), 2, -1 );

		// Give plugins the opportunity to create an order themselves.
		if ( $order_id = apply_filters( 'woocommerce_create_order', null, $this ) ) {
			return $order_id;
		}

		try {
			$order_id           = absint( WC()->session->get( 'order_awaiting_payment' ) );
			$cart_hash          = md5( json_encode( wc_clean( WC()->cart->get_cart_for_session() ) ) . WC()->cart->total );
			$available_gateways = WC()->payment_gateways->get_available_payment_gateways();

			// IgniteWoo mod: Switch to opc user
			$ign_opc->switch_user_context_to_customer( 'customer' );

			/**
			 * If there is an order pending payment, we can resume it here so
			 * long as it has not changed. If the order has changed, i.e.
			 * different items or cost, create a new order. We use a hash to
			 * detect changes which is based on cart items + order total.
			 */
			 
			 // Disable cart hash checking since the cart contents etc might have been modified
			if ( $order_id && ( $order = wc_get_order( $order_id ) ) /*&& $order->has_cart_hash( $cart_hash )*/ && $order->has_status( array( 'pending', 'failed' ) ) ) {
				// Action for 3rd parties.
				do_action( 'woocommerce_resume_order', $order_id );

				// Remove all items - we will re-add them later.
				$order->remove_order_items();
			} else {
				$order = new WC_Order();
			}

			foreach ( $data as $key => $value ) {
				if ( is_callable( array( $order, "set_{$key}" ) ) ) {
					$order->{"set_{$key}"}( $value );

				// Store custom fields prefixed with wither shipping_ or billing_. This is for backwards compatibility with 2.6.x.
				// TODO: Fix conditional to only include shipping/billing address fields in a smarter way without str(i)pos.
				} elseif ( ( 0 === stripos( $key, 'billing_' ) || 0 === stripos( $key, 'shipping_' ) )
					&& ! in_array( $key, array( 'shipping_method', 'shipping_total', 'shipping_tax' ) ) ) {
					$order->update_meta_data( '_' . $key, $value );
				}
			}

			$order->set_created_via( 'checkout' );
			$order->set_cart_hash( $cart_hash );
			$order->set_customer_id( apply_filters( 'woocommerce_checkout_customer_id', get_current_user_id() ) );
			$order->set_currency( get_woocommerce_currency() );
			$order->set_prices_include_tax( 'yes' === get_option( 'woocommerce_prices_include_tax' ) );
			$order->set_customer_ip_address( WC_Geolocation::get_ip_address() );
			$order->set_customer_user_agent( wc_get_user_agent() );
			$order->set_customer_note( isset( $data['order_comments'] ) ? $data['order_comments'] : '' );
			$order->set_payment_method( isset( $available_gateways[ $data['payment_method'] ] ) ? $available_gateways[ $data['payment_method'] ]  : $data['payment_method'] );
			
			if ( version_compare( WOOCOMMERCE_VERSION, '3.2', '<' ) ) {
				$order->set_shipping_total( WC()->cart->get_cart_shipping_total() );
				$order->set_discount_total( WC()->cart->get_cart_discount_total() );
				$order->set_discount_tax( WC()->cart->get_cart_discount_tax_total() );
				$order->set_cart_tax( WC()->cart->tax_total );
				$order->set_shipping_tax( WC()->cart->shipping_tax_total );
				$order->set_total( WC()->cart->total );
			} else { 
				$order->set_shipping_total( WC()->cart->get_shipping_total() );	
				$order->set_discount_total( WC()->cart->get_discount_total() );
				$order->set_discount_tax( WC()->cart->get_discount_tax() );
				$order->set_cart_tax( WC()->cart->get_cart_contents_tax() + WC()->cart->get_fee_tax() );
				$order->set_shipping_tax( WC()->cart->get_shipping_tax() );
				$order->set_total( WC()->cart->get_total( 'edit' ) );
			}
						
			$checkout = new WC_Checkout();
			
			$checkout->create_order_line_items( $order, WC()->cart );
			$checkout->create_order_fee_lines( $order, WC()->cart );

			// IgniteWoo mod: Switch to opc user to obtain session vars 
			$ign_opc->switch_user_context_to_customer( 'opc_user' );
			$checkout->create_order_shipping_lines( $order, WC()->session->get( 'chosen_shipping_methods' ), WC()->shipping->get_packages() );
			// IgniteWoo mod: Switch back to custom
			$ign_opc->switch_user_context_to_customer( 'customer' );

			$checkout->create_order_tax_lines( $order, WC()->cart );
			$checkout->create_order_coupon_lines( $order, WC()->cart );

			/**
			 * Action hook to adjust order before save.
			 * @since 3.0.0
			 */
			do_action( 'woocommerce_checkout_create_order', $order, $data );

			// Save the order.
			$order_id = $order->save();

			do_action( 'woocommerce_checkout_update_order_meta', $order_id, $data );

			$ign_opc->switch_user_context_to_customer( 'opc_user' );
			
			return $order_id;
			
		} catch ( Exception $e ) {
			$ign_opc->switch_user_context_to_customer( 'opc_user' );
			return new WP_Error( 'checkout-error', $e->getMessage() );
		}

	}

	// Based on WC core code
	function create_order( $null, $checkout ) {
		global $wpdb;

		if ( !$this->has_access() )
			return;

		try {
			// Start transaction if available
			$wpdb->query( 'START TRANSACTION' );

			$order_data = array(
				'status'        => apply_filters( 'woocommerce_default_order_status', 'pending' ),
				'customer_id'   => $checkout->order_customer_id,
				'customer_note' => isset( $checkout->posted['order_comments'] ) ? $checkout->posted['order_comments'] : ''
			);

			// Insert or update the post data
			$order_id = absint( WC()->session->order_awaiting_payment );

			// Resume the unpaid order if its pending
			if ( $order_id > 0 && ( $order = wc_get_order( $order_id ) ) && $order->has_status( array( 'pending', 'failed' ) ) ) {

				$order_data['order_id'] = $order_id;
				$order                  = wc_update_order( $order_data );

				if ( is_wp_error( $order ) ) {
					throw new Exception( sprintf( __( 'Error %d: Unable to create order. Please try again.', 'manual_phone_orders' ), 401 ) );
				} else {
					$order->remove_order_items();
					do_action( 'woocommerce_resume_order', $order_id );
				}

			} else {

				$order = wc_create_order( $order_data );

				if ( is_wp_error( $order ) ) {
					throw new Exception( sprintf( __( 'Error %d: Unable to create order. Please try again.', 'manual_phone_orders' ), 400 ) );
				} else {

					$order_id = ign_get_order_id( $order );
					do_action( 'woocommerce_new_order', $order_id );
				}

				/*
				// IGNITEWOO MOD - update the post author to be the customer user if set
				if ( !empty( $checkout->order_customer_id ) ) {
					$order_data['post_author'] = $checkout->order_customer_id;

					$order_id = wp_update_post( $order_data );
				}
				*/
			}

			// Store the line items to the new/resumed order
			foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
				$item_id = $order->add_product(
					$values['data'],
					$values['quantity'],
					array(
						'variation' => $values['variation'],
						'totals'    => array(
							'subtotal'     => $values['line_subtotal'],
							'subtotal_tax' => $values['line_subtotal_tax'],
							'total'        => $values['line_total'],
							'tax'          => $values['line_tax'],
							'tax_data'     => $values['line_tax_data'] // Since 2.2
						)
					)
				);

				if ( ! $item_id ) {
					throw new Exception( sprintf( __( 'Error %d: Unable to create order. Please try again.', 'manual_phone_orders' ), 402 ) );
				}

				// Allow plugins to add order item meta
				do_action( 'woocommerce_add_order_item_meta', $item_id, $values, $cart_item_key );
			}

			// Stores
			foreach ( WC()->cart->get_fees() as $fee_key => $fee ) {
				$item_id = $order->add_fee( $fee );

				if ( ! $item_id ) {
					throw new Exception( sprintf( __( 'Error %d: Unable to create order. Please try again.', 'manual_phone_orders' ), 403 ) );
				}

				// Allow plugins to add order item meta to fees
				do_action( 'woocommerce_add_order_fee_meta', $order_id, $item_id, $fee, $fee_key );
			}

			// Store shipping for all packages
			foreach ( WC()->shipping->get_packages() as $package_key => $package ) {
				if ( isset( $package['rates'][ $checkout->shipping_methods[ $package_key ] ] ) ) {
					$item_id = $order->add_shipping( $package['rates'][ $checkout->shipping_methods[ $package_key ] ] );

					if ( ! $item_id ) {
						throw new Exception( sprintf( __( 'Error %d: Unable to create order. Please try again.', 'manual_phone_orders' ), 404 ) );
					}

					// Allows plugins to add order item meta to shipping
					do_action( 'woocommerce_add_shipping_order_item', $order_id, $item_id, $package_key );
				}
			}

			// Store tax rows
			foreach ( array_keys( WC()->cart->taxes + WC()->cart->shipping_taxes ) as $tax_rate_id ) {
				if ( $tax_rate_id && ! $order->add_tax( $tax_rate_id, WC()->cart->get_tax_amount( $tax_rate_id ), WC()->cart->get_shipping_tax_amount( $tax_rate_id ) ) && apply_filters( 'woocommerce_cart_remove_taxes_zero_rate_id', 'zero-rated' ) !== $tax_rate_id ) {
					throw new Exception( sprintf( __( 'Error %d: Unable to create order. Please try again.', 'manual_phone_orders' ), 405 ) );
				}
			}

			// Store coupons
			foreach ( WC()->cart->get_coupons() as $code => $coupon ) {
				if ( ! $order->add_coupon( $code, WC()->cart->get_coupon_discount_amount( $code ), WC()->cart->get_coupon_discount_tax_amount( $code ) ) ) {
					throw new Exception( sprintf( __( 'Error %d: Unable to create order. Please try again.', 'manual_phone_orders' ), 406 ) );
				}
			}

			// Billing address
			$billing_address = array();
			if ( $checkout->checkout_fields['billing'] ) {
				foreach ( array_keys( $checkout->checkout_fields['billing'] ) as $field ) {
					$field_name = str_replace( 'billing_', '', $field );
					$billing_address[ $field_name ] = $checkout->get_posted_address_data( $field_name );
				}
			}

			// Shipping address.
			$shipping_address = array();
			if ( $checkout->checkout_fields['shipping'] ) {
				foreach ( array_keys( $checkout->checkout_fields['shipping'] ) as $field ) {
					$field_name = str_replace( 'shipping_', '', $field );
					$shipping_address[ $field_name ] = $checkout->get_posted_address_data( $field_name, 'shipping' );
				}
			}

			$order->set_address( $billing_address, 'billing' );
			$order->set_address( $shipping_address, 'shipping' );
			// IGNITEWOO MOD - USE OUR OWN VAR FOR PAYMENT SINCE IT IS PRIVATE IN THE CLASS AND CANNOT BE SET IN OUR CHECKOUT CODE
			$order->set_payment_method( $checkout->order_payment_method );
			$order->set_total( WC()->cart->shipping_total, 'shipping' );
			$order->set_total( WC()->cart->get_cart_discount_total(), 'cart_discount' );
			$order->set_total( WC()->cart->get_cart_discount_tax_total(), 'cart_discount_tax' );
			$order->set_total( WC()->cart->tax_total, 'tax' );
			$order->set_total( WC()->cart->shipping_tax_total, 'shipping_tax' );
			$order->set_total( WC()->cart->total );

			// Update user meta
			if ( $checkout->order_customer_id ) {
				if ( apply_filters( 'woocommerce_checkout_update_customer_data', true, $checkout ) ) {
					foreach ( $billing_address as $key => $value ) {
						update_user_meta( $checkout->order_customer_id, 'billing_' . $key, $value );
					}
					if ( WC()->cart->needs_shipping() ) {
						foreach ( $shipping_address as $key => $value ) {
							update_user_meta( $checkout->order_customer_id, 'shipping_' . $key, $value );
						}
					}
				}
				do_action( 'woocommerce_checkout_update_user_meta', $checkout->order_customer_id, $checkout->posted );
			}

			// Let plugins add meta
			do_action( 'woocommerce_checkout_update_order_meta', $order_id, $checkout->posted );

			// If we got here, the order was created without problems!
			$wpdb->query( 'COMMIT' );

		} catch ( Exception $e ) {
			// There was an error adding order data!
			$wpdb->query( 'ROLLBACK' );
			return new WP_Error( 'checkout-error', $e->getMessage() );
		}

		return $order_id;

	}

	protected function process_customer( $data ) {
		global $ign_opc, $user_ID;

		$checkout = WC()->checkout();

		if ( ! is_user_logged_in() && ( $checkout->is_registration_required() || ! empty( $data['createaccount'] ) ) ) {
			$username    = ! empty( $data['account_username'] ) ? $data['account_username'] : '';
			$password    = ! empty( $data['account_password'] ) ? $data['account_password'] : '';
			$customer_id = wc_create_new_customer( $data['billing_email'], $username, $password );

			if ( is_wp_error( $customer_id ) ) {
				throw new Exception( $customer_id->get_error_message() );
			}

			wp_set_current_user( $customer_id );
			wc_set_customer_auth_cookie( $customer_id );

			// As we are now logged in, checkout will need to refresh to show logged in data
			WC()->session->set( 'reload_checkout', true );

			// Also, recalculate cart totals to reveal any role-based discounts that were unavailable before registering
			WC()->cart->calculate_totals();
		}

		// if registration is not required, and the checkbox to create an account is not checked,
		// and there's no customer assigned to the order, then return to avoid overwriting the logged
		// in user's account info
		if ( empty( $data['create_account'] ) && !$checkout->is_registration_required() && empty( $_POST['ignitewoo_one_page_checkout_customer_id'] ) ) {
				return;
		}

		$ign_opc->switch_user_context_to_customer( 'customer' );

		if ( 0 == $user_ID ) {
			$ign_opc->switch_user_context_to_customer( 'opc_user' );
			return;
		}

		$customer_id = get_current_user_id();

		// Add customer info from other fields.
		if ( $customer_id && apply_filters( 'woocommerce_checkout_update_customer_data', true, $this ) ) {
			$customer = new WC_Customer( $customer_id );

			if ( ! empty( $data['billing_first_name'] ) ) {
				$customer->set_first_name( $data['billing_first_name'] );
			}

			if ( ! empty( $data['billing_last_name'] ) ) {
				$customer->set_last_name( $data['billing_last_name'] );
			}

			foreach ( $data as $key => $value ) {
				// Use setters where available.
				if ( is_callable( array( $customer, "set_{$key}" ) ) ) {
					$customer->{"set_{$key}"}( $value );

				// Store custom fields prefixed with wither shipping_ or billing_.
				} elseif ( 0 === stripos( $key, 'billing_' ) || 0 === stripos( $key, 'shipping_' ) ) {
					$customer->update_meta_data( $key, $value );
				}
			}

			/**
			 * Action hook to adjust customer before save.
			 * @since 2.7.0
			 */
			do_action( 'woocommerce_checkout_update_customer', $customer, $data );

			$customer->save();
		}

		do_action( 'woocommerce_checkout_update_user_meta', $customer_id, $data );

		$ign_opc->switch_user_context_to_customer( 'opc_user' );
	}

	// Based on WC core code
	public function process_checkout() {
		global $ign_opc;

		$woocommerce_checkout = WC()->checkout();

		try {

			// SWITCH TO CUSTOMER WHOSE NAME IS ON THE ORDER SINCE THE "UPDATE ORDER REVIEW"
			// CODE GENERATES A NONCE WHICH USES THE CURRENT USER ID, AND SINCE WE SWITCHED
			// CONTEXT TO SUPPORT WHOLESALE, TIERED, AND OTHER PLUGINS THAT WORK BASED ON THE
			// LOGGED IN USER.
			// DEPRECATED AS OF VERSION 3.0.22, nonce is now in admin context
			// $ign_opc->switch_user_context_to_customer( 'customer' );

			
			// IGNITEWOO MOD - CHANGE USER CONTEXT BACK TO ADMIN USER
			$ign_opc->switch_user_context_to_customer( 'opc_user' );

			$nonce = isset( $_REQUEST['woocommerce-process-checkout-nonce'] ) ? $_REQUEST['woocommerce-process-checkout-nonce'] : false;
			
			if ( empty( $nonce ) )
				$nonce = isset( $_REQUEST['_wpnonce'] ) ? $_REQUEST['_wpnonce'] : false;
				
			if ( empty( $nonce ) || ! wp_verify_nonce( $nonce, 'woocommerce-process_checkout' ) ) {
				$ign_opc->switch_user_context_to_customer( 'opc_user' );
				if ( !wp_verify_nonce( $_POST['_wpnonce'], 'woocommerce-process_checkout' ) ) {
					WC()->session->set( 'refresh_totals', true );
					throw new Exception( __( 'We were unable to process your order, please try again.', 'manual_phone_orders' ) );
				}
			}
			
			if ( ! defined( 'WOOCOMMERCE_CHECKOUT' ) ) {
				define( 'WOOCOMMERCE_CHECKOUT', true );
			}

			// Prevent timeout
			@set_time_limit(0);

			do_action( 'woocommerce_before_checkout_process' );

			if ( 0 === sizeof( WC()->cart->get_cart() ) ) {
				throw new Exception( sprintf( __( 'Sorry, your session has expired. <a href="%s" class="wc-backward">Return to homepage</a>', 'manual_phone_orders' ), home_url() ) );
			}

			do_action( 'woocommerce_checkout_process' );

			// Checkout fields (not defined in checkout_fields)
			$woocommerce_checkout->posted['terms']                     = isset( $_POST['terms'] ) ? 1 : 0;
			$woocommerce_checkout->posted['createaccount']             = isset( $_POST['createaccount'] ) && ! empty( $_POST['createaccount'] ) ? 1 : 0;
			$woocommerce_checkout->posted['payment_method']            = isset( $_POST['payment_method'] ) ? stripslashes( $_POST['payment_method'] ) : '';
			$woocommerce_checkout->posted['shipping_method']           = isset( $_POST['shipping_method'] ) ? $_POST['shipping_method'] : '';
			$woocommerce_checkout->posted['ship_to_different_address'] = isset( $_POST['ship_to_different_address'] ) ? true : false;

			$username = isset( $_POST['account_username'] ) ? $_POST['account_username'] : '';
			$password = isset( $_POST['account_password'] ) ? $_POST['account_password'] : '';

			if ( isset( $_POST['shiptobilling'] ) ) {
				_deprecated_argument( 'WC_Checkout::process_checkout()', '2.1', 'The "shiptobilling" field is deprecated. The template files are out of date' );

				$woocommerce_checkout->posted['ship_to_different_address'] = $_POST['shiptobilling'] ? false : true;
			}

			if ( function_exists( 'wc_ship_to_billing_address_only' ) )
				$ship_to_billing_address_only = wc_ship_to_billing_address_only();
			else 
				$ship_to_billing_address_only = WC()->cart->ship_to_billing_address_only();
				
			// Ship to billing only option
			if ( $ship_to_billing_address_only ) {
				$woocommerce_checkout->posted['ship_to_different_address']  = false;
			}

			// Update customer shipping and payment method to posted method
			$chosen_shipping_methods = WC()->session->get( 'chosen_shipping_methods' );

			if ( isset( $woocommerce_checkout->posted['shipping_method'] ) && is_array( $woocommerce_checkout->posted['shipping_method'] ) ) {
				foreach ( $woocommerce_checkout->posted['shipping_method'] as $i => $value ) {
					$chosen_shipping_methods[ $i ] = wc_clean( $value );
				}
			}

			WC()->session->set( 'chosen_shipping_methods', $chosen_shipping_methods );
			WC()->session->set( 'chosen_payment_method', $woocommerce_checkout->posted['payment_method'] );

			// Note if we skip shipping
			$skipped_shipping = false;

			// Get posted checkout_fields and do validation
			foreach ( $woocommerce_checkout->checkout_fields as $fieldset_key => $fieldset ) {

				// Skip shipping if not needed
				if ( $fieldset_key == 'shipping' && ( $woocommerce_checkout->posted['ship_to_different_address'] == false || ! WC()->cart->needs_shipping() ) ) {
					$skipped_shipping = true;
					continue;
				}

				// Skip account if not needed
				if ( $fieldset_key == 'account' && ( is_user_logged_in() || ( $woocommerce_checkout->must_create_account == false && empty( $woocommerce_checkout->posted['createaccount'] ) ) ) ) {
					continue;
				}

				foreach ( $fieldset as $key => $field ) {

					if ( ! isset( $field['type'] ) ) {
						$field['type'] = 'text';
					}

					// Get Value
					switch ( $field['type'] ) {
						case "checkbox" :
							$woocommerce_checkout->posted[ $key ] = isset( $_POST[ $key ] ) ? 1 : 0;
						break;
						case "multiselect" :
							$woocommerce_checkout->posted[ $key ] = isset( $_POST[ $key ] ) ? implode( ', ', array_map( 'wc_clean', $_POST[ $key ] ) ) : '';
						break;
						case "textarea" :
							$woocommerce_checkout->posted[ $key ] = isset( $_POST[ $key ] ) ? wp_strip_all_tags( wp_check_invalid_utf8( stripslashes( $_POST[ $key ] ) ) ) : '';
						break;
						default :
							$woocommerce_checkout->posted[ $key ] = isset( $_POST[ $key ] ) ? ( is_array( $_POST[ $key ] ) ? array_map( 'wc_clean', $_POST[ $key ] ) : wc_clean( $_POST[ $key ] ) ) : '';
						break;
					}

					// Hooks to allow modification of value
					$woocommerce_checkout->posted[ $key ] = apply_filters( 'woocommerce_process_checkout_' . sanitize_title( $field['type'] ) . '_field', $woocommerce_checkout->posted[ $key ] );
					$woocommerce_checkout->posted[ $key ] = apply_filters( 'woocommerce_process_checkout_field_' . $key, $woocommerce_checkout->posted[ $key ] );

					// Validation: Required fields
					if ( isset( $field['required'] ) && $field['required'] && empty( $woocommerce_checkout->posted[ $key ] ) ) {
						wc_add_notice( '<strong>' . $field['label'] . '</strong> ' . __( 'is a required field.', 'manual_phone_orders' ), 'error' );
					}

					if ( ! empty( $woocommerce_checkout->posted[ $key ] ) ) {

						// Validation rules
						if ( ! empty( $field['validate'] ) && is_array( $field['validate'] ) ) {
							foreach ( $field['validate'] as $rule ) {
								switch ( $rule ) {
									case 'postcode' :
										$woocommerce_checkout->posted[ $key ] = strtoupper( str_replace( ' ', '', $woocommerce_checkout->posted[ $key ] ) );

										if ( ! WC_Validation::is_postcode( $woocommerce_checkout->posted[ $key ], $_POST[ $fieldset_key . '_country' ] ) ) :
											wc_add_notice( __( 'Please enter a valid postcode/ZIP.', 'manual_phone_orders' ), 'error' );
										else :
											$woocommerce_checkout->posted[ $key ] = wc_format_postcode( $woocommerce_checkout->posted[ $key ], $_POST[ $fieldset_key . '_country' ] );
										endif;
									break;
									case 'phone' :
										$woocommerce_checkout->posted[ $key ] = wc_format_phone_number( $woocommerce_checkout->posted[ $key ] );

										if ( ! WC_Validation::is_phone( $woocommerce_checkout->posted[ $key ] ) )
											wc_add_notice( '<strong>' . $field['label'] . '</strong> ' . __( 'is not a valid phone number.', 'manual_phone_orders' ), 'error' );
									break;
									case 'email' :
										$woocommerce_checkout->posted[ $key ] = strtolower( $woocommerce_checkout->posted[ $key ] );

										if ( ! is_email( $woocommerce_checkout->posted[ $key ] ) )
											wc_add_notice( '<strong>' . $field['label'] . '</strong> ' . __( 'is not a valid email address.', 'manual_phone_orders' ), 'error' );
									break;
									case 'state' :
										// Get valid states
										$valid_states = WC()->countries->get_states( isset( $_POST[ $fieldset_key . '_country' ] ) ? $_POST[ $fieldset_key . '_country' ] : ( 'billing' === $fieldset_key ? WC()->customer->get_country() : WC()->customer->get_shipping_country() ) );

										if ( ! empty( $valid_states ) && is_array( $valid_states ) ) {
											$valid_state_values = array_flip( array_map( 'strtolower', $valid_states ) );

											// Convert value to key if set
											if ( isset( $valid_state_values[ strtolower( $woocommerce_checkout->posted[ $key ] ) ] ) ) {
												 $woocommerce_checkout->posted[ $key ] = $valid_state_values[ strtolower( $woocommerce_checkout->posted[ $key ] ) ];
											}
										}

										// Only validate if the country has specific state options
										if ( ! empty( $valid_states ) && is_array( $valid_states ) && sizeof( $valid_states ) > 0 ) {
											if ( ! in_array( $woocommerce_checkout->posted[ $key ], array_keys( $valid_states ) ) ) {
												wc_add_notice( '<strong>' . $field['label'] . '</strong> ' . __( 'is not valid. Please enter one of the following:', 'manual_phone_orders' ) . ' ' . implode( ', ', $valid_states ), 'error' );
											}
										}
									break;
								}
							}
						}
					}
				}
			}

			// Update customer location to posted location so we can correctly check available shipping methods
			if ( isset( $woocommerce_checkout->posted['billing_country'] ) ) {
				WC()->customer->set_country( $woocommerce_checkout->posted['billing_country'] );
			}
			if ( isset( $woocommerce_checkout->posted['billing_state'] ) ) {
				WC()->customer->set_state( $woocommerce_checkout->posted['billing_state'] );
			}
			if ( isset( $woocommerce_checkout->posted['billing_postcode'] ) ) {
				WC()->customer->set_postcode( $woocommerce_checkout->posted['billing_postcode'] );
			}

			// Shipping Information
			if ( ! $skipped_shipping ) {

				// Update customer location to posted location so we can correctly check available shipping methods
				if ( isset( $woocommerce_checkout->posted['shipping_country'] ) ) {
					WC()->customer->set_shipping_country( $woocommerce_checkout->posted['shipping_country'] );
				}
				if ( isset( $woocommerce_checkout->posted['shipping_state'] ) ) {
					WC()->customer->set_shipping_state( $woocommerce_checkout->posted['shipping_state'] );
				}
				if ( isset( $woocommerce_checkout->posted['shipping_postcode'] ) ) {
					WC()->customer->set_shipping_postcode( $woocommerce_checkout->posted['shipping_postcode'] );
				}

			} else {

				// Update customer location to posted location so we can correctly check available shipping methods
				if ( isset( $woocommerce_checkout->posted['billing_country'] ) ) {
					WC()->customer->set_shipping_country( $woocommerce_checkout->posted['billing_country'] );
				}
				if ( isset( $woocommerce_checkout->posted['billing_state'] ) ) {
					WC()->customer->set_shipping_state( $woocommerce_checkout->posted['billing_state'] );
				}
				if ( isset( $woocommerce_checkout->posted['billing_postcode'] ) ) {
					WC()->customer->set_shipping_postcode( $woocommerce_checkout->posted['billing_postcode'] );
				}

			}

			// Update cart totals now we have customer address
			$ign_opc->switch_user_context_to_customer( 'customer' );

			if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) {

				foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {

					$price = isset( $values['_price_altered'] ) ? $values['_price_altered'] : null;

					if ( null !== $price ) {
						WC()->cart->cart_contents[ $cart_item_key ]['data']->set_price( $price );
						WC()->cart->cart_contents[ $cart_item_key ]['data']->set_regular_price( $price );
						WC()->cart->cart_contents[ $cart_item_key ]['data']->set_sale_price( '' );
						WC()->cart->set_session();
					}
				}
			}

			WC()->cart->calculate_totals();

			$ign_opc->switch_user_context_to_customer( 'opc_user' );

			// Terms
			if ( ! isset( $_POST['woocommerce_checkout_update_totals'] ) && empty( $woocommerce_checkout->posted['terms'] ) && apply_filters( 'woocommerce_checkout_show_terms', wc_get_page_id( 'terms' ) > 0 ) ) {
				wc_add_notice( __( 'You must accept our Terms &amp; Conditions.', 'manual_phone_orders' ), 'error' );
			}

			if ( WC()->cart->needs_shipping() ) {

				if ( ! in_array( WC()->customer->get_shipping_country(), array_keys( WC()->countries->get_shipping_countries() ) ) ) {
					wc_add_notice( sprintf( __( 'Unfortunately <strong>we do not ship %s</strong>. Please enter an alternative shipping address.', 'manual_phone_orders' ), WC()->countries->shipping_to_prefix() . ' ' . WC()->customer->get_shipping_country() ), 'error' );
				}

				// Validate Shipping Methods
				$packages               = WC()->shipping->get_packages();
				$woocommerce_checkout->shipping_methods = WC()->session->get( 'chosen_shipping_methods' );

				foreach ( $packages as $i => $package ) {

					if ( ! isset( $package['rates'][ $woocommerce_checkout->shipping_methods[ $i ] ] ) ) {
						wc_add_notice( __( 'No shipping method has been selected. Please double check your address, or contact us if you need any help.', 'manual_phone_orders' ), 'error' );
						$woocommerce_checkout->shipping_methods[ $i ] = '';
					}
				}
			}

			if ( WC()->cart->needs_payment() ) {
				// Payment Method
				$available_gateways = WC()->payment_gateways->get_available_payment_gateways();

				if ( ! isset( $available_gateways[ $woocommerce_checkout->posted['payment_method'] ] ) ) {
					$woocommerce_checkout->order_payment_method = '';
					wc_add_notice( __( 'Invalid payment method.', 'manual_phone_orders' ), 'error' );
				} else {
					// IGNITEWOO MOD TO OVERRIDE PRIVATE VAR

					$method = isset( $_POST['payment_method'] ) ? stripslashes( $_POST['payment_method'] ) : '';
					$woocommerce_checkout->order_payment_method = $available_gateways[ $method ];

					if ( is_callable( array( $woocommerce_checkout->order_payment_method, 'validate_fields' ) ) )
						$woocommerce_checkout->order_payment_method->validate_fields();
				}
			} else {
				$available_gateways = array();
			}

			// Action after validation
			do_action( 'woocommerce_after_checkout_validation', $woocommerce_checkout->posted );

			if ( ! isset( $_POST['woocommerce_checkout_update_totals'] ) && wc_notice_count( 'error' ) == 0 ) {

				// IGNITEWOO MODS IN THIS "IF" WRAPPER AREA - Customer accounts

				// CUSTOM CUSTOMER_ID VAR SINCE IT IS PRIVATE IN THE CLASS
				$woocommerce_checkout->order_customer_id = isset( $_POST['ignitewoo_one_page_checkout_customer_id'] ) ? $_POST['ignitewoo_one_page_checkout_customer_id'] : null;

				$woocommerce_checkout->order_customer_id = apply_filters( 'woocommerce_checkout_customer_id', $woocommerce_checkout->order_customer_id );

				$woocommerce_checkout->must_create_account = $woocommerce_checkout->enable_guest_checkout ? false : true;

				//if ( ! is_user_logged_in() && ( $woocommerce_checkout->must_create_account || ! empty( $woocommerce_checkout->posted['createaccount'] ) ) ) {

				if ( ( $woocommerce_checkout->must_create_account || !empty( $woocommerce_checkout->posted['createaccount'] ) ) && empty( $woocommerce_checkout->order_customer_id ) ) {

					$new_customer = wc_create_new_customer( $woocommerce_checkout->posted['billing_email'], $username, $password );

					if ( is_wp_error( $new_customer ) ) {
						throw new Exception( $new_customer->get_error_message() );
					}

					$woocommerce_checkout->order_customer_id = $new_customer;

					$_POST['ignitewoo_one_page_checkout_customer_id'] = $new_customer;

					// wc_set_customer_auth_cookie( $woocommerce_checkout->customer_id );

					// As we are now logged in, checkout will need to refresh to show logged in data
					// WC()->session->set( 'reload_checkout', true );

					// Also, recalculate cart totals to reveal any role-based discounts that were unavailable before registering

					$ign_opc->switch_user_context_to_customer( 'customer' );

					WC()->cart->calculate_totals();

					// Add customer info from other billing fields

					if ( $woocommerce_checkout->posted['billing_first_name'] && apply_filters( 'woocommerce_checkout_update_customer_data', true, $woocommerce_checkout ) ) {
						$userdata = array(
							'ID'           => $woocommerce_checkout->order_customer_id,
							'first_name'   => $woocommerce_checkout->posted['billing_first_name'] ? $woocommerce_checkout->posted['billing_first_name'] : '',
							'last_name'    => $woocommerce_checkout->posted['billing_last_name'] ? $woocommerce_checkout->posted['billing_last_name'] : '',
							'display_name' => $woocommerce_checkout->posted['billing_first_name'] ? $woocommerce_checkout->posted['billing_first_name'] : ''
						);
						wp_update_user( apply_filters( 'woocommerce_checkout_customer_userdata', $userdata, $woocommerce_checkout ) );
					}

					$ign_opc->switch_user_context_to_customer( 'opc_user' );

				}

				// Do a final stock check at this point
				// This function calls an action "woocommerce_check_cart_items" which in turn
				// causes the cart class to check stock, availability, and coupons.
				// THIS PLUGIN doesn't need to do that, because it allows admin to add whatever they want to the cart
				// exactly as if manually creating an order in the admin area.
				// $woocommerce_checkout->check_cart_items();

				// Abort if errors are present
				if ( wc_notice_count( 'error' ) > 0 )
					throw new Exception();

				if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>' ) ) {

					//if ( !empty( $woocommerce_checkout->order_customer_id ) )
					//	$_POST['ignitewoo_one_page_checkout_customer_id'] = $woocommerce_checkout->order_customer_id;

					$this->process_customer( $woocommerce_checkout->posted );

					$order_id = $this->create_order_gt_27( $woocommerce_checkout->posted );

					if ( is_wp_error( $order_id ) ) {
						throw new Exception( $order->get_error_message() );
					}

					// No account being created, no customer assigned to the order, registration not required,
					// so set the customer ID to 0.
					if ( empty( $data['create_account'] ) && !$woocommerce_checkout->is_registration_required() && empty( $_POST['ignitewoo_one_page_checkout_customer_id'] ) ) {
							$o = wc_get_order( $order_id );
							$o->set_customer_id( 0 );
							$o->save();
					}

					//do_action( 'woocommerce_checkout_order_processed', $order_id, $posted_data, $order );

				} else {

					$order_id = $this->create_order( null, $woocommerce_checkout );

				}

				
				if ( empty( $order ) && !empty( $order_id ) ) {
					$order = wc_get_order( $order_id );
				}
				
				// Deprecated: This is now done via a "new order" hook along with a metabox to display it in the order.
				// IGNITEWOO MOD -- save user who is submitting this order to the order post meta data
				global $current_user;
				update_post_meta( $order_id, 'store_clerk', '( #' . $current_user->data->ID . ' ) - ' . $current_user->data->user_login );

				if ( is_wp_error( $order_id ) ) {
					throw new Exception( $order_id->get_error_message() );
				}

				do_action( 'woocommerce_checkout_order_processed', $order_id, $woocommerce_checkout->posted, $order );

				// IGNITEWOO MOD - unset this
				unset( WC()->session->order_awaiting_payment );

				// Process payment
				if ( WC()->cart->needs_payment() ) {

					// Store Order ID in session so it can be re-used after payment failure
					WC()->session->order_awaiting_payment = $order_id;

					// Switch to customer user so that stored card can be used if a payment gateway supports that feature
					$ign_opc->switch_user_context_to_customer( 'customer' );

					// Process Payment
					$result = $available_gateways[ $woocommerce_checkout->posted['payment_method'] ]->process_payment( $order_id );

					// Switch back to OPC user now that payment has been processed to whatever extent
					$ign_opc->switch_user_context_to_customer( 'opc_user' );

					// Redirect to success/confirmation/payment page
					if ( $result['result'] == 'success' ) {

						$result = apply_filters( 'woocommerce_payment_successful_result', $result, $order_id );

						if ( is_ajax() && version_compare( WOOCOMMERCE_VERSION, '2.4', '<' ) ) {
							echo '<!--WC_START-->' . json_encode( $result ) . '<!--WC_END-->';
							exit;
						} else if ( is_ajax() && version_compare( WOOCOMMERCE_VERSION, '2.4', '>=' ) ) {
							echo json_encode( $result );
							exit;
						} else {
							wp_redirect( $result['redirect'] );
							exit;
						}

					}

				} else {

					if ( empty( $order ) ) {
						$order = wc_get_order( $order_id );
					}

					// No payment was required for order
					$order->payment_complete();

					// Empty the Cart
					WC()->cart->empty_cart();

					// Get redirect
					$return_url = $order->get_checkout_order_received_url();

					// Redirect to success/confirmation/payment page
					if ( is_ajax() && version_compare( WOOCOMMERCE_VERSION, '2.4', '<' ) ) {
						echo '<!--WC_START-->' . json_encode(
							array(
								'result' 	=> 'success',
								'redirect'  => apply_filters( 'woocommerce_checkout_no_payment_needed_redirect', $return_url, $order )
							)
						) . '<!--WC_END-->';
						exit;
					} else if ( is_ajax() && version_compare( WOOCOMMERCE_VERSION, '2.4', '>=' ) ) {
						echo json_encode(
							array(
								'result' 	=> 'success',
								'redirect'  => apply_filters( 'woocommerce_checkout_no_payment_needed_redirect', $return_url, $order )
							)
						);
						exit;
					} else {
						wp_safe_redirect(
							apply_filters( 'woocommerce_checkout_no_payment_needed_redirect', $return_url, $order )
						);
						exit;
					}

				}

			}

		} catch ( Exception $e ) {
			if ( ! empty( $e ) ) {
				wc_add_notice( $e->getMessage(), 'error' );
			}
		}

		// If we reached this point then there were errors
		if ( is_ajax() && version_compare( WOOCOMMERCE_VERSION, '2.4', '<' ) ) {

			// only print notices if not reloading the checkout, otherwise they're lost in the page reload
			if ( ! isset( WC()->session->reload_checkout ) ) {
				ob_start();
				wc_print_notices();
				$messages = ob_get_clean();
			}

			echo '<!--WC_START-->' . json_encode(
				array(
					'result'	=> 'failure',
					'messages' 	=> isset( $messages ) ? $messages : '',
					'refresh' 	=> isset( WC()->session->refresh_totals ) ? 'true' : 'false',
					'reload'    => isset( WC()->session->reload_checkout ) ? 'true' : 'false'
				)
			) . '<!--WC_END-->';

			unset( WC()->session->refresh_totals, WC()->session->reload_checkout );
			exit;

		} else if ( is_ajax() && version_compare( WOOCOMMERCE_VERSION, '2.4', '>=' ) ) {

			// only print notices if not reloading the checkout, otherwise they're lost in the page reload
			if ( ! isset( WC()->session->reload_checkout ) ) {
				ob_start();
				wc_print_notices();
				$messages = ob_get_clean();
			}

			echo json_encode(
				array(
					'result'	=> 'failure',
					'messages' 	=> isset( $messages ) ? $messages : '',
					'refresh' 	=> isset( WC()->session->refresh_totals ) ? 'true' : 'false',
					'reload'    => isset( WC()->session->reload_checkout ) ? 'true' : 'false'
				)
			);

			unset( WC()->session->refresh_totals, WC()->session->reload_checkout );
			exit;
		}
	}


	public function before_customer_details() {
		?>

		<input type="hidden" value="1" name="opc_checkout_flag">

		<?php

	}

	// Used for products that needs special parms set, such as Donations and Gift Certs
	function setup_product( $pid, $_product ) {
		global $WC_Donations, $product, $post;

		$product = $_product;

		unset( $_product );

		if ( 'yes' == get_post_meta( $pid, '_donation', true ) ) {

			$product->opc_donation = $post->opc_donation = true;

			$WC_Donations->setup_product();

		} else {

			// Unset this var in case it was set for another product in the loop
			unset( $product->opc_donation, $post->opc_donation );

		}

		if ( '1' == get_post_meta( $pid, 'ignite_gift_enabled', true ) ) {

			if ( '1' == get_post_meta( $pid, 'ignite_buyer_sets_price', true ) )
				$product->opc_gift_cert = $post->opc_gift_cert = true;
			else
				unset( $product->opc_gift_cert, $post->opc_gift_cert );

		} else {

			unset( $product->opc_gift_cert, $post->opc_gift_cert );

		}

		// Product add-on forms support
		//global $addon_fields;

		$forms = get_post_meta( $pid, '_ign_forms', true );

		if ( !empty( $forms ) )
			$post->opc_product = $product->opc_product = true;
		else {
			// unset( $addon_fields );
			$post->opc_product = $product->opc_product = false;
		}

		return $product;

	}


	public function has_access() {
		global $ign_opc;
		return $ign_opc->has_access();
	}


	// Shortcode processor, loads the products and the checkout page.
	public function one_page( $attrs = array() ) {

		// Fix for pagebuilders that want to render shortcodes in the admin area, so they don't crash because no frontend code is loaded from WC
		if ( !function_exists( 'wc_notice_count' ) )
			return;
			
		if ( !$this->has_access() ) {
			echo '<p class="ignitewoo_one_page_access_denied" style="font-weight:bold; font-style:italic">' . __( 'You do not have access to this page.', 'manual_phone_orders' ) . '</p>';
			return;
		}

		if ( 'searchable' == $attrs['style'] ) {

			$this->style = $attrs['style'];

		} else {

			/* Not implemented - possible future mods
			if ( empty( $attrs['products'] ) )
				return;

			$pids = explode( ',' , $attrs['products'] );

			if ( empty( $pids ) )
				return;

			if ( empty( $attrs['style'] ) )
				$this->style = 'table';
			else
				$this->style = $attrs['style'];

			$this->products = array();


			// Load products, unless style is Single, in which case the template loads the products
			if ( 'single_consolidated' == $this->style ) {
				$this->products = $pids;
			} else
			foreach( $pids as $p ) {

				$this->load_product( $p );

			}
			*/
		}

		$this->maybe_disable_terms();

		if ( !empty( $this->settings['full_page'] ) && 'yes' == $this->settings['full_page'] )
			$class = 'ign_opc_full_width';
		else 
			$class = '';
			
		ob_start();

		echo '<div class="woocommerce-checkout ' . $class .'"><div class="woocommerce">';

		// Insert the checkout page
		$checkout = WC()->checkout();

		if ( empty( $_POST ) && wc_notice_count( 'error' ) > 0 ) {

			wc_get_template( 'checkout/cart-errors.php', array( 'checkout' => $checkout ) );

		} else {

			$non_js_checkout = ! empty( $_POST['woocommerce_checkout_update_totals'] ) ? true : false;

			if ( wc_notice_count( 'error' ) == 0 && $non_js_checkout )
				wc_add_notice( __( 'The order totals have been updated. Please confirm your order by pressing the Place Order button at the bottom of the page.', 'manual_phone_orders' ) );

			wc_get_template( 'checkout/form-checkout.php', array( 'checkout' => $checkout ) );

		}

		echo '</div></div>';

		return ob_get_clean();

	}

	public function maybe_disable_terms() {

		if ( empty( $this->settings ) )
			$this->settings = get_option( 'woocommerce_ignitewoo_manual_order_settings' );

		if ( isset( $this->settings['disable_terms'] ) && 'yes' == $this->settings['disable_terms'] ) {
			remove_all_filters( 'woocommerce_checkout_show_terms' );
			add_filter( 'woocommerce_checkout_show_terms', array( &$this, 'disable_terms' ), 1, 999999 );
		}

	}

	public function disable_terms( $val = null ) {
		return false;
	}

	public function load_product( $p ) {

		if ( empty( $p ) || absint( $p ) <= 0 )
			return;

		$prod = wc_get_product( $p );

		if ( version_compare( WOOCOMMERCE_VERSION, '2.4', '>=' ) )
			$type = $prod->get_type();
		else
			$type = $prod->product_type;

		if ( empty( $type ) || is_wp_error( $prod ) )
			return;

		if ( in_array( $type, array( 'simple', 'subscription' ) ) ) {

			// Any visibile attribs?
			$attributes = $this->map_attributes( $prod );

			$this->products[ $p ] = array( 'data' => $prod, 'attrs' => array(), 'url_parms' => array() );

		} else if ( $prod->is_type( 'grouped' ) ) {

			// Grouped products should have children, in theory
			// and those children are always simple products

			$children = $prod->get_children();

			if ( !empty( $children ) )
			foreach( $children as $child ) {

				$child_product = wc_get_product( $child );

				$this->products[ $child ] = array( 'data' => $child_product, 'attrs' => '', 'url_parms' => '' );

			}

		// Variable parent
		} else if ( in_array( $type, array( 'variable', 'variable-subscription' ) ) ) {

			$children = $prod->get_children();

			if ( !empty( $children ) ) {

				foreach( $children as $child ) {

					$child_product = wc_get_product( $child );

					$attributes = $this->map_attributes( $child_product );

					$parms = $this->get_parms( $child_product );

					$this->products[ $child ] = array( 'data' => $child_product, 'attrs' => $attributes, 'url_parms' => $parms );

				}
			}

		// Variation
		} else if ( $prod->is_type( 'variation' ) ) {

			$attributes = $this->map_attributes( $prod );

			// URL params
			$parms = $this->get_parms( $prod );

			$this->products[ $p ] = array( 'data' => $prod, 'attrs' => $attributes, 'url_parms' => $parms );

		}
		/*
		else if ( $prod->is_type( 'composite' ) ) {

			$cart = WC()->cart->get_cart();

			foreach( $cart as $cart_item_key => $item_data ) {

				$pid = !empty( $item_data['variation_id'] ) ? $item_data['variation_id'] : $item_data['product_id'];
				if ( $p == $pid ) {

					$this->products[ $p ] = array( 'data' => $item_data['data'], 'attrs' => array(), 'url_parms' => array() );
					$this->products[ $p ]['_price_altered'] = $prod->get_price();
					//$this->products[ $p ]['composite_children'] = $item_data['composite_children'];
					//$this->products[ $p ]['composite_data'] = $item_data['data']->get_composite_data();
//var_dump( $item_data['composite_children'] ); die;
					//$this->products[ $p ]['composite_children'] = $prod->();
				}

			}

		}
		*/
		$cart = WC()->cart->get_cart();

		foreach( $cart as $cart_item_key => $item_data ) {

			$pid = !empty( $item_data['variation_id'] ) ? $item_data['variation_id'] : $item_data['product_id'];

			if ( $p == $pid && isset( $item_data['_price_altered'] ) )
				$this->products[ $p ]['_price_altered'] = $item_data['_price_altered'];

			// Set the custom price for composite products
			/*
			if ( $item_data['data']->is_type( 'composite' ) ) {
var_dump( $item_data['data']->composite_data ); die;
				$this->products[ $p ]['_price_altered'] = $item_data['data']->get_price();
			}
			*/

		}

		return $prod; // useful for checking product types etc
	}


	public function load_template( $template_name = '', $template_path = '' ) {

		if ( empty( $template_name  ) )
			return;

		if ( ! $template_path ) {
			$template_path = WC()->template_path();
		}

		$template = locate_template( array( $template_path . 'phone-orders/' . $template_name ), false, false );

		if ( '' != $template )
			require( $template );
		else
			require( dirname( __FILE__ ) . '/phone-orders/' . $template_name );

	}

		
	function maybe_get_phone_manual_order_page_template( $template ) {

		if ( empty( $this->settings ) )
			return $template; 
			
		if ( !is_page( $this->settings['manual_order_page'] ) )
			return $template; 
		
		if ( empty( $this->settings['full_page'] ) || 'yes' !== $this->settings['full_page'] )
			return $template; 
			
		$page_template = $this->load_template( 'one_page_wrapper.php' );
		
		return $page_template;
	}


	public function get_parms( $prod ) {

		$parms = '';

		if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) ) {
			$variation_data = $this->wc_get_product_variation_attributes( $prod->get_id() );
		} else if ( version_compare( WOOCOMMERCE_VERSION, '2.3', '>=' ) ) {
			$variation_data = $this->wc_get_product_variation_attributes( $prod->variation_id );
		} else {
			$variation_data = $prod->variation_data;
		}

		if ( !empty( $variation_data ) )
			$parms = $this->array_implode( '=', '&', $variation_data );

		return $parms;

	}


	public function map_attributes( $prod ) {

		$attributes = array();

		//if ( $prod->is_type( 'variation' ) ) {
			//$data = $prod->variation_data;
			//var_dump( $data );
			//die;
		//}

		$attribs = $prod->get_attributes();

		if ( empty( $attribs ) )
			return $attributes;

		// Name / Value pairs
		foreach ( $attribs as $key => $attr ) {

			if ( $prod->is_type( 'variation' ) && empty( $attr['is_variation'] ) )
				continue;

			$name = wc_attribute_label( $attr['name'] );

			if ( taxonomy_exists( $attr['name'] ) )
				$key = 'attribute_pa_' . $key;
			else
				$key = 'attribute_' . $key;

			if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) )
				$variation_data = $this->wc_get_product_variation_attributes( $prod->get_id() );
			else if ( version_compare( WOOCOMMERCE_VERSION, '2.3', '>=' ) )
				$variation_data = $this->wc_get_product_variation_attributes( $prod->variation_id );
			else
				$variation_data = $prod->variation_data[ $key ];

			if ( isset( $variation_data ) && isset( $attributes[ $name ] ) && isset( $variation_data[ $key ] ) && $attributes[ $name ] )
				$attributes[ $name ] = $variation_data[ $key ];

			//if ( empty( $attributes[ $name ] ) && function_exists( 'wc_get_product_variation_attributes' ) )
			//	$attributes[ $name ] = wc_get_product_variation_attributes( $prod->variation_id );

			continue;

		}

		return $attributes;

	}

	public function json_headers() {

		header( 'Content-Type: application/json; charset=utf-8' );
	}


	public function get_refreshed_fragments() {

		if ( !class_exists( 'WC_AJAX' ) )
			return;

		$wc_ajax = new WC_AJAX();

		$wc_ajax->get_refreshed_fragments();

		unset( $wc_ajax );

	}

	public function ign_one_page_searchable_add( $args = null ) {
		global $cart_products, $display_images, $user_ID, $ign_opc;

		if ( !$this->has_access() )
			return;

		if ( !empty( $args ) )
			$ids = $args;
		else if ( isset( $_POST['items'] ) )
			$ids = $_POST['items'];

		if ( !is_array( $ids ) )
			$ids = explode( ',', $ids );

		if ( empty( $ids ) )
			die( __( 'No products found', 'manual_phone_orders' ) );

		$ign_opc->switch_user_context_to_customer( 'customer' );

		$cart_products = array();

		// Deprecrated in v2.4 to cause Select2 box to be emptied after each add to cart action
		// $products_qty_in_cart = self::get_qty_in_cart();

		$products_qty_in_cart = array();

		$temp_cart = WC()->cart->get_cart();

		// Deprecrated in v2.4 to cause Select2 box to be emptied after each add to cart action
		// WC()->cart->empty_cart();

		ob_start();

		foreach( $ids as $id ) {

			$pdata = $this->load_product( $id );

			// Check if the item is already in the cart, if so don't touch it, process next item
			if ( $pdata->product_type == 'variation' ) {

				$check_qty = isset( $products_qty_in_cart[ $id ] ) ? $products_qty_in_cart[ $id ] : 0;

			} else {

				$check_qty = isset( $products_qty_in_cart[ $id ] ) ? $products_qty_in_cart[ $id ] : 0;
			}

			if ( $check_qty > 0 )
				$item_quantity = $check_qty;
			else
				$item_quantity = 1;

			if ( 'grouped' == $pdata->product_type ) {

				$children = $pdata->get_children();

				$qty = array();

				if ( !empty( $children ) ) {

					foreach( $children as $child ) {

						$check_qty = isset( $products_qty_in_cart[ $child ] ) ? $products_qty_in_cart[ $child ] : 1;

						if ( empty( $check_qty) || $check_qty > 0 )
							$item_quantity = $check_qty;
						else
							$item_quantity = 1;

						$qty[ $child ] = $item_quantity;

					}

					$args = array(
						'add-to-cart' => $id,
						'product_id' => $id,
						'qty' => $qty,
						'empty_cart' => false
					);

					$this->add_to_cart_grouped( $args );

				}


			} else if ( 'variation' == $pdata->product_type ) {

				$_product = $this->products[ $id ];

				if ( version_compare( WOOCOMMERCE_VERSION, '2.3', '<=' ) )
					$variation_data = $this->get_parms( $_product['data'] );
				else
					$variation_data = $this->wc_get_product_variation_attributes( $id );

				$args = array(
					'product_id' => $_product['data']->parent->id,
					'variation_id' => $id,
					'variation_data' => $variation_data,
					'qty' => $item_quantity,
					'empty_cart' => false,
				);

				$this->add_variation_to_cart( $args );

			} else {

				//$_product = $this->products[ $id ];

				// Simple
				$args = array(
					'product_id' => $id,
					'qty' => $item_quantity,
					'empty_cart' => false
				);

				$this->add_item_to_cart( $args );

			}
		}

		//$cart = WC()->cart->get_cart();

		// Re-add any custom pricing that existed in the cart
		foreach( $temp_cart as $key => $vals ) {

			if ( empty( $vals['_price_altered'] )  )
				continue;

			WC()->cart->cart_contents[ $key ]['_price_altered'] = $vals['_price_altered'];
			WC()->cart->cart_contents[ $key ]['data']->set_price( $vals['_price_altered'] );
			WC()->cart->calculate_totals();

			$pid = isset( WC()->cart->cart_contents[ $key ]['variation_id'] ) ? WC()->cart->cart_contents[ $key ]['variation_id'] : WC()->cart->cart_contents[ $key ]['product_id'];

			foreach( $this->products as $id => $data ) {

				if ( $id == $pid )
					$this->products[ $id ]['_price_altered'] = $vals['_price_altered'];
			}

		}

		$cart_products = $this->products;

		$display_images = true;

		//$template_name = 'one_page_searchable_items_table.php';

		// Set this in case a third party plugin needs it, such as tiered pricing
		$_POST['opc_checkout_flag'] = 1;

 		self::display_cart_contents_table( $cart_products );

		$ign_opc->switch_user_context_to_customer( 'opc_user' );

		$out = ob_get_clean();

		if ( !empty( $args ) )
			$out = explode( '<!--WC_END-->', $out );

		// echo the last element in the array, there should not be any messages after the table
		echo end( $out );

		if ( !empty( $args ) )
			die();
	}


	public function update_items_table() {
		global $cart_products, $display_images, $ign_opc;

		$ign_opc->switch_user_context_to_customer( 'customer' );

		$display_images = true;

		// dummy load
		$cart_products = true;

		$template_name = 'one_page_searchable_items_table.php';

		$this->load_template( $template_name );

		die;
	}

	// For WC 2.3 support, newer version have this function already
	private function wc_get_product_variation_attributes( $variation_id ) {
		// Build variation data from meta
		$all_meta                = get_post_meta( $variation_id );
		$parent_id               = wp_get_post_parent_id( $variation_id );
		$parent_attributes       = array_filter( (array) get_post_meta( $parent_id, '_product_attributes', true ) );
		$found_parent_attributes = array();
		$variation_attributes    = array();

		// Compare to parent variable product attributes and ensure they match
		foreach ( $parent_attributes as $attribute_name => $options ) {
			$attribute                 = 'attribute_' . sanitize_title( $attribute_name );
			$found_parent_attributes[] = $attribute;
			if ( ! empty( $options['is_variation'] ) && ! array_key_exists( $attribute, $variation_attributes ) ) {
				$variation_attributes[ $attribute ] = ''; // Add it - 'any' will be asumed
			}
		}

		// Get the variation attributes from meta
		if ( !empty( $all_meta ) )
		foreach ( $all_meta as $name => $value ) {
			// Only look at valid attribute meta, and also compare variation level attributes and remove any which do not exist at parent level
			if ( 0 !== strpos( $name, 'attribute_' ) || ! in_array( $name, $found_parent_attributes ) ) {
				unset( $variation_attributes[ $name ] );
				continue;
			}
			/**
			* Pre 2.4 handling where 'slugs' were saved instead of the full text attribute.
			* Attempt to get full version of the text attribute from the parent.
			*/
			if ( sanitize_title( $value[0] ) === $value[0] && version_compare( get_post_meta( $parent_id, '_product_version', true ), '2.4.0', '<' ) ) {
				foreach ( $parent_attributes as $attribute ) {
					if ( $name !== 'attribute_' . sanitize_title( $attribute['name'] ) ) {
						continue;
					}
					$text_attributes = wc_get_text_attributes( $attribute['value'] );

					foreach ( $text_attributes as $text_attribute ) {
						if ( sanitize_title( $text_attribute ) === $value[0] ) {
							$value[0] = $text_attribute;
							break;
						}
					}
				}
			}

			$variation_attributes[ $name ] = $value[0];
		}

		return $variation_attributes;
	}

	public function display_cart_contents_table( $cart_products = null, $args = null  ) {
		global $cart_products, $display_images, $ign_opc;

		//if ( !empty( $args ) )
		//	$items = $args;

		//$ids = explode( ',', $ids );

		//if ( empty( $ids ) )
		//	die( __( 'No products found', 'manual_phone_orders' ) );

		//if ( empty( $cart_products ) ) {
			$cart_products = array();
		//}

		$cart_items = WC()->cart->get_cart();

		$items = array();

		foreach( $cart_items as $key => $item ) {

			//if ( $item['data']->is_type('composite' ) )
			//	continue;

			if ( $item['data']->is_type( 'variation' ) )
				$items[] = $item['variation_id'];
			else
				$items[] = $item['product_id'];
		}

		unset( $this->products );

		if ( !empty( $items ) ) {

			// Compare to cart, if it's not in the cart then don't load it into the table yet
			// This reloads the $this->products var.
			foreach( $items as $id ) {
				$pdata = $this->load_product( $id );

			}

			if ( !empty( $this->products ) ) {

				$cart_products = $this->products;

			} else {
				$cart_products = null;
			}
		}

		$display_images = true;

		$ign_opc->switch_user_context_to_customer( 'customer' );

		$template_name = 'one_page_searchable_items_table.php';

		$this->load_template( $template_name );

	}

	public function add_shipping_form() {

		$settings = get_option( 'woocommerce_phone_manual_dummy_shipping_settings' );

		if ( empty( $settings['enabled'] ) || 'yes' !== $settings['enabled'] )
			return;

		if ( !defined( 'DOING_AJAX' ) )
			return;

		if ( isset( $_POST['post_data'] )  ) {

			$args = wp_parse_args( $_POST['post_data'] );

			if ( empty( $args ) || empty( $args['opc_checkout_flag'] ) )
				return;

		}

		?>
		<tr>
			<th><?php _e( 'Custom Shipping Cost', 'manual_phone_orders' ) ?></th>
			<td>
				<input type="text" class="custom_shipping_cost" value="" style="width:100px">
				<button class="button set_shipping_cost"><?php _e( 'Set cost', 'manual_phone_orders' )?></button>
				<script> 
				jQuery( document ).ready( function( $ ) { 
					$( '.set_shipping_cost' ).on( 'click', function(e) {
						e.preventDefault();
						var cost = $( '.custom_shipping_cost' ).val();
						if ( null == cost || '' == cost )
							alert( opc_enhanced_select_params.set_shipping_cost_first );

						$.post( ign_one_page_ajax_url, { action: 'set_custom_shipping_cost', cost: cost }, function( data ) {
							if ( 'ok' == data ) {
								$( document.body ).trigger( 'update_checkout' );
							}

						});
						return false;
					});
				});
				</script>
			</td>
		</tr>

		<?php
	}

	public function set_custom_shipping_cost() {
		global $wpdb;

		if ( !isset( $_POST['cost'] ) )
			die;

		$sql = 'delete from ' . $wpdb->options . ' where ( option_name like "_transient_wc_ship_%" OR option_name like "_transient_timeout_wc_ship_%"  OR option_name like "_transient_shipping_%" )';

		$wpdb->query( $sql );

		$packages = WC()->cart->get_shipping_packages();

		$package_keys 		= array_keys( $packages );
		$package_keys_size 	= sizeof( $package_keys );

		for ( $i = 0; $i < $package_keys_size; $i ++ ) {
			//$this->packages[ $package_keys[ $i ] ] = $this->calculate_shipping_for_package( $packages[ $package_keys[ $i ] ] );
			$package_hash = 'wc_ship_' . md5( json_encode( $packages[ $package_keys[ $i ] ] ) . WC_Cache_Helper::get_transient_version( 'shipping' ) );

			delete_transient( $package_hash );
		}
		
		// Bust the shipping rates cache for WC 3.3.x so that the custom rate is updated properly
		for ( $i = 0; $i < $package_keys_size; $i ++ ) { 
		
			$session_key  = 'shipping_for_package_' . $packages[ $package_keys[ $i ] ];
			
			$the_key = WC()->session->get( $session_key );
			
			if ( empty( $the_key ) )
				continue;
			
			WC()->session->set( $session_key, false );
		}
		
		WC()->session->set( 'opc_custom_shipping_cost', false );
		WC()->session->set( 'opc_custom_shipping_cost', $_POST['cost'] );

		die('ok');

	}

	public function add_to_cart_donation() {

		$product_id        = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['product_id'] ) );

		$quantity          = 1;

		//$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );

		$passed_validation = true; // allow admin & store manager to add anyway, just like in the WP admin area

		//if ( 'true' === $_REQUEST['empty_cart'] )
		//	WC()->cart->empty_cart();

		$cik = null;

		foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {

			if ( $values['product_id'] == $product_id ) {
				$cik = $cart_item_key;
				break;
			}

		}

		if ( !empty( $cik ) )
			WC()->cart->set_quantity( $cik, 0 ); // remove so it can be re-added with the new qty

		if ( $passed_validation && self::add_to_cart( $product_id, $quantity ) ) {

			$_product = wc_get_product( $product_id );

			ob_start();

			//wc_add_notice( sprintf( __( '%s has been added to your order', 'manual_phone_orders' ), $_product->get_title() ) );

			//wc_print_notices();

			$messages = ob_get_clean();

			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
				'result' => 'success',
				'messages' => $messages
			);

			if ( is_ajax() && version_compare( WOOCOMMERCE_VERSION, '2.4', '<' ) ) {

				echo '<!--WC_START-->'. json_encode( $data ) . '<!--WC_END-->';
			} else {
				echo json_encode( $data );
			}


		} else {

			ob_start();

			wc_add_notice( __( 'Error adding donation to cart', 'manual_phone_orders' ), 'error' );

			wc_print_notices();

			$messages = ob_get_clean();

			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
				'result' => 'failure',
				'messages' => $messages
			);

			if ( is_ajax() && version_compare( WOOCOMMERCE_VERSION, '2.4', '<' ) ) {
				echo '<!--WC_START-->'. json_encode( $data ) . '<!--WC_END-->';
			} else {
				echo json_encode( $data );
			}
		}

		die();
	}


	public function add_to_cart_gift_cert() {

		$product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['product_id'] ) );

		$quantity = empty( $_POST['quantity'] ) ? 0 : apply_filters( 'woocommerce_stock_amount', $_POST['quantity'] );

		//$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );

		$passed_validation = true; // allow admin & store manager to add anyway, just like in the WP admin area

		//if ( 'true' === $_REQUEST['empty_cart'] )
		//	WC()->cart->empty_cart();

		$cik = null;

		foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {

			if ( $values['product_id'] == $product_id ) {
				$cik = $cart_item_key;
				break;
			}

		}

		if ( !empty( $cik ) )
			WC()->cart->set_quantity( $cik, 0 ); // remove so it can be re-added with the new qty

		if ( $passed_validation && self::add_to_cart( $product_id, $quantity ) ) {

			$_product = wc_get_product( $product_id );

			ob_start();

			//wc_add_notice( sprintf( __( '%s has been added to your order', 'manual_phone_orders' ), $_product->get_title() ) );

			//wc_print_notices();

			$messages = ob_get_clean();

			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
				'result' => 'success',
				'messages' => $messages
			);

			if ( is_ajax() && version_compare( WOOCOMMERCE_VERSION, '2.4', '<' ) ) {
				echo '<!--WC_START-->'. json_encode( $data ) . '<!--WC_END-->';
			} else {
				echo json_decode( $data );
			}


		} else {

			ob_start();

			wc_add_notice( __( 'Error adding item to cart', 'manual_phone_orders' ), 'error' );

			wc_print_notices();

			$messages = ob_get_clean();

			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
				'result' => 'failure',
				'messages' => $messages
			);

			if ( is_ajax() && version_compare( WOOCOMMERCE_VERSION, '2.4', '<' ) ) {
				echo '<!--WC_START-->'. json_encode( $data ) . '<!--WC_END-->';
			} else {
				echo json_encode( $data );
			}
		}

		die();
	}


	public function add_to_cart_product_remove() {

		//$product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['product_id'] ) );
		$product_id = absint( $_POST['product_id'] );

		if ( empty( $product_id ) )
			return;

		$variation_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['variation_id'] ) );

		foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {

			if ( !empty( $variation_id ) ) {

				if ( $values['variation_id'] == $variation_id && $values['product_id'] == $product_id )
					WC()->cart->set_quantity( $cart_item_key, 0 );

			} else if ( $values['product_id'] == $product_id ) {

				WC()->cart->set_quantity( $cart_item_key, 0 );
				
				// Delete an arbitrary product if removed from the cart.
				// 
				// This only applies to simple product since arbitrary products can only be simple products
					
				$product = $values['data'];
				
				if ( method_exists( $product, 'get_id' ) )
					$pid = $product->get_id();
				else 
					$pid = $product->id; 
					
				if ( empty( $pid ) ) 
					continue;
					
				if ( method_exists( $product, 'get_meta' ) )
					$is_arb = $product->get_meta( '_ign_opc_arbitrary_product', true );
				else 
					$is_arb = get_post_meta( $product_id, '_ign_opc_arbitrary_product', true );

				if ( !empty( $is_arb ) )
					wp_delete_post( $product_id, true );
			}
		}

		$out = array();

		if ( !empty( $variation_id ) )
			$out['sel'] = $variation_id;
		else
			$out['sel'] = $product_id;

		die( '<!--WC_START-->' . json_encode( $out ) . '<!--WC_END-->' );

	}

	public function standardize_number( $value ){

		//using OPC as a temp holder so that decimals and thousands don't end up the same
		$value = str_replace(
			array( get_option( 'woocommerce_price_thousand_sep' ), get_option( 'woocommerce_price_decimal_sep' ), 'NYP' ),
			array( 'OPC', '.', '' ),
			$value
		);

		if ( function_exists( 'wc_format_decimal' ) )
			return wc_format_decimal( $value );
		else
			return woocommerce_format_total ( $value );

	}

	function maybe_save_new_price( $cart_item_key, $product_id = null, $quantity= null, $variation_id= null, $variation= null ) {

		if ( isset( $_REQUEST['price'] ) && isset( $_REQUEST['action'] ) && in_array( $_REQUEST['action'], array( 'woocommerce_add_to_cart_opc', 'woocommerce_add_to_cart_opc_variable' ) ) ) {
		
			WC()->cart->cart_contents[ $cart_item_key ]['_price_altered'] = $this->standardize_number( $_REQUEST['price'] );
			WC()->cart->set_session();
		}
	}

	// This is not necessary

	function maybe_calculate_new_prices( $cart_object ) {

		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) )
			return;

		foreach ( $cart_object->get_cart() as $cart_item_key => $value ) {

			if ( !isset( WC()->cart->cart_contents[ $cart_item_key ]['_price_altered'] ) )
				continue;

			//if ( !WC()->session->__isset( $cart_item_key . '_price_altered' ) )
			//	continue;

			$price = WC()->cart->cart_contents[ $cart_item_key ]['_price_altered'];

			$value['data']->price = $price;
		}
	}

	function maybe_return_price( $price = '', $_product ) {

		if ( !$this->opc )
			return $price;
			
		if ( function_exists( 'get_current_screen' ) ) {
			$current_screen = get_current_screen();

			if ( !empty( $current_screen ) && 'edit-product' == $current_screen->id )
				return $price;
		}

		$cart = WC()->cart;

		if ( empty( $cart ) )
			return $price;

		//$product_id = ign_get_product_id( $_product );

		if ( $_product->is_type( 'variation' ) ) {
		
			if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) )
				$variation_id = $_product->get_id();
			else 
				$variation_id = $_product->variation_id;

			foreach( $cart->cart_contents as $k => $v ) {

				if ( $v['variation_id'] == $variation_id && isset( $v['_price_altered'] ) ) {
					$price = $v['_price_altered'];
				}
			}

		} else {
			if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) )
				$product_id = $_product->get_id();
			else 
				$product_id = $_product->id;
				
			foreach( $cart->cart_contents as $k => $v ) {

				if ( $v['product_id'] == $product_id && isset( $v['_price_altered'] ) ) {
					$price = $v['_price_altered'];
				}
			}
		}

		return $price;
	}

	// Our custom exit function.
	// Used to prevents all the add to cart functions from exiting if the 
	// cart is being updated in bulk via the Update the Cart button
	public function exit_ajax( $code = false ) { 
		if ( !empty( $this->updating_cart_in_bulk ) )
			return;
			
		if ( !empty( $code ) )
			die( $code );
		else 
			die;
	}
	
	public function clear_cart() { 
		global $cart_products, $display_images, $ign_opc;
		
		WC()->cart->empty_cart();
		
		$cart_products = false;

		$ign_opc->switch_user_context_to_customer( 'customer' );

		$template_name = 'one_page_searchable_items_table.php';

		$this->load_template( $template_name );
		
		die;
	}
	
	public function update_items_to_cart() { 

		if ( empty( $_REQUEST['items'] ) )
			die;
			
		$items = $_REQUEST['items'];
		
		$_REQUEST = array();

		$this->updating_cart_in_bulk = true;

		foreach( $items as $item ) { 
			$_REQUEST = $item;
			
			if ( !empty( $item['variation_id'] ) )
				$this->add_variation_to_cart();
			else 
				$this->add_item_to_cart();
		}
		
		$this->updating_cart_in_bulk = false;
		$this->exit_ajax( '1' );
	}
	
	// Based on WC core
	// $args is passed only when using the "Searchable" page template style
	public function add_item_to_cart( $args = null ) {

		if ( !empty( $args ) )
			$_REQUEST = $args;

		if ( isset( $_REQUEST['type'] ) && 'grouped' == $_REQUEST['type'] ) {
			$this->add_to_cart_grouped();
			return;
		}

		$product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_REQUEST['product_id'] ) );

		$quantity = empty( $_REQUEST['qty'] ) ? 0 : apply_filters( 'woocommerce_stock_amount', $_REQUEST['qty'] );

		//$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
		$passed_validation = true; // allow admin & store manager to add anyway, just like in the WP admin area

		//if ( 'true' === $_REQUEST['empty_cart'] )
		//	WC()->cart->empty_cart();

		$price = null;

		if ( isset( $_REQUEST['price'] ) ) {
			// Remove thousands sep first. 
			$th = wc_get_price_thousand_separator();
			$_REQUEST['price'] = str_replace( $th, '', $_REQUEST['price'] );
			$price = wc_format_decimal( $_REQUEST['price'] );
			//$price = floatval( $_REQUEST['price'] );
		}

		if ( isset( $_REQUEST['adjust_qty'] ) && 'true' == $_REQUEST['adjust_qty'] ) {

			$cik = null;

			foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {

				if ( $values['product_id'] == $product_id ) {
					$cik = $cart_item_key;
					break;
				}
			}
				
			if ( !empty( $cik ) )
				WC()->cart->set_quantity( $cik, 0 ); // remove so it can be re-added with the new qty
				
			if ( $quantity <= 0 ) {

				$_product = wc_get_product( $product_id );

				ob_start();

				wc_add_notice( sprintf( __( '%s has been removed from your order', 'manual_phone_orders' ), $_product->get_title() ) );

				wc_print_notices();

				$messages = ob_get_clean();

				// If there was an error adding to the cart, redirect to the product page to show any errors
				$data = array(
					'result' => 'failure',
					'messages' => $messages
				);

				echo '<!--WC_START-->' . json_encode( $data ) . '<!--WC_END-->';

				$this->exit_ajax();
			}
		}

		if ( $passed_validation && self::add_to_cart( $product_id, $quantity ) ) {

			//do_action( 'woocommerce_ajax_added_to_cart', $product_id );

			$cik = false; 
			
			$_product = wc_get_product( $product_id );
			
			foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {

				if ( $values['product_id'] == $product_id ) {
					$cik = $cart_item_key;
					break;
				}
			}
			
			if ( !empty( $cik ) && null !== $price ) {
				WC()->cart->cart_contents[ $cik ]['_price_altered'] = $price;
				WC()->cart->cart_contents[ $cik ]['data']->price_altered = $price;

				if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) {
					WC()->cart->cart_contents[ $cik ]['data']->set_price( $price );
					WC()->cart->cart_contents[ $cik ]['data']->set_regular_price( $price );
					WC()->cart->cart_contents[ $cik ]['data']->set_sale_price( '' );
					WC()->cart->set_session();
				} else {
					WC()->cart->cart_contents[ $cik ]['data']->price = $price;
					WC()->cart->cart_contents[ $cik ]['data']->regular_price = $price;
					WC()->cart->cart_contents[ $cik ]['data']->sale_price = $price;
				}
			}
			
			ob_start();

			//wc_add_notice( sprintf( __( '%s has been added to your order', 'manual_phone_orders' ), $_product->get_title() ) );

			//wc_print_notices();

			$messages = ob_get_clean();

			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
				'result' => 'success',
				'messages' => $messages
			);

			echo '<!--WC_START-->'. json_encode( $data ) . '<!--WC_END-->';

		} else {

			ob_start();

			wc_add_notice( __( 'Error adding item to cart', 'manual_phone_orders' ), 'error' );

			wc_print_notices();

			$messages = ob_get_clean();

			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
				'result' => 'failure',
				'messages' => $messages
			);

			echo '<!--WC_START-->'. json_encode( $data ) . '<!--WC_END-->';
		}

		if ( empty( $args ) )
			$this->exit_ajax();
	}

	public function add_to_cart_grouped( $args = null ) {

		if ( !empty( $args ) )
			$_REQUEST['args'] = $args;

		if ( empty( $_REQUEST['args'] ) )
			$this->exit_ajax();

		$args = wp_parse_args( $_REQUEST['args'] );

		if ( empty( $args['qty'] ) )
			$this->exit_ajax();

		$product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $args['add-to-cart'] ) );

		$removed_from_cart = $added_to_cart = array();

		unset( $_REQUEST['qty'] );

		foreach( $args['qty'] as $pid => $qty )
			$_REQUEST['qty'][ $pid ] = $qty;

		if ( ! empty( $_REQUEST['qty'] ) && is_array( $_REQUEST['qty'] ) ) {

			$quantity_set = false;

			foreach ( $_REQUEST['qty'] as $item => $quantity ) {

				if ( isset( $_REQUEST['adjust_qty'] ) && 'true' == $_REQUEST['adjust_qty'] ) {

					$cik = null;

					foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {

						if ( $values['product_id'] == $item ) {
							$cik = $cart_item_key;
							break;
						}

					}

					if ( !empty( $cik ) )
						WC()->cart->set_quantity( $cik, 0 ); // remove so it can be re-added with the new qty

				}

				if ( $quantity <= 0 ) {
					$removed_from_cart[] = $item;
					continue;
				}

				$quantity_set = true;

				// Add to cart validation
				$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $item, $quantity );

				if ( $passed_validation ) {
					if ( self::add_to_cart( $item, $quantity ) ) {
						$was_added_to_cart = true;
						$added_to_cart[] = $item;
					}
				}
			}

			$messages = '';

			/*
			if ( $was_added_to_cart && !empty( $added_to_cart ) ) {

				ob_start();

				foreach( $added_to_cart as $pid ) {

					//$_product = wc_get_product( $pid );

					//wc_add_notice( sprintf( __( '%s has been added to your order', 'manual_phone_orders' ), $_product->get_title() ) );

				}

				//wc_print_notices();

				$messages .= ob_get_clean();

			}
			*/

			if ( !empty( $removed_from_cart ) ) {

				ob_start();

				foreach( $removed_from_cart as $pid ) {

					$_product = wc_get_product( $pid );

					wc_add_notice( sprintf( __( '%s has been removed from your order', 'manual_phone_orders' ), $_product->get_title() ) );

				}

				wc_print_notices();

				$messages .= ob_get_clean();

			}

			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
				'result' => 'success',
				'messages' => $messages
			);

			echo '<!--WC_START-->'. json_encode( $data ) . '<!--WC_END-->';

			if ( empty( $args ) )
				die;

			/*
			if ( ! $was_added_to_cart && ! $quantity_set ) {

				ob_start();

				wc_add_notice( __( 'Please choose the quantity of items you wish to add to your cart&hellip;', 'manual_phone_orders' ), 'error' );

				wc_print_notices();

				$messages = ob_get_clean();

				// If there was an error adding to the cart, redirect to the product page to show any errors
				$data = array(
					'result' => 'failure',
					'messages' => $messages
				

				echo '<!--WC_START-->'. json_encode( $data ) . '<!--WC_END-->';

				$this->exit_ajax();
			}
			*/

		} elseif ( $product_id ) {

			ob_start();

			wc_add_notice( __( 'Please choose a product to add to your cart&hellip;', 'manual_phone_orders' ), 'error' );

			wc_print_notices();

			$messages = ob_get_clean();

			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
				'result' => 'failure',
				'messages' => $messages
			);

			echo '<!--WC_START-->'. json_encode( $data ) . '<!--WC_END-->';

			if ( empty( $args ) )
				$this->exit_ajax();
		}

	}

	// Based on WC core - REQUIRES 2.4.6 or newer
	// $args is passed only when using the "Searchable" page template style
	public function add_variation_to_cart( $args = null ) {
		$was_added_to_cart = false;

		$added_to_cart = array();

		if ( !empty( $args ) )
			$_REQUEST = $args;

		if ( isset( $_REQUEST['args'] ) ) {

			$args = wp_parse_args( $_REQUEST['args'] );

			unset( $args['add-to-cart'] );

			$quantity = empty( $args['qty'] ) ? 0 : apply_filters( 'woocommerce_stock_amount', $args['qty'] );

			unset( $args['qty'] );

			$product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $args['product_id'] ) );

			unset( $args['product_id'] );

			$adding_to_cart = wc_get_product( $product_id );

			//$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );

			$variation_id = empty( $args['variation_id'] ) ? '' : absint( $args['variation_id'] );

			unset( $args['variation_id'] );

			// Put the remaining args, which should be only attribute selections, in the $_REQUEST array for processing
			foreach( $args as $k => $d )
				$_REQUEST[ $k ] = $d;

		} else {

			$product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_REQUEST['product_id'] ) );

			$adding_to_cart = wc_get_product( $product_id );

			$quantity = empty( $_REQUEST['qty'] ) ? 0 : apply_filters( 'woocommerce_stock_amount', $_REQUEST['qty'] );

			//$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );

			$variation_id = empty( $_REQUEST['variation_id'] ) ? '' : absint( $_REQUEST['variation_id'] );

			$variation_data = empty( $_REQUEST['variation_data'] ) ? '' : $_REQUEST['variation_data'];

			if ( !empty( $variation_data ) ) {

				$data = wp_parse_args( $variation_data );

				foreach( $data as $k => $d )
					$_REQUEST[ $k ] = $d;
			}
		}

		try {
			$variation_id       = empty( $_REQUEST['variation_id'] ) ? '' : absint( $_REQUEST['variation_id'] );
			$quantity           = empty( $_REQUEST['qty'] ) ? 1 : $_REQUEST['qty'];
			$missing_attributes = array();
			$variations         = array();
			$adding_to_cart     = wc_get_product( $product_id );

			if ( ! $adding_to_cart ) {
				return false;
			}

			// If the $product_id was in fact a variation ID, update the variables.
			if ( $adding_to_cart->is_type( 'variation' ) ) {
				$variation_id   = $product_id;
				$product_id     = $adding_to_cart->get_parent_id();
				$adding_to_cart = wc_get_product( $product_id );

				if ( ! $adding_to_cart ) {
					return false;
				}
			}

			// If no variation ID is set, attempt to get a variation ID from posted attributes.
			if ( empty( $variation_id ) ) {
				$data_store   = WC_Data_Store::load( 'product' );
				$variation_id = $data_store->find_matching_product_variation( $adding_to_cart, array_map( 'sanitize_title', wp_unslash( $_REQUEST ) ) );
			}

			// Validate the attributes.
			if ( empty( $variation_id ) ) {
				throw new Exception( __( 'Please choose product options&hellip;', 'manual_phone_orders' ) );
			}

			$variation_data = wc_get_product_variation_attributes( $variation_id );

			foreach ( $adding_to_cart->get_attributes() as $attribute ) {
				if ( ! $attribute['is_variation'] ) {
					continue;
				}

				if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) )
					$taxonomy = sanitize_title( $attribute->get_name() );
				else 
					$taxonomy = sanitize_title( $attribute['name'] );

				$taxonomy = 'attribute_' . sanitize_title( $taxonomy );
//@ini_set( 'display_errors', true );
//@ini_set( 'error_reporting', E_ALL );
				if ( isset( $variation_data[ $taxonomy ] ) ) {
				
					if ( $attribute['is_taxonomy'] ) {
						// Don't use wc_clean as it destroys sanitized characters.
						$value = sanitize_title( wp_unslash( $variation_data[ $taxonomy ] ) );
					} else {
						$value = wc_clean( wp_unslash( $variation_data[ $taxonomy ] ) );
					}

					// Get valid value from variation data.
					$valid_value = isset( $variation_data[ $taxonomy ] ) ? $variation_data[ $taxonomy ] : '';

					// Allow if valid or show error.
					if ( $valid_value === $value ) {
						$variations[ $taxonomy ] = $value;
					} elseif ( '' === $valid_value && in_array( $value, $attribute->get_slugs() ) ) {
						// If valid values are empty, this is an 'any' variation so get all possible values.
						$variations[ $taxonomy ] = $value;
					} else {
						throw new Exception( sprintf( __( 'Invalid value posted for %s', 'manual_phone_orders' ), wc_attribute_label( $attribute['name'] ) ) );
					}
				} else {
					$missing_attributes[] = wc_attribute_label( $attribute['name'] );
				}
			}

			if ( ! empty( $missing_attributes ) ) {
				throw new Exception( sprintf( _n( '%s is a required field', '%s are required fields', count( $missing_attributes ), 'manual_phone_orders' ), wc_format_list_of_items( $missing_attributes ) ) );
			}
		} catch ( Exception $e ) {
			wc_add_notice( $e->getMessage(), 'error' );
			return false;
		}

		$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity, $variation_id, $variations );

		$cik = false; 

		// Find the cart item key, version 3.0.13 and newer send the key with a request when changing price or adding to cart, if the key is known because 
		// the item is in the cart already 
		foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
			if ( !empty( $_REQUEST['cart_item_key'] ) )
				$cik = $_REQUEST['cart_item_key'];
			else if ( $values['product_id'] == $product_id && $values['variation_id'] == $variation_id ) {
				$cik = $cart_item_key;
				break;
			}
		}
		
		if ( !empty( $cik ) )
			WC()->cart->set_quantity( $cik, 0 ); // remove so it can be re-added with the new qty

		if ( $quantity <= 0 ) {

			$_product = wc_get_product( $product_id );

			ob_start();
			wc_add_notice( sprintf( __( '%s has been removed from your order', 'manual_phone_orders' ), $_product->get_title() ) );
			wc_print_notices();

			$messages = ob_get_clean();

			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
				'result' => 'failure',
				'messages' => $messages
			);

			echo '<!--WC_START-->'. json_encode( $data ) . '<!--WC_END-->';

			$this->exit_ajax();
		}

		if ( $passed_validation && false !== self::add_to_cart( $product_id, $quantity, $variation_id, $variations ) ) {
			//wc_add_to_cart_message( array( $product_id => $quantity ), true );
		}
				
		if ( isset( $_REQUEST['adjust_qty'] ) && 'true' == $_REQUEST['adjust_qty'] ) {
		
			if ( !empty( $_REQUEST['price'] ) )
				$price = $this->standardize_number( $_REQUEST['price'] );

			if ( null !== $price ) {
				WC()->cart->cart_contents[ $cik ]['_price_altered'] = $price;
				@WC()->cart->cart_contents[ $cik ]['data']->price_altered = $price;
				if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) {
					WC()->cart->cart_contents[ $cik ]['data']->set_price( $price );
					WC()->cart->cart_contents[ $cik ]['data']->set_regular_price( $price );
					WC()->cart->cart_contents[ $cik ]['data']->set_sale_price( '' );
					WC()->cart->set_session();
				} else {
					WC()->cart->cart_contents[ $cik ]['data']->price = $price;
					WC()->cart->cart_contents[ $cik ]['data']->regular_price = $price;
					WC()->cart->cart_contents[ $cik ]['data']->sale_price = $price;
				}
			}
		}
		
		if ( empty( $args ) )
			$this->exit_ajax( '1' );
	}

	public function before_checkout_process() {

		if ( sizeof( WC()->cart->get_cart() ) == 0 ) {

			ob_start();

			wc_add_notice( __( 'You have not selected any items yet', 'manual_phone_orders' ), 'error' );

			wc_print_notices();

			$messages = ob_get_clean();

			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
				'result' => 'failure',
				'messages' => $messages
			);

			echo '<!--WC_START-->'. json_encode( $data ) . '<!--WC_END-->';

			die;   
		}
	}

	public static function get_qty_in_cart( $pid = null, $vid = null ) {

		$qty = 1;

		// return an array of item quantities in cart
		if ( empty( $pid ) && empty( $vid ) ) {

			if ( sizeof( WC()->cart->get_cart() ) <= 0 )
				return array();

			foreach ( WC()->cart->get_cart() as $key => $item ) {

				if ( !empty( $item['variation_id'] ) )
					$q[ $item['variation_id'] ] = $item['quantity'];
				else
					$q[ $item['product_id'] ] = $item['quantity'];

			}

			return $q;

		}

		// get the individual product quantity in cart
		if ( sizeof( WC()->cart->get_cart() ) <= 0 )
			return 1;

		foreach ( WC()->cart->get_cart() as $key => $item ) {

			if ( !empty( $pid ) && $pid !== $item['product_id'] )
				continue;

			if ( !empty( $vid ) && $vid !== $item['variation_id'] )
				continue;

			$qty = $item['quantity'];

			break;

		}

		return $qty;

	}
	
	// Based on WC core code
	public function add_to_cart( $product_id = 0, $quantity = 1, $variation_id = 0, $variation = array(), $cart_item_data = array() ) {
		// Wrap in try catch so plugins can throw an exception to prevent adding to cart

		try {
		
			$product_id   = absint( $product_id );
			$variation_id = absint( $variation_id );

			// Ensure we don't add a variation to the cart directly by variation ID.
			if ( 'product_variation' === get_post_type( $product_id ) ) {
				$variation_id = $product_id;
				$product_id   = wp_get_post_parent_id( $variation_id );
			}

			$product_data = wc_get_product( $variation_id ? $variation_id : $product_id );

			// Sanitity check
			if ( $quantity <= 0 || ! $product_data || 'trash' === $product_data->post->post_status  ) {
				if ( version_compare( WOOCOMMERCE_VERSION, '3.1', '>=' ) )
					return false;
				else 
					throw new Exception();
			}

			// Load cart item data - may be added by other plugins
			$cart_item_data = (array) apply_filters( 'woocommerce_add_cart_item_data', $cart_item_data, $product_id, $variation_id );

			// Generate a ID based on product ID, variation ID, variation data, and other cart item data
			$cart_id        = WC()->cart->generate_cart_id( $product_id, $variation_id, $variation, $cart_item_data );

			// Find the cart item key in the existing cart
			$cart_item_key  = WC()->cart->find_product_in_cart( $cart_id );

			// Force quantity to 1 if sold individually and check for exisitng item in cart
			/*
			if ( $product_data->is_sold_individually() ) {
				$quantity         = apply_filters( 'woocommerce_add_to_cart_sold_individually_quantity', 1, $quantity, $product_id, $variation_id, $cart_item_data );
				$in_cart_quantity = $cart_item_key ? WC()->cart->cart_contents[ $cart_item_key ]['quantity'] : 0;

				if ( $in_cart_quantity > 0 ) {
					throw new Exception( sprintf( '<a href="%s" class="button wc-forward">%s</a> %s', WC()->cart->get_cart_url(), __( 'View Cart', 'manual_phone_orders' ), sprintf( __( 'You cannot add another &quot;%s&quot; to your cart.', 'manual_phone_orders' ), $product_data->get_title() ) ) );
				}
			}

			// Check product is_purchasable
			if ( ! $product_data->is_purchasable() ) {
				throw new Exception( __( 'Sorry, this product cannot be purchased.', 'manual_phone_orders' ) );
			}

			// Stock check - only check if we're managing stock and backorders are not allowed
			if ( ! $product_data->is_in_stock() ) {
				throw new Exception( sprintf( __( 'You cannot add &quot;%s&quot; to the cart because the product is out of stock.', 'manual_phone_orders' ), $product_data->get_title() ) );
			}

			if ( ! $product_data->has_enough_stock( $quantity ) ) {
				throw new Exception( sprintf(__( 'You cannot add that amount of &quot;%s&quot; to the cart because there is not enough stock (%s remaining).', 'manual_phone_orders' ), $product_data->get_title(), $product_data->get_stock_quantity() ) );
			}


			// Stock check - this time accounting for whats already in-cart
			if ( $managing_stock = $product_data->managing_stock() ) {
				$products_qty_in_cart = WC()->cart->get_cart_item_quantities();

				if ( $product_data->is_type( 'variation' ) && true === $managing_stock ) {
					$check_qty = isset( $products_qty_in_cart[ $variation_id ] ) ? $products_qty_in_cart[ $variation_id ] : 0;
				} else {
					$check_qty = isset( $products_qty_in_cart[ $product_id ] ) ? $products_qty_in_cart[ $product_id ] : 0;
				}


				//Check stock based on all items in the cart

				if ( ! $product_data->has_enough_stock( $check_qty + $quantity ) ) {
					throw new Exception( sprintf(
						'<a href="%s" class="button wc-forward">%s</a> %s',
						WC()->cart->get_cart_url(),
						__( 'View Cart', 'manual_phone_orders' ),
						sprintf( __( 'You cannot add that amount to the cart &mdash; we have %s in stock and you already have %s in your cart.', 'manual_phone_orders' ), $product_data->get_stock_quantity(), $check_qty )
					) );
				}
			}
			*/

			// If cart_item_key is set, the item is already in the cart
			if ( $cart_item_key ) {
				$new_quantity = $quantity + WC()->cart->cart_contents[ $cart_item_key ]['quantity'];
				WC()->cart->set_quantity( $cart_item_key, $new_quantity, false );
			} else {
				$cart_item_key = $cart_id;

				// Add item after merging with $cart_item_data - hook to allow plugins to modify cart item
				WC()->cart->cart_contents[ $cart_item_key ] = apply_filters( 'woocommerce_add_cart_item', array_merge( $cart_item_data, array(
					'product_id'	=> $product_id,
					'variation_id'	=> $variation_id,
					'variation' 	=> $variation,
					'quantity' 		=> $quantity,
					'data'		=> $product_data
				) ), $cart_item_key );

			}

			if ( did_action( 'wp' ) ) {
				WC()->cart->set_cart_cookies( sizeof( WC()->cart->cart_contents ) > 0 );
			}

			do_action( 'woocommerce_add_to_cart', $cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data );

			return $cart_item_key;

		} catch ( Exception $e ) {
			if ( $e->getMessage() ) {
				wc_add_notice( $e->getMessage(), 'error' );
			}
			return false;
		}
	}


	public function update_order_review() {
		global $ign_opc;

		if ( empty( $_REQUEST['post_data'] ) )
			return;

		$args = wp_parse_args( $_REQUEST['post_data'] );

		if ( empty( $args['opc_checkout_flag'] ) )
			return;

		$ign_opc->switch_user_context_to_customer( 'opc_user' );
		
		check_ajax_referer( 'update-order-review', 'security' );

		$ign_opc->switch_user_context_to_customer( 'customer' );
		
		if ( isset( $args['ignitewoo_one_page_checkout_customer_id'] ) )
			$_POST['ignitewoo_one_page_checkout_customer_id']  = $args['ignitewoo_one_page_checkout_customer_id'];

		$this->maybe_disable_terms();

		/*
		if ( version_compare( WOOCOMMERCE_VERSION, '2.4', '>=' ) ) {
			remove_action( 'wp_ajax_woocommerce_update_order_review', array( 'WC_AJAX', 'update_order_review' ), 10 );
			remove_action( 'wp_ajax_nopriv_update_order_review', array( 'WC_AJAX', 'update_order_review' ), 10 );
		}
		*/

		if ( 0 == sizeof( WC()->cart->get_cart() ) ) {

			//ob_start();
			//wc_add_notice( __( 'You have not selected any items yet', 'manual_phone_orders' ), 'error' );
			//echo '<div class="woocommerce-info">' . __( 'You have not selected any items yet', 'manual_phone_orders' ) . '</div>';
			//throw new Exception( __( 'You have not selected any items yet', 'manual_phone_orders' ) );
			//wc_print_notices();
			$messages = ''; //ob_get_clean();

			
			
			// Get order review fragment
			ob_start();
			woocommerce_order_review();
			$woocommerce_order_review = ob_get_clean();

			// Get checkout payment fragment
			ob_start();
			woocommerce_checkout_payment();
			$woocommerce_checkout_payment = ob_get_clean();


			$data = array(
				'result'    => empty( $messages ) ? 'success' : 'failure',
				'messages'  => $messages,
				'reload'    => isset( WC()->session->reload_checkout ) ? 'true' : 'false',
				'fragments' => apply_filters( 'woocommerce_update_order_review_fragments', array(
					'.woocommerce-checkout-review-order-table' => $woocommerce_order_review,
					'.woocommerce-checkout-payment'            => $woocommerce_checkout_payment
				) )
			);

			// Send JSON and exit
			wp_send_json( $data );

			//$ign_opc->switch_user_context_to_customer( 'opc_user' );

			//die(0);
		}
		
		foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {

			$cik = $cart_item_key;

			$price = isset( $values['_price_altered'] ) ? $values['_price_altered'] : null;

			if ( null !== $price ) {
			
				WC()->cart->cart_contents[ $cik ]['data']->price_altered = $price;
				
				if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '>=' ) ) {
					WC()->cart->cart_contents[ $cik ]['data']->set_price( $price );
					WC()->cart->cart_contents[ $cik ]['data']->set_regular_price( $price );
					WC()->cart->cart_contents[ $cik ]['data']->set_sale_price( '' );

				} else {
					WC()->cart->cart_contents[ $cik ]['data']->price = $price;
					WC()->cart->cart_contents[ $cik ]['data']->regular_price = $price;
					WC()->cart->cart_contents[ $cik ]['data']->sale_price = $price;
				}

				WC()->cart->set_session();
			}
		}
		
		$ign_opc->switch_user_context_to_customer( 'customer' );

		// For versions 2.4 and newer the JS has changed, so use this as taken from class-wc-ajax.php "function update_order_review()" to
		// avoid the "Sorry, your session has expired." message being displayed.

		if ( version_compare( WOOCOMMERCE_VERSION, '2.4', '>=' ) ) {

			if ( ! defined( 'WOOCOMMERCE_CHECKOUT' ) ) {
				define( 'WOOCOMMERCE_CHECKOUT', true );
			}

			do_action( 'woocommerce_checkout_update_order_review', $_POST['post_data'] );

			$chosen_shipping_methods = WC()->session->get( 'chosen_shipping_methods' );

			if ( isset( $_POST['shipping_method'] ) && is_array( $_POST['shipping_method'] ) ) {
				foreach ( $_POST['shipping_method'] as $i => $value ) {
					$chosen_shipping_methods[ $i ] = wc_clean( $value );
				}
			}

			WC()->session->set( 'chosen_shipping_methods', $chosen_shipping_methods );
			WC()->session->set( 'chosen_payment_method', empty( $_POST['payment_method'] ) ? '' : $_POST['payment_method'] );

			if ( isset( $_POST['country'] ) ) {
				WC()->customer->set_country( $_POST['country'] );
			}

			if ( isset( $_POST['state'] ) ) {
				WC()->customer->set_state( $_POST['state'] );
			}

			if ( isset( $_POST['postcode'] ) ) {
				WC()->customer->set_postcode( $_POST['postcode'] );
			}

			if ( isset( $_POST['city'] ) ) {
				WC()->customer->set_city( $_POST['city'] );
			}

			if ( isset( $_POST['address'] ) ) {
				WC()->customer->set_address( $_POST['address'] );
			}

			if ( isset( $_POST['address_2'] ) ) {
				WC()->customer->set_address_2( $_POST['address_2'] );
			}

			if ( wc_ship_to_billing_address_only() ) {

				if ( isset( $_POST['country'] ) ) {
					WC()->customer->set_shipping_country( $_POST['country'] );
				}

				if ( isset( $_POST['state'] ) ) {
					WC()->customer->set_shipping_state( $_POST['state'] );
				}

				if ( isset( $_POST['postcode'] ) ) {
					WC()->customer->set_shipping_postcode( $_POST['postcode'] );
				}

				if ( isset( $_POST['city'] ) ) {
					WC()->customer->set_shipping_city( $_POST['city'] );
				}

				if ( isset( $_POST['address'] ) ) {
					WC()->customer->set_shipping_address( $_POST['address'] );
				}

				if ( isset( $_POST['address_2'] ) ) {
					WC()->customer->set_shipping_address_2( $_POST['address_2'] );
				}
			} else {

				if ( isset( $_POST['s_country'] ) ) {
					WC()->customer->set_shipping_country( $_POST['s_country'] );
				}

				if ( isset( $_POST['s_state'] ) ) {
					WC()->customer->set_shipping_state( $_POST['s_state'] );
				}

				if ( isset( $_POST['s_postcode'] ) ) {
					WC()->customer->set_shipping_postcode( $_POST['s_postcode'] );
				}

				if ( isset( $_POST['s_city'] ) ) {
					WC()->customer->set_shipping_city( $_POST['s_city'] );
				}

				if ( isset( $_POST['s_address'] ) ) {
					WC()->customer->set_shipping_address( $_POST['s_address'] );
				}

				if ( isset( $_POST['s_address_2'] ) ) {
					WC()->customer->set_shipping_address_2( $_POST['s_address_2'] );
				}
			}

			WC()->cart->calculate_totals();
			
			$ign_opc->switch_user_context_to_customer( 'customer' );
			
			// Get order review fragment
			ob_start();
			woocommerce_order_review();
			$woocommerce_order_review = ob_get_clean();
			
			$ign_opc->switch_user_context_to_customer( 'customer' );

			// Get checkout payment fragment
			ob_start();
			woocommerce_checkout_payment();

			$woocommerce_checkout_payment = ob_get_clean();

			//$ign_opc->switch_user_context_to_customer( 'customer' );

			// Get messages if reload checkout is not true
			$messages = '';
			if ( ! isset( WC()->session->reload_checkout ) ) {
				ob_start();
				wc_print_notices();
				$messages = ob_get_clean();
			}
			
			global $user_ID;

			$data = array(
				'user' => $user_ID,
				'result'    => empty( $messages ) ? 'success' : 'failure',
				'messages'  => $messages,
				'reload'    => isset( WC()->session->reload_checkout ) ? 'true' : 'false',
				'fragments' => apply_filters( 'woocommerce_update_order_review_fragments', array(
					'.woocommerce-checkout-review-order-table' => $woocommerce_order_review,
					'.woocommerce-checkout-payment'            => $woocommerce_checkout_payment
				) ),
			);

			wp_send_json( $data );

		}
	}


	function thankyou_page_additions( $order_id = null ) {
		global $user_ID;

		if ( empty( $order_id ) )
			return;

		if ( !$this->has_access() )
			return;

		if ( empty( $this->settings ) )
			$this->settings = get_option( 'woocommerce_ignitewoo_manual_order_settings' );

		$url = admin_url() . '/post.php?post=' . $order_id . '&action=edit';

		if ( isset( $this->settings['manual_order_page'] ) && !empty( $this->settings['manual_order_page'] ) )
			$aurl = get_permalink( $this->settings['manual_order_page'] );
		else
			$aurl = '';

		?>

		<p class="opc_thankyou_page_additions" style="background: #ffffc0 none repeat scroll 0 0;border: 1px solid #ccc;font-size: 1.1em;font-weight: bold;padding: 1em;border-radius:4px">
			<a href="<?php echo $url ?>"><?php _e( 'View this order in the admin area', 'manual_phone_orders' ) ?></a>
			<?php if ( !empty( $aurl ) ) { ?>
			&nbsp; | &nbsp; <a href="<?php echo $aurl ?>"><?php _e( 'Add new manual order', 'manual_phone_orders' ) ?></a>
			<?php } ?>
		</p>

		<?php
		
		// Look at each item in the order to see if it has meta data indicating it's an arbitrary product 
		// and if so delete the product 
		
		$order = wc_get_order( $order_id );
		
		// If the Hold Order gateway was used it puts the order on hold, so don't try to delete any arbitrary products 
		if ( 'on-hold' == $order->get_status() )
			return;
	}


	function checkout_billing() {
		global $user_ID, $ign_opc;

		if ( !$this->opc )
			return;

		remove_action( 'woocommerce_checkout_billing' , array( WC()->checkout, 'checkout_form_billing' ) );

		// Check theme
		$template = locate_template( array( 'phone-orders/form-billing.php' ), false, false );

		if ( '' != $template ) {
			require( $template );
		} else if ( file_exists( dirname( __FILE__ ) . '/phone-orders/form-billing.php' ) ) {
			$checkout = WC()->checkout;
			require( dirname( __FILE__ ) . '/phone-orders/form-billing.php' );
		} else { // fallback to stock
			wc_get_template( 'checkout/form-billing.php', array( 'checkout' =>  WC()->checkout ) );
		}

		echo '<input type="hidden" name="opc_admin_user_id" value="' . $ign_opc->opc_user_id . '">';
	}


	function checkout_shipping() {

		if ( !$this->opc )
			return;

		remove_action( 'woocommerce_checkout_shipping' , array( WC()->checkout, 'checkout_form_shipping' ) );

		// Check theme
		$template = locate_template( array( 'phone-orders/form-shipping.php' ), false, false );

		if ( '' != $template ) {
			require( $template );
		} else if ( file_exists( dirname( __FILE__ ) . '/phone-orders/form-shipping.php' ) ) {
			$checkout = WC()->checkout;
			require( dirname( __FILE__ ) . '/phone-orders/form-shipping.php' );
		} else { // fallback to stock
			wc_get_template( 'checkout/form-shipping.php', array( 'checkout' =>  WC()->checkout ) );
		}

	}


	public function wc_ajax_endpoint() {

		if ( !method_exists( 'WC_AJAX', 'get_endpoint' ) )
			return WC()->ajax_url();

		return WC_AJAX::get_endpoint( "%%endpoint%%" );

	}

	// Borrowed from WC core code. Because it works.
	public function load_scripts_styles() {
		global $post, $wp, $ign_opc; 

		if ( empty( $this->opc ) || !$this->opc )
			return;

		// Ensure that we're in the OPC user context so that nonces are created correctly
		$ign_opc->switch_user_context_to_customer( 'opc_user' );
		
		wp_enqueue_script( 'wc-single-product' );

		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		$lightbox_en = true; // get_option( 'woocommerce_enable_lightbox' ) == 'yes' ? true : false;

		//$ajax_cart_en = true; // get_option( 'woocommerce_enable_ajax_add_to_cart' ) == 'yes' ? true : false;

		$assets_path = str_replace( array( 'http:', 'https:' ), '', WC()->plugin_url() ) . '/assets/';

		$frontend_script_path = $assets_path . 'js/frontend/';

		wp_deregister_script( 'chosen' );

		// Register any scripts for later use, or used as dependencies
		//wp_register_script( 'chosen', $this->plugin_url() . 'scripts/chosen/chosen.jquery' . $suffix . '.js', array( 'jquery' ), '1.0.0', true );

		wp_register_script( 'jquery-blockui', $assets_path . 'js/jquery-blockui/jquery.blockUI' . $suffix . '.js', array( 'jquery' ), '2.60', true );

		wp_register_script( 'jquery-payment', $assets_path . 'js/jquery-payment/jquery.payment' . $suffix . '.js', array( 'jquery' ), '1.0.2', true );

		wp_register_script( 'wc-credit-card-form', $assets_path . 'js/frontend/credit-card-form' . $suffix . '.js', array( 'jquery', 'jquery-payment' ), WC_VERSION, true );

		wp_register_script( 'wc-add-to-cart-variation', $frontend_script_path . 'add-to-cart-variation' . $suffix . '.js', array( 'jquery' ), WC_VERSION, true );

		wp_register_script( 'wc-single-product', $frontend_script_path . 'single-product' . $suffix . '.js', array( 'jquery' ), WC_VERSION, true );

		wp_register_script( 'wc-country-select', $frontend_script_path . 'country-select' . $suffix . '.js', array( 'jquery' ), WC_VERSION, true );

		wp_register_script( 'wc-address-i18n', $frontend_script_path . 'address-i18n' . $suffix . '.js', array( 'jquery' ), WC_VERSION, true );

		wp_register_script( 'jquery-cookie', $assets_path . 'js/jquery-cookie/jquery.cookie' . $suffix . '.js', array( 'jquery' ), '1.3.1', true );

		// Queue frontend scripts conditionally
		//if ( $ajax_cart_en )
		wp_enqueue_script( 'wc-add-to-cart', $frontend_script_path . 'add-to-cart' . $suffix . '.js', array( 'jquery' ), WC_VERSION, true );

		// Not necessary as of WC 3.2 and newer, cart script not needed, and WC enqueues the checkout script on its own
		//if ( version_compare( WOOCOMMERCE_VERSION, '3.2', '<' ) ) { 
			wp_enqueue_script( 'wc-cart', $frontend_script_path . 'cart' . $suffix . '.js', array( 'jquery', 'wc-country-select' ), WC_VERSION, true );
		
			//if ( get_option( 'woocommerce_enable_chosen' ) == 'yes' ) {
				//wp_enqueue_script( 'wc-chosen', $frontend_script_path . 'chosen-frontend' . $suffix . '.js', array( 'chosen' ), WC_VERSION, true );
				//wp_enqueue_style( 'woocommerce_chosen_styles', $assets_path . 'css/chosen.css' );
			//}

		//}
		
		wp_enqueue_script( 'wc-checkout', $frontend_script_path . 'checkout' . $suffix . '.js', array( 'jquery', 'woocommerce', 'wc-country-select', 'wc-address-i18n' ), WC_VERSION, true );

		if ( is_page( get_option( 'woocommerce_myaccount_page_id' ) ) ) {
			if ( get_option( 'woocommerce_enable_chosen' ) == 'yes' ) {
				wp_enqueue_script( 'wc-chosen', $frontend_script_path . 'chosen-frontend' . $suffix . '.js', array( 'chosen' ), WC_VERSION, true );
				wp_enqueue_style( 'woocommerce_chosen_styles', $assets_path . 'css/chosen.css' );
			}
		}

		if ( is_add_payment_method_page() )
			wp_enqueue_script( 'wc-add-payment-method', $frontend_script_path . 'add-payment-method' . $suffix . '.js', array( 'jquery', 'woocommerce' ), WC_VERSION, true );


		if ( $lightbox_en && ( is_product() || ( ! empty( $post->post_content ) && strstr( $post->post_content, '[' . $this->shortcode ) ) ) ) {

			wp_enqueue_script( 'prettyPhoto', $assets_path . 'js/prettyPhoto/jquery.prettyPhoto' . $suffix . '.js', array( 'jquery' ), '3.1.5', true );

			wp_enqueue_script( 'prettyPhoto-init', $assets_path . 'js/prettyPhoto/jquery.prettyPhoto.init' . $suffix . '.js', array( 'jquery','prettyPhoto' ), WC_VERSION, true );

			wp_enqueue_style( 'woocommerce_prettyPhoto_css', $assets_path . 'css/prettyPhoto.css' );

		}

		//if ( strstr( $post->post_content, '[' . $this->shortcode )  )
		//	wp_enqueue_script( 'wc-single-product' );

		// Global frontend scripts
		// wp_enqueue_script( 'woocommerce', $frontend_script_path . 'woocommerce' . $suffix . '.js', array( 'jquery', 'jquery-blockui' ), WC_VERSION, true );

		wp_enqueue_script( 'wc-cart-fragments', $frontend_script_path . 'cart-fragments' . $suffix . '.js', array( 'jquery', 'jquery-cookie' ), WC_VERSION, true );

		// Variables for JS scripts
		wp_localize_script( 'woocommerce', 'woocommerce_params', apply_filters( 'woocommerce_params', array(
			'ajax_url'        => WC()->ajax_url(),
			'wc_ajax_url'     => WC()->ajax_url(),
			'ajax_loader_url' => $this->plugin_url() . '/assets/ajax-loader@2x.gif'
		) ) );

		wp_localize_script( 'wc-single-product', 'wc_single_product_params', apply_filters( 'wc_single_product_params', array(
			'i18n_required_rating_text' => esc_attr__( 'Please select a rating', 'manual_phone_orders' ),
			'review_rating_required'    => get_option( 'woocommerce_review_rating_required' ),
		) ) );

		wp_localize_script( 'wc-checkout', 'wc_checkout_params', apply_filters( 'wc_checkout_params', array(
			'ajax_url'                  => WC()->ajax_url(),
			'wc_ajax_url'      	    => $this->wc_ajax_endpoint(),
			'ajax_loader_url'           => $this->plugin_url() . '/assets/ajax-loader@2x.gif',
			'update_order_review_nonce' => wp_create_nonce( "update-order-review" ),
			'apply_coupon_nonce'        => wp_create_nonce( "apply-coupon" ),
			'remove_coupon_nonce'       => wp_create_nonce( 'remove-coupon' ),
			'option_guest_checkout'     => get_option( 'woocommerce_enable_guest_checkout' ),
			'checkout_url'              => add_query_arg( 'action', 'woocommerce_checkout', WC()->ajax_url() ),
			'is_checkout'               => is_page( wc_get_page_id( 'checkout' ) ) && empty( $wp->query_vars['order-pay'] ) && ! isset( $wp->query_vars['order-received'] ) ? 1 : 0
		) ) );

		wp_localize_script( 'wc-address-i18n', 'wc_address_i18n_params', apply_filters( 'wc_address_i18n_params', array(
			'locale'                    => json_encode( WC()->countries->get_country_locale() ),
			'locale_fields'             => json_encode( WC()->countries->get_country_locale_field_selectors() ),
			'i18n_required_text'        => esc_attr__( 'required', 'manual_phone_orders' ),
		) ) );

		wp_localize_script( 'wc-cart', 'wc_cart_params', apply_filters( 'wc_cart_params', array(
			'ajax_url'                     => WC()->ajax_url(),
			'wc_ajax_url'                  => WC()->ajax_url(),
			'ajax_loader_url'              => $this->plugin_url() . '/assets/ajax-loader@2x.gif',
			'update_shipping_method_nonce' => wp_create_nonce( "update-shipping-method" ),
		) ) );

		wp_localize_script( 'wc-cart-fragments', 'wc_cart_fragments_params', apply_filters( 'wc_cart_fragments_params', array(
			'ajax_url'      => WC()->ajax_url(),
			'wc_ajax_url'   => $this->wc_ajax_endpoint(),
			'fragment_name' => apply_filters( 'woocommerce_cart_fragment_name', 'wc_fragments' )
		) ) );

		wp_localize_script( 'wc-add-to-cart', 'wc_add_to_cart_params', apply_filters( 'wc_add_to_cart_params', array(
			'ajax_url'                => WC()->ajax_url(),
			'wc_ajax_url'             => WC()->ajax_url(),
			'ajax_loader_url'         => $this->plugin_url() . '/assets/ajax-loader@2x.gif',
			'i18n_view_cart'          => esc_attr__( 'View Cart', 'manual_phone_orders' ),
			'cart_url'                => get_permalink( wc_get_page_id( 'cart' ) ),
			'is_cart'                 => is_cart(),
			'cart_redirect_after_add' => get_option( 'woocommerce_cart_redirect_after_add' )
		) ) );

		wp_localize_script( 'wc-add-to-cart-variation', 'wc_add_to_cart_variation_params', apply_filters( 'wc_add_to_cart_variation_params', array(
			'i18n_no_matching_variations_text' => esc_attr__( 'Sorry, no products matched your selection. Please choose a different combination.', 'manual_phone_orders' ),
			'i18n_unavailable_text'            => esc_attr__( 'Sorry, this product is unavailable. Please choose a different combination.', 'manual_phone_orders' ),
		) ) );

		if ( version_compare( WOOCOMMERCE_VERSION, '3.2', '>=' ) ) {
			// Remove select2 in case a theme or other plugin added it
			wp_dequeue_style( 'select2' );
			wp_deregister_style( 'select2' );
			wp_deregister_script( 'select2' );
			wp_dequeue_script( 'select2' );

			wp_enqueue_style( 'selectWoo', WC()->plugin_url() . '/assets/css/select2.css' );
			wp_enqueue_script( 'selectWoo', WC()->plugin_url() . '/assets/js/selectWoo/selectWoo.full.min.js', array( 'jquery' ), WC_VERSION, true );
			
		} else if ( version_compare( WOOCOMMERCE_VERSION, '2.4', '>=' ) ) {
			// Remove select2 in case a theme or other plugin added it
			wp_dequeue_style( 'select2' );
			wp_deregister_style( 'select2' );
			wp_deregister_script( 'select2' );
			wp_dequeue_script( 'select2' );

			wp_enqueue_style( 'select2', WC()->plugin_url() . '/assets/css/select2.css' );
			wp_enqueue_script( 'select2', WC()->plugin_url() . '/assets/js/frontend/select2/select2.min.js', array( 'jquery' ), WC_VERSION, true );
			wp_enqueue_script( 'wc-country-select', WC()->plugin_url() . '/assets/js/frontend/country-select.js',  array( 'jquery' ), WC_VERSION, true );
		}

		wp_localize_script( 'wc-country-select', 'wc_country_select_params', apply_filters( 'wc_country_select_params', array(
			'countries'              => json_encode( array_merge( WC()->countries->get_allowed_country_states(), WC()->countries->get_shipping_country_states() ) ),
			'i18n_select_state_text' => esc_attr__( 'Select an option&hellip;', 'manual_phone_orders' ),
		) ) );

		// CSS Styles
		$enqueue_styles = WC_Frontend_Scripts::get_styles();

		if ( $enqueue_styles )
			foreach ( $enqueue_styles as $handle => $args )
				wp_enqueue_style( $handle, $args['src'], $args['deps'], $args['version'], $args['media'] );

		$js = "ign_one_page_checkout = {
			search_products_nonce: '". wp_create_nonce('search-products') . "',
			search_customers_nonce: '". wp_create_nonce('search-customers') . "',
			add_products_nonce: '" . wp_create_nonce('ign-add-products') . "',
			add_fees_nonce: '" . wp_create_nonce('ign-add-fees') . "',
			}";

		wc_enqueue_js( $js );

	}

	public function json_search_customers() {

		if ( !$this->has_access() )
			die( '-1' );
			
		ob_start();

		check_ajax_referer( 'search-customers', 'security' );

		$term    = wc_clean( wp_unslash( $_GET['term'] ) );
		$exclude = array();
		$limit   = '';

		if ( empty( $term ) ) {
			die();
		}

		if ( ! empty( $_GET['exclude'] ) ) {
			$exclude = array_map( 'intval', explode( ',', $_GET['exclude'] ) );
		}

		$found_customers = array();

		$term    = wc_clean( wp_unslash( $_GET['term'] ) );
		$exclude = array();
		$limit   = '';

		if ( empty( $term ) ) {
			wp_die();
		}

		// Usernames can be numeric so we first check that no users was found by ID before searching for numeric username, this prevents performance issues with ID lookups.
		// WC 3.0 and newer
		if ( class_exists( 'WC_Data_Store' ) ) { 
			
			$ids = array();
			
			// Search by ID.
			if ( is_numeric( $term ) ) {
				$customer = new WC_Customer( intval( $term ) );

				// Customer does not exists.
				if ( 0 !== $customer->get_id() ) {
					$ids = array( $customer->get_id() );
				}
			}
			
			if ( empty( $ids ) ) {
				$data_store = WC_Data_Store::load( 'customer' );

				// If search is smaller than 3 characters, limit result set to avoid
				// too many rows being returned.
				if ( 3 > strlen( $term ) ) {
					$limit = 20;
				}
				$ids = $data_store->search_customers( $term, $limit );
			}
			
		// WC 2.6 and older
		} else { 
		
			add_action( 'pre_user_query', array( __CLASS__, 'json_search_customer_name' ) );

			$customers_query = new WP_User_Query( apply_filters( 'woocommerce_json_search_customers_query', array(
				'fields'         => 'all',
				'orderby'        => 'display_name',
				'search'         => '*' . $term . '*',
				'search_columns' => array( 'ID', 'user_login', 'user_email', 'user_nicename' )
			) ) );

			remove_action( 'pre_user_query', array( __CLASS__, 'json_search_customer_name' ) );

			$customers = $customers_query->get_results();

			if ( ! empty( $customers ) ) {
				foreach ( $customers as $customer ) {
					if ( ! in_array( $customer->ID, $exclude ) ) {
						$found_customers[ $customer->ID ] = $customer->display_name . ' (#' . $customer->ID . ' &ndash; ' . sanitize_email( $customer->user_email ) . ')';
					}
				}
			}
		}

		if ( ! empty( $_GET['exclude'] ) ) {
			$ids = array_diff( $ids, (array) $_GET['exclude'] );
		}

		foreach ( $ids as $id ) {
			$customer = new WC_Customer( $id );
			/* translators: 1: user display name 2: user ID 3: user email */
			$found_customers[ $id ] = sprintf(
				esc_html__( '%1$s (#%2$s &ndash; %3$s)', 'manual_phone_orders' ),
				$customer->get_first_name() . ' ' . $customer->get_last_name(),
				$customer->get_id(),
				$customer->get_email()
			);
		}

		wp_send_json( apply_filters( 'woocommerce_json_search_found_customers', $found_customers ) );
	}
	
	// Backward compat for WC version before2.6
	/*
	function filter_user_search( $args = array(), $term = false, $limit = false, $type = false  ) {

		if ( !$this->has_access() )
			return $args;
		
		
		// don't filter the query unless this plugin is running the ajax query
		if ( !isset( $_GET['opc'] ) || $_GET['opc'] != 'true' )
			return $args;
		
		// admins are limited, everyone else is
		if ( current_user_can( 'administrator' ) )
			return $args;

		// no one except admins can query for admin accounts
		$admins_query = new WP_User_Query( array(
			'fields'         	=> 'ID',
			'orderby'        	=> 'ID',
			'role' 		=> 'administrator',
		) );

		// If the current user is not a shop manager then exclude shop manager accounts too
		if ( !current_user_can( 'shop_manager' ) && !current_user_can( 'administrator' ) ) {
			$shop_query = new WP_User_Query( array(
				'fields'         => 'ID',
				'orderby'        => 'ID',
				'role' => 'shop_manager',
			) );
		} else  {

			$shop_query = new stdClass();
			$shop_query->results = array();
		}

		$res = array_unique( array_merge( $admins_query->results, $shop_query->results ) );

		if ( empty( $res ) || !is_array( $res ) )
			$res = array();

		$args['exclude'] = $res;

			
		return $args;

	}
	*/

	public function get_chosen_product_input( $die = false, $return_parms = false ) {

		/* Older way where Select2 remained populated
		<input type="hidden" class="opc-product-search" data-multiple="true" style="width: 100%;" name="product_ids[]" data-placeholder="<?php esc_attr_e( 'Choose products &hellip;', 'manual_phone_orders' ); ?>" data-action="woocommerce_json_search_products_and_variations" data-selected="<?php
				//$product_ids = array_filter( array_map( 'absint', explode( ',', get_post_meta( $post->ID, 'product_ids', true ) ) ) );
				$product_ids = $items;
				$json_ids    = array();

				foreach ( $product_ids as $product_id ) {
					$product = wc_get_product( $product_id );
					if ( is_object( $product ) ) {
						$json_ids[ $product_id ] = wp_kses_post( $product->get_formatted_name() );
					}
				}

				echo esc_attr( json_encode( $json_ids ) );
			?>" value="<?php echo implode( ',', array_keys( $json_ids ) ); ?>" />
		*/
		if ( version_compare( WOOCOMMERCE_VERSION, '2.7', '<' ) ) {

			$cart_items = WC()->cart->get_cart();

			$items = array();

			foreach( $cart_items as $key => $item ) {

				if ( $item['data']->is_type( 'variation' ) ) {

					$items[] = $item['variation_id'];

				} else {

					$items[] = $item['product_id'];
				}
			}

			//if ( !empty( $_POST['product_ids'] ) )
			//	$items = array_merge( $items, $_POST['product_ids'] );

			$items = array_unique( $items, SORT_REGULAR );

			?>
			<input type="hidden" class="opc-product-search" data-multiple="false" style="width: 100%;" name="product_ids[]" data-placeholder="<?php esc_attr_e( 'Choose products &hellip;', 'manual_phone_orders' ); ?>" data-action="woocommerce_json_search_products_and_variations" data-selected="<?php
					//$product_ids = array_filter( array_map( 'absint', explode( ',', get_post_meta( $post->ID, 'product_ids', true ) ) ) );
					$product_ids = $items;
					$json_ids    = array();

					foreach ( $product_ids as $product_id ) {
						$product = wc_get_product( $product_id );
						if ( is_object( $product ) ) {
							$json_ids[ $product_id ] = wp_kses_post( $product->get_formatted_name() );
						}
					}

					echo esc_attr( json_encode( $json_ids ) );
				?>" value="" />

		<?php } else { ?>

			<p>
			<select type="text" name="product_ids[]" class="opc-product-search" data-placeholder="<?php _e( 'Choose products &hellip;', 'manual_phone_orders' ) ?>" style="width: 58%;" multiple="multiple">
			</select>
			
			<?php 
			$args = array(
				'show_option_all'    => '',
				'show_option_none'   => __( 'Narrow search by category', 'manual_phone_orders' ),
				'option_none_value'  => '-1',
				'orderby'            => 'ID',
				'order'              => 'ASC',
				'show_count'         => 0,
				'hide_empty'         => 1,
				'child_of'           => 0,
				'exclude'            => '',
				'include'            => '',
				'echo'               => 1,
				'selected'           => 0,
				'hierarchical'       => 0,
				'name'               => 'cat',
				'id'                 => '',
				'class'              => 'opc-search-cats',
				'depth'              => 0,
				'tab_index'          => 0,
				'taxonomy'           => 'product_cat',
				'hide_if_empty'      => false,
				'value_field'	     => 'term_id',
				);
			wp_dropdown_categories( $args );
			?>
			</p>
		<?php } ?>

		<?php

		if ( $return_parms )
			return $ids;

		if ( $die )
			die;

	}

	function user_can_change_price() {
		global $user_ID, $ign_opc;

		$can_change_price = true;

		$ign_opc->switch_user_context_to_customer( 'opc_user' );
		
		if ( current_user_can( 'administrator' ) || current_user_can( 'shop_manager' ) )
			return true;

		if ( empty( $this->settings ) )
			$this->settings = get_option( 'woocommerce_ignitewoo_manual_order_settings' );

		if ( !empty( $this->settings['change_price_users'] ) ) {
			if ( !in_array( $user_ID,  $this->settings['change_price_users'] ) )
				$can_change_price = false;
		}

		return $can_change_price;
	}

	function add_clerk_info( $order_id, $posted = null ) {
		global $current_user, $ign_opc; 
		
		if ( empty( $_REQUEST['opc_checkout_flag'] ) )
			return;

		/*
		if ( empty( $current_user ) && function_exists( 'wp_get_current_user' ) )
			$current_user = wp_get_current_user();
		else if ( empty( $current_user ) )
			$current_user = get_currentuserinfo();
		*/
		
		$ign_opc->switch_user_context_to_customer( 'opc_user' );
		
		update_post_meta( $order_id, '_ign_one_page_clerk', $current_user->ID );
		
	}

	function clerk_info_box() {
		global $post;
		$clerk = get_post_meta( $post->ID, '_ign_one_page_clerk', true );

		if ( false == $clerk )
			return;

		add_meta_box(
			'ign-one-page-clerk',
			__( 'Manual / Phone Order Clerk','manual_phone_orders' ),
			array(&$this, 'clerk_info_box_data' ),
			'shop_order',
			'side',
			'default'
		);

	}

	function clerk_info_box_data() {
		global $post;

		$clerk = get_post_meta( $post->ID, '_ign_one_page_clerk', true );

		$data = get_user_by( 'id', $clerk );

		if ( empty( $data ) || is_wp_error( $data ) ) {
			_e( 'User not found', 'manual_phone_orders' );
			return;
		}

		echo '<p>' . __( 'This order was placed manually by:', 'manual_phone_orders' ) . '</p>';

		echo '<p><a href="' . admin_url( 'user-edit.php?user_id=' . $data->ID ) . '" target="_blank">#' . $data->ID . '</a> &ndash; ' . $data->user_login . '</p>';

		echo '<p>' . get_user_meta( $data->ID, 'first_name', true ) . ' ' . get_user_meta( $data->ID, 'last_name', true ) . '</p>';
	}
	
	function maybe_delete_arbitrary_product( $order_id ) { 
		
		$order = wc_get_order( $order_id );

		if ( empty( $order ) )
			return;
			
		foreach( $order->get_items() as $item_id => $item ) { 
		
			$product = $order->get_product_from_item( $item );
			
			if ( empty( $product ) )
				continue;
			
			if ( method_exists( $product, 'get_id' ) )
				$pid = $product->get_id();
			else 
				$pid = $product->id; 
				
			if ( empty( $pid ) ) 
				continue;
			
			if ( method_exists( $product, 'get_meta' ) )
				$is_arb = $product->get_meta( '_ign_opc_arbitrary_product', true );
			else 
				$is_arb = get_post_meta( $pid, '_ign_opc_arbitrary_product', true );
			
			if ( empty( $pid ) )
				continue; 
			
			//wp_delete_post( $pid, true );	
		}
	}

	// CRON JOB - remove arbitrary products
	function maybe_schedule_cron_hook() { 
		if ( !wp_next_scheduled ( 'remove_arbitrary_products_hook' ) ) {
			wp_schedule_event(time(), 'hourly', 'remove_arbitrary_products_hook');
		}
	}
	
	// CRON FUNCTION - remove arbitrary products
	function remove_arbitrary_products() { 
		global $post; 
		
		$args = array( 
			'post_type' => 'product',
			'posts_per_page' => -1,
			'meta_query' => array( 
				relation => 'AND',
				array( 
					'key' => '_ign_opc_arbitrary_product',
					'value' => '',
					'compare' => '!=' 
				),
				array( 
					'key' => '_ign_opc_arbitrary_product',
					'value' => current_time( 'timestamp', true ) - ( HOUR_IN_SECONDS ), // 1 hour old or more
					'compare' => '<' 
				)
			)
		);
		
		$res = new WP_Query( $args );
		
		if ( $res->have_posts() ) foreach( $res->posts as $p ) {
			//wp_delete_post( $p->ID, true );
		}

	}
	
}
global $ign_opc_core;
$ign_opc_core = new IGN_Manual_Phone_Orders_Core();


