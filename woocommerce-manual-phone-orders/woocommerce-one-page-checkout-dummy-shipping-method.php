<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Copyright (c) 2014, 2015 - IgniteWoo.com - All Rights Reserved 
 */
class WC_Shipping_OPC_Dummy_Gateway extends WC_Shipping_Method {

	/**
	 * Constructor
	 */
	public function __construct( $instance_id = 0 ) {

		$this->instance_id = absint( $instance_id );
	
		$this->id                 = 'phone_manual_dummy_shipping';
		$this->method_title       = __( 'Phone & Manual Orders', 'manual_phone_orders' );
		$this->method_description = __( 'Custom shipping module for Phone Orders & Manual Orders, lets you set the shipping cost for the order', 'manual_phone_orders' );
		
		if ( $this->instance_id > 0 || ( version_compare( WC_VERSION, '2.6', '>' ) && is_admin() && !defined( 'DOING_AJAX' ) ) ) { 
			$this->supports = array(
				'settings',
				//'shipping-zones',
				//'instance-settings',
				//'instance-settings-modal',
			);
		}
		
		$this->init();
	}
	
	public function is_available( $package ) {

		// WC 3.x 
		if ( isset( $_POST['shipping_method'][0] ) && 'phone_manual_dummy_shipping' == $_POST['shipping_method'][0] )
			return true;
		
		if ( !$this->get_option( 'enabled' ) || 'yes' !== $this->get_option( 'enabled' ) )
			return false;
			
		if ( !empty( $_POST['opc_checkout_flag'] ) ) { 
			return true;
		} else if ( empty( $_POST['post_data'] ) ) {
			return false;
		}

		if ( isset( $_POST['post_data'] ) ) { 
		
			$args = wp_parse_args( $_POST['post_data'] );

			if ( !empty( $args['opc_checkout_flag'] ) )
				return true;
		}
		
		return false;
	}

	/**
	 * init function.
	 */
	public function init() {

		if ( $this->instance_id > 0 ) { 
			$this->instance_form_fields = $this->init_form_fields();
			$this->init_form_fields();
			$this->init_settings();
		} else { 
			$this->init_form_fields();
			$this->init_settings();
		}
		
		$this->title = $this->get_option('title');
		
		if ( empty( $this->title ) )
			$this->title = __( 'Custom Shipping Cost', 'manual_phone_orders' );

		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
	}


	/**
	 * calculate_shipping function.
	 *
	 * @param array $package (default: array())
	 */
	public function calculate_shipping( $package = array() ) {

		$cost = WC()->session->get( 'opc_custom_shipping_cost' );

		// No cost set in the session? Check if cost is being submitted
		if ( empty( $cost ) || isset( $_POST['post_data'] ) ) { 
		
			if ( isset( $_POST['post_data'] ) ) { 
				
				$args = wp_parse_args( $_POST['post_data'] );

				if ( empty( $args ) || empty( $args['opc_checkout_flag'] ) )
					return;
					
			} else { 
				// Use zero so that the shipping option shows up on the page
				$cost = 0;
			}
			
		// Cost is in the session, get than and use it
		} else { 
		
			$cost = round( floatval( WC()->session->get( 'opc_custom_shipping_cost' ) ), 2 );
		}
		
		if ( empty( $cost ) ) {
			$cost = 0;
			$label = $this->title . ' (' . __( 'Free!', 'manual_phone_orders' ) . ')';
		} else {
			$label = $this->title;
		}
		
		$rate = array(
			'id'    => $this->id,
			'label' => $label,
			'cost'  => $cost
		);

		$this->add_rate( $rate );
	}

	/**
	 * Init form fields.
	 */
	public function init_form_fields() {
		$this->form_fields = array(
			'enabled' => array(
				'title'   => __( 'Enable', 'manual_phone_orders' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable this shipping method for phone & manual orders', 'manual_phone_orders' ),
				'default' => 'no'
			),
			'title' => array(
				'title'       => __( 'Title', 'manual_phone_orders' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'manual_phone_orders' ),
				'default'     => __( 'Custom Shipping Cost', 'manual_phone_orders' ),
				'desc_tip'    => true,
			),
		);
	}
}
