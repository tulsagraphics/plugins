<?php
/**

Copyright (c) 2014 - IgniteWoo.com - All Rights Reserved 

*/

// Admin Configuration Stuff - this create a new menu item on the page WooCommerce -> Settings -> Integrations

if ( !class_exists( 'WC_Integration' ) )
	return;

class IgniteWoo_Manual_Order_Settings extends WC_Integration {

	function __construct() {

		$this->id = 'ignitewoo_manual_order';

		$this->method_title = __( 'Manual Order & Phone Order', 'manual_phone_orders' );

		$this->method_description = __( 'Adjust the settings before using this plugin. See the <a href="http://ignitewoo.com/ignitewoo-software-documentation/" target="_blank">documentation</a> for help', 'manual_phone_orders' );

		$this->init_form_fields();

		$this->init_settings();
		
		if ( empty( $this->settings['enable_stock'] ) ) 
			$this->settings['enable_stock'] = 'no';

		add_action( 'woocommerce_update_options_integration_' . $this->id , array( &$this, 'process_admin_options'), 999 );

		add_action( 'admin_enqueue_scripts', array( &$this, 'enqueue_scripts' ), 10 );
		
		add_action( 'admin_print_scripts', array( &$this, 'admin_print_scripts' ), 999 );
						
	}
	
	
	function enqueue_scripts() { 

		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		
		wp_register_script( 'iajax-chosen', IGN_Manual_Phone_Orders_Core::plugin_url() . '/scripts/ajax-chosen/lib/ajax-chosen' . $suffix . '.js', array('jquery', 'chosen'), WC_VERSION );
		
		wp_register_script( 'ichosen', IGN_Manual_Phone_Orders_Core::plugin_url() . '/scripts/chosen/chosen.jquery' . $suffix . '.js', array('jquery'), WC_VERSION );

		wp_enqueue_script( 'ichosen' );
		
		wp_enqueue_script( 'iajax-chosen' );

		wp_register_style( 'ichosen-style', IGN_Manual_Phone_Orders_Core::plugin_url() . '/scripts/chosen/chosen.css', WC_VERSION );
		
		wp_enqueue_style( 'ichosen-style' );

	}
	
	
	function admin_print_scripts() { 
	
		if ( !isset( $_GET['page'] ) || 'wc-settings' !== $_GET['page'] )
			return;

		$js = "
		jQuery( 'select.select_drop_users' ).ajaxChosen({
				method:         'GET',
				url:            '" . admin_url( 'admin-ajax.php' ) . "',
				dataType:       'json',
				afterTypeDelay: 100,
				minTermLength:  2,
				data:           {
					action:   'woocommerce_json_search_customers',
					security: '" . wp_create_nonce( 'search-customers' ) . "'
				}
			}, function ( data ) {

				var terms = {};

				$.each( data, function ( i, val ) {
					terms[i] = val;
				});

				return terms;
			});	
		";
		
		wc_enqueue_js( $js );
		
		?>
		<script>
		
		jQuery( document ).ready( function() { 
			jQuery( '.select_drop, .select_drop_roles' ).chosen();
		})
		
		</script>

		<?php 
	}

	
	function init_form_fields() {
	
		$this->form_fields = array(
				'enable' => array(
					'title' 	=> __( 'Enable', 'manual_phone_orders'),
					'type' 		=> 'checkbox',
					'default' 	=> '',
					'values'	=> 'yes',
					'desc'		=> '',
					'label'		=> __( 'Enable', 'manual_phone_orders'),
				),
				/*
				'disable_terms' => array(
					'title' 	=> __( 'Terms and Conditions', 'manual_phone_orders'),
					'type' 		=> 'checkbox',
					'default' 	=> 'no',
					'label'		=> __( 'Disable', 'manual_phone_orders' ),
					'desc_tip'	=> true,
					'description'	=> __( 'Disable the terms and conditions checkbox on the manual/phone order page if this feature is enabled in your regular store', 'manual_phone_orders'),
				),
				*/
		);

	}
	
	
	function generate_settings_html( $form_fields = array(), $echo = true ) { 
	
		?>
		<style>
		.chosen-container {
			width: 350px !important;
		}
		.chosen-container-multi .chosen-choices li.search-field input { 
			min-height: 22px
		}
		.chosen-container-multi .chosen-choices li.search-choice {
			background-clip: padding-box;
			background-color: #f4f4f4;
			background-image: none;
			border: 1px solid #aaa;
			border-radius: 3px;
			box-shadow: 0 0 2px #fff inset, 0 1px 0 rgba(0, 0, 0, 0.05);
			color: #333;
			cursor: default;
			line-height: 14px;
			margin: 5px 7px 3px;
			padding: 5px 20px 5px 5px;
			position: relative;
		}
		.chosen-container-single .chosen-single {
			background: #fff;
			border-radius: 2px;
			box-shadow: none;
			height: 26px;
		}
		.chosen-container-multi .chosen-choices {
			background-image: none;
		}
		.form-table th {
			width: 250px;
		}
		</style>
		<table class="form-table">
		<?php
		
		parent::generate_settings_html();
		
		$this->generate_pages_html();

			
		?>
		<tr valign="top">
			<th class="titledesc" scope="row">
				<label for="woocommerce_ignitewoo_manual_order_disable_terms"><?php _e( 'Terms and Conditions', 'manual_phone_orders' ) ?></label>
				<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.6', '>=' ) ) { ?>
				<span class="woocommerce-help-tip" data-tip="<?php _e( 'Disable the terms and conditions checkbox on the manual/phone order page if this feature is enabled in your regular store', 'manual_phone_orders' ) ?>">
				<?php } else { ?>
				<img height="16" width="16" src="<?php echo WC()->plugin_url() ?>/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Disable the terms and conditions checkbox on the manual/phone order page if this feature is enabled in your regular store', 'manual_phone_orders' ) ?>">
				<?php } ?>
			</th>
			<td class="forminp">
				<fieldset>
					<legend class="screen-reader-text"><span><?php _e( 'Terms and Conditions', 'manual_phone_orders' ) ?></span></legend>
					<label for="woocommerce_ignitewoo_manual_order_disable_terms">
					<input type="checkbox" value="yes" style="" id="woocommerce_ignitewoo_manual_order_disable_terms" name="disable_terms" class="" <?php checked( isset($this->settings['disable_terms']) ? $this->settings['disable_terms'] : null, 'yes', true ) ?>> <?php _e( 'Disable', 'manual_phone_orders' ) ?></label>
					<br>
				</fieldset>
			</td>
		</tr>
		
		<tr valign="top">
			<th class="titledesc" scope="row">
				<label for="woocommerce_ignitewoo_manual_order_disable_terms"><?php _e( 'Show stock level with title', 'manual_phone_orders' ) ?></label>
				<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.6', '>=' ) ) { ?>
				<span class="woocommerce-help-tip" data-tip="<?php _e( 'Show the product stock level with the product title in the product search results. Applicable only when Stock Management is turned on in WooCommerce', 'manual_phone_orders' ) ?>">
				<?php } else { ?>
				<img height="16" width="16" src="<?php echo WC()->plugin_url() ?>/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Show the product stock level with the product title in the product search results. Applicable only when Stock Management is turned on in WooCommerce', 'manual_phone_orders' ) ?>">
				<?php } ?>
			</th>
			<td class="forminp">
				<fieldset>
					<legend class="screen-reader-text"><span><?php _e( 'Enable', 'manual_phone_orders' ) ?></span></legend>
					<label for="woocommerce_ignitewoo_manual_order_disable_terms">
					<input type="checkbox" value="yes" style="" id="woocommerce_ignitewoo_manual_order_enable_stock" name="enable_stock" class="" <?php if ( isset( $this->settings['enable_stock'] ) ) checked( $this->settings['enable_stock'], 'yes', true ) ?>> <?php _e( 'Enable', 'manual_phone_orders' ) ?></label>
					<br>
				</fieldset>
			</td>
		</tr>
		
		<tr valign="top">
			<th class="titledesc" scope="row">
				<label for="woocommerce_ignitewoo_manual_order_disable_autorefresh"><?php _e( 'Disable automatic cart totals update', 'manual_phone_orders' ) ?></label>
				<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.6', '>=' ) ) { ?>
				<span class="woocommerce-help-tip" data-tip="<?php _e( 'Normally, <strong>when you change the price or quantity</strong> of items in the order the cart totals refresh immediately. If you enable this option then the totals will only refresh to reflect your new price and quantity settings when you click the Update Cart Total button', 'manual_phone_orders' ) ?>">
				<?php } else { ?>
				<img height="16" width="16" src="<?php echo WC()->plugin_url() ?>/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Show the product stock level with the product title in the product search results. Applicable only when Stock Management is turned on in WooCommerce', 'manual_phone_orders' ) ?>">
				<?php } ?>
			</th>
			<td class="forminp">
				<fieldset>
					<legend class="screen-reader-text"><span><?php _e( 'Enable', 'manual_phone_orders' ) ?></span></legend>
					<label for="woocommerce_ignitewoo_manual_order_disable_autorefresh">
					<input type="checkbox" value="yes" style="" id="woocommerce_ignitewoo_manual_order_disable_autorefresh" name="disable_autorefresh" class="" <?php if ( isset( $this->settings['disable_autorefresh'] ) ) checked( $this->settings['disable_autorefresh'], 'yes', true ) ?>> <?php _e( 'Enable', 'manual_phone_orders' ) ?></label>
					<br>
				</fieldset>
			</td>
		</tr>
		<?php 
			if ( !empty( $this->settings['default_country'] ) )
				$opts = $this->settings['default_country'];
			else 
				$opts = array();
				
			if ( !empty( $opts ) ) { 
				$opts = explode( ':', $opts );
				$country = $opts[0];
				$state = !empty( $opts[1] ) ? $opts[1] : '';
			} else { 
				$country = '';
				$state = '';
			}
					
			if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) )
				$class = 'wc-enhanced-select';
			else 
				$class = 'select_drop';

		?>
		<tr valign="top">
			<th class="titledesc" scope="row">
				<label for="woocommerce_ignitewoo_manual_order_disable_autorefresh"><?php _e( 'Default location', 'manual_phone_orders' ) ?></label>
				<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.6', '>=' ) ) { ?>
					<span class="woocommerce-help-tip" data-tip="<?php _e( 'The default location for the billing country/state', 'manual_phone_orders' ) ?>">
				<?php } else { ?>
					<img height="16" width="16" src="<?php echo WC()->plugin_url() ?>/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'The default location for the billing country/state', 'manual_phone_orders' ) ?>">
				<?php } ?>
			</th>
			<td class="forminp">
				<fieldset>
					<label for="woocommerce_ignitewoo_manual_order_disable_autorefresh">
					<select name="default_country" style="width:400px" data-placeholder="<?php esc_attr_e( 'Choose a country&hellip;', 'manual_phone_orders' ); ?>" aria-label="<?php esc_attr_e( 'Country', 'manual_phone_orders' ) ?>" class="<?php echo $class ?>">
						<?php WC()->countries->country_dropdown_options( $country, $state ); ?>
					</select> 
                                                </label>
					<br>
				</fieldset>
			</td>
		</tr>
		                                              

		<?php
		
		$this->generate_perms_html();
		
		$this->generate_roles_html();
		
		$this->generate_price_change_html();
	}

	function generate_pages_html() { 
		global $woocommerce, $wpdb;

		$sql = "select ID, post_title from " . $wpdb->posts . ' where post_type = "page" and post_status ="publish" order by post_title ASC';
		
		$pages = $wpdb->get_results( $sql );
	
		if ( !empty( $pages ) ) { 
		
			foreach( $pages as $p ) 
				$the_pages[ $p->ID ] = $p->post_title;
		
		} else { 
		
			$the_pages = array();
			
		}
		
		if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '>=' ) )
			$class = 'wc-enhanced-select';
		else 
			$class = 'select_drop';

		?>
		
		<tr valign="top">
			<th scope="row" class="titledesc">
				<label><?php _e( 'Order Page', 'manual_phone_orders' ) ?></label>
				<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.6', '>=' ) ) { ?>
				<span class="woocommerce-help-tip" data-tip="<?php _e( 'Select which page contains your manual order page shortcode', 'manual_phone_orders' ) ?>">
				<?php } else { ?>
				<img height="16" width="16" src="<?php echo WC()->plugin_url() ?>/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Select which page contains your manual order page shortcode', 'manual_phone_orders' ) ?>">
				<?php } ?>
			</th>
			
			<td class="forminp">
				<fieldset>
				<select class="<?php echo $class ?>" name="manual_order_page" style="width:400px">
				
					<?php foreach( $the_pages as $key => $val ) { ?>

						<?php 
						if ( $key == $this->settings['manual_order_page'] )
							$selected = 'selected="selected"';
						else
							$selected = '';
						?>
					
						<option value="<?php echo $key ?>" <?php echo $selected ?>>
							<?php echo $val ?>
						</option>
						
					<?php } ?>
					
				</select>
				<br/>
				<?php _e( 'Use the shortcode:', 'manual_phone_orders' ) ?>
				<br/>
				[woocommerce_manual_phone_order style="searchable"]
				</fieldset>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<label><?php _e( 'Expand to Full Page', 'manual_phone_orders' ) ?></label>
				<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.6', '>=' ) ) { ?>
				<span class="woocommerce-help-tip" data-tip="<?php _e( 'Attempt to override the theme to display the manual/phone order page as a full page with no other visible theme elements', 'manual_phone_orders' ) ?>">
				<?php } else { ?>
				<img height="16" width="16" src="<?php echo WC()->plugin_url() ?>/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Select which page contains your manual order page shortcode', 'manual_phone_orders' ) ?>">
				<?php } ?>
			</th>
			
			<td class="forminp">
				<fieldset>
					<?php 
					if ( empty( $this->settings['full_page'] ) || 'yes' == $this->settings['full_page'] )
						$checked = 'checked="checked"';
					else if ( 'no' == $this->settings['full_page'] )
						$checked = '';
					?>
					<input type="checkbox" name="full_page" <?php echo $checked ?>> <?php _e( 'Enable', 'manual_phone_orders' ) ?> &nbsp; <em>(<?php _e( 'this is an experimental feature', 'manual_phone_orders' ) ?>)</em>
				</fieldset>
			</td>
		</tr>
		<?php 
	}
	
	
	function generate_perms_html() { 
		global $woocommerce, $wpdb;

		if ( !empty( $this->settings['users'] ) )
			$the_users = $this->settings['users'];
		else 
			$the_users = array();

		?>

		<tr valign="top">
		
			<th scope="row" class="titledesc">
			
				<label><?php _e( 'Allowed Users', 'manual_phone_orders' ) ?></label>
				<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.6', '>=' ) ) { ?>
				<span class="woocommerce-help-tip" data-tip="<?php _e( 'Select which users can access the manual order page. Administrators and Shop Managers always have access', 'manual_phone_orders' ) ?>">
				<?php } else { ?>
				<img height="16" width="16" src="<?php echo WC()->plugin_url() ?>/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Select which users can access the manual order page. Administrators and Shop Managers always have access', 'manual_phone_orders' ) ?>">
				<?php } ?>
			</th>
			
			<td class="forminp">
				<fieldset>
				<select class="select_drop_users" name="users[]" style="width:200px; height:40px" data-placeholder="<?php _e( 'Select users', 'manual_phone_orders' ) ?>" multiple>
				
					<?php if ( !empty( $the_users ) ) foreach( $the_users as $key => $val ) { ?>

						<?php 
						if ( isset( $this->settings['users'] ) && in_array( $val, $this->settings['users'] ) ) { 
							$selected = 'selected="selected"';
							$u = get_user_by( 'id', $val );
							$txt = $u->data->user_login . ' (#' . $val . ' ' . $u->data->user_email . ')';
						} else { 
							$selected = '';
						}	
						?>
					
						<option value="<?php echo $val ?>" <?php echo $selected ?>>
							<?php echo $txt ?>
						</option>
						
					<?php } else { ?>
					
						<option value=""></option>
					
					<?php } ?>

				</select>
				</fieldset>
				
			</td>
		</tr>

		<?php 
	}

	
	function generate_roles_html() { 
		global $woocommerce, $wp_roles;


		if ( !empty( $this->settings['roles'] ) )
			$the_users = $this->settings['roles'];
		else 
			$the_users = array();

		?>
		
		<tr valign="top">
		
			<th scope="row" class="titledesc">
				<label><?php _e( 'Allowed Roles', 'manual_phone_orders' ) ?></label>
				<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.6', '>=' ) ) { ?>
				<span class="woocommerce-help-tip" data-tip="<?php _e( 'Select which roles can access the manual order page. Administrators and Shop Managers always have access', 'manual_phone_orders' ) ?>">
				<?php } else { ?>
				<img height="16" width="16" src="<?php echo WC()->plugin_url() ?>/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Select which roles can access the manual order page. Administrators and Shop Managers always have access', 'manual_phone_orders' ) ?>">
				<?php } ?>
			</th>
			
			<td class="forminp">
				<fieldset>
				<select class="select_drop_roles" name="roles[]" style="width:200px; height:40px" data-placeholder="<?php _e( 'Select roles', 'manual_phone_orders' ) ?>" multiple>
				
					<?php if ( !empty( $wp_roles->roles ) ) foreach( $wp_roles->roles as $role => $data ) { ?>

						<?php if ( in_array( $role, array( 'administrator', 'shop_manager' ) ) ) continue; ?>
						
						<?php 
						if ( isset( $this->settings['roles'] ) && in_array( $role, $this->settings['roles'] ) ) { 
							$selected = 'selected="selected"';
						} else { 
							$selected = '';
						}	
						?>
					
						<option value="<?php echo $role ?>" <?php echo $selected ?>>
							<?php echo $data['name'] ?>
						</option>
						
					<?php } ?>
					
					
				</select>
				</fieldset>
	
			</td>
		</tr>

		<?php 
	}
	
	
	function generate_price_change_html() { 
		global $woocommerce, $wp_roles;

		if ( !empty( $this->settings['change_price_users'] ) )
			$the_users = $this->settings['change_price_users'];
		else 
			$the_users = array();

		?>

		<tr valign="top">
		
			<th scope="row" class="titledesc">
			
				<label><?php _e( 'Users That Can Change Prices', 'manual_phone_orders' ) ?></label>
				<?php if ( version_compare( WOOCOMMERCE_VERSION, '2.6', '>=' ) ) { ?>
				<span class="woocommerce-help-tip" data-tip="<?php _e( 'Select which users can changes product prices on the manual order page. If you leave this empty then any user or role with access to the page can change prices. Administrators and Shop Managers always have that ability. ', 'manual_phone_orders' ) ?>">
				<?php } else { ?>
				<img height="16" width="16" src="<?php echo WC()->plugin_url() ?>/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Select which users can access the manual order page. If you leave this empty then any user or role with access to the page can change prices. Administrators and Shop Managers always have the ability. ', 'manual_phone_orders' ) ?>">
				<?php } ?>
			</th>
			
			<td class="forminp">
				<fieldset>
				<select class="select_drop_users" name="change_price_users[]" style="width:200px; height:40px" data-placeholder="<?php _e( 'Select users', 'manual_phone_orders' ) ?>" multiple>
				
					<?php if ( !empty( $the_users ) ) foreach( $the_users as $key => $val ) { ?>

						<?php 
						if ( isset( $this->settings['change_price_users'] ) && in_array( $val, $this->settings['change_price_users'] ) ) { 
							$selected = 'selected="selected"';
							$u = get_user_by( 'id', $val );
							$txt = $u->data->user_login . ' (#' . $val . ' ' . $u->data->user_email . ')';
						} else { 
							$selected = '';
						}	
						?>
					
						<option value="<?php echo $val ?>" <?php echo $selected ?>>
							<?php echo $txt ?>
						</option>
						
					<?php } else { ?>
					
						<option value=""></option>
					
					<?php } ?>

				</select>
				</fieldset>
				
			</td>
		</tr>

		<?php 
	}
	
	function process_admin_options() { 
	
		parent::process_admin_options();

		if ( isset( $_POST['manual_order_page'] ) )
			$page = absint( $_POST['manual_order_page'] );
			
		$this->settings['manual_order_page'] = $page; 

		if ( isset( $_POST['users'] ) )
			$this->settings['users'] = array_unique( $_POST['users'] );
		else 
			$this->settings['users'] = null;
			
		if ( isset( $_POST['roles'] ) )
			$this->settings['roles'] = array_unique( $_POST['roles'] );
		else 
			$this->settings['roles'] = null;
		
		if ( isset( $_POST['full_page'] ) )
			$this->settings['full_page'] = 'yes'; 
		else 
			$this->settings['full_page'] = 'no';
			
		if ( isset( $_POST['disable_terms'] ) )
			$this->settings['disable_terms'] = 'yes'; 
		else 
			$this->settings['disable_terms'] = 'no';
		
		if ( isset( $_POST['enable_stock'] ) )
			$this->settings['enable_stock'] = 'yes'; 
		else 
			$this->settings['enable_stock'] = 'no';
			
		if ( isset( $_POST['change_price_users'] ) )
			$this->settings['change_price_users'] = array_unique( $_POST['change_price_users'] );
		else 
			$this->settings['change_price_users'] = null;

		if ( isset( $_POST['disable_autorefresh'] ) )
			$this->settings['disable_autorefresh'] = 'yes';
		else 
			$this->settings['disable_autorefresh'] = 'no';

		if ( isset( $_POST['default_country'] ) )
			$this->settings['default_country'] = $_POST['default_country'];
		else 
			$this->settings['default_country'] = array();
			
		update_option( 'woocommerce_' . $this->id . '_settings', $this->settings );
	
	}
	

}


add_action( 'woocommerce_integrations', 'ignitewoo_manual_order_integration', 999 );

function ignitewoo_manual_order_integration( $integrations ) {

	$integrations[] = 'IgniteWoo_Manual_Order_Settings';
	
	return $integrations;
}
