#Restrict Content Pro - REST API

This plugin provides a REST API for [Restrict Content Pro](http://restrictcontentpro.com). It is dependent upon the [WordPress REST API](http://wp-api.org) and does not function without it.

###Endpoint:

The RCP REST API is available at `/wp-json/rcp/`.

If the default `wp-json` endpoint has been changed, the `rcp` endpoint will respect that change, so adjust accordingly.

###Versioning:

Each distinct version of the REST API is accessible via the `rcp` endpoint. Versions may be deprecated at anytime but at no time will breaking changes be implementing without incrementing the version number. Past versions are expected to be kept available for at least six months beyond the release of the proceeding version.

- /wp-json/rcp/{version}/{route}

Example:
- /wp-json/rcp/v1/{route}

###Authentication

The RCP REST API requires authenticating with a WordPress user account that has the necessary capabilities in order to view, modify, and delete information via the REST API.

Please see the [WordPress REST API Authentication](http://v2.wp-api.org/guide/authentication/) documentation to instructions on how to authenticate with the API.

###Routes

There are three routes provided with this API:

####Members:

- `GET` /wp-json/rcp/{version}/members/
- `GET` /wp-json/rcp/{version}/members/{member_id}
- `POST` /wp-json/rcp/{version}/members/new
- `POST` /wp-json/rcp/{version}/members/update/{member_id}
- `DELETE` /wp-json/rcp/{version}/members/

Each of the three HTTP methods support additional arguments that can be passed in with the `body` of the request.

*GET* `/wp-json/rcp/{version}/members/`:

Required parameters:

None.

Optional parameters:

- `status` - Retrieve only members with the specified status
- - `active`, `pending`, `cancelled`, `expired`
- `subscription` - The subscription level ID from which to retrieve members
- `recurring` - Whether to include recurring or non-recurring members only
- - `1`, `0`
- `number` - The total of members to retrieve per page
- `offset` - The number of members to skip (for pagination purposes)
- `orderby` - The field on which to order the members returned
- - Any field allowed to be passed to [get_users()](#)
- `order` - The direction which to sort members
- - `ASC`, `DESC`
- `s` - Search value, such as the name, email, or user log in of the member

*GET* `/wp-json/rcp/{version}/members/{member_id}`:

Required parameters:

- `id` - The member ID to retrieve. Can be passed in the URL or as part of the body.

Optional parameters:

None.

*POST* `/wp-json/rcp/{version}/members/new`:

Required parameters:

 - `login` - The log in name to assign to the newly created member account

Optional parameters:

- `status` - The subscription status to assign to the member
- `expiration` - The expiration date to assign to the member
- `subscription` - The subscription level ID to assign to the member
- `profile_id` - The payment profile ID of the member
- `recurring` - Boolean flag that indicates if the subscription is recurring or one-time only
- - `1` or `0`
- Any parameter supported by [wp_insert_user()](#)

*POST* `/wp-json/rcp/{version}/members/update/{member_id}`:

Required parameters:

- `id` - The member ID to update. Can be passed in the URL or as part of the body.

Optional parameters:

- `status` - The subscription status to assign to the member
- `expiration` - The expiration date to assign to the member
- `subscription` - The subscription level ID to assign to the member
- `profile_id` - The payment profile ID of the member
- `recurring` - Boolean flag that indicates if the subscription is recurring or one-time only
- - `1` or `0`
- Any parameter supported by [wp_insert_user()](#)

*DELETE* `/wp-json/rcp/{version}/members/delete`:

Required parameters:

- `id` - The ID of the member account to delete

Optional parameters:

None.

####Payments:

- `GET` /wp-json/rcp/{version}/payments/
- `GET` /wp-json/rcp/{version}/payments/{payment_id}
- `POST` /wp-json/rcp/{version}/payments/new
- `POST` /wp-json/rcp/{version}/payments/update/{payment_id}
- `DELETE` /wp-json/rcp/{version}/payments/

Each of the three HTTP methods support additional arguments that can be passed in with the `body` of the request.

*POST* `/wp-json/rcp/{version}/payments/new`:

Required parameters:

- `user_id` - The member account ID the payment belongs to
- `amount` - The total amount of the payment
- `subscription` - The name of the subscription level the payment was recorded for

Optional parameters:

- `status` - The status of the payment
- - `complete` or `refunded`
- `subscription_key` - The subscription key of the level the payment was recorded for
- `transaction_id` - The transaction ID from the merchant processor of the payment
- `date` - The date the payment was processed
- `payment_type` - A descriptor of the payment type
- - Example: `Credit Card`, `PayPal`, or `Credit Card One Time`

*POST* `/wp-json/rcp/{version}/payments/update/{payment_id}`:

- `id` - The payment ID to update. Can be passed in the URL or as part of the body.

None.

Optional parameters:

- `amount` - The total amount of the payment
- `user_id` - The member account ID the payment belongs to
- `status` - The status of the payment
- - `complete` or `refunded`
- `subscription` - The name of the subscription level the payment was recorded for
- `subscription_key` - The subscription key of the level the payment was recorded for
- `transaction_id` - The transaction ID from the merchant processor of the payment
- `date` - The date the payment was processed
- `payment_type` - A descriptor of the payment type
- - Example: `Credit Card`, `PayPal`, or `Credit Card One Time`

*DELETE* `/wp-json/rcp/{version}/payments/delete`:

Required parameters:

- `id` - The ID of the payment record to delete

Optional parameters:

####Earnings:

- `GET` /wp-json/rcp/{version}/earnings/
