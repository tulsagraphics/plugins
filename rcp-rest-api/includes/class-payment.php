<?php

class RCP_REST_API_Payment {

	public $subscription = '';
	public $subscription_id = 0;
	public $subscription_key = '';
	public $date = '00-00-00 00:00:00';
	public $amount = '';
	public $user_id = 0;
	public $transaction_id = '';
	public $status = '';

	/**
	 * Get things started
	 *
	 * @since 1.0
	 */
	public function __construct( $_id = 0 ) {
		$this->setup( $_id );
	}

	/**
	 * Initialize the payment object
	 *
	 * @since 1.0
	 */
	public function setup( $_id = 0 ) {

		if( empty( $_id ) ) {
			return;
		}

		$db = new RCP_Payments;
		$payment = $db->get_payment( $_id );

		if( $payment ) {

			foreach( $payment as $key => $value ) {

				$this->$key = $value;

			}

			$this->subscription_id = rcp_get_subscription_details_by_name( $payment->subscription )->id;

		}

	}

	/**
	 * Sanitize payment arguments for update / new queries
	 *
	 * @since 1.0
	 */
	public function sanitize_payment_args( $args = array() ) {

		$whitelist = array(
			'subscription',     // Name of the associated subscription level.
			'object_id',        // ID of the associated object (subscription level).
			'object_type',      // Type of object, such as "subscription".
			'date',             // Date the payment was made, in MySQL format.
			'amount',           // Total amount the payment was for, after fees/credits/discounts are applied.
			'subtotal',         // Base price of the subscription level.
			'credits',          // Credits applied towards the payment (such as prorated credits).
			'fees',             // Fees applied towards the payment.
			'discount_amount',  // Discounted amount.
			'discount_code',    // Applied discount code, if any.
			'user_id',          // ID number of the associated user.
			'payment_type',     // Type of payment, i.e. "Credit card one time".
			'subscription_key', // Unique key for this subscription.
			'transaction_id',   // Transaction ID from the gateway for this payment.
			'status',           // Status of the payment (complete, refunded, pending, etc.).
			'gateway',          // Slug of the gateway that was used for this payment.
		);

		foreach( $args as $key => $value ) {

			// Do not allow empty values
			if( empty( $key ) || empty( $value ) || ! in_array( $key, $whitelist ) ) {
				unset( $args[ $key ] );
			}

		}

		if( isset( $args['subscription'] ) && is_int( $args['subscription'] ) ) {
			
			if( ! empty( $this->subscription ) ) {

				$args['subscription'] = $this->subscription;
				
			} else {

				if ( empty( $args['object_id'] ) ) {
					$args['object_id'] = absint( $args['subscription'] );
				}

				$args['subscription'] = rcp_get_subscription_name( $args['subscription'] );

			}


		}

		if( isset( $args['subscription_id'] ) ) {

			// If `object_id` hasn't been supplied, we can use the subscription ID for this.
			if ( empty( $args['object_id'] ) && is_int( $args['subscription_id'] ) ) {
				$args['object_id'] = absint( $args['subscription_id'] );
			}

			unset( $args['subscription_id'] );

		}

		return $args;

	}

}