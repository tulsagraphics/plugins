<?php

class RCP_REST_API_Member extends RCP_Member {

	public $subscription_name;
	public $subscription_id;
	public $subscription_key;
	public $access_level;
	public $expiration_date;
	public $expiration_time;
	public $status;
	public $is_active;
	public $is_recurring;
	public $payment_profile_id;
	public $notes;
	public $payments;
	public $is_trialing;
	public $has_trialed;

	/**
	 * Setup our member properties
	 *
	 * @since 1.0
	 */
	public function setup( $_id = 0 ) {

		$this->subscription_id    = $this->get_subscription_id();
		$this->subscription_name  = $this->get_subscription_name();
		$this->subscription_key   = $this->get_subscription_key();
		$this->access_level       = rcp_get_subscription_access_level( $this->subscription_id );
		$this->expiration_date    = $this->get_expiration_date();
		$this->expiration_time    = $this->get_expiration_time();
		$this->status             = $this->get_status();
		$this->is_active          = $this->is_active();
		$this->is_recurring       = $this->is_recurring();
		$this->is_trialing        = $this->is_trialing();
		$this->has_trialed        = $this->has_trialed();
		$this->payment_profile_id = $this->get_payment_profile_id();
		$this->notes              = $this->get_notes();
		$this->payments           = $this->get_payments();

	}

	/**
	 * Helper method for updating subscription level
	 *
	 * @since 1.0
	 */
	public function set_subscription( $subscription_id = 0 ) {
		delete_user_meta( $this->ID, 'rcp_pending_subscription_level' );
		update_user_meta( $this->ID, 'rcp_subscription_level', $subscription_id );
	}

	/**
	 * Sanitize expiration date to ensure it's sent in with the proper format
	 *
	 * @since 1.0
	 */
	public function sanitize_expiration( $expiration = '' ) {

		if( is_int( $expiration ) ) {

			$expiration = date( 'Y-n-d H:i:s', $expiration );

		} elseif( 'none' === $expiration ) {

			$expiration = 'none';

		} else {

			$expiration = date( 'Y-n-d H:i:s', strtotime( $expiration, current_time( 'timestamp' ) ) );

		}

		return $expiration;
	}

}