<?php

class RCP_REST_API_Routes {

	/**
	 * Holds our routes
	 *
	 * @since 1.0
	 */
	private $routes;

	/**
	 * Get started
	 *
	 * @since 1.0
	 */
	public function __construct() {

		$this->routes['v1'] = array(
			'earnings' => 'RCP_REST_API_Earnings_Route_V1',
			'members'  => 'RCP_REST_API_Member_Route_V1',
			'payments' => 'RCP_REST_API_Payment_Route_V1',
		);

		$this->load();

		foreach( $this->routes as $version => $routes ) {		

			foreach( $routes as $route ) {
				new $route;
			}

		}
	}

	/**
	 * Load our files
	 *
	 * @since 1.0
	 */
	private function load() {

		$path = trailingslashit( dirname( __FILE__ ) );

		// Load each enabled integrations
		require_once $path . 'routes/class-route.php';

		foreach( $this->routes as $version => $routes ) {

			foreach( $routes as $filename => $class ) {

				if( file_exists( $path . 'routes/' . $version . '/class-' . $filename . '-route.php' ) ) {
					require_once $path . 'routes/' . $version . '/class-' . $filename . '-route.php';
				}

			}

		}

	}

}