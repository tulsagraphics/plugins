<?php

class RCP_REST_API_Earnings_Route_V1 extends RCP_REST_API_Route {
	
	protected $db;

	/**
	 * Get things started
	 *
	 * @since 1.0
	 */
	public function init() {
		$this->id = 'earnings';
		$this->db = new RCP_Payments;
	}

	/**
	 * Retrieve response data
	 *
	 * @since 1.0
	 */
	public function get_data( WP_REST_Request $request ) {

		$current_time = current_time( 'timestamp' );

		$today = array(
			'day'   => date( 'd', $current_time ),
			'month' => date( 'n', $current_time ),
			'year'  => date( 'Y', $current_time )
		);

		$month = array(
			'month' => date( 'n', $current_time ),
			'year'  => date( 'Y', $current_time )
		);

		$year = array(
			'year'  => date( 'Y', $current_time )
		);

		$response = array(
			'today'      => $this->db->get_earnings( array( 'date' => $today ) ),
			'month'      => $this->db->get_earnings( array( 'date' => $month ) ),
			'year'       => $this->db->get_earnings( array( 'date' => $year ) ),
		);

		return new WP_REST_Response( $response );

	}

	/**
	 * Determine if authenticated user has permission to access response data
	 *
	 * @since 1.0
	 */
	public function can_view() {
		return current_user_can( 'rcp_view_payments' );
	}

}