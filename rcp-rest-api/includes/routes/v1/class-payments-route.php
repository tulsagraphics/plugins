<?php

class RCP_REST_API_Payment_Route_V1 extends RCP_REST_API_Route {

	protected $db;

	/**
	 * Get things started
	 *
	 * @since 1.0
	 */
	public function init() {
		$this->id = 'payments';
		$this->db = new RCP_Payments;
	}

	/**
	 * Retrieve response data
	 *
	 * @since 1.0
	 */
	public function get_data( WP_REST_Request $request ) {

		if( ! empty( $this->query_args['id'] ) ) {

			$payment = new RCP_REST_API_Payment( $this->query_args['id'] );

			if( ! empty( $payment->id ) ) {

				$payment->setup();

			} else {

				$payment = new WP_Error( 'no_payment', __( 'Invalid payment', 'rcp-rest' ), array( 'status' => 404 ) );

			}


			return $payment;
		}

		return new WP_REST_Response( $this->get_payments() );

	}

	/**
	 * Retrieve response data for create requests
	 *
	 * @since 1.0
	 */
	public function new_post_data( WP_REST_Request $request ) {

		if( ! isset( $this->body['amount'] ) ) {
			$response = new WP_Error( 'missing_amount', __( 'No payment amount supplied', 'rcp-rest' ), array( 'status' => 500 ) );
		}

		if( empty( $this->body['subscription'] ) ) {
			$response = new WP_Error( 'missing_subscription', __( 'No subscription name supplied', 'rcp-rest' ), array( 'status' => 500 ) );
		}

		if( empty( $this->body['user_id'] ) ) {
			$response = new WP_Error( 'missing_user_id', __( 'No user ID supplied', 'rcp-rest' ), array( 'status' => 500 ) );
		}

		if( empty( $response ) ) {

			$payment    = new RCP_REST_API_Payment;
			$args       = $payment->sanitize_payment_args( $this->body );
			$payment_id = $this->db->insert( $args );

			if( $payment_id ) {

				$response = 1;

			} else {

				$response = new WP_Error( 'create_failed', __( 'Payment creation failed', 'rcp-rest' ), array( 'status' => 500 ) );

			}

		}

		return new WP_REST_Response( $response );

	}

	/**
	 * Retrieve response data for update requests
	 *
	 * @since 1.0
	 */
	public function update_post_data( WP_REST_Request $request ) {

		if( isset( $this->body['id'] ) ) {
			$this->body['ID'] = $this->body['id'];
			unset( $this->body['id'] );
		}

		if( empty( $this->body['ID'] ) ) {
			$response = new WP_Error( 'missing_id', __( 'No payment ID supplied', 'rcp-rest' ), array( 'status' => 500 ) );
			return new WP_REST_Response( $response );
		}

		$payment = new RCP_REST_API_Payment( $this->body['ID'] );
		$fields  = (array) $payment;
		$args    = wp_parse_args( $this->body, $fields );
		$args    = $payment->sanitize_payment_args( $args );

		if( $this->db->update( $this->body['ID'], $args ) ) {

			$response = 1;

		} else {

			$response = new WP_Error( 'update_failed', __( 'Update Failed', 'rcp-rest' ), array( 'status' => 500 ) );

		}


		return new WP_REST_Response( $response );

	}

	/**
	 * Retrieve response data for delete requests
	 *
	 * @since 1.0
	 */
	public function delete_data( WP_REST_Request $request ) {

		if( isset( $this->body['id'] ) ) {
			$this->body['ID'] = $this->body['id'];
			unset( $this->body['id'] );
		}

		$this->db->delete( $this->body['ID'] );

		return new WP_REST_Response( 1 );

	}

	/**
	 * Retrieve payment data
	 *
	 * @since 1.0
	 */
	private function get_payments() {

		$args = wp_parse_args( $this->query_args, array(
			'number'       => 20,
			'orderby'      => 'id',
			'order'        => 'DESC',
			'offset'       => 0,
			's'            => '',
			'status'       => '',
			'date'         => '',
			'fields'       => '*',
			'subscription' => 0,
		) );

		if( ! empty( $this->query_args['member'] ) ) {
			$args['user_id'] = absint( $this->query_args['member'] );
		}

		$payments = $this->db->get_payments( $args );

		if( ! empty( $payments ) ) {

			foreach( $payments as $key => $payment ) {

				$payments[ $key ] = new RCP_REST_API_Payment( $payment->id );

			}

		}

		return $payments;

	}

	/**
	 * Determine if authenticated user has permission to access response data
	 *
	 * @since 1.0
	 */
	public function can_view() {
		return current_user_can( 'rcp_view_payments' );
	}

	/**
	 * Determine if authenticated user has permission to edit data
	 *
	 * @since 1.0
	 */
	public function can_edit() {
		return current_user_can( 'rcp_manage_payments' );
	}

}