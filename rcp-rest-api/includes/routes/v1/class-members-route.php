<?php

class RCP_REST_API_Member_Route_V1 extends RCP_REST_API_Route {

	private $user_fields;

	/**
	 * Get things started
	 *
	 * @since 1.0
	 */
	public function init() {

		$this->id = 'members';

		// White list of user fields that can be altered and their associated query arg name
		$this->user_fields = array(
			'ID'           => 'ID',
			'login'        => 'user_login',
			'first_name'   => 'first_name',
			'last_name'    => 'last_name',
			'display_name' => 'display_name',
			'email'        => 'user_email',
			'password'     => 'user_pass'
		);
	}

	/**
	 * Retrieve response data
	 *
	 * @since 1.0
	 */
	public function get_data( WP_REST_Request $request ) {

		if( ! empty( $this->query_args['id'] ) ) {

			$member = new RCP_REST_API_Member( $this->query_args['id'] );

			if( ! empty( $member->ID ) ) {

				$member->setup();

			} else {

				$member = new WP_Error( 'no_member', 'Invalid member', array( 'status' => 404 ) );

			}


			return $member;
		}

		return new WP_REST_Response( $this->get_members() );

	}

	/**
	 * Retrieve response data for create requests
	 *
	 * @since 1.0
	 */
	public function new_post_data( WP_REST_Request $request ) {

		$args = array();

		foreach( $this->body as $key => $value ) {

			if( array_key_exists( $key, $this->user_fields ) ) {

				$args[ $this->user_fields[ $key ] ] = wp_slash( $value );

			}

		}

		// Status is required.
		if( empty( $this->body['status'] ) ) {
			$response = new WP_Error( 'missing_status', __( 'No status supplied', 'rcp-rest' ), array( 'status' => 500 ) );

			return new WP_REST_Response( $response );
		}

		if( empty( $args['user_pass'] ) ) {
			$args['user_pass'] = wp_generate_password( 20 );
		}

		$user_id = wp_insert_user( $args );

		if( is_wp_error( $user_id ) ) {

			return new WP_REST_Response( $user_id );

		} else {

			$member = new RCP_REST_API_Member( $user_id );

			if( isset( $this->body['status'] ) ) {
				$member->set_status( sanitize_text_field( $this->body['status'] ) );
			}

			if( isset( $this->body['subscription'] ) ) {
				$member->set_subscription( sanitize_text_field( $this->body['subscription'] ) );
			}

			if( isset( $this->body['expiration'] ) ) {
				$member->set_expiration_date( $member->sanitize_expiration( $this->body['expiration'] ) );
			} else {
				// Calculate automatically.
				$expiration = rcp_calculate_subscription_expiration( $member->get_subscription_id() );
				$member->set_expiration_date( $expiration );
			}

			if( isset( $this->body['recurring'] ) ) {
				$member->set_recurring( filter_var( $this->body['recurring'], FILTER_VALIDATE_BOOLEAN ) );
			}

			if( isset( $this->body['profile_id'] ) ) {
				$member->set_payment_profile_id( sanitize_text_field( $this->body['profile_id'] ) );
			}

			$response = 1;

		}

		return new WP_REST_Response( $response );

	}

	/**
	 * Retrieve response data for update requests
	 *
	 * @since 1.0
	 */
	public function update_post_data( WP_REST_Request $request ) {

		if( isset( $this->body['id'] ) ) {
			$this->body['ID'] = $this->body['id'];
			unset( $this->body['id'] );
		}


		$args = array();

		foreach( $this->body as $key => $value ) {

			if( array_key_exists( $key, $this->user_fields ) ) {

				$args[ $this->user_fields[ $key ] ] = wp_slash( $value );

			}

		}

		if( empty( $args['ID'] ) ) {
			$response = new WP_Error( 'missing_id', __( 'No user ID supplied', 'rcp-rest' ), array( 'status' => 500 ) );
			return new WP_REST_Response( $response );
		}

		if( wp_update_user( $args ) ) {

			$member = new RCP_REST_API_Member( $args['ID'] );

			if( ! empty( $this->body['renew'] ) ) {

				$recurring = ! empty( $this->body['recurring'] ) ?  filter_var( $this->body['recurring'], FILTER_VALIDATE_BOOLEAN ) : false;
				$member->renew( $recurring );

			} else {

				if( isset( $this->body['status'] ) ) {
					$member->set_status( sanitize_text_field( $this->body['status'] ) );
				}

				if( isset( $this->body['subscription'] ) ) {
					$member->set_subscription( sanitize_text_field( $this->body['subscription'] ) );
				}

				if( isset( $this->body['expiration'] ) ) {
					$member->set_expiration_date( $member->sanitize_expiration( $this->body['expiration'] ) );
				}

				if( isset( $this->body['recurring'] ) ) {
					$member->set_recurring( filter_var( $this->body['recurring'], FILTER_VALIDATE_BOOLEAN ) );
				}

				if( isset( $this->body['profile_id'] ) ) {
					$member->set_payment_profile_id( sanitize_text_field( $this->body['profile_id'] ) );
				}

			}

			$response = 1;

		} else {

			$response = new WP_Error( 'update_failed', __( 'Update Failed', 'rcp-rest' ), array( 'status' => 500 ) );

		}

		return new WP_REST_Response( $response );

	}

	/**
	 * Retrieve response data for delete requests
	 *
	 * @since 1.0
	 */
	public function delete_data( WP_REST_Request $request ) {

		if( isset( $this->body['id'] ) ) {
			$this->body['ID'] = $this->body['id'];
			unset( $this->body['id'] );
		}

		if( empty( $this->body['ID'] ) ) {
			$response = new WP_Error( 'missing_id', __( 'No user ID supplied', 'rcp-rest' ), array( 'status' => 500 ) );
			return new WP_REST_Response( $response );
		}

		if( ! function_exists( 'wp_delete_user' ) ) {
			require_once ABSPATH . 'wp-admin/includes/user.php';
		}

		if( wp_delete_user( $this->body['ID'] ) ) {

			$response = 1;

		} else {

			$response = new WP_Error( 'delete_failed', __( 'Delete Failed', 'rcp-rest' ), array( 'status' => 500 ) );

		}

		return new WP_REST_Response( $response );

	}

	/**
	 * Retrieve members data
	 *
	 * @since 1.0
	 */
	private function get_members() {

		$request = wp_parse_args( $this->query_args, array(
			'number'       => 20,
			'orderby'      => 'ID',
			'order'        => 'DESC',
			'offset'       => 0,
			's'            => '',
			'status'       => '',
			'subscription' => 0,
			'recurring'    => '',
		) );

		$members = array();

		$args = array(
			'offset'        => $request['offset'],
			'number'        => $request['number'],
			'orderby'       => $request['orderby'],
			'order'         => $request['order'],
			'meta_query'    => array()
		);

		if( ! empty( $request['status'] ) ) {

			$args['meta_query'][] = array(
				'key'   => 'rcp_status',
				'value' => $request['status']
			);

		}

		if( ! empty( $request['subscription'] ) ) {

			$args['meta_query'][] = array(
				'key'   => 'rcp_subscription_level',
				'value' => $request['subscription']
			);

		}

		if( ! empty( $request['recurring'] ) ) {

			if( 'no' === $request['recurring'] ) {

				// find non recurring users
				$args['meta_query'][] = array(
					'key'     => 'rcp_recurring',
					'compare' => 'NOT EXISTS'
				);

			} else {

				// find recurring users
				$args['meta_query'][] = array(
					'key'     => 'rcp_recurring',
					'value'   => 'yes'
				);

			}

		}

		if( ! empty( $request['s'] ) ) {
			$args['search'] = sanitize_text_field( $request['s'] );
		}

		$members = get_users( $args );

		if( ! empty( $members ) ) {

			foreach( $members as $key => $member ) {

				$members[ $key ] = new RCP_REST_API_Member( $member->ID );
				$members[ $key ]->setup();

			}

		}

		return $members;

	}

	/**
	 * Determine if authenticated user has permission to access response data
	 *
	 * @since 1.0
	 */
	public function can_view() {
		return current_user_can( 'rcp_view_members' );
	}

	/**
	 * Determine if authenticated user has permission to edit data
	 *
	 * @since 1.0
	 */
	public function can_edit() {
		return current_user_can( 'rcp_manage_members' );
	}

}