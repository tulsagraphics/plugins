<?php

class RCP_REST_API_Route {

	public $id = '';
	public $body = array();
	public $query_args = array();

	/**
	 * Constructor
	 *
	 * @since 1.0
	 */
	public function __construct() {
		$this->init();
		$this->actions();
	}

	/**
	 * Get things started
	 *
	 * @since 1.0
	 */
	public function init() {}

	/**
	 * Add our actions to register routes
	 *
	 * @since 1.0
	 */
	public function actions() {
		add_action( 'rest_api_init', array( $this, 'register_v1_routes' ) );
	}

	/**
	 * Register our routes
	 *
	 * @since 1.0
	 */
	public function register_v1_routes() {

		register_rest_route( 'rcp/v1', '/' . $this->id, array(
			'methods'             => 'GET',
			'callback'            => array( $this, 'process_get_data' ),
			'permission_callback' => array( $this, 'can_view' )
		) );

		register_rest_route( 'rcp/v1', '/' . $this->id . '/(?P<id>\d+)', array(
			'methods'             => 'GET',
			'callback'            => array( $this, 'process_get_data' ),
			'permission_callback' => array( $this, 'can_view' ),
		) );

		register_rest_route( 'rcp/v1', '/' . $this->id . '/new', array(
			'methods'             => 'POST',
			'callback'            => array( $this, 'process_new_post_data' ),
			'permission_callback' => array( $this, 'can_edit' )
		) );

		register_rest_route( 'rcp/v1', '/' . $this->id . '/update/(?P<id>\d+)', array(
			'methods'             => 'POST',
			'callback'            => array( $this, 'process_update_post_data' ),
			'permission_callback' => array( $this, 'can_edit' )
		) );

		register_rest_route( 'rcp/v1', '/' . $this->id . '/delete/(?P<id>\d+)', array(
			'methods'             => 'DELETE',
			'callback'            => array( $this, 'process_delete_data' ),
			'permission_callback' => array( $this, 'can_edit' )
		) );

	}

	/**
	 * Process GET requests
	 *
	 * @since 1.0
	 */
	public function process_get_data( WP_REST_Request $request ) {
		$this->query_args = $request->get_params();
		return $this->get_data( $request );
	}

	/**
	 * Process POST create requests
	 *
	 * @since 1.0
	 */
	public function process_new_post_data( WP_REST_Request $request ) {
		$this->body = $request->get_body_params();
		return $this->new_post_data( $request );
	}

	/**
	 * Process POST update requests
	 *
	 * @since 1.0
	 */
	public function process_update_post_data( WP_REST_Request $request ) {
		$this->body = array_merge( $request->get_url_params(), $request->get_body_params() );
		return $this->update_post_data( $request );
	}

	/**
	 * Process POST delete requests
	 *
	 * @since 1.0
	 */
	public function process_delete_data( WP_REST_Request $request ) {
		$this->body = $request->get_params();
		return $this->delete_data( $request );
	}

	/**
	 * Retrieve response data
	 *
	 * @since 1.0
	 */
	public function get_data( WP_REST_Request $request ) {
		$response = new WP_Error( 'invalid_route', __( 'Invalid route', 'rcp-rest' ), array( 'status' => 404 ) );
		return new WP_REST_Response( $response );
	}

	/**
	 * Retrieve new POST response data
	 *
	 * @since 1.0
	 */
	public function new_post_data( WP_REST_Request $request ) {
		$response = new WP_Error( 'invalid_route', __( 'Invalid route', 'rcp-rest' ), array( 'status' => 404 ) );
		return new WP_REST_Response( $response );
	}

	/**
	 * Retrieve update POST response data
	 *
	 * @since 1.0
	 */
	public function update_post_data( WP_REST_Request $request ) {
		$response = new WP_Error( 'invalid_route', __( 'Invalid route', 'rcp-rest' ), array( 'status' => 404 ) );
		return new WP_REST_Response( $response );
	}

	/**
	 * Retrieve DELETE response data
	 *
	 * @since 1.0
	 */
	public function delete_data( WP_REST_Request $request ) {
		$response = new WP_Error( 'invalid_route', __( 'Invalid route', 'rcp-rest' ), array( 'status' => 404 ) );
		return new WP_REST_Response( $response );
	}

	/**
	 * Determine if authenticated user has permission to access response data
	 *
	 * @since 1.0
	 */
	public function can_view() {
		return current_user_can( 'edit_users' );
	}

	/**
	 * Determine if authenticated user has permission to edit data
	 *
	 * @since 1.0
	 */
	public function can_edit() {
		return current_user_can( 'edit_users' );
	}
}