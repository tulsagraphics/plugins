<?php
/**
 * Plugin Name: Restrict Content Pro - REST API
 * Plugin URI: http://restrictcontentpro.com/addons/rest-api
 * Description: Adds a REST API to Restrict Content Pro
 * Author: Restrict Content Pro Team
 * Author URI: http://restrictcontentpro.com
 * Version: 1.0.4
 * Text Domain: rcp-rest
 * Domain Path: languages
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

final class RCP_REST_API {

	/**
	 * @var RCP_REST_API The one true RCP_REST_API
	 * @since 1.0
	 */
	private static $instance;

	/**
	 * @var $path The plugin directory path
	 * @since 1.0
	 */
	public static $path;

	/**
	 * @var $routes The API routes
	 * @since 1.0
	 */
	public static $routes;

	/**
	 * Main RCP_REST_API Instance.
	 *
	 * Insures that only one instance of RCP_REST_API exists in memory at any one
	 * time. Also prevents needing to define globals all over the place.
	 *
	 * @since 1.0
	 * @static
	 * @return The one true RCP_REST_API
	 */
	public static function instance() {

		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof RCP_REST_API ) ) {

			self::$path = plugin_dir_path( __FILE__ );

			self::$instance = new RCP_REST_API;
			self::$instance->text_domain();
			self::$instance->updater();
			self::$instance->includes();

			self::$routes = new RCP_REST_API_Routes;

		}
		return self::$instance;
	}

	/**
	 * Throw error on object clone.
	 *
	 * The whole idea of the singleton design pattern is that there is a single
	 * object therefore, we don't want the object to be cloned.
	 *
	 * @since 1.0
	 * @access public
	 * @return void
	 */
	public function __clone() {
		// Cloning instances of the class is forbidden.
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'rcp-rest' ), '1.0' );
	}

	/**
	 * Disable unserializing of the class.
	 *
	 * @since 1.0
	 * @access public
	 * @return void
	 */
	public function __wakeup() {
		// Unserializing instances of the class is forbidden.
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'rcp-rest' ), '1.0' );
	}

	/**
	 * Load our textdomain
	 *
	 * @since 1.0
	 * @access public
	 * @return void
	 */
	public function text_domain() {

		// Set filter for plugin's languages directory
		$lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
		$lang_dir = apply_filters( 'rcp_rest_languages_directory', $lang_dir );


		// Traditional WordPress plugin locale filter
		$locale   = apply_filters( 'plugin_locale',  get_locale(), 'rcp-rest' );
		$mofile   = sprintf( '%1$s-%2$s.mo', 'rcp-rest', $locale );

		// Setup paths to current locale file
		$mofile_local  = $lang_dir . $mofile;
		$mofile_global = WP_LANG_DIR . '/rcp/' . $mofile;

		if ( file_exists( $mofile_global ) ) {
			// Look in global /wp-content/languages/rcp folder
			load_textdomain( 'rcp-rest', $mofile_global );
		} elseif ( file_exists( $mofile_local ) ) {
			// Look in local /wp-content/plugins/restrict-content-pro/languages/ folder
			load_textdomain( 'rcp-rest', $mofile_local );
		} else {
			// Load the default language files
			load_plugin_textdomain( 'rcp-rest', false, $lang_dir );
		}

	}

	/**
	 * Load our plugin updater
	 *
	 * @since 1.0
	 * @access public
	 * @return void
	 */
	public function updater() {

		if( is_admin() && class_exists( 'RCP_Add_On_Updater' ) ) {
			$updater = new RCP_Add_On_Updater( 389, __FILE__, '1.0.4' );
		}

	}

	/**
	 * Load our files
	 *
	 * @since 1.0
	 * @access public
	 * @return void
	 */
	public function includes() {

		require_once self::$path . 'includes/class-routes.php';
		require_once self::$path . 'includes/class-member.php';
		require_once self::$path . 'includes/class-payment.php';

	}

}

/**
 * Load the REST API after other plugins are loaded
 *
 * @since 1.0
 * @access public
 * @return void
 */
function rcp_rest_api() {

	if( ! function_exists( 'rcp_is_active' ) ) {
		return false;
	}

	return RCP_REST_API::instance();

}
add_action( 'plugins_loaded', 'rcp_rest_api' );