( function( $ ) {

	"use strict";

	var JetEngine = {

		init: function() {

			var widgets = {
				'jet-listing-dynamic-field.default' : JetEngine.widgetDynamicField
			};

			$.each( widgets, function( widget, callback ) {
				window.elementorFrontend.hooks.addAction( 'frontend/element_ready/' + widget, callback );
			});

		},

		widgetDynamicField: function( $scope ) {

			var $slider = $scope.find( '.jet-engine-gallery-slider' );

			if ( $slider.length ) {
				$slider.slick( $slider.data( 'atts' ) );
			}

		},

	};

	$( window ).on( 'elementor/frontend/init', JetEngine.init );

}( jQuery ) );
