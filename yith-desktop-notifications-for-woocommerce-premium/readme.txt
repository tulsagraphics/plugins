=== YITH Desktop Notifications for WooCommerce Premium  ===

== Changelog ==  (Last version 1.2.0)

= Version 1.2.0 - Released: May 24, 2018 =

* New: Support to WordPress 4.9.6 RC2
* New: Support to WooCommerce 3.4.0 RC1
* New: translation in Persian. Thanks to Sadra
* Update: Dutch language
* Update: plugin framework

= Version 1.1.5 - Released: Jan 31, 2018 =

* New: support to WordPress 4.9.2
* New: support to WooCommerce 3.3.0
* Update: plugin framework 3.0.11
* Dev: new filter yith_wcdn_url_suborder

= Version 1.1.4 - Released: Jan 05, 2018 =

* Fix - vendor notification when there it is in the same rule with others roles
* Dev - added filter yith_wcdn_in_array
* Dev - added filter yith_wcdn_name_products
* Update - Plugin core

= Version 1.1.3 - Released: Oct 10, 2017 =

* New: Support to WooCommerce 3.2.0 RC2
* Update: Plugin core

= Version 1.1.2 - Released: Jul 27, 2017 =

* New - Added low stock notification

= Version 1.1.1 - Released: Jul 26, 2017 =

* New - Brazilian translation (thanks to Guilherme Tumolo)
* New - Italian translation (thanks to Antonio Mercurio)
* fix - display notifications only when a product is sold
* dev - yith_wcdn_change_protocol filter

= Version 1.1.0 - Released: Mar 09, 2017 =

* New - support to WooCommerce 2.7.0-RC1
* Update - YITH Plugin Framework
* Fix - display notifications for all roles

= Version 1.0.1 - Released: Jan 11, 2017 =

* New - compatibility with Request a Quote
* New - compatibility with Title Bar Effects
* Fix - issue when registering notification and Multi Vendor is activated.
* Fix - issue with "request permission" on IE
* Fix - issue with undefined notifications
* Fix - issue with "request permission" on safari.

= Version 1.0.0 - Released: Aug 10, 2016 =

* Initial release