<?php
namespace Bookly\Lib\Proxy;

use Bookly\Lib\Base;

/**
 * Class GoogleMapsAddress
 * @package Bookly\Lib\Proxy
 *
 * @method static string renderSearchField()
 * @see \BooklyGoogleMapsAddress\Lib\ProxyProviders\Local::renderSearchField()
 */
abstract class GoogleMapsAddress extends Base\ProxyInvoker
{

}