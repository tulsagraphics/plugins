��    V      �     |      x     y  	   �  N   �  3   �       +   2  9   ^  *   �  3   �  $   �  1   	  M   N	  *   �	     �	  ?   �	     
  	   /
     9
     J
     W
     d
     m
     ~
     �
  *   �
     �
     �
  	   �
     �
  
                  #     (     C  !   R     t  :   y     �     �      �     �               &     7     H     Z     j  ,   �  
   �  
   �     �     �  	   �  	   �     �               &     2     :     M  	   ]     g     n  ;        �     �     �     �  	   �  \   �     B     J     [     i  j   m  %   �     �            
   1  r   <     �  �  �     ?     Q  V   a  (   �  "   �  '     ;   ,  +   h  5   �  %   �  4   �  R   %  +   x     �  M   �       	        (     9     J     Y     b     r     �  +   �     �     �  
   �     �                  	        )     I     ^     c  @   i  	   �     �  '   �     �          $     5     G     \     r     �  ,   �  	   �     �     �     �               $     5     G     W  
   h     s     �     �     �     �  ?   �                    +  	   1  Z   ;     �     �     �     �  |   �  '   ;     c     �     �     �  w   �     #            3   R       '   !   *               M   C      A          H      .   S          "   V       %              >      N                       0   5   <                @      B   )   ;   Q               ?   4         ,          /   +              =      8   $   P          2           9       U          6      (      E   G   :       &   	               I   #      
                     D   L                 J       1         -   O           T              F       7   K    Add endpoint Add group An error has occurred or this endpoint field already exists. Please try again. Are you sure that you want to delete this endpoint? Choose a color for menu items. Choose colour of menu items on mouse hover. Choose the color of the Logout background on mouse hover. Choose the color of the Logout background. Choose the color of the Logout text on mouse hover. Choose the color of the Logout text. Choose the default endpoint for "My account" page Choose the position of the menu in "My Account" page (only for sidebar style) Choose the style for the "My Account" menu Custom Avatar Custom endpoint content. Leave it black to use default content. Customize My Account Page Dashboard Default endpoint Edit Account Edit Address Endpoint Endpoint Options Endpoint custom content Endpoint icon Endpoint icon for "My Account" menu option Endpoint label Endpoint slug Endpoints General Options Gift Cards Group Group label Hide Income/Expenditure History Invalid order. Label for new endpoint titleName Left Let users upload a custom avatar as their profile picture. Logout Logout background color Logout background color on hover Logout color Logout color on hover Make a Deposit Manage Endpoints Membership Plans Membership Plans: Menu item color Menu item color on hover Menu item for this endpoint in "My Account". Menu style My Account My Downloads My Membership My Orders My Quotes My Subscription My Subscriptions My Waiting List My Wishlist No icon One click checkout Payment Methods Quote #%s Remove Reset to default Restrict endpoint visibility to the following user role(s). Right Saved Cards Settings Show Show open Show the group open by default. (Please note: this option is valid only for "Sidebar" style) Sidebar Sidebar position Style Options Tab Text appended to your page URLs to manage new contents in account pages. It must be unique for every page. There are no available downloads yet. There are no orders yet. Upload Upload your avatar User roles YITH WooCommerce Customize My Account Page is enabled but not effective. It requires WooCommerce in order to work. sub item Project-Id-Version: YITH WooCommerce Customize My Account Page
POT-Creation-Date: 2018-09-18 15:22+0200
PO-Revision-Date: 2018-09-18 15:22+0200
Last-Translator: 
Language-Team: YIThemes <plugins@yithemes.com>
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.1.1
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: _e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;__
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: ../plugin-fw
 Voeg eindpunt toe Groep toevoegen Er is iets misgegaan of dit eindpunt veld bestaat al. Probeer het opnieuw alstublieft. Weet u zeker dat u dit wilt verwijderen? Kies een kleur voor de menu items. Kies kleur van menu items op aanwijzer. Kies de kleur van de Uitloggen achtergrond op de aanwijzer. Kies de kleur van de Uitloggen achtergrond. Kies de kleur van de Uitloggen tekst op de aanwijzer. Kies de kleur van de Uitloggen tekst. Kies een default eindpunt voor "Mijn Account" pagina Kies de positie van mijn menu in "Mijn Account" pagina (alleen voor sidebar stijl) Kiest de stijl voor het "Mijn Account" menu Aangepaste Avatar Aangepaste eindpunt inhoud. Laat leeg als u de default inhoud wilt gebruiken. Customize My Account Page Dashboard Default eindpunt Account bewerken Adres bewerken Eindpunt Eindpunt opties Eindpunt aangepaste inhoud Eindpunt icoon Eindpunt icoon voor "My Account" menu optie Eindpunt label Eindpunt slug Eindpunten Algemene opties Cadeaubonnen Groep Groep label Verbergen Inkomsten/uitgaven geschiedenis Onjuiste bestelling. Name Links Laat gebruikers aangepaste avatars uploaden als hun profielfoto. Uitloggen Uitloggen achtergrondkleur Uitloggen achtergrondkleur op aanwijzer Uitloggen kleur Uitloggen kleur op aanwijzer Doe een storting Beheer eindpunten Lidmaatschap plannen Lidmaatschap plannen: Menu item kleur Menu item kleur op aanwijzer Menu item voor dit eindpunt in "My Account". Menustijl Mijn Account Mijn Downloads Mijn lidmaatschap Mijn bestellingen Mijn offertes Mijn subscriptie Mijn subscripties Mijn wachtlijst Mijn wensenlijst Geen icoon Een klik checkout Betaalmethoden Offerte #%s Verwijderen Resetten naar standaard Beperkte eindpunt weergave voor de volgende gebruikersrol(len). Rechts Opgeslagen kaarten Instellingen Tonen Toon open Toon de open groep met default. (Let op: deze optie is alleen geldig voor "Sidebar" stijl) Sidebar Sidebar positie Stijlopties Tab Tekst bijgevoegd op uw pagina URLs om nieuwe inhoud in account pagina´s te beheren. Dit moet uniek zijn voor iedere pagina. Er zijn nog geen downloads beschikbaar. Er zijn nog geen bestellingen. Upload Upload uw avatar Gebruikersrollen YITH WooCommerce Customize My Account Page is ingeschakeld maar niet werkend. Het heeft WooCommerce nodig om te werken. sub item 